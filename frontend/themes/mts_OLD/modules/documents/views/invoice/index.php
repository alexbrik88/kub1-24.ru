<?php

use common\components\date\DateHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\widgets\ActiveForm;
use common\models\document\Invoice;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $prompt backend\models\Prompt */
/* @var $user Employee */

$this->title = $message->get(Message::TITLE_PLURAL);

$user = Yii::$app->user->identity;
$company = $user->company;

$countInvoice = \common\models\document\Invoice::find()->where(['and',
    ['invoice_status_author_id' => Yii::$app->user->identity->id],
    ['type' => $ioType],
])->count();

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
    'ioType' => $ioType,
]);
$canPay = Yii::$app->user->can(permissions\Cash::CREATE) &&
    Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW) &&
    Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS);

$dropItems = [];
if ($canCreate) {
    $dropItems[] = [
        'label' => 'Создать Акты',
        'url' => '#many-create-act',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один Акт',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-act',
            'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать ТН',
        'url' => '#many-create-packing-list',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    if ($company->companyTaxationType->osno) {
        $dropItems[] = [
            'label' => 'Создать СФ',
            'url' => '#many-create-invoice-facture',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать одну СФ',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-invoice-facture',
                'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Создать УПД',
        'url' => '#many-create-upd',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-upd',
            'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
}
if ($canIndex) {
    if (!$canCreate && $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_MANAGER) {
        $dropItems[] = [
            'label' => 'Создать один Акт',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-act',
                'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один УПД',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-upd',
                'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        if ($company->companyTaxationType->osno) {
            $dropItems[] = [
                'label' => 'Создать одну СФ',
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'create-one-invoice-facture',
                    'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
                ],
            ];
        }
    }
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
        $dropItems[] = [
            'label' => 'Без Акта',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'act-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'Акт',
                    'attribute_text' => 'Без Акта',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_ACT,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без ТН',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'packing-list-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'ТН',
                    'attribute_text' => 'Без ТН',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_PACKING_LIST,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без УПД',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'upd-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'УПД',
                    'attribute_text' => 'Без УПД',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_UPD,
                ],
            ],
        ];
    }

}
$invCount = 0;
if ($company->isFreeTariff) {
    $invCount = $company->getInvoiceLimitForFreeTariff() - $company->freeTariffInvoicesCount;
} elseif ($company->getIsTrialNotPaid()) {
    $invCount = $company->getInvoiceLimitForTrialTariff() - $company->trialTariffInvoicesCount;
}
?>

<?php /*Pjax::begin([
    'id' => 'invoice-auto-toggle-pjax',
    'timeout' => 5000,
    'linkSelector' => '.invoice-auto-toggle',
])*/ ?>

<?= Yii::$app->session->getFlash('invoiceFactureError'); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="title"><?= $message->get(Message::TITLE_PLURAL); ?></div>
        </div>
        <div class="col-md-6">
            <?php if (Yii::$app->user->can(\frontend\rbac\permissions\document\Invoice::CREATE, ['ioType' => $ioType])): ?>
                <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                    <a class="add_btn btn-mts btn-mts-red" href="<?= Url::toRoute(['create', 'type' => $ioType]); ?>">
                        <i class="plus"></i>
                        Добавить
                    </a>
                <?php else : ?>
                    <a href="#" class="add_btn action-is-limited" <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                        <i></i>
                        <?= $message->get(Message::DOCUMENT_CREATE); ?>
                    </a>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="summary-block-mts">
                <?= frontend\modules\documents\widgets\StatisticWidget::widget([
                    'type' => $ioType,
                    'outerClass' => 'col-md-3 col-sm-3',
                    'contractorId' => $searchModel->contractor_id,
                    'invoiceStatusId' => array_filter(is_array($searchModel->invoice_status_id) ? $searchModel->invoice_status_id : explode(',', $searchModel->invoice_status_id)),
                    'searchModel' => $searchModel,
                    'clickable' => true,
                    'id' => 'widgets',
                ]); ?>
                <div class="col-md-3 col-sm-3" style="padding:0 12px">
                    <div style="margin-top:12px">
                        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'btn-mts btn-mts-calendar btn-mts-cal width100']); ?>
                    </div>
                    <?php if ($ioType == Documents::IO_TYPE_OUT): ?>
                        <div style="margin-top:12px">
                            <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) && in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])): ?>
                                <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), ['class' => 'btn-mts btn-mts-white width100']); ?>
                            <?php endif; ?>
                        </div>
                        <div style="margin-top:12px">
                            <?= $this->render('_invoice-auto-toggle') ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-3">
            <?= Html::a('', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link small-btn-mts del',
                'title' => 'Скачать в Excel',
                'style' => 'margin-right:8px'
            ]); ?>
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'invoice_scan',
                    ],
                    [
                        'attribute' => 'invoice_paylimit',
                    ],
                    [
                        'attribute' => 'invoice_paydate',
                    ],
                    [
                        'attribute' => 'invoice_act',
                    ],
                    [
                        'attribute' => 'invoice_paclist',
                    ],
                    [
                        'attribute' => 'invoice_waybill',
                    ],
                    [
                        'attribute' => 'invoice_invfacture',
                    ],
                    [
                        'attribute' => 'invoice_upd',
                    ],
                    [
                        'attribute' => 'invoice_author',
                        'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
                    ],
                    [
                        'attribute' => 'invoice_comment',
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-9 col-sm-9">
            <div class="search_form">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <?= Html::activeInput('text', $searchModel, 'byNumber', [
                    'placeholder' => 'Номер счета или название контрагента',
                    'class' => 'mts-text-input'
                ]); ?>
                <?= Html::submitButton('Найти', ['class' => 'btn-mts btn-mts-red']) ?>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::begin([
        'method' => 'POST',
        'action' => ['generate-xls'],
        'id' => 'generate-xls-form',
    ]); ?>
    <?php ActiveForm::end(); ?>

    <?= $this->render('_invoices_table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'type' => $ioType,
        'company' => $company,
    ]); ?>

    <?php if ($countInvoice == 0) : ?>
        <?= \frontend\widgets\PromptWidget::widget([
            'prompt' => $prompt,
        ]); ?>
    <?php endif; ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="ico-print"></i> <span class="caption">Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn-mts btn-mts-gray multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="ico-send"></i> <span class="caption">Отправить</span>', null, [
            'class' => 'btn-mts btn-mts-gray document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<i class="ico-bank"></i> <span class="caption">Платежка</span>', '#many-charge', [
            'class' => '',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a('<i class="ico-check"></i> <span class="caption">Оплачены</span>', '#many-paid-modal', [
            'class' => 'btn-mts btn-mts-gray',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a('<i class="ico-trash"></i> <span class="caption">Удалить</span>', '#many-delete', [
            'class' => 'btn-mts btn-mts-gray',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn-mts btn-mts-gray dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>
<?php /*Pjax::end();*/ ?>

<div class="modal fade confirm-modal" id="many-send-error"
     tabindex="-1"
     role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Ошибка
                    при отправке счетов</h3>
            </div>
            <div class="modal-body">
                <div class="form-body">
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($canPay) : ?>
    <?php Modal::begin([
        'id' => 'many-paid-modal',
        'options' => [
            'data-url' => Url::to(['add-flow-form', 'type' => $ioType]),
        ]
    ]); ?>
    <div id="many-paid-content"></div>
    <?php Modal::end(); ?>

    <?php $this->registerJs('
                    $(document).on("shown.bs.modal", "#many-paid-modal", function() {
                        var idArray = $(".joint-operation-checkbox:checked").map(function () {
                            return $(this).closest("tr").data("key");
                        }).get();
                        var url = this.dataset.url + "&id=" + idArray.join();
                        $.post(url, function(data) {
                            $("#many-paid-content").html(data);
                            $("#many-paid-content input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
                            $("#many-paid-content input:radio:not(.md-radiobtn)").uniform();
                        })
                    });
                    $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
                        $("#many-paid-content").html("");
                    });
                ') ?>
<?php endif ?>
<?php if ($canDelete) : ?>
    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите удалить
                            выбранные счета?
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                'data-url' => Url::to(['many-delete', 'type' => $ioType]),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">Нет
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>


<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#many-charge", function () {
        var countChecked = 0;
        var totalSummary = 0;
        var data = {};
        var payedInvoices = {};
        var mainBlock = $(".main-block");
        var paymentInvoiceBlock = $(".payment-invoices-block");

        $(this).find(".generated-row").remove();
        mainBlock.show();
        paymentInvoiceBlock.hide();

        $(".joint-operation-checkbox:checked").each(function(){
            var price = $(this).closest("tr").find(".price").data("remaining-amount");
            var contractor = $(this).closest("tr").find(".contractor-cell a");
            var contractorID = contractor.data("id");
            var contractorTitle = contractor.attr("title");
            var invoiceNumber = $(this).closest("tr").find(".document_number span").data("full-name");

            price = parseFloat(price.replace(",", "."));
            if (price > 0) {
                if (data[contractorID] == undefined) {
                    data[contractorID] = {
                        "id": contractorID,
                        "title": contractorTitle,
                        "invoiceCount": 1,
                        "summary": price,
                    };
                } else {
                    data[contractorID]["summary"] = data[contractorID]["summary"] + price;
                    data[contractorID]["invoiceCount"] = data[contractorID]["invoiceCount"] + 1;
                }
                totalSummary += price;
                countChecked++;
            } else {
                payedInvoices[invoiceNumber] = invoiceNumber;
            }
        });

        var number = 1;
        for (var key in data) {
            var $template = $("#many-charge .many-charge-text .template").clone();
            $template.find(".number").text(number);
            $template.find(".contractor-title").text(data[key]["title"]);
            $template.find(".invoice-count").text(data[key]["invoiceCount"]);
            $template.find(".summary").text(number_format(data[key]["summary"], 2, ",", " "));
            $template.css("display", "block");
            $template.removeClass("template");
            $template.addClass("generated-row");
            $("#many-charge .many-charge-text.main-block").append($template);
            number++;
        };
        var $totalRow = $("#many-charge .many-charge-text.main-block .total-row").clone();
        $("#many-charge .many-charge-text.main-block .total-row").remove();
        $totalRow.find(".total-invoice-count").text(countChecked);
        $totalRow.find(".total-summary").text(number_format(totalSummary, 2, ",", " "));
        $("#many-charge .many-charge-text.main-block").append($totalRow);

        if (countChecked == 0) {
            mainBlock.hide();
            if (Object.keys(payedInvoices).length > 0) {
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.payment-invoices-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
            paymentInvoiceBlock.show();
        } else {
            if (Object.keys(payedInvoices).length > 0) {
               $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;margin-top:15px;>Эти счета уже оплачены:</span>");
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
        }
    });

    $(".act-not-need, .packing-list-not-need, .upd-not-need").click(function (e) {
        $("#not-need-document-modal span.document-name").text($(this).data("name"));
        $("#not-need-document-modal span.document-attribute_text").text($(this).data("attribute_text"));
        $("#not-need-document-modal .modal-not-need-document-submit").data("url",  $("#not-need-document-modal .modal-not-need-document-submit").data("url") + $(this).data("param"));
    });
'); ?>