<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */

$this->title = 'Создание контрагента';
$this->context->layoutWrapperCssClass = 'create-counterparty';
$model->contact_is_director = true;
$model->chief_accountant_is_director = true;
?>

<?= $this->render('form/_form', [
    'model' => $model,
    'face_type_opt' => 0,
]) ?>
