<?php

use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Autoinvoice;
use common\models\EmployeeCompany;
use frontend\models\ContractorSearch;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;
use common\widgets\Modal;
use common\models\Company;
use common\components\ImageHelper;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\widgets\Pjax;
use common\models\employee\EmployeeClick;
use yii\bootstrap\Dropdown;
use common\components\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $searchModel frontend\models\ContractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */
/* @var $prompt backend\models\Prompt */
/* @var $company Company */
/* @var $user Employee */

$this->title = Contractor::$contractorTitle[$type];

$countContractor = Contractor::find()->where(['and',
    ['employee_id' => Yii::$app->user->identity->id],
    ['type' => $type],
])->count();

$query = clone $dataProvider->query;

$contractorIdArray = $query->column();
if ($searchModel->duplicate) {
    $emptyMessage = 'Поиск дублей по ИНН - дубли не обнаружены';
} else {
    if ($type == Contractor::TYPE_CUSTOMER) {
        $emptyMessage = 'Вы еще не добавили ни одного покупателя';
    } elseif ($type == Contractor::TYPE_SELLER) {
        $emptyMessage = 'Вы еще не добавили ни одного поставщика';
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$user = Yii::$app->user->identity;
$userConfig = $user->config;
$company = $user->company;

$hasFilters = $searchModel->activityStatus != ContractorSearch::ACTIVITY_STATUS_ACTIVE;
$autoinvoiceFiterItems = array_merge(['' => 'Все', 'no' => 'Нет'], Autoinvoice::$STATUS);
unset($autoinvoiceFiterItems[Autoinvoice::DELETED]);
?>
<div class="contractor-index">
    <?php if (!$company->getCashBankStatementUploads()->exists() && Yii::$app->user->identity->showAlert) : ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×
            </button>
            <a href="<?= Url::to(['cash/bank/index']) ?>">Загрузите выписку</a>
            из банка и все имеющиеся у вас Покупатели и Поставщики будут созданы автоматически.
        </div>
        <?php $this->registerJs('
            $(document).on("click", "#contractor_alert_close", function() {
                $.ajax({"url": "' . Url::to(['alert-close']) . '"});
            });
        '); ?>
    <?php endif; ?>

    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?php if (frontend\rbac\permissions\Contractor::CREATE): ?>
                <a href="<?= Url::to(['/contractor/create', 'type' => $type]); ?>"
                   class="btn yellow">
                    <i class="fa fa-plus"></i>
                    ДОБАВИТь
                </a>
            <?php endif; ?>
        </div>

        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>

    <div class="row">
        <?= frontend\modules\documents\widgets\StatisticWidget::widget([
            'type' => $type,
            'outerClass' => 'col-md-3 col-sm-3',
            'contractorStatus' => $searchModel->activityStatus,
            'contractorId' => $contractorIdArray,
        ]); ?>
        <div class="col-md-3 col-sm-3">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
            <?php if ($type == Documents::IO_TYPE_OUT): ?>
                <div class="btn-group pull-right title-buttons" style="width: 100%;margin-top: 49px;">
                    <?php if (in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])): ?>
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                            'class' => 'btn yellow',
                            'style' => 'position: absolute;right: 0;bottom: 44px;width: 153px;',
                        ]); ?>
                    <?php endif; ?>
                    <a href="<?= Url::to(['/contractor/sale-increase',]); ?>"
                       data-url="<?= Url::to(['/site/employee-click', 'type' => EmployeeClick::SALE_INCREASE_TYPE]); ?>"
                       class="btn yellow employee-click_trigger"
                       style="width: 100%;">
                        Увеличение онлайн продаж
                    </a>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-sm-12 table-icons" style="margin-top: -20px;">
            <?= TableConfigWidget::widget([
                'items' => array_filter([
                    [
                        'attribute' => 'contractor_contact',
                    ],
                    [
                        'attribute' => 'contractor_phone',
                    ],
                    [
                        'attribute' => 'contractor_agreement',
                    ],
                    [
                        'attribute' => 'contractor_notpaidcount',
                    ],
                    [
                        'attribute' => 'contractor_notpaidsum',
                    ],
                    [
                        'attribute' => 'contractor_paidcount',
                    ],
                    [
                        'attribute' => 'contractor_paidsum',
                    ],
                    [
                        'attribute' => $type == Contractor::TYPE_CUSTOMER ? 'contractor_autionvoice' : null,
                    ],
                    [
                        'attribute' => 'contractor_responsible',
                    ],
                ]),
            ]); ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                <?= Html::a('<i class="fa fa-file-excel-o"></i>', Url::current(['xls' => 1]), [
                    'class' => 'get-xls-link pull-right',
                    'title' => 'Скачать в Excel',
                ]); ?>
            <?php endif ?>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption caption_for_input" style="width: 23%!important;">
                <?= $type == Contractor::TYPE_CUSTOMER ? 'Список покупателей' : 'Список поставщиков'; ?>:
                <?= $dataProvider->totalCount ?>
            </div>
            <div class="tools search-tools tools_button col-sm-4">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                            <?= $form->field($searchModel, 'name')->textInput([
                                'placeholder' => 'Поиск...',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
            <div class="filter-block pull-right">
                <div class="dropdown pull-right">
                    <?= Html::a('Фильтр  <span class="caret"></span>', null, [
                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => $hasFilters ? 'color: #f3565d!important;border-color: #f3565d!important;' : 'color: #def1ff;',
                    ]); ?>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="row">
                                <span class="filter-label">Показывать</span>
                                <?= Html::activeDropDownList($searchModel, 'activityStatus', [
                                    ContractorSearch::ACTIVITY_STATUS_ALL => 'Все',
                                    ContractorSearch::ACTIVITY_STATUS_ACTIVE => 'Активные',
                                    ContractorSearch::ACTIVITY_STATUS_INACTIVE => 'Неактивные',
                                ], [
                                    'class' => 'form-control',
                                    'data-id' => 'activityStatus',
                                ]); ?>
                            </div>
                        </li>
                        <li style="border-bottom: 0;">
                            <div class="form-actions">
                                <div class="row action-buttons buttons-fixed">
                                    <div class="spinner-button col-sm-3 col-xs-3"
                                         style="width: 35%;padding-left: 0;">
                                        <?= Html::button('Найти', [
                                            'class' => 'apply-filters btn darkblue widthe-100',
                                        ]); ?>
                                    </div>
                                    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 30%;"></div>
                                    <div class="spinner-button col-sm-1 col-xs-1"
                                         style="width: 35%;padding-right: 0;">
                                        <?= Html::button('Сбросить', [
                                            'class' => 'apply-default-filters btn darkblue widthe-100',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="pull-right" style="padding-right: 15px;">
                    <?= Html::a('<img src="/img/icons/duplicate-ico.png" style="top: -1px;left: -3px;position: relative;">Дубли', 'javascript:;', [
                        'class' => 'btn btn-default btn-sm pull-left duplicate-contractor' . ($searchModel->duplicate ? ' active' : null),
                        'style' => 'padding: 4px 9px 3px;background: transparent;border: 1px solid #def1ff;color: #def1ff;',
                    ]); ?>
                </div>
            </div>
            <div id="mass_actions" class="actions joint-operations col-sm-5 pull-right"
                 style="display: none;width: auto;padding-right: 0!important;padding-top: 8px!important;">
                <div class="pull-right" style="position: relative; display: inline-block;">
                    <div class="dropdown">
                        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> Изменить', null, [
                            'class' => 'btn btn-default btn-sm dropdown-toggle',
                            'data-toggle' => 'dropdown',
                            'style' => 'height: 28px; color: #def1ff;margin-right: 15px;',
                        ]); ?>
                        <?= Dropdown::widget([
                            'items' => [
                                [
                                    'label' => 'Изменить ответственного',
                                    'url' => '#change_responsible_modal',
                                    'linkOptions' => [
                                        'data-toggle' => 'modal',
                                    ],
                                ],
                                [
                                    'label' => 'Изменить статус',
                                    'url' => '#change_status_modal',
                                    'linkOptions' => [
                                        'data-toggle' => 'modal',
                                    ],
                                ],
                            ],
                            'options' => [
                                'style' => 'right: -42px !important; left: auto; top: 25px;',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'emptyText' => $emptyMessage,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],

                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center pad0',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center pad0-l pad0-r',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::checkbox('Contractor[' . $data->id . '][checked]', false, [
                                    'class' => 'joint-operation-checkbox',
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Название',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data->getTitle(true), ['contractor/view', 'type' => $data->type, 'id' => $data->id]);
                            },
                        ],
                        [
                            'attribute' => 'contact',
                            'label' => 'Контакт',
                            'class' => DropDownSearchDataColumn::className(),
                            'headerOptions' => [
                                'class' => 'col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                            ],
                            'filter' => $searchModel->getContactItemsByQuery($dataProvider->query),
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data->realContactName ?: '';
                            },
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => 'Телефон',
                            'headerOptions' => [
                                'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data->realContactPhone ?: '';
                            },
                        ],
                        [
                            'attribute' => 'agree',
                            'label' => 'Договор',
                            'headerOptions' => [
                                'class' => 'col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                            ],
                            'format' => 'html',
                            'value' => 'lastAgreementStr',
                        ],
                        [
                            'attribute' => 'not_paid_count',
                            'label' => 'Счетов не оплачено',
                            'headerOptions' => [
                                'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                            ],
                            'value' => function (Contractor $data) {
                                return TextHelper::moneyFormat($data->not_paid_count);
                            },
                        ],
                        [
                            'attribute' => 'not_paid_sum',
                            'label' => 'Не оплачено счетов на сумму',
                            'headerOptions' => [
                                'class' => 'col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                            ],
                            'value' => function (Contractor $data) {
                                return TextHelper::invoiceMoneyFormat($data->not_paid_sum, 2);
                            },
                        ],
                        [
                            'attribute' => 'paid_count',
                            'label' => 'Счетов оплачено',
                            'headerOptions' => [
                                'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                            ],
                            'value' => function (Contractor $data) {
                                return TextHelper::moneyFormat($data->paid_count);
                            },
                        ],
                        [
                            'attribute' => 'paid_sum',
                            'label' => 'Оплачено счетов на сумму',
                            'headerOptions' => [
                                'class' => 'col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                            ],
                            'value' => function (Contractor $data) {
                                return TextHelper::invoiceMoneyFormat($data->paid_sum, 2);
                            },
                        ],
                        [
                            'attribute' => 'autoinvoice',
                            'label' => 'АвтоСчет',
                            'headerOptions' => [
                                'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                            ],
                            'filter' => $autoinvoiceFiterItems,
                            'value' => function (Contractor $data) {
                                return ArrayHelper::getValue(Autoinvoice::$STATUS, $data->autoinvoice, '');
                            },
                            'visible' => $type == Contractor::TYPE_CUSTOMER,
                        ],
                        [
                            'header' => $searchModel->duplicate ? 'Дубли' : '',
                            'contentOptions' => [
                                'style' => $searchModel->duplicate ? 'color:#f3565d;' : '',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $data) use ($searchModel) {
                                if (!$searchModel->duplicate) {
                                    if ($data->company_id !== null) {
                                        return Html::a('<i class="fa fa-plus" style="padding: 0;"></i> Счёт', [
                                            'documents/invoice/create',
                                            'type' => $data->type,
                                            'contractorId' => $data->id,
                                        ], [
                                            'class' => 'btn btn-sm yellow',
                                        ]);
                                    }

                                    return '';
                                }

                                return $data->getDuplicateDifference();
                            },
                            'visible' => Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                'ioType' => $type, // same value
                            ]),

                        ],
                        [
                            'attribute' => 'responsible_employee_id',
                            'label' => 'От&shy;вет&shy;ствен&shy;ный',
                            'encodeLabel' => false,
                            'class' => DropDownSearchDataColumn::className(),
                            'headerOptions' => [
                                'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                                'width' => '17%',
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                            ],
                            'filter' => $searchModel->getResponsibleItemsByQuery($dataProvider->query),
                            'value' => function (Contractor $data) use ($company) {
                                $employee = EmployeeCompany::findOne([
                                    'employee_id' => $data->responsible_employee_id,
                                    'company_id' => $company->id,
                                ]);

                                return $employee ? $employee->getFio(true) : '';
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php if ($countContractor == 0) : ?>
        <?= \frontend\widgets\PromptWidget::widget([
            'prompt' => $prompt,
        ]); ?>
    <?php endif; ?>
</div>

<?php Modal::begin([
    'header' => '<h1>Изменить ответственного</h1>',
    'id' => 'change_responsible_modal',
]); ?>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="form-group" style="margin-left: 0;margin-right: 0;">
            <label class="col-md-12 control-label width-label font-bold">
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </label>
        </div>
        <div class="form-group" style="margin-left: 0;margin-right: 0;">
            <label for="contractor-responsible_employee" class="col-md-3 control-label width-label font-bold">
                Ответственный
            </label>
            <div class="col-md-9">
                <?= Select2::widget([
                    'id' => 'contractor-responsible_employee',
                    'name' => 'responsibleEmployee',
                    'pluginOptions' => [
                        'width' => '97%',
                        'placeholder' => '',
                    ],
                    'data' => ArrayHelper::map($company->getEmployeeCompanies()
                        ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                        ->orderBy([
                            'lastname' => SORT_ASC,
                            'firstname' => SORT_ASC,
                            'patronymic' => SORT_ASC,
                        ])->all(), 'employee_id', 'fio'),
                ]); ?>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row action-buttons" id="buttons-fixed">
        <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
            <?= Html::a('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', null, [
                'class' => 'btn darkblue darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button modal-many-change-responsible',
                'data-url' => Url::to(['/contractor/change-responsible', 'type' => $type]),
                'data-style' => 'expand-right',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                'class' => 'btn darkblue darkblue widthe-100 hidden-lg modal-many-change-responsible',
                'data-url' => Url::to(['/contractor/change-responsible', 'type' => $type]),
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%;">
            <?= Html::a('Отменить', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                'data-dismiss' => 'modal',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-lg back',
                'data-dismiss' => 'modal',
                'title' => 'Отменить',
            ]); ?>
        </div>
    </div>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h1>Изменить статус</h1>',
    'id' => 'change_status_modal',
]); ?>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="form-group" style="margin-left: 0;margin-right: 0;">
            <label for="contractor-responsible_employee" class="col-md-8 control-label width-label font-bold"
                   style="padding-top: 1px;width: 59%;">
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить статус
                на:
            </label>
            <div class="col-md-4" style="width: 41%;">
                <?= Html::radioList('status', Contractor::ACTIVE, [
                    Contractor::ACTIVE => 'Активный',
                    ContractorSearch::INACTIVE => 'Не активный',
                ], [
                    'id' => 'contractor-status',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row action-buttons" id="buttons-fixed">
        <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
            <?= Html::a('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', null, [
                'class' => 'btn darkblue darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button modal-many-change-status',
                'data-url' => Url::to(['/contractor/change-status', 'type' => $type]),
                'data-style' => 'expand-right',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                'class' => 'btn darkblue darkblue widthe-100 hidden-lg modal-many-change-status',
                'data-url' => Url::to(['/contractor/change-status', 'type' => $type]),
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%;">
            <?= Html::a('Отменить', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                'data-dismiss' => 'modal',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-lg back',
                'data-dismiss' => 'modal',
                'title' => 'Отменить',
            ]); ?>
        </div>
    </div>
</div>
<?php Modal::end(); ?>

<?php if ($company->show_popup_sale_increase && $type == Contractor::TYPE_CUSTOMER): ?>
    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Повышение продаж
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_BILL,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 6,
            'description' => 'ПРОДАВАЙТЕ БОЛЬШЕ без УЧАСТИЯ ПРОДАВЦОВ<br>
                    Ваш сайт сможет самостоятельно выставлять счета покупателям.<br>
                    Создайте личные ОНЛАЙН-КАБИНЕТЫ для покупателей с доступом к складу.',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/contractor/sale-increase']),
            'image' => ImageHelper::getThumb('img/modal_registration/block-6.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => 5,
            'nextModal' => null,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
<?php $this->registerJs('
    $(document).ready(function () {
        $(".filter-block ul.dropdown-menu").click(function(e) {
            e.stopPropagation();
        });
        $(".filter-block .apply-filters").click(function () {
            var $filterData = [];

            $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
                $filterData["ContractorSearch[" + $(this).data("id") + "]"] = $(this).val();
            });
            applyFilter($filterData);
        });
        $(".filter-block .apply-default-filters").click(function () {
            var $filterData = [];

            $(".filter-block ul.dropdown-menu select").each(function () {
                var $attrName = $(this).data("id");
                var $value = 0;
                if ($attrName == "activityStatus") {
                    $value = 1;
                }
                $filterData["ContractorSearch[" + $attrName + "]"] = $value;
            });
            $(".filter-block ul.dropdown-menu input").each(function () {
                $filterData["ContractorSearch[" + $(this).data("id") + "]"] = "";
            });
            applyFilter($filterData);
        });

        $(".duplicate-contractor").click(function (e) {
            var $filterData = [];

            $filterData["ContractorSearch[duplicate]"] = 0;
            if (!$(this).hasClass("active")) {
                $filterData["ContractorSearch[duplicate]"] = 1;
            }
            applyFilter($filterData);
        });

        function applyFilter(data) {
            var kvp = document.location.search.substr(1).split("&");
            for (var key in data) {
                var $key = key;
                var $value = data[key];
                if (kvp !== "") {
                    var i = kvp.length;
                    var x;
                    while (i--) {
                        x = kvp[i].split("=");
                        if (x[0] == $key) {
                            x[1] = $value;
                            kvp[i] = x.join("=");
                            break;
                        }
                    }
                    if (i < 0) {
                        kvp[kvp.length] = [$key, $value].join("=");
                    }
                } else {
                    kvp[0] = $key + "=" + $value;
                }
            }
            document.location.search = kvp.join("&");
        }
    });
'); ?>

