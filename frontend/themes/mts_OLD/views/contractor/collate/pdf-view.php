<?php

use common\components\TextHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Contractor;
use common\models\company\CompanyType;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$companyName = $model->company->getTitle(true);
$contractorName = $model->contractor->getShortName(true);
$agreement = $model->contractor->getAgreements()->orderBy(['document_date' => SORT_ASC])->one();

$signatureLink = (empty($addStamp) || !$model->company->chief_signature_link) ? null:
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

$printLink = (empty($addStamp) || !$model->company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

?>

<style type="text/css">
.collate-table th {
    font-weight: bold;
    text-align: center;
}
.collate-table th,
.collate-table td {
    border: 1px solid #ccc;
    padding: 5px 10px;
}
.parties-table td {
    border: none;
}
</style>

<div style="margin: 0 0 15px; text-align: center; font-size: 33px;">
    Акт сверки
</div>
<div style="margin: 15px 0 20px; text-align: center; font-size: 14px;">
    и взаимных расчетов за период <?= $model->dateFrom . ' - ' . $model->dateTill ?>
    <br>
    между <?= $companyName ?> и <?= $contractorName ?>
    <?php if ($agreement) : ?>
        по <?= $agreement->agreementType->name_dative ?>
        № <?= $agreement->document_number ?>
        от <?= date_create_from_format('Y-m-d', $agreement->document_date)->format('d.m.Y') ?>
    <?php endif ?>
</div>

<table id="collate-table" class="collate-table" cellspacing="0" style="width: 100%">
    <tr>
        <th style="width: 15%;">Дата</th>
        <th style="width: 35%;">Документ</th>
        <th style="width: 20%;">Дебет</th>
        <th style="width: 20%;">Кредит</th>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: bold;">Сальдо начальное</td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->initBalanceByFlows ? number_format($model->initBalanceByFlows / 100, 2, ',', ' ') : '';
            } else {
                echo $model->initBalanceByDocs ? number_format($model->initBalanceByDocs / 100, 2, ',', ' ') : '';
            } ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->initBalanceByDocs ? number_format($model->initBalanceByDocs / 100, 2, ',', ' ') : '';
            } else {
                echo $model->initBalanceByFlows ? number_format($model->initBalanceByFlows / 100, 2, ',', ' ') : '';
            } ?>
        </td>
    </tr>
    <?php if ($model->tableRows) : ?>
        <?= $model->tableRows ?>
    <?php else : ?>
        <tr>
            <td colspan="4">За указанный период движений не было</td>
        </tr>
    <?php endif ?>
    <tr>
        <td colspan="2" style="font-weight: bold;">Обороты за период</td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->periodBalanceByFlows ? number_format($model->periodBalanceByFlows / 100, 2, ',', ' ') : '';
            } else {
                echo $model->periodBalanceByDocs ? number_format($model->periodBalanceByDocs / 100, 2, ',', ' ') : '';
            } ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->periodBalanceByDocs ? number_format($model->periodBalanceByDocs / 100, 2, ',', ' ') : '';
            } else {
                echo $model->periodBalanceByFlows ? number_format($model->periodBalanceByFlows / 100, 2, ',', ' ') : '';
            } ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: bold;">Сальдо конечное</td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->totalBalanceByFlows ? number_format($model->totalBalanceByFlows / 100, 2, ',', ' ') : '';
            } else {
                echo $model->totalBalanceByDocs ? number_format($model->totalBalanceByDocs / 100, 2, ',', ' ') : '';
            } ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?php if ($model->contractor->type == Contractor::TYPE_SELLER) {
                echo $model->totalBalanceByDocs ? number_format($model->totalBalanceByDocs / 100, 2, ',', ' ') : '';
            } else {
                echo $model->totalBalanceByFlows ? number_format($model->totalBalanceByFlows / 100, 2, ',', ' ') : '';
            } ?>
        </td>
    </tr>
</table>

<div style="margin: 10px auto 20px; font-size: 14px;">
    По данным <?= $companyName ?> и <?= $contractorName ?>
    <br>
    на <?= $model->dateTill ?>
    <?php if ($model->totalBalanceByDocs) : ?>
        задолженность в пользу
        <?= ($model->contractor->type == Contractor::TYPE_SELLER ? $contractorName : $companyName) .
            ' ' . number_format($model->totalBalanceByDocs / 100, 2, ',', ' ') . ' руб.' ?>
        <br>
        (<?= TextHelper::amountToWords($model->totalBalanceByDocs / 100); ?>)
    <?php elseif ($model->totalBalanceByFlows) : ?>
        задолженность в пользу
        <?= ($model->contractor->type == Contractor::TYPE_SELLER ? $companyName : $contractorName) .
            ' ' . number_format($model->totalBalanceByFlows / 100, 2, ',', ' ') . ' руб.' ?>
        <br>
        (<?= TextHelper::amountToWords($model->totalBalanceByFlows / 100); ?>)
    <?php else : ?>
        задолженности нет
    <?php endif ?>
</div>
<div style="margin: 20px 0 ; background: url('<?= $signatureLink ?>'); background-repeat: no-repeat; background-position: left 80px;">
    <div style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: 165px bottom;">
        <table class="parties-table" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 47%; height: 80px; vertical-align: top;">
                    <?= $companyName ?>
                    <br>
                    <?= $model->company->chief_post_name ?>
                </td>
                <td style="width: 6%;"></td>
                <td style="width: 47%; height: 80px; vertical-align: top;">
                    <?= $contractorName ?>
                    <br>
                    <?= $model->contractor->director_post_name?>
                </td>
            </tr>
            <tr>
                <td style="width: 47%; height: 50px; border-bottom: 1px solid #333333; text-align: right; vertical-align: bottom;">
                    <?= ($name = $model->company->getChiefFio(true)) ? '(' . $name . ')' : ''; ?>
                </td>
                <td style="width: 6%;"></td>
                <td style="width: 47%; height: 50px; border-bottom: 1px solid #333333; text-align: right; vertical-align: bottom;">
                    <?= ($name = $model->contractor->getDirectorFio()) ? '(' . $name . ')' : ''; ?>
                </td>
            </tr>
            <tr>
                <td style="width: 47%; height: 70px; vertical-align: top;">
                    М.П.
                    <span class="pull-right">
                        <?= $model->dateSigned ? $model->dateSigned . 'г.' : ''; ?>
                    </span>
                </td>
                <td style="width: 6%;"></td>
                <td style="width: 47%; height: 70px; vertical-align: top;">
                    М.П.
                    <span class="pull-right">
                        <?= $model->dateSigned ? $model->dateSigned . 'г.' : ''; ?>
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>
