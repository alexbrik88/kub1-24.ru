<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<?= $this->render('pdf-view', ['model' => $model]) ?>

<div style="margin: -15px -15px 0; position: relative;">
    <table style="width: 100%; border-collapse: separate; border-spacing: 15px 0;">
        <tr>
            <td style="">
                <?= Html::a('Отправить', [
                    '/contractor/collate',
                    'step' => 'send',
                    'id' => $model->contractor->id,
                    'accounting' => $model->accounting,
                    'document' => $model->document,
                    'from' => $model->dateFrom,
                    'till' => $model->dateTill,
                ], [
                    'class' => 'btn darkblue text-white widthe-100 collate-pjax-link',
                    'title' => 'Отправить',
                ]) ?>
            </td>
            <td style="">
                <?= Html::a('Печать', [
                    '/contractor/collate',
                    'step' => 'print',
                    'id' => $model->contractor->id,
                    'accounting' => $model->accounting,
                    'document' => $model->document,
                    'from' => $model->dateFrom,
                    'till' => $model->dateTill,
                ], [
                    'class' => 'btn darkblue text-white widthe-100',
                    'title' => 'Печать',
                    'target' => '_blank'
                ]) ?>
            </td>
            <td>
                <span class="dropup">
                    <?= Html::a('Скачать', '#', [
                        'class' => 'btn darkblue text-white widthe-100',
                        'data-toggle' => 'dropdown'
                    ]); ?>
                    <?= Dropdown::widget([
                        'options' => [
                            'style' => 'width: 100%; min-width: 98px; border-color: #4276a4 !important;',
                            'class' => 'dropdown-menu-mini'
                        ],
                        'items' => [
                            [
                                'label' => '<span style="display: inline-block;">PDF</span> файл',
                                'encode' => false,
                                'url' => [
                                    '/contractor/collate',
                                    'step' => 'pdf',
                                    'id' => $model->contractor->id,
                                    'accounting' => $model->accounting,
                                    'document' => $model->document,
                                    'from' => $model->dateFrom,
                                    'till' => $model->dateTill,
                                ],
                                'linkOptions' => [
                                    'target' => '_blank',
                                    'style' => 'padding: 7px 0px; text-align: center;',
                                ]
                            ],
                            [
                                'label' => '<span style="display: inline-block;">Excel</span> файл',
                                'encode' => false,
                                'url' => [
                                    '/contractor/collate',
                                    'step' => 'excel',
                                    'id' => $model->contractor->id,
                                    'accounting' => $model->accounting,
                                    'document' => $model->document,
                                    'from' => $model->dateFrom,
                                    'till' => $model->dateTill,
                                ],
                                'linkOptions' => [
                                    'target' => '_blank',
                                    'style' => 'padding: 7px 0px; text-align: center;',
                                ]
                            ],
                        ],
                    ]);?>
                </span>
            </td>
            <td style="">
                <?= Html::a('Отменить', '#', [
                    'class' => 'btn darkblue text-white widthe-100',
                    'title' => 'Отменить',
                    'data-dismiss' => 'modal',
                    'aria-hidden' => 'true',
                ]) ?>
            </td>
        </tr>
    </table>
</div>

<?php $pjax->end(); ?>
