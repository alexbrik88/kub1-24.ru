<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$this->title = $model->title;
$this->registerJs('window.print();');
?>

<div class="page-content-in p-center pad-pdf-p">
<?= $this->render('pdf-view', ['model' => $model]) ?>
</div>
