<?php

use frontend\models\CollateForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<div class="modal-header" style="margin: -15px -15px 15px;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1>Акт сверки</h1>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'collate-form',
    'action' => ['/contractor/collate', 'step' => 'form', 'id' => $model->contractor->id],
    'class' => 'form-horizontal',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<?= $form->field($model, 'accounting', [
    'labelOptions' => [
        'class' => 'control-label width-label bold-text',
    ],
])->radioList(CollateForm::$accountingItems, [
    'style' => 'margin-left: -22px;',
    'item' => function ($index, $label, $name, $checked, $value) {
        return Html::radio($name, $checked, [
            'value' => $value,
            'label' => $label,
            'labelOptions' => [
                'class' => 'radio-inline',
            ],
        ]);
    },
]); ?>

<?= $form->field($model, 'document', [
    'labelOptions' => [
        'class' => 'control-label width-label bold-text',
    ],
])->radioList($model->documentItems, [
    'style' => 'margin-left: -22px;',
    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
        $disabled = !in_array((int) $value, $model->allowedDocIdArray);
        $isUpd = $value == CollateForm::DOC_UPD;
        return Html::radio($name, $checked, [
            'value' => $value,
            'label' => $label,
            'disabled' => $disabled,
            'labelOptions' => [
                'class' => 'radio-inline' . (($disabled && $isUpd) ? ' no-upd' : ''),
                'data-tooltip-content' => ($disabled && $isUpd) ? '#tooltip_doc_upd' : null,
            ],
        ]);
    },
]); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'dateFrom', [
            'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
            'labelOptions' => [
                'class' => 'control-label width-label bold-text',
            ],
            'wrapperOptions' => [
              'class' => 'col-md-5',
            ],
        ])->textInput(['class' => 'form-control date-picker']) ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'dateTill', [
            'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
            'labelOptions' => [
                'class' => 'control-label width-label bold-text',
            ],
            'wrapperOptions' => [
              'class' => 'col-md-5',
            ],
        ])->textInput(['class' => 'form-control date-picker']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'dateSigned', [
            'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
            'labelOptions' => [
                'class' => 'control-label width-label bold-text',
            ],
            'wrapperOptions' => [
              'class' => 'col-md-5',
            ],
        ])->textInput(['class' => 'form-control date-picker']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div style="margin-top: 20px; position: relative;">
    <table style="width: 100%">
        <tr>
            <td style="width: 50%;">
                <?= Html::submitButton('Сформировать', [
                    'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                    'form' => 'collate-form',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg',
                    'form' => 'collate-form',
                    'title' => 'Сформировать',
                ]); ?>
            </td>
            <td class="text-right" style="width: 50%;">
                <?= Html::tag('button', 'Отменить', [
                    'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                    'data-dismiss' => 'modal',
                    'aria-hidden' => 'true',
                ]); ?>
                <?= Html::tag('button', '<i class="fa fa-reply fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg',
                    'title' => 'Отменить',
                    'data-dismiss' => 'modal',
                    'aria-hidden' => 'true',
                ]); ?>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
$(".radio-inline.no-upd").tooltipster({
    "theme":["tooltipster-noir","tooltipster-noir-customized"],
    "trigger":"click",
    "contentAsHTML":true,
    "contentCloning": true
});
</script>

<?php $pjax->end(); ?>
