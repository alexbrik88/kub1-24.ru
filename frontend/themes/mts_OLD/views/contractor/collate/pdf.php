<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$this->title = $model->pdfTitle;
?>

<div class="page-content-in p-center pad-pdf-p">
<?= $this->render('pdf-view', ['model' => $model, 'addStamp' => true]) ?>
</div>
