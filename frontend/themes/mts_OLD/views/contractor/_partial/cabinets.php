<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.04.2018
 * Time: 8:12
 */

use common\models\Company;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use yii\data\ActiveDataProvider;
use frontend\models\StoreCompanyContractorSearch;
use common\models\store\StoreCompanyContractor;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\components\TextHelper;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\components\FilterHelper;
use frontend\models\Documents;
use yii\widgets\Pjax;
use common\models\Contractor;
use frontend\components\StatisticPeriod;
use common\models\service\Payment;
use php_rutils\RUtils;
use common\components\date\DateHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\employee\EmployeeRole;

/* @var $company Company
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel StoreCompanyContractorSearch
 * @var $payments Payment[]
 */

$dateRange = StatisticPeriod::getSessionPeriod();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$availableCabinetsCount = $company->getAvailableCabinetsCount();
$user = Yii::$app->user->identity;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<?php Pjax::begin([
    'id' => 'store-contractor-pjax',
    'timeout' => 10000,
]); ?>
    <div class="row" style="padding-top: 14px;">
        <div class="col-md-3 col-md-push-9">
            <?= RangeButtonWidget::widget([
                'cssClass' => 'doc-gray-button btn_select_days btn_row mrg-b-14',
                'pjaxSelector' => '#store-contractor-pjax',
            ]); ?>
        </div>
        <div class="col-md-9 col-md-pull-3 created-scroll">
            <?php if (!$payments): ?>
                <?php if ($canAddStoreCabinet): ?>
                    <?php if ($availableCabinetsCount == 5): ?>
                        <span style="font-size: 14px;display: block;margin-top: 14px;">
                            У вас есть 5 бесплатных кабинета.
                        </span>
                    <?php else: ?>
                        <span style="font-size: 14px;display: block;margin-top: 14px;">
                            У вас осталось <?= $availableCabinetsCount; ?> бесплатных кабинета.
                        </span>
                    <?php endif; ?>
                <?php else: ?>
                    <span style="font-size: 14px;display: block;margin-top: 14px;">
                        У вас не осталось бесплатных кабинетов. Перейдите в закладку
                        <?= Html::a('ОПЛАТА', 'javascript:;', [
                            'class' => 'payment-tab',
                        ]); ?>
                    </span>
                <?php endif; ?>
            <?php else: ?>
                <?php foreach ($payments as $payment): ?>
                    <span style="font-size: 14px;display: block;margin-top: 14px;">
                        У вас оплачено <?= $payment->getCabinetsCount() . ' ' .
                        RUtils::numeral()->choosePlural($payment->getCabinetsCount(), [
                            'кабинет', //1
                            'кабинета', //2
                            'кабинетов' //5
                        ]) . ' до ' . $payment->getExpiredAtCabinet(); ?>
                    </span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-md-3 col-sm-3">
                Список кабинетов
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table fix-thead',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => 'Покупатели',
                            'enableSorting' => false,
                            'class' => DropDownSearchDataColumn::className(),
                            'headerOptions' => [
                                'class' => 'dropdown-filter',
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'style' => 'overflow: hidden;text-overflow: ellipsis;',
                            ],
                            'filter' => FilterHelper::getContractorList(Documents::IO_TYPE_OUT, null, true, false, false),
                            'format' => 'raw',
                            'value' => function (Contractor $model) {
                                return Html::a($model->getTitle(true),
                                    Url::to(['/contractor/view',
                                        'id' => $model->id,
                                        'type' => $model->type,
                                    ]));
                            },
                        ],
                        [
                            'attribute' => 'storeStatus',
                            'label' => 'Кабинет Вкл\Выкл',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'class' => 'activate-store-account-row',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $model) use ($canAddStoreCabinet, $user) {
                                /* @var $storeCompanyContractor StoreCompanyContractor */
                                $storeCompanyContractor = $model->getStoreCompanyContractors()->one();
                                $storeActive = $storeCompanyContractor ?
                                    $storeCompanyContractor->status == StoreCompanyContractor::STATUS_ACTIVE :
                                    false;
                                $disabled = !$canAddStoreCabinet && !$storeActive;
                                $payLink = Html::a('оплаты', null, [
                                    'class' => 'payment-tab',
                                ]);

                                return '<label class="rounded-switch' . ($disabled ? ' tooltip2' : '') . '"
                                 for="activate_store_account"
                                 data-tooltip-content="' . ($disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : '') . '" style="margin: 4px 0 0;">' .
                                    Html::checkbox('store_account', $storeActive, [
                                        'class' => 'switch active-store-account',
                                        'data-contractor' => $model->id,
                                        'disabled' => $disabled,
                                        'data-toggle' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                            !$storeActive && !$disabled ?
                                            (empty($model->director_email) ? 'modal' : 'modal') : null :
                                        'modal',
                                        'data-target' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                            !$storeActive && !$disabled ?
                                            (empty($model->director_email) ?
                                                '#empty-contractor-email' :
                                                '#add-store-account') : null :
                                        '#no-rights-store-account',
                                    ]) .
                                    '<span class="sliderr no-gray yes-yellow round"></span>
                                </label>
                                <div class="tooltip-template" style="display: none;">
                                    <span id="tooltip_pay-to-add-cabinet-' . $model->id . '" style="display: inline-block; text-align: center;">
                                        Кабинет можно добавить только после ' . $payLink . '
                                    </span>
                                </div>';
                            },
                        ],
                        [
                            'attribute' => 'countOrderDocuments',
                            'label' => 'Кол-во заказов через кабинет',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $model) use ($dateRange) {
                                return $model->getOrderDocuments()
                                    ->andWhere(['from_store' => true])
                                    ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                                    ->andWhere(['between', OrderDocument::tableName() . '.document_date', $dateRange['from'], $dateRange['to']])
                                    ->count();
                            },
                        ],
                        [
                            'attribute' => 'totalOrderDocumentAmount',
                            'label' => 'Сумма заказов через кабинет',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $model) use ($dateRange) {
                                return TextHelper::invoiceMoneyFormat($model->getOrderDocuments()
                                    ->andWhere(['from_store' => true])
                                    ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                                    ->andWhere(['between', OrderDocument::tableName() . '.document_date', $dateRange['from'], $dateRange['to']])
                                    ->sum('total_amount'), 2);
                            },
                        ],
                        [
                            'attribute' => 'averageTotalOrderDocumentAmount',
                            'label' => 'Средняя сумма заказов',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $model) use ($dateRange) {
                                $orderDocumentsCount = $model->getOrderDocuments()
                                    ->andWhere(['from_store' => true])
                                    ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                                    ->andWhere(['between', OrderDocument::tableName() . '.document_date', $dateRange['from'], $dateRange['to']])
                                    ->count();
                                return TextHelper::invoiceMoneyFormat($model->getOrderDocuments()
                                        ->andWhere(['from_store' => true])
                                        ->andWhere(['is_deleted' => OrderDocument::NOT_IS_DELETED])
                                        ->andWhere(['between', OrderDocument::tableName() . '.document_date', $dateRange['from'], $dateRange['to']])
                                        ->sum('total_amount') / ($orderDocumentsCount ? $orderDocumentsCount : 1), 2);
                            },
                        ],
                        [
                            'attribute' => 'responsible_employee_id',
                            'label' => 'Ответственный',
                            'class' => DropDownSearchDataColumn::className(),
                            'enableSorting' => false,
                            'headerOptions' => [
                                'class' => 'dropdown-filter',
                                'width' => '20%',
                            ],
                            'filter' => $searchModel->getResponsibleEmployeeFilter(),
                            'format' => 'raw',
                            'value' => function (Contractor $model) {
                                return $model->responsibleEmployee ?
                                    $model->responsibleEmployee->getShortFio() : '';
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>
<div id="add-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        Вы уверены, что хотите создать личный кабинет для <br>
                        данного покупателя? После нажатия на кнопку ДА, <br>
                        покупателю будет отослано на e-mail письмо <br>
                        с приглашением в личный кабинет. <br>
                        В письме также будет логин и пароль для входа.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="empty-contractor-email" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У данного покупателя не заполнено поле с e-mail. <br>
                        Заполните в закладке "Карточка покупателя".
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue" style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="no-rights-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У вас не достаточно прав для создания кабинета. Обратитесь к руководителю.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(document).on("change", ".active-store-account:not(:checked)", function() {
        changeCabinet(0, $(this).data("contractor"));
    });
    $(document).on("click", "#add-store-account .yes", function() {
        changeCabinet(1, $(this).attr("data-contractor"));
    });
    $(document).on("click", ".active-store-account:checked", function() {
        $("#add-store-account .yes").attr("data-contractor", $(this).data("contractor"));
    });
    function changeCabinet(val, contractorID) {
        $.post("' . Url::to(['/contractor/store-account',]) . '?id=" + contractorID, {store_account: val}, function(data) {
            $.pjax.reload("#store-contractor-pjax", {timeout: 10000});
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value == false) {
                    $icon = "error";
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    }
    $(".payment-tab").click(function () {
        $(".sale-increase-tabs ul.nav-form-tabs li:last a").click();
    });
    $(document).on("pjax:end", function () {
        $(".tooltip2").tooltipster({
            "theme": ["tooltipster-noir", "tooltipster-noir-customized"],
            "trigger": "click",
            "contentAsHTML": true
        });
    });
'); ?>

