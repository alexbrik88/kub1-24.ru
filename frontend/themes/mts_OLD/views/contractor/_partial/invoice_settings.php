<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.04.2018
 * Time: 9:06
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\Company;
use common\models\employee\EmployeeClick;

/* @var $company Company */

$company = Yii::$app->user->identity->company;
?>
<div class="invoice-settings" style="margin-top: 20px;">
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-8"></div>
        <div class="col-sm-4">
            <?= Html::a('Создать ссылку', $company->canAddOutInvoice() ?
                ['/out-invoice/create', 'backTo' => 'contractor'] : 'javascript:;', [
                'class' => 'btn yellow pull-right' . ($company->canAddOutInvoice() ? null : ' payment-out-invoice-tab'),
            ]); ?>
        </div>
    </div>
    <span class="bold list-header" style="width: 346px;">Как подключить модуль выставления счета?</span>
    <ul class="list-body" style="padding-top: 20px;list-style: decimal;">
        <li>Нажмите на кнопку «Создать ссылку»;</li>
        <li>Выберите товар или услугу, которые будут выводится в модуле онлайн выставления счетов;</li>
        <li>При необходимости, укажите дополнительные настройки (Например: добавлять печать и подпись, добавить
            доп.номер к счету и т.д.);
        </li>
        <li>Сохраните ссылку и разместите ее на Вашем сайте.</li>
    </ul>
    <?= Html::a('Пример, как это работает', Yii::$app->params['testOutInvoiceLink'], [
        'class' => 'bold list-header-how-work employee-click_trigger',
        'style' => 'width: 205px;',
        'target' => '_blank',
        'data-url' => Url::to(['/site/employee-click', 'type' => EmployeeClick::PRE_VIEW_OUT_INVOICE]),
    ]); ?>
</div>
