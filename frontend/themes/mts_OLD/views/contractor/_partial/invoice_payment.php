<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.04.2018
 * Time: 9:32
 */

use common\models\service\StoreOutInvoiceTariff;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\models\PaymentOutInvoiceForm;
use common\components\TextHelper;
use yii2mod\slider\IonSlider;
use yii\web\JsExpression;

/* @var $storeOutInvoiceTariff StoreOutInvoiceTariff
 */

$outInvoiceTariffs = StoreOutInvoiceTariff::find()
    ->andWhere(['is_visible' => true])
    ->orderBy('sort')
    ->asArray()
    ->all();
$paymentStoreOutInvoiceForm = new PaymentOutInvoiceForm();
$paymentStoreOutInvoiceForm->tariffId = 1;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-invoice',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="row" style="padding-top: 20px;">
    <div class="col-md-5 col-md-push-7">
        <div class="link-payment">
            <span class="link-header">
                Выберите количество ссылок
            </span>
            <?= Html::activeHiddenInput($paymentStoreOutInvoiceForm, 'tariffId'); ?>
            <?= IonSlider::widget([
                'name' => 'out-invoice-slider',
                'pluginOptions' => [
                    'min' => 1,
                    'max' => 100,
                    'from' => 1,
                    'values' => [
                        1, 3, 10, 'Без ограничений',
                    ],
                    'grid' => true,
                    'onChange' => new JsExpression('
                            function (data) {
                                var $tariff =  ' . json_encode($outInvoiceTariffs) . ';
                                
                                changeTariff(data.from);
                                
                                function changeTariff($number) {
                                    var $newTariff = $tariff[$number];
                                    $(".links-count span").text($newTariff.links_count);
                                    $(".links-amount span").text($newTariff.total_amount);
                                    $("#paymentoutinvoiceform-tariffid").val($newTariff.id);
                                }
                            }
                        '),
                ],
                'options' => [
                    'id' => 'ion-slider-out-invoice-tariff',
                ],
            ]); ?>
            <div class="links-count">
                Количество: <span class="bold-text">3</span>
            </div>
            <div class="links-amount">
                Стоимость в год: <span class="bold-text">7200</span> <i class="fa fa-rub"></i>
            </div>
            <?= Html::button('Оплатить', [
                'class' => 'btn darkblue tooltip2-invoice pay',
                'data-tooltip-content' => '#tooltip_pay-out-invoice',
            ]); ?>
        </div>
    </div>
    <div class="col-md-6 col-md-pull-5 created-scroll">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="heading">
                <th class="text-center" width="20%">Кол-во ссылок</th>
                <th class="text-center" width="20%">
                    Стоимость <br>
                    за 1 ссылку в месяц
                </th>
                <th class="text-center" width="30%">
                    Оплата <br>
                    за 12 месяцев
                </th>
                <th width="30%">
                    Количество <br>
                    счетов в месяц
                </th>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">
                    250 <i class="fa fa-rub"></i>
                </td>
                <td class="text-center">
                    3 000 <i class="fa fa-rub"></i>
                </td>
                <td class="text-center">
                    до 5 счетов
                </td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">
                    2000 <i class="fa fa-rub"></i>
                </td>
                <td class="text-center">
                    24 000 <i class="fa fa-rub"></i>
                </td>
                <td class="text-center">
                    до 30 счетов
                </td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">
                    ---
                </td>
                <td class="text-center">
                    по договоренности
                </td>
                <td class="text-center">
                    больше 30 счетов
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="tooltip-template" style="display: none;">
        <span id="tooltip_pay-out-invoice" style="display: inline-block; text-align: center;">
            <span style="display: block;font-size: 16px;font-weight: bold;margin-bottom: 10px;">Оплата</span>
            <?= Html::a('Картой', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #45b6af;color: white;',
                'data-href' => Url::to(['/out-invoice-payment/online-payment']),
            ]); ?>
            <?= Html::a('Выставить счет', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #ffaa24;color: #ffffff;',
                'data-href' => Url::to(['/out-invoice-payment/expose-invoice']),
            ]); ?>
        </span>
</div>
<?php $this->registerJs('
    $(document).ready(function () {
        $(document).on("click", "#tooltip_pay-out-invoice a", function () {
            var $data = $("#paymentoutinvoiceform-tariffid:hidden").serialize();
            $.post($(this).data("href"), $data, function($data) {
                $("button.pay").append($data);
            });
        });
    });
'); ?>
