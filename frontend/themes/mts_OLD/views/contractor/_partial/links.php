<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.04.2018
 * Time: 5:56
 */

use yii\data\ActiveDataProvider;
use frontend\models\OutInvoiceSearch;

/* @var $outInvoiceSearch OutInvoiceSearch
 * @var $outInvoiceProvider ActiveDataProvider
 * @var $donateWidgetSearch DonateWidgetSearch
 * @var $donateWidgetProvider ActiveDataProvider
 */
?>
<div class="expose-invoice-links-block" style="margin-top: 20px;">
    <?= $this->render('//out-invoice/index', [
        'searchModel' => $outInvoiceSearch,
        'dataProvider' => $outInvoiceProvider,
        'backTo' => 'contractor',
    ]); ?>
    <?php if (Yii::$app->user->identity->company->canDonateWidget()) : ?>
        <div style="padding-top: 25px;">
            <?= $this->render('@frontend/modules/donate/views/donate-widget/index', [
                'searchModel' => $donateWidgetSearch,
                'dataProvider' => $donateWidgetProvider,
                'backTo' => 'contractor',
            ]); ?>
        </div>
    <?php endif ?>
</div>
<?php $this->registerJs('
    $(".payment-out-invoice-tab").click(function () {
        $(".sale-increase-invoice-tabs ul.nav-form-tabs li:last a").click();
    });
'); ?>
