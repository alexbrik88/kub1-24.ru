<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.03.2018
 * Time: 16:54
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use yii\bootstrap\ActiveForm;
use frontend\models\CompanySiteSearch;
use yii\data\ActiveDataProvider;
use common\models\employee\EmployeeClick;

/* @var $company Company
 * @var $companySiteSearch CompanySiteSearch
 * @var $companySiteProvider ActiveDataProvider
 */
?>
<div class="settings-block" data-url="<?= Url::to(['/site/change-store-settings', 'companyID' => $company->id]); ?>"
     style="margin-top: 14px;">
    <div class="col-md-12 p-l-0" style="padding-bottom: 14px;">
        <div class="btn-group pull-left title-buttons" style="width: 344px;">
            <a href="//store.kub-24.ru/site/login?key=<?= $company->storeUser->login_key; ?>"
               data-url="<?= Url::to(['/site/employee-click', 'type' => EmployeeClick::PRE_VIEW_STORE_CABINET]); ?>"
               target="_blank" class="btn yellow employee-click_trigger" style="width: 100%;">
                ПРЕДВАРИТЕЛЬНЫЙ ПРОСМОТР КАБИНЕТА
            </a>
        </div>
        <div style="margin-left: 360px;">
            Нажмите на кнопку и посмотрите, что будет видеть ваш клиент. <br>
            Настроить кабинет покупателя вы можете ниже.
        </div>
    </div>
    <div class="block-hr clear-box">
        <hr>
    </div>
    <?= Html::a('<i class="icon-pencil"></i>', null, [
        'class' => 'darkblue btn-sm btn-link update-store-settings',
        'title' => 'Редактировать',
        'style' => 'width: 33px;',
    ]); ?>
    <?= Html::a('<i class="fa fa-floppy-o fa-2x" style="color: white;font-size: 14px!important;"></i>', null, [
        'class' => 'darkblue btn-sm btn-link submit-settings hidden action-buttons',
        'title' => 'Сохранить',
        'style' => 'width: 33px;',
    ]); ?>
    <?= Html::a('<i class="ico-Cancel-smart-pls fs" style="color: white;"></i>', null, [
        'class' => 'darkblue btn-sm btn-link undo-update-store-settings hidden action-buttons',
        'title' => 'Отменить',
        'style' => 'width: 33px;',
    ]); ?>
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/site/change-store-settings', 'companyID' => $company->id]),
        'options' => [
            'class' => 'form-horizontal form-store-settings',
        ],
        'enableClientValidation' => true,
    ]); ?>
    <span class="bold" style="display: block;margin-top: 10px;">1) Вывод товара</span>

    <span style="display: block;">Покупатель в своем кабинете видит товар. Если у товара количество = 0,</span>

    <div class="where-empty-product">
        <?= Html::activeRadioList($company, 'store_where_empty_products_type', [
            Company::WHERE_EMPTY_PRODUCTS_TYPE_NOT_SHOW => 'то его не показываем',
            Company::WHERE_EMPTY_PRODUCTS_TYPE_SHOW => 'то его показываем и указываем, что количество «0»',
            Company::WHERE_EMPTY_PRODUCTS_TYPE_TEXT => 'то пишем',
        ], [
            'encode' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($company) {
                $input = ($value == Company::WHERE_EMPTY_PRODUCTS_TYPE_TEXT ?
                    Html::activeDropDownList($company, 'store_where_empty_products_text_type',
                        Company::$whereEmptyProductsTextType, [
                            'class' => 'form-control' .
                                ($company->store_where_empty_products_type !== Company::WHERE_EMPTY_PRODUCTS_TYPE_TEXT ?
                                    ' default-disabled' : null),
                            'style' => 'display: inline;width: 18%;margin-left: 10px;',
                            'disabled' => true,
                        ]) : null);

                return Html::radio($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'disabled' => true,
                    ]) . $input;
            },
        ]); ?>
    </div>
    <span class="bold" style="display: block;margin-top: 20px;">2) Кнопка «Новинки»</span>

    <div class="novelty">
        <?= Html::activeCheckbox($company, 'store_show_novelty_button', [
            'label' => 'Отображать кнопку «Новинки»',
            'disabled' => true,
        ]); ?>
        <span style="display: block;margin-left: 25px;">
            При нажатии на кнопку «Новинки», покупатели будут видеть товар, который вы добавили за последние
            <?= Html::activeTextInput($company, 'store_novelty_product_by_days_count', [
                'class' => 'form-control' . (!$company->store_show_novelty_button ? ' default-disabled' : null),
                'style' => 'width: 10%;display: inline;',
                'disabled' => true,
            ]); ?>
            дней
        </span>
    </div>
    <span class="bold" style="display: block;margin-top: 20px;">3) Скидки</span>

    <div class="client-discount">
        <?= Html::activeCheckbox($company, 'store_has_discount', [
            'label' => 'Если клиент сделал заказ на сумму больше' . Html::activeTextInput($company, 'store_discount_from_amount', [
                    'class' => 'form-control' . (!$company->store_has_discount ? ' default-disabled' : null),
                    'style' => 'width: 30%;display: inline;margin: 0 4px 0 4px;',
                    'disabled' => true,
                ]) . 'то:',
            'disabled' => true,
        ]); ?>
    </div>
    <div class="discount-for-client">
        <?= Html::activeRadioList($company, 'store_discount_type', [
            Company::DISCOUNT_TYPE_PERCENT => 'в счете делаем скидку',
            Company::DISCOUNT_TYPE_ANOTHER => 'что-то другое',
        ], [
            'encode' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($company) {
                $input = ($value == Company::DISCOUNT_TYPE_PERCENT ?
                    (Html::activeTextInput($company, 'store_discount', [
                            'class' => 'form-control' . ($company->store_discount_type !== Company::DISCOUNT_TYPE_PERCENT ? '' : null),
                            'style' => 'width: 5%;display: inline;margin-left: 5px;',
                            'disabled' => true,
                        ]) . '%') :
                    Html::activeTextInput($company, 'store_discount_another', [
                        'class' => 'form-control' . ($company->store_discount_type !== Company::DISCOUNT_TYPE_ANOTHER ? '' : null),
                        'style' => 'width: 25%;display: inline;margin-left: 5px;',
                        'disabled' => true,
                    ]));

                return '<div>' . Html::radio($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'disabled' => true,
                    ]) . $input . '</div>';
            },
        ]); ?>
    </div>
    <div class="form-actions hidden">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Применить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Применить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:59.45%;">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undo-update-store-settings',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg undo-update-store-settings',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
    <?php $form->end(); ?>

    <div class="portlet-body" style="margin-top: 30px;padding-top: 20px;border-top: 1px dashed #23527c;">
        <?= $this->render('//company-site/index', [
            'searchModel' => $companySiteSearch,
            'dataProvider' => $companySiteProvider,
            'header' => '<span class="bold" style="display: block;margin-bottom: 15px;">4) Разместите ВХОД в личные кабинеты покупателей на вашем сайте</span>',
        ]); ?>
    </div>
</div>
