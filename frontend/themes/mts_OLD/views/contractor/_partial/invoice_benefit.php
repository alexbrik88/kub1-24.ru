<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.04.2018
 * Time: 4:58
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\models\employee\EmployeeClick;

?>
<div class="benefit-block" style="margin-top: 20px;">
    <span class="bold" style="display: block;">На сайте принимаете оплату только по картам?</span>
    <span class="bold" style="display: block;">Добавьте возможность оплат для ООО и ИП безналичным переводом с помощью выставления счета. Вашим покупателям не нужно</span>
    <span class="bold" style="display: block;">звонить и просить выставить счет. 7 дней в неделю, 24 часа в сутки, покупатели смогут сами выставить себе счет и сразу его оплатить.</span>
    <span class="bold list-header" style="width: 118px;">Ваши выгоды:</span>

    <div class="list-body" style="margin-top: 20px;">
        <span>Ваш сайт сможет самостоятельно принимать и выставлять счета покупателям</span>
        <ul style="padding-top: 20px;">
            <li>Вашим клиентам не нужно звонить и просить выставить счет;</li>
            <li>У вас будет больше времени на развитее бизнеса, так как не нужно будет заниматься рутиной;</li>
            <li>Повышение лояльности клиентов за счет удобство выставления счета в удобное для клиента время;</li>
            <li>Любой ваш сайт сможет продавать, достаточно разместить ссылку на выставление счета;</li>
            <li>Стоимость одной ссылки всего 200 рублей в месяц! Эта стоимость равна всего чашке кофе.</li>
        </ul>
    </div>
    <span class="bold list-header" style="width: 203px;">Выгоды для покупателя:</span>
    <ul class="list-body hidden" style="padding-top: 20px;">
        <li>Возможность получить счет в любое удобное время и в любой точке мира;</li>
        <li>Не нужно дозваниваться до сотрудников, бухгалтеров, с просьбой выставить счет;</li>
        <li>Покупатель вместе со счетом, также получит платежку на его уплату. Ему останется только загрузить ее в банк
            и нажать на кнопу «Оплатить».
        </li>
    </ul>
    <span class="bold list-header" style="width: 203px;">Где можно применить?</span>
    <ul class="list-body hidden" style="padding-top: 20px;">
        <li>На любом сайте;</li>
        <li>Отправить ссылку по электронной почте;</li>
        <li>Разместить ссылку в соц.сетях (Например: в Инстаграме или ВКонтакте);</li>
        <li>Создайте QR код с ссылкой на выставление счета и разместите его в прайс-листах;</li>
        <li>Распечатайте QR код и разместите на рекламной продукции (Например: на баннеры, на флайеры, на автомобиле)
        </li>
    </ul>
    <?= Html::a('Пример, как это работает', Yii::$app->params['testOutInvoiceLink'], [
        'class' => 'bold list-header-how-work employee-click_trigger',
        'style' => 'width: 205px;',
        'target' => '_blank',
        'data-url' => Url::to(['/site/employee-click', 'type' => EmployeeClick::PRE_VIEW_OUT_INVOICE]),
    ]); ?>
</div>
