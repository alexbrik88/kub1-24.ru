<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.07.2018
 * Time: 12:48
 */

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\widgets\StatisticWidget;
use common\models\Company;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::VIEW, ['ioType' => $ioType]);
$canPay = Yii::$app->user->can(permissions\Cash::CREATE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW) &&
    Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS);

$dropItems = [];
if ($ioType == Documents::IO_TYPE_OUT) {
    if ($canCreate) {
        $dropItems[] = [
            'label' => 'Создать Акты',
            'url' => '#many-create-act',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один Акт',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-act',
                'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать ТН',
            'url' => '#many-create-packing-list',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        if ($company->companyTaxationType->osno) {
            $dropItems[] = [
                'label' => 'Создать СФ',
                'url' => '#many-create-invoice-facture',
                'linkOptions' => [
                    'data-toggle' => 'modal',
                ],
            ];
            $dropItems[] = [
                'label' => 'Создать одну СФ',
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'create-one-invoice-facture',
                    'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
                ],
            ];
        }
        $dropItems[] = [
            'label' => 'Создать УПД',
            'url' => '#many-create-upd',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один УПД',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-upd',
                'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    if ($canIndex) {
        $dropItems[] = [
            'label' => 'Скачать в Excel',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'generate-xls-many_actions',
            ],
        ];
    }
}
?>

<div class="row summary-block-mts" style="margin-top: -15px;">
    <?= frontend\modules\documents\widgets\StatisticWidget::widget([
        'type' => $ioType,
        'outerClass' => 'col-md-3 col-sm-3',
        'contractorId' => $searchModel->contractor_id,
        'invoiceStatusId' => array_filter(is_array($searchModel->invoice_status_id) ? $searchModel->invoice_status_id : explode(',', $searchModel->invoice_status_id)),
        'searchModel' => $searchModel,
        'clickable' => true,
        'id' => 'widgets',
    ]); ?>
    <div class="col-md-3 col-sm-3 summary-block-item overdue" style="padding: 10px;">
        <div class="row">
            <div class="col-md-12 nowrap m-l-n " style="margin-top: -4px;">
                <span style="color: #f3565d; font-weight: 700;">Просрочено на:</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>0-10 дней</span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, $model->id); ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>11-30 дней</span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, $model->id); ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>31-60 дней</span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, $model->id); ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>61-90 дней</span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, $model->id); ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>Больше 90 дней</span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, $model->id); ?></span>
            </div>
        </div>
        <div class="row" style="padding-top: 0px;">
            <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                <span>
                    <b style="color: #f3565d;">Вся задолженность</b>
                </span>
            </div>
            <div class="col-md-8 col-sm-3 pull-right text-right ">
                <span>
                    <b style="color: #f3565d;">
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, true, $model->id); ?>
                    </b>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-3">
        <?= Html::a('', array_merge(['get-xls'], Yii::$app->request->queryParams), [
            'class' => 'small-btn-mts del hidden',
            'title' => 'Скачать в Excel',
            'style' => 'margin-right:8px'
        ]); ?>
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'contr_inv_scan',
                ],
                [
                    'attribute' => 'contr_inv_paydate',
                ],
                [
                    'attribute' => 'contr_inv_paylimit',
                ],
                [
                    'attribute' => 'contr_inv_act',
                ],
                [
                    'attribute' => 'contr_inv_paclist',
                ],
                [
                    'attribute' => 'contr_inv_invfacture',
                ],
                [
                    'attribute' => 'contr_inv_upd',
                ],
                [
                    'attribute' => 'contr_inv_author',
                    'visible' => (
                        Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
                    ),
                ],
                [
                    'attribute' => 'contr_inv_comment',
                ],
            ],
        ]); ?>
    </div>
    <div class="col-md-9 col-sm-9">
        <div class="search_form">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'fieldConfig' => [
                    'template' => "{input}\n{error}",
                    'options' => [
                        'class' => '',
                    ],
                ],
            ]); ?>
            <?= Html::activeInput('text', $searchModel, 'byNumber', [
                'placeholder' => 'Номер счета',
                'class' => 'mts-text-input'
            ]); ?>
            <?= Html::submitButton('Найти', ['class' => 'btn-mts btn-mts-red']) ?>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/_invoices_table', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'useContractor' => true,
    'type' => $ioType,
    'id' => $model->id,
]); ?>

<div class="modal fade" id="basic" tabindex="-1" role="basic"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="uneditable created-by"
                     style="text-align: center">создал
                    <span><?= date(DateHelper::FORMAT_USER_DATE, $model->created_at); ?></span>
                    <span><?= $model->employee ? $model->employee->fio : ''; ?></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default"
                        data-dismiss="modal">OK
                </button>
            </div>
        </div>
    </div>
</div>

<div class="tooltip_templates" style="display: none;">
                <span id="tooltip_contractor-debt" style="display: inline-block; text-align: center;">
                    Расшифровка «Не оплачено в срок»<br>
                    за весь период работы с клиентом.<br>
                    В блоках слева, данные за период, указанный выше.
                </span>
    <span id="tooltip_doc_upd" style="display: inline-block; text-align: center;">
                    У вас нет ни одной УПД.<br>
                    Что бы начать формировать УПД,<br>
                    измените настройки в профиле компании
                </span>
</div>
<?php yii\bootstrap\Modal::begin([
    'id' => 'store-account-modal',
    'closeButton' => false,
]); ?>
<h4 id="store-account-modal-content"></h4>
<div style="text-align: center; margin-top: 30px;">
    <button type="button" class="btn darkblue text-white"
            data-dismiss="modal">OK
    </button>
</div>
<?php yii\bootstrap\Modal::end(); ?>
<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            '/documents/invoice/many-document-print',
            'actionType' => 'pdf',
            'type' => $searchModel->type,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
        ]) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<i class="fa fa-bank m-r-sm"></i> Платежка', '#many-charge', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a('Оплачены', '#many-paid-modal', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>
