<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.07.2018
 * Time: 16:18
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use kartik\checkbox\CheckboxX;
use yii\helpers\Json;
use yii\helpers\Url;
use common\components\TextHelper;
use common\models\Contractor;
use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use common\components\date\DateHelper;
use miloschuman\highcharts\HighchartsAsset;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $sellingMonthNumber string */
/* @var $sellingYearNumber string */

$ioType = ($model->type == Contractor::TYPE_SELLER) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;

$productsChartOptions = $model->getProductHighChartsOptions($ioType, $sellingMonthNumber, $sellingYearNumber);

$dateItems = [];
foreach ($model->getAnalyticsDateItems($ioType) as $key => $value) {
    $dateItems[] = [
        'label' => mb_strtolower($value) . ' ' . DateHelper::format($key, 'Y', 'Y-m'),
        'url' => Url::current([
            'sellingMonthNumber' => DateHelper::format($key, 'm', 'Y-m'),
            'sellingYearNumber' => DateHelper::format($key, 'Y', 'Y-m'),
        ]),
        'linkOptions' => ['class' => $key == ($sellingYearNumber . '-' . $sellingMonthNumber) ? 'active' : ''],
    ];
}
$previousMonthNumber = date('m', strtotime('-1 month', strtotime("01.{$sellingMonthNumber}." . $sellingYearNumber)));
$previousYear = date('Y', strtotime('-1 month', strtotime("01.{$sellingMonthNumber}." . $sellingYearNumber)));

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>
<div class="row">
    <div class="col-md-9 text-left" style="width: 78%;">
        <div class="bold" style="padding: 5px 10px;background-color: #e5e5e5;">
            <?= ($model->type == Contractor::TYPE_CUSTOMER) ? 'Проданные' : 'Купленные' ?> товары / услуги за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', mb_strtolower(FlowOfFundsReportSearch::$month[$sellingMonthNumber]) . ' ' . $sellingYearNumber, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #333333; cursor: pointer;',
                    ]) ?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'items' => $dateItems,
                    ]) ?>
                </div>
            </div>
        </div>
        <table id="selling-contractor-table" class="table table-bordered table-hover dataTable no-footer"
               style="table-layout: fixed;" role="grid">
            <tbody>
            <tr class="odd" role="row">
                <td width="37%" style="padding-left: 0;">
                    <?= CheckboxX::widget([
                        'id' => "product-main-checkbox",
                        'name' => "product-main-checkbox",
                        'value' => true,
                        'options' => [
                            'class' => 'selling-contractor-main-checkbox',
                        ],
                        'pluginOptions' => [
                            'size' => 'xs',
                            'threeState' => false,
                            'inline' => false,
                            'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                            'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                        ],
                    ]); ?>
                </td>
                <td width="15%" class="text-right">
                    <?= FlowOfFundsReportSearch::$month[$previousMonthNumber] . ' ' . $previousYear; ?>
                </td>
                <td width="15%" class="text-right">
                    <?= FlowOfFundsReportSearch::$month[$sellingMonthNumber] . ' ' . $sellingYearNumber; ?>
                </td>
                <td width="8%" class="text-right">
                    %%
                </td>
                <td width="25%" class="text-right">
                    Среднее (12 месяцев)
                </td>
            </tr>
            <?php foreach ($productsChartOptions as $productID => $productChartOption): ?>
                <?php $highChartsProductOptionsJson = Json::encode($productChartOption['chartInfo']);
                $currentProductCount = $model->getProductSumSellingData($ioType, $sellingMonthNumber, $sellingYearNumber, $productID);
                $previousMonthProductCount = $model->getProductSumSellingData($ioType, $previousMonthNumber, $previousYear, $productID);
                $averageProductAmount = 0;
                if (empty($previousMonthProductCount)) {
                    $invoiceProductPercent = 100;
                    if (empty($currentProductCount)) {
                        $invoiceProductPercent = 0;
                    }
                } else {
                    $invoiceProductPercent = $previousMonthProductCount > 0 ?
                        (round(($currentProductCount * 100 / $previousMonthProductCount), 2) - 100) : -100;
                };
                $seriesProductData = $productChartOption['chartInfo']['series'][0]['data'];
                foreach ($seriesProductData as $oneAmount) {
                    $averageProductAmount += $oneAmount * 100;
                }
                $averageProductAmount /= 12; ?>
                <tr>
                    <td width="37%" style="padding-right: 0;padding-left: 0;">
                        <?= CheckboxX::widget([
                            'id' => "product-{$productID}-checkbox",
                            'name' => "product-{$productID}-checkbox",
                            'value' => true,
                            'options' => [
                                'class' => 'selling-contractor-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]) . '<span class="product-title">' . $productChartOption['productTitle'] . '<i class="fa fa-rub"></i></span>'; ?>
                    </td>
                    <td width="15%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($previousMonthProductCount, 2); ?>
                    </td>
                    <td width="15%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($currentProductCount, 2); ?>
                    </td>
                    <td width="8%" class="text-right">
                        <?= $invoiceProductPercent > 0 ? ('+' . $invoiceProductPercent) : ($invoiceProductPercent) ?>
                    </td>
                    <td width="25%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($averageProductAmount, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="pad0">
                        <div id="high-charts-container-product-<?= $productID ?>"
                             class="high-chart contractor-selling-chart"
                             style="height: 200px;display: none;"></div>
                        <span class="product-unit-label-chart" style="display: none;">
                            КОЛ-ВО <?= $productChartOption['productUnitTitle'] ?
                                ('(' . $productChartOption['productUnitTitle'] . ')') : null; ?>
                        </span>
                    </td>
                </tr>
                <?php $this->registerJs('
                    Highcharts.chart("high-charts-container-product-" + ' . $productID . ', ' . $highChartsProductOptionsJson . ');
                '); ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>