<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */
?>

<div class="col-md-4 legal">
    <div class="portlet box box-contact darkblue">
        <div class="portlet-title">
            <div class="caption">Контакт</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">
            
            <div class="form-group">

                <label class="checkbox-inline match-with-leader" style="height: 39px; pointer-events: none;">
                    <div class="checker">
                        <span class="<?php if ($model->contact_is_director) echo 'checked' ?>">
                            <input value="1" checked="" disabled="" type="checkbox">
                        </span>
                    </div>
                    Совпадает с руководителем
                </label>

            </div>

            <div class="<?php if ($model->contact_is_director) echo 'hide' ?>">
                <div class="form-group">
                    <label for="contact_name"
                           class="control-label col-md-4 label-width bold-text">ФИО:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->contact_name; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contact_email"
                           class="control-label col-md-4 label-width bold-text">E-mail:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->contact_email; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contact_phone"
                           class="control-label col-md-4 label-width bold-text">Телефон:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->contact_phone; ?></span>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>
