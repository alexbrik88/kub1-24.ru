<?php

use yii\helpers\ArrayHelper;
use common\models\employee\Employee;

?>

<div class="portlet box darkblue edit-customer">
    <div class="portlet-body details">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="employee_id"
                           class="control-label col-md-6 label-width">Ответственный сотрудник:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static">
                            <?php echo $model->responsibleEmployee['lastname'] .' '. $model->responsibleEmployee['firstname'] .' '. $model->responsibleEmployee['patronymic']; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="source"
                           class="control-label col-md-6 label-width">Источник:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?php echo $model->campaign->name ?? ''; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment"
                           class="control-label col-md-6 label-width">Комментарии:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?php echo $model->comment; ?></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="payment_delay"
                           class="control-label col-md-6 label-width">Отсрочка оплаты в днях:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?php echo $model->payment_delay; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="payment_delay"
                           class="control-label col-md-6 label-width">Фиксированная скидка на всё:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?php echo ($model->discount > 0) ? "{$model->discount}%" : '0' ?></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>