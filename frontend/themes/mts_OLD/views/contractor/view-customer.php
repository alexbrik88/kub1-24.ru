<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use common\widgets\Modal;
use frontend\models\Documents;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */
/* @var $user Employee */
/* @var $activeTab integer */
/* @var $analyticsMonthNumber string */
/* @var $analyticsYearNumber string */
/* @var $sellingMonthNumber string */
/* @var $sellingYearNumber string */
/* @var $abcGroup integer */
/* @var $disciplineGroup integer */

$this->title = $model->nameWithType;

$tabFile = ArrayHelper::remove($tabData, 'tabFile');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-store',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'side' => 'top'
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

$user = Yii::$app->user->identity;
$canUpdate = Yii::$app->user->can(\frontend\rbac\permissions\Contractor::UPDATE, [
    'model' => $model,
]);
$canCreateInvoice = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => $ioType,
]);
$isStoreActive = $model->getStoreCompanyContractors()
    ->andWhere(['status' => StoreCompanyContractor::STATUS_ACTIVE])
    ->exists();
$hasStore = $model->getStoreCompanyContractors()->one();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$disabled = !$canAddStoreCabinet && !$isStoreActive;
$invCount = 0;
if ($company->isFreeTariff) {
    $invCount = $company->getInvoiceLimitForFreeTariff() - $company->freeTariffInvoicesCount;
} elseif ($company->getIsTrialNotPaid()) {
    $invCount = $company->getInvoiceLimitForTrialTariff() - $company->trialTariffInvoicesCount;
}

if ($model->type == Contractor::TYPE_CUSTOMER) {
    $tabInvoice = [
        'label' => ($tab == 'autoinvoice' ? 'АвтоСчета' : 'Счета') .
            Html::a('<b class="caret" style="margin: 0;"></b>', '#', [
                'data-toggle' => 'dropdown',
                'class' => 'dropdown-toggle',
            ]) . Dropdown::widget([
                'items' => [
                    ['label' => 'Счета', 'url' => Url::current(['tab' => null, 'edit' => null])],
                    ['label' => 'АвтоСчета', 'url' => Url::current(['tab' => 'autoinvoice', 'edit' => null])],
                ],
            ]),
        'url' => Url::current(['tab' => $tab == 'autoinvoice' ? 'autoinvoice' : null, 'edit' => null]),
        'active' => $tab == null || $tab == 'autoinvoice',
        'options' => [
            'class' => 'dropdown tab-label-dropdown',
            'style' => 'padding-right: 30px;',
        ],
        'encode' => false,
    ];
} else {
    $tabInvoice = [
        'label' => 'Счета',
        'url' => Url::current(['tab' => null, 'edit' => null]),
        'active' => $tab == null,
    ];
}

$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->getInvoicesAuto()->exists();

$this->registerJs('
    $(document).on("change", "#activate_store_account:not(:checked)", function() {
        changeCabinet($("#activate_store_account").serialize());
    });
    $(document).on("click", "#add-store-account .yes", function() {
        changeCabinet({store_account: 1});
    });

    function changeCabinet(val) {
        var isChecked = $("#activate_store_account").prop("checked");
        $.post("' . Url::to(['store-account', 'id' => $model->id]) . '", val, function(data) {
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value != undefined) {
                if (data.value == false) {
                    $icon = "error";
                }
                $("#activate_store_account").prop("checked", data.value);
            } else {
                $("#activate_store_account").prop("checked", !isChecked);
            }
            var $value = $("#activate_store_account").is(":checked");
            if ($value) {
                $("#activate_store_account").removeAttr("data-toggle");
                $("#activate_store_account").removeAttr("data-target");
            } else if (data.canAddStoreCabinet == 1) {
                $("#activate_store_account").attr("data-toggle", "modal");
                $("#activate_store_account").attr("data-target", "#add-store-account");
            }

            if (data.label != undefined) {
                $(".contractor-store-label").text(data.label);
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    }

    var $customerBlockIn = $(".contractor-in-block");
    var $customerBlockOut = $(".contractor-out-block");
    $(".update-contractor").click(function (e) {
        if ($(".contractor-info-tab").hasClass("active")) {
            $customerBlockIn.show();
            $customerBlockOut.hide();
        } else {
            location.href = $(".contractor-info-tab > a").attr("href") + "&edit=1";
        }
    });

    $(".undo-contractor").click(function (e) {
        $customerBlockIn.hide();
        $customerBlockOut.show();
    });
');
?>

<div class="contractor-view">
    <div>
        <?= Html::a('Назад к списку', ['/contractor/index', 'type' => $model->type]); ?>
    </div>

    <div class="light-box" style="margin-top: 15px;">
        <div style="position: relative;">
            <div style="margin-right: 250px;">
                <div style="position: relative;">
                    <div class="title" style="font-size: 21px; margin-right: 150px; padding-left: 10px;">
                        <?= Html::encode($this->title); ?>
                    </div>

                    <div class="actions" style="position: absolute; top: 0; right: 0;">
                        <?= Html::a('<i class="icon-info"></i>', '#basic', [
                            'class' => 'btn-mts btn-mts-white ico-btn info-button',
                            'data-toggle' => 'modal',
                        ]) ?>
                        <?php if ($canUpdate): ?>
                            <?= Html::a('<i class="icon-pencil"></i>', 'javascript:;', [
                                'class' => 'btn-mts btn-mts-white ico-btn update-contractor contractor-out-block',
                                'title' => 'Редактировать',
                                'data-toggle' => 'modal',
                            ]) ?>
                            <?= Html::submitButton('<span class="ico-Save-smart-pls"></span>', [
                                'class' => 'btn-mts btn-mts-white ico-btn contractor-in-block',
                                'title' => 'Сохранить',
                                'form' => 'update-contractor-form',
                                'style' => 'color: #FFF;display: none;',
                            ]); ?>
                            <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', 'javascript:;', [
                                'class' => 'btn-mts btn-mts-white ico-btn contractor-in-block undo-contractor',
                                'title' => 'Отменить',
                                'style' => 'color: #FFF;display: none;',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="contractor-info">
                    <table>
                        <tr>
                            <td>
                                <label class="control-label">Контакт</label>
                            </td>
                            <td>
                                <label class="control-label">Телефон</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $model->getRealContactName(); ?>
                            </td>
                            <td>
                                <?= $model->getRealContactPhone(); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top: 20px;">
                                <label class="control-label">E-mail</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php $email = $model->getRealContactEmail(); ?>
                                <?php if (!empty($email)): ?>
                                    <a href="mailto:<?= $email; ?>"><?= $email; ?></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                        <tr>
                            <td style="padding-top: 15px; padding-bottom: 0;">
                                <span class="contractor-store-label">
                                    <?= $hasStore ?
                                        ($isStoreActive ? 'Отключить кабинет' : 'Включить кабинет')
                                        : ($model->type == Contractor::TYPE_CUSTOMER ?
                                            Html::a('Создать кабинет покупателя', Url::to(['/contractor/sale-increase'])) :
                                            'Создать кабинет'); ?>
                                </span>
                            </td>
                            <td style="padding-top: 15px; padding-bottom: 0;">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <label class="rounded-switch<?= $disabled ? ' tooltip2-store' : ''; ?>"
                                                   for="activate_store_account"
                                                   data-tooltip-content="<?= $disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : ''; ?>"
                                                   style="margin: 4px 0 0;">
                                                <?= Html::checkbox('store_account', $isStoreActive, [
                                                    'id' => 'activate_store_account',
                                                    'class' => 'switch',
                                                    'disabled' => $disabled,
                                                    'data-toggle' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                        (!$isStoreActive && !$disabled ?
                                                            (empty($model->director_email) ? 'modal' : 'modal') : null) :
                                                        'modal',
                                                    'data-target' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                        !$isStoreActive && !$disabled ?
                                                            (empty($model->director_email) ?
                                                                '#empty-contractor-email' :
                                                                '#add-store-account') : null :
                                                        '#no-rights-store-account',
                                                ]); ?>
                                                <span class="sliderr no-gray yes-yellow round"></span>
                                            </label>
                                            <?php if ($disabled): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                    <span id="tooltip_pay-to-add-cabinet-<?= $model->id; ?>"
                                                          style="display: inline-block; text-align: center;">
                                                        Кабинет можно добавить после
                                                        <?= Html::a('оплаты', Url::to(['/contractor/sale-increase',
                                                            'activeTab' => Contractor::TAB_PAYMENT,
                                                        ])); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                        <td style="vertical-align: top;">
                                        <span class="tooltip2-right-click ico-question valign-middle"
                                              data-tooltip-content="<?= $isStoreActive ? '#tooltip_add_stamp' : '#tooltip_add_cabinet'; ?>"
                                              style="vertical-align: top;cursor: pointer;">
                                        </span>
                                            <?php if (!$isStoreActive): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                        <span id="tooltip_add_cabinet"
                                                              style="display: inline-block;">
                                                            Самый быстрый способ увеличить продажи товара – упростить процесс заказа товара. <br>
                                                            Для этого подключите покупателей к вашему складу! <br>
                                                            <?= Html::a('Узнать подробнее', Url::to(['/contractor/sale-increase'])); ?>
                                                        </span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php endif ?>
                    </table>
                </div>
            </div>
            <div class="right-btn-box">
                <?php if ($canCreateInvoice && $model->company_id !== null): ?>
                    <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                        <?= Html::a('<i class="material-icons">add_circle_outline</i> СЧЕТ', [
                            '/documents/invoice/create',
                            'type' => $ioType,
                            'contractorId' => $model->id,
                        ], [
                            'class' => 'btn-mts btn-mts-red width100',
                        ]) ?>
                    <?php else : ?>
                        <button class="btn-mts btn-mts-red width100 action-is-limited"
                            <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                            <i class="material-icons">add_circle_outline</i> СЧЕТ
                        </button>
                        <?php if ($invCount === 0 && date(DateHelper::FORMAT_USER_DATE, $company->show_invoice_payment_popup_date) == date(DateHelper::FORMAT_USER_DATE)): ?>
                            <?= $this->render('@frontend/widgets/views/invoicePaymentModal', [
                                'company' => $company,
                            ]); ?>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif; ?>

                <div style="margin-top:12px">
                    <?= frontend\widgets\RangeButtonWidget::widget([
                        'cssClass' => 'btn-mts btn-mts-calendar btn-mts-cal width100',
                    ]); ?>
                </div>

                <?php if ($model->type == Contractor::TYPE_CUSTOMER /*&& !$hasAutoinvoice*/ && $tab != 'autoinvoice') : ?>
                    <div style="margin-top:12px">
                        <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                            <?= Html::a('<i class="material-icons">add_circle_outline</i> АвтоСчет', [
                                'create-autoinvoice',
                                'type' => Documents::IO_TYPE_OUT,
                                'id' => $model->id,
                            ], [
                                'class' => 'btn-mts btn-mts-white width100',
                            ]) ?>
                        <?php else : ?>
                            <button class="btn-mts btn-mts-white width100 action-is-limited">
                                <i class="material-icons">add_circle_outline</i> АвтоСчет
                            </button>
                        <?php endif ?>
                    </div>
                <?php endif; ?>
                <?php if ($model->type == Contractor::TYPE_CUSTOMER && !in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_ACCOUNTANT, EmployeeRole::ROLE_ASSISTANT])): ?>
                    <div style="margin-top:12px">
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index', 'PaymentReminderMessageContractorSearch' => [
                            'contractor' => $model->id,
                        ]]), [
                            'class' => 'btn-mts btn-mts-white width100',
                        ]); ?>
                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)) : ?>
                    <div style="margin-top:12px">
                        <?php
                        $url = Url::to(['/contractor/collate', 'step' => 'form', 'id' => $model->id]);
                        Modal::begin([
                            'id' => 'modal-collate-report',
                            'closeButton' => false,
                            'toggleButton' => [
                                'tag' => 'button',
                                'id' => 'collate-report-button',
                                'label' => '<i class="material-icons">add_circle_outline</i> Акт сверки',
                                'class' => 'btn-mts btn-mts-white width100',
                            ],
                        ]);

                        $pjax = Pjax::begin([
                            'id' => 'collate-pjax-container',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                            'linkSelector' => '.collate-pjax-link',
                            'timeout' => 5000,
                        ]);

                        $pjax->end();

                        Modal::end();

                        $this->registerJs('
                        $(document).on("click", "#collate-report-button", function() {
                            $.pjax.reload("#collate-pjax-container", {url: ' . json_encode($url) . ', push: false, replace: false, timeout: 5000})
                        });
                        $(document).on("pjax:complete", "#collate-pjax-container", function(event) {
                            if ($("#collate-pjax-container .date-picker").length) {
                                $("#collate-pjax-container .date-picker").datepicker({
                                    format: "dd.mm.yyyy",
                                    keyboardNavigation: false,
                                    forceParse: false,
                                    language: "ru",
                                    autoclose: true
                                }).on("change.dp", dateChanged);

                                function dateChanged(ev) {
                                    if (ev.bubbles == undefined) {
                                        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                                        if (ev.currentTarget.value == "") {
                                            if ($input.data("last-value") == null) {
                                                $input.data("last-value", ev.currentTarget.defaultValue);
                                            }
                                            var $lastDate = $input.data("last-value");
                                            $input.datepicker("setDate", $lastDate);
                                        } else {
                                            $input.data("last-value", ev.currentTarget.value);
                                        }
                                    }
                                };
                            }
                        });
                        $(document).on("hidden.bs.modal", "#modal-collate-report", function () {
                            $("#collate-pjax-container").html("");
                        })
                        ');
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div style="margin: 20px 0 15px; border-bottom: 1px solid #e7eafa">
        <?= Nav::widget([
            'id' => 'contractor-menu',
            'items' => [
                $tabInvoice,
                [
                    'label' => 'Договоры',
                    'url' => Url::current(['tab' => 'agreements', 'edit' => null]),
                    'active' => $tab == 'agreements',
                ],
                [
                    'label' => 'Аналитика',
                    'url' => Url::current(['tab' => 'analytics', 'edit' => null]),
                    'active' => $tab == 'analytics',
                    'visible' => $ioType == Contractor::TYPE_CUSTOMER,
                ],
                [
                    'label' => ($ioType == Contractor::TYPE_CUSTOMER) ? 'Продажи' : 'Закупки',
                    'url' => Url::current(['tab' => 'selling', 'edit' => null]),
                    'active' => $tab == 'selling',
                    'visible' => ($ioType == Contractor::TYPE_CUSTOMER || $ioType == Contractor::TYPE_SELLER && !$model->is_agent)
                ],
                [
                    'label' => 'Отчеты',
                    'url' => Url::current(['tab' => 'agent_report', 'edit' => null]),
                    'active' => $tab == 'agent_report',
                    'visible' => ($ioType == Contractor::TYPE_SELLER && $model->is_agent)
                ],
                [
                    'label' => 'Карточка',
                    'url' => Url::current(['tab' => 'info', 'edit' => null]),
                    'active' => $tab == 'info',
                ],
            ],
            'options' => ['class' => 'nav-form-tabs row nav nav-tabs'],
        ]); ?>
    </div>

    <?php if ($tabFile) : ?>
        <div class="tab-content light-box">
            <div id="tab1" class="tab-pane invoice-tab active">
                <?= $this->render("//contractor/view/{$tabFile}", $tabData); ?>
            </div>
        </div>
    <?php endif ?>
</div>

<div id="add-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        Вы уверены, что хотите создать личный кабинет для <br>
                        данного покупателя? После нажатия на кнопку ДА, <br>
                        покупателю будет отослано на e-mail письмо <br>
                        с приглашением в личный кабинет. <br>
                        В письме также будет логин и пароль для входа.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="empty-contractor-email" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У данного покупателя не заполнено поле с e-mail. <br>
                        Заполните в закладке "Карточка покупателя".
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="no-rights-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У вас не достаточно прав для создания кабинета. Обратитесь к руководителю.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
