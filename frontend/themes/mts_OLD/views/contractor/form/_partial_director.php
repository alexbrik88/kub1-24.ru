<?php
/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $textInputConfig string */
?>

<div class="col-md-4 legal">
    <div class="portlet box box-leader darkblue">
        <div class="portlet-title">
            <div class="caption">Руководитель</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">
            <?= $form->field($model, 'director_post_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                'id' => 'legal-director_email',
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_phone', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                'options' => [
                    'class' => 'form-control',
                    'id' => 'legal-director_phone',
                    'placeholder' => '+7(XXX) XXX-XX-XX',
                ],
            ]); ?>
        </div>
    </div>
</div>