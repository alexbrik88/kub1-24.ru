<?php

use common\components\widgets\BikTypeahead;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorAccount */
/* @var $contractor common\models\Contractor */

?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => [
        'class' => 'contractor-account-form',
    ],
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'class' => 'form-group row',
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model, 'rs')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'bik')->widget(\common\components\widgets\BikTypeahead::classname(), [
    'remoteUrl' => Url::to(['/dictionary/bik']),
    'related' => [
        '#' . Html::getInputId($model, 'bank_name') => 'name',
        '#' . Html::getInputId($model, 'ks') => 'ks',
    ],
])->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'bank_name')->textInput([
    'maxlength' => true,
    'readonly' => true,
]); ?>
<?= $form->field($model, 'ks')->textInput([
    'maxlength' => true,
    'readonly' => true,
]); ?>
<?= $form->field($model, 'is_main')->checkbox(['class' => 'uniform-input'], false); ?>

<div class="action-buttons row">
    <div class="spinner-button col-md-4 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
            'data-style' => 'expand-right',
            'style' => 'width:130px!important;',
        ]); ?>
        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
            'title' => 'Сохранить',])   ?>
    </div>
    <div class="col-xs-7"></div>
    <div class="col-md-8 col-xs-1" style="float: right;">
        <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right" data-dismiss="modal" style="width: 130px !important;">Отмена</button>
        <button type="button" class="btn darkblue widthe-100 hidden-lg" data-dismiss="modal" title="Отмена"><i class="fa fa-reply fa-2x"></i></button>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?= Html::script(
<<<JS
$('input.uniform-input', $('form.contractor-account-form')).uniform();
JS
) ?>