<?php
/* @var $model common\models\Contractor */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\ContractorAgentPaymentType;
use yii\widgets\Pjax;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$textInputConfig2 = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$agreementArray = $model->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] + ArrayHelper::map($agreementArray, 'id', 'listItemName');

/** @var $model \common\models\Contractor */
?>

    <div class="portlet box darkblue legal">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group field-contractor-is_agent">
                        <label class="control-label col-md-6 label-width" for="contractor-is_agent">Агент</label>
                        <div class="col-md-6 field-width inp_one_line">
                            <?= Html::activeCheckbox($model, 'is_agent', [
                                'id' => 'contractor_is_agent_input',
                                'label' => false,
                            ]); ?>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_payment_type_id">
                        <label class="control-label col-md-6 label-width" for="contractor-agent_payment_type_id">Агентский договор</label>
                        <div class="col-md-6 field-width inp_one_line">
                            <?php
                            echo Select2::widget([
                                'model' => $model,
                                'attribute' => 'agent_agreement_id',
                                'data' => $agreementDropDownList,
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => '',
                                    'width' => '97%',
                                    'minimumResultsForSearch' => -1,
                                    'language' => []
                                ],
                                'options' => [
                                    'class' => 'agent-field',
                                ],
                            ]);
                            ?>
                        </div>
                    </div>


                    <div class="form-group field-contractor-agent_payment_percent">
                        <label class="control-label col-md-6 label-width" for="contractor-agent_payment_percent">Комиссионное вознаграждение</label>
                        <div class="col-md-6 field-width inp_one_line" style="max-width:145px;display:table;">
                            <?= Html::activeTextInput($model, 'agent_payment_percent', [
                                'class' => 'form-control agent-field',
                                'type' => 'number',
                                'maxlength' => true,
                                'min' => 0.01,
                                'max' => 99.99,
                                'step' => '0.01',
                            ]) ?>
                            <span style="display:table-cell;padding-left:10px;vertical-align:middle;">%</span>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_payment_type_id">
                        <label class="control-label col-md-6 label-width" for="contractor-agent_payment_type_id">Тип платежа</label>
                        <div class="col-md-6 field-width inp_one_line">
                            <?= Select2::widget([
                                'model' => $model,
                                'attribute' => 'agent_payment_type_id',
                                'data' => ArrayHelper::map(ContractorAgentPaymentType::find()->where(['id' => 1])->all(), 'id', 'name'),
                                'options' => [
                                    'class' => 'form-control agent-field',
                                ],
                                'pluginOptions' => [
                                    'width' => '97%',
                                    'minimumResultsForSearch' => -1
                                ],
                            ]); ?>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_start_date">
                        <label class="control-label col-md-6 label-width" for="contractor-agent_start_date">Дата начала расчетов</label>
                        <div class="col-md-6 field-width inp_one_line" style="max-width:165px">
                            <?= $form->field($model, 'agent_start_date', ['options' => ['class' => '']])->textInput([
                                'maxlength' => true,
                                'class' => 'agent-field form-control date-picker' . ($model->agent_start_date ? ' edited' : ''),
                                'autocomplete' => 'off',
                                'value' => DateHelper::format($model->agent_start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                            ])->label(false) ?>
                            <?= Html::tag('i', '', [
                                'class' => 'fa fa-calendar',
                                'style' => 'position: absolute; top: 10px; right: 35px; color: #cecece; cursor: pointer;',
                            ]) ?>
                        </div>
                    </div>

                </div>
            </div>

            <?= $this->render('_partial_agent_table', [
                 'model' => $model
            ]) ?>

        </div>
    </div>

<?= $this->render('_partial_agent_agreement', [
    'model' => $model
]) ?>

<?php
$this->registerJS(<<<JS
$(document).on('click', '.form-group .fa-calendar', function() {
    $(this).parents('.form-group').find('.date-picker').datepicker('show');
});
$('#contractor_is_agent_input').on('change', function() {
    if ($(this).prop('checked'))
        $('.agent-field').removeAttr('disabled');
    else
        $('.agent-field').attr('disabled', 'disabled');
});
JS
);

if (!$model->is_agent) {
    $this->registerJS("
    $(document).ready(function () {
        $('.agent-field').attr('disabled', 'disabled');
    });");
}