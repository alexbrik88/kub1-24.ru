<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-5 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-delete',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$subTab = Yii::$app->request->get('sub_tab');
if (!in_array($subTab, ['requisites', 'comments', 'agent']))
    $subTab = false;
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ?
        Url::to(['create', 'type' => $model->type]) :
        Url::to(['update', 'id' => $model->id,
            'tab' => Yii::$app->request->get('tab', 'info'),
            'sub_tab' => Yii::$app->request->get('sub_tab'),
        ]),
    'id' => 'update-contractor-form',
    'class' => 'form-horizontal',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
]); ?>

    <div class="form-body form-horizontal">
        <?= $form->errorSummary($model); ?>

        <?= Html::activeHiddenInput($model, 'type'); ?>
        <?= Html::hiddenInput(null, $model->isNewRecord, [
            'id' => 'contractor-is_new_record',
        ]); ?>

        <?= $this->render('_partial_main_info', [
            'model' => $model,
            'form' => $form,
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
        ]) ?>
        <div class="clearfix"></div>

        <div class="profile-form-tabs">
            <?= Tabs::widget([
                'options' => ['class' => 'nav-form-tabs row'],
                'headerOptions' => ['class' => 'col-xs-3'],
                'items' => array_merge([
                    [
                        'label' => '<span class="legal">Реквизиты</span><span class="physical">Данные паспорта</span>',
                        'encode' => false,
                        'content' => $this->render('_partial_details', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'active' => (!$subTab || $subTab == 'requisites')
                    ],
                    [
                        'label' => 'Комментарии',
                        'content' => $this->render('_partial_comment', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'active' => ($subTab == 'comments')
                    ],
                ], ($model->type == Contractor::TYPE_SELLER && !$model->isNewRecord) ?
                    [
                        [
                            'label' => 'Агент',
                            'content' => $this->render('_partial_agent', [
                                'model' => $model,
                                'form' => $form,
                            ]),
                            'active' => ($subTab == 'agent')
                        ]
                    ] : [])
            ]); ?>
        </div>

        <div class="clearfix"></div>

        <div class="portlet box darkblue physical">
            <div class="portlet-title">
                <div class="caption">Контактные данные</div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                            'id' => 'physical-director_email',
                            'maxlength' => true,
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'director_phone', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                            'options' => [
                                'id' => 'physical-director_phone',
                                'class' => 'form-control',
                                'placeholder' => '+7(XXX) XXX-XX-XX',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row small-boxes">
            <?= $this->render('_partial_director', [
                'model' => $model,
                'form' => $form,
                'textInputConfig' => $partialDirectorTextInputConfig,
            ]); ?>

            <?= $this->render('_partial_chief_accountant', [
                'model' => $model,
                'form' => $form,
                'textInputConfig' => $partialDirectorTextInputConfig,
            ]); ?>

            <?= $this->render('_partial_contact', [
                'model' => $model,
                'form' => $form,
                'textInputConfig' => $partialDirectorTextInputConfig,
            ]); ?>
        </div>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => $model->isNewRecord ?
                            'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button' :
                            'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]) ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn btn-primary widthe-100 hidden-lg'
                    ]) ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 55%;">
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::a('Отменить', $model->isNewRecord ? Url::to(['index', 'type' => $model->type]) : 'javascript:;', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undo-contractor',
                    ]); ?>
                    <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $model->isNewRecord ? Url::to(['index', 'type' => $model->type]) : 'javascript:;', [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]); ?>
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?php if (!$model->isNewRecord
                        && (\Yii::$app->authManager
                            && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE))
                    ): ?>
                        <?php if ($model->getDeleteItemCondition() && !$model->hasCashFlows()): ?>
                            <?= ConfirmModalWidget::widget([
                                'toggleButton' => [
                                    'label' => 'Удалить',
                                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                                ],
                                'confirmUrl' => Url::to(['delete', 'type' => $model->type, 'id' => $model->id,]),
                                'message' => 'Вы уверены, что хотите удалить контрагента?',
                            ]) ?>
                        <?php else: ?>
                            <?= Html::button('Удалить', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs tooltip2-delete',
                                'data-tooltip-content' => '#tooltip_not_delete',
                            ]); ?>
                            <div class="hidden">
                                <span id="tooltip_not_delete">
                                    <?php if (!$model->getDeleteItemCondition()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, у которого есть счета, удалить нельзя.
                                        <br>
                                    <?php endif; ?>
                                    <?php if ($model->hasCashFlows()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, по которому есть операции в разделе Деньги, удалить нельзя.
                                    <?php endif; ?>
                                </span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>


    <script>
        function chiefAccountantIsNotDirector(attribute, value) {
            return !$('#chief_accountant_is_director_input').is(':checked');
        }

        function contactIsNotDirector(attribute, value) {
            return !$('#contact_is_director_input').is(':checked');
        }

        function contractorIsIp() {
            return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsNotIp() {
            return $('#contractor-companytypeid').val() != "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsPhysical() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
        }

        function contractorIsLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
        }

        function contractorIsForeignLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
        }
        function contractorIsAgent() {
            return $("#contractor_is_agent_input:checked").length;
        }
    </script>

<?php
$typeArray = json_encode(array_flip($model->getTypeArray()));
$this->registerJs(<<<JS
    var checkDirectorPostName = function() {
        if ($( "#contractor_face_type" ).val() != '1' && $('#contractor-companytypeid').val() != '1') {
            $('#contractor-director_post_name').prop('disabled', false);
            $('.field-contractor-director_post_name').show();
        } else {
            $('#contractor-director_post_name').prop('disabled', true);
            $('.field-contractor-director_post_name').hide();
        }
    }

    $(document).on("change", "#contractor-companytypeid", function () {
        if ($("#contractor-itn").val()) {
            $(this).closest("form").yiiActiveForm("validateAttribute", "contractor-itn");
        }
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
        checkDirectorPostName();
    });
    $(document).on("change", "#chief_accountant_is_director_input, #contact_is_director_input", function () {
        $(this).closest(".portlet-body").find("input:text").prop("disabled", $(this).is(":checked"));
    });
    $(document).on("input", "#contractor-bic", function(){
        if ($(this).val() === "") {
            console.log(0);
            $("#contractor-bank_name").val("");
            $("#contractor-corresp_account").val("");
        }
    });
    var companyType = $typeArray;
    var directorEmail;
    var contractorIsNewRecord = $("#contractor-is_new_record").val();

    $('#contractor-itn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        beforeRender: function (container) {
            $(".field-contractor-itn p.exists-contractor").empty();
            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: this.value,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
        },
        onSelect: function(suggestion) {
            var companyTypeId = '-1';

            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: suggestion.data.inn,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId).trigger('change');
            if (companyTypeId == 1) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
                $('.field-contractor-bin label').text('ОГРНИП');
            } else {
                $('.field-contractor-ppc').show();
                $('.field-contractor-bin label').text('ОГРН');
            }
            var address = '';
            if (suggestion.data.address.data !== null && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
    $(document).ready(function() {
        if($( "#contractor_face_type" ).val() == '1'){
            $('.legal').hide();
            $('.foreign_legal').hide();
            $('.physical').show();
            $('.field-contractor-taxation_system input').prop('disabled', true);
            $('#physical-director_phone').prop('disabled', false);
            $('#physical-director_email').prop('disabled', false);
            $('#legal-director_phone').prop('disabled', true);
            $('#legal-director_email').prop('disabled', true);
        } else if ($( "#contractor_face_type" ).val() == '0'){
            $('.physical').hide();
            $('.foreign_legal').hide();
            $('.legal').show();
            $('#physical-director_phone').prop('disabled', true);
            $('#physical-director_email').prop('disabled', true);
            $('#legal-director_phone').prop('disabled', false);
            $('#legal-director_email').prop('disabled', false);
        } else if ($( "#contractor_face_type" ).val() == '2'){
            $('.physical').hide();
            $('.legal').show();
            $('.foreign_legal').show();
            $('[id="contractor-itn"]').suggestions('disable');
            $('#contractor-director_email').removeAttr('disabled');
            $('#director-phone').removeAttr('disabled');
            $('#contractor-bank_name').removeAttr('disabled');
            $('#contractor-corresp_account').removeAttr('disabled');
            $('#physical-director_phone').prop('disabled', true);
            $('#physical-director_email').prop('disabled', true);
            $('#legal-director_phone').prop('disabled', false);
            $('#legal-director_email').prop('disabled', false);
        }
        checkDirectorPostName();
    });

    $(document).on('change','#contractor_face_type', function() {
        switch (this.value) {
            case '0':
                $('.physical').hide();
                $('.legal').show();
                $('.not-foreign-legal').show();
                $('.foreign_legal').hide();
                $('.legal').addClass('required');
                $('[id="contractor-itn"]').suggestions('enable');
                $('#contractor-corresp_account').attr('disabled', 'disabled');
                $('.field-contractor-director_name').addClass('required');
                $('.field-contractor-chief_accountant_name').addClass('required');
                $('.field-contractor-contact_name').addClass('required');
                $('.legal').find('#contractor-itn').attr('placeholder','Автозаполнение по ИНН');
                $('.field-contractor-taxation_system input').prop('disabled', false);
                $('#physical-director_phone').prop('disabled', true).val('');
                $('#physical-director_email').prop('disabled', true).val('');
                $('#legal-director_phone').prop('disabled', false);
                $('#legal-director_email').prop('disabled', false);
                $('#contractor-current_account').attr('maxlength', 20).val('');
                break;
            case '1':
                $('.physical').show();
                $('.legal').hide();
                $('.foreign_legal').hide();
                $('.field-contractor-taxation_system input').prop('disabled', true);
                $('#physical-director_phone').prop('disabled', false);
                $('#physical-director_email').prop('disabled', false);
                $('#legal-director_phone').prop('disabled', true).val('');
                $('#legal-director_email').prop('disabled', true).val('');
                $('#contractor-current_account').attr('maxlength', 20).val('');
                break;
            case '2':
                $('.physical').hide();
                $('.legal').show();
                $('.foreign_legal').show();
                $('.not-foreign-legal').hide();
                $('.legal').removeClass('required');
                $('.physical').find('#contractor-director_email').empty();
                $('.legal').find('#contractor-bank_name').removeAttr('disabled');
                $('.legal').find('#contractor-corresp_account').removeAttr('disabled');

                $('.field-contractor-director_name').removeClass('required');
                $('.field-contractor-chief_accountant_name').removeClass('required');
                $('.field-contractor-contact_name').removeClass('required');
                $('[id="contractor-itn"]').suggestions('disable');
                $('.legal').find('#contractor-itn').attr('placeholder','');
                $('.field-contractor-taxation_system input').prop('disabled', false);
                $('#physical-director_phone').prop('disabled', true).val('');
                $('#physical-director_email').prop('disabled', true).val('');
                $('#legal-director_phone').prop('disabled', false);
                $('#legal-director_email').prop('disabled', false);
                $('#contractor-current_account').attr('maxlength', 35);
        }
        checkDirectorPostName();
    });

    $('#contractor-name').on('input', function(){
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
JS
);
