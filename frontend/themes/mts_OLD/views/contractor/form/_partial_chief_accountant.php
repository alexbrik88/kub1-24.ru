<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$requiredInputConfig = $textInputConfig;
if (isset($requiredInputConfig['options']['class'])) {
    $requiredInputConfig['options']['class'] .= ' required';
} else {
    $requiredInputConfig['options']['class'] = ' required';
}
?>

<div class="col-md-4 legal">
    <div class="portlet box box-accounter darkblue">
        <div class="portlet-title">
            <div class="caption">Главный бухгалтер</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'chief_accountant_is_director', [
                        'id' => 'chief_accountant_is_director_input',
                        'label' => false,
                    ]); ?>
                    Совпадает с руководителем
                </label>
            </div>

            <?= $form->field($model, 'chief_accountant_name', $requiredInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->chief_accountant_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'chief_accountant_email', $textInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->chief_accountant_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'chief_accountant_phone', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => '+7(XXX) XXX-XX-XX',
                    'disabled' => $model->chief_accountant_is_director? true: false,
                ],
            ]); ?>
        </div>
    </div>
</div>
