<?php
/* @var $model common\models\Contractor */
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$textInputConfig2 = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
?>
<span class="addColumns long-message-input">
    Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры
</span>
<div class="dopColumns selectedDopColumns">
    <?= $form->field($model, 'director_post_name', $textInputConfig)->label('Должность руководителя'); ?>
    <?= $form->field($model, 'director_name', $textInputConfig)->label('ФИО руководителя'); ?>
    <?= $form->field($model, 'legal_address', $textInputConfig)->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($model, 'current_account', $textInputConfig2)->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'BIC', $textInputConfig2)->widget(\common\components\widgets\BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($model, 'bank_name') => 'name',
            '#' . Html::getInputId($model, 'corresp_account') => 'ks',
        ],
    ])->textInput(['placeHolder' => 'Нужен для Актов и Товарных накладных']); ?>

    <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
        'maxlength' => true,
        'disabled' => true,
    ]); ?>
    <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
        'maxlength' => true,
        'disabled' => true,
    ]); ?>
</div>