<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */
/* @var $documentType integer */

$model->contact_is_director = true;
$model->chief_accountant_is_director = true;

echo $this->render('_form-contractor', [
    'model' => $model,
    'face_type_opt' => 0,
    'documentType' => $documentType,
]);

$this->registerJs('
    $("input:checkbox:not(.md-check, .md-radiobtn), input:radio:not(.md-check, .md-radiobtn)").uniform();
');
?>
<?php if (!empty($onlyFNS)): ?>
<style>
    #contractor-face_type > label:nth-child(2) {display:none;}
</style>
<?php endif; ?>
