<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\widgets\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use common\components\TextHelper;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $admin boolean */
/* @var $creating boolean */

$inputConfig = $inputConfigRequired = $inputConfigMB15 = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'wrapperOptions' => [
        'class' => 'control-data',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'required';
$inputConfigMB15['options']['class'] = 'mb-15';
$textInputConfig = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeholder' => 'Автозаполнение по ИНН',
    ],
];

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-kpp", function () {
            var kpp = $.trim($(this).val());
            if (kpp.length == 9) {
                $.post("/company/ifns", {"kpp": $(this).val()}, function(data) {
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>

<div class="edit-profile__info mts-block">
    <div class="form-group">
        <div class="col-sm-12">
            <?= $form->field($model, 'inn', $textInputConfig)->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>
    <?= $this->render('_partial_description', [
        'model' => $model,
        'form' => $form,
        'creating' => $creating,
    ]); ?>
</div>

<div class="profile-form-tabs mts-block">
    <?= Tabs::widget([
        'id' => 'add_tabs',
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => ''],
        'items' => [
            [
                'label' => 'Расчетные счета',
                'encode' => false,
                'content' => $this->render('_partial_checking_account', [
                    'model' => $model,
                    'form' => $form,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'banks' => $banks,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Кассы',
                'content' => $this->render('_partial_cashbox', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'Склады',
                'content' => $this->render('_partial_store', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'E-money',
                'content' => $this->render('_partial_emoney', [
                    'model' => $model,
                    'form' => $form,
                    'inputConfig' => $inputConfig,
                ]),
            ],
        ],
    ]); ?>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="mts-block">
            <div class="portlet-title">
                <div class="caption big">Руководитель (для документов)</div>
            </div>
            <div class="portlet-body buh-info-edit">
                <?= $form->field($model, 'chief_post_name', $inputConfigMB15)->label('Должность')->textInput(); ?>
                <?= $form->field($model, 'chief_lastname', $inputConfigMB15)->label('Фамилия')->textInput(); ?>
                <?= $form->field($model, 'chief_firstname', $inputConfigMB15)->label('Имя')->textInput(); ?>
                <?= $form->field($model, 'chief_patronymic', $inputConfigMB15)->label('Отчество')->textInput([
                    'readonly' => (boolean) $model->has_chief_patronymic,
                ]); ?>
                <?= $form->field($model, 'has_chief_patronymic', $inputConfig)->label('Нет отчества')->checkbox(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="mts-block">
            <div class="portlet-title">
                <div class="caption big">Главный бухгалтер (для документов)</div>
            </div>
            <div class="portlet-body buh-info-edit buh-mt">
                <div class="" style="padding-bottom: 5px;">
                    <label class="match-with-leader">
                        <?= \yii\helpers\Html::activeCheckbox($model, 'chief_is_chief_accountant', [
                            'id' => 'chief_is_chief_accountant_input',
                            'label' => false,
                        ]); ?>
                        Совпадает с руководителем
                    </label>
                </div>

                <div class="change-disabled-inputs">
                    <?= $form->field($model, 'chief_accountant_lastname', $inputConfigMB15)->label('Фамилия')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <?= $form->field($model, 'chief_accountant_firstname', $inputConfigMB15)->label('Имя')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <?= $form->field($model, 'chief_accountant_patronymic', $inputConfigMB15)->label('Отчество')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <?= $form->field($model, 'has_chief_accountant_patronymic', $inputConfig)->label('Нет отчества')->checkbox([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <div class="clearfix"></div>
                </div>

                <div id="chiefAccountantSignature-container" class="row <?= $model->chief_is_chief_accountant ? 'hidden' : '' ?>">
                    <div class="col-md-4 field-width inp_one_line">
                        <?php $imgPath = $model->getImage('chiefAccountantSignatureImage'); ?>
                        <div id="chiefAccountantSignatureImage-image" class="<?= is_file($imgPath) ? '' : 'hidden'; ?>">
                            <div id="chiefAccountantSignatureImage-image-result" style="max-width: 156px;">
                                <table class="company-image-preview">
                                    <tr>
                                        <td>
                                            <?php if (is_file($imgPath)) : ?>
                                                <?= EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 155px;">
                                <?= $form->field($model, 'deleteChiefAccountantSignatureImage', ['options' => ['class' => '']])->checkbox(); ?>
                            </div>
                        </div>
                        <div>
                            <?= Html::button('Добавить подпись', [
                                'class' => 'btn red mt-20',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-chiefAccountantSignatureImage',
                                'style' => 'margin-right: 20px;',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="details mts-block">
    <div class="portlet-title">
        <div class="caption big">Реквизиты</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'ogrn', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'kpp', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                    'ifns-exist' => $model->getIfns()->exists() ? 'true' : 'false',
                ]); ?>
                <?= $form->field($model, 'okpo', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okud', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfigMB15)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfigMB15)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'okved', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okato', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-md-6 left-column">
                <?= $form->field($model, 'oktmo', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okogu', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okfs', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okopf', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfigMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', array_merge($inputConfigMB15, [
                    'labelOptions' => [
                        'class' => 'control-label',
                        'style' => '',
                    ],
                    'wrapperOptions' => [
                        'class' => 'inp_one_line',
                        'style' => '',
                    ],
                ]))->textInput([
                    'class' => 'form-control date-picker m_b',
                ]); ?>
                <?= $form->field($model, 'capital', array_merge($inputConfigMB15, [
                    'labelOptions' => [
                        'class' => 'control-label label-width',
                        'style' => '',
                    ],
                    'inputOptions' => [
                        'class' => 'form-control js_input_to_money_format',
                    ],
                    'wrapperOptions' => [
                        'class' => 'inp_one_line',
                        'style' => '',
                    ],
                ]))->textInput([
                    'maxlength' => true,
                    'value' => TextHelper::invoiceMoneyFormat($model->capital, 2, '.', ''),
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('_partial_ifns', [
    'form' => $form,
    'model' => $model,
    'ifns' => $ifns,
]) ?>

<div style="margin-bottom: 20px;">
<?php echo $this->render('_partial_files', [
    'model' => $model,
]); ?>
</div>

<?php
Modal::begin([
    'id' => 'modal-chiefAccountantSignatureImage',
    'header' => '<h1>Загрузить подпись главного бухгалтера</h1>',
    'toggleButton' => false,
]);

echo $this->render('_partial_files_chief_accountant', [
    'model' => $model,
]);

Modal::end();
?>

<?php $this->registerJs('
    $(document).on("change", "#chief_is_chief_accountant_input", function () {
        $(".change-disabled-inputs input").prop("disabled", $(this).is(":checked"));
        $.uniform.update(".change-disabled-inputs input:checkbox");
        if ($(this).is(":checked")) {
            $("#chiefAccountantSignature-container").addClass("hidden");
        } else {
            $("#chiefAccountantSignature-container").removeClass("hidden");
        }
    });
') ?>
