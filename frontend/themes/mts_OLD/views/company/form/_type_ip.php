<?php
use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $creating boolean */

$inputConfig = $inputConfigRequired = $inputConfigMB15 = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'wrapperOptions' => [
        'class' => 'control-data',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'required';
$inputConfigMB15['options']['class'] = 'mb-15';

$inputConfigDetails = $inputConfigDetailsMB15 = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'wrapperOptions' => [
        'class' => 'control-data',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'style' => 'width: 100%',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigDetailsMB15['options']['class'] = 'mb-15';

$fioInitialsConfig = [
    'options' => [
        'class' => 'required',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
];

$textInputConfig = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeholder' => 'Автозаполнение по ИНН',
    ],
];

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-address_legal_city, #company-address_legal_street", function () {
            var city = $.trim($("#company-address_legal_city").val());
            var street = $.trim($("#company-address_legal_street").val());
            if (!empty(city) && !empty(street)) {
                $.post("/company/ifns", {"city": city, "street": street}, function(data) {
                    if (!empty(data.oktmo)) {
                        $("#company-oktmo").val(data.oktmo);
                    }
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>

<div class="edit-profile__info mts-block">
    <div class="form-group">
        <div class="col-sm-12">
            <?= $form->field($model, 'inn', $textInputConfig)->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">Индивидуальный предприниматель</div>
        </div>
        <div class="box-fio">
            <div class="form-group">
                <div class="col-sm-4">
                    <?= $form->field($model, 'ip_lastname', $inputConfigRequired)->label('Фамилия')->textInput(); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'ip_firstname', $inputConfigRequired)->label('Имя')->textInput(); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'ip_patronymic', $inputConfigRequired)->label('Отчество')->textInput([
                        'readonly' => (boolean) $model->has_chief_patronymic,
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div style="margin-left: 15px;">
                        <?= $form->field($model, 'has_chief_patronymic', ['validateOnChange' => true])->label('Нет отчества')->checkbox(false); ?>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <div style="margin-top:-15px">
        <?= $this->render('_partial_description', [
            'model' => $model,
            'form' => $form,
            'creating' => $creating,
        ]); ?>
    </div>
</div>

<div class="profile-form-tabs mts-block">
    <?= Tabs::widget([
        'id' => 'add_tabs',
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => ''],
        'items' => [
            [
                'label' => 'Расчетные счета',
                'encode' => false,
                'content' => $this->render('_partial_checking_account', [
                    'model' => $model,
                    'form' => $form,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'banks' => $banks,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Кассы',
                'content' => $this->render('_partial_cashbox', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'Склады',
                'content' => $this->render('_partial_store', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'E-money',
                'content' => $this->render('_partial_emoney', [
                    'model' => $model,
                    'form' => $form,
                    'inputConfig' => $inputConfig,
                ]),
            ],
        ],
    ]); ?>
</div>

<div class="details mts-block">
    <div class="portlet-title">
        <div class="caption big">Реквизиты</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'egrip', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okpo', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okud', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okved', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfigDetailsMB15)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfigDetailsMB15)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'oktmo', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_ip', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_employer', $inputConfigDetailsMB15)->textInput([
                    'maxlength' => true,
                ]); ?>

                <div class="form-group field-company-ip_certificate_number">
                    <div class="col-sm-12">
                        <?= Html::activeLabel($model, 'ip_certificate_number', [
                            'class' => 'control-label',
                        ]) ?>
                        <div class="control-data">
                            <table>
                                <tr>
                                    <td style="padding-right: 10px;">
                                        <?= Html::activeTextInput($model, 'ip_certificate_number', [
                                            'class' => 'form-control',
                                        ]) ?>
                                    </td>
                                    <td>
                                        <?= Html::activeLabel($model, 'certificateDate') ?>
                                    </td>
                                    <td style="position: relative;">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::activeTextInput($model, 'certificateDate', [
                                            'class' => 'form-control date-picker',
                                        ]) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'ip_certificate_issued_by', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('_partial_ifns', [
    'form' => $form,
    'model' => $model,
    'ifns' => $ifns,
]) ?>

<div style="margin-bottom: 20px;">
<?php echo $this->render('_partial_files', [
    'model' => $model,
]); ?>
</div>
