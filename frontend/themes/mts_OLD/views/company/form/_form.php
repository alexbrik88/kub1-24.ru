<?php

use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\widgets\Modal;
use backend\models\Bank;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $form ActiveForm */
/* @var $backUrl string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $checkingAccountant CheckingAccountant */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $submitAction string */

$params = !isset($cancelUrl) ? [
    'model' => $model,
    'ifns' => $ifns,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'banks' => $banks,
] : [
    'model' => $model,
    'ifns' => $ifns,
];

$view = $model->company_type_id == CompanyType::TYPE_IP ?
        Company::$companyFormView[CompanyType::TYPE_IP] :
        Company::$companyFormView[CompanyType::TYPE_OOO];

$this->registerJs('
$(document).on("change", "#company-has_chief_patronymic", function () {
    if ($(this).prop("checked")) {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .val("")
            .attr("readonly", true)
            .trigger("change");
        $(".field-company-chief_patronymic, .field-company-ip_patronymic")
            .removeClass("has-error")
            .find(".help-block").html("");
    } else {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .attr("readonly", false);
    }
});
');
?>

<div class="company-form">

    <?php $form = ActiveForm::begin([

        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'action' => isset($submitAction) ? $submitAction : ['update', 'backUrl' => isset($backUrl) ? $backUrl : null],
        'options' => [
            'enctype' => 'multipart/form-data',
            'id' => 'form-update-company',
        ],
    ]); ?>
    <?= Html::activeHiddenInput($model, 'tmpId') ?>
    <?= $form->field($model, 'hasAccountants')->label(false)->hiddenInput() ?>
    <?= $form->field($model, 'company_type_id')->label(false)->hiddenInput([
        'id' => 'company_form_input',
    ]); ?>

    <div class="form-body form-horizontal">

        <?= $form->errorSummary($model); ?>

        <?= $this->render($view, array_merge($params, [
            'form' => $form,
            'admin' => isset($admin) ? $admin : false,
            'creating' => isset($submitAction),
        ])); ?>

    </div>

    <?php
    Modal::begin([
        'id' => 'modal-companyImages',
        'header' => '<h1>Загрузить логотип, печать и подпись</h1>',
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('_partial_files_tabs', [
        'model' => $model,
    ]);

    Modal::end();
    ?>

    <div class="form-actions">
        <div class="row action-buttons">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn red text-white widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
            <div class="spinner-button col-sm-1 col-xs-1">
                <?php $undo = isset($cancelUrl) ? $cancelUrl : Url::to(['index']); ?>

                <?= Html::a('<span class="ladda-label">Отменить</span><span class="ladda-spinner"></span>', $undo, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $undo, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
    <?php $form->end(); ?>
</div>
<?php if (!isset($cancelUrl)): ?>
    <?= $this->render('modal_rs/_create', [
        'checkingAccountant' => $checkingAccountant,
        'company' => isset($submitAction) ? $model : null,
    ]); ?>
    <div id="company_rs_update_form_list">
        <?= $this->render('_rs_update_form_list', [
            'model' => $model,
        ]); ?>
    </div>
    <?php foreach ($banks as $bank) {
        echo $this->render('modal_bank/_create', [
            'bank' => $bank,
            'applicationToBank' => $applicationToBank,
            'redirectUrl' => Url::to(''),
        ]);
    }; ?>
<?php endif; ?>
<?php $this->render('_company_inn_api'); ?>
