<?php

use common\assets\JCropAsset;
use common\models\Company;
use devgroup\dropzone\DropZoneAsset;
use yii\bootstrap\Tabs;

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
JCropAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

if (empty($action)) {
    $action = 'view';
}
$items = [];
foreach (Company::$imageDataArray as $key => $value) {
    if ($key == 'chiefAccountantSignatureImage') {
        continue;
    }
    $items[] = [
        'label' => $value['label'],
        'content' => $this->render('_partial_files_tabs_item', [
            'model' => $model,
            'attr' => $key,
            'action' => $action,
        ]),
        'active' => $key === 'logoImage',
    ];
}
?>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'id' => 'company-image-tabs',
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => 'col-xs-4'],
        'linkOptions' => ['class' => 'text-center'],
        'items' => $items,
    ]); ?>
</div>
