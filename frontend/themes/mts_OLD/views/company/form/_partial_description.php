<?php
/* @var $model common\models\Company */
/* @var $creating boolean */

use common\components\helpers\Html;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\TaxationType;
use common\models\TimeZone;
use frontend\models\RegistrationForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'wrapperOptions' => [
        'class' => 'control-data',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$inputListConfig = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
];

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');
?>

<div class="form-group">
    <div class="col-sm-8">
        <?= $form->field($model, 'name_short', array_merge($textInputConfig, [
            'options' => [
                'class' => 'required',
            ],
        ]))->label()->textInput([
            'maxlength' => true,
            'disabled' => $model->company_type_id == CompanyType::TYPE_IP,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ]); ?>
    </div>
    <div class="col-sm-4">
        <label for="name" class="control-label">Форма</label>
        <div class="control-data">
            <?php $companyTypes = CompanyType::find()->andWhere(['in_company' => true])->all(); ?>
            <?= $form->field($model, 'company_type_id', [
                'template' => "{input}",
                'options' => [
                    'class' => '',
                ],
            ])->widget(Select2::class, [
                'data' => ArrayHelper::map($companyTypes, 'id', 'name_short'),
                'hideSearch' => true,
                'disabled' => count($companyTypes) === 1,
                'options' => [
                    'class' => 'form-control',
                    'id' => 'contractor_company_type',
                ]
            ])->label(false); ?>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <?= $form->field($model, 'name_full', array_merge($textInputConfig, [
            'options' => [
                'class' => 'required',
            ]
        ]))->label()->textInput([
            'maxlength' => true,
            'disabled' => $model->company_type_id == CompanyType::TYPE_IP,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ]); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label for="name" class="control-label">Часовой пояс</label>
        <div class="control-data">
            <?= $form->field($model, 'time_zone_id', [
                'template' => "{input}",
                'options' => [
                    'class' => '',
                ],
            ])->widget(Select2::class, [
                'data' => ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone'),
                'hideSearch' => true,
                'options' => [
                    'class' => 'form-control',
                ]
            ])->label(false); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($taxation, 'osno', array_merge($textInputConfig, [
            'options' => [
                'class' => 'required',
            ],
            'parts' => [
                '{input}' => Html::beginTag('div', [
                        'class' => '',
                        'style' => 'padding-top: 7px; display: inline-block;',
                    ]) . "\n" .
                    Html::activeCheckbox($taxation, 'osno', [
                        'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                        'disabled' => (boolean) $taxation->usn,
                    ]) . "\n" .
                    Html::activeCheckbox($taxation, 'usn', [
                        'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                        'disabled' => (boolean) $taxation->osno,
                    ]) . "\n" .
                    Html::activeCheckbox($taxation, 'envd', [
                        'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                    ]) . "\n" .
                    Html::activeCheckbox($taxation, 'psn', [
                        'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                        'disabled' => $model->company_type_id != CompanyType::TYPE_IP,
                    ]) . "\n" .
                    Html::beginTag('div', ['class' => 'tax-usn-config collapse' . ($taxation->usn ? ' in' : '')]) . "\n" .
                    $form->field($taxation, 'usn_type', [
                        'options' => [
                            'class' => '',
                            'style' => 'margin-bottom: 5px;',
                        ],
                    ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                        'class' => '',
                        'style' => 'margin-top: 10px;',
                        'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                            $item = Html::beginTag('div', ['style' => 'margin-bottom:10px;']);
                            $item .= Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                'style' => 'display: inline-block; width: 120px; margin-right: 30px;',
                            ]);
                            $item .= Html::activeTextInput($taxation, 'usn_percent', [
                                    'class' => 'form-control usn-percent usn-percent-' . $value,
                                    'style' => 'display: inline-block; width: 50px;',
                                    'disabled' => !$checked,
                                    'value' => $checked ? $taxation->usn_percent : $taxation->defaultUsnPercent($value),
                                    'data-default' => $taxation->defaultUsnPercent($value),
                                ]) . ' %';
                            $item .= Html::endTag('div');

                            return $item;
                        },
                    ])->label(false) . "\n" .

                    Html::beginTag('div', ['class' => 'col-lg-6 tax-patent-config collapse' . ($taxation->psn ? ' in' : '')]) . "\n" .

                    Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:4px']) .
                    Html::label('Субъект РФ, в котором получен патент', 'ip_patent_city', [
                        'class'=>'col-md-12 label-width',
                        'style' => 'padding-top:5px;']) .
                    Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-md-12']) .
                    Html::activeTextInput($model, 'ip_patent_city', [
                        'class' => 'form-control',
                        'style' => 'margin-left:0; max-width:270px',
                    ]) .
                    Html::endTag('div') .
                    Html::endTag('div') . "\n" .

                    Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:4px']) .
                    Html::label('Дата начала действия патента', 'patentDate', [
                        'class'=>'col-md-12 label-width',
                        'style' => 'padding-top:5px']) .
                    Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-md-12']) .
                    Html::activeTextInput($model, 'patentDate', [
                        'class' => 'form-control date-picker',
                        'style' => 'width:135px; padding-left:30px;margin-left:0',
                    ]) .
                    Html::tag('i', '', [
                        'class' => 'fa fa-calendar',
                        'style' => 'position: absolute; top: 10px; left: 25px; color: #cecece; cursor: pointer;',
                    ]) .
                    Html::endTag('div') .
                    Html::endTag('div') . "\n" .

                    Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:14px']) .
                    Html::label('Дата окончания действия патента', 'patentDateEnd', [
                        'class'=>'col-md-12 label-width',
                        'style' => 'padding-top:5px;text-align:right']) .
                    Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-md-12']) .
                    Html::activeTextInput($model, 'patentDateEnd', [
                        'class' => 'form-control date-picker',
                        'style' => 'width:135px; padding-left:30px;margin-left:0',
                    ]) .
                    Html::tag('i', '', [
                        'class' => 'fa fa-calendar',
                        'style' => 'position: absolute; top: 10px; left: 25px; color: #cecece; cursor: pointer;',
                    ]) .
                    Html::endTag('div') .
                    Html::endTag('div') .
                    Html::endTag('div') .
                    Html::endTag('div') . "\n" .



                    Html::endTag('div'),
            ],
        ]))->label('Система налогообложения'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="nds-view-osno collapse <?= $taxation->osno ? ' in' : ''; ?>">
            <div class="form-group">
                <div class="col-sm-12">
                    <?= $form->field($model, 'nds', $inputListConfig)->radioList($ndsViewOsno, [
                        'class' => 'inp_one_line_company',
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::tag('label',
                                    Html::radio($name, $checked, ['value' => $value]) . $label,
                                    [
                                        'class' => 'radio-inline p-o radio-padding',
                                    ]) . Html::a('Пример счета', [Url::to('/company/view-example-invoice'), 'nds' => $value == 1 ? true : false], [
                                    'class' => 'radio-inline p-o radio-padding',
                                    'target' => '_blank',
                                ]);
                        },
                    ])->label('В счетах цену за товар/услугу указывать:');?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <label class="control-label" style="padding-top: 0;">
            Формировать PDF с печатью и подписью
        </label>
        <div class="box-pad-15">
            <?= Html::activeCheckbox($model, 'pdf_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <label class="control-label" style="padding-top: 0;">
            Отправлять с печатью и подписью
        </label>
        <div class="box-pad-15">
            <?= Html::activeCheckbox($model, 'pdf_send_signed', [
                'labelOptions' => ['style' => 'width: 110px;'],
                'disabled' => true,
            ]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_send_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <?= $form->field($model, 'is_additional_number_before', $inputListConfig)->radioList(Company::$addNumPositions, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                    'class' => 'radio-inline p-o radio-padding',
                ]);
            },
        ]); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <?= $form->field($model, 'phone', array_merge($textInputConfig, [
            'options' => [
                'class' => 'required',
            ]
        ]))->widget(\yii\widgets\MaskedInput::className(), [

            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control',
                'placeholder' => '+7(XXX) XXX-XX-XX (Нужен для восстановления пароля)',
            ],
        ]); ?>

    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'email', $textInputConfig)->label()->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-sm-4">
    </div>
</div>



<?php

$js = <<<JS
$(document).on("change", "#form-update-company #company-company_type_id", function() {
    var form = this.form;
    if (this.value == '1') {
        $("#companytaxationtype-psn", form).prop("disabled", false).uniform("refresh");
    } else {
        $("#companytaxationtype-psn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-osno", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".nds-view-osno", form).collapse("show");
    } else {
        $("#companytaxationtype-usn", form).prop("disabled", false).uniform("refresh");
        $(".nds-view-osno", form).collapse("hide");
        $(".nds-view-osno input[type=radio]", form).prop("checked", false).uniform("refresh");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-usn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-usn-config", form).collapse("show");
    } else {
        $("#companytaxationtype-osno", form).prop("disabled", false).uniform("refresh");
        $(".tax-usn-config", form).collapse("hide");
        $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true).uniform("refresh");
        $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false).uniform("refresh");
        $("#companytaxationtype-usn_percent", form).val("");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-psn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-patent-config", form).collapse("show");
    } else {
        $("#companytaxationtype-envd", form).prop("disabled", false).uniform("refresh");
        $(".tax-patent-config", form).collapse("hide");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-usn_type input[type=radio]", function() {
    var form = this.form;
    var value = $('#companytaxationtype-usn_type input[type=radio]:checked', form).val();
    $('#companytaxationtype-usn_type input.usn-percent', form).each(function (i, item) {
        $(item).prop('disabled', true).val($(item).data('default'));
    });
    $('#companytaxationtype-usn_type input.usn-percent-' + value, form).prop('disabled', false);
});
JS;

$this->registerJs($js);
