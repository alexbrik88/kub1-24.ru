<?php
/* @var $model common\models\Company */
use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\models\Company;
use yii\helpers\Html;

/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-4">
        <div class="mts-block">
            <label for="name" class="control-label mb-15">Логотип компании</label>
            <div class="portlet-body">
                <div id="logoImage-image-result">
                    <table class="company-image-preview">
                        <tr>
                            <td>
                                <?php $imgPath = $model->getImage('logoImage', true) ? : $model->getImage('logoImage');
                                if (is_file($imgPath)) : ?>
                                    <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                <?php else : ?>
                                    <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 1090х370</div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 20px auto 10px;">
                    <?= Html::button('Добавить логотип', [
                        'class' => 'btn red',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-companyImages',
                        'style' => 'margin-right: 20px;',
                        'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab0"]\').tab(\'show\');',
                    ]) ?>

                    <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteLogoImage', [
                        'label' => 'Удалить',
                        'class' => 'radio-inline p-o radio-padding',
                    ]) : ''; ?>
                </div>
                <div>
                    <?php
                    $modificationDate = ImageHelper::getModificationDate($imgPath);
                    if ($modificationDate) {
                        echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="mts-block">
            <label for="name" class="control-label mb-15">Печать компании</label>
            <div class="portlet-body">
                <div id="printImage-image-result">
                    <table class="company-image-preview">
                        <tr>
                            <td>
                                <?php $imgPath = $model->getImage('printImage', true) ? : $model->getImage('printImage');
                                if (is_file($imgPath)) : ?>
                                    <?= EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                <?php else : ?>
                                    <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 400х400</div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 20px auto 10px;">
                    <?= Html::button('Добавить печать', [
                        'class' => 'btn red',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-companyImages',
                        'style' => 'margin-right: 20px;',
                        'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab1"]\').tab(\'show\');',
                    ]) ?>

                    <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deletePrintImage', [
                        'label' => 'Удалить',
                        'class' => 'radio-inline p-o radio-padding',
                    ]) : ''; ?>
                </div>
                <div>
                    <?php
                    $modificationDate = ImageHelper::getModificationDate($imgPath);
                    if ($modificationDate) {
                        echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="mts-block">
            <label for="name" class="control-label mb-15">Подпись руководителя</label>
            <div class="portlet-body">
                <div id="chiefSignatureImage-image-result">
                    <table class="company-image-preview">
                        <tr>
                            <td>
                                <?php $imgPath = $model->getImage('chiefSignatureImage', true) ? : $model->getImage('chiefSignatureImage');
                                if (is_file($imgPath)) : ?>
                                    <?= EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                <?php else : ?>
                                    <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 330x100</div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 20px auto 10px;">
                    <?= Html::button('Добавить подпись', [
                        'class' => 'btn red',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-companyImages',
                        'style' => 'margin-right: 20px;',
                        'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab2"]\').tab(\'show\');',
                    ]) ?>

                    <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteChiefSignatureImage', [
                        'label' => 'Удалить',
                        'class' => 'radio-inline p-o radio-padding',
                    ]) : ''; ?>
                </div>
                <div>
                    <?php
                    $modificationDate = ImageHelper::getModificationDate($imgPath);
                    if ($modificationDate) {
                        echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 30px;">
        Не получается загрузить логотип, печать или подпись?<br>
        Пришлите нам ваши файлы на
        <a href="mailto:<?= \Yii::$app->params['emailList']['support'] ?>"><?= \Yii::$app->params['emailList']['support'] ?></a>
        и мы самостоятельно загрузим их.
    </div>
</div>

<?php $this->render('_company_update_files'); ?>