<?php
use common\models\address;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
?>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box box-adress gray">
            <div class="portlet-title">
                <div class="caption">Юридический адрес</div>
            </div>
            <div class="portlet-body">

                <?= $this->render('_partial_address', [
                    'model' => $model,
                    'form' => $form,
                    'prefix' => 'address_legal',
                ]); ?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box box-adress gray">
            <div class="portlet-title">
                <label class="checkbox-inline match-with-leader pull-right">
                    <?= Html::activeCheckbox($model, 'address_legal_is_actual', [
                        'id' => 'address_legal_is_actual_input',
                        'label' => false,
                    ]); ?>
                    совпадает с юридическим
                </label>

                <div class="caption">Фактический адрес</div>
            </div>
            <div class="portlet-body">

                <?= $this->render('_partial_address', [
                    'model' => $model,
                    'form' => $form,
                    'prefix' => 'address_actual',
                ]); ?>

            </div>
        </div>
    </div>
</div>

<?php
$js = <<<JS
$(function () {
    var addressCheckBox = $('#address_legal_is_actual_input');
    addressCheckBox.on('change init', function (e) {
        addressCheckBox.parents('.box-adress:first').find('.portlet-body').find('select, input').attr('disabled', this.checked);
    }).trigger('init');
});
JS;
$this->registerJs($js);
