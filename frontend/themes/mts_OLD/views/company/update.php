<?php

use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use backend\models\Bank;
use common\models\company\ApplicationToBank;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $backUrl string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $checkingAccountant CheckingAccountant */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */

$this->title = 'Редактирование профиля компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
$model->checkFirstUpdate();
?>
<div class="company-update">
    <?= $this->render('form/_form', [
        'model' => $model,
        'ifns' => $ifns,
        'backUrl' => $backUrl,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'checkingAccountant' => $checkingAccountant,
        'banks' => $banks,
        'applicationToBank' => $applicationToBank,
    ]); ?>
</div>
<?php
$successFlashMessageCreate = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с добавлен</div>";
$successFlashMessageUpdate = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с изменен</div>";
$successFlashMessageDelete = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с удален</div>";
?>
<?php $this->registerJs('
    $(document).on("show.bs.modal", "#add-company-rs", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("show.bs.modal", ".exists-company-rs", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("submit", "form.form-checking-accountant", function(){
        var formId = $(this).attr("id");
        var $isNewRecord = $(this).attr("is_new_record");
        $.post(
            $("#" + formId).attr("action"),
            $("#" + formId).serialize(),
            function(data){
                if(data.status == 1) {
                    var $message = $isNewRecord == 1 ? "' . $successFlashMessageCreate . '" : "' . $successFlashMessageUpdate . '";
                    $("#form-checking-accountant-").find("input:text").val("");
                    $(".modal").modal("hide");
                    $("#company_rs_update_form_list").html(data.rs_form_list);
                    $(".page-content").prepend($message);
                    $.pjax.reload("#rs-pjax-container", {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
                } else {
                    $("#" + formId).html( $(data.html).find("#" + formId).html() );
                }
            }
        );
        return false;
    });
    $(document).on("click", "button.btn-confirm-yes", function(){
        var $this = $(this);
        $.ajax({
            url: $this.data("url"),
            type: $this.data("type"),
            data: $this.data("params"),
            success: function(data) {
                if(data.status == 1) {
                    $("#company_rs_update_form_list").html(data.rs_form_list);
                    $(".page-content").prepend("' . $successFlashMessageDelete . '");
                    $.pjax.reload("#rs-pjax-container", {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
                }
                if (data.js) {
                    eval(data.js);
                }
            }
        });
        return false;
    });
'); ?>
