<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.07.2017
 * Time: 8:55
 */


use common\components\ImageHelper;
use yii\helpers\Html;
use common\models\Company;
use yii\helpers\Url;
use common\components\TextHelper;

/** @var $this yii\web\View
 * @var Company $company
 */
?>
<div class="col-md-12 portlet border-darc widget-coming widget-home clear-box ov-hidden">
    <div class="col-xs-12 col-sm-4 col-md-3 text-center" style="text-align: center; padding-bottom: 10px">
        <?= ImageHelper::getThumb('img/ico-rub.png', ImageHelper::THUMB_LARGE); ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-9 pad3" style="text-align: center;">
        <div class="col-xs-12 col-sm-6 col-md-12 text-center pad3">
            <span style="font-size: 15px;font-weight: bold;">
                Как заработать вместе с КУБом?
            </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-12 text-center pad3 how-to-earn-more" style="margin-top: 0px;">
            <?= Html::a('Партнерская программа', 'javascript:;'); ?>
        </div>
    </div>


    <div class="col-md-12 more-affiliate_program-information">
        <p>
            Вы можете заработать, рекомендуя сервис КУБ. Ваш заработок
            составит 50% от суммы первого оплаченного пользователя и 25%
            от сумм последующих оплаченных пользователями, пришедшими по
            вашей рекомендации. Заработанные деньги вы сможете получить
            на
            свой расчетный счет или использовать для оплаты сервиса КУБ.
        </p>

        <p>
            Вы получите 150 руб. с каждой оплаты за 1 месяц. <br>
            Вы получите 540 руб. с каждой оплаты за 4 месяц. <br>
            Вы получите 1440 руб. с каждой оплаты за 12 месяцев.
        </p>
                <span class="col-md-12 text-center p-l-0 p-r-0"
                      style="font-size: 14px;font-weight: bold;margin-top: 10px;">Как начать зарабатывать?</span>

        <p>
            Нажмите на кнопку ниже и появится ссылка. Эта ссылка
            уникальная
            и привязана к вам. Скопируйте эту ссылку и отправьте
            партнерам,
            контрагентам и знакомым предпринимателям, что бы они перешли
            по ней на сервис КУБ и зарегистрировались. Когда они начнут
            платить за КУБ, вы начнете зарабатывать.
        </p>

        <div class="box-title-info-txt clear-box"
             style="margin-bottom: 10px;"></div>
                <span class="col-md-12 p-l-0 p-r-0"
                      style="font-size: 14px;font-weight: bold;">Ваша партнерская ссылка:</span>

        <?php if ($company->affiliate_link): ?>
            <?= Html::a(Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link,
                Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link, [
                    'target' => '_blank',
                    'class' => 'affiliate_invite',
                ]); ?>
            <?= ImageHelper::getThumb('img/copy.png', [20, 20], [
                'class' => 'copy-affiliate-link',
                'title' => 'Скопировать ссылку',
            ]); ?>
        <?php else: ?>
            <div class="form-actions">
                <div class="row action-buttons">
                    <div class="col-sm-6 col-xs-6">
                        <?= Html::a('Получить ссылку', 'javascript:;', [
                            'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs generate-affiliate-link',
                            'data-url' => Url::to(['generate-affiliate-link', 'page' => 'company']),
                        ]); ?>
                        <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                            'class' => 'btn darkblue text-white widthe-100 hidden-lg generate-affiliate-link',
                            'title' => 'Получить ссылку',
                            'data-url' => Url::to(['generate-affiliate-link', 'page' => 'company']),
                        ]); ?>
                    </div>
                    <div class="col-sm-6 col-xs-6"></div>
                </div>
            </div>
        <?php endif; ?>

        <span class="col-md-12 p-l-0 p-r-0"
              style="font-size: 14px;font-weight: bold;padding-top: 15px;">Ваше вознаграждение: <?= $company->affiliate_sum; ?>
            руб.</span>

        <p>
            Привлечено
            пользователей: <?= $company->getInvitedCompanies()->count(); ?> <br>
            Пользователей
            оплатило: <?= $company->getPayedInvitedCompaniesCount(); ?>
        </p>

        <div class="form-actions" style="margin-bottom: 15px;">
            <div class="row action-buttons">
                <div class="col-sm-6 col-xs-6">
                    <?= Html::a('Оплатить за КУБ', $company->affiliate_link ? Url::to(['/subscribe']) : null, [
                        'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs',
                        'disabled' => $company->affiliate_link ? false : true,
                    ]); ?>
                    <?= Html::a('<i class="fa fa-rub fa-2x"></i>', $company->affiliate_link ? Url::to(['/subscribe']) : null, [
                        'class' => 'btn darkblue text-white widthe-100 hidden-lg save-document',
                        'title' => 'Оплатить за КУБ',
                        'disabled' => $company->affiliate_link ? false : true,
                    ]); ?>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <?= Html::a('Выставить счет', null, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'disabled' => $company->affiliate_link ? false : true,
                        'data-toggle' =>  $company->affiliate_link ? 'modal' : null,
                        'data-target' =>  $company->affiliate_link ? '#bill-invoice-remuneration' : null,
                    ]); ?>
                    <?= Html::a('<i class="ico-qiwi-smart-pls fs fs-mini font-19"></i>', $company->affiliate_link ? 'javascript:;' : null, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Выставить счет',
                        'disabled' => $company->affiliate_link ? false : true,
                        'data-toggle' =>  $company->affiliate_link ? 'modal' : null,
                        'data-target' =>  $company->affiliate_link ? '#bill-invoice-remuneration' : null,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
