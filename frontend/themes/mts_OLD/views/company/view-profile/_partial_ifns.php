<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

$ifns = $company->ifns;

?>

<?php if ($ifns) : ?>

<div class="details mts-block">
    <div class="portlet-title">
        <div class="caption big">Реквизиты налоговой</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-sm-6">
                <label for="adress"
                       class="control-label">Код ИФНС</label>
                <div class="control-data">
                    <p class="form-control-static"><?= $company->ifns_ga ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'gb', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->gb ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g1', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g1 ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g2', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g2 ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g4', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g4 ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g8', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g8 ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g6', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g6 ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g9', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g9 ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g7', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g7 ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <?= Html::activeLabel($ifns, 'g11', [
                    'class' => 'control-label',
                ]) ?>
                <div class="control-data">
                    <p class="form-control-static"><?= $ifns->g11 ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif ?>
