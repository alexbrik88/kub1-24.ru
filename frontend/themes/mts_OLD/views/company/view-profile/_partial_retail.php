<?php

use common\components\grid\GridView;
use frontend\models\RetailPointSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new RetailPointSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>


<div class="portlet box gray">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'retail-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
                            ],

                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],

                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],

                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => "{items}\n{pager}",

                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'address',
                                'kpp',
                                'login',
                            ],
                        ]); ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>