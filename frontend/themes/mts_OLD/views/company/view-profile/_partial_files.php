<?php
/** @var common\models\Company $company */
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;

?>

<div class="row">
    <div class="col-sm-4">
        <div class="mts-block">
            <label for="name" class="control-label">Логотип компании</label>
            <div class="form-group signature">
                <div class="portlet align-center signature_block-image">
                    <?php
                    $imgPath = $company->getImage('logoImage');
                    if (is_file($imgPath)) {
                        echo EasyThumbnailImage::thumbnailImg($imgPath, 545, 186, EasyThumbnailImage::THUMBNAIL_INSET, [
                            'style' => 'max-width: 100%; max-height: 100%;'
                        ]);
                    } else {
                        echo '<img src="" class="signature_image">';
                    }; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="mts-block">
            <label for="name" class="control-label">Печать компании</label>
            <div class="form-group signature">
                <div class="portlet align-center signature_block-image">
                    <?php
                    $imgPath = $company->getImage('printImage');
                    if (is_file($imgPath)) {
                        echo EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET, [
                            'style' => 'max-width: 100%; max-height: 100%;'
                        ]);
                    } else {
                        echo '<img src="" class="signature_image">';
                    }; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="mts-block">
            <label for="name" class="control-label">Подпись руководителя</label>
            <div class="form-group signature">
                <div class="portlet align-center signature_block-image">
                    <?php
                    $imgPath = $company->getImage('chiefSignatureImage');
                    if (is_file($imgPath)) {
                        echo EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET, [
                            'style' => 'max-width: 100%; max-height: 100%;'
                        ]);
                    } else {
                        echo '<img src="" class="signature_image">';
                    }; ?>
                </div>
            </div>
        </div>
    </div>    
</div>