<?php

use frontend\models\EmoneySearch;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$searchModel = new EmoneySearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>


<div class="portlet box gray">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'emoney-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                        <?= $this->render('@frontend/views/emoney/_table', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]) ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>