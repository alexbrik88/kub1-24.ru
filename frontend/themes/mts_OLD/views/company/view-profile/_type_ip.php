<?php
/** @var common\models\Company $company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */

use common\components\date\DateHelper;
use common\models\Company;
use common\models\TimeZone;
use common\models\company\CheckingAccountantSearch;
use frontend\rbac\UserRole;
use backend\models\Bank;
use yii\bootstrap\Tabs;

?>

<div class="edit-profile__info mts-block" style="">
    <div class="form-group">
        <div class="col-sm-4">
            <label for="name" class="control-label">Краткое наименование организации</label>
            <div class="control-data">
                <p class="form-control-static"><?php echo $company->getTitle(true); ?></p>
            </div>
        </div>
        <div class="col-sm-8">
            <label for="name" class="control-label">Полное наименование организации</label>
            <div class="control-data">
                <p class="form-control-static"><?php echo $company->title; ?></p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label for="name" class="control-label">Система налогообложения</label>
            <div class="control-data">
                <p class="form-control-static"><?php echo $company->companyTaxationType->name; ?></p>
            </div>
        </div>
        <?php if (($nds = \common\models\NdsOsno::findOne($company->nds)) !== null): ?>
            <div class="col-sm-4">
                <label for="name" class="control-label">В счетах цену за товар/услугу указывать</label>
                <div class="control-data">
                    <p class="form-control-static"><?= $nds->name; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-sm-4">
            <label for="name" class="control-label">Часовой пояс</label>
            <div class="control-data">
                <p class="form-control-static"><?php $timeZone = TimeZone::findOne($company->time_zone_id);
                    echo $timeZone['out_time_zone'] ?></p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label for="name" class="control-label">Отправлять с печатью и подписью</label>
            <div class="control-data">
                <p class="form-control-static">
                    <?= join(', ', array_filter([
                        $company->pdf_send_signed ? $company->getAttributeLabel('pdf_send_signed') : '',
                        $company->pdf_act_send_signed ? $company->getAttributeLabel('pdf_act_send_signed') : '',
                    ])) ?>
                </p>
            </div>
        </div>
        <div class="col-sm-4">
            <label for="name" class="control-label">Формировать PDF с печатью и подписью</label>
            <div class="control-data">
                <p class="form-control-static">
                    <?= join(', ', array_filter([
                        $company->pdf_signed ? $company->getAttributeLabel('pdf_signed') : '',
                        $company->pdf_act_signed ? $company->getAttributeLabel('pdf_act_signed') : '',
                    ])) ?>
                </p>
            </div>
        </div>
        <div class="col-sm-4">
            <label for="name" class="control-label">К номеру счета Доп номер</label>
            <div class="control-data">
                <p class="form-control-static">
                    <?= (isset(Company::$addNumPositions[$company->is_additional_number_before])) ?
                        Company::$addNumPositions[$company->is_additional_number_before] : ''; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label for="name" class="control-label">Телефон:</label>
            <div class="control-data">
                <p class="form-control-static"><?php echo $company->phone; ?></p>
            </div>
        </div>
        <div class="col-sm-4">
            <label for="name" class="control-label">Электронная
                почта:</label>

            <div class="control-data">
                <p class="form-control-static">
                    <a href="mailto:<?php echo $company->email; ?>"><?php echo $company->email; ?></a>
                </p>
            </div>
        </div>
        <div class="col-sm-4">

        </div>
    </div>
</div>
<?php if (!isset($admin)): ?>
    <div class="profile-form-tabs mts-block">
        <?= Tabs::widget([
            'options' => ['class' => 'nav-form-tabs row'],
            'headerOptions' => ['class' => ''],
            'items' => [
                [
                    'label' => 'Расчетные счета',
                    'encode' => false,
                    'content' => $this->render('_partial_checking_accountant', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'banks' => $banks,
                    ]),
                    'active' => true,
                ],
                [
                    'label' => 'Магазины',
                    'content' => $this->render('_partial_retail', ['model' => $company]),
                ],
                [
                    'label' => 'Кассы',
                    'content' => $this->render('_partial_cashbox', ['model' => $company]),
                ],
                [
                    'label' => 'E-money',
                    'content' => $this->render('_partial_emoney', ['model' => $company]),
                ],
                [
                    'label' => 'Склады',
                    'content' => $this->render('_partial_store', ['model' => $company]),
                ],
            ],
        ]); ?>
    </div>
<?php endif; ?>

<div class="details mts-block">
    <div class="portlet-title">
        <div class="caption big">Реквизиты</div>
    </div>
    <div class="portlet-body" style="padding:5px;">
        <div class="row">
            <div class="col-xs-1-5">
                <label for="KPP"
                       class="control-label">ИНН</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->inn ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="INN"
                       class="control-label">ОГРНИП</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->egrip ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="adress"
                       class="control-label">ОКПО</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->okpo ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="adress"
                       class="control-label">ОКВЭД</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->okved ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="bank"
                       class="control-label">ОКТМО</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->oktmo ?: '—'; ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="padding:0 10px;">
                <label for="adress"
                       class="control-label">Юридический адрес</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->getAddressLegalFull() ?: '—'; ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="padding:0 10px;">
                <label for="adress"
                       class="control-label">Фактический адрес</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->getAddressActualFull() ?: '—'; ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1-5">
                <label for="correspondent-account"
                       class="control-label">ФСС</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->fss ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="bank"
                       class="control-label"
                       style="width:215px;">ПФР (как ИП)</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->pfr_ip ?: '—'; ?></p>
                </div>
            </div>
            <div class="col-xs-1-5">
                <label for="bank"
                       class="control-label"
                       style="width:215px;">ПФР (как работодателя)</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo $company->pfr_employer ?: '—'; ?></p>
                </div>
            </div>
            <div style="width:40%;float: left;padding:0 10px;">
                <label for="bank" class="control-label">Дата постановки на учёт в налоговом органе</label>

                <div class="control-data">
                    <p class="form-control-static"><?php echo DateHelper::format($company->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—'; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->render('_partial_ifns', [
    'company' => $company,
]); ?>

<?php echo $this->render('_partial_files', [
    'company' => $company,
]); ?>
