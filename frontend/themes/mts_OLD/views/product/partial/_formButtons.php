<?php
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

// B2B return
if ('B2B' == Yii::$app->request->get('backTo'))
    $returnUrl = ['/b2b/products'];
else
    $returnUrl = null;

/* @var $model common\models\product\Product */
?>

<div class="form-actions">
    <div class="row action-buttons buttons-fixed">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton($model->isNewRecord ?
                '<span class="ladda-label">Создать</span><span class="ladda-spinner"></span>' :
                '<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => $model->isNewRecord ?
                    'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button' :
                    'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-floppy-o fa-2x"></i>' : '<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn darkblue widthe-100 hidden-lg',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 55%;"></div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::a('Отменить', $model->isNewRecord
                ? ($returnUrl ?: ['product/index', 'productionType' => $model->production_type,])
                : ['product/view', 'productionType' => $model->production_type, 'id' => $model->id,],
                [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $model->isNewRecord
                ? ($returnUrl ?: ['product/index', 'productionType' => $model->production_type,])
                : ['product/view', 'productionType' => $model->production_type, 'id' => $model->id,],
                [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]); ?>
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <?php if (!$model->is_deleted && !$model->isNewRecord && Yii::$app->user->can(permissions\Product::DELETE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::toRoute(['delete', 'productionType' => $model->production_type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
                ]);
                echo \frontend\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                        'title' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
                ]);
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>