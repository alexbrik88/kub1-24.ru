<?php
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use dosamigos\fileinput\BootstrapFileInput;
use dosamigos\fileinput\FileInput;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$units = ProductUnit::findSorted()
    ->andWhere(['goods' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}

$storeArray = $model->company->getStores()->orderBy(['is_main' => SORT_DESC])->all();

if ($model->isNewRecord) {
    $model->group_id = ProductGroup::WITHOUT;
}
?>
<div class="row">
    <div class="col-sm-8">
        <?= $form->field($model, 'title')->textInput([
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'group_id', [
            'options' => [
                'class' => 'form-group'
            ],
        ])->widget(ProductGroupDropdownWidget::classname(), [
            'productionType' => Product::PRODUCTION_TYPE_GOODS,
        ])->label('Группа товара'); ?>

        <?= $form->field($model, 'code', $thinFieldOptions)->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>

        <?= $form->field($model, 'article', $thinFieldOptions)->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'barcode', $thinFieldOptions)->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'product_unit_id', [
            'options' => [
                'class' => 'form-group required'
            ],
        ])->widget(Select2::classname(), [
            'data' => ArrayHelper::map($units, 'id', 'name'),
            'options' => [
                'prompt' => '',
                'options' => $unitsOptions,
            ]
        ]); ?>

        <?= $form->field($model, 'weight', $thinFieldOptions)->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>

        <?= $form->field($model, 'volume', $thinFieldOptions)->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'image', [
            'wrapperOptions' => [
                'class' => 'col-md-12',
                'style' => 'margin-top: 15px;'
            ],
            'hintOptions' => [
                'tag' => 'div',
                'class' => 'hidden',
            ],
            'horizontalCssClasses' => [
                'offset' => '',
                'hint' => 'col-sm-12',
            ],
        ])->widget(FileInput::className(), [
            'options' => ['accept' => 'image/*', 'multiple' => false],
            'thumbnail' => ($src = $model->getImageThumb()) ?
                Html::img($src, [
                    'class' => 'product-photo-preview-form',
                    'alt' => '',
                ]) :
                null,
            'customView' => '@frontend/views/product/partial/_productImage.php',
            'style' => FileInput::STYLE_CUSTOM,
        ])->hint(Html::activeCheckbox($model, 'img_del', ['label' => false])); ?>
        <div class="">
            <div style="margin: 15px 0 0;">
                <span style="font-weight: bold;">Описание</span>
                <?= Html::tag('span', '', [
                    'id' => 'comment_photo_update',
                    'class' => 'glyphicon glyphicon-pencil',
                    'style' => 'cursor: pointer;',
                ]); ?>
            </div>
            <div id="comment_photo_view" class="">
                <?= Html::encode($model->comment_photo) ?>
            </div>
            <?= Html::beginTag('div', [
                'id' => 'comment_photo_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_photo_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: 1px; right: 3px; cursor: pointer; font-size: 20px;',
            ]); ?>
            <?= Html::activeTextarea($model, 'comment_photo', [
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
            ]); ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>


<div class="profile-form-tabs" style="margin-bottom: 15px;">
    <?= Tabs::widget([
        'options' => [
            'class' => 'nav-form-tabs',
            'style' => 'margin: 0 -5px;',
        ],
        'headerOptions' => [
            'style' => 'padding: 0 5px;',
        ],
        'linkOptions' => [
            'style' => 'padding: 10px 15px 11px;',
        ],
        'tabContentOptions' => [
            'class' => 'portlet box darkblue',
        ],
        'itemOptions' => [
            'class' => 'portlet-body',
        ],
        'items' => [
            [
                'label' => 'Цена',
                'content' => $this->render('_productPrice', [
                    'form' => $form,
                    'model' => $model,
                    'thinFieldOptions' => $thinFieldOptions,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Остатки',
                'content' => $this->render('_productQuantity', [
                    'form' => $form,
                    'model' => $model,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]),
            ],
            [
                'label' => 'Доп. информация',
                'content' => $this->render('_productInfo', [
                    'form' => $form,
                    'model' => $model,
                    'thinFieldOptions' => $thinFieldOptions,
                ]),
            ],
            [
                'label' => 'Комментарии',
                'content' => $this->render('_productComment', [
                    'form' => $form,
                    'model' => $model,
                    'thinFieldOptions' => $thinFieldOptions,
                ]),
            ],
        ],
    ]); ?>
</div>

<?php
$massGrossId = \yii\helpers\Html::getInputId($model, 'mass_gross');
$productUnitId = \yii\helpers\Html::getInputId($model, 'product_unit_id');
$countableUnits = json_encode(array_keys(ProductUnit::$countableUnits));
$js = <<<JS
    var productMassGross = $('#$massGrossId');
    var countableUnits = $countableUnits;
    $('#$productUnitId').on('change init', '', function (e) {
        productMassGross.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
    }).trigger('init');
    $(document).on("click", "#comment_photo_update", function () {
        $("#comment_photo_view").toggleClass("hidden");
        $("#comment_photo_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_photo_save", function () {
        $("#comment_photo_view").text($('#product-comment_photo').val().trim());
        $("#comment_photo_form").addClass("hidden");
        $("#comment_photo_view").removeClass("hidden");
    });
JS;
$this->registerJs($js);
?>
