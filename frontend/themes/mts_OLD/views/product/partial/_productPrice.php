<?php

use common\components\TextHelper;
use common\models\TaxRate;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$taxType = $model->company->companyTaxationType;
$taxItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');

$checkbox = Html::label('Не для продажи ' . Html::activeCheckbox($model, 'not_for_sale', ['label' => false]), null, [
    'class' => 'control-label bold-text content-right',
    'style' => 'width: 100%; text-align: right !important;',
]);
$notForSaleTemplate = "{label}\n{beginWrapper}\n<div class=\"row\">\n";
$notForSaleTemplate .= "<div class=\"col-xs-6\">{input}</div>\n";
$notForSaleTemplate .= "<div class=\"col-xs-6\">{$checkbox}</div>\n";
$notForSaleTemplate .= "</div>\n{error}\n{hint}\n{endWrapper}\n";
$notForSaleConf = [
    'template' => $notForSaleTemplate,
];
?>

<?= $form->field($model, 'price_for_buy_with_nds', [
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line-product',
        'style' => 'padding-left:4px;width:31%;',
    ],
])->textInput([
    'value' => $canViewPriceForBuy ?
        ($model->isNewRecord ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds)) :
        ($model->isNewRecord ? '0' : Product::DEFAULT_VALUE),
    'disabled' => !$canViewPriceForBuy,
    'class' => 'form-control js_input_to_money_format',
]); ?>

<?= $form->field($model, 'price_for_buy_nds_id', [
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
        'style' => 'padding-left:4px;',
    ],
])->label()->radioList($taxItems, [
    'class' => 'radio-list lastItemMrgRight0',
    'unselect' => $canViewPriceForBuy ? '' : null,
    'item' => function ($index, $label, $name, $checked, $value) use ($canViewPriceForBuy) {
        return Html::tag('label', Html::radio($name, $checked, [
            'value' => $value,
            'disabled' => !$canViewPriceForBuy,
        ]) . $label,[
            'class' => 'radio-inline m-l-20',
        ]);
    },
]); ?>
<?= $form->field($model, 'price_for_sell_with_nds', [
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
    ],
    'template' => '{label}{beginWrapper}<div class="row"><div style="padding-left: 4px;width:62%;" class="col-xs-6">{input}</div>
                    <div class="col-xs-3" style="margin-right: -15px;"></div><div class="col-xs-4" style="margin-left: -15px;">
                    <label class="control-label bold-text content-right" style="width: 100%; text-align: right !important;">
                    Не для продажи ' . Html::activeCheckbox($model, 'not_for_sale', ['label' => false]) .
                    '</label></div></div>{error}{hint}{endWrapper}'
])->textInput([
    'value' => empty($model->price_for_sell_with_nds) ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds),
    'class' => 'form-control js_input_to_money_format for_sale_input',
    'disabled' => (boolean)$model->not_for_sale,
]); ?>

<?php if ($taxType->osno || $taxType->usn) : ?>
    <?= $form->field($model, 'price_for_sell_nds_id', [
        'wrapperOptions' => [
            'class' => 'col-md-6 inp_one_line-product',
            'style' => 'padding-left:4px;',
        ],
    ])->label()->radioList($taxItems, [
        'class' => 'radio-list lastItemMrgRight0',
        'item' => function ($index, $label, $name, $checked, $value) use ($model) {
            $radio = Html::radio($name, $checked, [
                'value' => $value,
                'class' => 'for_sale_input',
                'disabled' => (boolean)$model->not_for_sale,
            ]);

            return Html::tag('label', $radio . $label, [
                'class' => 'radio-inline m-l-20',
            ]);
        },
    ]); ?>
<?php endif; ?>