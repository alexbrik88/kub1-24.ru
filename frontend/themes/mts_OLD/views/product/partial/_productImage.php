<?php

/* @var $thumbnail string */
/* @var $field string */

?>

<?php if ($thumbnail) : ?>
    <style type="text/css">
    .fileinput:not(.fileinput-empty) .fileinput-cancel {
        display: none;
    }
    .fileinput.fileinput-empty .fileinput-clear,
    .fileinput.fileinput-empty .thumbnail-exists {
        display: none;
    }
    </style>
<?php endif ?>

<div class="fileinput fileinput-new" data-provides="fileinput">
    <div>
        <div class="btn-file" style="width: 80px; cursor: pointer;">
            <button class="fileinput-new btn yellow btn-sm" style="width:33px; height: 27px; margin-right: 10px;">
                <span class="upload-button glyphicon glyphicon-download-alt"></span>
            </button>
            <button class="fileinput-exists btn yellow btn-sm" style="width:33px; height: 27px; margin-right: 10px;">
                <span class="upload-button glyphicon glyphicon-download-alt"></span>
            </button>
            <?=$field;?>
            <?php if ($thumbnail) : ?>
                <button class="fileinput-new fileinput-clear btn btn-danger btn-sm" style="width:33px; height: 27px;">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
                <button class="fileinput-cancel btn btn-primary btn-sm" style="width:33px; height: 27px;">
                    <span class="glyphicon glyphicon-refresh"></span>
                </button>
            <?php endif ?>
            <button class="fileinput-exists btn btn-primary btn-sm" style="width:33px; height: 27px;" data-dismiss="fileinput">
                <span class="glyphicon glyphicon-refresh"></span>
            </button>
        </div>
    </div>
    <div class="fileinput-new thumbnail" style="width: 200px; height: 200px; margin: 10px 0 0;">
        <div class="thumbnail-exists" style="width: 100%; height: 100%;">
            <?=$thumbnail;?>
        </div>
    </div>
    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px; margin: 10px 0 0;"></div>
</div>

<?php if ($thumbnail) : ?>
    <script type="text/javascript">
        $('.fileinput-clear').on('click', function (e) {
            e.preventDefault();
            $('#product-img_del').prop('checked', true);
            $(this).closest('.fileinput').addClass('fileinput-empty');
        });
        $('.fileinput-cancel').on('click', function (e) {
            e.preventDefault();
            $('#product-img_del').prop('checked', false);
            $(this).closest('.fileinput').removeClass('fileinput-empty');
        });
    </script>
<?php endif ?>
