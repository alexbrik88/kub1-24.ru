<?php

use common\models\product\Product;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$isTurnover = preg_match('|/product/turnover|', Yii::$app->request->referrer);
?>

<div class="portlet-body goods-info">
    <div class="row">
        <div class="col-sm-4">
            <table class="table table-resume" style="width: auto; max-width: 500px; margin-left: -15px;">
                <colgroup>
                    <col width="180" align="right">
                </colgroup>
                <tbody>
                <tr class="even">
                    <td class="bold-text">Группа:</td>
                    <td><?= $model->group ? $model->group->title : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Код:</td>
                    <td><?= Html::encode($model->code ?: Product::DEFAULT_VALUE); ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Артикул:</td>
                    <td><?=  Html::encode($model->article ?: Product::DEFAULT_VALUE); ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Штрих код:</td>
                    <td><?=  Html::encode($model->barcode ?: Product::DEFAULT_VALUE); ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Единица измерения:</td>
                    <td><?= $model->productUnit ? $model->productUnit->name : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Вес (кг):</td>
                    <td><?= $model->weight ? $model->weight : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Объем (м3):</td>
                    <td><?= $model->volume ? $model->volume : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-5" style="padding-top: 10px;">
                    <span style="display: block;font-weight: bold;padding-bottom: 10px;">Изображение</span>

                    <div class="product-photo-preview">
                        <?= ($src = $model->getImageThumb(200, 200)) ?
                            Html::img($src, ['alt' => '']) : null; ?>
                    </div>
                </div>
                <div class="col-sm-7" style="padding-top: 10px;">
                    <span style="display: block;font-weight: bold;padding-bottom: 10px;">Описание</span>
                    <?= Html::encode($model->comment_photo); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="profile-form-tabs" style="margin: 15px 0;">
        <?= Tabs::widget([
            'options' => [
                'class' => 'nav-form-tabs',
                'style' => 'margin: 0 -5px;',
            ],
            'headerOptions' => [
                'style' => 'padding: 0 5px;',
            ],
            'linkOptions' => [
                'style' => 'padding: 10px 15px 11px;',
            ],
            'tabContentOptions' => [
                'class' => 'portlet box darkblue',
            ],
            'itemOptions' => [
                'class' => 'portlet-body',
            ],
            'items' => [
                [
                    'label' => 'Цены',
                    'content' => $this->render('_productPrice', [
                        'model' => $model,
                        'canViewPriceForBuy' => $canViewPriceForBuy,
                    ]),
                    'active' => $isTurnover ? false : true,
                ],
                [
                    'label' => 'Остатки',
                    'content' => $this->render('_productQuantity', [
                        'model' => $model,
                        'canViewPriceForBuy' => $canViewPriceForBuy,
                    ]),
                ],
                [
                    'label' => 'Доп. информация',
                    'content' => $this->render('_productInfo', [
                        'model' => $model,
                    ]),
                ],
                [
                    'label' => 'Комментарии',
                    'content' => $this->render('_productComment', [
                        'model' => $model,
                    ]),
                ],
                [
                    'label' => 'Оборот',
                    'content' => $this->render('_productTurnover', [
                        'model' => $model,
                    ]),
                    'active' => $isTurnover ? true : false,
                ],
            ],
        ]); ?>
    </div>
</div>
