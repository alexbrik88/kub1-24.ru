<?php

use common\models\product\Product;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

?>

<table class="table table-resume" style="width:355px; margin-left: -10px;">
    <tbody>
        <tr class="even">
            <td class="bold-text">Вид упаковки:</td>
            <td><?= Html::encode($model->box_type ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="odd">
            <td class="bold-text">Количество в одном месте:</td>
            <td><?= Html::encode($model->count_in_place ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="even">
            <td class="bold-text">Количество мест, штук:</td>
            <td><?= Html::encode($model->place_count ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="odd">
            <td class="bold-text">Масса брутто:</td>
            <td><?= Html::encode($model->mass_gross ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="even">
            <td class="bold-text">Страна происхождения товара:</td>
            <td><?= $model->countryOrigin ? $model->countryOrigin->name_short : Product::DEFAULT_VALUE; ?></td>
        </tr>
        <tr class="odd">
            <td class="bold-text">Номер таможенной декларации:</td>
            <td><?= Html::encode($model->customs_declaration_number ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="even">
            <td class="bold-text"><?= $model->getAttributeLabel('item_type_code') ?>:</td>
            <td><?= Html::encode($model->item_type_code ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
        <tr class="odd">
            <td class="bold-text">Поставщик:</td>
            <td><?= $model->provider ? $model->provider->getTitle(true) : Product::DEFAULT_VALUE; ?></td>
        </tr>
    </tbody>
</table>