<?php

use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\product\ProductTurnoverSearch;
use frontend\components\StatisticPeriod;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */

$searchModel = new ProductTurnoverSearch([
    'product_id' => $model->id,
    'dateRange' => StatisticPeriod::getSessionPeriod(),
]);
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();
?>

<?php Pjax::begin([
    'id' => 'productTurnover-pjax',
    'timeout' => 10000,
]); ?>

<div class="row" style="margin-bottom: 10px;">
    <div class="col-sm-8 col-md-9">
        <div class="row statistic-block" style="margin-top: 0;">
            <div class="col-sm-3">
                <div class="statistic-cell" style="background-color: #dfba49; height: 70px;">
                    <i class="fa fa-comments"></i>
                    <div class="stat-value" style="padding: 5px 20px 0;">
                        <?= $model->balanceAtDate(true, [$model->id]) ?>
                    </div>
                    <div class="stat-label">
                        Остаток на начало
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="statistic-cell" style="background-color: #45b6af; height: 70px;">
                    <i class="fa fa-comments"></i>
                    <div class="stat-value" style="padding: 5px 20px 0;">
                        <?= $model->turnAtPeriod(true, [$model->id]) ?>
                    </div>
                    <div class="stat-label">
                        Закуплено всего
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="statistic-cell" style="background-color: #f3565d; height: 70px;">
                    <i class="fa fa-comments"></i>
                    <div class="stat-value" style="padding: 5px 20px 0;">
                        <?= $model->turnAtPeriod(false, [$model->id]) ?>
                    </div>
                    <div class="stat-label">
                        Продано всего
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="statistic-cell" style="background-color: #dfba49; height: 70px;">
                    <i class="fa fa-comments"></i>
                    <div class="stat-value" style="padding: 5px 20px 0;">
                        <?= $model->balanceAtDate(false, [$model->id]) ?>
                    </div>
                    <div class="stat-label">
                        Остаток на конец
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-md-3">
        <?= frontend\widgets\RangeButtonWidget::widget([
            'cssClass' => 'doc-gray-button btn_select_days btn_row',
            'pjaxSelector' => '#productTurnover-pjax',
        ]); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    //'showFooter' => true,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap',
        'role' => 'grid',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],

    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

    'columns' => [
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'footer' => 'Итого',
            'headerOptions' => [
                'width' => '10%',
            ],
        ],
        [
            'attribute' => 'coming',
            'label' => 'Закуплено',
            'footer' => $searchModel->searchQuery()->andWhere(['type' => 1])->sum('quantity') * 1,
            'headerOptions' => [
                'width' => '15%',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'footerOptions' => [
                'style' => 'text-align: center;',
            ],
        ],
        [
            'attribute' => 'selling',
            'label' => 'Продано',
            'footer' => $searchModel->searchQuery()->andWhere(['type' => 2])->sum('quantity') * 1,
            'headerOptions' => [
                'width' => '15%',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'footerOptions' => [
                'style' => 'text-align: center;',
            ],
        ],
        [
            'class' => DropDownSearchDataColumn::className(),
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'filter' => ['' => 'Все'] + $searchModel->searchQuery()->select([
                'contractor',
                'contractor_id',
            ])->distinct()->indexBy('contractor_id')->column(),
            'footer' => ' ',
            'headerOptions' => [
                'width' => '35%',
            ],
            'format' => 'html',
            'value' => function ($data) {
                return Html::a($data['contractor'], [
                    '/contractor/view',
                    'type' => $data['type'],
                    'id' => $data['contractor_id'],
                ]);
            }
        ],
        [
            'attribute' => 'basis',
            'label' => 'Документ',
            'footer' => ' ',
            'headerOptions' => [
                'width' => '25%',
            ],
            'format' => 'html',
            'value' => function ($data) {
                $label = ProductTurnoverSearch::$byDocument[$data['by_document']];
                $label .= " {$data['basis']}";
                return Html::a($label, [
                    "/documents/{$data['by_document']}/view",
                    'type' => $data['type'],
                    'id' => $data['basis_id'],
                ]);
            }
        ],
    ],
]); ?>

<?php Pjax::end(); ?>
