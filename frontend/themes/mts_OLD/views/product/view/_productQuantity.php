<?php

use yii\helpers\Html;
use common\models\product\ProductStore;
use yii\widgets\Pjax;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();
$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;
?>

<?php Pjax::begin([
    'id' => 'productStore-pjax-container',
    'enablePushState' => false,
]); ?>
<div style="display: inline-block; margin-right: 10px;">
    <table class="table table-bordered table-hover overal-result-table" style="width: auto; margin: 0;">
        <thead>
        <tr class="heading">
            <th rowspan="2">Склад</th>
            <th colspan="2" style="text-align: center; padding-bottom: 0 !important;">Начальное</th>
            <th rowspan="2">Остаток</th>
            <th rowspan="2">Неснижаемый остаток</th>
            <th rowspan="2">Цена покупки</th>
            <th rowspan="2">Стоимость покупки</th>
            <th rowspan="2">Цена продажи</th>
            <th rowspan="2">Стоимость продажи</th>
        </tr>
        <tr class="heading">
            <th style="text-align: center; border-right-width: 0; font-weight: normal;">Количество</th>
            <th style="text-align: center; font-weight: normal;">Дата</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($storeArray as $store) : ?>
            <?php /* @var $productStore ProductStore */
            $productStore = $model->getProductStoreByStore($store->id);
            $productInitQuantity = $model->getInitQuantity($productStore->store_id);
            $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
            $totalStoreQuantity += $productStore->quantity * 1;
            $totalIrreducibleQuantity += $productIrreducibleQuantity;
            $totalAmountForBuy += (int)$model->price_for_buy_with_nds * $productStore->quantity;
            $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
            <tr>
                <td style="vertical-align: middle;"><?= Html::encode($store->name); ?></td>
                <td style="text-align: right; vertical-align: middle; border-right-width: 0;">
                    <?= $productInitQuantity; ?>
                </td>
                <td style="text-align: center; vertical-align: middle;"><?= date('d.m.Y', $productStore->created_at); ?></td>
                <td style="text-align: right; vertical-align: middle;"><?= $productStore->quantity; ?></td>
                <td style="text-align: right; vertical-align: middle;">
                    <?= $productIrreducibleQuantity; ?>
                </td>
                <td style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
                <td style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds * $productStore->quantity, 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
                <td style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?>
                </td>
                <td style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds * $productStore->quantity, 2); ?>

                </td>
            </tr>
        <?php endforeach ?>
        <tr style="font-weight: bold;">
            <td colspan="3" style="vertical-align: middle;">Итого</td>
            <td style="text-align: right; vertical-align: middle;">
                <?= $totalStoreQuantity; ?>
            </td>
            <td style="text-align: right; vertical-align: middle;">
                <?= $totalIrreducibleQuantity; ?>
            </td>
            <td style="text-align: right; vertical-align: middle;"></td>
            <td style="text-align: right; vertical-align: middle;">
                <?= $canViewPriceForBuy ?
                    TextHelper::invoiceMoneyFormat($totalAmountForBuy, 2) :
                    Product::DEFAULT_VALUE; ?>
            </td>
            <td style="text-align: right; vertical-align: middle;"></td>
            <td style="text-align: right; vertical-align: middle;">
                <?= TextHelper::invoiceMoneyFormat($totalAmountForSell, 2); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php Pjax::end(); ?>
