<?php

use common\models\product\Product;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

?>

<table class="table table-resume" style="width:355px; margin-left: -10px;">
    <tbody>
        <tr class="odd">
            <td class="bold-text">Комментарий:</td>
            <td><?= \common\components\helpers\Html::encode($model->comment ? : Product::DEFAULT_VALUE); ?></td>
        </tr>
    </tbody>
</table>