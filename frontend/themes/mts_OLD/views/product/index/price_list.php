<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.10.2018
 * Time: 14:53
 */

use common\models\product\PriceList;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\product\Product;

/* @var $this yii\web\View
 * @var $priceList PriceList
 * @var $productionType integer
 * @var $company Company
 * @var $store
 * @var $user \common\models\employee\Employee
 */

$priceLists = $company->getPriceLists($productionType)
    ->andWhere(['is_deleted' => false])
    ->orderBy(['created_at' => SORT_DESC])
    ->all();
$priceListCount = count($priceLists);

$productsCount = Product::find()->byUser()->where(['and',
    ['company_id' => $company->id],
    ['production_type' => Product::PRODUCTION_TYPE_GOODS],
])->count();
$servicesCount = Product::find()->byUser()->where(['and',
    ['company_id' => $company->id],
    ['production_type' => Product::PRODUCTION_TYPE_SERVICE],
])->count();

$canCreate = in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR]);
?>
    <div class="price-list-panel">
        <?php if ($canCreate): ?>
            <div class="create-price-list" style="display: <?= $priceListCount ? 'none' : 'block' ?>;">
                <?= $this->render('create_price_list', [
                    'company' => $company,
                    'priceList' => $priceList,
                    'productionType' => $productionType,
                    'store' => $store,
                    'productsCount' => $productsCount,
                    'servicesCount' => $servicesCount
                ]); ?>
            </div>
        <?php endif; ?>
        <div class="view-price-list" style="display: <?= $priceListCount || !$canCreate ? 'block' : 'none' ?>;">
            <?= $this->render('view_price_list', [
                'company' => $company,
                'productionType' => $productionType,
                'priceLists' => $priceLists,
                'canCreate' => $canCreate,
            ]); ?>
        </div>
        <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
    </div>
    <div id="visible-right-menu-price-list-panel" style="display: none;">
        <div id="visible-right-menu-wrapper" style="z-index: 10050;"></div>
    </div>

    <div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body" id="block-modal-new-product-form">

                </div>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $priceList,
    'useContractor' => null,
]); ?>
<?php if (Yii::$app->session->remove('showPriceList')): ?>
    <?php $this->registerJs('
        $(document).ready(function (e) {
            $(".price-list-trigger").click();
        });
    '); ?>
<?php endif; ?>
<?php $this->registerJs('
$(document).on("click", ".price-list-trigger", function (e) {
    $(".price-list-panel").toggle("fast");
    $("#visible-right-menu-price-list-panel").show();
    $("html").attr("style", "overflow: hidden;");

    return false;
});

var hidePanel = function () {
    $(".price-list-panel").toggle("fast");
    $("#visible-right-menu-price-list-panel").hide();
    $("html").removeAttr("style");
}

$(document).on("click", ".price-list-panel .side-panel-close, #visible-right-menu-price-list-panel", function (e) {
    hidePanel();
});

$(document).on("click", ".send-message-panel .side-panel-close-button", function () {
    $(".price-list-panel").toggle("fast");
    $("#visible-right-menu-price-list-panel").show();
    $("html").attr("style", "overflow: hidden;");
});
'); ?>