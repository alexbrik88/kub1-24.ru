<?php
use common\widgets\Modal;
use yii\helpers\Html;

$header = ' Так будет выглядеть ваш прайс-лист';

Modal::begin([
    'header' => Html::tag('h1', Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-info-sign pull-left',
            'style' => 'font-size:inherit;line-height:inherit;',
        ]) . $header),
    'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
    'footerOptions' => ['style' => 'text-align: center;'],
    'clientOptions' => ['show' => true],
    'footer' => Html::button('Ок', [
        'class' => 'btn yellow',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]),
]);

echo Html::beginTag('div', ['class' => 'text-center']);
echo Html::img('/img/examples/price-list-example.jpg', ['style' => 'max-width: 100%;']);
echo Html::endTag('div');
Modal::end();
