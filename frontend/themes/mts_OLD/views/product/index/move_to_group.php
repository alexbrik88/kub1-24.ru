<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\widgets\ProductGroupDropdownWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $productionType integer
 */

$JS = <<<JS
$(document).on('click', 'button.move_to_group_apply', function() {
    $.ajax({
        type: 'post',
        url: $(this).data('url'),
        data: {
            group_id: $('#product_group_select').val(),
            product_id: $("input.product_checker:checkbox:checked").map(function(){
                return $(this).val();
            }).get(),
        },
        success: function() {
            location.reload();
        }
    });
})
JS;

$this->registerJs($JS);
?>

<?php Modal::begin([
    'header' => '<h1>Переместить в группу</h1>',
    'id' => 'move_to_group_modal',
    'options' => [
        'style' => 'width: 470px;',
    ]
]); ?>

<div class="move_to_group">
    <div style="margin-bottom: 10px;">
        Выберите группу, в которую будут перемещены выбранные товары
    </div>

    <?= ProductGroupDropdownWidget::widget([
        'id' => 'product_group_select',
        'name' => 'product_id',
        'productionType' => $productionType,
    ]) ?>

    <div style="margin-top: 30px;">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white move_to_group_apply',
            'style' => 'width: 110px;',
            'data' => [
                'url' => Url::to(['/product/to-group']),
            ],
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'style' => 'width: 110px;',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>
