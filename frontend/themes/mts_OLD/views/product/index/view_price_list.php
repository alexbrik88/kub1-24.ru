<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.10.2018
 * Time: 0:29
 */

use common\components\helpers\Html;
use common\models\Company;
use common\models\product\PriceList;
use common\components\date\DateHelper;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $company Company
 * @var $productionType integer
 * @var $priceLists PriceList[]
 * @var $previousPriceList PriceList
 * @var $canCreate boolean
 */
?>
    <div class="main-block">
    <div class="header">
        <table style="width: 100%; height: ">
            <tr>
                <td style="width: 10%; vertical-align: middle; text-align: center;">
                    <?= Html::tag('div', '<img src="/img/icons/price-list.png" style="width: 30px; height: 30px;">', [
                        'style' => 'display: inline-block;padding: 10px;background-color: #4276a4;border-radius: 30px!important;width: 60px;height: 60px;',
                    ]) ?>
                </td>
                <td style="padding-left: 10px; font-size: 18px;">
                    Прайс-листы
                </td>
            </tr>
        </table>
    </div>
        <div class="create-new">
            <?= Html::button('<i class="fa fa-plus"></i> Прайс-лист', [
                'class' => 'btn datkblue darkblue-invert create-new-price-list',
                'disabled' => !$canCreate,
            ]); ?>
        </div>
        <div class="saved-price-lists">
            <?php if (count($priceLists) == 0 && !$canCreate): ?>
                <span class="sub-header">
                    У вас нет возможности создавать прайс-лист. <br>
                    Обратитесь к руководителю.
                </span>
            <?php else: ?>
                <span class="sub-header">Сохраненные прайс-листы</span>
                <?php foreach ($priceLists as $key => $priceList): ?>
                    <?php $createdAt = date(DateHelper::FORMAT_USER_DATE, $priceList->created_at);
                    $deleteUrl = Url::to(['/price-list/delete', 'id' => $priceList->id, 'productionType' => $productionType]);
                    $sendUrl = Url::to(['/price-list/send', 'id' => $priceList->id, 'productionType' => $productionType]);
                    $viewLink = Html::a($priceList->name, Url::to(['/price-list/view', 'id' => $priceList->id, 'productionType' => $priceList->production_type]), [
                        'target' => '_blank',
                    ]); ?>
                    <?php if ($key == 0): ?>
                        <div class="price-list-by-date-block">
                        <span class="date">
                        <?= $createdAt; ?>
                    </span>
                        <div class="view-block" id="<?= $priceList->id; ?>">
                            <?= $viewLink; ?>
                            <?php if ($canCreate): ?>
                                <span class="glyphicon glyphicon-trash delete-price-list" title="Удалить"
                                      data-url="<?= $deleteUrl; ?>" data-name="<?= $priceList->name; ?>"
                                      data-target="#delete-confirm-price-list"></span>
                            <?php endif; ?>
                            <span class="glyphicon glyphicon-send send-price-list" title="Отправить"
                                  data-url="<?= $sendUrl; ?>"></span>
                        </div>
                    <?php else: ?>
                        <?php $previousPriceList = $priceLists[$key - 1];
                        $previousCreatedAt = date(DateHelper::FORMAT_USER_DATE, $previousPriceList->created_at); ?>
                        <?php if ($createdAt == $previousCreatedAt): ?>
                            <div class="view-block" id="<?= $priceList->id; ?>">
                                <?= $viewLink; ?>
                                <?php if ($canCreate): ?>
                                    <span class="glyphicon glyphicon-trash delete-price-list" title="Удалить"
                                          data-url="<?= $deleteUrl; ?>" data-name="<?= $priceList->name; ?>"
                                          data-target="#delete-confirm-price-list"></span>
                                <?php endif; ?>
                                <span class="glyphicon glyphicon-send send-price-list" title="Отправить"
                                      data-url="<?= $sendUrl; ?>"></span>
                            </div>
                        <?php else: ?>
                            </div>
                            <div class="price-list-by-date-block">
                            <span class="date">
                            <?= $createdAt; ?>
                        </span>
                            <div class="view-block" id="<?= $priceList->id; ?>">
                                <?= $viewLink; ?>
                                <?php if ($canCreate): ?>
                                    <span class="glyphicon glyphicon-trash delete-price-list" title="Удалить"
                                          data-url="<?= $deleteUrl; ?>" data-name="<?= $priceList->name; ?>"
                                          data-target="#delete-confirm-price-list"></span>
                                <?php endif; ?>
                                <span class="glyphicon glyphicon-send send-price-list" title="Отправить"
                                      data-url="<?= $sendUrl; ?>"></span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($key == count($priceLists) - 1): ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<?= $this->render('delete_price_list_modal'); ?>
<?php $this->registerJs('
    $(document).on("click", ".view-price-list .create-new-price-list", function (e) {
        var $createPriceListBlock = $(this).closest(".view-price-list").siblings(".create-price-list");
        $(this).closest(".view-price-list").fadeOut();
        $createPriceListBlock.find(".side-panel-close-button").addClass("to-view");
        $createPriceListBlock.fadeIn();
    });
    
    $(document).on("click", ".view-price-list .delete-price-list", function (e) {
        var $modal = $($(this).data("target"));
        $modal.find(".form-body .price-list-name").text("\"" + $(this).data("name") + "\"");
        $modal.find(".delete-price-list-modal").attr("data-url", $(this).data("url"));
        $modal.modal();
    });
    
    $(document).on("click", "#delete-confirm-price-list .delete-price-list-modal", function (e) {
        var $modal = $(this).closest(".modal");
        
        $.post($(this).attr("data-url"), null, function (data) {
            $modal.modal("hide");
            if (data.result == true) {
                var $block = $(".price-list-by-date-block .view-block#" + data.id);
                var $mainBlock = $block.closest(".price-list-by-date-block");
                
                $block.remove();
                if ($mainBlock.find(".view-block").length == 0) {
                    $mainBlock.remove();
                }
            } 
            toastr.remove();
            toastr.success(data.message, null, {
                closeButton: true,
                showDuration: 1000,
                hideDuration: 1000,
                timeOut: 5000,
                extendedTimeOut: 1000,
                positionClass: "toast-top-right",
                escapeHtml: false
            });
        });    
    });
    
    $(document).on("click", ".send-price-list", function (e) {
        e.preventDefault();
        hidePanel();
        var $viewUrl = $(this).siblings("a").attr("href");
        var $form = $(".send-message-panel form#send-document-form");
        var $data = $form.data("yiiActiveForm");
        
        $(".send-message-panel").toggle("fast");
        $form.attr("action", $(this).data("url")); 
        $data.settings.validationUrl = $(this).data("url");
        $(".send-message-panel .view-document-link").attr("href", $viewUrl);   
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $("#invoicesendform-sendto_tag").click()
    });
'); ?>