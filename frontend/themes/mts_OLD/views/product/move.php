<?php

use frontend\models\ProductMoveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductMoveForm */

$quantity = $model->product->getQuantity();
array_walk($quantity, function ($val) {
    $val = max(0, $val);
});
$hint = Html::tag('div', 'Допустимое количество к перемещению: 0', [
    'class' => 'quantity_hint quantity_hint_',
]);
foreach ($quantity as $key => $value) {
    $hint .= Html::tag('div', 'Допустимое количество к перемещению: ' . $value, [
        'class' => 'hidden quantity_hint quantity_hint_' . $key,
    ]);
}

$onSubmit = <<<JS
$.post(this.action, $(this).serialize(), function(data) {
    $('#ajax-modal-content').html(data);
});
return false;
JS;
?>

<?php if (Yii::$app->request->isAjax && $model->saved) : ?>
    <?= Html::script('
        $.pjax.reload("#productStore-pjax-container", {timeout: 5000});
        $(".modal.in").modal("hide");
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>

<div class="product-move-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'options' => [
            'onsubmit' => Yii::$app->request->isAjax ? $onSubmit : null,
        ],
    ])); ?>

        <?= $form->field($model, 'from')
        ->dropDownList(ArrayHelper::map($model->productStores, 'store.id', 'store.name'), [
            'prompt' => '',
            'onchange' => "$('.quantity_hint').addClass('hidden'); $('.quantity_hint_' + this.value).removeClass('hidden');",
        ]) ?>

        <?= $form->field($model, 'to')
        ->dropDownList(ArrayHelper::map($model->stores, 'id', 'name'), [
            'prompt' => '',
        ]) ?>

        <?= $form->field($model, 'count')->hint($hint) ?>

        <div class="row">
            <div class="col-sm-12">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue text-white ladda-button',
                ]) ?>
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue text-white pull-right',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
