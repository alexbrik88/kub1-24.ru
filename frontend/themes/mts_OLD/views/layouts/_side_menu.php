<?php

use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;
use kartik\checkbox\CheckboxX;

$visibleOutInvoiceFacture = false;
$visibleInInvoiceFacture = false;
if ($user && $company) {
    $visibleByTaxationType = $company->companyTaxationType->osno;
    $visibleOutInvoiceFacture = $visibleByTaxationType ||
        InvoiceFacture::find()
            ->alias('if')
            ->joinWith('invoice invoice')
            ->where([
                'invoice.company_id' => $company->id,
                'invoice.is_deleted' => false,
                'if.type' => Documents::IO_TYPE_OUT
            ])
            ->exists();
    $visibleInInvoiceFacture = $visibleByTaxationType ||
        InvoiceFacture::find()
            ->alias('if')
            ->joinWith('invoice invoice')
            ->where([
                'invoice.company_id' => $company->id,
                'invoice.is_deleted' => false,
                'if.type' => Documents::IO_TYPE_IN
            ])
            ->exists();
    $isIp = $company->company_type_id == CompanyType::TYPE_IP;
}

echo Html::beginTag('aside');
echo Menu::widget([
    'activateParents' => true,
    'items' => [
        [
            'label' => Html::tag('i', '', ['class' => 'i_0']) .
                Html::encode(ArrayHelper::getValue($company, 'shortTitle')),
            'encode' => false,
            'url' => ['/site/index'],
            'visible' => $company !== null,
        ],
        [
            'label' => '<i class="flaticon-new"></i>Продажи<b class="arrow"></b>',
            'encode' => false,
            'url' => 'javascript:void(0)',
            'visible' => $active == 'documents' && Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_OUT,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'encode' => false,
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'invoice' && $paramType != Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'encode' => false,
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'act' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Товарные накладные',
                    'encode' => false,
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'packing-list' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Товарно-транспортные накладные',
                    'encode' => false,
                    'url' => ['/documents/waybill/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'waybill' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Счета-фактуры',
                    'encode' => false,
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'invoice-facture' && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => $visibleOutInvoiceFacture,
                ],
                [
                    'label' => 'УПД',
                    'encode' => false,
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'upd' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Заказы',
                    'encode' => false,
                    'url' => ['/documents/order-document/index'],
                    'active' => $controller === 'order-document',
                ],
                [
                    'label' => 'Договоры',
                    'encode' => false,
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]),
                ],
            ],
        ],
        [
            'label' => '<i class="flaticon-bottom"></i>Покупки<b class="arrow"></b>',
            'encode' => false,
            'url' => 'javascript:void(0)',
            'visible' => $active == 'documents' && Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_IN,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'encode' => false,
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'invoice' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'encode' => false,
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'act' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Товарные накладные',
                    'encode' => false,
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'packing-list' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Счета-фактуры',
                    'encode' => false,
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'invoice-facture' && $paramType == Documents::IO_TYPE_IN,
                    'visible' => $visibleInInvoiceFacture,
                ],
                [
                    'label' => 'УПД',
                    'encode' => false,
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'upd' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Платёжные поручения',
                    'encode' => false,
                    'url' => ['/documents/payment-order/index'],
                    'active' => $controller === 'payment-order',
                    'visible' => Yii::$app->user->can(permissions\document\PaymentOrder::INDEX),
                ],
                [
                    'label' => 'Распознавание сканов',
                    'encode' => false,
                    'url' => ['/documents/upload-documents/index'],
                    'active' => $controller === 'upload-documents',
                    'visible' => false,
                ],
                [
                    'label' => 'Договоры',
                    'encode' => false,
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_IN,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_SELLER,
                    ])
                ],
                [
                    'label' => 'Доверенности',
                    'encode' => false,
                    'url' => ['/documents/proxy/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'proxy' && $paramType == Documents::IO_TYPE_IN,
                ],
            ],
        ],
        [
            'label' => '<i class="i_3"></i><span class="title">Выгрузка в 1С</span>',
            'encode' => false,
            'url' => ['/export/one-s/index'],
            'active' => $module == 'export' && $controller == 'one-s',
            'visible' => $active == 'documents',
        ],
        [
            'label' => '<i class="i_4"></i><span class="title">Выгрузка документов</span>',
            'encode' => false,
            'url' => '/export/files/index',
            'active' => $module == 'export' && $controller == 'files',
            'visible' => $active == 'documents',
        ],
        [
            'label' => '<i class="icon-basket"></i><span class="title">Покупатели</span>',
            'encode' => false,
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_CUSTOMER],
            'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_CUSTOMER,
            'visible' => $active == 'contractor' && Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_CUSTOMER,
            ]),
        ],
        [
            'label' => '<i class="flaticon-trolley"></i><span class="title">Поставщики</span>',
            'encode' => false,
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_SELLER],
            'active' => $controller === 'contractor' && $paramType == Contractor::TYPE_SELLER,
            'visible' => $active == 'contractor' && Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_SELLER,
            ]),
        ],
        [
            'label' => '<i class="fa fa-bank m-r-sm"></i>Банк',
            'encode' => false,
            'url' => ['/cash/bank/index'],
            'active' => $module == 'cash' && $controller == 'bank',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => '<i class="fa fa-money m-r-sm"></i>Касса',
            'encode' => false,
            'url' => ['/cash/order/index'],
            'active' => $module == 'cash' && $controller == 'order',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\CashOrder::INDEX),
        ],
        [
            'label' => '<i class="flaticon-wallet31 m-r-sm"></i>E-money',
            'encode' => false,
            'url' => ['/cash/e-money/index'],
            'active' => $module == 'cash' && $controller == 'e-money',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => '<i class="fa fa-refresh m-r-sm"></i>Итого',
            'encode' => false,
            'url' => ['/cash/default/index'],
            'active' => $module == 'cash' && $controller == 'default',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => '<i class="fa fa-pencil"></i><i class="fa fa-wrench fa-pencil-wrench"></i><span class="title">' . Product::$productionTypes[Product::PRODUCTION_TYPE_SERVICE] . '</span>',
            'encode' => false,
            'url' => [
                '/product/index',
                'productionType' => Product::PRODUCTION_TYPE_SERVICE,
                'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
            ],
            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_SERVICE,
            'visible' => $active == 'product' && Yii::$app->user->can(permissions\Product::INDEX),
        ],
        [
            'label' => '<i class="fa fa-cubes"></i>Товары<b class="arrow"></b>',
            'encode' => false,
            'url' => 'javascript:void(0)',
            'active' => $controller == 'product' &&
                Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
            'visible' => $active == 'product' && Yii::$app->user->can(permissions\Product::INDEX),
            'items' => [
                [
                    'label' => '<i class="fa fa-cubes m-r-sm"></i><span class="title">Склад</span>',//'Товар',
                    'encode' => false,
                    'url' => [
                        '/product/index',
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                    ],
                    'active' => $controller == 'product' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        in_array($action, ['index', 'view', 'create', 'update']),
                ],
                [
                    'label' => '<i class="fa fa-refresh m-r-sm"></i><span class="title">Оборот товара</span>',
                    'encode' => false,
                    'url' => [
                        '/product/turnover',
                        'per-page' => 20,
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'turnoverType' => ProductSearch::TURNOVER_BY_COUNT,
                    ],
                    'active' => $controller == 'product' &&
                        $action == 'turnover' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_COUNT,
                ],
                [
                    'label' => '<i class="fa fa-refresh m-r-sm"></i><span class="title">Оборот</span><i class="fa fa-rub"></i>',
                    'encode' => false,
                    'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                        && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_AMOUNT,
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ]),
                ],
                [
                    'label' => '<span class="title">XYZ Анализ</span>',
                    'encode' => false,
                    'url' => ['/product/xyz'],
                    'active' => $controller == 'product' && $action == 'xyz',
                    'visible' => (YII_ENV_DEV || $user->company_id == 486),// <-Удалить условие, когда задача будет принята
                ],
            ],
        ],
        [
            'label' => '<i class="fa fa-line-chart"></i><span class="title">Аналитика</span><b class="arrow"></b>',
            'encode' => false,
            'url' => 'javascript:;',
            'visible' => $module == 'reports' &&
                Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                Yii::$app->user->can(UserRole::ROLE_MANAGER) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER),
            'items' => [
                [
                    'label' => '<span class="title">Дашборд</span>',
                    'encode' => false,
                    'url' => ['/reports/dashboard/index'],
                    'active' => in_array(\Yii::$app->controller->id, ['dashboard']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF))
                ],
                [
                    'label' => '<span class="title">Финансы</span>',
                    'encode' => false,
                    'url' => ['/reports/finance/odds'],
                    'active' => in_array(\Yii::$app->controller->id, ['expenses-report', 'finance']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
                ],
                [
                    'label' => '<span class="title">Сотрудники</span>',
                    'encode' => false,
                    'url' => ['/reports/employees/index'],
                    'active' => \Yii::$app->controller->id == 'employees',
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)) &&
                        (YII_ENV_DEV || $user->company_id == 486),// <-Удалить условие, когда задача будет принята
                ],
                [
                    'label' => '<span class="title">По счетам</span>',
                    'encode' => false,
                    'url' => ['/reports/invoice-report/created'],
                    'active' => \Yii::$app->controller->id == 'invoice-report',
                    'visible' => true,
                ],
                [
                    'label' => '<span class="title">По клиентам</span>',
                    'encode' => false,
                    'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                        ['/reports/analysis/index'] :
                        ['/reports/debt-report/debtor'],
                    'active' => in_array(\Yii::$app->controller->id, ['debt-report', 'analysis', 'discipline']),
                    'visible' => true,
                ],
                [
                    'label' => '<span class="title">По поставщикам</span>',
                    'encode' => false,
                    'url' => ['/reports/debt-report-seller/debtor'],
                    'active' => in_array(\Yii::$app->controller->id, ['debt-report-seller',]),
                    'visible' => true,
                ],
                [
                    'label' => '<span class="title">По продажам</span>',
                    'encode' => false,
                    'url' => ['/reports/selling-report/index'],
                    'active' => \Yii::$app->controller->id == 'selling-report',
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR),
                ],
                [
                    'label' => '<span class="title">По прибыли</span>',
                    'encode' => false,
                    'url' => ['/reports/profit/index'],
                    'active' => \Yii::$app->controller->id == 'profit',
                    'visible' => YII_ENV_DEV || $user->company_id == 486 || $user->company_id == 628,
                ],
            ],
        ],
        [
            'label' => '<i class="fa fa-gear"></i>Настройки<b class="arrow"></b>',
            'encode' => false,
            'url' => 'javascript:;',
            'visible' => in_array($controller, ['company', 'employee']) ||
                in_array($module, ['subscribe']),
            'items' => [
                [
                    'label' => '<span class="title">Профиль компании</span>',
                    'encode' => false,
                    'url' => ['/company/index'],
                    'active' => $controller == 'company',
                    'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
                ],
                [
                    'label' => '<span class="title">Сотрудники</span>',
                    'encode' => false,
                    'url' => ['/employee/index'],
                    'active' => $controller == 'employee',
                    'visible' => Yii::$app->user->can(permissions\Employee::INDEX),
                ],
                [
                    'label' => '<span class="title">Оплатить КУБ</span>',
                    'encode' => false,
                    'url' => ['/mts/subscribe/index'],
                    'active' => $module == 'subscribe' && $controller == 'default',
                    'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
                ],
            ],
        ],
        [
            'label' => '<i class="icon-user"></i> Ваш профиль',
            'encode' => false,
            'url' => ['/profile/index'],
            'active' => $controller == 'profile',
            'visible' => $controller == 'profile',
        ],
    ],
]);
echo Html::endTag('aside');
