<?php
use frontend\components\PageSize;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}
?>

<?php if (!empty($scroll)) : ?>
    <div class="table_holder">{items}</div>
<?php else : ?>
    <div class="table_holder">{items}</div>
<?php endif ?>
<?= $this->render('perPage', [
    'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
    'pageSizeParam' => $pageSizeParam,
]) ?>
{pager}
<div class="clearfix"></div>