<?php
use yii\helpers\Url;
use kartik\select2\Select2;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}

$perPage = \Yii::$app->request->getQueryParam($pageSizeParam);
if ($perPage === null && Yii::$app->controller->id == 'finance' && Yii::$app->controller->action->id == 'item-list') {
    $perPage = 50;
}
if (empty($maxTitle)) {
    $maxTitle = 'Все';
}
?>

<div class="pag pull-right">
    <div class="per-page-text">Отображать по</div>
    <div class="per-page-select">
        <?= Select2::widget([
            'data' =>  [10 => '10', 20 => '20', 40 => '40', 60 => '60', 80 => '80', 0 => 'Все'],
            'name' => 'grid_items_per_page',
            'value' => $perPage,
            'options' => [
                'class' => 'grid_items_per_page',
            ],
            'pluginOptions' => [
                'allowClear' => false,
                'minimumResultsForSearch' => -1,
            ]
        ]); ?>
    </div>
</div>

<script>
    $(document).on('change', '.grid_items_per_page', function() {
        location.href = "<?= Url::current(['page' => null, 'per-page' => null]) ?>" + "&per-page=" + $(this).val();
    });
</script>