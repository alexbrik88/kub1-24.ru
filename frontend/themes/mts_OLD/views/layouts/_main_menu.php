<?php

use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;
use kartik\checkbox\CheckboxX;

$companyItems = [];
$userItems = [];
if ($user) {
    $employeeCompanyArray = $user->getEmployeeCompany()
        ->joinWith('company company')
        ->leftJoin([
            'company_type' => CompanyType::tableName(),
        ], "{{company}}.[[company_type_id]] = {{company_type}}.[[id]]")
        ->andWhere([
            'employee_company.is_working' => true,
            'company.blocked' => false,
        ])->orderBy([
            new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
            "company_type.name_short" => SORT_ASC,
            "company.name_short" => SORT_ASC,
        ])->all();
    $companyArray = ArrayHelper::getColumn($employeeCompanyArray, 'company');
    foreach ($companyArray as $item) {
        $companyItems[] = [
            'label' => '<i class = "comp_1"></i> '.Html::encode($item->shortTitle),
            'encode' => false,
            'url' => [
                '/site/change-company',
                'id' => $item->id,
            ],
            'active' => $item->id == ArrayHelper::getValue($company, 'id'),
        ];
    }
    $companyItems[] = [
        'label' => '<i class = "comp_add"></i> Добавить компанию',
        'url' => '#create-company',
        'encode' => false,
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];

    $userItems = [
        [
            'label' => Html::tag('b', Html::encode($user->shortFio)).Html::tag('br').$user->email,
            'url' => '#',
        ],
        [
            'label' => 'Профиль',
            'url' => ['/profile/index'],
        ],
        [
            'label' => 'Мои документы',
            'url' => '#',
        ],
        [
            'label' => 'Помощь',
            'url' => '#',
        ],
        [
            'label' => 'Выход',
            'url' => ['/mts/saml/logout'],
        ],
    ];
}
$moreItems = [
    [
        'label' => 'Аналитика',
        'url' => ['/reports/dashboard/index'],
        'active' => $module == 'reports',
        'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
            Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
            Yii::$app->user->can(UserRole::ROLE_MANAGER) ||
            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER),
    ],
    [
        'label' => 'Настройки',
        'url' => ['/company/index'],
    ],
    [
        'label' => 'Ваш профиль',
        'url' => ['/profile/index'],
    ],
];
?>

<menu class="main_menu">
    <div class="mts_container">
        <div class="logo">
            <a href="#">
                <i></i><span style="vertical-align: top; display: inline-block; margin-top: 2px;">Документы</span>
            </a>
        </div>
        <div class="main_nav">
            <div class="ser_company_list dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    Мои компании
                    <i class="triangle_bottom"></i>
                </a>
                <?= Dropdown::widget([
                    'items' => $companyItems,
                    'options' => [
                        'style' => 'margin-top: 0; height: auto; max-height: 268px; overflow-x: hidden;',
                    ],
                ]) ?>
            </div>
            <div>
                <?= Html::a('Документы', [
                    '/documents/invoice/index',
                    'type' => Documents::IO_TYPE_OUT,
                ], [
                    'class' => $active == 'documents' ? 'active' : '',
                ]) ?>
            </div>
            <div>
                <?= Html::a('Контрагенты', [
                    '/contractor/index',
                    'type' => Contractor::TYPE_CUSTOMER,
                ], [
                    'class' => $active == 'contractor' ? 'active' : '',
                ]) ?>
            </div>
            <div>
                <?= Html::a('Деньги', [
                    '/cash/bank/index',
                ], [
                    'class' => $active == 'cash' ? 'active' : '',
                ]) ?>
            </div>
            <div>
                <?= Html::a('Товары и Услуги', [
                    '/product/index',
                    'productionType' => Product::PRODUCTION_TYPE_GOODS,
                ], [
                    'class' => $active == 'product' ? 'active' : '',
                ]) ?>
            </div>
            <div class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Еще<i class="triangle_bottom"></i></a>

                <?= Dropdown::widget([
                    'items' => $moreItems,
                    'options' => [
                        'style' => 'margin-top: 0; height: auto; max-height: 200px; overflow-x: hidden; right: 0; left: auto;',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="side_nav">
            <div class="avatar"><a href="#"></a>
            <?php if ($userItems) : ?>
                <ul class="forth_part_menu">
                    <?php foreach ($userItems as $item) : ?>
                        <li>
                            <?= Html::a($item['label'], $item['url']) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
            </div>
            <div class="message" style="position: relative;">
                <a href="#">
                    <i class="glyphicon glyphicon-bell" style="font-size: 16px; line-height: 16px; margin: 4px 0 0;"></i>
                </a>
                <ul class="second_part_menu">
                    <li><a href="#"><i class="second_part_menu_0"></i><span>Выполнен вход в Ваш аккаунт МТС.</br>Если это были не Вы, позвоните …</span></br><b>Только что</b></a></li>
                    <li><a href="#"><i class="second_part_menu_1"></i><span>Мой МТС</br>3 уведомления</span></br><b>Сегодня в 09:15</b></a></li>
                    <li><a href="#"><i class="second_part_menu_2"></i><span>Сегодня запланирован урок по</br>математике в 15:00. Успехов!</span></br><b>Сегодня в 12:15</b></a></li>
                    <li><a href="#"><i class="second_part_menu_3"></i><span>Запланирован прием к терапевту в</br>клинике КДЦ МЕДСИ на Грохольск…</span></br><b>25 июля 2018</b></a></li>
                    <li><a href="#"><i class="second_part_menu_4"></i><span>Вам начислено 10 ₽ кешбека</span></br><b>25 июля 25</b></a></li>
                    <li><a href="#">Все уведомления</a></li>
                </ul>
            </div>
            <div class="dots" style="position: relative;">
                <a href="#">
                    <i class="glyphicon glyphicon-th icon-th" style="font-size: 16px; line-height: 16px; margin: 0;"></i>
                </a>
                <ul class="third_part_menu">
                    <li>Доступные продукты</li>
                    <li>
                        <a href="#" class="third_part_menu_1"><i class="third_part_menu_1_ico"></i><i class="third_part_menu_1_txt">Сайт МТС</i></a>
                        <a href="#" class="third_part_menu_2"><i class="third_part_menu_2_ico"></i><i class="third_part_menu_2_txt">Виртуальный помощник</i></a>
                        <a href="#" class="third_part_menu_3"><i class="third_part_menu_3_ico"></i><i class="third_part_menu_3_txt">Маркетолог</i></a>
                    </li>
                    <li>
                        <a href="#" class="third_part_menu_4"><i class="third_part_menu_4_ico"></i><i class="third_part_menu_4_txt">Cashback</i></a>
                        <a href="#" class="third_part_menu_5"><i class="third_part_menu_5_ico"></i><i class="third_part_menu_5_txt">Касса</i></a>
                        <a href="#" class="third_part_menu_6"><i class="third_part_menu_6_ico"></i><i class="third_part_menu_6_txt"></i></a>
                    </li>
                    <li>Рекомендованные продукты</li>
                    <li>
                        <a href="#" class="third_part_menu_7"><i class="third_part_menu_7_ico"></i><i class="third_part_menu_7_txt">Мобильные сотрудники</i></a>
                        <a href="#" class="third_part_menu_8"><i class="third_part_menu_8_ico"></i><i class="third_part_menu_8_txt">Wi-fi для бизнеса</i></a>
                        <a href="#" class="third_part_menu_9"><i class="third_part_menu_9_ico"></i><i class="third_part_menu_9_txt">М2М менеджер</i></a>
                    </li>
                </ul>
            </div>
            <div class="purse"><a href="#"><i></i>2 486,80₽</a></div>
        </div>
    </div>
</menu>
