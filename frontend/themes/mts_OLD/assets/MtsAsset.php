<?php

namespace frontend\themes\mts\assets;

use yii\web\AssetBundle;

/**
 * MtsAsset
 */
class MtsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/mts/assets/web';

    /**
     * @var array
     */
    public $css = [
        'style.css',
        //'form.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'script.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\assets\AppAsset',
        'common\assets\MaterialIconsAsset',
        //'macgyer\yii2materializecss\assets\MaterializeAsset',
    ];
}
