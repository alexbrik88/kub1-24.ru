<?php

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * MobileAsset
 */
class MobileAsset extends AssetBundle
{
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapThemeAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
    }

    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/mobile/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/air-datepicker.css',
        'css/vidimus.css',
        'css/document-file-scan.css',
        'css/style.css',
        'css/custom.css',
        'css/layout.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'js/air-datepicker.js',
        'js/vendor.min.js',
        'js/layout.js',
        'js/expenditure-item-widget.js',
        'js/export.js',
        'js/main.js',
        'js/custom.js',
        'js/custom2.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\mobile\assets\MobileCommonAsset',
        'frontend\themes\mobile\assets\FrontendAsset'
    ];
}
