(function($) {
    $('#sidebar-main-menu').on('show.bs.collapse', function (e) {
        if (e.target.id == 'sidebar-main-menu') {
            $('#nav-sidebar-menu, #sidebar-main-menu-shadow').toggleClass('hidden', false);
        }
    });
    $('#sidebar-main-menu').on('hidden.bs.collapse', function (e) {
        if (e.target.id == 'sidebar-main-menu') {
            $('#nav-sidebar-menu, #sidebar-main-menu-shadow').toggleClass('hidden', true);
        }
    });
    $(document).on("click", ".sidebar-main-menu-toggle", function (e) {
        console.log(e);
        $('#sidebar-main-menu').collapse('toggle');
        if ($(e.target).hasClass('sidebar-main-menu-toggle')) {
        }
    });
    $(document).ready(function () {
        $('#main-menu-items .submenu-items.collapse:has(.active)').collapse('show');
    });
} (jQuery));