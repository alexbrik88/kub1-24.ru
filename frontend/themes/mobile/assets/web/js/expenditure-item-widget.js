var expenditureDropdownItemsTemplate = function(data, container) {
    if ($(data.element).attr('data-key') == '1') {
        container.setAttribute('style', 'border-top: 1px solid gray;');
    }
    container.setAttribute('data-id', data.id);
    if ($(data.element).attr('data-editable') == '1') {
        var content = '<div class="expenditure-item-name-label">'
                    + '<i class="link pull-right edit-exp-item" data-id="'+data.id+'" title="Изменить"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use></svg></i>'
                    + '<i class="link pull-right del-exp-item mr-1"  data-id="'+data.id+'" title="Удалить"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use></svg></i>'
                    + '<div class="item-name">'+data.text+'</div>'
                    + '</div>';
        container.innerHTML = content;
    } else {
        container.innerHTML = data.text;
    }
    return container;
}
var dleteExpenditureItem = function(input, target) {
    editExpenditureItemCancel();
    var itemId = $(target).data("id");
    var modalSelector = '#' + $(input).attr('id') + '-del-modal';
    console.log($(target).parent().children('.item-name').text(), modalSelector);
    $(modalSelector).on('show.bs.modal', function() {
        $(input).select2("close");
    });
    $(modalSelector).modal('show');
    $(modalSelector + ' .item-name').text($(target).parent().children('.item-name').text());
    // unbind events
    $(modalSelector + ' .js-item-delete').off();
    $(modalSelector + ' .js-item-delete-cancel').off();

    $(modalSelector + ' .js-item-delete').on('click', function() {
        $.post($(input).data('delurl'), {"id":itemId}, function(data) {
            if (data.success) {
                $(input).find('option[value="'+itemId+'"]').remove();
                $(target).closest(".select2-results__option").remove();
                if ($(input).val() == itemId) {
                    $(input).val(null).trigger("change");
                }
            } else {
                alert(data.message);
            }
            $(modalSelector).modal('hide');
        });
    });
    $(modalSelector + ' .js-item-delete-cancel').on('click', function() {
        $(modalSelector).modal('hide');
    });
}
var editExpenditureItem = function(input, target) {
    editExpenditureItemCancel();
    var itemId = $(target).data('id');
    var $container = $(target).closest('.select2-results__option');
    var text = $container.find('div.item-name').html();
    var form = '<form class="expenditure-item-name-form" data-id="'+itemId+'" data-input="'+input.id+'">'
             + '<i class="link pull-right edit-exp-item-apply"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/images/svg-sprite/svgSprite.svg#check"></use></svg></i>'
             + '<i class="link pull-right edit-exp-item-cancel mr-1"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use></svg></i>'
             + '<div class="input-wrapper">'
             + '<input type="hidden" name="id" value="'+itemId+'"/>'
             + '<input type="text" name="name" value="'+text+'" class="item-name-input"/>'
             + '</div>'
             + '</form>';
    $container.addClass('editable-expenditure-item');
    $container.children('.expenditure-item-name-label').hide();
    $container.append(form);
    $container.find('form').on('submit', function(e) {
        e.preventDefault();
        return expenditureItemFormSubmit(input, $(this));
    });
}
var editExpenditureItemCancel = function() {
    $('form.expenditure-item-name-form').remove();
    $('div.expenditure-item-name-label').show();
    $('.editable-expenditure-item').removeClass('editable-expenditure-item');
}
var expenditureItemFormSubmit = function(input, $form) {
    var itemId = $form.data('id')
    var $container = $form.parent();
    var $input = $(input);
    $.post($input.data('editurl'), $form.serialize(), function(data) {
        if (data.success) {
            var $option = $input.find('option[value="'+itemId+'"]');
            $container.find('.item-name').text(data.name);
            $option.text(data.name);
            editExpenditureItemCancel();
            if ($input.data('select2')) {
                $input.select2('destroy');
            }
            $input.select2(eval($input.attr('data-krajee-select2')));
            $.when($input.select2(eval($input.attr('data-krajee-select2')))).done(initS2Loading($input.attr('id'), $input.attr('data-s2-options')));
            $input.val(itemId).trigger('change');
        } else if (data.message) {
            alert(data.message);
        } else {
            alert('Ошибка сохранения изменений.');
        }
    });
    return false;
}
var editExpenditureItemApply = function(input, target) {
    return expenditureItemFormSubmit(input, $(target).parent());
}
