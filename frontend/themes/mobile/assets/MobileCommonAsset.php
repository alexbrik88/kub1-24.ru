<?php

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

// TODO: move all possible libraries to bower.

/**
 * Class MobileCommonAsset
 * @package frontend\themes\mobile\assets
 */
class MobileCommonAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/tooltipster-kub.css',
        'css/tooltipster-kub.css',
        'css/font-awesome/css/font-awesome.min.css',
        //'plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
        // bootstrap
        //'css/bootstrap-theme.min.css',
        //'css/bootstrap-modal.css',
        //'css/bootstrap-modal-bs3patch.css',
        //'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        // other
        //'css/components.css',
        //'css/components-rounded.css',
        //'css/plugins.css',
        //'plugins/datatables/media/css/jquery.dataTables.min.css',
        //'plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',
        //'plugins/datatables-fixedheader/css/fixedHeader.bootstrap.css',
        //'plugins/datatables-fixedheader/css/fixedHeader.dataTables.css',
        //'plugins/nestable/css/jquery.nestable.css',
        // icons
        //'css/flaticon/css/flaticon.css',
        //'css/simple-line-icons/simple-line-icons.min.css',
        //'css/styler.css',
        // custom
        //'css/layout.css',
        //'css/bootstrap-themes/blue.css',
        //'css/upload.css',
        //'css/tooltipster-kub.css',
        //'css/custom.css',
    ];
    /**
     * @var array
     */
    public $js = [
        // bootstrap
        //'plugins/bootstrap.min.js',
        //'plugins/bootstrap-hover-dropdown.min.js',
        //'plugins/bootstrap-daterangepicker/bootstrap-datepicker.js',
        //'plugins/bootstrap-daterangepicker/daterangepicker.js',
        //'plugins/bootstrap-modal.js',
        //'plugins/bootstrap-modalmanager.js',
        //'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        //'plugins/bootstrap-confirmation.js',
        //
        'plugins/datatable.js',
        'plugins/metronic.js',
        'plugins/datatables/media/js/jquery.dataTables.min.js',
        'plugins/datatables/media/js/jquery.dataTables.js',
        'plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',
        'plugins/datatables-fixedheader/js/dataTables.fixedHeader.js',
        'plugins/quick-sidebar.js',
        // flot
        'plugins/flot/jquery.flot.min.js',
        'plugins/flot/jquery.flot.resize.min.js',
        'plugins/flot/jquery.flot.pie.min.js',
        'plugins/flot/jquery.flot.stack.min.js',
        'plugins/flot/jquery.flot.crosshair.min.js',
        'plugins/flot/jquery.flot.categories.min.js',
        //
        'plugins/charts-flotcharts.js',
        'plugins/jquery.knob.js',
        'plugins/jquery.formstyler.js',
        'plugins/sumPropis.js',
        //metisMenu
        'plugins/jquery.metisMenu.js',
        //slimScroll
        //'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        //nestable
        //'plugins/nestable/js/jquery.nestable.js',
        //Url Modification and Creation for jQuery
        'plugins/domurl-master/url.min.js',
        // custom
        'scripts/layout.js',
        'scripts/app.form.js',
        'scripts/app.helper.js',
        //'scripts/common.js',
        'scripts/validation/company.js',
        'scripts/validation/product.js',
        //'scripts/demo.js', // TODO: remove if isn't needed.
        'scripts/upload.js',
        'scripts/company/registration.js',
        'scripts/jquery-numerator.js',
        'scripts/zoomsl-3.0.min.js',
        'scripts/jquery.topzindex.min.js',
        'scripts/alert.js',
        'scripts/jquery.pulsate.min.js',
        'scripts/sweetalert.min.js',
        'scripts/taxrobot.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'common\assets\HistoryApiAsset',
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\widgets\PjaxAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'common\assets\IEAsset',
        'common\assets\MomentAsset',
        'common\assets\MomentTimezoneAsset',
        'common\assets\SimpleAjaxUploaderAsset',
        'common\assets\SuggestionsJqueryAsset',
        'common\assets\SuggestionsCssAsset',
        'common\assets\SlickAsset',
        'common\assets\MaskedInputAsset',
        'common\assets\TooltipsterAsset',
        'common\assets\FontsAsset',
        'common\assets\Select2Asset',
        'common\assets\MaterialIconsAsset',
        'common\assets\Fontawesome4Asset',
        'kartik\select2\ThemeKrajeeAsset',
        'philippfrenzel\yii2tooltipster\yii2tooltipsterAsset',
        'frontend\themes\mobile\assets\UniformAsset',
        'common\assets\JquerySerializeObjectAsset',
        'common\assets\LaddaButtonAsset',
    ];
}
