<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;

/* @var \yii\web\View $this */

/* @var \common\models\file\widgets\FileList $widget */
$widget = $this->context;
?>

<div class="file-list">
    <?php foreach ($widget->files as $file): ?>
        <?php $fileLinkClass = null;
        $tooltipId = null;
        $contentPreview = null;
        if (in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
            $fileLinkClass = ' file-link-preview';
            $tooltipId = 'file-link-preview-' . $file->id;
            $thumb = $file->getImageThumb(400, 600);
            if ($thumb) {
                $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                $contentPreview .= Html::img($thumb, ['alt' => '']);
                $contentPreview .= Html::endTag('span');
                $contentPreview .= Html::endTag('div');
            }
        }; ?>
        <div id="file-<?= $file->id; ?>" class="file <?= $file->isNewRecord ? 'file-template' : ''; ?>" data-id="<?= $file->id; ?>">
            <?php if ($widget->viewAccessCallback === null || call_user_func($widget->viewAccessCallback, $file)) : ?>
                <span class="icon-clip <?= $fileLinkClass ?>" data-tooltip-content="#<?= $tooltipId ?>">
                    <?= Icon::get('clip') ?>
                </span>
                <?= $contentPreview ?>
                <?= Html::a($file->filename_full, call_user_func($widget->fileLinkCallback, $file, $widget->ownerModel), [
                    'class' => 'file-link',
                    'target' => '_blank',
                    'download' => '',
                ]) ?>
            <?php else : ?>
                <?= $filename ?>
            <?php endif ?>

            <?php if ($widget->deleteAccessCallback === null || call_user_func($widget->deleteAccessCallback, $file)) : ?>
                <?php Modal::begin([
                    'id' => 'delete-file-modal-' . $file->id,
                    'title' => 'Вы уверены, что хотите удалить файл?',
                    'titleOptions' => [
                        'class' => 'ta-c',
                    ],
                    'closeButton' => false,
                    'toggleButton' => [
                        'label' => Icon::get('circle-close'),
                        'tag' => 'span',
                        'class' => 'icon-close',
                    ],
                    'options' => [
                        'class' => 'confirm-modal fade',
                    ],
                ]); ?>
                    <div class="text-center">
                        <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 delete-file',
                            'data-file' => $file->id,
                            'data-dismiss-single' => 'modal',
                        ]); ?>
                        <?= Html::button('НЕТ', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                            'data-dismiss-single' => 'modal',
                        ]); ?>
                    </div>
                <?php Modal::end(); ?>
            <?php endif ?>
        </div>
    <?php endforeach; ?>
</div>
