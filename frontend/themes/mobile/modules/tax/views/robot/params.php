<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use \php_rutils\RUtils;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $taxation CompanyTaxationType */
/* @var $accounts CheckingAccountant[] */

$this->title = 'Параметры вашего ИП';
$this->params['step'] = 2;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

if (empty($accounts)) {
    $accounts_title = 'Нет расчетных счетов в банках';
} else {
    $accounts_title = [];
    foreach ($accounts as $value) {
        $accounts_title[] =
            $value['cnt'] . ' ' .
            RUtils::numeral()->choosePlural($value['cnt'], ['расчетный счет', 'расчетных счета', 'расчетных счетов']) .
            ' в ' . $value['bank_name'];
    }
    $accounts_title = implode(', ', $accounts_title);
}

$supportEmail = Yii::$app->params['emailList']['support'];

$questions = [
    'no_accounts' => $accounts_title,
    'no_workers' => 'Нет сотрудников',
    'no_foreign_currency' =>  'Нет валютных счетов',
    'no_acquiring' => 'Нет эквайринга',
    'no_cashbox' => 'Нет кассы'
];
$ofds = [
    'Ofd[taxcom]'  => 'Такском',
    'Ofd[2]'  => 'Платформа ОФД',
    'Ofd[3]'  => 'Первый ОФД',
    'Ofd[4]'  => 'Ярус',
    'Ofd[5]'  => 'Петер-Сервис Спецтехнологии (OFD.RU)',
    'Ofd[6]'  => 'Тензор',
    'Ofd[7]'  => 'СКБ Контур',
    'Ofd[8]'  => 'Тандер',
    'Ofd[9]'  => 'Калуга Астрал',
    'Ofd[10]' => 'Яндекс.ОФД',
    'Ofd[11]' => 'ЭнвижнГруп',
    'Ofd[12]' => 'Вымпел-Коммуникации',
    'Ofd[13]' => 'Мультикарта',
];
?>

<?php
$questions_messages['no_accounts'] = '
    <div id="no_accounts_message" style="display:none">
        <p>Если данные по расчетным счетам не соответствуют действительности, то вернитесь на предыдущий шаг и либо добавьте отсутствующий счет, либо удалите счет, которого нет.</p>
        <p>Если у вас есть счет, но по нему нет оборотов, то укажите его.</p>
        <p>Если у вас был счет, но вы его закрыли, то не удаляйте его.</p>
    </div>';
$questions_messages['no_workers'] = '
    <div id="no_workers_message" style="display:none">
        <p>Если у вас есть сотрудники, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность расчета налогов при наличие сотрудников.</p>
    </div>';
$questions_messages['no_foreign_currency'] = '
    <div id="no_foreign_currency_message" style="display:none">
        <p>Если у вас есть валютные счета и по ним были операции, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций по валютным счетам.</p>
    </div>';
$questions_messages['no_acquiring'] = '
    <div id="no_acquiring_message" style="display:none">
        <p>Если у вас есть эквайринг, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций с учётом эквайринга.</p>
    </div>';
$questions_messages['no_cashbox'] = '
    <div id="no_cashbox_message" style="display:none">
        <p>Если у вас есть касса, то пока мы не можем загрузить данные из ОФД (Оператора фискальных данных).
            Вы сможете добавить общую сумму дохода по кассе вручную.
            Мы скоро добавим возможность загружать данные из ОФД.</p>
        <p>Укажите ваш ОФД и мы вам сообщим о возможности загрузки данных из вашего ОФД.</p>
        <div class="row">
        ';
foreach ($ofds as $key => $value) {
    $isChecked = ($key == 'Ofd[taxcom]' && Yii::$app->request->cookies['taxcom_user']);

    $questions_messages['no_cashbox'] .= '<div class="col-4">';
    $questions_messages['no_cashbox'] .= Html::label(Html::checkbox(htmlspecialchars($key), $isChecked, [
        'class' => 'form-group',
    ]) . $value, null, [
        'class' => 'text_size_14'
    ]);
    $questions_messages['no_cashbox'] .= '</div>';
}
        $questions_messages['no_cashbox'] .= '
        </div>
    </div>';

$taxNote = Html::tag('span', '', [
    'style' => 'position: absolute; left: 13px; top: 0; width: 20px; height: 20px;',
    'title' => 'Бухгалтерия ИП работает только для УСН доходы',
]);

$percent = $taxation->usn_percent;
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-ip-params',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['mobileFieldConfig'],
]); ?>

<div class="tab-content" id="tax-robot-company-form-tab-content">
    <div class="tab-pane fade show active" id="tab-item-tax" role="tabpanel">
        <div class="tab-pane-content">
            <?= Html::activeHiddenInput($model, 'ip_patent_city', ['value' => '']) ?>
            <?= Html::activeHiddenInput($model, 'patentDate', ['value' => '']) ?>
            <?= Html::activeHiddenInput($model, 'patentDateEnd', ['value' => '']) ?>
            <h5>Система налогообложения</h5>
            <?= Html::activeHiddenInput($taxation, 'osno', ['value' => 0]) ?>
            <?= Html::activeHiddenInput($taxation, 'usn', ['value' => 1]) ?>
            <?= Html::activeHiddenInput($taxation, 'envd', ['value' => 0]) ?>
            <?= Html::activeHiddenInput($taxation, 'psn', ['value' => 0]) ?>
            <?= Html::activeHiddenInput($taxation, 'usn_type', ['value' => CompanyTaxationType::INCOME]) ?>
            <?= Html::activeHiddenInput($taxation, 'usn_percent', [
                'value' => CompanyTaxationType::$usnDefaultPercent[CompanyTaxationType::INCOME],
            ]) ?>

            <?= Html::a('УСН "Доходы"', '#tab-item-percent', [
                'class' => 'button-regular button-hover-content-red w-100 mt-2',
                'data-toggle' => 'tab',
            ]) ?>
            <?= Html::a('УСН "Доходы минус расходы"', '#modal-not-available', [
                'class' => 'button-regular button-hover-content-red w-100 mt-3',
                'data-toggle' => 'modal',
            ]) ?>
            <?= Html::a('ОСНО', '#modal-not-available-osno', [
                'class' => 'button-regular button-hover-content-red w-100 mt-3',
                'data-toggle' => 'modal',
            ]) ?>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::a(Icon::get('arrow-left', [
                'class' => 'fix-left',
            ]).'Назад', ['company', '#' => 'c-account'], [
                'class' => 'button-regular button-grey w-100 mt-2',
            ]) ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-item-percent" role="tabpanel">
        <div class="tab-pane-content">
            <h5>Система налогообложения</h5>
            <?= Html::button('УСН "Доходы"', [
                'class' => 'button-regular button-grey w-100 mt-2',
            ]) ?>

            <div style="width: 20rem; margin: 0 auto;">
                <h6 class="mt-5 text-center">Налоговая ставка</h6>
                <div class="row usn_percent-radio-list">
                    <?php foreach (range(1, 6) as $val) : ?>
                        <div class="col-4">
                            <?= Html::activeRadio($taxation, 'usn_percent', [
                                'id' => 'companytaxationtype-usn_percent-'.$val,
                                'uncheck' => false,
                                'value' => $val,
                                'class' => 'usn_percent_radio',
                                'label' => Html::tag('span', $val.'%', [
                                    'class' => 'button-regular button-hover-content-red btn-tax-percent mt-3'.($percent == $val ? ' active' : ''),
                                ]),
                                'labelOptions' => [
                                    'data-value' => $val,
                                ],
                            ]) ?>
                        </div>
                    <?php endforeach ?>
                    <?= Html::a('', '#tab-item-summary', [
                        'class' => 'hidden to-tab-item-tax',
                        'data-toggle' => 'tab',
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::a(Icon::get('arrow-left', [
                'class' => 'fix-left',
            ]).'Назад', '#tab-item-tax', [
                'class' => 'button-regular button-grey w-100 mt-2',
                'data-toggle' => 'tab',
            ]) ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-item-summary" role="tabpanel">
        <div class="tab-pane-content">
            <h5><?= Html::encode($model->getTitle(true,true)) ?></h5>
            <div class="mb-2">
                <?= Html::activeLabel($model, 'inn', ['class' => 'label mb-1']) ?>
                <div>
                    <?= Html::encode($model->inn) ?>
                </div>
            </div>
            <div class="mb-2">
                <?= Html::activeLabel($model, 'egrip', ['class' => 'label mb-1']) ?>
                <div>
                    <?= Html::encode($model->egrip) ?>
                </div>
            </div>
            <div class="mb-2">
                <?= Html::activeLabel($model, 'address_legal', ['class' => 'label mb-1']) ?>
                <div>
                    <?= Html::encode($model->address_legal) ?>
                </div>
            </div>
            <div class="mb-2">
                <?= Html::activeLabel($model, 'companyTaxationType', ['class' => 'label mb-1']) ?>
                <div>
                    УСН
                    <span class="tax-usn-persent">
                        <?= Html::encode($model->companyTaxationType->usn_percent) ?>
                    </span>%
                </div>
            </div>
            <div class="mb-3">
                <?= Html::activeLabel($model, 'okved', ['class' => 'label mb-1']) ?>
                <div>
                    <?= Html::encode($model->okved) ?>
                </div>
            </div>
            <?php foreach ($questions as $key => $value) : ?>
                <div class="checkbox d-block mb-2">
                    <?= Html::checkbox("{$key}_checkbox", true, [
                        'id' => "{$key}_checkbox",
                        'uncheck' => 0,
                        'label' => $value,
                        'labelOptions' => [
                            'style' => 'width: 90%;',
                        ],
                        'class' => 'questions-checkboxes',
                        'data-message' => "{$key}_message",
                    ]) ?>
                    <div style="padding-left:30px">
                        <?= isset($questions_messages[$key]) ? $questions_messages[$key] : ''; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::submitButton('Подтвердить', [
                'class' => 'button-regular button-regular_red w-100 mt-2 ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::a(Icon::get('arrow-left', [
                'class' => 'fix-left',
            ]).'Назад', '#tab-item-percent', [
                'class' => 'button-regular button-grey w-100 mt-2',
                'data-toggle' => 'tab',
            ]) ?>
        </div>
    </div>
</div>

<?php $form->end(); ?>

<?php Modal::begin([
    'id' => 'modal-not-available',
    'title' => 'УСН 15%',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <div class="form-group">
        1. Для системы налогообложения "Доходы минус расходы",
        мы не сможем сделать автоматический расчет налогов и подготовку налоговой декларации.
    </div>
    <div class="form-group">
        2. Напишите нам на почту
        <?= Html::mailto($supportEmail, $supportEmail) ?>
        и мы порекомендуем вам одного из наших партнеров - проверенную бухгалтерскую компанию.
        Они рассчитают вам стоимость ведения бухгалтерии. От нас вы получите скидку до 20% на их услуги
    </div>
    <div class="form-group">
        3. Наш сервис помогает предпринимателям:
        <br>
        - Выставлять счета, акты, товарные наклвдные.
        <br>
        - Контролировать должников.
        <br>
        - Вести управленческий учет.
        <br>
        - Видеть финансовую аналитику по бизнесу.
        <br>
        - Учитывать товар.
    </div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal-not-available-osno',
    'title' => 'ОСНО',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <div class="form-group">
        1. Для системы налогообложения "ОСНО",
        мы не сможем сделать автоматический расчет налогов и подготовку налоговой декларации.
    </div>
    <div class="form-group">
        2. Напишите нам на почту
        <?= Html::mailto($supportEmail, $supportEmail) ?>
        и мы порекомендуем вам одного из наших партнеров - проверенную бухгалтерскую компанию.
        Они рассчитают вам стоимость ведения бухгалтерии. От нас вы получите скидку до 20% на их услуги
    </div>
    <div class="form-group">
        3. Наш сервис помогает предпринимателям:
        <br>
        - Выставлять счета, акты, товарные наклвдные.
        <br>
        - Контролировать должников.
        <br>
        - Вести управленческий учет.
        <br>
        - Видеть финансовую аналитику по бизнесу.
        <br>
        - Учитывать товар.
    </div>

<?php Modal::end(); ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_osno" style="display: inline-block; text-align: center;">
        Для ОСНО мы не сможем вам рассчитать налоги<br/> и подготовить декларацию
    </span>
</div>

<?php $this->registerJs(<<<JS
    var urlHash = window.location.hash || '#c-inn';
    if (urlHash && $('a'+urlHash+'.form-tab-link').length) {
        $('a'+urlHash).tab('show');
    } else {
        $('a#c-inn').tab('show');
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('a.active[data-toggle="tab"]').toggleClass('active', false);
    });
    $(document).on('click', '#tab-item-percent .usn_percent-radio-list label', function () {
        let form = this.form;
        $('.btn-tax-percent', form).toggleClass('active', false);
        $('.btn-tax-percent', this).toggleClass('active', true);
        let val = $(this).data('value');
        console.log(val);
        $('.tax-usn-persent', form).text(val);
        $('a.to-tab-item-tax').tab('show');
    });
    $(document).on('change', '#tab-item-percent .usn_percent-radio-list label', function () {
        let form = this.form;
        $('.btn-tax-percent', form).toggleClass('active', false);
        $('.btn-tax-percent', this).toggleClass('active', true);
        let val = this.for.value;
        console.log(val);
        $('.tax-usn-persent', form).text(val);
        $('a.to-tab-item-tax').tab('show');
    });
    // Подсказки
    $('.tax-usn-config input[type=radio]').change(function() {
        if (this.value == 1) {
            $('#ip-params-submit').prop('disabled', true);
            $('#message-robot-disabled-by-usn_type').show(250);
            $('#message-robot-enabled').hide(250);
        } else {
            $('#ip-params-submit').prop('disabled', false);
            $('#message-robot-disabled-by-usn_type').hide(250);
            $('#message-robot-enabled').show(250);
        }
    });
    $('.questions-checkboxes').change(function() {
       var msg_id = $(this).attr('data-message');
       if ($(this).prop('checked'))
           $('#'+msg_id).hide(250);
       else
           $('#'+msg_id).show(250);

       var disabled_next_steps = false;
       $('.questions-checkboxes').each(function() {
           if (!$(this).prop('checked') && !($(this).attr('name') == 'no_cashbox_checkbox')) {
               disabled_next_steps = true;
               return false;
           }
       });

       if (disabled_next_steps) {
           $('#ip-params-submit').attr('disabled', 'disabled');
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').addClass('disabled');
       } else {
           $('#ip-params-submit').removeAttr('disabled');
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').removeClass('disabled');
       }

    });
    $(document).on("click", ".sbs-el", function(e) {
          e.preventDefault();
          if ($(this).hasClass('disabled'))
            return false;
          else
            window.location.href = $(this).attr('href');
    });

    $(document).ready(function() {
        $("#ip-params-submit").parents(".button-pulsate").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    });
JS
)?>
