<?php

use frontend\models\Documents;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

?>

<div class="alert alert-danger">
    <?= nl2br($message) ?>
</div>

<h3>Вы можете:</h3>
<h4><?= Html::a('Перейти на главную страницу.', ['/site/index']) ?></h4>
<h4><?= Html::a('Выставить счет.', ['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]) ?></h4>