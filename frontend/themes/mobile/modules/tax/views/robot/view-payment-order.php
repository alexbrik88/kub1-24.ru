<?php

use common\components\TaxRobotHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */

$this->params['step'] = 5;

$year = mb_substr($model->tax_period_code, -4);
if ($model->kbk == TaxRobotHelper::$kbkUsn6) {
    $this->title = $taxRobot->getUsn6Title($taxRobot->getPeriodLabelByCode($model->tax_period_code));
    $title = 'Налог УСН Доходы';
    $payment = 'налог УСН за ';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr && mb_strpos($model->purpose_of_payment, '300') !== false) {
    $this->title = $taxRobot->getOver300Title($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в размере 1% за';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr) {
    $this->title = $taxRobot->getPfrTitle($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в ПФР за';
} elseif ($model->kbk == TaxRobotHelper::$kbkOms) {
    $this->title = $taxRobot->getOmsTitle($year);
    $title = 'Взносы в ОМС';
    $payment = 'взносы в ОМС за';
} else {
    $this->title = $model->purpose_of_payment;
    $title = '';
    $payment = '';
}

$needPay = $model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID;
$urlPeriod = $taxRobot->getUrlPeriodId();
?>

<?= Html::a('Назад к списку платежек', [
    'payment',
    'period' => $urlPeriod,
], [
    'class' => 'link mt-2',
]) ?>

<h4 class="mb-4 mt-2">
    <?= $this->title ?>
</h4>

<section class="mt-4">
    <?= $this->render('view-payment-order/status-block', [
        'model' => $model,
    ]); ?>
    <?= $this->render('view-payment-order/additional-info', [
        'model' => $model,
        'title' => $title,
        'payment' => $payment,
        'taxRobot' => $taxRobot,
        'needPay' => $needPay,
        'urlPeriod' => $urlPeriod,
    ]); ?>
</section>

<section class="mt-4">
    <div class="page-border table-responsive">
        <?= frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget::widget([
            'model' => $model,
            'toggleButton' => [
                'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                'title' => 'Последние действия',
            ]
        ]); ?>

        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model])) : ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                'update-payment-order',
                'id' => $model->id,
                'period' => $taxRobot->getUrlPeriodId(),
            ], [
                'title' => 'Редактировать',
                'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 ',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        <?php endif; ?>
        <div class="doc-container">
            <?= $this->render('@frontend/modules/documents/views/payment-order/_view', [
                'model' => $model,
                'company' => $model->company,
            ]); ?>
        </div>
    </div>
</section>

<?= $this->render('view-payment-order/action-buttons', [
    'model' => $model,
    'taxRobot' => $taxRobot,
    'needPay' => $needPay,
    'urlPeriod' => $urlPeriod,
]); ?>
