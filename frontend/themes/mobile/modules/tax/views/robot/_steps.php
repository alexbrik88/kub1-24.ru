<?php

use yii\bootstrap4\Html;
use yii\widgets\Menu;
use yii\helpers\Url;

$period = $this->context->taxRobot->getPeriod();
$periodId = $this->context->taxRobot->getUrlPeriodId();

?>

<div class="nav-tabs-row row pb-3 mb-1">
    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs w-100 justify-content-between',
        ],
        'itemOptions' => [
            'class' => 'nav-item pl-1 pr-1 d-flex flex-column',
        ],
        'encodeLabels' => false,
        'items' => [
            [
                //'label' => 'Реквизиты <br> вашего ИП',
                'url' => ['company'],
                'template' => $this->render('_steps_link', ['ok' => 1 < $step]),
            ],
            [
                //'label' => 'Параметры <br> вашего ИП',
                'url' => ['params'],
                'template' => $this->render('_steps_link', ['ok' => 2 < $step]),
            ],
            [
                //'label' => 'Доходы <br>' . $period->shortLabel,
                'url' => ['bank', 'period' => $periodId],
                'template' => $this->render('_steps_link', ['ok' => 3 < $step]),
            ],
            [
                //'label' => 'Рассчитать <br> налог',
                'url' => ['calculation', 'period' => $periodId],
                'template' => $this->render('_steps_link', ['ok' => 4 < $step]),
            ],
            [
                //'label' => 'Заплатить <br> налог',
                'url' => ['payment', 'period' => $periodId],
                'template' => $this->render('_steps_link', ['ok' => 5 < $step]),
            ],
            [
                //'label' => 'Налоговая <br> декларация',
                'url' => ['declaration', 'period' => $periodId],
                'template' => $this->render('_steps_link', ['ok' => false]),
            ],
        ],
    ]); ?>
</div>
