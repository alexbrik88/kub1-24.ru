<?php

use common\models\document\status\TaxDeclarationStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\components\Icon;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\bootstrap4\Dropdown;
use yii\helpers\Url;

/** @var \frontend\modules\tax\models\TaxDeclaration $model */
/** @var \frontend\modules\tax\models\Kudir $kudir */
/** @var \common\models\Company $company */
/** @var $isEmptyDeclaration boolean */

$this->title = 'Налоговая декларация';
$this->params['step'] = 6;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$taxation = $company->companyTaxationType;
$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-declaration-pjax',
]); ?>

<h4 class="mb-4">
    <div class="d-flex">
        <span class="pr-2">Налоговая декларация за</span>
        <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
            <?= Html::a(Html::tag('span', $taxRobot->period->label) . Icon::get('shevron'), '#', [
                'id' > 'cardProductTitle',
                'class' => 'link link_title',
                'role' => 'button',
                'data-toggle' => 'dropdown',
            ])?>
            <div class="dropdown-menu keep-open" aria-labelledby="cardProductTitle">
                <div class="popup-dropdown-in overflow-hidden">
                    <ul class="form-filter-list list-clr">
                        <?php foreach ($taxRobot->getPeriodDropdownItems('declaration') as $key => $item) : ?>
                            <li style="white-space: nowrap;">
                                <?= Html::a($item['label'], $item['url']) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php if (!preg_match('/^[0-9]{4}_4$/', $taxRobot->period->id)) : ?>
        <div style="font-size:1rem; font-style: italic; color: red; line-height: 1;">
            ВНИМАНИЕ! Декларация сдается<br>ТОЛЬКО за календарный год.
        </div>
    <?php endif ?>
</h4>

<section class="mt-4">
    <?= $this->render('parts_declaration/viewStatus', [
        'model' => $model,
        'canUpdate' => $canUpdate,
        'canUpdateStatus' => $canUpdateStatus,
        'sumTaxToPay' => $sumTaxToPay,
        'isEmptyDeclaration' => $isEmptyDeclaration,
        'period' => $taxRobot->getUrlPeriodId()
    ]); ?>
</section>

<section class="mt-4 table-responsive border-top">
    <div class="doc-container" style="width: 650px;">
        <?= $this->render('parts_declaration/view', [
            'model' => $model,
            'canUpdate' => $canUpdate,
            'canUpdateStatus' => $canUpdateStatus
        ]); ?>
    </div>
</section>

<section class="mt-4">
    <?= Html::a(Icon::get('print', ['class' => 'mr-1']) . ' <span class="ml-2">Печать</span>', [
        'declaration-print',
        'actionType' => 'print',
        'id' => $model->id,
        'empty' => $isEmptyDeclaration,
        'filename' => $model->getPrintTitle(),
    ], [
        'class' => 'button-regular button-regular_red w-100 mt-2',
        'target' => '_blank',
        'data' => [
            'pjax' => '0',
        ],
    ]) ?>
    <div class="dropup ">
        <?= Html::a(Icon::get('download', ['class' => 'mr-1']) . '<span class="ml-2">Скачать</span>', '#', [
            'class' => 'button-regular button-regular_red w-100 mt-2',
            'data-toggle' => 'dropdown',
        ]); ?>
        <?= \yii\bootstrap4\Dropdown::widget([
            'options' => [
                'class' => 'w-100',
            ],
            'items' => [
                [
                    'label' => 'Скачать PDF файл',
                    'encode' => false,
                    'url' => [
                        'declaration-print',
                        'actionType' => 'pdf',
                        'id' => $model->id,
                        'empty' => $isEmptyDeclaration,
                        'filename' => $model->getPdfFileName(),
                    ],
                    'linkOptions' => [
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]
                ],
                [
                    'label' => 'Скачать файл для ИФНС',
                    'encode' => false,
                    'url' => [
                        '/tax/declaration/xml',
                        'id' => $model->id,
                        'empty' => $isEmptyDeclaration
                    ],
                    'linkOptions' => [
                        'download' => 'download',
                        'data-pjax' => 0
                    ]
                ],
            ],
        ]); ?>
    </div>
    <?= Html::a(Icon::get('arrow-left', [
        'class' => 'fix-left',
    ]).'Назад', [
        ($isEmptyDeclaration) ? 'bank' : 'payment',
        'period' => $taxRobot->getUrlPeriodId(),
    ], [
        'class' => 'button-regular button-grey w-100 mt-2',
    ]) ?>
</section>

<?php \yii\widgets\Pjax::end(); ?>

<?= $this->render('parts_declaration/modal-video-instruction') ?>