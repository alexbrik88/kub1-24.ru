<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$company = $taxRobot->company;

$needSelect = count($groupIds) > 1;
?>

<?php Modal::begin([
    'id' => 'taxrobot-pay-modal',
    'title' => 'Выберите тариф для оплаты',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'bodyOptions' => [
        'class' => 'taxrobot-pay-panel',
    ],
]); ?>

    <div class="main-block">
        <?php if ($needSelect) : ?>
            <div class="main-content">
                <?php foreach ($groupIds as $groupId) : ?>
                    <?php
                    $group = SubscribeTariffGroup::findOne($groupId);
                    $tariff = $group->getActualTariffs()->orderBy(['price' => SORT_ASC])->one();
                    $discount = $company->getMaxDiscount($tariff->id);
                    $discVal = $discount ? $discount->value : 0;
                    $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                    ?>
                    <div class="tariff-block-wrapper" data-group="<?= $groupId ?>">
                        <div class="header">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 60px; vertical-align: middle; text-align: center;">
                                        <?= Html::tag('div', Icon::get(Icon::forTariff($groupId), ['style' => 'font-size: 36px;']), [
                                            'style' => '
                                                display: inline-flex;
                                                align-items: center;
                                                justify-content: center;
                                                width: 60px;
                                                height: 60px;
                                                background-color: #4679AE;
                                                color: #fff;
                                                border-radius: 30px;
                                            ',
                                        ]) ?>
                                    </td>
                                    <td style="padding-left: 10px; font-size: 18px;">
                                        <?= implode('<br>ИП', explode(' ИП', $group->name)) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding-top: 30px;">
                            <div class="text-bold">
                                <div style="display: inline-block; font-size: 18px;">
                                    Оплата за <?= $tariff->getTariffName() ?>
                                </div>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                                <button  class="button-regular button-regular_red w-100 mt-3 tariff-block-button" data-group="<?= $groupId ?>">Выбрать</button>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <?php foreach ($groupIds as $groupId) : ?>
            <?= $this->render('_select_tariff_form', [
                'taxRobot' => $taxRobot,
                'groupId' => $groupId,
                'needSelect' => $needSelect,
            ]) ?>
        <?php endforeach ?>
    </div>

<?php Modal::end(); ?>

<?php
$this->registerJs('
    $(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
        e.preventDefault();
        $("#taxrobot-pay-modal").modal("show");
    });
');
if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    $("#taxrobot-pay-modal").modal("show");
');
}
