<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use frontend\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\themes\mobile\widgets\BtnConfirmModalWidget;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Заполните реквизиты';
$this->params['step'] = 1;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$this->registerJsFile( '@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );

$buttonFindIfns = Icon::get('search', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Найти код ИФНС и ОКТМО</span>';
$buttonCheckIfns = Icon::get('attention', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Проверить код ИФНС и ОКТМО</span>';

$innHelp = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Данные подтягиваются автоматически при вводе ИНН, изменять не рекоммендуется, только если есть несовпадения',
]);
?>

<style type="text/css">
.suggestions-addon {
    display: none !important;
}
</style>

<?php $form = ActiveForm::begin([
    'id' => 'company-update-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['mobileFieldConfig'],
]); ?>

<div class="hidden">
    <?= Html::a('', '#tab-item-inn', [
        'id' => 'c-inn',
        'class' => 'hidden form-tab-link',
        'data-toggle' => 'tab',
        'role' => 'tab',
    ]) ?>
    <?= Html::a('', '#tab-item-addres', [
        'id' => 'c-addres',
        'class' => 'hidden form-tab-link',
        'data-toggle' => 'tab',
        'role' => 'tab',
    ]) ?>
    <?= Html::a('', '#tab-item-account', [
        'id' => 'c-account',
        'class' => 'hidden form-tab-link',
        'data-toggle' => 'tab',
        'role' => 'tab',
    ]) ?>
</div>
<div class="tab-content" id="tax-robot-company-form-tab-content">
    <div class="tab-pane fade" id="tab-item-inn" role="tabpanel" aria-labelledby="home-tab">
        <h5>Введите ИНН вашего ИП, остальные данные заполнятся автоматически:</h5>
        <div class="tab-pane-content column-flex-container justify-content-center">
            <?= Html::activeHiddenInput($model, 'company_type_id'); ?>
            <?= Html::activeHiddenInput($model, 'ip_lastname'); ?>
            <?= Html::activeHiddenInput($model, 'ip_firstname'); ?>
            <?= Html::activeHiddenInput($model, 'ip_patronymic'); ?>
            <?= Html::activeHiddenInput($model, 'has_chief_patronymic'); ?>
            <?= Html::activeHiddenInput($model, 'address_legal'); ?>
            <?= Html::activeHiddenInput($model, 'egrip'); ?>
            <?= Html::activeHiddenInput($model, 'okved'); ?>
            <?= Html::activeHiddenInput($model, 'okato'); ?>
            <?= Html::activeHiddenInput($model, 'oktmo'); ?>
            <?= Html::activeHiddenInput($model, 'ifns_ga'); ?>
            <?= Html::activeHiddenInput($model, 'taxRegistrationDate'); ?>
            <?= Html::hiddenInput('not_use_account', 0) ?>
            <?= $form->field($model, 'inn')->textInput([
                'class' => 'form-control',
                'type' => 'number',
                'maxlength' => true,
                'placeholder' => 'Автозаполнение по ИНН'
            ]) ?>
            <div class="mb-3">
                Посмотрите видео, как просто происходит расчет налогов и подготовка декларации ИП
            </div>
            <div class="text-center">
                <iframe style="width:96vw; height:54vw"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="allowfullscreen"
                        src="https://www.youtube.com/embed/6J30L5e5_ZU">
                </iframe>
            </div>
            <br/>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::a('Сохранить', '#tab-item-addres', [
                'class' => 'button-regular button-regular_red w-100',
                'data-toggle' => 'tab',
            ]) ?>
            <div class="mt-2" style="height: 44px;">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-item-addres" role="tabpanel">
        <h5>Введите адрес ИП по прописке, чтобы подтянуть данные налоговой:</h5>
        <div class="tab-pane-content column-flex-container justify-content-center">
            <div class="form-group">
                <label class="label" for="fias-address">Адрес по прописке:</label>
                <div class="inp_one_line-product">
                    <?= Html::textInput(null, $model->getLegalAddress(), [
                        'id' => 'fias-address',
                        'class' => 'form-control fias-query-value',
                    ]); ?>
                    <div class="fias-query-help mt-3" style="color:red;font-style:italic;font-size:12px">
                        Начните вводить адрес и выберите нужный из предложенного списка
                    </div>
                </div>
                <div class="hidden">
                    <?= Html::hiddenInput('ifns', $model->ifns_ga, [
                        'id' => 'fias-result-ifns',
                        'disabled' => true,
                    ]); ?>
                    <?= Html::hiddenInput('oktmo', $model->oktmo, [
                        'id' => 'fias-result-oktmo',
                        'disabled' => true,
                    ]); ?>
                    <?= Html::hiddenInput('index', $model->getLegalPostcode(), [
                        'id' => 'fias-result-index',
                        'disabled' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::button('Сохранить', [
                'id' => 'copy-fias-result',
                'class' => 'button-regular button-regular_red w-100',
            ]) ?>
            <?= Html::a(Icon::get('arrow-left', [
                'class' => 'fix-left',
            ]).'Назад', '#tab-item-inn', [
                'class' => 'button-regular button-grey w-100 mt-2',
                'data-toggle' => 'tab',
            ]) ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-item-account" role="tabpanel">
        <h5>Введите данные расчетного счета, чтобы подготовить платежки на уплату налога:</h5>
        <div class="tab-pane-content">
            <strong class="small-title d-block mb-3 mt-3">
                Расчёт счет
                <span class="show-first-account-number">№1</span>
            </strong>
            <?= $form->field($account, 'bik')->textInput([
                'maxlength' => true,
                // 'type' => 'number',
                'class' => 'form-control input-sm dictionary-bik',
                'data' => [
                    'url' => Url::to(['/dictionary/bik']),
                    'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                    'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                    'target-ks' => '#' . Html::getInputId($account, 'ks'),
                    'target-rs' => '#' . Html::getInputId($account, 'rs'),
                    'target-city' => '#company-address_legal_city',
                    'target-collapse' => '#company-bank-block',
                ]
            ])->label('БИК банка') ?>
            <?= $form->field($account, 'rs')->textInput([
                'maxlength' => true,
                // 'type' => 'number',
            ]); ?>

            <?php $i = 1; ?>
            <?php foreach ($additionalAccounts as $additionalAccount) : ?>
                <?php $i++; ?>
                <div class="company-account" data-id="<?= $additionalAccount->id ?>">
                    <strong class="small-title d-block mb-3 pt-3">
                        Расчёт счет №<span><?=$i?></span>

                        <?= BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                                'class' => 'ml-1 link',
                                'style' => 'color: #bbc1c7;',
                                'tag' => 'a'
                            ],
                            'modelId' => $additionalAccount->id,
                            'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                        ]); ?>
                    </strong>
                    <?= $form->field($additionalAccount, 'bik')->textInput([
                        'maxlength' => true,
                        // 'type' => 'number',
                        'class' => 'form-control input-sm dictionary-bik',
                        'data' => [
                            'url' => Url::to(['/dictionary/bik']),
                            'target-name' => '#' . Html::getInputId($additionalAccount, 'bank_name'),
                            'target-city' => '#' . Html::getInputId($additionalAccount, 'bank_city'),
                            'target-ks' => '#' . Html::getInputId($additionalAccount, 'ks'),
                            'target-rs' => '#' . Html::getInputId($additionalAccount, 'rs'),
                            'target-city' => '#company-address_legal_city',
                            'target-collapse' => '#company-bank-block',
                        ]
                    ])->label('БИК вашего банка') ?>
                    <?= $form->field($additionalAccount, 'rs')->textInput([
                        'maxlength' => true,
                        // 'type' => 'number',
                    ]); ?>
                </div>
            <?php endforeach; ?>

            <?php foreach ($newAccounts as $newAccount) : ?>
                <?php $i++; ?>
                <div class="new-account company-account" style="display: none" data-id="<?= "new-{$i}" ?>">
                    <strong class="small-title d-block mb-3 pt-3">
                        Расчёт счет №<span><?= $i ?></span>

                        <?= BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                                'class' => 'ml-1 link',
                                'tag' => 'a'
                            ],
                            'modelId' => "new-{$i}",
                            'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                        ]); ?>
                    </strong>
                    <?= $form->field($newAccount, 'bik')->textInput([
                        'maxlength' => true,
                        // 'type' => 'number',
                        'class' => 'form-control input-sm dictionary-bik',
                        'data' => [
                            'url' => Url::to(['/dictionary/bik']),
                            'target-name' => '#' . Html::getInputId($newAccount, 'bank_name'),
                            'target-city' => '#' . Html::getInputId($newAccount, 'bank_city'),
                            'target-ks' => '#' . Html::getInputId($newAccount, 'ks'),
                            'target-rs' => '#' . Html::getInputId($newAccount, 'rs'),
                            'target-city' => '#company-address_legal_city',
                            'target-collapse' => '#company-bank-block',
                        ]
                    ])->label('БИК вашего банка') ?>
                    <?= $form->field($newAccount, 'rs')->textInput([
                        'maxlength' => true,
                        // 'type' => 'number',
                    ]); ?>
                </div>
            <?php endforeach; ?>
            <?= Html::button(Icon::get('add-icon', [
                'class' => 'svg-icon mr-1',
            ]).' <span class="ml-2">Добавить еще р/с</span>', [
                'class' => 'button-regular button-regular_red w-100 mt-2',
                'id' => 'add-new-account',
            ]); ?>
            <?php if ($account !== $model->mainCheckingAccountant): ?>
                <?= Html::button('У меня нет расчетного счета', [
                    'id' => 'not_use_account',
                    'class' => 'button-regular button-grey w-100 mt-2',
                    'data-tooltip-content' => '#tooltip_not_use_account',
                    'style' => 'margin-top:15px;display:none'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="tab-pane-buttons">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-regular_red w-100 mt-2 ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::a(Icon::get('arrow-left', [
                'class' => 'fix-left',
            ]).'Назад', '#tab-item-addres', [
                'class' => 'button-regular button-grey w-100 mt-2',
                'data-toggle' => 'tab',
            ]) ?>
        </div>
    </div>
</div>

<?php $form->end(); ?>

<?= $this->render('//company/form/_company_inn_api') ?>

<?= $this->render('_company_modal') ?>

<?php if (Yii::$app->session->remove('show_example_popup') || Yii::$app->request->get('show_example')) {
    echo $this->render('_first_show_modal');
}
?>

<?php
$this->registerJs(<<<JS
    var urlHash = window.location.hash || '#c-inn';
    if (urlHash && $('a'+urlHash+'.form-tab-link').length) {
        $('a'+urlHash).tab('show');
    } else {
        $('a#c-inn').tab('show');
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('a.active[data-toggle="tab"]').toggleClass('active', false);
    });
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            if (!$($(this).data('target-rs')).val())
                $($(this).data('target-rs')).val('40802810').focus();
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
            if (suggestion.city && $(this).data('target-city')) {
                var city = suggestion.city[0].toUpperCase() + suggestion.city.slice(1).toLowerCase();
                $($(this).data('target-city')).addClass('edited').val(city);
            }
        }
    });

    window.isTaxRobot = true;

    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('.show-first-account-number').show();
        $('.new-account:hidden').first().show(250);
    });

    $("#fias-address").suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: "78497656dfc90c2b00308d616feb9df60c503f51",
        type: "ADDRESS",
        onSelect: function(suggestion) {
            console.log(suggestion.data);
            var error = 'Уточните адрес.';
            if (suggestion.data) {
                if (suggestion.data.tax_office_legal) {
                    $('#fias-result-ifns').val(suggestion.data.tax_office_legal);
                } else {
                    $('#fias-result-ifns').val('');
                }
                if (suggestion.data.oktmo) {
                    $('#fias-result-oktmo').val(suggestion.data.oktmo.slice(0,8));
                } else {
                    $('#fias-result-oktmo').val('');
                }
                if (suggestion.data.postal_code) {
                    $('#fias-result-index').val(suggestion.data.postal_code);
                } else {
                    $('#fias-result-index').val('');
                }
            }
        }
    });

    $(document).on("click", "#copy-fias-result", function(e) {
        var input = $('#fias-address');
        var form_group = input.closest(".form-group");
        var fiasAddressError = [];
        input.toggleClass('is-invalid', false);
        form_group.find('.invalid-feedback').remove();

        if ($('#fias-address').val().trim() == "") {
            fiasAddressError.push('Необходимо заполнить адрес.');
        }

        if ($("#fias-result-ifns").val()) {
            $("#company-ifns_ga").val($("#fias-result-ifns").val());
        } else {
            fiasAddressError.push('Код ИФНС не найден. Уточните адрес.');
        }
        if ($("#fias-result-oktmo").val()) {
            $("#company-oktmo").val($("#fias-result-oktmo").val());
        } else {
            fiasAddressError.push('Код ОКТМО не найден. Уточните адрес.');
        }
        if ($("#fias-result-index").val()) {
            $("#company-address_legal").val($("#fias-result-index").val() + ', ' + $("#fias-address").val());
        } else {
            fiasAddressError.push('Почтовый индекс не найден. Уточните адрес.');
        }

        if (fiasAddressError.length > 0) {
            input.toggleClass('is-invalid', true);
            input.after("<div class=\"invalid-feedback\">"+fiasAddressError[0]+"</div>");
            return;
        }

        window.ifns_error = 0;
        window.oktmo_error = 0;
        $('a#c-account').tab('show');
    });

    $(document).ready(function() {
       if (!$('#company-inn').val()) {
           $('#company-inn').parents('.form-group').find('label').css({'color':'red'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       }
    });

    $('#company-inn').on('change', function() {
       if (!$('#company-inn').val()) {
           $('#company-inn').parents('.form-group').find('label').css({'color':'red'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       } else {
           $('#company-inn').parents('.form-group').find('label').css({'color':'#999'});
       }
    });

   window.ifns_error = 0;
   window.oktmo_error = 0;
   window.show_pulsate = 0;
    $('#company-update-form').on('afterValidateAttribute', function (event, attribute, message) {

        //console.log(attribute);
        if (attribute.container == '.field-checkingaccountant-rs' && message.length
            || attribute.container == '.field-checkingaccountant-bik' && message.length) {

            $('#not_use_account').show();
        }

        if (attribute.container == '.field-company-ifns_ga') {
            window.ifns_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }
        if (attribute.container == '.field-company-oktmo') {
            window.oktmo_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }

        if (window.ifns_error || window.oktmo_error) {
            if (window.show_pulsate) {
                window.show_pulsate = 0;
                $("#get-fias-ittem").addClass('error').parent().pulsate({
                   color: "#f00",
                   reach: 20,
                   repeat: 3
                });
            }
        } else {
            $("#get-fias-ittem").removeClass('error');
        }
    });

    $('#not_use_account').on('click', function() {
       var form  = $('#company-update-form');
       var input = $(form).find('input[name="not_use_account"]');
       // use accounts
       if ($(input).val() == 1) {
           $('#add-new-account').removeAttr('disabled');
           $(input).val(0);
           $(form).submit();
           $(this).addClass('error');
       // not use accounts
       } else {
           $(input).val(1);
           $(form).find('.has-error').removeClass('has-error');
           $('.new-account').hide().find('input').val("");
           $('#add-new-account').prop('disabled', true);
           $('#checkingaccountant-bik, #checkingaccountant-rs, #checkingaccountant-bank_name, #checkingaccountant-bank_city, #checkingaccountant-ks').val("");
           $(this).removeClass('error');
       }
    });

    $('.sbs-el').bind('click', function(e) {
        e.preventDefault();
        $('#company-update-form').yiiActiveForm('validate');

        if ($("#company-update-form").find(".has-error").length) {
            taxrobotCompanyPanelOpen();
        }

        $('#company-update-form').yiiActiveForm('submitForm');
    });

    function repaintAccountNumbers() {
        $('.company-account').each(function(i,v) {
           $(v).find('.small-title > span').html(2+i);
        });
    }

    $(document).on("click", ".btn-confirm-yes", function() {
        var account_id = $(this).data('model_id');
        var account_block = $('.company-account').filter('[data-id="' + account_id + '"]');

        if (account_block.length) {

            if (!$(account_block).find('.dictionary-bik').val().trim())
               $('#add-new-account').removeAttr('disabled');

            if (!$(account_block).hasClass('new-account')) {
                $('#company-update-form').append('<input type="hidden" name="delete_rs[]" value="' + account_id + '" />');
            }

            $(account_block).remove();
            repaintAccountNumbers();
        }

        return false;
    });
JS
);
