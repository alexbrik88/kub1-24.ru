<?php

use yii\bootstrap4\Html;

/* @var \yii\web\View $this */
/* @var \frontend\modules\subscribe\forms\OnlinePaymentForm $form */

?>

<div class="hidden">
    <form id="robokassa-form" action="<?= \Yii::$app->params['robokassa']['merchantFormAction']; ?>" method="POST">
        <?php foreach ($form->getFormFields() as $name => $value) {
            echo Html::hiddenInput($name, $value);
        } ?>
    </form>
</div>
<script type="text/javascript">
    document.getElementById('robokassa-form').submit();
</script>