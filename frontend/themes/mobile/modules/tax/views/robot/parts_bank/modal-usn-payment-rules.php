<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */

?>

<?php Modal::begin([
    'id' => 'not-accounting-incomes',
    'title' => 'Какие приходы не нужно учитывать в УСН',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <div class="text_size_14 weight-300">
        Доходы Индивидуального Предпринимателя всегда увеличивают сумму налога. Однако не все приходы на расчетный счёт и в кассу являются доходом:
        <br><br>
        <ul class="mb-3 pl-2 bullet bullet_color_grey">
            <li class="mb-2">поступления на расчетные счёта ИП личных денег;</li>
            <li class="mb-2">возвраты платежей от поставщиков;</li>
            <li class="mb-2">полученные кредиты и займы;</li>
            <li class="mb-2">возврат займов, которые выдавал сам ИП;</li>
            <li class="mb-2">гранты;</li>
            <li class="mb-2">обеспечительный платёж (например, при сдаче недвижимости в аренду) или залог;</li>
            <li class="mb-2">деньги, полученные ИП как агентом по агентскому договору, кроме агентского вознаграждения;</li>
            <li class="mb-2">доходы от бизнеса на другой системе налогообложения, если вы совмещаете УСН с ЕНВД или патентом;</li>
            <li class="mb-2">другие доходы из ст. 251 НК.</li>
        </ul>
        Данные поступления не должны учитываться при расчёте налога УСН.
        <div class="weight-700 mb-2 mt-3">Остались вопросы?</div>
        Напишите нам на почту: <a href="mailto:support@kub-24.ru">support@kub-24.ru</a>
    </div>

<?php Modal::end(); ?>
