<?php

use common\models\company\CheckingAccountant;
use common\models\Company;
use yii\bootstrap4\Html;

/* @var $checkingAccountant CheckingAccountant */
/* @var $title string */
/* @var $id string */
/* @var $company Company|null */

?>
<div class="modal fade <?= $checkingAccountant->isNewRecord ? 'new-company-rs' : 'exists-company-rs'; ?>" id="<?= $id; ?>" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
                    'class' => 'modal-close close',
                    'data-dismiss' => 'modal',
                    'aria-label' => 'Close',
                ]) ?>
                <h4 class="modal-title"><?= $title ?></h4>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', [
                    'checkingAccountant' => $checkingAccountant,
                    'company' => $company,
                ]); ?>
            </div>
        </div>
    </div>
</div>
