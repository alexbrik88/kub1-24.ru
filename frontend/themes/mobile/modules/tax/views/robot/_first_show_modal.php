<style>
    #robot-first-show-modal .carousel-control.left,
    #robot-first-show-modal .carousel-control.right {
        background-image:none!important;
    }
    #robot-first-show-modal .glyphicon-chevron-right,
    #robot-first-show-modal .glyphicon-chevron-left {
        color: #000!important;
    }
    #robot-first-show-modal .glyphicon-chevron-left {
        margin-left: -30px;
    }
    #robot-first-show-modal .glyphicon-chevron-right {
        margin-right: -30px;
    }
</style>
<?php
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\bootstrap4\Carousel;

$header = ' Так будет выглядеть ваша декларация';

$carousel = [];
foreach (range(1, 3) as $n) {
    $src = "/img/examples/declaration-{$n}.jpg";
    if (($timestamp = @filemtime(Yii::getAlias("@frontend/web{$src}"))) > 0) {
        $src .= "?v={$timestamp}";
    }
    $carousel[] = [
        'content' => Html::img($src),
        'caption' => '',
        'options' => ['class' => 'my-class']
    ];
}

Modal::begin([
    'id' => 'robot-first-show-modal',
    'title' => $header,
    'footerOptions' => ['style' => 'text-align: center;'],
    'clientOptions' => ['show' => true],
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('Ок', [
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]),
]);

echo Html::beginTag('div', ['class' => 'text-center']);

echo Carousel::widget([
    'items' => $carousel,
    'options' => ['class' => 'carousel slide', 'data-interval' => '60000'],
    'controls' => [
        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
    ]
]);

echo Html::endTag('div');
Modal::end();