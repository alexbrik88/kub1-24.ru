<?php
use yii\bootstrap4\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use frontend\rbac\permissions\document\Document;
use yii\helpers\ArrayHelper;
use common\components\TaxRobotHelper;
use frontend\modules\tax\models\TaxrobotCashBankSearch;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $title string */
/* @var $payment string */
/* @var $needPay boolean */

?>

<div class="about-card mt-3">
    <div class="about-card-item">
        <span class="text-grey">Бюджетный платеж:</span>
        <?php if ($needPay) : ?>
            <?php if ($payment) : ?>
                <span><?= $payment ?> <?= $taxRobot->getPeriodLabelByCode($model->tax_period_code, true) ?></span>
            <?php endif ?>
            <br>
            <?php if ($title) : ?>
                <span class="text-grey">
                   <?= $title ?>:
                </span>
            <?php endif ?>
        <?php endif ?>
        <span><?= \common\components\TextHelper::moneyFormat($model->sum/100, 2) ?> ₽ </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Получатель::</span>
        <span>
            <?= $model->contractor ? Html::a($model->contractor_name, [
                '/contractor/view',
                'type' => $model->contractor->type,
                'id' => $model->contractor->id,
            ], [ 'data-pjax' => '0']) : $model->contractor_name; ?>
        </span>
    </div>
</div>

<div class="about-card mt-3">
    <div class="about-card-item">
        <span>
            Все платежки находятся в разделе
            <?= Html::a('Платежные поручения', [
                '/documents/payment-order/index'
            ], [
                'class' => 'link',
                'data-pjax' => '0',
            ]) ?>.
        </span>
    </div>
</div>
