<?php

use common\components\date\DateHelper;
use frontend\components\Icon;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
?>

<div class="button-regular paid w-100">
    <?= Icon::get('check-2', ['class' => 'mr-1']) ?>
    <span class="ml-3"><?= ArrayHelper::getValue($model, 'paymentOrderStatus.name') ?></span>
    <span class="ml-auto mr-1">
        <?= date('d.m.Y', $model->payment_order_status_updated_at) ?>
    </span>
</div>