<?php

use common\components\ImageHelper;
use common\models\document\status\PaymentOrderStatus;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use frontend\components\Icon;
use frontend\themes\mobile\modules\documents\widgets\PaymentOrderPaid;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $needPay boolean */

?>

<section class="mt-4">
    <?php if ($needPay &&
        $model->payment_order_status_id != PaymentOrderStatus::STATUS_SENT_TO_BANK &&
        (Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
        ($paymentAccount = $model->company->getBankingPaymentAccountants()->one()) !== null &&
        ($alias = Banking::aliasByBik($paymentAccount->bik)) &&
        ($bank = Banking::getBankByBik($paymentAccount->bik))
    ) : ?>
        <?php $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
            'class' => 'mr-2',
            'style' => 'display: inline-block;',
        ]);?>
        <?= Html::a($image.'<span>Отправить в ' . $bank->bank_name.'</span>', [
            "/cash/banking/{$alias}/default/payment",
            'account_id' => $paymentAccount->id,
            'po_id' => $model->id,
            'p' => Banking::routeEncode(['/documents/payment-order/view', 'id' => $model->id]),
        ], [
            'class' => 'link banking-module-open-link',
            'data' => [
                'pjax' => '0',
            ],
            'style' => ''
        ]); ?>
        <?= BankingModalWidget::widget([
            'pageTitle' => $this->title,
            'pageUrl' => Url::to(['robot/payment', 'period' => $urlPeriod]),
        ]) ?>
    <?php endif; ?>
    <?= Html::a('Импорт в файл', [
        '/documents/payment-order/import',
        'id' => $model->id,
    ], [
        'class' => 'button-regular button-regular_red w-100 mt-4',
        'data' => [
            'pjax' => '0',
        ],
    ]); ?>
    <?= Html::a(Icon::get('print', ['class' => 'mr-2']).'Печать', [
        '/documents/payment-order/document-print',
        'actionType' => 'print',
        'id' => $model->id,
        'type' => Documents::IO_TYPE_IN,
        'filename' => $model->getPrintTitle(),
    ], [
        'target' => '_blank',
        'class' => 'button-regular button-regular_red w-100 mt-2 no-reload-status print',
        'data' => [
            'pjax' => '0',
        ],
    ]); ?>
    <?= Html::a(Icon::get('pdf', ['class' => 'mr-2']).'PDF', [
        '/documents/payment-order/document-print',
        'actionType' => 'pdf',
        'id' => $model->id,
        'type' => Documents::IO_TYPE_IN,
        'filename' => $model->getPrintTitle(),
    ], [
        'target' => '_blank',
        'class' => 'button-regular button-regular_red w-100 mt-2',
        'data' => [
            'pjax' => '0',
        ],
    ]); ?>
    <?php if ($model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID) : ?>
        <?= PaymentOrderPaid::widget([
            'id' => 'many-paid',
            'title' => 'Платежное поручение оплачено',
            'modelId' => $model->id,
            'pjaxContainer' => null,
            'closeButton' => [
                'label' => Icon::get('close'),
                'class' => 'modal-close close',
            ],
            'toggleButton' => [
                'label' => Icon::get('check-double') . ' <span>Оплачено</span>',
                'class' => 'button-regular button-regular_red w-100 mt-2',
                'data-pjax' => 0,
            ],
        ]) ?>
    <?php endif ?>
    <?= Html::a(Icon::get('arrow-left', [
        'class' => 'fix-left',
    ]).'Назад', [
        'payment',
        'period' => $urlPeriod,
    ], [
        'class' => 'button-regular button-grey w-100 mt-2',
    ]) ?>
</section>
