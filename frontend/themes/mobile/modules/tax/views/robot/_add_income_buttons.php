<?php

use common\components\TaxRobotHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\components\Icon;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $accountArray common\models\company\CheckingAccountant[] */
/* @var $cashbox common\models\cash\Cashbox */
/* @var $p string */

$bankUrl = [
    '/cash/banking/default/select',
    'p' => $p,
];
foreach ($accountArray as $account) {
    if (($class = Banking::classByBik($account->bik)) !== null) {
        $bankUrl = [
            '/cash/banking/'.$class::ALIAS.'/default/index',
            'account_id' => $account->id,
            'p' => $p,
        ];
        break;
    }
}
?>

<?= Html::a(Icon::get('bank-3', [
    'class' => 'mr-3',
]).'Загрузить выписку из банка', $bankUrl, [
    'class' => 'button-regular button-hover-content-red w-100 mt-2 banking-module-open-link',
]) ?>
<?= Html::a(Icon::get('1c', [
    'class' => 'mr-3',
]).'Загрузить выписку файлом', [
    "/cash/banking/default/index",
    'p' => $p,
    's' => 'f',
], [
    'class' => 'button-regular button-hover-content-red w-100 mt-2 banking-module-open-link',
]) ?>

<div class="dropdown">
    <?= Html::button('Добавить вручную', [
        'class' => 'button-regular button-hover-content-red w-100 mt-2',
        'data-toggle' => 'dropdown',
    ]) ?>
    <div class="form-filter-drop dropdown-menu">
        <ul class="form-filter-list list-clr">
            <li>
                <?= Html::a('Добавить по банку', [
                    'create-movement',
                    'type' => TaxRobotHelper::TYPE_BANK,
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'redirect' => Url::to(Banking::currentRoute()),
                    'skipDate' => 1,
                    'canAddContractor' => 1,
                    'canAddAccount' => 1
                ], [
                    'class' => 'update-movement-link update-bank-movement-link link text-center',
                    'style' => 'font-size: 1.4rem;',
                    'data' => [
                        'pjax' => '0',
                    ],
                ]) ?>
            </li>
            <li class="border-top">
                <?= Html::a('Добавить по кассе', [
                    'create-movement',
                    'type' => TaxRobotHelper::TYPE_ORDER,
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'redirect' => Url::to(Banking::currentRoute()),
                    'skipDate' => 1,
                    'canAddContractor' => 1,
                    'id' => $cashbox->id,
                ], [
                    'class' => 'update-movement-link update-order-movement-link link text-center',
                    'style' => 'font-size: 1.4rem;',
                    'data' => [
                        'pjax' => '0',
                    ],
                ]) ?>
            </li>
        </ul>
    </div>
</div>

<?php /*
<?= Html::a('Загрузить данные из ОФД', [
    '/cash/ofd/default/index',
    'p' => $p,
], [
    'class' => 'button-regular button-hover-content-red w-100 mt-2 ofd-module-open-link',
]) ?>
*/