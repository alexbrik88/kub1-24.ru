<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

$company = $taxRobot->company;
$tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_TAXROBOT_PAY_EXTRA);
$model = new PaymentForm($company, ['tariffId' => $tariff->id]);

?>

<?php Modal::begin([
    'id' => 'taxrobot-pay-modal',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'bodyOptions' => [
        'class' => 'taxrobot-pay-panel',
    ],
]); ?>

    <div class="main-block">
        <div class="main-content">
            <div class="header">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 60px; vertical-align: middle; text-align: center;">
                            <?= Html::tag('div', Icon::get(Icon::forTariff($tariff->tariff_group_id), [
                                'style' => 'font-size: 36px;',
                            ]), [
                                'style' => '
                                    display: inline-flex;
                                    align-items: center;
                                    justify-content: center;
                                    width: 60px;
                                    height: 60px;
                                    background-color: #4679AE;
                                    color: #fff;
                                    border-radius: 30px;
                                ',
                            ]) ?>
                        </td>
                        <td style="padding-left: 10px; font-size: 18px;">
                            <?= implode('<br>ИП', explode(' ИП', $tariff->tariffGroup->name)) ?>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="part-pay">
                <div style="padding-top: 30px;">
                    <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                        'class' => 'tariff-group-payment-form',
                    ]) ?>
                        <?= Html::activeHiddenInput($model, 'tariffId', [
                            'class' => 'tariff-id-input',
                        ]) ?>
                        <div class="text-bold">
                            <div style="display: inline-block; font-size: 18px;">
                                <?= $tariff->proposition ?> на <?= $tariff->getTariffName() ?>
                            </div>
                            <?php
                            $discount = $company->getMaxDiscount($tariff->id);
                            $discVal = $discount ? $discount->value : 0;
                            $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                            ?>
                            <?= Html::beginTag('div', [
                                'class' => "tariff-price tariff-price-{$tariff->id}",
                            ]) ?>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month, -1) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                            <?= Html::endTag('div') ?>
                        </div>
                        <div class="mar-t-20">
                            <p style="font-weight:bold;font-size:18px">Вы получите:</p>
                            <p>Автоматический расчёт налогов.</p>
                            <p style="font-weight:bold">Уменьшение налога в соответствии с законом.</p>
                            <p>Подготовка налоговых платёжек.</p>
                            <?php if ($tariff->tariff_group_id == SubscribeTariffGroup::TAX_DECLAR_IP_USN_6) : ?>
                                <p>Автоматическая подготовка налоговой декларации.</p>
                                <p>Вся отчётность в налоговую, кроме отчётов за сотрудников.</p>
                                <p>Автоматическая подготовка КУДиР.</p>
                            <?php endif ?>
                            <p style="font-weight:bold;">
                                Тариф "Выставление счетов" не входит в стоимость тарифа
                                "<?= $tariff->tariffGroup->name ?>".
                            </p>
                        </div>

                        <div id="js-form-alert" class="mar-t-20"></div>

                        <div class="row mar-t-20">
                            <div class="col-6">
                                <?= Html::submitButton('Картой', [
                                    'class' => 'button-regular button-regular_red submit',
                                    'name' => Html::getInputName($model, 'paymentTypeId'),
                                    'value' => PaymentType::TYPE_ONLINE,
                                    'style' => 'width: 100%;',
                                    'data-style' => 'zoom-in',
                                ]); ?>
                            </div>
                            <div class="col-6">
                                <?= Html::submitButton('Выставить счет', [
                                    'class' => 'button-regular button-regular_red submit',
                                    'name' => Html::getInputName($model, 'paymentTypeId'),
                                    'value' => PaymentType::TYPE_INVOICE,
                                    'style' => 'width: 100%;',
                                    'data-style' => 'zoom-in',
                                ]); ?>
                            </div>
                        </div>
                        <div class="form-submit-result hidden"></div>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>

<?php Modal::end(); ?>

<?php
$this->registerJs('
    $(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
        e.preventDefault();
        $("#taxrobot-pay-modal").modal("show");
    });
');
if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    $("#taxrobot-pay-modal").modal("show");
');
}
