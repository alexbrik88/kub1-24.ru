<?php use yii\bootstrap4\Modal;

$src = 'https://www.youtube.com/embed/6J30L5e5_ZU';
?>

<?php if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company): ?>
    <?php if (!\Yii::$app->request->cookies['ip_usn_6_instruction_modal_showed'] || Yii::$app->request->get('show_modal')): ?>

        <?php

        // Show 1 time
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'ip_usn_6_instruction_modal_showed',
            'value' => '1',
            'expire' => 0
        ]));

        Modal::begin([
            'id' => 'modal-ip-usn-6-video-instruction',
            'closeButton' => false,
        ]); ?>

            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="form-body" style="min-height: 75vh">
                <div>
                    <span style="font-size: 19px; font-weight: 700; padding-right:20px;">Без загруженной выписки, декларация ИП будет пустой.</span><br><br>
                    Перейдите в Веб версию с компьютера, чтобы было удобнее получить расчет налога и заполненную декларацию ИП.<br><br>
                    Посмотрите видео как это просто:<br/><br>
                </div>
                <div class="text-center">
                    <iframe
                        style="width:100%;height: 70%;"
                        frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="allowfullscreen"
                        src=""
                    >
                    </iframe>
                </div>
            </div>

        <?php Modal::end(); ?>

        <script>
            $(document).ready(function() {
                const modal = $('#modal-ip-usn-6-video-instruction');
                const modalWidth = modal.width();
                modal.find('iframe').height((0.5625 * modalWidth) + 'px').attr('src', '<?=($src)?>');
                modal.modal('show');
            })
        </script>

    <?php endif; ?>
<?php endif; ?>

