<?=
$this->render('partial/_pdf-page-1', [
    'model' => $model,
    'company' => $company
]);
?>

<?=
$this->render('partial/_pdf-page-2', [
    'model' => $model,
    'company' => $company
]);
?>

<?=
$this->render('partial/_pdf-page-3', [
    'model' => $model,
    'company' => $company
]);
?>
