<?php

use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\models\employee\Employee;
use common\models\company\CompanyType;
use common\components\TextHelper;

/* @var $company \common\models\Company */

$this->title = 'Налоговые декларации';
$emptyMessage = 'У вас пока не создано ни одной декларации.';
$isIp = $company->company_type_id == CompanyType::TYPE_IP;
$addUrl = $isIp ? ['/tax/robot/index'] : ['/tax/declaration-osno/index'];
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center mb-3">
        <h4><?= Html::encode($this->title) ?></h4>
        <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to($addUrl) ?>">
            <?= Icon::get('add-icon') ?>
            <span>Добавить</span>
        </a>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <h6 class="mt-2">
                Список налоговых деклараций: <?= $dataProvider->totalCount ?>
            </h6>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Отчетный год',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            [
                'attribute' => 'tax_year',
                'label' => 'Отчетный год',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]) ?
                        Html::a($data['tax_year'], ['/tax/declaration/view', 'id' => $data['id']]) :
                        $data['tax_year'];
                },
            ],
            [
                'attribute' => 'full_year_tax',
                'label' => $company->company_type_id == CompanyType::TYPE_IP ? 'Сумма налога за год' : 'Название декларации',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'value' => function ($data) use ($company) {
                    return $company->company_type_id == CompanyType::TYPE_IP ?
                        TextHelper::invoiceMoneyFormat(100 * $data['full_year_tax']) :
                        ($data['nds'] ? 'Декларация на добавленную стоимость' :
                            ($data['org'] ? 'Декларация на прибыль организации' :
                                ($data['balance'] ? 'Бухгалтерский баланс' :
                                    ($data['szvm'] ? 'Сведения о застрахованных лицах' :
                                'Единая упрощенная декларация'))));
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'document_correction_number',
                'label' => '№ корректировки',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    return \common\models\document\status\TaxDeclarationStatus::findOne(['id'=>$data['status_id']])->name;
                },
                'format' => 'raw',
                's2width' => '150px',
                'filter' => [
                    '' => 'Все',
                    1 => 'Создана',
                    3 => 'Принята',
                    4 => 'Передана',
                    5 => 'Скорректирована'
                ]
            ],
            [
                'attribute' => 'document_author_id',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    $employee = \common\models\EmployeeCompany::findOne([
                        'employee_id' => $data['document_author_id'],
                        'company_id' => $data['company_id']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getCreators(),
                's2width' => '200px',
            ],
        ],
    ]); ?>
</div>
