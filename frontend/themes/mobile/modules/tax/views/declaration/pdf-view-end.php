<?php
$declarationHelper = isset($declarationHelper) ? $declarationHelper : new \frontend\modules\tax\models\DeclarationOsnoHelper();

echo $this->render('end/_pdf-page-1', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
