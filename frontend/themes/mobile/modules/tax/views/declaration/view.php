<?php

use frontend\components\Icon;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\UserRole;
use common\models\company\CompanyType;
use common\components\helpers\Html;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use common\models\document\status\TaxDeclarationStatus;
use yii\helpers\Url;

/* @var TaxDeclaration $model
 * @var DeclarationOsnoHelper $declarationOsnoHelper
 */

$isIp = $model->company->company_type_id == CompanyType::TYPE_IP;

$this->title = $isIp ?
    "Налоговая декларация за {$model->tax_year} год":
    ($model->nds ?
        "Декларация на добавленную стоимость за {$model->tax_quarter}-й квартал {$model->tax_year} года" :
        ($model->org ?
            "Декларация на прибыль организации за {$model->tax_quarter}-й квартал {$model->tax_year} года" :
            ($model->balance ?
                "Бухгалтерский баланс за {$model->tax_year} год" :
                ($model->szvm ?
                    "Сведения о застрахованных лицах за {$monthName} {$model->tax_year} года" :
                    "Единая (упрощенная) налоговая декларация ОСНО за {$model->tax_quarter}-й квартал {$model->tax_year} года"))));

$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';

$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;
if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}
if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value, $repeat = 11)
    {
        return ($value > 0) ? round($value) : str_repeat("&mdash;", $repeat);
    }
}

?>

<?= Html::a('Назад к списку', 'index', [
    'class' => 'link',
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">
                    <?= Html::encode($this->title) ?>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if ($model->status_id != TaxDeclarationStatus::STATUS_ACCEPTED && $canUpdate): ?>
                    <a href="<?= Url::to(['update', 'id' => $model->id]); ?>"
                       title="Редактировать" class="button-list button-hover-transparent button-clr mb-3 ml-1">
                        <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                    </a>
                <?php endif; ?>
                <div class="doc-container">
                    <?= $this->render('partial/_viewText', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus,
                        'declarationOsnoHelper' => $declarationOsnoHelper,
                        'isIp' => $isIp,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('partial/_viewStatus', [
                'model' => $model,
                'canUpdate' => $canUpdate,
                'canUpdateStatus' => $canUpdateStatus,
                'declarationOsnoHelper' => $declarationOsnoHelper,
                'isIp' => $isIp,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('partial/_viewActionsButtons', [
    'model' => $model,
    'canUpdate' => $canUpdate,
    'canUpdateStatus' => $canUpdateStatus,
    'isIp' => $isIp,
]); ?>
