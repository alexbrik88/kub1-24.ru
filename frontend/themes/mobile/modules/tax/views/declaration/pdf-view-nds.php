<?php
$declarationHelper = isset($declarationHelper) ? $declarationHelper : new \frontend\modules\tax\models\DeclarationOsnoHelper();

$columnData = [
    '010' => $company->oktmo,
    '020' => '18210301000011000110',
    '030' => 0,
    '040' => 0,
    '050' => 0,
    '060' => '',
    '070' => '',
    '080' => ''
];

?>
<?=
$this->render('nds/_pdf-page-1', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
?>
<?=
$this->render('nds/_pdf-page-2', [
    'model' => $model,
    'company' => $company,
    'columnData' => $columnData,
    'declarationHelper' => $declarationHelper,
]);
?>
