<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration */
?>
<div class="page-content-in">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => false,
        'validateOnBlur' => false,
        'id' => 'tax-declaration',
        //'options' => ['enctype' => 'multipart/form-data'],
    ])); ?>

    <div class="form-body form-horizontal form-body_width">

        <?= $this->render('partial/_formMain', [
            'model' => $model,
            'quarters' => $quarters,
            'form' => $form,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>

    </div>

    <?php ActiveForm::end(); ?>
</div>
