<?php

use common\models\company\CompanyType;
use yii\bootstrap4\Html;
use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$company = Yii::$app->user->identity->company;
$controllerId = Yii::$app->controller->id;
$linkTemplate = '<a href="{url}" class="nav-link d-flex flex-column flex-grow-1 align-items-center text-center pb-2">
    <div class="flex-grow-1 d-flex flex-column justify-content-center pb-1">{label}</div>
</a>';
?>

<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>

<div class="menu-nav-tabs nav-tabs-row row pb-3 mb-1">
    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3',
        ],
        'itemOptions' => [
            'class' => 'nav-item pl-2 pr-2 d-flex flex-column',
        ],
        'linkTemplate' => $linkTemplate,
        'encodeLabels' => false,
        'items' => [
            [
                'label' => 'Налоговая декларация',
                'url' => ['/tax/declaration-osno/index-list'],
                'visible' => $company->company_type_id != CompanyType::TYPE_IP,
                'active' => $controllerId == 'declaration-osno',
            ],
            [
                'label' => 'Налоговая декларация',
                'url' => ['/tax/declaration/index'],
                'visible' => $company->company_type_id == CompanyType::TYPE_IP,
                'active' => $controllerId == 'declaration',
            ],
            [
                'label' => 'КУДиР',
                'url' => ['/tax/kudir/index'],
                'active' => $controllerId == 'kudir',
            ],
        ],
    ]); ?>
</div>

<?= $content ?>

<?php $this->endContent(); ?>
