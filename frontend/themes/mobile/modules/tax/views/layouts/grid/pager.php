<?php

use frontend\components\PageSize;
use yii\helpers\ArrayHelper;

$pageSizeParam = ArrayHelper::getValue($_params_, 'pageSizeParam', 'per-page');
?>

<div class="table-settings-view row align-items-center">
    <div class="col-9">
        <nav>
            {pager}
        </nav>
    </div>
    <div class="col-3">
        <?= $this->render('perPage', [
            'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => $pageSizeParam,
        ]) ?>
    </div>
</div>
