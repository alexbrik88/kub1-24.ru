<?php

use common\components\widgets\StepMenu;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$period = $this->context->taxRobot->getPeriod();
$periodId = $this->context->taxRobot->getUrlPeriodId();
$actionId = ArrayHelper::getValue(Yii::$app, 'controller.action.id');
?>

<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<div class="tax-robot-container">
    <nav>
        <?= StepMenu::widget([
            'options' => [
                'class' => 'tax-robot-nav nav nav-tabs justify-content-between',
            ],
            'itemOptions' => [
                'class' => 'nav-item',
                'style' => 'flex: 1 1 0;',
            ],
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => 'Реквизиты <br> вашего ИП',
                    'url' => ['/tax/robot/company'],
                    'template' => $this->render('_robot_step_link', ['i' => 'man']),
                ],
                [
                    'label' => 'Параметры <br> вашего ИП',
                    'url' => ['/tax/robot/params'],
                    'template' => $this->render('_robot_step_link', ['i' => 'lists']),
                ],
                [
                    'label' => 'Доходы за<br>' . $period->shortLabel,
                    'url' => ['/tax/robot/bank', 'period' => $periodId],
                    'template' => $this->render('_robot_step_link', ['i' => 'profit']),
                ],
                [
                    'label' => 'Рассчитать <br> налог',
                    'url' => ['/tax/robot/calculation', 'period' => $periodId],
                    'template' => $this->render('_robot_step_link', ['i' => 'signs']),
                ],
                [
                    'label' => 'Заплатить <br> налог',
                    'url' => ['/tax/robot/payment', 'period' => $periodId],
                    'template' => $this->render('_robot_step_link', ['i' => 'tax']),
                    'active' => in_array($actionId, ['view-payment-order', 'update-payment-order']) ? true : null,
                ],
                [
                    'label' => 'Налоговая <br> декларация',
                    'url' => ['/tax/robot/declaration', 'period' => $periodId],
                    'template' => $this->render('_robot_step_link', ['i' => 'tax-return']),
                ],
            ],
        ]); ?>
    </nav>

    <section>
        <?= $content ?>
    </section>
</div>

<?php $this->endContent(); ?>
