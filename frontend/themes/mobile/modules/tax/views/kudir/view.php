<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\tax\models\Kudir;
use frontend\rbac\UserRole;
use common\components\date\DateHelper;
use common\models\document\status\KudirStatus;

$this->title = 'Книга учета доходов и расходов';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';

$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;

?>

<?= Html::a('Назад к списку', 'index', [
    'class' => 'link',
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">
                    <?= Html::encode($this->title) ?>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= \frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if ($model->status_id != KudirStatus::STATUS_ACCEPTED && $canUpdate): ?>
                    <a href="<?= Url::to(['update', 'id' => $model->id]); ?>"
                       title="Редактировать" class="button-list button-hover-transparent button-clr mb-3 ml-1">
                        <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                    </a>
                <?php endif; ?>
                <div class="doc-container">
                    <?= $this->render('partial/_viewText', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('partial/_viewStatus', [
                'model' => $model,
                'canUpdate' => $canUpdate,
                'canUpdateStatus' => $canUpdateStatus
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('partial/_viewActionsButtons', [
    'model' => $model,
    'canUpdate' => $canUpdate,
    'canUpdateStatus' => $canUpdateStatus,
]); ?>
