<?php
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

/** @var \common\models\Company $company */
?>

<?= $this->render('partial/_pdf-view-patent-1', [
    'model' => $model,
    'company' => $company
]); ?>
<?= $this->render('partial/_pdf-view-patent-2', [
    'model' => $model,
    'company' => $company,
    'quartersBankFlows' => $quartersBankFlows
]); ?>