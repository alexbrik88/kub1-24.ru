<?php

use common\components\date\DateHelper;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\status\KudirStatus;
use yii\bootstrap4\Nav;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\Kudir; */

$company = $model->company;

$bankCount = 0;
$bankItems = [];
$bankArray = $company->getCheckingAccountants()
    ->groupBy('bik')
    ->indexBy('bik')
    ->orderBy('type')
    ->all();

if ($bankArray) {
    foreach ($bankArray as $account) {
        $bankItems[] = "№ {$account->rs} в {$account->bank_name}";
    }
    $bankCount = count($bankItems);
}

$start_date = "{$model->tax_year}-01-01";
if ($tax_registration_date = \DateTime::createFromFormat(DateHelper::FORMAT_USER_DATE, $company->gettaxRegistrationDate())) {
    if ($model->tax_year == $tax_registration_date->format('Y')) {
        $start_date = $tax_registration_date->modify("+ 3 days")->format('Y-m-d');
    }
}
?>

<style>
    .table {margin:0; padding:0;}
    .table td {border-top:none;}
    .table td.border-bottom {border-bottom:1px solid #000;}
    .table td.tip {padding:0 0 3px 0; font-size:7pt; text-align: center}
    .table td.ver-top {vertical-align:top;}
    .table tr.middle td {vertical-align:middle;padding:4px 0;}
    .table td.codes-h {border:2px solid #000; border-bottom:1px solid #000;}
    .table td.codes-m {border-right:2px solid #000; border-left:2px solid #000; border-bottom:1px solid #000;}
    .table td.inner-table {margin:0;padding:0;vertical-align:top;}
    .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {border-top:none;padding:4px;}
</style>

<div class="page-content-in m-size-div container-first-account-table no_min_h pad0">

    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-12 pad0 font-bold" style="height: inherit">
                <div style="padding-top: 12px">
                    <p style="font-size: 17px; width:100%; text-align: center">
                        Книга учета доходов и расходов на <?= $model->tax_year; ?> год
                    </p>
                </div>

            </div>

        </div>

        <div class="col-xs-12 pad3 preview-declaration" style="margin-top: 15px;">
            <div class="col-xs-12 pad0" style="font-size: 12px; text-align:justify">

                <table class="table no-border">
                    <tr>

                        <td class="inner-table" style="padding-top:4px;">
                            <table class="table no-border">
                                <tr>
                                    <td colspan="3" class="text-right"> <br/> </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right">Форма по ОКУД</td>
                                </tr>
                                <tr>
                                    <td width="45%"></td>
                                    <td width="20%" class="border-bottom text-center">на <?= $model->tax_year ?> год</td>
                                    <td class="text-right">Дата (год, месяц, число)</td>
                                </tr>
                            </table>
                            <table class="table no-border" style="margin-right:20px">
                                <tr>
                                    <td width="40%">Налогоплательщик (наименование</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>организации/фамилия, имя, отчество</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        индивидуального предпринимателя)
                                    </td>
                                    <td class="border-bottom ver-bottom">
                                        <?= $company->getIpFio() ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="table no-border">
                                <tr>
                                    <td class="text-right">
                                        по ОКПО
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Идентификационный номер налогоплательщика-организации/ код
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        причины постановки на учет в налоговом органе (ИНН/КПП)
                                    </td>
                                </tr>
                            </table>
                            <table class="table no-border" style="margin:9px 2px">
                                <tr>
                                    <?php for ($i=0; $i<22; $i++) : ?>
                                        <td style="border: 1px solid #000; line-height:12pt" class="text-center">
                                            <?= $i==13 ? '/' : '&nbsp;' ?>
                                        </td>
                                    <?php endfor; ?>
                                    <td style="width:20px"></td>
                                </tr>
                            </table>
                            <table class="table no-border">
                                <tr>
                                    <td>
                                        Идентификационный номер налогоплательщика - индивидуального
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        предпринимателя (ИНН)
                                    </td>
                                </tr>
                            </table>
                            <table class="table no-border" style="margin:9px 2px">
                                <tr>
                                    <?php for ($i=0; $i<22; $i++) : ?>
                                        <td style="<?= ($i<12) ? 'border: 1px solid #000;':'' ?> line-height:12pt" class="text-center">
                                            <?= ($company->inn && isset($company->inn[$i])) ? $company->inn[$i] : '&nbsp;' ?>
                                        </td>
                                    <?php endfor; ?>
                                    <td style="width:20px"></td>
                                </tr>
                            </table>
                            <table class="table no-border">
                                <tr>
                                    <td width="27%">
                                        Объект налогообложения
                                    </td>
                                    <td class="border-bottom ver-bottom">
                                        Доходы
                                    </td>
                                    <td width="20px"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="tip">(наименование выбранного объекта налогообложения</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="border-bottom"> <br/> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="tip">в соответствии со статьей 346.14 Налогового кодекса Российской Федерации)</td>
                                    <td></td>
                                </tr>
                            </table>
                            <table class="table no-border">
                                <tr>
                                    <td width="21%">Единица измерения:</td>
                                    <td>руб.</td>
                                    <td class="text-right">по ОКЕИ</td>
                                </tr>
                            </table>
                        </td>

                        <td width="20%" class="inner-table">
                            <table class="table no-border">
                                <tr>
                                    <td colspan="3" class="text-center font-bold codes-h">
                                        Коды
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m">
                                        <?= $company->okud ?: '<br/>' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center border-bottom" style="border-left:2px solid #000">
                                        <?= date('Y', strtotime($start_date)) ?>
                                    </td>
                                    <td class="text-center border-bottom" style="border-left:1px solid #000;border-right:1px solid #000;">
                                        <?= date('m', strtotime($start_date)) ?>
                                    </td>
                                    <td class="text-center border-bottom" style="border-right:2px solid #000">
                                        <?= date('d', strtotime($start_date)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m"> <br/> </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m">
                                        <?= $company->okpo ?: '<br/>'; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m" style="line-height: 200pt"> <br/> </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center codes-m"> 383 </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td colspan="2">Адрес места нахождения организации</td>
                    </tr>
                    <tr>
                        <td colspan="2">(места жительства индивидуального</td>
                    </tr>
                    <tr>
                        <td width="16%">предпринимателя)</td>
                        <td class="border-bottom"><?= $company->getAddressActualFull() ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="border-bottom"> <br/> </td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td width="60%">Номера расчетных и иных счетов, открытых в учреждениях банков</td>
                        <td class="border-bottom"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tip">(номера расчетных</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="border-bottom"> <?php if ($bankItems > 0) echo $bankItems[0] ?> </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="tip">и иных счетов и наименование соответствующих банков)</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <?php if ($bankItems > 1) foreach ($bankItems as $key=>$item) : if ($key == 0) continue; ?>
                        <tr>
                            <td colspan="2" class="border-bottom"> <?= $item ?> </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>

    </div>
</div>

