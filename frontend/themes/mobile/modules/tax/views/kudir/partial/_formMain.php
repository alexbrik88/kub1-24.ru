<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\Kudir */

$textAreaStyle = 'width: 100%; font-size:14px; text-align:justify; white-space: pre-line;';

$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$isNewRecord = $model->isNewRecord;

$tax_years_list = [];
$date_max = date('Y') + 1;
$date_min = $model->company->ip_certificate_date ? date('Y', strtotime($model->company->ip_certificate_date)) : 2010;
if ($date_max > $date_min) {
    for ($i = $date_max; $i >= $date_min; $i--)
        $tax_years_list[$i] = $i;
} else {
    $tax_years_list[date('Y') + 1] = [date('Y') + 1];
    $tax_years_list[date('Y')] = [date('Y')];
}
?>

    <style>
        .kudir-form .control-label {
            font-weight: bold;
        }
        .kudir-form .form-column-max-width {
            max-width: 750px;
        }
        .kudir-form .label-max-width {
            max-width: 350px;
            padding-right: 0;
        }
        .kudir-form .label-horizontal label {
            padding-bottom:5px;
        }
        .kudir-form #tax_year {
            display: inline-block;
            width: 80px;
            margin-left: 5px;
            margin-right: 5px;
            height: 100%;
            vertical-align: middle;
        }
        .kudir-form input,
        .kudir-form select {margin-left:0;}
    </style>

    <?= $form->errorSummary($model); ?>
<div class="wrap">
    <div class="kudir-form">
        <div class="portlet">
            <div class="portlet-title form-group">
                <div class="caption" style="width: 100%">
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td valign="middle" style="width:1%; white-space:nowrap;">
                            <span>
                                <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                                    'id' => 'isNewRecord',
                                ]) ?>

                                <strong>КУДиР на </strong>

                                <?= Html::dropDownList('Kudir[tax_year]', $model->tax_year, $tax_years_list, [
                                    'id' => 'tax_year',
                                    'class' => 'form-control width-120',
                                ]); ?>

                                <strong> год</strong>
                                <br class="box-br">
                            </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="portlet-body">
                <div class="form-group row">
                    <div class="col-sm-5 label-max-width">
                        <label class="control-label">
                            Дата создания<span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div class="col-sm-7">
                        <div class="input-icon">
                            <?= Html::activeTextInput($model, 'document_date', [
                                'id' => 'document_date',
                                'class' => 'form-control date-picker ico',
                                'style' => 'width:130px',
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
$this->registerJs(<<<JS
// $(document).ready(function() {});
JS
);

