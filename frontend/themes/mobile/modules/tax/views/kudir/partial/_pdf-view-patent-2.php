<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
?>
<!-- I КВАРТАЛ -->
<div class="page-content-in p-center pad-pdf-p">
    <table class="table no-border">
        <tr>
            <td class="text-center font-size-9">I. Доходы</td>
        </tr>
    </table>
    <br/>
    <table class="table">
        <tr>
            <td colspan="3" class="text-center">Регистрация</td>
            <td rowspan="2" width="15%" class="text-center ver-top">Доходы<br/>(руб.)</td>
        </tr>
        <tr class="middle">
            <td width="5%"  class="text-center">№<br/>п/п</td>
            <td width="10%" class="text-center">Дата и номер первичного документа</td>
            <td width="50%" class="text-center">Содержание операции</td>
        </tr>
        <tr>
            <td class="text-center">1</td>
            <td class="text-center">2</td>
            <td class="text-center">3</td>
            <td class="text-center">4</td>
        </tr>

        <?php
        $num_pp = 0;
        $total = 0;
        for ($q = 1; $q <= 4; $q++)
            foreach ($quartersBankFlows[$q] as $bankFlows) {
            ?>
                <?php if ($bankFlows instanceof \common\models\cash\CashBankFlows): ?>
                    <tr>
                        <td class="text-center">
                            <?= (++$num_pp) ?>
                        </td>
                        <td class="">
                            <?= '№ ' . $bankFlows->payment_order_number .  ' от ' .
                            DateHelper::format($bankFlows->recognition_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  ?>
                        </td>
                        <td class="ver-top">
                            <?= $bankFlows->incomeReason->name // Содержание операции ?>
                            "<?= $bankFlows->contractor !== null ?
                                $bankFlows->contractor->nameWithType :
                                $bankFlows->cashContractor->text;
                            ?>"
                        </td>
                        <td class="ver-top text-right">
                            <?= is_numeric($bankFlows->amountIncome) // Приход
                                ? TextHelper::invoiceMoneyFormat($bankFlows->amountIncome, 2)
                                : ''; ?>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td class="text-center">
                            <?= (++$num_pp) ?>
                        </td>
                        <td class="">
                            <?= '№ ' . $bankFlows->number .  ' от ' .
                            DateHelper::format($bankFlows->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  ?>
                        </td>
                        <td class="ver-top">
                            Операции по кассе: <?= $bankFlows->description // Содержание операции ?>
                        </td>
                        <td class="ver-top text-right">
                            <?= is_numeric($bankFlows->amount) // Приход
                                ? TextHelper::invoiceMoneyFormat($bankFlows->amount, 2)
                                : ''; ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php
                if (is_numeric($bankFlows->amountIncome)) $total += $bankFlows->amountIncome;
                elseif (is_numeric($bankFlows->amount)) $total += $bankFlows->amount;
            } ?>

        <tr>
            <td colspan="3">Итого за налоговый период</td>
            <td class="ver-top text-right"><?= ($total > 0) ? TextHelper::invoiceMoneyFormat($total, 2) : '' ?></td>
        </tr>
    </table>
</div>
