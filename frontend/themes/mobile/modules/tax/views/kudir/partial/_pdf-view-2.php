<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
?>
<!-- I КВАРТАЛ -->
<div class="page-content-in p-center pad-pdf-p page-break">
    <table class="table no-border">
        <tr>
            <td class="text-center font-size-9">I. Доходы и расходы</td>
        </tr>
    </table>
    <br/>
    <table class="table">
        <tr>
            <td colspan="3" class="text-center">Регистрация</td>
            <td colspan="2" class="text-center">Сумма</td>
        </tr>
        <tr class="middle">
            <td width="5%"  class="text-center">№<br/>п/п</td>
            <td width="10%" class="text-center">Дата и номер первичного документа</td>
            <td class="text-center">Содержание операции</td>
            <td width="15%" class="text-center">Доходы, учитываемые при исчислении налоговой базы</td>
            <td width="15%" class="text-center">Расходы, учитываемые при исчислении налоговой базы</td>
        </tr>
        <tr>
            <td class="text-center">1</td>
            <td class="text-center">2</td>
            <td class="text-center">3</td>
            <td class="text-center">4</td>
            <td class="text-center">5</td>
        </tr>

        <?php
        $num_pp = 0;
        $total = 0;
        $quarterTotal = 0;
        foreach ($quartersBankFlows[1] as $bankFlows) { $num_pp++;
            echo $this->render('_pdf-view-table-inner', [
                'bankFlows' => $bankFlows,
                'num_pp' => $num_pp
            ]);
            if (is_numeric($bankFlows->amountIncome)) $quarterTotal += $bankFlows->amountIncome;
        } ?>

        <tr>
            <td colspan="3">Итого за I квартал</td>
            <td class="ver-top text-right"><?= ($quarterTotal > 0) ? TextHelper::invoiceMoneyFormat($quarterTotal, 2) : '' ?></td>
            <td></td>
        </tr>
    </table>
</div>
