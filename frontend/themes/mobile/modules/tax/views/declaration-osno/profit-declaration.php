<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.04.2019
 * Time: 1:31
 */

use common\components\helpers\Html;
use yii\bootstrap4\Dropdown;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use yii\helpers\Url;

/* @var \frontend\modules\tax\models\TaxDeclaration $model */
/* @var \common\models\Company $company */
/* @var DeclarationOsnoHelper $declarationHelper */

$this->title = 'Декларация на прибыль организации';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>
<?= $this->render('_style') ?>
<div class="row">
    <div class="col-xs-12 step-by-step" style="margin-bottom: 10px;">
        <?= $this->render('_steps', ['step' => 6, 'company' => $company]); ?>
    </div>
    <div class="col-xs-12">
        <div>
            <?= Html::a('Назад к списку', Url::to(['all', 'period' => $declarationHelper->period->id]), ['class' => 'back-to-customers']); ?>
        </div>
        <div class="" style="font-size:16px; font-weight: bold;">
            Декларация на прибыль организации за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $declarationHelper->period->label, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                    ]) ?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'encodeLabels' => false,
                        'items' => $declarationHelper->getPeriodDropdownItems(DeclarationOsnoHelper::TYPE_ORG),
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 step-by-step">
        <div class="col-xs-12 pad0 mar-t-15">
            <div class="col-xs-12 col-lg-8 pad0">
                <div class="portlet customer-info project-info">
                    <?= $this->render('parts_profit_declaration/view', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                    ]); ?>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4 pad0 pull-right"
                 style="padding-bottom: 5px !important; max-width: 480px;">
                <div class="col-xs-12" style="padding-right:0 !important;">
                    <?= $this->render('parts_profit_declaration/viewStatus', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                    ]); ?>
                </div>
            </div>
            <div id="buttons-bar-fixed">
                <div class="row action-buttons margin-no-icon" style="padding-right:30px">
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?= Html::a('Назад', Url::to(['all', 'period' => $declarationHelper->period->id]), [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ]); ?>
                        <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', Url::to(['all', 'period' => $declarationHelper->period->id]), [
                            'title' => 'Назад',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                        ]); ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?php echo Html::a('Печать', [
                            '/tax/declaration-osno/declaration-print',
                            'actionType' => 'print',
                            'id' => $model->id,
                            'filename' => $model->getPrintTitle(),
                            'org' => true,
                            'period' => $declarationHelper->period->id,
                        ], [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                            'target' => '_blank',
                        ]);
                        echo Html::a('<i class="fa fa-print fa-2x"></i>', [
                            '/tax/declaration-osno/declaration-print',
                            'actionType' => 'pdf',
                            'id' => $model->id,
                            'filename' => $model->getPrintTitle(),
                            'org' => true,
                            'period' => $declarationHelper->period->id,
                        ], [
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                            'target' => '_blank',
                            'title' => 'Печать',
                        ]);
                        ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <style>
                            .dropdown-menu-mini {
                                width: 100%;
                                min-width: 200px;
                                border-color: #4276a4 !important;
                            }

                            .dropdown-menu-mini a {
                                padding: 7px 0px;
                                text-align: center;
                            }
                        </style>
                        <span class="dropup">
                            <?= Html::a('Скачать', '#', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle dropdown-linkjs',
                                'data-toggle' => 'dropdown',
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle dropdown-linkjs',
                                'data-toggle' => 'dropdown'
                            ]); ?>
                            <?= Dropdown::widget([
                                'options' => [
                                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs'
                                ],
                                'items' => [
                                    [
                                        'label' => 'Скачать PDF файл',
                                        'encode' => false,
                                        'url' => [
                                            '/tax/declaration-osno/declaration-print',
                                            'actionType' => 'pdf',
                                            'id' => $model->id,
                                            'filename' => $model->getPdfFileName(),
                                            'org' => true,
                                            'period' => $declarationHelper->period->id,
                                        ],
                                        'linkOptions' => [
                                            'target' => '_blank',
                                        ]
                                    ],
                                ],
                            ]); ?>
                        </span>
                    </div>
                    <div class="button-bottom-page-lg button-bottom-page-lg-double col-sm-2 col-xs-2">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->registerJs('
    $(document).ready(function (e) {
        var $left = Math.ceil((+$(".dropdown-centerjs").width() - +$(".dropdown-linkjs:visible").width()) / 2);
        console.log($left);
        $(".dropdown-centerjs").css("left", "-" + $left + "px");
    });
'); ?>