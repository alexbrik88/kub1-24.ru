<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 18:10
 */

use common\components\helpers\Html;
use yii\bootstrap4\Dropdown;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use yii\data\ArrayDataProvider;
use common\components\grid\GridView;
use yii\helpers\Url;

/* @var DeclarationOsnoHelper $declarationHelper
 * @var ArrayDataProvider $dataProvider
 */

$this->title = 'Нулевая отчетность в налоговую и ПФР';

$canPrint = Yii::$app->user->can(\frontend\rbac\UserRole::ROLE_CHIEF);
?>
<?= $this->render('_style'); ?>
<div class="row">
    <div class="col-xs-12 step-by-step" style="margin-bottom: 10px;">
        <?= $this->render('_steps', ['step' => 3, 'company' => $company]); ?>
    </div>
    <div class="col-xs-12">
        <div class="" style="font-size:16px; font-weight: bold;">
            Нулевая отчетность за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $declarationHelper->period->label, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                    ]) ?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'encodeLabels' => false,
                        'items' => $declarationHelper->getPeriodDropdownItems(),
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portlet box darkblue" style="margin-top: 30px;">
    <div class="portlet-title">
        <div class="caption" style="padding-top: 10px;">Список отчетности</div>
        <div class="actions joint-operations pull-right"
             style="display:none; width: 220px;padding: 7px 0 0 0!important;">
            <?php if ($canPrint) : ?>
                <?= Html::a('<i class="fa fa-print"></i> Печать', [
                    'many-document-create',
                    'actionType' => 'pdf',
                    'multiple' => '',
                ], [
                    'class' => 'btn btn-default btn-sm multiple-print hidden-md hidden-sm hidden-xs',
                    'target' => '_blank',
                ]); ?>
                <?= Html::a('<i class="fa fa-print"></i>', [
                    'many-document-create',
                    'actionType' => 'pdf',
                    'multiple' => '',
                ], [
                    'class' => 'btn btn-default btn-sm multiple-print hidden-lg',
                    'target' => '_blank',
                ]); ?>
            <?php endif ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                    'id' => 'items-table',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'rowOptions' => function ($declaration, $key, $index, $grid) use ($declarationHelper) {
                    return [
                        'data' => ['key' => $declaration['type'].'_'.$declarationHelper->period->id.'_'.(int)$declaration['month']],
                    ];
                },
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center joint-operation-checkbox-td',
                        ],
                        'format' => 'raw',
                        'value' => function ($declaration) use ($declarationHelper) {
                            return Html::checkbox("declaration[]", false, [
                                'class' => 'joint-operation-checkbox',
                                'value' => $declaration['type'].'_'.$declarationHelper->period->id.'_'.(int)$declaration['month'],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Название отчетности',
                        'headerOptions' => [
                            'width' => '35%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) use ($declarationHelper) {
                            return Html::a($data['name'], Url::to([$data['action'], 'period' => $declarationHelper->period->id, 'month' => $data['month']]));
                        },
                    ],
                    [
                        'label' => 'Дата сдачи отчетности',
                        'headerOptions' => [
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) use ($declarationHelper) {
                            return '';
                        },
                    ],
                    [
                        'label' => 'Штраф за не сдачу отчетности',
                        'headerOptions' => [
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) use ($declarationHelper) {
                            return '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<div class="clearfix" style="padding-bottom:30px"></div>
<div id="buttons-bar-fixed">
    <div class="col-xs-12 pad0 buttons-block">
        <?= Html::a('Назад', ['params'], [
            'class' => 'btn darkblue',
            'style' => 'min-width: 180px;'
        ]) ?>
    </div>
</div>