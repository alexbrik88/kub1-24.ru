<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\Banking;

$tariffItems = [];
$tariffArray = SubscribeTariff::find()->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING,
])->orderBy(['price' => SORT_ASC])->all();
foreach ($tariffArray as $key => $tariff) {
    $tariffItems[] = [
        'label' => $tariff->getTariffName(),
        'url' => '#',
        'linkOptions' => [
            'class' => 'choose-tariff',
            'data' => [
                'tariff-id' => $tariff->id,
            ],
        ],
    ];
}
$baseTariff = reset($tariffArray);
$model = new PaymentForm($company, ['tariffId' => $baseTariff->id]);

//$bankName = ($account && $account->bank) ? $account->bank->name : null;
//$bankUrl = ($account && $account->sysBank) ? $account->sysBank->url : null;
//$clientBankHtml = 'клиент-банк';
//if ($bankUrl) {
//    $clientBankHtml = Html::a('клиент-банк', $bankUrl, ['target' => '_blank']);
//}
?>

<style>
    .taxrobot-pay-panel {
        position: fixed;
        top: 0;
        right: 0;
        display: none;
        -moz-border-radius-topright: 20px;
        -webkit-border-top-right-radius: 20px;
        -moz-border-radius-bottomright: 20px;
        -webkit-border-bottom-right-radius: 20px;
        width: 400px;
        height: 100%;
        filter: alpha(opacity=85);
        background-color: #ffffff;
    }
    .taxrobot-pay-panel {
        z-index: 10051;
    }
    .taxrobot-pay-panel {
        border-left: 1px solid #e4e4e4;
    }
    .taxrobot-pay-panel .main-block {
        padding: 20px 30px 10px 30px;
        position: absolute;
        width: 100%;
        height: 95%;
        overflow-y: scroll;
    }
    .taxrobot-pay-panel .header {
        text-align: left;
        font-size: 21px;
        font-weight: 700;
    }
    .taxrobot-pay-panel .side-panel-close {
        position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        opacity: 1;
    }
    .tariff-price > del {
        font-size:24px;
        color: red;
    }
</style>

<div class="page-shading-panel taxrobot-pay-panel-close hidden"></div>
<div class="taxrobot-pay-panel">
    <div class="main-block" style="height: 100%;">
        <div class="header">
            <button type="button" class="close side-panel-close taxrobot-pay-panel-close" aria-hidden="true">×&times;</button>
            <table style="width: 100%; height: ">
                <tr>
                    <td style="width: 10%; vertical-align: middle; text-align: center;">
                        <?= Html::tag('div', '<img src="/img/icons/tariff_4.png" style="width: 40px; height: 40px;">', [
                            'style' => '
                                display: inline-block;
                                padding: 10px;
                                background-color: #4276a4;
                                border-radius: 30px!important;
                            ',
                        ]) ?>
                    </td>
                    <td style="padding-left: 10px; font-size: 18px;">
                        Нулевая отчетность ООО на ОСНО
                    </td>
                </tr>
            </table>
        </div>

        <div class="part-pay">
            <div style="padding-top: 30px;">
                <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                    'class' => 'tariff-group-payment-form',
                ]) ?>
                    <?= Html::activeHiddenInput($model, 'tariffId', [
                        'class' => 'tariff-id-input',
                    ]) ?>
                    <div class="text-bold">
                        <div style="display: inline-block; font-size: 18px;">Оплата за </div>
                        <div style="display: inline-block; width: 120px; font-size: 18px;">
                            <div class="dropdown">
                                <?= Html::tag('div', $tariffItems[0]['label'], [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                ])?>
                                <?= \yii\bootstrap\Dropdown::widget([
                                    'id' => 'employee-rating-dropdown',
                                    'encodeLabels' => false,
                                    'items' => $tariffItems,
                                ])?>
                            </div>
                        </div>
                        <?php foreach ($tariffArray as $tariff) : ?>
                            <?php
                            $discount = $company->getMaxDiscount($tariff->id);
                            $discVal = $discount ? $discount->value : 0;
                            $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                            ?>
                            <?= Html::beginTag('div', [
                                'class' => "tariff-price tariff-price-{$tariff->id}" .
                                   ($baseTariff->id != $tariff->id ? ' hidden' : ''),
                            ]) ?>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                            <?= Html::endTag('div') ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="mar-t-20">
                        <p style="font-weight:bold;font-size:18px">Вы получите:</p>
                        <p>Автоматическая подготовка отчетности в налоговую.</p>
                        <p>Автоматическая подготовка отчетности по сотрудникам.</p>
                        <p>Подготовка описи.</p>
                        <p>Напоминания о сроках отчетности.</p>
                        <p style="font-weight:bold;">Тариф "Выставление счетов" не входит в стоимость тарифа "Нулевая отчетность ООО на ОСНО".</p>
                    </div>

                    <div id="js-form-alert" class="mar-t-20"></div>

                    <div class="row mar-t-20">
                        <div class="col-xs-6">
                            <?= Html::submitButton('Картой', [
                                'class' => 'btn darkblue text-white width100 submit',
                                'name' => Html::getInputName($model, 'paymentTypeId'),
                                'value' => PaymentType::TYPE_ONLINE,
                                'style' => 'background: #45b6af;',
                                'data-style' => 'zoom-in',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <?= Html::submitButton('Выставить счет', [
                                'class' => 'btn darkblue text-white width100 submit',
                                'name' => Html::getInputName($model, 'paymentTypeId'),
                                'value' => PaymentType::TYPE_INVOICE,
                                'style' => 'background: #ffaa24;',
                                'data-style' => 'zoom-in',
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-submit-result hidden"></div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>

</div>


<?php

$this->registerJs(<<<JS
    var taxrobotPayPanelOpen = function() {

        $(".taxrobot-pay-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".taxrobot-pay-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".taxrobot-pay-panel .main-block").scrollTop(0);
    }

    var taxrobotPayPanelClose = function() {
        $(".taxrobot-pay-panel-trigger").removeClass("active");
        $(".taxrobot-pay-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
    }

    $(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
        e.preventDefault();
        taxrobotPayPanelOpen();
    });

    $(document).on("click", ".taxrobot-pay-panel-close", function () {
        taxrobotPayPanelClose();
    });

    $(document).on("click", ".tariff-group-payment-form .choose-tariff", function(e) {
        e.preventDefault();
        var tariff = $(this);
        var form = tariff.closest('form');
        var tariffId = tariff.data('tariff-id');
        $('#paymentform-tariffid', form).val(tariffId);
        tariff.closest('.dropdown').removeClass('open').find('.dropdown-toggle').html(tariff.html());
        $('.tariff-price', form).addClass('hidden');
        $('.tariff-price-'+tariffId, form).removeClass('hidden');

        var per_month = $('.tariff-price-per-month');
        $('.tariff-price', per_month).addClass('hidden');
        $('.tariff-price-'+tariffId, per_month).removeClass('hidden');
    });

    $(document).on('click', '.tariff-group-payment-form .submit', function (e) {
        e.preventDefault();
        var button = this;
        var form = $(this).closest('form');
        var formData = form.serializeArray();
        formData.push({ name: button.name, value: button.value });
        $(button).addClass('ladda-button');
        var l = Ladda.create(button);
        l.start();
        $.post($(form).attr('action'), formData, function(data) {
            Ladda.stopAll();
            l.remove();
            $(button).removeClass('ladda-button');
            if (data.content) {
                $('.form-submit-result', form).html(data.content);
            }
            if (data.alert && typeof data.alert === 'object') {
                for (alertType in data.alert) {
                    var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                    $.alert('#js-form-alert', data.alert[alertType], {type: alertType, topOffset: offset});
                }
            }
            if (data.done) {
                if (data.link) {
                    var link=document.createElement("a");
                    link.id = 'document-print-link';
                    link.href = data.link;
                    link.target = '_blank';
                    $('.form-submit-result', form).html(link);
                    document.getElementById('document-print-link').click();
                }
            }
        });
    });
JS
);

if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    taxrobotPayPanelOpen();
');
}