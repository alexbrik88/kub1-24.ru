<?php

use yii\helpers\Html;
use yii\helpers\Url;

$i = 1
?>
<?php foreach ($additionalAccounts as $additionalAccount) : ?>
    <?php $i++; ?>

    <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
        <span class="caption"> Расчетный счет №<?=$i?></span>
    </div>

    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?= $form->field($additionalAccount, 'bik')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control input-sm dictionary-bik' . ($additionalAccount->bik ? ' edited' : ''),
                    'data' => [
                        'url' => Url::to(['/dictionary/bik']),
                        'target-name' => '#' . Html::getInputId($additionalAccount, 'bank_name'),
                        'target-city' => '#' . Html::getInputId($additionalAccount, 'bank_city'),
                        'target-ks' => '#' . Html::getInputId($additionalAccount, 'ks'),
                        'target-collapse' => '#company-bank-block',
                    ]
                ])->label('БИК вашего банка') ?>

            </div>
            <div class="col-xs-12 col-md-6">
                <?= $form->field($additionalAccount, 'rs')->textInput(['maxlength' => true]); ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?= $form->field($additionalAccount, 'bank_name')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]); ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?= $form->field($additionalAccount, 'ks')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?= $form->field($additionalAccount, 'bank_city')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]); ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

<?php endforeach; ?>

<?php foreach ($newAccounts as $newAccount) : ?>
    <?php $i++; ?>

    <div class="new-account" style="display: none">
        <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
            <span class="marg pad10-b font-bold" style="font-size:16px"> Расчетный счет №<?=$i?></span>
        </div>

        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($newAccount, 'bik')->textInput([
                        'maxlength' => true,
                        'class' => 'form-control input-sm dictionary-bik' . ($newAccount->bik ? ' edited' : ''),
                        'data' => [
                            'url' => Url::to(['/dictionary/bik']),
                            'target-name' => '#' . Html::getInputId($newAccount, 'bank_name'),
                            'target-city' => '#' . Html::getInputId($newAccount, 'bank_city'),
                            'target-ks' => '#' . Html::getInputId($newAccount, 'ks'),
                            'target-collapse' => '#company-bank-block',
                        ]
                    ])->label('БИК вашего банка') ?>

                </div>
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($newAccount, 'rs')->textInput(['maxlength' => true]); ?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($newAccount, 'bank_name')->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                </div>
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($newAccount, 'ks')->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($newAccount, 'bank_city')->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

<?php endforeach; ?>

<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="col-md-6 pad0">
                <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить расчетный счет', [
                    'class' => 'btn darkblue darkblue-invert',
                    'id' => 'add-new-account',
                    'style' => 'margin-top:10px'
                ]); ?>
            </div>
            <div class="col-md-6">

            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            &nbsp;
        </div>
    </div>
</div>
<div class="clearfix" style="margin-bottom: 10px;"></div>

<?php
$this->registerJs(<<<JS

    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('.show-first-account-number').show();
        $('.new-account:hidden').first().show(250);
    });

JS
);

?>

<style>
    .show-first-account-number{<?=(empty($additionalAccounts)) ? 'display:none':''?>}
</style>
