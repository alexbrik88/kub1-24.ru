<?php

use common\components\ImageHelper;
use common\models\bank\Bank;
use common\models\cash\CashFlowsBase;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $canClose boolean */

$p = Banking::currentRouteEncode();
$accountArray = $company->getCheckingAccountants()->active()->all();
$bankingItems = [];
foreach ($accountArray as $account) {
    if (!isset($bankingItems[$account->bik]) &&
        ($class = Banking::classByBik($account->bik)) !== null &&
        ($logoPath = Bank::getLogoPath($class::BIK)) !== null
    ) {
        $bankingItems[$account->bik] = [
            'logo' => ImageHelper::getThumb($logoPath, [225, 126], [
                'style' => 'max-width: 100%; max-height: 100%;',
            ]),
            'url' => [
                '/cash/banking/'.$class::ALIAS.'/default/index',
                'account_id' => $account->id,
                'p' => $p,
            ],
        ];
    }
}
$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();
?>

<style type="text/css">
#add-income-modal-wrap {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 9996;

}
#add-income-background {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #333 !important;
    opacity: 0.7;
    filter: alpha(opacity=70);
    z-index: 9997;
}
#add-income-modal {
    position: relative;
    width: 80%;
    max-width: 900px;
    max-height: 80%;
    margin: 50px auto;
    padding: 20px;
    opacity: 1;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 0.3rem;
    z-index: 9998;
}
#add-income-modal .close {
    padding: 1rem 1rem;
    margin: -1rem -1rem -1rem auto;
    position: absolute;
    top: 0;
    right: -35px;
    font-size: 20px;
    color: #fff;
    opacity: 1 !important;
}
.add-income-content {
}
#add-income-modal .add-income-header {
    padding: 0 10px 10px;
    border-bottom: 1px solid #ddd;
    font-size: 20px;
    text-align: center;
}
#add-income-modal .add-income-content .col-header {
    display: inline-block;
    position: relative;
    padding: 10px 0;
    font-weight: bold;
    font-size: 12px;
}
#add-income-modal .add-income-content .col-header .help-ico {
    position: absolute;
    top: 5px;
    right: -27px;
    color: #bbc1c7;
    font-size: 16px;
    cursor: pointer;
}
#add-income-modal .add-income-item-wrap {
    min-height: 115px;
}
#add-income-modal .add-income-item {
    position: relative;
    width: 100%;
    padding-top: 20%;
}
#add-income-modal .add-income-item-wrap .item-comment {
    font-size: 12px;
}
#add-income-modal .add-income-btn {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    text-align: center;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    border: 1px solid #ddd;
}
#add-income-modal .add-income-btn:hover{
    text-decoration: none;
}
</style>

<?php Modal::begin([
    'id' => 'add-income-modal',
    'title' => 'Для расчета налогов и заполнения декларации укажите ДОХОД по ИП одним из следующих способов:',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<?php Modal::end(); ?>

<div id="add-income-modal">
    <div class="add-income-content">
        <div class="row">
            <div class="col-sm-4">
                <div class="col-header">
                    ЗАГРУЗИТЬ ВЫПИСКУ из БАНКА
                    <span class="help-ico tooltip-add-income" data-tooltip-content="#tooltip-content-1">
                        <?= Icon::get('question') ?>
                    </span>
                </div>
                <?php foreach ($bankingItems as $item) : ?>
                    <div class="add-income-item-wrap">
                        <div class="add-income-item">
                            <?= Html::a($item['logo'], $item['url'], [
                                'class' => 'add-income-btn banking-module-open-link',
                                'data' => [
                                    'pjax' => '0',
                                ],
                            ]) ?>
                        </div>
                        <div class="item-comment">
                            <strong>Загрузка с помощью прямой интеграции.</strong>
                            Выписка загрузиться автоматически
                        </div>
                    </div>
                <?php endforeach ?>
                <div class="add-income-item-wrap">
                    <div class="add-income-item">
                        <?= Html::a(Html::img('/img/banking.png', ['style' => 'height: 80%']), [
                            "/cash/banking/default/index",
                            'p' => $p,
                        ], [
                            'class' => 'add-income-btn banking-module-open-link',
                            'style' => 'background-color: #70ced5;',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]) ?>
                    </div>
                    <div class="item-comment">
                        <strong>Загрузка выписки из файла.</strong>
                        В клиент-банке выбираете период выгрузки выписки с 01.01.2019 по 31.12.2019
                        и скачайте файл в формате для 1С.
                        Скаченный файл загрузите тут
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-header">
                    ДОБАВИТЬ доход ВРУЧНУЮ
                    <span class="help-ico tooltip-add-income" data-tooltip-content="#tooltip-content-2">
                        <?= Icon::get('question') ?>
                    </span>
                </div>
                <div class="add-income-item-wrap">
                    <div class="add-income-item">
                        <?= Html::a('Добавить вручную<br>ДОХОД ПО БАНКУ', [
                            'create-movement',
                            'type' => TaxRobotHelper::TYPE_BANK,
                            'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                            'redirect' => Url::to(Banking::currentRoute()),
                            'skipDate' => 1,
                            'canAddContractor' => 1,
                            'canAddAccount' => 1
                        ], [
                            'class' => 'add-income-btn update-movement-link update-bank-movement-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]) ?>
                    </div>
                </div>
                <?php if ($cashbox !== null) : ?>
                    <div class="add-income-item-wrap">
                        <div class="add-income-item">
                            <?= Html::a('Добавить вручную<br>ДОХОД ПО КАССЕ', [
                                'create-movement',
                                'type' => TaxRobotHelper::TYPE_ORDER,
                                'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                                'redirect' => Url::to(Banking::currentRoute()),
                                'skipDate' => 1,
                                'canAddContractor' => 1,
                                'id' => $cashbox->id,
                            ], [
                                'class' => 'add-income-btn update-movement-link update-order-movement-link',
                                'data' => [
                                    'pjax' => '0',
                                ],
                            ]) ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-sm-4">
                <div class="col-header">
                    ЗАГРУЗИТЬ ДАННЫЕ из ОФД
                    <span class="help-ico tooltip-add-income" data-tooltip-content="#tooltip-content-3">
                        <?= Icon::get('question') ?>
                    </span>
                </div>
                <div class="add-income-item-wrap">
                    <div class="add-income-item">
                        <?= Html::a('Загрузить данные<br>из ОФД', [
                            '/cash/ofd/default/index',
                            'p' => $p,
                        ], [
                            'class' => 'add-income-btn ofd-module-open-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden">
        <span id="tooltip-content-1">
            <strong style="color: red;">Лучший вариант!</strong>
            <br>
            Программа учтет помимо дохода,
            <br>
            все платежи по налогам и взносам,
            <br>
            которые были уплачены в течение года.
            <br>
            Это поможет более точно рассчитать
            <br>
            итоговые налоги и не заплатить лишнее.
        </span>
        <span id="tooltip-content-2">
            Можно добавить каждую операцию по
            <br>
            доходу за 2019 год или указать суммарно
            <br>
            поквартально или одну сумму за год.
            <br>
            Удобный способ, но так же придется вручную
            <br>
            внести все платежи по налогам за данный период,
            <br>
            что бы расчет налогов был точным.
        </span>
        <span id="tooltip-content-3">
            Если у вас вся выручка по кассе,
            <br>
            то загрузите выписку из вашего ОФД.
        </span>
    </div>
</div>



<?= BankingModalWidget::widget() ?>

<?= OfdModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to(Banking::currentRoute()),
]) ?>
