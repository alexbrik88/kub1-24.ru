<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap4\ActiveForm */

$this->title = 'Контакты покупателя';
$this->params['showHeader'] = true;
?>

<div class="out-invoice-form" style="width: 600px; padding-top: 25px;">

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'id' => (empty($isDemo)) ? $model->outInvoice->id : $model->outInvoice->demo_id,
            'uid' => $model->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?php if ($model->outInvoice->is_outer_shopcart) : ?>
        <?= $this->render('_orders1', [
            'form' => $form,
            'model' => $model,
        ]) ?>
    <?php else : ?>
        <?= $this->render('_orders2', [
            'form' => $form,
            'model' => $model,
        ]) ?>
    <?php endif ?>

    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'id' => 'legal-director_phone',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>
    <?= $form->field($model, 'comment')->textarea(['maxlength' => true]); ?>

    <?= Html::activeHiddenInput($model, 'legal_type') ?>
    <?= Html::activeHiddenInput($model, 'legal_name') ?>
    <?= Html::activeHiddenInput($model, 'legal_inn') ?>
    <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
    <?= Html::activeHiddenInput($model, 'legal_address') ?>
    <?= Html::activeHiddenInput($model, 'legal_rs') ?>
    <?= Html::activeHiddenInput($model, 'legal_bik') ?>
    <?= Html::activeHiddenInput($model, 'chief_position') ?>
    <?= Html::activeHiddenInput($model, 'chief_name') ?>
    <?= Html::activeHiddenInput($model, 'currency') ?>

    <div class="mt-3 d-flex justify-content-between">
        <?php if (!$model->outInvoice->is_outer_shopcart && $model->scenario != OutInvoiceForm::SCENARIO_FIXPRODUCT) : ?>
        <?= Html::submitButton('Назад', [
            'class' => 'button-regular button-hover-transparent width-160 go-back-btn',
            'name' => 'back',
            'value' => 'back',
        ]) ?>
        <?php endif; ?>
        <?= Html::submitButton('Продолжить', [
            'class' => 'button-regular button-hover-transparent width-160 ml-auto',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
