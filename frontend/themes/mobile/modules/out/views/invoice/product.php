<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $isDemo bool */

$this->title = 'Выберите товар/услугу';
$this->params['showHeader'] = true;

$totalCount = array_sum($model->product);
?>

<div class="out-invoice-form" style="width: 600px; padding-top: 25px;">

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'id' => (empty($isDemo)) ? $model->outInvoice->id : $model->outInvoice->demo_id,
            'uid' => $model->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $form->field($model, 'product', [
        'parts' => [
            '{input}' => $this->render('_product_table', ['model' => $model]),
        ]
    ])->label(false)->render(); ?>

    <?= Html::activeHiddenInput($model, 'email') ?>
    <?= Html::activeHiddenInput($model, 'phone') ?>
    <?= Html::activeHiddenInput($model, 'comment') ?>
    <?= Html::activeHiddenInput($model, 'legal_type') ?>
    <?= Html::activeHiddenInput($model, 'legal_name') ?>
    <?= Html::activeHiddenInput($model, 'legal_inn') ?>
    <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
    <?= Html::activeHiddenInput($model, 'legal_address') ?>
    <?= Html::activeHiddenInput($model, 'legal_rs') ?>
    <?= Html::activeHiddenInput($model, 'legal_bik') ?>
    <?= Html::activeHiddenInput($model, 'chief_position') ?>
    <?= Html::activeHiddenInput($model, 'chief_name') ?>
    <?= Html::activeHiddenInput($model, 'currency') ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton($totalCount > 0 ? 'Продолжить' : 'Выберите количество', [
            'id' => 'outinvoice-submit',
            'class' => 'button-regular button-hover-transparent width-160 ml-auto',
            'disabled' => $totalCount > 0 ? false : true,
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
