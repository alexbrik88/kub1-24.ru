<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap4\ActiveForm */

$this->title = 'Контакты покупателя';
$this->params['showHeader'] = true;
?>

<div class="out-invoice-form" style="width: 600px; padding-top: 25px;">

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'id' => $model->priceList->id,
            'uid' => $model->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $this->render('_orders', [
        'form' => $form,
        'model' => $model,
    ]) ?>

    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'id' => 'legal-director_phone',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>
    <?= $form->field($model, 'comment')->textarea(['maxlength' => true]); ?>

    <?= Html::activeHiddenInput($model, 'legal_type') ?>
    <?= Html::activeHiddenInput($model, 'legal_name') ?>
    <?= Html::activeHiddenInput($model, 'legal_inn') ?>
    <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
    <?= Html::activeHiddenInput($model, 'legal_address') ?>
    <?= Html::activeHiddenInput($model, 'legal_rs') ?>
    <?= Html::activeHiddenInput($model, 'legal_bik') ?>
    <?= Html::activeHiddenInput($model, 'chief_position') ?>
    <?= Html::activeHiddenInput($model, 'chief_name') ?>

    <div class="form-group row action-battons">
        <div class="col-xs-6">
            <?php if ($model->scenario != OutInvoiceForm::SCENARIO_FIXPRODUCT) : ?>
            <?= Html::submitButton('Назад', [
                'class' => 'btn go-back-btn',
                'name' => 'back',
                'value' => 'back',
            ]) ?>
            <?php endif; ?>
        </div>
        <div class="col-xs-6">
            <?= Html::submitButton('Продолжить', [
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; min-width: 120px; text-align: center',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
