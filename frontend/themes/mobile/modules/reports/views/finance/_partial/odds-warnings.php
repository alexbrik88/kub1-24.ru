<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.06.2018
 * Time: 18:14
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $warnings []
 */
?>

<div class="wrap pt-4 pl-4 pr-4 pb-0">
    <div class="pt-2 pr-2 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">Предупреждения</h4>
            </div>
            <div class="column ml-auto">
                <ul class="pagination list-clr">
                    <li class="active pr-1 mb-2">
                        <a class="button-regular pl-3 pr-3 mr-2" href="#">1-<?= count($warnings); ?></a>
                    <!--<li class="pr-1 mb-2"><a class="button-regular pl-3 pr-3 mr-2" href="#">5-8</a></li>
                    <li class="pr-1 mb-2"><a class="button-regular pl-3 pr-3 mr-2" href="#">9-12</a></li>-->
                    </li>
                </ul>
            </div>
        </div>
        <hr class="devider mb-0 mt-1 border-color-light-grey">
        <div class="row">
            <?php $number = 1; ?>
            <?php foreach ($warnings as $key => $data): ?>
                <div class="column warning-box pt-2 pb-4 col-4">
                    <div class="pb-2">
                        <div class="button-regular button-regular_red pt-1 pb-1 pl-2 pr-2 mb-2 mt-1"><?= $number; ?></div>
                        <div class="text-dark-alternative line-height-1-38">
                            <?php if ($key == 1): ?>
                                Остаток по движению денег не может быть отрицательным!<br>
                                У вас остаток на конец <?= $warnings[$key]['month'] ?>
                                <span style="color: red;">
                                -<?= TextHelper::invoiceMoneyFormat($warnings[$key]['amount'], 2); ?> <i class="fa fa-rub"></i>
                                <?= $warnings[$key]['moreThenOne'] ? 'и есть еще месяца с отрицательным остатком.' : '.'; ?><br>
                                Возможно у вас:
                                <ul style="padding-left: 17px;">
                                    <li>
                                        в кассе или в банке не внесены Начальные остатки;
                                    </li>
                                    <li>
                                        не отмечены все приходы.
                                    </li>
                                </ul>
                            <?php elseif ($key == 2): ?>
                                Просроченная задолженность покупателей составляет
                                <?= TextHelper::invoiceMoneyFormat($warnings[$key]['totalDebtSum'], 2); ?> <i class="fa fa-rub"></i><br>
                                Если вы соберете хотя бы 50% долгов уже в
                                <?= FlowOfFundsReportSearch::$monthIn[date('m')]; ?>, то приход по операционной
                                деятельности увеличится на <?= $warnings[$key]['incomePercent']; ?>%
                            <?php elseif ($key == 3): ?>
                                У вас есть неоплаченные счета от поставщиков на сумму
                                <?= TextHelper::invoiceMoneyFormat($warnings[$key]['inInvoiceDebtSum'], 2); ?> <i class="fa fa-rub"></i>
                                <br>
                                <?= $warnings[$key]['message']; ?>
                            <?php elseif ($key == 4): ?>
                                <?= $warnings[$key]['message']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php $number++; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>