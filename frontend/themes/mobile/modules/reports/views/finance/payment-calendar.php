<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.02.2019
 * Time: 23:27
 */

use frontend\modules\reports\models\ExpensesSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use common\components\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\reports\models\PlanCashFlows;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $hasFlows bool
 */

$this->title = 'Платежный календарь';
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltip_pay-odds'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <button class="button-list button-hover-transparent button-clr mb-2 collapsed" type="button" data-toggle="collapse" href="#helpCollapse" aria-expanded="false" aria-controls="helpCollapse">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use>
                    </svg>
                </button>
            </div>
            <div class="column pl-1 pr-2">
                <button class="button-list button-list_collapse button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse" aria-expanded="false" aria-controls="chartCollapse">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use>
                    </svg>
                </button>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['payment-calendar', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
        <div class="collapse" id="helpCollapse">
            <div class="pt-4 pb-3">
                <?php
                echo $this->render('_partial/payment-calendar-panel')
                ?>
            </div>
        </div>
        <div class="collapse show" id="chartCollapse">
            <div class="pt-4 pb-3">
                <?php
                // Charts
                echo $this->render('_charts/plan', [
                    'model' => $searchModel,
                    'planGrowingBalance' => $data['growingBalance'],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="d-flex flex-nowrap">
        <div class="radio_join mb-2">

            <?= \yii\bootstrap\Html::a('По видам деятельности',
                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY, 'PaymentCalendarSearch' => ['year' => $searchModel->year]]), [
                    'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? ' button-regular_red' : null),
                ]); ?>

        </div>
        <div class="radio_join mb-2">

            <?= Html::a('По кошелькам',
                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_PURSE, 'PaymentCalendarSearch' => ['year' => $searchModel->year]]), [
                    'class' => 'button-regular pl-5 pr-5 mb-0' . ($activeTab == PaymentCalendarSearch::TAB_BY_PURSE ? ' button-regular_red' : null),
                ]); ?>

        </div>
    </div>
    <?php if (!$searchModel->company->isFreeTariff): ?>
        <div class="column pl-1">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#exel"></use></svg>',
                Url::to(['/reports/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr ml-2 mb-2',
                ]); ?>
        </div>
    <?php endif; ?>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?php if ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY): ?>
        <?= $this->render('_partial/payment_calendar_by_activity', [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'data' => $data,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'hasFlows' => $hasFlows,
        ]); ?>
    <?php elseif ($activeTab == PaymentCalendarSearch::TAB_BY_PURSE): ?>
        <?= $this->render('_partial/payment_calendar_by_purse', [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'data' => $data,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'hasFlows' => $hasFlows,
        ]); ?>
    <?php endif; ?>
</div>

<?php /* OLD
    <div class="portlet box odds-report-header">
        <div class="btn-group pull-right title-buttons">
            <div class="portlet-body employee-wrapper">
                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                        <li class="<?= $activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? 'active' : null; ?>">
                            <?= Html::a('ПК по видам деятельности',
                                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY, 'PaymentCalendarSearch' => [
                                    'year' => $searchModel->year,
                                ]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                        <li class="<?= $activeTab == PaymentCalendarSearch::TAB_BY_PURSE ? 'active' : null; ?>"
                            style="margin-right: 0;float: right;">
                            <?= Html::a('ПК по кошелькам',
                                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_PURSE, 'PaymentCalendarSearch' => [
                                    'year' => $searchModel->year,
                                ]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <h4 class="page-title">
            <?= $this->title; ?>
            <img src="/img/open-book.svg" class="payment_calendar-panel-trigger">
        </h4>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade in active">
            <?php if ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY): ?>
                <?= $this->render('_partial/payment_calendar_by_activity', [
                    'searchModel' => $searchModel,
                    'activeTab' => $activeTab,
                    'data' => $data,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                    'hasFlows' => $hasFlows,
                ]); ?>
            <?php elseif ($activeTab == PaymentCalendarSearch::TAB_BY_PURSE): ?>
                <?= $this->render('_partial/payment_calendar_by_purse', [
                    'searchModel' => $searchModel,
                    'activeTab' => $activeTab,
                    'data' => $data,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                    'hasFlows' => $hasFlows,
                ]); ?>
            <?php endif; ?>
            <?= Html::hiddenInput('activeTab', $activeTab, [
                'id' => 'active-tab_report',
            ]); ?>
        </div>
    </div>
*/ ?>
<?= $this->render('_partial/plan_item_table', [
    'searchModel' => $searchModel,
    'itemsDataProvider' => $itemsDataProvider,
    'activeTab' => $activeTab,
]); ?>
<?php /* echo $this->render('_partial/payment-calendar-panel'); */ ?>
<?php /* echo $this->render('_partial/plan_item_panel'); */ ?>

<div class="modal fade" id="save-repeated-flows" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="font-size: 16px;">
                    Изменения сохранить для
                </div>
                <?= Html::radioList('update-repeated-operations', PlanCashFlows::UPDATE_REPEATED_ONLY_ONE, PlanCashFlows::$updateRepeatedOperations, [
                    'class' => 'col-md-12',
                    'id' => 'update-repeated-operations',
                    'style' => 'margin-top: 20px;',
                ]); ?>
                <div class="form-actions col-md-12" style="margin-top: 20px;">
                    <div class="row action-buttons">
                        <div class="col-sm-5 col-xs-5">
                            <?= Html::button('Сохранить', [
                                'class' => 'btn darkblue text-white widthe-100 update-repeated-operations-button',
                            ]); ?>
                        </div>
                        <div class="col-sm-2 col-xs-2"></div>
                        <div class="col-sm-5 col-xs-5">
                            <?= Html::button('Отменить', [
                                'class' => 'btn darkblue text-white pull-right widthe-100',
                                'data-dismiss' => 'modal',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>

<?php if (Yii::$app->session->remove('showAutoPlan')): ?>
    <?php $this->registerJs('
        $(document).ready(function (e) {
            $(".auto-plan-items").click();
        });
    '); ?>
<?php endif; ?>

<?php $this->registerJs('
    $(".payment_calendar-panel-trigger").click(function (e) {
        $(".payment_calendar-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        if ($panel.find(".plan_item-form").length > 0) {
            $panel.find(".plan_item-form").empty();
        }
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        if ($panel.find(".plan_item-form").length > 0) {
            $panel.find(".plan_item-form").empty();
        }
        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });

    $(".add-item-payment-calendar").click(function (e) {
        var $panel = $(".plan_item-panel");

        $panel.find(".plan_item-header").text("Добавить плановую операцию");
        $.post($(this).data("url"), null, function (data) {
            $panel.find(".plan_item-form").append(data);
            $panel.toggle("fast");
            $("#visible-right-menu").show();
            $("html").attr("style", "overflow: hidden;");
        });

        return false;
    });

    $(document).on("click", ".update-item-payment-calendar", function (e) {
        showUpdatePanel($(this).data("url"));
    });

    $(document).on("click", ".plan-item-table tbody td:not(.action-line, .joint-operation-checkbox-td)", function (e) {
        showUpdatePanel($(this).closest("tr").find(".update-item-payment-calendar").data("url"));
    });

    function showUpdatePanel($url) {
        var $panel = $(".plan_item-panel");

        $panel.find(".plan_item-header").text("Изменить плановую операцию");
        $.post($url, null, function (data) {
            $panel.find(".plan_item-form").append(data);
            $panel.toggle("fast");
            $("#visible-right-menu").show();
            $("html").attr("style", "overflow: hidden;");
        });

        return false;
    }

    $(".auto-plan-items").click(function (e) {
        var $autoPlanBlock = $(".auto-plan-block");
        var $reportTable = $(".flow-of-funds:not(.auto-plan-block), #paymentcalendarsearch-year, .add-item-payment-calendar, .download-odds-xls");

        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
            $autoPlanBlock.removeClass("hidden");
            $reportTable.addClass("hidden");
            $(".items-table-block").hide();
            $(this).text("Сохранить");
            $(this).closest("div").removeClass("col-md-10").addClass("col-md-8");
            $("#paymentcalendarsearch-year").closest("div").prepend("<span class=\"auto-plan-items_portlet-title\">Настройка АвтоПланирования</span>").removeClass("col-md-2").addClass("col-md-4");
            $(".undo-auto-plan-items").removeClass("hidden");
        } else {
            $("#save-auto-plan").modal();
        }
    });

    $(".auto-plan-block .submit-auto_plan").click(function (e) {
        $(".auto-plan-block").addClass("hidden");
        $(".flow-of-funds:not(.auto-plan-table), #paymentcalendarsearch-year, .add-item-payment-calendar, .download-odds-xls").removeClass("hidden");
        $(".auto-plan-items").removeClass("active").text("АвтоПланирование");
        $(".items-table-block").show();
        $(".auto-plan-items").closest("div").removeClass("col-md-8").addClass("col-md-10");
        $("#paymentcalendarsearch-year").closest("div").removeClass("col-md-4").addClass("col-md-2");
        $(".auto-plan-items_portlet-title").remove();
    });

    $(".update-repeated-operations-button").click(function (e) {
        $("#plancashflows-updaterepeatedtype").val($("#update-repeated-operations input:checked").val());
        $("#save-repeated-flows").modal("hide");
        $("#js-plan_cash_flow_form").submit();
    });
'); ?>