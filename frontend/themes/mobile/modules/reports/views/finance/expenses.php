<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:29
 */

use common\components\helpers\Html;
use frontend\modules\reports\models\ExpensesSearch;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 */

$this->title = 'Расходы';
?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2">Расходы</h4>
            </div>
            <div class="column pr-2">
                <button class="button-list button-hover-transparent button-clr mb-2 collapsed" type="button" data-toggle="collapse" href="#helpCollapse" aria-expanded="false" aria-controls="helpCollapse">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use>
                    </svg>
                </button>
            </div>
            <div class="column pl-1 pr-2">
                <button class="button-list button-list_collapse button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse" aria-expanded="false" aria-controls="chartCollapse">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use>
                    </svg>
                </button>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['expenses', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
        <div class="collapse" id="helpCollapse">
            <div class="pt-4 pb-3">
                <?php
                echo $this->render('_partial/expenses_panel')
                ?>
            </div>
        </div>
        <div class="collapse show" id="chartCollapse">
            <div class="pt-4 pb-3">
                <?php
                // Expense Charts
                $expenseSearchModel = new ExpensesSearch();
                echo $this->render('_charts/expenses', [
                    'searchModel' => $expenseSearchModel,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="d-flex flex-nowrap">
        <div class="radio_join mb-2">

            <?= Html::a('Расходы списком',
                Url::to(['expenses', 'activeTab' => ExpensesSearch::TAB_BY_ACTIVITY, 'ExpensesSearch' => ['year' => $searchModel->year]]), [
                    'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY ? ' button-regular_red' : null),
                ]); ?>

        </div>
        <div class="radio_join mb-2">

            <?= Html::a('Расходы по кошелькам',
                Url::to(['expenses', 'activeTab' => ExpensesSearch::TAB_BY_PURSE, 'ExpensesSearch' => ['year' => $searchModel->year]]), [
                    'class' => 'button-regular pl-5 pr-5 mb-0' . ($activeTab == ExpensesSearch::TAB_BY_PURSE ? ' button-regular_red' : null),
                ]); ?>

        </div>
    </div>
    <?php if (!$searchModel->company->isFreeTariff): ?>
        <div class="column pl-1">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#exel"></use></svg>',
                Url::to(['/reports/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr ml-2 mb-2',
                ]); ?>
        </div>
    <?php endif; ?>
    <!--<div class="d-flex flex-nowrap ml-auto">
        <div class="radio_join mb-2">
            <a class="button-regular pl-4 pr-4 mb-0" href="#">День</a>
        </div>
        <div class="radio_join mb-2">
            <a class="button-regular button-regular_red pl-3 pr-3 mb-0" href="#">Месяц</a>
        </div>
    </div>-->
</div>

<div class="wrap wrap_padding_none mb-2">
    <?php if ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY): ?>
        <?= $this->render('_partial/expenses_by_activity', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
        ]); ?>
    <?php elseif ($activeTab == ExpensesSearch::TAB_BY_PURSE): ?>
        <?= $this->render('_partial/expenses_by_purse', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
        ]); ?>
    <?php endif; ?>
    <?= Html::hiddenInput('activeTab', $activeTab, [
        'id' => 'active-tab_report',
    ]); ?>
</div>

<?php //echo $this->render('_partial/expenses_panel'); ?>
<?= $this->render('_partial/item_table', [
    'searchModel' => $searchModel,
]); ?>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<?php $this->registerJs('
     $(".expenses-panel-trigger").click(function (e) {
        $(".expenses-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });

    $(document).on("submit", "#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form", function (e) {
        $(this).prepend(\'<input type="hidden" name="fromOdds" value="1">\');
        $.ajax({
            "type": "post",
            "url": $(this).attr("action"),
            "data": $(this).serialize(),
            "success": function(data) {
                $(".odds-model-movement, #ajax-modal-box").modal("hide");
                location.href = location.href;
            }
        });

        return false;
    });
    $(document).on("shown.bs.modal", "#many-item", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
        var $header = $modal.find(".modal-header h1");
        var $additionalHeaderText = null;

        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        if ($includeExpenditureItem && $includeIncomeItem) {
            $additionalHeaderText = " прихода / расхода";
        } else if ($includeExpenditureItem) {
            $additionalHeaderText = " расхода";
        } else if ($includeIncomeItem) {
            $additionalHeaderText = " прихода";
        }
        $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
        $(".joint-operation-checkbox:checked").each(function() {
            $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
        });
    });
    $(document).on("hidden.bs.modal", "#many-item", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
        $(".additional-header-text").remove();
        $(".modal#many-item form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
    });
    $(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
        var l = Ladda.create($(this).find(".btn-save")[0]);
        var $hasError = false;

        l.start();
        $(".field-expensessearch-expenditureitemidmanyitem:visible").each(function () {
            $(this).removeClass("has-error");
            $(this).find(".help-block").text("");
            if ($(this).find("select").val() == "") {
                $hasError = true;
                $(this).addClass("has-error");
                $(this).find(".help-block").text("Необходимо заполнить.");
            }
        });
        if ($hasError) {
            return false;
        }
    });
'); ?>
