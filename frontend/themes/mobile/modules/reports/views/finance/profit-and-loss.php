<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2019
 * Time: 15:25
 */

use common\components\helpers\Html;
use frontend\modules\reports\models\PlanFactSearch;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $searchModel ProfitAndLossSearchModel
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $data array
 */

$this->title = "Отчет о Прибылях и Убытках для {$searchModel->company->companyTaxationType->getName()}";
?>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="d-flex flex-nowrap">
        <h4><?= $this->title; ?></h4>
    </div>
    <div class="column ml-auto mr-auto mb-2">
    </div>
    <div class="d-flex flex-nowrap ml-auto">
        <?= \yii\bootstrap\Html::beginForm(['profit-and-loss'], 'GET', [
            'validateOnChange' => true,
        ]); ?>
        <?= Select2::widget([
            'model' => $searchModel,
            'attribute' => 'year',
            'data' => $searchModel->getYearFilter(),
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]); ?>
        <?= Html::endForm(); ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?= $this->render('_partial/profit_and_loss_table', [
        'searchModel' => $searchModel,
        'currentMonthNumber' => $currentMonthNumber,
        'currentQuarter' => $currentQuarter,
        'data' => $data,
    ]); ?>
</div>

<?php // echo $this->render('_partial/profit_and_loss_panel'); ?>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>


<?php $this->registerJs('
     $(".profit-and-loss-panel-trigger").click(function (e) {
        $(".profit-and-loss-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); ?>