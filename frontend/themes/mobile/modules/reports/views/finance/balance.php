<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use kartik\select2\Select2;
use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use kartik\checkbox\CheckboxX;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\helpers\Month;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\reports\models\BalanceSearch */
/* @var $balance array */

$this->title = 'Баланс';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = date('Y') == $model->year;
$activeCheckbox = explode(',', Yii::$app->session->remove('activeCheckbox'));

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-balance',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$balanceFlat = [];
foreach ($balance as $row) {
    $balanceFlat[] = [
        'label' => $row['label'],
        'options' => $row['options'],
        'data' => $row['data']
    ];
    if (isset($row['items'])) {
        foreach ($row['items'] as $row2) {
            $balanceFlat[] = [
                'label' => $row2['label'],
                'options' => $row2['options'],
                'data' => $row2['data'],
                'addCheckboxX' => isset($row2['addCheckboxX']) ? $row2['addCheckboxX'] : false
            ];
            if (isset($row2['items'])) {
                foreach ($row2['items'] as $row3) {
                    $balanceFlat[] = [
                        'label' => $row3['label'],
                        'options' => $row3['options'],
                        'data' => $row3['data']
                    ];

                }
            }
        }
    }
}

?>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="d-flex flex-nowrap">
        <h4><?= $this->title; ?></h4>
    </div>
    <div class="column ml-auto mr-auto mb-2">
    </div>
    <div class="d-flex flex-nowrap ml-auto">
        <?= \yii\bootstrap\Html::beginForm(['balance'], 'GET', [
            'validateOnChange' => true,
        ]); ?>
        <?= Select2::widget([
            'model' => $model,
            'attribute' => 'year',
            'data' => $model->getYearFilter(),
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]); ?>
        <?= \common\components\helpers\Html::endForm(); ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            <table class="flow-of-funds table table-style table-count-list table-count-list_collapse_none mb-0">
                <thead>
                <tr class="quarters-flow-of-funds">
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                        <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1">Статьи</span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                </tr>
                <tr>
                    <?php foreach (ProfitAndLossSearchModel::$month as $key => $month): ?>
                        <?php $quarter = (int)ceil($key / 3); ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                            data-collapse-cell
                            data-id="<?= $cell_id[$quarter] ?>"
                            data-month="<?= $key; ?>"
                        >
                            <div class="pl-1 pr-1"><?= $month; ?></div>
                        </th>
                        <?php if ($key % 3 == 0): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                data-collapse-cell-total
                                data-id="<?= $cell_id[$quarter] ?>"
                            >
                                <div class="pl-1 pr-1">Итого</div>
                            </th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                </thead>

                <tbody>

                <?php $floorID = 0; ?>
                <?php foreach ($balanceFlat as $row): ?>
                    <?php
                    $currentQuarter = (int)ceil(date('m') / 3);
                    $suffix = ArrayHelper::remove($row, 'suffix');
                    $options = ArrayHelper::remove($row, 'options', []);
                    $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
                    $skipEmpty = \yii\helpers\ArrayHelper::remove($row, 'skipEmpty', false);
                    $canUpdate = ArrayHelper::remove($row, 'canUpdate', false);
                    if ($skipEmpty && array_sum($row['data']) === 0) {
                       continue;
                    }
                    $classBold = !$includedRow ? 'weight-700' : '';
                    ?>

                    <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']):
                        $floorID++; ?>

                        <tr class="not-drag expenditure_type sub-block">
                            <td class="pl-2 pr-2 pt-3 pb-3">
                                <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-row-trigger data-target="first-floor-<?= $floorID ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text_size_14 ml-1 <?= $classBold ?>"><?= $row['label']; ?></span>
                                </button>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $monthNumber = (int)$monthNumber;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($row['data'][$monthNumber]) ? $row['data'][$monthNumber] : 0;
                                ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                                <?php $quarterSum = isset($row['data']['quarter-' . $quarter]) ? $row['data']['quarter-' . $quarter] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>

                    <?php else: ?>
                        <tr class="item-block <?= ($includedRow) ? 'd-none' : '' ?>" <?= ($includedRow) ? 'data-id="first-floor-'.$floorID.'"' : '' ?>>
                            <td class="pl-3 pr-3 pt-3 pb-3">
                                <span class="<?= $classBold ?: 'text-grey' ?> text_size_14 ml-1 mr-5"><?= $row['label']; ?></span>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $monthNumber = (int)$monthNumber;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($row['data'][$monthNumber]) ? $row['data'][$monthNumber] : 0;
                                ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?: 'text-grey' ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                                <?php $quarterSum = isset($row['data']['quarter-' . $quarter]) ? $row['data']['quarter-' . $quarter] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?: 'text-grey' ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>

                    <?php endif; ?>



                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php /*
<!-- OLD -->
<div class="portlet box odds-report-header">
    <h4 class="page-title">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="balance-panel-trigger">
    </h4>
</div>
<div class="portlet box darkblue">
    <?= Html::beginForm(['balance',], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-12 pull-right serveces-search" style="padding-left: 5px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-2 p-l-0">
                        <?= Html::activeDropDownList($model, 'year', $model->getYearFilter(), [
                            'name' => 'year',
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $model->year,
                        ]); ?>
                    </div>
                    <div class="col-md-10" style="padding-right: 0; padding-left: 0">
                        <?= Html::a('<i class="fa fa-file-excel-o"></i> Скачать в Excel', '#', [
                            'class' => 'download-odds-xls pull-right',
                        ]); ?>
                        <?= Html::a('<img src="/img/icons/settings.png"> Инструкция', 'javascript:;', [
                            'class' => 'settings-balance-panel-trigger pull-right',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="balance-report scroll-table-wrapper" style="">
            <table class="table table-bordered balance-report-table" style="width: auto;">
                <thead>
                <tr class="heading quarters-flow-of-funds" role="row">
                    <th style="min-width: 270px;">
                        <?= CheckboxX::widget([
                            'id' => 'main-checkbox-side',
                            'name' => 'main-checkbox-side',
                            'value' => true,
                            'options' => [
                                'class' => 'main-checkbox-side',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Статьи
                    </th>
                    <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'first-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentQuarter == 1 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-1',
                                    'total-quarter-class' => 'quarter-1',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="first-quarter">
                            1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'second-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-2',
                                    'total-quarter-class' => 'quarter-2',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="second-quarter">
                            2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'third-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-3',
                                    'total-quarter-class' => 'quarter-3',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="third-quarter">
                            3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'fourth-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-4',
                                    'total-quarter-class' => 'quarter-4',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="fourth-quarter">
                            4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                        <?= $model->year; ?>
                    </th>
                </tr>
                <tr class="heading" role="row">
                    <th width="20%"></th>
                    <?php foreach (FlowOfFundsReportSearch::$month as $key => $month): ?>
                        <?php $quarter = ceil($key / 3); ?>
                        <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                            style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                            <?php if ($key == date('m') && $model->year == date('Y')): ?>
                                <?= date('d') . ' ' . Month::$monthGenitiveRU[$key]; ?>
                            <?php else: ?>
                                <?= $month; ?>
                            <?php endif; ?>
                        </th>
                        <?php if ($key % 3 == 0): ?>
                            <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                Итого
                            </th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <th class="text-center" width="10%">Итого</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($balance as $item) : ?>
                    <?= $model->renderRow($item) ?>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="update-balance-item-panel">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['update-balance-item',]),
        'id' => 'update-contractor-form',
    ]); ?>
    <?php foreach (Month::$monthShort as $monthNumber => $monthName): ?>
        <div class="month-block">
            <span class="name text-center" style="text-align: center;">
                <?= $monthName; ?> <?= substr($model->year, 2, 4); ?>
            </span>
            <?= Html::textInput("BalanceItemAmount[{$monthNumber}]", null, [
                'class' => 'js_input_to_money_format amount-input',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
    <?php endforeach; ?>
    <?= Html::hiddenInput('itemID', null, [
        'id' => 'item-id',
    ]); ?>
    <?= Html::hiddenInput('year', $model->year); ?>
    <?= Html::hiddenInput('activeCheckbox', null, [
        'id' => 'active-checkbox',
    ]); ?>
    <div class="text-center buttons">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue',
            'style' => 'width: 12%!important;',
        ]); ?>
        <?= Html::a('Очистить данные', 'javascript:;', [
            'class' => 'btn darkblue clear-balance-inputs',
            'style' => 'width: 15%!important;',
        ]); ?>
        <?= Html::a('Отменить', 'javascript:;', [
            'class' => 'btn darkblue undo-update-balance-item',
            'style' => 'width: 12%!important;',
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?= $this->render('_partial/balance_panel'); ?>
<?= $this->render('_partial/balance_settings_panel'); ?>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div class="tooltip-template" style="display: none;">
<span id="tooltip-balance-liabilities" style="display: inline-block; text-align: center;">
    <span style="font-weight: bold;">Обязательства.</span>
    <br>
    Откуда получены денежные средства.
</span>
    <span id="tooltip-balance-assets" style="display: inline-block; text-align: center;">
    <span style="font-weight: bold;">Чем вы владеете.</span>
    <br>
    Куда вложены и на что потрачены
    <br>
    денежные средства.
</span>
</div>

<?= $this->render('_partial/balance_chart', [
    'model' => $model,
]); ?>
*/ ?>

<?php $this->registerJs('
    var $activeCheckbox = ' . json_encode($activeCheckbox) . ';

    $(document).ready(function (e) {
        if ($activeCheckbox.indexOf("main-checkbox-side") != -1) {
            $("#main-checkbox-side").click();
        } else {
            for (var i in $activeCheckbox) {
                if ($activeCheckbox[i] !== "") {
                    if ($activeCheckbox[i] == "w2" || $activeCheckbox[i] == "w3" || $activeCheckbox[i] == "w4") {
                        if ($("#w2").val() == 1) {
                            $("#w2").click();
                        }
                    } else if ($activeCheckbox[i] == "w7" || $activeCheckbox[i] == "w8") {
                        if ($("#w7").val() == 1) {
                            $("#w7").click();
                        }
                    } else {
                        $("#" + $activeCheckbox[i]).click();
                    }
                }
            }
        }
    });

    $(document).on("click", ".update-balance-item", function (e) {
        var $panel = $(".update-balance-item-panel");
        var $line = $(this).closest("tr");

        $(".balance-report-table tr.active-balance").removeClass("active-balance");
        if ($panel.is(":visible") && +$panel.find("#item-id").val() == +$line.data("itemid")) {
            $panel.find(".undo-update-balance-item").click();
        } else {
            if ($panel.is(":visible")) {
                $panel.find(".undo-update-balance-item").click();
                setTimeout(animate, 420);
            } else {
                animate();
            }
            function animate() {
                $line.addClass("active-balance");
                $("#item-id").val($line.data("itemid"));
                $line.find(".quarter-month-1, .quarter-month-2, .quarter-month-3, .quarter-month-4").each(function (i) {
                    var $amount = $(this).text().trim();

                    ++i;
                    if ($amount !== "" && $amount !== "0,00") {
                        $("input[name=\"BalanceItemAmount[" + i + "]\"]").val($amount.replace(",", ".").replace(/ /g, ""));
                    }
                });
                var $checked = [];
                $(".balance-type, .main-checkbox-side").each(function (i) {
                    if ($(this).val() == 0) {
                        $checked[i] = $(this).attr("id");
                    }
                });
                $("#active-checkbox").val($checked);
                $panel.css("top", (+$line.offset().top - 10) + "px");
                $panel.slideDown();
            }
        }
    });

    $(document).on("click", ".undo-update-balance-item", function (e) {
        var $panel = $(this).closest(".update-balance-item-panel");

        $(".balance-report-table tr.active-balance").removeClass("active-balance");
        $panel.find("input.amount-input, input#item-id").val("");
        $panel.slideUp();
    });

    $(document).on("click", ".clear-balance-inputs", function (e) {
        var $panel = $(this).closest(".update-balance-item-panel");

        $panel.find("input.amount-input").val("");
    });

    $(".balance-panel-trigger").click(function (e) {
        $(".balance-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(".settings-balance-panel-trigger").click(function (e) {
        $(".balance-settings-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); ?>
