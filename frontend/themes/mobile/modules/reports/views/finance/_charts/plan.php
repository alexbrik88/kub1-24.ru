<?php

use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;

/* @var $model \frontend\modules\reports\models\PaymentCalendarSearch
 * @var $planGrowingBalance array
 */
$planGrowingBalance = array_map(function ($amount, $monthNumber) use ($model) {
    if ($model->year == date('Y') && $monthNumber <= date('m')) {
        return '';
    }

    return $amount / 100;
}, array_values($planGrowingBalance), array_keys($planGrowingBalance));
?>

<style>
    #chart-plan-fact {
        height: 200px;
    }

    #chart-plan-fact-2 {
        height: 200px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div style="min-height:100px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('redrawPlanMonths()')
                        ],
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'shared' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'formatter' => new jsExpression("
                        function(args) {
                            var index = this.series.data.indexOf( this.point );
                            var series_index = this.series.index;

                            if (index > (window.chartCurrMonth - 1)) {
                            
                                return '<b>' + this.x + '</b>' +
                                    '<br/>' + args.chart.series[2].name + ': ' + args.chart.series[2].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[3].name + ': ' + args.chart.series[3].data[index].y + ' Р';
                            }
                            
                            return '<b>' + this.x + '</b>' +
                                ((series_index == 0 || series_index == 2) ?                                
                                    ('<br/>' + args.chart.series[0].name + ': ' + args.chart.series[0].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[2].name + ': ' + args.chart.series[2].data[index].y + ' Р') :                                
                                    ('<br/>' + args.chart.series[1].name + ': ' + args.chart.series[1].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[3].name + ': ' + args.chart.series[3].data[index].y + ' Р'));
                        }
                    ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                    ],
                    'xAxis' => [
                        'categories' => array_values(Month::$monthShort),
                    ],
                    'series' => [
                        [
                            'name' => 'Приход Факт',
                            'data' => $model->getChartAmount(CashFlowsBase::FLOW_TYPE_INCOME),
                            'color' => 'rgba(93,173,226,1)',
                            'borderColor' => 'rgba(93,173,226,.3)'
                        ],
                        [
                            'name' => 'Расход Факт',
                            'data' => $model->getChartAmount(CashFlowsBase::FLOW_TYPE_EXPENSE),
                            'color' => 'rgba(255,213,139,1)',
                            'borderColor' => 'rgba(255,213,139,.3)'
                        ],
                        [
                            'name' => 'Приход План',
                            'data' => $model->getChartAmount(CashFlowsBase::FLOW_TYPE_INCOME, true),
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(21,67,96,1)',
                                'radius' => 10
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => -0.15,
                            'stickyTracking' => false,
                        ],
                        [
                            'name' => 'Расход План',
                            'data' => $model->getChartAmount(CashFlowsBase::FLOW_TYPE_EXPENSE, true),
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(50,50,50,1)',
                                'radius' => 10
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0.15,
                            'stickyTracking' => false,
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} Р</b>',
                            ]
                        ],
                        'series' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} Р</b>',
                            ]
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div style="min-height:100px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-2',
                'scripts' => [
                   // 'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'formatter' => new jsExpression("
                                function(args) {
                                    var index = this.series.data.indexOf( this.point );
                                    var series_index = this.series.index;

                                    if (series_index == 1) {
                                        return '<b>' + this.x + '</b>' +
                                            '<br/>' + args.chart.series[series_index].name + ': ' + args.chart.series[series_index].data[index].y + ' Р';
                                    }

                                    if (index > (window.chartCurrMonth - 1)) {
                                        return '<b>' + this.x + '</b>' +
                                            '<br/>' + args.chart.series[2].name + ': ' + args.chart.series[2].data[index].y + ' Р';
                                    }
                                        
                                    return '<b>' + this.x + '</b>' +
                                        '<br/>' + args.chart.series[0].name + ': ' + args.chart.series[0].data[index].y + ' Р' +
                                        '<br/>' + args.chart.series[1].name + ': ' + args.chart.series[1].data[index].y + ' Р';
                                }
                            ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                    ],
                    'xAxis' => [
                        'categories' => array_values(Month::$monthShort),
                        'title' => ''
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток Факт',
                            'data' => $model->getChartReminderAmount(),
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'zIndex' => 2
                        ],
                        [
                            'name' => 'Остаток Факт (предыдущий год)',
                            'data' => $model->getChartReminderAmount($model->year - 1),
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'zIndex' => 0
                        ],
                        [
                            'name' => 'Остаток План',
                            'data' => $planGrowingBalance,
                            'color' => 'rgba(26,184,93,1)',
                            'negativeColor' => 'red',
                            'fillColor' => [
                                'pattern' => [
                                    //'image' => '/img/pattern1.png',
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#27ae60',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'negativeFillColor' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#e74c3c',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'marker' => [
                                'symbol' => 'square'
                            ],
                            'zIndex' => 1
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    var chartCurrMonth = "<?= $model->year == date('Y') ? date('n') : 12; ?>";

    function redrawPlanMonths() {

        var custom_pattern = function (color) {
            return {
                pattern: {
                    path: 'M 0 0 L 10 10 M 9 - 1 L 11 1 M - 1 9 L 1 11',
                    width: 10,
                    height: 10,
                    color: color
                }
            }
        }

        var chartToLoad = window.setInterval(function () {
            var chart = $('#chart-plan-fact').highcharts();
            if (typeof(chart) !== 'undefined') {
                console.log(chart.series);
                for (var i = (chartCurrMonth); i < 12; i++) {
                    chart.series[0].points[i].color = custom_pattern("rgba(93,173,226,1)");
                    chart.series[1].points[i].color = custom_pattern("rgba(255,213,139,1)");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();

                window.clearInterval(chartToLoad);
            }

        }, 100);
    }

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });
</script>