<?php

use frontend\components\Icon;

/* @var $this yii\web\View */
/* @var $i string */
/* @var $ok bool */
?>
<a href="{url}" class="nav-link d-flex flex-column flex-grow-1 align-items-center text-center pb-2">
    <div class="mb-2 pb-1">
        <?php if ($i == 'diagram-custom'): ?>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="40px" height="40px" class=""><g transform="matrix(0.9508, 0, 0, 0.9508, 1.476, 1.476)"><path d="M53,41V29H31V19h7V3H22v16h7v10H7v12H0v16h16V41H9V31h20v10h-7v16h16V41h-7V31h20v10h-7v16h16V41H53z M24,5h12v12H24V5z   M14,55H2V43h12V55z M36,55H24V43h12V55z M58,55H46V43h12V55z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#BBC1C7"/></g> </svg>
        <?php else: ?>
            <?= Icon::get($i, ['class' => 'svg-icon svg-icon_font_size_40']) ?>
        <?php endif; ?>
    </div>
    <div class="flex-grow-1 d-flex flex-column justify-content-center pb-1">{label}</div>
</a>