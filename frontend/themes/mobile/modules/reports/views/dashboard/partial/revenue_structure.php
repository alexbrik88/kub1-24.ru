<?php
$categories = ['Товар 1', 'Товар 2', 'Товар 3', 'Товар 4', 'Товар 5'];
$fact = [45000, 32000, 25000, 24000, 15000];
$plan = [35000, 35000, 20000, 20000, 20000];
?>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-revenue-structure',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Структура выручки',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'inverted' => true
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                ],
                'grouping' => false,
                'shadow' => false,
                'borderWidth' => 0
            ]
        ],
        'tooltip' => [
            'shared' => true
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => $categories],

        ],
        'series' => [
            ['name' => 'Факт', 'pointPadding' => 0.4, 'data' => $fact, 'color' => 'rgba(93,173,226,1)'],
            ['name' => 'План', 'pointPadding' => 0.25, 'data' => $plan, 'color' => 'rgba(0,0,0,.25)']
        ]
    ],
]); ?>