<div class="chart-in-table">
    <table>
        <tr>
            <td>
                РАСХОДЫ
            </td>
            <td style="text-align:right;">12 750 000 ₽</td>
        </tr>
        <tr>
            <td style="font-size:11px;font-weight:normal;line-height:8px">
                с начала месяца
            </td>
        </tr>
    </table>
</div>
<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-expenses',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
        ],
        'tooltip' => [
            'shared' => true
        ],
        'title' => [
            'text' => 'Расходы',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']],

        ],
        'series' => [
            [
                'name' => 'Факт',
                'data' => [15000, 20000, 18000, 15000],
                'color' => 'rgba(255,213,139,1)'
            ],
            [
                'name' => 'План',
                'data' => array_reverse([15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000]),
                'marker' => [
                    'symbol' => 'c-rect',
                    'lineWidth' => 3,
                    'lineColor' =>  'rgba(50,50,50,1)',
                    'radius' => 5
                ],
                'type' => 'scatter',
            ]
        ],
        'plotOptions' => [
            'scatter' => [
                'tooltip' => [
                    'crosshairs' => true,
                    'headerFormat' => '{point.x}',
                    'pointFormat' => '<br />План: {point.y}',
                ]
            ],
            'series' => [
                'tooltip' => [
                    'crosshairs' => true,
                    'headerFormat' => '{point.x}',
                    'pointFormat' => '<br />Факт: {point.y}</b>',
                ]
            ]
        ]
    ],
]); ?>