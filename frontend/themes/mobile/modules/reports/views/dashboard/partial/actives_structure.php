<?php
$currMonth = date('m') - 1;
$currYear = date('y');
$monthArr = ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
$month = [];
for ($i=$currMonth+1; $i<12; $i++) {
    $month[] = $monthArr[$i].'.'.($currYear-1);
}
for ($i=0; $i<=$currMonth; $i++) {
    $month[] = $monthArr[$i].'.'.($currYear);
}

$stocks  = array_map(function($n){return 10E6*$n;}, [2,2,2,3,3,3,4,4,4,5,5,5]); // запасы
$debtors = array_map(function($n){return 10E6*$n;}, [5,5,5,4,4,4,3,3,3,2,2,2]); // дебиторы
$money   = array_map(function($n){return 10E6*$n;}, [1,1,1,2,2,2,3,3,3,4,4,4]); // деньги
$fixed_assets = array_map(function($n){return 10E6*$n;}, [2,2,2,2,2,2,2,2,2,2,2,2]); // внеоб. активы
?>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-actives-structure',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'exporting' => [
            'enabled' => false,
        ],
        'chart' => [
            'type' => 'column',
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    //'enabled' => true,
                ],
                'stacking' => true
            ]
        ],
        'tooltip' => [
            'shared' => true,
            'headerFormat' => '<b>{point.x}</b><br>',
            'pointFormat' => '<b>{series.name}:</b> {point.y}<br>',
            'footerFormat' => '<b>Итого: {point.total:,.0f}</b>'
        ],
        'title' => [
            'text' => 'Структура активов',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => $month],

        ],
        'series' => [
            ['name' => 'Запасы', 'data' => $stocks, 'color' => 'rgba(183,225,115,1)', 'states' => getPatternHover('rgba(183,225,115,1)')],
            ['name' => 'Дебиторы', 'data' => $debtors, 'color' => 'rgba(160,64,0,1)', 'states' => getPatternHover('rgba(160,64,0,1)')],
            ['name' => 'Деньги', 'data' => $money, 'color' => 'rgba(17,122,101,1)', 'states' => getPatternHover('rgba(17,122,101,1)')],
            ['name' => 'Внеоб. активы', 'data' => $fixed_assets, 'color' => 'rgba(41,138,188,1)', 'states' => getPatternHover('rgba(41,138,188,1)')]
        ],
    ],
]); ?>
