<?php
use yii\bootstrap4\ActiveForm;

$this->title = 'Выбор отчетов';

$form = ActiveForm::begin([
    'id' => 'statements-update-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);

?>

<div class="wrap p-4">
    <div class="p-2 text_size_14">
        <div class="mb-2">Что бы открыть описание отчетов, нажмите на такую - <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use></svg> иконку у названия отчета.</div>
        <div class="mb-2">Если вам не нужны какие-то отчеты, то отключите их. Их всегда можно вернуть. </div>
        <div>Мы рекомендуем оставить все отчёты, а в ходе работы уже убрать не нужные. На этом шаге важно увидеть, какая информация нужна для каждого отчета и какие интеграции помогут автоматически собирать данные. </div>
    </div>
</div>
<div class="wrap pl-0 pr-0">
    <div class="nav-tabs-row pb-3 mb-3">
        <ul class="nav nav-tabs nav-tabs_border_bottom_grey w-100 pl-4 pr-4" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link pt-0 active" id="odds-tab" data-toggle="tab" href="#odds" role="tab" aria-controls="odds" aria-selected="true">Финансы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link pt-0" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Маркетинг</a>
            </li>
            <li class="nav-item">
                <a class="nav-link pt-0" id="opiu-tab" data-toggle="tab" href="#opiu" role="tab" aria-controls="opiu" aria-selected="false">Продажи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link pt-0" id="balance-tab" data-toggle="tab" href="#balance" role="tab" aria-controls="balance" aria-selected="false">Товар</a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="myTabContent1">
        <div class="tab-pane fade show active" id="odds" role="tabpanel" aria-labelledby="info-tab">
            <div class="wrap pl-4 pr-4 pt-0 pb-0">
                <div id="chartParent1">
                    <div class="row row_indents_sm align-items-center">
                        <div class="column mr-auto">
                            <div class="switch">
                                <input class="md-check switch-input input-hidden" type="checkbox" id="switcher1" checked>
                                <label class="switch-label line-height-1" for="switcher1">
                                    <span class="switch-pseudo">&nbsp;</span>
                                    <span class="switch-txt-bold ml-3 text_size_20">Отчет о Движении Денежных Средств </span>
                                </label>
                            </div>
                        </div>
                        <div class="column">
                            <button class="button-list collapsed button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse1" aria-expanded="false" aria-controls="chartCollapse1">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse2" aria-expanded="false" aria-controls="chartCollapse2">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-list_collapse button-hover-transparent button-clr w-auto pl-3 pr-3 mb-2" type="button">Используемые данные</button>
                        </div>
                    </div>
                    <div class="collapse" id="chartCollapse1" data-parent="#chartParent1"></div>
                    <div class="collapse show" id="chartCollapse2" data-parent="#chartParent1">
                        <div class="pt-4 pb-3">
                            <div class="row">
                                <?= $this->render('_charts/expenses') ?>
                                <!--<div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">ПРИХОД-РАСХОД</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_yellow">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mb-1">
                                                <div class="symbol-line">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart.jpg" alt="">
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Структура расходов</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_blue">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_grey-light">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart-bars.jpg" alt="">
                                </div>-->
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Используемые данные</div>
                                    </div>
                                    <ul class="list-circle text_size_14 mb-2">
                                        <li>Выписка из клиент – банка.</li>
                                        <li>Выгрузка из ОФД.</li>
                                        <li>Данные по кассе из Excel.</li>
                                        <li>Ввод операций вручную.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="devider mt-3 mb-4">
                <!--
                <div id="chartParent2">
                    <div class="row row_indents_sm align-items-center">
                        <div class="column mr-auto">
                            <div class="switch">
                                <input class="md-check switch-input input-hidden" type="checkbox" id="switcher2" checked>
                                <label class="switch-label line-height-1" for="switcher2">
                                    <span class="switch-pseudo">&nbsp;</span>
                                    <span class="switch-txt-bold ml-3 text_size_20">Платежный календарь</span>
                                </label>
                            </div>
                        </div>
                        <div class="column">
                            <button class="button-list collapsed button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse3" aria-expanded="false" aria-controls="chartCollapse3">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse4" aria-expanded="false" aria-controls="chartCollapse4">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-list_collapse button-hover-transparent button-clr w-auto pl-3 pr-3 mb-2" type="button">Используемые данные</button>
                        </div>
                    </div>
                    <div class="collapse" id="chartCollapse3" data-parent="#chartParent2"></div>
                    <div class="collapse show" id="chartCollapse4" data-parent="#chartParent2">
                        <div class="pt-4 pb-3">
                            <div class="row">
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">ПРИХОД-РАСХОД</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_yellow">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mb-1">
                                                <div class="symbol-line">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart.jpg" alt="">
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Структура расходов</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_blue">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_grey-light">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart-bars.jpg" alt="">
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Для ручного планирования:</div>
                                    </div>
                                    <ul class="list-circle text_size_14 mb-2">
                                        <li>Ввод операций вручную. </li>
                                    </ul>
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Для автоматического планирования нужны данные:</div>
                                    </div>
                                    <ul class="list-circle text_size_14 mb-2">
                                        <li>Выписка из клиент – банка.</li>
                                        <li>Выгрузка из ОФД.</li>
                                        <li>Данные по кассе из Excel.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="devider mt-3 mb-4">
                <div id="chartParent3">
                    <div class="row row_indents_sm align-items-center">
                        <div class="column mr-auto">
                            <div class="switch">
                                <input class="md-check switch-input input-hidden" type="checkbox" id="switcher3" checked>
                                <label class="switch-label line-height-1" for="switcher3">
                                    <span class="switch-pseudo">&nbsp;</span>
                                    <span class="switch-txt-bold ml-3 text_size_20">План-Факт</span>
                                </label>
                            </div>
                        </div>
                        <div class="column">
                            <button class="button-list collapsed button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse5" aria-expanded="false" aria-controls="chartCollapse5">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-hover-transparent button-clr mb-2" type="button" data-toggle="collapse" href="#chartCollapse6" aria-expanded="false" aria-controls="chartCollapse6">
                                <svg class="svg-icon">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-list button-list_collapse button-hover-transparent button-clr w-auto pl-3 pr-3 mb-2" type="button">Используемые данные</button>
                        </div>
                    </div>
                    <div class="collapse" id="chartCollapse5" data-parent="#chartParent3"></div>
                    <div class="collapse show" id="chartCollapse6" data-parent="#chartParent3">
                        <div class="pt-4 pb-3">
                            <div class="row">
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">ПРИХОД-РАСХОД</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_yellow">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mb-1">
                                                <div class="symbol-line">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart.jpg" alt="">
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Структура расходов</div>
                                        <div class="d-flex flex-wrap align-items-center ml-auto">
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_blue">&nbsp;</div>
                                                <div class="sumbol-descr">Факт</div>
                                            </div>
                                            <div class="symbol mr-3 mb-1">
                                                <div class="symbol-square symbol-square_grey-light">&nbsp;</div>
                                                <div class="sumbol-descr">План</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/images/start-chart-bars.jpg" alt="">
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="d-flex flex-nowrap pb-1">
                                        <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Используемые данные:</div>
                                    </div>
                                    <ul class="list-circle text_size_14 mb-2">
                                        <li>Данные из отчетов ОДДС <br> и План-Факт. </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">...</div>
        <div class="tab-pane fade" id="opiu" role="tabpanel" aria-labelledby="opiu-tab">...</div>
        <div class="tab-pane fade" id="balance" role="tabpanel" aria-labelledby="balance-tab">...</div>
        <div class="tab-pane fade" id="costs" role="tabpanel" aria-labelledby="costs-tab">...</div>
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'statements',
    'submit_button' => true,
    'skip_button' => true,
]); ?>

<?php $form->end(); ?>
