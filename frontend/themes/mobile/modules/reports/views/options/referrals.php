<div class="wrap p-4">
    <div class="pt-4 pl-2 pr-2 text-center">
        <div class="weight-700">Получайте комиссию по реферальной программе, со всех платежей по тарифу <br> в течение года после регистрации с каждого привлеченного клиента.</div>
        <hr class="devider mt-5 mb-5">
        <div class="row justify-content-center pt-2">
            <div class="col-4 pl-4 pr-4 d-flex flex-column">
                <div class="h-120 d-flex flex-column justify-content-center mb-3 pb-3">
                    <div>
                        <img src="/images/20.svg" alt="">
                    </div>
                </div>
                <div class="weight-700">Получайте от 20% с платежей <br class="d-none d-xl-block"> за тариф от приведенных клиентов</div>
            </div>
            <div class="col-4 pl-4 pr-4 d-flex flex-column">
                <div class="h-120 d-flex flex-column justify-content-center mb-3 pb-3">
                    <div>
                        <img src="/images/people.svg" alt="">
                    </div>
                </div>
                <div class="weight-700">Начисления с каждого клиента <br class="d-none d-xl-block"> в течение года после регистрации</div>
            </div>
            <div class="col-4 pl-4 pr-4 d-flex flex-column">
                <div class="h-120 d-flex flex-column justify-content-center mb-3 pb-3">
                    <div>
                        <img src="/images/pouch-1.svg" alt="">
                    </div>
                </div>
                <div class="weight-700">Выводите заработанные средства <br class="d-none d-xl-block"> или используйте их для оплаты своих <br> компаний</div>
            </div>
        </div>
        <hr class="devider mt-5 mb-5">
        <h4 class="weight-700">Начните подключать клиентов</h4>
        <div class="text_size_14">Каждый привлеченный клиент получит бесплатный доступ к КУБ.ФинДиректор <br> на 14 дней и промокод на 1 интеграцию на месяц.</div>
        <div class="row justify-content-center pt-5">
            <div class="col-4 pl-4 pr-4">
                <div class="weight-700 mb-3 pb-1">Реферальная ссылка</div>
                <div class="pb-1">
                    <input class="form-control form-control-link mb-2" type="text" value="https://kub-24.ru/finance/utm1234">
                </div>
                <div class="text_size_14">С пользователей, зарегистрированных по данной ссылке, вы будите получать комиссию.</div>
            </div>
            <div class="col-4 pl-4 pr-4">
                <div class="weight-700 mb-3 pb-1">Письмо на электронную почту</div>
                <div class="pb-1">
                    <div class="position-relative mb-2">
                        <input class="form-control pr-5" type="email" placeholder="e-mail">
                        <svg class="form-control-icon svg-icon text-blue">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#envelope"></use>
                        </svg>
                    </div>
                </div>
                <div class="text_size_14">Приглашение зарегистрироваться в вашей реферальной программе будет отправлено на указанную почту.</div>
            </div>
            <div class="col-4 pl-4 pr-4">
                <div class="weight-700 mb-3 pb-1">Ссылка для соц сетей</div>
                <div class="row row_indents_m justify-content-center">
                    <div class="column mb-3">
                        <a class="d-block" href="#">
                            <img src="/images/FBLogo.png" width="37" alt="">
                        </a>
                    </div>
                    <div class="column mb-3">
                        <a class="d-block" href="#">
                            <img src="/images/InstLogo.png" width="37" alt="">
                        </a>
                    </div>
                    <div class="column mb-3">
                        <a class="d-block" href="#">
                            <img src="/images/VKLogo.png" width="37" alt="">
                        </a>
                    </div>
                </div>
                <div class="text_size_14">Отправьте клиенту ссылку в соц сетях, чтобы подключить его к своей реферальной программе.</div>
            </div>
        </div>
        <hr class="devider mt-5 mb-5">
        <div class="mb-3 pb-3">
            <h4 class="weight-700">Средний заработок участников <br> реферальной программы:</h4>
        </div>
        <div class="row row_indents_m flex-nowrap justify-content-center">
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">6 500 ₽</div>
                    <div class="earning-descr">Средний <br> чек</div>
                </div>
            </div>
            <div class="column d-flex flex-column">
                <div class="earning-symbol d-flex flex-column flex-grow-1 pt-1 pt-xl-2 mt-xl-1">Х</div>
            </div>
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">0,25</div>
                    <div class="earning-descr">Средняя <br> комиссия</div>
                </div>
            </div>
            <div class="column d-flex flex-column">
                <div class="earning-symbol d-flex flex-column flex-grow-1 pt-1 pt-xl-2 mt-xl-1">Х</div>
            </div>
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">12</div>
                    <div class="earning-descr">В течении <br> года</div>
                </div>
            </div>
            <div class="column d-flex flex-column">
                <div class="earning-value d-flex flex-column flex-grow-1">=</div>
            </div>
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">19 500 ₽</div>
                    <div class="earning-descr">В среднем с одного <br> клиента в год</div>
                </div>
            </div>
            <div class="column d-flex flex-column">
                <div class="earning-symbol d-flex flex-column flex-grow-1 pt-1 pt-xl-2 mt-xl-1">Х</div>
            </div>
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">17</div>
                    <div class="earning-descr">В среднем <br> привлеченных <br> клиентов</div>
                </div>
            </div>
            <div class="column d-flex flex-column">
                <div class="earning-value d-flex flex-column flex-grow-1">=</div>
            </div>
            <div class="column d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1">
                    <div class="earning-value mb-3 pb-1">331 500 ₽</div>
                    <div class="earning-descr">Средний доход <br> партнера за год</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => '',
    'submit_button' => true,
]); ?>