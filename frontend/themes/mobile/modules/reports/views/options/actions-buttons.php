<?php
/** @var string $next_step
 *  @var boolean $skip_button
 *  @var boolean $next_button
 *  @var boolean $sumbit_button
 */

?>
<div class="wrap wrap_btns fixed mb-0">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <?php /*
        <div class="column d-flex flex-nowrap">
            <div class="line-progress">
                <div class="line-progress-scale" style="width: 16.6%;">&nbsp;</div>
                <div class="line-progress-prize">
                    <img src="/images/prize-img.png" width="28" alt="">
                </div>
            </div>
            <div class="ml-2 text-grey weight-700">Cкидка 10 000 ₽</div>
        </div>*/ ?>
        <?php if (!empty($skip_button)): ?>
            <div class="ml-auto column">
                <a href="<?= $next_step ?>" class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button" data-step="next">Пропустить</a>
            </div>
        <?php endif; ?>
        <?php if (!empty($next_button)): ?>
            <div class="<?= (empty($skip_button)) ? 'ml-auto ' : '' ?>column">
                <a href="<?= $next_step ?>" class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button" data-step="next">
                    <span>Следующий шаг</span>
                    <svg class="svg-icon svg-icon_rotate_90_negative text_size_14 ml-2">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                    </svg>
                </a>
            </div>
        <?php endif; ?>
        <?php if (!empty($submit_button)): ?>
            <div class="<?= (empty($skip_button) && empty($next_button)) ? 'ml-auto ' : '' ?> column">
                <button class="button-clr button-regular button-regular_red pr-3 pl-3" type="submit">
                    <span>Сохранить</span>
                </button>
            </div>
        <?php endif; ?>
    </div>
</div>