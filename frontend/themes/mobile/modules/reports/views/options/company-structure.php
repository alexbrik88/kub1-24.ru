<?php

use backend\models\Bank;
use common\components\ImageHelper;
use common\models\company\CheckingAccountant;
use frontend\themes\mobile\helpers\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use common\models\companyStructure\SalePointType;
use common\models\companyStructure\SalePoint;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Структура компании';

/** @var SalePointType[] $salePointTypes */
/** @var SalePoint[] $companySalePoints */
/** @var \common\models\Company $company */

$salePointTypes = SalePointType::find()->all();
$companySalePoints = SalePoint::find()->where(['company_id' => $company->id])->all();
$accounts = $company->getCheckingAccountants()->andWhere(['<>', 'type', CheckingAccountant::TYPE_CLOSED])->orderBy([
    'type' => SORT_ASC,
    'id' => SORT_ASC,
])->all();

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

?>

<div class="wrap p-4 company-structure">
    <div class="p-2">
        <div class="pb-4"><h4><?= $this->title ?></h4></div>
        <div class="row">
        <?php foreach ($accounts as $account): ?>
            <?php $bank = Bank::findOne(['bik' => $account->bik, 'is_blocked' => false]); ?>
            <div class="col-4">
                <div class="sale-point">
                    <div class="point-title-single">
                        <span class="ico">
                            <?= ($bank && $bank->little_logo_link) ?
                                ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29]) :
                                $this->render('//svg-sprite', ['ico' => 'bank-4', 'class' => 'svg-icon']) ?>
                        </span>
                        <span class="name"><?= $account->bank_name ?></span>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php foreach ($company->ofdTypes as $ofd): ?>
                <div class="col-4">
                    <div class="sale-point">
                        <div class="point-title-single">
                            <span class="ico">
                                <?= $ofd->logo ?:  $this->render('//svg-sprite', ['ico' => 'pay-calendar', 'class' => 'svg-icon']) ?>
                            </span>
                            <span class="name"><?= $ofd->name ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
        <?php foreach ($companySalePoints as $salePoint): ?>
            <div class="col-4">
                <div class="sale-point" data-id="<?= $salePoint->id ?>">
                    <div class="point-title">
                        <span class="ico">
                            <img src="/images/<?= $salePoint->getIcon() ?>"/>
                        </span>
                        <span class="name"><?= $salePoint->name ?></span>
                    </div>
                    <div class="employee-list pad-ico">
                        <?php foreach ($salePoint->employers as $employee): ?>
                            <div class="employee-item">
                                <span class="ico-small"><img width="20" src="/images/sale-point-employee.png"/></span>
                                <span class="small-name"><?= $employee->getShortFio() ?></span>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="cashbox-list">
                        <?php foreach ($salePoint->cashboxes as $cashbox): ?>
                            <div class="cashbox-item">
                                <span class="ico"><img src="/images/sale-point-cashbox.png"/></span>
                                <span class="name"><?= $cashbox->name ?></span>
                            </div>
                        <?php endforeach; ?>
                        <?php if ($salePoint->type_id == SalePoint::TYPE_INTERNET_SHOP && $salePoint->onlinePaymentType): ?>
                            <div class="cashbox-item">
                                <span class="ico"><?= $this->render('//svg-sprite', ['ico' => 'bill', 'class' => 'svg-icon']) ?></span>
                                <span class="name"><?= $salePoint->onlinePaymentType->name ?></span>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="store-list">
                        <div class="store-item">
                            <span class="ico"><img src="/images/sale-point-store.png"/></span>
                            <span class="name"><?= $salePoint->store->name ?></span>
                        </div>
                    </div>

                    <div class="sale-point-actions">
                        <?= \yii\helpers\Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'class' => 'edit-sale-point button-regular button-hover-transparent button-clr',
                            'title' => 'Редактировать',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                            'class' => 'delete-sale-point button-regular button-hover-transparent button-clr',
                            'title' => 'Удалить',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
        </div>

        <div class="row mt-3">
            <div class="col-9">
                <div class="row">
                    <?php foreach ($salePointTypes as $key => $salePointType): ?>
                        <div class="col-4">
                            <div class="form-group">
                                <span class="label">
                                    <?= $salePointType->name ?>
                                    <?= Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip2_type_' . $salePointType->id,
                                    ]) ?>
                                </span>
                                <br/>
                                <button class="button-clr button-regular button-regular_red pl-3 pr-3 add-sale-point" data-type="<?= $salePointType->id ?>">
                                    <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
                                    <span class="ml-1">Добавить</span>
                                </button>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <div class="col-4">
                        <div class="form-group">
                                <span class="label">
                                    Другое
                                </span>
                            <br/>
                            <button class="button-clr button-regular button-regular_red pl-3 pr-3 send-proposal">
                                <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
                                <span class="ml-1">Добавить</span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div style="display: none">
    <?php foreach ($salePointTypes as $salePointType) {
        echo Html::tag('div', SalePointType::$helpText[$salePointType->id], [
            'id' => 'tooltip2_type_' . $salePointType->id
        ]);
    } ?>
</div>

<?php // Add SalePoint
Modal::begin([
    'id' => 'sale-point-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'sale-point-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Store
Modal::begin([
    'id' => 'store-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'store-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Cashbox
Modal::begin([
    'id' => 'cashbox-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'cashbox-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Employee
Modal::begin([
    'id' => 'employee-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'employee-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Proposal
Modal::begin([
    'id' => 'proposal-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'proposal-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>

<?= $this->render('actions-buttons', [
    'next_step' => 'reports',
    'skip_button' => true,
    'submit_button' => true,
]); ?>

<?php Modal::begin([
    'id' => 'delete-sale-point-modal',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить отдел?
    </h4>
    <div class="text-center">
        <?= \yii\helpers\Html::a('Да', null, [
            'class' => 'delete-sale-point-modal-btn button-clr button-regular button-hover-transparent button-width-medium',
            'data-id' => ''
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'delete-cashbox-modal',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить кассу?
    </h4>
    <div class="text-center">
        <?= \yii\helpers\Html::a('Да', null, [
            'class' => 'delete-cashbox-modal-btn button-clr button-regular button-hover-transparent button-width-medium',
            'data-id' => ''
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS
$(document).on("click", ".add-sale-point", function(e) {
    e.preventDefault();

    $.pjax({
        url: "add-sale-point",
        container: "#sale-point-pjax",
        push: false,
        timeout: 5000,
        scrollTo: false,
        data: {
            'type_id': $(this).attr('data-type')
        }
    });

    $(document).on("pjax:success", "#sale-point-pjax", function() {
        $("#sale-point-modal").modal("show");
    });

    return false;
});

$(document).on("change", "#salepoint-store_id", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal") {
        e.preventDefault();

        $.pjax({
            url: "add-store",
            container: "#store-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
        });

        $(document).on("pjax:success", "#store-pjax", function() {
            $("#store-modal").modal("show");
        });
        $("#salepoint-store_id").val("").trigger("change");
    }
});

$(document).on("click", ".add-cashbox", function(e) {
    e.preventDefault();

    if ($('.cashbox-item-modal').length >= 3) {

        window.toastr.success("Лимит 3 кассы", "", {
            "closeButton": true,
            "showDuration": 1000,
            "hideDuration": 1000,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            "escapeHtml": false,
        });

        return false;
    }

    $.pjax({
        url: "add-cashbox",
        container: "#cashbox-pjax",
        push: false,
        timeout: 5000,
        scrollTo: false,
    });

    $(document).on("pjax:success", "#cashbox-pjax", function() {
        $("#cashbox-modal").modal("show");
    });

    return false;
});

$(document).on("change", "#salepoint-employers", function(e) {

    var selectedEmployers = $(this).val();

    $.each(selectedEmployers, function(pos,value) {
        if (value == "add-modal") {
            $.pjax({
                url: "add-employee",
                container: "#employee-pjax",
                push: false,
                timeout: 5000,
                scrollTo: false,
            });

            $(document).on("pjax:success", "#employee-pjax", function() {
                $("#employee-modal").modal("show");
            });

            selectedEmployers.splice(pos,1);
            $("#salepoint-employers").val(selectedEmployers).trigger("change").select2("close");
        }
    });
});

$(document).on('click', '.delete-sale-point', function(e) {
    e.preventDefault();
    $('.delete-sale-point-modal-btn').data('id', $(this).parents('.sale-point').attr('data-id'));
    $('#delete-sale-point-modal').modal('show');
});

$(document).on('click', '.delete-sale-point-modal-btn', function(e) {
    e.preventDefault();
    var id = $('.delete-sale-point-modal-btn').data('id');
    $.get('delete-sale-point', {id: id}, function (data) {
        if (data.result) {
            $('.sale-point').filter('[data-id = "' + id + '"]').remove();

            window.toastr.success("Отдел удален", "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 1000,
                "extendedTimeOut": 1000,
                "escapeHtml": false,
            });
        }

        $('#delete-sale-point-modal').modal('hide');
    });

    return false;
});

$(document).on("click", ".edit-sale-point", function(e) {
    e.preventDefault();

    $.pjax({
        url: "update-sale-point",
        container: "#sale-point-pjax",
        push: false,
        timeout: 5000,
        scrollTo: false,
        data: {
            'id': $(this).parents('.sale-point').attr('data-id')
        }
    });

    $(document).on("pjax:success", "#sale-point-pjax", function() {
        $("#sale-point-modal").modal("show");
    });

    return false;
});

$(document).on("click", ".edit-cashbox", function(e) {
    e.preventDefault();

    $.pjax({
        url: "update-cashbox",
        container: "#cashbox-pjax",
        push: false,
        timeout: 5000,
        scrollTo: false,
        data: {
            'id': $(this).parents('.cashbox-item-modal').attr('data-id')
        }
    });

    $(document).on("pjax:success", "#cashbox-pjax", function() {
        $("#cashbox-modal").modal("show");
    });

    return false;
});

$(document).on('click', '.delete-cashbox', function(e) {
    e.preventDefault();
    $('.delete-cashbox-modal-btn').data('id', $(this).parents('.cashbox-item-modal').attr('data-id'));
    $('#delete-cashbox-modal').modal('show');
});

$(document).on('click', '.delete-cashbox-modal-btn', function(e) {
    e.preventDefault();
    var id = $('.delete-cashbox-modal-btn').data('id');
    $.get('delete-cashbox', {id: id}, function (data) {
        if (data.result) {
            $('.cashbox-item-modal').filter('[data-id = "' + id + '"]').remove();
        } else {

            window.toastr.success("Не удалось удалить кассу", "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 1000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });

        }

        $('#delete-cashbox-modal').modal('hide');
    });

    return false;
});

$(document).on("click", ".send-proposal", function(e) {
    e.preventDefault();

    $.pjax({
        url: "send-proposal",
        container: "#proposal-pjax",
        push: false,
        timeout: 5000,
        scrollTo: false,
    });

    $(document).on("pjax:success", "#proposal-pjax", function() {
        $("#proposal-modal").modal("show");
    });

    return false;
});

JS
);

/* preload styles */
echo Html::tag('div', Select2::widget(['name' => 'empty', 'data' => []]), ['style' => 'display:none']);