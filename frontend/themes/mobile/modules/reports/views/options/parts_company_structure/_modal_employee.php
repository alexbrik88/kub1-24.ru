<?php

use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use \common\models\companyStructure\OfdType;

/* @var bool $isSaved */
/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

if (Yii::$app->request->isAjax && $isSaved) {

    echo Html::script('
        var newState = new Option("'.$model->getShortFio().'", '.$model->employee_id.', true, true);
        var selectedEmployers = $("#salepoint-employers").val() || [];
        if (selectedEmployers) {
            selectedEmployers.push("'.$model->employee_id.'");
        }

        $("#salepoint-employers").append(newState).val(selectedEmployers).trigger("change");

        $("#employee-modal").modal("hide");',
        ['type' => 'text/javascript']);

} ?>

<h4 class="modal-title">
    <?= ($model->isNewRecord) ? 'Добавить' : 'Редактировать' ?> сотрудника
</h4>

<?php $form = ActiveForm::begin([
    'id' => 'employee-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->employee_id,
        'data-pjax' => true
    ],
]); ?>

<?php
$checkboxConfig = [
    'options' => [
        'class' => '',
        'style' => 'padding-top:35px'
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$roleArray = \common\models\employee\EmployeeRole::find()->actual()->orderBy([
    '([[id]] = ' . EmployeeRole::ROLE_CHIEF . ')' => SORT_DESC,
    '([[id]] = ' . EmployeeRole::ROLE_EMPLOYEE . ')' => SORT_ASC,
    'name' => SORT_ASC,
])->all();
?>

<div class="row">
    <div class="col-3">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-3">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'has_no_patronymic', $checkboxConfig)->label('Нет отчества')->checkbox([], true); ?>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'employee_role_id')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::map($roleArray, 'id', 'name'),
            'hideSearch' => true,
            'options' => [
                'prompt' => '',
                'disabled' => $model->scenario === Employee::SCENARIO_CONTINUE_REGISTRATION,
            ],
            'pluginOptions' => [
                'width' => '100%'
            ]]); ?>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_red button-width button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>


<?= Html::script('

// REFRESH_UNIFORMS
refreshUniform();
refreshDatepicker();

', [
'type' => 'text/javascript',
]); ?>
