<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.06.2017
 * Time: 19:08
 */

use yii\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\product\ProductUnit;
use common\models\document\UploadedDocuments;
use common\models\document\UploadedDocumentOrder;
use common\models\product\ProductType;
use common\components\TextHelper;
use yii\bootstrap4\ActiveForm;
use common\models\document\Invoice;

/* @var $this yii\web\View
 * @var $uploadedDocument UploadedDocuments
 * @var $uploadedDocumentOrder UploadedDocumentOrder
 * @var $form ActiveForm
 */

$inputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 p-l-0 p-r-0',
        'style' => 'width:34%;font-weight: bold;text-align: right!important;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-7 inp_one_line width-inp p-r-0',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
?>
<div class="portlet overl-auto">
    <table
        class="table table-striped table-bordered table-hover out-invoice_table"
        id="table-for-invoice">
        <thead>
        <tr class="heading" role="row">
            <th width="3%"></th>
            <th width="23%">Наименование</th>
            <th width="14%">Тип</th>
            <th width="7%">Кол-во</th>
            <th width="13%">Ед.</th>
            <th width="20%">Цена</th>
            <th width="20%">Сумма</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($uploadedDocument->uploadedDocumentOrders as $key => $uploadedDocumentOrder): ?>
            <tr class="order-row" role="row">
                <td class="text-center">
                    <span
                        class="icon-close remove-uploaded-document-order"></span>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'name', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][name]',
                        'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-name',
                        'data' => [
                            'id' => 'name',
                        ],
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeDropDownList($uploadedDocumentOrder, 'production_type_id',
                        ArrayHelper::merge([null => '---'], ArrayHelper::map(ProductType::find()->all(), 'id', 'name')), [
                            'class' => 'form-control',
                            'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][production_type_id]',
                            'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-production_type_id',
                            'data' => [
                                'id' => 'production_type_id',
                            ],
                        ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'count', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][count]',
                        'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-count',
                        'data' => [
                            'id' => 'count',
                        ],
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeDropDownList($uploadedDocumentOrder, 'product_unit_id',
                        ArrayHelper::merge([null => '---'], ArrayHelper::map(ProductUnit::find()->all(), 'id', 'name')), [
                            'class' => 'form-control',
                            'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][product_unit_id]',
                            'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-product_unit_id',
                            'data' => [
                                'id' => 'product_unit_id',
                            ],
                        ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'price', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][price]',
                        'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-price',
                        'data' => [
                            'id' => 'price',
                        ],
                        'value' => TextHelper::invoiceMoneyFormat($uploadedDocumentOrder->price, 2),
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'amount', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[' . $uploadedDocumentOrder->id . '][amount]',
                        'id' => 'uploadeddocumentorder-' . $uploadedDocumentOrder->id . '-amount',
                        'data' => [
                            'id' => 'amount',
                        ],
                        'value' => TextHelper::invoiceMoneyFormat($uploadedDocumentOrder->amount, 2),
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (!$uploadedDocument->getUploadedDocumentOrders()->exists()): ?>
            <tr class="order-row" role="row">
                <td class="text-center number"
                    style="font-size: 23px;">1
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'name', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[new][1][name]',
                        'id' => 'uploadeddocumentorder-new-1-name',
                        'data' => [
                            'id' => 'name',
                        ],
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeDropDownList($uploadedDocumentOrder, 'production_type_id',
                        ArrayHelper::map(ProductType::find()->all(), 'id', 'name'), [
                            'class' => 'form-control',
                            'name' => 'UploadedDocumentOrder[new][1][production_type_id]',
                            'id' => 'uploadeddocumentorder-new-1-production_type_id',
                            'data' => [
                                'id' => 'production_type_id',
                            ],
                        ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'count', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[new][1][count]',
                        'id' => 'uploadeddocumentorder-new-1-count',
                        'data' => [
                            'id' => 'count',
                        ],
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeDropDownList($uploadedDocumentOrder, 'product_unit_id',
                        ArrayHelper::map(ProductUnit::find()->all(), 'id', 'name'), [
                            'class' => 'form-control',
                            'name' => 'UploadedDocumentOrder[new][1][product_unit_id]',
                            'id' => 'uploadeddocumentorder-new-1-product_unit_id',
                            'data' => [
                                'id' => 'product_unit_id',
                            ],
                        ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'price', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[new][1][price]',
                        'id' => 'uploadeddocumentorder-new-1-price',
                        'data' => [
                            'id' => 'price',
                        ],
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeTextInput($uploadedDocumentOrder, 'amount', [
                        'class' => 'form-control',
                        'name' => 'UploadedDocumentOrder[new][1][amount]',
                        'id' => 'uploadeddocumentorder-new-1-amount',
                        'data' => [
                            'id' => 'amount',
                        ],
                    ]); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr class="button-add-line" role="row">
            <td class="bord-right" colspan="1">
                <div
                    class="portlet pull-left control-panel button-width-table">
                    <div class="btn-group pull-right"
                         style="display: inline-block;">
                        <span class="btn yellow btn-add-line-table" style="padding-left: 10px;">
                            <i class="pull-left fa icon fa-plus-circle"></i>
                        </span>
                    </div>
                </div>
            </td>
            <td class="" colspan="6">
            </td>
        </tr>
        </tbody>
    </table>
</div>

<div class="col-md-12 uploaded-document-total_amount_block"
     style="padding-right: 3px;padding-bottom: 30px;;">
    <div class="col-md-7">
    </div>
    <div class="col-md-5 p-r-0">
        <?= $form->field($uploadedDocument, 'total_amount_no_nds', array_merge($inputConfig, [
            'labelOptions' => [
                'class' => 'control-label col-md-4 p-l-0 p-r-0',
                'style' => 'width:50%;font-weight: bold;text-align: right!important;',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-5 inp_one_line width-inp',
                'style' => 'width: 50%;padding-right: 25px;',
            ],
        ]))->textInput([
            'value' => TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_no_nds, 2),
        ])->label('Итого:'); ?>

        <div
            class="form-group field-uploadeddocuments-total_amount_nds">
            <?= Html::activeDropDownList($uploadedDocument, 'nds_view_type_id', Invoice::$ndsViewList, [
                'class' => 'form-control nds-select',
            ]); ?>
            <div class="col-md-5 inp_one_line width-inp" style="width: 50%;padding-right: 25px;">
                <?= Html::activeTextInput($uploadedDocument, 'total_amount_nds', [
                    'class' => 'form-control m-l-n sel-w',
                    'value' => TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_nds, 2),
                    'disabled' => $uploadedDocument->nds_view_type_id == Invoice::NDS_VIEW_WITHOUT,
                ]); ?>
                <div class="help-block help-block-error "></div>
            </div>
        </div>

        <?= $form->field($uploadedDocument, 'total_amount_with_nds',  array_merge($inputConfig, [
            'labelOptions' => [
                'class' => 'control-label col-md-4 p-l-0 p-r-0',
                'style' => 'width:50%;font-weight: bold;text-align: right!important;',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-5 inp_one_line width-inp',
                'style' => 'width: 50%;padding-right: 25px;',
            ],
        ]))->textInput([
            'value' => TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_with_nds, 2),
        ])->label('Всего к оплате:'); ?>
    </div>
</div>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="col-sm-3 col-xs-3" style="padding-left: 30px;">
            <?= Html::a('Сохранить', 'javascript:;', [
                'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs save-upload-documents-order',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue text-white widthe-100 hidden-lg save-upload-documents-order',
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="col-sm-6 col-xs-6"></div>
        <div class="col-sm-3 col-xs-3" style="padding-right: 15px;">
            <?= Html::a('Отменить', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undo-update-uploaded-documents-order',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-lg undo-update-uploaded-documents-order',
                'title' => 'Отменить',
            ]); ?>
        </div>
    </div>
</div>
