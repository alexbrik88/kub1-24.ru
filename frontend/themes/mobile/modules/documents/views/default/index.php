<?php
/* @var $searchModelIn common\models\document\Invoice */
/* @var $dataProviderIn yii\data\ActiveDataProvider */
/* @var $searchModelOut common\models\document\Invoice */
/* @var $dataProviderOut yii\data\ActiveDataProvider */
?>

<div class="portlet box">
    <h4 class="page-title">Документы</h4>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Исходящие документы
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= $this->render('/invoice/_invoices_table', ['searchModel' => $searchModelOut, 'dataProvider' => $dataProviderOut]); ?>
        </div>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Входящие документы
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= $this->render('/invoice/_invoices_table', ['searchModel' => $searchModelIn, 'dataProvider' => $dataProviderIn]); ?>
        </div>
    </div>
</div>
