<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\file\File;

/* @var $this yii\web\View */
/* @var $model common\models\document\AbstractDocument */

$invoice = $model->hasAttribute('invoice_id') ? $model->invoice : $model;
if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice,])) {
    $tooltipId = "file-link-list-{$model->id}-{$model->id}";
    $previewId = "file-link-view-{$model->id}-{$model->id}";
    $links = [];
    $thumb = '';
    foreach ($model->files as $file) {
        if ($thumb == '' && in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
            $thumb = $file->getImageThumb(400, 600);
        }
        $links[] = Html::a($file->filename_full, [
            "/documents/{$model->urlPart}/file-get",
            'type' => $model->type,
            'id' => $model->id,
            'file-id' => $file->id,
        ], [
            'target' => '_blank',
        ]);
    }
    foreach ($model->scanDocuments as $scan) {
        $file = $scan->file;
        if ($thumb == '' && in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
            $thumb = $file->getImageThumb(400, 600);
        }
        $links[] = Html::a($file->filename_full, [
            '/documents/scan-document/file',
            'id' => $scan->id,
            'name' => $file->filename_full,
        ], [
            'target' => '_blank',
        ]);
    }
    $contentLinks = Html::beginTag('span', ['id' => $tooltipId]);
    $contentLinks .= implode('<br/>', $links);
    $contentLinks .= Html::endTag('span');

    if ($links) {
        echo Html::a('<span class="icon icon-paper-clip"></span>', null, [
            'class' => 'file-link-tooltip',
            'data' => [
                'src' => $thumb,
                'linksid' => $tooltipId,
                'viewid' => $previewId,
            ]
        ]);
        echo Html::tag('div', $contentLinks, ['class' => 'hidden']);
    }
} else {
    echo '<span class="icon icon-paper-clip"></span>';
}

