<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2018
 * Time: 18:42
 */

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\models\OrderDocumentSearch;
use frontend\rbac\permissions;
use frontend\components\Icon;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $dashBoardData []
 * @var $searchModel OrderDocumentSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $user Employee
 */

$this->title = 'Заказы покупателей';

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];

\frontend\themes\mobile\helpers\KubHelper::setStatisticsFontSize(max($dashBoardData[0]['amount'], $dashBoardData[1]['amount'], $dashBoardData[2]['amount']) );

$dropItems = [
];

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to(['create']) ?>">
            <svg class="svg-icon">
                <use xlink:href="<?=$baseUrl?>/images/svg-sprite/svgSprite.svg#add-icon"></use>
            </svg>
            <span>Добавить</span>
        </a>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="count-card wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[0]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[0]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[0]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_red wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[1]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[1]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[1]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_green wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[2]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[2]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[2]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'order_document_payment_sum',],
                    ['attribute' => 'order_document_ship_up_to_date',],
                    ['attribute' => 'order_document_stock_id',],
                    ['attribute' => 'order_document_invoice',],
                    ['attribute' => 'order_document_act',],
                    ['attribute' => 'order_document_packing_list',],
                    ['attribute' => 'order_document_invoice_facture',],
                    ['attribute' => 'order_document_responsible_employee_id',],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер заказа или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', [
            'totalCount' => $dataProvider->totalCount,
            'scroll' => true,
        ]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center pad0',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-center pad0-l pad0-r',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return Html::checkbox('OrderDocument[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->total_amount,
                    ]);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => '№ заказа',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $model,
                    ]) ? Html::a($model->fullNumber, ['/documents/order-document/view', 'id' => $model->id,])
                        : $model->fullNumber;
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата заказа',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контр&shy;агент',
                'encodeLabel' => false,
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'filter' => FilterHelper::getContractorList(Documents::IO_TYPE_OUT, OrderDocument::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return '<span title="' . htmlspecialchars($model->contractor_name_short) . '">' . $model->contractor_name_short . '</span>';
                },
            ],
            [
                'attribute' => 'total_amount',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $price = TextHelper::invoiceMoneyFormat($model->total_amount, 2);
                    return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                },
            ],
            [
                'label' => 'Оплачено',
                'attribute' => 'paymentSum',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->invoice ?
                        TextHelper::invoiceMoneyFormat($model->invoice->getPaidAmount(), 2) : '---';
                },
            ],
            [
                'attribute' => 'shipped',
                'label' => 'Отгружено',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->getShippedCount();
                },
            ],
            [
                'attribute' => 'reserve',
                'label' => 'Резерв',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->getOrderDocumentProducts()
                        ->sum('quantity') * 1 - $model->getShippedCount();
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'white-space: initial;',
                ],
                'filter' => $searchModel->getStatusFilter(),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->status->name;
                },
            ],
            [
                'attribute' => 'ship_up_to_date',
                'label' => 'План. дата отгрузки',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'store_id',
                'label' => 'Со склада',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getStoreFilter(),
                's2width' => '200px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->store ? $model->store->name : '---';
                },
            ],
            [
                'attribute' => 'invoiceState',
                'label' => 'Счет',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                    'style' => 'white-space: initial;text-align: center;',
                ],
                'format' => 'raw',
                'filter' => $searchModel->getInvoiceFilter(),
                'value' => function (OrderDocument $model) {
                    $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);

                    return $model->invoice ?
                        Html::a($model->invoice->fullNumber, [
                            '/documents/invoice/view',
                            'id' => $model->invoice_id,
                            'type' => Documents::IO_TYPE_OUT,
                        ]) :
                        Html::a(Icon::get('add-icon') . '<span class="pl-1">Счёт</span>', [
                            '/documents/invoice/create-from-order',
                            'type' => Documents::IO_TYPE_OUT,
                            'orderDocumentID' => $model->id,
                        ], [
                            'title' => 'Добавить счет',
                            'class' => 'button-regular button-hover-content-red' . ($canCreate ? null : ' disabled no-rights-link'),
                            'style' => 'width:81px;',
                        ]);
                },
            ],
            [
                'attribute' => 'actState',
                'label' => 'Акт',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                    'style' => 'text-align: center;',
                ],
                'filter' => $searchModel->getActFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->invoice) {
                        foreach ($model->invoice->acts as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/act/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);
                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/act/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';
                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddAct()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model, 'type' => Documents::IO_TYPE_OUT]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">Акт</span>', [
                                '/documents/act/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'class' => 'button-regular button-hover-content-red' . ($model->invoice->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                'style' => 'width:81px;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null),
                                'title' => 'Добавить акт',
                            ]);
                        }
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'packingListState',
                'label' => 'ТН',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                    'style' => 'text-align: center;',
                ],
                'filter' => $searchModel->getPackingListFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->invoice) {
                        foreach ($model->invoice->packingLists as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/packing-list/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);
                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/packing-list/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';
                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddPackingList()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">ТН</span>', [
                                '/documents/packing-list/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'title' => 'Добавить ТН',
                                'class' => 'button-regular button-hover-content-red' .
                                    ($model->invoice->isRejected ? ' disabled' : '') .
                                    ($canCreate ? '' : ' no-rights-link'),
                                'style' => 'width:81px;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null),
                            ]);
                        }
                    }

                    return $content;
                }
            ],
            [
                'attribute' => 'invoiceFactureState',
                'label' => 'Счёт-фактура',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                    'style' => 'text-align: center;',
                ],
                'filter' => $searchModel->getInvoiceFactureFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->has_nds == OrderDocument::HAS_NO_NDS) {
                        return $content;
                    }
                    if ($model->invoice) {
                        foreach ($model->invoice->invoiceFactures as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/invoice-facture/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);

                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/invoice-facture/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';

                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddInvoiceFacture()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">СФ</span>', [
                                '/documents/invoice-facture/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'class' => 'button-regular button-hover-content-red' .
                                    ($model->invoice->isRejected ? ' disabled' : '') .
                                    ($canCreate ? '' : ' no-rights-link'),
                                'style' => 'width:81px;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : ''),
                                'title' => 'Добавить СФ',
                            ]);
                        }
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'author_id',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'filter' => $searchModel->getAuthorFilter(),
                's2width' => '300px',
                'value' => function (OrderDocument $model) {
                    return $model->author ? $model->author->getFio(true) : $model->contractor_name_short;
                },
            ],
        ],
    ]); ?>
</div>

<?= $this->render('index/operations'); ?>
