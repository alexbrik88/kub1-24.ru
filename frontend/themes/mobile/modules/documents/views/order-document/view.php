<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 6:07
 */

use common\models\document\OrderDocument;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use php_rutils\RUtils;
use frontend\rbac\permissions;
use common\models\employee\Employee;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $user Employee
 */

$dateFormatted = RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
$this->context->layoutWrapperCssClass = 'order-document-view out-document';

$this->title = 'Заказ №' . $model->fullNumber . ' от ' . $dateFormatted;
$previousOrderDocument = $model->getPreviousOrderDocument();
$nextOrderDocument = $model->getNextOrderDocument();
?>

<?= Html::a('Назад к списку', ['index'], ['class' => 'link mb-2']); ?>

<?php if ($model->is_deleted): ?>
    <h4 class="text-warning">Заказ удалён</h4>
<?php endif; ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'id' => $model->id,
                    ], [
                        'data-tooltip-content' => '#unpaid-invoice',
                        'data-placement' => 'right',
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                    ]) ?>
                <?php endif; ?>
                <div class="doc-container doc-order-document doc-preview">
                    <?= $this->render('view/main_info', [
                        'model' => $model,
                        'dateFormatted' => $dateFormatted,
                        'user' => $user,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('view/status_block', [
                'model' => $model,
            ]); ?>
            <?= $this->render('view/upload_file_block', [
                'model' => $model,
                'dateFormatted' => $dateFormatted,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('view/action_buttons', [
    'model' => $model,
]); ?>
