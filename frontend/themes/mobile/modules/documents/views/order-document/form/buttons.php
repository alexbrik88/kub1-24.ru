<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2018
 * Time: 7:04
 */

use common\models\Company;
use frontend\rbac\permissions\document\Document;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $company Company */

$cancelUrl = Url::previous('lastPage');
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>
