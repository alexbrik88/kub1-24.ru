<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 18:07
 */

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Html;
use common\components\date\DateHelper;
use common\models\document\OrderDocument;
use yii\bootstrap4\ActiveForm;
use common\models\document\status\OrderDocumentStatus;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 */

?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Заказ покупателя №
                        </span>
                    </span>
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>

                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= Html::activeTextInput($model, 'document_additional_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                            'placeholder' => 'доп. номер',
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
                <div class="form-group d-inline-block mb-0 order-doc-status-set">
                    <select id="account-number" class="form-control order-document-status" name="OrderDocument[status_id]"
                            style="width: 150px;color: #ffffff;">
                        <?php /* @var $orderDocumentNumber OrderDocumentStatus */
                        foreach (OrderDocumentStatus::find()->all() as $orderDocumentNumber): ?>
                            <option data-color="<?= $orderDocumentNumber->color; ?>"
                                    value="<?= $orderDocumentNumber->id; ?>">
                                <?= $orderDocumentNumber->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $(document).ready(function() {
        var $orderDocumentStatus = $(".order-document-status");
        $orderDocumentStatus.select2({
            theme: "krajee-bs4",
            templateResult: formatState,
            minimumResultsForSearch: -1,
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        var $select2container = $("#select2-account-number-container").closest(".select2-selection");
        $("#select2-account-number-container").closest(".select2").css("display", "inline-block");
        $select2container.css("background-color", $(".order-document-status option[value=" + $orderDocumentStatus.val() + "]").data("color"));

        function formatState (state) {
            if (!state.id) {
              return state.text;
            }
            var $orderDocumentStatusOption = $(".order-document-status option[value=" + state.id + "]");
            var $state = "<span class=\"order-document-status-option\" style=\"background-color: " + $orderDocumentStatusOption.data("color") + "\"></span>" + state.text

            return $state;
        };

        $orderDocumentStatus.on("select2:select", function (e) {
            $select2container.css("background-color", $(".order-document-status option[value=" + e.params.data.id + "]").data("color"));
        });
    });
'); ?>
