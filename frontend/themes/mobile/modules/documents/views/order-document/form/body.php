<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 17:22
 */

use common\models\document\OrderDocument;
use frontend\models\Documents;
use frontend\themes\mobile\helpers\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use common\models\Contractor;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\Company;
use yii\web\JsExpression;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 * @var $company Company
 */

$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => !$model->isNewRecord,
    'prompt' => '',
];

$ioType = $model->type;
$contractorType = ($ioType == Documents::IO_TYPE_IN) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;

?>
<?= Html::activeHiddenInput($model, 'production_type', [
    'class' => 'order_document-production-type-hidden',
    'value' => '',
]); ?>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">
                    <?= ($ioType == Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?><span class="important">*</span>
                </label>
                <div class="row">
                    <div class="col-6">
                        <?php echo $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                            'class' => 'show-contractor-type-in-fields',
                        ]])->widget(Select2::class, [
                            'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить ' . ($ioType == Documents::IO_TYPE_IN ? 'поставщика' : 'покупателя')]
                                + Contractor::getALLContractorList($contractorType, false),
                            'options' => $contractorDropDownConfig,
                            'pluginOptions' => [
                                'width' => '100%',
                                'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <div class="form-filter">
                <label class="label" for="cause">Основание</label>
            </div>
            <?= $this->render('basis_document', [
                'model' => $model,
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6">
            <div class="form-filter">
                <label class="label" for="cause">Склад<span class="important">*</span></label>
            </div>
            <?= $form->field($model, 'store_id', ['template' => "{input}", 'options' => [
                'class' => '',
            ]])->widget(Select2::class, [
                'data' => OrderDocument::getStoreArray(),
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6">
            <div class="form-filter">
                <label class="label" for="input3">Отгрузить до<span class="important">*</span></label>
            </div>
            <div class="row align-items-center">
                <div class="form-group col-4">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'ship_up_to_date', [
                            'class' => 'form-control date-picker',
                            'value' => DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <?= Html::activeCheckbox($model, 'has_discount', [
                'class' => '',
                'label' => 'Указать скидку',
            ]); ?>
        </div>
        <?php if ($company->companyTaxationType->usn) : ?>
            <br>
            <div class="form-group col-6">
                <?= Html::activeCheckbox($model, 'has_nds', [
                    'class' => '',
                    'label' => 'Счет с НДС',
                ]); ?>
            </div>
            <?php $this->registerJs('
                $(document).on("change", "#orderdocument-has_nds", function() {
                    if ($(this).is(":checked")) {
                        $(".with-nds-item").removeClass("hidden");
                        $(".without-nds-item").addClass("hidden");
                    } else {
                        $(".with-nds-item").addClass("hidden");
                        $(".without-nds-item").removeClass("hidden");
                    }
                });
            ') ?>
        <?php endif; ?>
    </div>
</div>