<?php

use frontend\assets\AppAsset;
use frontend\modules\cash\modules\banking\components\Banking;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$canSave = ArrayHelper::getValue($this->params, 'canSave', false);
$canPay = ArrayHelper::getValue($this->params, 'canPay', false);
$invoice = ArrayHelper::getValue($this->params, 'invoice');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
    <?= $this->render('_out-view-style') ?>
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<?php NavBar::begin([
    'brandLabel' => $this->render('out_view/store_label'),
    'brandUrl' => Yii::$app->params['serviceSite'],
    'brandOptions' => [
        'target' => '_blank',
    ],
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
    ],
]); ?>

<?= Nav::widget([
    'options' => [
        'id' => 'out-view-navbar',
        'class' => 'navbar-nav',
    ],
    'encodeLabels' => false,
    'items' => [
        [
            'label' => $this->render('out_view/pdf_label'),
            'url' => [
                "/documents/{$controllerId}/download",
                'type' => 'pdf',
                'uid' => $uid
            ],
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'target' => '_blank',
            ],
        ],
        [
            'label' => $this->render('out_view/word_label'),
            'url' => [
                "/documents/{$controllerId}/download",
                'type' => 'docx',
                'uid' => $uid
            ],
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'target' => '_blank',
            ],
        ],
        [
            'label' => $this->render('out_view/save_label'),
            'url' => '#',
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'id' => 'document-save-link',
                'data' => [
                    'toggle' => 'modal',
                ],
            ],
            'visible' => false //$this->params['canSave'],
        ],
        [
            'label' => $this->render('out_view/invoice_label'),
            'url' => ["/documents/{$controllerId}/out-create-invoice", 'uid' => $uid],
            'options' => [
                'class' => 'out-view-item-pay',
            ],
            'linkOptions' => [
            ],
        ],
    ],
]); ?>

<?php NavBar::end(); ?>

<?= NotificationFlash::widget([
        'options' => [
            'closeButton' => true,
            'showDuration' => 1000,
            'hideDuration' => 1000,
            'timeOut' => 5000,
            'extendedTimeOut' => 1000,
            'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
        ],
]); ?>

<div style="margin-top: 30px;">
    <?= $content ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
