<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\file\File;

/* @var $this yii\web\View */
/* @var $model common\models\document\AbstractDocument */
/* @var $file File */
$tooltipId = 'file-link-list-' . $model->id;
$previewId = 'file-link-view-' . $model->id;
$links = [];
$thumb = '';
$alias = '/documents/invoice';
if ($model instanceof \common\models\vehicle\Vehicle) {
    $alias = '/logistics/vehicle';
} elseif ($model instanceof \common\models\document\Upd) {
    $alias = '/documents/upd';
}
foreach ($model->files as $file) {
    if ($thumb == '' && in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
        $thumb = $file->getImageThumb(400, 600);
    }
    $links[] = Html::a($file->filename_full, [
        "$alias/file-get",
        'type' => isset($model->type) ? $model->type : \frontend\models\Documents::IO_TYPE_OUT,
        'id' => $model->id,
        'file-id' => $file->id,
    ], [
        'target' => '_blank',
    ]);
}
foreach ($model->scanDocuments as $scan) {
    $file = $scan->file;
    if ($thumb == '' && in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
        $thumb = $file->getImageThumb(400, 600);
    }
    $links[] = Html::a($file->filename_full, [
        '/documents/scan-document/file',
        'id' => $scan->id,
        'name' => $file->filename_full,
    ], [
        'target' => '_blank',
    ]);
}
$contentLinks = Html::beginTag('span', ['id' => $tooltipId]);
$contentLinks .= implode('<br/>', $links);
$contentLinks .= Html::endTag('span');

$js = <<<JS
$(document).on("click", "a.file-link-tooltip.tooltipstered", function () {
    var link = $(this);
    link.tooltipster("content", $("#"+link.data("linksid")));
    link.removeClass("content-hover");
    link.addClass("contend-click");

    setTimeout(function () {
        link.tooltipster("show");
    }, 100);
});
$(document).on({
    mouseenter: function () {
        $('a.file-link-tooltip.tooltipstered').tooltipster('hide');
        var link = $(this);
        var src = link.data("src");
        if (!link.hasClass("content-hover")) {
            var viewid = link.data("viewid");
            if($("#"+viewid).length == 0) {
                $('<img alt="" />').attr("id",viewid).attr("src",src).insertAfter("#"+link.data("linksid"));
            }
            link.tooltipster("content", $("#"+viewid));
            link.addClass("content-hover");
        }
        if (link.hasClass("contend-click")) {
            link.removeClass("contend-click");
        }
        if (src) {
            setTimeout(function () {
                link.tooltipster("show");
            }, 100);
        }
    },
    mouseleave: function () {
        $('.tooltipstered.content-hover').tooltipster('hide');
    }
}, "a.file-link-tooltip.tooltipstered");

$("a.file-link-tooltip").tooltipster({
    theme: ["tooltipster-kub"],
    contentCloning: true,
    contentAsHTML: true,
    trigger: "custom",
    side: ["right", "left", "top", "bottom"],
    content: '',
});
$(document).on('click', function() {
    $('a.file-link-tooltip.tooltipstered').tooltipster('hide');
})
JS;

$this->registerJs($js, View::POS_READY, '_doc-file-link');
?>

<?php if ($links) : ?>
    <?= Html::a('<span class="icon icon-paper-clip"></span>', null, [
        'class' => 'pull-center file-link-tooltip',
        'data' => [
            'src' => $thumb,
            'linksid' => $tooltipId,
            'viewid' => $previewId,
        ]
    ]); ?>
    <div class="hidden">
        <?= $contentLinks; ?>
    </div>
<?php endif; ?>
