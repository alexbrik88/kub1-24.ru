<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;

$this->beginContent('//layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

    <?php echo $content ?>

</div>

<?php $this->endContent(); ?>
