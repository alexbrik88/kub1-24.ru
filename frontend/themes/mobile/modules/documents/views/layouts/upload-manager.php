<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;

$this->context->layoutWrapperCssClass = 'upload-manager';
$this->beginContent('//layouts/main.php');
$activePage = Yii::$app->controller->action->id;
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

<div class="navbar-report navbar-default navbar">
    <div class="container-fluid" style="padding: 0;">
        <div class="navbar-header">
            <span class="navbar-brand" style="margin-left: 0;">
                <?php if ($activePage == 'upload'): ?>
                    <i><img src="/img/documents/download-black.png" width="28" style="margin-top:-4px"></i>
                <?php elseif ($activePage == 'all-documents'): ?>
                    <i><img src="/img/documents/all-documents-black.png" width="28" style="margin-top:-4px"></i>
                <?php elseif ($activePage == 'directory'): ?>
                    <i><img src="/img/documents/folder-black-2.png" width="28" style="margin-top:-7px"></i>
                <?php endif; ?>
                <?= $this->title ?>
            </span>
        </div>
    </div>
</div>

<?php echo $content ?>

</div>

<?php $this->endContent(); ?>
