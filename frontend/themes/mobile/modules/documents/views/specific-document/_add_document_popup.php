<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;

$model = new frontend\modules\documents\models\SpecificDocument();
?>

<style type="text/css">
    #add-document .btn {
        width: auto !important;
        color: #FFFFFF;
    }

</style>

<?php
Modal::begin([
    'options' => [
        'id' => $modalId,
    ],
    'header' => Html::tag('h1', $modalTitle),
]);

$form = \yii\bootstrap4\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'action' => \yii\helpers\Url::to('/documents/specific-document/create'),
    'method' => 'POST',

    'options' => [
        'id' => 'add-document',
        'enctype' => 'multipart/form-data'
    ],

    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-4 control-label',
        ],
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]));
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'name', ['wrapperOptions' => ['class' => 'col-md-8']])
            ->textInput(['class' => 'form-control']);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'description', ['wrapperOptions' => ['class' => 'col-md-8']])->textarea(['class' => 'form-control']); ?>
    </div>
</div>

<div class="row">
    <?= $form->field($model, 'file', [
        'template' => "{input}\n{hint}\n{error}",
        'options' => ['class' => 'col-sm-6'],
    ])->fileInput(['class' => 'js_filetype_doc']) ?>
    <div class="col-md-6">
        <button class="btn darkblue pull-right"><i class="upload-button glyphicon glyphicon-download-alt"></i> Загрузить</button>
    </div>
</div>
<?php
$form->end();
?>
<?php
Modal::end();
?>
