<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use frontend\rbac\permissions\document\Document;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

$is_budget_payment = (bool)$model->presence_status_budget_payment;
?>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?php if ($is_budget_payment) : ?>
                Бюджетный платеж.<br/>
                Получатель:
            <?php else : ?>
                Поставщик:
            <?php endif; ?>
        </span>
        <?= $model->contractor ? Html::a($model->contractor_name, [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], ['class' => 'link']) : $model->contractor_name; ?>
    </div>
    <?php if (!$is_budget_payment) : ?>
        <?php $invoiceArray = $model->getPaymentOrderInvoices()->joinWith('invoice')->orderBy([
            'cast(document_number as unsigned)' => SORT_ASC,
        ])->all() ?>
        <div class="about-card-item">
            <span class="text-grey">
                Счета в платежном поручении:
            </span>
            <?php foreach ($invoiceArray as $paymentOrderInvoice) : ?>
                <?php $label = '№ '.$paymentOrderInvoice->invoice->fullNumber.' от '.DateHelper::format(
                    $paymentOrderInvoice->invoice->document_date,
                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE
                ) ?>
                <div>
                    <?php if ($paymentOrderInvoice->invoice && Yii::$app->user->can(Document::VIEW, [
                        'model' => $model,
                    ]) ): ?>
                        <?= Html::a($label, [
                            '/documents/invoice/view',
                            'id' => $paymentOrderInvoice->invoice->id,
                            'type' => $paymentOrderInvoice->invoice->type,
                        ], ['class' => 'link']); ?>
                    <?php else: ?>
                        <?= $label ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
