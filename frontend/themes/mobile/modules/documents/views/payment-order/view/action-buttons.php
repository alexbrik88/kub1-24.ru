<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.05.2018
 * Time: 20:32
 */

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

$items = [
    [
        'label' => '<span style="display: inline-block;">Импорт в файл</span>',
        'encode' => false,
        'url' => ['import', 'id' => $model->id],
        'linkOptions' => [
            'target' => '_blank',
        ]
    ],
];
if ((Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
    ($paymentAccount = $model->company->getBankingPaymentAccountants()->one()) !== null &&
    ($alias = Banking::aliasByBik($paymentAccount->bik))
) {
    $items[] = [
        'label' => '<span style="display: inline-block;">Отправить в банк</span>',
        'encode' => false,
        'linkOptions' => [
            'class' => 'banking-module-open-link',
        ],
        'url' => [
            "/cash/banking/{$alias}/default/payment",
            'account_id' => $paymentAccount->id,
            'po_id' => $model->id,
            'p' => Banking::routeEncode(['/documents/payment-order/view', 'id' => $model->id]),
        ],
    ];
}
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'building']).'<span>Импорт</span>', '#', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100 no-after',
                    'data-toggle' => 'dropdown'
                ]); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr'
                    ],
                    'items' => $items,
                ]); ?>
                <?= BankingModalWidget::widget([
                    'pageTitle' => $this->title,
                    'pageUrl' => Url::to(['view', 'id' => $model->id]),
                ]) ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => Documents::IO_TYPE_IN,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-hover-transparent w-full',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'pdf']).'<span>PDF</span>', [
                    'document-print',
                    'actionType' => 'pdf',
                    'id' => $model->id,
                    'type' => Documents::IO_TYPE_IN,
                    'filename' => $model->getPdfFileName(),
                ], [
                    'target' => '_blank',
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::VIEW)
                && Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::CREATE)
            ): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите скопировать это платёжное поручение?',
                ]);
                ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::DELETE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить платежное поручение?',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
