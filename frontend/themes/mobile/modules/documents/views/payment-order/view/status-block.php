<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: #26cd58;
            border-color: #26cd58;
            color: #ffffff;
        ">
            <?= $this->render('//svg-sprite', ['ico' => 'check-2']) ?>
            <span class="ml-3"><?= ArrayHelper::getValue($model, 'paymentOrderStatus.name') ?></span>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->payment_order_status_updated_at) ?></span>
        </div>
    </div>
</div>