<?php
use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\PackingList;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use common\models\product\Product;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use \frontend\themes\mobile\helpers\Icon;

/* @var \yii\web\View $this */
/* @var PackingList $model */
/* @var $message Message */
/* @var string $dateFormatted */

$productionType = explode(', ', $model->invoice->production_type);

$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$model->invoice->fullNumber}&{$model->invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$agreementDropDownList = ['' => '   '] + ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '] +
    [$invoiceItemValue => $invoiceItemName] + \common\components\helpers\ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');

$consignorArray = $model->consignorArray;
$model->consignor_id .= '';
$model->consignee_id .= '';
?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Входящая ТН
                        </span>
                    </span>
                    <span>№</span>
                </div>
                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <!-- document_date -->
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row d-block">

        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Транспортная накладная №</label>
                    <?= Html::activeTextInput($model, 'waybill_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'номер',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">От</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'waybill_date', [
                            'class' => 'form-control date-picker']) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Грузоотправитель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignor_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $consignorArray,
                'options' => [
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Доверенность</label>
                    <?= Html::activeTextInput($model, 'proxy_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'номер',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">От</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'proxy_date', [
                            'class' => 'form-control date-picker']) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Должность</label>
            <?= Html::activeTextInput($model, 'given_out_position', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => '',
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">ФИО</label>
            <?= Html::activeTextInput($model, 'given_out_fio', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => '',
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Грузополучатель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignee_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $consignorArray,
                'options' => [
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Покупатель<span class="important">*</span></label>
                <div class="row">
                    <div class="col-6">
                        <?php echo Select2::widget([
                            'name' => 'contractor_id',
                            'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                            'disabled' => true,
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Основание</label>
                <div class="row">
                    <div class="col-6">
                        <?php Pjax::begin([
                            'id' => 'agreement-pjax-container',
                            'enablePushState' => false,
                            'linkSelector' => false,
                        ]);
                        echo Select2::widget([
                            'model' => $model,
                            'attribute' => 'agreement',
                            'data' => $agreementDropDownList,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '100%',
                                'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            ],
                        ]);
                        Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6 mb-0">
            <?= Html::activeCheckbox($model, 'add_stamp', [
                'class' => 'kub-switch',
                'label' => false,
            ]); ?>
            <label class="label ml-1" for="packinglist-add_stamp">Печать и подпись</label>
            <span class="tooltip-hover ico-question valign-middle"
                  data-tooltip-content="#tooltip_add_stamp"
                  style="vertical-align: top;">
                </span>
        </div>

    </div>
</div>
