<?php

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\components\Icon;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\UpdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;

$exists = Invoice::find()->joinWith('upd', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет УПД. Измените период, чтобы увидеть имеющиеся УПД.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного УПД.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => UpdStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => UpdStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if ($ioType == Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ],
    ];
}

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_upd' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');
?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()) : ?>
        <?php
        Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как подготовить УПД, нужно создать Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php
        Modal::end();

        ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как подготовить УПД,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
            .
        </div>
    <?php endif; ?>
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер УПД, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('Upd[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $data->id,
                        'data-sum' => $data->totalAmountWithNds,
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата УПД',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ УПД',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $data,
                    ])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber;
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '70px',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'totalAmountWithNds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],

            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, Upd::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],

            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->statusOut->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],

            [
                'attribute' => 'invoice.document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])
                    ) {
                        if ($data->invoice->file !== null) {
                            $invoiceFileLinkClass = null;
                            $tooltipId = null;
                            $contentPreview = null;
                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                if ($thumb) {
                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                    $contentPreview .= Html::endTag('span');
                                    $contentPreview .= Html::endTag('div');
                                }
                            }

                            return Html::a($data->invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                            ]) . Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/invoice/file-get',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                                'file-id' => $data->invoice->file->id,
                            ], [
                                'class' => $invoiceFileLinkClass,
                                'target' => '_blank',
                                'data-tooltip-content' => '#' . $tooltipId,
                                'style' => 'float: right',
                            ]) . $contentPreview;
                        } else {
                            return Html::a($data->invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                            ]);
                        }
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>'. Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr'
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_UPD,
    'documentTypeName' => 'УПД'
]) ?>

<?php if ($canDelete) : ?>
    <?php \yii\bootstrap4\Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные товарные накладные?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php \yii\bootstrap4\Modal::end(); ?>
<?php endif ?>
