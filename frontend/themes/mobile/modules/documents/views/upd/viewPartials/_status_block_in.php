<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $useContractor string */

?>
<?php \yii\widgets\Pjax::begin([
    'id' => 'is-original-pjax-container',
    'enablePushState' => false,
    'linkSelector' => '.is-original-button',
]); ?>
<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: #26cd58;
            border-color: #26cd58;
            color: #ffffff;
        ">
            <?php if ($model->is_original) : ?>
                <?= $this->render('//svg-sprite', ['ico' => 'new-doc']) ?>
                <span class="ml-3">Оригинал</span>
            <?php else : ?>
                <i class="icon pull-left fa fa-copy"></i>
                <span class="ml-3">Копия</span>
            <?php endif; ?>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->is_original_updated_at ? : $model->created_at) ?></span>
        </div>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
    'model' => $model->invoice,
])) : ?>
    <?= Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span class="ml-3 mr-1">СЧЕТ</span>', [
        '/documents/invoice/view',
        'type' => $model->type,
        'id' => $model->invoice->id,
    ], [
        'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3',
    ]) ?>
<?php endif; ?>