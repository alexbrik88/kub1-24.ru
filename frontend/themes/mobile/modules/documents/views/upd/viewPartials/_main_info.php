<?php

use frontend\modules\documents\components\Message;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message frontend\modules\documents\components\Message; */

$invoice = $model->invoice;
$basis = '';
if ($invoice->basis_document_name &&
    $invoice->basis_document_number &&
    $invoice->basis_document_date
) {
    $basis = $invoice->basis_document_name;
    $basis .= ' № '. Html::encode($invoice->basis_document_number);
    $basis .= ' от ' . date('d.m.Y', strtotime($invoice->basis_document_date));
} else {
    $basis = 'Счет № '. $invoice->fullNumber;
    $basis .= ' от ' . date('d.m.Y', strtotime($invoice->basis_document_date));
}
?>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($invoice->contractor_name_short, [
            '/contractor/view',
            'type' => $invoice->contractor->type,
            'id' => $invoice->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Основание:</span>
        <span>
            <?= $basis ?>
        </span>
    </div>
    <?php if ($model->type == \frontend\models\Documents::IO_TYPE_OUT): ?>
    <div class="about-card-item">
        <label class="label" for="add_stamp">Печать и подпись:</label>
        <div>
            <?= \yii\bootstrap4\Html::checkbox('add_stamp', $model->add_stamp, [
                'id' => 'add_stamp',
                'class' => 'kub-switch'
            ]) ?>

            <button type="button" class="button-clr ml-2" data-toggle="tooltip" data-placement="bottom" title="Добавить в УПД печать и подпись при отправке по e-mail и при скачивании в PDF">
                <svg class="tooltip-question-icon svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use>
                </svg>
            </button>
        </div>
    </div>
    <?php endif; ?>
    <div class="about-card-item">
        <?= \frontend\themes\mobile\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>

<?php
$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize());
    });
');