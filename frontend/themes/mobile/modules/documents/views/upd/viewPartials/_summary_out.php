<?php
use common\components\TextHelper;
use common\models\document\OrderPackingList;
use common\models\product\Product;

/** @var \common\models\document\Upd $model */
/* @var \common\models\Company $company */

$totalQuantity = $model->getOwnOrders()->joinWith('product')->andWhere([
    'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
])->sum('order_upd.quantity');

?>

<table class="table table-resume">
    <tbody>
    <tr role="row" class="odd">
        <td><b>Итого:</b></td>
    </tr>
    <tr role="row" class="even">
        <td>Кол-во (объем):</td>
        <td><?= $totalQuantity * 1 ?></td>
    </tr>
    <?php if ($model->invoice->hasNds): ?>
        <tr role="row" class="odd">
            <td>Сумма без учёта НДС:</td>
            <td><?php echo TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2); ?></td>
        </tr>
        <tr role="row" class="odd">
            <td>Сумма НДС:</td>
            <td><?php echo TextHelper::invoiceMoneyFormat($model->getTotalNds(), 2); ?></td>
        </tr>
    <?php else: ?>
        <tr role="row" class="odd">
            <td>Без налога (НДС):</td>
            <td>-</td>
        </tr>
    <?php endif; ?>
    <tr role="row" class="even">
        <td>Сумма с учётом НДС:</td>
        <td><?php echo TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
    </tr>
    </tbody>
</table>
