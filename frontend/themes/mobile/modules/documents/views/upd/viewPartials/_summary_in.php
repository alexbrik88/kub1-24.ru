<?php
use common\components\TextHelper;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;

/* @var PackingList $model */
/** @var \common\models\document\OrderPackingList $order */
$order = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);
?>

<table class="table table-resume">
    <tbody>
    <tr role="row" class="even">
        <td class="bold-text">Итого:</td>
        <td><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
    </tr>
    <?php if ($model->invoice->hasNds): ?>
        <tr role="row" class="odd">
            <td class="bold-text">В том числе НДС:</td>
            <td><?= TextHelper::invoiceMoneyFormat($model->getTotalNds(), 2); ?></td>
        </tr>
        <tr role="row" class="even">
            <td class="bold-text">Всего к оплате:</td>
            <td><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
