<?php
/** @var \common\models\document\Upd $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
?>

<div class="col-lg-5 col-w-lg-4 col-md-5 control-panel control-panel-pre-edit margin991 pull-right">

    <?php \yii\widgets\Pjax::begin([
        'id' => 'is-original-pjax-container',
        'enablePushState' => false,
        'linkSelector' => '.is-original-button',
    ]); ?>
    <div style="text-align: right;">
        <div class="document-panel btn btn-status darkblue width179" title="Дата изменения статуса" style="display: inline-block;">
            <?=  date("d.m.Y", $model->is_original_updated_at ? $model->is_original_updated_at : $model->created_at); ?>
        </div>
        <div class="document-panel btn btn-status darkblue width179" title="Статус" style="display: inline-block;">
            <?php if ($model->is_original) : ?>
                <span class="icon pull-left icon-original-document"></span>Оригинал
            <?php else : ?>
                <span class="icon pull-left fa fa-copy"></span>Копия
            <?php endif; ?>
        </div>
    </div>
    <?php \yii\widgets\Pjax::end(); ?>

    <div class="document-panel widthe-i">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
            'model' => $model->invoice,
        ])): ?>
            <a href="<?php echo \yii\helpers\Url::to(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id, 'contractorId' => $contractorId,]); ?>"
               class="btn btn-account yellow pull-right m-t-0">
                <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                СЧЕТ
            </a>
        <?php endif; ?>
    </div>
</div>
