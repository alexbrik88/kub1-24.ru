<?php

use common\components\helpers\ArrayHelper;
use common\models\document\Order;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$company = Yii::$app->user->identity->company;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$invoiceOrderCount = Order::find()
    ->andWhere(['in', 'invoice_id', ArrayHelper::getColumn($model->invoices, 'id')])
    ->count();
$hideAddBtn = $invoiceOrderCount === count($model->ownOrders);
$precision = $model->invoice->price_precision;
?>

<?php if ($backUrl !== null) : ?>
    <?= \yii\helpers\Html::a('Назад к списку', $backUrl, [
        'class' => 'link mb-2',
    ]); ?>
<?php endif ?>
<div class="wrap wrap_padding_small mb-2 position-relative">
    <div class="page-in row">
        <div class="col-12 column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, [
                        'model' => $model,
                    ])
                ) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'type' => $ioType,
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->invoice->contractor_id : null),
                    ], [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                        'title' => 'Редактировать',
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container">
                    <div style="overflow-x: auto;">
                        <div style="width: 1200px; position: relative;">
                            <?= $this->render('/upd/template', [
                                'model' => $model,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar page-in-sidebar_absolute pl-3 column pl-3">
            <?= $this->render('viewPartials/_status_block_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'ioType' => $model->type,
                'useContractor' => $useContractor,
            ]); ?>
            <?= $this->render('viewPartials/_main_info', [
                'model' => $model,
                'message' => $message,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>
