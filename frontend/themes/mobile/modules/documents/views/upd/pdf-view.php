<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\product\Product;
use php_rutils\RUtils;
use frontend\assets\UpdAsset;
use yii\helpers\Html;
use common\models\document\Upd;
use common\components\image\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';

?>

<style type="text/css">
    @media print{@page {size: landscape}}
</style>

<div style="padding: 20px 30px;">
    <?= $this->render('template', [
        'model' => $model,
    ]); ?>
</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>
