<?php

use common\components\TextHelper;
use common\models\product\Product;

/* @var $this yii\web\View */
/* @var $key integer */
/* @var $hasNds boolean */
/* @var $order common\models\document\Order */
/* @var $model common\models\document\Upd */

$order = $orderUpd->order;
$product = $order->product;
$isGoods = ($product->production_type == Product::PRODUCTION_TYPE_GOODS);
if ($orderUpd->quantity != intval($orderUpd->quantity)) {
    $orderUpd->quantity = rtrim(number_format($orderUpd->quantity, 10, '.', ''), 0);
}
$hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
?>

<tr>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
        <?= $key + 1 ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
        <?= $product->code ? : Product::DEFAULT_VALUE ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: left; padding-left: 14px;">
        <?= $order->product_title; ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td style="border-right: 1px solid #000;width:26%;">
                    <?= (!$hideUnits && $product->productUnit) ?
                        $product->productUnit->code_okei :
                        Product::DEFAULT_VALUE ?>
                </td>
                <td>
                    <?= (!$hideUnits && $product->productUnit) ?
                        $product->productUnit->name :
                        Product::DEFAULT_VALUE ?>
                </td>
            </tr>
        </table>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= ($hideUnits && $orderUpd->quantity == 1) ? Product::DEFAULT_VALUE : strtr($orderUpd->quantity, ['.' => ',']); ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= $hideUnits ? Product::DEFAULT_VALUE : TextHelper::invoiceMoneyFormat($orderUpd->priceNoNds, $precision) ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= TextHelper::invoiceMoneyFormat($orderUpd->amountNoNds, $precision); ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= $order->excise ? TextHelper::invoiceMoneyFormat($order->excise_price, 2) : 'без&nbsp;акциза'; ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= $hasNds ? $order->saleTaxRate->name : Product::DEFAULT_VALUE ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= $hasNds ? TextHelper::invoiceMoneyFormat($orderUpd->amountNds, $precision) : Product::DEFAULT_VALUE ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <?= TextHelper::invoiceMoneyFormat($orderUpd->amountWithNds, $precision) ?>
    </td>
    <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td style="border-right: 1px solid #000;width: 41%;">
                    <?= $order->country->code ? $order->country->code : Product::DEFAULT_VALUE ?>
                </td>
                <td>
                    <?= $order->country->name_short ? $order->country->name_short : Product::DEFAULT_VALUE ?>
                </td>
            </tr>
        </table>
    </td>
    <td style="border-bottom: 1px solid #000;">
        <?= $order->custom_declaration_number ? $order->custom_declaration_number : Product::DEFAULT_VALUE ?>
    </td>
</tr>