<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use frontend\models\Documents;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;
use \frontend\themes\mobile\helpers\Icon;

/* @var \common\models\document\Act $model */
/* @var $message Message */
/* @var $actEssence \common\models\document\ActEssence */

$productionType = explode(', ', $model->invoice->production_type);
$invoice = $model->invoice;
$agreementArray = $invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$invoice->fullNumber}&{$invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $invoice->fullNumber . ' от ' .
    DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$agreementDropDownList = ['' => '   '] + ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');
?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Исходящий акт
                        </span>
                    </span>
                    <span>№</span>
                </div>
                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= Html::activeTextInput($model, 'document_additional_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                            'placeholder' => 'доп. номер',
                            'style' => 'display: inline;',
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <!-- document_date -->
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Покупатель<span class="important">*</span></label>
                <div class="row">
                    <div class="col-6">
                        <?php echo Select2::widget([
                            'name' => 'contractor_id',
                            'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                            'disabled' => true,
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Основание</label>
                <div class="row">
                    <div class="col-6">
                        <?php Pjax::begin([
                            'id' => 'agreement-pjax-container',
                            'enablePushState' => false,
                            'linkSelector' => false,
                        ]);
                        echo Select2::widget([
                            'model' => $model,
                            'attribute' => 'agreement',
                            'data' => $agreementDropDownList,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '100%',
                                'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            ],
                        ]);
                        Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs('
$(document).on("change", "#act-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/documents/agreement/create',
        'contractor_id' => $model->invoice->contractor_id,
        'type' => $model->type,
        'returnTo' => 'act',
        'container' => 'agreement-select-container',
    ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            // $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
        });
        $("#agreement-modal-container").modal("show");
        $("#act-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#act-agreement").val(window.AgreementValue).trigger("change");
    }
});
'); ?>
