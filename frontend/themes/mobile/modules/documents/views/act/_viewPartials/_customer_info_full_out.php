<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/* @var \common\models\document\Act $model */
/* @var $message Message */
/* @var string $dateFormatted */
/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$productionType = explode(', ', $model->invoice->production_type);

$invoice = $model->invoice;
$company = $model->company;
$contractor = $invoice->contractor;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$director_name = '';
if (!empty($invoice->contractor_director_name) && $invoice->contractor_director_name != 'ФИО Руководителя') {
    $director_name = TextHelper::nameShort($invoice->contractor_director_name);
} elseif (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
    $director_name = TextHelper::nameShort($contractor->director_name);
}

if ($model->signed_by_name) {
    $signatureLink = (!$model->employeeSignature) ? false :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$company->chief_signature_link) ? false :
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
}

$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$amount = $isNdsExclude ? $model->totalAmountNoNds : $model->totalAmountWithNds;
$ndsName = $invoice->ndsViewType->name;
$ndsValue = $invoice->hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-';
$precision = $model->invoice->price_precision;

$printLink = EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$isAuto = $model->invoice->invoice_status_id == \common\models\document\status\InvoiceStatus::STATUS_AUTOINVOICE;

/* add logo */
$images['logo'] = ['tab' => 0, 'link' => $logoLink, 'name' => '+ Добавить логотип'];
$images['print'] = ['tab' => 1, 'link' => $printLink, 'name' => '+ Добавить печать'];
$images['chief'] = ['tab' => 2, 'link' => $signatureLink, 'name' => '+ Добавить подпись'];
?>

<!-- Modal: add logo -->
<?php if ($images) {
    $this->registerJs('
        $(document).on("click", ".companyImages-button", function(event) {
            var tabId = parseInt($(this).data("tab"));
            $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
        });

        $(document).on("hidden.bs.modal", "#modal-companyImages", function() {
            location.reload();
        });
    ');
}
?>
<?php if ($images) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'title' => 'Загрузить логотип, печать и подпись',
        'titleOptions' => [
            'class' => 'mb-0',
        ],
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>
<!-- Modal: add logo. end -->
<style type="text/css">
    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }

    .pre-view-table h4 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }

    .auto_tpl {
        font-style: italic;
        font-weight: normal;
    }

    .m-size-div div {
        font-size: 12px;
    }

    .m-size-div div span {
        font-size: 9px;
    }

    .file_upload_block {
        text-align: center;
        border: 2px dashed #ffb848;
        color: #ffb848;
        padding: 12px;
    }

    .no_min_h {
        min-height: 0px !important;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="min-width: 520px; max-width: 735px; border: 1px solid rgb(66, 118, 164); margin-top: 3px; min-height: 430px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 30px;">
            <div class="col-xs-6 pad0">
                <div class="col-xs-12 pad0 actions mart-m-8">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                    ]); ?>

                    <?php if ($canUpdate) : ?>
                        <a href="<?= \yii\helpers\Url::to(['update', 'type' => $ioType, 'id' => $model->id]); ?>"
                           title="Редактировать" class="btn darkblue btn-sm" style="height: 27px;margin-left: 3px">
                            <i class="icon-pencil"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad0" style="padding-top: 5px !important; border-bottom: 2px solid #000000">
            <h4 class="font-bold" style="padding: 0; margin: 0; text-align: center; padding-bottom:10px">Акт
                № <?= $model->fullNumber; ?>
                от <?= $dateFormatted; ?></h4>
        </div>
        <div class="col-xs-12 padd10x0"
        <div class="col-xs-12 pad0">
            <div class="col-xs-2 pad0"> Исполнитель:</div>
            <div class="col-xs-10 pad0 bold padd-l-3">
                <?= $model->getExecutorFullRequisites() ?>
            </div>
        </div>
        <div class="col-xs-12 pad0">
            <div class="col-xs-2 pad0"> Заказчик:</div>
            <div class="col-xs-10 pad0 padd-l-3 bold">
                <?= $model->getCustomerFullRequisites() ?>
            </div>
        </div>
        <div class="col-xs-12 padd10x0">
            <div class="col-xs-2 pad0"> Основание:</div>
            <div class="col-xs-10 pad0 padd-l-3 bold">
                <?= $model->getBasisName(); ?>
            </div>
        </div>
        <div class="col-xs-12 pad3" style="border-bottom: 2px solid #000000;">
            <style>
                .bord-dark tr, .bord-dark td, .bord-dark th {
                    border: 1px solid #000000;
                }
            </style>
            <div>
                <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
                    <thead>
                    <tr style="text-align: center;">
                        <th> №</th>
                        <th> Наименование работ, услуг</th>
                        <th> Кол-во</th>
                        <th> Ед.</th>
                        <th> Цена</th>
                        <th> Сумма</th>
                    </tr>
                    </thead>
                    <tbody class="bord-dark">
                    <?php foreach ($model->getOrderActs()->all() as $key => $order): ?>
                        <?php
                        $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;
                        $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
                        $orderAmount = $isNdsExclude ? $order->amountNoNds : $order->amountWithNds;
                        ?>
                        <tr>
                            <td style="text-align: center; width: 5%"><?= $key + 1; ?></td>
                            <td style=" width: 0"><?= $order->order->product_title; ?></td>
                            <td style="text-align: right; width: 10%">
                                <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                            </td>
                            <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                            <td style="text-align: right; width: 13%"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
                            <td style="text-align: right; width: 15%"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <table class="it-b" style="width: 99.9%; margin-top:10px">
                    <tbody>
                    <tr>
                        <td width="450px" style="border: none"></td>
                        <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;">
                            Итого:
                        </td>
                        <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td width="430px" style="border: none"></td>
                        <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;"><?= $ndsName; ?>
                            :
                        </td>
                        <td class="txt-b2" style="text-align: right; border: none; width: 124px;"><?= $ndsValue; ?></td>
                    </tr>
                    <tr>
                        <td class="txt-b2" style="text-align: left; border: none">
                            Всего оказано услуг <?= $model->getOrderActs()->count(); ?>,
                            на сумму <?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> руб.
                        </td>
                    </tr>
                    <tr>
                        <td class="bold">
                            <?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->totalAmountWithNds / 100)); ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-12 padd10x0">
            <?= nl2br(Html::encode($model->comment)) ?>
        </div>
        <div class="col-xs-12 padd10x0">
            <div style="height: 200px;">
                <div style="position: relative">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                ИСПОЛНИТЕЛЬ
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                ЗАКАЗЧИК
                            </td>
                        </tr>
                        <tr>
                            <td style="position:relative; z-index:2; width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $invoice->company_name_short; ?></td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $invoice->contractor_name_short; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 49%; height: 34px; border: none; text-align: right; vertical-align: bottom;display: block;width: 100%;position: relative;">
                                <?php if (!empty($images['chief']['link'])) { ?>
                                    <div style="position: absolute;left: 0px;">
                                        <img style="max-width:130px" src="<?= $images['chief']['link'] ?>" alt="">
                                    </div>
                                <?php } ?>
                                <br/>
                                <div style="border-bottom: 1px solid #000; z-index:2; position:relative">
                                    / <?= $model->signed_by_name ? $model->signed_by_name : $invoice->getCompanyChiefFio(true) ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                    /
                                </div>

                                <?php if (!empty($images['print']['link'])): ?>
                                    <div style="text-align:center;position:absolute; right: 0px; z-index:<?= !empty($images['print']['link']) ? '1; bottom:-85px;' : '100;top:-16px;' ?>">
                                        <img class="img-responsive" style="max-width: 150px;"
                                             src="<?= $images['print']['link'] ?>" alt="">
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>
                                    <br>
                                    <br>
                                <?php endif; ?>
                                <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin-bottom:100px"></div>
