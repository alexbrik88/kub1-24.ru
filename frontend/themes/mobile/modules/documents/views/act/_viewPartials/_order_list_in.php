<?php

use common\models\document\Invoice;

/**
 * @var $this \yii\web\View
 * @var $model \common\models\document\Act
 */

if (empty($precision)) {
    $precision = $model->invoice->price_precision;
}
?>

<table class="table table-style table-count in-act_table">
    <thead>
    <tr class="heading" role="row">
        <th width="1%"></th>
        <th width="10%">Наименование</th>
        <th width="6%">Количество</th>
        <th width="5%">Ед.измерения</th>
        <?php if ($model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT): ?>
            <th width="5%">Ставка</th>
        <?php endif; ?>
        <th width="5%">Цена</th>
        <th width="5%">Сумма</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php foreach ($model->orderActs as $key => $order): ?>
        <?php echo
        $this->render('_template_in', [
            'key' => $key,
            'order' => $order,
            'model' => $model,
            'precision' => $precision,
        ]);
        ?>
    <?php endforeach; ?>
    </tbody>
</table>
