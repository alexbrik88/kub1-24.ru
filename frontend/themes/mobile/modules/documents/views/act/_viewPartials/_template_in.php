<?php
use common\components\TextHelper;
use common\components\helpers\Html;
use common\models\document\Invoice;
use common\models\product\Product;

$company = \Yii::$app->user->identity->company;
?>
<?php if (isset($order)): ?>
    <?php
    $order_id = $order->order_id;
    $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
    $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
    ?>
    <tr role="row" class="odd order">
        <?php $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?>
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td><?= $order->order->product_title; ?></td>
        <td>
            <?= Html::hiddenInput("OrderAct[{$order_id}][order_id]", $order_id, [
                'class' => 'quantity',
                'data-value' => 1,
            ]) ?>
            <?php if ($unitName == Product::DEFAULT_VALUE) : ?>
                <?= Html::hiddenInput('OrderAct[' . $order_id . '][quantity]', 1, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
                <span><?= Product::DEFAULT_VALUE ?></span>
            <?php else : ?>
                <?= Html::input('number', "OrderAct[{$order_id}][quantity]", $order->quantity, [
                    'min' => 1,
                    'class' => 'input-editable-field quantity hide form-control-number product-count',
                ]) ?>
            <?php endif ?>
        </td>
        <td><?= $unitName ?></td>
        <?php if ($model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT): ?>
            <td><?= $order->order->purchaseTaxRate->name; ?></td>
        <?php endif; ?>
        <?php if ($model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) : ?>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceNoNds, $precision); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
        <?php else : ?>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceWithNds, $precision); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
        <?php endif; ?>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td><?= Html::dropDownList('test', null, $result, ['id' => 'choose-product-in-table', 'class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <?php if ($model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT) : ?>
            <td></td>
        <?php endif ?>
    </tr>
<?php endif; ?>