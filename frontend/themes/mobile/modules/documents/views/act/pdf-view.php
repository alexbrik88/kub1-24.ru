<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

// $addStamp = 1;
$invoice = $model->invoice;
$company = $invoice->company;
$contractor = $invoice->contractor;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$director_name = '';
if ($contractor->director_in_act) {
    if (!empty($invoice->contractor_director_name) && $invoice->contractor_director_name != 'ФИО Руководителя') {
        $director_name = TextHelper::nameShort($invoice->contractor_director_name);
    } elseif (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
        $director_name = TextHelper::nameShort($contractor->director_name);
    }
} elseif ($contractor->chief_accountant_in_act) {
    if (!empty($contractor->chief_accountant_name)) {
        $director_name = TextHelper::nameShort($contractor->chief_accountant_name);
    }
} elseif ($contractor->contact_in_act) {
    if (!empty($contractor->contact_name)) {
        $director_name = TextHelper::nameShort($contractor->contact_name);
    }
}

if ($model->signed_by_name) {
    $signatureLink = (empty($addStamp) || $model->employeeSignature === null) ? false :
        (EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 145, 50, EasyThumbnailImage::THUMBNAIL_INSET) ? : false);
} else {
    $signatureLink = empty($addStamp) ? false :
        (EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 145, 50, EasyThumbnailImage::THUMBNAIL_INSET) ? : false);
}

$printLink = empty($addStamp)? false :
    (EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET) ? : false);

$signaturePosition = $model->type == Documents::IO_TYPE_OUT ? 'background-position: 0 50px;' : 'background-position: 350px 50px;';
$printPosition = $model->type == Documents::IO_TYPE_OUT ? 'background-position: 145px 0;' : 'background-position: 500px 0;';
$signatureLinkStyle = $signatureLink ? " background: url($signatureLink); background-repeat: no-repeat; {$signaturePosition}" : '';
$printLinkStyle = $printLink ? " background: url($printLink); background-repeat: no-repeat; {$printPosition}" : '';

$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$amount = $isNdsExclude ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds();
$ndsName = $invoice->ndsViewType->name;
$ndsValue = $invoice->hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-';
$precision = $model->invoice->price_precision;
?>

<div class="page-content-in p-center pad-pdf-p">
    <div>
        <h4 class="font-bold" style="padding: 0; margin: 0; text-align: center;">Акт
            № <?= $model->fullNumber; ?>
            от <?= $dateFormatted; ?></h4>
    </div>
    <br/>
    <table class="table table-bordered no-b" style="margin: 5px 0 5px 0">
        <tr>
            <td width="10%" class="txt-t m-l" style="vertical-align:top">Исполнитель:</td>
            <td width="90%" class="txt-b">
                <?= $model->getExecutorFullRequisites() ?>
            </td>
        </tr>
        <tr>
            <td width="10%" class="txt-t m-l" style="vertical-align:top">Заказчик:</td>
            <td width="90%" class="txt-b">
                <?= $model->getCustomerFullRequisites() ?>
            </td>
        </tr>
        <tr>
            <td width="10%" class="txt-t m-l">Основание:</td>
            <td width="90%" class="txt-b">
                <?= $model->getBasisName(); ?>
            </td>
        </tr>
    </table>
    <div class="portlet">
        <table class="table table-bordered table-act"
               style="border: 2px solid #000000; margin-bottom: 5px">
            <thead>
            <tr class="heading">
                <th width="5%">№</th>
                <th width="51%">Наименование работ, услуг</th>
                <th width="1px">Кол-вo</th>
                <th width="1px">Ед</th>
                <th width="12%">Цена</th>
                <th width="12%">Сумма</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->getOrderActs()->all() as $key => $order): ?>
                <?php
                $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;
                $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
                $orderAmount = $model->getPrintOrderAmount($order->order_id, $isNdsExclude);
                ?>
                <tr>
                    <td style="text-align: center"><?= $key + 1; ?></td>
                    <td><?= $order->order->product_title; ?></td>
                    <td style="text-align: right">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                    </td>
                    <td style="text-align: right"><?= $unitName ?></td>
                    <td style="text-align: right"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
                    <td style="text-align: right"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <table>
            <tbody>
            <tr>
                <td width="450px" style="border: none"></td>
                <td style="text-align: right; border: none; width: 22%"><b>Итого:</b>
                </td>
                <td style="text-align: right; border: none; width:12%">
                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </td>
            </tr>
            <tr>
                <td width="430px" style="border: none"></td>
                <td style="text-align: right; border: none"><b><?= $ndsName; ?>:</b></td>
                <td style="text-align: right; border: none"><?= $ndsValue; ?></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div>
        <div style="font-size: 8pt">
            Всего оказано услуг <?= $model->getOrderActs()->count(); ?>,
            на сумму <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?> руб.
        </div>
        <div style="font-size: 9pt;font-weight: bold" class="font-bold">
            <?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->getPrintAmountWithNds() / 100)); ?>
        </div>
    </div>

    <br/>

    <div style="font-size: 9pt;">
        <?= nl2br(Html::encode($model->comment)) ?>
    </div>
    <br/>

    <div style="height: 200px;<?= $printLinkStyle ?>">
        <div style="height: 200px;<?= $signatureLinkStyle ?>">
            <table style="width: 100%;">
                <tr>
                    <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                        ИСПОЛНИТЕЛЬ
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                        ЗАКАЗЧИК
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            <?= $invoice->company_name_short; ?>
                        <?php else: ?>
                            <?= $invoice->contractor_name_short; ?>
                        <?php endif; ?>
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            <?= $invoice->contractor_name_short; ?>
                        <?php else: ?>
                            <?= $invoice->company_name_short; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            / <?= $model->signed_by_name ? : $invoice->getCompanyChiefFio(true) ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                            /
                        <?php else: ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php endif; ?>
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                            / <?= $model->signed_by_name ? : $invoice->getCompanyChiefFio(true) ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                            /
                        <?php else: ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>
