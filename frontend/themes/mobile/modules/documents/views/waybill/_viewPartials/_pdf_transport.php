<?php
use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\document\OrderWaybill;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\Waybill;
use common\models\product\ProductUnit;

/* @var $this yii\web\View */
/* @var $model common\models\document\Waybill */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$invoiceItemName = '';
$invoiceFactureItemName = '';
$packingListItemName = '';

$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if ($model->invoice->invoiceFacture)
    $invoiceFactureItemName = 'Счет-фактура № ' . $model->invoice->invoiceFacture->fullNumber . ' от ' .
        DateHelper::format($model->invoice->invoiceFacture->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if ($model->invoice->packingList)
    $packingListItemName = 'ТН № ' . $model->invoice->packingList->fullNumber . ' от ' .
        DateHelper::format($model->invoice->packingList->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$hasMass = $realMassGross && $realMassNet;
?>

<style>
    .ttn .table {margin:0;width:100%;font-family: Arial, sans-serif;}
    .ttn {padding-top: 1px}
    .ttn td {font-size: 7pt;padding: 1px;line-height: 8pt;}
    .ttn td.fs6 {font-size: 6pt;}
    .ttn td.fs7 {font-size: 7pt;}
    .ttn td.fs8 {font-size: 8pt;}
    .ttn td.m-l {padding:0;margin:0;}
    .ttn .border-2 {border: 2px solid #000}
    .ttn .bt {border-top: 1px solid #000;}
    .ttn .bl {border-left: 1px solid #000;}
    .ttn .br {border-right: 1px solid #000;}
    .ttn .bb {border-bottom: 1px solid #000;}
    .ttn .bt2 {border-top: 2px solid #000;}
    .ttn .bl2 {border-left: 2px solid #000;}
    .ttn .br2 {border-right: 2px solid #000;}
    .ttn .bb2 {border-bottom: 2px solid #000;}
    .ttn .bt2 {border-top: 2px solid #000;}
    .ttn .bl3 {border-left: 3px solid #000;}
    .ttn .br3 {border-right: 3px solid #000;}
    .ttn .bb3 {border-bottom: 3px solid #000;}
    .ttn .bt3 {border-top: 3px solid #000;}
    .ttn .tc {text-align: center;}
    .ttn .va-t {vertical-align: top;}
    .ttn .va-b {vertical-align: bottom;}
    .ttn .tr {text-align: right;}
    .ttn .tl {text-align: left;}
    .ttn .tip {font-size:6pt; margin:0; padding:1px 0; line-height:6pt; text-align: center;}

    @media print {
        .ttn {
            page-break-after: always;
        }
    }

</style>

<div class="page-content-in p-center-album pad-pdf-p-album ttn">
    <div class="text-left line-h-small">
        <!-- Шапка -->
        <table class="table no-border">
            <tr>
                <td width="83%" class="tc fs8" style="padding-left: 12%">II. ТРАНСПОРТНЫЙ РАЗДЕЛ</td>
                <td colspan="2" class="tr fs6">Оборотная сторона формы № 1-Т</td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tbody>
                        <tr>

                            <td width="12%" class="" style="">
                                Срок доставки груза
                            </td>
                            <td width="3%">
                                "&nbsp;<?= $model->delivery_time ? DateHelper::format($model->delivery_time, 'd', DateHelper::FORMAT_DATE) : '&nbsp;&nbsp;'; ?>&nbsp;"
                            </td>
                            <td width="12%" class="tc bb">
                                <?= ($model->delivery_time) ? \php_rutils\RUtils::dt()->ruStrFTime(['date' => $model->delivery_time, 'format' => 'F', 'monthInflected' => true]) : ''; ?>
                            </td>
                            <td width="20%" class="">
                                <?= ($model->delivery_time) ? DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE) : '20 &nbsp;&nbsp;&nbsp;&nbsp;' ?> г.
                            </td>
                            <td></td>
                        </tr>

                    </table>
                </td>
                <td width="9%" class="tr">ТТН №</td>
                <td class="bt2 bl2 br2 bb fs8 tc">ТТН №<?=$model->getFullNumber()?></td>
            </tr>

            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="8%" class="va-b">Организация</td>
                            <td width="46%" class="va-b bb">
                                <?php if ($model->organization) : ?>
                                    <?= $model->organization ?>
                                <?php else : ?>
                                    <?php if ($model->consignor) {
                                        echo $model->consignor->getRequisitesFull();
                                    } else {
                                        echo $model->invoice->company_name_short,
                                        ", {$model->invoice->company_address_legal_full}",
                                        ', ИНН ', $model->invoice->company_inn,
                                        ", БИК {$model->invoice->company_bik}",
                                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';
                                    }  ?>
                                <?php endif; ?>
                            </td>
                            <td width="8%"  class="tc va-b">Автомобиль</td>
                            <td width="10%" class="bb va-b"><?= $model->car ?></td>
                            <td width="18%" class="tc va-b">Государственный номерной знак</td>
                            <td class="bb va-b"><?= $model->car_number ?></td>
                        </tr>
                    </table>
                </td>
                <td class="tr fs7" style="padding-left:35px">К путевому листу №</td>
                <td class="bl2 br2 bb fs7 tc">
                    <?= ($model->waybill_number && $model->waybill_date) ?
                        $model->waybill_number .' от '. DateHelper::format($model->waybill_date, 'd.m.y', DateHelper::FORMAT_DATE) :
                        ''
                    ?>
                </td>
            </tr>
            <tr>
                <td class="m-l">
                    <table class="table no-border">
                        <tr>
                            <td width="8%"></td>
                            <td width="46%" class="tip">наименование, адрес, номер телефона, банковские реквизиты</td>
                            <td width="8%"></td>
                            <td width="10%" class="tip">марка</td>
                            <td width="20%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="13%">Заказчик(плательщик)</td>
                            <td class="bb">
                                <?= $model->invoice->contractor_name_short,
                                $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                                $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                                $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                                $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                                $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                                $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                                $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2 bb"></td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="13%"></td>
                            <td class="tip">наименование, адрес, номер телефона, банковские реквизиты</td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="7%">Водитель</td>
                            <td width="24%" class="bb">
                                <?= $model->given_out_fio ?>
                            </td>
                            <td width="10%" class="tc">Удостоверение №</td>
                            <td width="24%" class="bb">
                                <?= $model->driver_license ?>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="7%"></td>
                            <td width="24%" class="tip">фамилия, имя, отчество</td>
                            <td width="10%"></td>
                            <td width="24%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="14%">Лицензионная карточка</td>
                            <td width="18%" class="tc bb va-b">
                                <?php if ($model->license_card == Waybill::LICENSE_LIMITED) : ?>
                                    <del>стандартная</del>, ограниченная
                                <?php else : ?>
                                    стандартная, <del>ограниченная</del>
                                <?php endif; ?>
                            </td>
                            <td width="10%" class="tc" style="padding-left:5px">Вид перевозки</td>
                            <td class="bb">
                                <?= $model->transportation_type ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tr">Код</td>
                <td class="bl2 br2 bb"></td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="14%"></td>
                            <td width="18%" class="tip">ненужное зачеркнуть</td>
                            <td width="10%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="12%">Регистрационный №</td>
                            <td width="8%" class="bb"></td>
                            <td width="5%" class="tc">серия</td>
                            <td width="5%" class="bb"></td>
                            <td width="2%" class="tc">№</td>
                            <td width="7%" class="bb"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%">Пункт погрузки</td>
                            <td width="30%" class="tc bb"></td>
                            <td width="10%">Пункт разгрузки</td>
                            <td class="bb"></td>
                        </tr>
                    </table>
                </td>
                <td class="tr">Маршрут</td>
                <td class="bl2 br2 bb tc">
                    <?= $model->transportation_route ?>
                </td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%"></td>
                            <td width="30%" class="tip">адрес, номер телефона</td>
                            <td width="10%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>


            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%">Переадресовка</td>
                            <td width="30%" class="tc bb"></td>
                            <td width="15%" class="tc bb"></td>
                            <td width="7%" class="tr">1. Прицеп</td>
                            <td width="10%" class="bb">
                                <?= $model->trailer1_brand ?>
                            </td>
                            <td width="18%" class="tc">Государственный номерной знак</td>
                            <td class="bb">
                                <?= $model->trailer1_number ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tr">Гаражный номер</td>
                <td class="bl2 br2 bb tc">
                    <?= $model->trailer1_garage_number ?>
                </td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%"></td>
                            <td width="30%" class="tip">наименование и адрес нового грузополучателя</td>
                            <td width="15%" class="tip">номер распоряжения</td>
                            <td width="7%"></td>
                            <td width="10%" class="tip">марка</td>
                            <td width="20%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class="bl2 br2"></td>
            </tr>

            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%"></td>
                            <td width="30%" class="bb"></td>
                            <td width="15%"></td>
                            <td width="7%" class="tr">2. Прицеп</td>
                            <td width="10%" class="bb">
                                <?= $model->trailer2_brand ?>
                            </td>
                            <td width="18%" class="tc">Государственный номерной знак</td>
                            <td class="bb">
                                <?= $model->trailer2_number ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tr">Гаражный номер</td>
                <td class="bl2 br2 bb2 tc">
                    <?= $model->trailer2_garage_number ?>
                </td>
            </tr>
            <tr>
                <td class="m-l va-b">
                    <table class="table no-border">
                        <tr>
                            <td width="10%"></td>
                            <td width="30%" class="tip">подпись ответственного лица</td>
                            <td width="15%" class="fs8" style="padding-left: 20px">СВЕДЕНИЯ О ГРУЗЕ</td>
                            <td width="7%"></td>
                            <td width="10%" class="tip">марка</td>
                            <td width="20%"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td class="tr"></td>
                <td class=""></td>
            </tr>


        </table>

        <!-- 1. Сведения о грузе -->
        <table class="table">
            <tr class="border-2">
                <td class="tc va-t br2">Краткое наименование</td>
                <td class="tc va-t br2">С грузом следуют документы</td>
                <td class="tc va-t br2">Вид упаковки</td>
                <td class="tc va-t br2">Количество мест</td>
                <td class="tc va-t br2">Способ определения массы</td>
                <td class="tc va-t br2">Код груза</td>
                <td class="tc va-t br2">Номер контейнера</td>
                <td class="tc va-t br2">Класс груза</td>
                <td class="tc va-t br2">Масса брутто, т</td>
            </tr>
            <tr class="border-2">
                <td width="20%" class="tc br2">1</td>
                <td width="20%" class="tc br2">2</td>
                <td width="10%" class="tc br2">3</td>
                <td width="6%"  class="tc br2">4</td>
                <td width="20%" class="tc br2">5</td>
                <td width="5%"  class="tc br2">6</td>
                <td             class="tc br2">7</td>
                <td             class="tc br2">8</td>
                <td width="5%"  class="tc br2">9</td>
            </tr>

            <?php
            /** @var OrderWaybill $order */
            $orderQuery = OrderWaybill::find()->where(['waybill_id' => $model->id]);
            $orderArray = $orderQuery->all();
            $ordersCount = count($orderArray);
            foreach ($orderArray as $key => $order):
                $priceWithNds = $order->priceWithNds;
                $amountWithNds = $order->amountWithNds;
                if ($model->invoice->hasNds) {
                    $priceNoNds = $order->priceNoNds;
                    $amountNoNds = $order->amountNoNds;
                    $TaxRateName = $order->order->saleTaxRate->name;
                    $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
                } else {
                    $priceNoNds = $order->priceWithNds;
                    $amountNoNds = $order->amountWithNds;
                    $TaxRateName = 'Без НДС';
                    $ndsAmount = Product::DEFAULT_VALUE;
                }

                if ($hasMass) {
                    $weight_in_tonn = ($order->order->unit_id == ProductUnit::UNIT_KG) ? $order->order->mass_gross / 1000 : $order->order->product->mass_gross;
                } else {
                    $weight_in_tonn = '---';
                }

                $borderBottom2 = ($key+1 < $ordersCount) ? '' : 'style="border-bottom:2px solid #000"';
                ?>

                <tr class="tc br2 bl2 bb" <?=$borderBottom2?>>
                    <td class="tl br2"><?= ($key+1) . '. ' . $order->order->product_title; ?></td>
                    <?php if ($key == 0) : ?>
                        <td class="tl br2" rowspan="<?=$ordersCount?>">
                            <?php
                                $docs = [];
                                if ($model->with_invoice && $invoiceItemName)
                                    $docs[] = $invoiceItemName;
                                if ($model->with_invoice_facture && $invoiceFactureItemName)
                                    $docs[] = $invoiceFactureItemName;
                                if ($model->with_packing_list && $packingListItemName)
                                    $docs[] = $packingListItemName;

                                echo ($docs) ? implode(', ', $docs) : '---';
                            ?>
                        </td>
                    <?php endif; ?>
                    <td class="tc br2"><?= $order->order->box_type; ?></td>
                    <td class="tc bl2 br2 bt2"><?= $order->order->place_count ?></td>
                    <td class="tc br2"><?= (isset(Waybill::$WEIGHT_DETERMINING_METHOD[$model->weight_determining_method])) ? Waybill::$WEIGHT_DETERMINING_METHOD[$model->weight_determining_method] : '---' ?></td>
                    <td class="tc bt2 bl2 br2"><?= $order->order->product_code; ?></td>
                    <td class="tc bt2 br2 tc"> - </td>
                    <td class="tc bt2 br2 tc"> - </td>
                    <td class="tc bt2 br2"><?= ($hasMass) ? TextHelper::moneyFormat($order->quantity * $weight_in_tonn, 1) : '---' ?></td>
                </tr>

            <?php endforeach; ?>

            <tr class="">
                <td colspan="5" style="border:none"></td>
                <td class="br3 bb3 bl3 bt2 tc">1</td>
                <td colspan="2" style="border:none"></td>
                <td class="tc br3 bb3 bl3 bt2" style="height:15pt"><?=($realMassGross) ?  TextHelper::moneyFormat($realMassGross/1000, 1) : '---'?></td>
            </tr>
        </table>

        <!-- между табличками (кол-во ездок, заездов) -->
        <table class="table no-border" style="margin-top: -15pt">
            <tr>
                <td width="33%" class="br va-t">
                    <table class="table no-border">
                        <tr>
                            <td>Указанный груз с исправной</td>
                            <td></td>
                            <td colspan="2">Количество</td>
                        </tr>
                        <tr>
                            <td width="40%">пломбой, тарой и упаковкой</td>
                            <td width="15%" class="bb"></td>
                            <td width="9%">мест</td>
                            <td class="bb">
                                <?php if ($realPlaceCount) {
                                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">оттиск</td>
                            <td></td>
                            <td class="tip">прописью</td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td width="23%" class="va-b">Массой брутто</td>
                            <td width="50%" class="bb va-b">
                                <?php if ($hasMass) {
                                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross/1000));
                                } ?>
                            </td>
                            <td>т к перевозке</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">прописью</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td width="10%" class="va-b">Сдал</td>
                            <td width="20%" class="bb va-b"><?= $model->release_produced_position ?></td>
                            <td width="1%"></td>
                            <td width="20%" class="bb"></td>
                            <td width="1%"></td>
                            <td class="bb va-b tc"><?= $model->release_produced_fullname ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">должность</td>
                            <td></td>
                            <td class="tip">подпись</td>
                            <td></td>
                            <td class="tip">расшифровка подписи</td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td width="30%">Принял <?= mb_strtolower($model->given_out_position) ?></td>
                            <td width="20%" class="bb"></td>
                            <td width="1%"></td>
                            <td width="30%" class="bb tc"><?= $model->given_out_fio ?></td>
                            <td width="1%"></td>
                            <td class="tip">место для</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">подпись</td>
                            <td></td>
                            <td class="tip">расшифровка подписи</td>
                            <td></td>
                            <td class="tip">штампа</td>
                        </tr>
                    </table>
                </td>
                <td width="33%" class="br va-t">
                    <table class="table no-border">
                        <tr>
                            <td>Указанный груз с исправной</td>
                            <td></td>
                            <td colspan="2">Количество</td>
                        </tr>
                        <tr>
                            <td width="40%">пломбой, тарой и упаковкой</td>
                            <td width="15%" class="bb"></td>
                            <td width="9%">мест</td>
                            <td class="bb">
                                <?php if ($realPlaceCount) {
                                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">оттиск</td>
                            <td></td>
                            <td class="tip">прописью</td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td width="23%" class="va-b">Массой брутто</td>
                            <td class="bb va-b">
                                <?php if ($hasMass) {
                                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross/1000));
                                } ?>
                            </td>
                            <td width="20%">т к перевозке</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">прописью</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td >Сдал <?= $model->given_out_position ?></td>
                            <td width="20%" class="bb"></td>
                            <td width="1%"></td>
                            <td class="bb tc"><?= $model->given_out_fio ?></td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">подпись</td>
                            <td></td>
                            <td class="tip">расшифровка подписи</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td width="12%">Принял</td>
                            <td width="20%" class="bb"><?= $model->cargo_accepted_position ?></td>
                            <td width="1%"></td>
                            <td width="20%" class="bb"></td>
                            <td width="1%"></td>
                            <td class="bb tc"><?= $model->cargo_accepted_fullname ?></td>
                            <td class="tip">место для</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="tip">должность</td>
                            <td></td>
                            <td class="tip">подпись</td>
                            <td></td>
                            <td class="tip">расшифровка подписи</td>
                            <td class="tip">штампа</td>
                        </tr>
                    </table>
                </td>
                <td class="va-t">
                    <table class="table no-border">
                        <tr>
                            <td width="25%" class="tl">Количество ездок, заездов</td>
                            <td width="23%"></td>
                            <td width="34%" class="">Итого: масса брутто, т</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="43%">Отметки о составленных актах</td>
                            <td class="bb"></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="32%">Транспортные услуги</td>
                            <td class="bb"></td>
                        </tr>
                    </table>
                    <table class="table no-border">
                        <tr>
                            <td style="padding-top: 5pt">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="100%" class="bb"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <!-- 2. Погрузочно-разгрузочные операции -->
        <table class="table border-2">
            <tr>
                <td colspan="10" class="tc">
                    Погрузочно-разгрузочные операции
                </td>
            </tr>
            <tr>
                <td rowspan="2" class="tc">операция</td>
                <td rowspan="2" class="tc">исполнитель (автовладелец, получатель, отправитель)</td>
                <td rowspan="2" class="tc">дополнительные операции (наименование, количество)</td>
                <td rowspan="2" class="tc">механизм, грузоподъемность, емкость ковша</td>
                <td colspan="2" class="tc">способ</td>
                <td colspan="2" class="tc">дата (число, месяц), время, ч. мин.</td>
                <td rowspan="2" class="tc">время дополнительных операций, мин.</td>
                <td rowspan="2" class="tc">подпись ответственного лица</td>
            </tr>
            <tr>
                <td class="tc">ручной, механизированный, наливом, самосвалом</td>
                <td class="tc">код</td>
                <td class="tc">прибытия</td>
                <td class="tc">убытия</td>
            </tr>
            <tr class="border-2">
                <td class="tc">10</td>
                <td class="tc">11</td>
                <td width="10%" class="tc">12</td>
                <td width="8%" class="tc">13</td>
                <td width="10%" class="tc">14</td>
                <td class="tc">15</td>
                <td width="8%" class="tc">16</td>
                <td width="8%" class="tc">17</td>
                <td width="9%" class="tc">18</td>
                <td width="9%" class="tc">19</td>
            </tr>
            <tr>
                <td>погрузка</td>
                <td><?= ($model->consignor) ? $model->consignor->getShortName() : $model->invoice->company_name_short ?></td>
                <td><?= $model->loading_operations ?></td>
                <td> - </td>
                <td><?= $model->loading_method_name ?></td>
                <td class="tc bt3 bl3"><?= $model->loading_method_code ?></td>
                <td class="tc bt3"><?= $model->loading_time_come ?></td>
                <td class="tc bt3"><?= $model->loading_time_gone ?></td>
                <td class="tc bt3 br3"><?= $model->loading_time_operations ?></td>
                <td></td>
            </tr>
            <tr>
                <td>разгрузка</td>
                <td><?= ($model->consignee) ? $model->consignee->getShortName() : $model->invoice->contractor->getShortName() ?></td>
                <td><?= $model->unloading_operations ?></td>
                <td> - </td>
                <td><?= $model->unloading_method_name ?></td>
                <td class="tc bb3 bl3"><?= $model->unloading_method_code ?></td>
                <td class="tc bb3"><?= $model->unloading_time_come ?></td>
                <td class="tc bb3"><?= $model->unloading_time_gone ?></td>
                <td class="tc bb3 br3"><?= $model->unloading_time_operations ?></td>
                <td></td>
            </tr>
        </table>

        <!-- 3. Прочие сведения -->
        <table class="table no-border" style="margin-top:2px">
            <tr>
                <td colspan="13" class="tc border-2">
                    прочие сведения <span style="font-size:7pt">(заполняется организацией, владельцев автотранспорта)</span>
                </td>

                <td width="15%" rowspan="5" class="va-t" style="position:relative;">
                    <?php if ($model->taxation) : ?>
                        <table class="table no-border">
                            <tr>
                                <td style=" line-height: 12pt; padding-left: 4px">
                                    Таксировка: <span style="border-bottom:1px solid #000;"><?= $model->taxation ?></span>
                                </td>
                        </table>
                    <?php else : ?>
                        <table class="table no-border">
                            <tr>
                                <td width="40%" style="border:none; padding-left: 4px">Таксировка</td>
                                <td class="bb"></td>
                            </tr>
                            <tr><td colspan="2" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                            <tr><td colspan="2" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                            <tr><td colspan="2" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                            <tr><td colspan="2" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                        </table>
                    <?php endif; ?>
                </td>

            </tr>
            <tr>
                <td colspan="5" class="tc bb br bl2">расстояние перевозки по группам дорог, км</td>
                <td rowspan="2" class="tc bb br">код экспедирования груза</td>
                <td colspan="2" class="tc bb br">за транспортные услуги</td>
                <td rowspan="2" class="tc bb br">сумма штрафа за неправильное оформление документов, руб. коп.</td>
                <td colspan="2" class="tc bb br">поправочный коэффициент</td>
                <td colspan="2" class="tc bb br2">время простоя, ч. мин.</td>
            </tr>
            <tr>
                <td class="tc br bl2">всего</td>
                <td class="tc br">в гор.</td>
                <td class="tc br">I гр.</td>
                <td class="tc br">II гр.</td>
                <td class="tc br">III гр.</td>
                <td width="8%" class="tc br">с клиента</td>
                <td width="7%" class="tc br">причитается водителю</td>
                <td class="tc br">расценка водителю</td>
                <td class="tc br">основной тариф</td>
                <td class="tc br">под погрузкой</td>
                <td class="tc br2">под разгрузкой</td>
            </tr>
            <tr>
                <td class="tc bt2 bb2 br bl2">20</td>
                <td class="tc bt2 bb2 br">21</td>
                <td class="tc bt2 bb2 br">22</td>
                <td class="tc bt2 bb2 br">23</td>
                <td class="tc bt2 bb2 br">24</td>
                <td width="7%" class="tc bt2 bb2 br">25</td>
                <td class="tc bt2 bb2 br">26</td>
                <td class="tc bt2 bb2 br">27</td>
                <td width="10%" class="tc bt2 bb2 br">28</td>
                <td width="7%" class="tc bt2 bb2 br">29</td>
                <td width="7%" class="tc bt2 bb2 br">30</td>
                <td width="7%" class="tc bt2 bb2 br">31</td>
                <td width="7%" class="tc bt2 bb2 br2">32</td>
            </tr>
            <tr>
                <td class="tc bt3 bb3 br bl3"><?= $model->transportation_distance ?></td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_in_city ?></td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_group_1 ?></td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_group_2 ?></td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_group_3 ?></td>
                <td class="tc bt3 bb3 br"> - </td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_cost ?> руб.</td>
                <td class="tc bt3 bb3 br"> - </td>
                <td class="tc bt3 bb3 br"> - </td>
                <td class="tc bt3 bb3 br"> - </td>
                <td class="tc bt3 bb3 br"> - </td>
                <td class="tc bt3 bb3 br"><?= $model->transportation_downtime_loading ?></td>
                <td class="tc bt3 bb3 br3"><?= $model->transportation_downtime_unloading ?></td>
            </tr>
        </table>

        <!-- 4. Расчёт стоимости -->
        <table class="table no-border" style="margin-top:2px">
            <tr>
                <td width="10%" rowspan="3" class="tc br bb2 bt2 bl2">Расчет стоимости</td>
                <td rowspan="2" class="tc br bl bt2">За тонны</td>
                <td rowspan="2" class="tc br bl bt2">За тонно-км</td>
                <td rowspan="2" class="tc br bl bt2">Погрузочно- разгрузочные работы, тонн</td>
                <td rowspan="2" class="tc br bl bt2">Недогрузка автомобиля и прицепа</td>
                <td rowspan="2" class="tc br bl bt2">Экспеди- рование</td>
                <td colspan="2" class="tc br bl bt2">Сверхнормативный простой, ч. мин. при</td>
                <td rowspan="2" class="tc br bl bt2">За сроч- ность заказа</td>
                <td rowspan="2" class="tc br bl bt2">За специ- альный транспорт</td>
                <td rowspan="2" class="tc br bl bt2">Прочие доплаты</td>
                <td rowspan="2" class="tc br bl bt2 br2">Всего</td>

                <td width="20%" rowspan="6" class="va-t">
                    <table class="table no-border">
                        <?php if ($model->taxation) : ?>
                            <tr><td colspan="4">&nbsp;</td></tr>
                            <tr><td colspan="4" style="padding-top: 6pt">&nbsp;</td></tr>
                            <tr><td colspan="4" style="padding-top: 6pt">&nbsp;</td></tr>
                        <?php else : ?>
                            <tr><td colspan="4" class="bb" style="padding-top: 0pt">&nbsp;</td></tr>
                            <tr><td colspan="4" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                            <tr><td colspan="4" class="bb" style="padding-top: 6pt">&nbsp;</td></tr>
                        <?php endif; ?>
                        <tr>
                            <td width="36%" style="padding-top: 6pt; padding-left:2px;">Таксировщик</td>
                            <td width="24%" class="bb"></td>
                            <td width="1%"></td>
                            <td class="bb tc"><?= $model->taximaster ?></td>
                        </tr>
                        <tr>
                            <td width="35%"></td>
                            <td width="25%" class="tip">подпись</td>
                            <td width="1%"></td>
                            <td class="tip">расшифровка подписи</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="bt br tc">погрузке</td>
                <td class="bt br tc">разгрузке</td>
            </tr>
            <tr>
                <td class="bt2 tc br bl2">33</td>
                <td class="bt2 tc br">34</td>
                <td class="bt2 tc br">35</td>
                <td class="bt2 tc br">36</td>
                <td class="bt2 tc br">37</td>
                <td class="bt2 tc br">38</td>
                <td class="bt2 tc br">39</td>
                <td class="bt2 tc br">40</td>
                <td class="bt2 tc br">41</td>
                <td class="bt2 tc br">42</td>
                <td class="bt2 tc br2">43</td>


            </tr>
            <tr>
                <td class="tc bb bl2 br">Выполнено</td>
                <td class="tc bb br bt3 bl3"><?= ($hasMass) ? TextHelper::moneyFormat($realMassNet/1000, 1) : '-' ?></td>
                <td width="8%" class="tc bb br bt3"><?= ($hasMass && $model->transportation_distance > 0) ? TextHelper::moneyFormat(floor($realMassNet/1000 * $model->transportation_distance), 0) : '-' ?></td>
                <td class="tc bb br bt3"><?= ($hasMass) ? TextHelper::moneyFormat($realMassNet/1000, 1) : '-' ?></td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td class="tc bb br bt3">-</td>
                <td width="8%" class="tc bb br bt3 br3">-</td>
            </tr>
            <tr>
                <td class="tc bb bl2 br">Расценка, руб.коп</td>
                <td class="tc bb br bl3">-</td>
                <td class="tc bb br"><?= ($hasMass && $model->transportation_cost > 0) ? TextHelper::moneyFormat(floor($model->transportation_cost / $realMassNet/1000 * $model->transportation_distance), 2) : '-' ?></td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br">-</td>
                <td class="tc bb br3">-</td>
            </tr>
            <tr>
                <td class="tc br bl2 bb2">К оплате, руб.коп</td>
                <td class="tc br bb3 bl3">-</td>
                <td class="tc br bb3"><?= ($model->transportation_cost > 0) ? TextHelper::moneyFormat($model->transportation_cost, 2) : '-' ?></td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3">-</td>
                <td class="tc br bb3 br3"><?= ($model->transportation_cost > 0) ? TextHelper::moneyFormat($model->transportation_cost, 2) : '-' ?></td>
            </tr>
        </table>

    </div>
</div>