<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use frontend\modules\documents\components\DocConverter;

/* @var $this yii\web\View */
/* @var $model common\models\document\Waybill */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
$plus = 0;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = \common\models\document\OrderWaybill::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$precision = $model->invoice->price_precision;

$isFullCustomerInfo = $ioType == Documents::IO_TYPE_OUT_URL;
$fullInfoPrefix = '';
if ($isFullCustomerInfo) {
    $fullInfoPrefix = 'full';
}
$script = <<< JS

if('$plus' === '1'){
  $('.testclass').hide();
}
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, $this::POS_READY);
?>
<div class="page-content-in padd-b-30" style="">
    <?php if ($backUrl !== null) {
        echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
            'class' => 'back-to-customers',
        ]);
    } ?>
    <div class="col-xs-12 pad0">
        <?php echo $this->render('_viewPartials/_pre-view-' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'message' => $message,
            'dateFormatted' => $dateFormatted,
            'ioType' => $ioType,
            'useContractor' => $useContractor
        ]);
        ?>
        <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
            'message' => $message
        ]); ?>
    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
        ]); ?>
    </div>
</div>

<?= \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id, 'contractorId' => $contractorId,]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить товарную накладную?',
]); ?>