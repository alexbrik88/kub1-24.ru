<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\ScanDocument;
use frontend\modules\documents\components\UploadManagerHelper;
use yii\helpers\Html;
use frontend\models\Documents;
use common\models\file\File;

/** @var array $files */

$filesIds = Yii::$app->request->get('files');

$jsParams = [
    'io_type' => 0,
    'document_number' => '',
    'document_date' => '',
    'contractor_id' => '',
    'products' => [],
    'products_ids' => [],
];

if (!empty($filesIds))
{
    foreach ((array)$filesIds as $fileId) {

        $file = File::findOne($fileId);

        if ($file instanceof File) {

            $scan = UploadManagerHelper::getScanDocument($file);

            if ($scan instanceof ScanDocument) {

                if ($scan->recognize && $scan->recognize->is_recognized && $scan->recognize->scanRecognizeDocuments) {
                    $rDocument = $scan->recognize->scanRecognizeDocuments[0];
                    $jsParams['document_number'] = (string)$rDocument->document_number;
                    $jsParams['document_date'] = DateHelper::format($rDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    foreach ($rDocument->scanRecognizeContractors as $rContractor) {
                        if ($rContractor->type == Contractor::TYPE_SELLER) {
                            if ($rContractor->inn == $company->inn) {
                                $jsParams['io_type'] = Documents::IO_TYPE_OUT;
                            } else {
                                $jsParams['contractor_id'] = (string)UploadManagerHelper::findContractorId(Contractor::TYPE_SELLER, $rContractor->inn, $rContractor->kpp);
                            }

                        } elseif ($rContractor->type == Contractor::TYPE_CUSTOMER) {
                            if ($rContractor->inn == $company->inn) {
                                $jsParams['io_type'] = Documents::IO_TYPE_IN;
                            } else {
                                $jsParams['contractor_id'] = (string)UploadManagerHelper::findContractorId(Contractor::TYPE_CUSTOMER, $rContractor->inn, $rContractor->kpp);
                            }
                        }
                    }
                    foreach ($rDocument->scanRecognizeProducts as $rProduct) {
                        if ($productId = (string)UploadManagerHelper::findProductId($rProduct->code))
                            $jsParams['products'][] = [
                                'id' => $productId,
                                'quantity' => $rProduct->quantity,
                                'price' => $rProduct->price
                            ];
                            $jsParams['products_ids'][] = $productId;
                    }
                }
            }
        }
    }
}

if ($jsParams['io_type']) {

    $jsParams = json_encode($jsParams);

    $this->registerJs("
        var jsRecognizeParams = $jsParams;
        $(document).ready(function() { 
            // console.log(jsRecognizeParams);
            if (jsRecognizeParams.document_number)
                $('#account-number').val(jsRecognizeParams.document_number);
            if (jsRecognizeParams.document_date)
                $('#under-date').val(jsRecognizeParams.document_date);
            if (jsRecognizeParams.contractor_id)
                $('#invoice-contractor_id').val(jsRecognizeParams.contractor_id).trigger('change');
                
            if (jsRecognizeParams.products_ids.length) {
                $.post('/documents/upload-manager/get-products-table', {in_order: jsRecognizeParams.products_ids}, function (data) {
                    $.each(data, function(i,v) {
                        if (jsRecognizeParams.products[i].price > 0)
                            v.price = jsRecognizeParams.products[i].price;
                        if (jsRecognizeParams.products[i].quantity > 0)
                            v.quantity = jsRecognizeParams.products[i].quantity;                            
                    });
                    INVOICE.addProductToTable(data);
                    $('#from-new-add-row').hide();
                });
            }
                
        });"
    );
}
?>