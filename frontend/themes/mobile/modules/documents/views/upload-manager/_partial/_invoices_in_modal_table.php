<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
/* @var array $files */

$user = Yii::$app->user->identity;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 style="text-align: center; margin: 0">Прикрепить файл к <?= $documentTypeName ?> от Поставщика</h4>
</div>
<div class="invoice-list">
    <?= Html::beginForm(["/documents/upload-manager/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>
    <?= Html::hiddenInput('doc_type', $docType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod): ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>
            <?php foreach ($files as $file)
                echo Html::hiddenInput('files[]', $file) ?>

            <div class="search-form-default">
                <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'byNumber', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div id="range-ajax-button">
                <div
                    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                    data-pjax="get-vacant-invoices-pjax" id="reportrange2">

                    <?php
                    $this->registerJs("
                    $('#reportrange2').daterangepicker({
                        format: 'DD.MM.YYYY',
                        ranges: {" . StatisticPeriod::getRangesJs() . "},
                        startDate: '" . $dateFrom . "',
                        endDate: '" . $dateTo . "',
                        locale: {
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    });

                    "); ?>

                    <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                    <b class="fa fa-angle-down"></b>
                </div>

            </div>

        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?php \yii\widgets\Pjax::begin([
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_act',
                                'label' => 'Акт',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_ACT ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_ACT ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($user, $files) {
                                    $content = '';
                                    foreach ($data->acts as $doc) {
                                        $docLink = Html::a($doc->fullNumber, [
                                            '/documents/act/update',
                                            'type' => $doc->type,
                                            'id' => $doc->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'data-pjax' => 0,
                                            'class' => $data->canView ? '' : 'no-rights-link',
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }
                                    if ($data->canAddAct) {
                                        $canCreate = $data->canCreate;
                                        $linkAdd = Html::a('Добавить', [
                                            '/documents/act/create',
                                            'type' => $data->type,
                                            'invoiceId' => $data->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'data-pjax' => 0,
                                            'class' => 'hidden-xs hidden-sm hidden-md btn btn-sm yellow' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                                        ]);

                                        $linkAdd .= ' <div class="visible-xs visible-sm visible-md text-center"> <a href="' . Url::to([
                                                '/documents/act/create',
                                                'type' => $data->type,
                                                'invoiceId' => $data->id,
                                                'mode' => 'previewScan',
                                                'files' => $files,
                                            ]) . '"
                                                    data-pjax="0"
                                                    class = "yellow ' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link') . '"
                                                    style = "' . ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : '') . '"
                                                    >

                                                        <span class="btn yellow btn-add-line-table" style="padding:5px 7px;">
                                                        <i class="pull-left fa icon fa-plus-circle" style="font-size: 18px;padding:0px;margin:0px;"></i>
                                                        </span>
                                                </a></div>';

                                        $content .= $linkAdd;
                                    }

                                    return $content;
                                },
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_packing_list',
                                'label' => 'ТН',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_PACKING_LIST ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_PACKING_LIST ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($user, $files) {
                                    $content = '';
                                        foreach ($data->packingLists as $doc) {
                                            $docLink = Html::a($doc->fullNumber, [
                                                '/documents/packing-list/update',
                                                'type' => $doc->type,
                                                'id' => $doc->id,
                                                'mode' => 'previewScan',
                                                'files' => $files,
                                            ], [
                                                'data-pjax' => 0,
                                                'class' => $data->canView ? '' : 'no-rights-link',
                                            ]);

                                            $fileLink = DocumentFileWidget::widget([
                                                'model' => $doc,
                                                'cssClass' => 'pull-right',
                                            ]);

                                            $content .= Html::tag('div', $docLink . $fileLink);
                                        }
                                        if ($data->canAddPackingList) {
                                            $canCreate = $data->canCreate;
                                            $content .= Html::a('Добавить', [
                                                '/documents/packing-list/create',
                                                'type' => $data->type,
                                                'invoiceId' => $data->id,
                                                'mode' => 'previewScan',
                                                'files' => $files,
                                            ], [
                                                'data-pjax' => 0,
                                                'class' => 'btn btn-sm yellow' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                                'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                                            ]);
                                        }

                                    return $content;
                                }
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_invoice_facture',
                                'label' => 'СФ',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_INVOICE_FACTURE ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_INVOICE_FACTURE ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($files) {
                                    $content = '';

                                    if (!$data->hasNds) {
                                        return $content;
                                    }
                                    /* @var $doc \common\models\document\InvoiceFacture */
                                    foreach ($data->invoiceFactures as $doc) {
                                        $docLink = Html::a($doc->fullNumber, [
                                            '/documents/invoice-facture/update',
                                            'type' => $doc->type,
                                            'id' => $doc->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'class' => $data->canView ? '' : 'no-rights-link',
                                            'data-pjax' => 0
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }
                                    if ($data->canAddInvoiceFacture) {
                                        $canCreate = $data->canCreate;
                                        $content .= Html::a('Добавить', [
                                            '/documents/invoice-facture/create',
                                            'type' => $data->type,
                                            'invoiceId' => $data->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'class' => 'btn btn-sm yellow' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                                            'data-pjax' => 0
                                        ]);
                                    }

                                    return $content;
                                },
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_upd',
                                'label' => 'УПД',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_UPD ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_UPD ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($files) {
                                    $content = '';

                                    /* @var $doc \common\models\document\Upd */
                                    foreach ($data->upds as $doc) {
                                        $docLink = Html::a($doc->fullNumber, [
                                            '/documents/upd/update',
                                            'type' => $doc->type,
                                            'id' => $doc->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'class' => $data->canView ? '' : 'no-rights-link',
                                            'data-pjax' => 0
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }
                                    if ($data->canAddUpd) {
                                        $canCreate = $data->canCreate;
                                        $content .= Html::a('Добавить', [
                                            '/documents/upd/create',
                                            'type' => $data->type,
                                            'invoiceId' => $data->id,
                                            'mode' => 'previewScan',
                                            'files' => $files,
                                        ], [
                                            'class' => 'btn btn-sm yellow' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                                            'data-pjax' => 0
                                        ]);
                                    }


                                    return $content;
                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) use ($files, $docType) {
                                    /** @var Invoice $data */
                                    if ($docType == Documents::DOCUMENT_INVOICE) {
                                        return Html::a($data->fullNumber, [
                                                'invoice/update',
                                                'type' => $data->type,
                                                'id' => $data->id,
                                                'mode' => 'previewScan',
                                                'files' => $files,
                                            ], [
                                                'data-pjax' => '0',
                                            ]);
                                    }

                                    return $data->fullNumber;
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'attribute' => 'total_amount_with_nds',
                                'value' => function (Invoice $model) {
                                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                },
                            ],
                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '24%',
                                ],
                                'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                                'format' => 'raw',
                                'value' => 'contractor_name_short',
                            ],
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12">
        <button class="btn btn-add darkblue pull-right" data-dismiss="modal">Отменить</button>
    </div>
</div>