<?php
use common\components\ImageHelper;
use yii\helpers\Html;

$emailArr = explode('.', $model->email);
$customEmailPart = array_shift($emailArr);
$basisEmailPart = implode('.', $emailArr);

/** @var $model \frontend\modules\documents\models\ScanUploadEmail */
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 style="text-align: center; margin: 0">Загрузка документов по e-mail</h4>
</div>
<div class="modal-body">

    <div class="row">
        <div class="col-md-12">
            <table class="content-table">
                <tr>
                    <td class="icon"><?= ImageHelper::getThumb('img/upload-files-email.png', [50, 50]) ?></td>
                    <td>
                        Присылайте счета, акты товарные накладные и другие документы
                        на e-mail, который мы создали для вас: <a href="mailto:<?= $oldEmail ?>"><?= $oldEmail ?></a> <br/>
                        Документы будут сохранены на этой странице.
                        Мы сделаем распознавание сканов, чтобы вы могли проверить  и преобразовать их в счета и другие документы,
                        для дальнейшей работы.
                        <div class="email-upload-text-wrap" style="margin-top:14px;<?= !$model->enabled ? '':'display:none' ?>">
                            Не нравится e-mail, который мы вам создали?
                            <a href="javascript:" class="btn-enable-email-upload">Выберите сами!</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="email-upload-text-wrap"  style="<?= !$model->enabled ? '':'display:none' ?>">
            <div class="row action-buttons" style="padding:0 25px;">
                <div class="col-sm-12" style="text-align:center">
                    <button class="btn btn-enable-email-upload darkblue">Включить</button>
                </div>
            </div>
        </div>
    </div>

    <div class="email-upload-form-wrap" style="<?= $model->enabled ? '':'display:none' ?>">
        <?php
        $form = \yii\bootstrap4\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
            'action' => \yii\helpers\Url::to("/documents/upload-manager/get-email-upload-modal"),
            'method' => 'POST',

            'options' => [
                'id' => 'email-upload-form',
                'enctype' => 'multipart/form-data',
                'data-pjax' => true
            ],

            'fieldConfig' => [
                'labelOptions' => [
                    'class' => 'col-md-4 control-label',
                ],
            ],
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'validateOnSubmit' => false,
            'validateOnBlur' => false,
        ]));
        ?>

        <?= Html::activeHiddenInput($model, 'enabled', ['value' => '1']); ?>

        <div class="row">
            <div class="col-md-12">
                <label class="control-label" style="font-weight: bold" for="scanuploademail-customemailpart">
                    Подтвердите ваш e-mail, на которые вы будите пересылать документы<span class="required" aria-required="true">*</span>:</label>
            </div>
            <div class="col-md-12">
                <input style="width:25%;display: inline-block" type="text" id="scanuploademail-customemailpart" class="form-control" name="ScanUploadEmail[customEmailPart]" value="<?=$customEmailPart?>" aria-required="true">
                <input style="width:73%;display: inline-block" type="text" id="scanuploademail-basisemailpart" class="form-control" name="ScanUploadEmail[basisEmailPart]" value="<?=$basisEmailPart?>" aria-required="true" readonly>
                <p class="help-block help-block-error ">
                    <?= $model->getFirstError('customEmailPart') ?>
                    <?= $model->getFirstError('email') ?>
                </p>
            </div>
        </div>

        <div class="action-buttons row" style="margin-top: 20px">
            <div class="spinner-button col-md-4 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width:130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',]) ?>
            </div>
            <div class="col-xs-7"></div>
            <div class="col-md-8 col-xs-1" style="float: right;">
                <button type="button"
                        class="btn darkblue btn-cancel darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right"
                        data-dismiss="modal" style="width: 130px !important;">Отмена
                </button>
                <button type="button" class="btn darkblue gray-darkblue btn-cancel darkblue widthe-100 hidden-lg" title="Отмена"><i
                            class="fa fa-reply fa-2x"></i></button>
            </div>
        </div>

        <?php
        $form->end();
        ?>
    </div>

</div>

<?php
$this->registerJs("
    $(document).on('click', '.btn-enable-email-upload', function(e) {
        $('.email-upload-text-wrap').hide();
        $('.email-upload-form-wrap').fadeIn(200);
    });
"); ?>

