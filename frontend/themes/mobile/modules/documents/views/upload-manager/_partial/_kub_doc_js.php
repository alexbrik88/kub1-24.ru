<?php use frontend\models\Documents; ?>
<script>
    KubDoc = {
        IO_TYPE_OUT: 2,
        IO_TYPE_IN:  1,
        DOCUMENT_INVOICE: <?= Documents::DOCUMENT_INVOICE ?>,
        DOCUMENT_INVOICE_FACTURE: <?= Documents::DOCUMENT_INVOICE_FACTURE ?>,
        DOCUMENT_ACT: <?= Documents::DOCUMENT_ACT ?>,
        DOCUMENT_PACKING_LIST: <?= Documents::DOCUMENT_PACKING_LIST ?>,
        DOCUMENT_UPD: <?= Documents::DOCUMENT_UPD ?>,
        DOCUMENT_AGREEMENT: <?= Documents::DOCUMENT_AGREEMENT ?>,
    }
    KubDoc.inType = {
        invoice:      KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_INVOICE,
        act:          KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_ACT,
        packing_list: KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_PACKING_LIST,
        upd:          KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_UPD,
        agreement:    KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_AGREEMENT,
        invoice_facture: KubDoc.IO_TYPE_IN + "-" + KubDoc.DOCUMENT_INVOICE_FACTURE
    }
    KubDoc.outType = {
        invoice:      KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_INVOICE,
        act:          KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_ACT,
        packing_list: KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_PACKING_LIST,
        upd:          KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_UPD,
        agreement:    KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_AGREEMENT,
        invoice_facture: KubDoc.IO_TYPE_OUT + "-" + KubDoc.DOCUMENT_INVOICE_FACTURE,
    }
    KubDoc.currentType = false;
    KubDoc.currentDocType = false;
    KubDoc.currentDocName = '';
    KubDoc.currentFiles = [];

    KubDoc.setParams = function(addToDoc, files) {
        KubDoc.currentType = false;
        KubDoc.currentDocType = false;
        KubDoc.currentDocName = '';
        KubDoc.currentFiles = files;

        // IN
        if (addToDoc === KubDoc.inType.act) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_ACT;
            KubDoc.currentDocName = "Акт";
        }
        if (addToDoc === KubDoc.inType.packing_list) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_PACKING_LIST;
            KubDoc.currentDocName = "ТН";
        }
        if (addToDoc === KubDoc.inType.invoice) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_INVOICE;
            KubDoc.currentDocName = "Счет";
        }
        if (addToDoc === KubDoc.inType.invoice_facture) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_INVOICE_FACTURE;
            KubDoc.currentDocName = "Счет-фактура";
        }
        if (addToDoc === KubDoc.inType.upd) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_UPD;
            KubDoc.currentDocName = "УПД";
        }
        if (addToDoc === KubDoc.inType.agreement) {
            KubDoc.currentType = KubDoc.IO_TYPE_IN;
            KubDoc.currentDocType = KubDoc.DOCUMENT_AGREEMENT;
            KubDoc.currentDocName = "Договор";
        }

        // OUT
        if (addToDoc === KubDoc.outType.act) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_ACT;
            KubDoc.currentDocName = "Акт";
        }
        if (addToDoc === KubDoc.outType.packing_list) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_PACKING_LIST;
            KubDoc.currentDocName = "ТН";
        }
        if (addToDoc === KubDoc.outType.invoice) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_INVOICE;
            KubDoc.currentDocName = "Счет";
        }
        if (addToDoc === KubDoc.outType.invoice_facture) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_INVOICE_FACTURE;
            KubDoc.currentDocName = "СФ";
        }
        if (addToDoc === KubDoc.outType.upd) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_UPD;
            KubDoc.currentDocName = "УПД";
        }
        if (addToDoc === KubDoc.outType.agreement) {
            KubDoc.currentType = KubDoc.IO_TYPE_OUT;
            KubDoc.currentDocType = KubDoc.DOCUMENT_AGREEMENT;
            KubDoc.currentDocName = "Договор";
        }

        if (KubDoc.currentType === false) {
            console.log('KubDoc: unknown document type');
            return false;
        }

        return true;
    }

    KubDoc.showModal = function() {

        var $confirmModal = $('#modal-attach-image');

        if (KubDoc.currentType === KubDoc.IO_TYPE_IN) {

            if (KubDoc.currentDocType === KubDoc.DOCUMENT_INVOICE) {
                $('.modal-attach-image-no', $confirmModal).click();
                return;
            }

            if (KubDoc.currentDocType == KubDoc.DOCUMENT_AGREEMENT) {
                $confirmModal.find('.agreement_part').show();
                $confirmModal.find('.invoice_part').hide();
            } else {
                $confirmModal.find('.invoice_part').show();
                $confirmModal.find('.agreement_part').hide();
                $confirmModal.find('.kubdoc_name').html(KubDoc.currentDocName);
            }

            $('#modal-attach-image').modal('show');

            return true;
        }

        if (KubDoc.currentType === KubDoc.IO_TYPE_OUT) {
            $('#invoices-list').modal();

            $.pjax({
                url: '/documents/upload-manager/get-invoices',
                container: '#get-vacant-invoices-pjax',
                data: {
                    type: KubDoc.currentType,
                    doc_type: KubDoc.currentDocType,
                    files: KubDoc.currentFiles
                },
                push: false,
                timeout: 5000
            });

            return true;
        }

        console.log('KubDoc: io type error');
        return false;
    };


</script>