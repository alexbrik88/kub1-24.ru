<?php

use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\file\widgets\FileUpload;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$this->context->layout = 'agreements-update';
$this->title = 'Просмотр договора';

$canUpdate = Yii::$app->user->can(permissions\Contractor::CREATE, [
    'model' => $model,
]);
$canUpdateStatus = Yii::$app->user->can(permissions\Contractor::CREATE, [
    'model' => $model,
]);

$showSendPopup = Yii::$app->session->remove('show_send_popup');
?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', str_replace('activeTab=4', 'tab=agreements', $backUrl), ['class' => 'link mb-2']); ?>
<?php else: ?>
    <?= Html::a('Назад к списку', ['index'], ['class' => 'link mb-2']); ?>
<?php endif ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <?php if ($model->essence) : ?>
            <div class="page-in-content column">
                <div class="page-border">
                    <?= frontend\themes\mobile\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                        'toggleButton' => [
                            'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                            'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                            'title' => 'Последние действия',
                        ]
                    ]); ?>
                    <?php if (!$model->is_completed && Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, [
                        'model' => $model,
                    ])) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'update-contract',
                            'id' => $model->id,
                        ], [
                            'title' => 'Редактировать',
                            'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                        ]) ?>
                    <?php endif; ?>
                    <div class="doc-container doc-agreement doc-preview">
                        <?= $this->render('partial/_viewText', [
                            'model' => $model,
                            'canUpdate' => $canUpdate,
                            'canUpdateStatus' => $canUpdateStatus
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="page-in-sidebar column">
                <?= $this->render('partial/_viewStatus', [
                    'model' => $model,
                    'canUpdate' => $canUpdate,
                    'canUpdateStatus' => $canUpdateStatus
                ]); ?>
            </div>
        <?php else: ?>
            <div class="agreement-form" data-header="Договор">
                <h4><b><?= Html::encode($model->document_name) ?></b></h4>
                <h4><b>№<?= Html::encode($model->document_number) ?>
                        от <?= \php_rutils\RUtils::dt()->ruStrFTime([
                            'format' => 'd F Y г.',
                            'monthInflected' => true,
                            'date' => $model->document_date,
                        ]); ?></b></h4>
                        <?php $file = ($model->files) ? $model->files[0] : null;
                        echo $file ? Html::a('<span class="icon icon-paper-clip m-r-10"></span>', [
                            '/contractor/agreement-file-get',
                            'id' => $model->id,
                            'file-id' => $file->id,
                        ], [
                            'class' => 'file-link',
                            'target' => '_blank',
                            'download' => '',
                            'data-pjax' => 0,
                        ]) : ''; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?= $this->render('partial/_viewActionsButtons', [
    'model' => $model,
    'canUpdate' => $canUpdate
]); ?>

<?php if ($model->essence && $canUpdateStatus): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'showSendPopup' => $showSendPopup,
        'useContractor' => true
    ]); ?>
<?php endif; ?>
