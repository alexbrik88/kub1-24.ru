<?php

use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\grid\GridView;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\Pjax;
use yii\grid\ActionColumn;
use frontend\widgets\BtnConfirmModalWidget;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use frontend\models\Documents;

/** @var \frontend\modules\documents\models\AgreementSearch $searchModel */

$this->title = 'Договоры';

if ($type)
    $this->title .= ($type == Documents::IO_TYPE_OUT) ? ' с покупателями' : ' с поставщиками';

$userConfig = Yii::$app->user->identity->config;
$tabConfig = [
    'scan' => $userConfig->agreement_scan,
    'responsible_employee' => $userConfig->agreement_responsible_employee_id
];
$tabViewClass = $userConfig->getTableViewClass('table_view_document');
$baseUrl = Yii::$app->params['kubAssetBaseUrl'];
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>

        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).Html::tag('span', 'Добавить'), '#', [
            'id' => 'agreement_add',
            'class' => 'button-regular button-regular_red button-width ml-auto agreement-modal-link',
            'data-url' => Url::to(['create', 'type' => $type])
        ]); ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <?= $this->render('../layouts/_agreement-new-toggle', ['type' => $type]) ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'agreement_scan',
                    ],
                    [
                        'attribute' => 'agreement_responsible_employee_id',
                        'visible' => (
                            Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
                        ),
                    ],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Номер или название договора',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'agreement-pjax-container',
        'enablePushState' => false,
        'timeout' => 5000
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'columns' => [
            [
                'headerOptions' => [
                    'width' => '5%',
                ],
                'attribute' => 'document_number',
                'label' => 'Номер',
                'format' => 'raw',
                'value' => function ($data) {
                    $full_number = ($data['document_additional_number']) ?
                        $data['document_additional_number'].'-'.$data['document_number'] :
                        $data['document_number'];

                    if (!$full_number)
                        $full_number = '---';

                    $can_view = Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]);

                    return  ($can_view && $data['tid']) ? // if has template
                        Html::a($full_number, ['/documents/agreement/view', 'id' => $data['id']], ['data-pjax' => 0]) :
                        Html::encode($full_number);
                },
            ],
            [
                'headerOptions' => [
                    'width' => '5%',
                ],
                'attribute' => 'document_date',
                'label' => 'Дата',
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'headerOptions' => [
                    'width' => '15%',
                    'class' => 'dropdown-filter',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'attribute' => 'contractor_id',
                'label' => 'Контр&shy;агент',
                'encodeLabel' => false,
                'filter' => $searchModel->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) {
                    $contractor = Contractor::findOne($data['cid']);
                    $name = Html::encode($contractor->getShortName());
                    return Html::a($name, [
                        '/contractor/view',
                        'type' => $data['type'],
                        'id' => $data['cid'],
                    ], [
                        'title' => html_entity_decode($name),
                        'data-pjax' => 0,
                        'data' => [
                            'id' => $data['cid'],
                        ]
                    ]);
                },
            ],
            /*
            [
                'headerOptions' => [
                    'width' => '9%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'type',
                'label' => 'Тип контр&shy;агента',
                'encodeLabel' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return ($contractor = Contractor::findOne($data['cid'])) ?
                        Contractor::$contractorTitleSingle[$contractor->type] :
                        '';
                },
                'filter' => [
                    '' => 'Все',
                    2 => 'Покупатель',
                    1 => 'Поставщик',
                ]
            ],
            */
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'document_type_id',
                'filter' => $searchModel->getAgreementTypeFilterItems(),
                's2width' => '200px',
                'format' => 'raw',
                'label' => 'Тип доку&shy;мента',
                'encodeLabel' => false,
                'value' => function ($data) {
                    return $data['agreementType'];
                },
            ],
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'attribute' => 'document_name',
                'label' => 'Название',
                'contentOptions' => [
                    'style' => 'overflow: hidden;text-overflow: ellipsis; ',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $content = '';
                    $tooltipId = 'tooltip-product-comment-' . $data['id'];
                    $content .= Html::tag('div', Html::tag('div', Html::encode($data['document_name'])), [
                        'class' => 'product-comment-box',
                        'data-tooltip-content' => '#' . $tooltipId,
                        'style' => 'cursor: pointer;'
                    ]);
                    $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                    $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                    $content .= Html::encode($data['document_name']);
                    $content .= Html::endTag('div');
                    $content .= Html::endTag('div');

                    return $content;
                },
            ],
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'agreement_template_id',
                'label' => 'Шаблон',
                'format' => 'raw',
                'filter' => $searchModel->getAgreementTemplates(),
                's2width' => '200px',
                'value' => function ($data) {
                    if ($data['tid']) {
                        $tpl = \common\models\AgreementTemplate::findOne($data['tid']);
                        if ($tpl) {
                            $text = '№ ' . $tpl->document_number . ' от ' . date('d.m.Y', strtotime($tpl->document_date));
                            return Html::a($text, ['/documents/agreement-template/view', 'id' => $data['tid']], ['data-pjax' => 0]);
                        }

                        return 'Шаблон удален';
                    }
                    return '---';
                },
            ],
            [
                'headerOptions' => [
                    'class' => 'col_agreement_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'col_agreement_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                ],
                'attribute' => 'has_file',
                'label' => 'Скан',
                'format' => 'raw',
                'content' => function ($data) {
                    $agreement = Agreement::findOne($data['id']);
                    $file = ($agreement && $agreement->files) ? $agreement->files[0] : null;
                    return $file ? Html::a('<span class="icon icon-paper-clip m-r-10"></span>', [
                        '/contractor/agreement-file-get',
                        'id' => $agreement->id,
                        'file-id' => $file->id,
                    ], [
                        'class' => 'file-link',
                        'target' => '_blank',
                        'download' => '',
                        'data-pjax' => 0,
                    ]) : '';
                },
            ],
            [
                'headerOptions' => [
                    'class' => 'col_agreement_responsible_employee_id' . ($tabConfig['responsible_employee'] ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_agreement_responsible_employee_id' . ($tabConfig['responsible_employee'] ? '' : ' hidden'),
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'created_by',
                'label' => 'Ответст&shy;венный',
                'encodeLabel' => false,
                'value' => function ($data) {
                    $employee = Employee::findOne([
                        'id' => $data['created_by']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getAgreementEmployees(),
                's2width' => '200px',
            ],

            [
                'class' => ActionColumn::className(),
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '4%',
                ],
                'buttons' => [
                    'update' => function ($url, $data) {
                        return Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), '#', [
                            'data-url' => $url,
                            'class' => 'agreement-modal-link link',
                            'title' => Yii::t('yii', 'Редактировать'),
                            'aria-label' => Yii::t('yii', 'Редактировать'),
                        ]);
                    },
                    'delete' => function ($url, $data) {
                        return BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                'class' => '',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить договор?',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url = 0;
                    switch ($action) {
                        case 'update':
                            $url = '/documents/agreement/update';
                            break;
                        case 'delete':
                            $url = '/contractor/agreement-delete';
                            break;
                    }

                    Yii::$app->session->set('return_from_agreement', '');

                    return Url::to([$url, 'id' => $model['id'], 'type' => $model['type'], 'contractor_id' => $model['cid'], 'old_record' => 1]);
                },
            ],
        ],
    ]); ?>

    <div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body" id="block-modal-new-product-form">

                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs('
        $(".product-comment-box").tooltipster({
            "theme": ["tooltipster-kub"],
            "trigger": "hover",
            "contentAsHTML": true
        });
    '); ?>

    <?php Pjax::end(); ?>
</div>

<div style="display: none">
    <?php /* preload styles */ ?>
    <?= Select2::widget([
        'name' => 'empty',
        'data' => []
    ]); ?>
</div>

<?php $showModal = Yii::$app->request->get('modal'); ?>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h4 id="agreement-modal-header">Добавить договор</h4>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$successFlashMessageDelete = "<div id='w2-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Договор удален</div>";

$this->registerJs('
$(document).on("click", ".agreement-modal-link", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).data("url"), container: "#agreement-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#agreement-modal-header").html($("[data-header]").data("header"));
        //$(".date-picker").datepicker({format:"dd.mm.yyyy",language:"ru",autoclose:true}).on("change.dp", dateChanged);
        //
        //    function dateChanged(ev) {
        //        if (ev.bubbles == undefined) {
        //            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
        //            if (ev.currentTarget.value == "") {
        //                if ($input.data("last-value") == null) {
        //                    $input.data("last-value", ev.currentTarget.defaultValue);
        //                }
        //                var $lastDate = $input.data("last-value");
        //                $input.datepicker("setDate", $lastDate);
        //            } else {
        //                $input.data("last-value", ev.currentTarget.value);
        //            }
        //        }
        //    };
    })
    $("#agreement-modal-container").modal("show");
});

$(document).ready(function () {
    $(".agreements-table tbody tr").each(function() {
        var $row = $(this);
        var height = $(".contractor-cell", $row).height();
        $(".product-comment-box", $row).css("height", height + "px");
        /*$(".product-comment-box", $row).pseudo(":before", "height", height + "px");*/
    });
});

$(document).on("click", "button.btn-confirm-yes", function(){
    var $this = $(this);
    $.ajax({
        url: $this.data("url"),
        type: $this.data("type"),
        data: $this.data("params"),
        success: function(data) {
            $.pjax.reload("#agreement-pjax-container", {push: false, timeout: 5000});
            $(".page-content").prepend("' . $successFlashMessageDelete . '");
        }
    });
    return false;
});

'); ?>

<?php if ($showModal) {
    $this->registerJs('
        $(document).ready(function () {
            $("#agreement_add").click();
        });
    ');
} ?>
