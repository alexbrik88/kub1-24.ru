<?php

use common\models\file\widgets\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $isNewRecord boolean */
/* @var $agreementPjax string */

$agreementPjax = isset($agreementPjax) ? $agreementPjax : '#agreement-pjax-container';
Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
]) ?>
<div class="agreement-form" data-header="Договор">

    <h4><b><?= Html::encode($model->document_name) ?></b></h4>

    <h4><b>№<?= Html::encode($model->document_number) ?>
            от <?= \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'd F Y г.',
                'monthInflected' => true,
                'date' => $model->document_date,
            ]); ?></b></h4>

    <?= FileUpload::widget([
        'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
        'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
        'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
    ]); ?>

</div>
<script type="text/javascript">
    var showModal = <?= isset($isNewRecord) ? 1 : 0; ?>;
    window.AgreementId = "<?= $model->id ?>";
    // Договор&77&2018-10-23&1
    window.AgreementValueLong = "<?= "{$model->agreementType->name}&{$model->getFullNumber()}&{$model->document_date}&{$model->agreementType->id}&{$model->id}" ?>";
    window.AgreementValue = "<?= "{$model->agreementType->name}&{$model->getFullNumber()}&{$model->document_date}&{$model->agreementType->id}" ?>";
    console.log('window.AgreementValue: ', window.AgreementValue);
    var options = {
        container: "<?= $agreementPjax ?>",
        push: false,
    };
    if ($('<?= $agreementPjax ?>').data('url')) {
        options.url = $('<?= $agreementPjax ?>').data('url');
        options.data = {contractorId: <?= $model->contractor_id ?>};
    }
    $.pjax(options);
    $("#agreement-modal-container").modal("hide");
    if (showModal == 1) {
        var isNewRecord = <?= $isNewRecord ? 1 : 0 ?>;
        $(document).ready(function () {
            var message = '';
            if (isNewRecord == 1) {
                message = 'добавлен';
            } else {
                message = 'изменен';
            }

            window.toastr.success("Договор " + message, '', {
                'closeButton': true,
                'showDuration': 1000,
                'hideDuration': 1000,
                'timeOut': 2000,
                'extendedTimeOut': 1000,
                'escapeHtml': false,
            });

        });
    }
</script>
<?php Pjax::end() ?>
