<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\document\status\AgreementStatus;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass  = $status->getIcon();

$statusIcon = [
    AgreementStatus::STATUS_CREATED => 'check-2',
    AgreementStatus::STATUS_PRINTED => 'print',
    AgreementStatus::STATUS_SEND => 'envelope',
    AgreementStatus::STATUS_RECEIVED => 'check-double',
    AgreementStatus::STATUS_REJECTED => 'close',
    AgreementStatus::STATUS_FINISHED => 'close',
];

$statusColor = [
    AgreementStatus::STATUS_CREATED => '#26cd58',
    AgreementStatus::STATUS_PRINTED => '#FAC031',
    AgreementStatus::STATUS_SEND => '#FAC031',
    AgreementStatus::STATUS_RECEIVED => '#FAC031',
    AgreementStatus::STATUS_REJECTED => '#4679AE',
    AgreementStatus::STATUS_FINISHED => '#4679AE',
];
$icon = ArrayHelper::getValue($statusIcon, $model->status_id);
$color = ArrayHelper::getValue($statusColor, $model->status_id);
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: <?= $color ?>;
            border-color: <?= $color ?>;
            color: #ffffff;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->status_updated_at) ?></span>
        </div>
    </div>
</div>

<?php if ($model->document_type_id == AgreementType::TYPE_AGREEMENT) : ?>
    <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).'<span class="ml-2">Счёт</span>',
        ($model->is_completed) ? '#' : Url::to(['/documents/invoice/create',
        'type' => $model->type,
        'contractorId' => $model->contractor_id,
        'fromAgreement' => $model->id]), [
        'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3' ,
        'disabled' => ($model->is_completed) ? true : false
    ]); ?>
<?php endif; ?>

<div class="about-card mb-3 mt-1 pb-0">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $model->documentType->name ?>:
        </span>
        № <?= $model->getFullNumber() ?> от <?= DateHelper::format(
            $model->document_date,
            DateHelper::FORMAT_USER_DATE,
            DateHelper::FORMAT_DATE); ?>
    </div>
    <?php if ($model->document_name): ?>
        <div class="about-card-item">
            <span class="text-grey">
                Название документа:
            </span>
            <?= $model->document_name ?>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">
            Тип контрагента:
        </span>
        <?= ($model->type == Documents::IO_TYPE_OUT) ? 'Покупатель' : 'Поставщик' ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Контрагент:
        </span>
        <?= $model->contractor ? Html::a($model->contractor->getNameWithType(), [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ]) : $model->contractor->getNameWithType(); ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Дата окончания:
        </span>
        <?= DateHelper::format(
            $model->payment_limit_date,
            DateHelper::FORMAT_USER_DATE,
            DateHelper::FORMAT_DATE); ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Ответственный:
        </span>
        <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
    </div>
    <?= frontend\themes\mobile\widgets\file\FileUpload::widget([
        'uploadUrl' => Url::to(['agreement-file-upload', 'id' => $model->id,]),
        'deleteUrl' => Url::to(['agreement-file-delete', 'id' => $model->id,]),
        'listUrl' => Url::to(['agreement-file-list', 'id' => $model->id,]),
    ]); ?>
</div>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span style="font-weight: bold;">Комментарий</span>
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
            'id' => 'comment_internal_update',
            'style' => 'cursor: pointer;',
        ]); ?>
        <div id="comment_internal_view" style="margin-top:5px" class="">
            <?= Html::encode($model->comment_internal) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
                'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_internal_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
            ]); ?>
            <?= Html::textarea('comment_internal', $model->comment_internal, [
                'id' => 'comment_internal_input',
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
            ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>