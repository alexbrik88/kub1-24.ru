<?php

use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\document\AgentReport;
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, ['model' => $model]);
?>

<div class="page-content-in">
    <?php if ($backUrl !== null) {
        echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
            'class' => 'back-to-customers',
        ]);
    } ?>
    <div class="col-xs-12 col-lg-7 pad0">
        <?= $this->render('_viewPartials/_customer_info_full_out', ['model' => $model,
            'message' => $message,
            'dateFormatted' => $dateFormatted,
            'ioType' => $ioType,
            'canUpdate' => $canUpdate,
            'useContractor' => $useContractor,
            'backUrl' => $backUrl
        ]); ?>
    </div>
    <?= $this->render('_viewPartials/_control_buttons_out', ['model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
        'useContractor' => $useContractor,
        'contractorId' => $contractorId,
    ]); ?>

    <div id="buttons-bar-fixed">
        <?= $this->render('_viewPartials/_action_buttons_out', [
            'model' => $model,
            'useContractor' => $useContractor,
            'canUpdate' => $canUpdate,
            'contractorId' => $contractorId,
            'ioType' => $ioType,
            'backUrl' => $backUrl
        ]); ?>
    </div>
</div>

<?php if ($model->type == Documents::IO_TYPE_OUT && $canUpdate): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>