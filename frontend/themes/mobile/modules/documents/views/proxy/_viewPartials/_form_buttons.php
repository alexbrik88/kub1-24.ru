<?php

use common\models\Company;
use common\models\document\Invoice;
use frontend\rbac\permissions\document\Document;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
/* @var $isAuto boolean */

$invoiceBlock = 'invoice-block';
$autoinvoiceBlock = 'autoinvoice-block';

?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-width button-regular button-regular_red button-clr pl-4 pr-4',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', ['view',
                'type' => $ioType,
                'id' => $model->id,
            ], [
                'class' => 'button-clr button-width button-regular button-hover-transparent pl-4 pr-4',
            ]); ?>
        </div>
    </div>
</div>
