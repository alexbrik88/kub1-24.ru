<?php
/* @var $model \common\models\EmployeeCompany */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => '',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

?>

<div class="row">
    <div class="col-6">
        <div class="form-group field-employeecompany-passport_isrf">
            <?= $form->field($model, 'passport_isRf', array_merge($textInputConfig, ['options' => ['class' => '']]))
                ->radioList(['1' => 'РФ', '0' => 'не РФ'], [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label',
                            Html::radio($name, $checked, ['value' => $value]) . $label,
                            [
                                'class' => 'radio-inline p-o radio-padding',
                            ]);
                    },
                ]); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group field-physical-passport-country <?= $model->passport_isRf == 1 ? 'hide' : '' ?>">
            <?= $form->field($model, 'passport_country', array_merge($textInputConfig, [
                'options' => [
                    'class' => '',

                ],
            ]))
            ?>
        </div>
    </div>


    <div class="col-6">
        <div class="form-group field-employeecompany-ppc">
            <?=
            $form->field($model, 'passport_series', array_merge($textInputConfig, ['options' => ['class' => '']]))
                ->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => ($model->passport_isRf == 1) ? '9{2} 9{2}' : '[9|a| ]{1,25}',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => ($model->passport_isRf == 1) ? 'XX XX' : '',
                    ],
                ]);
            ?>
        </div>
        <div class="form-group field-employeecompany-bin">
            <?=
            $form->field($model, 'passport_issued_by', array_merge($textInputConfig, [
                'options' => [
                    'class' => '',
                ],
            ]))->label('Кем выдан:')->textInput([
                'maxlength' => false,
            ]);
            ?>
        </div>
        <div class="form-group field-employeecompany-legal_address">
            <?=
            $form->field($model, 'passport_department', array_merge($textInputConfig, [
                'options' => [
                    'class' => '',
                ],
            ]))->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => ($model->passport_isRf == 1) ? '9{3}-9{3}' : '[9|a| ]{1,255}' ,
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => ($model->passport_isRf == 1) ? 'XXX-XXX' : '',
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group field-employeecompany-current_account">
            <?=
            $form->field($model, 'passport_number', array_merge($textInputConfig, [
                'options' => [
                    'class' => '',
                ],
            ]))->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => ($model->passport_isRf == 1) ? '9{6}' : '[9|a| ]{1,25}',
                //'regex'=> "[0-9]*",
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => ($model->passport_isRf == 1) ? 'XXXXXX' : '',
                ],
            ]);
            ?>
        </div>

        <div class="form-group field-employeecompany-current_account <?= !empty($model->getErrors('passport_date_output')) ? 'has-error' : '' ?>">
            <div class="field-employeecompany-passport_number">
                <label class="label">Дата выдачи:</label>
                <div class="date-picker-wrap" style="width: 130px">
                    <?= \yii\bootstrap4\Html::activeTextInput($model, 'passport_date_output', [
                        'class' => 'form-control date-picker',
                        'value' => DateHelper::format($model->passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]) ?>
                    <svg class="date-picker-icon svg-icon input-toggle">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                    </svg>
                </div>
            </div>
        </div>

        <div class="form-group form-employeecompany-address">
            <?= $form->field($model, 'passport_address', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'required',
                ],
            ]))->label('Адрес регистрации:')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>

    </div>

</div>
