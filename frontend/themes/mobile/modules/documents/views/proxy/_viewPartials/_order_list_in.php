<?php
/** @var \common\models\document\Proxy $model */
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\OrderProxy;
use common\models\product\Product;

$tableClass = isset($tableClass) ? $tableClass : 'table table-style table-count-list';
$plus = 0;
$hasNds = ($model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT);
$plus = OrderProxy::getAvailable($model->id) ? 1 : 0;
?>
<div class="wrap wrap_padding_none">
    <table class="<?= $tableClass; ?>">
        <thead>
        <tr class="heading" role="row">
            <th width="10%">Наименование</th>
            <th width="6%">Количество</th>
            <th width="5%">Ед.измерения</th>
            <?php if ($hasNds) : ?>
                <th width="5%">Ставка</th>
            <?php endif; ?>
            <th width="5%">Цена</th>
            <th width="5%">Сумма</th>
            <th width="1%"></th>
        </tr>
        </thead>
        <tbody id="tbody">
        <?php foreach (\common\models\document\OrderProxy::findAll(['proxy_id' => $model->id]) as $key => $order): ?>
            <?php echo $this->render('_template_in', [
                'key' => $key,
                'order' => $order,
                'model' => $model,
                'hasNds' => $hasNds,
                'precision' => $precision,
            ]);
            ?>

        <?php endforeach; ?>
        </tbody>
    </table>

    <span class="ml-3 mb-3 input-editable-field btn-add-line-table <?=(!$plus) ? 'hide' : '' ?> button-regular button-hover-content-red pl-3" id="plusbtn">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
        </svg>
        <span>Добавить</span>
    </span>

</div>
<?php

$count = count(\common\models\document\OrderProxy::findAll(['proxy_id' => $model->id]));
$urlAdd = \yii\helpers\Url::to('/documents/proxy-ajax/add-new-row');
$urlDel = \yii\helpers\Url::to('/documents/proxy-ajax/delete-row');
$urlSub = \yii\helpers\Url::to('/documents/proxy-ajax/subsitution');
$urlClose = \yii\helpers\Url::to('/documents/proxy-ajax/close');
$urlEdit = \yii\helpers\Url::to('/documents/proxy-ajax/edit');
$script = <<< JS
var count = parseInt('$count');

   $('#plusbtn').click(
    function() {
        $('#plusbtn').addClass('hide');
           var active = [];
        $(".status[value='active']").each(function( index, value ) {
           active.push($(value).closest('tr').find('.quantity').attr('id'));
        });
        console.log(active);
        jQuery.post({
          url : '$urlAdd',
          data : {key : count, invoice_id: '$model->invoice_id', proxy_id: '$model->id', active : active},
          success : function(data) {
              jQuery('#tbody').append(data);
              
              $("select.dropdownlist").select2({
                  theme: "krajee-bs4",
                  width: "100%",
                  minimumResultsForSearch: -1,
                  escapeMarkup: function(markup) {
                      return markup;
                  }
              });
          }
        });
    }

    );
    $(document).on('click', '.delete-row', function() {

       $(this).closest('tr').find('.status').val('deleted');
       var active = [];
                $(".status[value='active']").each(function( index, value ) {
                   active.push($(value).closest('tr').find('.quantity').attr('id'));
                });
                console.log(active);
       jQuery.post({
            url : '$urlDel',
            data : {proxy_id : '$model->id',active : active},
            success : function(data) {
            console.log(data);
                    if (data == 0) $('#plusbtn').addClass('hide');
                    if (data == 1) $('#plusbtn').removeClass('hide');
            }
        });

        if($(this).closest('tr').find('.status').val() == ''){
            $(this).closest('tr').remove();
        }else{
            $(this).closest('tr').hide();
        } ;

       if( $('.order:visible').length == 1){
            $('.delete-row').hide();
       }
       if( $('.order:visible').length > 1){
             $('.delete-row').show();
       }
    });

$(document).on('change','.dropdownlist',function() {
               var active = [];
                $(".status[value='active']").each(function( index, value ) {
                   active.push($(value).closest('tr').find('.quantity').attr('id'));
                });
           jQuery.post({
            url : '$urlSub',
            data : {order_id: $(this).val(), proxy_id : '$model->id',active : active},
            success : function(data) {
                jQuery('#tbody').append(data);
                $('.input-editable-field').removeClass('hide');
                $('.editable-field').addClass('hide');
                if( $('.order:visible').length == 1){
                $('.delete-row').hide();
                }
                if( $('.order:visible').length > 1){
                $('.delete-row').show();
                }
            }
        });
    $(this).closest('tr').remove();
});


    $(document).on('click','.btn-cancel', function() {
        $('.edit-in').hide();
        $('#plusbtn').addClass('hide');
       jQuery.get({
        url : '$urlClose',
        data : {proxy_id : '$model->id',ioType : 2} ,
        success : function(data) {
                jQuery('.customers_table').replaceWith(data);
                 $('.edit-in').show();
            }
       })
    });
   $(document).on('click','.edit-in', function() {
                $('#plusbtn').addClass('hide');
               if( $('.order:visible').length == 1){
                $('.delete-row').hide();
                }
                if( $('.order:visible').length > 1){
                $('.delete-row').show();
                }
                $('.btn-cancel').hide();
        jQuery.get({
            url: '$urlEdit',
            data : {proxy_id : '$model->id',ioType : 2},
            success : function(data) {
               if (data == 0) $('#plusbtn').addClass('hide');
               if (data == 1) $('#plusbtn').removeClass('hide');
               $('.btn-cancel').show();
            }
        });
   })  ;
JS;
$this->registerJs($script, $this::POS_READY);
?>