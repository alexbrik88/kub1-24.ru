<?php

use common\models\AgreementTemplate;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */

$this->context->layout = 'agreements-update';
$this->title = 'Добавить шаблон договора';
?>

<?= $this->render('_form', [
    'model' => $model,
    'type' => $type
]) ?>
