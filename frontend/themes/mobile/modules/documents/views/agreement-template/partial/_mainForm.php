<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use dosamigos\tinymce\TinyMce;
use common\models\AgreementType;

/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */
/* @var $model \common\models\AgreementTemplate */

$textAreaStyle = 'width: 100%; font-size:14px; text-align:justify; white-space: pre-line;';

$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$isNewRecord = $model->isNewRecord;

if ($isNewRecord) {
    if (!trim($model->document_requisites_executer)) {
        $model->document_requisites_executer = <<<STR
<p><strong>{Название_Компании_Контрагента}</strong></p>
<p></p>
<p>Юридический адрес: {Адрес_Контрагента}</p>
<p>ОГРН {ОГРН_Контрагента}</p>
<p>ОКПО {ОКПО_Контрагента}</p>
<p>ИНН {ИНН_Контрагента}</p>
<p>КПП {КПП_Контрагента}</p>
<p>р/с {РС_Контрагента}</p>
<p>в {Название_Банка_Контрагента}</p>
<p>к/с {КС_Контрагента}</p>
<p>БИК {БИК_Банка_Контрагента}</p>
<p></p>
<p>{ФИО_Руководителя_Контрагента_сокращенно}</p>
<p>{Должность_Руководителя_Контрагента}</p>
<p></p>
<p>_________________________________</p>
<p>(подпись)</p>
<p>М.П.</p>
STR;
    }

    if (!trim($model->document_requisites_customer)) {
        $model->document_requisites_customer = <<<STR
<p><strong>{Название_Компании}</strong></p>
<p></p>
<p>Юридический адрес: {Адрес}</p>
<p>ОГРН {ОГРН}</p>
<p>ОКПО {ОКПО}</p>
<p>ИНН {ИНН}</p>
<p>КПП {КПП}</p>
<p>р/с {РС}</p>
<p>в {Название_Банка}</p>
<p>к/с {КС}</p>
<p>БИК {БИК_Банка}</p>
<p></p>
<p>{ФИО_Руководителя_сокращенно}</p>
<p>{Должность_Руководителя}</p>
<p></p>
<p>_________________________________</p>
<p>(подпись)</p>
<p>М.П.</p>
STR;

    }
}
?>

<style>
<?php if ($model->document_type_id == AgreementType::TYPE_AGREEMENT) : ?>
    .agreement-template-form .show_agreement {display: inherit}
    .agreement-template-form .show_not_agreement {display: none}
<?php else : ?>
    .agreement-template-form .show_agreement {display: none}
    .agreement-template-form .show_not_agreement {display: inherit}
<?php endif; ?>
</style>

<?= $form->errorSummary($model); ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Шаблон договора №
                        </span>
                    </span>
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date_input', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-7">
            <div class="row d-block">
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Тип документа<span class="important" aria-required="true">*</span>
                        </label>
                    <?php echo $form->field($model, 'document_type_id', ['template' => "{input}", 'options' => []])
                        ->widget(Select2::class, [
                            'id' => 'document_type_id',
                            'data' => AgreementType::find()->select(['name', 'id'])->orderBy(['sort' => SORT_ASC])->indexBy('id')->column(),
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                            'options' => [
                                'class' => 'form-control',
                            ]
                        ])->label(false); ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Название
                            <span class="show_agreement">договора<span class="important" aria-required="true">*</span></span>
                            <span class="show_not_agreement">документа</span>
                        </label>
                        <?= Html::activeTextInput($model, 'document_name', [
                            'maxlength' => true,
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Тип<span class="show_agreement">&nbsp;договора</span><span class="important" aria-required="true">*</span>
                        </label>
                        <?= $form->field($model, 'type', [
                            'template' => "{input}",
                            'options' => [
                                'class' => '',
                            ],
                        ])->widget(Select2::class, [
                            'hideSearch' => true,
                            'data' => AgreementTemplate::$TYPE,
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'disabled' => ($type) ? true : false,
                            ]
                        ])->label(false); ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-filter">
                                <label class="label">
                                    Доп. номер к №
                                </label>
                                <?= Html::activeTextInput($model, 'document_additional_number', [
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-6">
                            <div style="padding-top: 35px; font-style: italic; color: #999;"> Договор № <strong>АВС</strong>-22</div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-filter">
                                <label class="label">
                                    Отсрочка платежа<span class="important" aria-required="true">*</span>
                                </label>
                                <?= Html::activeTextInput($model, 'payment_delay', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'class' => 'form-control',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-6">
                            <div style="padding-top: 35px;"> дней</div>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Шапка<span class="show_agreement">&nbsp;договора</span><span class="important" aria-required="true">*</span>
                        </label>
                        <?= $form->field($model, 'document_header', [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ]
                        ])->widget(TinyMce::class, [
                            'options' => ['rows' => 8],
                            'language' => 'ru',
                            'clientOptions' => [
                                'plugins' => [
                                    "table paste code"

                                ],
                                'paste_webkit_styles' => "font-style text-align",
                                'menubar' => false,
                                'toolbar' => false, //"code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                'content_style' => "p {margin-top:0;margin-bottom:0}",
                                'branding' => false
                            ]
                        ]) ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Предмет договора<span class="important" aria-required="true">*</span>
                        </label>
                        <?= $form->field($model, 'document_body', [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ]
                        ])->widget(TinyMce::class, [
                            'options' => ['rows' => 8],
                            'language' => 'ru',
                            'clientOptions' => [
                                'plugins' => [
                                    "table paste code"

                                ],
                                'paste_webkit_styles' => "font-style text-align",
                                'menubar' => false,
                                'toolbar' => false, //"code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                'content_style' => "p {margin-top:0;margin-bottom:0}",
                                'branding' => false
                            ]
                        ]) ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Ваши реквизиты<span class="important" aria-required="true">*</span>
                        </label>
                        <?= $form->field($model, 'document_requisites_customer', [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ]
                        ])->widget(TinyMce::class, [
                            'options' => ['rows' => 8],
                            'language' => 'ru',
                            'clientOptions' => [
                                'plugins' => [
                                    "table paste code"

                                ],
                                'paste_webkit_styles' => "font-style text-align",
                                'menubar' => false,
                                'toolbar' => false, //"code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                'content_style' => "p {margin-top:0;margin-bottom:0}",
                                'branding' => false
                            ]
                        ]) ?>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Реквизиты контрагента<span class="important" aria-required="true">*</span>
                        </label>
                        <?= $form->field($model, 'document_requisites_executer', [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ]
                        ])->widget(TinyMce::class, [
                            'options' => ['rows' => 8],
                            'language' => 'ru',
                            'clientOptions' => [
                                'plugins' => [
                                    "table paste code"

                                ],
                                'paste_webkit_styles' => "font-style text-align",
                                'menubar' => false,
                                'toolbar' => false, //"code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                'content_style' => "p {margin-top:0;margin-bottom:0}",
                                'branding' => false
                            ]
                        ]) ?>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="col-5">
            <div class="agreement-template-words">
                <div class="atw-customer">
                    <p><strong>Ваши реквизиты</strong></p>
                    <span>
                    <?php foreach (AgreementTemplate::$CUSTOMER_PATTERNS as $pattern) : ?>
                        <p><?=$pattern?></p>
                    <?php endforeach; ?>
                </span>
                </div>
                <div class="atw-contragent">
                    <p><strong>Реквизиты контрагента</strong></p>
                    <span>
                    <?php foreach (AgreementTemplate::$CONTRACTOR_PATTERNS as $pattern) : ?>
                        <p><?=$pattern?></p>
                    <?php endforeach; ?>
                </span>
                </div>
                <div class="atw-document show_agreement">
                    <p><strong>Данные договора</strong></p>
                    <span>
                    <?php foreach (AgreementTemplate::$DOCUMENT_PATTERNS_AGREEMENT as $pattern) : ?>
                        <p><?=$pattern?></p>
                    <?php endforeach; ?>
                </span>
                </div>
                <div class="atw-document show_not_agreement">
                    <p><strong>Данные документа</strong></p>
                    <span>
                    <?php foreach (AgreementTemplate::$DOCUMENT_PATTERNS as $pattern) : ?>
                        <p><?=$pattern?></p>
                    <?php endforeach; ?>
                </span>
                </div>
                <div class="atw-document">
                    <p><strong>Добавить свой вариант</strong></p>
                    <span>
                    <?php $model->setOtherPatterns(); ?>
                        <?php $i=0; foreach (AgreementTemplate::$OTHER_PATTERNS as $pattern=>$value) : $i++; ?>
                            <p class="pattern_row pattern-<?=$i?>" data-row="<?=$i?>">
                            <span class="pattern_key"  style="display: inline-block"><?=Html::encode($pattern);?></span>
                            <span style="display: inline-block"> - </span>
                            <span class="pattern_value" style="display: inline-block"><?=Html::encode($value);?></span>
                            <span style="display: inline-block; line-height: 22px; padding-left:10px;">
                            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                                'class' => 'pl-1 pr-1 btn-link other_pattern_edit',
                                'title' => 'Редактировать',
                                'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                                'type'  => 'submit'
                            ]); ?>
                            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                                'class' => 'pl-1 pr-1 btn-link other_pattern_delete',
                                'title' => 'Удалить',
                                'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                            ]); ?>
                            </span>
                            <?=Html::input('hidden', 'pattern['.$i.']', json_encode(['key'=>$pattern, 'value'=>$value])); ?>
                        </p>
                        <?php endforeach; ?>

                    <p id="new_pattern_row" style="display: none; margin-top: 25px">
                        <span style="display: inline-block">
                            <?= Html::input('text', 'new_pattern_key', '', [
                                'type' => 'text',
                                'class' => 'form-control pattern_key',
                                'title' => 'Шаблон',
                                'style' => 'display: inline-block; margin-right:0px; width: 30%;',])
                            ?>
                            <span style="display: inline-block"> - </span>
                            <?= Html::input('text', 'new_pattern_value', '', [
                                'type' => 'text',
                                'class' => 'form-control pattern_value',
                                'title' => 'Подставлять этот текст',
                                'style' => 'display: inline-block; margin-right:0px; width: 50%;',])
                            ?>

                            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'check-2']), [
                                'id' => 'new_pattern_save',
                                'class' => 'pl-1 pr-1 btn-link',
                                'title' => 'Сохранить',
                                'style' => 'display: inline-block; margin-right:0px; margin-top:3px; cursor: pointer',
                                'type'  => 'submit'
                            ]); ?>
                        </span>
                    </p>

                    <p id="edit_pattern_row" style="display: none;" data-edited="">
                        <span style="display: inline-block">
                            <?= Html::input('text', 'edit_pattern_key', '', [
                                'type' => 'text',
                                'class' => 'form-control pattern_key',
                                'title' => 'Шаблон',
                                'style' => 'display: inline-block; margin-right:0px; width: 30%;',])
                            ?>
                            <span style="display: inline-block"> - </span>
                            <?= Html::input('text', 'edit_pattern_value', '', [
                                'type' => 'text',
                                'class' => 'form-control pattern_value',
                                'title' => 'Подставлять этот текст',
                                'style' => 'display: inline-block; margin-right:0px; width: 50%;',])
                            ?>

                            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'check-2']), [
                                'class' => 'pl-1 pr-1 btn-link edit_pattern_save',
                                'title' => 'Сохранить',
                                'style' => 'display: inline-block; margin-right:0px; margin-top:3px; cursor: pointer',
                                'type'  => 'submit'
                            ]); ?>
                        </span>
                    </p>

                    <p class="clear_pattern_row" data-row="-1" style="display: none">
                        <span class="pattern_key"  style="display: inline-block"></span>
                        <span style="display: inline-block"> - </span>
                        <span class="pattern_value" style="display: inline-block"></span>
                        <span style="display: inline-block; line-height: 22px; padding-left:10px;">
                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'class' => 'pl-1 pr-1 btn-link other_pattern_edit',
                            'title' => 'Редактировать',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                            'type'  => 'submit'
                        ]); ?>
                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                            'class' => 'pl-1 pr-1 btn-link other_pattern_delete',
                            'title' => 'Удалить',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                        </span>
                    </p>

                    <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'add-icon']), [
                        'id' => 'other_pattern_add',
                        'class' => 'btn btn-add-line-table button-clr button-regular button-hover-content-red',
                        'title' => 'Добавить свой вариант',
                    ]); ?>

                    <button id="plusbtn" class="input-editable-field hide btn yellow btn-add-line-table button-clr button-regular button-hover-content-red" type="button">
                        <?= $this->render('//svg-sprite', ['ico' => 'add-icon']) ?>
                        <span class="add-emails">Добавить</span>
                    </button>

                </span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS

    $(document).on("click", ".other_pattern_delete", function () {
        $(this).parents(".pattern_row").remove();
    });

    $(document).on("click", "#other_pattern_add", function () {
        if ($("#new_pattern_row").is(":hidden")) {
            $(".pattern_row").show();
            $("#edit_pattern_row").hide();
            $("#new_pattern_row").show();
        } else {
            $("#new_pattern_row").hide();
        }
    });

     $(document).on("click", ".other_pattern_edit", function () {
        $(".pattern_row").show();
        $("#new_pattern_row").hide();
        var row = $(this).parents(".pattern_row");
        var p_key = $(row).find(".pattern_key").html();
        var p_val = $(row).find(".pattern_value").html();
        var edit_pattern_row = $("#edit_pattern_row");
        $(row).after(edit_pattern_row).hide();
        $(edit_pattern_row).show();
        $(edit_pattern_row).find("input.pattern_key").val(p_key);
        $(edit_pattern_row).find("input.pattern_value").val(p_val);
        $(edit_pattern_row).data("edited", $(row).data("row"));
    });

    $(document).on("click", "#new_pattern_save", function () {
        var editable_row = $("#new_pattern_row");
        var p_key = $(editable_row).find(".pattern_key").val().trim().replace(/[&\/\\#,+()$~%.\'":*?<>{}]/g, "");
        var p_val = $(editable_row).find(".pattern_value").val().trim().replace(/[&\/\\#,+()$~%.\'":*?<>{}]/g, "");

        p_key = "{" + p_key + "}";

        if (!p_key || !p_val || p_key.length < 3)
            return false;

        var max_row = 0;
        $(".pattern_row").each(function() {
            if ($(this).data("row") > max_row)
                max_row = $(this).data("row");
        });
        max_row += 1;

        var new_row = $(".clear_pattern_row").clone();
        $(new_row).removeClass("clear_pattern_row").addClass("pattern_row").addClass("pattern-" + max_row);
        $(new_row).attr("data-row", max_row);
        $(new_row).find(".pattern_key").html(p_key);
        $(new_row).find(".pattern_value").html(p_val);

        $(new_row).append(
            $("<input/>",{
                type:"hidden",
                name: "pattern["+max_row+"]",
                value:JSON.stringify({"key":p_key, "value":p_val})})
        );

        $(editable_row).before(new_row);
        $(new_row).show();
        $(editable_row).find("input").val("");
    });

    $(document).on("click", ".edit_pattern_save", function () {
        var edit_pattern_row = $("#edit_pattern_row");
        var edited_row_num = $(edit_pattern_row).data("edited");
        var row = $(".pattern-" + edited_row_num);
        var p_key = $(edit_pattern_row).find(".pattern_key").val().replace(/[&\/\\#,+()$~%.\'":*?<>{}]/g, "");
        var p_val = $(edit_pattern_row).find(".pattern_value").val().replace(/[&\/\\#,+()$~%.\'":*?<>{}]/g, "");

        p_key = "{" + p_key + "}";

        $(row).find(".pattern_key").html(p_key);
        $(row).find(".pattern_value").html(p_val);
        $(row).find("input").val(JSON.stringify({"key":p_key, "value":p_val}));
        $(row).show();
        $(edit_pattern_row).hide();
    });

    $('#agreementtemplate-document_type_id').change(function() {
        if ($(this).val() == 1) {
            $('.show_agreement').show();
            $('.show_not_agreement').hide();
        } else {
            $('.show_agreement').hide();
            $('.show_not_agreement').show();
        }
    })
JS
);
