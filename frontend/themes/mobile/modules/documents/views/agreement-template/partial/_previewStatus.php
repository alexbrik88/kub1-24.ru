<?php

use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use common\widgets\Modal;
use frontend\models\Documents;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$statusIcon = [
    AgreementTemplate::STATUS_ACTIVE => 'check-2',
    AgreementTemplate::STATUS_ARCHIVED => 'close',
    AgreementTemplate::STATUS_DELETED => 'garbage',
];

$statusColor = [
    AgreementTemplate::STATUS_ACTIVE => '#FAC031',
    AgreementTemplate::STATUS_ARCHIVED => '#FAC031',
    AgreementTemplate::STATUS_DELETED => '#4679AE',

];
$icon = ArrayHelper::getValue($statusIcon, $model->status);
$color = ArrayHelper::getValue($statusColor, $model->status);

?>

    <div class="sidebar-title d-flex flex-wrap align-items-center">
        <div class="column flex-grow-1 mt-1 mt-xl-0">
            <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
                    background-color: <?= $color ?>;
                    border-color: <?= $color ?>;
                    color: #ffffff;
                    ">
                <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
                <span class="ml-3">
                    <?php if ($model->status == AgreementTemplate::STATUS_ACTIVE) echo 'В работе' ?>
                    <?php if ($model->status == AgreementTemplate::STATUS_ARCHIVED) echo 'В архиве' ?>
                    <?php if ($model->status == AgreementTemplate::STATUS_DELETED) echo 'Удален' ?>
                </span>
                <span class="ml-auto mr-1"><?= date('d.m.Y', $model->created_at) ?></span>
            </div>
        </div>
    </div>

    <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).'<span class="ml-2">'.mb_strtoupper($model->documentType->name).'</span>',
    '#', [
        'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3 ' . ($model->status != AgreementTemplate::STATUS_ACTIVE ? '' : 'agreement-modal-link'),
        'style' => 'padding-left:4px;' . ($model->status != AgreementTemplate::STATUS_ACTIVE ? 'background:#a2a2a2;' : ''),
        'disabled' => $model->status != AgreementTemplate::STATUS_ACTIVE,
        'data-url' => Url::to(['/documents/agreement/create', 'type' => $model->type, 'from_template' => $model->id])
    ]); ?>

    <div class="about-card mb-3 mt-1 pb-3">
        <div class="about-card-item">
            <span class="text-grey">
                Шаблон:
            </span>
            № <?= $model->document_number ?> от <?= DateHelper::format(
                $model->document_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE); ?>
        </div>
        <div class="about-card-item">
            <span class="text-grey">
                Название документа:
            </span>
            <?= $model->document_name ?>
        </div>
        <div class="about-card-item">
            <span class="text-grey">
                Тип контрагента:
            </span>
            <?= ($model->type == $model::TYPE_WITH_BUYER) ? 'Покупатель' : 'Поставщик' ?>
        </div>
        <?php if ($model->payment_delay > 0) : ?>
        <div class="about-card-item">
            <span class="text-grey">
                Отсрочка платежа:
            </span>
            <?= $model->payment_delay; ?> дней
        </div>
        <?php endif; ?>
        <div class="about-card-item">
            <span class="text-grey">
                Ответственный:
            </span>
            <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
        </div>
        <div class="about-card-item">
            <span class="text-grey">
                Создано документов:
            </span>
            <?= (int)$model->documents_created ?>
        </div>
    </div>

    <div>
        <div class="title-small mb-3 nowrap">
            <strong class="mr-1">
                <?php if ($model->comment_internal) : ?>
                    Комментарий
                <?php else : ?>
                    Комментарий
                <?php endif ?>
            </strong>
            <span id="comment_internal_update">
            <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
        </span>
            <div id="comment_internal_view" style="margin-top:5px" class="">
                <?= Html::encode($model->comment_internal) ?>
            </div>
            <?php if ($canUpdate) : ?>
                <?= Html::beginTag('div', [
                    'id' => 'comment_internal_form',
                    'class' => 'hidden',
                    'style' => 'position: relative; margin-top: 4px;',
                    'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
                ]) ?>
                <?= Html::tag('i', '', [
                    'id' => 'comment_internal_save',
                    'class' => 'fa fa-floppy-o',
                    'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
                ]); ?>
                <?= Html::textarea('comment_internal', $model->comment_internal, [
                    'id' => 'comment_internal_input',
                    'rows' => 3,
                    'maxlength' => true,
                    'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
                ]); ?>
                <?= Html::endTag('div') ?>
            <?php endif ?>
        </div>
    </div>


<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>