<?php

use common\models\AgreementTemplate;
use common\components\date\DateHelper;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AgreementType;
use common\components\image\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$signatureLink = (!$model->company->chief_signature_link) ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

$printLink = (!$model->company->print_link) ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$images = [
    'logo' => $logoLink,
    'print' => $printLink,
    'signature' => $signatureLink
];

?>
<div class="row">
    <div class="col-12 pre-view-table">
        <div class="col-12 pad0" style="height: <?= $model->document_type_id == AgreementType::TYPE_AGREEMENT ? '80px' : '40px' ?>;">
            <div class="col-12 pad0 font-bold" style="height: inherit">

                <div style="padding-top: 12px">

                    <?php if ($model->hasLogo()) : ?>
                        <img style="max-height: 70px;max-width: 170px; position:absolute; top:25px; <?= $model->getLogoPosition() == 'RIGHT' ? 'right:0' : 'left:0'; ?>" src="<?= $logoLink ?>" alt="">
                    <?php endif; ?>

                    <?php if ($model->document_name) : ?>
                        <p style="font-size: 17px; width:100%; text-align: center">
                            <?php if ($model->document_type_id == \common\models\AgreementType::TYPE_AGREEMENT): ?>
                                Договор <br/>
                            <?php else : ?>
                                <br/>
                            <?php endif; ?>
                            <?= $model->document_name; ?>
                        </p>
                    <?php endif; ?>
                </div>

            </div>

        </div>

        <div class="col-12 pad0 preview-agreement-template" style="margin-top: 40px;">
            <div class="col-12 pad0" style="font-size: 12px; text-align:justify">
                <?= ($model->highlightPatterns('document_header', $images)) ?>
                <br/>
                <?= ($model->highlightPatterns('document_body', $images)) ?>
                <br/>
                <div class="row">
                    <div class="col-6" style="text-align: left">
                        <?= ($model->highlightPatterns(($model->type == Documents::IO_TYPE_IN) ? 'document_requisites_executer' : 'document_requisites_customer', $images)) ?>
                    </div>
                    <div class="col-6" style="text-align: left">
                        <?= ($model->highlightPatterns(($model->type == Documents::IO_TYPE_IN) ? 'document_requisites_customer' : 'document_requisites_executer', $images)) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
