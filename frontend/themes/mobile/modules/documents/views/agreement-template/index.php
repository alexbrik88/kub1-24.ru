<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\grid\GridView;
use common\models\AgreementTemplate;
use common\models\employee\Employee;
use frontend\widgets\BtnConfirmModalWidget;
use yii\grid\ActionColumn;
use frontend\models\Documents;

/* @var $searchModel \frontend\modules\documents\models\AgreementTemplateSearch */

$this->title = 'Шаблоны договоров';

if ($type) {
    $this->title .= ($type == Documents::IO_TYPE_OUT) ? ' с покупателями' : ' с поставщиками';
}

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>

        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).Html::tag('span', 'Добавить'), [
            'create',
            'type' => $type,
        ], [
            'class' => 'button-regular button-regular_red button-width ml-auto agreement-modal-link',
        ]); ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <?= $this->render('../layouts/_agreement-new-toggle', ['type' => $type]) ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Номер шаблона или название шаблона',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            [
                'attribute' => 'document_number',
                'label' => '№ шаблона',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '8%',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if (!$data['document_number'])
                        $data['document_number'] = '---';
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]) ?
                        Html::a($data['document_number'], ['/documents/agreement-template/view', 'id' => $data['id']]) :
                        $data['document_number'];
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'headerOptions' => [
                    'width' => '18%',
                ],
                'contentOptions' => [
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'document_type_id',
                'filter' => $searchModel->getAgreementTypeFilterItems(),
                'format' => 'raw',
                'label' => 'Тип доку&shy;мента',
                'encodeLabel' => false,
                'value' => function ($data) {
                    return $data['agreementType'];
                },
            ],
            [
                'attribute' => 'document_name',
                'label' => 'Название',
                'headerOptions' => ['style' => 'width:35%']
            ],
            /*
            [
                'attribute' => 'type',
                'label' => 'Тип',
                'headerOptions' => [
                    'width' => '13%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    return isset(AgreementTemplate::$TYPE[$data['type']]) ? AgreementTemplate::$TYPE[$data['type']] : null;
                },
                'format' => 'raw',
                'filter' => [
                    '' => 'Все',
                    1 => 'С продавцом',
                    2 => 'С покупателем',
                ]
            ],
            */
            [
                'attribute' => 'documents_created',
                'label' => 'Кол-во созданных договоров',
                'headerOptions' => ['style' => 'width:8%;']
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '7%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    return isset(AgreementTemplate::$STATUS[$data['status']]) ? AgreementTemplate::$STATUS[$data['status']] : null;
                },
                'format' => 'raw',
                'filter' => [
                    '' => 'Все',
                    1 => 'В работе',
                    2 => 'В архиве',
                ]
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    $employee = Employee::findOne([
                        'id' => $data['created_by']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getCreators()
            ],
        ],
    ]); ?>
</div>
