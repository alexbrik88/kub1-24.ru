<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

//$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';


$paymentDocuments = [];
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
}
$precision = $model->invoice->price_precision;
?>
<style>
@media print {
    thead {
        display: table-header-group;
    }
}
</style>

<?php
ob_start();
?>
<div class="page-content-in p-center-album pad-pdf-p-album">

    <table class="table no-border" style="margin-bottom:10px;">
        <tr>
            <td class="text-center">
                <h4>Счет-фактура № <?php echo $model->fullNumber; ?> от <?php echo $dateFormatted; ?></h4>
            </td>
        </tr>
    </table>

    <div class="font-size-8 padding-min">
        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Поставщик:
            <?php echo $model->invoice->company_name_short; ?>
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            БИН и адрес места нахождения поставщика:
            <?php echo $model->invoice->company_name_short . ', ' . $model->invoice->company_address_legal_full; ?>
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            ИИК поставщика:
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Договор (контракт) на поставку товаров (работ, услуг):
            &nbsp;
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Условия оплаты по договору (контракту):
            безналичный расчет
        </div>

        <div style="border-bottom: 1px solid #000;">
            Пункт назначения поставляемых товаров (работ, услуг):
            <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                echo $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
            } else {
                echo $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
            } ?>
        </div>
        <div style="margin-bottom: 3px;text-align:center;font-style:italic" class="font-size-6">
            государство, регион, область, город, район
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Поставка товаров (работ, услуг) осуществлена по доверенности:
            &nbsp;
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Способ отправления:
            &nbsp;
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Товарно-транспортная накладная:
            &nbsp;
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Грузоотправитель:
            <?= $model->invoice->production_type ? (
            $model->consignor ?
                $model->consignor->getRequisitesFull() :
                'он же'
            ) : Product::DEFAULT_VALUE; ?>
        </div>
        <div style="margin-bottom: 3px;text-align:center;font-style:italic" class="font-size-6">
            (БИН, наименование и адрес)
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            Грузополучатель:
            <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
            } else {
                $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
            }
            echo $model->invoice->production_type ? ($model->consignee ?
                $model->consignee->getRequisitesFull($address) :
                $model->invoice->contractor_name_short.', '.$address
            ) : \common\models\product\Product::DEFAULT_VALUE; ?>
        </div>
        <div style="margin-bottom: 3px;text-align:center;font-style:italic" class="font-size-6">
            (БИН, наименование и адрес)
        </div>

        <div style="margin-bottom: 3px; font-weight:bold; border-bottom: 1px solid #000;">
            Получатель:
            <?php echo $model->invoice->contractor_name_short; ?>
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            БИН и адрес места нахождения получателя: <?php echo $model->invoice->contractor_address_legal_full; ?>
        </div>

        <div style="margin-bottom: 3px; border-bottom: 1px solid #000;">
            ИИК покупателя:
        </div>
    </div>

    <table class="table" style="margin-top:10px;">
        <thead>
        <tr>
            <th class="text-center font-size-8" width="4%" data-column="1" rowspan="2">№ п/п</th>
            <th class="text-center font-size-8" width="13%" data-column="2" rowspan="2">Наименование товаров (работ, услуг)</th>
            <th class="text-center font-size-8" data-column="3" rowspan="2">Ед. изм.</th>
            <th class="text-center font-size-8" width="5%" data-column="4" rowspan="2">Кол-во (объем)</th>
            <th class="text-center font-size-8" data-column="5" rowspan="2">Цена (KZT)</th>
            <th class="text-center font-size-8" width="10%" data-column="6" rowspan="2">Стоимость товаров (работ, услуг) без НДС</th>
            <th class="text-center font-size-8" data-column="7,8" colspan="2">НДС</th>
            <th class="text-center font-size-8" width="10%" data-column="9" rowspan="2">Всего стоимость реализации</th>
            <th class="text-center font-size-8" data-column="10,11" colspan="2">Акциз</th>
        </tr>
        <tr>
            <th class="text-center font-size-8">Ставка</th>
            <th class="text-center font-size-8">Сумма</th>
            <th class="text-center font-size-8">Ставка</th>
            <th class="text-center font-size-8">Сумма</th>
        </tr>
        </thead>
        <tbody class="no-border-buttom">
        <tr class="td-p-0">
            <td class="text-center font-size-8">1</td>
            <td class="text-center font-size-8">2</td>
            <td class="text-center font-size-8">3</td>
            <td class="text-center font-size-8">4</td>
            <td class="text-center font-size-8">5</td>
            <td class="text-center font-size-8">6</td>
            <td class="text-center font-size-8">7</td>
            <td class="text-center font-size-8">8</td>
            <td class="text-center font-size-8">9</td>
            <td class="text-center font-size-8">10</td>
            <td class="text-center font-size-8">11</td>
        </tr>
        <?php $pos=0; foreach ($model->ownOrders as $ownOrder): $pos++; ?>
            <?php
            $invoiceOrder = $ownOrder->order;
            $product = $invoiceOrder->product;
            $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
            ?>
            <tr>
                <td data-column="1" class="text-center">
                    <?= $pos ?>
                </td>
                <td data-column="2" class="text-left">
                    <?= $invoiceOrder->product_title; ?>
                </td>
                <td data-column="3" class="text-center">
                    <?php
                    $value = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
                    echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                    ?>
                </td>
                <td data-column="4" class="text-center">
                    <?= ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']); ?>
                </td>
                <td data-column="5" class="text-right">
                    <?php
                    $value = $invoiceOrder->selling_price_no_vat ?
                        TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
                        Product::DEFAULT_VALUE;
                    echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                    ?>
                </td>
                <td data-column="6" class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($ownOrder->amountNoNds, $precision); ?>
                </td>
                <td data-column="7" class="text-center">
                    <?= $invoiceOrder->saleTaxRate->name; ?>
                </td>
                <td data-column="8" class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision); ?>
                </td>
                <td data-column="9" class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($ownOrder->amountWithNds, $precision); ?>
                </td>
                <td data-column="10" class="text-center">
                    <?= $invoiceOrder->excise_price > 0 ? TextHelper::invoiceMoneyFormat($ownOrder->amountNoNds / $invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                </td>
                <td data-column="11" class="text-center">
                    <?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="5" class="font-bold">Всего по счету</td>
            <td class="text-right font-bold"><?= TextHelper::invoiceMoneyFormat($model->totalAmountNoNds, 2); ?></td>
            <td class="text-center">X</td>
            <td class="text-right font-bold"><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
            <td class="text-right font-bold"><?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?></td>
            <td class="text-center" colspan="2">X</td>
        </tr>
        </tbody>
    </table>

    <table class="table no-border" style="">
        <tr>
            <td class="font-size-8-bold" width="20%">
                Руководитель:
                <?= $model->invoice->getCompanyChiefFio(true); ?>
            </td>
            <td width="25%"></td>
            <!--М.П.-->
            <td width="5%" rowspan="6"></td>
            <td width="3%"></td>
            <td width="5%" rowspan="6"></td>
            <!--М.П.-->
            <td class="font-size-8-bold" width="42%">ВЫДАЛ (ответственное лицо поставщика)</td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid #000">&nbsp;</td>
            <!--М.П.-->
            <td></td>
            <!--М.П.-->
            <td style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-6 text-center" style="font-style: italic;">(Ф.И.О., подпись)</td>
            <!--М.П.-->
            <td></td>
            <!--М.П.-->
            <td class="font-size-6 text-center" style="font-style: italic;">должность</td>
        </tr>

        <tr>
            <td class="font-size-8-bold">
                Главный бухгалтер:
            </td>
            <td></td>
            <!--М.П.-->
            <td class="font-size-8-bold text-center" style="border:1px solid #000;padding:10px;">М.П.</td>
            <!--М.П.-->
            <td class="font-size-8-bold"></td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid #000">&nbsp;</td>
            <!--М.П.-->
            <td></td>
            <!--М.П.-->
            <td style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-6 text-center" style="font-style: italic;">(Ф.И.О., подпись)</td>
            <!--М.П.-->
            <td></td>
            <!--М.П.-->
            <td class="font-size-6 text-center" style="font-style: italic;">(Ф.И.О., подпись)</td>
        </tr>
    </table>

</div>

<?php
// замена тройного тире на одно длинное
$content = ob_get_contents();
ob_end_clean();
echo strtr($content, ['---' => '&mdash;']);
?>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>
