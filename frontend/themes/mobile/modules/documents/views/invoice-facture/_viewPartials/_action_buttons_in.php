<?php

use frontend\rbac\permissions;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $useContractor boolean */

$candelete = Yii::$app->user->can(permissions\document\Document::DELETE, [
    'model' => $model,
]);
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $model->type,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-hover-transparent w-full',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => [
                                'document-print',
                                'actionType' => 'pdf',
                                'id' => $model->id,
                                'type' => $model->type,
                                'filename' => $model->getPdfFileName(),
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => [
                                'docx',
                                'type' => $model->type,
                                'id' => $model->id,
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a('<i class="icon pull-left fa fa-copy"></i> <span>Копия</span>', [
                'original',
                'id' => $model->id,
                'type' => $model->type,
                'val' => 0,
            ], [
                'class' => 'button-clr button-regular button-hover-transparent w-full is-original-button',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Оригинал</span>', [
                'original',
                'id' => $model->id,
                'type' => $model->type,
                'val' => 1,
            ], [
                'class' => 'button-clr button-regular button-hover-transparent w-full is-original-button',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($candelete): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'type' => $model->type,
                        'id' => $model->id,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить этот счёт?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>