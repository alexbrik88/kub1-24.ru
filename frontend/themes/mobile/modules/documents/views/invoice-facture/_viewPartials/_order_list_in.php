<?php

use common\models\address\Country;

/** @var \common\models\document\InvoiceFacture $model */

$countryList = Country::find()->select('name_short')->indexBy('id')->column();
?>

<table class="table table-style table-count in-packinglist_table">
    <thead>
    <tr class="heading" role="row">
        <th width="10%">Наименование</th>
        <th width="5%">Количество</th>
        <th width="5%">Ед.измерения</th>
        <th width="5%">Ставка</th>
        <th width="5%">Цена</th>
        <th width="5%">Сумма</th>
        <th width="5%" class="editable-field">Страна происх. то&shy;ва&shy;ра</th>
        <th width="5%" class="editable-field">
            Регистрацион&shy;ный но&shy;мер та&shy;мо&shy;жен&shy;ной де&shy;кла&shy;ра&shy;ции
        </th>
        <th width="1%"></th>
    </tr>
    </thead>
    <tbody class="document-orders-rows">
    <?php foreach ($model->ownOrders as $key => $order): ?>
        <?= $this->render('_order_list_in_row', [
            'model' => $model,
            'order' => $order,
            'key' => $key,
            'precision' => $precision,
            'countryList' => $countryList,
        ]) ?>
    <?php endforeach; ?>
    </tbody>
</table>
