<?php

use common\components\date\DateHelper;
use yii\helpers\Url;
use \common\components\helpers\Html;
use common\components\TextHelper;
use \philippfrenzel\yii2tooltipster\yii2tooltipster;

/** @var \common\models\document\InvoiceFacture $model */
/** @var $useContractor boolean */
/** @var $contractorId integer|null */
?>


<div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
    <div class="col-xs-12" style="padding-right:0px !important;">
        <div class="control-panel col-xs-12 pad0 pull-right">
            <div class="status-panel col-xs-12 pad0">
                <div class="col-xs-12 col-sm-3 pad3">
                    <div class="btn full_w marg darkblue no-margin-l box-status-act"
                         style="padding-left:0px; padding-right:0px;text-align: center; "
                         title="Дата изменения статуса">
                        <?= date("d.m.Y", $model->status_out_updated_at); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9 pad0">
                    <div class="col-xs-12 pad3">
                        <div class="btn full_w marg btn-status full_w  darkblue" title="Статус">
                            <i class="pull-left icon <?= $model->statusOut->getIcon(); ?>"></i>
                            <?= $model->statusOut->name; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
                <div class="col-xs-3"> &nbsp;</div>
                <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
                    <div class="document-panel widthe-i overflow-hide"
                         style="width: 99.9% !important; margin-bottom: 0px;">
                        <?php if ($model->getInvoices()->count() == 1): ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $model->invoice,
                            ])
                            ): ?>
                                <a href="<?php echo Url::toRoute(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id, 'contractorId' => $contractorId,]); ?>"
                                   class="btn btn-account yellow pull-right full_w"
                                   style="height: 34px;margin-left: 0; text-transform: uppercase;">
                                    <?php $class = $model->invoice->invoiceStatus->className(); ?>
                                    <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                                    СЧЕТ
                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a data-toggle="dropdown" class="btn yellow dropdown-toggle"
                               style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;"
                               title="Счет">
                                <i class="pull-left icon icon-doc"></i>
                                Счет
                            </a>
                            <ul class="dropdown-menu documents-dropdown">
                                <?php foreach ($model->invoices as $invoice): ?>
                                    <li>
                                        <a class="dropdown-item" style="position: relative;"
                                           href="<?= Url::to(['invoice/view', 'type' => $model->type, 'id' => $invoice->id]) ?>">
                                            <span class="dropdown-span-left">
                                                № <?= $invoice->document_number; ?>
                                                от  <?= DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                            </span>
                                            <span class="dropdown-span-right">
                                                <i class="fa fa-rub"></i>
                                                <?= TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
            <div class="portlet">
                <div class="customer-info" style="min-height:auto !important">
                    <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                        <table class="table no_mrg_bottom">
                            <tr>
                                <td>
                        <span
                                class="customer-characteristic color-black"><?= $message->get(\frontend\modules\documents\components\Message::CONTRACTOR); ?>
                            :</span>
                                    <span><?= Html::a($model->invoice->contractor_name_short, [
                                            '/contractor/view',
                                            'type' => $model->invoice->contractor->type,
                                            'id' => $model->invoice->contractor->id,
                                        ]) ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if ($model->invoice->basis_document_name &&
                                        $model->invoice->basis_document_number &&
                                        $model->invoice->basis_document_date
                                    ) : ?>
                                        <span class="customer-characteristic color-black">Основание:</span>
                                        <span class="color-black">
                                            <?= $model->invoice->basis_document_name ?>
                                            № <?= Html::encode($model->invoice->basis_document_number) ?>
                                            от <?= DateHelper::format($model->invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </span>
                                    <?php else : ?>
                                        <span class="customer-characteristic color-black">Основание:</span>
                                        <span class="color-black">
                                            Счет № <?= $model->invoice->fullNumber; ?>
                                            от <?= DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </span>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="table no_mrg_bottom editable-field" style="max-width: 548px;">
                                        <tr>
                                            <td style="padding-left:0;line-height: 32px; width: 1%; white-space: nowrap;">
                                                Подпись:
                                            </td>
                                            <td style="padding-left: 0;">
                                                <table>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <label class="rounded-switch" for="add_stamp" style="margin: 4px 0 0;">
                                                                <?= Html::checkbox('add_stamp', $model->add_stamp, [
                                                                    'id' => 'add_stamp',
                                                                    'class' => 'switch'
                                                                ]) ?>
                                                                <span class="sliderr no-gray yes-yellow round"></span>
                                                            </label>
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                        <span class="tooltip-hover ico-question valign-middle"
                                                              data-tooltip-content="#tooltip_add_stamp"
                                                              style="vertical-align: top;">
                                                        </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="margin-bottom: 5px;">
                                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                            'model' => $model,
                                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                                        ]); ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 visible-lg-block" style="padding: 68px 0px;border-right: 1px solid #4276a4;"></div>
    </div>
</div>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
        Добавить в СФ подпись
        <br>
        при отправке по e-mail и
        <br>
        при скачивании в PDF
    </span>
</div>

<?php
$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>