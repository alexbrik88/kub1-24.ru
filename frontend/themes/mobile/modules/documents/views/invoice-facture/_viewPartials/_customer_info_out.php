<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\InvoiceFacturePaymentDocument;
use common\models\document\UpdPaymentDocument;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\assets\TooltipAsset;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\InvoiceFacture;
use yii\widgets\Pjax;
use \frontend\themes\mobile\helpers\Icon;

/** @var \common\models\document\InvoiceFacture $model */
/* @var $message Message */
/** @var string $dateFormatted */

$paymentDocumentsArray = $model->paymentDocuments;
$paymentDocuments = [];
if ($paymentDocumentsArray) {
    foreach ($paymentDocumentsArray as $doc) {
        $date = date('d.m.Y', strtotime($doc->payment_document_date));
        $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
    }
} else {
    $paymentDocumentsArray[] = new InvoiceFacturePaymentDocument;
}

TooltipAsset::register($this);
$tooltipJs = '$("#document_date_item").tooltipster({"theme":["tooltipster-noir","tooltipster-noir-customized"],"trigger":"hover","side":"bottom"})';
if (Yii::$app->session->remove('show_document_date_tooltip')) {
    $tooltipJs .= '.tooltipster("open")';
    $tooltipJs .= "\n";
    $tooltipJs .= '$(document).on("click", function() {$("#document_date_item").tooltipster("close");});';
}
$tooltipJs .= ';';
$this->registerJs($tooltipJs);

$hasService = $model->getOrders()->joinWith('product product')
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])->exists();

$consignorList = ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($model->getConsignors()->all(), 'id', 'shortName'));
$model->consignor_id .= '';
$model->consignee_id .= '';

$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}
?>

    <div class="wrap">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="column">
                <div class="row row_indents_s flex-nowrap align-items-center">
                    <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Исходящая СФ
                        </span>
                    </span>
                        <span>№</span>
                    </div>
                    <!-- document_number -->
                    <div class="form-group d-inline-block mb-0 col-xl-2">
                        <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                    <!-- document_additional_number -->
                    <div class="form-group d-inline-block mb-0 col-xl-2">
                        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                            <?= \yii\bootstrap4\Html::activeTextInput($model, 'document_additional_number', [
                                'maxlength' => true,
                                'id' => 'account-number',
                                'data-required' => 1,
                                'class' => 'form-control',
                                'placeholder' => 'доп. номер',
                                'style' => 'display: inline;',
                            ]); ?>
                        <?php endif ?>
                    </div>
                    <div class="form-txt d-inline-block mb-0 column">от</div>
                    <!-- document_date -->
                    <div class="form-group d-inline-block mb-0">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'document_date', [
                                'id' => 'under-date',
                                'class' => 'form-control form-control_small date-picker invoice_document_date',
                                'size' => 16,
                                'data-date' => '12-02-2012',
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->document_date,
                                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <span style="font-size:13px">По законодательству РФ, дата счет-фактуры не должна превышать 5 дней от даты акта или товарной накладной.</span>
    </div>

    <div class="wrap">
        <div class="row d-block">

            <div class="form-group col-12">
                <label class="label">К платежно-расчётному документу</label>
                <?php if ($paymentDocumentsArray) : ?>
                    <?php /** @var $doc UpdPaymentDocument */ ?>
                    <div id="payment-inputs-block">
                        <?php foreach ($paymentDocumentsArray as $doc) : ?>
                            <div class="row payment-row mb-2">
                                <div class="col-3">
                                    <?= \yii\bootstrap4\Html::textInput('payment[number][]', $doc->payment_document_number, [
                                        'class' => 'form-control',
                                        'placeholder' => 'номер',
                                    ]) ?>
                                </div>
                                <div class="col-3">
                                    <div class="date-picker-wrap">
                                        <?= Html::textInput('payment[date][]',
                                            $doc->payment_document_date ? date('d.m.Y', strtotime($doc->payment_document_date)) : '', [
                                                'class' => 'form-control date-picker',
                                                //'placeholder' => 'дата',
                                            ]) ?>
                                        <svg class="date-picker-icon svg-icon input-toggle">
                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                                        </svg>
                                    </div>
                                    <span class="close del-payment-doc" style="position: absolute;font-size: 26px;padding: 3px;right: -15px;top:2px;cursor: pointer">×</span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <!-- template -->
                        <div class="row payment-row-template mb-2" style="display: none">
                            <div class="col-3">
                                <?= Html::textInput('payment[number][]', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'номер',
                                ]) ?>
                            </div>
                            <div class="col-3">
                                <div class="date-picker-wrap">
                                    <?= Html::textInput('payment[date][]', null, [
                                        'class' => 'form-control date-picker-field',
                                        //'placeholder' => 'дата',
                                    ]) ?>
                                    <svg class="date-picker-icon svg-icon input-toggle">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                                    </svg>
                                </div>
                                <span class="close del-payment-doc" style="position: absolute;font-size: 26px;padding: 3px;right: -15px;top:2px;cursor: pointer">×</span>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-3">
                        <div id="add-payment-doc" class="btn-group pull-left" style="display: inline-block;">
                        <span class="btn-add-line-table button-regular button-hover-content-red" style="width:40px">
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                            </svg>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            <div class="form-group col-6">
                <label class="label">Гос.контракт, договор (соглашение)</label>
                <?= Html::activeTextInput($model, 'state_contract', [
                    'maxlength' => true,
                    'class' => 'form-control input-editable-field ',
                    'placeholder' => '',
                ]); ?>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузоотправитель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignor_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $consignorList,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <br>
            <div class="form-group col-12">
                <div class="form-filter">
                    <label class="label" for="client">Покупатель<span class="important">*</span></label>
                    <div class="row">
                        <div class="col-6">
                            <?php echo Select2::widget([
                                'name' => 'contractor_id',
                                'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                                'disabled' => true,
                                'pluginOptions' => [
                                    'width' => '100%'
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузополучатель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignee_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $consignorList,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>

        </div>
    </div>

<?php /*
    <div class="portlet customer-info">
        <div class="portlet-title" style="position: relative;">
            <div class="actions" style="position: absolute; top: 0; right: 0;">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                    <button type="submit" class="btn-save btn darkblue btn-sm" style="color: #FFF;" title="Сохранить">
                        <span class="ico-Save-smart-pls"></span>
                    </button>
                    <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', ['view', 'type' => $model->type, 'id' => $model->id], [
                        'class' => 'btn darkblue btn-sm invoice-facture-reload input-editable-field',
                        'style' => "color: #FFF;",
                        'title' => "Отменить"
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="caption">
                <div style="padding-right: 60px;">
                    <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                    № <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control input-editable-field',
                        'style' => 'max-width: 60px; display:inline-block;',
                        'value' => $model->getDocumentNumber(),
                    ]); ?>
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'class' => 'form-control  input-editable-field',
                        'placeholder' => 'доп. номер',
                        'style' => 'max-width: 110px; display:inline-block;',
                        'value' => $model->document_additional_number
                    ]); ?>

                    от
                    <div class="input-icon input-calendar input-editable-field">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </div>
                <div class="input-editable-field"
                     style="margin: 11px 0 0; clear: both; font-size: 10px; font-weight: normal; line-height: 15px;">
                    По законодательству РФ, дата счет-фактуры не должна превышать 5 дней от даты акта или товарной
                    накладной
                </div>
            </div>
        </div>
        <div class="portlet-body no_mrg_bottom">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <b>К платежно-расчётному документу:</b>
                            </div>
                            <div class="col-sm-6">
                                <div id="payment-inputs-block" class="input-editable-field">
                                    <?php if ($paymentDocumentsArray) : ?>
                                        <?php foreach ($paymentDocumentsArray as $doc) : ?>
                                            <div class="payment-row" style="padding-bottom: 5px;">
                                                <?= Html::textInput('payment[number][]', $doc->payment_document_number, [
                                                    'class' => 'form-control',
                                                    'style' => 'width:120px; display: inline-block;',
                                                ]) ?>
                                                от
                                                <div class="input-icon input-calendar input-editable-field">
                                                    <i class="fa fa-calendar"></i>
                                                    <?= Html::textInput(
                                                        'payment[date][]',
                                                        $doc->payment_document_date ? date('d.m.Y', strtotime($doc->payment_document_date)) : '',
                                                        [
                                                            'class' => 'form-control date-picker',
                                                            'size' => 16,
                                                            'data-date-viewmode' => 'years',
                                                            'style' => 'width:120px; display: inline-block;',
                                                        ]
                                                    ) ?>
                                                </div>
                                                <span class="icon icon-close del-payment-doc"
                                                      style="cursor:pointer;"></span>
                                            </div>
                                        <?php endforeach ?>
                                    <?php else : ?>

                                    <?php endif ?>
                                </div>
                                <div class="input-editable-field">
                                    <div class="portlet pull-left control-panel button-width-table">
                                        <div id="add-payment-doc" class="btn-group pull-left"
                                             style="display: inline-block;">
                                        <span class="btn yellow btn-add-line-table">
                                            <i class="pull-left fa icon fa-plus-circle"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <b><?= $model->getAttributeLabel('state_contract') ?>:</b>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-editable-field">
                                    <?= Html::activeTextInput($model, 'state_contract', [
                                        'maxlength' => true,
                                        'class' => 'form-control',
                                        'value' => $model->state_contract,
                                        'style' => 'width: 100%;',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <span class="customer-characteristic">Грузоотправитель:</span>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-editable-field">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignor_id',
                                        'data' => ['add-modal-contractor' => '[ + Добавить грузоотправителя ]'] + $consignorList,
                                        'options' => [
                                            'value' => $model->consignor_id ?: '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                            </div>
                            <div class="col-sm-6">
                                <span><?= Html::a($model->invoice->contractor_name_short, [
                                        '/contractor/view',
                                        'type' => $model->invoice->contractor->type,
                                        'id' => $model->invoice->contractor->id,
                                    ]); ?>
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <span class="customer-characteristic">Грузополучатель:</span>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-editable-field">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignee_id',
                                        'data' => ['add-modal-contractor' => '[ + Добавить грузополучателя ]'] + $consignorList,
                                        'options' => [
                                            'value' => $model->consignee_id ?: '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="contractor-address-block"
                    data-current="<?= $model->invoice->contractor->id; ?>"
                    style="<?= 'display: ' . ($showAddress ? 'table-row;' : 'none;'); ?>">
                    <td>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 270px; padding-right: 0;">
                                <span class="customer-characteristic"></span>
                            </div>
                            <div class="col-sm-6">
                                <?= Html::activeRadioList($model, 'contractor_address', [
                                    InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL => 'Юр. адрес',
                                    InvoiceFacture::CONTRACTOR_ADDRESS_ACTUAL => 'Физ. адрес',
                                ]); ?>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php if ($hasService) : ?>
                    <tr class="input-editable-field">
                        <td>
                            <div style="margin-top: 10px;">
                                <span class="customer-characteristic">Отобразить данные по услугам в столбцах 3, 4, 5, 6:</span>
                                <span>
                                <?= Html::activeCheckbox($model, 'show_service_units', [
                                    'label' => false,
                                ]); ?>
                            </span>
                            </div>
                            <div style="clear: both; font-size: 10px; font-weight: normal; line-height: 15px;">
                                Обычно данные не должны быть отображены, но если ваш клиент просит, то отобразите.
                            </div>
                        </td>
                    </tr>
                <?php endif ?>
                <tr>
                    <td>
                        <div style="margin-bottom: 5px;">
                            <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                'model' => $model,
                                'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                            ]); ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="tooltip_templates" style="display: none;">
    <span id="tooltip_document_date" style="display: inline-block; text-align: left;">
        По законодательству РФ, дата счет-фактуры
        <br>
        не должна превышать 5 дней от даты акта
        <br>
        или товарной накладной
    </span>
    </div>
*/ ?>

<?php $this->registerJs('
$(document).on("click", "#add-payment-doc", function() {
    var row = $("#payment-inputs-block .payment-row-template").clone();
    $(row).removeClass("payment-row-template").addClass("payment-row");
    $(row).find(".date-picker-field").removeClass("date-picker-field").addClass("date-picker");
    $(row).appendTo("#payment-inputs-block").show();
    refreshDatepicker();
});
$(document).on("click", ".del-payment-doc", function() {
    if ($("#payment-inputs-block .payment-row").length == 1) {
        $(this).closest(".payment-row").find("input[type=text]").val("");
    } else {
        $(this).closest(".payment-row").remove();
    }
    return false;
});
$("#invoicefacture-consignee_id").change(function () {
    var $contractorAddressBlock = $(".contractor-address-block");
    var $contractorID = $(this).val();
    if ($contractorID == "") {
        $contractorID = $contractorAddressBlock.data("current");
    }

    $.post("/contractor/is-different-address", {
            id: $contractorID
       }, function (data) {
            if (data.isDifferent == true) {
                $contractorAddressBlock.show();
            } else {
                $contractorAddressBlock.hide();
            }
            $contractorAddressBlock.find("#invoicefacture-contractor_address input[value=\"0\"]").prop("checked", true).click();
       });
});
') ?>