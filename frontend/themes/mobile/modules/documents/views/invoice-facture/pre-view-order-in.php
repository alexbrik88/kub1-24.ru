<?php
use common\models\product\Product;
use common\components\TextHelper;
use \common\models\company\CompanyType;
use common\components\date\DateHelper;
$precision = $model->invoice->price_precision;
?>
<style>
    thead {
        display: table-header-group;
    }
</style>
<div class="over-x m-size-div container-first-account-table no_min_h pad0" style="min-width: 520px;border:1px solid #4276a4; border-top:none;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3">
            <div class="portlet">
                <style>
                    table.ifacture th {
                        font-weight: normal;
                        font-size:9pt;
                        border: 1px solid #000000;
                        padding: 2px 4px;
                    }
                    table.ifacture {
                        border-spacing: 0;
                        border-collapse: collapse !important;
                    }
                    tbody > tr > td {
                        border-top: none!important;
                    }
                    .ifacture td {
                        border: 1px solid #000000;
                        padding: 2px 2px;
                    }
                    .no-border {
                        border:none!important;
                    }
                    .no-border td {
                        border: none;
                    }
                    table.ifacture .ver-top{vertical-align: top!important; padding:4px!important}
                    table.ifacture .ver-bottom{vertical-align: bottom!important;}
                    table.ifacture .font-size-6{font-size: 8px}
                    table.ifacture .font-size-12{font-size: 12px}

                </style>
                <table class="ifacture">
                    <thead>
                    <tr>
                        <th rowspan="2" class="text-center font-size-10" width="13%">
                            Наименование товара (описание выполненных работ, оказанных
                            услуг), имущественного права
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="">
                            Код вида товара
                        </th>
                        <th colspan="2" class="text-center font-size-10" width="13%">
                            Единица измерения
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="3%">
                            Коли-чество (объем)
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="7%">Цена
                            (тариф) за единицу измерения
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="10%">
                            Стоимость товаров (работ, услуг), имущественных прав без
                            налога -
                            всего
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="7%">В том
                            числе сумма акциза
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="6%">
                            Налоговая ставка
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="9%">Сумма
                            налога, предъявляемая покупателю
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="9%">
                            Стоимость товаров (работ, услуг), имущественных прав с
                            налогом -
                            всего
                        </th>
                        <th colspan="2" class="text-center font-size-10" width="13%">
                            Страна происхождения товара
                        </th>
                        <th rowspan="2" class="text-center font-size-10" width="">
                            Регистрационный номер таможенной декларации
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center font-size-10" width="3%">код</th>
                        <th class="text-center font-size-10" width="">условное
                            обозначение (национальной)
                        </th>
                        <th class="text-center font-size-10" width="6%">цифровой код</th>
                        <th class="text-center font-size-10" width="">краткое
                            наименование
                        </th>
                    </tr>
                    </thead>
                    <tbody class="no-border-buttom">
                    <tr class="td-p-0">
                        <td class="text-center  font-size-10">1</td>
                        <td class="text-center  font-size-10">1a</td>
                        <td class="text-center  font-size-10">2</td>
                        <td class="text-center  font-size-10">2a</td>
                        <td class="text-center  font-size-10">3</td>
                        <td class="text-center  font-size-10">4</td>
                        <td class="text-center  font-size-10">5</td>
                        <td class="text-center  font-size-10">6</td>
                        <td class="text-center  font-size-10">7</td>
                        <td class="text-center  font-size-10">8</td>
                        <td class="text-center  font-size-10">9</td>
                        <td class="text-center  font-size-10">10</td>
                        <td class="text-center  font-size-10">10a</td>
                        <td class="text-center  font-size-10">11</td>
                    </tr>

                    <?php foreach ($model->ownOrders as $ownOrder): ?>
                        <?php
                        $invoiceOrder = $ownOrder->order;
                        $product = $invoiceOrder->product;
                        $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
                        if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                            $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
                        } ?>
                        <tr style="font-size:10px; font-family: 'sans-serif'">
                            <td><?= $invoiceOrder->product_title; ?></td>
                            <td><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
                            <td>
                                <?php
                                $value = $invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE;
                                echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                                ?>
                            </td>
                            <td>
                                <?php
                                $value = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
                                echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                                ?>
                            </td>
                            <td class="text-right">
                                <?= ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']); ?>
                            </td>
                            <td class="text-right">
                                <?php
                                $value = $invoiceOrder->purchase_price_no_vat ?
                                    TextHelper::invoiceMoneyFormat($invoiceOrder->purchase_price_no_vat, $precision) :
                                    Product::DEFAULT_VALUE;
                                echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                                ?>
                            </td>
                            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountNoNds, $precision); ?></td>
                            <td class="text-center">
                                <?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                            </td>
                            <td class="text-center"><?= $invoiceOrder->purchaseTaxRate->name; ?></td>
                            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision); ?></td>
                            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountWithNds, $precision); ?></td>
                            <td><?= $invoiceOrder->country->code == '--'? Product::DEFAULT_VALUE : $invoiceOrder->country->code ; ?></td>
                            <td><?= $invoiceOrder->country->name_short == '--'? Product::DEFAULT_VALUE : $invoiceOrder->country->name_short ; ?></td>
                            <td><?= $invoiceOrder->custom_declaration_number; ?></td>
                        </tr>
                    <?php endforeach; ?>

                    <tr style="font-size:10px; font-family: 'sans-serif'">
                        <td colspan="6" class="font-bold">Всего к оплате</td>
                        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->totalAmountNoNds, 2); ?></td>
                        <td colspan="2" class="text-center">X</td>
                        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
                        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?></td>
                        <td colspan="3" style="border: none"></td>
                    </tr>
                    </tbody>
                </table>

                <table border="0" class="ifacture table no-border" style="margin-top:15px">
                    <tr>
                        <td class="font-size-12" width="21%">
                            Руководитель организации<br>или иное уполномоченное лицо
                        </td>
                        <td width="10%" style="border-bottom: 1px solid black"></td>
                        <td width="1%"></td>
                        <td class="ver-bottom m-l font-size-12 text-center" width="17%"
                            style="border-bottom: 1px solid black"><?=$model->invoice->contractor->director_name?></td>
                        <td class="font-size-12" width="21%">Главный бухгалтер<br>или иное
                            уполномоченное лицо
                        </td>
                        <td width="10%" style="border-bottom: 1px solid black"></td>
                        <td width="1%"></td>
                        <td class="ver-bottom m-l font-size-12 text-center" width="17%" style="border-bottom: 1px solid black">
                            <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                                <?=$model->invoice->contractor->director_name?>
                            <?php else: ?>
                                <?=$model->invoice->contractor->chief_accountant_name?>
                            <?php endif ?>
                        </td>
                        <td width="6%"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
                        <td></td>
                        <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
                        <td></td>
                        <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
                        <td></td>
                        <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
                        <td width="6%"></td>
                    </tr>
                </table>
                <table class="ifacture table no-border" style="">
                    <tr>
                        <td class="ver-bottom m-l font-size-12" width="21%">
                            Индивидуальный предприниматель<br>или иное уполномоченное лицо
                        </td>
                        <td width="10.4%" style="border-bottom: 1px solid black"></td>
                        <td width="1%"></td>
                        <td width="17.5%" class="ver-bottom m-l font-size-10"
                            style="border-bottom: 1px solid black"><?php if ($model->invoice->company->company_type_id == CompanyType::TYPE_IP): ?>
                                <?= $model->invoice->getCompanyChiefFio(true); ?>
                            <?php endif; ?></td>
                        <td width="1%"></td>
                        <td class="ver-bottom m-l font-size-12 text-center" style="border-bottom: 1px solid black">
                            <?= $model->company->company_type_id == CompanyType::TYPE_IP ? $model->company->getCertificate(false) : null; ?>
                        </td>
                        <td width="6%"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
                        <td width="1%"></td>
                        <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
                        <td width="9%"></td>
                        <td width="40%" class="text-center font-size-6 ver-top m-l">(реквизиты
                            свидетельства
                            о государственной,<br/>
                            регистрации индивидуального предпринимателя)
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>