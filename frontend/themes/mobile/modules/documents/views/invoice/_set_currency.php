<?php

use common\models\currency\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('span', $model->currency_name, [
    'class' => 'currency_name',
    'data' => [
        'initial' => $model->currency_name,
    ],
]);
$currencyAmount = Html::tag('span', $model->currency_amount, [
    'class' => 'currency_rate_amount',
    'data' => [
        'initial' => $model->currency_amount,
    ],
]);
$currencyRate = Html::tag('span', $model->currency_rate, [
    'class' => 'currency_rate_value',
    'data' => [
        'initial' => $model->currency_rate,
    ],
]);

$radioName = Html::getInputName($model, 'currency_rate_type');

?>

<div class="col-6 col-xl mb-3 currency-view-toggle collapse <?= $collapseState ?>" id="currency-rate-box">

    <?= Html::beginTag('div', [
        'id' => Html::getInputId($model, 'currency_rate_type'),
        'class' => '',
        'data' => [
            'geturl' => Url::to(['/site/currency-rate']),
            'seturl' => Url::to(['/documents/invoice/set-currency-rate', 'type' => $model->type, 'id' => $model->id]),
        ],
    ]) ?>

    <?php /*<?= Html::hiddenInput($radioName, '') ?>*/ ?>

    <div class="mb-2" style="display: block">Установить обменный курс</div>
    <div class="mr-3">
        <?= Html::activeHiddenInput($model, 'currency_rate_amount', [
            'class' => 'currency_rate_amount',
            'data' => [
                'initial' => $model->currency_rate_amount,
            ],
        ]) ?>
        <?= Html::activeHiddenInput($model, 'currency_rate_value', [
            'class' => 'currency_rate_value',
            'data' => [
                'initial' => $model->currency_rate_value,
            ],
        ]) ?>
        <label class="radio-label" for="radio_currency_rate_ondate">
            <input id="radio_currency_rate_ondate"
                   class="radio-input input-hidden currency-rate-type-radio"
                   type="radio"
                   name="<?=($radioName)?>"
                   value="<?=(Currency::RATE_SERTAIN_DATE)?>"
                <?=($model->currency_rate_type == Currency::RATE_SERTAIN_DATE) ? ' checked="" ' : '' ?>
                   data-initial="<?=($model->currency_rate_type == Currency::RATE_SERTAIN_DATE)?>">
            <span class="radio-txt">
                <a class="link link_bold" href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a>
                <strong>на</strong>
                <span class="date-current date-picker-wrap">
                    <?= Html::activeTextInput($model, 'currency_rateDate', [
                        'class' => 'invoice-currency_ratedate currency_rate_date date-current date-picker form-control',
                        'data' => [
                            'default' => $model->document_date,
                            'initial' => $model->currency_rateDate,
                        ],
                        'readonly' => $model->currency_rate_type != Currency::RATE_SERTAIN_DATE,
                    ]) ?>
                <svg class="date-picker-icon svg-icon input-toggle">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                </svg>
                </span>
                <?= Html::tag('span', $model->currency_rate_amount, [
                    'class' => 'currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_rate_amount,
                    ],
                ]); ?>
                <?= $currencyName ?> =
                <?= Html::tag('span', $model->currency_rate_value, [
                    'class' => 'currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate_value,
                    ],
                ]) ?>
                RUB
            </span>
        </label>
    </div>
    <div class="mr-3">
        <label class="radio-label" for="radio_currency_rate_onpayment">
            <input id="radio_currency_rate_onpayment"
                   class="radio-input input-hidden currency-rate-type-radio default-type"
                   type="radio"
                   name="<?=($radioName)?>"
                   value="<?=(Currency::RATE_PAYMENT_DATE)?>"
                <?=($model->currency_rate_type == Currency::RATE_PAYMENT_DATE) ? ' checked="" ' : '' ?>
                   data-initial="<?=($model->currency_rate_type == Currency::RATE_PAYMENT_DATE)?>">
            <span class="radio-txt">
                    <a class="link link_bold" href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a>
                    <strong>на день оплаты</strong>
                    <br>
                    <span class="small">(До оплаты счет учитывается по курсу на <?= Html::tag('span', $model->currency_rateDate, [
                            'class' => 'currency_rate_date',
                            'data' => [
                                'initial' => $model->currency_rateDate,
                            ],
                        ]) ?>, <br> при оплате пересчитывается)</span>
                </span>
        </label>
    </div>
    <div class="mr-3">
        <label class="radio-label" for="radio_currency_rate_custom">
            <input id="radio_currency_rate_custom"
                   class="radio-input input-hidden currency-rate-type-radio"
                   type="radio"
                   name="<?=($radioName)?>"
                   value="<?=(Currency::RATE_CUSTOM)?>"
                <?=($model->currency_rate_type == Currency::RATE_CUSTOM) ? ' checked="" ' : '' ?>
                   data-initial="<?=($model->currency_rate_type == Currency::RATE_CUSTOM)?>">
            <span class="radio-txt">
                    <span class="d-block mb-2 mt-1">Установить другой обменный курс:</span>

                    <?= Html::activeTextInput($model, 'currencyAmountCustom', [
                        'value' => $model->currency_amount,
                        'class' => 'form-control currency-custom-input currency_rate_amount',
                        'style' => 'width: 80px; height: 40px;',
                        'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                        'data' => [
                            'initial' => $model->currency_amount,
                        ],
                    ]) ?>
                    <?= $currencyName ?>
                    =
                    <?= Html::activeTextInput($model, 'currencyRateCustom', [
                        'value' => $model->currency_rate,
                        'class' => 'form-control currency-custom-input currency_rate_value',
                        'style' => 'width: 120px; height: 40px;',
                        'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                        'data' => [
                            'initial' => $model->currency_rate,
                        ],
                    ]) ?>
                    RUB
                </span>
        </label>
    </div>

    <?= Html::endTag('div') ?>

</div>
<?php /*
    <?= Html::hiddenInput($radioName, '') ?>

    <?= Html::beginTag('div', [
        'id' => Html::getInputId($model, 'currency_rate_type'),
        'class' => 'row',
        'data' => [
            'geturl' => Url::to(['/site/currency-rate']),
            'seturl' => Url::to(['/documents/invoice/set-currency-rate', 'type' => $model->type, 'id' => $model->id]),
        ],
    ]) ?>

    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_SERTAIN_DATE, [
                'id' => 'radio_currency_rate_ondate',
                'value' => Currency::RATE_SERTAIN_DATE,
                'class' => 'currency-rate-type-radio',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_SERTAIN_DATE,
                ],
            ])?>
            <div class="label-part" style="max-width: 200px;">
                <?= Html::activeHiddenInput($model, 'currency_rate_amount', [
                    'class' => 'currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_rate_amount,
                    ],
                ]) ?>
                <?= Html::activeHiddenInput($model, 'currency_rate_value', [
                    'class' => 'currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate_value,
                    ],
                ]) ?>
                <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на
                <?= Html::activeTextInput($model, 'currency_rateDate', [
                    'class' => 'invoice-currency_ratedate currency_rate_date input-underline',
                    'data' => [
                        'default' => $model->document_date,
                        'initial' => $model->currency_rateDate,
                    ],
                    'readonly' => true,
                ]) ?>:
            </div>
            <div class="label-part" style="max-width: 180px;">
                <?= Html::tag('span', $model->currency_rate_amount, [
                    'class' => 'currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_rate_amount,
                    ],
                ]); ?>
                <?= $currencyName ?> =
                <?= Html::tag('span', $model->currency_rate_value, [
                    'class' => 'currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate_value,
                    ],
                ]) ?>
                RUB
            </div>
        </label>
    </div>
    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_PAYMENT_DATE, [
                'id' => 'radio_currency_rate_onpayment',
                'value' => Currency::RATE_PAYMENT_DATE,
                'class' => 'currency-rate-type-radio default-type',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_PAYMENT_DATE,
                ],
            ])?>
            <div class="label-part">
                <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на день оплаты.
            </div>
            <div class="label-part" style="font-size: 12px;">
                (До оплаты счет учитывается по курсу на <?= Html::tag('span', $model->currency_rateDate, [
                    'class' => 'currency_rate_date',
                    'data' => [
                        'initial' => $model->currency_rateDate,
                    ],
                ]) ?>, при оплате пересчитывается).
            </div>
        </label>
    </div>
    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_CUSTOM, [
                'id' => 'radio_currency_rate_custom',
                'value' => Currency::RATE_CUSTOM,
                'class' => 'currency-rate-type-radio',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_CUSTOM,
                ],
            ])?>
            <div class="label-part" style="max-width: 250px;">
                Установить другой обменный курс:
            </div>
            <div class="label-part" style="max-width: 225px;">
                <?= Html::activeTextInput($model, 'currencyAmountCustom', [
                    'value' => $model->currency_amount,
                    'class' => 'currency-custom-input currency_rate_amount',
                    'style' => 'width: 50px;',
                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                    'data' => [
                        'initial' => $model->currency_amount,
                    ],
                ]) ?>
                <?= $currencyName ?>
                =
                <?= Html::activeTextInput($model, 'currencyRateCustom', [
                    'value' => $model->currency_rate,
                    'class' => 'currency-custom-input currency_rate_value',
                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                    'data' => [
                        'initial' => $model->currency_rate,
                    ],
                ]) ?>
                RUB
            </div>
        </label>
    </div>

<?= Html::endTag('div') ?>
*/ ?>