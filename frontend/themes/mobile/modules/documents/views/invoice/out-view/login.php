<?php

use frontend\models\LoginForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $invoice common\models\document\Invoice */
/* @var $model frontend\modules\documents\forms\InvoiceOutViewForm */
/* @var $userExists boolean */
/* @var $email string */
?>

<?php Pjax::begin([
    'id' => 'out-view-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?= \frontend\widgets\Alert::widget(); ?>

<div class="row"">
    <div class="col-sm-12 form-header border-bottom-e4">
        <?php if ($userExists) : ?>
            Для
            <span class="text-bold"><?= $email ?></span>,
            уже есть личный кабинет в сервисе КУБ.
            Для сохранения счёта в этом личном кабинете,
            введите пароль и нажмите кнопку «Войти».
        <?php else : ?>
            Для входа в личный кабинет, введите e-mail, пароль и нажмите кнопку «Войти».
        <?php endif ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'action' => [
                'out-view-login',
                'uid' => $invoice->uid,
                'email' => $email,
            ],
            'options' => [
                'data-pjax' => '',
            ],
        ]); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Войти', [
                'class' => 'button-regular button-hover-transparent',
                'name' => 'login-button',
                'style' => 'width: 115px; margin-right: 20px;'
            ]) ?>
            <?= Html::a('Забыли пароль?', [
                'out-view-reset',
                'uid' => $invoice->uid,
                'email' => $email,
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div>
            Еще не зарегистрированы?
        </div>
        <?= Html::a('Регистрация', [
            'out-view-signup',
            'uid' => $invoice->uid,
            'email' => $email,
        ]) ?>
    </div>
</div>

<?php Pjax::end(); ?>
