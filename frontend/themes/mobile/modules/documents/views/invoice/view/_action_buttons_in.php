<?php

use common\models\document\status\InvoiceStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\Documents;

/* @var $this View */
/* @var $model \common\models\document\Invoice */
/* @var $useContractor boolean */
/* @var $showSendPopup integer */

$contractorId = $useContractor ? $model->contractor_id : null;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);

$candelete = !$model->is_deleted &&
    in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed) &&
    Yii::$app->user->can(permissions\document\Document::DELETE, [
        'model' => $model,
    ]);

?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= Html::a('Перевыставить', ['create', 'type' => Documents::IO_TYPE_OUT, 'from' => $model->id], [
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                'title' => 'Выставить исходящий счет',
                'style' => 'padding-left: 15px; padding-right: 15px;'
            ]) ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php $printUrl = [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $model->type,
                'filename' => $model->getPrintTitle(),
            ]; ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', $printUrl, [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100 no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE)): ?>
                <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($model->type)) : ?>
                    <?= ConfirmModalWidget::widget([
                        'size' => yii\bootstrap4\Modal::SIZE_SMALL,
                        'toggleButton' => [
                            'label' => $this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>',
                            'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                        ],
                        'confirmUrl' => Url::to([
                            'create',
                            'type' => $model->type,
                            'clone' => $model->id,
                            'contractorId' => $contractorId,
                        ]),
                        'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                    ]);
                    ?>
                <?php else : ?>
                    <button class="button-clr button-regular button-width button-hover-transparent w-100 action-is-limited" type="button">
                        <?= $this->render('//svg-sprite', ['ico' => 'copied']) ?>
                        <span>Копировать</span>
                    </button>
                <?php endif ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($model->isFullyPaid) : ?>
                <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)): ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Снять оплату',
                            'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                        ],
                        'confirmUrl' => Url::to([
                            'unpaid',
                            'type' => $model->type,
                            'id' => $model->id,
                            'contractorId' => $contractorId,
                        ]),
                        'message' => 'Вы уверены, что нужно снять оплату со счета?',
                    ]);
                    ?>
                <?php endif; ?>
            <?php else : ?>
                <?php if (!$model->is_deleted
                    && $model->invoiceStatus->paymentAllowed($model->type)
                    && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
                ): ?>
                    <?= Html::button($this->render('//svg-sprite', ['ico' => 'check-2']).'<span>Оплачен</span>', [
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                        'data-toggle' => 'modal',
                        'data-target' => '#paid',
                    ]) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($model->invoice_status_id !== InvoiceStatus::STATUS_PAYED): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'payment-order',
                    ],
                    'toggleButton' => false,
                    'toggleButton' => [
                        'label' => '<span>Платежка</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::toRoute(['create-payment-order', 'type' => $model->type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите подготовить платежку в банк?",
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($candelete): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'type' => $model->type,
                        'id' => $model->id,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить этот счёт?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>