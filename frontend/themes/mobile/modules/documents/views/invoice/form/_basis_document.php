<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\Agreement;
use common\components\date\DateHelper;
use \frontend\themes\mobile\helpers\Icon;

$agreementDropDownList = [];
$companyAgreementID = [];
$outnds = 0;
if (isset ($agreementBasis) && $agreementBasis instanceof Agreement) {
    if ($agreementBasis->contractor) {
        $doc_date = DateHelper::format($agreementBasis->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $agreementDropDownList = [
            "Договор&{$agreementBasis->getFullNumber()}&{$agreementBasis->document_date}&1&{$agreementBasis->id}"
            => 'Договор № ' . $agreementBasis->getFullNumber() . ' от ' . $doc_date];

        if ($agreementBasis->contractor->isOutInvoiceHasNds) {
            $outnds = 1;
        }
    }

} else {
    if ($model->contractor) {
        $agreementArray = $model->contractor->getAgreements()->joinWith('agreementType')->orderBy([
            'agreement_type.name' => SORT_ASC,
            'agreement.document_date' => SORT_DESC,
        ])->all();
        //$agreementDropDownList += ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');
        $agreementDropDownList += ['' => '   '] + ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '];
        //$agreementDropDownList[$model->agreement] = $model->getBasisDocString();

        /** @var $agreement \common\models\Agreement */
        foreach ($agreementArray as $agreement) {
            $agreement_date = DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $key = "{$agreement->agreementType->name}&{$agreement->getFullNumber()}&{$agreement->document_date}&{$agreement->agreementType->id}&{$agreement->id}";
            $value = "{$agreement->agreementType->name} № {$agreement->getFullNumber()} от {$agreement_date}";
            $agreementDropDownList[$key] = $value;
            $companyAgreementID[$key] = $agreement->id;
        }
        if ($model->contractor->isOutInvoiceHasNds) {
            $outnds = 1;
        }
    }

}

Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['/documents/invoice/basis-document']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'invoice-agreement',
    'model' => $model,
    'attribute' => 'agreement',
    'data' => $agreementDropDownList,
    'hideSearch' => true,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'templateResult' => new \yii\web\JsExpression('formatAgreementResult'),
        'language' => [
            'noResults' =>  new \yii\web\JsExpression('function(){
                return $("<span />").attr("style", "color:red").html("Сначала выберите покупателя");
            }'),
        ],
        'escapeMarkup' => new JsExpression('function(text) {return text;}')
    ],
    'options' => [
        'disabled' => !$agreementDropDownList,
        'data' => [
            'delay' => !empty($delay) ? $delay : 10,
            'refresh' => empty($refresh) ? 'false' : 'true',
            'invoice_count' => empty($invoiceCount) ? 0 : $invoiceCount,
            'invoice_sum' => empty($invoiceSum) ? 0 : $invoiceSum,
            'expenditure' => $model->contractor ? $model->contractor->invoice_expenditure_item_id : '',
            'outnds' => $outnds,
            'contacts' => $model->contractor ? $model->contractor->getAttributes([
                'director_name',
                'director_email',
                'chief_accountant_is_director',
                'chief_accountant_name',
                'chief_accountant_email',
                'contact_is_director',
                'contact_name',
                'contact_email',
            ]) : null,
        ]
    ]
]);

$this->registerJs('

    $companyAgreementID = ' . json_encode($companyAgreementID) . ';
    function formatAgreementResult(data, container) {
        if (data.id && data.id !== "add-modal-agreement" && data.disabled !== true) {
            var input = $("#invoice-agreement");
            var content = \'<div class="agreement-item-name-label">\';

            content += \'<i class="pull-right link" title="Редактировать"><svg class="edit-agreement-item svg-icon" data-id="\' + $companyAgreementID[data.id] + \'" ><use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use></svg></i>\';

            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            container.innerHTML = content;
        } else if (data.id) {
            var input = $("#invoice-agreement");
            $(container).addClass("ajax-modal-btn").attr("title", "Добавить договор").attr("data-url", input.data("create-url"));
            container.innerHTML = data.text;
        } else {
            container.innerHTML = data.text;
        }

        return container;
    };
', \yii\web\View::POS_HEAD);

Pjax::end();

$this->registerJs('

    if ($("#agreement-pjax-container .select2-container--disabled").length) {
        $("#agreement-pjax-container .select2-container--disabled").attr("data-tooltip-content", "#agreement-basis-empty").tooltipster({
            theme: ["tooltipster-kub"],
            trigger: "hover",
            side: "top",
        });
    }
'); ?>

<div style="display:none">
    <div id="agreement-basis-empty">
        Укажите покупателя, чтобы добавить основание
    </div>
</div>
