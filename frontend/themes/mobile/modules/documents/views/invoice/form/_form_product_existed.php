<?php
use common\models\document\Invoice;
use yii\widgets\Pjax;

/* @var $model Invoice */

$class = isset($contentCssClass) ? $contentCssClass : 'to-invoice-content';
?>

<div class="modal mobile-modal" id="add-from-exists">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content <?= $class ?>">

            <?php Pjax::begin([
                'id' => 'pjax-product-grid',
                'linkSelector' => false,
                'formSelector' => '#products_in_order',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
