<?php $this->registerJs(<<<JS
    $('[id="new_contractor_inn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,
        hint: 'Выберите вариант из списка ниже',

        onSelect: function(suggestion) {

            $(this).val(suggestion.data.inn);

            if (!$(this).hasClass('done')) {
                var form = {
                    documentType: INVOICE.documentIoType,
                    Product: {
                        production_type: 1,
                        flag: 1,
                    },
                    inn: suggestion.data.inn
                };
                INVOICE.addNewContractor('add-modal-contractor-fast', form);
            }

        }
    });
JS
);