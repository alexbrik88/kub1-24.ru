<?php
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $model common\models\product\Product */
/* @var $textInputConfig Array */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */

$storeId = $model->productStoreByStore ? $model->productStoreByStore->store_id : null;
?>

<?= $form->field($model, "article", $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, "initQuantity[$storeId]", $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'code', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'box_type', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'count_in_place', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'place_count', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'mass_gross', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
    'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
]); ?>

<?= $form->field($model, 'country_origin_id', $textInputConfig)
    ->widget(Select2::class, [
        'data' => ArrayHelper::map(Country::getCountries(), 'id', 'name_short'),
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'width' => '100%',
            'minimumResultsForSearch' => -1
        ]
    ]); ?>

<?= $form->field($model, 'customs_declaration_number', $textInputConfig)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?php
$massGrossId = \yii\helpers\Html::getInputId($model, 'mass_gross');
$productUnitId = \yii\helpers\Html::getInputId($model, 'product_unit_id');
$countableUnits = json_encode(array_keys(ProductUnit::$countableUnits));
$js = <<<JS
    var productMassGross = $('#$massGrossId');
    var countableUnits = $countableUnits;
    $('#$productUnitId').on('change init', '', function (e) {
        productMassGross.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
    }).trigger('init');

JS;
$this->registerJs($js);
?>
