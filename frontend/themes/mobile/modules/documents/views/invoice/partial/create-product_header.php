<?php if (!Yii::$app->request->post('isB2B')): ?>
    <?php
    $document = Yii::$app->request->getBodyParam('document');
    if ($document == 'act') {
        echo 'Добавить услугу';
    } elseif ($document == 'packing-list') {
        echo 'Добавить товар';
    } else {
        echo 'Добавить товар/услугу';
    }
    ?>
<?php else: ?>
    <?php if ($productionType == 1): ?>
        Добавить товар
    <?php else: ?>
        Добавить услугу
    <?php endif; ?>
<?php endif; ?>