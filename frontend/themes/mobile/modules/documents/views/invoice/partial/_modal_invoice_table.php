<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="invoice-list">
    <?= Html::beginForm(['/documents/invoice/get-invoices'], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>
    <div class="table-settings row row_indents_s mb-3">
        <div class="col-12">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'document_number', [
                        'type' => 'search',
                        'id' => 'invoice-number-search',
                        'placeholder' => '№ счёта...',
                        'class' => 'form-control',
                        'style' => 'margin-bottom: 0;'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                        'style' => 'margin-bottom: 0;'
                    ]) ?>
                </div>
            </div>
        </div>
        <?php /*
        <div id="range-ajax-button">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'ajax m-r-10 m-t-10',]); ?>
        </div>*/ ?>
    </div>

    <div class="accounts-list">
        <div id="in_invoice_table" class="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'attribute' => 'invoice_id',
                        'label' => '',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'padding: 7px 5px 5px 5px;',
                        ],
                        'value' => function ($data) {
                            /** @var Invoice $data */
                            return Html::checkbox(Html::getInputName($data, 'id') . '[]', false, [
                                'value' => $data->id,
                                'class' => 'checkbox-invoice-id',
                            ]);
                        },
                        'headerOptions' => [
                            'width' => '4%',
                        ],
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '24%',
                        ],
                        'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '24%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            /** @var Invoice $data */
                            return Html::a($data->fullNumber, ['invoice/view', 'type' => $data->type, 'id' => $data->id]);
                        },
                    ],
                    [
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '24%',
                        ],
                        'attribute' => 'total_amount_with_nds',
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'width' => '24%',
                        ],
                        'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => 'contractor_name_short',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <button id="add-invoice-to-payment-order" class="btn-add button-regular button-width button-regular_red button-clr" data-dismiss="modal">Добавить</button>
    <button type="button" class="button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>