<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\rbac\permissions\document\Document;
use frontend\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

//$addStamp = 1;

TooltipAsset::register($this);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'side' => 'right',
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$company = $model->company;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$signatureLink = '';
$accountantSignatureLink = '';
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$model->employeeSignature) ? null :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}

$printLink = EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$isAuto = $model->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE;
$totalSum = $model->view_total_with_nds;

/* add logo */
$images['logo'] = ['tab' => 0, 'link' => $logoLink, 'name' => '+ Добавить логотип'];
$images['print'] = ['tab' => 1, 'link' => $printLink, 'name' => '+ Добавить печать'];
$images['chief'] = ['tab' => 2, 'link' => $signatureLink, 'name' => '+ Добавить подпись'];
?>

<!-- Modal: add logo -->
<?php if ($images) {
    $this->registerJs('
        $(document).on("click", ".companyImages-button", function(event) {
            var tabId = parseInt($(this).data("tab"));
            $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
        });

        $(document).on("hidden.bs.modal", "#modal-companyImages", function() {
            location.reload();
        });
    ');
}
?>

<?php if ($images) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'title' => 'Загрузить логотип, печать и подпись',
        'titleOptions' => [
            'class' => 'mb-0',
        ],
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>
<!-- Modal: add logo. end -->

<style type="text/css">
    <?php
    if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
        //echo $this->renderFile($file);

    }
    if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
        //echo $this->renderFile($file);
    }
    ?>

    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }

    .pre-view-table h3 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }

    .auto_tpl {
        font-style: italic;
        font-weight: normal;
    }

    .m-size-div div {
        font-size: 12px;
    }

    .m-size-div div span {
        font-size: 9px;
    }

    .file_upload_block {
        text-align: center;
        border: 2px dashed #ffb848;
        color: #ffb848;
        padding: 12px;
    }

    .no_min_h {
        min-height: 0px !important;
    }
</style>

<div class="pre-view-box" style="min-width: 520px; max-width:735px; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table m-size-div">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-8 pad0 font-bold" style="height: inherit">
                <?= $this->render('/invoice/view/_actions', [
                    'model' => $model,
                    'ioType' => $ioType,
                    'useContractor' => $useContractor,
                ]) ?>
                <div style="padding-top: 12px !important;">
                    <p style="font-size: 17px">
                        <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                            $model->company_name_full :
                            $model->company_name_short; ?>
                    </p>
                </div>
            </div>

            <div class="col-xs-4 pad3 v_middle" style="height: inherit; text-align: right;">
                <div>
                    <?php if (isset($images['logo']['link'])) { ?>
                        <img style="max-height: 70px;max-width: 170px;" src="<?= $logoLink ?>" alt="">
                    <?php } else { ?>
                        <div style="width: auto !important;">
                            <div class="file_upload_block companyImages-button"
                                 data-toggle='modal'
                                 data-target='#modal-companyImages'
                                 data-tab='<?= $images['logo']['tab'] ?>'>
                                <?= $images['logo']['name'] ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="col-xs-12 pad3" style="padding-top: 10px !important;">
            <div class="col-xs-12 pad0 bord-dark"
                 style="height: 60px; border-bottom: 0px">
                <div class="col-xs-6 pad0">
                    <div class="col-xs-12 pad1" style="height: 40px;">
                        <div class="col-xs-12 pad1 v_middle" style="height: inherit">
                            <?= $model->company_bank_name; ?>
                        </div>
                    </div>
                    <div class="col-xs-12 pad1"> Банк получателя</div>
                </div>
                <div class="col-xs-2 pad0 bord-dark-r bord-dark-l"
                     style="height: inherit;">
                    <div class="col-xs-12 pad1"> БИК</div>
                    <div class="col-xs-12 pad1 bord-dark-t"
                         style="padding-top: 5px"> Сч. №
                    </div>
                </div>
                <div class="col-xs-4 pad0">
                    <div
                            class="col-xs-12 pad1">  <?= $model->company_bik; ?> </div>
                    <div class="col-xs-12 pad1"
                         style="padding-top: 5px">  <?= $model->company_ks; ?> </div>
                </div>
            </div>

            <div class="col-xs-12 pad0 bord-dark" style="height: 82px;">
                <div class="col-xs-6 pad0 full_h">
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-6 pad1 bord-dark-r">
                            ИНН <?= $model->company_inn; ?> </div>
                        <div class="col-xs-6 pad1">
                            КПП <?= $model->company_kpp; ?> </div>
                    </div>

                    <div class="col-xs-12 pad0 bord-dark-t">
                        <div class="col-xs-12 pad1" style="height: 41px">
                            <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                                $model->company_name_full :
                                $model->company_name_short; ?>
                        </div>
                        <div class="col-xs-12 pad1" style="padding-top: 5px">
                            Получатель
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 pad0 bord-dark-r bord-dark-l full_h">
                    Сч. №
                </div>
                <div class="col-xs-4 pad0 full_h">
                    <div
                            class="col-xs-12 pad1">  <?= $model->company_rs; ?> </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 pad0"
             style="padding-top: 5px !important; border-bottom: 2px solid #000000">
            <h3 style="marg"> <?= $model->is_invoice_contract ? 'Счет-договор' : 'Счет'; ?>
                № <?= $isAuto ? '<i class="auto_tpl">##</i>' : $model->fullNumber; ?>
                от <?= $isAuto ? $model->auto->day . ' <i class="auto_tpl">месяц</i> 20<i class="auto_tpl">##</i> г.' : $dateFormatted; ?>
            </h3>
        </div>

        <div class="col-xs-12 pad3" style="padding-top: 15px !important;">
            <div class="col-xs-12 pad0">
                <div class="col-xs-2 pad0"> Поставщик <br/> (Исполнитель):</div>
                <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                    <?= $model->getExecutorFullRequisites(); ?>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-top: 5px !important;">
                <div class="col-xs-2 pad0"> Покупатель<br/>(Заказчик):</div>
                <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                    <?= $model->getCustomerFullRequisites(); ?>
                </div>
            </div>

            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                <div class="col-xs-12 pad0"
                     style="padding-top: 5px !important;">
                    <div class="col-xs-2 pad0"> Основание:</div>
                    <div class="col-xs-10 pad3" style="font-weight: bold;">
                        <?= $model->basis_document_name ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>

        <div class="col-xs-12 pad3">
            <?= $this->render('view/_pre_view_orders_table', [
                'model' => $model,
                'ioType' => $ioType,
                'isAuto' => $isAuto,
            ]); ?>
        </div>

        <div class="col-xs-12 pad3">
            <div class="col-xs-12 pad0 m-t-n" style="border-bottom: 2px solid #000000;">
                <div class="col-xs-12 pad3">
                    <div class="txt-9">
                        Всего наименований <?= count($model->orders) ?>, на сумму
                        <?= TextHelper::invoiceMoneyFormat($totalSum, 2); ?>
                        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
                    </div>
                    <div class="txt-9-b text-bold">
                        <?= TextHelper::mb_ucfirst(Currency::textPrice($totalSum / 100, $model->currency_name)); ?>
                    </div>
                    <?php if ($model->has_weight): ?>
                        <div class="txt-9">
                            Общий вес:
                            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalWeight($model), $model->weightPrecision) ?> кг
                        </div>
                    <?php endif; ?>
                    <?php if ($model->has_volume): ?>
                        <div class="txt-9">
                            Общий объем:
                            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalVolume($model), $model->volumePrecision) ?> м2
                        </div>
                    <?php endif; ?>
                    <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT): ?>
                        <?php if ($model->contract_essence): ?>
                            <div class="txt-9-b text-bold" style="padding-top: 10px;">
                                Предмет договора:
                            </div>
                            <div class="txt-9">
                                <?= nl2br(Html::encode($model->contract_essence)) ?>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if ($model->comment) : ?>
                            <div class="txt-9" style="padding-top: 10px;">
                                <?= nl2br(Html::encode($model->comment)) ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT && $invoiceContractorSignature->is_active): ?>
                <div class="col-xs-12 pad0 text-center" style="padding-top: 20px!important;">
                        <span style="font-size: 17px;font-weight: 600;">
                            Адреса и реквизиты сторон:
                        </span>
                </div>
                <div class="col-xs-12 pad0" style="padding-top: 10px!important;margin-bottom: 20px;">
                    <div class="col-xs-6" style="padding-left: 0!important;">
                        <div style="font-weight: 600;font-size: 14px;">Исполнитель</div>
                        <div class="company-name-short">
                            <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                                $model->company_name_full :
                                $model->company_name_short; ?>
                        </div>
                        <div>
                            <b>ИНН</b>
                            <?= $model->company_inn; ?>
                        </div>
                        <div>
                            <b>КПП</b>
                            <?= $model->company_kpp; ?>
                        </div>
                        <div>
                            <b><?= $model->company->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'; ?></b>
                            <?= $model->company->company_type_id == CompanyType::TYPE_IP ?
                                $model->company_egrip : $model->company->ogrn; ?>
                        </div>
                        <div class="company-address-legal">
                            <b>Юридический адрес</b>
                            <?= $model->company_address_legal_full; ?>
                        </div>
                        <div>
                            <b>Р/сч</b>
                            <?= $model->company_rs; ?> <br>
                            <?= $model->company_bank_name ? ('в ' . $model->company_bank_name) : '<br>'; ?>
                        </div>
                        <div>
                            <b>Кор.счет</b>
                            <?= $model->company_ks; ?>
                        </div>
                        <div>
                            <b>БИК</b>
                            <?= $model->company_bik; ?>
                        </div>
                        <div style="margin-top: 30px;">
                            <?= $model->company->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель'; ?>
                        </div>

                        <div style="height: 60px;">
                            <div class="col-xs-6 v_bottom" style="padding-left: 3px;padding-right: 3px;z-index: 2;">
                                <div>
                                    <?php if (isset($images['chief']['link'])): ?>
                                        <img style="max-width: 117px" src="<?= $images['chief']['link'] ?>" alt="">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-6 v_bottom text-center"
                                 style="padding-left: 3px;padding-right: 3px;z-index: 2;">
                                <div>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col-xs-12 pad0">
                                <div style="text-align:center;position:absolute;right: 70px;z-index:1;top: -50px;">
                                    <img style="width: 100px;" src="<?= $images['print']['link'] ?>" alt="">
                                </div>
                            </div>
                            <div class="col-xs-6" style="padding-left: 3px;padding-right: 3px;z-index: 2;">
                                <div class="bord-dark-t text-center"><span> (подпись) </span></div>
                            </div>
                            <div class="col-xs-6" style="padding-left: 3px;padding-right: 3px;z-index: 2;">
                                <div class="bord-dark-t text-center"><span> (расшифровка подписи) </span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" style="padding-left: 0!important;">
                        <div style="font-weight: 600;font-size: 14px;">Заказчик</div>
                        <div class="contractor-name-short">
                            <?= $model->contractor_name_short; ?>
                        </div>
                        <div>
                            <b>ИНН</b>
                            <?= $model->contractor_inn; ?>
                        </div>
                        <div>
                            <b>КПП</b>
                            <?= $model->contractor_kpp; ?>
                        </div>
                        <div>
                            <b><?= $model->contractor->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'; ?></b>
                            <?= $model->contractor->BIN; ?>
                        </div>
                        <div class="contractor-address-legal">
                            <b>Юридический адрес</b>
                            <?= $model->contractor_address_legal_full; ?>
                        </div>
                        <div>
                            <b>Р/сч</b>
                            <?= $model->contractor_rs; ?> <br>
                            <?= $model->contractor_bank_name ? ('в ' . $model->contractor_bank_name) : '<br>'; ?>
                        </div>
                        <div>
                            <b>Кор.счет</b>
                            <?= $model->contractor_ks; ?>
                        </div>
                        <div>
                            <b>БИК</b>
                            <?= $model->contractor_bik; ?>
                        </div>
                        <div style="margin-top: 30px;">
                            <?= $model->contractor->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель'; ?>
                        </div>

                        <div style="height: 60px;">
                            <div class="col-xs-6 v_bottom" style="padding-left: 3px;padding-right: 3px;">
                                <div>
                                    &nbsp;
                                </div>
                            </div>
                            <div class="col-xs-6 v_bottom text-center" style="padding-left: 3px;padding-right: 3px;">
                                <div>
                                    <?= $model->contractor->getDirectorFio(); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 pad0">
                                <div style="text-align:center;position:absolute;right: 70px;z-index:1;top: -50px;">
                                </div>
                            </div>
                            <div class="col-xs-6" style="padding-left: 3px;padding-right: 3px;">
                                <div class="bord-dark-t text-center"><span> (подпись) </span></div>
                            </div>
                            <div class="col-xs-6" style="padding-left: 3px;padding-right: 3px;">
                                <div class="bord-dark-t text-center"><span> (расшифровка подписи) </span></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <?php if ($company->company_type_id != CompanyType::TYPE_IP): ?>
                    <!-- Руководитель -->
                    <div class="col-xs-12 pad0" style="padding-top: 10px !important; z-index: 2;">
                        <div class="col-xs-12 pad0" style="height: 65px; text-align: center">
                            <div class="col-xs-3 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Руководитель:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div> <?= ($model->signEmployeeCompany) ? $model->signEmployeeCompany->position : $model->company_chief_post_name ?> </div>
                            </div>
                            <div class="col-xs-3 pad0 v_bottom">
                                <div>
                                    <?php if (isset($images['chief']['link'])) { ?>
                                        <img style="max-width: 130px" src="<?= $images['chief']['link'] ?>" alt="">
                                    <?php } else { ?>
                                        <div style="width: auto !important;">
                                            <div
                                                    class="file_upload_block companyImages-button"
                                                    data-toggle='modal'
                                                    data-target='#modal-companyImages'
                                                    data-tab='<?= $images['chief']['tab'] ?>'>
                                                <?= $images['chief']['name'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0"
                             style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-3 pad3"> &nbsp;</div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> должность </span></div>
                            </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> подпись </span></div>
                            </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                            </div>
                        </div>
                    </div>
                    <!-- Руководитель.end -->
                    <!-- Печать -->
                    <div class="col-xs-12 pad3">
                        <div style="text-align:center;position:absolute; right: 15px; z-index:<?= !empty($images['print']['link']) ? '1; top:-71px;' : '100;top:-16px;' ?>">
                            <?php if (!empty($images['print']['link'])) { ?>
                                <img style="width: 150px;" src="<?= $images['print']['link'] ?>" alt="">
                            <?php } else { ?>
                                <div
                                        class="file_upload_block companyImages-button"
                                        data-toggle='modal'
                                        data-target='#modal-companyImages'
                                        data-tab='<?= $images['print']['tab'] ?>'> <?= $images['print']['name'] ?>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                    <!-- Печать.end -->

                    <!-- Главбух -->
                    <div class="col-xs-12 pad0" style="padding-top: 0px !important;">
                        <div class="col-xs-12 pad0" style="height: 52px;text-align: center;">
                            <div class="col-xs-5 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Главный (старший) бухгалтер:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div>
                                    <?php if (isset($accountantSignatureLink)) { ?>
                                        <img style="max-width: 130px" src="<?= $accountantSignatureLink ?>" alt="">
                                    <?php } ?>
                                    &nbsp;
                                </div>
                            </div>
                            <div class="col-xs-4 pad3 v_bottom" style="z-index: 2">
                                <div>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefAccountantFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0"
                             style="z-index: 2;padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-5 pad3"> &nbsp;</div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> подпись </span></div>
                            </div>
                            <div class="col-xs-4 pad3">
                                <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                            </div>
                        </div>
                    </div>
                    <!-- Главбух.end -->
                <?php else: ?>
                    <div class="m-t-xl">
                        <div class="col-xs-12 pad0"
                             style="background: url('<?= $printLink ?>');background-repeat: no-repeat;height: 150px;-webkit-print-color-adjust: exact;background-position-y: center;background-position-x: 445px;">
                            <br/>
                            <table class="podp-r"
                                   style="background: url('<?= $signatureLink ?>'); background-repeat: no-repeat; background-position: 175px 5px; margin-top: -20px; width: 100%; -webkit-print-color-adjust: exact;">
                                <tr>
                                    <td class="txt-9-b"
                                        style="padding: 0 0 0 0;width: 20%; vertical-align: bottom;">
                                        Предприниматель
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 35%; padding-top: 48px!important;"></td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; vertical-align: bottom; margin: 0 0 0 0; width: 41%;">
                                        <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                        <?php if ($model->signed_by_employee_id) : ?>
                                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <tr class="small-txt">
                                    <td style="padding: 0 0 0 0;width: 20%"></td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        подпись
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        расшифровка подписи
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>
<div class="hidden">
    <span id="unpaid-invoice">
        Что бы отредактировать счет, вам нужно снять оплату.<br>
        Для этого нажмите внизу на кнопку "Снять оплату"
    </span>
</div>
<?php $this->registerJs('
    $(document).ready(function () {
        var $contractorAddress = $(".contractor-address-legal");
        var $companyAddress = $(".company-address-legal");
        var $companyNameShort = $(".company-name-short");
        var $contractorNameShort = $(".contractor-name-short");

        if ($contractorAddress.height() > $companyAddress.height()) {
            $companyAddress.css("height", $contractorAddress.height() + "px");
        } else {
            $contractorAddress.css("height", $companyAddress.height() + "px");
        }

        if ($contractorNameShort.height() > $companyNameShort.height()) {
            $companyNameShort.css("height", $contractorNameShort.height() + "px");
        } else {
            $contractorNameShort.css("height", $companyNameShort.height() + "px");
        }
    });
');
?>
