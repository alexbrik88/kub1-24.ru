<?php

use common\components\grid\GridView;
use common\components\ImageHelper;
use common\models\document\ScanDocument;
use frontend\models\Documents;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\ScanDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фото документов из мобильного приложения';
$this->params['breadcrumbs'][] = $this->title;

$newModel = new ScanDocument([
    'company_id' => Yii::$app->user->identity->company->id,
    'employee_id' => Yii::$app->user->id,
]);
?>

<style>
    @media (min-width: 480px) {
        .scan-document-index .qr-code {
            margin-right: 41px;
        }
    }
    @media (min-width: 768px) {
        .scan-document-index .qr-code {
            margin-right: 61px;
        }
    }
</style>

<div class="scan-document-index">
    <div class="portlet box">
        <?php /*
        <div class="btn-group pull-right title-buttons">
            <button class="btn yellow modal-form-btn" data-url="<?= Url::to(['create']) ?>">
                <i class="fa fa-plus"></i> ДОБАВИТЬ
            </button>
        </div> */ ?>
        <div style="width: 60%;display: inline-block;">
            <h4 class="page-title"><?= $this->title; ?></h4>
            <p>
                Не надо сканировать документы!<br/>
                Скачайте мобильное приложение "КУБ" и делайте в нём фото документов и они попадают на эту страницу.<br/>
                Далее вы прикрепляете их к нужному документу в КУБе.<br/>
                Удобно и быстро ваши документы собираются в одном месте.
            </p>
        </div>
        <div class="qr-code pull-right" style="width: 100px;display: inline-block;">
            <?= ImageHelper::getThumb('img/android/android-qr.jpeg', [100, 100], [
                'class' => 'pull-right',
            ]); ?>
            <?= Html::a(ImageHelper::getThumb('img/android/download-from-google-play.png', [100, 30], [
                'style' => 'margin-top: 10px;',
            ]), Yii::$app->params['googlePlayLink'], [
                'target' => '_blank',
            ]); ?>
        </div>
    </div>

    <div class="portlet box darkblue">
        <?= Html::beginForm(['index'], 'GET'); ?>
        <div class="search-form-default">
            <div class="col-md-9 pull-right serveces-search" style="max-width: 595px;">
                <div class="input-group">
                    <div class="input-cont">
                        <?= Html::activeTextInput($searchModel, 'name', [
                            'type' => 'search',
                            'placeholder' => 'Поиск...',
                            'class' => 'form-control',
                        ]) ?>
                    </div>
                    <span class="input-group-btn">
                        <?= Html::submitButton('Найти', [
                            'class' => 'btn green-haze',
                        ]); ?>
                    </span>
                </div>
            </div>
        </div>
        <?= Html::endForm(); ?>

        <div class="portlet-title">
            <div class="caption caption_for_input">
                Фото документов
            </div>
        </div>

        <div class="portlet-body accounts-list">
            <div class="table-container">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'emptyText' => 'Вы еще не добавили ни одного фото документа.',
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => ['date', 'php:d.m.Y'],
                        ],
                        [
                            'label' => 'Контрагент',
                            'attribute' => 'contractor_id',
                            'class' => 'common\components\grid\DropDownSearchDataColumn',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'html',
                            'filter' => $searchModel->getContractorFilterItems(),
                            'value' => function ($model) {
                                return $model->contractor ? Html::a(Html::encode($model->contractor->getTitle(true)), [
                                    '/contractor/view',
                                    'type' => $model->contractor->type,
                                    'id' => $model->contractor->id,
                                ]) : '';
                            }
                        ],
                        [
                            'label' => 'Тип документа',
                            'attribute' => 'document_type_id',
                            'class' => 'common\components\grid\DropDownSearchDataColumn',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'filter' => $searchModel->getTypeFilterItems($dataProvider->query),
                            'value' => function ($model) {
                                return Documents::typeNameById($model->document_type_id) ? : '';
                            }
                        ],
                        [
                            'label' => 'Фото',
                            'headerOptions' => [
                                'width' => '70px',
                                'style' => 'padding-left: 10px!important;'
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $content = '';
                                if ($model->file) {
                                    $url = [
                                        'file',
                                        'id' => $model->id,
                                        'name' => $model->file->filename_full,
                                    ];
                                    $content .= Html::a('<span class="pull-center icon icon-paper-clip"></span>', $url, [
                                            'class' => 'scan_link',
                                            'data' => [
                                                'tooltip-content' => '#scan-tooltip-' . $model->id,
                                            ]
                                        ]) . "\n";
                                    if ($model->file->ext == 'pdf') {
                                        $img = Html::tag('embed', '', [
                                            'src' => Url::to($url),
                                            'width' => '250px',
                                            'height' => '400px',
                                            'name' => 'plugin',
                                            'type' => 'application/pdf',
                                        ]);
                                    } else {
                                        $img = Html::img($url, [
                                            'style' => 'max-width: 300px; max-height: 300px;',
                                            'alt' => '',
                                        ]);
                                    }
                                    $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $model->id]);
                                    $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);
                                }

                                return $content;
                            }
                        ],
                        [
                            'attribute' => 'description',
                        ],
                        [
                            'attribute' => 'owner',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($owner = $model->owner) {
                                    return Html::a($owner->title, $owner->viewUrl);
                                }

                                return '';
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update} {delete}',
                            'headerOptions' => [
                                'width' => '50px',
                            ],
                            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\document\Document::DELETE),
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    $content = '<span aria-hidden="true" class="icon-pencil"></span>';
                                    $options = [
                                        'title' => 'Изменить',
                                        'class' => 'modal-form-btn',
                                        'data' => [
                                            'url' => $url,
                                        ],
                                    ];

                                    return Html::tag('span', $content, $options);
                                },
                                'delete' => function ($url) {
                                    return \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить фото документа?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <?php Modal::begin([
            'id' => 'scan-document-modal',
            'header' => Html::tag('h1', 'Фото документа'),
        ]); ?>

        <?php Modal::end(); ?>

    </div>
</div>

<?php
$js = <<<JS
$(document).on('click', '.modal-form-btn', function() {
    $("#scan-document-modal").modal('show');
    $('#scan-document-modal .modal-body').load($(this).data('url'));
});

$(document).on("hidden.bs.modal", "#scan-document-modal", function () {
    $('#scan-document-modal .modal-body').html('')
});

$('.scan_link').tooltipster({
    theme: ['tooltipster-kub'],
    contentCloning: true,
    side: ['right', 'left', 'top', 'bottom'],
});
JS;
$this->registerJs($js);
?>
