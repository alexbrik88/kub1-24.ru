<?php

namespace frontend\themes\mobile\modules\documents\widgets;

use frontend\components\Icon;

/**
 * Class PaymentOrderPaid
 * @package frontend\modules\documents\widgets
 */
class PaymentOrderPaid extends \yii\bootstrap4\Modal
{
    public $title = 'Выбранные платежки оплачены';
    public $toggleButton = [
        'tag' => 'a',
        'label' => 'Оплачено',
        'class' => 'btn btn-default btn-sm',
        'data-pjax' => 0,
    ];
    public $itemsContainer = '#payment-order-search tbody';
    public $pjaxContainer = '#payment-order-search-pjax';
    public $modelId;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $setIds = $this->modelId ? "
                ids.push('{$this->modelId}');
        " : "
                $('{$this->itemsContainer} tr:has(.joint-operation-checkbox:checked)').each(function() {
                    ids.push($(this).data('key'));
                });
        ";

        $reload = $this->pjaxContainer ? "
                        $.pjax.reload('{$this->pjaxContainer}', {timeout : false});
        " : "
                        location.reload();
        ";

        $this->getView()->registerJs(<<<JS
            $(document).on('click', '#{$this->getId()} button.confirm', function(e) {
                e.preventDefault();
                var modal = $('#{$this->getId()}');
                var ids = [];
                {$setIds}
                if (ids.length > 0) {
                    $.post($(this).data('url'), {id: ids, date: $('#date_of_payment', modal).val()}, function (data) {
                        {$reload}
                    });
                }
                $('#many-paid').modal('hide');
            });
JS
        );
    }

    /**
     * @return string
     */
    public function run()
    {
        echo $this->render('paymentOrderPaid');

        parent::run();
    }
}
