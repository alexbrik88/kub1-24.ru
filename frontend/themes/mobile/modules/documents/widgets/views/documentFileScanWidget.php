<?php

use common\models\file\File;
use common\models\file\widgets\FileList;
use frontend\modules\documents\widgets\ScanListWidget;
use frontend\components\Icon;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var \yii\web\View $this */

/* @var \frontend\modules\documents\widgets\DocumentFileScanWidget $widget */
$widget = $this->context;
$model = $widget->model;
$ident = "{$model->urlPart}-{$model->id}";
$containerId = "{$ident}-file-scan-container";

$options = [
    'id' => $containerId,
    'class' => 'document-file-scan-container row',
    'data' => [
        'upload-url' => $widget->uploadUrl,
        'delete-url' => $widget->deleteUrl,
        'list-url' => $widget->listUrl,
        'free-url' => $widget->scanFreeUrl,
        'bind-url' => $widget->scanBindUrl,
        'scan-url' => $widget->scanListUrl,
        'model-id' => $model->id,
        'csrf-parameter' => Yii::$app->request->csrfParam,
        'csrf-token' => Yii::$app->request->csrfToken,
    ],
];
$btnOptions = [
    'class' => 'inputFile-btn document-file-scan-button upl_btn',
    //'style' => 'width:33px; height: 27px; margin: 5px 0 0;',
];
$buttonsAreaOptions = [
    'class' => 'document-file-scan-buttons-area col-sm-12 hidden',
];

Html::addCssClass($btnOptions, $widget->hasFreeScan ? 'document-file-scan-source-btn' : $widget->uploadBtnClass);

$fileCount = \common\models\file\File::find()->andWhere([
    'owner_model' => $model->className(),
    'owner_id' => $model->id,
])->count();
$scanCount = \common\models\document\ScanDocument::find()->andWhere([
    'owner_model' => $model->className(),
    'owner_id' => $model->id,
])->count();

$filesCount = $fileCount + $scanCount;
$maxFilesCount = 3; // FileUploadAction::$maxFileCount

?>

<?= Html::beginTag('div', $options) ?>

    <div class="col-sm-12">
        <div class="list-container hidden">
            <div class="document-file-list file-list"></div>
            <?php if (method_exists($model, 'getScanDocuments')) : ?>
                <?= ScanListWidget::widget([
                    'model' => $model,
                ]); ?>
            <?php endif ?>
        </div>
        <div class="document-file-loading" style="display: none;">
            <img src="/img/loading.gif">
        </div>
    </div>

    <?= Html::beginTag('div', $buttonsAreaOptions) ?>

        <div class="inputFile">
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']), $btnOptions); ?>
        </div>

        <?php if ($widget->hasFreeScan) : ?>
            <div class="select-source-popover popover bottom hidden" style="padding: 10px;">
                <div class="arrow" style="left: 14%;"></div>
                <div style="text-align: center;">
                    <strong>Загрузить скан документа</strong>
                </div>
                <table class="file-scan-buttons" style="width: 100%;">
                    <tr>
                        <td style="width:50%">
                            <?= Html::button('Из<br>компьютера', [
                                'class' => "{$widget->uploadBtnClass} button-regular button-width button-hover-transparent load-button",
                                'style' => 'margin-bottom: 0;'
                            ]); ?>
                        </td>
                        <td style="width:50%">
                            <?= Html::button('Из<br>"Документы"', [
                                'class' => "button-regular button-width button-hover-transparent",
                                'data-toggle' => 'modal',
                                'data-target' => "#{$ident}-free-scan-modal",
                                'style' => 'margin-bottom: 0;'
                            ]); ?>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center; font-size: 10px; padding-top:5px;">
                    Вы можете загрузить еще
                    <span class="uploaded-scans-count">
                        <?= \php_rutils\RUtils::numeral()->getPlural($maxFilesCount - $filesCount, ['файл', 'файла', 'файлов']) ?>
                    </span>
                    объемом 5 MB
                </div>
            </div>
            <?php Modal::begin([
                'id' => "{$ident}-free-scan-modal",
                'title' => 'Выберите скан документа',
                'closeButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'close']),
                    'class' => 'modal-close close',
                ],
                'options' => [
                    'class' => 'free-scan-list-modal fade',
                    'data' => [
                        'container' => "#{$containerId}",
                    ],
                ],
            ]); ?>
            <?php Modal::end(); ?>
        <?php endif ?>

    <?= Html::endTag('div') ?>

<?= Html::endTag('div') ?>
