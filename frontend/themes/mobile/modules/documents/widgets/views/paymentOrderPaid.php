<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */

?>

<div class="row">
    <div class="col-6">
        <?= Html::label('Дата оплаты', '', [
            'class' => 'label',
        ]) ?>
        <div class="date-picker-wrap">
            <div class="position-relative">
                <?= Html::textInput('date', date('d.m.Y'), [
                    'id' => 'date_of_payment',
                    'class' => 'form-control date-picker',
                    'style' => 'width: 100%; cursor: pointer; background-color: #ffffff;',
                    'readonly' => true,
                    'data-position' => 'bottom right',
                ]) ?>
                <svg class="date-picker-icon svg-icon input-toggle">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                </svg>
            </div>
        </div>
    </div>
</div>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr confirm',
        'style' => 'width: 130px!important;',
        'data-url' => Url::to(['/documents/payment-order/paid',]),
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'style' => 'width: 130px!important;',
        'data-dismiss' => 'modal',
    ]); ?>
</div>