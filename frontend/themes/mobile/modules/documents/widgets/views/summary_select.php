<?php

use yii\helpers\Html;

/* @var $widget frontend\modules\cash\widgets\SummarySelectWidget */

$js = <<<SCRIPT
;(function($){
    var checkSum = function() {
        var count = 0;
        var sum = 0;
        $('.joint-operation-checkbox:checked').each(function(){
            count++;
            sum += parseInt($(this).data('sum'));
        });
        $('.selected-count').text(count);
        $('.selected-sum').text(number_format(sum/100, 2, ',',' '));
    }
    $(document).on('change', '.joint-operation-main-checkbox', function () {
        $('.joint-operation-main-checkbox').not(this).prop('checked', $(this).is(':checked')).uniform('refresh');
        $('.joint-operation-checkbox').prop('checked', $(this).is(':checked')).uniform('refresh');
        checkSum();
    });
    $(document).on('change', '.joint-operation-checkbox', function(){
        checkSum();
        $('.joint-operation-main-checkbox')
            .prop('checked', $('.joint-operation-checkbox:not(:checked)').length == 0)
            .uniform('refresh');
    });
    if ($('.page-proxy').length) {
        $('.wrap_btns.check-condition').find('.total-sum').parent().hide();
        $('.wrap_btns.check-condition').find('.total-cnt').parent().addClass('mr-auto');
    }
}(jQuery));
SCRIPT;

$this->registerJs($js);
?>
<div class="wrap wrap_btns check-condition" style="padding-right: 10px; padding-left: 10px;">
    <div class="row align-items-center justify-content-end actions-many-items">
        <div class="column flex-shrink-0">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table">
            <span class="total-cnt total-txt-foot ml-3">
                Выбрано:
                <strong class="selected-count ml-1" >0</strong>
            </span>
        </div>
        <div class="column flex-shrink-0 mr-auto ml-5">
            <span class="total-sum total-txt-foot">
                Сумма:
                <strong class="selected-sum ml-1">0,00</strong>
            </span>
        </div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
