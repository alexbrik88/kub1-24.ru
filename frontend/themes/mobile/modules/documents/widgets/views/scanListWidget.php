<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var \common\models\document\AbstractDocument $model */

$modelName = empty($model->urlPart) ? 'model' : $model->urlPart;
$ident = "{$modelName}-{$model->id}";
$listId = "{$ident}-scan-list";
?>

<div id="<?= $listId ?>" class="document-scan-list">
    <?php foreach ($model->scanDocuments as $scan) : ?>
        <?php if ($scan->file && !$scan->file->is_deleted) : ?>
            <div id="scan-<?= $scan->id; ?>" class="scan" data-id="<?= $scan->id; ?>">
                <?= Html::tag('span', Icon::get('clip'), [
                    'class' => 'icon-clip',
                ]) . Html::a($scan->file->filename_full, [
                    '/documents/scan-document/file',
                    'id' => $scan->id,
                    'name' => $scan->file->filename_full,
                ], [
                    'class' => 'scan-link link',
                    'target' => '_blank',
                    'download' => '',
                ]); ?>
                <?php if (empty($deleteAccessCallback) || call_user_func($deleteAccessCallback, $scan)) : ?>
                    <?php Modal::begin([
                        'id' => 'scan-del-confirm-' . $scan->id,
                        'title' => 'Вы уверены, что хотите удалить файл?',
                        'titleOptions' => [
                            'class' => 'ta-c',
                        ],
                        'closeButton' => false,
                        'toggleButton' => [
                            'label' => Icon::get('circle-close'),
                            'tag' => 'span',
                            'class' => 'icon-close',
                        ],
                        'options' => [
                            'class' => 'confirm-modal fade',
                        ],
                    ]); ?>
                        <div class="text-center">
                            <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
                                'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 mt-ladda-btn ladda-button scan-list-del-button',
                                'data-style' => 'expand-right',
                                'data' => [
                                    'list' => "#{$listId}",
                                    'params' => [
                                        'model-id' => 0,
                                        'scan-id' => $scan->id,
                                        '_csrf' => Yii::$app->request->csrfToken,
                                    ],
                                ],
                            ]); ?>
                            <?= Html::button('НЕТ', [
                                'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                                'data' => [
                                    'dismiss' => 'modal',
                                ],
                            ]); ?>
                        </div>
                    <?php Modal::end(); ?>
                <?php endif ?>
            </div>
        <?php endif ?>
    <?php endforeach; ?>
</div>
