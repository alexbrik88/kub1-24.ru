<?php

namespace frontend\themes\mobile\modules\documents\widgets;

use common\models\store\StoreUser;
use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;


use yii\grid\GridView;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\models\log\LogSearch;

use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use frontend\models\Documents;


/**
 * Class CreatedByWidget
 * @package frontend\modules\documents\widgets
 */
class CreatedByWidget extends Widget
{
    /**
     * @var
     */
    public $createdAt;
    /**
     * @var
     */
    public $author;

    /**
     * @var
     */
    public $statusUpdatedAt;
    /**
     * @var
     */
    public $statusAuthor;
    /**
     * @var
     */
    public $statusAuthorId;

    /**
     * @var
     */
    public $closeButton;
    /**
     * @var
     */
    public $toggleButton;
    /**
     * @var
     */
    public $footer;
    /**
     * @var
     */
    public $model;

    /**
     *
     */
    public function run()
    {
        /* @var $user Employee|StoreUser */
        $user = Yii::$app->user->identity;
        Modal::begin([
            'closeButton' => $this->closeButton !== null ? $this->closeButton : false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'class' => 'button-list button-hover-transparent button-clr mb-2 mr-2',
                'label' => '<svg class="svg-icon svg-icon_size_"><use xlink:href="/images/svg-sprite/svgSprite.svg#info"></use></svg>'
            ],
            'footer' => $this->footer !== null ? $this->footer : Html::button('OK', [
                'class' => 'button-list button-hover-transparent button-clr',
                'data-dismiss' => 'modal',
            ]),
        ]);

        ?>
        <div class="created-by text-center">
            Создан <span><?= $this->createdAt; ?></span> <span><?= Html::encode($this->author); ?></span>
        </div>
        <?php if (!empty($this->statusUpdatedAt) && (date_create($this->statusUpdatedAt) >= date_create($this->createdAt))): ?>
        <div class="created-by">
            последняя смена статуса <span><?= $this->statusUpdatedAt; ?></span>

            <?php if ($this->statusAuthor) : ?>
                <span><?= Html::encode($this->statusAuthor); ?></span>
            <?php elseif ($this->statusAuthorId): ?>
                <span> (id#<?= $this->statusAuthorId; ?>)</span>
            <?php endif ?>
        </div>
    <?php endif; ?>

        <?php
        $modelName = is_object($this->model) ? get_class($this->model) : '';
        $dataLog = !empty($this->model) ? Log::find()
            ->select('message')
            ->where([
                'model_id' => $this->model->id,
                'company_id' => $user instanceof Employee ?
                    $user->company->id :
                    $user->currentStoreCompany->currentStoreCompanyContractor->contractor->company_id,
                'model_name' => $modelName
            ])
            ->asArray()
            ->all() : [];

        foreach (!empty($dataLog) ? $dataLog : [] as $log) { ?>
            <?php if (isset($log['message'])): ?>
                <div class="created-by">
                 <span>
                     <?= $log['message'] ?>
                 </span>
                </div>
            <?php endif; ?>
        <?php } ?>

        <?php Modal::end();
    }
}
