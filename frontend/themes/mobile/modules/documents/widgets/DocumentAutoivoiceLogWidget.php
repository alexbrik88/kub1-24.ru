<?php

namespace frontend\themes\mobile\modules\documents\widgets;

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\status\AgreementStatus;
use common\models\document\Waybill;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


/**
 * Class DocumentLogWidget
 * @package frontend\themes\mobile\modules\documents\widgets
 */
class DocumentAutoivoiceLogWidget extends Widget
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $closeButton;
    /**
     * @var
     */
    public $toggleButton;
    /**
     * @var
     */
    public $footer;

    public static $eventsM = [
        LogEvent::LOG_EVENT_CREATE => 'Создан',
        LogEvent::LOG_EVENT_DELETE => 'Удален',
        LogEvent::LOG_EVENT_UPDATE => 'Изменен',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];
    public static $eventsF = [
        LogEvent::LOG_EVENT_CREATE => 'Создана',
        LogEvent::LOG_EVENT_DELETE => 'Удалена',
        LogEvent::LOG_EVENT_UPDATE => 'Изменена',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];
    public static $eventsN = [
        LogEvent::LOG_EVENT_CREATE => 'Создано',
        LogEvent::LOG_EVENT_DELETE => 'Удалено',
        LogEvent::LOG_EVENT_UPDATE => 'Изменено',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];

    /**
     * @return array
     */
    public function getEvents($model)
    {
        switch ($model->className()) {
            case InvoiceFacture::className():
            case PackingList::className():
            case Waybill::className():
            case TaxDeclaration::className():
                return self::$eventsF;
                break;

            default:
                return self::$eventsM;
                break;
        }
    }

    /**
     *
     */
    public function run()
    {
        $modelNameArray = $this->model->className();
        if ($this->model instanceof Invoice) {
            $modelNameArray = [$this->model->className(), InvoiceAuto::className()];
        }
        $logArray = Log::find()->andWhere([
            'model_id' => $this->model->id,
            'model_name' => $modelNameArray,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->orderBy(['id' => SORT_ASC])->all();
        $events = $this->getEvents($this->model);

        Modal::begin([
            'options' => [
                'class' => 'doc-history-modal fade',
            ],
            'closeButton' => $this->closeButton !== null ? $this->closeButton : false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'class' => 'btn darkblue btn-sm info-button',
                'style' => 'width:33px; height: 27px;',
                'label' => '<i class="icon-info" style="color:white"></i>'
            ],
            'footer' => $this->footer !== null ? $this->footer : Html::button('OK', [
                'class' => 'button-list button-hover-transparent button-clr',
                'data-dismiss' => 'modal',
            ]),
        ]);

        echo '<h4 class="modal-title">Последние действия</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>';

        //$count = $this->model->auto->getInvoices()->andWhere(['not', ['email_messages' => null]])->count();
        //var_dump($count);

        $fio = $this->model->documentAuthor->getShortFio();

        echo Html::tag('div', date('d.m.Y', $this->model->created_at) . " создан $fio", [
            'class' => 'created-by',
            'style' => 'margin-top: 5px;',
        ]);
        echo Html::tag('div', DateHelper::format($this->model->auto->next_invoice_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . " дата отправки счета", [
            'class' => 'created-by',
            'style' => 'margin-top: 5px;',
        ]);


        Modal::end();
    }
}
