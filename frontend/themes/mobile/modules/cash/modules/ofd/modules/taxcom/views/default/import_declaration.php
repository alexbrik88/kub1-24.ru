<?php
/**
 * @var $this  yii\web\View
 */
/** @var \frontend\modules\tax\models\TaxDeclaration $declaration */

use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$cancelUrl = [
    '/cash/ofd/default/import',
    'p' => Yii::$app->request->get('p'),
];

?>
<div id='statement-request-form-container' style="overflow-y: hidden; overflow-x: hidden;">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'send-declaration',
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-6">

        </div>
        <?php if (!$success): ?>
            <div class="col-12" style="font-size: 14px;">
                Отправить налоговую декларацию за <?= $declaration->tax_year ?> г. в ОФД
                <b><?= $model->getOfdName(); ?></b>.
            </div>
        <?php endif; ?>
        <br/>
        <br/>
    </div>

    <?= Html::activeHiddenInput($model, 'declarationId', [
        'value' => $declaration->id
    ]); ?>

    <?php if (!$success): ?>
        <div class="clearfix">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary dis-none-bank']) ?>
            <?= Html::submitButton('<i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg',
                'style' => 'width: 205px !important; color: white; float: left;',
            ]) ?>

            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'btn btn-primary ofd-module-link dis-none-bank',
                'style' => 'float:right;',
                'data-dismiss' => 'modal'
            ]) ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $cancelUrl, [
                'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg ofd-module-link',
                'style' => 'float:right; width: 100px !important; color: white;',
                'data-dismiss' => 'modal'
            ]) ?>
        </div>
    <?php else: ?>
        <?php
        $id1 = ArrayHelper::getValue($ids, 'PackageChainId');
        $id2 = ArrayHelper::getValue($ids, 'DocumentId');
        ?>
        <div style="padding: 10px 0">
            <a href="<?= "https://uts.taxcom.ru/Documents/ViewPackageChain.aspx?Folder=AllDrafts&id={$id1}" ?>" target="_blank">
                Перейти в Такском-Спринтер
            </a>
        </div>
        <?= Html::a('Ок', '#', [
            'class' => 'btn btn-primary dis-none-bank',
            'style' => '',
            'data-dismiss' => 'modal'
        ]) ?>
        <?= Html::a('<i class="fa fa-reply fa-2x"></i>', '#', [
            'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg',
            'style' => 'width: 100px !important; color: white;',
            'data-dismiss' => 'modal'
        ]) ?>
    <?php endif; ?>

    <?php $form->end() ?>

</div>