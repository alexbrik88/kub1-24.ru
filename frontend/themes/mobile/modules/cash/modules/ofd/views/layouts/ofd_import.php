<?php
/**
 * @var $this  yii\web\View
 */

use common\components\ImageHelper;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\widgets\Alert;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$ofdModel = ArrayHelper::getValue($this->params, 'ofdModel');

$ofdUrl =
    Url::to([
        "/cash/ofd/{$ofdModel::$alias}/default/import",
        'p' => Yii::$app->request->get('p'),
    ]);

$this->beginContent('@frontend/modules/cash/modules/ofd/views/layouts/main.php');
?>

<h4 class="modal-title">Загрузить декларацию в ОФД</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body">

    <div class="bank-form">
        <div class="row pb-3">
            <?php if ($ofdModel) : ?>
                <div class="col-5 d-flex flex-column align-items-center justify-content-center">
                    <img class="flex-shrink-0" src="/img/icons/<?=$ofdModel::$alias?>.png" width="250" height="150"/>
                </div>
                <?php if ($ofdModel->showSecureText) : ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3">
                            <div class="p-1">
                                Для обеспечения безопасности данных используется <br>
                                протокол зашифрованного соединения. <br>
                                SSL - надежный протокол для передачи конфиденциальной <br>
                                банковской информации и соблюдаются требования <br>
                                международного стандарта PCI DSS по хранению и передаче <br>
                                конфиденциальной информации в банковской сфере.
                            </div>
                        </div>
                    </div>
                <?php elseif ($ofdModel->showOfdBatton) : ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3">
                            <div class="p-1">
                                <a href="<?= $ofdUrl ?>" class="ofd-module-link button-clr button-regular button-regular_red w-100">
                                    Загрузить из ОФД
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>

    </div>

</div>

<?php $this->endContent(); ?>
