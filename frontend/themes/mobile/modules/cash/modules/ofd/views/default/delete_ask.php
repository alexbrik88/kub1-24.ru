<?php

use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\models\AbstractOfdModel;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this  yii\web\View */
/* @var $model AbstractOfdModel */
/* @var $delete bool */
/* @var $message string */
/* @var $ofdName string */
/* @var $backUrl string|array */

$hasData = OfdParams::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'ofd_alias' => Yii::$app->controller->ofdModelClass::$alias,
])->exists()
?>

<?php if ($hasData) : ?>
    <?php $ofdName = Yii::$app->controller->ofdModelClass::NAME; ?>
    <div class="clearfix" style="margin-top: 20px;">
        <?php
        echo \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
            'options' => [
                'id' => 'delete-confirm',
            ],
            'toggleButton' => [
                'tag' => 'a',
                'label' => 'Удалить интеграцию с ОФД ' . $ofdName,
                'class' => 'link',
            ],
            'confirmUrl' => Url::toRoute([
                'delete',
                'p' => Yii::$app->request->get('p'),
            ]),
            'confirmParams' => [],
            'message' => "Вы уверены, что хотите удалить интеграцию с {$ofdName}?",
        ]);
        ?>
    </div>
<?php endif ?>
