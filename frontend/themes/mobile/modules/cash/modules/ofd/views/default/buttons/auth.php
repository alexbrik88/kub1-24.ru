<?php

use yii\helpers\Html;

/* @var $cancelUrl array|string */

?>

<div class="row pt-3">
    <div class="col-4">
        <?= Html::submitButton('Подтвердить интеграцию', [
            'class' => 'button-clr button-regular button-regular_red w-100'
        ]) ?>
    </div>
    <div class="column ml-auto">
        <?= Html::a('Отменить', $cancelUrl, [
            'class' => 'button-regular button-grey button-clr button-width ofd-module-link',
        ]) ?>
    </div>
</div>
