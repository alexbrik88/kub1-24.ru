<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$cashbox_id = Yii::$app->request->get('cashbox_id');
$formData = Html::hiddenInput('className', 'CashOrderFlows') . Html::hiddenInput('cashbox', $cashbox_id);
?>

<div class="row">
    <div class="col-12" style="margin-top: 20px;">
        <div class="xls-error"></div>
        <div class="row">
            <?php ActiveForm::begin([
                'action' => '#',
                'method' => 'post',
                'id' => 'xls-ajax-form-2',
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>
            <?= $formData; ?>
            <?= Html::hiddenInput('createdModels', null, [
                'class' => 'created-models',
            ]); ?>
            <div class="file-list col-sm-12">
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
    <div class="row action-buttons buttons-fixed xls-buttons"
         style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
             style="text-align: left;width: 50%">
            <?= Html::a('Сохранить', 'javascript:;', [
                'class' => 'button-regular button-width button-regular_red button-clr undelete-models',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
             style="text-align: right;width: 50%;">
            <?= Html::a('Отменить', 'javascript:;', [
                'class' => 'button-clr button-width button-regular button-hover-transparent xls-close',
            ]); ?>
        </div>
    </div>
    <div class="col-sm-12">
        <div id="progressBox2"></div>
    </div>
</div>