<?php
use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\XlsHelper;

$uploadXlsTemplateUrl = Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_ORDER]);

$ofdUrl = Url::to([
        '/cash/ofd/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);

?>

<h4 class="modal-title">Загрузка выписки из ОФД</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body" id="ofd-upload">

    <div class="row pb-1">
        <div class="form-group col-6 mb-0">
            <button class="add-xls-file-2 button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button" style="max-width: 179px">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Загрузить файл</span>
            </button>
            <br><br>
            <div class="upload-1C-hide">
                Заполните шаблон данными из ОФД <br>
                и загрузите его сюда.
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-show" style="display: none; ">
            <div class="upload-xls-button-2">
                <a href="#" class="button-clr button-regular button-regular_red width-160 pull-right disabled">
                    Загрузить
                </a>
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-hide">
            <a href="<?= $ofdUrl ?>" class="ofd-module-link button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button" style="max-width: 183px">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#bank-3"></use>
                </svg>
                <span>Загрузить из ОФД</span>
            </a>
            <br><br>
            <div>
                Мы уже интегрировались с несколькими ОФД. <br>
                Если у вас касса в этом ОФД, то операции по кассе  <br>
                будут загружаться напрямую из ОФД.
            </div>
        </div>
    </div>

    <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
        'class' => 'upload-xls-template-url',
        'data-pjax' => '0',
        'target' => '_blank',
    ]); ?>

    <?= $this->render('_import_xls') ?>
</div>

<script>
    $(document).on('click', '.upload-xls-button-2 a', function (e) {
        e.preventDefault();
        $('#ofd-upload').find('.file-list .item').remove();
        $('#ofd-upload').find('.upload-xls-button-2 a').addClass('disabled');
        $('#ofd-upload').find('.add-xls-file-2').removeClass('disabled');
        $('#ofd-upload').find('#xls-ajax-form-2').submit();
    });
    $('#ofd-upload .file-list').on('click', '.delete-file', function (e) {
        e.preventDefault();
        $(this).parent().remove();
        $('#ofd-upload').find('.xls-title').show();
        vidimusUploader['_queue'].splice(0, 1);
        $('#ofd-upload').find('.upload-xls-template-url').show();
        $('#ofd-upload').find('.upload-xls-button-2 a').addClass('disabled');
        $('#ofd-upload').find('.add-xls-file-2').removeClass('disabled');
    });
</script>