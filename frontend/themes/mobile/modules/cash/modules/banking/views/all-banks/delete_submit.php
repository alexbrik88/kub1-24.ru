<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$submitText = ArrayHelper::getValue($_params_, 'submitText', 'Подтвердить');
$cancelUrl = ArrayHelper::getValue($_params_, 'cancelUrl', ['index', 'p' => Yii::$app->request->get('p')]);
?>

<div class="form-group">
    <?= Html::submitButton($submitText, ['class' => 'button-clr button-regular button-regular_red w-100']) ?>
</div>
<div class="form-group">
    <?= Html::a('Отменить', $cancelUrl, [
        'class' => 'button-regular button-regular_red w-100',
    ]) ?>
</div>
