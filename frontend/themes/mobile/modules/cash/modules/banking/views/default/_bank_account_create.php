<?php
use frontend\widgets\Alert;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $bank backend\models\Bank */
/* @var $model common\models\company\ApplicationToBank */

?>

<div class="modal-header">
    <h4 class="modal-title">Заявка на открытие расчетного счета</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>
</div>
<div class="modal-body">

    <?php $form = ActiveForm::begin([
        'action' => [
            'account',
            'bankId' => $bank->id,
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'id' => 'form-apply-bank-' . $bank->id,
            'data' => [
                'pjax' => true,
                'pjax-container' => 'banking-module-pjax'
            ]
        ],
        'fieldConfig' => Yii::$app->params['mobileFieldConfig'],
        'enableAjaxValidation' => true,
    ]); ?>

    <?php if ($bank->description): ?>
        <div class="form-group description-bank">
            <?= nl2br($bank->description); ?>
        </div>
    <?php endif; ?>

    <?= Alert::widget(); ?>

    <?= $form->field($model, 'company_name'); ?>

    <?= $form->field($model, 'inn'); ?>

    <?= $form->field($model, 'kpp'); ?>

    <?= $form->field($model, 'legal_address'); ?>

    <?= $form->field($model, 'fio'); ?>

    <?= $form->field($model, 'contact_phone')->textInput(['data-inputmask' => "'mask': '+7(9{3}) 9{3}-9{2}-9{2}'"]); ?>

    <?= $form->field($model, 'contact_email'); ?>

    <div class="form-group license-bank">
        Нажимая кнопку «Открыть счет», вы даете свое согласие на
        <?= Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank',
        ]) ?>
        и отправку этих данных в банк «<?= $bank->bank_name ?>»
    </div>

    <div class="form-group">
        <?= Html::submitButton('Открыть счет', [
            'class' => 'button-regular button-regular_red w-100',
        ]); ?>
    </div>
    <?= Html::a('Отменить', [
        '/cash/banking/default/index',
        'p' => Yii::$app->request->get('p'),
    ], [
        'class' => 'banking-module-link button-regular button-regular_red w-100',
    ]); ?>

    <?php $form->end(); ?>

</div>