<?php
/**
 * @var $this  yii\web\View
 * @var $model common\components\banking\AbstractService||null
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$bankArray = BikDictionary::find()
        ->select(['bik', 'name'])
        ->byBik(Banking::bikList())
        ->byActive()->all();
?>


<h4 class="modal-title">Запрос выписки из банка</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="statement-service-content" style="position: relative; min-height: 110px;">
        <div style="min-height: 100px;">
            <?php foreach ($bankArray as $bank) {
                echo $this->render('_select_bank_item', ['model' => $bank]);
            } ?>
        </div>
        <?= Html::a('Отменить', [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'button-regular button-regular_red w-100 banking-module-link',
        ]); ?>
        <div class="statement-loader"></div>
    </div>
</div>