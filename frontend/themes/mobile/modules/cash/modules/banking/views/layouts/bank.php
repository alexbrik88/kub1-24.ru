<?php
/**
 * @var $this  yii\web\View
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\components\Icon;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\widgets\Alert;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$bankArray = BikDictionary::find()
    ->select(['bik', 'name'])
    ->byBik(Banking::bikList())
    ->byActive()->all();

$bankModel = ArrayHelper::getValue($this->params, 'bankingModel');
$bankName = Html::encode($bankModel->bank ? $bankModel->bank->name : '');
$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;
$bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
$bankingUrl = $bankingAccount && ($alias = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$alias}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);

$this->beginContent('@frontend/modules/cash/modules/banking/views/layouts/main.php');
?>

<div class="modal-header">
    <h4 class="modal-title">Запрос выписки из банка</h4>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
        'aria-label' => 'close',
    ]) ?>
</div>

<div class="modal-body">
    <div class="bank-form">
        <?php if ($bankModel && ($bankPartner = $bankModel->bankPartner)) : ?>
            <div class="border form-group">
                <?= ImageHelper::getThumb($bankPartner->getUploadDirectory() . $bankPartner->logo_link, [250, 150], [
                    'class' => 'flex-shrink-0',
                    'alt' => $bankName,
                ]); ?>
            </div>
            <?php if (!$bankModel->showBankBatton): ?>
                <div class="form-group">
                    <strong>
                        <?= $bankName ?><br>
                        <?= $bankModel->currentAccount ? 'р/с ' . $bankModel->currentAccount->rs : ''; ?>
                    </strong>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>
    </div>
</div>

<?php if (Yii::$app->request->get('start_load_urgently')): ?>
    <script>
        $(document).ready(function() {
            $('#statement-request-form').submit();
        });
    </script>
<?php endif; ?>

<?php $this->endContent(); ?>
