<?php

use backend\models\Bank;
use frontend\components\Icon;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $company common\models\Company */

$rs = ArrayHelper::getValue(json_decode(base64_decode(Yii::$app->request->get('p')), true), 'rs');
$bankingUrl = Url::to([
    '/cash/banking/default/select',
    'p' => Yii::$app->request->get('p'),
    'show_no_bank' => true
]);
foreach ($company->checkingAccountants as $account) {
    if (!empty($rs) && $rs != 'all' && $rs != $account->rs) {
        continue;
    }
    $alias = Banking::aliasByBik($account->bik);

    if ($alias && (empty($rs) || $rs == 'all' || $rs == $account->rs)) {
        $bankingUrl = Url::to([
            "/cash/banking/{$alias}/default/index",
            'account_id' => $account->id,
            'p' => Yii::$app->request->get('p'),
        ]);
        break;
    }
}
?>

<div class="modal-header">
    <h4 class="modal-title">Загрузка выписки</h4>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
        'aria-label' => 'close',
    ]) ?>
</div>
<div class="modal-body">
    <?php if (Yii::$app->getRequest()->get('s') == 'f') : // source is file ?>
        <section>
            <button class="button-hover-content-red button-regular w-100" type="button"
                onclick="$(this).siblings('.add-vidimus-file').first().click()">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#1c"></use>
                </svg>
                <span>Загрузить файл 1С</span>
            </button>
            <button class="add-vidimus-file pt-3" style="display: none"></button>
            <div class="mt-3 upload-1C-hide">
                Выгрузите из Клиент-Банка файл формата 1С <br>
                и загрузите его сюда.
            </div>
        </section>
    <?php else : ?>
        <section>
            <button class="button-hover-content-red button-regular w-100" type="button"
                onclick="$(this).siblings('.add-vidimus-file').first().click()">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#1c"></use>
                </svg>
                <span>Загрузить файл 1С</span>
            </button>
            <button class="add-vidimus-file" style="display: none"></button>
            <div class="mt-3 upload-1C-hide">
                Выгрузите из Клиент-Банка файл формата 1С <br>
                и загрузите его сюда.
            </div>
        </section>
        <section class="mt-4 border-top pt-4 upload-1C-hide">
            <a href="<?= $bankingUrl ?>" class="banking-module-link button-hover-content-red button-regular w-100" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#bank-3"></use>
                </svg>
                <span>Загрузить из банка</span>
            </a>
            <div class="mt-3">
                Мы уже интегрировались с несколькими банками. <br>
                Если у вас есть счет в этом банке, то выписка будет <br>
                загружаться напрямую из банка.
            </div>
        </section>
    <?php endif ?>
    <div class="mt-4 upload-1C-show" style="display: none; ">
        <div class="upload-button pb-2">
            <a href="#" class="button-clr button-regular button-regular_red w-100 disabled">
                Загрузить
            </a>
        </div>
    </div>

    <?= $this->render('_vidimus_upload') ?>
</div>