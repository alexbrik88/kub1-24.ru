<?php

use yii\helpers\Html;

/* @var $cancelUrl array|string */

?>

<div class="">
    <div class="form-group">
        <?= Html::submitButton('Подтвердить интеграцию', [
            'class' => 'button-regular button-regular_red w-100'
        ]) ?>
    </div>
    <div class="form-group">
        Для обеспечения безопасности данных используется
        протокол зашифрованного соединения. <br/>
        SSL - надежный протокол для передачи конфиденциальной
        банковской информации и соблюдаются требования
        международного стандарта PCI DSS по хранению и передаче
        конфиденциальной информации в банковской сфере.
    </div>
    <div class="form-group">
        <?= Html::a('Отменить', $cancelUrl, [
            'class' => 'button-regular button-grey w-100 banking-module-link',
        ]) ?>
    </div>
</div>