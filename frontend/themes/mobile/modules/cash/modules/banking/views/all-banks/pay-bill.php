<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\modules\bank044525999\models\BankModel
 */

use frontend\widgets\Alert;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="pay-bill" style="position: relative;">
    <div style="max-width: 430px; margin: 5% auto auto; padding: 15px; border: 1px solid #ccc;">
        <?= Alert::widget(); ?>

        <?php if ($model->paymentOrder) : ?>
            <h4>
                Платежное поручение
            </h4>
            <div class="form-group">
                <span style="width: 100px; display: inline-block; font-weight: bold;">
                    Сумма
                </span>
                <?= $model->paymentOrder->amountFormated ?> руб.
            </div>
            <div class="form-group">
                <span style="width: 100px; display: inline-block; font-weight: bold;">
                    Получатель
                </span>
                <?= $model->paymentOrder->contractor_name ?>
            </div>
            <div class="form-group">
                <span style="width: 100px; display: inline-block; font-weight: bold;">
                    Счет
                </span>
                <?= $model->paymentOrder->contractor_current_account ?>
            </div>
            <div class="form-group">
                <span style="width: 100px; display: inline-block; font-weight: bold;">
                    Банк
                </span>
                <?= $model->paymentOrder->contractor_bik ?>
                <?= $model->paymentOrder->contractor_bank_name ?>
            </div>
        <?php endif; ?>

        <?php if ($sent) : ?>
            <div class="form-group">
                <?php
                $bankUrl = $model->bankPartner ? $model->bankPartner->url : null;
                $msg = 'Платежное поручение успешно отправлено в клиент банк<br>и находится в разделе "Черновики".<br>';
                $msg .= ($bankUrl ? Html::a('Подтвердите платежку', $bankUrl, ['target' => '_blank']) : 'Подтвердите платежку');
                $msg .= ', чтобы оплатить по счету.';
                echo $msg;
                ?>
            </div>
        <?php else : ?>
            <div class="form-group" style="color: red;">
                Платежное поручение не удалось отправить в клиент банк.
            </div>
        <?php endif; ?>
    </div>
</div>
