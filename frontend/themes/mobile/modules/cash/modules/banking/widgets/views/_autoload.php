<?php

use common\models\cash\CashBankStatementUpload;
use yii\bootstrap4\Collapse;

?>

<?php if ($model->getHasAutoload() && $model->currentAccount) : ?>

<?php
$uploadedArray = $model->currentAccount ? $model->currentAccount->getStatementUploads()
    ->andWhere(['source' => CashBankStatementUpload::SOURCE_BANK_AUTO])
    ->orderBy(['created_at' => SORT_DESC])
    ->limit(5)
    ->all() : [];
?>

<button class="link link_collapse link_bold button-clr mt-3 collapsed" type="button" data-toggle="collapse" data-target="#moreBankDetails" aria-expanded="false" aria-controls="moreBankDetails">
    <span class="link-txt">Автоматически загружать выписку</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
    </svg>
</button>
<div class="collapse" id="moreBankDetails">
    <div class="row">
        <div class="col">
            <?= $this->render('_autoload_form', [
                'model' => $model,
                'needPay' => $needPay,
            ]) ?>
        </div>
    </div>
    <div class="bank-form-wrap mt-3">
        <strong class="mb-2 d-block">Последниие автозагрузки</strong>
        <table class="table table-style table-style_2 mb-0">
            <thead>
            <tr>
                <th class="align-middle">Период</th>
                <th class="align-middle">Дата <br> загрузки</th>
                <th class="align-middle">Загружено <br> операций</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($uploadedArray) : ?>
                <?php foreach ($uploadedArray as $uploaded) : ?>
                    <?php
                    $from = new \DateTime($uploaded->period_from);
                    $till = new \DateTime($uploaded->period_till);
                    ?>
                    <tr>
                        <td>
                            <?= ($from && $till) ? $from->format('d.m.y') . '—' . $till->format('d.m.y') : '' ?>
                        </td>
                        <td>
                            <?= date('d.m.Y', $uploaded->created_at) ?>
                        </td>
                        <td>
                            <?= $uploaded->saved_count ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php else : ?>
                <tr>
                    <td colspan="3">
                        Автозагрузки не найдены
                    </td>
                </tr>
            <?php endif ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).on('submit', '#autoload-mode-form', function(e) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#autoload_save_report').show();
            setTimeout(function() {
                $('#autoload_save_report').hide();
            }, 3000);
        });
    });
</script>

<?php endif ?>
