
<?php

use common\models\Company;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $table string */
/* @var $company common\models\Company */
/* @var $currentAccount common\models\company\CheckingAccountant */
/* @var $vidimus array */
/* @var $statistic array */
/* @var $newRequestUrl string */

?>

<?php if ($table) : ?>
    <div class="vidimus-head">
        Проверка на дубли
        <?= Html::tag('img', null, [
            'class' => 'banking-modal-scroll-down pull-right',
            'src' => '/img/chevron-sign-down.png',
            'width' => '32',
            'height' => '32',
        ]) ?>
    </div>

    <?= Html::beginForm([
        '/cash/banking/default/upload',
        'p' => Yii::$app->getRequest()->get('p'),
    ], 'post', [
        'id' => 'vidimus_form_ajax',
    ]) ?>

    <?= Html::hiddenInput('accountData[accountId]', $currentAccount->rs); ?>
    <?= Html::hiddenInput('accountData[accountBik]', $currentAccount->bik); ?>
    <?= Html::hiddenInput('accountData[isValidCompany]', 1); ?>
    <?= Html::hiddenInput('source', CashBankStatementUpload::SOURCE_BANK); ?>
    <?= Html::hiddenInput('dateStart', isset($statistic['dateStart']) ? $statistic['dateStart'] : null); ?>
    <?= Html::hiddenInput('dateEnd', isset($statistic['dateEnd']) ? $statistic['dateEnd'] : null); ?>
    <?= Html::hiddenInput('balanceStart', isset($statistic['balanceStart']) ? $statistic['balanceStart'] : null); ?>
    <?= Html::hiddenInput('balanceEnd', isset($statistic['balanceEnd']) ? $statistic['balanceEnd'] : null); ?>
    <?= Html::hiddenInput('totalIncome', isset($statistic['totalIncome']) ? $statistic['totalIncome'] : null); ?>
    <?= Html::hiddenInput('totalExpense', isset($statistic['totalExpense']) ? $statistic['totalExpense'] : null); ?>

    <?= $table ?>

    <?= Html::submitButton('Принять и сохранить', [
        'class' => 'button-regular button-regular_red w-100 ladda-button',
        'form' => 'vidimus_form_ajax',
    ]) ?>
    <?= Html::a('Отменить', [
        '/cash/banking/default/index',
        'p' => Yii::$app->request->get('p'),
    ], [
        'class' => 'button-regular button-regular_red w-100 mt-2',
    ]) ?>

    <?= Html::endForm(); ?>
<?php else : ?>
    <div class="form-group">
        <div class="gray-alert">
            За указанный период не найдено операций по указанному счету.
        </div>
    </div>

    <?= Html::a('Новый запрос', $newRequestUrl, [
        'class' => 'button-regular button-regular_red w-100',
    ]); ?>
<?php endif ?>
