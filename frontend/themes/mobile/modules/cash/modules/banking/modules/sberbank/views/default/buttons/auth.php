<?php

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $cancelUrl array|string */

?>
<style>
    .tooltipster-kub.tooltipster-top {margin-left:2px;}
</style>

<div class="pt-3">
    <div class="form-group">
        <div id="sberbank-choose-token" class="collapse mb-2" style="text-align:center;">
            <div class="mar-b-5 text-bold text-center">У вас есть токен?</div>
            <div class="btn-group" style="border: 1px solid #ddd; border-radius:4px;">
                <button class="btn sberbank-token">Да</button>
                <button class="btn sberbank-web">Нет</button>
            </div>
        </div>
        <?= Html::button('Подтвердить интеграцию', [
            'class' => 'button-regular button-regular_red w-100 tooltip-sberbank-token collapsed',
            'data-target' => '#sberbank-choose-token',
            'data-toggle' => 'collapse'
        ]) ?>
    </div>
    <div class="">
        <?= Html::a('Отменить', $cancelUrl, [
            'class' => 'button-regular button-grey w-100 banking-module-link',
        ]) ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click', '.sberbank-token', function() {
            $('#bankmodel-user_type').val('1');
            $('#statement-request-form').submit();
        });
        $(document).on('click', '.sberbank-web', function() {
            $('#bankmodel-user_type').val('0');
            $('#statement-request-form').submit();
        });
    });
</script>