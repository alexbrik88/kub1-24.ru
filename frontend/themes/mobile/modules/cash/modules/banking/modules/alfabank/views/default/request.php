<?php

use common\components\date\DateHelper;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\widgets\AccountSelectWidget;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\alfabank\models\BankModel */

?>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'tochka-statement-request-form',
        'action' => [
            'request',
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'account_id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>

    <?php  if ($model->currentAccount) {
        echo Html::hiddenInput('account', $model->currentAccount->rs, ['id' => 'tochka-currentAccount']);
    } ?>

    <div class="form-group">
        <?= AccountSelectWidget::widget([
            'form' => $form,
            'bankModel' => $model,
            'linkOptions' => [
                'class' => 'banking-module-link',
            ],
        ]); ?>
    </div>
    <div class="form-group">
        <label class="label" for="bankPeriod1">Начало периода</label>
        <div class="date-picker-wrap w-130">
            <?= \yii\bootstrap4\Html::activeTextInput($model, 'start_date', [
                'class' => 'form-control date-picker',
                'data-date-viewmode' => 'years',
                'value' => DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
            <svg class="date-picker-icon svg-icon input-toggle">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
            </svg>
        </div>
    </div>
    <div class="form-group">
        <label class="label" for="bankPeriod1">Конец периода</label>
        <div class="date-picker-wrap w-130">
            <?= \yii\bootstrap4\Html::activeTextInput($model, 'end_date', [
                'class' => 'form-control date-picker',
                'data-date-viewmode' => 'years',
                'value' => DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
            <svg class="date-picker-icon svg-icon input-toggle">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
            </svg>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Отправить запрос', [
            'class' => 'button-regular button-regular_red w-100',
        ]); ?>
    </div>
    <div class="form-group">
        <?= Html::a('Отменить', [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'button-regular button-regular_red w-100',
        ]); ?>
    </div>
    <?php $form->end() ?>

    <?php if ($model->isValidToken()) : ?>
        <?= \frontend\modules\cash\modules\banking\widgets\AutoloadWidget::widget([
            'model' => $model,
        ]); ?>
    <?php else : ?>
        <div class="mar-t-15">
            Для интеграции с банком необходимо воити в
            <?= Html::a('интернет-банк', $model->clientBankUrl(), ['target' => '_blank']) ?>
            и настроить "Услуги партнеров Альфа-Банка" для партнера "КУБ".
        </div>
    <?php endif ?>
</div>
