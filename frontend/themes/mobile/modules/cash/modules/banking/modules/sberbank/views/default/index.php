<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\modules\sberbank\models\BankModel
 */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\widgets\AccountSelectWidget;
use kartik\select2\Select2;

?>

<div id='statement-request-form-container' style="overflow-y: auto; overflow-x: hidden;">
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'index',
            'account_id' => Yii::$app->request->get('account_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="form-group">
        <?= AccountSelectWidget::widget([
            'form' => $form,
            'bankModel' => $model,
            'linkOptions' => [
                'class' => 'banking-module-link',
            ],
        ]); ?>
        <?= $form->field($model, 'user_type')->hiddenInput()->label(false); ?>
    </div>
    <div class="cform-group">
        Для автоматической загрузки выписки по счёту, нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в интернет-банке
        <b><?= $model->getBankName(); ?></b>. После этого вы будете перенаправлены обратно в сервис для загрузки
        банковских операций.
    </div>

    <?= $form->field($model, 'auth_redirect', ['template' => "{input}"])->hiddenInput(['value' => 1]); ?>

    <?= $this->render('buttons/auth', [
        'cancelUrl' => [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ]
    ]) ?>

    <?php $form->end() ?>

    <?php if ($model->scenario != $model::SCENARIO_DEFAULT) : ?>
        <?= \frontend\modules\cash\modules\banking\widgets\AutoloadWidget::widget([
            'model' => $model,
        ]); ?>
    <?php endif ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>
