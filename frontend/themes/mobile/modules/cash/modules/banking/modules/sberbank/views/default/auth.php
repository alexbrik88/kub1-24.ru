<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model frontend\modules\cash\modules\banking\modules\bank044525999\models\BankModel */

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
</head>
<body>
<?= Html::beginForm($model->authUrl, 'post', [
    'id' => 'form',
    'csrf' => false,
]) ?>

    <?= Html::hiddenInput('redirectUri', $model->returnUrl) ?>
    <?= Html::hiddenInput('client_id', $model->client_id) ?>
    <?= Html::hiddenInput('scope', $model->scope) ?>

<?= Html::endForm() ?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){
        document.forms["form"].submit();
    });
</script>
</body>
</html>
