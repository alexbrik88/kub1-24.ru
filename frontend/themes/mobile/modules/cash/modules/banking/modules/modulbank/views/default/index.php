<?php
/**
 * @var $this  yii\web\View
 * @var $action  string
 * @var $model frontend\modules\cash\modules\banking\modules\bank044525999\models\BankModel
 */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\widgets\AccountSelectWidget;

?>

<div id='statement-request-form-container'>
    <div class="form-group">
        <?= AccountSelectWidget::widget([
            'action' => $action,
            'bankModel' => $model,
            'linkOptions' => [
                'class' => 'banking-module-link',
            ],
        ]); ?>
    </div>
    <div class="form-group" style="font-size: 1.4rem;">
        Для автоматической загрузки выписки по счёту, нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в интернет-банке
        <b><?= $model->getBankName(); ?></b>. После этого вы будете перенаправлены обратно в сервис для загрузки
        банковских операций.
    </div>

    <?= Html::beginForm($model->authUrl, 'post', ['csrf' => false]) ?>

        <?= Html::hiddenInput('responseType', 'code') ?>
        <?= Html::hiddenInput('redirectUri', $model->returnUrl) ?>
        <?= Html::hiddenInput('clientId', $model->clientId) ?>
        <?= Html::hiddenInput('scope', $model->scope) ?>

        <?= $this->render('@banking/views/all-banks/buttons/auth', [
            'cancelUrl' => [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ]
        ]) ?>

    <?= Html::endForm() ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>
