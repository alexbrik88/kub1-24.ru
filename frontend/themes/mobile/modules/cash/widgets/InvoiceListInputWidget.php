<?php

namespace frontend\themes\mobile\modules\cash\widgets;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Contractor;
use common\models\employee\Employee;
use kartik\select2\Select2Asset;
use yii\base\InvalidParamException;
use yii\bootstrap4\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\InputWidget;

/**
 * Class InvoiceListInputWidget
 * @package frontend\modules\cash\widgets
 */
class InvoiceListInputWidget extends InputWidget
{
    public $type_attribute = 'flow_type';
    public $contractor_attribute = 'contractor_id';
    public $amount_attribute = 'amount';
    public $date_attribute = 'date';
    public $invoicesListUrl = ['/cash/bank/un-payed-invoices'];

    /** @inheritdoc */
    public function init()
    {
        parent::init();

        if (!$this->hasModel()) {
            throw new InvalidParamException("Only active mode available");
        }

        if (!$this->model instanceof CashBankFlowsForm && !$this->model instanceof CashFlowsBase) {
            throw new InvalidParamException("Incorrect model class (need CashBankFlowsForm)");
        }
    }

    /** @inheritdoc */
    public function run()
    {
        Select2Asset::register($this->view);

        /** @var CashBankFlows $model */
        $model = $this->model;

        $flowTypeName = Html::getInputName($model, $this->type_attribute);
        $contractorName = Html::getInputName($model, $this->contractor_attribute);
        $dateName = Html::getInputName($model, $this->date_attribute);

        $selectId = Html::getInputId($model, $this->attribute);
        $amountId = Html::getInputId($model, $this->amount_attribute);
        $dateId = Html::getInputId($model, $this->date_attribute);

        $value = (array)$model->{$this->attribute};
        $valueJs = Json::encode($value);
        $modelId = !$model->isNewRecord ? $model->id : 0;
        $flowType = $model->flow_type;

        $selectLabelHtml = Html::label('Оплаченные счета', null, [
            'class' => 'label',
        ]);

        $selectHtml = Html::activeDropDownList($model, $this->attribute, [], [
            'multiple' => true,
            'class' => 'form-control',
            'id' => $selectId,
            'pluginOptions' => [
                'width' => '100%'
            ]
        ]);

        $inputHtml = Html::input('text', null, null, [
            'class' => 'form-control',
            'id' => $selectId . '_amountLabel',
            'disabled' => 'disabled',
            //'style' => 'width: 100%;',
        ]);

        echo Html::tag('div', $selectLabelHtml.$selectHtml, [
            'class' => 'invoice_list_widget_invoices form-group col-6',
            //'style' => 'padding-left: 0;',
        ]);

        $labelHtml = Html::label('На сумму:', $selectId . '_amountLabel', [
            'class' => 'label bold-text',
            //'style' => 'padding-left: 0px !important; padding-right: 0px !important;',
        ]);

        echo Html::tag('div', $labelHtml.$inputHtml, [
            'class' => 'form-group col-6',
        ]);

        $invoices = Json::encode($this->_getInvoices());
        $invoicesListUrl = Url::to($this->invoicesListUrl);

        $js = <<<JS
            (function() {
                var
                    invoices = {$invoices},
                    value = {$valueJs},
                    flowId = {$modelId},
                    flowType = '{$flowType}',
                    flowTypeName = '{$flowTypeName}',
                    contractorName = '{$contractorName}',
                    dateName = '{$dateName}',
                    \$select = $('#{$selectId}'),
                    \$dateInput = $('#{$dateId}'),
                    \$realAmountInput = $('#{$amountId}'),
                    \$amountInput = $('#{$selectId}_amountLabel'),
                    \$contractorInput = $('[name="{$contractorName}"]'),
                    \$flowTypeInput = $('[name="{$flowTypeName}"]');

                var reinitSelect = function(invoices) {
                    if (\$select.data('select2')) {
                        \$select.select2("destroy");
                    }

                    \$select.find('option').remove();

                    if (invoices.length > 0) {
                        for (var i in invoices) {
                            var params = {
                                value: invoices[i].id,
                                text: '№' + invoices[i].number,
                                data: {
                                    amount: invoices[i].amount,
                                    amountPrint: invoices[i].amountPrint,
                                    number: invoices[i].number
                                }
                            };

                            if ($.inArray(invoices[i].id, value) !== -1) {
                                params['selected'] = 'selected';
                            }

                            \$select.append($('<option>', params));
                        }
                    }

                    \$select.select2({
                        width: '100%',
                        templateResult: function (data) {
                            var
                                element = data.element,
                                number = $(element).data('number'),
                                amountPrint = $(element).data('amountPrint');

                            return $('<span><div class="checkbox_select2">&nbsp;</div>№' + number + ' на ' + amountPrint + ' р.' + '</span>');
                        },
                        placeholder: " ",
                    });

                    updateAmount();
                    updateSelected();
                    \$select.off('change.amount').on('change.amount', function() {
                         updateAmount();
                         updateSelected();

                         value = $(this).val();
                    });

                    \$realAmountInput.off('change.amount').on('change.amount', updateAmount);
                };

                var updateSelected = function() {

                    var
                        numbers=[],
                        selectedOptions = \$select.find('option:selected');

                    selectedOptions.each(function() {
                        numbers.push('№' + $(this).data('number'));
                    });

                    $('.invoice_list_widget_invoices .select2-selection__rendered').each(function () {
                        if (this.nextSibling) {
                            this.nextSibling.parentNode.removeChild(this.nextSibling);
                        }
                    });

                    $('.invoice_list_widget_invoices .select2-selection__rendered').after(numbers.join(', '));
                };

                var updateAmount = function() {
                    var
                        amount=0,
                        selectedOptions = \$select.find('option:selected'),
                        realAmount = parseFloat(\$realAmountInput.val().replace(",", "."));

                    selectedOptions.each(function() {
                        amount += $(this).data('amount');
                    });

                    \$amountInput.val(formatMoney(amount, 2, ',', ' ') + ' р.');

                    var isError = (isNaN(realAmount) || realAmount != amount);

                    if (isError) {
                        \$amountInput.addClass('amount-error').removeClass('amount-success');
                    } else {
                        \$amountInput.addClass('amount-success').removeClass('amount-error');
                    }
                };

                var formatMoney = function(n, c, d, t){
                    var c = isNaN(c = Math.abs(c)) ? 2 : c,
                        d = d == undefined ? "." : d,
                        t = t == undefined ? "," : t,
                        s = n < 0 ? "-" : "",
                        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                        j = (j = i.length) > 3 ? j % 3 : 0;
                   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
                 };

                var ajaxInvoicesData = {};
                var lastSelectedDate;

                var reloadInvoicesList = function(\$form) {
                    var formData = {};

                    \$form.serializeArray().forEach(function(e) {
                        formData[e.name] = e.value;
                    });

                    reinitSelect([]);

                    if (formData[contractorName]) {
                        var key = formData[contractorName] + '_' + formData[dateName];
                        if (ajaxInvoicesData[key] == undefined) {
                            $.ajax({
                                url: '{$invoicesListUrl}',
                                data: {
                                    'contractorId': formData[contractorName],
                                    'type': formData[flowTypeName],
                                    'flowId': flowId,
                                    'flowDate': formData[dateName],
                                },
                                type: 'get',
                                async: false,
                                dataType: 'json',
                                success: function(invoices) {
                                    ajaxInvoicesData[key] = invoices;
                                }
                            });
                        }
                        reinitSelect(ajaxInvoicesData[key] || []);
                    }
                };

                \$flowTypeInput.on('change', function() {
                    reinitSelect([]);
                });

                \$contractorInput.on('change', function() {
                    reloadInvoicesList($(this).closest('form'));
                });

                \$dateInput.on('change', function() {
                    if (lastSelectedDate != this.value) {
                        reloadInvoicesList($(this).closest('form'));
                    }
                    lastSelectedDate = this.value;
                });

                reinitSelect(invoices);
            })();
JS;

        $this->view->registerJs($js);
    }

    /**
     * @return Contractor|null
     */
    protected function _getContractor()
    {
        $attributeName = $this->contractor_attribute;
        $contractorId =  $this->model->{$attributeName};

        if ($contractorId > 0) {
            /** @var Contractor $contractor */
            $contractor = Contractor::findOne(['id' => $contractorId]);

            return $contractor;
        }

        return null;
    }

    /**
     * @return bool
     */
    protected function _isFlowTypeIn()
    {
        $attributeName = $this->type_attribute;
        $flowType = $this->model->{$attributeName};

        return ($flowType == CashBankFlowsForm::FLOW_TYPE_INCOME);
    }

    /**
     * @return array
     */
    protected function _getInvoices()
    {
        /** @var Employee $employer */
        $employer = \Yii::$app->user->identity;
        $contractor = $this->_getContractor();

        if ($employer instanceof Employee && !empty($contractor)) {
            return CashBankFlowsForm::getAvailableInvoices($employer->company, $contractor, !$this->_isFlowTypeIn(), $this->model);
        }

        return [];
    }
}
