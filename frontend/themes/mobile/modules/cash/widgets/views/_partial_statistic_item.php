<?php
use common\components\TextHelper;

/**
 * @var $text string
 * @var $class string
 * @var $amount int
 * @var $statistic_text string
 * @var $statistic_text_class string
 */
?>

<div class="count-card-column col-6">
    <div class="count-card <?= $class; ?> wrap">
        <div class="count-card-main"><?= TextHelper::$format($amount, $decimals); ?> ₽</div>
        <div class="count-card-title"><?= $text; ?></div>
        <hr>
        <div class="count-card-foot"><?= $statistic_text; ?></div>
    </div>
</div>

