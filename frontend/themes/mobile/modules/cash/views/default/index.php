<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

use common\components\grid\GridView;
use common\components\TextHelper;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\widgets\RangeButtonWidget;
use yii\grid\DataColumn;
use yii\helpers\Html;

$this->title = 'Итого';
?>
<!-- default/index -->

<div class="stop-zone-for-fixed-elems  cash-total-flows-index">

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-9 align-single-h4">
                <h4>Обороты</h4>
            </div>
            <div class="col-3 justify-content-end">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="row">
            <div class="col-12">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'tableOptions' => [
                        'class' => 'table table-style table-cash-totals',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'rowOptions' => function($model, $key, $index, $grid) {
                        return ['class' => $model['cssClass']];
                    },
                    'columns' => [
                        [
                            'attribute' => 'typeName',
                            'label'     => '',
                            'headerOptions' => [
                            ],
                            'value' => function($model, $key, $index, DataColumn $column) {
                                return $model['cssClass'] ?
                                    Html::tag('span', $model[$column->attribute], ['class' => $model['cssClass']]) :
                                    $model[$column->attribute];
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'beginBalance',
                            'label'     => 'Баланс на начало периода',
                            'headerOptions' => [
                                'class' => 'align-middle',
                                'style' => [
                                    'width' => '180px'
                                ]
                            ],
                            'value' => function($model, $key, $index, DataColumn $column) {
                                return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                            },
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                        ],
                        [
                            'attribute' => 'income',
                            'label'     => 'Приход',
                            'headerOptions' => [
                                'class' => 'align-middle',
                                'style' => [
                                    'width' => '180px'
                                ]
                            ],
                            'value' => function($model, $key, $index, DataColumn $column) {
                                return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                            },
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                        ],
                        [
                            'attribute' => 'expense',
                            'label'     => 'Расход',
                            'headerOptions' => [
                                'class' => 'align-middle',
                                'style' => [
                                    'width' => '180px'
                                ]
                            ],
                            'value' => function($model, $key, $index, DataColumn $column) {
                                return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                            },
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                        ],
                        [
                            'attribute' => 'endBalance',
                            'label'     => 'Баланс на конец периода',
                            'headerOptions' => [
                                'class' => 'align-middle',
                                'style' => [
                                    'width' => '180px'
                                ]
                            ],
                            'value' => function($model, $key, $index, DataColumn $column) {
                                return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                            },
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>

</div>
