<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */
?>
<div class="cash-bank-flows-update">

    <?php if (!Yii::$app->request->isAjax):?>
        <h3><?= Html::encode($this->title) ?></h3>
    <?php endif;?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
