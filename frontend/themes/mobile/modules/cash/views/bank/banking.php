<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $modalOptions array */
/* @var $pjaxOptions array */
/* @var $content mixed */

$this->title = 'Банк';
$enablePushState = true;

$url = Url::to([
    '/cash/banking/default/index',
    'p' => Banking::routeEncode(['/cash/bank/banking']),
]);

$js = <<<JS
var vidimusSubmitClicked = false;

$(document).ready(function(e) {
    $.pjax({url: "{$url}", container: "#banking-module-pjax", push: $("#banking-module-modal").data("push-state")});
});

$(document).on("change", "#bankmodel-account_id", function (e) {
    $('span.banking-accoun-select-loader').removeClass('hidden');
    $.pjax({
        url: $("option:selected", this).data("url"),
        container: "#banking-module-pjax",
        push : $("#banking-module-modal").data("push-state"),
    });
});
$(document).on("ajaxComplete", function(){
    $('span.banking-accoun-select-loader').addClass('hidden');
});

$(document).on("click", "button.statement-form-submit", function(e) {
    if (vidimusSubmitClicked) {
        e.preventDefault();
    } else {
        vidimusSubmitClicked = true;
    }
    setTimeout(vidimusSubmitClicked = false, 1000);
});
JS;

$this->registerJs($js);
?>

<?= Html::beginTag('div', [
    'id' => 'banking-module-modal',
    'data-push-state' => $enablePushState,
    'data-page-title' => $this->title,
    'data-page-url' => Url::to(['/cash/bank/banking', 'bik' => Yii::$app->getRequest()->get('bik')]),
]); ?>
    <?php Pjax::begin([
        'id' => 'banking-module-pjax',
        'enablePushState' => $enablePushState,
        'enableReplaceState' => false,
        'timeout' => 10000,
        'linkSelector' => '.banking-module-link',
        'options' => [
            'data-push-state' => $enablePushState,
        ]
    ]); ?>

    <?php Pjax::end(); ?>
<?= Html::endTag('div') ?>