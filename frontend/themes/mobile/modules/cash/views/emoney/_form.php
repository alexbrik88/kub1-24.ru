<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashEmoneyFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\date\DateHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\Contractor;
use frontend\modules\cash\models\CashContractorType;
use frontend\themes\mobile\helpers\Icon;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$company = Yii::$app->user->identity->company;
if (empty($model)) {
    $model = new CashEmoneyFlows([
        'company_id' => $company->id,
        'emoney_id' => $company->emoney->id,
        'flow_type' => CashEmoneyFlows::FLOW_TYPE_INCOME,
        'date' => date(DateHelper::FORMAT_USER_DATE),
        'is_accounting' => 1,
    ]);
}
$emoneyData = $company->getEmoneys()->select('name')->orderBy([
    'is_closed' => SORT_ASC,
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->indexBy('id')->column();

$income = 'income' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');

switch ($model->flow_type) {
    case CashEmoneyFlows::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $contractorData = ['add-modal-contractor' =>  Icon::PLUS . ' Добавить покупателя '] +
            ArrayHelper::map(CashContractorType::find()
                ->andWhere(['not in', 'name', [
                    $model->getNewCashContractorId(),
                    CashContractorType::COMPANY_TEXT,
                ]])->all(), 'name', 'text')
            +
            $company->sortedContractorList(Contractor::TYPE_CUSTOMER);
        $contractorListOptions = Contractor::getAllContractorSelect2Options(Contractor::TYPE_CUSTOMER);
        break;
    case CashEmoneyFlows::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $contractorData = ['add-modal-contractor' => Icon::PLUS . ' Добавить поставщика '] +
            ArrayHelper::map(CashContractorType::find()
                ->andWhere(['not in', 'name', [
                    $model->getNewCashContractorId(),
                    CashContractorType::BALANCE_TEXT,
                    CashContractorType::COMPANY_TEXT,
                ]])->all(), 'name', 'text')
            +
            $company->sortedContractorList(Contractor::TYPE_SELLER);
        $contractorListOptions = Contractor::getAllContractorSelect2Options(Contractor::TYPE_SELLER);
        break;

    default:
        $contractor = 'Контрагент';
        $contractorData = [];
        $contractorListOptions = [];
        break;
}
$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' движение по e-money';

$typeItems = $model->isNewRecord ? CashEmoneyFlows::getFlowTypes() : [$model->flow_type => CashEmoneyFlows::getFlowTypes()[$model->flow_type]];

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use></svg></div>';
?>

<?php Pjax::begin([
    'id' => $model->isNewRecord ? 'add_emoney_flow_pjax' : 'update_emoney_flow_pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->id],
    'id' => 'cash-emoney-form',
    'options' => [
        'data' => [
            'pjax' => true,
        ]
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => Yii::$app->params['mobileFieldConfig'],
])); ?>

<div class="form-body row">

    <?= $form->field($model, 'flow_type')->label('Тип')->radioList($typeItems, [
        'encode' => false,
        'item' => function ($index, $label, $name, $checked, $value) use ($model) {

            $input = Html::radio($name, $checked, [
                'class' => 'flow-type-toggle-input',
                'value' => $value,
                'label' => '<span class="radio-txt-bold">'.$label.'</span>',
                'labelOptions' => [
                    'class' => 'label mb-3 mr-3 mt-2',
                ]
            ]);

            return Html::a($input, ['create', 'type' => $value], [
                'class' => 'col-xs-5 m-l-n',
                'style' => 'padding: 0; color: #333333; text-decoration: none;'
            ]);
        }
    ]); ?>

    <?= $form->field($model, 'emoney_id')->widget(Select2::classname(), [
        'data' => $emoneyData,
        'options' => [
            'id' => 'cashemoneyflows-emoney_id-' . $model->id,
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'placeholder' => '',
            'width' => '100%'
        ],
    ]); ?>

    <?= $form->field($model, 'contractor_id')->label($contractor)->widget(Select2::class, [
        'data' => $contractorData,
        'options' => [
            'id' => 'cashemoneyflows-contractor_id-' . $model->id,
            'class' => 'contractor-items-depend form-group ' . ($model->flow_type ? 'customer' : 'seller'),
            'placeholder' => '',
            'data' => [
                'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
            ],
            'options' => $contractorListOptions
        ],
        'pluginOptions' => [
            'width' => '100%',
            'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 25); }'),
            'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
        ],
    ]); ?>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE): ?>
        <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
            'loadAssets' => false,
            'options' => [
                'id' => 'cashemoneyflows-expenditure_item_id-' . $model->id,
                'class' => 'flow-expense-items',
                'prompt' => '--',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ]
        ]); ?>
    <?php endif; ?>

    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'cashemoneyflows-expenditure_item_id-',
    ]); ?>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME): ?>
        <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::class, [
            'loadAssets' => false,
            'income' => true,
            'options' => [
                'id' => 'cashemoneyflows-income_item_id-' . $model->id,
                'class' => 'flow-income-items',
                'prompt' => '--',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ]
        ]); ?>
    <?php endif; ?>

    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'cashemoneyflows-income_item_id-',
        'type' => 'income',
    ]); ?>

    <?php $config = [
        'options' => [
            'class' => 'form-group col-6', // remove `form-group` class
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
    ]; ?>

    <?= $form->field($model, 'amount', $config)->textInput([
        'value' => !empty($model->amount) ? $model->amount / 100 : '',
        'class' => 'form-control js_input_to_money_format',
    ]); ?>

    <div class="col-12 mb-0">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'date', [
                    'inputTemplate' => $inputCalendarTemplate,
                    'options' => ['class' => 'form-group'],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ])->label('Дата оплаты'); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'date_time', ['options' => ['class' => 'form-group']])->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '9{2}:9{2}:9{2}',
                    'options' => [
                        'class' => 'form-control',
                        'value' => ($model->date_time) ? date('H:i:s', strtotime($model->date_time)) : date('H:i:s')
                    ],
                ])->label('Время оплаты'); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'number', [
                    'options' => ['class' => 'form-group'],
                ])->textInput(); ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'description', [
        'options' => [
            'class' => 'col-12 form-group'
        ]])->textarea([
        'style' => 'resize: none;',
        'rows' => '2',
    ]); ?>

    <?= $form->field($model, 'invoices_list', [
        'options' => [
            'class' => 'col-12',
        ],
        'wrapperOptions' => [
            'class' => 'row'
        ]
    ])->widget(\frontend\themes\mobile\modules\cash\widgets\InvoiceListInputWidget::class, [
        'invoicesListUrl' => ['/cash/order/un-payed-invoices'],
    ])->label(false); ?>

    <?= $form->field($model, 'is_accounting', [
        'options' => [
            'class' => 'form-group col-12',
        ],
        'labelOptions' => [
            'style' => 'margin-bottom:0; margin-top:6px;'
        ]
    ])->checkbox(); ?>

    <div class="col-12 mb-0">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'recognitionDateInput', [
                    'inputTemplate' => $inputCalendarTemplate,
                    'options' => ['class' => 'form-group'],
                    'labelOptions' => [
                        'class' => 'label bold-text',
                        'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                'class' => 'flow-type-toggle ' . $income,
                            ]) . Html::tag('span', 'расхода', [
                                'class' => 'flow-type-toggle ' . $expense,
                            ]),
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                    'disabled' => (bool)$model->is_prepaid_expense
                ]); ?>

            </div>
            <div class="col-3">
                <?= $form->field($model, 'is_prepaid_expense', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'padding-top: 36px'
                    ],
                ])->checkbox(); ?>
            </div>
        </div>
    </div>

</div>

<div class="mt-4 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php $form->end(); ?>

<?php Pjax::end(); ?>
<script type="text/javascript">
    var currentScript = document.currentScript || (function() {
        var scripts = document.getElementsByTagName("script");
        return scripts[scripts.length - 1];
    })();
    var currentModalContent = $(currentScript.parentNode).closest(".modal-content");

    if (currentModalContent) {
        currentModalContent.find(".modal-header > h1").html("<?= $header ?>");
    }

    $(document).on("change", "#cashemoneyflows-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashemoneyflows-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });
</script>