<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use frontend\assets\ExpenditureDropdownWidgetAsset;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\rbac\permissions;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\XlsHelper;
use frontend\widgets\CashBoxFilterWidget;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashOrderFlows
 * @var $company \common\models\Company
 * @var $showImportModal integer
 */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$currentCashboxId = (array)$model->cashbox_id;

$pageRoute = ['/cash/order/index'];

// fix filter defaults
if ($model->flow_type === null)
    $model->flow_type = -1;
if ($model->is_accounting === null)
    $model->is_accounting = -1;

$hasFilters =
    (bool)($model->flow_type > -1) ||
    (bool)$model->contractor_id ||
    (bool)($model->reason_id != null) ||
    (bool)($model->is_accounting > -1);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_cash');
?>

<div class="stop-zone-for-fixed-elems  cash-order-flows-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <?= \frontend\themes\mobile\widgets\CashBoxFilterWidget::widget([
            'pageTitle' => $this->title,
            'cashbox' => $cashbox,
            'company' => $company,
        ]); ?>
        <?php if ($canCreate): ?>
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить</span>', [
                'class' => "ajax-modal-btn button-regular button-regular_padding_medium button-regular_red ml-auto button-clr",
                'title' => "Добавить движение по кассе",
                'data-pjax' => "0",
                'data-url' => Url::to([
                    'create',
                    'type' => CashOrderFlows::FLOW_TYPE_INCOME,
                    'id' => reset($currentCashboxId),
                ]),
            ]) ?>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if (Yii::$app->user->can(permissions\CashOrder::CREATE)): ?>
                    <?= Html::a(' Загрузить из ОФД',
                    [
                        '/cash/ofd/default/index',
                        'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
                        'cashbox_id' => reset($currentCashboxId)
                    ],
                    [
                        'class' => 'ofd-module-open-link button-regular w-100 button-hover-content-red',
                        'disabled' => true
                    ]) ?>

                    <?= \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget::widget([
                        'pageTitle' => $this->title,
                        'pageUrl' => Url::to($pageRoute),
                    ]) ?>
                <?php endif; ?>

                <?php if (true || Yii::$app->user->can(permissions\CashOrder::CREATE)): ?>
                    <?= Html::button('Загрузить из Excel', [
                        'class' => 'button-regular w-100 button-hover-content-red mb-0',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#import-xls',
                        ],
                    ]); ?>
                    <?= $this->render('//xls/_import_xls', [
                        'header' => 'Загрузка операций по кассе из Excel',
                        'text' => '<p>Для загрузки списка операций из Excel,
                                    <br>
                                    заполните шаблон таблицы и загрузите ее тут.
                                    </p>',
                        'formData' => Html::hiddenInput('className', 'CashOrderFlows') . Html::hiddenInput('cashbox', reset($currentCashboxId)),
                        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_ORDER]),
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'cash_bank_filters',
        'enableClientValidation' => false,
        'method' => 'GET',
    ]); ?>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <div class="row align-items-center">
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                    <?= TableViewWidget::widget(['attribute' => 'table_view_cash']) ?>
                    <!-- buttons -->
                    <?php if ($canUpdate) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'article']) . '<span>Статья</span>',
                            '#many-item', [
                                'class' => 'button-regular button-hover-transparent button-clr mr-2 check-condition',
                                'data-toggle' => 'modal',
                            ]); ?>
                    <?php endif; ?>
                    <?php if ($canDelete) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>',
                            '#many-delete', [
                                'class' => 'button-regular button-hover-transparent button-clr mr-2 check-condition',
                                'data-toggle' => 'modal',
                            ]); ?>
                    <?php endif ?>
                    <!-- filter -->
                    <div class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter cash-filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
                        <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="button-txt">Фильтр</span>
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="filter">
                            <div class="popup-dropdown-in p-3">
                                <div class="p-1">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Приход/Расход</div>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                                    <span>
                                                        <?= $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? 'Приход' :
                                                            ($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расход' : 'Все') ?>
                                                    </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown1">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($model, 'flow_type', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->radioList([
                                                            -1 => 'Все',
                                                            CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
                                                            CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
                                                        ], [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                return Html::radio($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'labelOptions' => [
                                                                        'class' => 'p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1:0
                                                                ]);
                                                            },
                                                        ]); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown2">
                                                <div class="label">Контрагент</div>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                                    <span><?= ($model->contractor_id) ? $model->contractor->shortName : 'Все контрагенты' ?></span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown2">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($model, 'contractor_id', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->radioList($model->getContractorFilterItems(), [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                return Html::radio($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'labelOptions' => [
                                                                        'class' => 'p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1:0
                                                                ]);
                                                            },
                                                        ]); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-6">
                                            <?php $reasonFilterItems = $model->getReasonFilterItems(); ?>
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown3">
                                                    <div class="label">Статья</div>
                                                    <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                        <span><?= ($model->reason_id && isset($reasonFilterItems[$model->reason_id])) ? $reasonFilterItems[$model->reason_id] : ($model->reason_id == 'empty' ? '-' : 'Все статьи') ?></span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown3">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($model, 'reason_id', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->radioList(array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems), [
                                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                                    return Html::radio($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'labelOptions' => [
                                                                            'class' => 'p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $accountingFilterItems = [-1 => 'Все', (string)CashFlowsBase::ACCOUNTING => 'Для учёта', (string)CashFlowsBase::NON_ACCOUNTING => 'Не для учёта', ]; ?>
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown4">
                                                    <div class="label">Учет в бухгалтерии</div>
                                                    <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown4">
                                                        <span><?= (isset($accountingFilterItems[$model->is_accounting])) ? $accountingFilterItems[$model->is_accounting] : 'Все' ?></span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown4">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($model, 'is_accounting', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->radioList($accountingFilterItems, [
                                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                                    return Html::radio($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'labelOptions' => [
                                                                            'class' => 'p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-between">
                                        <div class="col-6 pr-0">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                <span>Применить</span>
                                            </button>
                                        </div>
                                        <div class="col-6 pl-0 text-right">
                                            <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                                <span>Сбросить</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">

            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= $form->field($model, 'contractor_query')->label(false)->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => 'Поиск по контрагенту...']); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading line-height-1em',
        ],
        //'options' => [
        //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        //],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => '',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $typeCss = 'income-item';
                        $income = bcdiv($data->amount, 100, 2);
                        $expense = 0;
                    } else {
                        $typeCss = 'expense-item';
                        $income = 0;
                        $expense = bcdiv($data->amount, 100, 2);
                    }

                    return Html::checkbox('flowId[]', false, [
                        'class' => 'joint-operation-checkbox ' . $typeCss,
                        'value' => $data->id,
                        'data' => [
                            'income' => $income,
                            'expense' => $expense,
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amountIncome',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px;'
                ],
                'contentOptions' => [
                    'class' => 'nowrap'
                ],
                'value' => function (CashOrderFlows $orderFlows) {
                    return is_numeric($orderFlows->amountIncome)
                        ? \common\components\TextHelper::invoiceMoneyFormat($orderFlows->amountIncome, 2)
                        : $orderFlows->amountIncome;
                },
            ],
            [
                'attribute' => 'amountExpense',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px;'
                ],
                'contentOptions' => [
                    'class' => 'nowrap'
                ],
                'value' => function (CashOrderFlows $orderFlows) {
                    return is_numeric($orderFlows->amountExpense)
                        ? \common\components\TextHelper::invoiceMoneyFormat($orderFlows->amountExpense, 2)
                        : $orderFlows->amountExpense;
                },
            ],
            [
                //'class' => DropDownSearchDataColumn::className(),
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function (CashOrderFlows $model) {
                    $text = $model->contractorText;

                    return '<span title="' . htmlspecialchars($text) . '">' . $text . '</span>';
                },
                'filter' => $model->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '250px'
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'purpose-cell',
                ],
                'format' => 'raw',
                'value' => function (CashFlowsBase $data) {
                    if ($invoiceArray = $data->getInvoices()->all()) {
                        $linkArray = [];
                        foreach ($invoiceArray as $invoice) {
                            $linkArray[] = Html::a($data->formattedDescription . $invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $invoice->type,
                                'id' => $invoice->id,
                            ]);
                        }
                        return join(', ', $linkArray);
                    } else {
                        return $data->description;
                    }
                },
            ],
            [
                'attribute' => 'billPaying',
                'label' => 'Опл. счета',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => 'raw',
            ],
            [
                //'class' => DropDownSearchDataColumn::className(),
                'attribute' => 'reason_id',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'clause-cell',
                ],
                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function (CashOrderFlows $model) {
                    $reason = ($model->flow_type == $model::FLOW_TYPE_INCOME) ? $model->incomeReason : $model->expenditureReason;

                    return $reason ? $reason->name : '-';
                },
                's2width' => '200px'
            ],
            [
                'attribute' => 'number',
                'label' => 'Ордер',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '7%',
                    'style' => 'max-width:50px'
                ],
                'format' => 'raw',
                'value' => function (CashOrderFlows $model) {
                    return Html::a($model->name, ['view', 'id' => $model->id]);
                },
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{delete}',
                'headerOptions' => [
                    'width' => '2%',
                    'style' => 'max-width:20px'
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'buttons' => [
                    'delete' => function ($url) {
                        return \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
                            'theme' => 'gray',
                            'toggleButton' => [
                                'label' => '<svg class="svg-icon" style="min-width:20px"><use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use></svg>',
                                'class' => 'button-clr link',
                                'tag' => 'button',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php if ($showImportModal): ?>
    <?php $this->registerJs('
        $("#import-xls").modal();
    '); ?>
<?php endif; ?>

<?= $this->render('_partial/modal-add-contractor'); ?>
<?= $this->render('_partial/modal-many-delete'); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
]); ?>

<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    var l = Ladda.create($(this).find(".btn-save")[0]);
    var $hasError = false;

    l.start();
    $(".field-cashordersearch-incomeitemidmanyitem:visible, .field-cashordersearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        return false;
    }
});

$(document).on("pjax:complete", function(event) {
    //$(".date-picker").datepicker({"language":"ru","autoclose":true}).on("change.dp", function (ev) {
    //    if (ev.bubbles == undefined) {
    //        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
    //        if (ev.currentTarget.value == "") {
    //            if ($input.data("last-value") == null) {
    //                $input.data("last-value", ev.currentTarget.defaultValue);
    //            }
    //            var $lastDate = $input.data("last-value");
    //            $input.datepicker("setDate", $lastDate);
    //        } else {
    //            $input.data("last-value", ev.currentTarget.value);
    //        }
    //    }
    //});

    $("#cash-bank-flows-pjax-container input[type=radio]:not(.md-radiobtn)").uniform();
});

// FILTERS
$("form#cash_bank_filters").find("input:radio").on("change", function() {
        $(this).parents(".dropdown-drop").find("a > span").html($(this).parents("label").text());
        $(this).parents(".dropdown-drop-menu, .dropdown-drop").removeClass("visible show");
});
$(".cash_filters_reset").on("click", function() {
    $(this).parents(".dropdown").find(".dropdown-drop-in").each(function() {
        $(this).find("input:radio").first().prop("checked", true);
        $(this).find("input:radio").uniform();
        $(this).parents(".dropdown-drop").find("a > span").html($(this).find("input:radio").first().parents("label").text());
    });
    refreshProductFilters();
});

// REFRESH_UNIFORMS
$(document).on("shown.bs.modal", "#ajax-modal-box", function() {
    refreshUniform();
    refreshDatepicker();
});

// SEARCH
$("input#cashordersearch-contractor_query").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});

$(document).on("pjax:complete", "#ofd-module-pjax", function(event) {
    // REFRESH_UNIFORMS
    refreshUniform();
    refreshDatepicker();
    if ($("#kktRegNumber").length) {
        createSimpleSelect2("kktRegNumber");
    }
});

/* FILTERS DROPDOWN */
$(".cash-filter .popup-dropdown-in").on("click", function(e) {
    e.stopPropagation();
    $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop.visible.show").removeClass("visible show");
    $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop-menu.visible.show").removeClass("visible show");
    $(e.target).parents(".col-6").siblings(".col-6").find(".dropdown-drop.visible.show").removeClass("visible show");
    $(e.target).parents(".col-6").siblings(".col-6").find(".dropdown-drop-menu.visible.show").removeClass("visible show");
});


/* VIDIMUS BUTTON */
$(document).on(\'mouseover\', \'input[name="uploadfile"]\', function() {
    $(\'.add-xls-file-2\').css({\'color\': \'#335A82\', \'cursor\':\'pointer\'});
});
$(document).on(\'mouseout\', \'input[name="uploadfile"]\', function() {
    $(\'.add-xls-file-2\').css(\'color\', \'#001424\');
});

// FILTER BUTTON TOGGLE COLOR
function refreshProductFilters() {
    var pop = $(".cash-filter");
    var submit = $(pop).find("[type=submit]");
    var filter_on = false;
    $(pop).find(".form-filter-list").each(function() {
        var a_val = $(this).find("input").filter("[data-default=1]").val();
        var i_val = $(this).find("input:checked").val();
        if (i_val === undefined) {
            i_val = $(this).find("label:first-child").find("input").val();
        }
        if (a_val === undefined) {
            a_val = $(this).find("label:first-child").find("input").val();
        }
        if (a_val != i_val) {
            filter_on = true;
            return false;
        }
    });

    if (filter_on)
        $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
    else
        $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
}
$(".cash-filter").find("input").on("change", function(e) {
    e.preventDefault();
    refreshProductFilters();
});

') ?>