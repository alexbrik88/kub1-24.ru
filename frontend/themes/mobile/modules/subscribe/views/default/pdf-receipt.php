<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/29/15
 * Time: 11:36 AM
 * Email: t.kanstantsin@gmail.com
 */
/* @var \yii\web\View $this */
/* @var \common\models\service\Payment $payment */

$this->title = 'Квитанция Сбербанка';

$price = $payment->sum;
$priceRub = (int) $price;
$priceCop = sprintf('%02d', (int) (($price - $priceRub) * 100));

$createdAtDay = date('d', $payment->created_at);
$createdAtMonth = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'F',
    'date' => $payment->created_at,
    'monthInflected' => true,
]);
$createdAtYear = date('y', $payment->created_at);
$paymentName = 'логин ' . $company->email . ', за пользование сервисом "КУБ", www.kub-24.ru срок покупаемой подписки ' . $tariff->getTariffName();
?>

<div class="page-content-in p-center pad-pdf-p">
    <div style="border-right: 3px solid black;">
    <table class="table no-border" style="margin: 0">
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-top: 3px solid black;border-left: 3px solid black;border-right: 3px solid black;">Извещение</td>
            <td class="text-left" width="28%" style="border-top: 3px solid black;"><img src="/img/temp/sber-bank-img.jpg" alt="лого Сбербанка"></td>
            <td class="text-right font-size-8-bold-italic" width="45%" style="border-top: 3px solid black;">Форма № ПД-4</td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" colspan="2" style="border-bottom: 1px solid black"><?= $contractor->getTitle(true); ?></td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l" colspan="2">(наименование получателя платежа)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" width="28%" style="border-bottom: 1px solid black"><?= $contractor->ITN; ?>&nbsp;</td>
            <td width="3%"></td>
            <td class="font-size-8 text-left" width="42%" style="border-bottom: 1px solid black"><?= $contractor->current_account; ?>&nbsp;</td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l">(ИНН получателя платежа)</td>
            <td></td>
            <td class="font-size-6 text-center ver-top m-l">(номер счета получателя платежа)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td width="42%" style="border-bottom: 1px solid black"><?= $contractor->bank_name; ?>&nbsp;</td>
            <td class="font-size-8 text-right" width="6%">БИК</td>
            <td width="25%" style="border-bottom: 1px solid black"><?= $contractor->BIC; ?></td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l">(наименование банка получателя платежа)</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="34%">Номер кор./сч. банка получателя платежа</td>
            <td width="39%" style="border-bottom: 1px solid black"><?= $contractor->corresp_account; ?></td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="34%"></td>
            <td width="39%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0;">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" width="47%" style="border-bottom: 1px solid black"><?= $paymentName; ?></td>
            <td width="3%"></td>
            <td class="font-size-8 text-left" width="23%" style="border-bottom: 1px solid black;">&nbsp;</td>
        </tr>
        <!--cap-->
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left"></td>
            <td></td>
            <td class="font-size-8 text-left"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l" width="47%">(наименование платежа)</td>
            <td class="font-size-6 text-right ver-top m-l" width="26%">(номер лицевого счета (код) плательщика)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%">Ф.И.О. плательщика:</td>
            <td width="55%" style="border-bottom: 1px solid black"><?= $company->getChiefFio(); ?></td>
        </tr>
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%">Адрес плательщика:</td>
            <td class="font-size-8 text-left" width="55%" style="border-bottom: 1px solid black"></td>
        </tr>
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%"></td>
            <td class="font-size-8 text-left" width="55%"></td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="13%" style="padding: 0">Сумма платежа:</td>
            <td width="8%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="5%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">коп.</td>
            <td class="font-size-8 text-center" width="20%" style="padding: 0">Сумма платы за услуги:</td>
            <td width="7%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="4%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0;" width="4%">коп.</td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="13%" style="padding: 0"></td>
            <td width="8%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="5%" class="font-size-7 text-right" ></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td class="font-size-8 text-center" width="20%" style="padding: 0"></td>
            <td width="7%" class="font-size-7 text-right" ></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="4%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0;" width="4%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-right" width="7%">Итого</td>
            <td width="14%" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="6%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">коп.</td>
            <td width="2%" class="text-right" style="padding: 0">"</td>
            <td class="font-size-7 text-center" width="5%" style="border-bottom: 1px solid black"><?= $createdAtDay; ?></td>
            <td width="2%" style="padding: 0">"</td>
            <td class="font-size-7 text-center" width="14%" style="border-bottom: 1px solid black"><?= $createdAtMonth; ?></td>
            <td class="font-size-7 text-right" width="4%">20</td>
            <td class="font-size-7 text-left" width="5%" style="border-bottom: 1px solid black"><?= $createdAtYear; ?></td>
            <td class="font-size-7 text-left" width="6%" style="padding: 0;">г.</td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-right" width="7%"></td>
            <td width="14%"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="6%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="2%" class="text-right" style="padding: 0"></td>
            <td class="font-size-7 text-center" width="5%"></td>
            <td width="2%" style="padding: 0"></td>
            <td class="font-size-7 text-center" width="14%"></td>
            <td class="font-size-7 text-right" width="4%"></td>
            <td class="font-size-7 text-left" width="5%"></td>
            <td class="font-size-7 text-left" width="6%" style="padding: 0;"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-left: 3px solid black;border-right: 3px solid black;">Кассир</td>
            <td class="font-size-6" colspan="2">С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка </td>
        </tr>
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6" width="27%">ознакомлен и согласен.</td>
            <td class="font-size-8-bold" width="46%">Подпись плательщика</td>
        </tr>
    </table>

    <table class="table no-border" style="margin: 0">
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-top: 3px solid black;border-left: 3px solid black;border-right: 3px solid black;">Извещение</td>
            <td class="text-left" width="28%" style="border-top: 3px solid black;"></td>
            <td class="text-right font-size-8-bold-italic" width="45%" style="border-top: 3px solid black;">Форма № ПД-4</td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" colspan="2" style="border-bottom: 1px solid black"><?= $contractor->getTitle(true); ?></td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l" colspan="2">(наименование получателя платежа)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" width="28%" style="border-bottom: 1px solid black"><?= $contractor->ITN; ?>&nbsp;</td>
            <td width="3%"></td>
            <td class="font-size-8 text-left" width="42%" style="border-bottom: 1px solid black"><?= $contractor->current_account; ?>&nbsp;</td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l">(ИНН получателя платежа)</td>
            <td></td>
            <td class="font-size-6 text-center ver-top m-l">(номер счета получателя платежа)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td width="42%" style="border-bottom: 1px solid black"><?= $contractor->bank_name; ?>&nbsp;</td>
            <td class="font-size-8 text-right" width="6%">БИК</td>
            <td width="25%" style="border-bottom: 1px solid black"><?= $contractor->BIC; ?></td>
        </tr>
        <tr>
            <td style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l">(наименование банка получателя платежа)</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="34%">Номер кор./сч. банка получателя платежа</td>
            <td width="39%" style="border-bottom: 1px solid black"><?= $contractor->corresp_account; ?></td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="34%"></td>
            <td width="39%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" width="47%" style="border-bottom: 1px solid black"><?= $paymentName; ?></td>
            <td width="3%"></td>
            <td class="font-size-8 text-left" width="23%" style="border-bottom: 1px solid black;">&nbsp;</td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-left" width="47%"></td>
            <td width="3%"></td>
            <td class="font-size-8 text-left" width="23%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6 text-center ver-top m-l" width="47%">(наименование платежа)</td>
            <td class="font-size-6 text-right ver-top m-l" width="26%">(номер лицевого счета (код) плательщика)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%">Ф.И.О. плательщика:</td>
            <td width="55%" style="border-bottom: 1px solid black"><?= $company->getChiefFio(); ?></td>
        </tr>
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%">Адрес плательщика:</td>
            <td class="font-size-8 text-left" width="55%" style="border-bottom: 1px solid black"></td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="18%"></td>
            <td class="font-size-8 text-left" width="55%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="13%" style="padding: 0">Сумма платежа:</td>
            <td width="8%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="5%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">коп.</td>
            <td class="font-size-8 text-center" width="20%" style="padding: 0">Сумма платы за услуги:</td>
            <td width="7%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="4%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0;" width="4%">коп.</td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-center" width="13%" style="padding: 0"></td>
            <td width="8%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="5%" class="font-size-7 text-right" ></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td class="font-size-8 text-center" width="20%" style="padding: 0"></td>
            <td width="7%" class="font-size-7 text-right" ></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="4%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0;" width="4%"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-right" width="7%">Итого</td>
            <td width="14%" style="border-bottom: 1px solid black"><?= $priceRub; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">руб.</td>
            <td width="6%" class="font-size-7 text-right" style="border-bottom: 1px solid black"><?= $priceCop; ?></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%">коп.</td>
            <td width="2%" class="text-right" style="padding: 0">"</td>
            <td class="font-size-7 text-center" width="5%" style="border-bottom: 1px solid black"><?= $createdAtDay; ?></td>
            <td width="2%" style="padding: 0">"</td>
            <td class="font-size-7 text-center" width="14%" style="border-bottom: 1px solid black"><?= $createdAtMonth; ?></td>
            <td class="font-size-7 text-right" width="4%">20</td>
            <td class="font-size-7 text-left" width="5%" style="border-bottom: 1px solid black"><?= $createdAtYear; ?></td>
            <td class="font-size-7 text-left" width="6%" style="padding: 0;">г.</td>
        </tr>
        <!--cap-->
        <tr>
            <td width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-8 text-right" width="7%"></td>
            <td width="14%"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="6%" class="font-size-7 text-right"></td>
            <td class="font-size-7 text-left" style="padding: 0" width="4%"></td>
            <td width="2%" class="text-right" style="padding: 0"></td>
            <td class="font-size-7 text-center" width="5%"></td>
            <td width="2%" style="padding: 0"></td>
            <td class="font-size-7 text-center" width="14%"></td>
            <td class="font-size-7 text-right" width="4%"></td>
            <td class="font-size-7 text-left" width="5%"></td>
            <td class="font-size-7 text-left" width="6%" style="padding: 0;"></td>
        </tr>
        <!--end cap-->
    </table>
    <table class="table no-border" style="margin: 0">
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-left: 3px solid black;border-right: 3px solid black;">Квитанция</td>
            <td class="font-size-6" colspan="2"></td>
        </tr>
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-left: 3px solid black;border-right: 3px solid black;"></td>
            <td class="font-size-6" colspan="2">С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка </td>
        </tr>
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-left: 3px solid black;border-right: 3px solid black;">Кассир</td>
            <td class="font-size-6" width="27%">ознакомлен и согласен.</td>
            <td class="font-size-8-bold" width="46%"></td>
        </tr>
        <tr>
            <td class="font-size-8-bold text-center" width="27%" style="border-left: 3px solid black;border-right: 3px solid black;border-bottom: 3px solid black;"></td>
            <td class="font-size-6" width="27%" style="border-bottom: 3px solid black;"></td>
            <td class="font-size-8-bold" width="46%" style="border-bottom: 3px solid black;">Подпись плательщика</td>
        </tr>
    </table>
    </div>
    <div class="line-cutting" style="margin: 3px 0 0 6px">
        <img src="/img/temp/cutting-line.jpg" alt="линия отреза">
    </div>
</div>
