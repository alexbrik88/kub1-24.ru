<style>
    .subscribe-tariff-box {
        padding: 5px;
    }
    .subscribe-tariff-box .bordered {
        border-bottom:2px solid #d7d7d7;
        padding: 20px 0;
    }
    .subscribe-tariff-box .bordered:first-child {
        padding-top: 0;
    }
    .subscribe-tariff-box .bordered:last-child {
        border: none;
        padding-bottom: 0;
    }
    .subscribe-tariff-box .tariff-icon-wrap {
        float:left;
        padding-right: 20px;
    }
    .subscribe-tariff-box .tariff-icon {
        display: flex;
        width: 44px;
        height: 44px;
        border-radius: 8px;
        text-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        background-color: #4679AE;

    }
    .subscribe-tariff-box .tariff-icon .svg-icon {
        display: block;
        font-size: 36px;
        color: #fff;
    }
    .subscribe-tariff-box .tariff-group-name {
        font-weight: bold;
        font-size: 18px;
        min-height: 45px;
        line-height: 1;
    }
    .subscribe-tariff-box .active-subscribe {
        font-size: 14px;
        padding-top:10px;
        min-height: 44px;
    }
    .tariff-rules {
        min-height: 58px;
    }
    .tariff-options {

    }
    .tariff-price {
        font-size:32px;
        padding-top: 16px;
    }
    .tariff-discount-wrap {
        margin-top: 20px;
        min-height: 40px;
    }
    .tariff-discount-proposition {
        color:#f3565d;
    }
    .tariff-description {
        overflow-y: hidden;
        height: 150px;
        position:relative;
    }
    .text-opacity {
        position:absolute;
        height: 100px;
        width:100%;
        bottom:0;
        left:0;
        z-index: 2;
        background-image: linear-gradient(to bottom, rgba(255,255,255,0), rgba(255,255,255,1));
    }
    .panel-promo-code {
        background-color: #eee;
        border: 1px solid  #d7d7d7;
        margin-bottom: 30px;
    }
    .tariff-read-more {
        color:#3175af;
        border-bottom: 1px dashed #3175af;
        width:70px;
        margin: 0 auto;
        cursor:pointer;
    }
    .pay-tariff-wrap {
        display: block;
        font-size: 14px;
        padding: 5px 0;
    }
    .pay-tariff-wrap:first-child {
        padding-top: 0;
    }
    .pay-tariff-wrap-title {
        display: block;
        font-size: 16px;
        padding: 10px 0;
    }
    .bold-italic {
        font-weight:bold;
        font-style:italic;
    }
    .bold-italic.red {
        color: #f3565d;
    }
    .tooltipster-kub .tooltipster-content {
        color: #fff;
        line-height: 18px;
        padding: 15px;
    }
    .affiliate-program-subscribe td, .affiliate-program-block-admin td {
        border-top: none !important;
        background-color: white;
        padding: 0 !important;
    }
    .affiliate-program-list {
        padding-left:0;
    }
    .affiliate-program-list > li {
        list-style-type: none;
    }
    .free-tariff-list {
        margin-bottom:0;
        padding-left: 5px;
        list-style: none;
    }
    .free-tariff-list li {
        padding-bottom: 10px;
    }
    .free-tariff-list li:last-child {
        padding-bottom: 0;
    }
    .free-tariff-list li:before {
        content:  "—";
        position: relative;
        left: -5px;
        padding-left: 0;
    }
    .table-payment-history th:first-child,
    .table-payment-history td:first-child {
        padding-left:0px!important;
    }
    #paymentform-companylist .checker {
        display: block!important;
        float:left!important;
    }
</style>