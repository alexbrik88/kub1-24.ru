<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 6:40 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\widgets\ActiveForm $form */
/* @var \frontend\modules\subscribe\forms\PaymentMethodForm $paymentMethodForm */
?>
Счёт будет выслан вам на e-mail

<?= $form->field($paymentMethodForm, 'createInvoice')->checkbox(); ?>