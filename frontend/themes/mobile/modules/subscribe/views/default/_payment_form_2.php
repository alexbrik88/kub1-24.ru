<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\company\CompanyType;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

$paymentMethodForm = new PaymentForm($company);
$paymentMethodForm->load(\Yii::$app->request->post());
$paymentMethodForm->tariffId = $tariffId;

$companyArray = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->isBlocked(false)
    ->orderBy([
        new Expression('IF({{company}}.[[id]] = :id, 0, 1) ASC'),
        new \yii\db\Expression("ISNULL({{companyType}}.[[name_short]])"),
        "companyType.name_short" => SORT_ASC,
        "company.name_short" => SORT_ASC,
    ])
    ->params([':id' => $company->id])
    ->all();

$companyTariffDiscounts = [];
$companyList = [];
foreach ($companyArray as $company) {
    $companyList[$company->id] = $company->shortName;
    $companyTariffDiscounts[$company->id] = [];
    foreach ($tariffArray as $tariff) {
        if ($tariff->group->activeByCompany($company)) {
            $companyTariffDiscounts[$company->id][$tariff->id] = $discount = $company->getDiscount($tariff->id);
            if ($tariff->id == $tariffId && $discount) {
                $companyList[$company->id] .= " <span style=\"font-weight: bold; color: red;\">Скидка -{$discount}%</span>";
            }
        }
    }
}

$paymentTypeArray = [
    [
        'id' => PaymentType::TYPE_ONLINE,
        'title' => 'Онлайн оплата',
        'view' => 'payment_type/online',
    ],
    [
        'id' => PaymentType::TYPE_INVOICE,
        'title' => 'Выставить счёт',
        'view' => 'payment_type/take_invoice',
    ],
    [
        'id' => PaymentType::TYPE_REWARD,
        'title' => 'Использовать вознаграждение',
        'view' => 'payment_type/reward',
    ],
];

?>
<?php $pjax = Pjax::begin([
    'id' => 'subscribe-tariff-block-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'subscribe-tariff-form',
    'action' => ['payment'],
    'validationUrl' => ['validate'],
    'enableAjaxValidation' => true,
]); ?>

<?= Html::activeHiddenInput($paymentMethodForm, 'tariffId', [
    'id' => 'payment-method-tariff',
]); ?>
<div class="row">
    <?= $form->field($paymentMethodForm, 'companyList', [
        'template' => "{label}\n<br>\n{input}\n{hint}\n{error}",
        'options' => [
            'class' => 'col-12'
        ],
        'labelOptions' => [
            'class' => 'label mb-3',
        ],
    ])->checkboxList($companyList, [
        'class' => 'row',
        'item' => function ($index, $label, $name, $checked, $value) use ($companyTariffDiscounts) {
            $tarifDiscounts = ArrayHelper::getValue($companyTariffDiscounts, $value, []);

            return Html::tag('label', Html::checkbox($name, $checked, [
                    'value' => $value,
                    'class' => 'company-id-checker',
                    'style' => 'display:block; float:left',
                    'data' => [
                        'tariffs' => array_keys($tarifDiscounts),
                        'discounts' => $tarifDiscounts,
                    ],
                ]) . '<div style="display:block;margin-left:24px">'.$label.'</div>', [
                'class' => 'col-3',
            ]);
        },
    ]); ?>
</div>

<?php $form->end(); ?>

<?php $pjax->end(); ?>