<?php

/* @var $paymentForm frontend\modules\subscribe\forms\OnlinePaymentForm */

?>

<div style="display: none;">
<?= $this->render('/online-payment/pay', [
    'paymentForm' => $paymentForm,
]); ?>
</div>
<script type="text/javascript">
document.getElementById("robokassa-form").submit();
</script>