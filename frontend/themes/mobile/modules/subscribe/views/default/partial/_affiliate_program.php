<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.07.2017
 * Time: 18:33
 */

use common\components\ImageHelper;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\components\Icon;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var common\models\Company $company */
/* @var common\models\service\SubscribeTariff[] $tariffArray */

if (!isset($tariffArray)) {
    $model = new PaymentForm($company);
    $formData = $model->getFormData();
    $tariffArray = ArrayHelper::getValue($formData, 'tariffArray', []);
}

$isOpened = (isset($isOpenedAffiliateBlock));

$percent = Subscribe::REWARD_PERCENT;
?>

<div class="wrap">
    <div class="<?= ($isOpened) ? '' : 'collapsed' ?>" role="button" data-toggle="collapse" data-target="#affiliate_program" aria-expanded="false" aria-controls="affiliate_program">
        <div class="row">
            <div class="column">
                <h4 style="line-height:1; margin-bottom:0;">Партнерская программа - «Зарабатывай вместе с КУБом»</h4>
            </div>
            <div class="column ml-auto">
                <?= Icon::get('shevron', ['class' => 'svg-icon-collapse text_size_14 text-grey-light']) ?>
            </div>
        </div>
    </div>
    <div class="collapse <?= ($isOpened) ? 'show' : '' ?>" id="affiliate_program">
        <table role="grid" class="affiliate-program-subscribe">
            <tbody>
            <tr>
                <td width="60%" style="vertical-align: top; padding-right:50px;">
                    <br/>
                    <ul class="affiliate-program-list">
                        <li style="margin-bottom: 20px;">
                            Вы можете заработать, рекомендуя сервис КУБ.
                            Ваш заработок составит <?= $percent ?>%
                            от сумм оплаченных пользователями,
                            пришедшими по вашей рекомендации.
                            Заработанные деньги вы сможете получить на свой расчетный счет
                            или использовать для оплаты сервиса КУБ.
                        </li>
                        <?php foreach ($tariffArray as $tariff) : ?>
                            <?php if ($tariff->tariff_group_id == SubscribeTariffGroup::STANDART) : ?>
                                <?php
                                $sum = round($tariff->price * $percent / 100);
                                $period = Yii::$app->i18n->format(
                                    '{n, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяцев}}',
                                    ['n' => $tariff->duration_month],
                                    'ru_RU'
                                );
                                ?>
                                <li>
                                    Вы получите <?= $sum ?> руб. с каждой оплаты за <?= $period ?>.
                                </li>
                            <?php endif ?>
                        <?php endforeach; ?>
                        <li style="font-weight: bold;padding-top: 20px;">
                            Как начать зарабатывать?
                        </li>
                        <li>
                            Нажмите на кнопку ниже и появится ссылка.
                            Эта ссылка уникальная и привязана к вам.
                            Скопируйте эту ссылку и отправьте партнерам,
                            контрагентам и знакомым предпринимателям,
                            чтобы они перешли по ней на сервис КУБ и зарегистрировались.
                            Когда они начнут платить за КУБ, вы начнете зарабатывать.
                        </li>
                    </ul>
                </td>
                <td style="vertical-align: top;">
                    <br/>
                    <ul class="affiliate-program-list">
                        <li style="font-weight: bold;">
                            Ваша партнерская ссылка:
                        </li>
                        <li style="white-space: initial;">
                            <?php if ($company->affiliate_link): ?>
                                <?= Html::a(Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link,
                                    Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link, [
                                        'target' => '_blank',
                                        'class' => 'affiliate_invite',
                                    ]); ?>
                                <?= ImageHelper::getThumb('img/copy.png', [20, 20], [
                                    'class' => 'copy-affiliate-link',
                                    'title' => 'Скопировать ссылку',
                                ]); ?>
                            <?php else: ?>
                                <div class="form-actions" style="margin-top:20px;">
                                    <div class="row action-buttons">
                                        <div class="col-6">
                                            <?= Html::a('Получить ссылку', 'javascript:;', [
                                                'class' => 'button-clr button-regular button-regular_red w-100 generate-affiliate-link',
                                                'data-url' => Url::to(['/company/generate-affiliate-link', 'page' => 'subscribe']),
                                            ]); ?>
                                        </div>
                                        <div class="col-6"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </li>
                        <li style="font-weight: bold;padding-top: 20px;">
                            Ваше
                            вознаграждение: <?= $company->affiliate_sum; ?>
                            руб.
                        </li>
                        <li>
                            Привлечено
                            пользователей: <?= $company->getInvitedCompanies()->count(); ?>
                        </li>
                        <li>
                            Пользователей
                            оплатило: <?= $company->getPayedInvitedCompaniesCount(); ?>
                        </li>
                        <li style="padding-top: 20px;">
                            <div class="form-actions">
                                <div class="row action-buttons">
                                    <div class="col-6">
                                        <?= Html::a('Выставить счет', null, [
                                            'class' => 'button-clr button-regular button-regular_red w-100',
                                            'disabled' => $company->affiliate_link ? false : true,
                                            'data-toggle' =>  $company->affiliate_link ? 'modal' : null,
                                            'data-target' =>  $company->affiliate_link ? '#bill-invoice-remuneration' : null,
                                            'style' => 'color:#fff'
                                        ]); ?>
                                    </div>
                                    <div class="col-6"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
