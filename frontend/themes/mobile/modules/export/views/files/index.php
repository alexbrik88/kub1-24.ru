<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */

use common\models\product\ProductSearch;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Выгрузить документы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stop-zone-for-fixed-elems">
    <div class="wrap pt-2 pb-1 pl-4 pr-3">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="col-9">
                    <h4 class="mb-2"><?= $this->title ?></h4>
                </div>
                <div class="col-3 pl-1 pr-0" style="margin-top:-9px">
                    <?= RangeButtonWidget::widget(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap mt-2">
        <div class="row">
            <?= $this->render('_viewPartials/_form', [
                'exportFilesModel' => $exportFilesModel,
            ]); ?>
        </div>
    </div>

    <div class="wrap wrap_count mt-2">
        <div class="col-12">
            <div class="mt-1 mb-2"><strong>История выгрузок</strong></div>

            <?php Pjax::begin([
                'id' => 'export-list',
            ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
