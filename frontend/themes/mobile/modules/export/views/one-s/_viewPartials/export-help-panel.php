<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.06.2018
 * Time: 7:40
 */

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="invoice-wrap" data-id="invoice">
    <div class="invoice-wrap-in invoice-wrap-scroll">
        <button class="invoice-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="invoice">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>

        <div class="main-block">
            <p class="bold" style="font-size:19px;">
                Выгрузка из КУБ в 1С
            </p>

            <p class="bold">
                Как сформировать выгрузку в КУБе?
            </p>

            <p>
                <strong>1</strong>. В окне «Параметры выгрузки» выберите период, за который хотите выгрузить данные в 1С.
            </p>
            <p>
                <img style="width:247px" src="/img/documents/kub/export_1c_1.png"/>
            </p>
            <p class="odds-line"></p>

            <p>
                <strong>2</strong>. Выберите, что нужно вам выгрузить - проставьте галочки.
            </p>
            <p>
                <img style="width:100%" src="/img/documents/kub/export_1c_2.png"/>
            </p>
            <p class="odds-line"></p>

            <p>
                <strong>3</strong>. Нажмите по кнопке:
            </p>
            <p>
                <img style="width:162px" src="/img/documents/kub/export_1c_3.png"/>
            </p>
            <p class="odds-line"></p>

            <p>
                <strong>4</strong>. После того как сформируется файл, нажмите по кнопке:
            </p>
            <p>
                <img style="width:116px" src="/img/documents/kub/export_1c_4.png"/>
            </p>
            <p class="odds-line"></p>

            <p>
                <strong>5</strong>. Выгрузится ДВА файла - один с данными в формате xml, второй с обработкой, которая загрузит данные в 1С.
            </p>
            <p>
                <img style="width:100%" src="/img/documents/kub/export_1c_5.png"/>
            </p>
            <p class="odds-line"></p>


            <p class="bold">
                Как загрузить в 1С?
            </p>

            <p>
                <strong>1</strong>. Откройте 1С.
            </p>

            <p>
                <strong>2</strong>. Перетяните в нее файл «ибЗагрузкаВыгрузкаXml». <br/> <strong>ВАЖНО</strong>: У вашего профиля в 1С должны быть права на работу с обработкой.
            </p>

            <p>
                <strong>3</strong>. В появившемся окне нажмите по вкладке «Загрузка выгрузка xml» затем нажмите по кнопке «Загрузить ХМЛ».
            </p>
            <p>
                <img style="width:100%; max-width: 548px;" src="/img/documents/kub/export_1c_6.png"/>
            </p>
            <p class="odds-line"></p>

            <p>
                <strong>4</strong>. Укажите файл ХМЛ, который вы ранее скачали из КУБа.
            </p>

            <p>
                <strong>5</strong>. После того как информация загрузиться, появиться информация по загруженным элементам.
            </p>
            <p>
                <img style="width:100%; max-width: 506px;" src="/img/documents/kub/export_1c_7.png"/>
            </p>
            <p class="odds-line"></p>


            <p class="bold" style="font-weight: bold; color: red;">ВАЖНО:</p>
            <p><strong>1</strong>. Услуги и Товары добавляются в справочник в папки «Услуги» и «Товары» соответственно.</p>
            <p><strong>2</strong>. Счета учета:</p>
            <div style="margin-left: 7px">
                <p>- Счет учета: 41.01</p>
                <p>- Счет дохода: 90.01.1</p>
                <p>- Номенклатурная группа: Основная номенклатурная группа</p>
                <p>- Счет расходов: 90.02.1</p>
                <p>- Счет НДС: 90.03</p>
            </div>
            <br/>
            <p>
                <strong>Остались вопросы?</strong><br/>
                <strong>Напишите нам: support@kub-24.ru</strong>
            </p>
            <br/>
        </div>

    </div>
</div>

<?php $this->registerJs('
$(document).ready(function(){
    $(".odds-panel-trigger").click(function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    $("#visible-right-menu").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $(this).hide();
        $("html").removeAttr("style");
        $(".odds-panel-trigger").pulsate({
           color: "#f00",
           reach: 20,
           repeat: 3
        });
    });
    $(".odds-panel .side-panel-close").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
        $(".odds-panel-trigger").pulsate({
           color: "#f00",
           reach: 20,
           repeat: 3
        });
    });

    $(".odds-panel-trigger").click();
});
'); ?>