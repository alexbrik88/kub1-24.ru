<?php
use common\components\grid\DropDownDataColumn;
use common\models\employee\Employee;
use frontend\modules\export\models\export\export;

?>
<div class="table-container" style="">
    <div class="dataTables_wrapper dataTables_extended_wrapper">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],
            //'options' => [
            //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            //],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата выгрузки',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        return $data->getCreatedAt();
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'label' => 'Сотрудник',
                    'filter' => $searchModel->getEmployeeByCompanyArray(\Yii::$app->user->identity->company->id),
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        return $data->employee instanceof Employee ? $data->employee->getFio(true) : '-';
                    },
                    's2width' => '200px'
                ],
                [
                    'attribute' => 'period_start_date',
                    'label' => 'Период выгрузки',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        return implode(' - ', [$data->getPeriodStartDate(), $data->getPeriodEndDate()]);
                    },
                ],
                [
                    'attribute' => 'only_new',
                    'label' => 'Только новые',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        return $data->only_new == Export::ONLY_NEW ? 'Да' : 'Нет';
                    }
                ],
                [
                    'attribute' => 'io_type_out',
                    'label' => 'Исходящие документы',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        if ($data->io_type_out == true) {
                            $out = 'Все';
                        } else if ($list = Export::getExportDocumentTypeList($data->io_type_out_items)) {
                            $out = implode('<br />', $list);
                        } else {
                            $out = '-';
                        }

                        return $out;
                    }
                ],
                [
                    'attribute' => 'io_type_in',
                    'label' => 'Входящие документы',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        if ($data->io_type_in == true) {
                            $out = 'Все';
                        } else if ($list = Export::getExportDocumentTypeList($data->io_type_in_items)) {
                            $out = implode('<br />', $list);
                        } else {
                            $out = '-';
                        }

                        return $out;
                    }
                ],
                [
                    'attribute' => 'product_and_service',
                    'label' => 'Товары и услуги',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        return $data->product_and_service == true ? 'Да' : 'Нет';
                    }
                ],
                [
                    'attribute' => 'contractor',
                    'label' => 'Контрагенты',
                    'format' => 'raw',
                    'value' => function (Export $data) {
                        if ((bool)$data->contractor == true) {
                            $out = 'Все';
                        } else if ($list = Export::getExportContractorTypes($data->contractor_items)) {
                            $out = implode('<br />', $list);
                        } else {
                            $out = '-';
                        }

                        return $out;
                    }
                ]
            ],
        ]); ?>
    </div>
</div>