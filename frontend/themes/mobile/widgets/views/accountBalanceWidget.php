<?php

use common\models\bank\BankingParams;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $accounts common\models\company\CheckingAccountant[] */
/* @var $balance array */
/* @var $balanceTotal integer */

$p = Banking::currentRouteEncode();
$banking = [];
$integratedBanking = [];
foreach ($balance as $data) {
    if ($class = Banking::classByBik($data['bik'])) {
        $accountId = ArrayHelper::getValue($accounts, "{$data['rs']}.id");
        $params = [
            'class' => $class,
            'alias' => $class::ALIAS,
            'accountId' => $accountId,
        ];
        $banking[] = $params;
        $hasUpload = CashBankStatementUpload::find()->where([
            'rs' => $data['rs'],
            'company_id' => $company->id,
            'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
        ])->orderBy(['created_at' => SORT_DESC])->exists();
        if ($hasUpload || BankingParams::getValue($company, $class::ALIAS, 'access_token') !== null) {
            $integratedBanking[] = $params;
        }
    }
}
if ($integratedBanking) {
    $data = reset($integratedBanking);
    $url = [
        "/cash/banking/{$data['alias']}/default/index",
        'account_id' => $data['accountId'],
        'start_load_urgently' => true,
        'p' => $p,
    ];
} elseif ($banking) {
    $data = reset($banking);
    $url = [
        "/cash/banking/{$data['alias']}/default/index",
        'account_id' => $data['accountId'],
        'p' => $p,
    ];
} else {
    $url = [
        "/cash/banking/default/index",
        'p' => $p,
    ];
}
?>

<style type="text/css">
.banking-balance-container {
    position: relative;
    display: inline-block;
}
.banking-balance-container .refresh-banking-statements {
    font-size: 20px;
    margin-right: 5px;
    padding: 0;
}
.banking-balance-container .banking-balance-view {
    height: 42px;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 3px;
}
.banking-balance-container .banking-balance-view .page-header-date b {
    margin: 0;
    font-size: 14px;
    vertical-align: baseline;
}
.banking-balance-container .by-rs-toggle {
    position: absolute;
    width: 25px;
    right: 0;
    top: -35px;
    font-size: 24px;
    cursor: pointer;
}
.banking-balance-container .dropdown .dropdown-menu a {
    color: #555;
    display: flex; font-size: 12px;
    justify-content: space-between;
}
#dropdownBanking {
    cursor: pointer;
}
</style>

<div class="banking-balance-container">
    <div class="banking-balance-view">
        <div style="align-self: center; line-height: 1;">
            <?= Html::a(Icon::get('repeat'), $url, [
                'class' => 'refresh-banking-statements banking-module-open-link',
            ]) ?>
        </div>
        <div class="dropdown">
            <div id="dropdownBanking" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div style="font-size: 16px; font-weight: bold; text-align: right;">
                    <?= Yii::$app->formatter->asDecimal(bcdiv($balanceTotal, 100, 2), 2) ?> ₽
                </div>
                <div style="line-height: 20px; text-align: right;">
                    на рублевых счетах
                </div>
            </div>
            <div class="dropdown-popup dropdown-popup_notifications dropdown-menu" aria-labelledby="dropdownBanking">
                <div class="dropdown-popup-in">
                    <ul class="dropdown-list list-clr">
                        <?php foreach ($balance as $data) : ?>
                            <li>
                                <a class="dropdown-list-link" href="<?= Url::to(['/cash/bank/index', 'rs' => $data['rs']]) ?>">
                                    <div style="padding-right: 10px;">
                                        <?= $data['bank_name'] ?>
                                    </div>
                                    <div style="text-align: right; white-space: nowrap;">
                                        <?= Yii::$app->formatter->asDecimal(bcdiv($data['balance'], 100, 2), 2) ?>
                                    </div>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?= BankingModalWidget::widget() ?>