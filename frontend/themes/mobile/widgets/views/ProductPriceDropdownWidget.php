<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\Html;

/* @var \yii\web\View $this */
/* @var string $inputId */
/* @var integer $productionType */
?>

<?php if (Yii::$app->user->can(UserRole::ROLE_PRODUCT_ADMIN)) : ?>

<div class="add-product-block" style="display: none;">
    <span class="add-new-product-group-form"
        style="display: inline-block; position: relative; width: 100%; padding: 15px 52px 15px 15px; border-top: 2px solid #e2e5eb; z-index: 999999">

        <button class="add-button button-clr new-product-group-submit" type="button"
                style="position: absolute; right: 12px; top: 24px; padding: 0 7px;">
            <svg class="add-button-icon svg-icon" style="font-size: 22px;">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
            </svg>
        </button>

        <?= Html::textInput('title', null, [
            'class' => 'form-control new_product_price_input',
            'maxlength' => 45,
            'placeholder' => 'Добавить новую цену',
            'style' => 'display: inline-block; width: 100%;',
        ]); ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().children('.add-new-product-group-form').length) {
            var $newProductForm = $('select#<?= $inputId; ?>').siblings('.add-product-block').find('.add-new-product-group-form').clone();
            $newProductForm.find('.new-product-group-submit').attr('id', 'new-product-price-submit-<?= $inputId ?>');
            $newProductForm.clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
        $('#add-new').addClass('no-overflow'); // fix bug - otherwise input not focused
    });
    $('#<?= $inputId ?>').on('select2:close', function (evt) {
        $('#add-new').removeClass('no-overflow'); // fix bug - otherwise input not focused
    });
    $(document).on('click', '#new-product-price-submit-<?= $inputId ?>', function() {
        var input = $(this).parent().children('input');
        $.post('/product/add-price', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                input.val('');
                var newOption = new Option(data.itemName, data.itemId, false, true);
                newOption.dataset.editable = 1;
                newOption.dataset.deletable = 1;
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
            }
        });
    });
</script>

<div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal"  role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <h4 class="text-center mb-4" style="margin-bottom:30px;">
                        Вы уверены, что хотите удалить цену
                        <br>
                        "<span class="item-name"></span>"?
                    </h4>
                    <div class="text-center">
                        <button class="button-clr button-regular button-hover-transparent button-width-medium mr-2 js-item-delete" type="button">Да</button>
                        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1 js-item-delete-cancel" type="button">Нет</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.js-item-delete, .js-item-delete-cancel').on('click', function() {
        $(this).closest('.modal').modal('hide');
    });
</script>

<?php endif ?>
