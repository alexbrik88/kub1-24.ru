<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $modalId string */
/** @var $modalOptions array */
/** @var $text string */

$options = array_merge([
    'closeButton' => false,
    'toggleButton' => false,
], $modalOptions, [
    'id' => $modalId,
]);

?>

<?php Modal::begin($options); ?>

    <h4 class="text-center">
        <?= $text ?>
    </h4>
    <h4 class="text-center">
        Перейти к оплате?
    </h4>
    <div class="text-center" style="margin-top: 20px;">
        <?= Html::a('ДА', ['/subscribe/default/index'], [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
        ]) ?>
        <?= Html::button('НЕТ', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php Modal::end();
