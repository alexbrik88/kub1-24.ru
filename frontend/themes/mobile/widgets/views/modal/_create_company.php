<?php

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\TaxationType;
use frontend\models\RegistrationForm;
use frontend\rbac\permissions;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var Company $newCompany */
/* @var $companyTaxation common\models\company\CompanyTaxationType */

$config = [
    /*'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",*/
];

$this->registerJs('
    $(document).on("submit", "#create-company-modal", function() {
        $("#create-company-modal-loading").show();
    })
');

$typeArray = CompanyType::find()->andWhere(['id' => RegistrationForm::$typeIds])->all();
?>

<?php Modal::begin([
    'id' => 'create-company',
    'title' => 'Добавление новой компании',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ]
]) ?>
    <?php if (Yii::$app->user->can(permissions\Company::CREATE)) : ?>
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/company/create']),
            'options' => [
                'class' => 'form-horizontal form-checking-accountant',
                'id' => 'create-company-modal',
            ],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        ]); ?>

        <div>
            <?= $form->field($newCompany, 'company_type_id', array_merge($config, [
                'options' => [
                    'class' => 'form-group company-type-chooser',
                ],
            ]))->radioList(ArrayHelper::map($typeArray, 'id', 'name_short'), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'pr-3 no-border',
                    ],
                ]);
            },
        ])->label('Форма собственности', ['class' => 'label']); ?>
            <div class="company-type-block-non-ip">
            </div>
            <div class="company-type-block-ip">
            </div>
        </div>
        <div class="taxation-type field-registrationform-taxationtypeosno required">
            <label class="label">Система налогобложения</label>

            <div class="fields required">
                <?= $form->field($companyTaxation, 'osno', [
                    'options' => [
                        'class' => 'pr-3',
                        'style' => 'display: inline-block;',
                    ],
                    'checkOptions' => [
                        'class' => 'form-group',
                        'labelOptions' => ['class' => ''],
                    ],
                ])->checkbox()->error(false); ?>
                <?= $form->field($companyTaxation, 'usn', [
                    'options' => [
                        'class' => 'pr-3',
                        'style' => 'display: inline-block;',
                    ],
                    'checkOptions' => [
                        'class' => 'form-group',
                        'labelOptions' => ['class' => ''],
                    ],
                ])->checkbox(); ?>
                <?= $form->field($companyTaxation, 'envd', [
                    'options' => [
                        'class' => 'pr-3',
                        'style' => 'display: inline-block;',
                    ],
                    'checkOptions' => [
                        'class' => 'form-group',
                        'labelOptions' => ['class' => ''],
                    ],
                ])->checkbox(); ?>
                <?= $form->field($companyTaxation, 'psn', [
                    'options' => [
                        'class' => 'pr-3',
                        'style' => 'display: inline-block;',
                    ],
                    'checkOptions' => [
                        'class' => 'form-group',
                        'labelOptions' => ['class' => ''],
                    ],
                ])->checkbox([
                    'disabled' => true,
                ]); ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent back',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
        <?php $form->end(); ?>

        <div id="create-company-modal-loading"
            class="ajax-loader-wrapper"
            style="display: none; position: absolute; top: 0; bottom: 0; left: 0; right: 0; background-color: rgba(255,255,255,0.5);">
            <img src="/img/loading.gif">
        </div>
    <?php else : ?>
        <div>
            Ваш аккаунт имеет компании на «Пробном» или «Бесплатном» тарифе.
        </div>
        <div>
            Чтобы добавить еще одну компанию, нужно
            <?= Html::a('оплатить сервис', '/subscribe/default/index') ?>.
        </div>
    <?php endif ?>
<?php Modal::end() ?>
