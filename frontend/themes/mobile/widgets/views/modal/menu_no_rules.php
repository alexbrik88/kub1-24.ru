<?php

use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;

?>
<?php Modal::begin([
    'id' => 'menu-no-rules',
    'closeButton' => false,
]); ?>
    <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
        'aria-label' => 'Close',
    ]) ?>

    <div class="text">
        У вас недостаточно прав для использования данного раздела
    </div>
    <div class="text" style="margin-top: 20px;">
        Обратитесь к вашему Руководителю для добавления дополнительных возможностей сервиса КУБ
    </div>
    <div style="margin-top: 30px; text-align: center;">
        <button type="button" class="button-regular button-regular_red" data-dismiss="modal" style="width: 65px;">OK</button>
    </div>
<?php Modal::end(); ?>
