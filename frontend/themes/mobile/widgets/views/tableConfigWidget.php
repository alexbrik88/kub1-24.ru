<?php

use yii\helpers\Html;

/* @var $model yii\db\ActiveRecord */
/* @var $items array */
/* @var $sortingItems array */
/* @var $showItems array */
/* @var $customPriceItems array */
/* @var $buttonClass string */

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];
?>
<div class="dropdown dropdown-settings d-inline-block" title="Настройка таблицы">
    <?= Html::button('<svg class="svg-icon"><use xlink:href="'.$baseUrl.'/images/svg-sprite/svgSprite.svg#cog"></use></svg>', [
        'class' => ($buttonClass) ?: 'button-list button-hover-transparent button-clr mr-2',
        'data-toggle' => 'dropdown',
        'aria-haspopup' => 'true',
        'aria-expanded' => 'false',
    ]) ?>
    <ul id="config_items_box" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
        <li>
            <label style="font-weight: bold;">Настройка таблицы</label>
        </li>
        <li class="bold" style="border-top: 1px solid #ddd;">
            <label style="font-weight: bold;">Столбцы</label>
        </li>
        <?php foreach ($items as $item) : ?>
            <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                <li>
                    <?= Html::activeCheckbox($model, $item['attribute'], [
                        'data-target' => 'col_' . $item['attribute'],
                        'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                    ]); ?>
                </li>
            <?php endif ?>
        <?php endforeach ?>
        <?php foreach ($customPriceItems as $item): ?>
            <?php $name = 'custom_price_'.$item['id']; ?>
            <li>
                <?= Html::checkbox($name, $item['show_column'], [
                    'id' => 'config-' . $name,
                    'data-target' => 'col_' . $name,
                    'data-url' => $item['url'],
                    'label' => $item['label'],
                ]); ?>
            </li>
        <?php endforeach; ?>
        <?php if (!empty($sortingItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                Сортировка по умолчанию <br>
                по столбцу
            </li>
        <?php endif; ?>
        <?php foreach ($sortingItems as $sortingItem): ?>
            <?php if (!empty($sortingItem['attribute']) && (!array_key_exists('visible', $sortingItem) || $sortingItem['visible'])) : ?>
                <li style="white-space: nowrap;">
                    <?= Html::radio($sortingItem['attribute'], $sortingItem['checked'], [
                        'class' => 'sorting-table-config-item',
                        'id' => $sortingItem['attribute'],
                        'data-target' => 'col_' . $sortingItem['attribute'],
                        'label' => isset($sortingItem['label']) ? $sortingItem['label'] : $sortingItem['attribute'],
                    ]); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if (!empty($showItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                Показывать
            </li>
            <?php foreach ($showItems as $item) : ?>
                <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                    <li>
                        <?= Html::activeCheckbox($model, $item['attribute'], [
                            //'data-target' => 'row_' . $item['attribute'],
                            'data-refresh-page' => '1',
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        ]); ?>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif; ?>
    </ul>
</div>
