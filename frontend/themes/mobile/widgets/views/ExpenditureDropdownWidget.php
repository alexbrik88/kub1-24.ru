<?php

/* @var $income boolean */
/* @var $inputId string */

$itemType = $income ? 'inc' : 'exp';
 ?>

<div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal"  role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <h4 class="text-center mb-4" style="margin-bottom:30px;">
                        Вы уверены, что хотите удалить
                        <?php if ($income) : ?>
                            тип прихода
                        <?php else : ?>
                            статью расхода
                        <?php endif; ?>
                        <br>
                        "<span class="item-name"></span>"?
                    </h4>
                    <div class="text-center">
                        <button class="button-clr button-regular button-hover-transparent button-width-medium mr-2 js-item-delete" type="button">Да</button>
                        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1 js-item-delete-cancel" type="button">Нет</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>