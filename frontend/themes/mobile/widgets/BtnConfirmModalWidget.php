<?php

namespace frontend\themes\mobile\widgets;

use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;

/**
 * Class ConfirmModalWidget
 * @package frontend\widgets
 *
 * @example usage:
 * With target button :
 * <?= \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
 *      'toggleButton' => [
 *          'label' => 'Удалить',
 *          'class' => 'button-regular button-regular_red',
 *      ],
 *      'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить этот элемент?',
 * ]); ?>
 *
 * Without target button:
 * widget:
 * <?= \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
 *      'options' => [
 *          'id' => 'delete-confirm',
 *      ],
 *      'toggleButton' => false,
 *      'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить исходящий счёт?',
 * ]);
 * button:
 * <button type="button" class="button-regular button-regular_red" data-toggle="modal" href="#delete-confirm">Удалить</button>
 *
 */
class BtnConfirmModalWidget extends Widget
{
    /**
     * @var string
     */
    public $message = 'Are you sure?';
    /**
     * @var string
     */
    public $confirmUrl = '#';
    /**
     * @var array
     */
    public $confirmParams = [];

    /**
     * @var
     */
    public $options;
    /**
     * @var string the modal size. Can be [[SIZE_LARGE]] or [[SIZE_SMALL]], or empty for default.
     */
    public $size;
    /**
     * @var
     */
    public $toggleButton;

    /**
     * @var string
     */
    public $theme = 'red'; // red||gray

    /**
     * @var integer
     */
    public $modelId;

    /**
     *
     */
    public function init()
    {
        $this->theme = 'gray'; // ALWAYS

        Html::addCssClass($this->options, 'confirm-modal modal');
        Html::addCssClass($this->options, 'fade');

        Modal::begin([
            'id' => $this->id,
            'options' => $this->options,
            'size' => $this->size,
            'closeButton' => false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'label' => 'Confirm'
            ],
        ]); ?>

        <?php /*
        <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
            'class' => 'modal-close close',
            'data-dismiss' => 'modal',
            'aria-label' => 'Close',
        ]) ?>*/ ?>

        <?php if ($this->theme == 'red'): ?>
            <div style="margin-bottom: 30px;">
                <?= $this->message; ?>
            </div>
            <div class="form-actions row">
                <div class="col">
                    <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn-confirm-yes button-regular button-regular_red pull-right mt-ladda-btn ladda-button',
                        'style' => 'width: 50px;',
                        'data-style' => 'expand-right',
                        'data-url' => $this->confirmUrl,
                        'data-model_id' => $this->modelId,
                        'data' => [
                            'type' => 'post',
                            'params' => array_merge($this->confirmParams, [
                                '_csrf' => Yii::$app->request->csrfToken,
                            ]),
                        ],
                    ]); ?>
                </div>
                <div class="col">
                    <?= Html::button('НЕТ', [
                        'class' => 'button-regular button-regular_red',
                        'style' => 'width: 50px;',
                        'data' => [
                            'dismiss' => 'modal',
                        ],
                    ]); ?>
                </div>
            </div>

        <?php else: ?>

            <h4 class="modal-title text-center mb-4"><?= $this->message; ?></h4>
            <div class="text-center">
                <?= Html::button('Да', [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => $this->confirmUrl,
                    'data-model_id' => $this->modelId,
                    'data' => [
                        'type' => 'post',
                        'params' => array_merge($this->confirmParams, [
                            '_csrf' => Yii::$app->request->csrfToken,
                        ]),
                    ],
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        <?php endif; ?>
        <?php Modal::end();
    }
}
