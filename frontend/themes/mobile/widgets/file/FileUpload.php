<?php

namespace frontend\themes\mobile\widgets\file;


use yii\base\InvalidConfigException;
use yii\base\Widget;

class FileUpload extends Widget
{
    /**
     * @var string
     */
    public $uploadUrl;
    /**
     * @var string
     */
    public $deleteUrl;
    /**
     * @var string
     */
    public $listUrl;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->uploadUrl === null) {
            throw new InvalidConfigException('Ссылка на загрузку файла не определена.');
        }
        if ($this->deleteUrl === null) {
            throw new InvalidConfigException('Ссылка на удаление файла не определена.');
        }
        if ($this->listUrl === null) {
            throw new InvalidConfigException('Ссылка на список файлов не определена.');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {

        return $this->render('fileUpload');
    }
}