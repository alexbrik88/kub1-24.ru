<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CashboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Кассы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <div style="margin-bottom: 15px;">
        <?= $this->render('_table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>

    <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить кассу', [
        'class' => 'btn yellow ajax-modal-btn',
        'data-title' => 'Добавить кассу',
        'data-url' => Url::to(['/cashbox/create']),
    ]); ?>
</div>
