<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-delete',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$tab = Yii::$app->request->get('tab');
$subTab = Yii::$app->request->get('sub_tab');
if (!in_array($subTab, ['requisites', 'comments', 'agent']))
    $subTab = false;

$isPhysicalPerson = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ?
        Url::to(['create', 'type' => $model->type]) :
        Url::to(['update', 'id' => $model->id,
            'tab' => Yii::$app->request->get('tab', 'info'),
            'sub_tab' => Yii::$app->request->get('sub_tab'),
        ]),
    'id' => 'update-contractor-form',
    'class' => 'form-horizontal',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

    <div class="form-body form-horizontal">

        <?= $form->errorSummary($model); ?>
        <?= Html::activeHiddenInput($model, 'type'); ?>
        <?= Html::hiddenInput(null, $model->isNewRecord, [
            'id' => 'contractor-is_new_record',
        ]); ?>

        <?= $this->render('_partial_main_info', [
            'model' => $model,
            'form' => $form,
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
        ]) ?>
        <div class="clearfix"></div>

        <div class="wrap">
            <div class="profile-form-tabs">
            <?= Tabs::widget([
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'headerOptions' => ['class' => 'nav-item'],
                'items' => array_merge([
                    [
                        'label' => $isPhysicalPerson ? 'Данные паспорта' : 'Реквизиты',
                        'linkOptions' => ['class' => 'nav-link' . (!$subTab || $subTab == 'requisites' ? ' active' : '')],
                        'content' => $this->render('_partial_details', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                    ],
                    [
                        'label' => 'Комментарии',
                        'linkOptions' => ['class' => 'nav-link' .  ($subTab == 'comments' ? ' active' : '')],
                        'content' => $this->render('_partial_comment', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                    ],
                ], (FALSE && $model->type == Contractor::TYPE_SELLER && !$model->isNewRecord) ?
                    [
                        [
                            'label' => 'Агент',
                            'linkOptions' => ['class' => 'nav-link' . ($subTab == 'agent' ? ' active' : '')],
                            'content' => $this->render('_partial_agent', [
                                'model' => $model,
                                'form' => $form,
                            ]),
                        ]
                    ] : [])
            ]); ?>
            </div>
        </div>

        <div class="clearfix"></div>
            <!-- LEGAL -->
            <div class="legal row" style="<?= ($isPhysicalPerson) ? 'display:none' : '' ?>">
                <?= $this->render('_partial_director', [
                    'model' => $model,
                    'form' => $form,
                    'textInputConfig' => $partialDirectorTextInputConfig,
                ]); ?>

                <?= $this->render('_partial_chief_accountant', [
                    'model' => $model,
                    'form' => $form,
                    'textInputConfig' => $partialDirectorTextInputConfig,
                ]); ?>

                <?= $this->render('_partial_contact', [
                    'model' => $model,
                    'form' => $form,
                    'textInputConfig' => $partialDirectorTextInputConfig,
                ]); ?>
            </div>
            <!-- PHYSICAL -->
            <div class="physical wrap" style="<?= (!$isPhysicalPerson) ? 'display:none' : '' ?>">
                <div class="row">
                    <div class="col-12">
                        <h4 class="mb-3 pb-3">Контактные данные</h4>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                                    'id' => 'physical-director_email',
                                    'maxlength' => true,
                                ]); ?>
                            </div>
                            <div class="col-6">
                                <?= $form->field($model, 'director_phone', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                                    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                    'options' => [
                                        'id' => 'physical-director_phone',
                                        'class' => 'form-control',
                                        'placeholder' => '+7(XXX) XXX-XX-XX',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <br>


    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'button-regular button-width button-regular_red button-clr mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]) ?>
            </div>
            <div class="column">
                <span style="padding: 0 6px;">
                    <?php if (!$model->isNewRecord
                        && (\Yii::$app->authManager
                            && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE))
                    ): ?>
                        <?php if ($model->getDeleteItemCondition() && !$model->hasCashFlows()): ?>
                            <?= \frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
                                'toggleButton' => [
                                    'label' => 'Удалить',
                                    'class' => 'button-regular button-width button-hover-content-red',
                                ],
                                'confirmUrl' => Url::to(['delete', 'type' => $model->type, 'id' => $model->id,]),
                                'message' => 'Вы уверены, что хотите удалить контрагента?',
                            ]) ?>
                        <?php else: ?>
                            <?= Html::button('Удалить', [
                                'class' => 'button-clr button-width button-regular button-hover-transparent tooltip2-delete',
                                'data-tooltip-content' => '#tooltip_not_delete',
                            ]); ?>
                            <div style="display:none">
                                <span id="tooltip_not_delete">
                                    <?php if (!$model->getDeleteItemCondition()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, у которого есть счета, удалить нельзя.
                                        <br>
                                    <?php endif; ?>
                                    <?php if ($model->hasCashFlows()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, по которому есть операции в разделе Деньги, удалить нельзя.
                                    <?php endif; ?>
                                </span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
                <span style="padding: 0 6px;">
                    <?= Html::a('Отменить', $model->isNewRecord ? Url::to(['index', 'type' => $model->type]) : Url::to(['view', 'type' => $model->type, 'id' => $model->id, 'tab' => $tab]), [
                        'class' => 'button-clr button-width button-regular button-hover-transparent undo-contractor',
                    ]); ?>
                </span>
            </div>
        </div>
    </div>


<?php ActiveForm::end(); ?>


    <script>
        function chiefAccountantIsNotDirector(attribute, value) {
            return !$('#chief_accountant_is_director_input').is(':checked');
        }

        function contactIsNotDirector(attribute, value) {
            return !$('#contact_is_director_input').is(':checked');
        }

        function contractorIsIp() {
            return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsNotIp() {
            return $('#contractor-companytypeid').val() != "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsPhysical() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
        }

        function contractorIsLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
        }

        function contractorIsForeignLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
        }
        function contractorIsAgent() {
            return $("#contractor_is_agent_input:checked").length;
        }
    </script>

<?php
$typeArray = json_encode(array_flip($model->getTypeArray()));
$this->registerJs(<<<JS
    var checkDirectorPostName = function() {
        if ($( "#contractor_face_type" ).val() != '1' && $('#contractor-companytypeid').val() != '1') {
            $('#contractor-director_post_name').prop('disabled', false);
            $('.field-contractor-director_post_name').show();
        } else {
            $('#contractor-director_post_name').prop('disabled', true);
            $('.field-contractor-director_post_name').hide();
        }
    }

    $(document).on("change", "#contractor-companytypeid", function () {
        if ($("#contractor-itn").val()) {
            $(this).closest("form").yiiActiveForm("validateAttribute", "contractor-itn");
        }
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
        checkDirectorPostName();
    });
    $(document).on("change", "#chief_accountant_is_director_input, #contact_is_director_input", function () {
        var wrap = $(this).closest(".wrap");
        wrap.find("input:text").prop("disabled", $(this).is(":checked"));
        if ($(this).is(":checked")) {
            wrap.find("input.fio_in_act").prop("checked", false).prop("disabled", true);
        } else {
            wrap.find("input.fio_in_act").prop("disabled", false);
        }
        wrap.find("input.fio_in_act").uniform("refresh");
    });
    $(document).on("change", ".fio_in_act", function() {
        if ($(this).prop("checked")) {
            $(".fio_in_act").not(this).prop("checked", false).uniform("refresh");
        }
    });
    $(document).on("input", "#contractor-bic", function(){
        if ($(this).val() === "") {
            console.log(0);
            $("#contractor-bank_name").val("");
            $("#contractor-bank_city").val("");
            $("#contractor-corresp_account").val("");
        }
    });
    var companyType = $typeArray;
    var directorEmail;
    var contractorIsNewRecord = $("#contractor-is_new_record").val();

    $('#contractor-itn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        beforeRender: function (container) {
            $(".field-contractor-itn p.exists-contractor").empty();
            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: this.value,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
        },
        onSelect: function(suggestion) {
            var companyTypeId = '-1';

            if ($('#contractor-itn').val() != suggestion.data.inn) {
                $('#legal-director_email').val('');
                $('#legal-director_phone').val('');
                $('#contractor-chief_accountant_email').val('');
                $('#contractor-chief_accountant_email').val('');
                $('#contractor-contact_email').val('');
                $('#contractor-contact_phone').val('');
            }
            $('#contractor-itn').val(suggestion.data.inn);
            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: suggestion.data.inn,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId).trigger('change');
            if (companyTypeId == 1) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
                $('.field-contractor-bin label').text('ОГРНИП');
            } else {
                $('.field-contractor-ppc').show();
                $('.field-contractor-bin label').text('ОГРН');
            }
            var address = '';
            if (suggestion.data.address.data !== null && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
    $(document).ready(function() {
        if($( "#contractor_face_type" ).val() == '1'){
            $('.legal').hide();
            $('.foreign_legal').hide();
            $('.physical').show();
            $('.field-contractor-taxation_system input').prop('disabled', true);
            $('#physical-director_phone').prop('disabled', false);
            $('#physical-director_email').prop('disabled', false);
            $('#legal-director_phone').prop('disabled', true);
            $('#legal-director_email').prop('disabled', true);
            $('.contractor-local', this.form).toggleClass('hidden', false);
            $('.contractor-foreign', this.form).toggleClass('hidden', true).find('input').prop('disabled', true);
        } else if ($( "#contractor_face_type" ).val() == '0'){
            $('.physical').hide();
            $('.foreign_legal').hide();
            $('.legal').show();
            $('#physical-director_phone').prop('disabled', true);
            $('#physical-director_email').prop('disabled', true);
            $('#legal-director_phone').prop('disabled', false);
            $('#legal-director_email').prop('disabled', false);
            $('.contractor-local', this.form).toggleClass('hidden', false);
            $('.contractor-foreign', this.form).toggleClass('hidden', true).find('input').prop('disabled', true);
        } else if ($( "#contractor_face_type" ).val() == '2'){
            $('.physical').hide();
            $('.legal').show();
            $('.foreign_legal').show();
            $('[id="contractor-itn"]').suggestions('disable');
            $('#contractor-director_email').removeAttr('disabled');
            $('#director-phone').removeAttr('disabled');
            $('#contractor-bank_name').removeAttr('disabled');
            $('#contractor-bank_city').removeAttr('disabled');
            $('#contractor-corresp_account').removeAttr('disabled');
            $('#physical-director_phone').prop('disabled', true);
            $('#physical-director_email').prop('disabled', true);
            $('#legal-director_phone').prop('disabled', false);
            $('#legal-director_email').prop('disabled', false);
            $('.contractor-local', this.form).toggleClass('hidden', true);
            $('.contractor-foreign', this.form).toggleClass('hidden', false).find('input').prop('disabled', false);
        }
        checkDirectorPostName();
    });

    $(document).on('change','#contractor_face_type', function() {
        switch (this.value) {
            case '0':
                $('.physical').hide();
                $('.legal').show();
                $('.not-foreign-legal').show();
                $('.foreign_legal').hide();
                $('.legal').addClass('required');
                $('[id="contractor-itn"]').suggestions('enable');
                $('#contractor-corresp_account').attr('disabled', 'disabled');
                $('.field-contractor-director_name').addClass('required');
                $('.field-contractor-chief_accountant_name').addClass('required');
                $('.field-contractor-contact_name').addClass('required');
                $('.legal').find('#contractor-itn').attr('placeholder','Автозаполнение по ИНН');
                $('.field-contractor-taxation_system input').prop('disabled', false);
                $('#physical-director_phone').prop('disabled', true).val('');
                $('#physical-director_email').prop('disabled', true).val('');
                $('#legal-director_phone').prop('disabled', false);
                $('#legal-director_email').prop('disabled', false);
                $('.contractor-local', this.form).toggleClass('hidden', false);
                $('.contractor-foreign', this.form).toggleClass('hidden', true).find('input').prop('disabled', true);
                break;
            case '1':
                $('.physical').show();
                $('.legal').hide();
                $('.foreign_legal').hide();
                $('.field-contractor-taxation_system input').prop('disabled', true);
                $('#physical-director_phone').prop('disabled', false);
                $('#physical-director_email').prop('disabled', false);
                $('#legal-director_phone').prop('disabled', true).val('');
                $('#legal-director_email').prop('disabled', true).val('');
                $('.contractor-local', this.form).toggleClass('hidden', false);
                $('.contractor-foreign', this.form).toggleClass('hidden', true).find('input').prop('disabled', true);
                break;
            case '2':
                $('.physical').hide();
                $('.legal').show();
                $('.foreign_legal').show();
                $('.not-foreign-legal').hide();
                $('.legal').removeClass('required');
                $('.physical').find('#contractor-director_email').empty();
                $('.legal').find('#contractor-bank_name').removeAttr('disabled');
                $('.legal').find('#contractor-bank_city').removeAttr('disabled');
                $('.legal').find('#contractor-corresp_account').removeAttr('disabled');

                $('.field-contractor-director_name').removeClass('required');
                $('.field-contractor-chief_accountant_name').removeClass('required');
                $('.field-contractor-contact_name').removeClass('required');
                $('[id="contractor-itn"]').suggestions('disable');
                $('.legal').find('#contractor-itn').attr('placeholder','');
                $('.field-contractor-taxation_system input').prop('disabled', false);
                $('#physical-director_phone').prop('disabled', true).val('');
                $('#physical-director_email').prop('disabled', true).val('');
                $('#legal-director_phone').prop('disabled', false);
                $('#legal-director_email').prop('disabled', false);
                $('.contractor-local', this.form).toggleClass('hidden', true);
                $('.contractor-foreign', this.form).toggleClass('hidden', false).find('input').prop('disabled', false);
        }
        checkDirectorPostName();
    });

    $('#contractor-name').on('input', function(){
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
JS
);
