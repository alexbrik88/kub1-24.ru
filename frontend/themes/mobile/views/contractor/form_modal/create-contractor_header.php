<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */
?>
<h4 class="modal-title"><?= $model->isNewRecord ? 'Добавить' : 'Редактировать' ?> <?= $model->type == Contractor::TYPE_SELLER ? 'поставщика' : 'покупателя'; ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>