<?php

use frontend\models\Documents;
use frontend\rbac\permissions\document\Invoice;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\AutoinvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны АвтоСчетов';
?>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
])
): ?>
<div class="row mb-2 pb-1">
    <div class="col-12" style="text-align: right">
        <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>АвтоСчет</span>', [
                'create-autoinvoice',
                'type' => Documents::IO_TYPE_OUT,
                'id' => $model->id,
            ], [
                'class' => 'button-regular button-regular_red button-width ml-auto',
            ]) ?>
        <?php else : ?>
            <button class="button-regular button-regular_red button-width ml-auto action-is-limited">
                <?= $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>АвтоСчет</span>' ?>
            </button>
        <?php endif ?>
    </div>
</div>
<?php endif; ?>

<div class="wrap">
    <div class="row" id="widgets">
        <div class="col-12">
        <span class="index-auto-invoice-text">
            Если вы ежемесячно выставляете счета за одни и те же услуги или товары и в назначении меняется только месяц
            и год, то настройте шаблон АвтоСчета. Счета автоматически будут формироваться и отправляться клиенту
            в день, который вы зададите.
        </span>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'find_by', [
                'type' => 'search',
                'placeholder' => 'Номер шаблона АвтоСчета или название контрагента',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="wrap">

    <?= $this->render('@documents/views/invoice/_template_table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'useContractor' => true,
        'noScroll' => true
    ]); ?>

</div>

<?php /*
<div id="templates-table">
    <div class="portlet-title">
        <div class="actions joint-operations col-md-4 col-sm-4" style="display:none;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete-auto', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-ban-circle"></i> Остановить', '#many-stop', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'many-stop',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['autoinvoice/many-stop']),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите остановить выбранные шаблоны?',
            ]); ?>
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'many-delete-auto',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['autoinvoice/many-delete']),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить выбранные шаблоны?',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= $this->render('@documents/views/invoice/_template_table', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'useContractor' => true,
            ]); ?>
        </div>
    </div>
</div>
*/ ?>