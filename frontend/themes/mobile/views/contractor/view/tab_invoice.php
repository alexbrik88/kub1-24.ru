<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.07.2018
 * Time: 12:48
 */

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\widgets\StatisticWidget;
use common\models\Company;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */

$canChief = Yii::$app->user->can(UserRole::ROLE_CHIEF);
$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::VIEW, ['ioType' => $ioType]);
$canPay = Yii::$app->user->can(permissions\Cash::CREATE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW) &&
    Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS);

$dropItems = [];
if ($ioType == Documents::IO_TYPE_OUT) {
    if ($canCreate) {
        $dropItems[] = [
            'label' => 'Создать Акты',
            'url' => '#many-create-act',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один Акт',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-act',
                'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать ТН',
            'url' => '#many-create-packing-list',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        if ($company->companyTaxationType->osno) {
            $dropItems[] = [
                'label' => 'Создать СФ',
                'url' => '#many-create-invoice-facture',
                'linkOptions' => [
                    'data-toggle' => 'modal',
                ],
            ];
            $dropItems[] = [
                'label' => 'Создать одну СФ',
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'create-one-invoice-facture',
                    'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
                ],
            ];
        }
        $dropItems[] = [
            'label' => 'Создать УПД',
            'url' => '#many-create-upd',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один УПД',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-upd',
                'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    if ($canIndex) {
        $dropItems[] = [
            'label' => 'Скачать в Excel',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'get-xls-link generate-xls-many_actions',
            ],
        ];
    }
    if ($canChief) {
        $dropItems[] = [
            'label' => 'Без Акта',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'act-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'Акт',
                    'attribute_text' => 'Без Акта',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_ACT,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без ТН',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'packing-list-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'ТН',
                    'attribute_text' => 'Без ТН',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_PACKING_LIST,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без УПД',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'upd-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'УПД',
                    'attribute_text' => 'Без УПД',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_UPD,
                ],
            ],
        ];
    }
}

$sendDropItems = [];
if ($canSend) {
    $sendDropItems[] = [
        'label' => 'Счета',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
        ],
    ];
    $sendDropItems[] = [
        'label' => 'Счета + Акт/ТН/СФ/УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs send-invoices-with-documents',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType, 'send_with_documents' => true]),
        ],
    ];
}

$dateRange = StatisticPeriod::getSessionPeriod();
$invoiceStatusId = array_filter(is_array($searchModel->invoice_status_id) ?
    $searchModel->invoice_status_id :
    explode(',', $searchModel->invoice_status_id));
$stat1 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel
);
$stat2 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID_IN_TIME,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel
);
$stat3 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::PAID,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel
);
$baseUrl = Yii::$app->params['kubAssetBaseUrl'];

$user = Yii::$app->user->identity;
$company = $user->company;
$userConfig = $user->config;
?>

<div class="wrap wrap_count">
    <div class="row">
        <div class="col-6 col-xl-3">
            <div class="count-card wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                <div class="count-card-title">Не оплачено ВСЕГО</div>
                <hr>
                <div class="count-card-foot">Количество счетов: <?= $stat1['count'] ?></div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_red wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                <div class="count-card-title">Не оплачено в срок</div>
                <hr>
                <div class="count-card-foot">Количество счетов: <?= $stat2['count'] ?></div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                <div class="count-card-title">Оплачено</div>
                <hr>
                <div class="count-card-foot">Количество счетов: <?= $stat3['count'] ?></div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap p-3 text_size_14">
                <div class="mb-1 weight-700 text-red">Просрочено на:</div>
                <div class="line-height-1-5">
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>0-10 дней</span>
                        <span class="weight-700"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, $model->id); ?></span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>11-30 дней </span>
                        <span class="weight-700"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, $model->id); ?></span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>31-60 дней</span>
                        <span class="weight-700"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, $model->id); ?></span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>61-90 дней </span>
                        <span class="weight-700"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, $model->id); ?></span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>Больше 90 дней</span>
                        <span class="weight-700"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, $model->id); ?></span>
                    </div>
                </div>
                <div class="d-flex flex-nowrap justify-content-between mt-1 weight-700 text-red">
                    <span>Просрочено на:</span>
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, true, $model->id); ?></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'contr_inv_scan',
                ],
                [
                    'attribute' => 'contr_inv_paylimit',
                ],
                [
                    'attribute' => 'contr_inv_paydate',
                ],
                [
                    'attribute' => 'contr_inv_act',
                ],
                [
                    'attribute' => 'contr_inv_paclist',
                ],
                [
                    'attribute' => 'contr_inv_waybill',
                ],
                [
                    'attribute' => 'contr_inv_invfacture',
                ],
                [
                    'attribute' => 'contr_inv_upd',
                ],
                [
                    'attribute' => 'contr_inv_author',
                    'visible' => (
                        Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
                    ),
                ],
                [
                    'attribute' => 'contr_inv_comment',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_contractor']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'byNumber', [
                'type' => 'search',
                'placeholder' => 'Номер счета, название или ИНН контрагента',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['/documents/invoice/generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/_invoices_table', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'useContractor' => true,
    'type' => $ioType,
    'id' => $model->id,
    'tabViewClass' => $userConfig->getTableViewClass('table_view_contractor')
]); ?>

<div class="tooltip_templates" style="display: none;">
                <span id="tooltip_contractor-debt" style="display: inline-block; text-align: center;">
                    Расшифровка «Не оплачено в срок»<br>
                    за весь период работы с клиентом.<br>
                    В блоках слева, данные за период, указанный выше.
                </span>
    <span id="tooltip_doc_upd" style="display: inline-block; text-align: center;">
                    У вас нет ни одной УПД.<br>
                    Что бы начать формировать УПД,<br>
                    измените настройки в профиле компании
                </span>
</div>
<?php Modal::begin([
    'id' => 'store-account-modal',
    'closeButton' => false,
]); ?>
<h4 id="store-account-modal-content"></h4>
<div style="text-align: center; margin-top: 30px;">
    <button type="button" class="btn darkblue text-white"
            data-dismiss="modal">OK
    </button>
</div>
<?php Modal::end(); ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            '/documents/invoice/many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? (Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent document-many-send',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
        ])  . ($sendDropItems ? Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', '#', [
                    'id' => 'dropdownMenu3',
                    'class' => 'document-many-send-dropdown hidden' . ' button-clr button-regular button-hover-transparent space-between',
                    'data-toggle' => 'dropdown',
                ]) . Dropdown::widget([
                    'items' => $sendDropItems,
                    'options' => [
                        'class' => 'dropdown-menu-right form-filter-list list-clr',
                    ],
                ]), ['class' => 'dropup']) : null)) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<span>Платежка</span>', '#many-charge', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a($this->render('//svg-sprite', ['ico' => 'check-2']).' <span>Оплачены</span>', '#many-paid-modal', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span class="pr-2">Еще</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'class' => 'form-filter-list list-clr dropdown-menu-right'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?php if ($canPay) : ?>
    <?php Modal::begin([
        'id' => 'many-paid-modal',
        'closeButton' => false,
        'options' => [
            'data-url' => Url::to(['/documents/invoice/add-flow-form', 'type' => $ioType]),
        ]
    ]); ?>
    <div id="many-paid-content"></div>
    <?php Modal::end(); ?>

    <?php $this->registerJs('
        $(document).on("shown.bs.modal", "#many-paid-modal", function() {
            var container = this;
            var idArray = $(".joint-operation-checkbox:checked").map(function () {
                return $(this).closest("tr").data("key");
            }).get();
            var url = this.dataset.url + "&id=" + idArray.join();
            $.post(url, function(data) {
                $("#many-paid-content", container).html(data);
                $("input:checkbox, input:radio", container).uniform();
                $(".date-picker", container).datepicker(kubDatepickerConfig);
            })
        });
        $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
            $("#many-paid-content").html("");
        });
    ') ?>
<?php endif ?>

<?php if ($canDelete) : ?>
<!-- <div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <h3 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h3>
            <div class="text-center">
                <?= Html::a('Да', null, [
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['many-delete']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div> -->

<?php Modal::begin([
    'id' => 'many-delete',
    'title' => 'Вы уверены, что хотите удалить выбранные счета?',
    'closeButton' => false,
    'toggleButton' => false,
    'options' => [
        'class' => 'confirm-modal modal fade',
    ],
    'titleOptions' => [
        'class' => 'ta-c',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
            'data-url' => Url::to(['/documents/invoice/many-delete', 'type' => $ioType]),
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>
<?php endif; ?>

<?php if ($canCreate) : ?>
    <?php Modal::begin([
        'id' => 'many-create-act',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Акты создать от даты:</label>
                <div>
                    <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act',
            'data-url' => Url::to(['/documents/act/many-create', 'type' => $ioType]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-packing-list',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Товарные Накладные создать от даты:</label>
                <div>
                    <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Создать', [
            'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act',
            'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => $ioType]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-invoice-facture',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Счета-Фактуры создать от даты:</label>
                <div>
                    <?= Html::textInput('InvoiceFacture[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act',
            'data-url' => Url::to(['/documents/invoice-facture/many-create', 'type' => $ioType]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-upd',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">УПД создать от даты:</label>
                <div>
                    <?= Html::textInput('Upd[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act',
            'data-url' => Url::to(['/documents/upd/many-create', 'type' => $ioType]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>

<?php if ($canChief) : ?>
    <?php $this->registerJs('
        $(".act-not-need, .packing-list-not-need, .upd-not-need").click(function (e) {
            var url = $("#not-need-document-modal .modal-not-need-document-submit").data("url").replace("_param_", $(this).data("param"));
            $("#not-need-document-modal span.document-name").text($(this).data("name"));
            $("#not-need-document-modal span.document-attribute_text").text($(this).data("attribute_text"));
            $("#not-need-document-modal .modal-not-need-document-submit").data("url", url);
        });
    '); ?>

    <?php Modal::begin([
        'id' => 'not-need-document-modal',
        'closeButton' => false,
    ]); ?>
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'close']), [
            'class' => 'modal-close close',
            'data-dismiss' => 'modal',
        ]) ?>
        <div class="form-group">
            <div class="bold">
                Для выбранных счетов в столбце <span class="document-name"></span> проставить
                «<span class="document-attribute_text"></span>».
            </div>
            Это можно будет отменить, кликнув на «<span class="document-attribute_text"></span>»
        </div>
        <div class="form-group">
            <div class="bold uppercase">Внимание!</div>
            Это делается для того, чтобы в разделе финансы у вас правильно строился Управленческий
            Баланс. Это позволит убрать суммы из строки “Предоплата покупателей (авансы)”<br>
            Ситуации, когда <b>НУЖНО</b> проставить, что «<span class="document-attribute_text"></span>»:
            <ul style="list-style: decimal;padding-inline-start: 15px;">
                <li>
                    <b><i>Счет покупатель оплатил наличными</i></b>. Вы эти деньги проводить по
                    бухгалтерии НЕ будете. Соответственно покупателю выставлять
                    <span class="document-name"></span> не будете.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил через Банк</i></b>. Вы проводить эти деньги по
                    бухгалтерии обязаны. Но <span class="document-name"></span> выставляете не в КУБ,
                    а в КУБ хотите получить правильный Управленческий Баланс.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил наличными, и вы пробили кассовый чек</i></b>. Вы
                    проводить эти деньги по бухгалтерии обязаны. Но <span class="document-name"></span>
                    выставляете не в КУБ, а в КУБ хотите получить правильный Управленческий
                    Баланс.
                </li>
            </ul>
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Применить', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-not-need-document-submit',
                'data-url' => Url::to([
                    '/documents/invoice/not-need-document',
                    'type' => $ioType,
                    'param' => '_param_',
                ]),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
<?php endif; ?>
