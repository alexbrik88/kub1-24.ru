<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$isPhysicalPerson = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
?>

<?= $this->render('customer_info/_partial_main', [
    'model' => $model,
]) ?>

<div class="wrap pl-2 pr-2 pt-3 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <div class="nav-tabs-row">
        <?= Tabs::widget([
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'headerOptions' => ['class' => 'nav-item'],
            'items' => array_merge([
                [
                    'label' => $isPhysicalPerson ? 'Данные паспорта' : 'Реквизиты',
                    'linkOptions' => ['class' => 'nav-link active'],
                    'content' => $this->render('customer_info/_partial_details', [
                        'model' => $model,
                    ]),
                ],
                [
                    'label' => 'Комментарии',
                    'linkOptions' => ['class' => 'nav-link'],
                    'content' => $this->render('customer_info/_partial_comment', [
                        'model' => $model,
                    ]),
                ],
            ], (FALSE && $model->type == Contractor::TYPE_SELLER) ?
                [
                    [
                        'label' => 'Агент',
                        'linkOptions' => ['class' => 'nav-link'],
                        'content' => $this->render('customer_info/_partial_agent', [
                            'model' => $model,
                        ]),
                    ]
                ] : []),
        ]); ?>
        </div>
    </div>
</div>

<div class="row">
    <?= $this->render('customer_info/_partial_director', [
        'model' => $model,
    ]); ?>

    <?= $this->render('customer_info/_partial_chief_accountant', [
        'model' => $model,
    ]); ?>

    <?= $this->render('customer_info/_partial_contact', [
        'model' => $model,
    ]); ?>
</div>