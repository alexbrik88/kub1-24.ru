<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Autoinvoice;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\ContractorSearch;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\rbac\permissions\document\Invoice;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;
use yii\bootstrap4\Modal;
use common\models\Company;
use common\components\ImageHelper;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\widgets\Pjax;
use common\models\employee\EmployeeClick;
use yii\bootstrap4\Dropdown;
use common\components\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $searchModel frontend\models\ContractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */
/* @var $prompt backend\models\Prompt */
/* @var $company Company */
/* @var $user Employee */

$this->title = Contractor::$contractorTitle[$type];

$countContractor = Contractor::find()->where(['and',
    ['employee_id' => Yii::$app->user->identity->id],
    ['type' => $type],
])->count();

$query = clone $dataProvider->query;

$contractorIdArray = $query->column();
if ($searchModel->duplicate) {
    $emptyMessage = 'Поиск дублей по ИНН - дубли не обнаружены';
} else {
    if ($searchModel->name) {
        $emptyMessage = 'Ничего не найдено';
    }
    elseif ($type == Contractor::TYPE_CUSTOMER) {
        $emptyMessage = 'Вы еще не добавили ни одного покупателя';
    } elseif ($type == Contractor::TYPE_SELLER) {
        $emptyMessage = 'Вы еще не добавили ни одного поставщика';
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$user = Yii::$app->user->identity;
$userConfig = $user->config;
$company = $user->company;

$hasFilters = $searchModel->activityStatus != ContractorSearch::ACTIVITY_STATUS_ACTIVE
    || (bool) $searchModel->filterTaxation
    || (bool) $searchModel->filterIsAccounting;
$autoinvoiceFiterItems = array_merge(['' => 'Все', 'no' => 'Нет'], Autoinvoice::$STATUS);
unset($autoinvoiceFiterItems[Autoinvoice::DELETED]);

$invCount = $company->getInvoiceLeft();
$dateRange = StatisticPeriod::getSessionPeriod();
$stat1 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::NOT_PAID,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
$stat2 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::NOT_PAID_IN_TIME,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
$stat3 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::PAID,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
$filterStatusItems = [
    ContractorSearch::ACTIVITY_STATUS_ALL => 'Все',
    ContractorSearch::ACTIVITY_STATUS_ACTIVE => 'Активные',
    ContractorSearch::ACTIVITY_STATUS_INACTIVE => 'Неактивные',
];
$filterTaxationItems = [
    ContractorSearch::TAXATION_ALL => 'Все',
    ContractorSearch::TAXATION_NDS => 'С НДС',
    ContractorSearch::TAXATION_NO_NDS => 'Без НДС'
];
$filterFormItems = ['' => 'Все'] + ArrayHelper::map(
    CompanyType::find()->where([
        'id' => Contractor::find()
            ->select('company_type_id')
            ->distinct()->where([
                'company_id' => $company->id,
                'type' => $type
            ])
            ->column()
        ])
        ->all(),
    'id',
'name_short');
$filterIsAccountingItems = [
    ContractorSearch::NO_ACCOUNTING_ALL => 'Все',
    ContractorSearch::NO_ACCOUNTING_YES => 'Да',
    ContractorSearch::NO_ACCOUNTING_NO => 'Нет'
];
$tabViewClass = $userConfig->getTableViewClass('table_view_contractor_list');
?>

<div class="stop-zone-for-fixed-elems contractor-index">

    <?php /*
    <?php if (!$company->getCashBankStatementUploads()->exists() && Yii::$app->user->identity->showAlert) : ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×
            </button>
            <a href="<?= Url::to(['cash/bank/index']) ?>">Загрузите выписку</a>
            из банка и все имеющиеся у вас Покупатели и Поставщики будут созданы автоматически.
        </div>
        <?php $this->registerJs('
            $(document).on("click", "#contractor_alert_close", function() {
                $.ajax({"url": "' . Url::to(['alert-close']) . '"});
            });
        '); ?>
    <?php endif; ?> */ ?>

    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if (\frontend\rbac\permissions\Contractor::CREATE): ?>
        <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
            '/contractor/create',
            'type' => $type,
        ]) ?>">
            <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use></svg>
            <span>Добавить</span>
        </a>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="count-card wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено ВСЕГО</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat1['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_red wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено в срок</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat2['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_green wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Оплачено</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat3['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php /* todo
                <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) &&
                    in_array($user->currentEmployeeCompany->employee_role_id, [
                        EmployeeRole::ROLE_CHIEF,
                        EmployeeRole::ROLE_SUPERVISOR
                    ])
                ): ?>

                    <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                        'class' => 'button-regular w-100 button-hover-content-red'
                    ]); ?>
                <?php endif; ?>*/ ?>
                <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
                    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php else : ?>
                    <?php /*
                    <?= Html::a('Увеличение онлайн продаж', ['/contractor/sale-increase'], [
                        'class' => 'employee-click_trigger button-regular w-100 button-hover-content-red',
                        'data-url' => Url::to(['/site/employee-click', 'type' => EmployeeClick::SALE_INCREASE_TYPE])
                    ]) */ ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="table-settings row row_indents_s">
        <div class="col-3">
            <?= Html::a(
                '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#exel"></use></svg>',
                Url::current(['xls' => 1]),
                [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                    'title' => 'Скачать в Excel',
                ]
            ); ?>

            <?= TableConfigWidget::widget([
                'items' => array_filter([
                    [
                        'attribute' => 'contractor_contact',
                    ],
                    [
                        'attribute' => 'contractor_phone',
                    ],
                    [
                        'attribute' => 'contractor_agreement',
                    ],
                    [
                        'attribute' => 'contractor_notpaidcount',
                    ],
                    [
                        'attribute' => 'contractor_notpaidsum',
                    ],
                    [
                        'attribute' => 'contractor_paidcount',
                    ],
                    [
                        'attribute' => 'contractor_paidsum',
                    ],
                    [
                        'attribute' => $type == Contractor::TYPE_CUSTOMER ? 'contractor_autionvoice' : null,
                    ],
                    [
                        'attribute' => 'contractor_responsible',
                    ],
                ]),
            ]); ?>

            <?= TableViewWidget::widget(['attribute' => 'table_view_contractor_list']) ?>
        </div>
        <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
            <?= Html::a('Дубли', Url::current([
                'ContractorSearch' => [
                    'duplicate' => $searchModel->duplicate ? null : 1,
                ],
            ]), [
                'class' => 'button-regular button-clr duplicate-contractor' . (
                    $searchModel->duplicate ? ' active button-regular_red' : ' button-hover-transparent'
                ),
                'title' => 'Показать дубли ' . ($type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков')
            ]); ?>
        </div>
        <div class="col-6">
            <?= Html::beginForm(Url::current(), 'get', [
                'id' => 'contractor-filter-form',
                'class' => 'd-flex flex-nowrap align-items-center',
            ]); ?>

            <!-- filter -->
            <div class="dropdown popup-dropdown popup-dropdown_filter products-filter <?= $hasFilters ? 'itemsSelected' : '' ?>">
                <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="button-txt">Фильтр</span>
                    <svg class="svg-icon svg-icon-shevron">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                    </svg>
                </button>
                <div class="dropdown-menu keep-open" aria-labelledby="filter">
                    <div class="popup-dropdown-in p-3">
                        <div class="p-1">
                            <div class="row">
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown1" data-default="<?= ContractorSearch::ACTIVITY_STATUS_ACTIVE ?>">
                                        <div class="label">Статус</div>
                                        <?= Html::activeHiddenInput($searchModel, 'activityStatus') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                                    <span class="drop-title">
                                                        <?= \yii\helpers\ArrayHelper::getValue($filterStatusItems, (int)$searchModel->activityStatus) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown1">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterStatusItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->activityStatus) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown2">
                                        <div class="label">Налогообложение</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterTaxation') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterTaxationItems, (int)$searchModel->filterTaxation) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown2">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterTaxationItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterTaxation) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown3">
                                        <div class="label">Форма</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterForm') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                    <span class="drop-title">
                                                        <?= \yii\helpers\ArrayHelper::getValue($filterFormItems, $searchModel->filterForm) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown3">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterFormItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterForm) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown4">
                                        <div class="label">Не для бухгалтерии</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterIsAccounting') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown4">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterIsAccountingItems, (int)$searchModel->filterIsAccounting) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown4">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterIsAccountingItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterIsAccounting) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mt-3">
                                    <div class="row justify-content-between">
                                        <div class="form-group column">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                <span>Применить</span>
                                            </button>
                                        </div>
                                        <div class="form-group column">
                                            <button id="product_filters_reset" class="button-regular button-hover-content-red button-width-medium button-clr" data-clear="dropdown" type="button">
                                                <span>Сбросить</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group flex-grow-1 ml-2">
                <?= Html::activeTextInput($searchModel, 'name', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>

            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list' . $tabViewClass,
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '1%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::checkbox('Contractor[' . $data->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'contractor-cell-main'
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a($data->getTitle(true), ['contractor/view', 'type' => $data->type, 'id' => $data->id]);
                        },
                    ],
                    [
                        'attribute' => 'contact',
                        'label' => 'Контакт',
                        'headerOptions' => [
                            'class' => 'col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'contact-cell col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getContactItemsByQuery($dataProvider->query),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->realContactName ?: '';
                        },
                        's2width' => '250px'
                    ],
                    [
                        'attribute' => 'phone',
                        'label' => 'Телефон',
                        'headerOptions' => [
                            'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->realContactPhone ?: '';
                        },
                    ],
                    [
                        'attribute' => 'agree',
                        'label' => 'Договор',
                        'headerOptions' => [
                            'class' => 'col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'agreement-cell col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                        ],
                        'format' => 'html',
                        'value' => 'lastAgreementStr',
                    ],
                    [
                        'attribute' => 'not_paid_count',
                        'label' => 'Счетов<br>не<br>оплачено',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::moneyFormat($data->not_paid_count);
                        },
                    ],
                    [
                        'attribute' => 'not_paid_sum',
                        'label' => 'Не оплачено счетов на сумму',
                        'headerOptions' => [
                            'class' => 'col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->not_paid_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'paid_count',
                        'label' => 'Счетов оплачено',
                        'headerOptions' => [
                            'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::moneyFormat($data->paid_count);
                        },
                    ],
                    [
                        'attribute' => 'paid_sum',
                        'label' => 'Оплачено счетов на сумму',
                        'headerOptions' => [
                            'class' => 'col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->paid_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'autoinvoice',
                        'label' => 'АвтоСчет',
                        'headerOptions' => [
                            'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:80px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                        ],
                        'filter' => $autoinvoiceFiterItems,
                        'value' => function (Contractor $data) {
                            return ArrayHelper::getValue(Autoinvoice::$STATUS, $data->autoinvoice, '');
                        },
                        'visible' => $type == Contractor::TYPE_CUSTOMER,
                        's2width' => '200px'
                    ],
                    [
                        'header' => $searchModel->duplicate ? 'Дубли' : '',
                        'headerOptions' => [
                            'width' => '1%',
                        ],
                        'contentOptions' => [
                            'style' => 'max-width:120px;' . ($searchModel->duplicate ? ' color:#f3565d;' : 'padding-right:10px;'),
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $data) use ($searchModel) {
                            if (!$searchModel->duplicate) {
                                if ($data->company_id !== null) {
                                    return Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span class="pl-1">Счёт</span>', [
                                        'documents/invoice/create',
                                        'type' => $data->type,
                                        'contractorId' => $data->id,
                                    ], [
                                        'class' => 'button-regular button-hover-content-red',
                                        'style' => 'width:81px;'
                                    ]);
                                }

                                return '';
                            }

                            return $data->getDuplicateDifference();
                        },
                        'visible' => Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                            'ioType' => $type, // same value
                        ]),

                    ],
                    [
                        'attribute' => 'responsible_employee_id',
                        'label' => 'От&shy;вет&shy;ствен&shy;ный',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getResponsibleItemsByQuery($dataProvider->query),
                        'value' => function (Contractor $data) use ($company) {
                            $employee = EmployeeCompany::findOne([
                                'employee_id' => $data->responsible_employee_id,
                                'company_id' => $company->id,
                            ]);

                            return $employee ? $employee->getFio(true) : '';
                        },
                        's2width' => '250px'
                    ],
                ],
            ]); ?>

    <?php /* if ($countContractor == 0) : ?>
        <?= \frontend\widgets\PromptWidget::widget([
            'prompt' => $prompt,
        ]); ?>
    <?php endif; */ ?>

</div>

<?= \frontend\themes\mobile\widgets\SummarySelectContractorWidget::widget([
    'buttons' => [
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Изменить ответственного',
                        'url' => '#change_responsible_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Изменить статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align']),
    ],
]); ?>


<?php Modal::begin([
    'title' => false,
    'id' => 'change_responsible_modal',
]); ?>
<h4 class="modal-title">Изменить ответственного</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Ответственный
            </label>
            <?= Select2::widget([
                'id' => 'contractor-responsible_employee',
                'name' => 'responsibleEmployee',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => ArrayHelper::map($company->getEmployeeCompanies()
                    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                    ->orderBy([
                        'lastname' => SORT_ASC,
                        'firstname' => SORT_ASC,
                        'patronymic' => SORT_ASC,
                    ])->all(), 'employee_id', 'fio'),
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-responsible button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/contractor/change-responsible', 'type' => $type]),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => false,
    'id' => 'change_status_modal',
]); ?>
<h4 class="modal-title">Изменить статус</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">

    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Статус
            </label>
            <?= Html::radioList('status', Contractor::ACTIVE, [
                Contractor::ACTIVE => 'Активный',
                ContractorSearch::INACTIVE => 'Не активный',
            ], [
                'id' => 'contractor-status',
            ]); ?>
        </div>
    </div>

</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/contractor/change-status', 'type' => $type]),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php /* todo
<?php if ($company->show_popup_sale_increase && $type == Contractor::TYPE_CUSTOMER): ?>
    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Повышение продаж
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_BILL,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 6,
            'description' => 'ПРОДАВАЙТЕ БОЛЬШЕ без УЧАСТИЯ ПРОДАВЦОВ<br>
                    Ваш сайт сможет самостоятельно выставлять счета покупателям.<br>
                    Создайте личные ОНЛАЙН-КАБИНЕТЫ для покупателей с доступом к складу.',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/contractor/sale-increase']),
            'image' => ImageHelper::getThumb('img/modal_registration/block-6.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => 5,
            'nextModal' => null,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
*/ ?>

<?php $this->registerJs('

    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });

    // SEARCH
    $("input#contractorsearch-name").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $("#contractor-filter-form").submit();
      }
    });

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }

    $("#contractor-filter-form").find(".filter-item").on("click", function(e) {
        e.preventDefault();
            var pop =  $(this).parents(".popup-dropdown_filter");
            var drop = $(this).parents(".dropdown-drop");
            var value = $(this).data("id");
            $(drop).find("input").val(value);
            $(drop).find(".drop-title").html($(this).text());
            $(drop).find(".dropdown-drop-menu").removeClass("visible show");

            refreshProductFilters();
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");

            if (drop.attr("data-default")) {
                a_first = $(drop).find("li").eq(drop.attr("data-default")).find("a");
            }

            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

') ?>