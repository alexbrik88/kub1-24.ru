<style>
    .tabs-line__wrap > label {
        float: left;
        display: block;
        width: 33%;
        color: #9198a0;
    }
    .accounting-table p {
        margin:0;
        padding: 5px 0;
    }
    .disclaimer {
        font-weight: 300;
    }
    .company-name.nabors {
        margin-bottom: 1rem;
    }
    .founder-item__dl dd {
        margin-bottom:0;
    }
    .company-item__title > strong,
    .company-item__title > a {
        display: inline-block;
        margin-bottom:2px;
    }
    .tooltip-popup > span {
        font-size: 0.85em;
        background: #fff;
    }
    h2 {
        font-size: 28px;
    }
    .wrap .caption {
        position: relative;
        font-size: 20px;
        font-weight: bold;
        line-height: 24px;
        margin-bottom: 1rem;
    }
    .wrap .caption .reload {
        line-height: 18px;
    }
    .wrap .caption .reload > a {
        font-size: 22px;
    }
    .reload {
        position: absolute;
        top: 0;
        right: 0;
        width: 183px;
    }
    .founders-wrap {
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 9999;
        background-color: rgba(51, 51, 51, 0.8);
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: linear .2s;
        -o-transition: linear .2s;
        transition: linear .2s;
        overflow: hidden;
    }
    .founders-wrap-scroll {
        overflow-y: auto;
        padding-right: 30px;
    }
    .founders-wrap-in {
        position: relative;
        margin-left: auto;
        right: -100%;
        top: 0;
        width: 503px;
        padding: 25px 30px 30px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        flex-grow: 1;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        background-color: #fff;
        -webkit-transition: linear .6s;
        -o-transition: linear .6s;
        transition: linear .6s;
    }
    .founders-wrap-close {
        position: absolute;
        top: 10px;
        right: 20px;
    }
    .founders-wrap-title {
        font-size: 20px;
        margin-bottom: 20px;
    }
    .founders-wrap.visible {
        opacity: 1;
        visibility: visible;
    }
    .founders-wrap.visible .founders-wrap-in {
        right: 0;
    }

    .company-name {
        border-bottom: none;
        padding-bottom: 0;
        margin-bottom: 0;
    }
    .chief-title {
        font-weight: 300;
    }
    .company-item {
        margin-bottom:10px;
    }
    .arbitr.accounts-list .company-item-info {
        font-size: 16px;
        line-height: 20px;
    }
    .reliability.accounts-list {
        padding-top:10px;
    }
    .rely-tile-badge,
    .reliability.accounts-list .rely-rating-badge > span {
        padding: 10px;
        font-size: 16px;
        line-height: 20px;
        color: #fff;
        margin: 1px 0 17px;
        border-radius: 8px;
        text-transform: uppercase;
    }
    .finance.accounts-list .company-item__title > strong {
        display: block;
        font-size: 18px;
        margin: 20px 0 10px 0;
    }
    .connections.accounts-list .company-item__title { margin-bottom:3px; margin-top:20px; font-size: 16px; }
    .connections.accounts-list .company-item__text {  margin-bottom:3px;  font-size: 16px; }
    .connections.accounts-list .company-item-info {  margin-bottom:3px;  font-size: 16px; }

    .company-item-info {
        font-size: 14px;
    }
    .company-item-info dl {
        margin-bottom: 5px;
    }
    .tabs-line {
        margin-top: 15px;
        margin-bottom: 20px;
    }
    .tabs-line .btn-link,
    .sort .sort-item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        width: auto;
        padding: 3px 10px;
    }
    .tabs-line .btn-link:hover,
    .tabs-line .btn-link:focus,
    .tabs-line .btn-link:active {
        background-color: transparent;
    }
    .sort .sort-item-wrap {
        display: inline-block;
        vertical-align: top;
        position: relative;
        width: auto;
    }
    .sort-list, .sort__label {
        display: inline-block;
        vertical-align: middle;
    }
    .sort-list {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .requisites-item__name {
        font-size: 16px;
        line-height: 19px;
    }
    .rely-short-negative {
        color: #c40000;
    }
    .rely-short-warning {
        color: #dca52a;
    }
    .rely-short-positive, .rely-rating-positive {
        color: #09c400;
    }
    .requisites-item {
        display: -webkit-flex;
        display: flex;
        padding: 5px 8px;
        margin: 0 0 0 -8px;
        -ms-align-items: flex-start;
        align-items: flex-start;
        line-height: 15px;
    }
    .requisites-item__name {
        flex: 0 0 364px;
        flex-basis: 364px;
        font-weight: 400;
        margin-right: 20px;
    }
    .requisites-item__value {
        flex: 1 1 auto;
        font-weight: 400;
    }
    .reliability-page .requisites-item {
        padding: 13px 8px 11px;
    }

    .finance-header {
        font-weight: 700;
        font-size: 16px;
        margin-bottom: 10px;
        margin-top: 15px;
    }
    .finance-list {
        display: -webkit-flex;
        display: flex;
        flex-wrap: wrap;
    }
    .fl-block {
        flex: 0 0 25%;
        padding: 0 8px 12px 0;
    }
    .fl-text .num {
        font-weight: 400;
        font-size: 20px;
    }
    .finance-changes {
        position: relative;
        color: #646e74;
        font-size: 11px;
        line-height: 16px;
        font-weight: 400;
    }
    .finance-changes.lt, .finance-changes.mt.reverted {
        color: #f91d0a;
        font-size: 12px;
    }
    .finance-changes.lt.reverted, .finance-changes.mt {
        color: #61a517;
        font-size: 12px;
    }

    .okved-list {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .okved-item {
        display: -webkit-flex;
        display: flex;
        padding: 5px 8px;
        margin: 0 0 0 -8px;
    }
    .okved-item__num {
        flex: 0 0 80px;
        min-width: 80px;
        max-width: 80px;
        font-weight: 400;
    }
    .okved-item__text {
        flex: 1 1 auto;
    }
    .okved-list + .okved-list-title {
        margin-top: 24px;
    }
    .okved-list-title {
        margin-bottom: 21px;
    }
    .tile-item__title {
        font-size: 18px;
        line-height: 24px;
        font-weight: 700;
        margin: 0 0 8px 0;
    }

    .filter-block__wrap {
        display: -webkit-flex;
        display: flex;
        align-items: center;
        -webkit-justify-content: space-between;
        justify-content: space-between;
        padding: 5px 0;
        flex-wrap: wrap;
    }
    .filter-block__frame {
        width: 946px;
        margin: 0 -36px 0 0;
    }
    .filter-block__item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        padding: 8px 0;
        margin: 0 36px 0 0;
    }
    .filter-block__label {
        line-height: 29px;
        margin: 2px 5px 0 0;
        vertical-align: top;
        display: inline-block;
    }
    .filter-block__item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        padding: 8px 0;
        margin: 0 36px 0 0;
    }

    .company-item-info dl {
        display: inline-block;
        vertical-align: top;
        margin: 0;
        padding: 0;
        line-height: 15px;
    }
    .company-item-info dt {
        display: inline;
        margin: 0;
        padding: 0 4px 0 0;
        font-weight: 700;
    }
    .company-item-info dd {
        display: inline;
        line-height: 15px;
        margin: 0 16px 0 0;
        padding: 0;
        font-weight: normal;
    }
    .license-name::before {
        content: "№ ";
        font-size: 1rem;
    }
    .license-name {
        font-size: 16px;
        line-height: 22px;
        font-weight: 400;
        margin: 0 0 7px;
    }

    .info-table--egrul {
        margin-top: 21px;
    }
    .info-table {
        width: 100%;
        table-layout: fixed;
        margin: 0;
        border-collapse: collapse;
        margin-top: 5px;
    }
    .info-table--egrul td:first-child {
        width: 1px;
        text-align: center;
    }
    .info-table td {
        padding: 4px 8px;
        vertical-align: top;
        line-height: 16px;
    }
    .info-table--egrul td:nth-child(2) {
        width: 113px;
        padding-left: 1rem;
    }

    .accounting-header__title {
        float: left;
    }
    .accounting-description {
        margin: 0 0 16px;
        font-weight: 400;
    }
    .accounting-table {
        margin: 0;
        border-collapse: collapse;
        table-layout: fixed;
        width: 100%;
    }
    .accounting-table td:first-child, .accounting-table th:first-child {
        width: 240px;
        padding-right: 10px;
    }
    .accounting-table th {
        text-align: left;
        background: rgba(234,236,237,.6);
        vertical-align: top;
    }
    .accounting-table td:nth-child(2), .accounting-table th:nth-child(2) {
        width: 107px;
    }
    .accounting-table td.has-value.table-desktop, .accounting-table th.has-value.table-desktop {
        display: table-cell;
    }
    .accounting-table td.has-value, .accounting-table th.has-value {
        text-align: right;
        display: none;
    }
    .accounting-table-wrap + .tile-item__title {
        margin-top: 36px;
    }
    .accounting-table tr:nth-child(2n) {
        background: rgba(234,236,237,.6);
    }

    .tile-item {
        background: #fff;
        margin-bottom: 24px;
        overflow: hidden;
        position: relative;
    }
    .anketa-top {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 16px;
    }
    .anketa-bottom {
        margin-top: 24px;
        padding-top: 16px;
        font-weight: 300;
    }
    .anketa-bottom .anketa-actual {
        flex: none;
        font-size: 1rem;
        font-weight: bold;
    }
    .anketa-bottom .flextext {
        margin-left: 25px;
    }
    .clear::after {
        display: block;
        clear: both;
        content: "";
    }
    #anketa .leftcol {
        float: left;
        width: 58%;
    }
    #anketa .rightcol {
        float: right;
        width: 42%;
        padding-left: 24px;
        box-sizing: border-box;
    }
    .company-requisites .company-row:first-child, .rightcol .company-row:first-child {
        margin-top: 0;
    }
    .company-requisites .company-row {
        margin-top: 10px;
    }
    .company-row {
        margin: 16px 0 0;
    }
    .company-row::after {
        display: block;
        clear: both;
        content: "";
    }
    .company-col {
        width: calc(50% - 10px);
        float: left;
        margin: 0;
    }
    .company-info__text, .company-info__title {
        font-size: 1rem;
        display: block;
    }
    .company-info__title {
        font-weight: 700;
        margin: 0 0 5px;
    }
    .company-info__text {
        padding: 0 11px;
        margin: 0 -11px;
        font-style: normal;
    }
    .company-col + .company-col {
        margin-left: 20px;
    }
    .connexion > .connexion-col {
        min-width: 27%;
    }
    .connexion-col {
        display: inline-block;
        vertical-align: top;
        padding: 6px 30px 0 0;
        min-width: 156px;
        box-sizing: border-box;
    }
    .connexion-col__title {
        font-weight: 500;
        font-size: 1rem;
    }
    .connexion-col__num, .connexion-col__num.tosmall .num {
        font-weight: 600;
        font-size: 24px;
    }
    .see-details {
        display: inline-block;
        margin: 5px 0 4px;
        padding-right: 1rem;
        position: relative;
    }
    .text-dl dt {
        float: left;
        margin: 0 4px 0 0;
        padding: 0;
    }
    .text-dl dd {
        margin: 0;
        padding: 0;
    }
    .finance-columns {
        display: flex;
        margin: 0 -10px;
    }
    .finance-col {
        flex: 1 1 33.3%;
        box-sizing: border-box;
        padding: 0 10px 15px;
    }
    .finance-link {
        margin: 0 0 12px;
    }
    .btn-link.active, .fake-tab-opener.active, .tab-opener.active {
        border-color: #007bff;
        color: #007bff;
    }
    .tile-area .fake-tab-opener, .tile-area .tab-opener {
        padding: 3.5px 8px 2.5px;
    }
    .btn-link, .fake-tab-opener, .tab-opener {
        border: 1px solid transparent;
        line-height: 17px;
        font-size: 1rem;
        font-weight: 400;
        border-radius: 4px;
        padding: 2px 8px;
        color: #007bff;
        cursor: pointer;
        transition: border-color .2s;
        background: none;

    }
    .btn-link, .fake-tab-opener, .tab-control li, .tab-opener {
        display: inline-block;
        vertical-align: top;
    }
    .finance-data {
        margin: 0 0 7px;
    }
    .finance-data .num {
        font-size: 24px;
        font-weight: 500;
    }

    /** LOADER **/
    .loader {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        position: relative;
        margin: 0 auto;
    }
    .loader span:nth-child(1) {
        animation: opacitychange 1s ease-in-out infinite;
    }
    .loader span {
        display: inline-block;
        width: 20px;
        height: 20px;
        border-radius: 100%;
        background-color: #3498db;
        margin: 35px 5px;
        opacity: 0;
    }
    @keyframes opacitychange {
        0%, 100% {
            opacity: 0;
        }
        60% {
            opacity: 1;
        }
    }
    .loader span:nth-child(1) {
        animation: opacitychange 1s ease-in-out infinite;
    }
    .loader span:nth-child(2) {
        animation: opacitychange 1s ease-in-out 0.33s infinite;
    }
    .loader span:nth-child(3) {
        animation: opacitychange 1s ease-in-out 0.66s infinite;
    }

    /* TARIFFS PAGE */
    /* grid */
    .row-5ths {
        padding-left:10px;
        padding-right:10px;
    }
    .col-x5 {
        position: relative;
        min-height: 1px;
        padding-right: 5px;
        padding-left: 5px;
    }
    .col-x5 {
        width: 25%;
        float: left;
    }
    @media (min-width: 1316px) {
        .col-x5 {
            width: 20%;
            float: left;
        }
    }
    /* blocks */
    .t-subtitle { color: #666; font-size: 16px; }
    .t-tile { border-radius: 4px!important; }
    .t-tile .title { min-height: 60px; font-size: 18px; color: #4679ae; }
    .t-tile .title > .img { float:left; }
    .img-width-1 { width: 53px; }
    .img-width-2 { width: 65px; }
    .t-tile .title > .txt {  }
    .t-tile .title > span {}
    .t-tile .list {}
    .t-tile .list ul { padding-left: 0; }
    .t-tile .list ul li { margin-left: 15px; }

    .st-tile { border-radius: 4px!important; border:1px solid #ddd; padding:20px; }
    .st-tile .title { font-size: 18px; color: #4679ae; }
    .st-tile .subtitle { width: 50%; font-size: 16px; }

    .tariff-block { margin-top: 20px; min-height: 250px; border: 1px solid #ddd; margin-bottom: 10px; position: relative; border-radius: 4px; }
    .tariff-block .header { padding: 20px; background-color: #eee; color:#999; font-size: 18px; }
    .tariff-block .body .price { padding: 15px 20px 0 20px; font-size: 32px; font-weight: bold; }
    .tariff-block .body .average_price { position: relative; padding: 0 20px 15px 20px; color:#9299a1; font-size: 24px; }
    .tariff-block .body .average_price > .small { font-size: 13px; }
    .tariff-block .body .average_price > .top-left { position: absolute; top:0; left:13px; }
    .tariff-block .footer { padding: 20px; text-align: center; }
    .tariff-block.hit { margin-top: 0; }
    .tariff-block.hit .header { background-color: #7098c0; color: #fff; padding-top: 40px; }
    .tariff-block.hit .body { background-color: #4679ae;  color: #fff; }
    .tariff-block.hit .body .average_price { color: #d0d8e0; }
    .tariff-block.hit .footer { background-color: #4679ae; }
    .tariff-block.hit .hit {position: absolute; right: 5px; top: 5px; color:#fff; font-size: 18px; }

    .bg_opacity {
        background-color: #fff;
        opacity: .95;
    }
    .bg_man_in_lens {
        background-image: url(/img/dossier/man_in_lens.png);
        background-repeat: no-repeat;
        background-position: 99% 99%;
    }
    .bg_man_1 {
        min-height: 180px;
        background-image: url(/img/dossier/man_on_server.png);
        background-repeat: no-repeat;
        background-position: 100% 100%;
    }
    .bg_woman_2 {
        min-height: 180px;
        background-image: url(/img/dossier/woman_by_board.png);
        background-repeat: no-repeat;
        background-position: 100% 100%;
    }
    .bg_woman_3 {
        min-height: 180px;
        background-image: url(/img/dossier/womans_flying_papers.png);
        background-repeat: no-repeat;
        background-position: 100% 100%;
    }
</style>