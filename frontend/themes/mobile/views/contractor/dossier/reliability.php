<?php

use common\components\zchb\ReliabilityHelper;
use common\models\Contractor;
use yii\web\View;

/**
 * @var View              $this
 * @var Contractor        $model
 * @var ReliabilityHelper $reliability
 */
$model = $this->params['dossierContractor'];
$this->title = 'Досье ' . $model->getShortName(true);
echo $this->render('_menu');

?>

<div class="wrap">
    <div class="caption">
        Анализ надёжности организации
    </div>
    <div class="accounts-list reliability">
        <div class="content-frame__header content-frame__header--rely">
            <div class="rely-rating-badge">
                <span style="background-color:<?= $reliability->color(true) ?>">
                    <?= $reliability->reliabilityString(true) ?>
                </span>
            </div>
        </div>
        <p><br /></p>
        <div class="tabs-line">
            <div class="tabs-line__wrap">
                <?php /* <span class="btn-link size-md active">Все</span> */ ?>
                <a href="#negative" class="btn-link size-md rely-negative">Отрицательные
                    (<?= $reliability->negativeCount() ?>)</a>
                <a href="#attention" class="btn-link size-md rely-warning">Требуют внимания
                    (<?= $reliability->attentionCount() ?>)</a>
                <a href="#positive" class="btn-link size-md rely-positive">Положительные
                    (<?= $reliability->positiveCount() ?>)</a>
            </div>
        </div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Отрицательные факты</h2></div>
            </div>
            <?php foreach ($reliability->negative() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-negative"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div id="attention"></div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Требующие внимания факты</h2></div>
            </div>
            <?php foreach ($reliability->attention() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-warning"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div id="positive"></div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Положительные факты</h2></div>
            </div>
            <?php foreach ($reliability->positive() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-positive"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <br/>
        <div class="disclaimer">
            Значение рейтинга формируется в результате анализа открытых общедоступных данных РФ и фактов хозяйственной
            деятельности организации с использованием скоринговой модели.
            Данная оценка является мнением сервиса &laquo;КУБ&raquo; и не дает каких-либо гарантий или заверений третьим лицам,
            а также не является рекомендацией для принятия коммерческих или иных решений. Значение рейтинга
            автоматически пересчитывается при изменении фактов хозяйственной деятельности и поступлении новой информации
            из источников открытых данных.
        </div>
    </div>
</div>