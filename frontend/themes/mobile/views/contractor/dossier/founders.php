<?php

use common\components\zchb\Card;
use common\models\Company;
use common\models\Contractor;
use frontend\assets\InterfaceCustomAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

/**
 * @var View $this
 * @var Card $card
 *
 */

/** @var Company $company */
/** @var Contractor $model */

$model = $this->params['dossierContractor'];
$this->title = 'Досье ' . $model->getShortName(true);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

//InterfaceCustomAsset::register($this);
echo $this->render('_menu');
echo $this->render('_founders_panel');
?>

<div class="wrap">
    <div class="caption">
        Учредители (<?= $card->founderCount() ?>)
    </div>
    <div class="accounts-list founders">

        <div class="content-frame__header">
            <div class="content-frame__description">
                Согласно данным ЕГРЮЛ

                <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Единый государственный реестр юридических лиц ФНС РФ">
                    <svg class="tooltip-question-icon svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use></svg>
                </button>

                <br/>
                учредителем организации является <?= $card::plural($card->founderCount(), 'физическое лицо', 'физических лица', 'физических лиц') ?>.
                <br />
            </div>
            <div class="content-frame__description">Уставный капитал: <?= number_format($card->СумКап, 0, '.', ' ') ?> руб.</div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content">
                <?php foreach ($card->founders as $item) { ?>
                    <div class="company-item">
                        <div class="company-item__title">
                            <?php if ($item['isFL'] === true && $item['inn']) { ?>
                                <a href="javascript:void(0)" class="open-founders-panel" data-target="founders" data-url="<?= Url::toRoute(['ajax-fl-card', 'id' => $item['inn']]) ?>"><?= $item['name'] ?></a>
                                <span class="loader" style="display: none"><img width="7" src="/img/ofd-preloader.gif"/></span>
                            <?php } else { ?>
                                <strong><?= $item['name'] ?></strong>
                            <?php } ?>
                        </spandiv>
                        <div class="company-item-info">
                            <dl>
                                <dt>Доля:</dt>
                                <dd>
                                    <?php if ($item['dol_abs'] > 0 && $card->СумКап > 0){ ?>
                                    <?= number_format($item['dol_abs'], 0, '.', ' ') ?> руб. <span class="green-mark">(<?= round($item['dol_abs'] / $card->СумКап * 100) ?>%)</span></dd>
                                <?php } else { ?>
                                    нет информации
                                <?php } ?>
                            </dl>
                        </div>
                        <div class="company-item-info">
                            <dl>
                                <?php if ($item['inn']) { ?>
                                    <dt>ИНН:</dt>
                                    <dd><?= $item['inn'] ?></dd>
                                <?php } ?>
                                <?php if ($item['ogrn']) { ?>
                                    <dt>ОГРН:</dt>
                                    <dd><?= $item['ogrn'] ?></dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>
