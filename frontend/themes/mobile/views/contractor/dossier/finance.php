<?php

use common\components\zchb\Card;
use common\components\zchb\FinancialStatement;
use common\models\Contractor;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View                    $this
 * @var Card                    $card
 * @var Contractor              $model
 * @var FinancialStatement|null $fs
 * @var int[]                   $revenue     Выручка (текущий год, разница с предыдущим)
 * @var int[]                   $profit      Прибыль (текущий год, разница с предыдущим)
 * @var int[]                   $fixedAssets ОС (текущий год, разница с предыдущим)
 * @var int[]                   $expenses    Расходы (текущий год, разница с предыдущим)
 * @var int[]                   $receivables Дебиторская задолженность (текущий год, разница с предыдущим)
 * @var string[]                $taxList     Список уплаченных налогов (названий)
 */
$model = $this->params['dossierContractor'];

$this->title = 'Досье ' . $model->getShortName(true);
echo $this->render('_menu');
if ($fs !== null) {
    $yearList = implode(', ', $fs->yearList(true));
}
?>

<div class="wrap">

        <div class="caption">
            Финансы за <?= $fs->getYear() ?> год
        </div>

        <div class="portlet-body accounts-list finance">

            <div class="content-frame__header">
                <div class="content-frame__description">Сведения на основании данных Росстат и ФНС.</div>
            </div>

            <div class="finance-header">Основные показатели</div>

            <div class="finance-list">
                <div class="fl-block">
                    <div class="fl-title">Выручка</div>
                    <div class="fl-text"><?= $fs::amountFormat($revenue[0], null, '0') ?></div>
                    <?php if ($revenue[1]) { ?>
                        <div class="finance-changes <?= $revenue[1] > 0 ? 'mt' : 'lt' ?>"><?= $fs::amountFormat($revenue[1],
                                true) ?></div>
                    <?php } ?>
                </div>

                <div class="fl-block">
                    <div class="fl-title">Прибыль</div>
                    <div class="fl-text"><?= $fs::amountFormat($profit[0], null, '0') ?></div>
                    <?php if ($profit[1]) { ?>
                        <div class="finance-changes <?= $profit[1] > 0 ? 'mt' : 'lt' ?>">
                            <?= $fs::amountFormat($profit[1], true) ?>
                        </div>
                    <?php } ?>
                </div>

                <div class="fl-block">
                    <div class="fl-title">Сумма расходов</div>
                    <div class="fl-text"><?= $fs::amountFormat($expenses[0], null, '0') ?></div>
                    <?php if ($expenses[1]) { ?>
                        <div class="finance-changes <?= $expenses[1] > 0 ? 'mt' : 'lt' ?>">
                            <?= $fs::amountFormat($expenses[1], true) ?>
                        </div>
                    <?php } ?>
                </div>
                <?php /*
            <div class="fl-block">
                <div class="fl-title" style="color:red">Стоимость</div>
                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
*/ ?>
                <div class="fl-block">
                    <div class="fl-title">Уставный капитал</div>
                    <div class="fl-text"><?= $fs::amountFormat($card->СумКап ?: 0.0) ?></div>
                    <div class="finance-changes">
                        <span class="num">0</span> ₽
                    </div>
                </div>
                <?php /*
            <div class="fl-block">
                <div class="fl-title" style="color:red">Налоговая нагрузка</div>
                <div class="fl-text"><span class="num">1,4 %</span></div>
            </div>
*/ ?>
                <div class="fl-block">
                    <div class="fl-title">Основные средства</div>

                    <div class="fl-text"><?= $fs::amountFormat($fixedAssets[0], null, '0') ?></div>
                    <?php if ($fixedAssets[1]) { ?>
                        <div class="finance-changes <?= $fixedAssets[1] > 0 ? 'mt' : 'lt' ?>">
                            <?= $fs::amountFormat($fixedAssets[1], true) ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="fl-block">
                    <div class="fl-title">Дебиторская задолженность</div>

                    <div class="fl-text"><?= $fs::amountFormat($receivables[0], null, '0') ?></div>
                    <?php if ($receivables[1]) { ?>
                        <div class="finance-changes <?= $receivables[1] > 0 ? 'mt' : 'lt' ?>">
                            <?= $fs::amountFormat($receivables[1], true) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php if (empty($yearList) === false) { ?>
                <a href="<?= Url::toRoute([
                    'dossier/accounting',
                    'id' => $model->id,
                ]) ?>" class="see-details arrowfix" rel="nofollow">Смотреть
                    бухгалтерскую отчетность
                    за <span><?= $yearList ?></span></a>
            <?php } ?>

            <div class="finance-header">Уплачено налогов</div>

            <div class="finance-list">
                <?php foreach ($taxList as $tax) { ?>
                    <div class="fl-block">
                        <div class="fl-title"><?= constant(Card::class . '::TAX_' . strtoupper($tax) . '_TITLE') ?></div>
                        <div class="fl-text">
                            <span class="num"><?= number_format($card->tax[$tax], 0, '.', ' ') ?></span> ₽
                        </div>
                    </div>
                <?php } ?>
                <?php /*
            <div class="fl-block">
                <div class="fl-title" style="color:red">Налог на добавленную стоимость</div>
                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
*/ ?>
                <?php /*
            <div class="fl-block">
                <div class="fl-title" style="color:red">Неналоговые доходы, администрируемые налоговыми органами</div>
                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
*/ ?>
            </div>

            <div class="finance-header">Уплачено взносов</div>
            <div class="finance-list">
                <?php foreach ([$card::TAX_PENSION, $card::TAX_MEDIC, $card::TAX_DISABILITY] as $tax) { ?>
                    <div class="fl-block">
                        <div class="fl-title"><?= constant(Card::class . '::TAX_' . strtoupper($tax) . '_TITLE') ?></div>
                        <div class="fl-text">
                            <span class="num"><?= number_format($card->tax[$tax], 0, '.', ' ') ?></span> ₽
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="finance-header">Пени и штрафы по налогам</div>
            <?php $arrears = $card->arrearsTotal(); ?>
            <div class="finance-list">
                <div class="fl-block">
                    <div class="fl-title">Недоимки</div>
                    <div class="fl-text">
                        <span class="num"><?= number_format($arrears['amount'], 0, '.', ' ') ?></span> ₽
                    </div>
                </div>
                <div class="fl-block">
                    <div class="fl-title">Пени</div>
                    <div class="fl-text">
                        <span class="num"><?= number_format($arrears['peni'], 0, '.', ' ') ?></span> ₽
                    </div>
                </div>
                <div class="fl-block">
                    <div class="fl-title">Штрафы</div>
                    <div class="fl-text">
                        <span class="num"><?= number_format($arrears['fine'], 0, '.', ' ') ?></span> ₽
                    </div>
                </div>
            </div>

        </div>
    </div>
