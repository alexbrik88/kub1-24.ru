<?php
/** @var array $tariffList */
/** @var bool $isAllowedDossier */

$this->title = 'Досье';
echo $this->render('_menu');

?>
<div class="wrap">
    <div class="caption">Проверка контрагентов</div>
    <div class="accounts-list">

        <div class="t-subtitle">
            Вы уверены, что клиент вам заплатит, а поставщик не пропадет с вашими деньгами?<br/>
            Получите полную информацию о ваших контрагентах:
        </div>
        <div class="bg_man_in_lens">
            <br/>
            <br/>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="t-tile">
                        <div class="title">
                            <div class="img img-width-1"><img src="/img/dossier/1.png"/></div>
                            <div class="txt"><b>ДАННЫЕ</b><br/>по контрагенту</div>
                        </div>
                        <div class="list">
                            <ul>
                                <li>Полная информация о контрагенте</li>
                                <li>Актуальные данные из ЕГРЮЛ</li>
                                <li>Виды деятельности</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="t-tile">
                        <div class="title">
                            <div class="img img-width-2"><img src="/img/dossier/2.png"/></div>
                            <div class="txt"><b>МАССОВЫЙ</b><br/>директор и учредитель</div>
                        </div>
                        <div class="list">
                            <ul>
                                <li>Данные о наличии руководителя в реестре массовых директоров</li>
                                <li>Данные о наличии учредителя в реестре массовых учредителей</li>
                                <li>Данные по компаниям, где совпадают учредители и генеральные директора</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="t-tile">
                        <div class="title">
                            <div class="img img-width-2"><img src="/img/dossier/3.png"/></div>
                            <div class="txt"><b>ФИНАНСОВЫЕ</b><br/>отчеты</div>
                        </div>
                        <div class="list">
                            <ul>
                                <li>Выручка, прибыль, кол-во сотрудников</li>
                                <li>Уплаченные налоги и взносы</li>
                                <li>Бухгалтерский баланс и другие отчеты</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="t-tile">
                        <div class="title">
                            <div class="img img-width-2"><img src="/img/dossier/4.png"/></div>
                            <div class="txt"><b>РИСК</b><br/>неисполнения обязательств</div>
                        </div>
                        <div class="list">
                            <ul>
                                <li>Суды в роли ответчика, истца или третьего лица</li>
                                <li>Судебные производства о банкротстве или ликвидации</li>
                                <li>Исполнительные производства</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-sm-4 col-xs-6" style="padding-right: 5px">
                <div class="st-tile bg_man_1">
                    <div class="title">ДАННЫЕ</div>
                    <div class="subtitle">предоставляются из открытых источников</div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6" style="padding-right: 5px; padding-left: 5px;">
                <div class="st-tile bg_woman_2">
                    <div class="title">12 МЕСЯЦЕВ</div>
                    <div class="subtitle">срок действия оплаченных проверок<br/><span style="white-space: nowrap">Не использованные</span> - обнулятся</div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6" style="padding-left: 5px">
                <div class="st-tile bg_woman_3">
                    <div class="title">ДОСТУПНО</div>
                    <div class="subtitle">для всех компаний в вашем аккаунте</div>
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <div class="row row-5ths">
            <?php foreach ($tariffList as $tariff): ?>
            <div class="col-x5">
                <div class="tariff-block <?= $tariff['hit'] ? 'hit' : '' ?>">
                    <?php if ($tariff['hit']): ?>
                        <div class="hit">ХИТ</div>
                    <?php endif; ?>
                    <div class="header">
                        <?= \php_rutils\RUtils::numeral()->getPlural($tariff['count'], ['проверку', 'проверки', 'проверок']) ?>
                    </div>
                    <div class="body">
                        <div class="price">
                            <?= \common\components\TextHelper::numberFormat($tariff['price']); ?> ₽
                        </div>
                        <div class="average_price">
                            <span class="small top-left">₽</span>
                            <?= \common\components\TextHelper::numberFormat(($tariff['count']) ? round($tariff['price'] / $tariff['count']) : 0)  ?>
                            <span class="small">/проверка</span>
                        </div>
                    </div>
                    <div class="footer">
                        <button class="button-clr button-regular <?= ($tariff['hit']) ? 'button-hover-transparent' : 'button-regular_red' ?>">Оплатить</button>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php if (empty($isAllowedDossier)): ?>
<script>
    $(document).ready(function() {
        $('.nav-dossier .nav-link').attr('href' , 'javascript:void(0)');
    });
</script>
<?php endif; ?>