<?php

use frontend\assets\ContractorDossierAsset;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

ContractorDossierAsset::register($this);

echo $this->render('_style');

/**
 * @var int $type
 */
$type = Yii::$app->request->get('type');
$id = Yii::$app->request->get('id');
$tab = Yii::$app->request->get('tab', 'index');

/** @noinspection PhpUnhandledExceptionInspection */
echo Nav::widget([
    'items' => [
        [
            'label' => 'Главная',
            'url' => ['/dossier', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ((!$tab || $tab == 'index') ? ' active' : '')],
            'active' => $tab == 'index',
        ],
        [
            'label' => 'Учредители',
            'url' => ['/dossier/founders', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'founders' ? ' active' : '')],
            'active' => $tab == 'founders'
        ],
        [
            'label' => 'Связи',
            'url' => ['/dossier/connections', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'connections' ? ' active' : '')],
            'active' => $tab == 'connections'
        ],
        [
            'label' => 'Надежность',
            'url' => ['/dossier/reliability', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'reliability' ? ' active' : '')],
            'active' => $tab == 'reliability'
        ],
        [
            'label' => 'Финансы',
            'url' => ['/dossier/finance', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'finance' ? ' active' : '')],
            'active' => $tab == 'finance'
        ],
        [
            'label' => 'Суды',
            'url' => ['/dossier/arbitr', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'arbitr' ? ' active' : '')],
            'active' => $tab == 'arbitr'
        ],
        [
            'label' => 'Долги',
            'url' => ['/dossier/debt', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'debt' ? ' active' : '')],
            'active' => $tab == 'debt'
        ],
        [
            'label' => 'Виды деят.',
            'url' => ['/dossier/okved', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'okved' ? ' active' : '')],
            'active' => $tab == 'okved'
        ],
        [
            'label' => 'Отчетность',
            'url' => ['/dossier/accounting', 'id' => $id],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'accounting' ? ' active' : '')],
            'active' => $tab == 'accounting'
        ],
    ],
    'options' => ['class' => 'nav-dossier nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
]);
