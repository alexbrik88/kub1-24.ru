<?php

use common\components\zchb\FinancialStatement;
use yii\base\InvalidArgumentException;
/**
 * @var FinancialStatement $fs
 * @var string             $title
 * @var int[]              $yearList
 * @var int|string         $indicator
 */
try {
    $code = is_int($indicator) === true ? $indicator : $fs::path2Code($indicator);
} catch (InvalidArgumentException $e) {
    $code='';
    die($e->getMessage());
}
?>
<tr>
    <td><?= $title ?></td>
    <td><?= $code ?></td>
    <?php
    tableClass(true);
    foreach ($yearList as $item) { ?>
        <td class="has-value <?= tableClass() ?>">
            <p class="dark-text"><?= number_format(round((int)$fs->valueOf($indicator, $item) / 1000, 2),0,'.',' ') ?></p>
        </td>
    <?php } ?>
</tr>