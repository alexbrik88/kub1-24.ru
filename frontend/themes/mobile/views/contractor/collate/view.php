<?php

use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<?= $this->render('pdf-view', ['model' => $model]) ?>

<div class="pt-3 d-flex justify-content-between">
    <?= Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', [
        '/contractor/collate',
        'step' => 'send',
        'id' => $model->contractor->id,
        'accounting' => $model->accounting,
        'document' => $model->document,
        'from' => $model->dateFrom,
        'till' => $model->dateTill,
    ], [
        'class' => 'button-width button-clr button-regular button-hover-transparent collate-pjax-link',
        'title' => 'Отправить',
    ]) ?>

    <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
        '/contractor/collate',
        'step' => 'print',
        'id' => $model->contractor->id,
        'accounting' => $model->accounting,
        'document' => $model->document,
        'from' => $model->dateFrom,
        'till' => $model->dateTill,
    ], [
        'class' => 'button-width button-clr button-regular button-hover-transparent',
        'title' => 'Печать',
        'target' => '_blank'
    ]) ?>

    <div class="dropup">
        <?= Html::a($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', '#', [
            'class' => 'button-width button-clr button-regular button-hover-transparent',
            'data-toggle' => 'dropdown',
            'aria-expanded' => 'false'
        ]); ?>
        <?= Dropdown::widget([
            'options' => [
                'class' => 'dropdown-menu form-filter-list list-clr'
            ],
            'items' => [
                [
                    'label' => 'PDF файл',
                    'encode' => false,
                    'url' => [
                        '/contractor/collate',
                        'step' => 'pdf',
                        'id' => $model->contractor->id,
                        'accounting' => $model->accounting,
                        'document' => $model->document,
                        'from' => $model->dateFrom,
                        'till' => $model->dateTill,
                    ],
                    'linkOptions' => [
                        'target' => '_blank',
                    ]
                ],
                [
                    'label' => 'Excel файл',
                    'encode' => false,
                    'url' => [
                        '/contractor/collate',
                        'step' => 'excel',
                        'id' => $model->contractor->id,
                        'accounting' => $model->accounting,
                        'document' => $model->document,
                        'from' => $model->dateFrom,
                        'till' => $model->dateTill,
                    ],
                    'linkOptions' => [
                        'target' => '_blank',
                    ],
                ],
            ],
        ]);?>
    </div>

    <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']).'<span>Отменить</span>', [
        'class' => 'button-clr button-regular button-width button-hover-transparent',
        'data-dismiss' => 'modal',
    ]); ?>
</div>

<?php $pjax->end(); ?>
