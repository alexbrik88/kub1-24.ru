<?php

use common\components\date\DateHelper;
use frontend\models\CollateForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$templateDateInput = "<div class='date-picker-wrap'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";
?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<h4 class="modal-title">Акт сверки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'collate-form',
    'action' => ['/contractor/collate', 'step' => 'form', 'id' => $model->contractor->id],
    'class' => 'form-horizontal',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => [
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group mb-0'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

<?= $form->field($model, 'accounting', [
    'labelOptions' => [
        'class' => 'label mb-3',
    ],
])->radioList(CollateForm::$accountingItems, [
    'class' => '',
    'style' => 'margin-left:-15px',
    'item' => function ($index, $label, $name, $checked, $value) {
        return Html::radio($name, $checked, [
            'value' => $value,
            'label' => $label,
            'labelOptions' => [
                'class' => 'form-group column mb-3 pb-3 radio-inline',
            ],
        ]);
    },
]); ?>

<?= $form->field($model, 'document', [
    'labelOptions' => [
        'class' => 'label mb-3',
    ],
])->radioList($model->documentItems, [
    'class' => '',
    'style' => 'margin-left:-15px',
    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
        $disabled = !in_array((int) $value, $model->allowedDocIdArray);
        $isUpd = $value == CollateForm::DOC_UPD;
        return Html::radio($name, $checked, [
            'value' => $value,
            'label' => $label,
            'disabled' => $disabled,
            'labelOptions' => [
                'class' => 'form-group column mb-3 pb-3 radio-inline' . (($disabled && $isUpd) ? ' no-upd' : ''),
                'data-tooltip-content' => ($disabled && $isUpd) ? '#tooltip_doc_upd' : null,
            ],
        ]);
    },
]); ?>

<div class="form-group">
    <div class="row">
        <div class="column">
            <label class="label">Начало периода</label>
            <?= $form->field($model, 'dateFrom', [
                'template' => $templateDateInput,
            ])->textInput(['class' => 'form-control form-control_small date-picker']) ?>
        </div>
        <div class="column">
            <label class="label">Конец периода</label>
            <?= $form->field($model, 'dateTill', [
                'template' => $templateDateInput,
            ])->textInput(['class' => 'form-control form-control_small date-picker']) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="label">Дата подписания акта</label>
    <div class="row">
        <div class="column">
            <?= $form->field($model, 'dateSigned', [
                'template' => $templateDateInput,
            ])->textInput(['class' => 'form-control form-control_small date-picker']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div class="pt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сформировать', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'form' => 'collate-form',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-regular button-width button-hover-transparent',
        'data-dismiss' => 'modal',
    ]); ?>
</div>

<script type="text/javascript">
$(".radio-inline.no-upd").tooltipster({
    "theme":["tooltipster-noir","tooltipster-noir-customized"],
    "trigger":"click",
    "contentAsHTML":true,
    "contentCloning": true
});
</script>

<?php $pjax->end(); ?>

<style>
    .date-picker-icon {
        right:unset;
        left: 104px;
    }
</style>