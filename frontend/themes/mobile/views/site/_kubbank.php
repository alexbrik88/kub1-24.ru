<?php

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $view string */
/* @var $typeItems array */

$class = 'nav-link form-toggle-link';
?>

<div class="page-in row">
    <div class="page-in-content column">
        <div class="row">
            <div class="col-6 pb-4">
                <div class="wrap kubbank-tile d-flex flex-column ">
                    <div class="kubbank-tile-head">
                        <span class="notif-marker notif-marker_red">
                            <?= $this->render('//layouts/svg/invoicing') ?>
                        </span>
                        <h5 class="pl-3 mt-2">
                            Выставление счетов
                        </h5>
                    </div>
                    <div class="kubbank-tile-content">
                        <span class="font-weight-bold">
                            Простое выставление счетов.
                        </span>
                        <br>
                        Заполнение реквизитов по ИНН.
                        Все формулы без ошибок.
                        Добавление логотипа, печати и подписи.
                        <br>
                        <br>
                        <span class="font-weight-bold mt-2">
                            В одном онлайн сервисе, всё, что нужно для бизнеса:
                        </span>
                        Счета, Акты, Товарные накладные, УПД, Счета-фактуры, Доверенности и Договора.
                        Все документы формируется в 2 клика или автоматически.
                    </div>
                </div>
            </div>
            <div class="col-6 pb-4">
                <div class="wrap kubbank-tile d-flex flex-column ">
                    <div class="kubbank-tile-head">
                        <span class="notif-marker notif-marker_red">
                            <?= $this->render('//svg-sprite', ['ico' => 'mix']) ?>
                        </span>
                        <h5 class="pl-3 mt-2">
                            Бухгалтерия ИП
                        </h5>
                    </div>
                    <div class="kubbank-tile-content">
                        Полностью автоматизированная
                        <br>
                        <span class="font-weight-bold">
                            бухгалтерия для ИП
                        </span>
                        на УСН 6%.
                        <br>
                        Пользователь получает декларацию, расчет налогов и советы по законному уменьшению налогов.
                        <br>
                        <br>
                        <span class="font-weight-bold">
                            За 5 минут:
                        </span>
                        расчет налогов, подготовка платежек в налоговую и налоговая декларация.
                    </div>
                </div>
            </div>
            <div class="col-6 pb-4">
                <div class="wrap kubbank-tile d-flex flex-column ">
                    <div class="kubbank-tile-head">
                        <span class="notif-marker notif-marker_red">
                            <?= $this->render('//layouts/svg/analytics') ?>
                        </span>
                        <h5 class="pl-3 mt-2">
                            ФинДиректор
                        </h5>
                    </div>
                    <div class="kubbank-tile-content">
                        Финансовые отчеты онлайн, помогают контролировать бизнес и принимать правильные бизнес решения.
                        Сервис позволяет настроить автоматический сбор данных по бизнесу из банка,
                        ОФД, 1С, СРМ, Excel, интернет-магазина и т.д.
                        <br>
                        <br>
                        На основе собранных данных строятся финансовые отчеты,
                        такие как ОДДС, ОПиУ, ПЛАН-ФАКТ, Баланс, Точка безубыточности и т.д.
                        Отчеты по бизнесу не только строятся автоматически,
                        но робот-консультант дает комментарии и советы по ним.
                    </div>
                </div>
            </div>
            <div class="col-6 pb-4">
                <div class="wrap kubbank-tile d-flex flex-column ">
                    <div class="kubbank-tile-head">
                        <span class="notif-marker notif-marker_red">
                            <?= $this->render('//svg-sprite', ['ico' => 'bank']) ?>
                        </span>
                        <h5 class="pl-3 mt-2">
                            Бизнес платежи
                        </h5>
                    </div>
                    <div class="kubbank-tile-content">
                        Выставлять счета на вашем сайте Счёт создает сам покупатель на вашем сайте.
                        Информация заполняется по ИНН.
                        Счёт в электронном виде.
                        Оплата счёта сразу из счёта.
                        <br>
                        <br>
                        Оплачивать счета на вашем сайте
                        <br>
                        Электронный счёт передается в банк покупателя и на его основе автоматически создается платежка.
                        Покупателю нужно только подписать платежку.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-in-sidebar column">
        <div class="wrap">
            <?php Pjax::begin([
                'id' => 'auth-form-toggle-pjax',
                'linkSelector' => '.form-toggle-link'
            ]) ?>

            <ul class="nav mb-4 nav-tabs justify-content-center" style="border-bottom: 1px solid #dee2e6;">
                <li class="nav-item">
                    <?= Html::a('<span class="h6">Вход</span>', ['login'], [
                        'class' => $class . ($view == '_login' ? ' active' : ''),
                    ]) ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('<span class="h6">Регистрация</span>', ['registration'], [
                        'class' => $class . ($view == '_registration' ? ' active' : ''),
                    ]) ?>
                </li>
            </ul>

            <?= $this->render($view, [
                'model' => $model,
                'typeItems' => $typeItems,
            ]) ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>
