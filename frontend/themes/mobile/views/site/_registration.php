<?php

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

?>

<?= \frontend\widgets\Alert::widget(); ?>

<?php $form = \yii\bootstrap4\ActiveForm::begin([
    'id' => 'registration-form',
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnSubmit' => true,
]); ?>

<?= $form->field($model, 'email')->textInput(); ?>

<div class="form-group">
    <?= $form->field($model, 'companyType', [
        'options' => [
            'class' => 'company-type-chooser',
        ],
    ])->radioList($typeItems); ?>
</div>

<?= $form->field($model, 'taxationTypeOsno', [
    'parts' => [
        '{input}' => Html::tag(
            'div',
            Html::activeCheckbox($model, 'taxationTypeOsno', [
                'labelOptions' => ['class' => 'label'],
            ]) . '<br>' .
            Html::activeCheckbox($model, 'taxationTypeUsn', [
                'labelOptions' => ['class' => 'label'],
            ]) . '<br>' .
            Html::activeCheckbox($model, 'taxationTypeEnvd', [
                'labelOptions' => ['class' => 'label'],
            ]) . '<br>' .
            Html::activeCheckbox($model, 'taxationTypePsn', [
                'labelOptions' => ['class' => 'label'],
                'disabled' => true,
            ])
        )
    ]
])->label('Система налогобложения')->render(); ?>

<?= \yii\helpers\Html::submitButton('Попробовать бесплатно', [
    'class' => 'button-regular button-regular_red w-100',
]); ?>

<div class="label mt-4">
    Регистрируясь в системе, Вы принимаете условия
    <a href="#" target="_blank">лицензионного соглашения</a>
</div>

<?php $form->end(); ?>
