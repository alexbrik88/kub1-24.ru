<?php

use backend\models\Bank;
use common\components\grid\GridView;
use common\components\ImageHelper;
use frontend\rbac\UserRole;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\company\CheckingAccountantSearch */
/* @var $banks backend\models\Bank[] */

$banks = Bank::find()->andWhere([
    'is_special_offer' => true,
    'is_blocked' => false,
])->all();

$banksOrdered = [];
if ($banks) {
    // custom order banks
    $bikOrder = [
        '044525225', // Sberbank
        '044525974', // Tinkoff
        '044525985', // Otkrytie
        '044525092', // Modulbank
        '044525999', // Tochka
        '044525593'  // Alphabank
    ];
    foreach ($bikOrder as $bik) {
        foreach ($banks as $key => $bank) {
            if ($bik == $bank->bik) {
                $banksOrdered[] = $bank;
                unset($banks[$key]);
                continue;
            }
        }
    }
    foreach ($banks as $key => $bank) {
        $banksOrdered[] = $bank;
    }

    unset($banks);
}
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">

            <?php \yii\widgets\Pjax::begin([
                'id' => 'rs-pjax-container',
                'enablePushState' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'bik',
                        'label' => 'БИК',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->bik;
                        },
                    ],
                    [
                        'attribute' => 'ks',
                        'label' => 'К/с',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->ks;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Р/с',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->typeText[$model->type];
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>

            <?php if ($banksOrdered && Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                <div class="pt-3 pb-1">
                        <div class="col-12">
                            <div class="label weight-700 mb-3" style="margin-left: -8px;">Открыть расчетный счет со скидкой</div>
                            <div class="row">
                            <?php foreach ($banksOrdered as $key => $bank) : ?>
                            <?php $dir = $bank->getUploadDirectory() . $bank->logo_link; ?>
                            <div class="col-3 mb-3" style="padding: 0 8px;">
                                <div class="page-border page-border_width_2 page-border_grey">
                                    <?= ImageHelper::getThumb($dir, [246, 146], [
                                        'style' => 'cursor: pointer;',
                                        'data-toggle' => 'modal',
                                        'href' => '#apply-to-the-bank-' . $bank->id,
                                        'alt' => '',
                                    ]) ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>



