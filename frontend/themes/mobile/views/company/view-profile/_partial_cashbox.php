<?php

use common\models\company\CheckingAccountant;
use common\models\EmployeeCompany;
use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\models\CashboxSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new CashboxSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <div class="table-container">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'cashbox-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",

                        'columns' => [
                            [
                                'attribute' => 'name',
                                'header' => 'Название',
                                'headerOptions' => [
                                ],
                            ],
                            [
                                'attribute' => 'responsible_employee_id',
                                'class' => DropDownSearchDataColumn::class,
                                'headerOptions' => [
                                ],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $e = EmployeeCompany::find()->andWhere([
                                        'employee_id' => $model->responsible_employee_id,
                                        'company_id' => $model->company_id,
                                    ])->one();

                                    return $e ? $e->getFio(true) : '';
                                },
                            ],
                            [
                                'attribute' => 'is_accounting',
                                'format' => 'boolean',
                            ],
                            [
                                'label' => 'Тип',
                                'value' => function ($model) {
                                    return $model->is_main ? 'Основная' : ($model->is_closed ? 'Закрыта' : 'Дополнительная');
                                },
                            ],
                        ],
                    ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>