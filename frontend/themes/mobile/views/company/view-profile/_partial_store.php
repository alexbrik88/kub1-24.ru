<?php

use common\models\EmployeeCompany;
use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\models\StoreSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new StoreSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <div class="table-container">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'store-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",

                        'columns' => [
                            [
                                'attribute' => 'name',
                                'header' => 'Название',
                                'headerOptions' => [
                                ],
                            ],
                            [
                                'attribute' => 'responsible_employee_id',
                                'class' => DropDownSearchDataColumn::class,
                                'headerOptions' => [
                                ],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $e = EmployeeCompany::find()->andWhere([
                                        'employee_id' => $model->responsible_employee_id,
                                        'company_id' => $model->company_id,
                                    ])->one();

                                    return $e ? $e->getFio(true) : '';
                                },
                            ],
                            [
                                'label' => 'Тип',
                                'value' => function ($model) {
                                    return $model->is_main ? 'Основной' : ($model->is_closed ? 'Закрыт' : 'Дополнительный');
                                },
                            ],
                        ],
                    ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>