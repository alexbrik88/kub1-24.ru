<?php

use common\components\grid\GridView;
use frontend\models\RetailPointSearch;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new RetailPointSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <div class="table-container">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'retail-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",

                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'name',
                                'header' => 'Название',
                            ],
                            'address',
                            'kpp',
                            'login',
                        ],
                    ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>