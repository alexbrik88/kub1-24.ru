<?php
use backend\models\Bank;
use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CompanyType;
use common\models\NdsOsno;
use frontend\models\AffiliateProgramForm;
use frontend\rbac\permissions as rbac;
use frontend\rbac\UserRole;
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $outInvoiceSearch frontend\models\OutInvoiceSearch */
/* @var $outInvoiceProvider yii\data\ActiveDataProvider */

$this->title = 'Профиль компании';
$this->context->layoutWrapperCssClass = 'edit-profile';

$view = $company->company_type_id == CompanyType::TYPE_IP ? 'ip' : 'ooo';
?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <h4 class="column mb-2"><?= $company->shortTitle; ?></h4>
            <div class="column mb-2">
                <?= Html::a('Визитка компании', ['visit-card'], ['class' => 'link']) ?>
            </div>
            <div class="column mb-2 ml-auto">
                <?php if (Yii::$app->user->can(rbac\Company::UPDATE)) {
                    $linkOptions = [
                        'id' => 'company-edit-link',
                        'class' => 'button-list button-hover-transparent button-clr',
                    ];

                    if ($company->strict_mode == Company::ON_STRICT_MODE) {
                        $linkOptions = \yii\helpers\ArrayHelper::merge($linkOptions, [
                            'data' => [
                                'toggle' => 'popover',
                                'placement' => 'left',
                                'content' => 'Внесите все данные по Вашей компании, чтобы использовать все возможности сервиса.',
                                'template' => '<div class="strict-mode-popover popover m-popover" role="tooltip"><div class="arrow"></div><h4 class="popover-title"></h4><div class="popover-content"></div></div>',
                            ],
                        ]);
                        $this->registerJs('$("#company-edit-link").popover("show");');
                    } else {
                        $linkOptions['title'] = 'Редактировать';
                    }

                    echo Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), ['update'], $linkOptions);
                } ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Краткое наименование организации</div>
                <div><?= $company->shortTitle; ?>&nbsp;</div>
            </div>
            <div class="column pb-3 mb-3">
                <div class="label weight-700 mb-3">Полное наименование организации</div>
                <div><?= $company->title; ?>&nbsp;</div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Система налогообложения</div>
                <div><?= $company->companyTaxationType->name; ?>&nbsp;</div>
            </div>
            <?php if (ArrayHelper::getValue($company, 'companyTaxationType.osno')) : ?>
                <div class="col-4 pb-3 mb-3">
                    <div class="label weight-700 mb-3">В счетах цену за товар/услуги указывать</div>
                    <div>
                        <?php if ($company->nds == NdsOsno::WITH_NDS) : ?>
                            НДС включен в цену
                        <?php elseif ($company->nds == NdsOsno::WITHOUT_NDS) : ?>
                            НДС сверху цены
                        <?php endif ?>
                        &nbsp;
                    </div>
                </div>
            <?php endif ?>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Часовой пояс</div>
                <div><?= ArrayHelper::getValue($company, 'timeZone.out_time_zone') ?>&nbsp;</div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Отправлять с печатью и подписью</div>
                <div>
                    <?= join(', ', array_filter([
                        $company->pdf_send_signed ? $company->getAttributeLabel('pdf_send_signed') : '',
                        $company->pdf_act_send_signed ? $company->getAttributeLabel('pdf_act_send_signed') : '',
                    ])) ?>
                    &nbsp;
                </div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Формировать PDF с печатью и подписью</div>
                <div>
                    <?= join(', ', array_filter([
                        $company->pdf_signed ? $company->getAttributeLabel('pdf_signed') : '',
                        $company->pdf_act_signed ? $company->getAttributeLabel('pdf_act_signed') : '',
                    ])) ?>
                    &nbsp;
                </div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">К номеру счета Доп номер</div>
                <div>
                    <?= (isset(Company::$addNumPositions[$company->is_additional_number_before])) ?
                        Company::$addNumPositions[$company->is_additional_number_before] : ''; ?>
                    &nbsp;
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Телефон</div>
                <div>
                    <?= $company->phone; ?>
                </div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Электронная почта</div>
                <div>
                    <?= $company->email; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap pt-2 pl-4 pr-4 pb-0">
    <div class="pt-1 pl-1 pr-2">
        <?= Tabs::widget([
            'options' => [
                'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
            ],
            'items' => [
                [
                    'label' => 'Расчетные счета',
                    'encode' => false,
                    'content' => $this->render('view-profile/_partial_checking_accountant', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'banks' => $banks,
                    ]),
                    'active' => true,
                ],
                [
                    'label' => 'Кассы',
                    'content' => $this->render('view-profile/_partial_cashbox', ['model' => $company]),
                ],
                [
                    'label' => 'E-money',
                    'content' => $this->render('view-profile/_partial_emoney', ['model' => $company]),
                ],
                [
                    'label' => 'Склады',
                    'content' => $this->render('view-profile/_partial_store', ['model' => $company]),
                ],
                [
                    'label' => 'Магазины',
                    'content' => $this->render('view-profile/_partial_retail', ['model' => $company]),
                ],
            ],
        ]); ?>
    </div>
</div>

<?= $this->render("view-profile/_details_{$view}", [
    'company' => $company,
]) ?>

<?= $this->render("view-profile/_partial_ifns", [
    'company' => $company,
]) ?>

<?= $this->render("view-profile/_partial_files", [
    'company' => $company,
]) ?>

<?php foreach ($banks as $bank) {
    echo $this->render('form/modal_bank/_create', [
        'bank' => $bank,
        'applicationToBank' => $applicationToBank,
        'redirectUrl' => Yii::$app->controller->action->id,
    ]);
} ?>