<?php
use common\models\Company;
use common\components\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */

$this->title = 'Визитка компании';
$mainCheckingAccountant = $company->mainCheckingAccountant ? $company->mainCheckingAccountant : new \common\models\company\CheckingAccountant();
?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-3 pl-2">
        <div class="row align-items-center">
            <h4 class="column mb-2"><?= $company->getTitle(true, true); ?></h4>
            <div class="column mb-2">
                <?php if (\Yii::$app->user->can(\frontend\rbac\permissions\Company::PROFILE)): ?>
                    <?= \yii\bootstrap4\Html::a('Профиль компании', ['company/index'], ['class' => 'link']) ?>
                <?php endif; ?>
            </div>
            <div class="column mb-2 ml-auto">
            </div>
        </div>
        <br>
        <table style="border-collapse: separate; margin-left: -0.15rem;">
            <tbody>
            <tr>
                <th>Система налогообложения:</th>
                <td><?= $company->companyTaxationType->name; ?></td>
            </tr>

            <tr><th>&nbsp;</th></tr>

            <tr>
                <th>Юридический адрес:</th>
                <td><?= $company->getAddressLegalFull(); ?></td>
            </tr>
            <tr>
                <th>Фактический адрес:</th>
                <td><?= $company->getAddressActualFull(); ?></td>
            </tr>
            </tbody>

            <tr><th>&nbsp;</th></tr>

            <?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
                <tr>
                    <th><?= $company->getAttributeLabel('egrip'); ?>:</th>
                    <td><?= $company->egrip; ?></td>
                </tr>
                <tr>
                    <th><?= $company->getAttributeLabel('inn'); ?>:</th>
                    <td><?= $company->inn; ?></td>
                </tr>
                <tr>
                    <th><?= $company->getAttributeLabel('okved'); ?>:</th>
                    <td><?= $company->okved; ?></td>
                </tr>
            <?php else: ?>
                <tr>
                    <th><?= $company->getAttributeLabel('ogrn'); ?>:</th>
                    <td><?= $company->ogrn; ?></td>
                </tr>
                <tr>
                    <th><?= $company->getAttributeLabel('inn'); ?>:</th>
                    <td><?= $company->inn; ?></td>
                </tr>
                <tr>
                    <th><?= $company->getAttributeLabel('kpp'); ?>:</th>
                    <td><?= $company->kpp; ?></td>
                </tr>
                <tr>
                    <th><?= $company->getAttributeLabel('okved'); ?>:</th>
                    <td><?= $company->okved; ?></td>
                </tr>
            <?php endif; ?>

            <tr><th>&nbsp;</th></tr>

            <tr>
                <th><?= $mainCheckingAccountant->getAttributeLabel('rs'); ?>:</th>
                <td><?= $mainCheckingAccountant->rs; ?></td>
            </tr>
            <tr>
                <th><?= $mainCheckingAccountant->getAttributeLabel('bank_name'); ?>:</th>
                <td><?= $mainCheckingAccountant->bank_name; ?></td>
            </tr>
            <tr>
                <th><?= $mainCheckingAccountant->getAttributeLabel('ks'); ?>:</th>
                <td><?= $mainCheckingAccountant->ks; ?></td>
            </tr>
            <tr>
                <th><?= $mainCheckingAccountant->getAttributeLabel('bik'); ?>:</th>
                <td><?= $mainCheckingAccountant->bik; ?></td>
            </tr>

            <?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
                <tr><th>&nbsp;</th></tr>

                <tr>
                    <th><?= !empty($company->chief_post_name) ? $company->chief_post_name : 'Генеральный директор'; ?>:</th>
                    <td><?= $company->getChiefFio(); ?></td>
                </tr>
                <tr>
                    <th>Главный бухгалтер:</th>
                    <td><?= $company->getChiefAccountantFio(); ?></td>
                </tr>
            <?php endif; ?>

            <tr><th>&nbsp;</th></tr>

            <tr>
                <th><?= $company->getAttributeLabel('phone'); ?>:</th>
                <td><?= $company->phone; ?></td>
            </tr>

        </table>

    </div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php Modal::begin([
                'options' => [
                    'id' => 'send-visit-card',
                ],
                'header' => '<h4></h4>',
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                ],
            ]);

            echo $this->render('_modal_send_visit_card', [
                'sendForm' => new \frontend\models\VisitCardSendForm,
            ]);

            Modal::end(); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', ['document-print', 'actionType' => 'print'], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'pdf']).'<span>PDF</span>', ['document-print', 'actionType' => 'pdf'], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
    </div>
</div>
