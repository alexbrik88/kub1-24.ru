<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:24
 */

use backend\models\Bank;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\company\ApplicationToBank;
use common\models\Company;

/* @var $bank Bank */
/* @var $checkingAccountantForm yii\bootstrap\ActiveForm */
/* @var $applicationToBank ApplicationToBank */
/* @var $redirectUrl string */

$config = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filte',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$bankForm = ActiveForm::begin([
    'action' => Url::to(['apply-to-bank', 'bankId' => $bank->id, 'redirectAction' => $redirectUrl]),
    'options' => [
        'id' => 'form-apply-bank-' . $bank->id,
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group col-6'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]);
?>
<div class="form-body">
    <?php if ($bank->description): ?>
        <div class="description-bank  mb-3">
            <?= nl2br($bank->description); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <?= $bankForm->field($applicationToBank, 'company_name', $config); ?>

        <?= $bankForm->field($applicationToBank, 'inn', $config); ?>

        <?= $bankForm->field($applicationToBank, 'legal_address', $config); ?>

        <?= $bankForm->field($applicationToBank, 'fio', $config); ?>

        <?= $bankForm->field($applicationToBank, 'contact_phone', $config)->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control contact_phone',
                'placeholder' => '+7(XXX) XXX-XX-XX',
            ],
        ]);; ?>

        <?= $bankForm->field($applicationToBank, 'contact_email', $config); ?>

        <div class="col-12 license-bank mb-3">
            <span>
            <?= 'Нажимая кнопку «Открыть счет», вы даете свое согласие на ',
            Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank']),
            " и отправку этих данных в банк «{$bank->bank_name}»."; ?>
            </span>
        </div>
    </div>

    <div class="d-flex justify-content-between">
        <?= Html::submitButton('Открыть счет', [
            'class' => 'button-regular button-width button-regular_red button-clr',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

</div>
<?php $bankForm->end(); ?>
