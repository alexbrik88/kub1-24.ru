<?php
/* @var $model common\models\Company */
/* @var $creating boolean */

use common\components\helpers\Html;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\TaxationType;
use common\models\TimeZone;
use frontend\models\RegistrationForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $form yii\widgets\ActiveForm */

$inputListConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
];

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');

$companyTypes = array_merge(
    $model->getIsLikeIP() ? [['id' => 0, 'name_short' => 'Самозанятый']] : [],
    CompanyType::find()->inCompany(true)->likeIp($model->getIsLikeIP())->all()
);
$typeList = ArrayHelper::map($companyTypes, 'id', 'name_short');
$templateDateInput = "<div class='date-picker-wrap' style='width:135px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>{error}";
?>

<div class="row align-items-center form-group">
    <div class="col-4 mb-br">
        <?= $form->field($model, 'name_short')->textInput([
            'maxlength' => true,
            'disabled' => $model->getIsLikeIP(),
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ]); ?>
    </div>
    <div class="col-6 mb-br">
        <?= $form->field($model, 'name_full')->textInput([
            'maxlength' => true,
            'disabled' => $model->getIsLikeIP(),
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ]); ?>
    </div>
    <div class="col-2 mb-br">
        <?= $form->field($model, 'company_type_id')->label('Форма')->widget(Select2::class, [
            'data' => $typeList,
            'disabled' => count($companyTypes) === 1,
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%'
            ]
        ]) ?>
    </div>
</div>

<?= $form->field($taxation, 'osno', [
    'parts' => [
        '{input}' => Html::beginTag('div', [
                'class' => 'field-width inp_one_line_company',
            ]) . "\n" .
            Html::activeCheckbox($taxation, 'osno', [
                'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                'disabled' => (boolean) $taxation->usn,
            ]) . "\n" .
            Html::activeCheckbox($taxation, 'usn', [
                'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                'disabled' => (boolean) $taxation->osno,
            ]) . "\n" .
            Html::activeCheckbox($taxation, 'envd', [
                'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
            ]) . "\n" .
            Html::activeCheckbox($taxation, 'psn', [
                'labelOptions' => ['style' => 'margin-right: 15px; cursor: pointer;'],
                'disabled' => $model->company_type_id != CompanyType::TYPE_IP,
            ]) . "\n" .
            Html::beginTag('div', ['class' => 'tax-usn-config collapse' . ($taxation->usn ? ' show' : '')]) . "\n" .
            $form->field($taxation, 'usn_type', [
                'options' => [
                    'class' => 'col-6',
                    'style' => 'margin-bottom: 5px; margin-left: 72px;',
                ],
            ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                'class' => '',
                'style' => 'margin-top: 10px; margin-bottom: -19px;',
                'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                    $item = Html::beginTag('div', ['style' => 'margin-bottom:2px;']);
                    $item .= Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                        'style' => 'display: inline-block; width: 120px; margin-right: 30px;',
                    ]);
                    $item .= Html::activeTextInput($taxation, 'usn_percent', [
                        'class' => 'form-control usn-percent usn-percent-' . $value,
                        'style' => 'display: inline-block; width: 80px;',
                        'disabled' => !$checked,
                        'value' => $checked ? $taxation->usn_percent : $taxation->defaultUsnPercent($value),
                        'data-default' => $taxation->defaultUsnPercent($value),
                    ]) . ' %';
                    $item .= Html::endTag('div');

                    return $item;
                },
            ])->label(false) . "\n" .

            Html::beginTag('div', ['class' => 'tax-patent-config collapse' . ($taxation->psn ? ' show' : '')]) . "\n" .

            Html::beginTag('div', ['class' => 'row', 'style' => 'margin-top:52px; margin-bottom:-25px']) .

            Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-4']) .
                Html::label('Субъект РФ, в котором получен патент', 'ip_patent_city', [
                    'class'=>'label',
                    'style' => '']) .
                Html::activeTextInput($model, 'ip_patent_city', [
                    'class' => 'form-control',
                    'style' => 'margin-left:0; max-width:270px',
                ]) .
            Html::endTag('div') .

            Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-4']) .
                Html::label('Дата начала действия патента', 'patentDateEnd', [
                    'class'=>'label',
                    'style' => '']) .
                $form->field($model, 'patentDate', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'stylw' => 'width: 135px'
                ]) .
            Html::endTag('div') .

            Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-4']) .
                Html::label('Дата окончания действия патента', 'patentDateEnd', [
                    'class'=>'label',
                    'style' => '']) .
                $form->field($model, 'patentDateEnd', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'stylw' => 'width: 135px'
                ]) .
            Html::endTag('div') .

            Html::endTag('div') .
            Html::endTag('div') .
            Html::endTag('div') . "\n" .

            Html::endTag('div'),
    ],
])->label('Система налогообложения'); ?>
<br>
<div class="nds-view-osno collapse <?= $taxation->osno ? ' show' : ''; ?>">
    <div class="mb-3">
        <?= $form->field($model, 'nds')->radioList($ndsViewOsno, [
            'class' => 'inp_one_line_company',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, [
                    'value' => $value,
                ]) . $label, [
                    'class' => 'label',
                    'style' => 'margin-right: 10px;',
                ]) . Html::a('Пример счета', [
                    '/company/view-example-invoice',
                    'nds' => $value == 1 ? true : false,
                ], [
                    'class' => 'link',
                    'target' => '_blank',
                    'style' => 'margin-right: 20px;',
                ]);
            },
        ])->label('В счетах цену за товар/услугу указывать') ?>
    </div>
</div>
<br>
<div class="row align-items-top">
    <div class="col-4">
        <label class="label">
            Формировать PDF с <?= $model->self_employed ? '' : 'печатью и'; ?> подписью
        </label>
        <div class="form-filter">
            <?= Html::activeCheckbox($model, 'pdf_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
    <div class="col-4">
        <label class="label">
            Отправлять с <?= $model->self_employed ? '' : 'печатью и'; ?> подписью
        </label>
        <div class="form-filter">
            <?= Html::activeCheckbox($model, 'pdf_send_signed', [
                'labelOptions' => ['style' => 'width: 110px;'],
                'disabled' => true,
            ]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_send_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
    <div class="col-4">
        <?= $form->field($model, 'is_additional_number_before')->radioList(Company::$addNumPositions, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                    //'class' => 'label',
                    'style' => 'margin-right: 20px;',
                ]);
            },
        ]) ?>
    </div>
</div>

<div class="row align-items-center">
    <div class="col-4 mb-br">
        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control field-width field-w inp_one_line_company',
                'placeholder' => '+7(XXX) XXX-XX-XX (Нужен для восстановления пароля)',
            ],
        ]); ?>
    </div>
    <div class="col-4 mb-br">
        <?= $form->field($model, 'email')->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-4 mb-br">
        <?= $form->field($model, 'time_zone_id')
            ->widget(Select2::class, [
                'data' => ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone'),
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]) ?>
    </div>
</div>
<br>
<?php
$js = <<<JS
$(document).on("change", "#form-update-company #company-company_type_id", function() {
    var form = this.form;
    $("#company-self_employed", form).val(this.value == '0' ? '1' : '0');
    if (this.value == '1') {
        $("#companytaxationtype-psn", form).prop("disabled", false).uniform("refresh");
    } else {
        $("#companytaxationtype-psn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-osno", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".nds-view-osno", form).collapse("show");
    } else {
        $("#companytaxationtype-usn", form).prop("disabled", false).uniform("refresh");
        $(".nds-view-osno", form).collapse("hide");
        $(".nds-view-osno input[type=radio]", form).prop("checked", false).uniform("refresh");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-usn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-usn-config", form).collapse("show");
    } else {
        $("#companytaxationtype-osno", form).prop("disabled", false).uniform("refresh");
        $(".tax-usn-config", form).collapse("hide");
        $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true).uniform("refresh");
        $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false).uniform("refresh");
        //$("#companytaxationtype-usn_percent", form).val("");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-psn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-patent-config", form).collapse("show");
    } else {
        $("#companytaxationtype-envd", form).prop("disabled", false).uniform("refresh");
        $(".tax-patent-config", form).collapse("hide");
    }
});
$(document).on("change", "#form-update-company #companytaxationtype-usn_type input[type=radio]", function() {
    var form = this.form;
    var value = $('#companytaxationtype-usn_type input[type=radio]:checked', form).val();
    $('#companytaxationtype-usn_type input.usn-percent', form).each(function (i, item) {
        $(item).prop('disabled', true).val($(item).data('default'));
    });
    $('#companytaxationtype-usn_type input.usn-percent-' + value, form).prop('disabled', false);
});
JS;

$this->registerJs($js);
