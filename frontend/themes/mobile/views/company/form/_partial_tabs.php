<?php

use yii\bootstrap4\Tabs;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\company\CheckingAccountantSearch */
/* @var $banks backend\models\Bank[] */

?>

<div class="wrap pt-2 pl-4 pr-4 pb-0">
    <div class="pt-1 pl-1 pr-2">
        <?= Tabs::widget([
            'id' => 'add_tabs',
            'options' => [
                'class' => 'nav-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
            ],
            'items' => [
                [
                    'label' => 'Расчетные счета',
                    'encode' => false,
                    'content' => $this->render('_partial_checking_account', [
                        'model' => $model,
                        'form' => $form,
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'banks' => $banks,
                    ]),
                    'active' => true,
                ],
                [
                    'label' => 'Кассы',
                    'content' => $this->render('_partial_cashbox', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'Склады',
                    'content' => $this->render('_partial_store', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'E-money',
                    'content' => $this->render('_partial_emoney', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
            ],
        ]); ?>
    </div>
</div>
