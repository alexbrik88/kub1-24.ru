<?php

use common\components\grid\GridView;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use frontend\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmployeeCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prompt backend\models\Prompt */

$this->title = 'Сотрудники';
$countEmployee = Employee::find()->where([
    'author_id' => Yii::$app->user->identity->id,
])->count();

$isRemoteEmployee = EmployeeCompany::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'employee_id' => Yii::$app->params['service']['remote_employee_id']
])->exists();
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center switch-remote-employee">
        <h4><?= $this->title; ?></h4>
        <div class="switch">
            <?= Html::checkbox('remote_employee', $isRemoteEmployee, [
                'id' => 'activate_remote_employee',
                'class' => 'switch md-check switch-input input-hidden'
            ]); ?>
            <label class="switch-label line-height-1" for="activate_remote_employee">
                <span class="switch-pseudo">&nbsp;</span>
                <span class="switch-txt ml-3" style="line-height:20px">Включить доступ сотруднику технической поддержки</span>
            </label>
        </div>
        <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Employee::CREATE)): ?>
            <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to(['create']) ?>">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </a>
        <?php endif; ?>
    </div>
</div>

<?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'dataColumnClass' => \common\components\grid\DataColumn::className(),

        'tableOptions' => [
            'class' => 'table table-style table-count-list',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading line-height-1em',
        ],
        //'options' => [
        //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        //],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'attribute' => 'fio',
                'label' => 'Фамилия Имя Отчество',
                'format' => 'html',
                'value' => function (\common\models\EmployeeCompany $data) {
                    $data = $data->employee->getEmployeeCompany()->andWhere(['employee_company.company_id' => Yii::$app->user->identity->company_id])->one();
                    $name = trim($data->fio);
                    if (empty($name)) {
                        $name = '(не задано)';
                    }
                    return (Yii::$app->user->can(\frontend\rbac\permissions\Employee::VIEW))
                        ? Html::a(Html::encode($name), ['view', 'id' => $data->employee_id], ['class' => 'link'])
                        : $name;
                },
            ],

            'position',

            [
                'attribute' => 'employee_role_id',
                'filter' => ArrayHelper::merge(['' => 'Все'], $searchModel->getEmployeeRoleArray()),
                'value' => 'employeeRole.name',
                's2width' => '300px'
            ],

            [
                'attribute' => 'is_working',
                'label' => 'Статус',
                'filter' => $searchModel->getEmployeeStatusArray(),
                'headerOptions' => [
                    'class' => 'dropdown-filter dropdown-left',
                ],
                'value' => function ($data) {
                    return Employee::$status_message[$data->is_working];
                }
            ],
        ],
    ]); ?>

<?php $this->registerJs(<<<JS

$(document).on('change', '#activate_remote_employee', function() {
    $(this).attr('disabled', true);
    var isChecked = $(this).prop('checked');
    $.post('change-remote-employee', {'isChecked': isChecked ? 1:0}, function(data) { /* refresh page */ });
});

JS
); ?>

<?php /* if ($countEmployee == 0) : ?>
    <?= \frontend\widgets\PromptWidget::widget([
        'prompt' => $prompt,
    ]); ?>
<?php endif; */ ?>
