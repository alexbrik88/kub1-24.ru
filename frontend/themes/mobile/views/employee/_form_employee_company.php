<?php

use common\components\date\DateHelper;
use common\models\document\DocumentType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\themes\mobile\helpers\Icon;
use yii\bootstrap4\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use common\components\image\EasyThumbnailImage;

/* @var yii\web\View $this */
/* @var EmployeeCompany $model */

/* @var \yii\bootstrap\ActiveForm $form */
/* @var $completeRegistration boolean */
/* @var $salaryModel \frontend\models\SalaryConfigForm */

$checkboxConfig = [
    'template' => "{input}",
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
    ],
]);

$roleArray = \common\models\employee\EmployeeRole::find()->actual()->orderBy([
    'sort' => SORT_ASC,
])->all();


$showChBxRoles = EmployeeCompany::$addFlowRolesNeedConfig;
$payInvoiceCss = in_array($model->employee_role_id, $showChBxRoles) ? '' : 'hidden';
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'employee-form',
    'method' => 'POST',
    'options' => [
        'class' => '',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
        'data' => [
            'calc-url' => Url::to(['/salary/config-calc', 'id' => $model->employee_id]),
        ]
    ],
    'fieldConfig' => [
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'text-muted',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'template' => "{input}\n{error}\n{hint}",
    ],

    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
])); ?>

    <div class="form-body">

        <div class="wrap pb-3">
            <h4 class="mb-4"><?= $this->title ?></h4>
            <div class="row">
                <div class="col-3">
                    <div class="label">Email</div>
                    <?= $form->field($model, 'email', [
                        'hintOptions' => ['tag' => 'div',],
                    ])->widget(\common\components\widgets\EmployeeTypeahead::className(), [
                        'remoteUrl' => Url::to(['/dictionary/employee']),
                        'related' => [
                            '#' . Html::getInputId($model, 'lastname') => 'lastname',
                            '#' . Html::getInputId($model, 'firstname') => 'firstname',
                            '#' . Html::getInputId($model, 'firstname_initial') => 'firstname_initial',
                            '#' . Html::getInputId($model, 'patronymic') => 'patronymic',
                            '#' . Html::getInputId($model, 'patronymic_initial') => 'patronymic_initial',
                            '#' . Html::getInputId($model, 'time_zone_id') => 'time_zone_id',
                            '#' . Html::getInputId($model, 'sex') => 'sex',
                            '#' . Html::getInputId($model, 'birthday') => 'birthday_format',
                            '#' . Html::getInputId($model, 'date_hiring') => 'date_hiring_format',
                            '#' . Html::getInputId($model, 'date_dismissal') => 'date_dismissal_format',
                            '#' . Html::getInputId($model, 'position') => 'position',
                            '#' . Html::getInputId($model, 'employee_role_id') => 'employee_role_id',
                            '#' . Html::getInputId($model, 'phone') => 'phone',
                        ],
                    ])->textInput([
                        'placeholder' => 'Автозаполнение по email',
                        'disabled' => $model->scenario !== 'create',
                    ]); ?>
                </div>
                <div class="col-3">
                    <div class="label">Телефон</div>
                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="label">Фамилия</div>
                    <?= $form->field($model, 'lastname')->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
                <div class="col-3">
                    <div class="label">Имя</div>
                    <?= $form->field($model, 'firstname')->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
                <div class="col-3">
                    <div class="label">Отчество</div>
                    <?= $form->field($model, 'patronymic')->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
                <div class="col-3" style="padding-top: 4px;">
                    <?= $form->field($model, 'has_no_patronymic', ['labelOptions' => ['class' => 'pt-4 mt-2']])
                        ->label('Нет отчества')
                        ->checkbox(['template' => '{beginLabel}{input}{labelTitle}{endLabel}{error}{hint}'], true); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="label">Должность</div>
                        <?= $form->field($model, 'position')->textInput([
                            'placeholder' => $model->getAttributeLabel('position'),
                        ]); ?>
                </div>
                <div class="col-6">
                    <div class="label">Роль
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => '#tooltip_employye_role',
                        ]) ?>
                    </div>
                    <div style="width: 100%">
                    <?= $form->field($model, 'employee_role_id')->widget(\kartik\select2\Select2::class, [
                        'data' => ArrayHelper::map($roleArray, 'id', 'name'),
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => '',
                            'disabled' => $model->scenario === Employee::SCENARIO_CONTINUE_REGISTRATION,
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]]); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="employee-view wrap pl-2 pr-2 pt-3 pb-2" style="margin-bottom: 20px;">
            <div class="pl-1 pr-1">
                <div class="nav-tabs-row">
                    <?= Tabs::widget([
                        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                        'headerOptions' => ['class' => 'nav-item'],
                        'items' => [
                            [
                                'label' => 'Общая информация',
                                'linkOptions' => ['class' => 'nav-link active'],
                                'content' => $this->render('_tab_general_info', [
                                    'model' => $model,
                                    'form' => $form
                                ]),
                            ],
                            [
                                'label' => 'Паспортные данные',
                                'linkOptions' => ['class' => 'nav-link'],
                                'content' => $this->render('_tab_passport', [
                                    'model' => $model,
                                    'form' => $form
                                ]),
                            ],
                            [
                                'label' => 'Для отчета С3В-М',
                                'linkOptions' => ['class' => 'nav-link'],
                                'content' => $this->render('_tab_c3b', [
                                    'model' => $model,
                                    'form' => $form
                                ]),
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>
<?php \yii\bootstrap\ActiveForm::end(); ?>

<?php Modal::begin([
    'id' => 'modal-employeeSignatureImage',
    'title' => false,
    'toggleButton' => false,
]);
echo $this->render('partial/_partial_files_signature', [
    'model' => $model,
    'attr' => 'signature_file',
]);
Modal::end(); ?>

<?php Modal::begin(['id' => 'delete-company-image']); ?>
    <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить <span></span>?</h4>
    <div class="text-center">
        <?= Html::button('Да', [
            'class' => 'del-company-image-ok button-clr button-regular button-hover-transparent button-width-medium mr-2',
            'data-attr' => ''
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
    </div>
<?php Modal::end(); ?>

    <div class="tooltip_templates" style="display: none">
    <span id="tooltip_employye_role"
          style="display: inline-block;">
        <?php foreach ($roleArray as $role) : ?>
            <b><?= $role->name ?></b> – <?= nl2br($role->description) ?><br>
        <?php endforeach ?>
    </span>
    </div>

<?php $this->registerJs('
var showChBxRoles = ' . json_encode($showChBxRoles) . ';
var roleAccountant = ' . EmployeeRole::ROLE_ACCOUNTANT . ';

$(document).on("change", "#employeecompany-can_sign", function () {
    if ($(this).is(":checked")) {
        $(".employee_signature_element").removeClass("hidden_element");
    } else {
        $(".employee_signature_element").addClass("hidden_element");
    }
});
$(document).on("change", "#employeecompany-employee_role_id", function () {
    var canViewPriceForBuy = $("#employeecompany-can_view_price_for_buy");
    let form = this.form;
    let docAccessInput = $("#employeecompany-document_access_own_only", form);
    if (docAccessInput.length > 0) {
        let value = parseInt(this.value);
        let valOn = docAccessInput.data("default-on");
        let valOff = docAccessInput.data("always-off");
        docAccessInput.prop("checked", valOn.includes(value)).uniform("refresh");
        if (valOff.includes(value)) {
            docAccessInput.prop("checked", false).prop("disabled", true).uniform("refresh");
        } else {
            docAccessInput.prop("disabled", false).uniform("refresh");
        }
    }

    if (showChBxRoles.indexOf($(this).val()*1) === -1) {
        $(".employeecompany-can_invoice_add_flow").addClass("hidden");
        $("#employeecompany-can_invoice_add_flow").prop("checked", false).uniform("refresh");
    } else {
        $(".employeecompany-can_invoice_add_flow").removeClass("hidden");
    }
    if ($(this).val() == roleAccountant) {
        if (!canViewPriceForBuy.is(":checked")) {
            canViewPriceForBuy.prop("checked", true).uniform("refresh");
        }
    } else {
        if (canViewPriceForBuy.is(":checked")) {
            canViewPriceForBuy.prop("checked", false).uniform("refresh");
        }
    }
});

') ?>