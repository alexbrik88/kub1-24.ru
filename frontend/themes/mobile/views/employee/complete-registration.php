<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */

$this->title = 'Добавить сотрудника';
?>
    <div class="employee-create">

        <?= $this->render('_form', [
            'model' => $model,
            'completeRegistration' => true,
        ]); ?>

        <div class="form-actions">
            <div class="row action-buttons" id="buttons-fixed">
                <div class="button-bottom-page col-md-1">
                    <?= Html::submitButton('Продолжить', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'form' => 'employee-form',

                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                        'form' => 'employee-form',
                        'title' => 'Продолжить'
                    ]); ?>
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
                <div class="button-bottom-page col-md-1">
                </div>
            </div>
        </div>

    </div>

<?php echo \common\widgets\AlertModal::widget([
    'id' => 'complete-registration-modal',
    'content' => 'Для продолжения работы в сервисе необходимо заполнить все обязательные поля',
]); ?>

<?php
$js = <<<JS
    $(function () {
        var modalWindow = $('#complete-registration-modal');
        $(document).on('click', 'a', function (e) {
            e.preventDefault();
            modalWindow .modal('show');
        });
    });
JS;
$this->registerJs($js);
?>