<?php
/* @var $model \common\models\EmployeeCompany */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */


if ($model->passport_isRf === null) {
    $model->passport_isRf = 1;
}
?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Паспорт</div>
                    <div><?= $model->passport_isRf ? 'РФ' : 'не РФ' ?></div>
                </div>
                <?php if (!$model->passport_isRf): ?>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Страна</div>
                    <div><?= $model->passport_country ?: '—' ?></div>
                </div>
                <?php endif; ?>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Серия</div>
                    <div><?= $model->passport_series ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Номер</div>
                    <div><?= $model->passport_number ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Дата выдачи</div>
                    <div><?= DateHelper::format($model->passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—' ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Кем выдан</div>
                    <div><?= $model->passport_issued_by ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Код подразделения</div>
                    <div><?= $model->passport_department ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-3">
                    <div class="label weight-700 mb-3">Адрес регистрации</div>
                    <div><?= $model->passport_address ?: '—' ?></div>
                </div>
            </div>
        </div>
    </div>
</div>