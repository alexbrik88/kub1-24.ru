<?php

use common\models\employee\Employee;
use common\models\employee\EmployeeSalary;
use frontend\models\SalaryConfigForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $salaryModel frontend\models\SalaryConfigForm */

$employeeCompany = $salaryModel->getEmployeeCompany();

$fieldConf = [
    'options' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control text-right',
        'style' => 'width: 100%; min-width: 65px;',
    ],
    'template' => "{input}{error}",
];

$dayRange = range(1, 31);
$dayList = ['' => ''] + array_combine($dayRange, $dayRange);
$result = $salaryModel->getFormatCalculatedData();


$form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'employee-form',
    'method' => 'POST',
    'options' => [
        'class' => 'form-horizontal b-shadow-hide',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
        'data' => [
            'calc-url' => Url::to(['/salary/config-calc', 'id' => $model->employee_id]),
        ]
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'text-muted',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
])); ?>


<div class="employee-salary pl-2 pr-2 pt-3">
    <div class="row pl-1 pr-1">

        <div class="pl-2 pr-2">

            <?= Html::hiddenInput('is_save_salary', 0, ['id' => 'is_save_salary']); ?>

            <div class="row">
                <div class="col-12" style="height:60px">
                    <div class="box pull-left"><h4 class="page-title"><?= Html::encode($this->title); ?></h4></div>
                    <div class="pull-right">
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), 'javascript:;', [
                            'class' => 'button-list button-hover-transparent button-clr btn-edit-salary-table',
                            'title' => 'Редактировать',
                        ]); ?>
                    </div>
                </div>
                <div class="column mb-3 pb-3">

                    <table class="table table-style table-count-list employee-salary-table">
                        <thead>
                        <tr class="heading">
                            <th rowspan="2"></th>
                            <th class="text-center" rowspan="2" style="width: 80px">Вид расчета</th>
                            <th class="text-center" rowspan="2">Сумма на руки (руб.)</th>
                            <th class="text-center" rowspan="2">НДФЛ</th>
                            <th class="text-center" rowspan="2">Соц. налоги</th>
                            <th class="text-center" colspan="2" style="border: 1px solid #ddd;">Аванс</th>
                            <th class="text-center" colspan="2" style="border: 1px solid #ddd;">Зарплата</th>
                            <th class="text-center" rowspan="2">Итого затраты для компании</th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center">Дата</th>
                            <th class="text-center">Сумма</th>
                            <th class="text-center">Дата</th>
                            <th class="text-center">Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?= $form->field($salaryModel, 'has_salary_1', $fieldConf)->checkbox(['label' => false]) ?>
                            </td>
                            <td>
                                Оклад 1
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary1Amount', $fieldConf)->textInput() ?>
                            </td>
                            <td class="text-center">
                                <?= EmployeeSalary::TAX_NDFL ?>%
                            </td>
                            <td class="text-center">
                                <?= EmployeeSalary::TAX_SOCIAL ?>%
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary_1_prepay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary1PrepaySum', $fieldConf)->textInput() ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary_1_pay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td class="text-right nowrap salary_1_pay_sum">
                                <?= $result['salary_1_pay_sum'] ?>
                            </td>
                            <td class="text-right nowrap salary_1_expenses">
                                <?= $result['salary_1_expenses'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $form->field($salaryModel, 'has_bonus_1', $fieldConf)->checkbox(['label' => false]) ?>
                            </td>
                            <td>
                                Премия 1
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus1Amount', $fieldConf)->textInput() ?>
                            </td>
                            <td class="text-center">
                                <?= EmployeeSalary::TAX_NDFL ?>%
                            </td>
                            <td class="text-center">
                                <?= EmployeeSalary::TAX_SOCIAL ?>%
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus_1_prepay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus1PrepaySum', $fieldConf)->textInput() ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus_1_pay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td class="text-right nowrap bonus_1_pay_sum">
                                <?= $result['bonus_1_pay_sum'] ?>
                            </td>
                            <td class="text-right nowrap bonus_1_expenses">
                                <?= $result['bonus_1_expenses'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $form->field($salaryModel, 'has_salary_2', $fieldConf)->checkbox(['label' => false]) ?>
                            </td>
                            <td>
                                Оклад 2
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary2Amount', $fieldConf)->textInput() ?>
                            </td>
                            <td class="text-center">
                                -
                            </td>
                            <td class="text-center">
                                -
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary_2_prepay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary2PrepaySum', $fieldConf)->textInput() ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'salary_2_pay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td class="text-right nowrap salary_2_pay_sum">
                                <?= $result['salary_2_pay_sum'] ?>
                            </td>
                            <td class="text-right nowrap salary_2_expenses">
                                <?= $result['salary_2_expenses'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $form->field($salaryModel, 'has_bonus_2', $fieldConf)->checkbox(['label' => false]) ?>
                            </td>
                            <td>
                                Премия 2
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus2Amount', $fieldConf)->textInput() ?>
                            </td>
                            <td class="text-center">
                                -
                            </td>
                            <td class="text-center">
                                -
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus_2_prepay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus2PrepaySum', $fieldConf)->textInput() ?>
                            </td>
                            <td>
                                <?= $form->field($salaryModel, 'bonus_2_pay_day', $fieldConf)->widget(Select2::class, ['data' => $dayList, 'hideSearch' => true]) ?>
                            </td>
                            <td class="text-right nowrap bonus_2_pay_sum">
                                <?= $result['bonus_2_pay_sum'] ?>
                            </td>
                            <td class="text-right nowrap bonus_2_expenses">
                                <?= $result['bonus_2_expenses'] ?>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot class="text-bold">
                        <tr class="summary">
                            <td>
                            </td>
                            <td>
                                Итого
                            </td>
                            <td class="text-right nowrap total_amount">
                                <?= $result['total_amount'] ?>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="text-right nowrap total_prepay_sum">
                                <?= $result['total_prepay_sum'] ?>
                            </td>
                            <td>
                            </td>
                            <td class="text-right nowrap total_pay_sum">
                                <?= $result['total_pay_sum'] ?>
                            </td>
                            <td class="text-right nowrap total_expenses">
                                <?= $result['total_expenses'] ?>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                    <div class="mt-2 mb-2 text-grey">
                        Настроенные данные буду использоваться при расчете ежемесячной ЗП.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="mt-3 d-flex justify-content-between pb-4">
            <div class="spinner-button">
                <?= Html::a('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', 'javascript:;', [
                    'class' => 'button-regular button-width button-regular_red button-clr btn-save-salary-table',
                    'data-style' => 'expand-right',
                    'form' => 'employee-form',
                    'type' => 'submit'
                ]) ?>
            </div>
            <div class="spinner-button">
                <?= Html::a('Отменить', ['/employee/view', 'id' => $employeeCompany->employee_id], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent btn-cancel-salary-table',
                ]); ?>
            </div>
        </div>
    </div>

</div>



<?php $form->end(); ?>

<?php

$this->registerJs('
function disableSalaryTable() {
    $(".employee-salary-table").find("input, select, checkbox").prop("disabled", true);
    $(".employee-salary-table").find("checkbox").uniform("refresh");
    $(".employee-salary .form-actions").hide();
}
function enableSalaryTable() {
    $(".employee-salary-table").find("input, select, checkbox").removeAttr("disabled");
    $(".employee-salary-table").find("checkbox").uniform("refresh");
    $(".employee-salary .form-actions").show();
}

$(document).ready(function() {
    disableSalaryTable();
});

$(".btn-edit-salary-table").on("click", function(e) {
    e.preventDefault();
    enableSalaryTable();
    window.tmpSalaryTable = $(".employee-salary-table").html();
    $(".btn-edit-salary-table").hide();
});
$(".btn-cancel-salary-table").on("click", function(e) {
    e.preventDefault();
    $(".employee-salary-table").html(window.tmpSalaryTable);
    window.tmpSalaryTable = "";
    disableSalaryTable();
    $(".btn-edit-salary-table").show();
});
$(".btn-save-salary-table").on("click", function(e) {
    e.preventDefault();
    $("#is_save_salary").val(1);
    $("#employee-form").submit();
});

');
