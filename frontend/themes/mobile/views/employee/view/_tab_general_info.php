<?php

use common\components\date\DateHelper;
use common\models\document\DocumentType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\widgets\Modal;
use common\components\image\EasyThumbnailImage;

/* @var yii\web\View $this */
/* @var common\models\employee\Employee $model */

/* @var \yii\bootstrap\ActiveForm $form */
/* @var $completeRegistration boolean */

$initialConfig = [
    'wrapperOptions' => [
        'class' => 'col-md-2 inp_one_line',
    ],
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);

$showChBxRoles = EmployeeCompany::$addFlowRolesNeedConfig;
$payInvoiceCss = in_array($model->employee_role_id, $showChBxRoles) ? '' : 'hidden';
$roleArray = \common\models\employee\EmployeeRole::find()->actual()->orderBy([
    'sort' => SORT_ASC,
])->all();
$timeZones = TimeZone::find()->orderBy(['priority' => SORT_ASC])->indexBy('id')->asArray()->all();
$timeZone = ArrayHelper::getValue($timeZones, $model->time_zone_id);
?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-9">
            <div class="row">
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Часовой пояс</div>
                    <div><?= $timeZone ? ArrayHelper::getValue($timeZone, 'out_time_zone') : '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Пол</div>
                    <div><?= ArrayHelper::getValue(Employee::$sex_message, $model->sex, '—') ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Дата рождения</div>
                    <div><?= DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—' ?></div>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Дата приёма на работу</div>
                    <div><?= DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Дата увольнения</div>
                    <div><?= DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—' ?></div>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Право подписи на документах</div>
                    <div><?= $model->can_sign ? 'да' : 'нет' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Видит закупочные цены </div>
                    <div><?= $model->can_view_price_for_buy ? 'да' : 'нет' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Может добавлять номенклатуру </div>
                    <div><?= $model->is_product_admin ? 'да' : 'нет' ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

