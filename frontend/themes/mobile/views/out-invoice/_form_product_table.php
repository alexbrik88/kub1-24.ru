<?php

use common\models\out\OutInvoice;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\out\OutInvoice */

$company = Yii::$app->user->identity->company;
$serviceCount = $company->getProducts()->byDeleted()->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale(false)->count();
$goodsCount = $company->getProducts()->byDeleted()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale(false)->count();
$productCount = $serviceCount + $goodsCount;
?>

<?= Html::hiddenInput(Html::getInputName($model, 'productId')) ?>

<table id="selected-product-list" class="table table-style table-count-list">
    <thead>
        <tr class="heading">
            <th style="width: 50px;"></th>
            <th style="width: 60px;">id</th>
            <th>Артикул</th>
            <th>Наименование</th>
            <th style="width: 100px;">Цена продажи</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($model->selectedProducts as $product) : ?>
        <?= $this->render('_form_product_row', [
            'model' => $model,
            'product' => $product,
        ]) ?>
    <?php endforeach; ?>
        <tr id="new-product-row" class="new-product-row <?= empty($model->productId) ? '' : 'hidden'; ?>">
            <td style="">
                <span class="new-product-hide">
                    <i class="icon-close" style="font-size: 20px; cursor: pointer;"></i>
                </span>
            </td>
            <td style="">
            </td>
            <td style="">
            </td>
            <td style="">
                <?= Select2::widget([
                    'id' => 'product-add-select',
                    'name' => 'addOrder',
                    'initValueText' => '',
                    'theme' => Select2::THEME_KRAJEE_BS4 . ' product-add-select--dropdown',
                    'options' => [
                        'placeholder' => '',
                        'class' => 'form-control ',
                        'data-doctype' => Documents::IO_TYPE_OUT,
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'width' => '100%',
                        'minimumInputLength' => 1,
                        'dropdownCssClass' => 'product-search-dropdown',
                        'ajax' => [
                            'url' => "/product/search",
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                            'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(product) { return product.text; }'),
                        'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                    ],
                ]); ?>
                <div id="order-add-static-items" style="display: none;">
                    <ul class="order-add-static-items select2-results__options">
                        <li class="select2-results__option add-modal-produc-new yellow" aria-selected="false">
                            <?= Icon::get('add-icon') ?>
                            Добавить новый товар/услугу
                        </li>
                        <li class="select2-results__option add-modal-services<?= $serviceCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                        </li>
                        <li class="select2-results__option add-modal-products<?= $goodsCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                        </li>
                    </ul>
                </div>
                <script type="text/javascript">
                    $('#product-add-select').on('select2:open', function (evt) {
                        var prodItems = $('#select2-product-add-select-results').parent().children('.order-add-static-items');
                        if (prodItems.length) {
                            prodItems.remove();
                        }
                        $('.order-add-static-items').clone().insertAfter('#select2-product-add-select-results');
                    });
                    $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function() {
                        $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                        $(this).addClass('select2-results__option--highlighted');
                    });
                    $('#product-add-select').on('select2:select', function (evt) {

                        var item_id = $('#product-add-select').val();
                        console.log(item_id);

                        var exists = $('input.selected-product-id').map(function (idx, elem) {
                            return $(elem).val();
                        }).get();
                        $.post('/out-invoice/products-table', {
                            'productType': [0, 1],
                            'in_order': [item_id],
                            'exists': exists
                        }, function (data) {
                            if (data.result) {
                                $(data.result).insertBefore("#new-product-row");
                            }
                        });
                        $('#new-product-row').addClass('hidden');
                        $('#product-add-select').empty().trigger('change');
                    });
                </script>
            </td>
            <td></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="pl-0">
                <span class="button-regular button-hover-content-red new-product-show" style="margin: 0;">
                    <?= Icon::get('add-icon') ?>
                    <span class="ml-1">Добавить</span>
                </span>
            </td>
        </tr>
    </tfoot>
</table>
