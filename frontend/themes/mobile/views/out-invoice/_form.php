<?php

use common\models\Company;
use common\models\out\OutInvoice;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\out\OutInvoice */
/* @var $form yii\widgets\ActiveForm */
/* @var $backUrl string */
/* @var $company Company */

$this->registerJs('
    $("#outinvoice-is_outer_shopcart").on("change", function() {
        if ($(this).prop("checked")) {
            $("#selected-product-list .selected-product-row").remove();
            $("#new-product-row").removeClass("hidden");
            $(".outer_shopcart-collapse").collapse("hide");
            $(".outer_shopcart-collapse_in").collapse("show");
            $(".outer_shopcart-collapse input[type=checkbox]").prop("checked", false).trigger("change").uniform("refresh");
        } else {
            $(".outer_shopcart-collapse").collapse("show");
            $(".outer_shopcart-collapse_in").collapse("hide");
        }
    });
    $("#add_num").on("change", function() {
        if ($(this).prop("checked")) {
            $("#add_num_collapse").collapse("show");
        } else {
            $("#add_num_collapse").collapse("hide");
            $("#outinvoice-additional_number").val("");
            $("#outinvoice-is_additional_number_before input[value=0]").prop("checked", true);
            $("#outinvoice-is_additional_number_before input[type=radio]:not(.md-radiobtn)").uniform("refresh");
        }
    });
    $("#invoice_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#invoice_comment_collapse").collapse("show");
        } else {
            $("#invoice_comment_collapse").collapse("hide");
            $("#outinvoice-invoice_comment").val("");
        }
    });
    $("#add_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#add_comment_collapse").collapse("show");
        } else {
            $("#add_comment_collapse").collapse("hide");
            $("#outinvoice-add_comment").val("");
        }
    });
    $(".check-collapse").on("change", function() {
        if ($(this).prop("checked")) {
            $($(this).data("target")).collapse("show");
        } else {
            $($(this).data("target")).collapse("hide");
            $("input", $($(this).data("target"))).val("");
        }
    });
    $(".tooltip-hint").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "contentAsHTML": true,
    });
');

$checkboxTemplate = "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}";
$dataHint = Html::tag('span', Icon::get('question'), [
    'class' => 'tooltip-hint',
    'data-tooltip-content' => '#tooltip_strict_data',
    'style' => 'margin: 0; cursor: pointer;'
]);
$resultHint = Html::tag('span', Icon::get('question'), [
    'class' => 'tooltip-hint',
    'data-tooltip-content' => '#tooltip_send_result',
    'style' => 'margin: 0; cursor: pointer;'
]);
$hintOptions = [
    'style' => 'position: absolute; top: -2px; right: -20px;'
];
$company = Yii::$app->user->identity->company;

$code = '<?php
$orderList = [
    [
        "type" => "Укажите ТИП товара \'PRODUCT\' или \'SERVICE\'",
        "title" => "Укажите НАЗВАНИЕ ТОВАРА",
        "unit_code" => "Укажите код ОКЕИ ЕДИНИЦЫ ИЗМЕРЕНИЯ (796)",
        "quantity" => "Укажите КОЛИЧЕСТВО",
        "price" => "Укажите ЦЕНУ ЗА ЕДИНИЦУ в рублях",
        "nds" => "Укажите НДС в процентах, если есть",
    ],
];
?>
<form action="'.$model->outUrl.'" method="post">
    <?php foreach ($orderList as $key => $orderParams) : ?>
        <?php foreach ($orderParams as $name => $value) : ?>
            <input type="hidden" name="order_list[<?= $key ?>][<?= $name ?>]" value="<?= $value ?>">
        <?php endforeach ?>
    <?php endforeach ?>
    <input type="submit" name="form" value="Выставить счет" style="
        cursor: pointer;
        background: #4276a4;
        color: #fff;
        border-width: 0;
        padding: 7px 14px;
        font-size: 14px;">
</form>';

$openAdditionalOptions = false;

if ($isB2B = (strpos($backUrl, '/b2b/') !== false) && 'create' == $this->context->action->id) {
    $typeB2B = Yii::$app->request->get('type');
    if ($typeB2B == 'landing') {
        $model->is_autocomplete = 1;
        $model->is_bik_autocomplete = 1;
        $openAdditionalOptions = true;
    } else {
        $model->is_outer_shopcart = 1;
    }
}

?>

<?php $form = ActiveForm::begin([
    'id' => 'out-invoice-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ]
        ],
        'radioOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
            'wrapperOptions' => [
                'class' => '',
            ],
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
        'checkTemplate' => "{input}\n{label}\n{error}\n{hint}",
        'radioTemplate' => "{input}\n{label}\n{error}\n{hint}",
    ],
]); ?>

<div class="out-invoice-form" style="max-width: 600px;">
    <div class="form-errors-fixed">
        <?= $form->errorSummary($model); ?>
        <span></span>
    </div>

    <?= Html::hiddenInput('out_id', $model->id) ?>

    <?= $form->field($model, 'status')->widget(Select2::className(), [
        'data' => OutInvoice::$statusAvailable,
        'hideSearch' => true,
        'options' => [
            'disabled' => !$company->canAddOutInvoice() && $model->status == OutInvoice::INACTIVE,
        ],
        'pluginOptions' => [
            'width' => '100%',
            'hideSearch' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'note')->textInput() ?>

    <?= $form->field($model, 'return_url')->textInput([
        'placeholder' => 'http://name.com',
    ]) ?>

    <?= $form->field($model, 'is_outer_shopcart', [
        'labelOptions' => [
            'style' => 'padding-top: 0;',
        ]
    ])->checkbox([], false); ?>

    <div class="outer_shopcart-collapse collapse <?= $model->is_outer_shopcart ? '' : 'show'; ?>">
        <?= $form->field($model, 'productId', [
            'parts' => [
                '{input}' => $this->render('_form_product_table', ['model' => $model]),
            ],
            'horizontalCssClasses' => [
                'label' => 'col-sm-12',
                'offset' => '',
                'wrapper' => 'col-sm-12',
                'error' => '',
                'hint' => '',
            ],
            'labelOptions' => [
                'style' => 'margin-bottom: 5px;',
            ]
        ])->render(); ?>
    </div>
</div>

<div class="outer_shopcart-collapse_in collapse <?= $model->is_outer_shopcart ? 'show' : ''; ?>">
   <div style="margin: 0 0 15px;">
        <?= Html::A('Пример кода для вставки на сайт '.Icon::get('shevron', [
            'class' => 'link-shevron ml-1',
        ]), '#outer_shopcart_code', [
            'class' => 'collapse-toggle-link link_collapse',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'true',
            'aria-controls' => 'outer_shopcart_code',
        ]) ?>
    </div>
    <div class="collapse show" id="outer_shopcart_code" style="padding-bottom: 15px;">
        <xmp style="border: 1px solid #ccc; margin: 0; padding: 5px 10px; background-color: #f5f5f5;"><?= $code ?></xmp>
    </div>
</div>

<div class="out-invoice-form" style="max-width: 600px;">
    <div style="margin: 0 0 15px;">
        <?= Html::A('Дополнительные настройки '.Icon::get('shevron', [
            'class' => 'link-shevron ml-1',
        ]), '#additionalSettings', [
            'class' => 'collapse-toggle-link link_collapse' . ($isB2B && $openAdditionalOptions ? '' : ' collapsed'),
            'data-toggle' => 'collapse',
            'aria-expanded' => ($isB2B && $openAdditionalOptions) ? 'true' : 'false',
            'aria-controls' => 'additionalSettings',
        ]) ?>
    </div>
    <div class="collapse <?= ($isB2B && $openAdditionalOptions) ? 'show':'' ?>" id="additionalSettings">
        <div class="">
            <?= $form->field($model, 'show_article')->checkbox([], false); ?>

            <?= $form->field($model, 'is_autocomplete')->checkbox([], false); ?>

            <?= $form->field($model, 'is_bik_autocomplete')->checkbox([], false); ?>

            <?= $form->field($model, 'send_with_stamp')->checkbox([], false); ?>

            <?= $form->field($model, 'send_with_stamp')->checkbox([], false); ?>

            <div class="form-group">
                <?= Html::checkbox('add_num', (boolean)$model->additional_number, [
                    'id' => 'add_num',
                    'label' => 'К номеру счета Доп номер',
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ]) ?>
                <div id="add_num_collapse" class="collapse <?= $model->additional_number ? 'show' : ''; ?>" style="padding-left: 30px;">
                    <?= $form->field($model, 'is_additional_number_before', [
                        /*'horizontalCssClasses' => [
                            'label' => 'col-sm-12',
                            'offset' => '',
                            'wrapper' => 'col-sm-12',
                            'error' => 'col-sm-12',
                            'hint' => 'col-sm-12',
                        ],*/
                    ])->radioList(Company::$addNumPositions, [
                        'encode' => false,
                    ])->label(false)->error(false)->hint($form->field($model, 'additional_number', [
                        'inputOptions' => [
                            'style' => 'width: 150px;'
                        ],
                    ])); ?>
                </div>
            </div>

            <?= $form->field($model, 'invoice_comment')->checkbox([
                'id' => 'invoice_comment',
                'checked' => (boolean) $model->invoice_comment,
            ], false)->hint(Html::tag('div', Html::activeTextarea($model, 'invoice_comment', [
                'class' => 'form-control',
            ]), [
                'id' => 'invoice_comment_collapse',
                'class' => 'collapse' . ($model->invoice_comment ? ' show' : ''),
                'style' => 'padding-left: 30px;',
            ])) ?>

            <?= $form->field($model, 'add_comment')->checkbox([
                'id' => 'add_comment',
                'checked' => (boolean) $model->add_comment,
            ], false)->hint(Html::tag('div', Html::activeTextarea($model, 'add_comment', [
                'class' => 'form-control',
            ]), [
                'id' => 'add_comment_collapse',
                'class' => 'collapse' . ($model->add_comment ? ' show' : ''),
                'style' => 'padding-left: 30px;',
            ])) ?>

            <?= $form->field($model, 'notify_to_email')->checkbox([], false); ?>

            <div class="form-group" style="font-weight: 600;font-size: 14px; ">
                Для интеграции по API (информация для программиста):
            </div>

            <div class="pb-3 outer_shopcart-collapse collapse <?= $model->is_outer_shopcart ? '' : 'show'; ?>">
                <?= $form->field($model, 'strict_data', [
                    'template' => $checkboxTemplate,
                    'hintOptions' => $hintOptions,
                    'options' => [
                        'class'=> '',
                        'style' => 'display: inline-block; position: relative;',
                    ],
                ])->hint($dataHint)->checkbox([
                    'class' => 'check-collapse',
                    'data-target' => '#data_url_collapse'
                ], false) ?>
                <div id="data_url_collapse" class="collapse <?= $model->strict_data ? 'show' : ''; ?>">
                    <?= $form->field($model, 'data_url', ['options' => [
                        'class'=> '',
                        'style' => 'margin-left: 27px;',
                    ]])->textInput([
                        'placeholder' => 'http://name.com/get-data',
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'send_result', [
                    'template' => $checkboxTemplate,
                    'hintOptions' => $hintOptions,
                    'options' => [
                        'class'=> '',
                        'style' => 'display: inline-block; position: relative;',
                    ],
                ])->hint($resultHint)->checkbox([
                    'class' => 'check-collapse',
                    'data-target' => '#result_url_collapse'
                ], false) ?>
                <div id="result_url_collapse" class="collapse <?= $model->send_result ? 'show' : ''; ?>">
                    <?= $form->field($model, 'result_url', ['options' => [
                        'class'=> '',
                        'style' => 'margin-left: 27px;',
                    ]])->textInput([
                        'placeholder' => 'http://name.com/result',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-between">
    <div class="column">
        <?= Html::submitButton('<span class="ladda-label svg svg-check">Сохранить</span>', [
            'class' => 'svg-btn button-regular button-width button-hover-grey mr-2',
            'data-style' => 'expand-right',
        ]); ?>
    </div>
    <div class="column">
        <?= Html::a('<span class="svg svg-cancel">Отменить</span>', $backUrl, [
            'class' => 'button-regular button-width button-hover-grey',
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div style="display: none;">
    <span id="tooltip_strict_data">
        Выберите и укажите "URL данных",
        <br>
        чтобы запросить количество и
        <br>
        цену товаров со своего сайта.
        <br>
        При перенаправлении пользователя
        <br>
        необходимо добавлять параметры
        <br>
        "uid" и "hash",
        <br>
        которые будут использованы в
        <br>
        запросе для получения данных.
        <br>
        Остались вопросы? Пишите нам
        <br>
        на support@kub-24.ru
    </span>
    <span id="tooltip_send_result">
        Выберите и укажите "URL результата",
        <br>
        чтобы отправить на свой сайт
        <br>
        данные созданного счета.
        <br>
        Остались вопросы? Пишите нам
        <br>
        на support@kub-24.ru
    </span>
</div>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_existed', [
    'contentCssClass' => 'to-outInvoice-content',
]); ?>
