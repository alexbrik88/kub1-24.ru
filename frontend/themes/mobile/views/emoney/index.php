<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'E-money';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emoney-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Create Emoney', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_id',
            'name',
            'is_main',
            'is_accounting',
            // 'is_closed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
