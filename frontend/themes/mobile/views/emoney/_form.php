<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Emoney */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emoney-form">
    <?php $form = ActiveForm::begin([
        'id' => 'emoney-form',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
            'checkOptions' => [
                'class' => '',
                'labelOptions' => [
                    'class' => 'label'
                ],
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_accounting')->checkbox([], true) ?>

    <?php if (!$model->is_main) : ?>
        <?= $form->field($model, 'is_main')->checkbox([], true) ?>

        <?php if (!$model->isNewRecord) : ?>
            <?= $form->field($model, 'is_closed')->checkbox([], true) ?>
        <?php endif ?>
    <?php endif ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-width button-clr',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
            'data-dismiss' => 'modal'
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Html::script('
$("#emoney-form input:checkbox:not(.md-check)").uniform();
', [
    'type' => 'text/javascript',
]); ?>
