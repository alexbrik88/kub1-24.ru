<?php
/**
 * @var $x array
 * @var $y array
 * @var $units array
 */
use yii\web\JsExpression;

$y = array_map(function($v) { return (float)$v; }, $y);

?>
<?= \miloschuman\highcharts\Highcharts::widget([
    'scripts' => [
        //'modules/exporting',
        //'modules/pattern-fill',
        'themes/grid-light',
    ],
    'options' => [
        'credits' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'height' => '100px',
            'spacing' => [0,0,0,0],
            'width' => new JsExpression('0.927 * $(".page-content").width()')
        ],
        'tooltip' => [
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'useHTML' => true,
            'formatter' => new JsExpression("function() {
                  return '<div style=\"text-align: center;\">'+this.x+'<br><b>'+this.y+' {$units}</b></div>';
                }")
        ],
        'title' => [
            'text' => '',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
                false,
            ],
            'labels' => [
                'enabled' => false,
            ],
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'lineWidth' => 0,
        ],
        'xAxis' => [
            'categories' => $x,
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'lineWidth' => 0,
        ],
        'series' => [
            [
                'name' => 'Y',
                'color' => '#0079f1',
                'data' => $y,
                'borderRadius' => 3
            ],
        ],
        'legend' => [
            'enabled' => false,
        ],

    ],
]); ?>