<?php

use common\models\product\Product;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;

?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Группа <?= ($isGoods) ? 'товара' : 'услуги' ?></div>
                <div><?= $model->group ? $model->group->title : Product::DEFAULT_VALUE; ?></div>
            </div>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Категория</div>
                <div>---</div>
            </div>
        </div>
    </div>
    <?php if ($isGoods): ?>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Код</div>
                    <div><?= Html::encode($model->code ?: Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Артикул</div>
                    <div><?= Html::encode($model->article ?: Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Штрих код</div>
                    <div><?=  Html::encode($model->barcode ?: Product::DEFAULT_VALUE); ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Единица измерения</div>
                    <div><?= $model->productUnit ? $model->productUnit->name : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Вес (кг)</div>
                    <div><?= $model->weight ? $model->weight : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Объем (м3)</div>
                    <div><?= $model->volume ? $model->volume : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Масса брутто</div>
                    <div><?= Html::encode($model->mass_gross ? : Product::DEFAULT_VALUE); ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Вид упаковки</div>
                    <div><?= Html::encode($model->box_type ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Количество в одном месте</div>
                    <div><?= Html::encode($model->count_in_place ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Количество мест, штук</div>
                    <div><?= Html::encode($model->place_count ? : Product::DEFAULT_VALUE); ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
        <div class="row">
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Страна происхождения товара</div>
                <div><?= $model->countryOrigin ? $model->countryOrigin->name_short : Product::DEFAULT_VALUE; ?></div>
            </div>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Номер таможенной декларации</div>
                <div><?= Html::encode($model->customs_declaration_number ? : Product::DEFAULT_VALUE); ?></div>
            </div>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Код вида товара</div>
                <div><?= Html::encode($model->item_type_code ? : Product::DEFAULT_VALUE); ?></div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>