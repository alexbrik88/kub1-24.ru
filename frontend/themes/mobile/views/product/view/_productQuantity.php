<?php

use yii\helpers\Html;
use common\models\product\ProductStore;
use yii\widgets\Pjax;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();
$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;
?>

<?php Pjax::begin([
    'id' => 'productStore-pjax-container',
    'enablePushState' => false,
]); ?>

<div class="row flex-column pr-2 pb-2">
    <div class="page-border pl-3 pr-3 pt-3 pb-0 mb-1 mr-1">
        <div class="pb-4">
            <table class="table table-style mb-0">
                <thead>
                <tr>
                    <th class="align-middle pl-1 pb-2 pt-1">Название склада</th>
                    <th class="align-middle pb-2 pt-1">Остаток</th>
                    <th class="align-middle pb-2 pt-1">Неснижаемый <br> остаток</th>
                    <th class="align-middle pb-2 pt-1">Цена <br> продажи</th>
                    <th class="align-middle pb-2 pt-1">Стоимость <br> продажи</th>
                    <th class="align-middle pb-2 pt-1">Цена <br> покупки</th>
                    <th class="align-middle pb-2 pt-1">Стоимость <br> покупки</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($storeArray as $store) : ?>
                <?php /* @var $productStore ProductStore */
                $productStore = $model->getProductStoreByStore($store->id);
                $productInitQuantity = $model->getInitQuantity($productStore->store_id);
                $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
                $totalStoreQuantity += $productStore->quantity * 1;
                $totalIrreducibleQuantity += $productIrreducibleQuantity;
                $totalAmountForBuy += (int)$model->price_for_buy_with_nds * $productStore->quantity;
                $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
                <tr>
                    <td><div class="table-count-cell pl-0"><?= Html::encode($store->name); ?></div></td>
                    <td><div class="table-count-cell"><?= $productStore->quantity; ?></div></td>
                    <td><div class="table-count-cell"><?= $productIrreducibleQuantity; ?></div></td>
                    <td><div class="table-count-cell"><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?></div></td>
                    <td><div class="table-count-cell"><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds * $productStore->quantity, 2); ?></div></td>
                    <td><div class="table-count-cell"><?= $canViewPriceForBuy ?
                                TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                                Product::DEFAULT_VALUE; ?></div></td>
                    <td><div class="table-count-cell"><?= $canViewPriceForBuy ?
                                TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds * $productStore->quantity, 2) :
                                Product::DEFAULT_VALUE; ?></div></td>
                </tr>
                <?php endforeach ?>
                <tr>
                    <td><div class="table-count-cell weight-700 text-grey pl-0">Итого</div></td>
                    <td><div class="table-count-cell weight-700 text-grey"><?= $totalStoreQuantity; ?></div></td>
                    <td><div class="table-count-cell weight-700 text-grey"><?= $totalIrreducibleQuantity; ?></div></td>
                    <td></td>
                    <td><div class="table-count-cell weight-700 text-grey"><?= TextHelper::invoiceMoneyFormat($totalAmountForSell, 2); ?></div></td>
                    <td></td>
                    <td><div class="table-count-cell weight-700 text-grey">
                            <?= $canViewPriceForBuy ?
                                TextHelper::invoiceMoneyFormat($totalAmountForBuy, 2) :
                                Product::DEFAULT_VALUE; ?></div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php Pjax::end(); ?>
