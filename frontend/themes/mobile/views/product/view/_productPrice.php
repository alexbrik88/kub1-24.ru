<?php

use common\components\TextHelper;
use common\models\document\NdsViewType;
use common\models\product\Product;
use common\models\product\ProductToSupplier;
use common\models\TaxRate;
use common\models\document\PackingList;
use common\models\document\Invoice;
use yii\web\View;
use common\models\document\Order;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$minSupplierPrice = ProductToSupplier::find()->where(['product_id' => $model->id])->orderBy(['price' => SORT_ASC])->select('price')->scalar();

if ($model->price_for_buy_with_nds == 0 && $minSupplierPrice) {
    $suppliersIds = ProductToSupplier::find()
        ->select('supplier_id')
        ->andWhere(['product_id' => $model->id])
        ->column();
    $suppliersPrices = Order::find()
        ->select('order.purchase_price_with_vat')
        ->joinWith('invoice')
        ->andWhere(['invoice.contractor_id' => $suppliersIds])
        ->andWhere(['invoice.has_packing_list' => 1])
        ->andWhere(['order.product_id' => $model->id])
        ->orderBy(['invoice.document_date' => SORT_DESC])
        ->column();
    if ($suppliersPrices)
        $model->price_for_buy_with_nds = $suppliersPrices[0];
    else
        $model->price_for_buy_with_nds = $minSupplierPrice;
}
?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="column mb-4 pb-2 mr-4">
                <div class="label weight-700 mb-3">Цена покупки</div>
                <div class="mb-2"><?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                        Product::DEFAULT_VALUE; ?> ₽</div>
                <?php if (Yii::$app->user->identity->company->hasNds()): ?>
                    <div class="small-text text-grey">
                        <?= ($model->priceForBuyNds->id != TaxRate::RATE_WITHOUT) ? 'НДС ' : '' ?>
                        <?= $model->company->nds_view_type_id == NdsViewType::NDS_VIEW_WITHOUT ? ' в т.ч. ' : '' ?>
                        <?= $model->priceForBuyNds->name; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="column mb-4 pb-2">
                <input class="" id="template1" type="checkbox" disabled <?= $model->not_for_sale ? 'checked' : '' ?>>
                <label class="checkbox-label" for="template1" style="padding-left:0">
                    <span class="checkbox-txt ml-2">Не для продажи</span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-12 <?= $model->not_for_sale ? 'hidden' : '' ?>">
        <div class="row">
            <div class="column mb-4 pb-2 mr-4">
                <div class="label weight-700 mb-3">Цена продажи</div>
                <div class="mb-2"><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?> ₽</div>
                <?php if (Yii::$app->user->identity->company->hasNds()): ?>
                    <div class="small-text text-grey mb-1">
                        <?= ($model->priceForSellNds->id != TaxRate::RATE_WITHOUT) ? 'НДС ' : '' ?>
                        <?= $model->company->nds_view_type_id == NdsViewType::NDS_VIEW_WITHOUT ? ' в т.ч. ' : '' ?>
                        <?= $model->priceForSellNds->name; ?>
                    </div>
                <?php endif; ?>
                <?php if ($model->price_for_sell_with_nds > 0 && $model->price_for_sell_with_nds >= $model->price_for_buy_with_nds && $model->price_for_buy_with_nds > 0) : ?>
                    <div class="small-text text-grey">Маржа
                        <?php
                        $margin = $model->price_for_sell_with_nds - $model->price_for_buy_with_nds;
                        $margin_percent = round($margin / $model->price_for_buy_with_nds * 100);
                        ?>
                        <?= $margin_percent .'% / ' . TextHelper::invoiceMoneyFormat($margin, 2) ?> ₽</div>
                <?php endif; ?>
            </div>
            <?php foreach ($model->customPrices as $cp): ?>
                <div class="column mb-4 pb-2 mr-4">
                    <div class="label weight-700 mb-3"><?= $cp->priceGroup->name ?></div>
                    <div class="mb-2"><?= TextHelper::invoiceMoneyFormat($cp->price, 2); ?> ₽</div>
                    <?php if (Yii::$app->user->identity->company->hasNds()): ?>
                        <div class="small-text text-grey mb-1">
                            <?= ($cp->nds_id != TaxRate::RATE_WITHOUT) ? 'НДС ' : '' ?>
                            <?= $model->company->nds_view_type_id == NdsViewType::NDS_VIEW_WITHOUT ? ' в т.ч. ' : '' ?>
                            <?= $cp->nds->name; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($cp->price >= $model->price_for_buy_with_nds && $model->price_for_buy_with_nds > 0) : ?>
                        <div class="small-text text-grey">Маржа
                            <?php
                            $margin = $cp->price - $model->price_for_buy_with_nds;
                            $margin_percent = round($margin / $model->price_for_buy_with_nds * 100);
                            ?>
                            <?= $margin_percent .'% / ' . TextHelper::invoiceMoneyFormat($margin, 2) ?> ₽</div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>