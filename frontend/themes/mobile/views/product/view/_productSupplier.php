<?php

use common\components\TextHelper;
use common\models\Contractor;
use common\models\contractor\ContractorAgentBuyer;
use common\models\product\Product;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use \common\models\product\ProductToSupplier;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $pts ProductToSupplier */

$productToSupplier = ProductToSupplier::find()->where(['product_id' => $model->id])->orderBy('price')->all();
$allSuppliersIds = $model->company->getContractors()->andWhere(['type' => Contractor::TYPE_SELLER, 'is_deleted' => false])->select('id')->asArray()->column();
$supplierData = $model->getSuppliersData($allSuppliersIds);
$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
$goodsClass = (!$isGoods) ? 'hidden' : '';
?>
<div class="pb-3">
    <?php $number = 0; ?>
    <?php foreach ($productToSupplier as $pts): ?>
        <?php $baseName = 'supplier[' . $number . ']'; ?>
        <div class="supplier-price-block row align-items-top mb-3 pr-3 supplier-<?= $pts->supplier_id ?>">
            <input type="hidden" name="<?= $baseName . '[id]' ?>" value="<?= $pts->supplier_id ?>">
            <div class="col-4 mb-4 pb-2 mr-auto">
                <div class="label weight-700 mb-3"><?= $pts->supplier->getShortName() ?></div>
                <div><?= $model->title ?></div>
            </div>
            <div class="col-2 column-price mb-4 pb-2">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Цена <?= ($isGoods) ? 'покупки' : 'покупки' ?></div>
                    <div class="pts-text">
                        <span><?= TextHelper::invoiceMoneyFormat($pts->price, 2) ?></span> ₽
                    </div>
                    <div class="pts-input hidden">
                        <input type="text" name="<?= $baseName . '[price]' ?>" value="<?= number_format($pts->price/100, 2, ',', '') ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-2 column-article mb-4 pb-2 <?= $goodsClass ?>">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Артикул</div>
                    <div class="pts-text">
                        <span><?= $pts->article ?: '--' ?></span>
                    </div>
                    <div class="pts-input hidden">
                        <input type="text" name="<?= $baseName . '[article]' ?>" value="<?= $pts->article ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-3 column-date mb-4 pb-2">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Последнее поступление</div>
                    <div><span><?= isset($supplierData[$pts->supplier_id]) ? $supplierData[$pts->supplier_id]['date'] : '--' ?></span></div>
                </div>
            </div>
            <div class="col-1 view-buttons column mb-4 pb-2">
            </div>
        </div>
        <?php $number++; ?>
    <?php endforeach; ?>
</div>