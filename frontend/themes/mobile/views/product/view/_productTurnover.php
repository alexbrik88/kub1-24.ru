<?php

use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\document\OrderPackingList;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\product\ProductTurnoverSearch;
use frontend\components\StatisticPeriod;
use frontend\widgets\RangeButtonWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\document\Act;
use common\components\TextHelper;
use common\models\Contractor;
use frontend\models\Documents;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
$searchModel = new ProductTurnoverSearch([
    'product_id' => $model->id,
    'dateRange' => StatisticPeriod::getSessionPeriod(),
    'searchByProductionType' => $model->production_type,
]);
//$searchModel::$productType
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get('productTurnover-grid');
$isGoods = ($model->production_type == Product::PRODUCTION_TYPE_GOODS);
?>

<?php Pjax::begin([
    'id' => 'productTurnover-pjax',
    'timeout' => 10000,
]); ?>

<div class="row flex-column pr-3 pb-2">
    <div class="wrap wrap_count pl-0 pr-0 pt-0 pb-0 mb-0">
        <div class="row">
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main"><?= ($isGoods) ? $model->balanceAtDate(true, [$model->id]) : '—' ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Остаток на начало</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_turquoise wrap">
                    <div class="count-card-main"><?= $model->turnAtPeriod(true, [$model->id], true, $model->production_type) ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Закуплено всего</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_grey wrap">
                    <div class="count-card-main"><?= $model->turnAtPeriod(false, [$model->id], true, $model->production_type) ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Продано всего</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main"><?= ($isGoods) ? $model->balanceAtDate(false, [$model->id]) : '—' ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Остаток на конец</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6 d-flex flex-column flex-grow-1 flex-shrink-0">
            <?= frontend\widgets\RangeButtonWidget::widget([
                'cssClass' => 'doc-gray-button btn_select_days btn_row',
                'pjaxSelector' => '#productTurnover-pjax',
            ]); ?>
            </div>
        </div>
    </div>
    <div class="page-border pl-3 pr-3 pt-3 pb-0 mb-1 mr-1">
        <div class="pb-3">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                //'showFooter' => true,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                //'options' => [
                //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                //],

                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

                'columns' => [
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'footer' => 'Итого',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                    ],
                    [
                        'attribute' => 'coming',
                        'label' => 'Закуплено',
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            /** @var $doc PackingList */
                            if ($doc = ProductTurnoverSearch::getDocumentModel($data['by_document'], $data['basis_id'], Documents::IO_TYPE_IN)) {
                                if ($doc instanceof PackingList) {
                                    $docOrders = $doc->orderPackingLists;
                                }  elseif ($doc instanceof Upd) {
                                    $docOrders = $doc->orderUpds;
                                }  elseif ($doc instanceof Act) {
                                    $docOrders = $doc->orderActs;
                                } else {
                                    $docOrders = [];
                                }
                                $label = '';
                                foreach ($docOrders as $docOrder) {

                                    $order = $docOrder->order;

                                    if ($order->product_id == $model->id) {

                                        $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                                        $quantity = ($unitName == Product::DEFAULT_VALUE) ? $unitName : str_replace('.', ',', $docOrder->quantity);
                                        $totalAmount = TextHelper::invoiceMoneyFormat($doc->getPrintOrderAmount($order->id));

                                        $label .= '<b>'.$quantity . ' ' . $unitName.'</b>';
                                        $label .= '<br>';
                                        $label .= '<span class="text-grey">' . $totalAmount . ' ₽</span>';

                                        break;
                                    }
                                }

                                return $label;
                            }

                            return '';
                        },
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                    ],
                    [
                        'attribute' => 'selling',
                        'label' => 'Продано',
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            /** @var $doc PackingList */
                            if ($doc = ProductTurnoverSearch::getDocumentModel($data['by_document'], $data['basis_id'], Documents::IO_TYPE_OUT)) {
                                if ($doc instanceof PackingList) {
                                    $docOrders = $doc->orderPackingLists;
                                }  elseif ($doc instanceof Upd) {
                                    $docOrders = $doc->orderUpds;
                                }  elseif ($doc instanceof Act) {
                                    $docOrders = $doc->orderActs;
                                } else {
                                    $docOrders = [];
                                }
                                $label = '';
                                foreach ($docOrders as $docOrder) {

                                    $order = $docOrder->order;

                                    if ($order->product_id == $model->id) {

                                        $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                                        $quantity = ($unitName == Product::DEFAULT_VALUE) ? $unitName : str_replace('.', ',', $docOrder->quantity);
                                        $totalAmount = TextHelper::invoiceMoneyFormat($doc->getPrintOrderAmount($order->id));

                                        $label .= '<b>'.$quantity . ' ' . $unitName.'</b>';
                                        $label .= '<br>';
                                        $label .= '<span class="text-grey">' . $totalAmount . ' ₽</span>';

                                        break;
                                    }
                                }

                                return $label;
                            }

                            return '';
                        },
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                    ],
                    [
                        //'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'filter' => ['' => 'Все'] + $searchModel->searchQuery()->select([
                                'contractor',
                                'contractor_id',
                            ])->distinct()->indexBy('contractor_id')->column(),
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-ellipsis'
                        ],
                        's2width' => '300px',
                        'format' => 'html',
                        'value' => function ($data) {

                            if ($contractor = Contractor::findOne($data['contractor_id'])) {
                                $inn = $contractor->ITN;
                                $kpp = $contractor->PPC;
                                return Html::beginTag('div', ['style' => 'max-width:300px']).Html::a($contractor->getShortName(), [
                                    '/contractor/view',
                                    'type' => $contractor->type,
                                    'id' => $contractor->id,
                                ]) . '<br>' . '<span class="text-grey">ИНН ' . $contractor->ITN . ($contractor->PPC ? (', КПП ' . $contractor->PPC) : '') . '</span>'.Html::endTag('div');
                            }

                            return '';
                        }
                    ],
                    [
                        'attribute' => 'basis',
                        'label' => 'Документ',
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            /** @var $doc PackingList */
                            if ($doc = ProductTurnoverSearch::getDocumentModel($data['by_document'], $data['basis_id'], $data['type'])) {
                                $name = ($data['by_document'] == 'upd' ? 'УПД №' : ($data['by_document'] == 'act' ? 'Акт №' : 'Товарная накладная №')) . $doc->getFullNumber();

                                $label = Html::a($name, [
                                    "/documents/{$data['by_document']}/view",
                                    'type' => $data['type'],
                                    'id' => $data['basis_id'],
                                ]);

                                $label .= '<br>';
                                $label .= '<span class="text-grey">Дата ' . DateHelper::format($doc->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . ', </span>';
                                $label .= '<span class="text-grey">Сумма ' . TextHelper::invoiceMoneyFormat($doc->totalAmountWithNds, 2) . ' ₽</span>';

                                return $label;
                            }

                            return '';
                        }
                    ],
                    [
                        'attribute' => 'invoice',
                        'label' => 'Счет',
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {

                            $cashFlowData = [];
                            $invoiceStatus = null;

                            if ($invoice = \common\models\document\Invoice::findOne(['id' => $data['invoice_id']])) {
                                if ($invoice->cashBankFlows) {
                                    $dateArray = ArrayHelper::getColumn($invoice->cashBankFlows, 'date');
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по Банку';
                                }
                                if ($invoice->cashOrderFlows) {
                                    $dateArray = ArrayHelper::getColumn($invoice->cashOrderFlows, 'date');
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по Кассе';
                                }
                                if ($invoice->cashEmoneyFlows) {
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по E-money';
                                }

                                $invoiceStatus = ($invoice->isOverdue()
                                    || $invoice->invoice_status_id == InvoiceStatus::STATUS_OVERDUE
                                    || $invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) ?
                                    '<span style="color:red">'.$invoice->invoiceStatus->name.'</span>' :
                                    $invoice->invoiceStatus->name;

                            }
                            $content = Html::a("Счет {$data['invoice']}", [
                                    "/documents/invoice/view",
                                    'type' => $data['type'],
                                    'id' => $data['invoice_id'],
                                ], ['target' => '_blank', 'data-pjax' => 0]) . '<br/>';

                            if ($cashFlowData) {
                                $content .= '<span class="text-grey">' . $invoiceStatus . '. ';
                                $content .= implode(', ', $cashFlowData) . '</span>';
                            } elseif ($invoice && $invoice->isOverdue()) {
                                $content .= '<span class="text-grey">' . $invoiceStatus . '. ';
                                $content .= 'Оплатить до ' . DateHelper::format($invoice->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . '</span>';
                            }

                            return $content;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php Pjax::end(); ?>
