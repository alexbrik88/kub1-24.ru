<?php


use common\components\helpers\Month;
use common\models\document\Invoice;
use common\models\document\Order;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;use yii\widgets\Pjax;
use common\models\product\ProductStore;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();
$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;

$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
?>

<div class="nav-tabs-row row">
    <ul class="nav nav-tabs nav-tabs_border_transparent w-100 mr-3" id="myTabIn" role="tablist">
        <?php if ($isGoods): ?>
        <li class="nav-item">
            <a class="nav-link <?= $isGoods ? 'active' : 'hidden' ?>" id="stocks-balance-tab" data-toggle="tab" href="#stocks-balance" role="tab" aria-controls="stocks-balance" aria-selected="false">Неснижаемый остаток</a>
        </li>
        <?php endif; ?>
        <li class="nav-item">
            <a class="nav-link <?= !$isGoods ? 'active' : '' ?>" id="stocks-indicators-tab" data-toggle="tab" href="#stocks-indicators" role="tab" aria-controls="stocks-indicators" aria-selected="true">Ключевые показатели</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="stocks-analytic-tab" data-toggle="tab" href="#stocks-analytic" role="tab" aria-controls="stocks-analytic" aria-selected="false">Аналитика</a>
        </li>
    </ul>
</div>
<div class="tab-content" id="myTabContentIn">
    <!-- TAB 1 -->
    <div class="tab-pane fade <?= $isGoods ? 'show active' : 'hidden' ?>" id="stocks-balance" role="tabpanel" aria-labelledby="stocks-balance-tab">
        <div class="row flex-column pr-2 pb-2">
            <div class="page-border pl-3 pr-3 pt-3 pb-0 mb-1 mr-1">
                <div class="pb-4">
                    <table class="table table-style mb-0">
                        <thead>
                        <tr>
                            <th class="align-middle pl-1 pb-2 pt-1">Название склада</th>
                            <th class="align-middle pb-2 pt-1">Остаток</th>
                            <th class="align-middle pb-2 pt-1">Неснижаемый остаток</th>
                            <th class="align-middle pb-2 pt-1">Действие, если остаток меньше</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($storeArray as $store): ?>
                        <?php /* @var $productStore ProductStore */
                        $productStore = $model->getProductStoreByStore($store->id);
                        $productInitQuantity = $model->getInitQuantity($productStore->store_id);
                        $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
                        $totalStoreQuantity += (int)$productStore->quantity;
                        $totalIrreducibleQuantity += $productIrreducibleQuantity;
                        $totalAmountForBuy += (int)$model->price_for_buy_with_nds * $productStore->quantity;
                        $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
                        <tr>
                            <td><div class="table-count-cell pl-0"><?= Html::encode($store->name) ?></div></td>
                            <td><div class="table-count-cell">
                                    <?= $productStore->quantity ?>
                                </div></td>
                            <td><div class="table-count-cell">
                                    <?= Html::activeTextInput($model, "irreducibleQuantity[$productStore->store_id]", [
                                        'class' => 'form-control product-store-irreducible-quantity',
                                        'style' => 'width: 100%; text-align: right;',
                                    ]); ?>
                                </div></td>
                            <td><div class="table-count-cell">Отправить сообщение на почту <span style="color:#4679AE">(Скоро)</span></div></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- TAB 2 -->
    <div class="tab-pane fade <?= !$isGoods ? 'show active' : '' ?>" id="stocks-indicators" role="tabpanel" aria-labelledby="stocks-indicators-tab">
        <div class="row flex-column pr-2 pb-2">
            <div class="page-border pl-3 pr-3 pt-3 pb-0 mb-1 mr-1">
                <div class="pb-4">
                    <table class="table table-style mb-0">
                        <thead>
                        <tr>
                            <th class="align-middle pl-1 pb-2 pt-1">Название показателя</th>
                            <th class="align-middle pb-2 pt-1">Значение </th>
                            <th class="align-middle pb-2 pt-1">Контрольное значение </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><div class="table-count-cell pl-0">Тип <?= ($isGoods) ? 'товара' : 'услуги' ?> по АВС-XYZ анализу</div></td>
                            <td><div class="table-count-cell">AX</div></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div class="table-count-cell pl-0">Маржинальность </div></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php if ($isGoods): ?>
                        <tr>
                            <td><div class="table-count-cell pl-0">Оборачиваемость</div></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- TAB 3 -->
    <div class="tab-pane fade" id="stocks-analytic" role="tabpanel" aria-labelledby="stocks-analytic-tab">

    </div>
</div>
