<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\product\ProductStore;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();
$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;
?>

<?php Pjax::begin([
    'id' => 'productStore-pjax-container',
    'enablePushState' => false,
]); ?>
<div style="display: inline-block; margin-right: 10px;">
    <table class="table table-style table-quantity"
           style="width: auto; margin: 0;">
        <thead>
        <tr class="heading va-top">
            <th>Склад</th>
            <th>Начальное количество</th>
            <th>Остаток</th>
            <th>Неснижаемый остаток</th>
            <th>Цена покупки</th>
            <th>Стоимость покупки</th>
            <th>Цена продажи</th>
            <th>Стоимость продажи</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($storeArray as $store): ?>
            <?php /* @var $productStore ProductStore */
            $productStore = $model->getProductStoreByStore($store->id);
            $productInitQuantity = $model->getInitQuantity($productStore->store_id);
            $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
            $totalStoreQuantity += (int)$productStore->quantity;
            $totalIrreducibleQuantity += $productIrreducibleQuantity;
            $totalAmountForBuy += (int)$model->price_for_buy_with_nds * $productStore->quantity;
            $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
            <tr class="store-row">
                <td style="vertical-align: middle; min-width: 100px;"><?= Html::encode($store->name) ?></td>
                <td class="store-initial-quantity"
                    style="text-align: right; vertical-align: middle; border-right-width: 0;">
                    <?= Html::activeTextInput($model, "initQuantity[$productStore->store_id]", [
                        'class' => 'form-control product-store-quantity',
                        'style' => 'width: 100%; text-align: right; max-width:150px;',
                    ]); ?>
                </td>
                <td style="text-align: right; vertical-align: middle;"><?= $productStore->quantity ?></td>
                <td class="store-irreducible-quantity" style="text-align: right; vertical-align: middle;">
                    <?php /*
                    <?= Html::activeTextInput($model, "irreducibleQuantity[$productStore->store_id]", [
                        'class' => 'form-control product-store-irreducible-quantity',
                        'style' => 'width: 100%; text-align: right;',
                    ]); ?>*/ ?>
                    <?= (isset($model->irreducibleQuantity[$productStore->store_id])) ? $model->irreducibleQuantity[$productStore->store_id] : '' ?>
                </td>
                <td class="price-for-buy" data-value="<?= $model->price_for_buy_with_nds; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
                <td class="amount-for-buy"
                    data-value="<?= $model->price_for_buy_with_nds * $productStore->quantity; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds * $productStore->quantity, 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
                <td class="price-for-sell" data-value="<?= $model->price_for_sell_with_nds; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?>
                </td>
                <td class="amount-for-sell"
                    data-value="<?= $model->price_for_sell_with_nds * $productStore->quantity; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds * $productStore->quantity, 2); ?>
                </td>
            </tr>
        <?php endforeach ?>
        <tr class="total-row text-grey" style="font-weight: bold;">
            <td colspan="2" style="vertical-align: middle;">Итого</td>
            <td class="total-store-quantity" style="text-align: right; vertical-align: middle;">
                <?= $totalStoreQuantity; ?>
            </td>
            <td class="total-irreducible-quantity" style="text-align: right; vertical-align: middle;">
                <?= $totalIrreducibleQuantity; ?>
            </td>
            <td class="total-price-for-buy" style="text-align: right; vertical-align: middle;"></td>
            <td class="total-amount-for-buy" style="text-align: right; vertical-align: middle;">
                <?= $canViewPriceForBuy ?
                    TextHelper::invoiceMoneyFormat($totalAmountForBuy, 2) :
                    Product::DEFAULT_VALUE; ?>
            </td>
            <td class="total-price-for-sell" style="text-align: right; vertical-align: middle;"></td>
            <td class="total-amount-for-sell" style="text-align: right; vertical-align: middle;">
                <?= TextHelper::invoiceMoneyFormat($totalAmountForSell, 2); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php Pjax::end(); ?>
<?php $this->registerJs('
    $(document).on("keyup change", "input.product-store-irreducible-quantity", function() {
        recalculateProductStoreTable();
    });

    function recalculateProductStoreTable() {
        var $items = $(".product-store-table .store-row");
        var $totalRow = $(".product-store-table .total-row");
        var $totalIrreducibleQuantity = 0;
        $items.each(function () {
            $irreducibleQuantity = +$(this).find("td.store-irreducible-quantity input").val();
            if (!isNaN($irreducibleQuantity)) {
                $totalIrreducibleQuantity += $irreducibleQuantity;
            }
        });
        $totalRow.find(".total-irreducible-quantity").text($totalIrreducibleQuantity);
    }
'); ?>
