<?php

use common\models\Contractor;
use kartik\select2\Select2;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

?>
<?= $form->field($model, 'comment')->textarea([
    'maxlength' => true,
    'rows' => 4,
    'style' => 'width: 100%;'
]); ?>