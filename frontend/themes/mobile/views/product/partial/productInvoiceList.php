<?php
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\Store;
use frontend\models\Documents;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$productIdArray = Yii::$app->request->post('product', []);
?>

<?php Pjax::begin([
    'id' => 'pjax-product-grid',
    'linkSelector' => false,
    'formSelector' => '#products_in_order',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

    <?php if ($productType == 1 ) : ?>
        <?php $emptyText = 'Все товары уже добавлены'; ?>
        <h4 class="modal-title">Выбрать товар из списка</h4>
    <?php else : ?>
        <?php $emptyText = 'Все услуги уже добавлены'; ?>
        <h4 class="modal-title">Выбрать услугу из списка</h4>
    <?php endif; ?>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>

    <?= Html::beginForm(['/product/get-products'], 'get', [
        'id' => 'products_in_order',
        'class' => 'add-to-invoice',
        'data' => [
            'pjax' => true,
        ]
    ]); ?>

    <?= Html::hiddenInput('documentType', $documentType, [
        'id' => 'documentTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('productType', $productType, [
        'id' => 'productTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('searchTitle', $searchModel->title, [
        'id' => 'searchTitleHidden',
    ]); ?>

    <?=  Html::hiddenInput('store_id', $storeId, [
        'id' => 'storeIdHidden',
    ]); ?>

    <?php foreach ((array) $searchModel->exclude as $value) {
        echo Html::hiddenInput('exists[]', $value);
    } ?>

    <?php if ($productType == 1) {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;

        $storeSelectList = $user->getStores()
            ->orderBy(['is_main' => SORT_DESC])
            ->indexBy('id')
            ->all();

        $selectedStoreId = $storeId;

        if ($selectedStoreId && ($selectedStore = Store::findOne($selectedStoreId))) {
            $currentStoreId = $selectedStore->id;
            $currentStoreName = $selectedStore->name;
        } elseif ($storeSelectList) {
            $currentStore = reset($storeSelectList);
            $currentStoreId = $currentStore->id;
            $currentStoreName = $currentStore->name;
        } else {
            $currentStoreId = null;
            $currentStoreName = "НЕТ СКЛАДА";
        }

        //foreach ($storeSelectList as $key => $store) {
        //    $storeItems[] = [
        //        'label' => $store->name,
        //        'url' => 'javascript:;',
        //        'linkOptions' => [
        //            'class' => 'store-opt ' . ($currentStoreId == $key ? 'active' : ''),
        //            'data-id' => $store->id
        //        ],
        //    ];
        //}
    } ?>

    <div class="d-flex flex-nowrap">
        <?php if ($productType == 1 && $storeSelectList): ?>
            <div class="form-group flex-grow-1 mr-2">
                <div class="store-dropdown-wrap dropdown">
                    <button id="storeDropdown" class="store-dropdown" type="button" data-toggle="dropdown" aria-expanded="true">
                        <span class="store-name"><?= $currentStoreName ?></span><span class="caret"></span>
                    </button>
                    <div id="storeDropdown" class="form-filter-drop dropdown-menu" aria-labelledby="storeDropdown" x-placement="bottom-start">
                        <ul class="form-filter-list list-clr">
                            <?php foreach ($storeSelectList as $key => $store): ?>
                                <li><a data-id="<?= $store->id ?>" class="store-opt <?= ($currentStoreId == $key ? 'active' : '') ?>" href="javascript:;"><?= $store->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::textInput('title', $searchModel->title, [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control',
                'id' => 'product-title-search',
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-red button-red_small',
            ]); ?>
        </div>
    </div>


    <div class="add-to-invoice-table" id="add-to-invoice-tbody">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-goods table-style',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'rowOptions' => [
                        'role' => 'row',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr justify-content-end',
                        ],
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'layout' => "{items}\n{pager}",
                    'emptyText' => $searchModel->exclude ? $emptyText : 'Ничего не найдено.',
                    'columns' => [
                        [
                            'format' => 'raw',
                            'value' => function (Product $model) use ($productIdArray) {
                                return Html::checkbox('in_order[]', in_array($model->id, $productIdArray), [
                                    'id' => 'product-' . $model->id,
                                    'value' => $model->id,
                                    'class' =>'product_selected',
                                ]);
                            },
                            'headerOptions' => [
                                'width' => '3%'
                            ],
                            'header' => Html::checkbox('in_order_all', false, [
                                'id' => 'product_selected-all',
                            ]),
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Продукция',
                            'headerOptions' => [
                                'width' => '65%'
                            ],
                            'format' => 'text',
                        ],
                        [
                            'attribute' => 'article',
                            'headerOptions' => [
                                'width' => '30%'
                            ],
                            'format' => 'text',
                            'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                        ],
                        //Группа
                        [
                            'attribute' => 'group_id',
                            'label' => 'Группа',
                            'class' => DropDownSearchDataColumn::className(),
                            'headerOptions' => [
                                'width' => '30%'
                            ],
                            'filter' => [null => 'Все'] + ArrayHelper::map(ProductGroup::getGroups(), 'id', 'title'),
                            'format' => 'raw',
                            'value' => 'group.title',
                            'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                        ],
                        //Количество на складе
                        [
                            'attribute' => 'quantity',
                            'label' => 'Количество на складе',
                            'headerOptions' => [
                                'width' => '30%'
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data->quantity * 1;
                            },
                            'visible' => $productType == Product::PRODUCTION_TYPE_GOODS &&
                                         $documentType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'attribute' => 'price_for_sell_with_nds',
                            'label' => 'Цена продажи',
                            'format' => 'raw',
                            'headerOptions' => [
                                'width' => '30%'
                            ],
                            'value' => function (Product $model) {
                                return TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds);
                            },
                            'visible' => $documentType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'attribute' => 'price_for_buy_with_nds',
                            'label' => 'Цена покупки',
                            'format' => 'raw',
                            'headerOptions' => [
                                'width' => '30%'
                            ],
                            'value' => function (Product $model) {
                                return TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds);
                            },
                            'visible' => $documentType == Documents::IO_TYPE_IN,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?= Html::endForm(); ?>

    <br>
    <div class="d-flex justify-content-between mt-2">
        <?= Html::button('Добавить отмеченные', [
            'class' => 'button-regular button-regular_red button-clr',
            'data-dismiss' => 'modal',
            'id' => 'add-to-invoice-button',
            'style' => 'margin-left: 15px;'
        ]); ?>
    </div>

<script type="text/javascript">
$(document).on("change", "#product_selected-all", function() {
    $("input.product_selected").prop("checked", $(this).prop("checked")).trigger("change").uniform();
});
$(document).ready(function () {
    // init dropdown
    $('#storeDropdown').click();
});
</script>

<?php Pjax::end(); ?>