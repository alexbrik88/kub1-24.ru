<?php
use common\models\product\Product;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */

$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;

?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'action' => ['create-from-invoice', 'productionType' => $model->production_type,],
    'id' => 'new-product-invoice-form',
    'options' => [
        'class' => 'js_form_ajax_submit',
        'data' => [
            //'callback' => ['INVOICE', 'addProductToTable'],
            'callback' => new \yii\web\JsExpression('
                if (INVOICE.addProductToTable(data)) {
                    $(form).parents(".modal").modal("hide");
                } else {
                    alert("Ошибка!");
                }
            '),
        ],
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-5 control-label bold-text',
        ],
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
])); ?>

    <div class="form-body form-horizontal">

        <?= Html::hiddenInput('documentType', $documentType); ?>

        <?= $this->render('_mainForm', [
            'model' => $model,
            'form' => $form,
        ]); ?>

        <div class="form-actions">
            <?= Html::submitButton('Добавить', [
                'class' => 'btn darkblue t-col',
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

