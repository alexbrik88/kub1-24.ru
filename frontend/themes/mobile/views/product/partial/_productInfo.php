<?php
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Contractor;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

$units = ProductUnit::findSorted()
    ->andWhere(['goods' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}

$shortFieldOptions = [
    'inputOptions' => [
        'class' => 'form-control form-control_width_140'
    ]
];
$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Группа <?= ($isGoods) ? 'товара' : 'услуги' ?></label>
                </div>
                <?= $form->field($model, 'group_id', [
                    'options' => [
                        'class' => 'form-group'
                    ],
                ])->widget(ProductGroupDropdownWidget::classname(), [
                    'productionType' => Product::PRODUCTION_TYPE_GOODS,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Категория</label>
                </div>
                <?= Select2::widget([
                    'hideSearch' => true,
                    'name' => 'product_category',
                    'data' => ['--'],
                    'options' => [
                        'prompt' => '--',
                        'options' => $unitsOptions,
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
        </div>
    </div>
    <?php if ($isGoods): ?>
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <label class="label weight-700" for="">Код</label>
                <?= $form->field($model, 'code')->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <label class="label weight-700" for="">Артикул</label>
                <?= $form->field($model, 'article')->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="form-group column mb-3 pb-2">
                <label class="label weight-700" for="">Штрих код</label>
                <?= $form->field($model, 'barcode', $shortFieldOptions)->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <div class="form-filter">
                        <label class="label weight-700" for="cause">Единица измерения</label>
                    </div>
                    <?= $form->field($model, 'product_unit_id', [
                        'options' => [
                            'class' => 'form-group required'
                        ],
                    ])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($units, 'id', 'name'),
                        'options' => [
                            'prompt' => '',
                            'options' => $unitsOptions,
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                </div>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Вес (кг)</label>
                    <?= $form->field($model, 'weight')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group column mb-3 pb-2">
                    <label class="label weight-700" for="">Объем (м3)</label>
                    <?= $form->field($model, 'volume', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group column mb-3 pb-2">
                    <label class="label weight-700" for="">Масса брутто</label>
                    <?= $form->field($model, 'mass_gross', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                        'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Вид упаковки</label>
                    <?= $form->field($model, 'box_type')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Количество в одном месте</label>
                    <?= $form->field($model, 'count_in_place')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group column mb-3 pb-2">
                    <label class="label weight-700" for="">Количество мест, штук</label>
                    <?= $form->field($model, 'place_count', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <div class="form-filter">
                        <label class="label weight-700" for="cause">Страна происхождения товара</label>
                    </div>
                    <?= $form->field($model, 'country_origin_id')
                        ->widget(Select2::class, [
                            'data' => ArrayHelper::map(Country::getCountries(), 'id', 'name_short'),
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>

                </div>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Номер таможенной декларации</label>
                    <?= $form->field($model, 'customs_declaration_number')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group column mb-3 pb-2">
                    <label class="label weight-700" for="">Код вида товара</label>
                    <?= $form->field($model, 'item_type_code', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <?php /*<div class="form-group column mb-3 pb-2">
                    <?= $form->field($model, 'is_alco')->checkbox([], true); ?>
                </div>*/ ?>
            </div>
        </div>
    <?php endif; ?>
</div>
















