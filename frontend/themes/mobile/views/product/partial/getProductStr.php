<?php
use common\components\TextHelper;
use frontend\models\Documents;
use yii\helpers\Html;

/* @var $product common\models\product\Product */
/* @var $documentType int */
?>

<tr role="row" class="odd">
    <td>
        <span class="">
            <?= Html::checkbox('in_order[' . $product['id'] . ']', false, [
                'id' => $product['id'],
            ]) ?>
        </span>
    </td>
    <td><?= $product['title']; ?></td>
    <td>
        <?= ($documentType == Documents::IO_TYPE_OUT ?
            TextHelper::invoiceMoneyFormat($product['price_for_sell_with_nds'], 2) :
            TextHelper::invoiceMoneyFormat($product['price_for_buy_with_nds'], 2)); ?>
    </td>
</tr>