<?php
use common\components\TextHelper;
use common\models\product\Product;
use yii\helpers\Html;
/* @var $product Product */
?>

<tr role="row" class="odd" id="<?= $product->id; ?>">
    <td><span aria-hidden="true" class="icon-close remove-product-from-invoice" data-id="<?= $product->id; ?>"></span>
    </td>
    <td><?= $product->title; ?></td>
    <td>
        <?= Html::input('string', 'product_count[' . $product->id . ']', 1, ['class' => 'form-control',])?>
    </td>
    <td><?= $product->productUnit ? $product->productUnit->name : Product::DEFAULT_VALUE; ?></td>
    <td><?= $product->priceForSellNds->rate; ?></td>
    <td>
        <?= TextHelper::invoiceMoneyFormat($product->price_for_sell_with_nds, 2); ?>
    </td>
    <td><?= TextHelper::invoiceMoneyFormat($product->price_for_sell_with_nds, 2); ?></td>
</tr>