<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\components\XlsHelper;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use frontend\rbac\permissions;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\document\status\OrderDocumentStatus;
use common\models\Company;
use common\components\ImageHelper;
use common\models\product\ProductSearch;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\models\product\PriceList;
use common\models\product\CustomPriceGroup;
use common\models\product\CustomPrice;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */
/* @var $defaultSortingAttr string */
/* @var $priceList PriceList */
/* @var $store */
/* @var $user \common\models\employee\Employee */

$this->title = Product::$productionTypes[$productionType];

$userConfig = Yii::$app->user->identity->config;

$taxationType = $company->companyTaxationType;

$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();
$xlsMessageType = $productionType ? 'товаров' : 'услуг';

if ($productionType == 1) {
    $emptyMessage = 'Вы еще не добавили ни одного товара';
} elseif ($productionType == 0) {
    $emptyMessage = 'Вы еще не добавили ни одной услуги';
} else {
    $emptyMessage = 'Ничего не найдено';
}

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);

$actionItems = [];
if ($canUpdate) {
    $actionItems[] = [
        'label' => 'Изменить цену',
        'url' => '#many-price-modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    if (count($storeList) > 1 && $productionType == Product::PRODUCTION_TYPE_GOODS) {
        $actionItems[] = [
            'label' => 'Переместить на склад',
            'url' => '#move_to_store_modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    if ($taxationType->osno || $taxationType->usn) {
        $actionItems[] = [
            'label' => 'Изменить НДС',
            'url' => '#product-nds-modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    $actionItems[] = [
        'label' => 'Объединить ' . ($productionType == Product::PRODUCTION_TYPE_GOODS ? 'товары' : 'услуги'),
        'url' => '#combine_to_one_modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
}
$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';
}
$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$filterDateItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::FILTER_YESTERDAY => 'Вчера',
    ProductSearch::FILTER_TODAY => 'Сегодня',
    ProductSearch::FILTER_WEEK => 'Неделя',
    ProductSearch::FILTER_MONTH => 'Месяц',
];

$filterCommentItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::HAS_COMMENT => 'Есть',
    ProductSearch::NO_COMMENT => 'Нет',
];

$filterImageItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::HAS_IMAGE => 'Есть',
    ProductSearch::NO_IMAGE => 'Нет',
];

$hasFilters = !($searchModel->filterStatus == ProductSearch::IN_WORK) ||
    (bool)$searchModel->filterComment ||
    (bool)$searchModel->filterImage ||
    (bool)$searchModel->filterDate;

if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $tableConfigItems = [
        [
            'attribute' => 'product_article',
        ],
        [
            'attribute' => 'product_group',
            'label' => 'Группа товара',
        ],
        [
            'attribute' => 'product_image',
        ],
        [
            'attribute' => 'product_comment',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа товара',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
    $showItems = [
        [
            'attribute' => 'product_zeroes_quantity',
            'label' => 'Товары с нулевыми остатками',
        ]
    ];

    $customPriceItems = [];
    if ($customPriceGroups = CustomPriceGroup::find()->where(['company_id' => $company->id])->all()) {
        foreach ($customPriceGroups as $customPriceGroup) {
            $customPriceItems[] = [
                'id' => $customPriceGroup->id,
                'show_column' => $customPriceGroup->show_column,
                'label' => $customPriceGroup->name,
                'url' => '/product/config-custom-price'
            ];
        }
    }

} else {
    $tableConfigItems = [
        [
            'attribute' => 'product_group',
            'label' => 'Группа услуги',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа услуги',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
    $showItems = [];
    $customPriceItems = [];
    if ($customPriceGroups = CustomPriceGroup::find()->where(['company_id' => $company->id])->all()) {
        foreach ($customPriceGroups as $customPriceGroup) {
            $customPriceItems[] = [
                'id' => $customPriceGroup->id,
                'show_column' => $customPriceGroup->show_column,
                'label' => $customPriceGroup->name,
                'url' => '/product/config-custom-price'
            ];
        }
    }
}

$this->registerJs('
    $(document).on("click", "input.select-on-check-all", function(e) {
        $("input.product_checker").prop("checked", this.checked).uniform("refresh");
        $("input.select-on-check-all").prop("checked", this.checked).uniform("refresh");
    });
    $(document).on("change", "input.product_checker", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#mass_actions").fadeIn();
            if ($("input.product_checker:not(:checked)").length) {
                $(".select-on-check-all").prop("checked", false).uniform("refresh");
            } else {
                $(".select-on-check-all").prop("checked", true).uniform("refresh");
            }
        } else {
            $("#mass_actions").fadeOut();
            $(".select-on-check-all").prop("checked", false).uniform("refresh");
        }
    });
    $(document).on("click", "#confirm-many-delete", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
    $(".tooltip-help").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "side": "right",
        "contentAsHTML": true,
    });
');
$help = $productionType == Product::PRODUCTION_TYPE_GOODS ? Html::tag('span', '', [
    'class' => 'tooltip-help ico-question',
    'data-tooltip-content' => '#tooltip_store_help',
    'style' => 'margin: 0 0 0 15px;; cursor: pointer;'
]) : null;

$priceListsExists = $company->getPriceLists($productionType)
    ->andWhere(['is_deleted' => false])
    ->orderBy(['created_at' => SORT_DESC])
    ->exists();

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_product');
?>

<div class="stop-zone-for-fixed-elems product-index">
    <div class="page-head d-flex flex-wrap align-items-center">

        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
            <?= \frontend\themes\mobile\widgets\StoreFilterWidget::widget([
                'pageTitle' => $this->title,
                'store' => $store,
                'productionType' => $productionType,
                'help' => $help,
                'company' => $company,
            ]); ?>
        <?php else: ?>
            <h4><?= Html::encode($this->title); ?></h4>
        <?php endif; ?>

        <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
            <a href="<?= Url::toRoute(['create', 'productionType' => $productionType]) ?>"
               class="button-regular button-regular_padding_medium button-regular_red ml-auto mb-2">
                <svg class="svg-icon mr-2">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                </svg>
                <span class="ml-1">Добавить</span>
            </a>
        <?php endif; ?>
    </div>

    <?php if ($dataProvider->totalCount <= 10000) : ?>
        <?php
            $stat1 = [
                'sum' => $searchModel->getAllAmountBuy(),
                'count' => $searchModel->getAllQuantity()
            ];
            $stat2 = [
                'sum' => $searchModel->getAllAmountSell(),
                'count' => $searchModel->getAllQuantity()
            ];
            $stat3 = [
                'sum' => $searchModel->getAllReserveAmount(),
                'count' => $searchModel->getAllReserve()
            ];
            \frontend\themes\mobile\helpers\KubHelper::setStatisticsFontSize(max($stat1['sum'], $stat2['sum'], $stat3['sum']) );
        ?>
        <div class="wrap wrap_count">
            <div class="row">
                <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                <div class="col-6 col-xl-3">
                    <div class="count-card count-card_yellow wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Стоимость склада в ЗАКУПКЕ</div>
                        <hr>
                        <div class="count-card-foot">Количество штук: <?= TextHelper::numberFormat($stat1['count']); ?></div>
                    </div>
                </div>
                <div class="col-6 col-xl-3">
                    <div class="count-card count-card_turquoise wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Стоимость склада при ПРОДАЖЕ</div>
                        <hr>
                        <div class="count-card-foot">Количество штук: <?= TextHelper::numberFormat($stat2['count']); ?></div>
                    </div>
                </div>
                <div class="col-6 col-xl-3">
                    <div class="count-card count-card_grey wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Зарезервированно на СУММУ</div>
                        <hr>
                        <div class="count-card-foot">Количество штук: <?= TextHelper::numberFormat($stat3['count']); ?></div>
                    </div>
                </div>
                <?php else: ?>
                    <div class="col-xl-9"></div>
                <?php endif; ?>
                <div class="col-6 col-xl-3 d-flex flex-column">
                    <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)
                    ) : ?>
                    <div class="dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductSales" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span>Продажа</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductSales">
                            <div class="popup-dropdown-in overflow-hidden overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('Выставить счёт', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2]),
                                            'title' => 'Выберите ' . ($productionType ? 'товар' : 'услугу') . ' для создания счета',
                                            'disabled' => true,
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Выставить счёт-договор', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2, 'isContract' => 1]),
                                            'title' => 'Выберите ' . ($productionType ? 'товар' : 'услугу') . ' для создания счета-договора',
                                            'disabled' => true,
                                        ]) ?>
                                    </li>
                                    <?php /*
                                    <li>
                                        <?= Html::a('Создать заказ', null, [
                                            'class' => 'nowrap add-to-order',
                                            'data-action' => Url::to(['documents/order-document/create',]),
                                            'title' => 'Выберите ' . ($productionType ? 'товар' : 'услугу') . ' для создания заказа',
                                            'disabled' => true,
                                        ]); ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">Накладная на возраст</a></li>-->
                                    */ ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_IN)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)
                    ) : ?>
                    <div class="dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductPurchase" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span>Покупка</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductPurchase">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('По счёту', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 1]),
                                            'title' => 'Выберите ' . ($productionType ? 'товар' : 'услугу') . ' для создания счета',
                                            'disabled' => true,
                                        ]) ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">По приходному ордеру</a></li>
                                    <li><a class="nowrap" href="#">Без документов</a></li>
                                    <li><a class="nowrap" href="#">Создать заказ</a></li>
                                    <li><a class="nowrap" href="#">Начальные остатки</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                        <?= Html::a('Загрузить из Excel', '#import-xls', [
                            'class' => 'button-regular w-100 button-hover-content-red',
                            'data' => [
                                'toggle' => 'modal',
                            ],
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="mt-3"></div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([
        'method' => 'GET',
        'action' => Url::current([$searchModel->formName() => null]),
        'id' => 'form_product_filters',
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'options' => [
                'class' => '',
            ],
        ],
    ]); ?>
    <div class="table-settings row row_indents_s">
        <div class="col-8">
            <div class="row align-items-center">
                <div class="sub-title column"><?= $tableHeader[$productionType] ?>: <strong><?= $dataProvider->totalCount; ?></strong></div>
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                    <?php $iconExcel = '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#exel"></use></svg>'; ?>
                    <?= Html::a($iconExcel, array_merge(['get-xls'], Yii::$app->request->queryParams), [
                        'class' => 'get-xls-link button-regular button-regular_red button-clr w-44 mr-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
                    <?= TableConfigWidget::widget([
                        'items' => $tableConfigItems,
                        'sortingItems' => $sortingItems,
                        'showItems' => $showItems,
                        'customPriceItems' => $customPriceItems,
                        'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2'
                    ]); ?>
                    <?= TableViewWidget::widget(['attribute' => 'table_view_product']) ?>
                    <!-- Filters -->
                    <div class="dropdown popup-dropdown popup-dropdown_filter products-filter <?= $hasFilters ? 'itemsSelected' : '' ?>">
                        <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="button-txt">Фильтр</span>
                            <svg class="svg-icon svg-icon-shevron">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div class="dropdown-menu keep-open" aria-labelledby="filter">
                            <div class="popup-dropdown-in p-3">
                                <div class="p-1">
                                    <div class="row">
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Показывать</div>
                                                <?= Html::activeHiddenInput($searchModel, 'filterStatus') ?>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterStatusItems, $searchModel->filterStatus) ?>
                                                    </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown1">
                                                    <div class="dropdown-drop-in">
                                                        <ul class="form-filter-list list-clr">
                                                            <?php foreach ($filterStatusItems as $key=>$item): ?>
                                                                <li>
                                                                    <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterStatus) ?>"><?= $item ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown2">
                                                <div class="label">По дате добавления</div>
                                                <?= Html::activeHiddenInput($searchModel, 'filterDate') ?>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterDateItems, $searchModel->filterDate) ?>
                                                    </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown2">
                                                    <div class="dropdown-drop-in">
                                                        <ul class="form-filter-list list-clr">
                                                            <?php foreach ($filterDateItems as $key=>$item): ?>
                                                                <li>
                                                                    <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterDate) ?>"><?= $item ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown3">
                                                <div class="label">Наличие картинки</div>
                                                <?= Html::activeHiddenInput($searchModel, 'filterImage') ?>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterImageItems, $searchModel->filterImage) ?>
                                                    </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown3">
                                                    <div class="dropdown-drop-in">
                                                        <ul class="form-filter-list list-clr">
                                                            <?php foreach ($filterImageItems as $key=>$item): ?>
                                                                <li>
                                                                    <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterImage) ?>"><?= $item ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown4">
                                                <div class="label">Наличие описания</div>
                                                <?= Html::activeHiddenInput($searchModel, 'filterComment') ?>
                                                <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown4">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterCommentItems, $searchModel->filterComment) ?>
                                                    </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown4">
                                                    <div class="dropdown-drop-in">
                                                        <ul class="form-filter-list list-clr">
                                                            <?php foreach ($filterCommentItems as $key=>$item): ?>
                                                                <li>
                                                                    <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterComment) ?>"><?= $item ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="col-12 mt-3">
                                            <div class="row justify-content-between">
                                                <div class="form-group column">
                                                    <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                        <span>Применить</span>
                                                    </button>
                                                </div>
                                                <div class="form-group column">
                                                    <button id="product_filters_reset" class="button-regular button-hover-content-red button-width-medium button-clr" data-clear="dropdown" type="button">
                                                        <span>Сбросить</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= $form->field($searchModel, 'title', [
                    ])->textInput([
                        'placeholder' => 'Поиск...',
                        'type' => 'search',
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php $form->end(); ?>

</div>

<?php

$columnsLeft = [
    [
        'class' => 'yii\grid\CheckboxColumn',
        'cssClass' => 'product_checker',
        'headerOptions' => [
            'width' => '1%',
        ],
    ],
    [
        'attribute' => 'article',
        'headerOptions' => [
            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
        ],
        'contentOptions' => [
            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
        ],
    ],
    [
        'attribute' => 'title',
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'contentOptions' => [
            'class' => 'title-cell col_product_name',
        ],
        'format' => 'raw',
        'value' => function (Product $data) use ($productionType) {
            $title = (mb_strlen($data->title, 'UTF-8') > 100) ? (mb_substr($data->title, 0, 95, 'utf-8') . '...') : $data->title;
            if (Yii::$app->user->can(\frontend\rbac\permissions\Product::VIEW)) {
                $content = Html::a(Html::encode($title), [
                    'view',
                    'productionType' => $productionType,
                    'id' => $data->id,
                ]);
            } else {
                $content = Html::encode($title);
            }

            return $content;
        },
    ],
    [
        'attribute' => 'group_id',
        'enableSorting' => false,
        'label' => 'Группа ' . '<br>' . ($productionType ? 'товара' : 'услуги'),
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => 'dropdown-filter col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
            'width' => '14%',
        ],
        'contentOptions' => [
            'class' => 'col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
        ],
        'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
        'format' => 'raw',
        'value' => 'group.title',
        's2width' => '200px'
    ],
    [
        'attribute' => 'available',
        'label' => 'Доступно',
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            return $data->available * 1;
        },
        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
    ],
    [
        'attribute' => 'reserve',
        'label' => 'Резерв',
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            $tooltipId = 'product-reserve-tooltip-' . $data->id;
            $content = Html::a($data->reserve * 1, null, [
                'class' => 'product-reserve-value',
                'data' => [
                    'tooltip-content' => '#' . $tooltipId,
                    'url' => Url::to(['/product/reserve-documents', 'id' => $data->id]),
                ],
            ]);
            $content .= Html::tag('span', Html::tag('span', '', ['id' => $tooltipId]), [
                'class' => 'hidden',
            ]);

            return $content;
        },
        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
    ],
    [
        'attribute' => 'quantity',
        'label' => 'Остаток',
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'contentOptions' => [
            'class' => 'col_quantity',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            return $data->quantity * 1;
        },
        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
    ],
    [
        'attribute' => 'product_unit_id',
        'enableSorting' => false,
        'label' => 'Ед. измерения',
        'headerOptions' => [
            'class' => 'dropdown-filter',
            'width' => '5%',
        ],
        'contentOptions' => [
            'class' => 'col_product_unit',
        ],
        'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
        'format' => 'raw',
        'value' => 'productUnit.name',
    ],
    [
        'label' => 'Картинка',
        'headerOptions' => [
            'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
            'width' => '30px',
        ],
        'contentOptions' => [
            'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
        ],
        'format' => 'raw',
        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
        'value' => function (Product $data) {
            $content = '';
            if ($thumb = $data->getImageThumb(200, 300)) {
                $tooltipId = 'tooltip-product-image-' . $data->id;
                $content = Html::tag('span', '', [
                    'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                    'data-tooltip-content' => '#' . $tooltipId,
                    'style' => 'cursor: pointer;',
                ]);
                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                $content .= Html::beginTag('div', ['id' => $tooltipId]);
                $content .= Html::img($thumb, ['alt' => '']);
                $content .= Html::endTag('div');
                $content .= Html::endTag('div');
            }

            return $content;
        },
    ],
    [
        'attribute' => 'comment_photo',
        'label' => 'Описание',
        'headerOptions' => [
            'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
            'width' => '25%',
        ],
        'contentOptions' => [
            'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
        ],
        'format' => 'raw',
        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
        'value' => function ($data) {
            $content = '';
            if ($data->comment_photo) {
                $tooltipId = 'tooltip-product-comment-' . $data->id;
                $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                    'class' => 'product-comment-box',
                    'data-tooltip-content' => '#' . $tooltipId,
                    'style' => 'cursor: pointer;'
                ]);
                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                $content .= Html::encode($data->comment_photo);
                $content .= Html::endTag('div');
                $content .= Html::endTag('div');
            }

            return $content;
        },
    ],
    [
        'attribute' => 'price_for_buy_with_nds',
        'label' => 'Цена<br>покупки',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
        ],
        'format' => 'raw',
        'value' => function ($data) use ($user) {
            $currentEmployeeCompany = $user->currentEmployeeCompany;
            $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                (!$currentEmployeeCompany->can_view_price_for_buy &&
                    $currentEmployeeCompany->is_product_admin &&
                    $data->creator_id == $currentEmployeeCompany->employee_id);

            return $canViewPriceForBuy ?
                TextHelper::invoiceMoneyFormat($data->price_for_buy_with_nds, 2) :
                Product::DEFAULT_VALUE;
        },
        'visible' => Yii::$app->user->can(permissions\document\Document::DOCUMENTS_IN),
    ],
    [
        'attribute' => 'price_for_buy_nds_id',
        'label' => 'НДС<br>покупки',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => 'priceForBuyNds.name',
        'visible' => Yii::$app->user->can(permissions\document\Document::DOCUMENTS_IN) &&
            Yii::$app->user->identity->company->hasNds(),
    ],
    [
        'attribute' => 'amountForBuy',
        'label' => 'Стоимость<br>покупки',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
        ],
        'format' => 'raw',
        'value' => function ($data) use ($user) {
            $currentEmployeeCompany = $user->currentEmployeeCompany;
            $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                (!$currentEmployeeCompany->can_view_price_for_buy &&
                    $currentEmployeeCompany->is_product_admin &&
                    $data->creator_id == $currentEmployeeCompany->employee_id);

            return $canViewPriceForBuy ?
                TextHelper::invoiceMoneyFormat($data->amountForBuy, 2) :
                Product::DEFAULT_VALUE;
        },
        'visible' => Yii::$app->user->can(permissions\document\Document::DOCUMENTS_IN) &&
            ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
    ]
];

$columnsRight = [
    [
        'attribute' => 'price_for_sell_with_nds',
        'label' => 'Цена<br>продажи',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            return $data->not_for_sale ? 'Не для продажи' :
                TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
        },
    ],
    [
        'attribute' => 'price_for_sell_nds_id',
        'label' => 'НДС<br>продажи',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            return $data->not_for_sale ? 'Не для продажи' : ($data->priceForSellNds ? $data->priceForSellNds->name : '---');
        },
        'visible' => Yii::$app->user->identity->company->hasNds(),
    ],
    [
        'attribute' => 'amountForSell',
        'label' => 'Стоимость<br>продажи',
        'encodeLabel' => false,
        'headerOptions' => [
            'class' => 'sorting',
        ],
        'format' => 'raw',
        'value' => function ($data) {
            return TextHelper::invoiceMoneyFormat($data->amountForSell, 2);
        },
        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
    ],
];

$customPriceColumns = [];
if ($customPriceGroups = CustomPriceGroup::find()->where(['company_id' => $company->id])->all()) {
    foreach ($customPriceGroups as $customPriceGroup) {
        $customPriceColumns[] = [
            'attribute' => 'none',
            'headerOptions' => [
                'class' => "col_custom_price_{$customPriceGroup->id}" . ($customPriceGroup->show_column ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => "col_custom_price_{$customPriceGroup->id}" . ($customPriceGroup->show_column ? '' : ' hidden'),
            ],
            'label' => $customPriceGroup->name,
            'encodeLabel' => false,
            'format' => 'raw',
            'value' => function ($data) use ($customPriceGroup) {
                if ($customPrice = CustomPrice::findOne(['product_id' => $data['id'], 'price_group_id' => $customPriceGroup->id]))
                    return TextHelper::invoiceMoneyFormat($customPrice->price, 2);

                return '';
            },
        ];
    }
}

$gridColumns = $columnsLeft;
if ($customPriceColumns) $gridColumns = array_merge($gridColumns, $customPriceColumns);
$gridColumns = array_merge($gridColumns, $columnsRight);

?>


<?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list' . $tabViewClass,
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    //'options' => [
    //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    //],
    'rowOptions' => [
        'role' => 'row',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => $gridColumns
    ,
]); ?>
<?= Html::endForm(); ?>

<!-- ДЕЙСТВИЯ -->
<?php /*
<div class="portlet box darkblue">
    <div class="portlet-title">

        <div id="mass_actions" class="actions joint-operations col-sm-5"
             style="float:left;display: none;width: 25%;">
            <?php if ($canUpdate) : ?>
                <?= $this->render('partial/_many_price', [
                    'company' => $company,
                    'productionType' => $productionType,
                ]) ?>
            <?php endif ?>
            <?php if ($actionItems) : ?>
                <div class="pull-left" style="position: relative; display: inline-block;">
                    <div class="dropdown">
                        <?= Html::a('Действия  <span class="caret"></span>', null, [
                            'class' => 'btn btn-default btn-sm dropdown-toggle',
                            'data-toggle' => 'dropdown',
                            'style' => 'height: 28px; color: #def1ff;',
                        ]); ?>
                        <?= Dropdown::widget([
                            'items' => $actionItems,
                            'options' => [
                                'style' => 'right: -30px !important; left: auto; top: 25px;',
                            ],
                        ]); ?>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm pull-left',
                    'data-toggle' => 'modal',
                ]); ?>
                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        Вы уверены, что хотите удалить выбранные
                                        <?= $productionType ? 'товары' : 'услуги'; ?>?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::tag('button', '<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', [
                                            'id' => 'confirm-many-delete',
                                            'class' => 'btn darkblue pull-right ladda-button',
                                            'data-action' => Url::to(['many-delete', 'productionType' => $productionType]),
                                            'data-style' => 'expand-right',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue" data-dismiss="modal">
                                            НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

    <div class="portlet-body accounts-list">

        <div class="table-container products-table clearfix" style="">

        </div>

    </div>
</div>
*/ ?>

<?php /*
if ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE) {
    $actionItems[] = [
        'label' => 'Переместить в архив',
        'url' => 'javascript:;',
        'linkOptions' => [
            'id' => 'many-to-archive',
            'data' => [
                'url' => '/product/to-archive',
            ],
        ],
    ];
} else {
    $actionItems[] = [
        'label' => 'Извлечь из архива',
        'url' => 'javascript:;',
        'linkOptions' => [
            'id' => 'many-to-store-from-archive',
            'data' => [
                'url' => '/product/to-store-from-archive',
            ],
        ],
    ];
}
*/ ?>

<?= \frontend\themes\mobile\widgets\SummarySelectProductWidget::widget([
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'folder']).' <span>В группу</span>', '#move_to_group_modal', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => $actionItems,
                'options' => [
                    'class' => 'form-filter-list list-clr '
                ],
            ]), ['class' => 'dropup']) : null,
        ($canUpdate && ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE)) ? Html::a($this->render('//svg-sprite', ['ico' => 'archiev']).' <span>В архив</span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'id' => 'many-to-archive',
            'data' => [
                'url' => '/product/to-archive',
            ],
        ]) : null,
        ($canUpdate && ((int)$searchModel->filterStatus === ProductSearch::IN_ARCHIVE)) ? Html::a($this->render('//svg-sprite', ['ico' => 'archiev']).' <span>Из архива</span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'id' => 'many-to-store-from-archive',
            'data' => [
                'url' => '/product/to-store-from-archive',
            ],
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'productionType' => $productionType
]); ?>

<!-- MODALS -->
<?php if ($canUpdate) : ?>
    <?= $this->render('partial/_many_price', [
        'company' => $company,
        'productionType' => $productionType,
    ]) ?>
<?php endif ?>

<?php if ($canDelete) : ?>
    <div id="many-delete" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные <?= $productionType ? 'товары' : 'услуги'; ?>?</h4>
                <div class="text-center">
                    <?= \yii\bootstrap4\Html::a('Да', null, [
                        'id' => 'confirm-many-delete',
                        'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-action' => Url::to(['many-delete', 'productionType' => $productionType]),
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($canUpdate && ($taxationType->osno || $taxationType->usn)): ?>
    <div class="pull-right" style="padding-right: 15px;">
        <?= $this->render('_nds_form', [
            'productionType' => $productionType,
            'toggleButton' => false
        ]) ?>
    </div>
<?php endif; ?>

<?php /*
<?php if ($countProduct == 0 && !Yii::$app->request->get('modal')) : ?>
    <?= \frontend\widgets\PromptWidget::widget([
        'prompt' => $prompt,
    ]); ?>
<?php endif; ?>
*/ ?>

<?php
if (Yii::$app->user->can(permissions\Product::CREATE)) {
    echo $this->render('/xls/_import_xls', [
        'header' => 'Загрузка ' . $xlsMessageType . ' из Excel',
        'text' => '<p>Для загрузки списка ' . $xlsMessageType . ' из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
        'formData' => Html::hiddenInput('className', 'Product') . Html::hiddenInput('Product[production_type]', $productionType),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => $productionType ? XlsHelper::PRODUCT_GOODS : XlsHelper::PRODUCT_SERVICES]),
    ]);

    if (Yii::$app->request->get('modal')) {
        $this->registerJs('$("#import-xls").modal();');
    }
    if (Yii::$app->request->get('modal_price')) {
        $this->registerJs('$(document).ready(function() { $(".price-list-trigger").click(); }); ');
    }
    if ($company->show_popup_import_products) {
        $company->updateAttributes(['show_popup_import_products' => false]);
        $this->registerJs('$("#import-xls").modal();');
    }
}

$this->registerJs('
    $(".preview-product-photo").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "side": "right",
        "contentAsHTML": true
    });
    $(".product-comment-box").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "contentAsHTML": true
    });
    $(document).ready(function () {
        $("#product-grid tbody tr").each(function() {
            var $row = $(this);
            var height = $(".product-title-cell", $row).height();
            $(".product-comment-box", $row).css("height", height + "px");
            /*$(".product-comment-box", $row).pseudo(":before", "height", height + "px");*/
        });
    });
    $(document).on("click", ".product-reserve-value:not(.tooltipstered)", function() {
        var reserve = $(this);
        var tooltipId = reserve.data("tooltip-content")
        $.ajax({
            url: reserve.data("url"),
            success: function(data) {
                if (data.result) {
                    $(reserve.data("tooltip-content")).html(data.result);
                    reserve.tooltipster({
                        theme: "tooltipster-kub",
                        trigger: "click",
                        contentCloning: true
                    });
                    reserve.tooltipster("show");
                }
            }
        })
    });
    $("#config-product_group").change(function () {
        if (!$(this).is(":checked")) {
            $(".sorting-table-config-item#title").click().uniform();
        }
    });
    $(document).on("click", "a#many-to-archive, a#many-to-store-from-archive", function() {
        $.ajax({
            type: "post",
            url: $(this).data("url"),
            data: {
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    });
    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = $(this).val();
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select").each(function () {
            var $attrName = $(this).data("id");
            var $value = 0;
            if ($attrName == "filterStatus") {
                $value = 2;
            }
            $filterData["ProductSearch[" + $attrName + "]"] = $value;
        });
        $(".filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
    }

    $(document).on("change", "#productsearch-filterdate", function (e) {
        changePeriod($(this).val(), $(this).data("url"));
    });

    function changePeriod(dateType, url) {
        var dateFrom = $("#productsearch-datefrom");
        var dateTo = $("#productsearch-dateto");
        var dateText = $(".block-date .date-text");
        var blockDate = $(".block-date");

        if (dateType == 0) {
            dateFrom.val("");
            dateTo.val("");
            dateText.text("");
            blockDate.hide();
        } else {
            $.post(url, {
                date_type: dateType,
                date_from: dateFrom.val(),
                date_to: dateTo.val()
            }, function (data) {
                dateFrom.val(data.dateFrom);
                dateTo.val(data.dateTo);
                dateText.text(data.dateText);
                blockDate.show();
            });
        }
    }
'); ?>

<?php /*
<?php if (Yii::$app->session->remove('show_example_popup')): ?>

    <?= $this->render('index/_first_show_modal') ?>

<?php elseif (!Yii::$app->request->get('modal_price') && !Yii::$app->request->get('modal') && ($company->show_popup_product && $priceListsExists) && $productionType == Product::PRODUCTION_TYPE_GOODS): ?>
    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Учёт товара
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_STOCK_CONTROL,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 5,
            'description' => 'ОПРИХОДОВАНИЕ, СПИСАНИЕ, ПЕРЕМЕЩЕНИЕ ТОВАРА.<br>
            КОНТРОЛЬ ОСТАТКОВ. ПЛАНИРОВАНИЕ ЗАКУПОК.',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-3.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => 4,
            'nextModal' => 6,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
*/ ?>
<?php if ($canUpdate): ?>
    <?= $this->render('index/move_to_group', [
        'productionType' => $productionType,
    ]); ?>
    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
        <?= $this->render('index/move_to_store', ['storeList' => $storeList, 'store' => $store]); ?>
    <?php endif; ?>
    <?= $this->render('index/combine_to_one', ['productionType' => $productionType, 'userConfig' => $userConfig]); ?>
<?php endif; ?>
<?php /*
<?= $this->render('index/price_list', [
    'priceList' => $priceList,
    'productionType' => $productionType,
    'company' => $company,
    'store' => $store,
    'user' => $user,
]); ?>
*/ ?>

<?php $this->registerJs('
    $(document).on("change", "input.product_checker", function(e) {
        if ($("input.product_checker:checked").length) {
            $(".add-to-invoice, .add-to-order").attr("disabled", false);
            $(".add-to-invoice, .add-to-order").tooltipster("disable");
        } else {
            $(".add-to-invoice, .add-to-order").attr("disabled", true);
            $(".add-to-invoice, .add-to-order").tooltipster("enable");
        }
    });
    $(document).on("click", ".add-to-invoice, .add-to-order", function(e) {
        e.preventDefault();
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });

    // SEARCH
    $("input#productsearch-title").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $(this).closest("form").first().submit();
      }
    });

    // FILTERS
    function refreshProductFiltersOLD() {
        var pop = $(".products-filter").first();
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").first().data("id");
            var i_val = $(this).find("input").val();
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(pop).addClass("itemsSelected");
        else
            $(pop).removeClass("itemsSelected");
    }

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }


    $("#form_product_filters").find(".filter-item").on("click", function(e) {
        e.preventDefault();
            var pop =  $(this).parents(".popup-dropdown_filter");
            var drop = $(this).parents(".dropdown-drop");
            var value = $(this).data("id");
            $(drop).find("input").val(value);
            $(drop).find(".drop-title").html($(this).text());
            $(drop).find(".dropdown-drop-menu").removeClass("visible show");

            refreshProductFilters();
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");
            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

    // FIX OVERFLOW BUG - otherwise input not focused
    $("#move_to_group_modal").on("select2:open", function (evt) {
        $(".modal.show").addClass("no-overflow").removeAttr("tabindex");
    });
    $("#move_to_group_modal").on("select2:close", function (evt) {
        $(".modal.show").removeClass("no-overflow");
    });

    $("#config_items_box").find("input").on("change", function(e) {
        setTimeout("$(\'.custom-scroll-table\').mCustomScrollbar(\'update\')", 250);
    });

    /* VIDIMUS BUTTON */
    $(document).on("mouseover", "input[name=\'uploadfile\']", function() {
        $(".add-xls-file").css({"color": "#335A82", "cursor":"pointer"});
    });
    $(document).on("mouseout", "input[name=\'uploadfile\']", function() {
        $(".add-xls-file").css("color", "#001424");
    });


'); ?>