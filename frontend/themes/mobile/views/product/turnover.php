<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\DataColumn;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\product\ProductSearch;
use frontend\widgets\RangeButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */

//$this->title = Product::$productionTypes[$productionType].', оборот.';
$this->title = 'Оборот товаров';
$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Оборот товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Оборот услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();

$getParam = Yii::$app->request->get((new ReflectionClass($searchModel))->getShortName());
(isset($getParam['group_id']) && $getParam['group_id'] > 0) ? $group_id = $getParam['group_id'] : $group_id = null;
(isset($getParam['product_unit_id']) && $getParam['product_unit_id'] > 0) ? $product_unit_id = $getParam['product_unit_id'] : $product_unit_id = null;

$searchQuery = clone $dataProvider->query;
$productIdArray = $searchQuery->select(['product.id'])->column();
if (!isset($turnoverType))
    $turnoverType = Yii::$app->request->getQueryParam('turnoverType');
?>

<div class="stop-zone-for-fixed-elems product-index">
    <div class="page-head d-flex flex-wrap align-items-center">

        <h4><?= Html::encode($this->title) . ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? ' в ₽' : null); ?></h4>

        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE)): ?>
            <a href="<?= Url::toRoute(['create', 'productionType' => $productionType]) ?>"
               class="button-regular button-regular_padding_medium button-regular_red ml-auto mb-2">
                <svg class="svg-icon mr-2">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                </svg>
                <span class="ml-1">Добавить</span>
            </a>
        <?php endif; ?>
    </div>



    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-3 offset-9 mb-2">
                    <?= RangeButtonWidget::widget(['cssClass' => 'btn_select_days btn_no_right cash-btn-select-days',]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->balanceAtDate(true, $productIdArray, false), 2); ?>
                            <i class="fa fa-rub"></i>
                        <?php else: ?>
                            <?= $searchModel->balanceAtDate(true, $productIdArray) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Остаток на начало периода</div>
                    <hr>
                    <div class="count-card-foot"><?= $searchModel->dateStart->format('d.m.Y') ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_turquoise wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->turnAtPeriod(true, $productIdArray, false), 2); ?>
                            <i class="fa fa-rub"></i>
                        <?php else: ?>
                            <?= $searchModel->turnAtPeriod(true, $productIdArray) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Закуплено всего</div>
                    <hr>
                    <div class="count-card-foot">&nbsp;</div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_grey wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->turnAtPeriod(false, $productIdArray, false), 2); ?>
                            <i class="fa fa-rub"></i>
                        <?php else: ?>
                            <?= $searchModel->turnAtPeriod(false, $productIdArray) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Продано всего</div>
                    <hr>
                    <div class="count-card-foot">&nbsp;</div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->balanceAtDate(false, $productIdArray, false), 2) ?>
                            <i class="fa fa-rub"></i>
                        <?php else: ?>
                            <?= $searchModel->balanceAtDate(false, $productIdArray) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Остаток на конец периода</div>
                    <hr>
                    <div class="count-card-foot"><?= $searchModel->dateEnd->format('d.m.Y') ?></div>
                </div>
            </div>
        </div>
    </div>

    <?= Html::beginForm(['turnover', 'productionType' => $productionType, 'turnoverType' => $turnoverType], 'GET'); ?>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <div class="row align-items-center">
                <div class="sub-title column" style="padding-top:10px;"><?= $tableHeader[$productionType] ?>: <strong><?= $dataProvider->totalCount; ?></strong></div>
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'title', [
                        'type' => 'search',
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'tableOptions' => [
            'class' => 'table table-style table-count-list',
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],

        //'options' => [
        //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        //],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'showFooter' => false,
        'footerRowOptions' => ['class' => 'padding-left-10'],
        'columns' => [
            [
                'attribute' => 'title',
                'class' => DataColumn::className(),
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'footer' => 'Итого единиц ',
                'value' => function (Product $data) use ($productionType) {
                    $content = (Yii::$app->user->can(\frontend\rbac\permissions\Product::VIEW))
                        ? Html::a($data->title, Url::toRoute(['view', 'productionType' => $productionType, 'id' => $data->id]))
                        : $data->title;

                    return Html::tag('div', $content, ['class' => 'product-title-wide-cell']);
                },
            ],
            [
                'attribute' => 'group_id',
                'class' => DropDownSearchDataColumn::className(),
                'enableSorting' => false,
                'label' => 'Группа товара',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '14%',
                ],
                'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups($product_unit_id), 'id', 'title')),
                'format' => 'raw',
                'value' => 'group.title',
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
            ],
            [
                'attribute' => 'product_unit_id',
                'class' => DropDownSearchDataColumn::className(),
                'enableSorting' => false,
                'label' => 'Ед.измерения',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '14%',
                ],
                'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductUnit::getUnits($group_id), 'id', 'name')),
                'format' => 'raw',
                'value' => 'productUnit.name',
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE || $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? false : true),
            ],
            [
                'attribute' => 'balanceStartValue',
                'label' => 'Остаток на начало периода',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model->balanceStartValue * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'periodBuyValue',
                'label' => 'Закуплено',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model->periodBuyValue * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'periodSaleValue',
                'label' => 'Продано',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model->periodSaleValue * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'balanceEndValue',
                'label' => 'Остаток на конец периода',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model->balanceEndValue * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
        ],
    ]); ?>

</div>