<?php

use common\models\TaxRate;
use frontend\models\ProductNdsForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $toggleButton array */

$model = new ProductNdsForm;
$ndsItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');
$idsName = Html::getInputName($model, 'ids');

$js = <<<JS
var checkOldNdsValueStatus = function() {
    $('#productndsform-from input[type=radio]').prop({
        disabled: $('#productndsform-type input[type=checkbox]:checked').length == 0
    }).uniform('refresh');
}
$(document).on('hidden.bs.modal', '#product-nds-modal', function (e) {
    $('.has-error', this).removeClass('has-error');
    $('.has-success', this).removeClass('has-success');
    $('.help-block', this).html('');
    $('input', this).prop('checked', false).uniform('refresh');
});
$(document).on('change', '.product_checker', function (e) {
    var productId = this.value;
    console.log(productId);
    if (this.checked) {
        $('<input>').attr({
            type: 'hidden',
            id: 'product-nds-selected-' + productId,
            name: '{$idsName}[]',
            value: productId
        }).appendTo('#product-nds-selected');
    } else {
        $('#product-nds-selected-' + productId).remove();
    }
    if ($('.product_checker:checked').length) {
        $('#productndsform-change').prop({checked: true}).uniform('refresh');
        $('#productndsform-type input[type=checkbox]').prop({checked: false}).uniform('refresh');
    } else {
        $('#productndsform-change').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
});
$(document).on('change', '#productndsform-change', function (e) {
    if (this.checked) {
        $('#productndsform-type input[type=checkbox]').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
    $('#product-nds-form').yiiActiveForm('validateAttribute', 'productndsform-type');
});
$(document).on('change', '#productndsform-type input[type=checkbox]', function (e) {
    if ($('#productndsform-type input[type=checkbox]:checked').length) {
        $('#productndsform-change').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
});
checkOldNdsValueStatus();
JS;

$this->registerJs($js);

if (isset($productionType))
    $model->type = $productionType;
?>

<?php Modal::begin([
    'id' => 'product-nds-modal',
    'toggleButton' => $toggleButton
]); ?>

    <h4 class="modal-title">Массовое изменение НДС</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>

    <style type="text/css">
        #product-nds-form label {
            margin-right: 30px;
        }
        #productndsform-type,
        #productndsform-price,
        #productndsform-from,
        #productndsform-to,
        .checkgroup {
            display: flex;
        }
        #productndsform-type > div,
        #productndsform-price > div,
        #productndsform-from > div,
        #productndsform-to > div,
        .checkgroup > div {
            display: inline-block;
            margin-right: 30px;
        }
    </style>
    <?php $form = ActiveForm::begin([
        'id' => 'product-nds-form',
        'action' => ['/product/change-nds'],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
    ]); ?>

        <div class="checkgroup">
            <?= $form->field($model, 'type')->checkboxList(ProductNdsForm::$typeItems, []) ?>
            <?= $form->field($model, 'change', [
                'options' => ['style' => 'margin-top: 26px;']
            ])->checkbox() ?>
        </div>
        <div id="product-nds-selected"></div>

        <?= $form->field($model, 'price')->checkboxList(ProductNdsForm::$priceItems, []) ?>
        <?= $form->field($model, 'from')->radioList($ndsItems, []); ?>
        <?= $form->field($model, 'to')->radioList($ndsItems, []); ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr',
                'data' => [
                    'url' => Url::to(['/product/to-group']),
                ],
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'data' => [
                    'dismiss' => 'modal',
                ],
            ]) ?>
        </div>


    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
