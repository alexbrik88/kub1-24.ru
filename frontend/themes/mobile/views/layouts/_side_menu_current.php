<div class="sidebar-menu-head">
    <button class="sidebar-menu-btn button-clr" style="height: 22px;">
        <span class="sidebar-menu-icon side-menu-logo">
            <?= $logo ?>
        </span>
        <span class="sidebar-menu-btn-txt" style="display: block; margin-top: -8px;">{label}</span>
    </button>
</div>