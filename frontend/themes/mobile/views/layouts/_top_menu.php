<?php

use common\models\notification\Notification;
use frontend\components\Icon;
use frontend\components\NotificationHelper;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\mobile\widgets\ConfirmModalWidget;
use frontend\widgets\AccountBalanceWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\ServiceModule;

/* @var $this \yii\web\View */
/* @var $isKUBbank bool */

$theme = $this->theme;
$company = null;
$companyItems = [];
$userItems = [
    [
        'label' => 'Профиль компании',
        'url' => ['/company/index'],
        'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
    ],
    [
        'label' => 'Визитка компании',
        'url' => ['/company/visit-card'],
        'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
    ],
    [
        'label' => 'Ваш профиль',
        'url' => ['/profile/index'],
    ],
    [
        'label' => 'Оплатить КУБ',
        'url' => ['/subscribe'],
        'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
    ],
];
$notifications = [];
if ($user) {
    $company = $user->company;

    if ($company !== null) {
        $notifications = (new NotificationHelper([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->getNotifications($company);
    }
}

$module = Yii::$app->controller->module->id;
$controller = Yii::$app->controller->id;

if (Yii::$app->user->isGuest) {
    $logoText = '';
    $logoUrl  = '#';
    $currentModule = null;
} elseif ($module == 'reports') {
    $logoText = 'ФинДиректор';
    $logoUrl  = Url::to(['/reports/finance/odds']);
    $currentModule = ServiceModule::ANALYTICS;
} elseif ($module == 'tax' || ($module == 'app-frontend' && $controller == 'notification')) {
    $logoText = 'Бухгалтерия ИП';
    $logoUrl = Url::to(['/tax/robot/index']);
    $currentModule = ServiceModule::IP_USN_6;
} elseif ($module == 'app-frontend' && $controller == 'b2b') {
    $logoText = 'Бизнес платежи';
    $logoUrl = Url::to(['/b2b/module']);
    $currentModule = ServiceModule::B2B_PAYMENT;
} else {
    $logoText = 'Выставление счетов';
    $logoUrl  = Url::to(['/documents/invoice/index', 'type' => 2]);
    $currentModule = ServiceModule::STANDART;
}

$canBuhIP = $company && $company->getCanTaxModule() && Yii::$app->user->can(UserRole::ROLE_CHIEF);
$notificationsUrl = Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]);
?>
<nav id="nav-top-menu" class="nav-top container-fluid">
    <div class="row justify-content-between">
        <div class="column">
            <div class="site-logo-box">
                <?= Html::a(Html::img('/img/fav.svg', [
                    'class' => 'logo-image',
                    'alt' => 'КУБ',
                ]), $logoUrl, [
                    'style' => 'display: inline-block; width: 2.8rem;',
                ]) ?>
            </div>
            <div class="nav-main-menu-btn">
                <?= Icon::get('menu3', [
                    'style' => 'vertical-align: top;',
                    'data-toggle' => 'modal',
                    'data-target' => '#nav-menu-modal',
                    'role' => 'button',
                ]) ?>
                <?= $this->render('_side_menu', [
                    'user' => $user,
                    'company' => $company,
                    'controller' => $controller,
                    'module' => $module,
                    'action' => $action,
                    'route' => $route,
                    'paramType' => $paramType,
                    'paramDocument' => $paramDocument,
                    'active' => $active,
                ]) ?>
            </div>
        </div>
        <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="column">
                <?= $this->render('_plus_menu') ?>
            </div>
            <div class="column">
                <div class="header-others-row row align-items-center justify-content-between">
                    <div class="header-others-column column">
                        <div class="dropdown dropdown_products">
                            <button class="button-clr dropdown-btn" type="button" id="dropdownProducts" data-toggle="modal" data-target="#products-list-modal">
                                <?= $this->render('svg/tiles') ?>
                            </button>
                        </div>
                    </div>
                    <div class="header-others-column column">
                        <div class="dropdown">
                            <button class="button-clr dropdown-btn" type="button" id="dropdownNotif" data-toggle="modal" data-target="#notification-list-modal">
                                <?= $this->render('svg/bell') ?>
                                <span class="dropdown-btn-indicator indicator <?= ($notifications ? 'active' : '') ?>">&nbsp;</span>
                            </button>
                        </div>
                    </div>
                    <div class="header-others-column column">
                        <div class="dropdown">
                            <button class="button-clr dropdown-btn" type="button" id="dropdownUser" data-toggle="modal" data-target="#user-menu-modal">
                                <img class="dropdown-btn-img" src="/images/user-img-gray.png" alt="">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
</nav>

<?php Modal::begin([
    'id' => 'products-list-modal',
    'title' => 'Доступные продукты',
    'size' => 'modal-dialog-scrollable',
    'options' => [
        'class' => 'fade nav-menu-modal',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="d-flex justify-content-around flex-wrap">
        <div style="max-width: 140px;" class="<?= ($currentModule == ServiceModule::STANDART) ? 'active' : '' ?>">
            <a class="products-list-link" href="<?= Url::to(['/documents/invoice/index', 'type' => 2]) ?>">
            <span class="notif-marker notif-marker_red">
                <?= $this->render('svg/invoicing') ?>
            </span>
                <span class="products-link-txt">Выставление счетов</span>
            </a>
        </div>
        <div style="max-width: 140px;" class="<?= ($currentModule == ServiceModule::ANALYTICS) ? 'active' : '' ?>">
            <a class="products-list-link" href="<?= Url::to(['/reports/options']) ?>">
            <span class="notif-marker notif-marker_red">
                <?= $this->render('svg/analytics') ?>
            </span>
                <span class="products-link-txt">ФинДиректор</span>
            </a>
        </div>
        <?php if ($canBuhIP) : ?>
            <div style="max-width: 140px;" class="<?= ($currentModule == ServiceModule::IP_USN_6) ? 'active' : '' ?>">
                <a class="products-list-link" href="<?= Url::to(['/tax/robot/index']) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'mix']) ?>
                </span>
                    <span class="products-link-txt">
                    Налоги и Декларация ИП
                </span>
                </a>
            </div>
        <?php endif ?>
        <div style="max-width: 140px;" class="<?= ($currentModule == ServiceModule::B2B_PAYMENT) ? 'active' : '' ?>">
            <a class="products-list-link" href="<?= Url::to(['/b2b/module']) ?>">
            <span class="notif-marker notif-marker_red">
                <?= $this->render('//svg-sprite', ['ico' => 'bank']) ?>
            </span>
                <span class="products-link-txt">Бизнес платежи</span>
            </a>
        </div>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'notification-list-modal',
    'title' => 'Уведомления',
    'size' => 'modal-dialog-scrollable',
    'options' => [
        'class' => 'fade nav-menu-modal',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <ul class="notification-list list-clr">
        <?php foreach ($notifications as $notification) : ?>
            <li>
                <a class="notification-link" href="<?= $notificationsUrl ?>">
                    <span class="notif-marker">
                        <?= Icon::get('attention') ?>
                    </span>
                    <span class="notif-details">
                        <span class="notification-message" title="<?= $notification->title; ?>">
                            <?= $notification->title; ?>
                        </span>
                        <br>
                        <span class="notification-time">
                            <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                'format' => 'd F',
                                'monthInflected' => true,
                            ]); ?>
                        </span>
                    </span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <a class="link link_bold" href="<?= $notificationsUrl ?>">
        Все уведмоления
    </a>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'user-menu-modal',
    'title' => $user ? $user->shortFio : null,
    'size' => 'modal-dialog-scrollable',
    'options' => [
        'class' => 'fade nav-menu-modal',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <?php if ($user) : ?>
        <ul class="dropdown-list list-clr" style="font-size: 1.6rem;">
            <li>
                <a class="dropdown-list-link link" href="mailto:<?= $user->email ?>">
                    <?= $user->email ?>
                </a>
            </li>
            <?php foreach ($userItems as $item) : ?>
                <li>
                    <?= Html::a($item['label'], $item['url'], [
                        'class' => 'dropdown-list-link',
                    ]) ?>
                </li>
            <?php endforeach ?>
            <li>
                <?= Html::a('Выход', '#logout-confitm-modal', [
                    'class' => 'dropdown-list-link',
                    'data-toggle' => 'modal',
                ]) ?>
            </li>
        </ul>
    <?php endif ?>
<?php Modal::end(); ?>

<?= ConfirmModalWidget::widget([
    'toggleButton' => false,
    'options' => [
        'id' => 'logout-confitm-modal',
    ],
    'confirmUrl' => Url::to(['/site/logout']),
    'message' => 'Вы уверены, что хотите выйти?',
]) ?>

<?php if (ArrayHelper::getValue(Yii::$app->user->identity, 'company.isFreeTariff')) : ?>
    <?php Modal::begin([
        'id' => 'freeTariffNotify',
        'title' => Yii::$app->user->identity->company->freeTariffNotified ?
                    'Текущая подписка - БЕСПЛАТНО' :
                    'Ваш аккаунт переведен на тариф БЕСПЛАТНО',
    ]) ?>

        <div style="font-weight: 400; font-size: 16px; color: #333;">
            <p>
                Ограничения на тарифе БЕСПЛАТНО:
            </p>
            <ul>
                <li>5 счетов в месяц</li>
                <li>Только 1 организация</li>
                <li>Не больше 3-х сотрудников</li>
                <li>Место на диске - 1 ГБ</li>
            </ul>
            <p>
                Что бы работать без ограничений нужно <?= Html::a('перейти на платный тариф', ['/subscribe']) ?>.
            </p>
        </div>

    <?php Modal::end() ?>

    <?php $this->registerJs('
        $(document).on("click", ".action-is-limited", function(e) {
            e.preventDefault();
            $("#freeTariffNotify").modal("show");
        });
    ') ?>
<?php endif ?>
