<?php

use backend\models\Bank;
use common\components\ImageHelper;
use common\models\Chat;
use common\models\company\CheckingAccountant;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\document\ScanDocument;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\product\Product;
use common\models\service;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\rbac\permissions;
use frontend\rbac\permissions\Cash;
use php_rutils\RUtils;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

if (!Yii::$app->user->isGuest) :
    $linkText = '';
    /* @var $user Employee */
    $user = Yii::$app->user->identity;
    $company = $user->company;
    $canCreateCash = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
    $canCreateProduct = Yii::$app->user->can(\frontend\rbac\permissions\Product::CREATE);
    $actualSubscribes = $company ? SubscribeHelper::getPayedSubscriptions($company->id) : [];
    $expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
    $expireDays = SubscribeHelper::getExpireLeftDays($expireDate);
    $isTrial = (count($actualSubscribes) === 1 && $actualSubscribes[0]->tariff_id == SubscribeTariff::TARIFF_TRIAL);
    $canViewAutoAct = $canViewAutoInvoice = Yii::$app->user->can(permissions\document\Document::INDEX, [
        'ioType' => Yii::$app->request->get('type', null),
    ]);
    $canViewPaymentReminder = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_MANAGER, EmployeeRole::ROLE_SUPERVISOR, EmployeeRole::ROLE_SUPERVISOR_VIEWER,]);
    $canViewSaleIncrease = Yii::$app->user->can(permissions\Contractor::INDEX, [
        'type' => Contractor::TYPE_CUSTOMER,
    ]);
    $canActivate = $canViewAutoAct || $canViewAutoInvoice || $canViewPaymentReminder || $canViewSaleIncrease;
    $canCreateDocument = Yii::$app->user->can(permissions\document\Document::CREATE);

    echo Html::hiddenInput(null, Yii::$app->user->identity->id, [
        'class' => 'identity_employee_id',
    ]);
    echo Html::hiddenInput(null, YII_ENV_DEV ? 8085 : 8086, [
        'class' => 'chat-port',
    ]);

    $banking = [
        'logo' => '',
    ];

    $bankItems = [];
    /* @var $bankArray CheckingAccountant[] */
    $bankArray = $company->getCheckingAccountants()
        ->groupBy('rs')
        ->indexBy('rs')
        ->orderBy('type')
        ->all();
    if ($bankArray && count($bankArray) == 1) {
        foreach ($bankArray as $account) {
            if (($bankingClass = Banking::classByBik($account->bik)) !== null) {
                if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
                    $banking['logo'] = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29], [
                        'class' => 'ml-1',
                    ]);
                    break;
                }
            }
        }
    }
?>

<div class="dropdown dropdown_plus">
    <button class="button-clr dropdown-btn" role="button" data-toggle="modal" data-target="#plus-menu-modal">
        <?= Icon::get('increase-icon', ['class' => 'header-plus-icon']) ?>
    </button>
</div>

<?php Modal::begin([
    'id' => 'plus-menu-modal',
    'size' => 'modal-dialog-scrollable',
    'options' => [
        'class' => 'fade nav-menu-modal',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div style="font-size: 1.4rem;">
        <ul class="header-plus-list columns-2 list-clr">
            <li class="title">Добавить</li>
            <li class="title"></li>
            <li>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                    'ioType' => Documents::IO_TYPE_OUT,
                ])
                ): ?>
                    <?= Html::a('Счет покупателю', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]), [
                        'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT) ?
                            'btn-p' :
                            'btn-p action-is-limited',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_CUSTOMER])): ?>
                    <?= Html::a('Покупателя', Url::to(['/contractor/create', 'type' => Contractor::TYPE_CUSTOMER]), [
                        'class' => 'btn-p',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                    'ioType' => Documents::IO_TYPE_IN,
                ])
                ): ?>
                    <?= Html::a('Счет от поставщика', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_IN]), [
                        'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_IN) ?
                            'btn-p' :
                            'btn-p action-is-limited',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_SELLER])): ?>
                    <?= Html::a('Поставщика', Url::to(['/contractor/create', 'type' => Contractor::TYPE_SELLER]), [
                        'class' => 'btn-p',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if ($canCreateProduct): ?>
                    <?= Html::a('Товар', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_GOODS]), [
                        'class' => 'btn-p',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Employee::CREATE)): ?>
                    <?= Html::a('Сотрудника', Url::to(['/employee/create']), [
                        'class' => 'btn-p',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if ($canCreateProduct): ?>
                    <?= Html::a('Услугу', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]), [
                        'class' => 'btn-p',
                    ]); ?>
                <?php endif; ?>
            </li>
            <li>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE)): ?>
                    <?= Html::a('Договор',
                        Url::to(['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT, 'modal' => 1]
                        ), [
                            'class' => 'btn-p',
                            'data-url' => '/documents/agreement/index?modal=1'
                        ]); ?>
                <?php endif; ?>
            </li>
        </ul>
        <ul class="header-plus-list list-clr">
            <?php if ($canCreateCash || $canCreateProduct) : ?>
                <li class="title">Загрузить</li>
                <?php if ($canCreateCash): ?>
                    <li class="btn-line">
                        <?= Html::a('Выписку из банка', [
                            '/cash/banking/default/index',
                            'p' => Banking::routeEncode(['/cash/bank/index']),
                        ], [
                            'class' => 'btn-p',
                        ]); ?>
                        <?= $banking['logo'] ?>
                    </li>
                <?php endif; ?>
                <?php if ($canCreateProduct) : ?>
                    <li class="btn-line">
                        <?= Html::a('Товары из таблицы Excel', Url::to(['/product/index', 'productionType' => 1, 'modal' => true]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                    <li class="btn-line">
                        <?= Html::a('Услуги из таблицы Excel', Url::to(['/product/index', 'productionType' => 0, 'modal' => true]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                <?php endif ?>
            <?php endif ?>
        </ul>
        <ul class="header-plus-list list-clr">
            <?php if ($canActivate): ?>
                <li class="title">Включить</li>
                <?php if ($canViewAutoInvoice): ?>
                    <li class="btn-line">
                        <?= Html::a('Рассылку АвтоСчетов', Url::to(['/documents/invoice/index-auto',]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                <?php endif; ?>
                <?php if ($canViewAutoAct): ?>
                    <li class="btn-line">
                        <?= Html::a('Выставление АвтоАктов', Url::to(['/documents/act/index', 'type' => Documents::IO_TYPE_OUT, 'autoActModal' => true]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                <?php endif; ?>
                <?php if ($canViewPaymentReminder && false): ?>
                    <li class="btn-line">
                        <?= Html::a('Рассылку писем должникам', Url::to(['/payment-reminder/index',]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                <?php endif; ?>
                <?php /* if ($canViewSaleIncrease): ?>
                    <li class="btn-line">
                        <?= Html::a('Увеличение онлайн продаж', Url::to(['/contractor/sale-increase',]), [
                            'class' => 'btn-p',
                        ]); ?>
                    </li>
                <?php endif; */ ?>
            <?php endif; ?>
        </ul>
    </div>
<?php Modal::end(); ?>

<?php endif; ?>