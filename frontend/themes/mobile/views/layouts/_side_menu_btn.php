<div class="sidebar-menu-head">
    <button id="item<?=$id?>" class="sidebar-menu-btn button-clr collapsed" type="button"
        data-toggle="collapse" data-target="#item<?=$id?>Content"
        aria-expanded="false" aria-controls="item<?=$id?>Content">
        <svg class="sidebar-menu-icon svg-icon" <?= (!empty($style)) ? 'style="'.$style.'"' : '' ?>>
            <use xlink:href="/images/svg-sprite/svgSprite.svg#<?=$i?>"></use>
        </svg><span class="sidebar-menu-btn-txt">{label}</span>
        <svg class="sidebar-menu-angle svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#arrow"></use>
        </svg>
    </button>
</div>