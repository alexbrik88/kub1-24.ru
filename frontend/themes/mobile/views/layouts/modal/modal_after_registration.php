<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.03.2018
 * Time: 5:49
 */

use common\widgets\Modal;
use common\components\ImageHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;
use yii\widgets\MaskedInput;
use common\models\Company;
use common\models\Contractor;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

Modal::begin([
    'header' => '
        <h1 style="text-align: center;margin: 0;text-transform: uppercase;">ДОБРО ПОЖАЛОВАТЬ!</h1>
        <h4 style="text-align: center;margin: 0;padding-top: 10px;">
            Автоматизировать ваш бизнес <br>
            с помощью программы КУБ очень просто
        </h4>
        ',
    'footer' => $this->render('_partial_footer', [
        'type' => Company::AFTER_REGISTRATION_MAIN_MODAL,
    ]),
    'id' => 'modal-kub-do-it',
]); ?>
<div class="form-body">
    <div class="row choose-text">
        <span style="font-size: 15px;">
            Выберите, с чего начать:
        </span>
    </div>
    <div class="col-md-12 steps">
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4 next-modal expose-invoice"
             data-url="<?= Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]); ?>"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/invoice.png', [160, 160]); ?>)">
            <div class="name">
                Выставить счет
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="2"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/business_analyse.png', [160, 160]); ?>)">
            <div class="name">
                Подготовить<br> акт, накладную,<br> счет-фактуру
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="3"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/expose_other_documents.png', [160, 160]); ?>)">
            <div class="name">
                Анализ бизнеса
            </div>
        </div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4 next-modal" data-type="4"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/employee_plan.png', [160, 160]); ?>)">
            <div class="name">
                Контроль работы сотрудников
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="5"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/stock_control.png', [160, 160]); ?>)">
            <div class="name">
                Учет товаров
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="6"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/bill.png', [160, 160]); ?>)">
            <div class="name">
                Повышение продаж
            </div>
        </div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
    </div>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h2 class="header-name" style="text-transform: uppercase;"></h2>',
    'footer' => $this->render('_partial_footer', [
        'type' => Company::AFTER_REGISTRATION_MAIN_MODAL,
    ]),
    'id' => 'modal-loader-items'
]);
?>
<div class="col-xs-12" style="padding: 0" id="modal-loader">
</div>
<style>
    #modal-loader-items .modal-body {
        padding: 0;
    }
</style>
<?php Modal::end(); ?>
