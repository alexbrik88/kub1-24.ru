<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.04.2018
 * Time: 17:01
 */

use yii\widgets\MaskedInput;
use yii\helpers\Html;

/* @var $type integer */
?>
<h3>Нужна помощь?</h3>
<div class="send-phone-block text-center">
    <?= Html::textInput('question', null, [
        'class' => 'form-control',
        'placeholder' => 'Напишите свой вопрос',
        'style' => 'width: 70%;margin: 0 auto;margin-bottom: 10px;',
    ]); ?>
    <?= MaskedInput::widget([
        'name' => 'phone',
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+7(___) ___-__-__',
            'style' => 'width: 23%;display: inline;',
        ],
    ]); ?>
    <?= Html::a('Отправить', null, [
        'class' => 'btn white send-phone',
        'data-url' => '/site/phone-feedback',
        'data-type' => $type,
        'style' => 'margin-bottom: 3px;margin-left: 20px;width: 23%;',
    ]); ?>
</div>
