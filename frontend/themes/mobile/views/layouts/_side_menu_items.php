<div id="item<?=$id?>Content" class="submenu-items collapse" aria-labelledby="item<?=$id?>" data-parent="#main-menu-items">
    <div class="sidebar-menu-body">
        <ul class="sidebar-menu-list list-clr">
            {items}
        </ul>
    </div>
</div>