<?php

use frontend\themes\mobile\assets\MobileAsset;

/* @var $this \yii\web\View */
/* @var $content string */

MobileAsset::register($this);
?>
<?php $this->beginPage() ?>

<?php $this->beginBody() ?>

<?php echo $content ?>

<?php $this->endBody(); ?>

<?php $this->endPage(); ?>
