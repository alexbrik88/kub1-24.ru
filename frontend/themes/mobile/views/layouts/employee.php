<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>
<?php /* todo /salary/index
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
   <?= Nav::widget([
        'id' => 'debt-report-menu',
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            'style' => 'margin-bottom: 20px;'
        ],
        'items' => [
            [
                'label' => 'Сотрудники',
                'url' => ['/employee/index'],
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . (Yii::$app->controller->id == 'employee' ? ' active' : '')
                ]
            ],
            [
                'label' => 'Расчет ЗП',
                'url' => ['/salary/index'],
                'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . (Yii::$app->controller->id == 'salary' ? ' active' : '')
                ]
            ],
        ],
    ]);
    ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
*/ ?>

<?= $content; ?>

<?php $this->endContent(); ?>
