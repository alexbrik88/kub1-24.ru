<?php

use yii\bootstrap4\Modal;
use common\models\paymentReminder\PaymentReminderMessage;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use dosamigos\datetimepicker\DateTimePicker;
use common\components\date\DateHelper;

/* @var $model PaymentReminderMessage
 * @var $isFreeTariff boolean
 */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
Modal::begin([
    'id' => 'modal-update-payment-reminder-' . $model->id,
]);
$form = ActiveForm::begin([
    'action' => Url::to(['update', 'id' => $model->id]),
    'options' => [
        'id' => 'form-payment-reminder-message-' . $model->id,
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'checkOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => '',
            ],
        ],
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized',],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<h4 class="modal-title"><?= 'Настройка шаблона письма №' . $model->number ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="form-body">
    <span class="text-center col-md-12 warning-text hidden">Текст и параметры шаблона измените под ваши требования</span>
    <?= Html::activeHiddenInput($model, 'activate'); ?>
    <?= Html::activeHiddenInput($model, 'status'); ?>
    <?= Html::activeHiddenInput($model, 'number'); ?>
    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="label">
                Условие отправки:
            </label>
            <div class="">
                <?= $model->event_header; ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [6, 7, 8, 9])): ?>
        <div class="form-group required">
            <label class="label" for="checkingaccountant-rs">
                Условие отправки:
            </label>
            <div class="">
                Сумма просроченного долга больше суммы: <?= Html::activeTextInput($model, 'debt_sum', [
                    'class' => 'form-control',
                    'style' => 'width: 20%;display: inline;',
                ]); ?>
                <span id="tooltip_payment-reminder-debt-sum-<?= $model->id; ?>"
                      class="tooltip3 ico-question valign-middle"
                      data-tooltip-content="#tooltip_debt-sum-reminder-<?= $model->id; ?>"
                      style="margin-left: 10px;cursor: pointer;"></span>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Условие отправки:
            </label>
            <div class="">
                Счет оплачен до истечения срока оплаты.
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number == 1): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "За {input}  дней до окончания срока отсрочки платежа по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input}  день просрочки оплаты по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif ((int)$model->number == 6): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                В день превышения суммы всей просроченной задолженности над указанной выше суммой.
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [7, 8, 9])): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>

            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input}  день превышения суммы всей просроченной задолженности над указанной выше суммой. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>

            <div class="">
                В день изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'message_template_subject', $config)->textInput([
        'maxlength' => true,
    ])->label('Тема письма:'); ?>

    <div class="form-group required">
        <label class="label">
            Текст письма:
        </label>

        <div class="">
            <div class="message-template-body_editable_block" contenteditable="true"
                 style="border: solid 2px #e2e5eb;width: 100%;padding: 6px 12px;border-radius: 4px;">
                <?= $model->message_template_body; ?>
            </div>
            <?= Html::activeHiddenInput($model, 'message_template_body'); ?>
        </div>
    </div>

    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 6, 7, 8, 10])): ?>
        <div class="form-group required">
            <label class="label">
                Кому:
            </label>
            <div class="">
                <?= $form->field($model, 'send_for_chief', [
                    'labelOptions' => ['class' => 'label mr-4'],
                    'options' => [
                        'style' => 'width: 30%;display: inline-block;',
                    ],
                ])->checkbox([], true)->label('Руководителю'); ?>

                <?= $form->field($model, 'send_for_contact', [
                    'options' => [
                        'class' => '',
                        'style' => 'display: inline-block;',
                    ],
                ])->checkbox([], true)->label('Контактному лицу'); ?>

                <?= $form->field($model, 'send_for_all', [
                    'options' => [
                        'class' => '',
                    ],
                ])->checkbox([], true)->label('Всем, кто есть в карточке Покупателя'); ?>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Кому:
            </label>

            <div class="">
                <?= Html::activeTextInput($model, 'for_email_list[]', [
                    'class' => 'form-control',
                    'style' => 'margin-bottom: 15px;',
                    'value' => isset($model->for_email_list[0]) ? $model->for_email_list[0] : null,
                ]); ?>
                <?= Html::activeTextInput($model, 'for_email_list[]', [
                    'class' => 'form-control',
                    'style' => 'margin-bottom: 15px;',
                    'value' => isset($model->for_email_list[1]) ? $model->for_email_list[1] : null,
                ]); ?>
                <?= Html::activeTextInput($model, 'for_email_list[]', [
                    'class' => 'form-control',
                    'style' => 'display: inline;width: 88%;',
                    'value' => isset($model->for_email_list[2]) ? $model->for_email_list[2] : null,
                ]); ?>
                <span id="tooltip_payment-reminder-send-to-<?= $model->id; ?>"
                      class="tooltip3 ico-question valign-middle"
                      data-tooltip-content="#tooltip_send-to-reminder-<?= $model->id; ?>"
                      style="cursor: pointer;"></span>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number == 10): ?>
        <div class="form-group required">
            <label class="label">
                Не отправлять письмо:
            </label>

            <div class="">
                <?= Html::activeCheckbox($model, 'not_send_where_order_payment', [
                    'label' => 'При оплате по кассе',
                    'labelOptions' => [
                        'style' => 'padding-top: 7px;',
                    ],
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_emoney_payment', [
                    'label' => 'При оплате по e-money',
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_partial_payment', [
                    'label' => 'При частичной оплате счета',
                ]); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number !== 10): ?>
        <?php $hours = $model->time ? DateHelper::format($model->time, 'G', 'H:i:s') : null;
        $minutes = $model->time ? DateHelper::format($model->time, 'i', 'H:i:s') : null;
        $model->time = $model->time ? DateHelper::format($model->time, DateHelper::FORMAT_USER_DATE . ' H:i', 'H:i:s') : null; ?>
        <?= $form->field($model, 'time', array_merge($config, [
            'wrapperOptions' => [
                'style' => 'width: 130px',
            ],
        ]))->widget(DateTimePicker::className(), [
            'language' => 'ru',
            'size' => 'ms',
            'template' => '{input}{button}',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'inline' => false,
            'options' => [
                'class' => 'paymentremindermessage-time',
                'id' => 'paymentremindermessage-time-' . $model->id,
                'style' => 'border-radius:4px!important',
                'data-hour' => $hours,
                'data-minute' => $minutes,
            ],
            'clientOptions' => [
                'startView' => 1,
                'minView' => 0,
                'maxView' => 1,
                'autoclose' => true,
                'minuteStep' => 30,
                'format' => 'hh:ii',
                'pickerPosition' => 'top-left',
                'useCurrent' => true,
            ],
        ])->label('Время:'); ?>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Время отправки:
            </label>
            <div class="">
                Через 5 минут после изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>
    <div class="mt-3 d-flex justify-content-between">
        <?php if (!$isFreeTariff || $model->number == 1): ?>
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        <?php else: ?>
            <?= Html::button('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr no-rules-update',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#activate-modal-no-rules-' . $model->id,
                ],
            ]); ?>
            <?= $modalNoRules = $this->render('no_rules_modal', [
                'model' => $model,
            ]); ?>
        <?php endif; ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent back',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>
<?php Modal::end(); ?>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_send-to-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Поставьте e-mail нашего юриста: urist@kub-24.ru <br>
        При получении письма он свяжется с вами, что бы подготовить претензию. <br>
        Стоимость подготовки претензии 3000 рублей
    </span>
    <span id="tooltip_debt-sum-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Укажите сумму, если сумма всей просроченной <br>
        задолженности ее превысит, то отправится <br>
        письмо
    </span>
</div>
<?php $this->registerJs('
    $(".no-rules-update").click(function () {
        $(this).closest(".modal").modal("hide");
    });
'); ?>

