<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.05.2018
 * Time: 17:04
 */

use common\components\helpers\Html;
use frontend\components\Icon;
use yii\helpers\Url;
use common\models\employee\Employee;
use frontend\models\PaymentReminderMessageSearch;
use yii\data\ActiveDataProvider;
use common\components\grid\GridView;
use common\models\paymentReminder\PaymentReminderMessage;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\grid\ActionColumn;
use common\components\grid\DropDownDataColumn;
use frontend\models\PaymentReminderMessageContractorSearch;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use frontend\widgets\TableConfigWidget;
use common\components\debts\DebtsHelper;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\components\FilterHelper;
use frontend\models\Documents;
use yii\widgets\Pjax;
use common\models\employee\EmployeeRole;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\widgets\ActiveForm;
use common\components\date\DateHelper;

/* @var \yii\web\View $this
 * @var $user Employee
 * @var $paymentReminderMessageSearchModel PaymentReminderMessageSearch
 * @var $paymentReminderMessageDataProvider ActiveDataProvider
 * @var $paymentReminderMessageContractorSearchModel PaymentReminderMessageContractorSearch
 * @var $paymentReminderMessageContractorDataProvider ActiveDataProvider
 * @var $paymentReminderMessage PaymentReminderMessage
 */

$this->title = 'Шаблоны писем должникам';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-message-body'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-contractor-count',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-contractor-count'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
$isFreeTariff = $user->company->isFreeTariff;
$hasActiveTemplate = PaymentReminderMessage::find()
    ->andWhere(['company_id' => $user->company_id])
    ->andWhere(['status' => PaymentReminderMessage::STATUS_ACTIVE])
    ->exists();
?>
    <div class="wrap">
        <h3 class="page-title mb-3"><?= $this->title; ?></h3>
        <div class="payment-reminder-index">
            <div class="row">
                <div class="page-description col-7">
                    <span style="display: block;margin-bottom: 15px;">
                        Надоело звонить клиентам и напоминать о долгах?
                        <br>
                        Поручите дебиторку КУБу. В случае не оплаты счета вовремя, КУБ запустит автоматическую серию писем:
                        от вежливых напоминаний об оплате до “последнего китайского предупреждения”.
                    </span>
                    <div>
                        Для этого вам нужно:
                        <ul style="list-style-type: decimal;padding-left: 13px;">
                            <li>
                                Включить нужные письма и исправить в них текст темы и текст письма на нужный вам.
                            </li>
                            <li>
                                Выбрать конкретных покупателей или сразу всех покупателей, по которым запустить «Работу
                                с
                                должниками».
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="page-banner col-5">
                    <div class="text">
                        <b>Внимание:</b>
                        <?php if ($isFreeTariff): ?>
                            на БЕСПЛАТНОМ тарифе вы можете включить только шаблон письма №1.
                            Что бы использовать все шаблоны писем автоматической рассылки,
                            <?= Html::a('оплатите сервис', Url::to(['/subscribe'])); ?>.
                        <?php else: ?>
                            Если у вас закончится платный тариф, то все письма отключаться, кроме письма №1.
                            Что бы работа с должниками не прекращалась,
                            <?= Html::a('оплатите сервис на несколько месяцев', Url::to(['/subscribe'])); ?>.
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap">
        <h4>Настройка шаблонов писем</h4>
        <?= GridView::widget([
            'id' => 'payment-reminder-message-grid',
            'dataProvider' => $paymentReminderMessageDataProvider,
            'filterModel' => $paymentReminderMessageSearchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => "{items}\n{pager}",
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
            ],
            'columns' => [
                [
                    'attribute' => 'number',
                    'label' => '№',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) use ($user) {
                        if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                            return Html::a($model->number, null, [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                'data-pjax' => 0,
                                'data' => [
                                    'toggle' => 'modal',
                                    'target' => '#modal-update-payment-reminder-' . $model->id,
                                    'pjax' => 0,
                                ],
                            ]);
                        }

                        return $model->number;
                    }
                ],
                [
                    'attribute' => 'category_id',
                    'label' => 'Категория',
                    'headerOptions' => [
                        'width' => '7%',
                        'class' => 'dropdown-filter category-header',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'filter' => $paymentReminderMessageSearchModel->getCategoryFilter(),
                    'value' => function (PaymentReminderMessage $model) {
                        return $model->category->name;
                    },
                    's2width' => '200px'
                ],
                [
                    'attribute' => 'event_header',
                    'label' => 'Событие',
                    'headerOptions' => [
                        'width' => '27%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) {
                        return '<b>' . $model->event_header . '</b><br>' . $model->event_body;
                    }
                ],
                [
                    'attribute' => 'days',
                    'label' => '"X" дней',
                    'headerOptions' => [
                        'width' => '7%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) {
                        return $model->days ? $model->days : '-';
                    }
                ],
                [
                    'attribute' => 'message_template_subject',
                    'label' => 'Тема шаблона письма',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) {
                        return $model->message_template_subject;
                    }
                ],
                [
                    'attribute' => 'message_template_body',
                    'label' => 'Текст шаблона письма',
                    'headerOptions' => [
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) {
                        return '<span class="tooltip2" data-tooltip-content="#template_body-' . $model->id . '">' .
                            mb_substr($model->message_template_body, 0, 35) .
                            '...</span>' .
                            '<div class="tooltip-template hidden">
                                    <span class="tooltip-payment-reminder-body text-left" id="template_body-' . $model->id . '" style="display: inline-block;">' .
                            $model->message_template_body .
                            '</span>
                                </div>';
                    }
                ],
                [
                    'attribute' => 'status',
                    'label' => 'Статус',
                    'headerOptions' => [
                        'width' => '19%',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                        $modalNoRules = $this->render('no_rules_modal', [
                            'model' => $model,
                        ]);
                        $canChange = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,]);

                        return '<span class="payment-reminder-switch">' .
                            Html::a('Вкл.', "javascript:;", [
                                'class' => 'button-clr button-regular activate-message-template ' . ($model->status ? 'active button-hover-transparent' : 'button-regular_red'),
                                'style' => 'width: 50%;',
                                'data' => $canChange ? [
                                    'toggle' => 'modal',
                                    'target' => ($isFreeTariff && $model->number != 1) ? '#activate-modal-no-rules-' . $model->id :
                                        ('#modal-update-payment-reminder-' . $model->id),
                                ] : [],
                            ]) .
                            Html::a('Откл.', $canChange ? Url::to(['change-status', 'id' => $model->id]) : null, [
                                'class' => 'button-clr button-regular ' . (!$model->status ? 'active button-hover-transparent' : 'button-regular_red'),
                                'style' => 'width: 50%;',
                                'data' => $canChange ? [
                                    'method' => 'post',
                                    'params' => [
                                        'status' => PaymentReminderMessage::STATUS_INACTIVE,
                                        '_csrf' => Yii::$app->request->csrfToken,
                                    ],
                                ] : [],
                            ]) . '</span>' . $modalNoRules;
                    }
                ],
                [
                    'label' => 'Кол-во покупателей',
                    'headerOptions' => [
                        'class' => 'tooltip2-contractor-count',
                        'data-tooltip-content' => '#template_contractor_count',
                        'width' => '10%',
                        'style' => 'cursor: pointer;',
                    ],
                    'contentOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'raw',
                    'value' => function (PaymentReminderMessage $model) {
                        return $model->getContractorCount();
                    }
                ],
                [
                    'headerOptions' => [
                        'width' => '90px',
                    ],
                    'class' => ActionColumn::className(),
                    'template' => '{edit}',
                    'buttons' => [
                        'edit' => function ($url, PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                            if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                                $updateModal = $this->render('update_modal', [
                                    'model' => $model,
                                    'isFreeTariff' => $isFreeTariff,
                                ]);

                                return Html::a(Icon::get('pencil'), null, [
                                        'class' => 'update-payment-reminder-message link',
                                        'title' => Yii::t('yii', 'Редактировать'),
                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                        'data-pjax' => 0,
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#modal-update-payment-reminder-' . $model->id,
                                            'pjax' => 0,
                                        ],
                                    ]) . $updateModal;
                            }

                            return null;
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>

    <div class="wrap">
        <h4>Настройка автоматической рассылки писем</h4>
        <div class="row mb-3">
            <div class="col-4">
                <div class="mb-2"><?= TableConfigWidget::widget([
                        'items' => [
                            [
                                'attribute' => 'payment_reminder_message_contractor_invoice_sum',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_1',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_2',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_3',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_4',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_5',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_6',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_7',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_8',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_9',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_10',
                            ],
                        ],
                    ]); ?></div>
            </div>
            <div class="col-8">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['update-message-contractor', '#' => 'settings']),
                    'options' => [
                        'class' => 'form-horizontal form-payment-reminder-message-contractor',
                        'id' => 'form-payment-reminder-message-contractor',
                    ],
                ]); ?>
                <div class="actions float-l pensil-box text-right">
                    <?= Html::a('<i class="icon-pencil" style="font-size: inherit;"></i> Редактировать', "javascript:;", [
                        'class' => 'update-payment-reminder-message-contractor button-regular button-width button-regular_red button-clr' . ($hasActiveTemplate ? ' tooltip3' : null),
                        'data-tooltip-content' => $hasActiveTemplate ? null : '#tooltip-active-template',
                    ]); ?>
                    <?= Html::a('<span class="ico-Save-smart-pls" style="font-size: inherit;"></span> Сохранить', "javascript:;", [
                        'class' => 'save-payment-reminder-message-contractor button-regular button-width button-regular_red button-clr hidden',
                    ]); ?>
                    <?= Html::a('<i class="ico-Cancel-smart-pls" style="font-size: inherit;"></i> Отменить', "javascript:;", [
                        'class' => 'undo-payment-reminder-message-contractor hidden button-clr button-regular button-width button-hover-transparent',
                    ]); ?>
                </div>
                <div class="tooltip-template hidden">
                    <span class="text-left" id="tooltip-active-template" style="display: inline-block;">
                        Что бы назначить шаблон письма для оправки покупателю,<br>
                        сначала включите его.
                    </span>
                </div>
                <?php foreach ($paymentReminderMessageContractorDataProvider->query->each() as $messageContractor): ?>
                    <?php foreach (range(1, 10) as $key): ?>
                        <?php if (!(!$messageContractor->getMessageTemplateStatus($key) ||
                            $user->employee_role_id == EmployeeRole::ROLE_SUPERVISOR_VIEWER)): ?>
                            <?= Html::activeTextInput($messageContractor, "message_{$key}", [
                                'class' => "paymentremindermessagecontractor-message message_{$key}-contractor",
                                'name' => "PaymentReminderMessageContractor[message_{$key}][" . $messageContractor->contractor_id . ']',
                                'style' => 'display: none;'
                            ]); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php ActiveForm::end(); ?>
        </div>

        <div class="table-container" style="">
            <?php Pjax::begin([
                'id' => 'payment-reminder-message-contractor-pjax',
                'timeout' => 10000,
            ]); ?>
            <?= GridView::widget([
                'id' => 'payment-reminder-message-contractor-grid',
                'dataProvider' => $paymentReminderMessageContractorDataProvider,
                'filterModel' => $paymentReminderMessageContractorSearchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'columns' => [
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor',
                        'label' => 'Покупатель',
                        'headerOptions' => [
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => FilterHelper::getContractorList(Documents::IO_TYPE_OUT, PaymentReminderMessageContractor::tableName(), true, false, false),
                        'value' => function (PaymentReminderMessageContractor $model) {
                            return $model->contractor->getTitle(true);
                        }
                    ],
                    [
                        'attribute' => 'invoiceSum',
                        'label' => 'Задолженность',
                        'headerOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_invoice_sum' .
                                ($user->config->payment_reminder_message_contractor_invoice_sum ? null : ' hidden'),
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting col_payment_reminder_message_contractor_invoice_sum' .
                                ($user->config->payment_reminder_message_contractor_invoice_sum ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) {
                            return DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, false, true, $model->contractor_id);
                        }
                    ],
                    [
                        'attribute' => 'message_1',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_1), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №1',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_1) ||
                                    $user->employee_role_id == EmployeeRole::ROLE_SUPERVISOR_VIEWER),
                                'type' => 'message_1',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_1' .
                                ($user->config->payment_reminder_message_contractor_message_1 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-1',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_1' .
                                ($user->config->payment_reminder_message_contractor_message_1 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_1, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_1', [
                                    'class' => 'paymentremindermessagecontractor-message message_1-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_1][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_1,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_1) ||
                                            $user->employee_role_id == EmployeeRole::ROLE_SUPERVISOR_VIEWER),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_1 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_2',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_2), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №2',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_2) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_2',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_2' .
                                ($user->config->payment_reminder_message_contractor_message_2 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-2',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_2' .
                                ($user->config->payment_reminder_message_contractor_message_2 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_2, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_2', [
                                    'class' => 'paymentremindermessagecontractor-message message_2-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_2][' . $model->contractor_id . ']',
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_2,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_2) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                    'disabled' => true,
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_2 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_3',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_3), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №3',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_3) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_3',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_3' .
                                ($user->config->payment_reminder_message_contractor_message_3 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-3',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_3' .
                                ($user->config->payment_reminder_message_contractor_message_3 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_3, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_3', [
                                    'class' => 'paymentremindermessagecontractor-message message_3-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_3][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_3,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_3) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_3 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_4',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_4), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №4',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_4) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_4',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_4' .
                                ($user->config->payment_reminder_message_contractor_message_4 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-4',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_4' .
                                ($user->config->payment_reminder_message_contractor_message_4 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_4, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_4', [
                                    'class' => 'paymentremindermessagecontractor-message message_4-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_4][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_4,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_4) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_4 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_5',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_5), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №5',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_5) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_5',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_5' .
                                ($user->config->payment_reminder_message_contractor_message_5 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-5',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_5' .
                                ($user->config->payment_reminder_message_contractor_message_5 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_5, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_5', [
                                    'class' => 'paymentremindermessagecontractor-message message_5-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_5][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_5,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_5) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_5 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_6',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_6), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №6',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_6) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_6',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_6' .
                                ($user->config->payment_reminder_message_contractor_message_6 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-6',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_6' .
                                ($user->config->payment_reminder_message_contractor_message_6 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_6, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_6', [
                                    'class' => 'paymentremindermessagecontractor-message message_6-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_6][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_6,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_6) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_6 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_7',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_7), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №7',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_7) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_7',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_7' .
                                ($user->config->payment_reminder_message_contractor_message_7 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-7',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_7' .
                                ($user->config->payment_reminder_message_contractor_message_7 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_7, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_7', [
                                    'class' => 'paymentremindermessagecontractor-message message_7-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_7][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_7,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_7) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_7 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_8',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_8), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №8',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_8) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_8',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_8' .
                                ($user->config->payment_reminder_message_contractor_message_8 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-8',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_8' .
                                ($user->config->payment_reminder_message_contractor_message_8 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_8, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_8', [
                                    'class' => 'paymentremindermessagecontractor-message message_8-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_8][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_8,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_8) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_8 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_9',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_9), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №9',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_9) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_9',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_9' .
                                ($user->config->payment_reminder_message_contractor_message_9 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-9',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_9' .
                                ($user->config->payment_reminder_message_contractor_message_9 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_9, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_9', [
                                    'class' => 'paymentremindermessagecontractor-message message_9-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_9][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_9,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_9) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_9 . '"
                                data-date="' . $lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                    [
                        'attribute' => 'message_10',
                        'header' => Html::checkbox(null, PaymentReminderMessageContractor::isAllContractorsChecked(PaymentReminderMessage::MESSAGE_10), [
                            'class' => 'paymentremindermessagecontractor-message all',
                            'label' => 'Письмо №10',
                            'labelOptions' => [
                                'style' => 'font-weight: 600;margin-right: 5px;',
                            ],
                            'data' => [
                                'can-update' => !(!PaymentReminderMessageContractor::getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_10) ||
                                    in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                'type' => 'message_10',
                            ],
                            'disabled' => true,
                        ]),
                        'headerOptions' => [
                            'class' => 'tooltip2 col_payment_reminder_message_contractor_message_10' .
                                ($user->config->payment_reminder_message_contractor_message_10 ? null : ' hidden'),
                            'data-tooltip-content' => '#template_body-header-10',
                            'width' => '5%',
                            'style' => 'cursor: pointer;',
                        ],
                        'contentOptions' => [
                            'class' => 'col_payment_reminder_message_contractor_message_10' .
                                ($user->config->payment_reminder_message_contractor_message_10 ? null : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (PaymentReminderMessageContractor $model) use ($user) {
                            $lastDate = DateHelper::format($model->last_message_date_10, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            return Html::activeCheckbox($model, 'message_10', [
                                    'class' => 'paymentremindermessagecontractor-message message_10-contractor',
                                    'label' => false,
                                    'name' => 'PaymentReminderMessageContractor[message_10][' . $model->contractor_id . ']',
                                    'disabled' => true,
                                    'data' => [
                                        'number' => PaymentReminderMessage::MESSAGE_10,
                                        'can-update' => !(!$model->getMessageTemplateStatus(PaymentReminderMessage::MESSAGE_10) ||
                                            in_array($user->employee_role_id, [EmployeeRole::ROLE_SUPERVISOR_VIEWER, EmployeeRole::ROLE_MANAGER])),
                                    ],
                                ]) .
                                '<span class="payment-reminder-tooltip"
                                data-contractor_email="' . $model->last_message_contractor_email_10 . '"
                                data-date="' .$lastDate . '">' . $lastDate . '</span>';
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>

        <div class="tooltip-template hidden">
            <?php foreach (PaymentReminderMessage::find()->andWhere(['company_id' => $user->company_id])->all() as $paymentReminderMessage): ?>
                <span class="tooltip-payment-reminder-body text-left"
                      id="template_body-header-<?= $paymentReminderMessage->number; ?>"
                      style="display: inline-block;">
                <?= $paymentReminderMessage->message_template_body; ?>
            </span>
            <?php endforeach; ?>
            <span class="tooltip-payment-reminder-contractor-count text-left" id="template_contractor_count"
                  style="display: inline-block;">
                Кол-во покупателей,<br>
                которым подключен<br>
                данный шаблон
            </span>
        </div>
    </div>
