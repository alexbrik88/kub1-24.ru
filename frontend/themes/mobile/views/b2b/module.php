<?php

use frontend\components\Icon;
use yii\helpers\Html;

$this->title = 'Список ссылок на Модуль В2В оплат';

$backTo = isset($backTo) ? $backTo : 'company';
?>

<div class="wrap p-4">
    <div class="row pb-2">
        <div class="column pl-3 mb-3">
            <div class="text_size_20">Список Ваших ссылок В2В эквайринга.</div>
            <a href="<?= Yii::$app->params['testOutInvoiceLink'] ?>" class="link" target="_blank">
                Пример, как это работает.
            </a>
        </div>
        <div class="column ml-auto mt-1 pr-1 mb-3">
            <?= Html::a(Icon::get('add-icon', ['class' => 'mr-1',]).' <span class="ml-2">Создать ссылку</span>', [
                '/out-invoice/create',
                'backTo' => $backTo,
                'type' => 'landing',
            ], [
                'class' => 'button-clr button-regular button-regular_red pl-3 pr-3'
            ]); ?>
        </div>
    </div>
    <?= $this->render('parts_module/links_table', [
        'searchModel' => $outInvoiceSearch,
        'dataProvider' => $outInvoiceProvider,
        'backTo' => 'B2B',
        'isPaid' => $isPaid
    ]); ?>
</div>
<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['services'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
            ]) ?>
        </div>
    </div>
</div>

<?= $this->render('_pay_modal', [
    'company' => $company
]); ?>

<?php if (!$isPaid && Yii::$app->request->get('modal')) {
    $this->registerJs("$('.b2b-pay-panel').modal('show');");
} ?>