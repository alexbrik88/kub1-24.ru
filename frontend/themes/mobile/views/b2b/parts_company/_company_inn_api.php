<?php

use common\models\company\CompanyType;

$house = \common\models\address\AddressHouseType::TYPE_HOUSE;
$flat = \common\models\address\AddressApartmentType::TYPE_APARTMENT;

$typeOOO = CompanyType::TYPE_OOO;
$typeZAO = CompanyType::TYPE_ZAO;
$typePAO = CompanyType::TYPE_PAO;
$typeOAO = CompanyType::TYPE_OAO;
$typeIp = CompanyType::TYPE_IP;

$this->registerJs(<<<JS

var companyType = {
    'ООО' : $typeOOO,
    'ЗАО' : $typeZAO,
    'ПАО' : $typePAO,
    'ОАО' : $typeOAO,
    'ИП': $typeIp
};
var body = $('.form-body');
$('#company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#company-inn').val(suggestion.data.inn);
            if (suggestion.data.name !== null) {
                $('#company-name_short').val(suggestion.data.name.short);
                $('#company-name_full').val(suggestion.data.name.full);
            }
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                var address = '';
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
                address += suggestion.data.address.value;
                $('#company-address_legal').val(address);
                $('#company-address_actual').val(address);
            } else {
                $('#company-address_legal').val('000000, ' + suggestion.data.address.value);
                $('#company-address_actual').val('000000, ' + suggestion.data.address.value);
            }

            if (suggestion.data.address.data !== null) {
                $('#company-oktmo').val(suggestion.data.address.data.oktmo);
            }

            $('#company-okpo').val(suggestion.data.okpo);
            $('#company-okved').val(suggestion.data.okved);

            if (companyType[suggestion.data.opf.short] == $typeIp) {
                var nameArray = suggestion.data.name.full.split(' ');
                $('#company-ip_lastname').val(nameArray[0] || '');
                $('#company-ip_firstname').val(nameArray[1] || '');
                $('#company-ip_patronymic').val(nameArray[2] || '');
                $('#company-egrip').val(suggestion.data.ogrn);

                $.each(['ip_lastname', 'ip_firstname', 'ip_patronymic', 'address_legal', 'egrip', 'chief_post_name'], function(i,attr) {
                    $('#company-' + attr).addClass('edited').parent().removeClass('has-error');
                });

            } else {
                $('#company-company_type_id').val(companyType[suggestion.data.opf.short]);
                $('#company-kpp').val(suggestion.data.kpp);
                $('#company-ogrn').val(suggestion.data.ogrn);
                if (suggestion.data.management) $('#company-chief_post_name').val(suggestion.data.management.post);
                var nameArray = suggestion.data.management.name.split(' ');
                $('#company-chief_lastname').val(nameArray[0] || '');
                $('#company-chief_firstname').val(nameArray[1] || '');
                $('#company-chief_patronymic').val(nameArray[2] || '');

                $.each(['kpp', 'name_full', 'chief_post_name', 'chief_lastname', 'chief_firstname', 'chief_patronymic', 'address_legal'], function(i,attr) {
                    $('#company-' + attr).addClass('edited').parent().removeClass('has-error');
                });
            }

            if (typeof suggestion.data.state !== "undefined") {
                var registration_date = suggestion.data.state.registration_date;
                if (registration_date) {
                    $('#company-taxregistrationdate').val(moment(registration_date).format("DD.MM.YYYY")).addClass('edited').parent().removeClass('has-error');
                }
            }

        }
    });

JS
);