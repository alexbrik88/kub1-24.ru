<?php

use frontend\components\Icon;
use yii\helpers\Html;

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];
?>

<div class="wrap p-4">
    <div class="p-2">
        <h4 class="text_size_20 mb-3">Зарабатывайте больше с модулем B2B платежей</h4>
        <a class="link mb-3" href="<?= Yii::$app->params['testOutInvoiceLink'] ?>">Пример, как это работает</a>
        <strong class="title-small d-block mb-3">Возможности:</strong>
        <ul class="list-clr line-height-1-5 pb-3">
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img1.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Выставлять счета на вашем сайте</strong>
                        <ul class="list-square pl-4">
                            <li>Счёт создает сам покупатель на вашем сайте. Информация заполняется по ИНН.</li>
                            <li>Счёт в электронном виде.</li>
                            <li>Оплата счёта сразу из счёта.</li>

                        </ul>
                    </div>
                </div>
            </li>
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img2.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Оплачивать счета на вашем сайте</strong>
                        <ul class="list-square pl-4">
                            <li>Электронный счёт передается в банк покупателя и на его основе автоматически создается платежка.</li>
                            <li>Покупателю нужно только подписать платежку.</li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <strong class="title-small d-block mb-3">Бизнес выгоды</strong>
        <ul class="list-clr line-height-1-5 pb-3">
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img3.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Больше продаж и прибыли</strong>
                        <ul class="list-square pl-4">
                            <li>Увеличение количества новых В2В покупателей – ООО и ИП.</li>
                            <li>Конверсия в покупку у В2В (компаний) составляет 10%, а конверсия в покупку у В2С (физ. лиц) всего 3%</li>
                            <li>Увеличение среднего чека. Компании покупают больше и чаще.</li>
                            <li>Повышение лояльности В2В клиентов за счет удобного и быстрого процесса оплаты заказа.</li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img4.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Экономия времени</strong>
                        <ul class="list-square pl-4">
                            <li>Сокращение времени сотрудников интернет-магазина на выставление счетов, за счет полной автоматизации процесса оплат В2В покупателей.</li>
                            <li>Автоматизация  документооборота и финансов</li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <strong class="title-small d-block mb-3">Выгоды Покупателя интернет магазина</strong>
        <ul class="list-clr line-height-1-5 pb-3">
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img4.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Экономия времени</strong>
                        <ul class="list-square pl-4">
                            <li>Не нужно звонить в интернет-магазин, отсылать свои реквизиты и ждать счёт.</li>
                            <li>Упрощение оплаты с расчетного счета ООО и ИП.</li>
                            <li>Платеж от компании, так же прост, как платеж от физ лица по банковской карте.</li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="mb-3">
                <div class="d-flex flex-nowrap">
                    <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                        <img src="<?=$baseUrl?>/images/img5.png" style="height:40px;" alt="">
                    </div>
                    <div>
                        <strong>Удобный документооборот</strong>
                        <ul class="list-square pl-4">
                            <li>Получение товарной накладной в электронном виде, для загрузки в свою учетную систему – не нужно перебивать каждую позицию.</li>
                            <li>На сайте интернет-магазина, для покупателя, после оплаты счета, будет формироваться доверенность на получение товара, на основе реквизитов покупателя и оплаченного товара. <em class="text-red weight-700">СКОРО</em></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <div class="pb-3 mb-3">
            <h4 class="text_size_20 mb-3">Для НЕ интернет магазинов (нельзя добавить товар в корзину): landing page, социальных сетей, e-mail писем</h4>
            <div class="pb-3">
                <h4 class="text_size_20 mb-3">Тариф: Стоимость одной ссылки B2B эквайринга</h4>
                <table class="table table-style text-center">
                    <thead>
                        <tr>
                            <th class="align-middle pb-2">Кол-во <br> счетов в месяц</th>
                            <th class="align-middle pb-2">Стоимость <br> за 1 месяц</th>
                            <th class="align-middle pb-2">Стоимость <br> за 4 месяцa</th>
                            <th class="align-middle pb-2">Стоимость <br> за 12 месяцев</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="pt-2 pb-2 pl-3 pr-3">до 150</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">2000 P</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">7200 P</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">19200 P</td>
                        </tr>
                    </tbody>
                </table>
                <strong class="title-small d-block mb-3">В стоимость входит сервис "Выставление счетов"</strong>
            </div>
            <?= Html::a(Icon::get('add-icon', [
                'class' => 'svg-icon mr-1',
            ]).'<span class="ml-2">Создать модуль B2B для НЕ интернет магазинов</span>', [
                '/out-invoice/create',
                'backTo' => 'B2B',
                'type' => 'landing',
            ], [
                'class' => 'button-regular button-regular_width_360 button-regular_red pl-3 pr-3' .
                            ($isPaid ? '' : ' b2b-pay-panel-trigger'),
            ]); ?>
        </div>
        <div>
            <h4 class="text_size_20 mb-3">Для интернет магазинов</h4>
            <strong class="title-small d-block mb-3">Дополнительные Возможности, к указанным выше:</strong>
            <ul class="list-clr line-height-1-5 pb-3">
                <li class="mb-3">
                    <div class="d-flex flex-nowrap">
                        <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                            <img src="<?=$baseUrl?>/images/img6.png" style="height:40px;" alt="">
                        </div>
                        <div>
                            <strong>Информирование Вас об оплате</strong>
                            <ul class="list-square pl-4">
                                <li>Мы проинформируем Вас об оплате, в течении 5-10 минут,  после проведения платежа банком. </li>
                                <li>Вы будите знать об оплате до поступления средств на расчетный счет.</li>
                                <li>Возможность планирования доставки день-в-день..</li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <strong class="title-small d-block mb-3">Дополнительные Бизнес выгоды, к указанным выше</strong>
            <ul class="list-clr line-height-1-5 pb-3">
                <li class="mb-3">
                    <div class="d-flex flex-nowrap">
                        <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                            <img src="<?=$baseUrl?>/images/img7.png" style="height:40px;" alt="">
                        </div>
                        <div>
                            <strong>Мгновенные платежи</strong>
                            <ul class="list-square pl-4">
                                <li>После того, как платежка получила статус «ИПОЛНЕНО», мгновенное информирование интернет-магазина о том, что счет оплачен.</li>
                                <li>Использование новых технологий в E-commerce.</li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="mb-3">
                    <div class="d-flex flex-nowrap">
                        <div class="flex-shrink-0 mr-3" style="padding-top: 8px;">
                            <img src="<?=$baseUrl?>/images/img8.png" style="height:40px;" alt="">
                        </div>
                        <div>
                            <strong>Планирование доставки в день оплаты</strong>
                            <ul class="list-square pl-4">
                                <li>Имея информацию об оплате, вы можете запланировать или даже доставить товар покупателю в день оплаты. </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="pb-3">
                <h4 class="text_size_20 mb-3">Тариф: Стоимость одной ссылки B2B эквайринга</h4>
                <table class="table table-style text-center">
                    <thead>
                        <tr>
                            <th class="align-middle pb-2">Кол-во <br> счетов в месяц</th>
                            <th class="align-middle pb-2">Стоимость <br> за 1 месяц</th>
                            <th class="align-middle pb-2">Стоимость <br> за 4 месяцa</th>
                            <th class="align-middle pb-2">Стоимость <br> за 12 месяцев</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="pt-2 pb-2 pl-3 pr-3">до 150</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">2000 P</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">7200 P</td>
                            <td class="pt-2 pb-2 pl-3 pr-3">19200 P</td>
                        </tr>
                    </tbody>
                </table>
                <strong class="title-small d-block mb-3">Тариф указан без функции Уведомления об оплате и без интеграции по API. <br> Для получения индивидуального предложения напишите нам на <a href="mailto:support@kub-24.ru">support@kub-24.ru</a></strong>
            </div>
            <?= Html::a(Icon::get('add-icon', [
                'class' => 'svg-icon mr-1',
            ]).'<span class="ml-2">Создать модуль B2B для Интернет магазинов</span>', [
                '/out-invoice/create',
                'backTo' => 'B2B',
                'type' => 'shop',
            ], [
                'class' => 'button-regular button-regular_width_360 button-regular_red pl-3 pr-3' .
                            ($isPaid ? '' : ' b2b-pay-panel-trigger'),
            ]); ?>
        </div>
    </div>
</div>
<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['company'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Далее', ['products'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
            ]); ?>
        </div>
    </div>
</div>