<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\components\Icon;
use yii\helpers\Html;

$tariffItems = [];
$tariffArray = SubscribeTariff::find()->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::B2B_PAYMENT,
])->orderBy(['price' => SORT_ASC])->all();
foreach ($tariffArray as $key => $tariff) {
    $tariffItems[] = [
        'label' => $tariff->getTariffName(),
        'url' => '#',
        'linkOptions' => [
            'class' => 'choose-tariff',
            'data' => [
                'tariff-id' => $tariff->id,
            ],
        ],
    ];
}
$baseTariff = reset($tariffArray);
$model = new PaymentForm($company, ['tariffId' => $baseTariff->id]);

?>

<style>
    .b2b-pay-panel {
        position: fixed;
        top: 0;
        right: 0;
        display: none;
        -moz-border-radius-topright: 20px;
        -webkit-border-top-right-radius: 20px;
        -moz-border-radius-bottomright: 20px;
        -webkit-border-bottom-right-radius: 20px;
        width: 400px;
        height: 100%;
        filter: alpha(opacity=85);
        background-color: #ffffff;
    }
    .b2b-pay-panel {
        z-index: 10051;
    }
    .b2b-pay-panel {
        border-left: 1px solid #e4e4e4;
    }
    .b2b-pay-panel .main-block {
        padding: 20px 30px 10px 30px;
        position: absolute;
        width: 100%;
        height: 95%;
        overflow-y: scroll;
    }
    .b2b-pay-panel .header {
        text-align: left;
        font-size: 21px;
        font-weight: 700;
    }
    .b2b-pay-panel .side-panel-close {
        position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        opacity: 1;
    }
    .b2b-pay-panel .flaticon-growth::before,
    .b2b-pay-panel .flaticon-growth::after {
        font-size: 27px;
        margin-left:0;
    }
</style>

<div class="page-shading-panel b2b-pay-panel-close hidden"></div>
<div class="b2b-pay-panel">
    <div class="main-block" style="height: 100%;">
        <div class="title">
            <button type="button" class="close side-panel-close b2b-pay-panel-close" aria-hidden="true">&times;</button>
            <table style="width: 100%; height: ">
                <tr>
                    <td style="width: 10%; vertical-align: middle; text-align: center;">
                        <span class="notif-marker notif-marker_red">
                            <?= $this->render('//svg-sprite', ['ico' => 'bank']) ?>
                        </span>
                        <?php /*= Html::tag('div', '<i class="flaticon-growth" style="color:#fff;"></i>', [
                            'style' => '
                                display: inline-block;
                                padding: 9px;
                                background-color: #4276a4;
                                border-radius: 30px!important;
                                width:60px;
                                height:60px;
                            ',
                        ])*/ ?>
                    </td>
                    <td style="padding-left: 10px;">
                        <h4 class="m-0">Модуль B2B оплат</h4>
                    </td>
                </tr>
            </table>
        </div>

        <div class="part-pay">
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'class' => 'tariff-group-payment-form mt-2',
            ]) ?>
                <?= Html::hiddenInput('redirectAfterPay', '/b2b') ?>
                <?= Html::activeHiddenInput($model, 'tariffId', [
                    'class' => 'tariff-id-input',
                ]) ?>
                <div class="text-bold">
                    <h5>
                        <span>Оплата за </span>
                        <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
                            <?= Html::a(Html::tag('span', $tariffItems[0]['label'], [
                                'class' => 'dropdown-toggle',
                            ]) . Icon::get('shevron'), '#', [
                                'class' => 'link link_title',
                                'role' => 'button',
                                'data-toggle' => 'dropdown',
                            ])?>
                            <div id="employee-rating-dropdown" class="dropdown-menu keep-open">
                                <div class="popup-dropdown-in overflow-hidden">
                                    <ul class="form-filter-list list-clr">
                                        <?php foreach ($tariffItems as $item) : ?>
                                            <li style="white-space: nowrap;">
                                                <?= Html::a($item['label'], $item['url'], $item['linkOptions']) ?>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <div style="font-size: 44px;">
                        <?php foreach ($tariffArray as $tariff) : ?>
                            <?= Html::tag('div', $tariff->price . " ₽", [
                                'class' => "tariff-price tariff-price-{$tariff->id}" .
                                   ($baseTariff->id != $tariff->id ? ' hidden' : ''),
                            ]) ?>
                        <?php endforeach; ?>
                    </div>
                    <div>
                        <div class="tariff-price-per-month" style="display: inline-block">
                            <?php foreach ($tariffArray as $tariff) : ?>
                                <?= Html::tag('div', round($tariff->price / $tariff->duration_month), [
                                    'class' => "tariff-price tariff-price-{$tariff->id}" .
                                        ($baseTariff->id != $tariff->id ? ' hidden' : ''),
                                ]) ?>
                            <?php endforeach; ?>
                        </div>
                        <div style="display: inline-block"> ₽ / месяц</div>
                    </div>
                </div>
                <div class="mt-2">
                    <h5 style="font-weight:bold">Вы получите:</h5>
                    <p>Выставление счетов и других документов без ограничений.</p>
                    <p>Весь функционал в соответствии с тарифом "Выставление счетов".</p>
                    <p>На вашем сайте вы сможете:</p>
                    <p>Разместить ссылку на Модуль В2В.</p>
                    <p>Выставление счетов для ООО и ИП.</p>
                    <p>Прием онлайн платежей от ООО и ИП.</p>
                    <p>Подготовка покупателем Доверенности на получение у Вас оплаченного товара.</p>
                </div>

                <div id="js-form-alert" class="mar-t-20"></div>

                <div class="mt-3">
                    <?= Html::submitButton('Оплатить', [
                        'class' => 'button-clr button-regular button-regular_red w-full submit',
                        'name' => Html::getInputName($model, 'paymentTypeId'),
                        'value' => PaymentType::TYPE_ONLINE,
                        'data-style' => 'zoom-in',
                    ]); ?>
                </div>
                <div class="form-submit-result hidden"></div>
            <?= Html::endForm() ?>
        </div>
    </div>

</div>


<?php

$this->registerJs(<<<JS
    var b2bPayPanelOpen = function() {
        $(".b2b-pay-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".b2b-pay-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".b2b-pay-panel .main-block").scrollTop(0);
    }

    var b2bPayPanelClose = function() {
        $(".b2b-pay-panel-trigger").removeClass("active");
        $(".b2b-pay-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
    }

    $(document).on("click", ".b2b-pay-panel-trigger", function (e) {
        e.preventDefault();
        b2bPayPanelOpen();
    });

    $(document).on("click", ".b2b-pay-panel-close", function () {
        b2bPayPanelClose();
    });

    $(document).on("click", ".tariff-group-payment-form .choose-tariff", function(e) {
        e.preventDefault();
        var tariff = $(this);
        var form = tariff.closest('form');
        var tariffId = tariff.data('tariff-id');
        $('#paymentform-tariffid', form).val(tariffId);
        tariff.closest('.dropdown-menu').removeClass('show').closest('.dropdown').find('.dropdown-toggle').html(tariff.html());
        $('.tariff-price', form).addClass('hidden');
        $('.tariff-price-'+tariffId, form).removeClass('hidden');

        var per_month = $('.tariff-price-per-month');
        $('.tariff-price', per_month).addClass('hidden');
        $('.tariff-price-'+tariffId, per_month).removeClass('hidden');
    });

    $(document).on('submit', 'form.tariff-group-payment-form', function (e) {
        e.preventDefault();
        var button = document.activeElement;
        var form = $(this).closest('form');
        var formData = form.serializeArray();
        formData.push({ name: button.name, value: button.value });
        $(button).addClass('ladda-button');
        var l = Ladda.create(button);
        l.start();
        $.post($(this).attr('action'), formData, function(data) {
            Ladda.stopAll();
            l.remove();
            $(button).removeClass('ladda-button');
            if (data.content) {
                $('.form-submit-result', form).html(data.content);
            }
            if (data.alert && typeof data.alert === 'object') {
                for (alertType in data.alert) {
                    var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                    $.alert('#js-form-alert', data.alert[alertType], {type: alertType, topOffset: offset});
                }
            }
            if (data.done) {
                if (data.link) {
                    var link=document.createElement("a");
                    link.id = 'document-print-link';
                    link.href = data.link;
                    link.target = '_blank';
                    $('.form-submit-result', form).html(link);
                    document.getElementById('document-print-link').click();
                }
            }
        });
    });
JS
);
