<?php

use common\models\TaxRate;
use frontend\models\ProductNdsForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $toggleButton array */

$model = new ProductNdsForm;
$ndsItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');

$js = <<<JS
$(document).on('hidden.bs.modal', '#product-nds-modal', function (e) {
    $('.has-error', this).removeClass('has-error');
    $('.has-success', this).removeClass('has-success');
    $('.help-block', this).html('');
    $('input', this).prop('checked', false).uniform('refresh');
});
JS;

$this->registerJs($js);
?>

<?php Modal::begin([
    'id' => 'product-nds-modal',
    'header' => Html::tag('h1', 'Массовое изменение НДС'),
    'toggleButton' => $toggleButton
]); ?>
    <style type="text/css">
        #product-nds-form label {
            margin-right: 20px;
        }
    </style>
    <?php $form = ActiveForm::begin([
        'id' => 'product-nds-form',
        'action' => ['/product/change-nds'],
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
    ]); ?>

        <?= $form->field($model, 'type')->checkboxList(ProductNdsForm::$typeItems, []) ?>
        <?= $form->field($model, 'price')->checkboxList(ProductNdsForm::$priceItems, []) ?>
        <?= $form->field($model, 'from')->radioList($ndsItems, []); ?>
        <?= $form->field($model, 'to')->radioList($ndsItems, []); ?>

        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]); ?>
        <?= Html::submitButton('Изменить', [
            'class' => 'btn darkblue text-white',
        ]); ?>

    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
