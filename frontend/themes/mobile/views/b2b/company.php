<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\widgets\AddressTypeahead;
use common\components\widgets\BikTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use frontend\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $account CheckingAccountant */

$this->title = 'Заполните реквизиты';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');
$companyTypes = CompanyType::find()->andWhere(['in_company' => true])->all();
$typeList = ArrayHelper::map($companyTypes, 'id', 'name_short');

$this->registerJsFile('@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);

$view = $model->company_type_id == CompanyType::TYPE_IP ?
    Company::$companyFormView[CompanyType::TYPE_IP] :
    Company::$companyFormView[CompanyType::TYPE_OOO];
?>

<?php $form = ActiveForm::begin([
    'id' => 'company-update-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <div class="wrap p-4 b2b-company">
        <div class="p-2">
            <h4 class="text_size_20 mb-4">Шаг 1: Заполните данные по вашему <?= $model->companyType->name_short ?></h4>
            <strong class="small-title d-block mb-3">Реквизиты <?= $model->companyType->name_short ?></strong>

            <?= $this->render("parts_company/{$view}", [
                'form' => $form,
                'model' => $model,
            ]); ?>

            <?= $this->render('parts_company/_accounts', [
                'form' => $form,
                'model' => $model,
                'accounts' => $accounts,
            ]) ?>

            <strong class="small-title d-block mb-3 pt-3">
                Система налогообложения
            </strong>

            <?= $form->field($taxation, 'osno', [
                'parts' => [
                    '{input}' => Html::beginTag('div', [
                            'class' => 'field-width inp_one_line_company',
                            'style' => 'padding-top: 7px; display: inline-block;',
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'osno', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => (boolean) $taxation->usn,
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'usn', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => (boolean) $taxation->osno,
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'envd', [
                            'labelOptions' => ['class' => 'label mr-4'],
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'psn', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => $model->company_type_id != CompanyType::TYPE_IP,
                        ]) . "\n" .
                        Html::beginTag('div', ['class' => 'tax-usn-config collapse' . ($taxation->usn ? ' show' : '')]) . "\n" .
                        $form->field($taxation, 'usn_type', [
                            'options' => [
                                'class' => 'row',
                                'style' => 'max-width: 300px',
                            ],
                        ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                            'class' => 'column mb-2',
                            'style' => 'margin-top: 10px;',
                            'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                                $radio = Html::radio($name, $checked, [
                                    'id' => 'usn_radio_'.$index,
                                    'class' => 'usn_type_radio',
                                    'value' => $value,
                                    'label' => $label,
                                    'labelOptions' => [
                                        'wrapInput' => true,
                                        'class' => 'label',
                                    ],
                                ]);
                                $itemRadio = Html::tag('div', $radio, [
                                    'class' => 'column mb-2',
                                ]);
                                $percent = Html::activeTextInput($taxation, 'usn_percent', [
                                    'class' => 'form-control d-inline-block text-right usn-percent usn-percent-' . $value,
                                    'style' => 'width: 75px;',
                                    'disabled' => !$checked,
                                    'value' => $checked ? $taxation->usn_percent : $taxation->defaultUsnPercent($value),
                                    'data-default' => $taxation->defaultUsnPercent($value),
                                ]) . ' <span class="ml-1">%</span>';
                                $itemPercent = Html::tag('div', $percent, [
                                    'class' => 'column mb-2 pl-0',
                                ]);

                                return Html::tag('div', "$itemRadio\n$itemPercent", [
                                    'class' => 'row align-items-center justify-content-between usn_type_row',
                                ]);
                            },
                        ])->label(false) . "\n" .

                        Html::endTag('div') . "\n" . // .tax-usn-config

                        Html::endTag('div'), // .inp_one_line_company
                ],
            ])->label(false); ?>

            <div class="tax-patent-config collapse <?=($taxation->psn ? ' show' : '')?>">
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'ip_patent_city')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="position: relative;">
                        <?= $form->field($model, 'patentDate')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control date-picker ico',
                            'autocomplete' => 'off',
                            'value' => ($taxation->psn) ? $model->patentDate : null,
                        ])->label('Дата начала действия патента') ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'patentDateEnd')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control input-sm date-picker ico',
                            'autocomplete' => 'off',
                            'value' => ($taxation->psn) ? $model->patentDateEnd : null,
                        ])->label('Дата окончания действия патента') ?>
                    </div>
                </div>
            </div>

            <div class="nds-view-osno collapse <?= $taxation->osno ? ' show' : ''; ?>">
                <?= $form->field($model, 'nds')->radioList($ndsViewOsno, [
                    'class' => 'inp_one_line_company',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, [
                            'value' => $value,
                        ]) . $label, [
                            'class' => 'label',
                            'style' => 'margin-right: 10px;',
                        ]) . Html::a('Пример счета', [
                            '/company/view-example-invoice',
                            'nds' => $value == 1 ? true : false,
                        ], [
                            'class' => 'link',
                            'target' => '_blank',
                            'style' => 'margin-right: 20px;',
                        ]);
                    },
                ])->label('В счетах цену за товар/услугу указывать') ?>
            </div>
        </div>
    </div>

<?php $form->end(); ?>

<div class="wrap p-4">
    <div class="p-2">
        <?php Modal::begin([
            'id' => 'modal-companyImages',
            'title' => 'Загрузить логотип, печать и подпись',
            'titleOptions' => [
                'class' => 'mb-0',
            ],
            'toggleButton' => [
                'label' => Icon::get('add-icon', [
                    'class' => 'svg-icon mr-1',
                ]) . Html::tag('span', 'Загрузить логотип, печать и подпись', [
                    'class' => 'ml-2',
                ]),
                'class' => 'button-regular button-hover-content-red pl-3 pr-3',
            ],
            'closeButton' => [
                'label' => Icon::get('close'),
                'class' => 'modal-close close',
            ],
        ]) ?>

        <?= $this->render('//company/form/_partial_files_tabs', [
            'model' => $model,
        ]) ?>

        <?php Modal::end() ?>
    </div>
</div>

<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-clr button-width button-regular button-regular_red mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
                'form' => 'company-update-form',
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('parts_company/_company_inn_api') ?>

<?php
$this->registerJs(<<<JS
    $(document).on('show.bs.modal', '#modal-companyImages', function () {
        $('a[href="#company-image-tabs-tab2"]').tab('show');
    });
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
        }
    });

    window.isB2B = true;

    $('#company-has_chief_patronymic').change(function() {
        var checked = $(this).is(':checked');
        var target = $(this).data('target');
        if (checked) {
            $(target).val('');
        }
        $(target).prop('readonly', checked).removeClass('is-invalid').parent().find('.invalid-feedback').html('');
    });

    // Система налогообложения
    $(document).ready(function() {
        if (0 === $(".nds-view-osno").find('input[type="radio"]:checked').length)
            $(".nds-view-osno").find('input[type="radio"]').first().prop('checked', 'checked').uniform();
    });

    function resetDisabledCheckboxes() {
        $('.taxation-system').find('.form-group').removeClass('has-success').removeClass('has-error');
    }

    $(document).on("change", "#companytaxationtype-osno", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true);

            $(".nds-view-osno", form).collapse("show");
        } else {
            $("#companytaxationtype-usn", form).prop("disabled", false);
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-usn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true);
            $(".tax-usn-config", form).collapse("show");
            $("input.usn-percent", form).each(function (i, el) {
                $(el).val($(el).data('default'));
            });
        } else {
            $("#companytaxationtype-osno", form).prop("disabled", false);
            $(".tax-usn-config", form).collapse("hide");
            $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#companytaxationtype-usn_percent", form).val("");
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "input.usn_type_radio", function() {
        $("input.usn_type_radio", this.form).each(function (i, el) {
            $(el).closest('.usn_type_row').find('input.usn-percent').prop('disabled', !$(this).is(':checked'));
        });
    });
    $(document).on("change", "#companytaxationtype-psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true);
            $(".tax-patent-config", form).collapse("show");
        } else {
            $("#companytaxationtype-envd", form).prop("disabled", false);
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes();
    });

JS
);

// FILES UPLOAD
$this->registerJS(<<<JS
$('#company-logoimage').change(function () {
        var preview = document.querySelector('div[id=company_logo] img');
        var file    = document.querySelector('input[id=company-logoimage]').files[0];
        previewFile(preview, file);
});
$('#company-printimage').change(function () {
        var preview = document.querySelector('div[id=company_print] img');
        var file    = document.querySelector('input[id=company-printimage]').files[0];
        previewFile(preview, file);
});
$('#company-chiefsignatureimage').change(function () {
        var preview = document.querySelector('div[id=company_chief] img');
        var file    = document.querySelector('input[id=company-chiefsignatureimage]').files[0];
        previewFile(preview, file);
});
function previewFile(preview, file) {
    var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
}
JS
);

$this->registerJs(<<<JS

function repaintAccountNumbers() {
    $('.company-account').each(function(i,v) {
       $(v).find('.small-title > span').html(2+i);
    });
}

$(document).on("click", ".btn-confirm-yes", function() {
    var account_id = $(this).data('model_id');
    var account_block = $('.company-account').filter('[data-id="' + account_id + '"]');

    if (account_block.length) {

        if (!$(account_block).find('.dictionary-bik').val().trim())
           $('#add-new-account').removeAttr('disabled');

        if (!$(account_block).hasClass('new-account')) {
            $('#company-update-form').append('<input type="hidden" name="delete_rs[]" value="' + account_id + '" />');
        }

        $(account_block).remove();
        repaintAccountNumbers();
    }

    return false;
});

JS
);