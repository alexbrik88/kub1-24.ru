<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\out\OutInvoice;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\components\Icon;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OutInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $backTo string */
/* @var $company Company */
/* @var $isPaid bool */

$backTo = isset($backTo) ? $backTo : 'company';
$company = Yii::$app->user->identity->company;

//$isBlocked = !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT);

$actualSubscribes = SubscribeHelper::getPayedSubscriptions($company->id, SubscribeTariffGroup::B2B_PAYMENT);
$expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
$expireDateFormatted = ($expireDate) ? date(DateHelper::FORMAT_USER_DATE, $expireDate) : '';
?>

<?php $pjax = Pjax::begin([
    'id' => 'out-invoice-contractor-pjax',
    'timeout' => 10000,
]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => 'У вас пока нет ни одной ссылки на модуль B2B. ' .
        Html::a('Создать ссылку', ['/out-invoice/create', 'backTo' => $backTo, 'type' => 'landing'], ['data-pjax' => 0]),
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'layout' => "{items}\n{pager}",
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => [
                'width' => '20',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'header' => '##',
        ],
        [
            'attribute' => 'site',
            'label' => 'Сайт',
            'headerOptions' => [
                'class' => 'align-middle pt-0 pb-1',
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->return_url;
            }
        ],
        [
            'headerOptions' => [
                'width' => '15%',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'attribute' => 'is_demo',
            'label' => 'Ссылка на модуль В2В',
            'format' => 'raw',
            'value' => function ($model) use ($isPaid) {
                if ($model->is_outer_shopcart) {
                    return '';
                }

                $moduleLink = Html::a('Ссылка для сайта', $model->outUrl, [
                    'title' => 'Посмотреть',
                    'aria-label' => 'Посмотреть',
                    'target' => '_blank',
                    'data' => [
                        'pjax' => 0
                    ]]);
                $demoModuleLink = ($model->demoOutUrl ? Html::a("Демо ссылка", $model->demoOutUrl, [
                    'title' => 'Посмотреть',
                    'aria-label' => 'Посмотреть',
                    'target' => '_blank',
                    'data' => [
                        'pjax' => 0
                    ]
                    ]) : '');

                $copyLink = Html::a("<span class='fa fa-clipboard'></span>" . Html::hiddenInput('link', $model->outUrl), '#', [
                    'title' => 'Скопировать ссылку',
                    'aria-label' => 'Скопировать ссылку',
                    'class' => 'copy-link',
                    'data' => [
                        'pjax' => 0
                    ]
                ]);

                return  ($isPaid) ? ($moduleLink . '&nbsp;&nbsp;' . $copyLink) : $demoModuleLink;
            }
        ],
        [
            'headerOptions' => [
                'width' => '10%',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'attribute' => 'has_payment',
            'contentOptions' => ['class' => 'text-center'],
            'label' => 'Оплата',
            'format' => 'raw',
            'value' => function ($model) use ($isPaid, $expireDateFormatted) {

                $linkPayModule = Html::a('Оплатить', 'javascript:;', [
                    'class' => 'button-regular button-hover-content-red pl-3 pr-3 b2b-pay-panel-trigger',
                ]);

                return  ($isPaid) ? $expireDateFormatted : $linkPayModule;
            }
        ],
        [
            'headerOptions' => [
                'width' => '10%',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'attribute' => 'links_count',
            'label' => 'Кол-во счетов',
            'format' => 'raw',
            'value' => function ($model) {

            return (int)Invoice::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->byDeleted()
                ->byDemo()
                ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
                ->andWhere(['from_out_invoice' => true])
                ->andWhere(['by_out_link_id' => $model->id])
                ->count();
            }
        ],
        [
            'headerOptions' => [
                'width' => '10%',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'attribute' => 'bills_count',
            'label' => 'Кол-во оплат',
            'format' => 'raw',
            'value' => function ($model) use ($company) {
                return (int)Invoice::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted()
                    ->byStatus(InvoiceStatus::STATUS_PAYED)
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['from_out_invoice' => true])
                    ->andWhere(['by_out_link_id' => $model->id])
                    ->count();
            }
        ],
        [
            'attribute' => 'note',
            'enableSorting' => false,
            'headerOptions' => [
                'width' => '30%',
                'class' => 'align-middle pt-0 pb-1',
            ],
        ],
        [
            'attribute' => 'status',
            'enableSorting' => false,
            'filter' => [null => 'Все'] + OutInvoice::$statusAvailable,
            's2width' => '120px',
            'headerOptions' => [
                'width' => '100',
                'class' => 'align-middle pt-0 pb-1',
            ],
            'value' => 'statusValue',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'out-invoice',
            'template' => '<div class="p-3 ta-r nowrap"> {view} {update} {delete} </div>',
            'headerOptions' => [
                'width' => '40',
            ],
            'buttons' => [
                'view' => function ($url, $model, $key) use ($isPaid) {
                    if ($model->is_outer_shopcart) {
                        return '';
                    }

                    $label = Icon::get($isPaid ? 'eye' : 'eye-ban');
                    $url = $isPaid ? $model->outUrl : null;
                    return Html::a($label, $url, [
                        'title' => 'Посмотреть',
                        'aria-label' => 'Посмотреть',
                        'class' => 'button-clr link mr-2' . ($isPaid ? '' : 'b2b-pay-panel-trigger'),
                        'target' => '_blank',
                        'data' => [
                            'pjax' => 0,
                        ]
                    ]);
                },
                'update' => function ($url, $model, $key) use ($backTo) {
                    return Html::a(Icon::get('pencil'), [
                        '/out-invoice/update',
                        'id' => $model->id,
                        'backTo' => $backTo,
                    ], [
                        'class' => 'button-clr link ml-1',
                        'title' => 'Изменить',
                        'aria-label' => 'Изменить',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model, $key) use ($backTo) {
                    return frontend\themes\mobile\widgets\ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => Icon::get('garbage'),
                            'class' => 'button-clr link ml-2',
                            'title' => 'Удалить',
                            'aria-label' => 'Удалить',
                            'data-pjax' => '0',
                            'tag' => 'a',
                        ],
                        'confirmUrl' => Url::to(['/out-invoice/delete', 'id' => $model->id, 'backTo' => $backTo]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить ссылку?',
                    ]);
                }
            ],
        ],
    ],
]); ?>

<?php $pjax->end(); ?>

<?php
$this->registerJs('
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).val()).select();
  document.execCommand("copy");
  $temp.remove();
}

$(".copy-link").bind("click", function(e) {
   e.preventDefault();
   var linkElement = $(this).find("input[name=\'link\']");
   copyToClipboard(linkElement);

   window.toastr.success("Ссылка скопирована в буфер обмена", "", {
        "closeButton": true,
        "showDuration": 1000,
        "hideDuration": 1000,
        "timeOut": 1000,
        "extendedTimeOut": 1000,
        "escapeHtml": false,
    });

});
');
