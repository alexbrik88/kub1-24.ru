<?php
namespace frontend\themes\kub\widgets;

use common\components\helpers\Html;
use common\models\product\Product;
use common\models\product\ProductSearch;
use yii\base\Widget;
use Yii;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

/**
 * Class StoreFilterWidget
 * @package frontend\widgets
 */
class StoreFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $store;
    /**
     * @var
     */
    public $productionType;
    /**
     * @var
     */
    public $priceList = null;
    /**
     * @var
     */
    public $help;
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->priceList = Yii::$app->request->get('priceList');

        Yii::$app->view->registerJs('
            $(document).on("submit", "#store-form", function () {
                $this = $(this);
                $modal = $this.closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"store\" value=\"' . $this->store . '\">");
                $action = $this.data("isnewrecord") == 1 ? "/product/create-store?productionType=' . $this->productionType . '&priceList=' . $this->priceList . '" : "/product/update-store?id=" + $this.data("modelid") + "&productionType=' . $this->productionType . '&priceList=' . $this->priceList . '";
                $.post($action, $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        //$("ul#user-bank-dropdown").replaceWith(data.html);
                        if (data.label) {
                            $("span.store-label").text(data.label + " ");
                        }
                        Ladda.stopAll();
                        
                        var block = $("#user-bank-dropdown");
                        var update_block = $(block).find(".form-edit").filter("[data-id="+data.id+"]");
                        
                        if ($(update_block).length) {
                            $(update_block).find("a.goto-store").html(data.name);
                        } else {
                            var template = $(block).find(".store-template").clone();
                            var href_main = $(template).find("a.goto-store").attr("href");
                            var href_update = $(template).find("a.update-store").attr("href");                        
                            $(template).addClass("form-edit form-edit_alternative").removeClass("store-template").attr("data-id", data.id);
                            $(template).find("a.goto-store").attr("href", href_main.replace("store=0", "store=" + data.id)).html(data.name);
                            $(template).find("a.update-store").attr("href", href_update.replace("id=0", "id=" + data.id));
                            $(".form-edit").last().before(template);
                            $(template).show();                        
                        }                          
                        
                    } else {
                        $this.html($(data.html).find("form").html());
                        refreshUniform();
                        refreshDatepicker();
                        createSimpleSelect2("store-responsible_employee_id");
                    }
                });

                return false;
            });
        ');

        return $this->getStoreList();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStoreList()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $storeSelectList = $user->getStores()
            ->select('name')
            ->orderBy(['is_main' => SORT_DESC])
            ->indexBy('id')
            ->column();
        $hasArchive = $this->company->getProducts()->andWhere([
            'production_type' => $this->productionType,
            'status' => Product::ARCHIVE,
        ])->exists();
        $storeItems = [];
        if ($storeSelectList) {
            $this->pageTitle = 'Склад';
            if (count($storeSelectList) > 1) {
                $storeSelectList += ['all' => 'Все склады'];
            }
        }
        if ($hasArchive) {
            if (count($storeSelectList) === 0) {
                $storeSelectList[''] = $this->pageTitle;
            }
            $storeSelectList += ['archive' => 'Архив'];
        }
        foreach ($storeSelectList as $key => $name) {
            $storeItems[] = [
                'id' => $key,
                'label' => $name,
                'url' => [
                    $this->priceList ? 'add-to-price-list' : 'index',
                    'productionType' => $this->productionType,
                    'store' => $key ? : null,
                    'priceList' => $this->priceList,
                ],
                'linkOptions' => [
                    'class' => $this->store == $key ? 'active' : '',
                ],
            ];
        }
        $currentName = isset($storeSelectList[$this->store]) ? $storeSelectList[$this->store] : $this->pageTitle;


        if ($storeItems) {

            return $this->render('storeFilterWidget', [
                'currentName' => $currentName,
                'storeItems' => $storeItems,
            ]);
        }

        return "<h4>".Html::encode($currentName)."</h4>";
    }
}