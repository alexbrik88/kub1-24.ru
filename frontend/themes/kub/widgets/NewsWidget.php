<?php

namespace frontend\themes\kub\widgets;

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\news\News;
use Yii;
use yii\base\Widget;
use yii\db\Expression;

class NewsWidget extends Widget
{
    public Employee $employee;

    public function run(): ?string {
        $currentEmployeeCompany = $this->employee->currentEmployeeCompany;
        $company = $currentEmployeeCompany->company;
        $registeredAt = Carbon::createFromTimestamp($company->created_at);
        $label = 'Обновления';
        $includeUsefulNews = false;
        $buttonClass = null;
        $orderBy = [
            'type' => SORT_ASC,
            new Expression('date IS NULL'),
            'date' => SORT_DESC,
            new Expression('serial_number IS NULL'),
            'serial_number' => SORT_ASC,
            'created_at' => SORT_DESC,
        ];
        if (Carbon::now()->diffInDays($registeredAt) <= 7) {
            $label = 'Полезное';
            $includeUsefulNews = true;
        }

        $query = News::find()
            ->andWhere(['status' => News::STATUS_ACTIVE])
            ->andWhere([
                'OR',
                ['<=', 'date', date(DateHelper::FORMAT_DATE)],
                ['date' => null],
            ]);
        if (!$includeUsefulNews) {
            $query->andWhere(['type' => News::TYPE_NEWS]);
        }

        $cloneQuery = clone $query;
        $items = $query->orderBy($orderBy)->all();
        if (empty($items)) {
            return null;
        }

        if ($cloneQuery->andWhere(['>', 'date', date(DateHelper::FORMAT_DATE, $currentEmployeeCompany->news_viewed_at)])->exists()) {
            $buttonClass = 'active';
        }

        return $this->render('newsWidget', [
            'label' => $label,
            'items' => $items,
            'company' => $company,
            'buttonClass' => $buttonClass,
        ]);
    }
}
