<?php
namespace frontend\themes\kub\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Expression;
use common\models\Company;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\Emoney;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use frontend\rbac\permissions\Service;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/**
 * Class CashFilterWidget
 * @package frontend\widgets
 */
class CashFilterWidget extends Widget
{
    /**
     * @var
     */
    public $activeWallet;

    /**
     * @var
     */
    public $activeCurrency;

    /**
     * @var Employee
     */
    public $user;

    /**
     * @var Company
     */
    public $company;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->user = Yii::$app->user->identity;
        $this->company = Yii::$app->user->identity->company;
        $this->activeWallet = Yii::$app->session->get('cash_widget_wallet', 'all');
        $this->activeCurrency = Yii::$app->session->get('cash_widget_currency', Currency::DEFAULT_NAME);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $result = null;
        $result .= $this->getWalletList();
        $result .= $this->getAccountantModals();
        $result .= $this->getAccountantTooltipsters();
        Yii::$app->view->registerJs($this->getAccountantJS());

        return $result;
    }

    private function getWalletList()
    {
        $bankArray = $this->getBanks();
        $cashboxArray = $this->getCashboxes();
        $emoneyArray = $this->getEmoneys();
        $acquiringArray = $this->getAcquirings();
        $cardArray = $this->getCards();

        $items = $this->buildItems($bankArray, $cashboxArray, $emoneyArray, $acquiringArray, $cardArray);
        $items = $this->sortItems($items);
        $items = $this->addToItemsButtonAll($items);
        $currentName = $this->getCurrentName($items);

        if ($this->company->strict_mode == Company::ON_STRICT_MODE && count($items) <= 3) {
            return $this->render('cashFilterWidgetStrictMode', [
                'currentName' => $currentName,
                'items' => $items,
            ]);
        }

        return $this->render('cashFilterWidget', [
            'currentName' => $currentName,
            'items' => $items,
            'bankingInfoCheckingAccount' => $this->getCheckingAccount2BankingInfo($bankArray)
        ]);
    }

    private function getCheckingAccount2BankingInfo($bankArray)
    {
        if ($this->activeCurrency == Currency::DEFAULT_NAME) {
            if ($this->activeWallet == 'all') {
                foreach ($bankArray as $bank)
                    if ($bank->isMain)
                        return $bank; // return main account (RUB only)
            } else {
                @list($walletType, $walletRs) = explode('_', $this->activeWallet);
                if ($walletType == CashFlowsBase::WALLET_BANK) {
                    foreach ($bankArray as $bank)
                        if ($bank->rs === $walletRs)
                            return $bank; // return current account (RUB only)
                }
            }
        }

        return null;
    }

    private function getBanks()
    {
        $showBanks = Yii::$app->user->can(Service::HOME_CASH);

        if ($showBanks) {
            return $this->company->getCheckingAccountants()
                ->with('currency')
                ->groupBy('rs')
                ->indexBy('rs')
                ->orderBy(new Expression(
                    'IF(currency_id = '.Currency::DEFAULT_ID.', 0, currency_id), type'))
                ->all();
        }

        return [];
    }

    private function getCashboxes()
    {
        return $this->user->getCashboxes()
            ->with('currency')
            ->orderBy(new Expression(
                'IF(currency_id = '.Currency::DEFAULT_ID.', 0, currency_id), is_main DESC, name'))
            ->indexBy('id')
            ->all();
    }

    private function getEmoneys()
    {
        return $this->company->getEmoneys()
            ->with('currency')
            ->orderBy(new Expression(
                'IF(currency_id = '.Currency::DEFAULT_ID.', 0, currency_id), is_main DESC, name'))
            ->indexBy('id')
            ->all();
    }

    private function getAcquirings()
    {
        return Acquiring::find()->andWhere(['company_id' => $this->company->id])->all();
    }

    private function getCards()
    {
        return CardAccount::find()->andWhere(['company_id' => $this->company->id])->all();
    }

    private function getAccountantModals()
    {
        $modals = $this->render('@frontend/modules/cash/views/account/create/_modal');
        $modals .= '<div id="company_rs_update_form_list">' .
            $this->render('@frontend/views/company/form/_rs_update_form_list', [
                'model' => $this->company,
                'actionUrl' => '/cash/bank/update-checking-accountant'
            ]) .
            '</div>';

        return $modals;
    }

    private function getWalletType($wallet)
    {
        if ($wallet instanceof CheckingAccountant) {
            return ($this->getIsRub($wallet)) ? CashFlowsBase::WALLET_BANK : CashFlowsBase::WALLET_FOREIGN_BANK;
        } elseif ($wallet instanceof Cashbox) {
            return ($this->getIsRub($wallet)) ? CashFlowsBase::WALLET_CASHBOX : CashFlowsBase::WALLET_FOREIGN_CASHBOX;
        } elseif ($wallet instanceof Emoney) {
            return ($this->getIsRub($wallet)) ? CashFlowsBase::WALLET_EMONEY : CashFlowsBase::WALLET_FOREIGN_EMONEY;
        } elseif ($wallet instanceof Acquiring) {
            return CashFlowsBase::WALLET_ACQUIRING;
        } elseif ($wallet instanceof CardAccount) {
            return CashFlowsBase::WALLET_CARD;
        }

        return null;
    }

    private function getWalletKey($walletType, $wallet)
    {
        if ($wallet instanceof CheckingAccountant) {
            return $walletType . '_' . $wallet->rs;
        } elseif ($wallet instanceof Cashbox) {
            return $walletType . '_' . $wallet->id;
        } elseif ($wallet instanceof Emoney) {
            return $walletType . '_' . $wallet->id;
        } elseif ($wallet instanceof Acquiring) {
            return $walletType . '_' . $wallet->identifier;
        } elseif ($wallet instanceof CardAccount) {
            return $walletType . '_' . $wallet->id;
        }

        return null;
    }

    private function getLabel($wallet)
    {
        if ($wallet instanceof CheckingAccountant) {
            $label = $this->getIsRub($wallet) ? $wallet->name : ($wallet->currency->name.' '.$wallet->name);
        } elseif ($wallet instanceof Cashbox) {
            $label = $this->getIsRub($wallet) ? $wallet->name : ($wallet->currency->name.' '.$wallet->name);
        } elseif ($wallet instanceof Emoney) {
            $label = $this->getIsRub($wallet) ? $wallet->name : ($wallet->currency->name.' '.$wallet->name);
        } elseif ($wallet instanceof Acquiring) {
            $label = $wallet->getFullName();
        } elseif ($wallet instanceof CardAccount) {
            $label = $wallet->getFullName();
        } else {
            $label = '???';
        }

        if ($wallet instanceof CheckingAccountant) {
            $title = "р/с: {$wallet->rs}";
            if ($wallet->isClosed) {
                $title .= ' - закрыт';
            }
        } else {
            $title = $label;
        }

        return Html::tag('span', $label, ['class' => 'tooltip_rs', 'title' => $title]);
    }

    private function buildItems(...$accountsArr)
    {
        $items = [];
        foreach ($accountsArr as $wallets)
        {
            foreach ($wallets as $account) {

                $walletType = self::getWalletType($account);
                $wallet = self::getWalletKey($walletType, $account);
                $currency = $account->currency->name ?? Currency::DEFAULT_NAME;

                $label = self::getLabel($account);
                $items[] = [
                    'walletType' => $walletType,
                    'walletKey' => $wallet,
                    'currency' => $currency,
                    'id' => $account->id,
                    'label' => $label,
                    'url' => [
                        '/cash/default/operations',
                        'wallet' => $wallet,
                        'currency' => ($currency == Currency::DEFAULT_NAME) ? null : $currency
                    ],

                ];
            }
        }

        return $items;
    }

    private function sortItems($items)
    {
        uasort($items, function ($a, $b) {
            if ($a['currency'] == Currency::DEFAULT_NAME) $a['currency'] = 'AAA';
            if ($b['currency'] == Currency::DEFAULT_NAME) $b['currency'] = 'AAA';

            if ($a['currency'] == $b['currency']) {
                return ($a['walletType'] < $b['walletType']) ? -1 : 1;
            }

            return ($a['currency'] < $b['currency']) ? -1 : 1;
        });

        return $items;
    }

    private function addToItemsButtonAll($items)
    {
        $currencyCount = [];
        foreach ($items as $item) {
            if (!isset($currencyCount[$item['currency']]))
                $currencyCount[$item['currency']] = 0;

            $currencyCount[$item['currency']]++;
        }

        $return = [];
        foreach ($items as $item)
        {
            if ($currencyCount[$item['currency']] > 1) {
                $wallet = 'all';
                $currency = $item['currency'];
                $return[] = [
                    'walletType' => null,
                    'walletKey' => $wallet,
                    'currency' => $currency,
                    'id' => null,
                    'label' => 'Все счета ' . $item['currency'],
                    'url' => [
                        '/cash/default/operations',
                        'wallet' => $wallet,
                        'currency' => ($currency == Currency::DEFAULT_NAME) ? null : $currency
                    ]
                ];
            }

            $return[] = $item;

            $currencyCount[$item['currency']] = -1;
        }

        return $return;
    }

    private function getCurrentName($items)
    {
        $name = 'Все счета';
        foreach ($items as $item) {
            if ($this->activeWallet == $item['walletKey'] && $this->activeCurrency == $item['currency'])
                $name = $item['label'];
        }

        return $name;
    }

    private function getIsRub($wallet)
    {
        return ($wallet->currency_id == Currency::DEFAULT_ID);
    }

    private function getAccountantTooltipsters()
    {
        return yii2tooltipster::widget([
            'options' => [
                'class' => '.tooltip_rs:not(.tooltipstered)',
            ],
            'clientOptions' => [
                'theme' => ['tooltipster-kub'],
                'trigger' => 'hover',
                'position' => 'right',
            ],
        ]);
    }

    private function getAccountantJS()
    {
        return <<<JS
            window.companyAccountAfterSave = function (data) {
                $('#add_company_account_modal').modal('hide');
                var multiWalletSelect = $('.modal.show #multi_wallet_select');
                if (multiWalletSelect.length) {
                    let newOption = $("<option/>", {
                        "value": data.id,
                        "text": data.bank_name,
                        "data-wallet": data.data_wallet,
                        "data-currency": data.data_currency,
                        "data-symbol": data.data_symbol,
                        "data-balance": data.data_balance,
                        "data-id": data.data_id,
                        "data-rs": data.data_rs,
                    });
                    $(multiWalletSelect).prepend(newOption);
                    $(multiWalletSelect).val(data.id);
                    $(multiWalletSelect).trigger('change');
                    $("#add-company-rs").modal("hide");

                    window.toastr.success("Счет добавлен", "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                }
                $.pjax.reload({container: '#company_cash_account_filter_pjax'});
            };
            $("#user-bank-dropdown .link").on("click", function(e) {
                var target = $(this).attr("data-target");
                if (target)
                    $(target).modal("show");
            });
JS;
    }
}
