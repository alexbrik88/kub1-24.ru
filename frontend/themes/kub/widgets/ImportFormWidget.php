<?php

namespace frontend\themes\kub\widgets;

use common\modules\import\models\AbstractImportForm2;
use common\modules\import\models\ImportJobDataRepository;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class ImportFormWidget extends Widget
{
    /**
     * @var AbstractImportForm2
     */
    public $form;

    /**
     * @var string
     */
    public $disconnectUrl;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $headerContent;

    /**
     * @var bool
     */
    public bool $disableDateTo = false;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->disconnectUrl)) {
            throw new InvalidConfigException();
        }

        if (!($this->form instanceof AbstractImportForm2)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     * @return void
     */
    public function run(): void
    {
        $jobDataRepository = new ImportJobDataRepository($this->form->getEmployee()->company);
        $lastAutoJobs = $jobDataRepository->getAutoJobsData($this->form->getCommandType());

        echo $this->renderFile(__DIR__ . '/views/importFormWidget.php', [
            'headerContent' => $this->headerContent,
            'form' => $this->form,
            'identifier' => $this->identifier,
            'disconnectUrl' => $this->disconnectUrl,
            'lastAutoJobs' => $lastAutoJobs,
            'disableDateTo' => $this->disableDateTo,
        ]);
    }
}
