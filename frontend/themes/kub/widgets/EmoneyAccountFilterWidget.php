<?php

namespace frontend\themes\kub\widgets;


use common\components\helpers\Html;
use common\models\currency\Currency;
use frontend\rbac\permissions;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;
use yii\base\Widget;
use Yii;

/**
 * Class EmoneyAccountFilterWidget
 * @package frontend\widgets
 */
class EmoneyAccountFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $emoney;
    /**
     * @var
     */
    public $company;

    /**
     * @return null|string
     * @throws \Exception
     */
    public function run()
    {
        $this->registerScript();

        $canUpdate = Yii::$app->user->can(permissions\Company::ADMINISTRATOR);
        $currentName = $this->pageTitle;
        $activeAccount = null;
        $itemsCount = 0;
        $items = [];
        $foreignItems = [];
        $route = ['/cash/bank/index'];

        $emoneyArray = $this->company->getEmoneys()
            ->with('currency')
            ->orderBy([
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])
            ->indexBy('id')
            ->all();

        if ($emoneyArray) {
            foreach ($emoneyArray as $account) {
                $isActive = false;
                if ($this->emoney == $account->id) {
                    $activeAccount = $account;
                    $isActive = true;
                }
                $label = $account->currency_id == Currency::DEFAULT_ID ?
                        $account->name :
                        $account->currency->name.' '.$account->name;

                $item = [
                    'canUpdate' => $canUpdate,
                    'currency' => $account->currency->name,
                    'id' => $account->id,
                    'label' => Html::tag('span', $label, [
                        'class' => 'account_tooltip',
                        'title' => $label,
                    ]),
                    'url' => ['index', 'emoney' => $account->id],
                    'updateUrl' => ['/cash/account/emoney/update', 'id' => $account->id],
                    'isActive' => $isActive,
                ];

                if ($account->currency_id == Currency::DEFAULT_ID) {
                    $items[] = $item;
                } else {
                    $foreignItems[$account->currency->name][] = $item;
                }
            }

            $itemsCount = count($items);
            $foreignItemsCount = count($foreignItems);

            if ($itemsCount + $foreignItemsCount > 0) {
                if ($this->emoney == 'all') {
                    $currentName = 'Все кассы'.($foreignItemsCount > 0 ? ' RUB' : '');
                } elseif (in_array($this->emoney, Currency::$foreignArray)) {
                    $currentName = 'Все кассы '.$this->emoney;
                } elseif ($account = $emoneyArray[$this->emoney] ?? null) {
                    $currentName = $account->currency_id == Currency::DEFAULT_ID ?
                        $account->name :
                        $account->currency->name.' '.$account->name;
                }
                if ($itemsCount > 1) {
                    $items[] = [
                        'canUpdate' => false,
                        'id' => null,
                        'label' => 'Все кассы'.($foreignItemsCount > 0 ? ' RUB' : ''),
                        'url' => ['index', 'emoney' => 'all'],
                        'isActive' => $this->emoney == 'all',
                    ];
                }
                foreach ($foreignItems as $currencyName => $currencyItems) {
                    if (count($currencyItems) > 1) {
                        $currencyItems[] = [
                            'canUpdate' => false,
                            'id' => null,
                            'label' => 'Все кассы '.$currencyName,
                            'url' => ['index', 'emoney' => $currencyName],
                            'isActive' => $this->emoney == $currencyName,
                        ];
                    }

                    $items = array_merge($items, $currencyItems);
                }
            }
        }

        return $this->render('emoneyAccountFilterWidget', [
            'currentName' => $currentName,
            'allItems' => $items,
            'canCreate' => $canUpdate,
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function registerScript()
    {
        Yii::$app->view->registerJs('
            window.emoneyAccountFilterReload = function() {
                $("#emoney_account_filter_modal").modal("hide");
                $.pjax.reload({container : "#emoney_account_filter_pjax"});
            }
            $(document).on("click", ".emoney_account_filter_modal_link", function(e) {
                e.preventDefault();
                let url = this.href || $(this).data("url");
                console.log(url);
                $.get(url, function (data) {
                    $("#emoney_account_filter_modal .emoney_account_filter_modal_content").html(data);
                });
                $("#emoney_account_filter_modal").modal("show");
            });
            $(document).on("submit", "#emoney_account_filter_modal form", function(e) {
                e.preventDefault();
                var url = $(this).attr("action");
                var method = $(this).attr("method");
                var data = $(this).serialize();

                $.ajax({
                    url : url,
                    type: method,
                    data : data
                }).done(function(data){
                    $("#emoney_account_filter_modal .emoney_account_filter_modal_content").html(data);
                });
            });
            $(document).on("hidden.bs.modal", "#emoney_account_filter_modal", function(e) {
                $("#emoney_account_filter_modal .emoney_account_filter_modal_content").html("");
            });
        ');
    }
}
