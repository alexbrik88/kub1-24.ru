<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.07.2020
 * Time: 22:25
 */

namespace frontend\themes\kub\widgets;

use common\modules\acquiring\models\Acquiring;
use yii\base\Widget;
use yii\bootstrap4\Html;

class AcquiringFilterWidget extends Widget
{
    public $pageTitle;
    public $activeAcquiring;
    /* @var \common\models\Company */
    public $company;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->getContent();
    }

    private function getContent(): string
    {
        return $this->render('acquiringFilterWidget', [
            'currentName' => $this->getList()[$this->activeAcquiring] ?? $this->pageTitle,
            'items' => $this->getItems(),
        ]);
    }

    private function getList(): array
    {
        $items = [];
        /** @var Acquiring $acquiring */
        foreach (Acquiring::find()->andWhere(['company_id' => $this->company->id])->all() as $acquiring) {
            $items[$acquiring->id] = Acquiring::$typeMap[$acquiring->type] . " ({$acquiring->identifier})";
        }

        return $items;
    }

    public function getItems(): array
    {
        $list = $this->getList();
        $items = [];
        if (count($list) > 1) {
            $list = $list + [0 => 'Все'];
        }

        foreach ($list as $key => $name) {
            $items[] = [
                'id' => $key,
                'label' => $name,
                'url' => ['index', 'activeItem' => $key],
                'linkOptions' => [
                    'class' => $this->activeAcquiring == $key ? 'active' : '',
                ],
            ];
        }

        return $items;
    }

}
