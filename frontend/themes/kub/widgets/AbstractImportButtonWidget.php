<?php

namespace frontend\themes\kub\widgets;

use yii\base\InvalidConfigException;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\User;

abstract class AbstractImportButtonWidget extends Widget
{
    /**
     * @inheritDoc
     */
    public static $autoIdPrefix = 'import-button-widget';

    /**
     * @var string
     */
    public $cssClass = 'button-clr button-regular button-hover-transparent pr-3 pl-3';

    /**
     * @var string
     */
    public $content = 'Подключить';

    /**
     * @var bool
     */
    public $hasImport = false;

    /**
     * @var bool
     */
    public $hasBlock = true;

    /**
     * @var string
     */
    public string $tag = 'button';

    /**
     * @var string
     */
    public string $redirectUrl = '';

    /**
     * @var User
     */
    private User $user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->content)) {
            throw new InvalidConfigException();
        }

        if (empty($this->redirectUrl)) {
            $this->redirectUrl = Url::current();
        }
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $options = [
            'id' => $this->getButtonId('connect'),
            'type' => 'button',
            'class' => 'import-dialog',
            'data-url' => $this->getConnectUrl($this->redirectUrl),
        ];

        Html::addCssClass($options, $this->cssClass);

        if ($this->hasAccounts()) {
            if ($this->hasImport) {
                $options['data-url'] = $this->getImportUrl($this->redirectUrl);
                $options['id'] = $this->getButtonId('import');
            } elseif ($this->hasBlock) {
                $options['id'] = $this->getButtonId('disabled');
                ArrayHelper::remove($options, 'data-url');
                Html::removeCssClass($options, 'import-dialog');
                Html::addCssClass($options, 'button-disabled');
            }
        }

        $this->view->registerJs($this->getButtonJs());

        echo Html::tag($this->tag, $this->content, $options);
    }

    /**
     * @param string $action
     * @return string
     */
    protected function getButtonId(string $action): string
    {
        return sprintf('%s-%s', $this->getId(), $action);
    }

    /**
     * @return bool
     */
    abstract protected function hasAccounts(): bool;

    /**
     * @param string $redirectUrl
     * @return string
     */
    abstract protected function getConnectUrl(string $redirectUrl): string;

    /**
     * @param string $redirectUrl
     * @return string
     */
    abstract protected function getImportUrl(string $redirectUrl): string;

    /**
     * @return string
     */
    private function getButtonJs(): string
    {
        $json = json_encode([
            '#' . $this->getButtonId('import') => $this->getImportUrl($this->redirectUrl),
            '#' . $this->getButtonId('connect') => $this->getConnectUrl($this->redirectUrl),
        ]);

        return <<<JS
            (function (w) {
                const actions = JSON.parse('{$json}');
                
                if (actions[w.location.hash] !== undefined) {
                    let url = actions[w.location.hash];
                    
                    w.location.hash = '';
                    w.ImportDialog.loadAjax(url);
                }
            })(window);
JS;
    }
}
