<?php

namespace frontend\themes\kub\widgets;

use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class ConfirmModalWidget
 * @package frontend\widgets
 *
 * @example usage:
 * With target button :
 * <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
 *      'toggleButton' => [
 *          'label' => 'Удалить',
 *          'class' => 'button-regular button-regular_red',
 *      ],
 *      'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить этот элемент?',
 * ]); ?>
 *
 * Without target button:
 * widget:
 * <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
 *      'options' => [
 *          'id' => 'delete-confirm',
 *      ],
 *      'toggleButton' => false,
 *      'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить исходящий счёт?',
 * ]);
 * button:
 * <button type="button" class="button-regular button-regular_red" data-toggle="modal" href="#delete-confirm">Удалить</button>
 *
 */
class ConfirmModalWidget extends Widget
{
    /**
     * @var string
     */
    public $message = 'Are you sure?';
    /**
     * @var string
     */
    public $confirmUrl = '#';

    /** @var array */
    public $confirmOptions = [];
    /**
     * @var array
     */
    public $confirmParams = [];

    /**
     * @var
     */
    public $options;
    /**
     * @var string the modal size. Can be [[SIZE_LARGE]] or [[SIZE_SMALL]], or empty for default.
     */
    public $size;
    /**
     * @var
     */
    public $toggleButton;

    public $theme = 'red'; // red||gray

    /**
     *
     */
    public function init()
    {
        $this->theme = 'gray'; // ALWAYS

        Html::addCssClass($this->options, 'confirm-modal modal');
        Html::addCssClass($this->options, 'fade');

        Modal::begin([
            'id' => sprintf('confirm_%s', md5(Url::to($this->confirmUrl))),
            'options' => $this->options,
            'size' => $this->size,
            'closeButton' => false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'label' => 'Confirm'
            ],
        ]); ?>

        <?php /*

        <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
            'class' => 'modal-close close',
            'data-dismiss' => 'modal',
            'aria-label' => 'Close',
        ]) ?>*/ ?>

        <?php if ($this->theme == 'red'): ?>
        <div style="margin-bottom: 30px;">
            <?= $this->message; ?>
        </div>
        <div class="form-actions row">
            <div class="col">
                <?= Html::a('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', $this->confirmUrl, $this->confirmOptions + [
                        'class' => 'button-regular button-regular_red pull-right mt-ladda-btn ladda-button link-bleak',
                        'style' => 'width: 50px;',
                        'data-style' => 'expand-right',
                        'data' => [
                            'method' => 'post',
                            'params' => array_merge($this->confirmParams, [
                                '_csrf' => Yii::$app->request->csrfToken,
                            ]),
                        ],
                    ]); ?>
            </div>
            <div class="col">
                <?= Html::tag('span', 'НЕТ', [
                    'class' => 'button-regular button-regular_red link-bleak',
                    'style' => 'width: 50px;',
                    'onclick' => new JsExpression('$(this).closest(".modal").modal("hide")')
                ]); ?>
            </div>
        </div>

    <?php else: ?>

        <h4 class="modal-title text-center mb-4"><?= $this->message; ?></h4>
        <div class="text-center">
            <?= Html::a('Да', $this->confirmUrl, $this->confirmOptions + [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 link-bleak',
                    'data' => [
                        'method' => 'post',
                        'params' => array_merge($this->confirmParams, [
                            '_csrf' => Yii::$app->request->csrfToken,
                        ]),
                    ]
                ]); ?>
            <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1 link-bleak" type="button" onclick="$(this).closest('.modal').modal('hide')">Нет</button>
        </div>
    <?php endif; ?>
        <?php Modal::end();
    }
}
