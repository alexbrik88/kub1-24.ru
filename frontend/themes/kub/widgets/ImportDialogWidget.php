<?php

namespace frontend\themes\kub\widgets;

use yii\base\InvalidConfigException;
use yii\bootstrap4\Modal;
use yii\web\View;
use yii\widgets\Pjax;

class ImportDialogWidget extends Modal
{
    /**
     * @inheritDoc
     */
    public $options = ['id' => 'import-dialog-widget'];

    /**
     * @inheritDoc
     */
    public $closeButton = [
        'data-dismiss' => 'false',
        'class' => 'modal-close close',
        'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close" /></svg>',
        'onclick' => <<<JS
                $(this).closest('.modal').first().modal('hide'); return false;
JS
    ];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->view->registerJs($this->getDialogJS(), View::POS_HEAD);
        parent::init();

        Pjax::begin(['id' => 'import-dialog-pjax', 'enablePushState' => false, 'linkSelector' => false]);
        Pjax::end();
    }

    /**
     * @return string
     */
    private function getDialogJS(): string
    {
        return <<<JS
        (function (w) {
            const ImportDialog = {
                loadAjax: function (url) {
                    $.pjax({
                        url: url,
                        container: '#import-dialog-pjax',
                        push: false,
                        scrollTo: false
                    });
                },
            
                showDialog: function () {
                    $("#import-dialog-widget").modal();
                }
            };

            $(document).on('click', '.import-dialog', function(e) {
                e.preventDefault();
                ImportDialog.loadAjax($(this).data('url'));
            });

            $(document).on("pjax:success", "#import-dialog-pjax", function () {
                ImportDialog.showDialog();
            });
            
            w.ImportDialog = ImportDialog;
        })(window);
JS;
    }
}
