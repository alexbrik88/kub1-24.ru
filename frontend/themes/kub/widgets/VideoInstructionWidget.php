<?php
namespace frontend\themes\kub\widgets;

use yii\base\Widget;

class VideoInstructionWidget extends Widget {

    public $title;
    public $titleWidth;
    public $buttonTitle;
    public $src;
    public $text;

    public $width = 560;
    public $height = 315;

    private static $modalNumber;

    public function run() {

        ++self::$modalNumber;

        return $this->render('videoInstructionWidget', [
            'id' => 'modal-instruction' . (self::$modalNumber > 1 ? ('-'.self::$modalNumber) : ''),
            'title' => $this->title,
            'titleWidth' => $this->titleWidth,
            'buttonTitle' => $this->buttonTitle ?: $this->title,
            'src' => $this->src,
            'text' => $this->text,
            'width' => $this->width,
            'height' => $this->height
        ]);
    }

    public static function setModalNumber($number)
    {
        self::$modalNumber = (int)$number;
    }
}