<?php

namespace frontend\themes\kub\widgets;

use yii\base\Widget;

class SummarySelectProjectWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('summary_select_project', ['widget' => $this]);
    }
}
