<?php

namespace frontend\themes\kub\widgets;


use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;
use yii\base\Widget;
use Yii;

/**
 * Class CashBoxFilterWidget
 * @package frontend\widgets
 */
class CashBoxFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $cashbox;
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     * @var
     */
    public $disable = false;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    public function run()
    {
        Yii::$app->view->registerJs('
            $(document).on("submit", "#cashbox-form", function () {
                $this = $(this);
                $modal = $this.closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"cashbox\" value=\"' . $this->cashbox . '\">");
                $action = $this.data("isnewrecord") == 1 ? "/cash/order/create-cashbox" : "/cash/order/update-cashbox?id=" + $this.data("modelid");
                $.post($action, $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        //$("ul#user-bank-dropdown").replaceWith(data.html);
                        if (data.label) {
                            $("span.cashbox-label").text(data.label + " ");
                        }
                        Ladda.stopAll();
                        
                        var block = $("#user-bank-dropdown");
                        var update_block = $(block).find(".form-edit").filter("[data-id="+data.id+"]");
                        
                        if ($(update_block).length) {
                            $(update_block).find("a.goto-cashbox").html(data.name);                        
                        } else {
                            var template = $(block).find(".cash-template").clone();
                            var href_main = $(template).find("a.goto-cashbox").attr("href");
                            var href_update = $(template).find("a.update-cashbox").attr("href");                        
                            $(template).addClass("form-edit form-edit_alternative").removeClass("cash-template").attr("data-id", data.id);
                            $(template).find("a.goto-cashbox").attr("href", href_main.replace("cashbox=0", "cashbox=" + data.id)).html(data.name);
                            $(template).find("a.update-cashbox").attr("href", href_update.replace("id=0", "id=" + data.id));
                            $(".form-edit").last().before(template);
                            $(template).show();                        
                        }  
                        
                    } else {
                        $this.html($(data.html).find("form").html());
                        refreshUniform();
                        refreshDatepicker();
                        createSimpleSelect2("cashbox-accessible");

                        $("#cashbox-createstartbalance").on("change", function (e) {
                            $(".start-balance-block").toggleClass("hidden");
                        });
                    }
                });

                return false;
            });
        ');

        return $this->getCheckboxesList();
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getCheckboxesList()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $cashboxList = $user->getCashboxes()
            ->select(['name', 'id'])
            ->orderBy([
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])->indexBy('id')->column();
        $cashboxItems = [];
        if ($cashboxList) {
            if (count($cashboxList) > 1) {
                $cashboxList = $cashboxList + ['all' => 'Все кассы'];
            }
            foreach ($cashboxList as $key => $name) {
                $cashboxItems[] = [
                    'id' => $key,
                    'label' => $name,
                    'url' => Url::to(['index', 'cashbox' => $key]),
                    'linkOptions' => [
                        'class' => $this->cashbox == $key ? 'active' : '',
                    ],
                ];
            }
        }
        $currentName = isset($cashboxList[$this->cashbox]) ? $cashboxList[$this->cashbox] : $this->pageTitle;

        if ($cashboxItems) {

            return $this->render('cashboxFilterWidget', [
                'currentName' => $currentName,
                'cashboxItems' => $cashboxItems,
                'disable' => $this->disable,
            ]);
        }

        return "<h4>".Html::encode($this->pageTitle)."</h4>";
    }
}