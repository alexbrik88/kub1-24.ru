<?php

namespace frontend\themes\kub\widgets;

use yii\base\Widget;

class SummarySelectWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('summary_select', ['widget' => $this]);
    }
}
