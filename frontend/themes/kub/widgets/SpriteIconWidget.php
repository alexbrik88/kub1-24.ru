<?php

namespace frontend\themes\kub\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\bootstrap4\Html;

class SpriteIconWidget extends Widget
{
    /**
     * @var string
     */
    public $icon;

    /**
     * @var array
     */
    public array $options = ['class' => 'svg-icon'];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->icon)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        return Html::tag('svg', sprintf('<use xlink:href="/img/svg/svgSprite.svg#%s" />', $this->icon), $this->options);
    }
}
