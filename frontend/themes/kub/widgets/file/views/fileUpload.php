<?php

/**
 * Created by Konstantin Timoshenko
 * Date: 21.10.15
 * Time: 18.25
 * Email: t.kanstantsin@gmail.com
 */

use yii\bootstrap\Html;

/* @var \yii\web\View $this */
/* @var \frontend\themes\kub\widgets\file\FileUpload $widget */

$widget = $this->context;
?>

<div class="upload-area"
     data-upload-url="<?= $widget->uploadUrl ?>"
     data-delete-url="<?= $widget->deleteUrl; ?>"
     data-list-url="<?= $widget->listUrl; ?>"
     data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
     data-csrf-token="<?= Yii::$app->request->csrfToken; ?>"
>
    <?php if ($widget->listUnderButton !== true): ?>
        <div id="file-ajax-loading" style="display: none;"><img src="/img/loading.gif"></div>
        <div class="file-list mt-2"></div>
    <?php endif; ?>
    <?= Html::button('<svg class="inputFile-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#download"></use>
                </svg>', [
        'class' => 'upload-button hide inputFile-btn',
    ]); ?>
    <?php if ($widget->listUnderButton === true): ?>
        <div id="file-ajax-loading" style="display: none;"><img src="/img/loading.gif"></div>
        <div class="file-list mt-2"></div>
    <?php endif; ?>
</div>
