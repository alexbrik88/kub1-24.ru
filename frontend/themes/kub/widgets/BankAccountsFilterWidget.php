<?php

namespace frontend\themes\kub\widgets;


use backend\models\Bank;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\bank\BankingParams;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\employee\Employee;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\base\Widget;
use yii\bootstrap4\Dropdown;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class BankAccountsFilterWidget
 * @package frontend\widgets
 */
class BankAccountsFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $rs;
    /**
     * @var
     */
    public $company;

    /**
     * @return null|string
     * @throws \Exception
     */
    public function run()
    {
        $this->registerScript();

        $currentBankName = $this->pageTitle;
        $activeAccount = null;
        $bankItems = [];
        $foreignBankItems = [];

        /* @var $bankArray CheckingAccountant[] */
        $bankArray = $this->company->getCheckingAccountants()
            ->with('currency')
            ->groupBy('rs')
            ->indexBy('rs')
            ->orderBy('type')
            ->all();

        if ($bankArray) {
            foreach ($bankArray as $account) {
                $rsLabel = $account->name;

                $title = "р/с: {$account->rs}";
                if ($account->isClosed) {
                    $title .= ' - закрыт';
                }
                $item = [
                    'currency' => $account->currency->name,
                    'id' => $account->id,
                    'label' => Html::tag('span', $rsLabel, [
                        'class' => 'tooltip_rs',
                        'title' => $title,
                    ]),
                    'url' => ['index', 'rs' => $account->rs],
                    'update_url' => ['/cash/account/bank/update', 'id' => $account->id],
                    'linkOptions' => [
                        'class' => $this->rs == $account->rs ? 'active' : '',
                    ],
                ];

                if ($account->currency_id == Currency::DEFAULT_ID) {
                    $bankItems[] = $item;
                } else {
                    $foreignBankItems[$account->currency->name][] = $item;
                }

                if ($this->rs == $account->rs) {
                    $activeAccount = $account;
                }
            }
            $bankCount = count($bankItems);
            $foreignBankCount = count($foreignBankItems);
            $rsCount = $bankCount + $foreignBankCount;

            if ($rsCount > 0) {
                if ($this->rs == 'all') {
                    $currentBankName = 'Все счета';
                } elseif (in_array($this->rs, Currency::$foreignArray)) {
                    $currentBankName = 'Все счета '.$this->rs;
                } elseif ($account = $bankArray[$this->rs] ?? null) {
                    $rsLabel = $account->currency_id == Currency::DEFAULT_ID ? $account->name :
                        $account->currency->name.' '.$account->name;
                    $title = "р/с: {$account->rs}";
                    if ($account->isClosed) {
                        $title .= ' - закрыт';
                    }
                    $currentBankName = Html::tag('span', $rsLabel, [
                        'class' => 'tooltip_rs',
                        'title' => $title,
                    ]);
                }
                if ($bankCount > 1) {
                    $bankItems[] = [
                        'id' => null,
                        'label' => 'Все счета'.($foreignBankCount > 0 ? ' RUB' : ''),
                        'url' => ['index', 'rs' => 'all'],
                        'linkOptions' => [
                            'class' => $this->rs == 'all' ? 'active' : '',
                        ],
                    ];
                }
            }
        }

        return $this->render('bankAccountsFilterWidget', [
            'currentBankName' => $currentBankName,
            'bankItems' => $bankItems,
            'foreignBankItems' => $foreignBankItems,
            'activeAccount' => $activeAccount
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function registerScript()
    {
        Yii::$app->view->registerJs('
            window.bankAccountFilterReload = function() {
                $("#bank_account_filter_modal").modal("hide");
                $.pjax.reload({container : "#bank_account_filter_pjax"});
            }
            $(document).on("click", ".bank_account_filter_modal_link", function(e) {
                e.preventDefault();
                let url = $(this).data("url");
                console.log(url);
                $.get(url, function (data) {
                    $("#bank_account_filter_modal .bank_account_filter_modal_content").html(data);
                });
                $("#bank_account_filter_modal").modal("show");
            });
            $(document).on("submit", "#bank_account_filter_modal form", function(e) {
                e.preventDefault();
                var url = $(this).attr("action");
                var method = $(this).attr("method");
                var data = $(this).serialize();

                $.ajax({
                    url : url,
                    type: method,
                    data : data
                }).done(function(data){
                    $("#bank_account_filter_modal .bank_account_filter_modal_content").html(data);
                });
            });
            $(document).on("hidden.bs.modal", "#bank_account_filter_modal", function(e) {
                $("#bank_account_filter_modal .bank_account_filter_modal_content").html("");
            });
        ');
    }
}
