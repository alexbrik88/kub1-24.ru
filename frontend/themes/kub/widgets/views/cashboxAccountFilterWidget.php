<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var string $currentName */
/** @var array $allItems */
/** @var bool $canCreate */

?>

<?php yii\widgets\Pjax::begin([
    'id' => 'cashbox_account_filter_pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'class' => 'd-flex',
    ]
]) ?>

<h4>
<div class="dropdown popup-dropdown popup-dropdown_storage">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="cashbox-label"><?=($currentName)?></span>
        <?= Icon::get('shevron') ?>
    </a>
    <div class="dropdown-menu" aria-labelledby="cardProductTitle">
        <div class="popup-dropdown-in" id="user-bank-dropdown">
            <?php foreach ($allItems as $item): ?>
                <div class="form-edit form-edit_alternative <?= $item['isActive'] ? 'active' : ''; ?>" data-id="<?=$item['id'] ?>">
                    <div class="form-edit-result hide show" data-id="form-edit">
                        <div class="weight-400">
                            <?= Html::a($item['label'], $item['url'], [
                                'class' => 'black-link',
                            ]) ?>
                        </div>
                        <?php if ($item['canUpdate']): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <?= Html::a(Icon::get('pencil'), $item['updateUrl'], [
                                    'class' => 'cashbox_account_filter_modal_link',
                                    'title' => 'Редактировать кассу',
                                    'data-title' => 'Редактировать кассу',
                                ]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <hr class="popup-devider">
            <?php if ($canCreate) : ?>
                <div class="popup-content pt-3 pb-3">
                    <?= Html::a(Icon::get('add-icon').'<span class="ml-2">Добавить кассу</span>', [
                        '/cash/account/cashbox/create',
                    ], [
                        'class' => 'cashbox_account_filter_modal_link black-link nowrap',
                        'title' => 'Добавить кассу',
                        'data-title' => 'Добавить кассу',
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</h4>

<?php yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    'id' => 'cashbox_account_filter_modal',
    'closeButton' => false,
]) ?>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
    <div class="cashbox_account_filter_modal_content"></div>
<?php Modal::end(); ?>
