<?php
use backend\models\Bank;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\Banking;

/** @var null|CheckingAccountant $activeAccount */

$company = Yii::$app->user->identity->company;

$route = ['/cash/bank/index'];

$banking = [
    'logo' => '',
    'text' => ''
];

if (!Yii::$app->request->isPjax && $activeAccount instanceof CheckingAccountant && ($bankingClass = Banking::classByBik($activeAccount->bik)) !== null) {

    if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
        $banking['logo'] = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29], [
            'class' => 'mr-1',
        ]);
    }

    if ($activeAccount->autoload_mode_id === null && $bankingClass::$hasAutoload) {
        $route['rs'] = $activeAccount->rs;
        $banking['text'] = Html::a("Подгружать выписку автоматически", [
            "/cash/banking/{$bankingClass::$alias}/default/index",
            'account_id' => $activeAccount->id,
            'p' => Banking::routeEncode($route),
        ], [
            'class' => 'banking-module-open-link',
        ]);
    }

    if ($bankingClass::$hasStatementsEmail && $bankingClass::isEmailActivated($activeAccount)) {
        $banking['text'] = $bankingClass::getStatementsListModalLink($activeAccount, $route);
    }

    else if ($lastStatementUpload = CashBankStatementUpload::find()->where([
        'rs' => $activeAccount->rs,
        'company_id' => $company->id,
        'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
    ])->orderBy(['created_at' => SORT_DESC])->one()
    ) {
        $banking['text'] = Html::tag('span', 'Выписка загружена ' . date_timestamp_set(new \DateTime, $lastStatementUpload->created_at)->format('d.m.Y в H:i'), [
            'class' => 'tooltip3',
            'title' => 'Последняя ' . ($lastStatementUpload->source == CashBankStatementUpload::SOURCE_BANK ? 'выгрузка' : 'автовыгрузка') . ' выписки из Вашего клиент-банка',
        ]);
        $route['rs'] = $activeAccount->rs;
        $banking['text'] .= Html::a($this->render('//svg-sprite', ['ico' => 'repeat']), [
            "/cash/banking/{$bankingClass::$alias}/default/index",
            'account_id' => $activeAccount->id,
            'start_load_urgently' => true,
            'p' => Banking::routeEncode($route),
        ], [
            'class' => 'refresh-banking-statements banking-module-open-link ml-2',
            'style' => 'font-size:22px'
        ]);
    }
}

if ($banking['logo'])
    echo $banking['logo'];
if ($banking['text'])
    echo Html::tag('span', $banking['text'], ['class' => 'ml-3']);