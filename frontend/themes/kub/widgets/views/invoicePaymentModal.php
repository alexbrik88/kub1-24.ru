<?php

use frontend\themes\kub\components\Icon;
use php_rutils\RUtils;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data array */

$data = $data ?? [];

Modal::begin([
    'id' => 'modal-invoice-payment',
    'title' => 'Добавить счет',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ]
]); ?>

    <div class="row blocks">
        <?php foreach ($data as $key => $item) : ?>
            <div class="col-4">
                <div class="one-block">
                    <div class="tariff-name">
                        <?= $item['count'] ?>
                        <?= RUtils::numeral()->choosePlural($item['count'], [
                            'счет', //1
                            'счета', //2
                            'счетов' //5
                        ]); ?>

                        <?= ($key == array_key_last($data) ? ' на месяц <br/>за ' : ' за <br/>') . $item['price']; ?> руб.
                    </div>
                    <div class="tariff-info">
                        Вместе со счетом вы сможете создать акт, накладную и счет-фактуру
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <div class="row mt-4">
        <?php foreach ($data as $item) : ?>
            <div class="col-4">
                <?= Html::beginForm($item['action'], $item['method'], ['target' => '_blank',]) ?>
                    <?= $item['input'] ?>
                    <?= Html::submitButton('Оплатить', [
                        'class' => 'button-regular button-regular_red w-100',
                    ]); ?>
                <?= Html::endForm() ?>
            </div>
        <?php endforeach ?>
    </div>

<?php Modal::end(); ?>
