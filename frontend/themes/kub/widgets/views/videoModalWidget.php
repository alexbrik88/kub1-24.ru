<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/** @var $modalOptiions array */
/** @var $videoUrl string */

if (!array_key_exists('closeButton', $modalOptiions)) {
    $modalOptiions['closeButton'] = [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ];
}
?>

<?php Modal::begin($modalOptiions); ?>
    <div style="height: 440px;">
        <?=  Html::tag('iframe', '', [
            'src' => $videoUrl,
            'width' => '100%',
            'height' => '100%',
            'frameborder' => '0',
            'allow' => 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture',
            'allowfullscreen' => true,
        ]) ?>
    </div>
<?php Modal::end();