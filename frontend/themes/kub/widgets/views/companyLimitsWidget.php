<?php

use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $data array */

if ($mark) {
    $mark = Html::tag('span', '', [
        'class' => 'company-limits-mark',
        'style' => '
            display: inline-block;
            position: absolute;
            top: -3px;
            right: -3px;
            background-color: #e30611;
            width: 12px;
            height: 12px;
            border: 2px solid #fff;
            border-radius: 50% !important;
        ',
    ]);
} else {
    $mark = '';
}
?>

<style type="text/css">
#company-limits-dropdown-menu {
    width: 650px !important;
    right: -115px !important;
}
#company-limits-dropdown-menu:before {
    right: 119px;
}
#company-limits-dropdown-menu:after {
    right: 120px;
}
</style>

<div class="header-others-column column">
    <div class="dropdown dropdown_products">
        <?= Html::button(Icon::get('briefcase', [
            'class' => 'dropdown-btn-icon',
        ]).$mark, [
            'id' => 'dropdownLimits',
            'class' => 'button-clr dropdown-btn',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
            'role' => 'button',
            'style' => 'position: relative; padding-left: 5px;',
        ]) ?>
        <div id="company-limits-dropdown-menu" class="dropdown-popup dropdown-menu" aria-labelledby="dropdownLimits">
            <div class="dropdown-popup-in p-0">
                <div style="width: 650px; padding: 20px 10px 25px; font-size: 16px;">
                    <div style="margin-bottom: 0 20px 20px; text-align: center;">
                        <strong>У вас осталось</strong>
                    </div>
                    <div style="margin: 20px 0 0; position: relative;">
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <?php foreach ($data as $key => $value) : ?>
                                        <td style="
                                            padding: 0 10px 15px;
                                            white-space: nowrap;
                                            <?= $key ? 'border-left: 1px solid #f2f3f7;' : ''; ?>
                                        ">
                                            <span style="margin-right: 10px">
                                                <?= $value['label'] ?>
                                            </span>
                                            <span title="<?= $value['title'] ?>" style="color: #b8c0c6;" title-as-html="1">
                                                <?= Icon::get('question') ?>
                                            </span>
                                        </td>
                                    <?php endforeach ?>
                                </tr>
                                <tr>
                                    <?php foreach ($data as $key => $value) : ?>
                                        <td style="
                                            padding: 0 10px;
                                            white-space: nowrap;
                                            vertical-align: middle;
                                            <?= $key ? 'border-left: 1px solid #f2f3f7;' : ''; ?>
                                        ">
                                            <div style="
                                                display: flex;
                                                align-items: flex-end;
                                                font-size: 24px;
                                            ">
                                                <span style="
                                                    line-height: 1;
                                                    color: #b8c0c6;
                                                    margin-right: 10px;
                                                    padding-bottom: 1px;
                                                ">
                                                    <?= Icon::get($value['icon']) ?>
                                                </span>
                                                <span style="font-size: 28px; line-height: 1; color: #3979b2;">
                                                    <?= $value['left'] > 1000000 ? '&infin;' : $value['left'] ?>
                                                </span>
                                                <?php if (($value['pending'] ?? 0) > 0) : ?>
                                                    <span class="ml-1 text-secondary">+<?= $value['pending'] ?></span>
                                                <?php endif ?>
                                            </div>
                                        </td>
                                    <?php endforeach ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
