<?php

namespace frontend\themes\kub\widgets\views;

use common\modules\import\models\ImportJobData;
use frontend\modules\analytics\modules\marketing\assets\JobStatusAsset;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var ImportJobData|null $importJobData
 * @var string $action
 * @var string $statusUrl
 * @var string $cssClass
 * @var string $logoSrc
 */

JobStatusAsset::register($this);

?>

<?= Html::beginForm($action, 'post', [
    'class' => $cssClass,
    'style' => 'height: 28px; line-height: 28px;',
]) ?>

<?php if ($importJobData): ?>
    <?php if ($logoSrc): ?>
        <?= Html::img($logoSrc, [
            'class' => 'd-flex mr-3',
            'style' => 'height: 28px;',
        ]) ?>
    <?php endif; ?>
    <?php if ($importJobData->getJobStatus() === ImportJobData::STATUS_JOB_PROCESSING): ?>
        <?= Html::tag('span', $importJobData->getJobStatus(), [
            'id' => 'jobStatus',
            'data-url' => $statusUrl,
            'class' => 'd-flex',
            'style' => 'color: red;',
        ]) ?>
    <?php else: ?>
        <?= Html::tag('span', $importJobData->getJobStatus(), ['class' => 'd-flex']) ?>
        <?= Html::a($this->render('//svg-sprite', ['ico' => 'repeat']), '#', [
            'id' => 'quickImportButton',
            'class' => 'd-flex ml-2',
            'style' => 'font-size: 22px;',
        ]) ?>
    <?php endif; ?>
<?php endif; ?>

<?= Html::endForm() ?>

<?php
$this->registerJs(<<<JS
    $(document).on('click', '#quickImportButton', function () {
        $(this).closest('form').first().submit();

        return false;
    });
JS);
?>
