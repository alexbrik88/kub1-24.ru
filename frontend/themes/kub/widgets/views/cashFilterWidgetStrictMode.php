<?php

use common\models\cash\CashFlowsBase;
use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var string $currentName */
/** @var array $items */

?>

<?php Pjax::begin([
    'id' => 'company_cash_account_filter_pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<div class="dropdown popup-dropdown">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown">
        <span class="bank-label">Добавить счет или кассу</span>
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </a>
    <div class="dropdown-menu">
        <div id="user-bank-dropdown" class="popup-dropdown-in">
            <div class="popup-content pt-3 pb-3">
                <?= Html::a(Icon::get('add-icon').'<span class="ml-2">Добавить счет / кассу</span>', '#add_company_account_modal', [
                    'class' => 'black-link link_bold link_font-14 link_decoration_none nowrap button-clr',
                    'data-title' => 'Добавить счет / кассу',
                    'data-toggle' => 'modal',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php Pjax::end() ?>
