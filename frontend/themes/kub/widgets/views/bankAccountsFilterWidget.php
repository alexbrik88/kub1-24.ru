<?php

use frontend\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var string $currentBankName */
/** @var array $bankItems */
/** @var array $foreignBankItems */
/** @var array $banking */

?>

<?php yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip_rs:not(.tooltipstered)',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'position' => 'right',
    ],
]) ?>

<?php yii\widgets\Pjax::begin([
    'id' => 'bank_account_filter_pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'class' => 'd-flex',
    ]
]) ?>

<h4>
    <div class="dropdown popup-dropdown">
        <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="bank-label"><?=($currentBankName)?></span>
            <?= Icon::get('shevron') ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="cardProductTitle">
            <div class="popup-dropdown-in" id="user-bank-dropdown">
                <?php foreach ($bankItems as $bankItem): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <?=Html::a($bankItem['label'], $bankItem['url'], [
                                    'class' => 'black-link',
                                ]) ?>
                            </div>
                            <?php if ($bankItem['id']): ?>
                                <div class="form-edit-btns form-edit-btns_position_right_center">
                                    <?= Html::tag('span', Icon::get('pencil'), [
                                        'class' => 'link bank_account_filter_modal_link',
                                        'title' => 'Обновить',
                                        'data-url' => Url::to($bankItem['update_url']),
                                    ]) ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($foreignBankItems as $currency => $items): ?>
                    <?php foreach ($items as $bankItem): ?>
                        <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                            <div class="form-edit-result hide show" data-id="form-edit">
                                <div class="weight-400">
                                    <?=Html::a($bankItem['label'], $bankItem['url'], [
                                        'class' => 'black-link',
                                    ]) ?>
                                </div>
                                <?php if ($bankItem['id']): ?>
                                    <div class="form-edit-btns form-edit-btns_position_right_center">
                                        <?= Html::tag('span', Icon::get('pencil'), [
                                            'class' => 'link bank_account_filter_modal_link',
                                            'title' => 'Обновить',
                                            'data-url' => Url::to($bankItem['update_url']),
                                        ]) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php if(count($items) > 1) : ?>
                        <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                            <div class="form-edit-result hide show" data-id="form-edit">
                                <div class="weight-400">
                                    <?= Html::a("Все счета $currency", [
                                        'index',
                                        'rs' => $currency
                                    ], [
                                        'class' => 'black-link',
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach; ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3">
                    <?= Html::tag('div', Icon::get('add-icon').'<span class="ml-2">Добавить банковский счет</span>', [
                        'class' => 'link black-link link_bold link_font-14 nowrap bank_account_filter_modal_link',
                        'data-url' => Url::to(['/cash/account/bank/create', 'double' => 1]),
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</h4>

<div class="text-grey d-flex flex-nowrap align-items-center ml-4 mr-4 mb-2">
    <?= $this->render('banking/accountBankingInfo', [
        'activeAccount' => $activeAccount ?? null
    ]) ?>
</div>

<?php yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    'id' => 'bank_account_filter_modal',
    'closeButton' => false,
]) ?>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
    <div class="bank_account_filter_modal_content"></div>
<?php Modal::end(); ?>
