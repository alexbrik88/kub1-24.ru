<?php

namespace frontend\themes\kub\widgets;

use common\modules\cards\models\CardAccount;
use frontend\modules\cards\widgets\ZenmoneyButtonWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var CardAccount|null $account
 * @var array $items
 */

?>
<div class="h4">
    <div class="dropdown popup-dropdown popup-dropdown_storage">
        <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="acquiring-label"><?= Html::encode($this->title) ?></span>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </a>
        <div class="dropdown-menu" aria-labelledby="cardProductTitle">
            <div class="popup-dropdown-in" id="user-bank-dropdown">
                <?php foreach ($items as $item): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $item['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <a href="<?= Url::to($item['url']) ?>" class="goto-cashbox black-link">
                                    <?= $item['label'] ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3">
                    <?= ZenmoneyButtonWidget::widget([
                        'hasBlock' => false,
                        'tag' => 'a',
                        'cssClass' => 'black-link link_bold link_font-14 link_decoration_none nowrap button-clr',
                        'identifier' => ($account && $account->account_type == CardAccount::ACCOUNT_TYPE_ZENMONEY)
                            ? $account->identifier
                            : null,
                        'content' => <<<HTML
                            <svg class="add-button-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                            </svg>
                            <span class="ml-2">Добавить аккаунт</span>
HTML,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
