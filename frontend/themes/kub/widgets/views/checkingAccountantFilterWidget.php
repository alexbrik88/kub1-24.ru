<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $currentBankName */
/** @var array $bankItems */
/** @var array $foreignBankItems */
/** @var array $banking */
?>

<h4>
<div class="dropdown popup-dropdown popup-dropdown_storage">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="bank-label"><?=($currentBankName)?></span>
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </a>
    <div class="dropdown-menu" aria-labelledby="cardProductTitle">
        <div class="popup-dropdown-in" id="user-bank-dropdown">
            <?php foreach ($bankItems as $bankItem): ?>
                <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                    <div class="form-edit-result hide show" data-id="form-edit">
                        <div class="weight-400">
                            <a href="<?= Url::to($bankItem['url']) ?>" class="black-link">
                                <?= $bankItem['label'] ?>
                            </a>
                        </div>
                        <?php if ($bankItem['id']): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <button class="button-clr link" data-toggle="modal" title="Обновить" data-target="#update-company-rs-<?=($bankItem['id'])?>">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php foreach ($foreignBankItems as $currency => $items): ?>
                <?php foreach ($items as $bankItem): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <a href="<?= Url::to($bankItem['url']) ?>" class="black-link">
                                    <?= $bankItem['label'] ?>
                                </a>
                            </div>
                            <?php if ($bankItem['id']): ?>
                                <div class="form-edit-btns form-edit-btns_position_right_center">
                                    <button class="button-clr link" data-toggle="modal" title="Обновить" data-target="#update-company-rs-<?=($bankItem['id'])?>">
                                        <svg class="svg-icon">
                                            <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                        </svg>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php if(count($items) > 1) : ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $bankItem['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <?= Html::a("Все счета $currency", [
                                    'index',
                                    'rs' => $currency
                                ], [
                                    'class' => 'black-link',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach; ?>
            <div class="cash-template" style="display: none!important;" data-id="0">
                <div class="form-edit-result hide show" data-id="form-edit">
                    <div class="weight-400">
                        <a href="<?= Url::to(['/cash/bank/index', 'rs' => 0]) ?>" class="goto-cashbox black-link">
                            Новый
                        </a>
                    </div>
                    <div class="form-edit-btns form-edit-btns_position_right_center">
                        <button class="button-clr link update-cashbox" data-toggle="modal" title="Обновить" data-target="#update-company-rs-0">
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <hr class="popup-devider">
            <div class="popup-content pt-3 pb-3">
                <a class="add-checking-accountant black-link link_bold link_font-14 link_decoration_none nowrap button-clr" href="#">
                    <svg class="add-button-icon svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-2">Добавить расч / счет</span>
                </a>
            </div>
        </div>
    </div>
</div>
</h4>

<div class="text-grey d-flex flex-nowrap align-items-center ml-4 mr-4 mb-2">
    <?php if ($banking['logo']): ?>
        <?= $banking['logo'] ?>
    <?php endif; ?>
    <?php if ($banking['text']): ?>
        <span class="ml-3"><?= $banking['text'] ?></span>
    <?php endif; ?>
</div>