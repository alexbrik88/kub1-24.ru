<?php

use common\models\product\Product;
use yii\helpers\Html;

/* @var $widget frontend\modules\cash\widgets\SummarySelectWidget */

$js = <<<SCRIPT
$('.product_checker').on('change', function(){

    var countChecked = 0;
    var quantity = 0;
    $('.product_checker:checked').each(function(){
        countChecked++;
        quantity += parseFloat($(this).parents('tr').find('.col_quantity').text());
    });
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-quantity').text(number_format(quantity, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

$('#Allcheck').on('click', function() {
        if (!this.checked) {
            $('#summary-container').removeClass('visible check-true');     
        }
});
SCRIPT;

$this->registerJs($js);
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="select-on-check-all" id="Allcheck" type="checkbox" name="count-list-table">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано:
                <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <?php if ($widget->productionType == Product::PRODUCTION_TYPE_GOODS): ?>
            <div class="column total-txt-foot mr-3">Остаток: <strong class="total-quantity">0</strong></div>
        <?php endif; ?>
        <?php /*
        <div class="column total-txt-foot mr-3">Приход: <strong class="total-income">0</strong></div>
        <div class="column total-txt-foot mr-3">Расход: <strong class="total-expense">0</strong></div>
        <div style="display:none!important;" class="column total-txt-foot mr-3">Разница: <strong class="total-difference">0</strong></div>*/ ?>
        <div class="column ml-auto"></div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
