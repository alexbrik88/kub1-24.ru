<?php

use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use common\models\employee\Employee;

/* @var $model yii\db\ActiveRecord */
/* @var $items array */
/* @var $mainTitle string */
/* @var $sortingItemsTitle string */
/* @var $sortingItems array */
/* @var $showItems array */
/* @var $hideItems array */
/* @var $customPriceItems array */
/* @var $buttonClass string */
/* @var $showColorSelector */

if ($showColorSelector) {
    echo yii2tooltipster::widget([
        'options' => [
            'class' => '.tooltip-color-selector',
        ],
        'clientOptions' => [
            'theme' => ['tooltipster-kub'],
            'trigger' => 'hover',
            'contentAsHTML' => true,
            'interactive' => true,
            'position' => 'right',
        ],
    ]);
}

?>
<div class="dropdown dropdown-settings d-inline-block">
    <?= Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>', [
        'class' => ($buttonClass) ?: 'button-list button-hover-transparent button-clr mr-2',
        'data-toggle' => 'dropdown',
        'aria-haspopup' => 'true',
        'aria-expanded' => 'false',
        'title' => 'Настройка таблицы'
    ]) ?>
    <ul id="config_items_box" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
        <li>
            <label style="font-weight: bold;">Настройка таблицы</label>
        </li>
        <?php if (count($items) > 0 || count($customPriceItems) > 0): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                <label style="font-weight: bold;"><?= $mainTitle ?></label>
            </li>
            <?php foreach ($items as $item) : ?>
                <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                    <?php $type = $item['type'] ?? null; ?>
                    <li <?=(isset($item['style'])) ? 'style="'.$item['style'].'"' : '' ?>>
                    <?php if ($type === null): ?>
                        <?= Html::activeCheckbox($model, $item['attribute'], [
                            'disabled' => ($item['disabled'] ?? false),
                            'data-target' => 'col_' . $item['attribute'],
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute'])]
                          + (isset($item['invert_attribute']) ? (['data-target-invert' => 'col_' . $item['invert_attribute']]) : [])
                          + (isset($item['callback-object']) ? ['data-callback-object' => $item['callback-object']] : [])
                          + (isset($item['callback-method']) ? ['data-callback-method' => $item['callback-method']] : [])
                          + (isset($item['refresh-page']) ? ['data-refresh-page' => 1] : [])
                          + (!empty($item['need_pay']) ? ['data-need-pay' => 1, 'data-need-pay-modal' => $item['need_pay_modal']] : [])
                        ) . (isset($item['help']) ? Icon::get('question', ['class' => 'ml-2 tooltip-question-icon', 'title' => $item['help'], 'title-as-html' => 1]) : '') ?>
                    <?php elseif ($type === 'radio'): ?>
                        <?= Html::activeRadio($model, $item['attribute'], [
                                'data-target' => 'col_' . $item['attribute'],
                                'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute'])]
                            + (isset($item['invert_attribute']) ? (['data-target-invert' => 'col_' . $item['invert_attribute']]) : [])
                            + (isset($item['callback-object']) ? ['data-callback-object' => $item['callback-object']] : [])
                            + (isset($item['callback-method']) ? ['data-callback-method' => $item['callback-method']] : [])
                            + (isset($item['refresh-page']) ? ['data-refresh-page' => 1] : [])
                        ) . (isset($item['help']) ? Icon::get('question', ['class' => 'ml-2 label-help', 'title' => $item['help'], 'title-as-html' => 1]) : '') ?>
                    <?php endif; ?>
                    </li>
                <?php elseif (!empty($item['subtitle'])): ?>
                    <li class="bold" style="border-top: 1px solid #ddd;">
                        <label style="font-weight: bold;"><?= $item['subtitle'] ?></label>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
            <?php foreach ($customPriceItems as $item): ?>
                <?php $name = 'custom_price_'.$item['id']; ?>
                <li>
                    <?= Html::checkbox($name, $item['show_column'], [
                        'id' => 'config-' . $name,
                        'data-target' => 'col_' . $name,
                        'data-url' => $item['url'],
                        'label' => $item['label'],
                    ]); ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (!empty($sortingItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                <?= $sortingItemsTitle ?>
            </li>
        <?php endif; ?>
        <?php foreach ($sortingItems as $sortingItem): ?>
            <?php if (!empty($sortingItem['attribute']) && (!array_key_exists('visible', $sortingItem) || $sortingItem['visible'])) : ?>
                <li style="white-space: nowrap;">
                    <?= Html::radio($sortingItem['attribute'], $sortingItem['checked'], [
                        'class' => 'sorting-table-config-item',
                        'id' => $sortingItem['attribute'],
                        'data-target' => 'col_' . $sortingItem['attribute'],
                        'label' => isset($sortingItem['label']) ? $sortingItem['label'] : $sortingItem['attribute'],
                        'data-url' => $sortingItem['data-url'] ?? null
                    ]); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if (!empty($showItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                Показывать
            </li>
            <?php foreach ($showItems as $item) : ?>
                <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                    <li>
                        <?= Html::activeCheckbox($model, $item['attribute'], [
                            'data-refresh-page' => '1',
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        ]); ?>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif; ?>
        <?php if (!empty($hideItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                Не выводить
            </li>
            <?php foreach ($hideItems as $item) : ?>
                <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                    <li>
                        <?= Html::activeCheckbox($model, $item['attribute'], [
                            'data-refresh-page' => '1',
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        ]); ?>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif; ?>
        <?php if ($showColorSelector && Yii::$app->user && ($employee = Yii::$app->user->identity)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                <label class="bold mb-2">
                    Цвет
                    <span class="tooltip-color-selector" data-tooltip-content="#tooltip_color_selector_help">
                        <svg class="tooltip-question-icon svg-icon ml-1"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>
                    </span>
                </label>
                <div id="tooltip_color_selector">
                    <div class="form-filter">
                    <?php for ($i=1; $i<=3; $i++): ?>
                        <input id="clr<?=($i)?>" type="radio" name="color-selector-item" <?=($employee->links_color == $i || (!$employee->links_color && $i == 1) ? 'checked':'') ?> data-color="<?= Employee::$linkColors[$i] ?>" data-color-index="<?=($i)?>" />
                        <label class="pr-3 no-border color-selector-item" data-color="<?= Employee::$linkColors[$i] ?>" data-color-index="<?=($i)?>" style="color:<?= Employee::$linkColors[$i] ?>; margin-top:2px;" for="clr<?=($i)?>">ООО "КУБ24"</label>
                        <br/>
                    <?php endfor; ?>
                    <div id="tooltip_color_selector_style"></div>
                    </div>
                </div>
                <div style="display: none">
                    <div id="tooltip_color_selector_help">
                        Выберите цвет синего, который будет в таблице<br/>
                        у элементов, по которым можно кликнуть
                    </div>
                </div>
            </li>
        <?php endif; ?>
    </ul>
</div>
