<?php

namespace frontend\modules\cards\views;

use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\AbstractAutoImportForm;
use common\modules\import\models\AbstractImportForm2;
use common\modules\import\models\AutoImportSettings;
use common\modules\import\models\ImportJobData;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use common\components\date\DateHelper;
use Yii;
use yii\bootstrap\Collapse;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var AbstractImportForm2 $form
 * @var ImportJobData $lastAutoJobs
 * @var AbstractAutoImportForm $autoImportForm
 * @var string $disconnectUrl
 * @var string $identifier Получен из GET-параметра запроса
 * @var string $headerContent
 * @var bool $disableDateTo
 */

$autoImportForm = $form->createAutoImportForm();
$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';
$arrowIco = '<svg class="arrow-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#arrow"></use></svg>';

?>

<?php $this->beginBlock('autoImportForm', false) ?>

<?= Html::radioList(
    Html::getInputName($autoImportForm, 'period_type'),
    $autoImportForm->getAutoImportSettings()->period_type,
    AutoImportSettings::PERIOD_TYPE_MAP,
    ['id' => 'autoload-mode-selector']
) ?>

<?= Html::button('Сохранить', [
    'class' => 'btn yellow save-autoload',
    'style' => 'margin-right: 10px; margin-bottom: 15px;',
    'data-url' => Url::to(['set-auto-import', 'identifier' => $form->getIdentifier()]),
]);
?>

<span id="autoload_save_report">
    Настройки сохранены
</span>

<?php $this->endBlock() ?>

<div class="statement-service-content" style="position: relative; min-height: 110px;">
<?= $headerContent ?>

<?php $widget = ActiveForm::begin([
    'id' => 'import-dialog-form',
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-pjax' => true,
    ],
]) ?>

<div class="row">
    <?= $widget->field($form, 'identifier', [
        'options' => [
            'class' => 'form-group col-12',
        ],
    ])->widget(Select2::class, [
        'data' => $form->identifierList,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'width' => '100%',
            'disabled' => (strlen($identifier) > 0),
        ],
    ]) ?>
</div>

<div class="row">
    <?= $widget->field($form, 'dateFrom', [
        'options' => [
            'class' => 'form-group col-6',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter date-picker-wrap',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
    ])->textInput([
        'class' => 'form-control date-picker',
    ]); ?>

    <?= $widget->field($form, 'dateTo', [
        'options' => [
            'class' => 'form-group col-6',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter date-picker-wrap',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
    ])->textInput([
        'class' => 'form-control date-picker',
        'disabled' => $disableDateTo,
    ]); ?>
</div>

<div class="d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Отправить запрос</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
        'style' => 'width: 170px;',
    ]); ?>
    <?= Html::button('Отмена', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal',
        'title' => 'Отмена',
        'style' => 'width: 150px;',
    ]); ?>
</div>
<?php ActiveForm::end() ?>

<div class="row">
    <div class="col-5">
        <?= Collapse::widget([
            'id' => 'autoload-form-container',
            'encodeLabels' => false,
            'options' => [
                'class' => 'autoload-form-collapse',
            ],
            'items' => [
                [
                    'label' => "<span>Автоматически выгружать операции</span> {$arrowIco}",
                    'content' => $this->blocks['autoImportForm'],
                ],
            ]
        ]); ?>
    </div>

    <div id="statement-history" class="col-7 panel-collapse collapse">
        <label>Последние автозагрузки</label>
        <table class="table table-style table-count-list last-auto-operations-table">
            <thead>
            <tr>
                <th style="min-width: 120px;">Период</th>
                <th style="min-width: 120px;">Дата загрузки</th>
                <th style="min-width: 90px;">Загружено операций</th>
            </tr>
            </thead>
            <tbody>
            <?php if (empty($lastAutoJobs)): ?>
                <tr>
                    <td colspan="3">
                        Автозагрузки не найдены
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($lastAutoJobs as $jobData): $helper = new ImportParamsHelper($jobData) ?>
                    <tr>
                        <td>
                            <?php if ($helper->hasDateFrom() && $helper->hasDateTo()): ?>
                            <?= sprintf(
                                '%s-%s',
                                $helper->getDateFrom()->format(DateHelper::FORMAT_USER_DATE),
                                $helper->getDateTo()->format(DateHelper::FORMAT_USER_DATE)
                            ) ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?= $jobData->getCreatedAt()->format(DateHelper::FORMAT_USER_DATE) ?>
                        </td>
                        <td>
                            <?= $jobData->count ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

</div>

<?php if (strlen($identifier) > 0): ?>
<?= ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => [
        'tag' => 'a',
        'label' => 'Отключить интеграцию',
        'class' => 'link'
    ],
    'confirmUrl' => $disconnectUrl,
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите отключить интеграцию?',
]) ?>
<?php endif; ?>
