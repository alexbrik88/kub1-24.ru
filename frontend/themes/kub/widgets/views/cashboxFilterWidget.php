<?php

use yii\helpers\Url;

/** @var string $currentName */
/** @var array $cashboxItems */
/** @var array $banking */
/** @var boolean $disable */

?>

<h4>
<div class="dropdown popup-dropdown popup-dropdown_storage">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="cashbox-label"><?=($currentName)?></span>
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </a>
    <div class="dropdown-menu" aria-labelledby="cardProductTitle">
        <div class="popup-dropdown-in" id="user-bank-dropdown">
            <?php foreach ($cashboxItems as $cashboxItem): ?>
                <div class="form-edit form-edit_alternative" data-id="<?=$cashboxItem['id'] ?>">
                    <div class="form-edit-result hide show" data-id="form-edit">
                        <div class="weight-400">
                            <a href="<?= Url::to($cashboxItem['url']) ?>" class="goto-cashbox black-link">
                                <?= $cashboxItem['label'] ?>
                            </a>
                        </div>
                        <?php if (!$disable && $cashboxItem['id'] > 0): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <a class="update-cashbox ajax-modal-btn button-clr link" title="Редактировать кассу" data-title="Редактировать кассу" href="<?= Url::to(['/cashbox/update', 'id' => $cashboxItem['id']]) ?>">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="cash-template" style="display: none!important;" data-id="0">
                <div class="form-edit-result hide show" data-id="form-edit">
                    <div class="weight-400">
                        <a href="<?= Url::to(['/cash/order/index', 'cashbox' => 0]) ?>" class="goto-cashbox black-link">
                            Новый
                        </a>
                    </div>
                    <div class="form-edit-btns form-edit-btns_position_right_center">
                        <?php if (!$disable) : ?>
                        <a class="update-cashbox ajax-modal-btn button-clr link" title="Редактировать e-money" data-title="Редактировать кассу" href="<?= Url::to(['/cashbox/update', 'id' => 0]) ?>">
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                            </svg>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <hr class="popup-devider">
            <?php if (!$disable): ?>
            <div class="popup-content pt-3 pb-3">
                <a class="add-cashbox ajax-modal-btn black-link link_bold link_font-14 link_decoration_none nowrap button-clr" title="Добавить кассу" data-title="Добавить кассу"  href="<?= Url::to(['/cashbox/create']) ?>" data-url="<?= Url::to(['/cashbox/create']) ?>">
                    <svg class="add-button-icon svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-2">Добавить кассу</span>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</h4>

