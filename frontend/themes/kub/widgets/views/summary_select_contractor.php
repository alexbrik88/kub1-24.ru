<?php

use yii\helpers\Html;

/* @var $widget frontend\modules\cash\widgets\SummarySelectWidget */

$js = <<<SCRIPT
$(document).on('change', '.joint-operation-checkbox', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    $('.joint-operation-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
        if (countChecked == 1) {
            $('#summary-container .one-line-button').removeClass('hidden');
        } else {
            $('#summary-container .one-line-button').addClass('hidden');
        }
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});
SCRIPT;

$this->registerJs($js);
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table">
            <span class="checkbox-txt total-txt-foot">
                Выбрано:
                <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <?php /*
        <div class="column total-txt-foot mr-3">Приход: <strong class="total-income">0</strong></div>
        <div class="column total-txt-foot mr-3">Расход: <strong class="total-expense">0</strong></div>
        <div style="display:none!important;" class="column total-txt-foot mr-3">Разница: <strong class="total-difference">0</strong></div>*/ ?>
        <div class="column ml-auto"></div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
