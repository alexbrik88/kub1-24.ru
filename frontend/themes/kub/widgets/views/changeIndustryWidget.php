<?php

use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modalId string */
/* @var $modalTitle string */
/* @var $formAction string */
/* @var $formTitle string */
/* @var $inputName string */
/* @var $inputValue string */
/* @var $toggleButton array */
/* @var $data array */

?>

<?php Modal::begin([
    'id' => $modalId,
    'title' => $modalTitle ?: 'Изменить направление',
    'toggleButton' => $toggleButton,
    'titleOptions' => [
        'class' => 'text-left mb-4'
    ],
    'closeButton' => [
        'label' => frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>

    <?php $form = ActiveForm::begin([
        'id' => $modalId.'_form',
        'action' => $formAction,
    ]); ?>

    <div class="hidden additional_inputs"></div>

    <?php if ($formTitle) : ?>
        <div class="form-group">
            <?= $formTitle ?>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных документов изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label class="label">
                Направление
            </label>
            <?= Select2::widget([
                'name' => $inputName,
                'value' => $inputValue,
                'data' => $data,
                'options' => [
                    'multiple' => false,
                    'class' => 'form-control',
                    'placeholder' => empty($inputValue) ? '' : null,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ],
            ]); ?>
        </div>
    </div>
    <br>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-width ladda-button',
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php Modal::end() ?>
