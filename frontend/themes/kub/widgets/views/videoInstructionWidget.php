<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;

/**
 * @var $width
 * @var $height
 * @var $buttonTitle
 * @var $title
 * @var $titleWidth
 */

Modal::begin([
    'id' => $id,
    'closeButton' => false,
]) ?>

<button type="button" class="modal-close close" aria-label="Close" onclick="$(this).closest('.modal').modal('hide')">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php if (!empty($title)): ?>
    <h4 class="modal-title" style="<?= ($titleWidth) ? "width:{$titleWidth}":"" ?>">
        <?= $title ?>
    </h4>
<?php endif; ?>

<?php if (!empty($src)): ?>
    <?php if (strpos($src, 'youtube')): ?>
        <iframe
            <?=($width) ? "width=\"{$width}\"":"" ?>
            <?=($height) ? "height=\"{$height}\"":""?> src="<?= $src ?>"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen="allowfullscreen">
        </iframe>
    <?php else: ?>
        <video controls
            <?=($width) ? "width=\"{$width}\"":"" ?>
            <?=($height) ? "height=\"{$height}\"":""?> src="<?= $src ?>">
            <source src="<?= $src ?>" type="video/mp4">
        </video>
    <?php endif; ?>
<?php endif; ?>

<?php if (!empty($text)): ?>
    <div class="mb-1"><?= $text ?></div>
<?php endif; ?>

<?php Modal::end(); ?>

<?= Html::button($this->render('//svg-sprite', ['ico' => 'video-instruction-2']), [
    'class' => "button-list button-hover-transparent button-clr mb-2 collapsed",
    'title' => $buttonTitle ?: $title,
    'data-toggle' => 'modal',
    'data-target' => '#' . $id,
]);
