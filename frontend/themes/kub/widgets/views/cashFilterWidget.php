<?php

use common\models\company\CheckingAccountant;
use common\models\cash\CashFlowsBase;
use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var string $currentName */
/** @var array $items */
/** @var null|CheckingAccountant $bankingInfoCheckingAccount */
?>

<?php Pjax::begin([
    'id' => 'company_cash_account_filter_pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<div class="d-flex">
    <div class="dropdown popup-dropdown nowrap" style="z-index: 1002">
        <a class="link link_title" href="#" role="button" data-toggle="dropdown">
            <span class="bank-label"><?=($currentName)?></span>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </a>
        <div class="dropdown-menu">
            <div id="user-bank-dropdown" class="popup-dropdown-in">
                <?php foreach ($items as $item): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $item['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <a href="<?= Url::to($item['url']) ?>" class="black-link">
                                    <?= $item['label'] ?>
                                </a>
                            </div>
                            <?php if ($item['id']): ?>
                                <?php if ($item['walletType'] == CashFlowsBase::WALLET_BANK): ?>
                                    <div class="form-edit-btns form-edit-btns_position_right_center">
                                        <button class="button-clr link" data-toggle="modal" title="Обновить" data-target="#update-company-rs-<?=($item['id'])?>">
                                            <svg class="svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                            </svg>
                                        </button>
                                    </div>
                                <?php elseif ($item['walletType'] == CashFlowsBase::WALLET_CASHBOX): ?>
                                    <div class="form-edit-btns form-edit-btns_position_right_center">
                                        <a class="update-cashbox ajax-modal-btn button-clr link" title="Обновить" data-title="Редактировать Кассу" href="<?= Url::to(['/cashbox/update', 'id' => $item['id']]) ?>">
                                            <svg class="svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                            </svg>
                                        </a>
                                    </div>
                                <?php elseif ($item['walletType'] == CashFlowsBase::WALLET_EMONEY): ?>
                                    <div class="form-edit-btns form-edit-btns_position_right_center">
                                        <a class="update-cashbox ajax-modal-btn button-clr link" title="Обновить" data-title="Редактировать E-money" href="<?= Url::to(['/emoney/update', 'id' => $item['id']]) ?>">
                                            <svg class="svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                            </svg>
                                        </a>
                                    </div>
                                <?php endif; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3">
                    <?= Html::a(Icon::get('add-icon').'<span class="ml-2">Добавить счет / кассу</span>', '#add_company_account_modal', [
                        'class' => 'black-link link_bold link_font-14 link_decoration_none nowrap button-clr',
                        'data-title' => 'Добавить счет / кассу',
                        'data-toggle' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="text-grey d-flex flex-nowrap align-items-center ml-4 mr-4 nowrap" style="margin-top: 3px;">
        <?= $this->render('banking/accountBankingInfo', [
            'activeAccount' => $bankingInfoCheckingAccount ?? null
        ]) ?>
    </div>
</div>
<?php Pjax::end() ?>
