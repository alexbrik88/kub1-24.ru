<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/** @var $notification \common\models\product\PriceListNotification */

if ($notification->contractor) {
    $viewer = Html::a($notification->contractor->getShortName(),
            ['/contractor/view', 'type' => 2, 'id' => $notification->contractor_id]) . ' ' . ' ' . $notification->contractor->getRealContactName();
    $viewerPhone = $notification->contractor->getRealContactPhone();
    if ($viewerPhone)
        $viewerPhone .= '<br/>Позвоните клиенту, возможно у него есть вопросы.';
    $viewerEmail = $notification->contractor->getRealContactEmail()
        . '<br/>Напишите клиенту, возможно у него есть вопросы.';
} else {
    $viewer = 'клиент';
    $viewerPhone = '';
    $viewerEmail = $notification->contractor_email
        . '<br/>Напишите клиенту, возможно у него есть вопросы.';
}
?>

<?php
Modal::begin([
    'options' => [
        'class' => 'confirm-modal modal-price-list-notification',
        'data-notification-id' => $notification->id
    ],
    'closeButton' => false,
    'toggleButton' => false
]); ?>

    <h4 class="modal-title text-center mb-4" style="font-size: 18px;">
        Ваш прайс-лист
        <b><?= Html::a($notification->priceList->name, ['/price-list/view-card', 'id' => $notification->priceList->id], ['target' => '_blank']) ?></b>
        сейчас смотрит <br/>
        <?= $viewer ?>. <br/>
        <?= $viewerPhone ? ('Его телефон: ' . $viewerPhone) : ('Его e-mail: ' . $viewerEmail) ?>
    </h4>
    <div class="text-center">
        <button class="button-clr button-regular button-hover-transparent button-width-medium" type="button" onclick="$(this).closest('.modal').modal('hide')">Ок</button>
    </div>

<?php Modal::end();