<?php

use common\models\EmployeeCompany;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\EmployeeCompany */
/* @var $data array */
/* @var $modalId string */
/* @var $modalTitle string */
/* @var $modalText string */

?>

<?php Modal::begin([
    'id' => $modalId,
    'title' => $modalTitle,
    'toggleButton' => false,
    'titleOptions' => [
        'class' => 'text-left mb-4'
    ],
    'closeButton' => false,
]) ?>

    <div class="row">
        <div class="col-12 mb-3">
            <strong>
                <?= $modalText ?>
            </strong>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'new_responsible_employee_id')->label('Ответственный')->widget(Select2::class, [
                'data' => $data,
                'hideSearch' => true,
                'options' => [
                    'prompt' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-width ladda-button',
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php Modal::end() ?>
