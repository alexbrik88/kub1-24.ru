<?php

use yii\helpers\Html;

/* @var $widget frontend\modules\cash\widgets\SummarySelectWidget */

$js = <<<SCRIPT
$(document).on('change', '.joint-operation-checkbox', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var profitSum = 0;
    var revenueSum = 0;

    $('.joint-operation-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
        profitSum += parseFloat($(this).data('profit'));
        revenueSum += parseFloat($(this).data('revenue'));
    });

    if (countChecked > 0) {
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        $('#summary-container .total-profit').text(number_format(profitSum, 2, ',', ' '));
        $('#summary-container .total-profitability').text(number_format(profitSum / (revenueSum || 9E99) * 100, 2, ',', ' '));
        $('#summary-container .one-line-button').toggleClass('hidden', countChecked != 1);
        $('#summary-container .rentabelnost').toggleClass('hidden', countChecked == 1);
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});
SCRIPT;

$this->registerJs($js);
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
        <span class="checkbox-txt total-txt-foot">
            Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
        </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Приход: <strong class="total-income ml-1">0</strong>
        </div>
        <div class="column column-expense total-txt-foot mr-3">
            Расход: <strong class="total-expense ml-1">0</strong>
        </div>
        <div class="column total-txt-foot mr-3">
            Прибыль: <strong class="total-profit ml-1">0</strong>
        </div>
        <div class="column total-txt-foot mr-3 rentabelnost hidden">
            Рент-ть: <strong class="total-profitability ml-1">0</strong>
        </div>
        <div class="column ml-auto"></div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
