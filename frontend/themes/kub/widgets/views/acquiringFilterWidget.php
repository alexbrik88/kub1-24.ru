<?php

use frontend\modules\acquiring\widgets\MonetaButtonWidget;
use frontend\modules\acquiring\widgets\YookassaButtonWidget;
use yii\helpers\Url;
use yii\bootstrap4\Html;

/* @var string $currentName
 * @var array $items
 */
?>
<h4>
    <div class="dropdown popup-dropdown popup-dropdown_storage">
        <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="acquiring-label"><?=($currentName)?></span>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </a>
        <div class="dropdown-menu" aria-labelledby="cardProductTitle">
            <div class="popup-dropdown-in" id="user-bank-dropdown">
                <?php foreach ($items as $item): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $item['id'] ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <a href="<?= Url::to($item['url']) ?>" class="goto-cashbox black-link">
                                    <?= $item['label'] ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3">
                    <a class="black-link link_bold link_font-14 link_decoration_none nowrap button-clr"
                       title="Добавить эквайринг" data-title="Добавить эквайринг"
                       data-toggle="modal" data-target="#add-acquiring" style="cursor: pointer;">
                        <svg class="add-button-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span class="ml-2">Добавить Эквайринг</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</h4>
<div class="modal fade" id="add-acquiring" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Импорт операций по интернет-эквайрингу</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" style="margin-top: 25px;">
                <div class="row">
                    <div class="col-6 col-xl-3 acquiring-button-block">
                        <?= YookassaButtonWidget::widget([
                            'cssClass' => 'button-clr button-width button-regular button-hover-transparent',
                            'content' => Html::img('/img/yookassa/integration.png'),
                            'hasImport' => false,
                            'hasBlock' => false,
                        ]) ?>
                    </div>
                    <div class="col-6 col-xl-3 acquiring-button-block">
                        <?= MonetaButtonWidget::widget([
                            'cssClass' => 'button-clr button-width button-regular button-hover-transparent',
                            'content' => Html::img('/images/acquiring/moneta_logo_acquiring.png'),
                            'hasImport' => false,
                            'hasBlock' => false,
                        ]) ?>
                    </div>
                    <div class="col-6 col-xl-3 acquiring-button-block">
                        <?= Html::button('<img src="/images/acquiring/robokassa_logo.png" style="height: 34px;">', [
                            'class' => 'button-clr button-width button-regular button-hover-transparent disabled',
                            'style' => 'padding-top: 10px;padding-bottom: 10px;',
                            'title' => 'В работе. Скоро подключим.',
                        ]) ?>
                    </div>
                    <div class="col-6 col-xl-3 acquiring-button-block">
                        <?= Html::button('<img src="/images/acquiring/tinkoff_logo.png">', [
                            'class' => 'button-clr button-width button-regular button-hover-transparent disabled',
                            'title' => 'В работе. Скоро подключим.',
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 acquiring-description">
                        Загрузите операции по продажам на Вашем сайте.<br>
                        Данные загрузятся  за прошлые периоды.<br>
                        Ежедневно будут подгружаться операции за прошедший день
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
