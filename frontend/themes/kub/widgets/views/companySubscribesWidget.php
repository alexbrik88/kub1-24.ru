<?php

use frontend\components\Icon;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $data array */

if ($mark) {
    $mark = Html::tag('span', '', [
        'class' => 'company-subscribes-mark',
        'style' => '
            display: inline-block;
            position: absolute;
            top: -3px;
            right: -3px;
            background-color: #e30611;
            width: 12px;
            height: 12px;
            border: 2px solid #fff;
            border-radius: 50% !important;
        ',
    ]);
} else {
    $mark = '';
}
?>

<style type="text/css">
#company-subscribes-dropdown-menu {
    width: 490px !important;
    right: -90px !important;
}
#company-subscribes-dropdown-menu:before {
    right: 94px;
}
#company-subscribes-dropdown-menu:after {
    right: 91px;
}
#company-subscribes-dropdown-menu table td {
    padding-bottom: 12px;
}
.notif-marker_blue {
    border: 2px solid rgba(70, 121, 174, 0.9);
    border-radius: 4px;
}
.notif-marker_blue .svg-icon {
    color:#4679AE;
}
</style>

<div class="header-others-column column">
    <div class="dropdown dropdown_products">
        <?= Html::button(Icon::get('purse', [
            'class' => 'dropdown-btn-icon',
        ]).$mark, [
            'id' => 'dropdownLimits',
            'class' => 'button-clr dropdown-btn',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
            'role' => 'button',
            'style' => 'position: relative; padding-left: 5px;',
        ]) ?>
        <div id="company-subscribes-dropdown-menu" class="dropdown-popup dropdown-menu" aria-labelledby="dropdownLimits">
            <div class="dropdown-popup-in p-0">
                <div style="width: 100%; padding: 20px 12px 20px 12px; font-size: 14px;">
                    <table style="width: 100%">
                        <?php foreach ($data as $d): ?>
                        <tr>
                            <td width="56px">
                                <span class="notif-marker notif-marker_blue">
                                    <?= $this->render('//svg-sprite', ['ico' => $d['icon']]); ?>
                                </span>
                            </td>
                            <td>
                                <div><strong>Тариф "<?= $d['name'] ?>"</strong></div>
                                <div>активен до <?= $d['expired_date'] ?>. <span style="<?= $d['mark_row'] ? 'color:#e30611':'' ?>">Осталось <?= RUtils::numeral()->getPlural($d['days_left'], ['день', 'дня', 'дней']); ?></span></div>
                            </td>
                            <td width="134px">
                                <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span class="pl-1">Продлить</span>', ['/subscribe'], [
                                    'class' => 'button-regular w-100 button-hover-content-red',
                                ]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td colspan="2" class="pt-2 pb-0">
                                <?= Html::a('Перейти на страницу оплаты КУБ24', ['/subscribe'], [
                                    'style' => 'font-size:16px'
                                ]) ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
