<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\Html;

/* @var \yii\web\View $this */
/* @var string $inputId */
/* @var integer $productionType */
/* @var bool $addDeleteModal */
?>

<?php if (Yii::$app->user->can(UserRole::ROLE_PRODUCT_ADMIN)) : ?>

<div class="add-product-block" style="display: none;">
    <span class="add-new-product-group-form"
        style="display: inline-block; position: relative; width: 100%; padding: 15px 52px 15px 15px; border-top: 2px solid #e2e5eb; z-index: 999999">

        <button class="add-button button-clr new-product-group-submit" type="button"
                style="position: absolute; right: 12px; top: 24px; padding: 0 7px;">
            <svg class="add-button-icon svg-icon" style="font-size: 22px;">
                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
            </svg>
        </button>

        <?= Html::textInput('title', null, [
            'class' => 'form-control new_product_group_input',
            'maxlength' => 45,
            'placeholder' => 'Добавить новую группу',
            'style' => 'display: inline-block; width: 100%;',
        ]); ?>
        <?= Html::hiddenInput('productionType', $productionType); ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().children('.add-new-product-group-form').length) {
            var $newProductForm = $('select#<?= $inputId; ?>').siblings('.add-product-block').find('.add-new-product-group-form').clone();
            $newProductForm.find('.new-product-group-submit').attr('id', 'new-product-group-submit-<?= $inputId ?>');
            $newProductForm.clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
        $('#add-new').addClass('no-overflow-modal'); // fix bug - otherwise input not focused
    });
    $('#<?= $inputId ?>').on('select2:close', function (evt) {
        $('#add-new').removeClass('no-overflow-modal'); // fix bug - otherwise input not focused
    });
    $(document).on('click', '#new-product-group-submit-<?= $inputId ?>', function() {
        var input = $(this).parent().children('input');
        $.post('/product/add-group', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                input.val('');
                var newOption = new Option(data.itemName, data.itemId, false, true);
                newOption.dataset.editable = 1;
                newOption.dataset.deletable = 1;
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
            }
        });
    });
</script>

<?php if ($addDeleteModal): ?>
    <div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить группу товара
                </h4>
                <div class="text-center">
                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 js-item-delete ladda-button',
                        'data-style' => 'expand-right',
                    ]) ?>
                    <?= Html::button('Нет', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    $('.js-item-delete, .js-item-delete-cancel').on('click', function() {
        $(this).closest('.modal').modal('hide');
    });
</script>

<?php endif ?>
