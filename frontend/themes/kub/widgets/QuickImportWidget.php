<?php

namespace frontend\themes\kub\widgets;

use common\modules\import\models\ImportJobData;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Widget;

class QuickImportWidget extends Widget
{
    /**
     * @var ImportJobData|null
     */
    public ?ImportJobData $importJobData = null;

    /**
     * @var string
     */
    public string $action = '';

    /**
     * @var string
     */
    public string $statusUrl = '';

    /**
     * @var string
     */
    public string $logoSrc = '';

    /**
     * @var string
     */
    public string $cssClass = 'text-grey ml-4 mr-4 mb-2 d-flex align-items-center';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->action) || empty($this->statusUrl)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        echo $this->render('quickImportWidget', [
            'importJobData' => $this->importJobData,
            'action' => $this->action,
            'statusUrl' => $this->statusUrl,
            'cssClass' => $this->cssClass,
            'logoSrc' => $this->logoSrc,
        ]);
    }
}
