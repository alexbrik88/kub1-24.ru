<?php

namespace frontend\themes\kub\widgets;

use common\models\employee\Employee;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardAccountList;
use common\modules\cards\models\CardAccountRepository;
use frontend\components\WebUser;
use yii\base\Widget;
use yii\web\User;

class CardAccountWidget extends Widget
{
    /**
     * @var CardAccountList
     */
    private CardAccountList $accountList;

    /**
     * @var Employee|null
     */
    private ?Employee $employee;

    /**
     * @var int|null
     */
    private ?int $lastAccountId;

    /**
     * @var CardAccount|null
     */
    private ?CardAccount $account = null;

    /**
     * @param User|WebUser $user
     * @param array $config
     */
    public function __construct(User $user, array $config = [])
    {
        $this->employee = $user->identity;
        $this->accountList = new CardAccountList($this->employee->company);
        $this->lastAccountId = (int) $this->employee->currentEmployeeCompany->last_card_account_id;

        if ($this->lastAccountId) {
            $this->account = CardAccountRepository::findAccountById($this->lastAccountId, $this->employee->company);
        }

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $this->view->title = $this->accountList->getItems()[$this->lastAccountId] ?? $this->view->title;

        echo $this->render('cardAccountWidget', [
            'title' => $this->view->title,
            'items' => $this->buildItems(),
            'account' => $this->account,
        ]);
    }

    /**
     * @return array
     */
    private function buildItems(): array
    {
        $list = $this->accountList->getItems();
        $items = [];

        if (count($list) > 1) {
            $list = $list + ['' => 'Все'];
        }

        foreach ($list as $accountId => $label) {
            $items[] = [
                'id' => $accountId,
                'label' => $label,
                'url' => ['index', 'account_id' => $accountId],
                'linkOptions' => [
                    'class' => ($this->lastAccountId == $accountId) ? 'active' : '',
                ],
            ];
        }

        return $items;
    }
}
