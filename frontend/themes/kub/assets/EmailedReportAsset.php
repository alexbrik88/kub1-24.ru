<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;
/***
 * DocSendAsset
 */
class EmailedReportAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];
    /**
     * @var array
     */
    public $js = [
        'js/emailed-report.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'common\assets\SimpleAjaxUploaderAsset'
    ];
}
