<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;

class RowSelectAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @inheritDoc
     */
    public $js = [
        'js/row-select.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
