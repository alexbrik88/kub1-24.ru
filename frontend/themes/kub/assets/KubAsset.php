<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * KubAsset
 */
class KubAsset extends AssetBundle
{
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapThemeAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
    }

    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/vendorCss.css',
        'css/style.css',
        'css/vidimus.css',
        'css/document-file-scan.css',
        'custom.css',
        'style_theme_kub_redesign.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'js/vendor.min.js',
        'js/main.js',
        'js/custom.js',
        'js/export.js',
        'js/bootstrap-tabdrop.js',
        'custom2.js',
        'script_theme_kub_redesign.js',
        ['expenditure-item-widget.js', 'position' => View::POS_HEAD],
        ['product-group-widget.js', 'position' => View::POS_HEAD],
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\kub\assets\KubCommonAsset',
        'frontend\themes\kub\assets\FrontendAsset'
    ];
}
