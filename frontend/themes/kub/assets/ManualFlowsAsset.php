<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;

/**
 * ManualFlowsAsset
 */
class ManualFlowsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $js = [
        'js/has_manual_flows.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];
}
