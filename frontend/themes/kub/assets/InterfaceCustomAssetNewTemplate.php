<?php


namespace frontend\themes\kub\assets;
use frontend\assets\InterfaceCustomAsset;

class InterfaceCustomAssetNewTemplate extends InterfaceCustomAsset
{

    public $css = [
        'css/interface-custom-kub.css'
    ];

}