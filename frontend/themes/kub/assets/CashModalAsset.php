<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;

/**
 * DocSendAsset
 */
class CashModalAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $css = [];

    /**
     * @var array
     */
    public $js = [
        'js/cash_modal.js',
        'js/cash_modal_many_create.js',
        'js/cash_modal_update_pieces.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
