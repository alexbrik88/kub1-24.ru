<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;

/**
 * LinkFlowToInvoiceAsset
 */
class LinkFlowToInvoiceAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $js = [
        'js/flow_to_invoice.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];
}
