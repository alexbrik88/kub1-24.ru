// polyfills

if (window.NodeList && !NodeList.prototype.forEach) {
	NodeList.prototype.forEach = function (callback, thisArg) {
			thisArg = thisArg || window;
			for (var i = 0; i < this.length; i++) {
					callback.call(thisArg, this[i], i, this);
			}
	};
}

(function(e){
	e.closest = e.closest || function(css){
		var node = this;

		while (node) {
			 if (node.matches(css)) return node;
			 else node = node.parentElement;
		}
		return null;
	}
 })(Element.prototype);

// ======================

function createAndInsertCollapse(menuItem, collapseItemClass) {

	var collapseHTML = `<div class="${collapseItemClass} sidebar-menu-collapse_created">
												<div class="sidebar-menu-body">
													<div class="sidebar-menu-list list-clr"></div>
												</div>
											</div>`
	menuItem.insertAdjacentHTML('beforeEnd', collapseHTML)
};

function removeCollapse(collapse) {

	if(collapse) {
		collapse.parentElement.removeChild(collapse)
	}
}

function insertTitle(dropdownDrop) {

	var button = dropdownDrop.parentElement.querySelector('.sidebar-menu-btn-txt')
	if(button) {
		text = button.textContent
		var title = `<div class="sidebar-menu-title">${text}</div>`
		dropdownDrop.querySelector('.sidebar-menu-body').insertAdjacentHTML('afterBegin', title)
	}
};

function removeTitle(dropdownDrop) {
	var title = dropdownDrop.querySelector('.sidebar-menu-title')
	if(title) {
		title.parentElement.removeChild(title)
	}
}

function setPosition(trigger, dropdownDrop, footer) {
	var coords = trigger.getBoundingClientRect();
	var dropdownDropHeight = dropdownDrop.offsetHeight
	var windowHeight = window.innerHeight
	var footerHeight = footer.offsetHeight
	if((coords.top + dropdownDropHeight + footerHeight) > windowHeight) {
		dropdownDrop.style.left = coords.right + 'px'
		dropdownDrop.style.top = coords.top + 'px'
		//dropdownDrop.style.top = coords.top + coords.height - 1 - dropdownDropHeight + 'px'
	} else {
		dropdownDrop.style.left = coords.right + 'px'
		dropdownDrop.style.top = coords.top + 'px'
	}
};

function removePosition(dropdownDrop) {
	dropdownDrop.removeAttribute('style')
}

document.addEventListener('DOMContentLoaded', function(e) {
	/*if(document.body.classList.contains('theme-kub')) {*/
		var hamburgers = document.querySelectorAll(".theme-kub .hamburger");
		var body = document.body;
		var sidebar = document.querySelector(".theme-kub .page-sidebar");
		var collapseElems = document.querySelectorAll(".theme-kub .sidebar-menu-collapse");
		var collapseElemsIn = document.querySelectorAll(".theme-kub .sideabar-menu-item-in .sidebar-menu-collapse-in");
		var footer = document.querySelector(".theme-kub .footer");
		var sidebarItems = document.querySelectorAll(".theme-kub .sideabar-menu-item");
		var sidebarItemsIn = document.querySelectorAll(".theme-kub .sideabar-menu-item-in");
		var $url = '/site/sidebar-status';

		var transformSidebarDropdownsOnToggle = function(menuItems, collapseItemsClass, classListMethod) {
			menuItems.forEach(function(menuItem) {
				var collapse = menuItem.querySelector(`.${collapseItemsClass}:not(.sidebar-menu-collapse_created)`)
				var trigger = menuItem

				if(collapse) {
					collapse.classList[classListMethod]('collapse')
					if(classListMethod === 'remove') {
						insertTitle(collapse)
						trigger.addEventListener('mouseover', function(e) {
							setPosition(this, collapse, footer)
						})
					} else if(classListMethod === 'add') {
						removeTitle(collapse)
						removePosition(collapse)
					}
				} else {
					if(classListMethod === 'remove') {
						if(!menuItem.querySelector('#itemextra1Content')) {
							createAndInsertCollapse(menuItem, collapseItemsClass)
							var createdCollapse = menuItem.querySelector(`.${collapseItemsClass}`)
							insertTitle(createdCollapse)
							trigger.addEventListener('mouseover', function(e) {
								setPosition(this, createdCollapse, footer)
							})
						}
					} else if(classListMethod === 'add') {
						if(!menuItem.querySelector('#itemextra1Content')) {
							var createdCollapse = menuItem.querySelector(`.${collapseItemsClass}.sidebar-menu-collapse_created`)
							removeCollapse(createdCollapse)
						}
					}
				}
			})
		}

		if(body.classList.contains('theme-kub_sidebar_collapsed')) {
			transformSidebarDropdownsOnToggle(sidebarItems, 'sidebar-menu-collapse', 'remove')
			transformSidebarDropdownsOnToggle(sidebarItemsIn, 'sidebar-menu-collapse-in', 'remove')
		} else {
			transformSidebarDropdownsOnToggle(sidebarItems, 'sidebar-menu-collapse', 'add')
			transformSidebarDropdownsOnToggle(sidebarItemsIn, 'sidebar-menu-collapse-in', 'add')
		}

		if(hamburgers.length) {
			hamburgers.forEach(function(hamburger) {
				hamburger.addEventListener("click", function(e) {
					e.stopPropagation();
					var $status = 0;

					body.classList.toggle("theme-kub_sidebar_collapsed");

					if(body.classList.contains('theme-kub_sidebar_collapsed')) {
						$status = 1;
						transformSidebarDropdownsOnToggle(sidebarItems, 'sidebar-menu-collapse', 'remove')
						transformSidebarDropdownsOnToggle(sidebarItemsIn, 'sidebar-menu-collapse-in', 'remove')
					} else {
						transformSidebarDropdownsOnToggle(sidebarItems, 'sidebar-menu-collapse', 'add')
						transformSidebarDropdownsOnToggle(sidebarItemsIn, 'sidebar-menu-collapse-in', 'add')
					}

					$.post($url, {
						minimize_side_menu: $status
					}, function (data) {
						if (window.hookMinimizeSideMenu && typeof window.hookMinimizeSideMenu === 'function')
							window.hookMinimizeSideMenu();
					});

					window.dispatchEvent(new Event('resize'));

				}, false);
			});
		}

		window.onscroll = function(e) {
			if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
				sidebar.classList.add('page-sidebar_on_scroll_correction')
			} else {
				sidebar.classList.remove('page-sidebar_on_scroll_correction')
			}
		};
	/*}*/
})
