
ManyCreateModal = {
    table: '.table-order-many-create',
    flowType: null,
    contractorType: null,
    init: function() {
        this.bindEvents();
        this.setS2Options();
    },
    bindEvents: function () {
        this.events.row.add();
        this.events.row.delete();
        this.events.row.copy();
        this.events.contractor.change();
        this.events.invoices.change();
        this.events.config.toggleColumn();
        this.events.date.change();
    },
    setS2Options: function() {
        window.s2options_many_create_modal = {
            "themeCss": ".select2-container--krajee-bs4",
            "sizeCss": "",
            "doReset": true,
            "doToggle": false,
            "doOrder": false
        };
    },
    events: {
        row: {
            // add row
            add: function() {
                $(document).on('click', '#add-many-create-order-position', function() {
                    const that = ManyCreateModal;
                    const table = $(that.table + ' tbody');
                    const hiddenRows = $(table).find('tr.hidden');

                    if (hiddenRows.length === 1)
                        $(this).addClass('hidden');

                    $(hiddenRows).first().removeClass('hidden');
                    that.render();
                });
            },
            // delete row
            delete: function() {
                $(document).on('click', '.delete-many-create-order-position', function() {
                    const tr = $(this).closest('tr');
                    const table = $(tr).closest('table');
                    const addBtn = $('#add-many-create-order-position');

                    $(this).tooltipster("hide");

                    $(tr).addClass('hidden').appendTo(table);
                    $(tr).find('input[type=text]').filter(':not(.date-picker)').val('').removeClass('is-invalid').trigger('change');
                    $(tr).find('select').val('').removeClass('is-invalid').trigger('change');
                    $(addBtn).removeClass('hidden');
                });
            },
            // copy row
            copy: function() {
                $(document).on('click', '.copy-many-create-order-position', function() {
                    const trDest = $(this).closest('tr');
                    const trSource = trDest.prev();

                    if (!trSource.length) {
                        return false;
                    }

                    let dest, source;
                    let v, opt;

                    // copy amount
                    source = trSource.find('td[data-attr=amount] input');
                    dest = trDest.find('td[data-attr=amount] input');
                    dest.val(source.val());

                    // copy date
                    source = trSource.find('td[data-attr=date] input');
                    dest = trDest.find('td[data-attr=date] input');
                    v = source.val().split('.');
                    if (v.length === 3) {
                        const date = new Date(v[2], v[1] - 1, v[0]);
                        if (date.getTime() === date.getTime())
                            dest.data('datepicker').selectDate(date);
                    } else {
                        dest.val('');
                    }

                    // copy recognition date
                    source = trSource.find('td[data-attr=recognition_date] input');
                    dest = trDest.find('td[data-attr=recognition_date] input');
                    v = source.val().split('.');
                    if (v.length === 3) {
                        const date = new Date(v[2], v[1] - 1, v[0]);
                        if (date.getTime() === date.getTime())
                            dest.data('datepicker').selectDate(date);
                    } else {
                        dest.val('');
                    }

                    // copy contractor
                    source = trSource.find('td[data-attr=contractor] select');
                    dest = trDest.find('td[data-attr=contractor] select');
                    if (source.val()) {
                        opt = source.find('option:selected').clone();
                        $(dest).append(opt[0]).val(source.val()).trigger('change.select2');
                    } else {
                        dest.val('');
                    }

                    // copy article
                    source = trSource.find('td[data-attr=article] select');
                    dest = trDest.find('td[data-attr=article] select');
                    dest.val(source.val()).trigger('change.select2');

                    // copy description
                    source = trSource.find('td[data-attr=description] input');
                    dest = trDest.find('td[data-attr=description] input');
                    dest.val(source.val());

                    // copy invoices list
                    source = trSource.find('td[data-attr=invoices_list] select');
                    dest = trDest.find('td[data-attr=invoices_list] select');
                    dest.find('option').remove();
                    source.find('option').each(function(_i, _v) {
                        let params = {
                            value: $(_v).data('id'),
                            text: $(_v).html(),
                            data: {
                                id: $(_v).data('id'),
                                amount: $(_v).data('amount'),
                                date: $(_v).data('date'),
                                amountPrint: $(_v).data('amountPrint'),
                                number: $(_v).data('number'),
                            }
                        };
                        $(dest).append($('<option>', params));
                    });
                    $(dest).val(source.val()).trigger('change');

                    // copy project
                    source = trSource.find('td[data-attr=project] select');
                    dest = trDest.find('td[data-attr=project] select');
                    dest.find('option').remove();
                    source.find('option').each(function(_i, _v) {
                        let params = {
                            value: $(_v).attr('value'),
                            text: $(_v).html()
                        };
                        $(dest).append($('<option>', params));
                    });
                    $(dest).val(source.val()).trigger('change');

                    // copy sale_point
                    source = trSource.find('td[data-attr=sale_point] select');
                    dest = trDest.find('td[data-attr=sale_point] select');
                    dest.find('option').remove();
                    source.find('option').each(function(_i, _v) {
                        let params = {
                            value: $(_v).attr('value'),
                            text: $(_v).html()
                        };
                        $(dest).append($('<option>', params));
                    });
                    $(dest).val(source.val()).trigger('change');

                    // copy industry
                    source = trSource.find('td[data-attr=industry] select');
                    dest = trDest.find('td[data-attr=industry] select');
                    dest.find('option').remove();
                    source.find('option').each(function(_i, _v) {
                        let params = {
                            value: $(_v).attr('value'),
                            text: $(_v).html()
                        };
                        $(dest).append($('<option>', params));
                    });
                    $(dest).val(source.val()).trigger('change');

                    trSource.after(trDest);
                    trDest.removeClass('hidden');
                });
            }
        },
        contractor: {
            change: function () {
                $(document).on("change", "select.contractor-items-depend-in-table-row", function () {
                    const tr = $(this).closest('tr');
                    const projectSelect = $(tr).find('td[data-attr=project] select');
                    const industrySelect = $(tr).find('td[data-attr=industry] select');
                    const salePointSelect = $(tr).find('td[data-attr=sale_point] select');
                    const invoiceSelect = $(tr).find('td[data-attr=invoices_list] select');
                    let url = $(this).data("items-url").replace("_cid_", $(this).val());
                    if (+$(this).val() > 0) {
                        $.get(url, function (data) {
                            // change article
                            $("select.flow-income-items", tr).val(data.income).trigger("change");
                            $("select.flow-expense-items", tr).val(data.expense).trigger("change");
                            // change project
                            $(projectSelect).find("option").remove();
                            $(projectSelect).val("").trigger("change");
                            if (data.projects && !projectSelect.prop('disabled')) {
                                let newOption = new Option("Без проекта", "0", false, false);
                                $(projectSelect).append(newOption);
                                $.each(data.projects, function(id, name) {
                                    newOption = new Option(name, id, false, false);
                                    $(projectSelect).append(newOption);
                                });
                            }
                            // change invoices
                            $(invoiceSelect).find("option").remove();
                            $(invoiceSelect).val("").trigger("change");
                            if (data.invoices) {
                                $.each(data.invoices, function(id, data) {
                                    let params = {
                                        value: id,
                                        text: data.text,
                                        data: {
                                            id: id,
                                            amount: data.amount,
                                            date: data.date,
                                            amountPrint: data.amountPrint,
                                            number: data.number
                                        }
                                    };
                                    $(invoiceSelect).append($('<option>', params));
                                });
                            }
                        });
                    } else {
                        // reset article
                        $("select.flow-income-items", tr).val('').trigger("change");
                        $("select.flow-expense-items", tr).val('').trigger("change");
                        // reset project
                        $(projectSelect).find("option").remove();
                        $(projectSelect).val("").trigger("change");
                        // reset invoices
                        $(invoiceSelect).find("option").remove();
                        $(invoiceSelect).val("").trigger("change");
                    }
                });
            }
        },
        invoices: {
            change: function() {
                $(document).on("change", "select.invoices-items-depend-in-table-row", function () {
                    const td = $(this).closest('td');
                    const selectedOptions = $(td).find('option:selected');
                    let numbers = [];

                    selectedOptions.each(function() {
                        numbers.push('№' + $(this).data('number'));
                    });

                    $(td).find('.invoice_list_widget_invoices .select2-selection__rendered').each(function () {
                        if (this.nextSibling) {
                            this.nextSibling.parentNode.removeChild(this.nextSibling);
                        }
                    });

                    $(td).find('.invoice_list_widget_invoices .select2-selection__rendered').after(numbers.join(', '));
                });
            }
        },
        config: {
            toggleColumn: function() {
                // recognition date
                //$(document).on('change', '#config-order_many_create_recognition_date', function() {
                //    const that = ManyCreateModal;
                //    const table = $(that.table + ' tbody');
                //    const isDoubleContractorWidth = !$(this).prop('checked');
                //    $(table).find('tr:hidden td[data-attr=contractor] .form-group').each(function(i, div) {
                //        $(div).css({width: (isDoubleContractorWidth) ? '254px' : '127px'});
                //    });
                //    $(table).find('tr:visible td[data-attr=contractor] .form-group').each(function(i, div) {
                //        $(div).css({width: (isDoubleContractorWidth) ? '254px' : '127px'});
                //    });
                //});
            }
        },
        date: {
            change: function() {
                $(document).on('change', 'input.date-picker-master', function () {
                    const datePickerMaster = $(this);
                    const datePickerSlave = $(datePickerMaster).closest('tr').find('td[data-attr=recognition_date]').find('input.date-picker-slave');
                    const date = moment($(datePickerMaster).val(), "DD.MM.YYYY");
                    if (date.isValid() && datePickerSlave.length > 0 && datePickerSlave.data('datepicker')) {
                        datePickerSlave.data('datepicker').selectDate(date.toDate());
                    }
                });
            }
        }
    },
    render: function() {
        const that = this;
        const table = $(that.table + ' tbody');
        that.flowType = Number($(that.table).closest('.modal').find('.flow-type-toggle-input:checked').val());
        that.contractorType = that.flowType + 1;

        // render elements
        $(table).find('tr:visible:not(._rendered)').each(function(idx, tr) {
            that.renderDatePicker.date($(tr).find('td[data-attr=date] input').prop('id'));
            that.renderDatePicker.recognitionDate($(tr).find('td[data-attr=recognition_date] input').prop('id'));
            that.renderSelect2.contractor($(tr).find('td[data-attr=contractor] select').prop('id'));
            that.renderSelect2.article($(tr).find('td[data-attr=article] select').prop('id'));
            that.renderSelect2.invoices($(tr).find('td[data-attr=invoices_list] select').prop('id'));
            that.renderSelect2.project($(tr).find('td[data-attr=project] select').prop('id'));
            that.renderSelect2.industry($(tr).find('td[data-attr=industry] select').prop('id'));
            that.renderSelect2.sale_point($(tr).find('td[data-attr=sale_point] select').prop('id'));
            $(tr).addClass('_rendered');
        });
    },
    renderSelect2: {
        contractor: function(id) {
            const that = ManyCreateModal;
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "placeholder": "",
                "language": "ru-RU",
                "allowClear": true,
                "minimumInputLength": 0,
                "dropdownCssClass":"s2-dropdown-wide",
                "ajax": {
                    "url": "\/dictionary\/contractor-dropdown",
                    "dataType": "json",
                    "delay": 250,
                    "data":
                        function(params) {
                            params.type = that.contractorType;
                            params.faceType = '';
                            params.excludeCashbox = $('#cashorderflows-cashbox_id').length ? $('#cashorderflows-cashbox_id').val() : null;
                            params.excludeIds = "";
                            params.staticData = {
                                //"add-modal-contractor":"<svg class=\"add-button-icon svg-icon\"><path fill-rule=\"evenodd\" d=\"M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zm0-1.486c3.866 0 6.505-3.295 6.505-6.514 0-3.219-2.639-6.534-6.505-6.534s-6.534 3.2-6.534 6.524c0 3.323 2.668 6.524 6.534 6.524zm.667-9.847v2.666h2.666v1.334H8.667v2.666H7.333V8.667H4.667V7.333h2.666V4.667h1.334z\"></path></svg> <span class=\"bold\">Добавить поставщика</span> ",
                                "balance": "Баланс начальный",
                                "customers": "Физ.лица"
                            };
                            return params;
                        }
                },
                "escapeMarkup": function(text) {
                    return text;
                },
                "templateSelection": function(data, container) {
                    $(data.element).attr('data-inn', data.ITN).attr('data-kpp', data.PPC).attr('data-verified', data.verified).attr('data-face_type', data.face_type);
                    return data.text;
                },
                "templateResult": function(data, container) {
                    const max_name_length = 30;
                    if (data.showPPC) {
                        $(container).attr('title', data.showPPC);
                        $(container).addClass('opt-tooltip-4');
                        $(container).attr('data-placement', 'right');
                        $(container).parents('.select2-dropdown').css('overflow', 'visible');
                        if (data.text.length > max_name_length) {
                            container.innerHTML = data.text;
                        } else {
                            container.innerHTML =
                                '<div class="opt-contractor">' +
                                '<div class="name">' + data.text + '</div>' +
                                '<div class="kpp">' + data.showPPC.substr(0, 8) + '</div>' +
                                '</div>';
                        }
                    } else {
                        container.innerHTML = data.text;
                    }

                    return container;
                },
            };

            this._render(id, options);
        },
        article: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": 0,
                "dropdownCssClass":"s2-dropdown-wide",
            };

            this._render(id, options);
        },
        project: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-medium",
            };

            this._render(id, options);
        },
        industry: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-medium",
            };

            this._render(id, options);
        },
        sale_point: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-medium",
            };

            this._render(id, options);
        },
        invoices: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-wide s2-dropdown-multiple",
                "templateResult": function (data) {
                    const
                        element = data.element,
                        date = $(element).data('date'),
                        number = $(element).data('number'),
                        amountPrint = $(element).data('amountPrint');

                    return $('<span><div class="checkbox_select2">&nbsp;</div>№'+number+' от '+date+' на '+amountPrint+' р.'+'</span>');
                },
                "language": {
                    noResults: function () {
                        let input = $('form select.contractor-items-depend:enabled');
                        if (input.length > 0) {
                            if (input.val()) {
                                return 'У контрагента нет неоплаченных счетов';
                            } else {
                                return 'Сначала выберите контрагента';
                            }
                        }
                        return 'Совпадений не найдено';
                    },
                },
            };

            this._render(id, options);
        },
        _render: function(id, options) {

            if (!$("#" + id).length)
                return;

            if (jQuery("#" + id).data("select2")) {
                jQuery("#" + id).select2("destroy");
            }
            jQuery.when(jQuery("#" + id).select2(options)).done(
                initS2Loading(id,"s2options_many_create_modal")
            );
        },
    },
    renderDatePicker: {
        date: function(id) {
            this._render(id);
        },
        recognitionDate: function(id) {
            this._render(id);
        },
        _render: function(id) {

            const dp = $("#" + id);

            if (!$(dp).length || $(dp).hasClass('hasDatepicker'))
                return;

            const v = $(dp).val().split('.');
            $(dp).datepicker(kubDatepickerConfig);
            $(dp).addClass('hasDatepicker').attr('autocomplete', 'off').inputmask({"mask": "9{2}.9{2}.9{4}"});
            if (v.length === 3) {
                let date = new Date(v[2], v[1] - 1, v[0]);
                if (date.getTime() === date.getTime()) {
                    $(dp).data('datepicker').selectDate(date);
                }
            }
        }
    }
};