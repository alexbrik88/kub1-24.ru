    PlanModal =
    {
        modalCreate: '#ajax-modal-box',
        planFactSelect: '#flow_plan_fact_select',
        modalUpdate: null, //'#update-movement',
        repeated: {
            checkbox: '#is_repeated_plan_flow',
            inputDateEnd: '#flow_repeated_date_end',
        },
        const: {
            bank: {
                input: '#cashbankflowsform-date, #cashbankflows-date'
            },
            cashbox: {
                input: '#cashorderflows-date'
            },
            emoney: {
                input: '#cashemoneyflows-date'
            },
            foreign_bank: {
                input: '#cashbankforeigncurrencyflows-date_input'
            },
            foreign_cashbox: {
                input: '#cashorderforeigncurrencyflows-date_input'
            },
            foreign_emoney: {
                input: '#cashemoneyforeigncurrencyflows-date_input'
            },
        },
        init: function()
        {
            this.bindEvents();
        },
        bindEvents: function()
        {
            const obj = this;
            $(document).on('change', obj.planFactSelect, function() {
                let value = this.value;
                let $planFactOption = $('option:selected', this);
                if ($planFactOption.length) {
                    let planFactDate = obj._parseDate($planFactOption.data('date'));
                    let $dateInput = $("input[name$='[date]'], input[name$='[date_input]']", this.form).first();
                    let inputDate = obj._parseDate($dateInput.val());
                    if (planFactDate instanceof Date) {
                        if (!(inputDate instanceof Date) ||
                            (value == 'fact' && inputDate.setHours(0,0,0,0) > planFactDate.setHours(0,0,0,0)) ||
                            (value == 'plan' && inputDate.setHours(0,0,0,0) < planFactDate.setHours(0,0,0,0))
                        ) {
                            $dateInput.data('datepicker').selectDate(planFactDate);
                        }
                    }
                }
            });
            $(document).on('change', obj.const.bank.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.cashbox.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.emoney.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_bank.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_cashbox.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_emoney.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.repeated.checkbox, function() {
                const modal = obj._getActiveModal();
                if ($(this).prop('checked')) {
                    $(modal).find('.label.repeated-date-start').text('Плановая дата первая');
                    $(modal).find('.in-plan-flow-show-repeated').show();
                } else {
                    $(modal).find('.label.repeated-date-start').text('Дата плановой оплаты');
                    $(modal).find('.in-plan-flow-show-repeated').hide();
                }
            });
            $(document).on('blur', obj.repeated.inputDateEnd, function() {
                const modal = obj._getActiveModal();
                const inputDateStart = $(modal).find('.repeated-date-start').closest('.form-group').find('input.date-picker');
                const inputDateEnd = $(obj.repeated.inputDateEnd);
                const startDate = obj._parseDate(inputDateStart.val());
                const endDate = obj._parseDate(inputDateEnd.val());

                if (endDate instanceof Date && startDate instanceof Date) {
                    if (endDate < startDate)
                        inputDateEnd.data('datepicker').selectDate(startDate);
                } else if (startDate instanceof Date) {
                    inputDateEnd.data('datepicker').selectDate(startDate);
                }
            });
        },
        checkRepeatedDateEnd: function(select)
        {
            const obj = this;
            const inputDateEnd = $(obj.repeated.inputDateEnd);
            const startDate = obj._parseDate(select.value);
            const endDate = obj._parseDate(inputDateEnd.val());

            if (inputDateEnd.length && startDate instanceof Date) {
                if (endDate < startDate)
                    inputDateEnd.data('datepicker').selectDate(startDate);
            }
        },
        togglePlanFactFields: function(select)
        {
            const obj = this;
            const modal = obj._getActiveModal();
            const date = obj._parseDate(select.value);
            const currDate = new Date();
            if (modal && date instanceof Date) {
                date.setHours(0,0,0,0);
                currDate.setHours(0,0,0,0);
                const isPlan = date > currDate;
                const title = $(modal).find('.modal-title').html();
                if (isPlan) {
                    $(modal).find('.label.repeated-date-start').text('Дата плановой оплаты');
                    $(modal).find('.in-plan-flow-show').show();
                    $(modal).find('.in-fact-flow-show').hide();
                    $(modal).find('#is_plan_flow').val('1');
                    $(modal).find('.select2-change-one-many-create-orders').hide();
                    if (title.indexOf('план') === -1)
                        $(modal).find('.modal-title').text(title.replace('операцию', 'плановую операцию'));
                } else {
                    $(modal).find('.label.repeated-date-start').text('Дата оплаты');
                    $(modal).find('.in-fact-flow-show').show();
                    $(modal).find('.in-plan-flow-show').hide();
                    $(modal).find('.in-plan-flow-show-repeated').hide();
                    $(modal).find('#is_plan_flow').val('');
                    $(modal).find('#is_repeated_plan_flow').prop('checked', false).uniform('refresh');
                    $(modal).find('.select2-change-one-many-create-orders').show();
                    if (title.indexOf('план') !== -1)
                        $(modal).find('.modal-title').text(title.replace('плановую операцию', 'операцию'));
                }

                let $planFactSelect = $(obj.planFactSelect);
                if ($planFactSelect.length) {
                    let value = $planFactSelect.val();
                    let $planFactOption = $('option:selected', $planFactSelect);
                    if ($planFactOption.length) {
                        let planFactDate = obj._parseDate($planFactOption.data('date'));
                        if (value && planFactDate instanceof Date) {
                            planFactDate.setHours(0,0,0,0);
                            if (value == 'fact' && date > planFactDate) {
                                $planFactSelect.val('plan').trigger('change');
                            }
                            if (value == 'plan' && date < planFactDate) {
                                $planFactSelect.val('fact').trigger('change');
                            }
                        }

                    }
                }
            }
        },
        _getActiveModal: function()
        {
            if ($(this.modalCreate).hasClass('show')) {
                return $(this.modalCreate);
            }
            if ($(this.modalUpdate).hasClass('show')) {
                return $(this.modalUpdate);
            }

            return null;
        },
        _parseDate: function(str)
        {
            if (typeof str === "undefined") {
                console.log('str is undefined');
                return null;
            }

            const f = str.split(".");
            const d = new Date(Date.parse(f[2] +'-'+ f[1] +'-'+ f[0]));

            return !isNaN(d.getTime()) ? d : null;
        },
        _isUpdateAction: function()
        {
            return $(this.modalUpdate).hasClass('show');
        },
    };

    MultiWallet =
    {
        modalCreate: '#ajax-modal-box',
        modalUpdate: '#update-movement',
        const: {
            bank: {
                id: 1, // CashFlowsBase::WALLET_BANK
                urlCreate: '/cash/multi-wallet/create-bank-flow?',
                urlUpdate: '/cash/multi-wallet/update-bank-flow?',
                hiddenInput: '#cash_bank_flows_id',
            },
            cashbox: {
                id: 2, // CashFlowsBase::WALLET_CASHBOX
                urlCreate: '/cash/multi-wallet/create-order-flow?',
                urlUpdate: '/cash/multi-wallet/update-order-flow?',
                hiddenInput: '#cash_order_flows_id',
            },
            emoney: {
                id: 3, // CashFlowsBase::WALLET_EMONEY
                urlCreate: '/cash/multi-wallet/create-emoney-flow?',
                urlUpdate: '/cash/multi-wallet/update-emoney-flow?',
                hiddenInput: '#cash_emoney_flows_id',
            },
            plan: {
                urlUpdate: '/cash/multi-wallet/update-plan-flow?'
            },
            foreign_bank: {
                id: 11, // CashFlowsBase::WALLET_FOREIGN_BANK
                urlCreate: '/cash/multi-wallet/create-bank-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-bank-flow?&isForeign=1',
                hiddenInput: '#cash_bank_flows_id',
            },
            foreign_cashbox: {
                id: 12, // CashFlowsBase::WALLET_FOREIGN_CASHBOX
                urlCreate: '/cash/multi-wallet/create-order-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-order-flow?&isForeign=1',
                hiddenInput: '#cash_order_flows_id',
            },
            foreign_emoney: {
                id: 13, // CashFlowsBase::WALLET_FOREIGN_EMONEY
                urlCreate: '/cash/multi-wallet/create-emoney-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-emoney-flow?&isForeign=1',
                hiddenInput: '#cash_emoney_flows_id',
            },
            foreign_plan: {
                urlUpdate: '/cash/multi-wallet/update-plan-flow?&isForeign=1'
            },
        },
        currentVal: null,
        currentWallet: null,
        originWallet: null,
        originFlow: null,
        init: function()
        {
            this.bindEvents();
        },
        bindEvents: function()
        {
            const obj = this;

            // open multi-wallet select
            $(document).on('select2:open', '#multi_wallet_select', function() {
                if (obj.currentWallet === null) {
                    obj.currentWallet = Number($(this).find('option:selected').attr('data-wallet'));
                    obj.currentVal = $(this).val();
                }
            });

            // close create modal
            $(document).on('hide.bs.modal', obj.modalCreate, function() {
                obj.currentWallet = obj.currentVal = null;
            });

            // close update modal
            $(document).on('hide.bs.modal', obj.modalUpdate, function() {
                obj.currentWallet = obj.currentVal = null;
                obj.originWallet = obj.originFlow = null;
            });

            $(document).on('click', '.update-flow-item:not(.internal)', function() {
                const href = $(this).attr('href');
                const _wallet = href.match(/originWallet=(\d+)/);
                const _flow = href.match(/originFlow=(\d+)/);
                if (_wallet && _flow) {
                    obj.originWallet = _wallet[1];
                    obj.originFlow = _flow[1];
                }
            });

            // change wallet
            $(document).on('change', '.flow_form_wallet_select', function(e) {
                const selectedOption = $("option:selected", this);
                const selectedWallet = Number($(selectedOption).attr('data-wallet'));

                if (!selectedWallet) {
                    if (this.value === 'add-modal-rs') {
                        $(this).val(obj.currentVal).trigger('change');
                        $("#add_company_account_modal").appendTo("body").modal("show");
                        return;
                    }
                } else if (selectedWallet !== obj.currentWallet) {
                    if (obj.changeWalletModal(selectedOption)) {
                        obj.currentWallet = selectedWallet;
                        obj.currentVal = $(this).val();
                    } else {
                        alert('Невозможно изменить кошелек');
                    }
                }
                else {
                    obj.changeWalletId(selectedOption);
                }
            });
        },
        changeWalletId: function(selectedOption)
        {
            const obj = this;
            const selectedWallet = Number($(selectedOption).attr('data-wallet'));

            switch (selectedWallet) {
                case obj.const.bank.id:
                    $(obj.const.bank.hiddenInput).val(selectedOption.attr('data-rs'));
                    break;
                case obj.const.cashbox.id:
                    $(obj.const.cashbox.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.emoney.id:
                    $(obj.const.emoney.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.foreign_bank.id:
                    $(obj.const.foreign_bank.hiddenInput).val(selectedOption.attr('data-rs'));
                    break;
                case obj.const.foreign_cashbox.id:
                    $(obj.const.foreign_cashbox.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.foreign_emoney.id:
                    $(obj.const.foreign_emoney.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                default:
                    alert('Кошелек не найден (1)');
                    break;
            }
        },
        changeWalletModal: function(selectedOption)
        {
            const obj = this;
            const ajaxModal = (obj._isUpdateAction()) ? $(obj.modalUpdate) : $(obj.modalCreate);
            const selectedWallet = Number($(selectedOption).attr('data-wallet'));
            const isPlan = $('#is_plan_flow').val() || false;
            const type = $(ajaxModal).find('.flow-type-toggle-input:checked').val() || 1;
            const rs = $(selectedOption).attr("data-rs") || "";
            const id = $(selectedOption).attr("data-id") || "";
            const contractorId = $(ajaxModal).find('select.contractor-items-depend').val() || '';

            if (isPlan && obj._isUpdateAction()) {
                //console.log('plan modal pseudo reload');
                return true;
            }

            let url;
            switch (selectedWallet) {
                case obj.const.bank.id:
                    url = (obj._isUpdateAction() ? obj.const.bank.urlUpdate : obj.const.bank.urlCreate) + '&wallet=' + selectedWallet+'_'+rs + '&type=' + type + '&rs=' + rs + '&contractorId=' + contractorId;
                    break;
                case obj.const.cashbox.id:
                    url = (obj._isUpdateAction() ? obj.const.cashbox.urlUpdate : obj.const.cashbox.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.emoney.id:
                    url = (obj._isUpdateAction() ? obj.const.emoney.urlUpdate : obj.const.emoney.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_bank.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_bank.urlUpdate : obj.const.foreign_bank.urlCreate) + '&wallet=' + selectedWallet+'_'+rs + '&type=' + type + '&rs=' + rs + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_cashbox.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_cashbox.urlUpdate : obj.const.foreign_cashbox.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_emoney.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_emoney.urlUpdate : obj.const.foreign_emoney.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                default:
                    alert('Кошелек не найден (2)');
                    break;
            }

            if (url) {

                if (obj._isUpdateAction()) {

                    if (!obj._validateWallet() || !obj._validateFlow())
                        return false;

                    url += '&originWallet=' + obj.originWallet + '&originFlow=' + obj.originFlow;
                }

                ajaxModal.find('.modal-body > div').first().load(url);

                return true;
            }

            return false;
        },
        _isUpdateAction: function()
        {
            return $(this.modalUpdate).hasClass('show');
        },
        _validateWallet: function()
        {
            if (!this.originFlow) {
                alert ('Кошелек не определен (3)');
                $(this.modalUpdate).modal('hide');
                return false;
            }

            return true;
        },
        _validateFlow: function()
        {
            if (!this.originWallet) {
                alert ('Операция не определена (3)');
                $(this.modalUpdate).modal('hide');
                return false;
            }

            return true;
        }
    };