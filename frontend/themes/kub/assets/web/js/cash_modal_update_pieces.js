///////////////////////////
// Split bank operations //
///////////////////////////

CashModalUpdatePieces = {
    table: '.table-order-many-create',
    flowType: null,
    contractorType: null,
    init: function() {
        this.bindEvents();
        this.setS2Options();
        this.setS2DefaultValues();
    },
    bindEvents: function () {
        this.events.modal.changeMode();
        this.events.modal.addIncomeItem();
        this.events.modal.addExpenditureItem();
        this.events.row.add();
        this.events.row.delete();
        this.events.invoices.change();
        //this.events.contractor.change(); // todo
    },
    setS2Options: function() {
        window.s2options_many_create_modal = {
            "themeCss": ".select2-container--krajee-bs4",
            "sizeCss": "",
            "doReset": true,
            "doToggle": false,
            "doOrder": false
        };
    },
    setS2DefaultValues: function() {
        const that = this;
        $(that.table).find("select.invoices-items-depend-in-table-row").each(function() {
            if (this.value)
                $(this).trigger('change.select2');
        });
    },
    showNewRow: function(source, destination) {
        destination.find('td[data-attr=article] select').val(
            source.find('td[data-attr=article] select').val()
        ).trigger('change');
        destination.find('td[data-attr=project] select').val(
            source.find('td[data-attr=project] select').val()
        ).trigger('change');
        destination.find('td[data-attr=sale_point] select').val(
            source.find('td[data-attr=sale_point] select').val()
        ).trigger('change');
        destination.find('td[data-attr=direction] select').val(
            source.find('td[data-attr=direction] select').val()
        ).trigger('change');
        destination.removeClass('hidden');
        this.render();
    },
    events: {
        modal: {
            changeMode: function() {
                $(document).on('click', '.cash-modal-split-to-pieces', function(e){
                    e.preventDefault();
                    const modal = $(this).closest('.modal');
                    const modalBody = modal.find('.modal-body');
                    const modalTitle = modal.find('.modal-title');
                    const url = $(this).data('url') || $(this).attr('href');

                    if (modal.attr('id') !== 'update-movement') {
                        window.toastr.success('Функция временно недоступна. Обратитесь в тех.поддержку.');
                        $(this).prop('disabled', true);
                        return false;
                    }

                    if (url) {
                        $(modalBody).load(url);
                        let title = $(this).data('title') || $(this).attr('title');
                        if (title) {
                            $(modalTitle).html(title).show();
                        } else {
                            $(modalTitle).html('Редактировать операцию');
                        }
                    }
                });
            },
            addIncomeItem: function() {
                const that = this;
                $(document).on('change', '.flow-income-items-pieces', function(e) {
                    const inputId = $(this).attr('id');
                    const url = '/reference/articles/create-from-dropdown/?type=0&inputSelector=' + inputId;
                    const title = 'Добавить статью прихода';
                    if (this.value === 'add-modal-income-item') {
                        e.preventDefault();
                        that._showArticleModal(inputId, url, title);
                    }
                });
            },
            addExpenditureItem: function() {
                const that = this;
                $(document).on('change', '.flow-expense-items-pieces', function(e) {
                    const inputId = $(this).attr('id');
                    const url = '/reference/articles/create-from-dropdown/?type=1&inputSelector=' + inputId;
                    const title = 'Добавить статью расхода';

                    if (this.value === 'add-modal-expense-item') {
                        e.preventDefault();
                        that._showArticleModal(inputId, url, title);
                    }
                });
            },
            _showArticleModal: function (inputId, url, title) {
                $('#' + inputId).val('').trigger('change');
                $.pjax({url: url, container: '#article-dropdown-form-container', push: false, scrollTo: false});
                $(document).on('pjax:success', '#article-dropdown-form-container', function(e, data) {
                    $('#' + inputId).select2('close');
                    $('#article-dropdown-modal-container .modal-title').text(title);
                    $('#article-dropdown-modal-container').modal();
                });
            }
        },
        row: {
            // add row
            add: function() {
                $(document).on('click', '#add-many-create-order-position', function() {
                    const that = CashModalUpdatePieces;
                    const table = $(that.table + ' tbody');
                    const hiddenRows = $(table).find('tr.hidden');
                    const activeHiddenRow = $(hiddenRows).first();
                    const lastRow = activeHiddenRow.prev();

                    if (hiddenRows.length === 1) {
                        $(this).addClass('hidden');
                    }
                    if (activeHiddenRow.length) {
                        that.showNewRow(lastRow, activeHiddenRow);
                    } else {
                        window.toastr.success('Функция временно недоступна. Обратитесь в тех.поддержку.');
                    }
                });
            },
            // delete row
            delete: function() {
                $(document).on('click', '.delete-many-create-order-position', function() {
                    const tr = $(this).closest('tr');
                    const table = $(tr).closest('table');
                    const addBtn = $('#add-many-create-order-position');

                    $(this).tooltipster("hide");

                    $(tr).addClass('hidden').appendTo(table);
                    $(tr).find('input[type=text]').filter(':not(.date-picker)').val('').removeClass('is-invalid').trigger('change');
                    $(tr).find('select').val('').removeClass('is-invalid').trigger('change');
                    $(addBtn).removeClass('hidden');
                });
            },
        },
        contractor: {
            change: function () {
                $(document).on("change", "select.contractor-items-depend-in-table-row", function () {
                    const tr = $(this).closest('tr');
                    const projectSelect = $(tr).find('td[data-attr=project] select');
                    const invoiceSelect = $(tr).find('td[data-attr=invoices_list] select');
                    let url = $(this).data("items-url").replace("_cid_", $(this).val());
                    if (+$(this).val() > 0) {
                        $.get(url, function (data) {
                            // change article
                            $("select.flow-income-items", tr).val(data.income).trigger("change");
                            $("select.flow-expense-items", tr).val(data.expense).trigger("change");
                            // change project
                            $(projectSelect).find("option").remove();
                            $(projectSelect).val("").trigger("change");
                            if (data.projects && !projectSelect.prop('disabled')) {
                                let newOption = new Option("Без проекта", "0", false, false);
                                $(projectSelect).append(newOption);
                                $.each(data.projects, function(id, name) {
                                    newOption = new Option(name, id, false, false);
                                    $(projectSelect).append(newOption);
                                });
                            }
                            // change invoices
                            $(invoiceSelect).find("option").remove();
                            $(invoiceSelect).val("").trigger("change");
                            if (data.invoices) {
                                $.each(data.invoices, function(id, data) {
                                    let params = {
                                        value: id,
                                        text: data.text,
                                        data: {
                                            id: id,
                                            amount: data.amount,
                                            date: data.date,
                                            amountPrint: data.amountPrint,
                                            number: data.number
                                        }
                                    };
                                    $(invoiceSelect).append($('<option>', params));
                                });
                            }
                        });
                    } else {
                        // reset article
                        $("select.flow-income-items", tr).val('').trigger("change");
                        $("select.flow-expense-items", tr).val('').trigger("change");
                        // reset project
                        $(projectSelect).find("option").remove();
                        $(projectSelect).val("").trigger("change");
                        // reset invoices
                        $(invoiceSelect).find("option").remove();
                        $(invoiceSelect).val("").trigger("change");
                    }
                });
            }
        },
        invoices: {
            change: function() {
                const that = this;
                $(document).on("change", "select.invoices-items-depend-in-table-row", function (e) {
                    const td = $(this).closest('td');
                    const tr = $(td).closest('tr');
                    const selectedOptions = $(td).find('option:selected');
                    let remainingAmount = 0;

                    selectedOptions.each(function() {
                        remainingAmount += Number($(this).data('amountremaining')) || 0;
                    });

                    // put invoices remaining amount as piece amount
                    $(tr).find('td[data-attr=amount] input').val(remainingAmount);
                });

                $(document).on("change.select2", "select.invoices-items-depend-in-table-row", function (e) {
                    const td = $(this).closest('td');
                    const selectedOptions = $(td).find('option:selected');
                    let numbers = [];

                    selectedOptions.each(function() {
                        numbers.push('№' + $(this).data('number'));
                    });

                    $(td).find('.invoice_list_widget_invoices .select2-selection__rendered').each(function () {
                        if (this.nextSibling) {
                            this.nextSibling.parentNode.removeChild(this.nextSibling);
                        }
                    });

                    $(td).find('.invoice_list_widget_invoices .select2-selection__rendered').after(numbers.join(', '));

                    that._changeMainInvoiceInput();
                });
            },
            _changeMainInvoiceInput: function() {
                const mainInvoiceInput = $('#_main_invoices_list');
                let ids = [];
                $(CashModalUpdatePieces.table).find("select.invoices-items-depend-in-table-row").each(function () {
                    $(this).find('option:selected').each(function() {
                        ids.push($(this).data('id'));
                    });
                });

                $(mainInvoiceInput).val(ids).trigger('change');
            }
        },
    },
    render: function() {
        const that = this;
        const table = $(that.table + ' tbody');
        that.flowType = Number($(that.table).closest('.modal').find('.flow-type-toggle-input:checked').val());
        that.contractorType = that.flowType + 1;

        // render elements
        $(table).find('tr:visible:not(._rendered)').each(function(idx, tr) {
            that.renderDatePicker.date($(tr).find('td[data-attr=date] input').prop('id'));
            that.renderDatePicker.recognitionDate($(tr).find('td[data-attr=recognition_date] input').prop('id'));
            that.renderSelect2.contractor($(tr).find('td[data-attr=contractor] select').prop('id'));
            that.renderSelect2.article($(tr).find('td[data-attr=article] select').prop('id'));
            that.renderSelect2.project($(tr).find('td[data-attr=project] select').prop('id'));
            that.renderSelect2.salePoint($(tr).find('td[data-attr=sale_point] select').prop('id'));
            that.renderSelect2.direction($(tr).find('td[data-attr=direction] select').prop('id'));
            that.renderSelect2.invoices($(tr).find('td[data-attr=invoices_list] select').prop('id'));
            $(tr).addClass('_rendered');
        });
    },
    renderSelect2: {
        contractor: function(id) {
            const that = CashModalUpdatePieces;
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "placeholder": "",
                "language": "ru-RU",
                "allowClear": true,
                "minimumInputLength": 0,
                "dropdownCssClass":"s2-dropdown-wide",
                "ajax": {
                    "url": "\/dictionary\/contractor-dropdown",
                    "dataType": "json",
                    "delay": 250,
                    "data":
                        function(params) {
                            params.type = that.contractorType;
                            params.faceType = '';
                            params.excludeCashbox = $('#cashorderflows-cashbox_id').length ? $('#cashorderflows-cashbox_id').val() : null;
                            params.excludeIds = "";
                            params.staticData = {
                                //"add-modal-contractor":"<svg class=\"add-button-icon svg-icon\"><path fill-rule=\"evenodd\" d=\"M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zm0-1.486c3.866 0 6.505-3.295 6.505-6.514 0-3.219-2.639-6.534-6.505-6.534s-6.534 3.2-6.534 6.524c0 3.323 2.668 6.524 6.534 6.524zm.667-9.847v2.666h2.666v1.334H8.667v2.666H7.333V8.667H4.667V7.333h2.666V4.667h1.334z\"></path></svg> <span class=\"bold\">Добавить поставщика</span> ",
                                "balance": "Баланс начальный",
                                "customers": "Физ.лица"
                            };
                            return params;
                        }
                },
                "escapeMarkup": function(text) {
                    return text;
                },
                "templateSelection": function(data, container) {
                    $(data.element).attr('data-inn', data.ITN).attr('data-kpp', data.PPC).attr('data-verified', data.verified).attr('data-face_type', data.face_type);
                    return data.text;
                },
                "templateResult": function(data, container) {
                    const max_name_length = 30;
                    if (data.showPPC) {
                        $(container).attr('title', data.showPPC);
                        $(container).addClass('opt-tooltip-4');
                        $(container).attr('data-placement', 'right');
                        $(container).parents('.select2-dropdown').css('overflow', 'visible');
                        if (data.text.length > max_name_length) {
                            container.innerHTML = data.text;
                        } else {
                            container.innerHTML =
                                '<div class="opt-contractor">' +
                                '<div class="name">' + data.text + '</div>' +
                                '<div class="kpp">' + data.showPPC.substr(0, 8) + '</div>' +
                                '</div>';
                        }
                    } else {
                        container.innerHTML = data.text;
                    }

                    return container;
                },
            };

            this._render(id, options);
        },
        article: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": 0,
                "dropdownCssClass":"s2-dropdown-wide",
                "escapeMarkup": function(markup) { return markup; }
            };

            this._render(id, options);
        },
        project: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-wide",
            };

            this._render(id, options);
        },
        salePoint: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-wide",
            };

            this._render(id, options);
        },
        direction: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "language": "ru-RU",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-wide",
            };

            this._render(id, options);
        },
        invoices: function(id) {
            const options = {
                "width": "100%",
                "theme": "krajee-bs4",
                "allowClear": false,
                "placeholder": "",
                "minimumResultsForSearch": Infinity,
                "dropdownCssClass":"s2-dropdown-wide s2-dropdown-multiple",
                "templateResult": function (data) {
                    const
                        element = data.element,
                        date = $(element).data('date'),
                        number = $(element).data('number'),
                        amountPrint = $(element).data('amountprint');

                    return $('<span><div class="checkbox_select2">&nbsp;</div>№'+number+' от '+date+' на '+amountPrint+' р.'+'</span>');
                },
                "language": {
                    noResults: function () {
                        let input = $('form select.contractor-items-depend:enabled');
                        if (input.length > 0) {
                            if (input.val()) {
                                return 'У контрагента нет неоплаченных счетов';
                            } else {
                                return 'Сначала выберите контрагента';
                            }
                        }
                        return 'Совпадений не найдено';
                    },
                },
            };

            this._render(id, options);
        },
        _render: function(id, options) {

            if (!$("#" + id).length)
                return;

            if (jQuery("#" + id).data("select2")) {
                jQuery("#" + id).select2("destroy");
            }
            jQuery.when(jQuery("#" + id).select2(options)).done(
                initS2Loading(id,"s2options_many_create_modal")
            );
        },
    },
    renderDatePicker: {
        date: function(id) {
            this._render(id);
        },
        recognitionDate: function(id) {
            this._render(id);
        },
        _render: function(id) {

            const dp = $("#" + id);

            if (!$(dp).length || $(dp).hasClass('hasDatepicker'))
                return;

            const v = $(dp).val().split('.');
            $(dp).datepicker(kubDatepickerConfig);
            $(dp).addClass('hasDatepicker').attr('autocomplete', 'off').inputmask({"mask": "9{2}.9{2}.9{4}"});
            if (v.length === 3) {
                let date = new Date(v[2], v[1] - 1, v[0]);
                if (date.getTime() === date.getTime()) {
                    $(dp).data('datepicker').selectDate(date);
                }
            }
        }
    }
};