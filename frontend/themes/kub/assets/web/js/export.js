$(function () {
    var form = $('#one-s-export-form');
    var _enableUpdateProgress = false;

    var exportProgressTimeoutId = null;
    var exportProgressUpdate = function () {
        /*var form = $('#one-s-export-form');*/
        $.ajax({
            url: form.data('progress-url'),
            type: form.attr('method'),
            data: {userId: $('#user_id').val()},
            dataType: 'json',
            success: function (response) {
                var progress = response.progress;

                if (_enableUpdateProgress && progress != undefined) {
                    progress = Math.min(progress, 100);
                    form.find('.progress-bar').css('width', progress + '%');
                    form.find('span.progress-value').text(progress + '%');
                }
            }
        });
    };

    var updateExportHistoryList = function () {
        $.pjax.reload('#export-list');
    };

    form.find('#export-type-out, #export-type-in, #export-contractor').on('change', function (e) {
        if ($(this).prop('checked')) {
            var id = $(this).attr('id');

            $('.' + id).find('input[type="checkbox"]').each(function (i, v) {
                $(v).prop('checked', true).closest('span').addClass('checked');
            });
        } else {
            var id = $(this).attr('id');

            $('.' + id).find('input[type="checkbox"]').each(function (i, v) {
                $(v).prop('checked', false).closest('span').removeClass('checked');
            });
        }
    });

    form.find('.export-type-out, .export-type-in, .export-contractor').find('input[type="checkbox"]').on('change', function (e) {
        if (!$(this).prop('checked')) {
            var id = $(this).closest("div[class^=export]").attr('class');
            $('#' + id).prop('checked', false).closest('span').removeClass('checked');
        }
    });

    form.on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        _enableUpdateProgress = true;

        var button = form.find('.load_button button');

        if (form.find('.load_button button').hasClass('completed')) {
            var fileUrl = form.find('.load_button button').data('url');

            if (fileUrl) {
                form.find('.checker span.checked').removeClass('checked');
                form.find('.load_button button').removeClass('completed');
                form.find('.export-progress-action').text('');
                form.find('.progress').hide();
                //button.text('Создать архив').removeAttr('style');
                button.text('Сформировать архив').addClass('button-hover-content-red').removeClass('button-regular_red');
                window.location.href = fileUrl;
            }
        } else {
            var progressStatus = form.find('.export-progress-action');
            button.prop('disabled', true);
            form.find('.progress-bar').css('width', 0);
            form.find('span.progress-value').text('0%');
            form.find('.progress-status').show();
            form.find('.progress').addClass('active').addClass('progress-striped').show();
            progressStatus.text(progressStatus.data('in-progress'));

            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                    _enableUpdateProgress = false;
                    clearInterval(exportProgressTimeoutId);
                    if (response.success != undefined && response.success) {
                        form.find('.progress-bar').css('width', '100%');
                        form.find('span.progress-value').text('');
                        form.find('.load_button button').data('url', response.url).addClass('completed');
                        form.find('.progress').removeClass('active').removeClass('progress-striped');//.hide();
                        //if (form.attr('action') == '/export/one-s/create') {
                        //    form.find('.progress').hide();
                        //}
                        progressStatus.text(progressStatus.data('completed'));
                        //button.text('Скачать файл').attr('style', 'background-color: #45b6af;');
                        button.text('Скачать файл').removeClass('button-hover-content-red').addClass('button-regular_red');
                        updateExportHistoryList();
                    } else if (response.error != undefined) {
                        form.find('.progress').removeClass('active').removeClass('progress-striped').hide();
                        form.find('.progress-bar').css('width', '0%');
                        form.find('span.progress-value').text('');
                        progressStatus.text('');

                        window.toastr.error(response.error, '', {
                            'closeButton': true,
                            'showDuration': 1000,
                            'hideDuration': 1000,
                            'timeOut': 2000,
                            'extendedTimeOut': 1000,
                            'escapeHtml': false,
                        });
                    }
                    $(form)[0].reset();
                    $('input[type=checkbox]').uniform('refresh');
                    button.removeAttr('disabled');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    _enableUpdateProgress = false;
                    clearTimeout(exportProgressTimeoutId);
                    $(form)[0].reset();
                    $('input[type=checkbox]').uniform('refresh');
                    form.find('.progress-bar').css('width', 0);
                    form.find('span.progress-value').text('');
                    button.removeAttr('disabled');
                    alert(xhr.status);
                    alert(thrownError);
                }
            });

            exportProgressTimeoutId = setInterval(function() { exportProgressUpdate(); }, 500);
        }

    });
});