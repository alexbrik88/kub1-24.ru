$(document).ready(function () {
    $(document).on('click', '#projectTaskLink', function (e) {
        e.preventDefault();
        window.openAjaxDialog('Добавить задачу', '/project/create-task?project_id=' + $(this).data('project-id'));
    });
});
