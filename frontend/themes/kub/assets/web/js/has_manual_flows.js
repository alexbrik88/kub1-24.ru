!function($) {
    $(document).on('click', '#manual_flows_modal .has_manual_flows', function (e) {
        var $button = $(this);
        let laddaButton = Ladda.create(this);
        laddaButton.start();
        $.ajax({
            url: $button.data('check-url'),
            success: function (data) {
                if (data.hasManual) {
                    window.location.href = $button.data('index-url');
                } else {
                    laddaButton.stop();
                    $('#manual_flows_modal .no_manual_flows').toggleClass('hidden', false);
                }
            },
            error:  function (data) {
                laddaButton.stop();
            },
        });
    });
    $(document).on('hidden.bs.modal', '#manual_flows_modal', function (e) {
        $('#manual_flows_modal .no_manual_flows').toggleClass('hidden', true);
    });
}(window.jQuery);
