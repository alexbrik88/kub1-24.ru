
!function ($, window, undefined) {
    $(document).on("keyup change", "#new-template-name", function (e) {
        if ($(this).val() !== "") {
            $('.save-email-template').show();
            $('.save-email-template-ico').hide();
        } else {
            $('.save-email-template').hide();
            $('.save-email-template-ico').show();
        }
    });

    $(document).on("keyup change", ".container-who-send-input input", function (e) {
        var reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (reg.test($(this).val()) != false) {
            $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").removeAttr("disabled").uniform();
        } else {
            $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").attr("disabled", true).uniform();
        }
    });

    $(document).on("click", ".email-uploaded-files .one-file .delete-file", function () {
        var $oneFile = $(this).closest(".one-file");
        var $fileBlock = $(this).closest(".email-uploaded-files");
        console.log($(this).data("url"));
        $.post($(this).data("url"), null, function (data) {
            if (data.result) {
                var $needUploadFiles = [];

                $oneFile.remove();
                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    console.log($(this).data("id"))
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#reportcompaniesform-attachment").val($needUploadFiles.join(", "));
                if ($fileBlock.find(".one-file:not(.template)").length == 0) {
                    $fileBlock.hide();
                }
            } else {
                alert(data.msg);
            }
        });
    });

    var _$uploadButton = $(".block-files-email span.upload-file");
    var _uploadUrl = _$uploadButton.data("url");
    var _csrfParameter = _$uploadButton.data("csrf-parameter");
    var _csrfToken = _$uploadButton.data("csrf-token");
    var uploadData = {};
    uploadData[_csrfParameter] = _csrfToken;

    var uploader = new ss.SimpleUpload({
        button: _$uploadButton[0], // HTML element used as upload button
        url: _uploadUrl, // URL of server-side upload handler
        data: uploadData,
        multipart: true,
        multiple: true,
        multipleSelect: true,
        encodeCustomHeaders: true,
        responseType: "json",
        name: "emailFile", // Parameter name of the uploaded file
        onSubmit: function() {
            $("#file-ajax-loading").css("display", "inline-block");
        },
        onComplete: function (filename, response) {
            if (response["result"] == true) {
                var $templateFile = $(".email-uploaded-files .one-file.template").clone();
                var $needUploadFiles = [];

                $templateFile.removeClass("template");
                $templateFile.attr("data-id", response["id"]);
                $templateFile.prepend(response["previewImg"]);
                $templateFile.find(".file-name").text(response["name"]).attr("title", response["name"]);
                $templateFile.find(".file-size").text(response["size"] + " КБ");
                $templateFile.find(".delete-file").attr("data-url", response["deleteUrl"]);
                if (response["downloadUrl"]) {
                    $templateFile.find(".download-file").attr("href", response["downloadUrl"]).show();
                }
                $templateFile.show();
                $(".email-uploaded-files").append($templateFile);
                $("#file-ajax-loading").hide();
                $(".email-uploaded-files").show();

                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#reportcompaniesform-attachment").val($needUploadFiles.join(", "));
            } else {
                alert(response["msg"]);
                $("#file-ajax-loading").hide();
            }
        }
    });

    $employeesEmail = $(".contractor-employees-email");
    $signatureBlock = $(".field-reportcompaniesform-signature, .update-email-signature");
    $templateBlock = $(".field-reportcompaniesform-template");

    var sendMessagePanelOpen = function (sendToMe) {
        if (sendToMe) {
            $("#user-email-dropdown input:checkbox:checked:not(#reportcompaniesform-sendtome)").click();
            if (!$("#reportcompaniesform-sendtome").is(":checked")) {
                $("#reportcompaniesform-sendtome").click();
            }
            //$("form#send-document-form button:submit").closest("div").pulsate({
            //    color: "#bf1c56",
            //    reach: 20,
            //    repeat: 3
            //});
        }
        $(".send-message-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".send-message-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".send-message-panel .main-block").scrollTop(0);
        if ($("#reportcompaniesform-emailto").val().length < 1) {
            $("#reportcompaniesform-emailto_tag").click()
        }
        if ($(".send-message-tooltip-panel").length > 0) {
            $(".send-message-tooltip-panel").show(500);
        }
    }

    var sendMessagePanelClose = function() {
        $(".send-message-panel-trigger").removeClass("active");
        $(".send-message-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
        if ($(".send-message-tooltip-panel").length > 0) {
            $(".send-message-tooltip-panel").hide(500);
        }
        $("form#send-document-form button:submit").removeClass("pulse-button");
    }
    $(document).on("click", ".send-message-panel-trigger", function (e) {
        e.preventDefault();

        var sendToMe = false;
        if ($(this).hasClass("send-to-me")) {
            sendToMe = true;
        }
        sendMessagePanelOpen(sendToMe);
    });

    $("#reportcompaniesform-emailto_tag").focus(function () {
        $employeesEmail.slideDown();
        $templateBlock.slideUp();
        $signatureBlock.slideUp();
        $(".field-reportcompaniesform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    $("#reportcompaniesform-sendcopy_tag, #message-subject, #reportcompaniesform-emailtext").focus(function () {
        $employeesEmail.slideUp();
        $templateBlock.slideUp();
        $signatureBlock.slideUp();
        $(".field-reportcompaniesform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    // resize TextArea
    function textAreaAdjust(o) {
        if (o && o.style) {
            o.style.height = "1px";
            o.style.height = (10 + o.scrollHeight) + "px";
        }
    }

    $(document).ready(function () {
        $(".add-signature-email, .template-email, .block-files-email, input[name=\"emailFile\"]").click(function () {
            $employeesEmail.slideUp();
        });
        $("input[name=\"emailFile\"]").click(function () {
            $templateBlock.slideUp();
            $signatureBlock.slideUp();
            $(".field-reportcompaniesform-signature .form-actions").hide();
            $("textarea.email-signature").attr("disabled", true);
        });
        $("#reportcompaniesform-emailto_tagsinput #reportcompaniesform-emailto_tag")
            .attr("placeholder", "Укажите e-mail или выберите из списка")
            .addClass("visible");

        var text = $("#reportcompaniesform-emailtext").val(),
            matches,
            breaks;
        if (text) {
            matches = text.match(/\n/g),
            breaks = matches ? matches.length : 0;
        }

        $("#reportcompaniesform-emailtext").attr("rows", breaks + 1);

        textAreaAdjust($("#reportcompaniesform-emailtext")[0]);
    });

    $(".save-email-template-ico").click(function() {
        return false;
    });

    $(".save-email-template").click(function () {
        $saveButton = $(this);
        $.post($(this).data("url"), $("#new-template-name, #reportcompaniesform-emailtext").serialize(), function (data) {
            $templateVariants = $(".template-variants");
            if (data.result) {

                $templateVariants.append("<div class='row flex-nowrap justify-content-between mb-2'><div class='column nowrap'><label class='radio-label'>" +
                    "<input type=\"radio\" name=\"template\" value=\"" + data.id +"\" data-message=\"" + data.text + "\">" +
                    "<span class=\"radio-txt nowrap\">" + data.name + "</span></label></div> " +
                    "<div class=\"column nowrap\"> " +
                    "<button class=\"update-template link button-clr mr-1\" data-url=\"/email/update-template?id=" + data.id + "\"><svg class=\"svg-icon\"><use xlink:href=\"/img/svg/svgSprite.svg#pencil\"></use></svg></button>" +
                    "<button class=\"delete-template template-5 link button-clr template-" + data.id + "\" data-url=\"/email/delete-template?id=" + data.id + "\" data-toggle=\"modal\" data-target=\"#delete-confirm-template\" data-id=\"" + data.id + "\"><svg class=\"svg-icon\"><use xlink:href=\"/img/svg/svgSprite.svg#garbage\"></use></svg></button>" +
                    "</div>" +
                    "</div>").find("input[value=\"" + data.id + "\"]").click();
                $.uniform.restore("[name=\'template\']");
                $("[name=\'template\']").uniform();
                if (data.canAdd == false) {
                    $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").hide();
                }
                $saveButton.hide();
                $saveButton.siblings(".glyphicon-pencil").show();
            }
            $("#new-template-name").val("");
        });
    });

    $(".template-email").click(function () {
        if ($templateBlock.is(":visible")) {
            $templateBlock.slideUp();
        } else {
            $(".field-reportcompaniesform-signature .form-actions").hide();
            $("textarea.email-signature").attr("disabled", true);
            $signatureBlock.slideUp();
            $templateBlock.slideDown();
        }
    });

    $(document).on("keydown", "#reportcompaniesform-emailtext", function() {
        textAreaAdjust($("#reportcompaniesform-emailtext")[0]);
    });

    $(document).on("change", "#template-variant-radio", function () {
        var message = $(this).find("input:checked").data("message");
        if (message) {
            $("#reportcompaniesform-emailtext").val(message);
        }
        textAreaAdjust($("#reportcompaniesform-emailtext")[0]);
    });

    $(document).on("click", ".delete-template", function () {
        $("#delete-confirm-template .delete-template-modal").attr("data-url", $(this).data("url")).attr("data-id", $(this).data("id"));
        return false;
    });

    $(document).on('click', "#delete-confirm-template .delete-template-modal", function () {
        var $modal = $(this).closest("#delete-confirm-template");
        var $this = $(this);

        $.post($(this).attr("data-url"), null, function (data) {
            if (data.result) {
                var $item = $("button.template-" + $this.attr("data-id")).closest(".row");

                $modal.modal("hide");
                $item.remove();
                $(".template-text-block label input:first").click();
                if (data.canAdd == true) {
                    $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").show();
                }
                $(".template-text-block label input:first").click();
            }
        });
    });

    $(document).on("click", ".update-template", function () {
        $item = $(this).closest(".row");
        $labelText = $item.find(".radio-txt");
        $labelText.hide();
        $labelText.after("<input type=\"text\" class=\"form-control update-template-name inline-block\" value=\"" + $labelText.text().trim() + "\">");
        $(this).hide();
        $(this).siblings(".delete-template").hide();
        $(this).after('<button class="undo-template button-clr link"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close"></use></svg></button>');
        $(this).after('<button class="save-template button-clr link mr-1" data-url="' + $(this).data("url") + '"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#check-2"></use></svg></button>');
        //$(this).after("<span class=\"fa fa-floppy-o fa-2x save-template\" data-url=\"" + $(this).data("url") + "\"></span>");
        //$(this).after("<span class=\"fa fa-reply fa-2x undo-template\"></span>");
        return false;
    });

    $(document).on("click", ".undo-template", function () {
        $item = $(this).closest(".row");

        $item.find(".update-template-name, .save-template, .undo-template").remove();
        $item.find(".delete-template, .update-template, .radio-txt").show();

        return false;
    });

    $(document).on("click", ".save-template", function () {
        $item = $(this).closest(".row");
        $labelText = $item.find(".radio-txt");

        $.post($(this).data("url"), {
            name: $item.find(".update-template-name").val(),
            text: $("#reportcompaniesform-emailtext").val(),
        }, function (data) {
            if (data.result) {
                $labelText.text(data.name);
                $item.find(".radio input").data("message", data.text);
            }
            $item.find(".update-template-name, .save-template, .undo-template").remove();
            $item.find(".delete-template, .update-template").show();
            $labelText.show();
        });

        return false;
    });

    $('#signEmail').on('click', function(e) {
        $(this).siblings('.dropdown-menu').toggleClass('visible show');
    });

    $('.email-sign .form-edit-result').on('click', function(e) {
        if (!$(e.target).closest('.link', '.email-sign').length)
            $('.email-sign .dropdown-menu').removeClass('visible show');
    });

    $('#tplEmail').on('click', function(e) {
        $(this).siblings('.dropdown-menu').toggleClass('visible show');
    });

    $(".undo-update-email-signature").click(function () {
        $(".update-email-signature").show();
        $(".save-email-signature, .undo-update-email-signature").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    $(".save-email-signature").click(function () {
        $.post($(this).data("url"), $("textarea.email-signature").serialize(), function (data) {
            if (data.result) {
                $('.form-edit-result > span').html(data.signature);
                $(".email-signature-message-text").html(data.signature);
            }
        });
    });

}(jQuery, window);
