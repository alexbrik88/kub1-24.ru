!function ($, window, undefined) {
    $employeesEmail = $(".contractor-employees-email");
    $signatureBlock = $(".field-invoicesendform-signature, .update-email-signature");
    $templateBlock = $(".field-invoicesendform-template");

    var sendMessagePanelOpen = function (sendToMe) {
        if (sendToMe) {
            $("#user-email-dropdown input:checkbox:checked:not(#invoicesendform-sendtome)").click();
            if (!$("#invoicesendform-sendtome").is(":checked")) {
                $("#invoicesendform-sendtome").click();
            }
            //$("form#send-document-form button:submit").closest("div").pulsate({
            //    color: "#bf1c56",
            //    reach: 20,
            //    repeat: 3
            //});
        }
        $(".send-message-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".send-message-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".send-message-panel .main-block").scrollTop(0);
        if ($("#invoicesendform-sendto").val().length < 1) {
            $("#invoicesendform-sendto_tag").click()
        }
        if ($(".send-message-tooltip-panel").length > 0) {
            $(".send-message-tooltip-panel").show(500);
        }
    }

    var sendMessagePanelClose = function() {
        $(".send-message-panel-trigger").removeClass("active");
        $(".send-message-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
        if ($(".send-message-tooltip-panel").length > 0) {
            $(".send-message-tooltip-panel").hide(500);
        }
        $("form#send-document-form button:submit").removeClass("pulse-button");
    }
    $(document).on("click", ".send-message-panel-trigger", function (e) {
        e.preventDefault();

        var sendToMe = false;
        if ($(this).hasClass("send-to-me")) {
            sendToMe = true;
        }
        sendMessagePanelOpen(sendToMe);
    });

    $("#invoicesendform-sendto_tag").focus(function () {
        $employeesEmail.slideDown();
        $templateBlock.slideUp();
        $signatureBlock.slideUp();
        $(".field-invoicesendform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    $("#invoicesendform-sendcopy_tag, #message-subject, #invoicesendform-emailtext").focus(function () {
        $employeesEmail.slideUp();
        $templateBlock.slideUp();
        $signatureBlock.slideUp();
        $(".field-invoicesendform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    // resize TextArea
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (10+o.scrollHeight)+"px";
    }

    $(document).ready(function () {
        $(".add-signature-email, .template-email, .block-files-email, input[name=\"emailFile\"]").click(function () {
            $employeesEmail.slideUp();
        });
        $("input[name=\"emailFile\"]").click(function () {
            $templateBlock.slideUp();
            $signatureBlock.slideUp();
            $(".field-invoicesendform-signature .form-actions").hide();
            $("textarea.email-signature").attr("disabled", true);
        });
        $("#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag")
        .attr("placeholder", "Укажите e-mail или выберите из списка")
        .addClass("visible");

        var text = $("#invoicesendform-emailtext").val(),
        matches = text.match(/\n/g),
        breaks = matches ? matches.length : 0;

        $("#invoicesendform-emailtext").attr("rows", breaks + 1);

        textAreaAdjust($("#invoicesendform-emailtext")[0]);
    });

    $(".save-email-template-ico").click(function() {
        return false;
    });

    $(".save-email-template").click(function () {
        $saveButton = $(this);
        $.post($(this).data("url"), $("#new-template-name, #invoicesendform-emailtext").serialize(), function (data) {
            $templateVariants = $(".template-variants");
            if (data.result) {

                $templateVariants.append("<div class='row flex-nowrap justify-content-between mb-2'><div class='column nowrap'><label class='radio-label'>" +
                    "<input type=\"radio\" name=\"template\" value=\"" + data.id +"\" data-message=\"" + data.text + "\">" +
                    "<span class=\"radio-txt nowrap\">" + data.name + "</span></label></div> " +
                    "<div class=\"column nowrap\"> " +
                    "<button class=\"update-template link button-clr mr-1\" data-url=\"/email/update-template?id=" + data.id + "\"><svg class=\"svg-icon\"><use xlink:href=\"/img/svg/svgSprite.svg#pencil\"></use></svg></button>" +
                    "<button class=\"delete-template template-5 link button-clr template-" + data.id + "\" data-url=\"/email/delete-template?id=" + data.id + "\" data-toggle=\"modal\" data-target=\"#delete-confirm-template\" data-id=\"" + data.id + "\"><svg class=\"svg-icon\"><use xlink:href=\"/img/svg/svgSprite.svg#garbage\"></use></svg></button>" +
                    "</div>" +
                "</div>").find("input[value=\"" + data.id + "\"]").click();
                $.uniform.restore("[name=\'template\']");
                $("[name=\'template\']").uniform();
                if (data.canAdd == false) {
                    $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").hide();
                }
                $saveButton.hide();
                $saveButton.siblings(".glyphicon-pencil").show();
            }
            $("#new-template-name").val("");
        });
    });

    $(".template-email").click(function () {
        if ($templateBlock.is(":visible")) {
            $templateBlock.slideUp();
        } else {
            $(".field-invoicesendform-signature .form-actions").hide();
            $("textarea.email-signature").attr("disabled", true);
            $signatureBlock.slideUp();
            $templateBlock.slideDown();
        }
    });

    $(document).on("keydown", "#invoicesendform-emailtext", function() {
        textAreaAdjust($("#invoicesendform-emailtext")[0]);
    });

    $(document).on("change", "#template-variant-radio", function () {
        $("#invoicesendform-emailtext").val($(this).find("input:checked").data("message"));
        textAreaAdjust($("#invoicesendform-emailtext")[0]);
    });

    $(document).on("click", ".delete-template", function () {
        $("#delete-confirm-template .delete-template-modal").attr("data-url", $(this).data("url")).attr("data-id", $(this).data("id"));
        return false;
    });

    $(document).on("click", ".delete-user-email", function () {
        $("#delete-confirm-user-email .delete-user-email-modal").attr("data-url", $(this).data("url")).attr("data-id", $(this).data("id"));
    });

    $("#delete-confirm-template .delete-template-modal").click(function () {
        var $modal = $(this).closest("#delete-confirm-template");
        var $this = $(this);

        $.post($(this).attr("data-url"), null, function (data) {
            if (data.result) {
                var $item = $("button.template-" + $this.attr("data-id")).closest(".row");

                $modal.modal("hide");
                $item.remove();
                $(".template-text-block label input:first").click();
                if (data.canAdd == true) {
                    $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").show();
                }
                $(".template-text-block label input:first").click();
            }
        });
    });

    $("#delete-confirm-user-email .delete-user-email-modal").click(function () {
        var $modal = $(this).closest("#delete-confirm-user-email");
        var $this = $(this);

        $.post($(this).attr("data-url"), null, function (data) {
            if (data.result) {
                var $item = $("span.user-email-" + $this.attr("data-id")).closest(".who-send-container");
                var $checkBox = $item.find("#invoicesendform-sendtouser");

                if ($checkBox.is(":checked")) {
                    $item.find("#invoicesendform-sendtouser").click();
                }
                $modal.modal("hide");
                $item.remove();
                if (data.canAdd == true) {
                    $("#user-email-dropdown .add-new-user-email-form").show();
                }
            } else {
                alert(data.msg);
            }
        });
    });

    $(document).on("click", ".update-template", function () {
        $item = $(this).closest(".row");
        $labelText = $item.find(".radio-txt");
        $labelText.hide();
        $labelText.after("<input type=\"text\" class=\"form-control update-template-name inline-block\" value=\"" + $labelText.text().trim() + "\">");
        $(this).hide();
        $(this).siblings(".delete-template").hide();
        $(this).after('<button class="undo-template button-clr link"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close"></use></svg></button>');
        $(this).after('<button class="save-template button-clr link mr-1" data-url="' + $(this).data("url") + '"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#check-2"></use></svg></button>');
        //$(this).after("<span class=\"fa fa-floppy-o fa-2x save-template\" data-url=\"" + $(this).data("url") + "\"></span>");
        //$(this).after("<span class=\"fa fa-reply fa-2x undo-template\"></span>");
        return false;
    });

    $(document).on("click", ".undo-template", function () {
        $item = $(this).closest(".row");

        $item.find(".update-template-name, .save-template, .undo-template").remove();
        $item.find(".delete-template, .update-template, .radio-txt").show();

        return false;
    });

    $(document).on("click", ".save-template", function () {
        $item = $(this).closest(".row");
        $labelText = $item.find(".radio-txt");

        $.post($(this).data("url"), {
            name: $item.find(".update-template-name").val(),
            text: $("#invoicesendform-emailtext").val(),
        }, function (data) {
            if (data.result) {
                $labelText.text(data.name);
                $item.find(".radio input").data("message", data.text);
            }
            $item.find(".update-template-name, .save-template, .undo-template").remove();
            $item.find(".delete-template, .update-template").show();
            $labelText.show();
        });

        return false;
    });

    $(document).on("change", "#invoicesendform-sendtoall", function (event) {
        $("#user-email-dropdown .who-send-container input:checkbox:not(.all):not(:disabled)")
            .prop('checked', $(this).is(":checked"))
            .uniform('refresh');
    });

    $(document).on("change", "#user-email-dropdown .who-send-container:not(.all) input:checkbox", function (event) {
        var hasUnchecked = $("#user-email-dropdown .who-send-container:not(.all) input:checkbox:not(:checked):not(:disabled)").length > 0;
        $("#invoicesendform-sendtoall").prop('checked', !hasUnchecked).uniform('refresh');
    });

    $(".add-contractor-email input[type=\"checkbox\"]").change(function () {
        var $this = $(this);
        var $labelBlock = $(this).closest(".who-send-container");
        var $blockWhoSendInput = $($labelBlock).find(".container-who-send-input");
        var $input = $blockWhoSendInput.find("input");

        $.post($(this).data("url"), $input.serialize(), function (data) {
            if (data.result == true) {
                $blockWhoSendInput.remove();
                $labelBlock.find("span.email-label").html(data.labelText);
                $labelBlock.find("input:checkbox").attr("data-value", data.email);
            }
        });
    });

    $(document).on("keyup change", "#new-template-name", function (e) {
        if ($(this).val() !== "") {
            $('.save-email-template').show();
            $('.save-email-template-ico').hide();
        } else {
            $('.save-email-template').hide();
            $('.save-email-template-ico').show();
        }
    });

    $(document).on("keyup change", ".container-who-send-input input", function (e) {
        var reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (reg.test($(this).val()) != false) {
            $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").removeAttr("disabled").uniform();
        } else {
            $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").attr("disabled", true).uniform();
        }
    });

    $(document).on("click", ".email-uploaded-files .one-file .delete-file", function () {
        var $oneFile = $(this).closest(".one-file");
        var $fileBlock = $(this).closest(".email-uploaded-files");
        $.post($(this).data("url"), null, function (data) {
            if (data.result) {
                var $needUploadFiles = [];

                $oneFile.remove();
                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#invoicesendform-sendemailfiles").val($needUploadFiles.join(", "));
                if ($fileBlock.find(".one-file:not(.template)").length == 0) {
                    $fileBlock.hide();
                }
            } else {
                alert(data.msg);
            }
        });
    });

    $(document).on("click", ".dropdown-email", function () {
        $(this).toggleClass("open-users");
        if ($(this).hasClass("open-users")) {
            $("#send-document-form .form-actions button").attr("disabled", true);
        } else {
            $("#send-document-form .form-actions button").removeAttr("disabled");
        }
    });
    $(document).on("click", "#user-email-dropdown", function(e) {
        if (!$(e.target).hasClass("delete-user-email") && !$(e.target).hasClass("update-user-email") &&
        !$(e.target).hasClass("undo-user-email") && !$(e.target).hasClass("save-user-email") &&
        !$(e.target).hasClass("add-emails")) {
            e.stopPropagation();
        } else {
            $(this).closest(".dropdown-email").removeClass("open-users");
            if ($(e.target).hasClass("add-emails")) {
                $tagsInput = $("#invoicesendform-sendto");
                $tagsInputVal = $tagsInput.val().split(",");
                $mainCheckBox = $("#user-email-dropdown .who-send-container.all input:checkbox");

                $("#user-email-dropdown .who-send-container:not(.all) input:checkbox").each(function () {
                    $email = $(this).attr("data-value");
                    if ($(this).is(":checked")) {
                        if ($tagsInputVal.indexOf($email) == -1) {
                            $tagsInput.addTag($email);
                        }
                    } else {
                        if ($tagsInputVal.indexOf($email) !== -1) {
                            $tagsInput.removeTag($email);
                        }
                    }
                });
                if ($tagsInput.val() == "") {
                    $(".field-invoicesendform-sendto").addClass("has-error");
                    $(".field-invoicesendform-sendto .help-block").text("Нужно выбрать получателя.");
                } else {
                    $(".field-invoicesendform-sendto").removeClass("has-error");
                    $(".field-invoicesendform-sendto .help-block").text("");
                    e.stopPropagation();
                    $("#send-document-form .form-actions button").removeAttr("disabled");
                    $(this).closest(".dropdown-email").removeClass("open-users");
                }
            }
        }
    });

    $(document).mouseup(function (e) {
        if ($(e.target).hasClass("page-shading-panel")) return;
        var div = $(".dropdown-email");
        if ($(e.target).hasClass("undo-adding-emails")) {
            div.removeClass("open-users");
        }
        if (!div.is(e.target) && div.has(e.target).length === 0 && !$(e.target).hasClass("page-shading-panel")) {
            div.removeClass("open-users");
        }

        if (!div.hasClass("open-users")) {
            $("#send-document-form .form-actions button").removeAttr("disabled");
        }
    });

    $(document).on("click", ".who-send-container .email-actions .save-user-email", function () {
        var $emailActionsBlock = $(this).closest(".email-actions");
        var $inputs = $emailActionsBlock.siblings(".update-user-email-block").find("input");
        var $requiredInput = $emailActionsBlock.siblings(".update-user-email-block").find("input#user-email");
        var $label = $emailActionsBlock.siblings(".checkbox").find("label");
        var $sendXhr = true;

        if ($requiredInput.val() == "") {
            $sendXhr = false;
        }
        if ($sendXhr) {
            var $this = $(this);
            $.post($(this).data("url"), $inputs.serialize(), function (data) {
                if (data.result == true) {
                    $label.find(".email-label").html(data.label);
                    if ($label.find("input#invoicesendform-sendtouser").is(":checked")) {
                        $label.find("input#invoicesendform-sendtouser").click();
                    }
                    $label.find("input#invoicesendform-sendtouser").attr("data-value", data.email);
                    $emailActionsBlock.find(".save-user-email, .undo-user-email").hide();
                    $emailActionsBlock.find(".update-user-email, .delete-user-email").show();
                    $emailActionsBlock.siblings(".checkbox").show();
                    $emailActionsBlock.siblings(".update-user-email-block").hide();
                }
            });
        }
    });

    $(document).on("click", ".who-send-container .email-actions .update-user-email", function () {
        var $emailActionsBlock = $(this).closest(".email-actions");

        $(this).closest(".who-send-container").css("margin-bottom", "10px").css("margin-top", "10px");
        $(this).hide();
        $(this).siblings(".delete-user-email").hide();
        $(this).siblings(".undo-user-email, .save-user-email").show();
        $emailActionsBlock.siblings(".checkbox").hide();
        $emailActionsBlock.siblings(".update-user-email-block").show();
    });

    $(document).on("click", ".who-send-container .email-actions .undo-user-email", function () {
        var $emailActionsBlock = $(this).closest(".email-actions");

        $(this).closest(".who-send-container").removeAttr("style");
        $(this).hide();
        $(this).siblings(".save-user-email").hide();
        $(this).siblings(".update-user-email, .delete-user-email").show();
        $emailActionsBlock.siblings(".checkbox").show();
        $emailActionsBlock.siblings(".update-user-email-block").hide();
    });

    var _$uploadButton = $(".block-files-email span.upload-file");
    var _uploadUrl = _$uploadButton.data("url");
    var _csrfParameter = _$uploadButton.data("csrf-parameter");
    var _csrfToken = _$uploadButton.data("csrf-token");
    var uploadData = {};
    uploadData[_csrfParameter] = _csrfToken;

    var uploader = new ss.SimpleUpload({
        button: _$uploadButton[0], // HTML element used as upload button
        url: _uploadUrl, // URL of server-side upload handler
        data: uploadData,
        multipart: true,
        multiple: true,
        multipleSelect: true,
        encodeCustomHeaders: true,
        responseType: "json",
        name: "emailFile", // Parameter name of the uploaded file
        onSubmit: function() {
            $("#file-ajax-loading").css("display", "inline-block");
        },
        onComplete: function (filename, response) {
            if (response["result"] == true) {
                var $templateFile = $(".email-uploaded-files .one-file.template").clone();
                var $needUploadFiles = [];

                $templateFile.removeClass("template");
                $templateFile.attr("data-id", response["id"]);
                $templateFile.prepend(response["previewImg"]);
                $templateFile.find(".file-name").text(response["name"]).attr("title", response["name"]);
                $templateFile.find(".file-size").text(response["size"] + " КБ");
                $templateFile.find(".delete-file").attr("data-url", response["deleteUrl"]);
                if (response["downloadUrl"]) {
                    $templateFile.find(".download-file").attr("href", response["downloadUrl"]).show();
                }
                $templateFile.show();
                $(".email-uploaded-files").append($templateFile);
                $("#file-ajax-loading").hide();
                $(".email-uploaded-files").show();

                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#invoicesendform-sendemailfiles").val($needUploadFiles.join(", "));
            } else {
                alert(response["msg"]);
                $("#file-ajax-loading").hide();
            }
        }
    });


    $('#signEmail').on('click', function(e) {
        $(this).siblings('.dropdown-menu').toggleClass('visible show');
    });
    $('.email-sign .form-edit-result').on('click', function(e) {
        if (!$(e.target).closest('.link', '.email-sign').length)
            $('.email-sign .dropdown-menu').removeClass('visible show');

        console.log(e.target);
    });

    $('#tplEmail').on('click', function(e) {
        $(this).siblings('.dropdown-menu').toggleClass('visible show');
    });

    //$(".add-signature-email").click(function () {
    //    if ($signatureBlock.is(":visible")) {
    //        $signatureBlock.slideUp();
    //        $(".field-invoicesendform-signature .form-actions").hide();
    //        $("textarea.email-signature").attr("disabled", true);
    //    } else {
    //        $templateBlock.slideUp();
    //        $signatureBlock.slideDown();
    //    }
    //});

    //$(".update-email-signature").click(function () {
    //    $(this).hide();
    //    $("textarea.email-signature").removeAttr("disabled");
    //    $(".save-email-signature, .undo-update-email-signature").css("display", "inline-block");
    //});

    $(".undo-update-email-signature").click(function () {
        $(".update-email-signature").show();
        $(".save-email-signature, .undo-update-email-signature").hide();
        $("textarea.email-signature").attr("disabled", true);
    });

    $(document).on("click", ".save-email-signature", function () {
        $.post($(this).data("url"), $("textarea.email-signature").serialize(), function (data) {
            if (data.result) {
                $('.form-edit-result > span').html(data.signature);
                $(".email-signature-message-text").html(data.signature);
            }
        });
    });


}(jQuery, window);
