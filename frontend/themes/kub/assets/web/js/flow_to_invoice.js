!function($) {
    $(document).on('click', '#link_invoices_modal .link_invoices_find', function (e) {
        let laddaButton = Ladda.create(this);
        laddaButton.start();
        $.ajax({
            url: $(this).data('url'),
            success: function (data) {
                $('#link_invoices_modal .content_step_2').html(data).toggleClass('hidden', false);
                $('#link_invoices_modal .content_step_1').toggleClass('hidden', true);
                laddaButton.stop();
            }
        });
    });
    $(document).on('change', '#link_invoices_modal .selected_flow', function (e) {
        $($(this).data('target'), this.form).prop('disabled', !this.checked).uniform('refresh');
    });
    $(document).on('hidden.bs.modal', '#link_invoices_modal', function (e) {
        $('#link_invoices_modal .content_step_2').html('').toggleClass('hidden', true);
        $('#link_invoices_modal .content_step_1').toggleClass('hidden', false);
    });
}(window.jQuery);
