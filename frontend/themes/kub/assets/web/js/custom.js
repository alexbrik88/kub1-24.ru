/**
 * Getting number precision
 * @return integer
 */
Number.prototype.precision = function () {
    if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
    return this.toString().split(".")[1].length || 0;
};

$.pjax.defaults.timeout = 20000;

$(function () {
    var selectedProdType = null;
    // create invoice facture
    var $modal = $('#create-invoice-facture-modal');
    $(document).on('click', '.invioce-facture-create', function (e) {
        e.preventDefault();
        $modal.find('form').attr('action', $(this).attr('url'));
        $modal.modal('show');
    });


    // create invoice
    window.INVOICE = {
        productSeletedToInvoice: [],
        $productAddExistedButton: $('#product-add-existed'),
        $addToInvoiceButton: $('#add-to-invoice-button'),
        $existedProductForm: $('#products_in_order'),
        $productNewBlock: $('#add-new'),
        $productAddNewButton: $('#product-add-new'),
        $addNewContractor: $('contractor-add-new'),
        $productTypeBlock: $('.invoice-production-type-block'),
        $invoiceTable: $('#product-table-invoice'),
        $invoiceSum: $('#invoice-sum'),
        $btnProductSearch: $('#btn-product-search'),
        documentIoType: $('#invoice-form').data('iotype') || $('#first-invoice-form').data('iotype'),
        InvoiceOut: 2,
        InvoiceIn: 1,
        CurrencyRateRequestData: [],
        InvoiceAmountRequestData: [],
        getFloatPrice: function (price) {
            return (price / 100).toFixed(2);
        },
        addProductToTable: function (items) {
            if (items) {
                var isForeign = window.location.href.indexOf("foreign-currency-invoice") > -1;
                var $template = INVOICE.$invoiceTable.find('.template').first().clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input, select').attr('disabled', false);
                var $itemArray = [];
                var itemNumber = parseInt($('#table-product-list-body').data('number')) || 0;
                var documentIoType = $('#documentType').val();

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? (
                        isForeign && item.productUnit.intl_short_name !== null && item.productUnit.intl_short_name.length > 0 ?
                        item.productUnit.intl_short_name :
                        item.productUnit.name
                    ) : '---';

                    var totalQuantity = item.hasEstimate ? item.totalQuantity : 1;

                    $item = $template.clone();

                    $item.find('[name^="orderArray"]').each(function () {
                        this.name = this.name.replace(/orderArray\[\]/, 'orderArray[' + itemNumber + ']');
                    });
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-article').html(item.article || '&nbsp;');
                    $item.find('.product-delete').find('.remove-product-from-invoice').data('id', item.id);
                    $item.find('.product-title')
                        .val(isForeign && item.title_en !== null && item.title_en.length > 0 ? item.title_en : item.title);

                    if (documentIoType == INVOICE.InvoiceOut) {
                        if (item.priceForSellNds) {
                            $item.find('.tax-rate').val(item.priceForSellNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    } else {
                        if (item.priceForBuyNds) {
                            $item.find('.tax-rate').val(item.priceForBuyNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    }

                    $item.find('.order-id').val('');
                    $item.find('.product-id').val(item.id);
                    $item.find('.max-product-count').val(totalQuantity);
                    if ($item.find('.product-unit-name').find('select').length) {
                        $item.find('.product-unit-name').find('select')
                            .attr('id', 'orderArray__unit_' + itemNumber)
                            .addClass('select2me')
                            .removeAttr('disabled')
                            .val(item.productUnit ? item.productUnit.id : null);
                    } else {
                        $item.find('.product-unit-name').text(unitName);
                    }

                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        //.attr('max', Math.round(totalQuantity))
                        .val(totalQuantity);
                    $item.find('.max-product-count')
                        .val(totalQuantity);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    } else if (unitName == 'ч') {
                        $item.find('.product-count').attr('type', 'text');
                        $item.find('.product-count').attr('data-hour', 'true');
                    }

                    $item.find('.weight-input').val(item.weight);
                    $item.find('.volume-input').val(item.volume);

                    var itemNds = (documentIoType == INVOICE.InvoiceOut ? item.priceForSellNds : item.priceForBuyNds);
                    if (itemNds) {
                        $item.find('.price-for-sell-nds-name').attr('data-id', itemNds.id).attr('data-name', itemNds.name)
                            .find('select')
                            .attr('name', 'orderArray[' + itemNumber + '][' + (documentIoType == INVOICE.InvoiceOut ? 'sale_tax_rate_id' : 'purchase_tax_rate_id') + ']')
                            .attr('id', 'orderArray__nds_' + itemNumber)
                            .addClass('select2me')
                            .removeAttr('disabled')
                            .val(itemNds.id);
                    }

                    var viewType = parseInt($('#invoice-nds_view_type_id').val());

                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }

                    var priceForSell = 0;

                    let priceForSellNdsRate = 0;
                    let priceForBuyNdsRate = 0;
                    if (item.priceForSellNds !== null) {
                        priceForSellNdsRate = Number(item.priceForSellNds.rate);
                    }

                    if (item.priceForBuyNds !== null) {
                        priceForBuyNdsRate = Number(item.priceForBuyNds.rate);
                    }

                    if (item.price) {
                        var priceForSell = item.hasNds == 1 ? Math.round(item.price * (priceForSellNdsRate + 1)) : item.price;
                    } else {
                        priceForSell = viewType == 1 ? Math.round(item.price_for_sell_with_nds / (priceForSellNdsRate + 1)) : item.price_for_sell_with_nds;
                    }

                    var priceForBuy = viewType == 1 ? Math.round(item.price_for_buy_with_nds / (priceForBuyNdsRate + 1)) : item.price_for_buy_with_nds;
                    var priceForWithNds = documentIoType == INVOICE.InvoiceOut ? priceForSell : priceForBuy;

                    $item.find('.price-input').val(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-one-with-nds').text(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-with-nds').text(INVOICE.getFloatPrice((priceForWithNds)));

                    // For PriceList
                    if ($('#price-list-form').length) {
                        if (item.weight > 0 && item.weight == parseInt(item.weight)) {
                            item.weight = Math.round(item.weight);
                        } else {
                            item.weight = Number(item.weight);
                        }
                        $item.find('.product-group-name').text(item.group_name);
                        $item.find('.product-comment').text(item.comment_photo);
                        $item.find('.product-production-type').val(item.production_type);
                        $item.find('.product-weight').text(item.weight);
                        $item.find('.product-netto').text(item.mass_net);
                        $item.find('.product-brutto').text(item.mass_gross);
                        $item.find('.product-custom_field').text(item.custom_field_value);
                        $item.find('.product-count_in_package').text(item.count_in_package);
                        $item.find('.product-box_type').text(item.box_type);
                        if (item.productStores.length) {
                            var reserveCol = $item.find('.product-quantity');
                            var quantityAllStores = 0;
                            $.each(item.productStores, function(i,v) {
                                $(reserveCol).attr('data-store-' + v.store_id, parseFloat(v.quantity));
                                quantityAllStores += parseFloat(v.quantity);
                            });
                            $(reserveCol).attr('data-store-all', quantityAllStores);
                        }
                        if (item.categoryColumnValues) {
                            $.each(item.categoryColumnValues, function(i,v) {
                                $item.find('.col_product_category_column_' + i).text(v);
                            });
                        }
                    }

                    // For ScanRecognize
                    if (item.quantity > 0) {
                        $item.find('.product-count').val(item.quantity);
                    }
                    if (item.unknown_product_has_similar) {
                        $item.find('.product-title').after(this.getRecognizeLink(item, 'Б) Выбрать из похожих', 'exists'));
                    }
                    if (item.unknown_product_has_similar && item.unknown_product) {
                        $item.find('.product-title').after('<br/>');
                    }
                    if (item.unknown_product) {
                        $item.find('.product-title').after(this.getRecognizeLink(item, 'А) Добавить как новый', 'new'));
                    }

                    $itemArray.push($item);
                    itemNumber++;
                }
                for (i in $itemArray) {
                    $itemArray[i].show().insertBefore('#from-new-add-row');
                }
                // create select2
                $('#table-product-list-body').find('.select2me').each(function() {
                    createSimpleSelect2($(this).attr('id'));
                    $(this).removeClass('select2me');
                });

                // For PriceList
                if ($('#price-list-form').length) {
                    window.PriceListForm.refreshMarkupDiscount();
                }

                if (!$("th.discount_column").hasClass("hidden")) {
                    $("table .discount-input").each(function () {
                        INVOICE.recalculateItemPrice($(this));
                    });
                }
                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
                INVOICE.tultipProduct('.tooltip-product');
                INVOICE.recalculateInvoiceReady();
                $('#table-product-list-body').data('number', itemNumber);
                if ($('#invoice-is_invoice_contract').val() == 1) {
                    checkContractEssenceTemplate();
                }

                return true;
            }

            return false;
        },
        replaceProductInTable: function (items, pos) {
            if (items) {
                var isForeign = window.location.href.indexOf("foreign-currency-invoice") > -1;
                var $template = INVOICE.$invoiceTable.find('.template').clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input').attr('disabled', false);
                var $itemArray = [];
                var $posElement = $('#table-product-list-body').find('tr').eq(pos);
                var itemNumber = parseInt($('#table-product-list-body').data('number')) || 0;
                var quantity = parseInt($posElement.find('.unknown_product').data('quantity')) || 0;
                var documentIoType = $('#documentType').val();

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? (
                        isForeign && item.productUnit.intl_short_name !== null && item.productUnit.intl_short_name.length > 0 ?
                        item.productUnit.intl_short_name :
                        item.productUnit.name
                    ) : '---';
                    $item = $template.clone();
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-article').html(item.article || '&nbsp;');
                    $item.find('.product-delete').find('.remove-product-from-invoice').data('id', item.id);
                    $item.find('.product-title')
                        .attr('name', 'orderArray[' + itemNumber + '][title]')
                        .val(isForeign && item.title_en !== null && item.title_en.length > 0 ? item.title_en : item.title);

                    if (documentIoType == INVOICE.InvoiceOut) {
                        if (item.priceForSellNds) {
                            $item.find('.tax-rate').val(item.priceForSellNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    } else {
                        if (item.priceForBuyNds) {
                            $item.find('.tax-rate').val(item.priceForBuyNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    }

                    $item.find('.order-id')
                        .attr('name', 'orderArray[' + itemNumber + '][id]')
                        .val('');
                    $item.find('.product-id')
                        .attr('name', 'orderArray[' + itemNumber + '][product_id]')
                        .val(item.id);

                    $item.find('.product-unit-name').text(unitName);
                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        .attr('name', 'orderArray[' + itemNumber + '][count]')
                        .val(1);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    } else if (unitName == 'ч') {
                        $item.find('.product-count').attr('type', 'text');
                        $item.find('.product-count').attr('data-hour', 'true');
                    }

                    $item.find('.price-input')
                        .attr('name', 'orderArray[' + itemNumber + '][price]');
                    $item.find('.discount-input')
                        .attr('name', 'orderArray[' + itemNumber + '][discount]');
                    $item.find('.markup-input')
                        .attr('name', 'orderArray[' + itemNumber + '][markup]');
                    $item.find('.weight-input')
                        .attr('name', 'orderArray[' + itemNumber + '][weight]').val(item.weight);
                    $item.find('.volume-input')
                        .attr('name', 'orderArray[' + itemNumber + '][volume]').val(item.volume);

                    var itemNds = (documentIoType == INVOICE.InvoiceOut ? item.priceForSellNds : item.priceForBuyNds);
                    $item.find('.price-for-sell-nds-name').text(itemNds.name).attr('data-id', itemNds.id).attr('data-name', itemNds.name);

                    var viewType = parseInt($('#invoice-nds_view_type_id').val());

                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }

                    var priceForSell = viewType == 1 ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;
                    var priceForBuy = viewType == 1 ? Math.round(item.price_for_buy_with_nds / (Number(item.priceForBuyNds.rate) + 1)) : item.price_for_buy_with_nds;
                    var priceForWithNds = documentIoType == INVOICE.InvoiceOut ? priceForSell : priceForBuy;

                    $item.find('.price-input').val(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-one-with-nds').text(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-with-nds').text(INVOICE.getFloatPrice((priceForWithNds)));

                    // For ScanRecognize
                    $item.find('.product-count').val(quantity);

                    $itemArray.push($item);
                    itemNumber++;
                }

                for (i in $itemArray) {
                    $itemArray[i].show().insertAfter($posElement);
                }

                $posElement.remove();

                if (!$("th.discount_column").hasClass("hidden")) {
                    $("table .discount-input").each(function () {
                        INVOICE.recalculateItemPrice($(this));
                    });
                }
                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
                INVOICE.tultipProduct('.tooltip-product');
                INVOICE.recalculateInvoiceReady();
                $('#table-product-list-body').data('number', itemNumber);
                if ($('#invoice-is_invoice_contract').val() == 1) {
                    checkContractEssenceTemplate();
                }

                return true;
            }

            return false;
        },
        recalculateInvoiceTable: function () {
            var discount = 0,
                total = 0,
                tax = 0,
                amount = 0,
                weight = 0,
                volume = 0,
                ndsViewId = parseInt($('#invoice-nds_view_type_id').val()) || parseInt($('#projectestimate-nds_out').val());
            items = this.getItems();

            var productWeightPrecision = 2;
            var productVolumePrecision = 2;
            items.each(function () {
                var $row = $(this);
                var productWeight = parseFloat($row.find('.weight-input').val() || 0);
                var productVolume = parseFloat($row.find('.volume-input').val() || 0);
                productWeightPrecision = Math.max((productWeight + '').indexOf('.') > 0 ? (productWeight + '').split('.')[1].length : 0, productWeightPrecision);
                productVolumePrecision = Math.max((productVolume + '').indexOf('.') > 0 ? (productVolume + '').split('.')[1].length : 0, productVolumePrecision);
            });
            productWeightPrecision = Math.min(4, productWeightPrecision);
            productVolumePrecision = Math.min(4, productVolumePrecision);

            items.each(function () {
                var $row = $(this);
                var taxRate = parseFloat($row.find('.tax-rate').val() || 0),
                    productBasePrice = parseFloat($row.find('.price-input').val()),
                    productPrice = parseFloat($row.find('.price-one-with-nds').eq(0).text()),
                    productCount = parseFloat($row.find('.product-count').val()),
                    productWeight = parseFloat($row.find('.weight-input').val() || 0),
                    productVolume = parseFloat($row.find('.volume-input').val() || 0),

                    productTax = 0,
                    productTotal = 0,
                    productDiscount = 0;

                productTotal = (productPrice || productBasePrice) * productCount;

                if (ndsViewId === 2) {
                    productTax = 0;
                } else if (ndsViewId === 1) {
                    productTax = productTotal * taxRate;
                } else {
                    productTax = productTotal * (taxRate / (1 + taxRate));
                }

                if (productBasePrice > productPrice) {
                    productDiscount = (productBasePrice - productPrice) * productCount;
                }

                discount += productDiscount;
                total += productTotal;
                tax += productTax;

                weight += productWeight * productCount;
                volume += productVolume * productCount;
            });

            amount = (ndsViewId === 1) ? total + tax : total;

            $('.table-resume .discount_sum').text(discount.toFixed(2));
            $('.table-resume .total_price').text(total.toFixed(2));
            $('.table-resume .including_nds').text(tax.toFixed(2));
            $('.table-resume .amount').text(amount.toFixed(2));
            $('.table-resume .total_weight').text(weight.toFixed(productWeightPrecision));
            $('.table-resume .total_volume').text(volume.toFixed(productVolumePrecision));
        },
        getItem: function (id) {
            return this.$invoiceTable.find('#model_' + id);
        },
        recalculateItemPrice: function ($input) {
            var priceOne;
            var precision = parseInt($("#invoice-price_precision").val() || 2);
            var ratio = Math.pow(10, precision);
            var $row = $input.closest('tr');
            var $priceInput = $row.find('.price-input');
            var $discountInput = $row.find('.discount-input');
            var $markupInput = $row.find('.markup-input');
            var price = Math.round($priceInput.val() * ratio) / ratio || 0;
            var markup = 0;
            var discount = 0;
            var discountType = $('#invoice-discount_type').val();
            var $countInput = $row.find('.product-count');
            var countValue = $countInput.val();
            var count;
            if ($countInput.attr('type') == 'number') {
                count = countValue || 0;
            } else {
                if ($countInput.data('hour') && countValue.includes(':')) {
                    var countItems = countValue.split(':', 3);
                    countItems.forEach(function(el, i) {
                        if (i == 0) {
                            count = el;
                        } else if (i == 1) {
                            count = (count * 1 + (el.substring(0, 2) * 1) / 60).toFixed(2);
                        } else if (i == 2) {
                            count = (count * 1 + (el.substring(0, 2) * 1) / 3600).toFixed(4);
                        }
                    });
                } else {
                    count = countValue.replace(',', '.').replace(/[^\d\.]/g, '').split('.', 2).join('.');
                }
                count = count || '0';
            }
            count = count.replace(/^\./,"0.").replace(/(\.0*)$/,"");
            if (count != countValue) {
                $countInput.val(count);
            }

            if ($('#invoice-has_markup').prop('checked') || $('#pricelist-has_markup').prop('checked')) {
                markup = Math.min(9999.9999, Math.round($markupInput.val() * 10000) / 10000 || 0);
                priceOne = Math.round((price * (100 + markup) / 100) * ratio) / ratio;
            } else if ($('#invoice-has_discount').prop('checked') || $('#pricelist-has_discount').prop('checked')) {
                if (discountType == '1') {
                    discount = Math.round($discountInput.val() * 10000) / 10000 || 0;
                    priceOne = Math.round(Math.max((price - discount), 0) * ratio) / ratio;
                } else {
                    discount = Math.min(100.0000, Math.round($discountInput.val() * 1000000) / 1000000 || 0);
                    discount = INVOICE.adjustDiscount(
                        price,
                        discount,
                        parseInt($('#invoice-price_round_rule').val()),
                        parseInt($('#invoice-price_round_precision').val())
                    );
                    priceOne = Math.round((price * (100 - discount) / 100) * ratio) / ratio;
                }
            } else {
                priceOne = price;
            }
            if ($priceInput.val() != price) {
                $priceInput.val(price.toFixed(precision));
            }
            if ($markupInput.val() != markup) {
                $markupInput.val(markup);
            }

            $row.find('.price-one-with-nds').text(priceOne.toFixed(precision));
            $row.find('.price-with-nds').text((priceOne * count).toFixed(precision));
        },
        adjustDiscount: function (price, discount, roundRule, roundPrecision) {
            if (discount == 0 || !roundRule || !roundPrecision || roundRule == 1) {
                return discount;
            }
            var p = price * 100;
            var k;
            switch (roundPrecision) {
                case 2:
                    k = 10;
                    break;
                case 3:
                    k = 100;
                    break;
                case 1:
                default:
                    k = 1;
                    break;
            }
            var f;
            switch (roundRule) {
                case 3:
                    f = 'ceil';
                    break;
                case 4:
                    f = 'floor';
                    break;
                case 2:
                default:
                    f = 'round';
                    break;
            }
            var s = Math[f](p * (100 - discount) / 100 / k);

            return Math.round((100 - s * k * 100 / p) * 1000000) / 1000000;
        },
        incrementProductCount: function ($item) {
            var $input = $item.find('.product-count');

            $input.val(parseInt($input.val()) + 1);

            this.recalculateItemPrice($input);
            this.recalculateInvoiceTable();
        },
        getItems: function () {
            return INVOICE.$invoiceTable.find('.product-row');
        },
        checkProductTypeEnabled: function () {
            var disabled = this.getItems().length != 0;
            var $radioArray = this.$productTypeBlock.find('input.invoice-production-type');

            this.$productTypeBlock.find('input.invoice-production-type-hidden')
                .val($radioArray.filter(':checked').val());
        },
        searchProduct: function (type, doctype) {
            var exists = $('input.selected-product-id').map(function (idx, elem) {
                return $(elem).val();
            }).get();
            var store_id = $('input#invoice-store_id, select#orderdocument-store_id').first().val() || null;
            var contractorId = $('select#invoice-contractor_id').val();

            $.pjax({
                url: '/product/get-products',
                container: '#pjax-product-grid',
                data: {documentType: doctype, productType: type, exists: exists, store_id: store_id, contractorId: contractorId},
                push: false,
                timeout: 10000,
                scrollTo: false,
            });
            $('#add-from-exists').modal();
        },
        saveNewProduct: function ($modal) {
            var form = $('#new-product-invoice-form').serialize();
            var productionType = 1;//INVOICE.getProductionType();

            $.post('/product/create-from-invoice?productionType=' + productionType, form, function (data) {

                INVOICE.addProductToTable(data);
            });
        },
        getProductionType: function () {
            return INVOICE.$productTypeBlock.find('input.invoice-production-type:checked').val();
        },
        addNewProduct: function (url, form) {
            $.post(
                url, //+'?productionType=2&document_type=1',
                form,
                function (data) {
                    $('#add-new').modal();
                    if (data) {
                        if (data.header !== undefined) {
                            $('#add-new .modal-title').html(data.header);
                            $('#block-modal-new-product-form').html(data.body);
                            $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                            var urlunits = $('#get-product-units-url').val();
                            $("#product-production_type").on("change", "input", function () {
                                $('#new-product-invoice-form input, #new-product-invoice-form select').attr('disabled', false);
                                $('#new-product-invoice-form input:radio:not(.md-radiobtn), #new-product-invoice-form input:checkbox:not(.md-check)').uniform("refresh");
                                $('#new-product-invoice-form input[type="text"]').val('');
                                $('.error-summary').remove();
                                if ($(this).val() == '0') {
                                    $('.forProduct').addClass('selectedDopColumns');
                                    $('.forService').removeClass('selectedDopColumns');
                                    $('#product-goods-group_id').attr('disabled', true);
                                    $('#product-service-group_id').removeAttr('disabled');
                                    $('.field-product-product_unit_id').removeClass('required');
                                    $('.import-products').hide();
                                    $('.new_product_group_input').attr('placeholder', 'Добавить новую группу услуг');
                                } else {
                                    $('.forProduct').removeClass('selectedDopColumns');
                                    $('#product-goods-group_id').removeAttr('disabled');
                                    $('#product-service-group_id').attr('disabled', true);
                                    $('.forService').addClass('selectedDopColumns');
                                    $('.import-products').show();
                                    $('.field-product-product_unit_id').addClass('required');
                                    $('.new_product_group_input').attr('placeholder', 'Добавить новую группу товаров');
                                }
                                if ($('#is_first_invoice').val() == '1') {
                                    if ($(this).val() == '0') {
                                        $('#product-title').attr('placeholder', 'Например: Доставка товара');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 500 (указывать только цифры)');
                                    }
                                    if ($(this).val() == '1') {
                                        $('#product-title').attr('placeholder', 'Например: Доска обрезная');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 1000 (указывать только цифры)');
                                    }
                                }

                                $.post(urlunits, {
                                    productType: $(this).val()
                                }).done(function (data) {
                                    if (data.html) {
                                        $('#product-product_unit_id').html($(data.html).html());
                                    }
                                });
                            });
                            if ($('#new-product-invoice-form #product-production_type input').length == 1) {
                                $("#new-product-invoice-form #product-production_type input").click().prop("checked", true);
                            }
                        } else if (data.items !== undefined) {
                            var prodCount = data.services + data.goods;

                            $('.order-add-static-items .service-count-value').text(data.services);
                            if (data.services > 0) {
                                $('li.select2-results__option.add-modal-services').removeClass('hidden');
                            }
                            $('.order-add-static-items .product-count-value').text(data.goods);
                            if (data.goods > 0) {
                                $('li.select2-results__option.add-modal-products').removeClass('hidden');
                            }
                            if ($('.add-exists-product.hidden').length && prodCount > 0) {
                                $('.add-first-product-button').addClass('hidden');
                                $('.add-exists-product.hidden').removeClass('hidden');
                            }

                            if ($("#out-invoice-form").length) {
                                var params = {
                                    'in_order': data.items[0].id,
                                    'productType': data.items[0].production_type
                                };
                                $.post('/out-invoice/products-table', params, function (data) {
                                    if (data.result) {
                                        $(data.result).insertBefore("#new-product-row");
                                    }
                                    $('#new-product-row').addClass('hidden');
                                });
                            } else if ($('#unknown_product_recognized_id').length && $('#unknown_product_recognized_id').val()) {
                                // add recognized product
                                var current_recognized_id = $('#unknown_product_recognized_id').val();
                                var pos = $('.unknown_product').filter('[data-recognized_id="' + current_recognized_id + '"]').first().parents('tr').index();
                                INVOICE.replaceProductInTable(data.items, pos);
                                $('#from-new-add-row').hide();
                                $('#unknown_product_recognized_id').val('');
                            } else {
                                INVOICE.addProductToTable(data.items);
                                $('#from-new-add-row').hide();
                            }

                            if ($('#redirect-after-add-product').length) {
                                var production_type = $("#production_type").val();
                                var product_id = data.items[0].id;
                                location.href = '/product/view?productionType=' + production_type + '&id=' + product_id;
                            } else {

                                $('#add-new').modal('hide');
                            }

                        } else if (data.id && $("#new_product_id").length) {
                            $('#add-new').modal('hide');
                            $("#new_product_id").val(data.id).trigger("change");
                        } else {
                            $('#add-new').modal('hide');
                        }
                    }

                    $("input[type=\"checkbox\"]").uniform('refresh');
                    $("input[type=\"radio\"]").uniform('refresh');
                }
            );
        },
        closeModalDetailsAddress: function (data) {
            $('span.username').text(data.type + ' "' + data.name + '"');
            $('#add-new').modal('hide');
            $('.your-company').hide();
            $('[name="Company[strict_mode]"]').remove();
            if ($('*').is('.company-strict-mode-error')) {
                $('.company-strict-mode-error').parent().remove();
            }
            $('.company-in-strict-mode').remove();
            if ($('.error-summary ul').children().length > 1) {
                $('li.strict-mode-error').remove();
            } else {
                $('.error-summary').hide();
            }
            $('.create-invoice').show();

            return true;
        },
        addDetailsAddress: function (url, form) {
            $.post(
                url,
                form,
                function (data) {
                    console.log(data);
                    $('#add-new').modal();
                    if (data.hasOwnProperty('header') && data.hasOwnProperty('body')) {
                        $('#add-new .modal-title').html(data.header);
                        $('#block-modal-new-product-form').html(data.body);
                        if ($('#block-modal-new-product-form').find('.dopColumns').length > 0) {
                            var dopData = $('#block-modal-new-product-form').find('.dopColumns');
                            if (dopData.find('.has-error').length > 0) {
                                dopData.removeClass('selectedDopColumns');
                            }
                        }
                        $('a.btn').click(function (event) {
                            event.preventDefault();
                            $('#add-new').modal('hide');
                        });
                    } else if (data.hasOwnProperty('type') && data.hasOwnProperty('name')) {
                        if (data.type == 'ИП') {
                            $('span.username').text(data.type + ' ' + data.name);
                        } else {
                            $('span.username').text(data.type + ' "' + data.name + '"');
                        }
                        $('#add-new').modal('hide');
                        $('.your-company').siblings('br').first().remove();
                        $('.your-company').remove();
                        if ($('li.strict-mode-error').length) {
                            if ($('.error-summary ul').children().length > 1) {
                                $('li.strict-mode-error').remove();
                            } else {
                                $('.error-summary').hide();
                            }
                        }
                        if ($('.company-in-strict-mode').length) {
                            $('.company-in-strict-mode').removeClass('company-in-strict-mode');
                        }
                        if ($('.block-pricelist-new-company').length) {
                            var $priceListBlock = $(".block-price-list, #create-price-list-form .form-actions");

                            $('.block-pricelist-new-company').remove();
                            if (!$('.block-pricelist-add-products').length) {
                                $('.block-pricelist-complete').remove();
                                $('.block-price-options').remove();
                                $('#create-price-list-form .submit').tooltipster('destroy');
                                $('#create-price-list-form .submit').removeClass('disabled');
                                $priceListBlock.slideDown();
                            }
                        }
                    } else if (data.hasOwnProperty('is_payment_order_page')) {
                        $('#add-new').modal('hide');
                        // Company info
                        $('.company_inn_button').remove();
                        $('.company_inn').html(data.company_inn);
                        $('.company_kpp').html(data.company_kpp);
                        $('.company_name').html(data.company_name);
                        // Company bank info
                        $('.company_bank_button').remove();
                        $('input[name="PaymentOrder[company_bank_name]"]').val(data.company_bank_name);
                        $('input[name="PaymentOrder[company_bik]"]').val(data.company_bik).show();
                        //$('input[name="PaymentOrder[company_rs]"]').val(data.company_rs);
                        $('input[name="PaymentOrder[company_ks]"]').val(data.company_ks);

                        var newOption = "<option value='" + data.company_rs + "' " +
                            "data-bank='" + data.company_bank_name + "' " +
                            "data-bik='" + data.company_bik + "' " +
                            "data-ks='" + data.company_ks + "' " +
                            " selected>" + data.company_rs +
                        "</option>";
                        $('#paymentorder-company_rs').append(newOption).trigger('change');

                    } else {
                        alert('Ошибка!');
                    }
                    INVOICE.recalculateInvoiceReady();
                }
            );
        },
        addLogo: function (url, form) {
            $.post(
                url,
                form,
                function (data) {
                    $('#add-new').modal();
                    if (data.header !== undefined) {
                        $('#add-new .modal-header').html(data.header);
                        $('#block-modal-new-product-form').html(data.body);
                        $('a.btn').click(function (event) {
                            event.preventDefault();
                            $('#add-new').modal('hide');
                        });
                    } else {
                        $('.detal').hide();
                    }
                }
            );
        },
        addNewContractor: function (url, form, input) {
            var $contractorInput = null;
            if ($("select#product_supplier").length) {
                $contractorInput = $("#product_supplier");
            } else if ($("select#invoice-contractor_id").length) {
                $contractorInput = $("#invoice-contractor_id");
            } else if ($("#orderdocument-contractor_id").length) {
                $contractorInput = $("#orderdocument-contractor_id");
            } else if ($("#vehicle-contractor_id").length) {
                $contractorInput = $("#vehicle-contractor_id");
            } else if ($("#driver-contractor_id").length) {
                $contractorInput = $("#driver-contractor_id");
            } else if ($("#logisticsrequest-customer_id").length && $("#logisticsrequest-carrier_id").length) {
                if (form.type == 2 || $("#new-contractor-invoice-form:visible").find("input:hidden[name=\"type\"]").val() == 2) {
                    $contractorInput = $("#logisticsrequest-customer_id");
                } else {
                    $contractorInput = $("#logisticsrequest-carrier_id");
                }
            } else if ($("#articleform-customers").length || $("#articleform-sellers").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('articleform-customers');
                    $contractorInput = $("#articleform-customers");
                } else {
                    $('#adding-contractor-from-input').val('articleform-sellers');
                    $contractorInput = $("#articleform-sellers");
                }
            } else if ($("#updatearticleform-customers").length || $("#updatearticleform-sellers").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('updatearticleform-customers');
                    $contractorInput = $("#updatearticleform-customers");
                } else {
                    $('#adding-contractor-from-input').val('updatearticleform-sellers');
                    $contractorInput = $("#updatearticleform-sellers");
                }
            } else if ($("#upd-consignor_id").length && $("#upd-consignee_id").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('upd-consignor_id');
                    $contractorInput = $("#upd-consignor_id");
                } else {
                    $('#adding-contractor-from-input').val('upd-consignee_id');
                    $contractorInput = $("#upd-consignee_id");
                }
            } else if ($("#packinglist-consignor_id").length && $("#packinglist-consignee_id").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('packinglist-consignor_id');
                    $contractorInput = $("#packinglist-consignor_id");
                } else {
                    $('#adding-contractor-from-input').val('packinglist-consignee_id');
                    $contractorInput = $("#packinglist-consignee_id");
                }
            }  else if ($("#waybill-consignor_id").length && $("#waybill-consignee_id").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('waybill-consignor_id');
                    $contractorInput = $("#waybill-consignor_id");
                } else {
                    $('#adding-contractor-from-input').val('waybill-consignee_id');
                    $contractorInput = $("#waybill-consignee_id");
                }
            } else if ($("#invoicefacture-consignor_id").length && $("#invoicefacture-consignee_id").length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('invoicefacture-consignor_id');
                    $contractorInput = $("#invoicefacture-consignor_id");
                } else {
                    $('#adding-contractor-from-input').val('invoicefacture-consignee_id');
                    $contractorInput = $("#invoicefacture-consignee_id");
                }
            } else if ($('#loadingandunloadingaddress-consignor_id').length && $('#loadingandunloadingaddress-consignee_id').length) {
                if (form.type == undefined) {
                    $contractorInput = $("#" + $('#adding-contractor-from-input').val());
                } else if (form.type == 2) {
                    $('#adding-contractor-from-input').val('loadingandunloadingaddress-consignor_id');
                    $contractorInput = $("#loadingandunloadingaddress-consignor_id");
                } else {
                    $('#adding-contractor-from-input').val('loadingandunloadingaddress-consignee_id');
                    $contractorInput = $("#loadingandunloadingaddress-consignee_id");
                }
            } else if ($("#agreement-contractor_id").length) {
                $contractorInput = $("#agreement-contractor_id");
            } else if ($("#project-customer_id").length) {
                $contractorInput = $("#project-customer_id");
            } else if ($("#customer_contractor_id").length || $("#seller_contractor_id").length) {
                if ($("#customer_contractor_id").parents('.form-group').is(':visible'))
                    $contractorInput = $("#customer_contractor_id");
                else
                    $contractorInput = $("#seller_contractor_id");
            } else if ($("#cashorderflows-contractorinput").length) {
                $contractorInput = $("#cashorderflows-contractorinput");
            } else if ($("#cashemoneyflows-contractor_id-").length) {
                $contractorInput = $("#cashemoneyflows-contractor_id-");
            } else if ($("#agent-buyer").length) {
                $contractorInput = $("#agent-buyer");
            } else if ($("#sellbalancearticleform-contractor_id").length) {
                $contractorInput = $("#sellbalancearticleform-contractor_id");
            } else if ($("#entityrent-contractor_id").length) {
                $contractorInput = $("#entityrent-contractor_id");
            } else if ($('#creditform-contractor_id').length) {
                $contractorInput = $('#creditform-contractor_id');
            } else if ($('#rentagreementform-contractor_id').length) {
                $contractorInput = $('#rentagreementform-contractor_id');
            } else if ($('#project_customer_select').length) {
                $contractorInput = $('#project_customer_select');
            }

            $.post(
                url,
                form,
                function (data) {
                    if (data) {

                        if (url != 'add-modal-contractor-fast')
                            $('#add-new').modal();

                        if (data.header !== undefined) {
                            $('#add-new .modal-title').html(data.header);
                            $('#block-modal-new-product-form').html(data.body);
                            $('#block-modal-new-product-form input').uniform('refresh');
                            $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                            $("#contractor-face_type").on("change", "input", function () {
                                if (!$('.page-content.payment-order').length) {
                                    $('.error-summary').remove();
                                }
                                var form = this.form;
                                var value = $("#contractor-face_type input:checked", form).val();
                                $('.face_type_depend', form).toggleClass("hidden", true);
                                $('.face_type_depend', form).find('input, select').prop('disabled', true);
                                $('.face_type_depend.face_type_'+value, form).toggleClass("hidden", false);
                                $('.face_type_depend.face_type_'+value, form).each(function () {
                                    if ($('.face_type_depend', this).length == 0) {
                                        $('input, select', this).prop('disabled', false);
                                    }
                                });
                            });
                            $('a.btn').click(function () {
                                $('#add-new').modal('hide');
                            });
                        } else if (data.hasOwnProperty('is_payment_order_page')) {
                            $('#add-new').modal('hide');
                            // Contractor info
                            $('.contractor_inn_button').remove();
                            $('input[name="PaymentOrder[contractor_name]"]').val(data.contractor_name);
                            $('input[name="PaymentOrder[contractor_inn]"]').val(data.contractor_inn);
                            $('input[name="PaymentOrder[contractor_kpp]"]').val(data.contractor_kpp);
                            // Contractor bank info
                            if (data.contractor_bik && data.contractor_bank_name && data.contractor_current_account) {
                                $('.contractor_bank_button').remove();
                                $('input[name="PaymentOrder[contractor_bank_name]"]').val(data.contractor_bank_name);
                                $('input[name="PaymentOrder[contractor_bik]"]').val(data.contractor_bik).show();
                                $('input[name="PaymentOrder[contractor_current_account]"]').val(data.contractor_current_account);
                                $('input[name="PaymentOrder[contractor_corresponding_account]"]').val(data.contractor_corresponding_account);
                            }

                            $('#payment_order_new_contractor_id').val(data.id);

                        } else {
                            $('#add-new').modal('hide');

                            if (data['update-contractor']) {
                                $('#contractor_update_button').val(data['name-short'] + ' ' + data['name']);
                            } else if (data['name-short'].length > 0) {
                                $contractorInput.append('<option value="' + data.id + '">' + data['name-short'] + ' ' + data.name + '</option>');
                            } else {
                                $contractorInput.append('<option value="' + data.id + '">' + data.name + '</option>');
                            }
                            $("#add-first-contractor").hide();
                            $("#select-existing-contractor").show();

                            let contractorVal = $contractorInput.val();
                            if (Array.isArray(contractorVal)) {
                                contractorVal.push(data.id);
                                $contractorInput.val(contractorVal).trigger("change");
                            } else {
                                $contractorInput.val(data.id).trigger("change");
                            }
                        }

                        $("input[type=\"checkbox\"]").uniform('refresh');
                        $("input[type=\"radio\"]").uniform('refresh');

                        INVOICE.recalculateInvoiceReady();
                    }
                }
            );
        },
        tultipProduct: function (selector) {
            if ($.isFunction($().tooltipster)) {
                $(selector).tooltipster({
                    "multiple": true,
                    "theme": ['tooltipster-kub', 'tooltipster-max-width'],
                    'maxWidth': 450,
                    "functionBefore": function (instance, helper) {
                        instance.content(helper.origin.value);
                    }
                });
            }
        },
        recalculateInvoiceReady: function () {
            if ($('#invoise-step-1').length) {
                if ($("#invoice-contractor_id").val()) {
                    $('#invoise-step-1').addClass('done');
                } else {
                    $('#invoise-step-1').removeClass('done');
                }
            }
            if ($('#invoise-step-2').length) {
                if ($('.your-company').length) {
                    $('#invoise-step-2').removeClass('done');
                } else {
                    $('#invoise-step-2').addClass('done');
                }
            }
            if ($('#invoise-step-3').length) {
                if (INVOICE.$invoiceTable.find('.product-row').length) {
                    $('#invoise-step-3').addClass('done');
                } else {
                    $('#invoise-step-3').removeClass('done');
                }
            }
            if ($('.invoice-ready-value').length > 0) {
                var sum = 0;
                $('.done[data-invoice-ready]').each(function () {
                    sum += parseInt($(this).data('invoice-ready'));
                });
                $('.invoice-ready-value').html(sum > 100 ? 100 : sum);
            }
        },
        removeProductFromInvoiceId: {},
        removeProductFromInvoice: function () {
            var btn = INVOICE.removeProductFromInvoiceId;

            if ($(btn).hasClass('from-new')) {
                $('#from-new-add-row').hide();
            } else {
                $(btn).parents('tr:first').remove();
                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
            }
            if ($('#invoice-is_invoice_contract').val() == 1) {
                checkContractEssenceTemplate();
            }
            INVOICE.recalculateInvoiceReady();
        },
        getCurrencyRate: function (currencyName, rateDate) {
            if (currencyName && currencyName != 'RUB' && rateDate) {
                if (INVOICE.CurrencyRateRequestData[currencyName] == undefined) {
                    INVOICE.CurrencyRateRequestData[currencyName] = {};
                }
                if (INVOICE.CurrencyRateRequestData[currencyName][rateDate] == undefined) {
                    INVOICE.CurrencyRateRequestData[currencyName][rateDate] = {};
                }
                if (!INVOICE.CurrencyRateRequestData[currencyName][rateDate].value) {
                    $.ajax({
                        type: 'post',
                        url: $('#invoice-currency_rate_type').data('geturl'),
                        data: {name: currencyName, date: rateDate},
                        async: false,
                        success: function (data) {
                            console.log(data);
                            if (data.name && data.name == currencyName && data.amount && data.value) {
                                INVOICE.CurrencyRateRequestData[currencyName][rateDate] = data;
                                $(".invoice-panel div.currency_rate_value").data("initial", data.value).text(data.value);
                            }
                        },
                        complete: function () {
                        },
                    });
                }
                return INVOICE.CurrencyRateRequestData[currencyName][rateDate];
            }
            return {amount: 1, value: 1};
        },
        changeInvoiceCurrency: function (input) {
            var $input = $(input);
            var defaultName = $input.data('default');
            var currencyName = $input.val();
            var rateDate = $('#invoice-currency_ratedate').val();
            var rate = INVOICE.getCurrencyRate(currencyName, rateDate);
            var comment = $('#invoice-comment').val();
            var defaultComment = $('#invoice-comment').data('currency');
            var newComment;
            $('#invoice-currency_rate_type input.default-type').prop('checked', true);
            $('#invoice-currency_rate_type input[type=radio]:not(.md-radiobtn)').uniform('refresh');
            if (rate && rate.amount && rate.value) {
                $('span.currency_rate_amount').html(rate.amount);
                $('input.currency_rate_amount').val(rate.amount);
                $('span.currency_rate_value').html(rate.value);
                $('input.currency_rate_value').val(rate.value);
                $('span.currency_name').html(currencyName);
            }
            if (currencyName == defaultName) {
                var documentDate = $('input.invoice_document_date').val();
                $('span.currency_rate_date').html(documentDate);
                $('input.currency_rate_date').val(documentDate);
                $('#currency-rate-box').collapse('hide');
                $('.currency-view-toggle').collapse('hide');
                $('#invoice-comment').val('');
                $('.total_currency_label').html('');
                if (comment != '') {
                    newComment = comment.replace(new RegExp(defaultComment + '[\s\n]*'), '').trim();
                    $('#invoice-comment').val(newComment);
                }
            } else {
                $('.total_currency_label').html(' (' + currencyName + ')');
                if (comment == '' || comment.search(new RegExp(defaultComment.substring(0, 50))) == -1) {
                    newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                    $('#invoice-comment').val(newComment);
                }
                $('.currency-view-toggle').collapse('show');
            }
        },
        changeDocumentDate: function (date) {
            var $nameInput = $('#invoice-currency_name');
            var currencyName = $nameInput.val();
            var defaultName = $nameInput.data('default');
            $('span.currency_rate_date').html(date);
            $('input.currency_rate_date').val(date);
            if (currencyName != defaultName) {
                INVOICE.changeCurrencyRateDate(currencyName, date);
            }
        },
        changeCurrencyRateDate: function (currencyName, rateDate) {
            $('span.currency_rate_date').html(rateDate);
            var rate = INVOICE.getCurrencyRate(currencyName, rateDate);
            if (rate && rate.amount && rate.value) {
                $('span.currency_rate_amount, div.currency_rate_amount').html(rate.amount);
                $('input.currency_rate_amount').val(rate.amount);
                $('span.currency_rate_value, div.currency_rate_value').html(rate.value);
                $('input.currency_rate_value').val(rate.value);
            }
        },
        changeCurrencyAmount: function (input) {
            var $input = $(input);
            var inputValue = $input.val();
            var value = inputValue.match(/1[0]{0,9}/) || '1';
            if (inputValue !== value) {
                $input.val(value);
            }
            $('span.currency_amount').html(value);
            $('input.currency_amount').val(value);
        },
        changeCurrencyRate: function (input) {
            var $input = $(input);
            var str = $input.val();
            var value = str.replace(/,/, '.').match(/(0|[1-9][0-9]{0,9})(\.[0-9]{0,4})?/);
            value = value ? value[0] : '';
            if (str != value) {
                $input.val(value);
            }
            value = parseFloat(value).toFixed(4);
            $('span.currency_rate').html(value);
            $('input.currency_rate').val(value);
        },
        setInvoiceCurrencyRate: function ($context, data) {
            //$('#currency-rate-box', $context).collapse('hide');
            $.ajax({
                type: 'post',
                url: $('#invoice-currency_rate_type').data('seturl'),
                data: data,
                async: false,
                success: function (data) {
                    if (data.content) {
                        $context.html(data.content);
                        $('input[type=radio]:not(.md-radiobtn)', $context).uniform();
                        $('.collapse', $context).collapse({toggle: false});
                        //$('#invoice-currency_ratedate').datepicker({
                        //    format: 'dd.mm.yyyy',
                        //    language: 'ru',
                        //    autoclose: true,
                        //    endDate: new Date(),
                        //});
                    }
                },
                complete: function () {
                },
            });
        },
        setPayLimitInfo: function (input) {
            if ($('#invoice-comment').length > 0) {
                var $input = $(input);
                var newComment;
                var comment = $('#invoice-comment').val();
                var defaultComment = $('#invoice-comment').data('paylimit')
                    .replace('{{date}}', $('#invoice-payment_limit_date').val());
                if ($input.is(':checked')) {
                    var search = $('#invoice-comment').data('paylimit')
                        .replace('{{date}}', '');
                    if (comment == '' || comment.search(new RegExp(search)) == -1) {
                        newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                        $('#invoice-comment').val(newComment);
                    }
                } else {
                    var pattern = $('#invoice-comment').data('paylimit')
                        .replace('{{date}}.', '\\d{2}\\.\\d{2}\\.\\d{4}\\.[\\s\\n]*');
                    if (comment != '') {
                        newComment = comment.replace(new RegExp(pattern), '').trim();
                        $('#invoice-comment').val(newComment);
                    }
                }
            }
        },
        setContractPayLimitInfo: function (input) {
            if ($('#invoice-contract_essence').length > 0) {
                var $input = $(input);
                var newComment;
                var comment = $('#invoice-contract_essence').val();
                var defaultComment = $('#invoice-contract_essence').data('paylimit')
                    .replace('{{date}}', $('#invoice-payment_limit_date').val());
                if ($input.is(':checked')) {
                    var search = $('#invoice-contract_essence').data('paylimit')
                        .replace('{{date}}', '');
                    if (comment == '' || comment.search(new RegExp(search)) == -1) {
                        newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                        $('#invoice-contract_essence').val(newComment);
                    }
                } else {
                    var pattern = $('#invoice-contract_essence').data('paylimit')
                        .replace('{{date}}.', '\\d{2}\\.\\d{2}\\.\\d{4}\\.[\\s\\n]*');
                    if (comment != '') {
                        newComment = comment.replace(new RegExp(pattern), '').trim();
                        $('#invoice-contract_essence').val(newComment);
                    }
                }
            }
        },
        getInvoicesWithAmountOnDate: function (url, id, date) {
            if (INVOICE.InvoiceAmountRequestData[id] == undefined) {
                INVOICE.InvoiceAmountRequestData[id] = {};
            }
            if (INVOICE.InvoiceAmountRequestData[id][date] == undefined) {
                INVOICE.InvoiceAmountRequestData[id][date] = {};
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {'id': id, 'date': date},
                    async: false,
                    success: function (data) {
                        if (data.result) {
                            INVOICE.InvoiceAmountRequestData[id][date] = data;
                        }
                    },
                    complete: function () {
                    },
                });
            }
            return INVOICE.InvoiceAmountRequestData[id][date];
        },
        getRecognizeLink: function (item, txt, cls) {
            var rid = item.recognized_id;
            var quantity = item.quantity || 0;
            return '<a href="#" style="color:red" class="link unknown_product ' + cls + '" data-recognized_id="' + rid + '" data-quantity="' + quantity + '">' + txt + '</a>';
        }
    };

    window.ORDERDOCUMENT = {
        productSeletedToOrderDocument: [],
        $table: $('#product-table-order-document'),
        $addToOrderDocumentButton: $('#add-to-invoice-button'),
        getItems: function () {
            return this.$table.find('tbody > tr.product-row');
        },
        getFloatPrice: function (price) {
            return (price / 100).toFixed(2);
        },
        tultipProduct: function (selector) {
            if ($.isFunction($().tooltipster)) {
                $(selector).tooltipster({
                    "multiple": true,
                    "theme": ['tooltipster-kub'],
                    "functionBefore": function (instance, helper) {
                        instance.content(helper.origin.value);
                    }
                });
            }
        },
        removeProductFromOrderDocumentId: {},
        removeProductFromOrderDocument: function () {
            var btn = ORDERDOCUMENT.removeProductFromOrderDocumentId;

            if ($(btn).hasClass('from-new')) {
                $('#from-new-add-row').hide();
            } else {
                $(btn).parents('tr:first').remove();
                this.recalculateOrderDocumentTable();
            }
            this.recalculateOrderDocumentTable();
        },
        recalculateOrderDocumentTable: function () {
            var subTotal = 0;
            var totalDiscount = 0;
            var totalMarkup = 0;
            var totalWeight = 0;
            var totalTax = 0;
            var ndsViewId = parseInt($('#orderdocument-nds_view_type_id').val());

            this.getItems().each(function () {
                var $this = $(this);
                if (!$this.hasClass('from-new-add')) {
                    var productCount = $this.find('.product-count').val() || '';
                    var basePrice = APP.helper.inputToMoneyFormat($this.find('.price-input').val() || '');
                    var priceWithNds = APP.helper.inputToMoneyFormat($this.find('.price-one-with-nds').first().text() || '');
                    var weight = parseFloat($this.find('.product-weight').text() || 0);
                    var productSubTotal = priceWithNds * productCount;
                    var taxRate = parseFloat($this.find('.tax-rate').val() || 0);
                    var productTax = 0;

                    if (ndsViewId === 2) {
                        productTax = 0;
                    } else if (ndsViewId === 1) {
                        productTax = productSubTotal * taxRate;
                    } else {
                        productTax = productSubTotal * (taxRate / (1 + taxRate));
                    }

                    totalDiscount += basePrice * productCount - productSubTotal;
                    totalMarkup += productSubTotal - basePrice * productCount;
                    subTotal += productSubTotal;
                    totalWeight += weight * productCount;
                    totalTax += productTax;
                }
            });

            var totalTotalAmount = (ndsViewId === 1) ? subTotal + totalTax : subTotal;

            $('#discount_sum').text(parseFloat(totalDiscount).toFixed(2));
            $('#markup_sum').text(parseFloat(totalMarkup).toFixed(2));
            $('#total_price').text(parseFloat(subTotal).toFixed(2));
            $('#total_mass').text(totalWeight.toFixed(2));
            $('.table-resume .including_nds').text(totalTax.toFixed(2));
            $('#total_total_amount').text(totalTotalAmount.toFixed(2));
        },
        recalculateItemPrice: function ($input) {
            var $row = $input.closest('tr');
            var $priceInput = $row.find('.price-input');
            var $discountInput = $row.find('.discount-input');
            var $markupInput = $row.find('.markup-input');
            var count = $row.find('.product-count').val() || 0;
            var price = Math.round($priceInput.val() * 100) / 100 || 0;
            var discount = Math.min(100.0000, Math.round($discountInput.val() * 10000) / 10000 || 0);
            var markup = Math.min(99.9999, Math.round($markupInput.val() * 10000) / 10000 || 0);
            var priceOne = price - Math.round((price * discount / 100) * 100) / 100 + Math.round((price * markup / 100) * 100) / 100;
            var $availableCount = $row.find('.product-available').text() * 1 > 0 ? $row.find('.product-available').text() * 1 : 1;
            if ($priceInput.val() != price) {
                $priceInput.val(parseFloat(price).toFixed(2));
            }
            if ($discountInput.val() != discount) {
                $discountInput.val(discount);
            }
            if ($markupInput.val() != markup) {
                $markupInput.val(markup);
            }

            $row.find('.price-one-with-nds').text(parseFloat(priceOne).toFixed(2));
            $row.find('.price-one-with-nds-input').val(priceOne);
            $row.find('.price-with-nds').text(parseFloat(priceOne * count).toFixed(2));
        },
        addProductToTable: function (items, type = 2) {
            if (items) {
                var documentIoType = $('#documentType').val();
                var $template = ORDERDOCUMENT.$table.find('.template').clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input').attr('disabled', false);
                var $itemArray = [];
                var itemNumber = ORDERDOCUMENT.getItems().length;

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? item.productUnit.name : '---';
                    $item = $template.clone();
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-delete').find('.remove-product-from-order-document').data('id', item.id);
                    $item.find('.product-title')
                        .attr('name', 'orderArray[' + itemNumber + '][title]')
                        .val(item.title);
                    $item.find('.product-article').text(item.article ? item.article : '---');
                    $item.find('.tax-rate').val(item.priceForSellNds.rate);
                    $item.find('.product-id')
                        .attr('name', 'orderArray[' + itemNumber + '][id]')
                        .val(item.id);
                    $item.find('.product-model')
                        .attr('name', 'orderArray[' + itemNumber + '][model]');
                    $item.find('.product-unit-name').text(unitName);
                    $item.find('.product-quantity').text(item.totalQuantity ? item.totalQuantity * 1 : '---');
                    $item.find('.product-reserve').text(item.reserve);
                    $item.find('.product-available').text(item.available);
                    $item.find('.product-weight').text(item.weight ? item.weight * 1 : '---');
                    $item.find('.product-volume').text(item.volume ? item.volume * 1 : '---');
                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        .attr('name', 'orderArray[' + itemNumber + '][count]')
                        .val(1);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    }
                    $item.find('.price-input')
                        .attr('name', 'orderArray[' + itemNumber + '][price]');
                    $item.find('.discount-input')
                        .attr('name', 'orderArray[' + itemNumber + '][discount]');
                    $item.find('.price-one-with-nds-input')
                        .attr('name', 'orderArray[' + itemNumber + '][priceOneWithVat]');


                    // 20-456
                    //$item.find('.price-for-sell-nds-name').text(item.priceForSellNds.name);
                    var itemNds = (documentIoType == 1) ? item.priceForBuyNds : item.priceForSellNds;
                    if (itemNds) {
                        $item.find('.price-for-sell-nds-name').attr('data-id', itemNds.id).attr('data-name', itemNds.name)
                            .find('select')
                            .attr('name', 'orderArray[' + itemNumber + '][' + (documentIoType == INVOICE.InvoiceOut ? 'sale_tax_rate_id' : 'purchase_tax_rate_id') + ']')
                            .attr('id', 'orderArray__nds_' + itemNumber)
                            .addClass('select2me')
                            .removeAttr('disabled')
                            .val(itemNds.id);
                    }

                    var viewType = parseInt($('#orderdocument-nds_view_type_id').val());

                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }
                    //var priceForSell = viewType === 2 ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;
                    //var priceForBuy = viewType === 1 ? Math.round(item.price_for_buy_with_nds / (Number(item.priceForBuyNds.rate) + 1)) : item.price_for_buy_with_nds;
                    var priceForSell = (viewType == 1) ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;
                    var priceForBuy = (viewType == 1) ? Math.round(item.price_for_buy_with_nds / (Number(item.priceForBuyNds.rate) + 1)) : item.price_for_buy_with_nds;
                    var price = (documentIoType == 1) ? priceForBuy : priceForSell;

                    $item.find('.price-input').val(ORDERDOCUMENT.getFloatPrice(parseInt(price)));
                    $item.find('.price-one-with-nds').text(ORDERDOCUMENT.getFloatPrice(parseInt(price)));
                    $item.find('.price-one-with-nds-input').val(ORDERDOCUMENT.getFloatPrice(parseInt(price)));
                    $item.find('.price-with-nds').text(ORDERDOCUMENT.getFloatPrice((price)));

                    $itemArray.push($item);
                    itemNumber++;
                }
                for (i in $itemArray) {
                    $itemArray[i].show().insertBefore('#from-new-add-row');
                }

                // create select2
                $('#table-product-list-body').find('.select2me').each(function() {
                    createSimpleSelect2($(this).attr('id'));
                    $(this).removeClass('select2me');
                });

                this.recalculateOrderDocumentTable();
                INVOICE.tultipProduct('.tooltip-product');

                return true;
            }

            return false;
        },
        addNewProduct: function (url, form) {
            $.post(
                url,
                form,
                function (data) {
                    $('#add-new').modal();
                    if (data) {
                        if (data.header !== undefined) {
                            $('#add-new .modal-title').html(data.header);
                            $('#block-modal-new-product-form').html(data.body);
                            $('#block-modal-new-product-form input').uniform('refresh');
                            $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                            $("#contractor-face_type").on("change", "input", function () {
                                if (!$('.page-content.payment-order').length) {
                                    $('.error-summary').remove();
                                }
                                $('.forContractor').toggleClass('selectedDopColumns');
                            });
                            $('a.btn').click(function () {
                                $('#add-new').modal('hide');
                            });

                            var urlunits = $('#get-product-units-url').val();

                            $("#product-production_type").on("change", "input", function () {
                                $('#new-product-order-document-form input, #new-product-order-document-form select').attr('disabled', false);
                                $('#new-product-order-document-form input:radio:not(.md-radiobtn), #new-product-order-document-form input:checkbox:not(.md-check)').uniform("refresh");
                                $('#new-product-order-document-form input[type="text"]').val('');
                                $('.error-summary').remove();
                                if ($(this).val() == '0') {
                                    $('.forProduct').addClass('selectedDopColumns');
                                    $('.forService').removeClass('selectedDopColumns');
                                    $('#product-goods-group_id').attr('disabled', true);
                                    $('#product-service-group_id').removeAttr('disabled');
                                    $('.field-product-product_unit_id').removeClass('required');
                                    $('.import-products').hide();
                                    $('.new_product_group_input').attr('placeholder', 'Добавить новую группу услуг');
                                } else {
                                    $('.forProduct').removeClass('selectedDopColumns');
                                    $('#product-goods-group_id').removeAttr('disabled');
                                    $('#product-service-group_id').attr('disabled', true);
                                    $('.forService').addClass('selectedDopColumns');
                                    $('.import-products').show();
                                    $('.field-product-product_unit_id').addClass('required');
                                    $('.new_product_group_input').attr('placeholder', 'Добавить новую группу товаров');
                                }
                                if ($('#is_first_invoice').val() == '1') {
                                    if ($(this).val() == '0') {
                                        $('#product-title').attr('placeholder', 'Например: Доставка товара');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 500 (указывать только цифры)');
                                    }
                                    if ($(this).val() == '1') {
                                        $('#product-title').attr('placeholder', 'Например: Доска обрезная');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 1000 (указывать только цифры)');
                                    }
                                }

                                $.post(urlunits, {
                                    productType: $(this).val()
                                }).done(function (data) {
                                    if (data.html) {
                                        $('#product-product_unit_id').html($(data.html).html());
                                    }
                                });
                            });

                        } else if (data.items !== undefined) {
                            var prodCount = data.services + data.goods;

                            $('.order-add-static-items .service-count-value').text(data.services);
                            if (data.services > 0) {
                                $('li.select2-results__option.add-modal-services').removeClass('hidden');
                            }
                            $('.order-add-static-items .product-count-value').text(data.goods);
                            if (data.goods > 0) {
                                $('li.select2-results__option.add-modal-products').removeClass('hidden');
                            }
                            if ($('.add-exists-product.hidden').length && prodCount > 0) {
                                $('.add-first-product-button').addClass('hidden');
                                $('.add-exists-product.hidden').removeClass('hidden');
                            }
                            ORDERDOCUMENT.addProductToTable(data.items);
                            $('#from-new-add-row').hide();

                            $('#add-new').modal('hide');
                        } else {
                            $('#add-new').modal('hide');
                        }
                    }
                }
            );
        },
    };

    $(document).on("change", "input.invoice_document_date", function () {
        INVOICE.changeDocumentDate($(this).val());
    });

    $(document).on("change", "#invoice-currency_name", function () {
        INVOICE.changeInvoiceCurrency(this);
    });

    $(document).on("change", "input.currency-rate-type-radio", function () {
        if (!$('#radio_currency_rate_custom').is(':checked')) {
            INVOICE.changeCurrencyRateDate($('#invoice-currency_name').val(), $('#invoice-currency_ratedate').val());
        }
        $('input.currency-custom-input').prop('disabled', !$('#radio_currency_rate_custom').is(':checked'));
        $('input.invoice-currency_ratedate').prop('readonly', !$('#radio_currency_rate_ondate').is(':checked'));
    });

    $(document).on("change", "#invoice-currency_ratedate", function () {
        INVOICE.changeCurrencyRateDate($('#invoice-currency_name').val(), $(this).val());
    });

    $(document).on("input", "#invoice-currencyamountcustom", function () {
        INVOICE.changeCurrencyAmount(this);
    });
    $(document).on("input", "#invoice-currencyratecustom", function () {
        INVOICE.changeCurrencyRate(this);
    });

    $(document).on("click", "#currency_rate_apply", function () {
        var $context = $('#invoice_currency_rate_box');
        var data = $('input', $context).serialize();
        INVOICE.setInvoiceCurrencyRate($context, data);
    });

    $(document).on("click", "#currency_rate_cancel", function () {
        var $context = $('#invoice_currency_rate_box');
        INVOICE.setInvoiceCurrencyRate($context, {});
    });

    $(document).on("change", "#invoice-show_paylimit_info", function () {
        if ($(this.form).data("foreign") == '1') return;
        INVOICE.setPayLimitInfo(this);
        INVOICE.setContractPayLimitInfo(this);
    });

    $(document).on("change", "#invoice-hasnds", function () {
        var hasNds = $(this).is(":checked");
        var ndsViewId = hasNds ? 0 : 2;
        $(".with-nds-item").toggleClass("hidden", !hasNds);
        $(".without-nds-item").toggleClass("hidden", hasNds);
        $("#invoice-nds_view_type_id").val(ndsViewId);
        $("#invoice-nds_view_type_id").data('id', ndsViewId);
        $(".nds-view-item").addClass("hidden");
        $(".nds-view-item.type-" + ndsViewId).removeClass("hidden");
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('click', '.first-invoice-company', function () {
        if (!$(this).hasClass('done')) {
            var url = 'add-modal-details-address';
            var form = '';
            INVOICE.addDetailsAddress(url, form);
        }
    });

    $(document).on('click', '.first-invoice-contractor', function () {
        if (!$(this).hasClass('done')) {
            var form = {
                documentType: INVOICE.documentIoType,
                Product: {
                    production_type: 1,
                    flag: 1
                }
            };
            INVOICE.addNewContractor('add-modal-contractor', form);
        }
    });

    $(document).on('click', '.payment-order-add-contractor', function () {
        var form = {
            documentType: INVOICE.documentIoType,
            contractor_id: $('#payment_order_new_contractor_id').val()
        };
        INVOICE.addNewContractor('add-modal-contractor', form);
    });

    $(document).on('click', '.first-invoice-product', function () {
        if (!$(this).hasClass('done')) {
            var form = {
                documentType: INVOICE.documentIoType,
                Product: {
                    production_type: null,
                    flag: 1
                }
            };
            selectedProdType = null;
            INVOICE.addNewProduct('add-modal-product', form);
        }
    });

    /*$(document).on('click', 'span.addLogo', function () {
     var url = 'add-modal-logo';
     var form = '';
     INVOICE.addLogo(url, form);
     });*/

    $(document).on('click', '.add-modal-produc-new', function () {
        var ioType = $("#out-invoice-form").length ? INVOICE.InvoiceOut : INVOICE.documentIoType;
        var document = $("input#create-document").val();
        var form = {
            documentType: ioType || 2,
            Product: {production_type: null, flag: 1},
            document: document
        };
        $('#order-add-select, #product-add-select').select2('close');
        INVOICE.addNewProduct('/documents/invoice/add-modal-product', form);
    });

    $(document).on('click', '.add-modal-new-service', function () {
        var ioType = $(this).data('type');
        var document = $(this).data('document');
        var form = {
            documentType: ioType,
            Product: {production_type: 0, flag: 1},
            document: document
        };
        $('#order-add-select, #product-add-select').select2('close');
        ACT.addNewProduct('/documents/invoice/add-modal-product', form);
    });

    $(document).on('click', '.add-modal-product-new-order-document', function () {
        var ioType = 2;
        var form = {
            documentType: ioType,
            Product: {production_type: null, flag: 1}
        };
        $('#order-add-select, #product-add-select').select2('close');
        ORDERDOCUMENT.addNewProduct('/documents/order-document/add-modal-product', form);
    });

    $(document).on('click', '.add-modal-products', function () {
        $('#order-add-select, #product-add-select').select2('close');
        INVOICE.searchProduct('1', $('#order-add-select').data('doctype'));
    });

    $(document).on('click', '.add-modal-services', function () {
        $('#order-add-select, #product-add-select').select2('close');
        INVOICE.searchProduct('0', $('#order-add-select').data('doctype'));
    });

    $(document).on('change', 'select', (function () {
        var docType = $(this).data('doctype');
        var $isOrderDocument = $('form#order-document-form').length > 0;
        var value = $(this).val();
        var form = {
            documentType: INVOICE.documentIoType,
            Product: {
                production_type: 1,
                flag: 1
            }
        };

        if ($(this).parent().hasClass('show-contractor-type-in-fields'))
            form.show_contractor_type = 1;

        if (Array.isArray(value)) {
            if ($.inArray('add-modal-contractor', value) !== -1) {
                let formattedValue = jQuery.grep(value, function (id) {
                    return id != 'add-modal-contractor';
                });

                $(this).val(formattedValue).trigger("change");
                value = 'add-modal-contractor';
                if ($(this).hasClass("select2-hidden-accessible")) {
                    $(this).select2('close');
                }
            } else if ($.inArray('toggle-all', value) !== -1) {
                let formattedValue = jQuery.grep(value, function (id) {
                    return id != 'toggle-all';
                });

                $(this).val(formattedValue).trigger("change");
                value = 'toggle-all';
            }
        }

        if (value == 'add-modal-contractor') {
            if ($(this).attr("id") == "logisticsrequest-customer_id" || $(this).attr("id") == "upd-consignor_id" ||
                $(this).attr("id") == "packinglist-consignor_id" || $(this).attr("id") == "invoicefacture-consignor_id" ||
                $(this).attr("id") == "customer_contractor_id" || $(this).attr("id") == "articleform-customers" ||
                $(this).attr("id") == "loadingandunloadingaddress-consignor_id" || $(this).attr("id") == "updatearticleform-customers" ||
                $(this).attr("id") == "waybill-consignor_id" || $(this).hasClass("customer") ||
                $(this).attr("id") == "creditform-contractor_id" || $(this).attr("id") == "project-customer_id" ||
                $(this).attr("id") == "rentagreementform-contractor_id"
            ) {
                form = {
                    type: 2,
                    documentType: 2
                };
            } else if ($(this).attr("id") == "logisticsrequest-carrier_id" || $(this).attr("id") == "upd-consignee_id" ||
                $(this).attr("id") == "packinglist-consignee_id" || $(this).attr("id") == "invoicefacture-consignee_id" ||
                $(this).attr("id") == "seller_contractor_id" || $(this).attr("id") == "articleform-sellers" ||
                $(this).attr("id") == "loadingandunloadingaddress-consignee_id" || $(this).attr("id") == "updatearticleform-sellers" ||
                $(this).attr("id") == "waybill-consignee_id" || $(this).hasClass("seller")) {
                form = {
                    type: 1,
                    documentType: 1
                };
            }
            $(this).val(null).trigger("change");

            if ($(this).data('createurl')) {
                value = $(this).data('createurl');
            } else {
                if ($(this).closest('#agreement-form-container').length) {
                    value = '/documents/agreement/add-modal-contractor';
                }
                if ($(this).closest('form#product-form').length || $(this).closest('form#creditForm').length) {
                    value = '/documents/agreement/add-modal-contractor';
                }
            }
            INVOICE.addNewContractor(value, form, this);
            return;
        }
        if (value == 'add-modal-product') {
            $(this).val(null).trigger("change");
            form.Product.production_type = selectedProdType = null;
            $isOrderDocument ? ORDERDOCUMENT.addNewProduct(value, form) : INVOICE.addNewProduct(value, form);
        }
        if (value == 'add-modal-products') {
            $(this).val(null).trigger("change");
            $isOrderDocument ? ORDERDOCUMENT.searchProduct('1', $(this).data('doctype')) : INVOICE.searchProduct('1', $(this).data('doctype'));
        }
        if (value == 'add-modal-services') {
            $(this).val(null).trigger("change");
            $isOrderDocument ? ORDERDOCUMENT.searchProduct('0', $(this).data('doctype')) : INVOICE.searchProduct('0', $(this).data('doctype'));
        }
        if ($(this).attr('id') == 'order-add-select' && parseInt(value)) {
            $(this).val(null).trigger("change");
            if ($isOrderDocument) {
                $.post('/product/get-products-table?toOrderDocument=1&documentType=' + docType, {in_order: [value]}, function (data) {
                    ORDERDOCUMENT.addProductToTable(data, docType);
                    $('#from-new-add-row').hide();
                });
            } else {
                $.post('/product/get-products-table', {in_order: [value]}, function (data) {
                    INVOICE.addProductToTable(data);
                    $('#from-new-add-row').hide();
                });
            }
        }

        let newValue = [];
        if (value == 'toggle-all') {
            $(this).find('option').each(function(i, el) {
                el = $(el);
                if (
                    el.val() !== 'add-modal-contractor'
                    && el.val() !== 'toggle-all'
                    && el.attr('disabled') === undefined
                ) {
                    newValue.push(el.val());
                }
            });

            if (newValue.length === $(this).val().length) {
                newValue = null;
            }

            $(this).val(newValue).trigger("change").select2('close');
        }
    }));

    $("input[name=addLogo]").on("click", function () {
        $("#log").toggle();
    });

    $(document).on('submit', '#new-product-invoice-form', function (event) {
        event.preventDefault();
        var url = '/documents/invoice/add-modal-product';
        var form = $('#new-product-invoice-form').serialize();
        var $self_add_method = $('#has_self_add_modal_product_method');

        if ($self_add_method.length) {
            var param_input = $self_add_method.attr('data-param-input');
            url = $self_add_method.data('url') || 'add-modal-product';
            if (param_input && $(param_input).length) {
                url += (url.indexOf('?') === -1 ? '?' : '&') + $(param_input).attr('name') + '=' + $(param_input).val();
            }
        }

        if ($('#edit-act').length) {

            ACT.addNewProduct(url, form);
        } else if ($('#edit-upd').length) {

            UPD.addNewProduct(url, form);
        } else {

            INVOICE.addNewProduct(url, form);
        }
    });

    $(document).on('submit', '#new-product-order-document-form', function (event) {
        event.preventDefault();
        var url = '/documents/order-document/add-modal-product';
        var form = $('#new-product-order-document-form').serialize();
        ORDERDOCUMENT.addNewProduct(url, form);
    });

    $(document).on('submit', '#new-company-invoice-form', function (event) {
        event.preventDefault();
        var url = 'add-modal-details-address';
        var form = $(this).serialize();
        INVOICE.addDetailsAddress(url, form);
    });

    $(document).on('afterValidate', '#new-company-invoice-form', function (event) {
        if ($(this).find('.dopColumns').length > 0) {
            var dopData = $(this).find('.dopColumns');
            if (dopData.find('.has-error').length > 0) {
                dopData.removeClass('selectedDopColumns');
            }
        }
    });

    var findNewContractorDuplicates = function(form) {
        let hasDuplicate = false;
        let url = $(form).data('find-duplicate');
        $.ajax({
            type: "POST",
            url: url,
            async : false,
            data: $(form).serialize(),
            success: function(data) {
                if (data) {
                    hasDuplicate = true;
                    $("#new-contractor-duplicates-modal .modal-body").html(data);
                    $("#new-contractor-duplicates-modal").modal("show");
                }
            }
        });

        return hasDuplicate;
    };
    $(document).on("hidden.bs.modal", "#new-contractor-duplicates-modal", function (e) {
        $("#new-contractor-duplicates-modal .modal-body").html("");
    });
    $(document).on('submit', '#new-contractor-invoice-form', function (event) {
        event.preventDefault();
        if (findNewContractorDuplicates(this)) {
            return false;
        }
        INVOICE.addNewContractor(this.action, $('#new-contractor-invoice-form').serialize());
    });

    $(document).on('submit', '#new-company-logo-invoice-form', function (event) {
        event.preventDefault();
        var url = 'add-modal-logo';
        var form = $('#new-company-logo-invoice-form').serialize();
        var fd = new FormData;
        fd.append('logoImage', $('#company-logoimage')[0].files[0]);
        fd.append('printImage', $('#company-printimage')[0].files[0]);
        fd.append('chiefSignatureImage', $('#company-chiefsignatureimage')[0].files[0]);
        fd.append('Company[deleteLogoImage]', +$('#company-deletelogoimage').is(':checked'));
        fd.append('Company[deletePrintImage]', +$('#company-deleteprintimage').is(':checked'));
        fd.append('Company[deleteChiefSignatureImage]', +$('#company-deletechiefsignatureimage').is(':checked'));

        $.ajax({
            url: url,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                if (data.header !== undefined) {
                    $('#add-new .modal-header').html(data.header);
                    $('#block-modal-new-product-form').html(data.body);

                } else {
                    $('#add-new').modal('hide');
                }
            }
        });
    });


    // select product from list and add to invoice
    $('#add-from-exists').on('shown.bs.modal', function () {
        INVOICE.productSeletedToInvoice = [];
        ORDERDOCUMENT.productSeletedToOrderDocument = [];
    });
    $(document).on('click', '#pjax-product-grid a:not(.store-opt)', function (e) {
        e.preventDefault();
        var $product = INVOICE.productSeletedToInvoice;
        if ($('#order-document-form').length) {
            $product = ORDERDOCUMENT.productSeletedToOrderDocument;
        }
        $.pjax.reload('#pjax-product-grid', {
            url: $(this).attr('href'),
            type: 'post',
            data: {product: $product},
            push: false,
            replace: false,
            timeout: 5000
        });
    });
    $(document).on('change', '#products_in_order .product_selected', function () {
        var prodId = $(this).val();
        if (this.checked) {
            INVOICE.productSeletedToInvoice.push(prodId);
            ORDERDOCUMENT.productSeletedToOrderDocument.push(prodId);
        } else {
            INVOICE.productSeletedToInvoice = jQuery.grep(INVOICE.productSeletedToInvoice, function (value) {
                return value != prodId;
            });
            ORDERDOCUMENT.productSeletedToOrderDocument = jQuery.grep(ORDERDOCUMENT.productSeletedToOrderDocument, function (value) {
                return value != prodId;
            });
        }
    });
    $(document).on('change', '#products_in_order .product_selected_one', function () {
        var prodId = $(this).val();
        if (this.checked) {
            INVOICE.productSeletedToInvoice = [prodId];
            ORDERDOCUMENT.productSeletedToOrderDocument = [prodId];
            $('#products_in_order .product_selected_one').each(function(i, checkbox) {
                if ($(checkbox).val() != prodId) {
                    $(checkbox).prop('checked', false).uniform('refresh');
                }
            });
        } else {
            if (INVOICE.productSeletedToInvoice[0] == prodId) {
                INVOICE.productSeletedToInvoice = [];
                ORDERDOCUMENT.productSeletedToOrderDocument = [];
            }
        }
    });
    $(document).on('click', '#add-to-invoice-button, #add-all-to-invoice-button', function () {
        var docType = $(this).data('type');
        if ($('#edit-upd').length) {
            $.post('/product/get-products-table', {in_order: INVOICE.productSeletedToInvoice}, function (data) {
                UPD.addProductToTable(data);
                INVOICE.productSeletedToInvoice = [];
                $('#from-new-add-row').hide();
            });
        }
        else if ($('#edit-act').length) {
            $.post('/product/get-products-table', {in_order: INVOICE.productSeletedToInvoice}, function (data) {
                ACT.addProductToTable(data);
                INVOICE.productSeletedToInvoice = [];
                $('#from-new-add-row').hide();
            });
        }
        else if ($('#order-document-form').length) {
            $.post('/product/get-products-table?toOrderDocument=1&documentType='+docType, {in_order: ORDERDOCUMENT.productSeletedToOrderDocument}, function (data) {
                ORDERDOCUMENT.addProductToTable(data, docType);
                ORDERDOCUMENT.productSeletedToOrderDocument = [];
                $('#from-new-add-row').hide();
            });
        } else {
            if ($("#donate-widget-form").length) {
                var exists = $('input.selected-product-id').map(function (idx, elem) {
                    return $(elem).val();
                }).get();
                $.post('/donate/donate-widget/products-table', {
                    'productType': $('#productTypeHidden').val(),
                    'add_all': ($(this).hasClass('button-add-all') ? 1 : 0),
                    'title': $('#searchTitleHidden').val(),
                    'in_order': INVOICE.productSeletedToInvoice,
                    'exists': exists,
                }, function (data) {
                    if (data.result) {
                        $(data.result).insertBefore("#new-product-row");
                    }
                    INVOICE.productSeletedToInvoice = [];
                    $('#new-product-row').addClass('hidden');
                });
            } else if ($("#out-invoice-form").length) {
                var exists = $('input.selected-product-id').map(function (idx, elem) {
                    return $(elem).val();
                }).get();
                $.post('/out-invoice/products-table', {
                    'productType': $('#productTypeHidden').val(),
                    'add_all': ($(this).hasClass('button-add-all') ? 1 : 0),
                    'title': $('#searchTitleHidden').val(),
                    'in_order': INVOICE.productSeletedToInvoice,
                    'exists': exists,
                }, function (data) {
                    if (data.result) {
                        $(data.result).insertBefore("#new-product-row");
                    }
                    INVOICE.productSeletedToInvoice = [];
                    $('#new-product-row').addClass('hidden');
                });
            } else if ($('#unknown_product_recognized_id').length && $('#unknown_product_recognized_id').val()) {

                if (INVOICE.productSeletedToInvoice.length !== 1) {
                    $('#add-from-exists .empty-choose-error').show();
                    return false;
                }
                $('#add-from-exists .empty-choose-error').hide();

                var current_recognized_id = $('#unknown_product_recognized_id').val();
                var pos = $('.unknown_product').filter('[data-recognized_id="' + current_recognized_id + '"]').first().parents('tr').index();

                $.post('/documents/upload-manager/get-products-table?recognizedId=' + current_recognized_id, {in_order: INVOICE.productSeletedToInvoice}, function (data) {
                    INVOICE.replaceProductInTable(data, pos);
                    INVOICE.productSeletedToInvoice = [];
                    $('#from-new-add-row').hide();
                    $('#unknown_product_recognized_id').val('');
                    $('#add-from-exists').modal('hide');
                });

            } else {
                $.post('/product/get-products-table', {
                    in_order: INVOICE.productSeletedToInvoice,
                    contractorId: $('select#invoice-contractor_id').val()
                }, function (data) {
                    INVOICE.addProductToTable(data);
                    INVOICE.productSeletedToInvoice = [];
                    $('#from-new-add-row').hide();
                });
            }
        }
    });
    $(document).on('hidden.bs.modal', '#add-from-exists', function () {
        // mxfi for BS4
        //INVOICE.productSeletedToInvoice = [];
        //ORDERDOCUMENT.productSeletedToOrderDocument = [];
    });
    // end select product ============================================

    $(document).on('click', '#discount_submit.invoice', function (e) {
        if ($(this).hasClass('button-regular_red')) {
            var val = 0;
            var box = $(this).closest('#invoice_all_discount');
            var allDiscount = $("#all-discount", box);
            var allDiscountValue = allDiscount.val();
            var discountTypeValue = $('#config-discount_type input[type=radio]:checked', box).val() || '0';
            var hiddenDiscountValue = $('#config-is_hidden_discount', box).is(':checked') ? '1' : '0';
            $('#invoice-discount_type').val(discountTypeValue);
            $('#invoice-is_hidden_discount').val(hiddenDiscountValue);
            $('#invoice-price_round_rule').val($('#config-price_round_rule', box).val());
            $('#invoice-price_round_precision').val($('#config-price_round_precision', box).val());
            let currency = $(".currency_name_label");
            if (discountTypeValue == '1') {
                if (currency.length > 0) {
                    $('#all_discount_toggle').html('Discount '+currency.first().prop('outerHTML'));
                } else {
                    $('#all_discount_toggle').text('Скидка (руб.)');
                }
            } else {
                val = Math.max(Math.min(parseFloat(allDiscountValue), 100.0000), 0);
                if (val.toString() != allDiscountValue) {
                    allDiscount.val(val.toString());
                }
                if (allDiscount.val() != allDiscount.data('old-value')) {
                    allDiscount.data('old-value', val.toString());
                    $("table .discount-input").val(val);
                }
                $('#all_discount_toggle').text(currency.length > 0 ? 'Discount %' : 'Скидка %');
            }
            $('table .markup-input').val(0);
            $('table .discount-input').each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
            INVOICE.recalculateInvoiceTable();
            $(this).removeClass('button-regular_red').addClass('button-hover-grey');
        }
        $('.tooltipstered').tooltipster('hide');
    });
    $(document).on('click', '#discount_submit.order-document', function (e) {
        console.log($(this).hasClass('button-regular_red'));
        if ($(this).hasClass('button-regular_red')) {
            var val = 0;
            var box = $(this).closest('#order-document_all_discount');
            var allDiscount = $("#all-discount", box);
            var allDiscountValue = allDiscount.val();
            val = Math.max(Math.min(parseFloat(allDiscountValue), 100.0000), 0);
            if (val.toString() != allDiscountValue) {
                allDiscount.val(val.toString());
            }
            allDiscount.data('old-value', val.toString());
            $('table .discount-input').each(function () {
                $(this).val(val);
                ORDERDOCUMENT.recalculateItemPrice($(this));
            });
            ORDERDOCUMENT.recalculateOrderDocumentTable();
            $(this).removeClass('button-regular_red').addClass('button-hover-grey');
        }
        $('.tooltipstered').tooltipster('hide');
    });
    $(document).on('click', '#markup_submit.order-document', function (e) {
        console.log($(this).hasClass('button-regular_red'));
        if ($(this).hasClass('button-regular_red')) {
            var val = 0;
            var box = $(this).closest('#order-document_all_markup');
            var allMarkup = $("#all-markup", box);
            var allMarkupValue = allMarkup.val();
            val = Math.max(Math.min(parseFloat(allMarkupValue), 99.9999), 0);
            if (val.toString() != allMarkupValue) {
                allMarkup.val(val.toString());
            }
            allMarkup.data('old-value', val.toString());
            $('table .markup-input').each(function () {
                $(this).val(val);
                ORDERDOCUMENT.recalculateItemPrice($(this));
            });
            ORDERDOCUMENT.recalculateOrderDocumentTable();
            $(this).removeClass('button-regular_red').addClass('button-hover-grey');
        }
        $('.tooltipstered').tooltipster('hide');
    });

    $(document).on("click", "#markup_submit", function (e) {
        if ($(this).hasClass('button-regular_red')) {
            var box = $(this).closest('#invoice_all_markup');
            var allMarkup = $("#all-markup", box);
            var allMarkupValue = allMarkup.val();
            var val = Math.max(Math.min(parseFloat(allMarkupValue), 9999.9999), 0);
            if (val.toString() != allMarkupValue) {
                allMarkup.val(val);
            }
            allMarkup.data('old-value', val.toString());
            $("table .discount-input").val(0);
            $("table .markup-input").val(val).each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
            INVOICE.recalculateInvoiceTable();
            $(this).removeClass('button-regular_red').addClass('button-hover-grey');
        }
        $('.tooltipstered').tooltipster('hide');
    });

    $(document).on('change', '#invoice-has_discount', function () {
        if ($(this).prop('checked')) {
            $('#invoice-has_markup').prop('checked', false);
            $('#invoice-has_markup:not(.md-check)').uniform('refresh');
            $('.markup_column').addClass('hidden');
            $("#invoice_all_markup input").val(0);
            //$('.markup-input').val(0);
            $('.discount_column').removeClass('hidden');
            if ($('#config-discount_type input:checked').val() == '0') {
                $('select.price_round_config').prop('disabled', false);
            }
        } else {
            $('.discount_column').addClass('hidden');
            $("#invoice_all_discount input").val(0);
            //$('.discount-input').val(0);
            $('#discount_sum').text('0.00');
            $('select.price_round_config').prop('disabled', true);
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_markup', function () {
        if ($(this).prop('checked')) {
            $('#invoice-has_discount').prop('checked', false);
            $('#invoice-has_discount:not(.md-check)').uniform('refresh');
            $('.discount_column').addClass('hidden');
            $("#invoice_all_discount input").val(0);
            //$('.discount-input').val(0);
            $('#discount_sum').text('0.00');
            $('.markup_column').removeClass('hidden');
        } else {
            $('.markup_column').addClass('hidden');
            $("#invoice_all_markup input").val(0);
            //$('.markup-input').val(0);
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_weight', function () {
        if ($(this).prop('checked')) {
            $('.weight_column').removeClass('hidden');
        } else {
            $('.weight_column').addClass('hidden');
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_volume', function () {
        if ($(this).prop('checked')) {
            $('.volume_column').removeClass('hidden');
        } else {
            $('.volume_column').addClass('hidden');
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#orderdocument-has_discount', function () {
        if ($(this).prop('checked')) {
            $('.discount_column').removeClass('hidden');
            //markup
            if ($('#orderdocument-has_markup').prop('checked'))
                $('#orderdocument-has_markup').click();

        } else {
            $('.discount_column').addClass('hidden');
            $('.discount-input').val(0);
            $('#discount_sum').text('0.00');
            $('tr input.price-input:enabled').each(function () {
                ORDERDOCUMENT.recalculateItemPrice($(this));
            });
        }
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });

    $(document).on('change', '#orderdocument-has_markup', function () {
        if ($(this).prop('checked')) {
            $('.markup_column').removeClass('hidden');
            // discount
            if ($('#orderdocument-has_discount').prop('checked'))
                $('#orderdocument-has_discount').click();
            ORDERDOCUMENT.recalculateItemPrice($(this));
        } else {
            $('.markup_column').addClass('hidden');
            $('.markup-input').val(0);
            $('#markup_sum').text('0.00');
            $('tr input.price-input:enabled').each(function () {
                ORDERDOCUMENT.recalculateItemPrice($(this));
            });
        }
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });

    INVOICE.$invoiceTable.on('click', '.remove-product-from-invoice', function () {
        $('#modal-remove-one-product').modal("show");
        INVOICE.removeProductFromInvoiceId = this;
    });

    ORDERDOCUMENT.$table.on('click', '.remove-product-from-order-document', function () {
        $('#modal-remove-one-product').modal("show");
        ORDERDOCUMENT.removeProductFromOrderDocumentId = this;
    });

    $(document).on('click', '#modal-remove-one-product .yes', function (e) {
        INVOICE.removeProductFromInvoice();
        ORDERDOCUMENT.removeProductFromOrderDocument();
    });

    $(document).on("click", "#invoice_price_conf button", function () {
        var $btn = $(this);
        var $table = $btn.closest("table");
        var precision = parseInt($btn.val());
        $("#invoice-price_precision").val(precision);
        $("#table-product-list-body .product-row input.price-input").each(function () {
            $(this).val(parseFloat($(this).val()).toFixed(precision));
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
        $("#invoice_price_conf button").each(function () {
            var val = parseInt($(this).val());
            $(this).toggleClass('button-regular_red', val == precision).toggleClass('button-hover-content-red', val != precision);
        });
        $('.tooltipstered').tooltipster('hide');
    });

    INVOICE.$invoiceTable.on('keyup change', '.price-input, .discount-input, .markup-input, .weight-input, .volume-input', function (e) {
        INVOICE.recalculateItemPrice($(this));
        INVOICE.recalculateInvoiceTable();
    });

    ORDERDOCUMENT.$table.on('keyup change', '.price-input, .discount-input, .markup-input', function (e) {
        ORDERDOCUMENT.recalculateItemPrice($(this));
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });

    INVOICE.$invoiceTable.on('blur', '.product-count', function () {
        INVOICE.recalculateItemPrice($(this));
        INVOICE.recalculateInvoiceTable();
    });

    ORDERDOCUMENT.$table.on('keyup change', '.product-count', function () {
        ORDERDOCUMENT.recalculateItemPrice($(this));
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });

    INVOICE.checkProductTypeEnabled();

    $(document).on('change', '#invoice-nds_view_type_id', function () {
        $(this).data('id', this.value);
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#projectestimate-nds_out', function () {
        $(this).data('id', this.value);
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#orderdocument-nds_view_type_id', function () {
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });

    /* end invoice event */

    $('.report-item').click(function () {
        var id = $(this).data('id');

        $('#report_' + id).hide();
        $('#report_edit_' + id).hide();
        $('#report_form_' + id).removeClass('hide');
    });

    $('.cancel-edit-report').click(function () {
        var id = $(this).data('id');

        $('#report_' + id).show();
        $('#report_edit_' + id).show();
        $('#report_form_' + id).addClass('hide');
    });

    window.PaymentOrder = {
        ifns: null,
        payer: '',
        $paymentOrderType: $('#payment-order-type'),
        $budgetPaymentCheckbox: $('#budget-payment-checkbox'),
        $budgetPaymentInputs: $('.budget-payment'),
        $relationWithInvoiceCheckbox: $('#relation-with-invoice-checkbox'),
        $addInvoiceButton: $('#add-invoice-to-payment-order'),
        $sumInWords: $('.summery__sum-in-words'),
        $sumInDigits: $('.summery__sum-in-digits'),
        clearInputs: function () {
            this.$budgetPaymentInputs.val('').trigger('refresh');
            $('#paymentorder-invoice_id').val('');
            $('#paymentorder-sum').val('');
            $('#paymentorder-contractor_name').val('');
            $('#paymentorder-contractor_inn').val('');
            $('#paymentorder-contractor_kpp').val('');
            $('#paymentorder-contractor_bank_name').val('');
            $('#paymentorder-contractor_bik').val('');
            $('#paymentorder-contractor_current_account').val('');
            $('#paymentorder-contractor_corresponding_account').val('');
            $('#paymentorder-purpose_of_payment').val('');
            $.uniform.update(':not(.md-check, .md-radiobtn)');
        },
        setActivityBudgetPaymentInputs: function () {
            if (this.$relationWithInvoiceCheckbox.prop('checked'))
                this.$relationWithInvoiceCheckbox.click();
            //this.$relationWithInvoiceCheckbox.prop('checked', false);
            //this.$relationWithInvoiceCheckbox.prop('disabled', true);
            this.$budgetPaymentInputs.each(function (i, value) {
                var $input = $(this);
                if (this.value === '' && $input.data('default-value') !== undefined) {
                    this.value = $input.data('default-value');
                }
            });
            $('#paymentorder-taxpayers_status_id').trigger('refresh');
            $('#paymentorder-sum').val($('#paymentorder-sum').data('default-value'));
            $('#paymentorder-purpose_of_payment').val($('#paymentorder-purpose_of_payment').data('default-value'));
            if (PaymentOrder.ifns) {
                $('#paymentorder-contractor_name').val(PaymentOrder.ifns.contractorName);
                $('#paymentorder-contractor_inn').val(PaymentOrder.ifns.g6);
                $('#paymentorder-contractor_kpp').val(PaymentOrder.ifns.g7);
                $('#paymentorder-contractor_bank_name').val(PaymentOrder.ifns.g8);
                $('#paymentorder-contractor_bik').val(PaymentOrder.ifns.g9);
                $('#paymentorder-contractor_current_account').val(PaymentOrder.ifns.g11);
            }
            $.uniform.update(':not(.md-check, .md-radiobtn)');
        },
        unsetActivityBudgetPaymentInputs: function () {
            //this.$relationWithInvoiceCheckbox.prop('disabled', false);
            //this.$relationWithInvoiceCheckbox.prop('checked', false);
            $.uniform.update(':not(.md-check, .md-radiobtn)');
        },
        setActivityByRelationWithInvoice: function () {
            if (this.$budgetPaymentCheckbox.prop('checked'))
                this.$budgetPaymentCheckbox.click();
            //this.$budgetPaymentCheckbox.prop('checked', false);
            //this.$budgetPaymentCheckbox.prop('disabled', true);
            this.$budgetPaymentInputs.prop('disabled', true).trigger('refresh');
            $.uniform.update(':not(.md-check, .md-radiobtn)');
            $('#accounts-list').modal();
        },
        unsetActivityByRelationWithInvoice: function () {
            //this.$budgetPaymentCheckbox.prop('disabled', false);
            //this.$budgetPaymentCheckbox.prop('checked', false);
            this.$budgetPaymentInputs.prop('disabled', false).trigger('refresh');
            $.uniform.update(':not(.md-check, .md-radiobtn)');
        },
        setInvoiceDataToInputs: function (data) {
            $('#paymentorder-invoiceids').val(data.invoiceIDs);
            $('#paymentorder-contractor_id').val(data.contractor_id);
            $('#paymentorder-sum').val((data.totalAmount / 100).toFixed(2));
            $('#paymentorder-company_bik').val(data.company_bik);
            $('#paymentorder-company_bank_name').val(data.company_bank_name);
            $('#paymentorder-company_ks').val(data.company_ks);
            $('#paymentorder-company_rs').val(data.company_rs);
            $('#paymentorder-contractor_bank_name').val(data.contractor_bank_name);
            $('#paymentorder-contractor_inn').val(data.contractor_inn);
            $('#paymentorder-contractor_kpp').val(data.contractor_kpp);
            $('#paymentorder-contractor_bik').val(data.contractor_bik);
            $('#paymentorder-contractor_corresponding_account').val(data.contractor_corresponding_account);
            $('#paymentorder-contractor_current_account').val(data.contractor_current_account);
            $('#paymentorder-contractor_name').val(data.contractor_name);
            $('#paymentorder-purpose_of_payment').val(data.purpose_of_payment);

            PaymentOrder.$sumInDigits.trigger('change');
        },
        showCreatePaymentOrders: function (data) {
            var manyChargeModal = $("#many-charge");
            manyChargeModal.find(".many-charge-text .generated-row, .many-charge-text .total-row").remove();
            var $k = 1;
            for (var $i in data.contractor) {
                manyChargeModal.find(".many-charge-text").append('<span class="generated-row" style="display: block;">' +
                    $k + data.contractor[$i] +
                    '</span>');
                $k++;
            }
            manyChargeModal.find(".many-charge-text").append('<span class="total-row" style="display: block;font-weight: bold;">' +
                data.total +
                '</span>');
            for (var $j in data.invoices) {
                manyChargeModal.find(".many-charge-text").append('<input type="checkbox" class="joint-operation-checkbox hidden" name="Invoice[' +
                    data.invoices[$j] + '][checked]" value="1">');
            }
            manyChargeModal.find(".many-charge-text .joint-operation-checkbox").attr('checked', 'checked');
            $("#accounts-list.modal").modal('hide');
            manyChargeModal.modal();
        }
    };

    $(document).on('click', '#add-invoice-to-payment-order', function () {
        $.post('/documents/invoice/get-invoice', $('.checkbox-invoice-id').filter(":checked").serialize(), function (data) {
            data = JSON.parse(data);
            if (data.contractorCount > 1) {
                PaymentOrder.showCreatePaymentOrders(data);
            } else {
                PaymentOrder.setInvoiceDataToInputs(data);
            }
        });
        $('.modal').modal('hide');
    });

    PaymentOrder.$relationWithInvoiceCheckbox.on('change', function () {
        PaymentOrder.clearInputs();
        if (this.checked) {
            PaymentOrder.setActivityByRelationWithInvoice();
            var $contractorID = $(this).data('contractor') !== undefined ? $(this).data('contractor') : null;
            $.pjax({
                url: '/documents/invoice/get-invoices',
                container: '#get-invoices-pjax',
                data: {contractor_id: $contractorID},
                push: false,
                timeout: 5000,
            });
        } else {
            PaymentOrder.unsetActivityByRelationWithInvoice();
        }
    });

    PaymentOrder.$budgetPaymentCheckbox.on('change', function () {
        PaymentOrder.clearInputs();
        if (this.checked) {
            PaymentOrder.setActivityBudgetPaymentInputs();
        } else {
            PaymentOrder.unsetActivityBudgetPaymentInputs();
        }
    }).trigger('init');

    PaymentOrder.$sumInDigits.on('change keyup init', function (event) {
        var text = this.value;
        text = text.replace(',', '.').replace(/[^0-9\.]/g, '');
        PaymentOrder.$sumInWords.text(ucfirst(parseFloat(text).toPhrase().trim()));
    }).trigger('init');

    $(document).on('click', '.addColumns', function () {
        $(this).closest('form').find('.dopColumns').toggleClass('selectedDopColumns');
    });

    INVOICE.tultipProduct('.tooltip-product');
});

$('input[type=radio][name="Company[nds]"]').change(function () {
    $('#view-type-input').val($('input[type=radio][name="Company[nds]"]:checked').val());
    INVOICE.recalculateInvoiceTable();
});

$(document).on('change', '#invoice-contractor_id', function () {
    if ($("#invoise-step-1").length) {
        INVOICE.recalculateInvoiceReady();
    }
});

$(document).on('change', 'input[name="Invoice[document_date]"]', function () {

    if (!$(this).data('data-is-inited')) {
        $(this).data('data-is-inited', 1);
        return;
    }

    if ($('input[name="Invoice[payment_limit_date]"]').length > 0) {
        var $payInput = $('input[name="Invoice[payment_limit_date]"]');
        var date = moment($(this).val(), "DD.MM.YYYY");
        if (date.isValid()) {
            var payDate = date.add(parseInt($payInput.data('delay')), 'days');
            $payInput.data('datepicker').selectDate(payDate.toDate());
        }
    }
});

/* CREATE FLOW inside invoice */
$(function () {
    var $flowForm = $('#add-flow-form');
    var $flowIsAccounting = $flowForm.find('.isAccounting').parents('.form-group:first');
    var $flowTypeRadio = $flowForm.find('.flow-type-radio input[type=radio]');

    $flowTypeRadio.on('change init', function () {
        if (this.checked) {
            if ($(this).hasClass('allowIsAccounting')) {
                $flowIsAccounting.show();
            } else {
                $flowIsAccounting.hide();
            }
        }
    }).trigger('init');
});

function getCurrentQuarterStartDate() {
    var d = new Date(),
        m = d.getMonth() - d.getMonth() % 3;
    return moment(new Date(d.getFullYear(), m, 1));
}

function ucfirst(str) {
    //  discuss at: http://phpjs.org/functions/ucfirst/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    //   example 1: ucfirst('kevin van zonneveld');
    //   returns 1: 'Kevin van zonneveld'

    str += '';
    var f = str.charAt(0)
        .toUpperCase();
    return f + str.substr(1);
}

$(document).on('click', 'span.btn-add-line-table', function () {
    $('#from-new-add-row').show();
});

$(document).ready(function () {
    if ($('#isNewRecord').val() == 0) {
        $('.disabled-row').hide();
    }
});

$('option[value=add-modal-contractor]').addClass('bg-add-buyer');

$('#add-from-exists').on('hide', function (e) {
    $('#product-title-search').val('');
});

$('#contractor-company_type_id').change(function () {
    if ($(this).val() == 1) {
        $('.field-contractor-ppc').hide();
        $('.field-contractor-bin label').text('ОГРНИП');
    } else {
        $('.field-contractor-ppc').show();
        $('.field-contractor-bin label').text('ОГРН');
    }
});

$(document).on('click', '.back', function () {
    $('.modal').modal('hide');
});
$(document).on('click', '.company-in-strict-mode', function (e) {
    e.preventDefault();
    var errorBlock = $('.error-summary');
    if (errorBlock.is(':hidden')) {
        errorBlock.show();
    }
    addError('strict-mode-error', 'Необходимо заполнить реквизиты вашей компании.');
    if (checkField($('#invoice-contractor_id'))) {
        addError('contractor-error', 'Контрагент не найден.');
    } else {
        removeError('contractor-error');
    }
    if ($('#company-nds').length && checkField($('#company-nds'), 'checkbox')) {
        addError('nds-error', 'Необхоидмо заполнить тип счета.');
    } else {
        removeError('nds-error');
    }
    //        '<li>Необходимо заполнить «Номер документа».</li>' +
    //        '<li>Необходимо заполнить «Дата документа».</li>' +
    //        '<li>Необходимо заполнить «Срок оплаты».</li>' +
    //        '<li>Значение «Количество заказов» должно быть не меньше 1.</li>' +
    function checkField(selector, type) {
        if (type == 'checkbox') {
            return selector.find(':checked').val() == undefined;
        }
        return selector.val() == "";
    }

    function addError(className, error) {
        if (!$('li').is('.' + className)) {
            errorBlock.find('ul').append('<li class="' + className + '">' + error + '</li>');
        }
    }

    function removeError(className) {
        if ($('li').is('.' + className)) {
            $('li.' + className).remove();
        }
    }
});

$('.modal').on('shown.bs.modal', function () {
    var phone = $('.contact_phone');
    if (phone.length > 0) {
        phone.each(function () {
            $(this).inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
        });
    }
});

//date range statistic button
$(document).on('apply.daterangepicker', '#reportrange, .statistic-period-btn', function (ev, picker) {
    var button = $(this);

    $.post(
        '/site/statistic-range',
        {
            name: picker.chosenLabel,
            date_from: picker.startDate.format('YYYY-MM-DD'),
            date_to: picker.endDate.format('YYYY-MM-DD')
        },
        function (data) {
            if (button.hasClass('ajax')) {
                $('#search-invoice-form').submit();
            } else if (button.data("pjax")) {
                $.pjax.reload(button.data("pjax"), {timeout: 10000});
            } else if (data) {
                location.href = location.href;
            }
        }
    );
});

var $osno = $('#registrationform-taxationtypeosno');
var $usn = $('#registrationform-taxationtypeusn');
var $psn = $('#registrationform-taxationtypepsn');
var $ooo = $('#registrationform-companytype [value=2], #registrationform-companytype [value=3], #registrationform-companytype [value=4]');
var $companyType = $('#registrationform-companytype');
var $companyOsno = $('#companytaxationtype-osno');
var $companyUsn = $('#companytaxationtype-usn');
var $companyPsn = $('#companytaxationtype-psn');
var $companyOoo = $('#company-company_type_id [value=2], #company-company_type_id [value=3], #company-company_type_id [value=4]');


$companyType.change(function () {
    if ($.inArray($(this).find('.checked').children().val(), ['2', '3', '4']) + 1 > 0) {
        $psn.attr('disabled', true);
        $psn.closest('div').addClass('disabled', true);
    } else {
        $psn.removeAttr('disabled');
        $psn.closest('div').removeClass('disabled');
    }
});
$('#create-company-modal #company-company_type_id').change(function () {
    var $cpPsn = $('#create-company-modal #companytaxationtype-psn');
    if ($.inArray($(this).find('.checked').children().val(), ['2', '3', '4']) + 1 > 0) {
        $cpPsn.attr('disabled', true);
        $cpPsn.closest('div').addClass('disabled', true);
    } else {
        $cpPsn.removeAttr('disabled');
        $cpPsn.closest('div').removeClass('disabled');
    }
});

changeTaxationType($companyPsn, $companyOoo);
changeTaxationType($companyOsno, $companyUsn);
changeTaxationType($companyUsn, $companyOsno);
changeTaxationType($psn, $ooo);
changeTaxationType($osno, $usn);
changeTaxationType($usn, $osno);

function changeTaxationType($changeSelector, $blockSelector) {
    $changeSelector.change(function () {
        if ($(this).is(':checked')) {
            $blockSelector.attr('disabled', true);
            $blockSelector.closest('div').addClass('disabled', true);
        } else {
            $blockSelector.removeAttr('disabled');
            $blockSelector.closest('div').removeClass('disabled');
        }
    });
}

$('#form-update-company #company-company_type_id').change(function () {
    var $companyPsn = $('#form-update-company #exist-company-psn');
    if ($(this).val() == 8) {
        $companyPsn.removeAttr('disabled');
        $companyPsn.closest('div').removeClass('disabled');
    } else {
        $companyPsn.attr('disabled', true);
        $companyPsn.closest('div').addClass('disabled', true);
    }

});

var lastClickedInput;
$(document).on('click', 'input.tooltip-product', function (e) {
    if (lastClickedInput != this) {
        e.preventDefault();
        var tmpVal = this.value;
        this.value = '';
        this.value = tmpVal;
        this.scrollLeft = this.scrollWidth;
    }
    lastClickedInput = this;
});
$(document).on('blur', 'input.tooltip-product', function (e) {
    lastClickedInput = null;
});

$(document).on('change', '#debtreportsearch-year', function () {
    location.href = $(this).closest('form').attr('action') + '?' + $('#debtreportsearch-debttype').serialize() + '&' + $(this).serialize();
});
$(document).on('change', '#debtreportsearch-debttype', function () {
    location.href = 'debtor-month?' + $(this).serialize() + '&' + $('#debtreportsearch-year').serialize();
});

$(document).on('change', '.joint-operation-main-checkbox', function () {
    if ($(this).is(':checked')) {
        $('.joint-operation-checkbox:not(:checked)').prop('checked', true).uniform('refresh');
        $('.joint-operation-main-checkbox:not(:checked)').prop('checked', true).uniform('refresh');
    } else {
        $('.joint-operation-checkbox:checked').prop('checked', false).uniform('refresh');
        $('.joint-operation-main-checkbox:checked').prop('checked', false).uniform('refresh');
    }
    $('.joint-operation-checkbox').first().trigger('change');
});

var $defaultSendHref = $('.multiple-send').attr('href');
var $defaultPrintHref = $('.multiple-print').attr('href');
var $defaultPrintHrefWithStamp = $('.multiple-print-with-stamp').attr('href');
var $defaultPrintClientHref = $('.multiple-print-client').attr('href');
var $defaultPrintSelfHref = $('.multiple-print-self').attr('href');
var $defaultDownloadPackageClientHref = $('.download-package-client').attr('href');
var $defaultDownloadPackageSelfHref = $('.download-package-self').attr('href');
var $defaultItemCopyHref = $('.item-copy').attr('href');

$(document).on('change', '.joint-operation-checkbox', function () {
    if ($('.multiple-import-link').length) {
        var idArray = $('.joint-operation-checkbox:checked').map(function () {
            return $(this).closest('tr').data('key');
        }).get();
        $('.multiple-import-link').attr('href', $('.multiple-import-link').attr('data-url') + '?' + $.param({'id': idArray}));
    }
    var $printUrlPartial = '';
    $('.joint-operation-checkbox:checked').each(function (i) {
        if (i == 0) {
            $printUrlPartial += $(this).closest('tr').data('key');
        } else {
            $printUrlPartial += ',' + $(this).closest('tr').data('key');
        }
    });
    $('.multiple-send').attr('href', $defaultSendHref + $printUrlPartial);
    $('.multiple-print').attr('href', $defaultPrintHref + $printUrlPartial);
    $('.multiple-print-with-stamp').attr('href', $defaultPrintHrefWithStamp + $printUrlPartial);
    $('.multiple-print-client').attr('href', $defaultPrintClientHref + $printUrlPartial);
    $('.multiple-print-self').attr('href', $defaultPrintSelfHref + $printUrlPartial);
    $('.download-package-client').attr('href', $defaultDownloadPackageClientHref + $printUrlPartial);
    $('.download-package-self').attr('href', $defaultDownloadPackageSelfHref + $printUrlPartial);
    $('.item-copy').attr('href', $defaultItemCopyHref + $printUrlPartial);

    var $buttons = $('.joint-operations');
    if ($('.joint-operation-checkbox:checked').length > 0) {
        $buttons.fadeIn();
        if ($('.joint-operation-checkbox:not(:checked)').length == 0) {
            $('.joint-operation-main-checkbox:not(:checked)').prop('checked', true).uniform('refresh');
        } else {
            $('.joint-operation-main-checkbox:checked').prop('checked', false).uniform('refresh');
        }
    } else {
        $buttons.fadeOut();
        $('.joint-operation-main-checkbox:checked').prop('checked', false).uniform('refresh');
    }
    var $canAddDocumentsXhr = null;
    if ($(this).closest('table').hasClass('invoice-table')) {
        var $operationsBlock = $('.actions.joint-operations');
        var $fixedOperationsBlock = $('.actions-many-items');
        var invoiceIDs = $('.joint-operation-checkbox:checked').map(function () {
            return $(this).closest('tr').data('key');
        }).get();
        if ($canAddDocumentsXhr !== null) {
            $canAddDocumentsXhr.abort();
        }
        $canAddDocumentsXhr = $.post("/documents/invoice/can-add-one-document", {
            invoices: invoiceIDs
        }, function (data) {
            var $showBorderLine = false;
            if (invoiceIDs.length > 1) {
                if (data.canAddAct) {
                    $operationsBlock.find('.create-one-act').show();
                    $fixedOperationsBlock.find('.create-one-act').show();
                } else {
                    $operationsBlock.find('.create-one-act').hide();
                    $fixedOperationsBlock.find('.create-one-act').hide();
                }
                if (data.canAddUpd) {
                    $operationsBlock.find('.create-one-upd').show();
                    $fixedOperationsBlock.find('.create-one-upd').show();
                } else {
                    $operationsBlock.find('.create-one-upd').hide();
                    $fixedOperationsBlock.find('.create-one-upd').hide();
                }
                if (data.canAddInvoiceFacture) {
                    $operationsBlock.find('.create-one-invoice-facture').show();
                    $fixedOperationsBlock.find('.create-one-invoice-facture').show();
                } else {
                    $operationsBlock.find('.create-one-invoice-facture').hide();
                    $fixedOperationsBlock.find('.create-one-invoice-facture').hide();
                }
            } else {
                $operationsBlock.find('.create-one-act').hide();
                $operationsBlock.find('.create-one-upd').hide();
                $operationsBlock.find('.create-one-invoice-facture').hide();
                $fixedOperationsBlock.find('.create-one-act').hide();
                $fixedOperationsBlock.find('.create-one-upd').hide();
                $fixedOperationsBlock.find('.create-one-invoice-facture').hide();
            }

            if (data.canAddSomeActs) {
                $operationsBlock.find('.create-acts').show();
                $fixedOperationsBlock.find('.create-acts').show();
            } else {
                $operationsBlock.find('.create-acts').hide();
                $fixedOperationsBlock.find('.create-acts').hide();
            }
            if (data.canAddSomePackingLists) {
                $operationsBlock.find('.create-packing-list').show();
                $fixedOperationsBlock.find('.create-packing-list').show();
            } else {
                $operationsBlock.find('.create-packing-list').hide();
                $fixedOperationsBlock.find('.create-packing-list').hide();
            }

            if (data.notNeedAct) {
                $operationsBlock.find('.act-not-need').show();
                $fixedOperationsBlock.find('.act-not-need').show();
                if (!$showBorderLine)
                    $showBorderLine = $('.act-not-need');
            } else {
                $operationsBlock.find('.act-not-need').hide();
                $fixedOperationsBlock.find('.act-not-need').hide();
            }
            if (data.notNeedPackingList) {
                $operationsBlock.find('.packing-list-not-need').show();
                $fixedOperationsBlock.find('.packing-list-not-need').show();
                if (!$showBorderLine)
                    $showBorderLine = $('.packing-list-not-need');
            } else {
                $operationsBlock.find('.packing-list-not-need').hide();
                $fixedOperationsBlock.find('.packing-list-not-need').hide();
            }
            if (data.notNeedUpd) {
                $operationsBlock.find('.upd-not-need').show();
                $fixedOperationsBlock.find('.upd-not-need').show();
                if (!$showBorderLine)
                    $showBorderLine = $('.upd-not-need');
            } else {
                $operationsBlock.find('.upd-not-need').hide();
                $fixedOperationsBlock.find('.upd-not-need').hide();
            }

            $('.act-not-need, .packing-list-not-need, .upd-not-need').css('border-top', 'none');
            if ($showBorderLine) {
                $showBorderLine.css('border-top', '1px solid #eee');
            }
            $('.generate-xls-many_actions').css('border-top', '1px solid #eee');

            if (data.hasAct || data.hasPackingList || data.hasInvoiceFacture || data.hasUpd) {
                if ($('.document-many-send-dropdown').length) {
                    $operationsBlock.find('.document-many-send').addClass('hidden');
                    $operationsBlock.find('.document-many-send-dropdown').removeClass('hidden');
                    $fixedOperationsBlock.find('.document-many-send').addClass('hidden');
                    $fixedOperationsBlock.find('.document-many-send-dropdown').removeClass('hidden');
                }
                var docTypes = [];
                if (data.hasAct) docTypes.push('Акт');
                if (data.hasPackingList) docTypes.push('ТН');
                if (data.hasInvoiceFacture) docTypes.push('СФ');
                if (data.hasUpd) docTypes.push('УПД');
                $operationsBlock.find('.send-invoices-with-documents').html("Счета + " + docTypes.join('/'));
            } else {
                if ($('.document-many-send-dropdown').length) {
                    $operationsBlock.find('.document-many-send').removeClass('hidden');
                    $operationsBlock.find('.document-many-send-dropdown').addClass('hidden');
                    $fixedOperationsBlock.find('.document-many-send').removeClass('hidden');
                    $fixedOperationsBlock.find('.document-many-send-dropdown').addClass('hidden');
                }
                $operationsBlock.find('.send-invoices-with-documents').html("Счета + Акт/ТН/СФ/УПД");
            }
        });
    }

    if ($('.joint-operation-checkbox:checked').length > 0 && $('#generate-xls-form').length > 0) {
        $('#generate-xls-form input:not([name="_frontendCSRF"])').remove();
        $('.joint-operation-checkbox:checked').each(function () {
            $('#generate-xls-form').append('<input type="hidden" name="' + $(this).attr("name") + '" value="1">');
        });
    }
});

$(".generate-xls-many_actions").click(function (e) {
    $(this).addClass('clicked');
    if ($('#if-free-tariff-xls').length == 0) {
        $('#generate-xls-form').submit();
    }
});

$(document).on('click', 'a.document-many-send, .modal-many-delete, .modal-many-create-act, .modal-many-charge, ' +
    '.create-one-act, .create-one-upd, .create-one-invoice-facture, .modal-many-change-responsible, ' +
    '.modal-many-change-status, .modal-not-need-document-submit, .modal-move-files-to, .modal-many-restore, .modal-many-recognize, ' +
    'a.document-many-send-with-docs', function () {
    if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
    $('#ajax-loading').show();
    var $this = $(this);
    $('#many-send-error .form-body .row').remove();
    if (!$this.hasClass('clicked')) {
        if ($('.joint-operation-checkbox:checked').length > 0) {
            $this.addClass('clicked');
            $.post($this.data('url'), $('.joint-operation-checkbox, .modal-document-date, #contractor-responsible_employee,' +
                '#contractor-status, #many-change-status-select, #many-change-sale-point-employers-select, #project-end-date').serialize(), function (data) {
                $('#ajax-loading').hide();
                if (data.notSend !== undefined) {
                    $('.joint-operation-checkbox:checked').click();
                    $('#many-send-error .form-body').append('<div class="row"><div class="col-12">' + data.notSend + '</div></div>');
                    $('#many-send-error').modal();
                    $this.removeClass('clicked');
                }
                if (data.message !== undefined) {
                    if ($('.alert-success').length > 0) {
                        $('.alert-success').remove();
                    }
                    $('.page-content').prepend('<div id="w2-success-0" class="alert-success alert fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        data.message + '</div>');
                }
            });
        }
    }
});

$(document).on('click', '.many-documents-update-status', function () {
    $('#ajax-loading').show();
    var $this = $(this);
    $('#many-send-error .form-body .row').remove();
    if (!$this.hasClass('clicked')) {
        if ($('.joint-operation-checkbox:checked').length > 0) {
            $this.addClass('clicked');
            $.post($this.data('url'), $('.joint-operation-checkbox').serialize(), function (data) {
                $('#ajax-loading').hide();
                if (data.notSend !== undefined) {
                    $('.joint-operation-checkbox:checked').click();
                    $('#many-send-error .form-body').append('<div class="row">' + data.notSend + '</div>');
                    $('#many-send-error').modal();
                    $this.removeClass('clicked');
                }
                if (data.message !== undefined) {
                    location.href = location.href;
                }
            });
        }
    }
});

$('.send-contractor').click(function () {
    var $invoices = '';
    $('.joint-operation-checkbox:checked').each(function (i) {
        if (i !== 0) {
            $invoices += ', ';
        }
        $invoices += $(this).closest('tr').data('key');
    });
    $('.send-invoices').val($invoices);
});

$(document).on('click', '.check-color', function () {
    if (!$(this).hasClass('checked-document')) {
        $('.checked-document').removeClass('checked-document');
        $(this).addClass('checked-document');
        $.post($(this).data('url'), null, function (data) {
            $('#uploaded-documents-form').closest('.uploaded-documents-data-block').replaceWith(data);
            //$('.date-picker').datepicker({
            //    keyboardNavigation: false,
            //    forceParse: false,
            //    language: 'ru',
            //    autoclose: true
            //}).on("change.dp", dateChanged);

            //function dateChanged(ev) {
            //    if (ev.bubbles == undefined) {
            //        var $input = $("#" + ev.currentTarget.id);
            //        if (ev.currentTarget.value == "") {
            //            if ($input.data("last-value") == null) {
            //                $input.data("last-value", ev.currentTarget.defaultValue);
            //            }
            //            var $lastDate = $input.data("last-value");
            //            $("#" + ev.currentTarget.id).datepicker("setDate", $lastDate);
            //        } else {
            //            $input.data("last-value", ev.currentTarget.value);
            //        }
            //    }
            //}

            $('.preview-document-photo').imagezoomsl({
                innerzoommagnifier: true,
                classmagnifier: window.external ? window.navigator.vendor === 'Yandex' ? '' : 'round-loupe' : '',
                magnifierborder: '5px solid #F0F0F0',
                zoomrange: [2, 8],
                zoomstart: 2,
                magnifiersize: [300, 300]
            });
            if ($("select").is(".nds-select")) {
                $(".nds-select").select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    }
});

$('.documents-slick').slick({
    dots: true,
    infinite: false,
    slidesToShow: 7,
    slidesToScroll: 7,
    prevArrow: ".slide-documents-left",
    nextArrow: ".slide-documents-right"
});

$(document).on('click', '.is_complete-document', function () {
    var $manyDelete = $('.delete-preview-document');
    var $manySave = $('.many-save-preview-document');
    if ($('.is_complete-document:checked').length > 0) {
        if ($('.is_complete-document.complete:checked').length > 0) {
            $manySave.show();
        } else {
            $manySave.hide();
        }
        $manyDelete.show();
        $(this).closest('.border-darc').css('margin-bottom', '10px');
    } else {
        $manySave.hide();
        $manyDelete.hide();
        $(this).closest('.border-darc').css('margin-bottom', '44px');
    }
});

$('.many-delete-uploaded-document-modal-button').click(function () {
    if ($('.is_complete-document:checked').length > 0) {
        $.post($(this).data('url'), $('.is_complete-document:checked').serialize(), function () {
            location.href = location.href;
        });
    }
});

$('.many-save-preview-document a').click(function () {
    if ($('.is_complete-document:checked').length > 0) {
        $.post($(this).data('url'), $('.is_complete-document:checked').serialize(), function () {
            location.href = location.href;
        });
    }
});

$(document).on('click', '.save-upload-documents-requisites', function () {
    var $form = $('#uploaded-documents-form');
    $form.find('input, select').css('border-color', '');
    $.post($form.data('save-upload-documents-requisites-url'), $form.serialize(), function (data) {
        if (data.result == false) {
            if (data.uploadedDocument !== undefined) {
                for (var uploadedDocumentAttributeName in data.uploadedDocument) {
                    $form.find('#uploadeddocuments-' + uploadedDocumentAttributeName).css('border-color', '#ef9ba8');
                }
            }
        }
    });
});

$(document).on('click', '.save-document', function () {
    var $form = $('#uploaded-documents-form');
    $form.find('input, select').removeAttr('style');
    $form.find('td').css('color', '').css('background-color', '').css('opacity', '');
    $.post($form.data('save-document-url'), $form.serialize(), function (data) {
        if (data.result == false) {
            if (data.uploadedDocument !== undefined) {
                for (var uploadedDocumentAttributeName in data.uploadedDocument) {
                    $form.find('#uploadeddocuments-' + uploadedDocumentAttributeName).css('border-color', '#ef9ba8');
                    $form.find('.total-amount-out #uploadeddocuments-' + uploadedDocumentAttributeName + '-td').css('color', '#ef9ba8');
                    $form.find('.table-requisites #uploadeddocuments-' + uploadedDocumentAttributeName + '-td').css('cssText', 'background-color: #f3565d !important').css('opacity', '0.3');
                }
            }
            if (data.uploadedDocumentOrder !== undefined) {
                for (var key in data.uploadedDocumentOrder) {
                    if (key == 'new') {
                        for (var newUploadedDocumentID in data.uploadedDocumentOrder[key]) {
                            for (var oldUploadedDocumentOrderAttributeName in data.uploadedDocumentOrder[key][newUploadedDocumentID][0]) {
                                $form.find('#uploadeddocumentorder-new-' + newUploadedDocumentID + '-' + oldUploadedDocumentOrderAttributeName).css('border-color', '#ef9ba8');

                            }
                        }
                    } else {
                        for (var uploadedDocumentOrderAttributeName in data.uploadedDocumentOrder[key][0]) {
                            $form.find('#uploadeddocumentorder-' + key + '-' + uploadedDocumentOrderAttributeName).css('border-color', '#ef9ba8');
                            $form.find('#uploadeddocumentorder-' + key + '-' + uploadedDocumentOrderAttributeName + '-td').css('background-color', '#f3565d').css('opacity', '0.3');
                        }
                    }
                }
            }
        }
    });
});

$(document).on('click', '.save-upload-documents-order', function () {
    var $form = $('#uploaded-documents-form');
    $form.find('input, select').css('border-color', '');
    $.post($form.data('save-upload-documents-order-url'), $form.serialize(), function (data) {
        if (data.result == false) {
            if (data.uploadedDocument !== undefined) {
                for (var uploadedDocumentAttributeName in data.uploadedDocument) {
                    $form.find('#uploadeddocuments-' + uploadedDocumentAttributeName).attr('style', 'border-color:#ef9ba8;');
                }
            }
            if (data.uploadedDocumentOrder !== undefined) {
                for (var key in data.uploadedDocumentOrder) {
                    if (key == 'new') {
                        for (var newUploadedDocumentID in data.uploadedDocumentOrder[key]) {
                            for (var oldUploadedDocumentOrderAttributeName in data.uploadedDocumentOrder[key][newUploadedDocumentID][0]) {
                                $form.find('#uploadeddocumentorder-new-' + newUploadedDocumentID + '-' + oldUploadedDocumentOrderAttributeName).attr('style', 'border-color:#ef9ba8;');

                            }
                        }
                    } else {
                        for (var uploadedDocumentOrderAttributeName in data.uploadedDocumentOrder[key][0]) {
                            $form.find('#uploadeddocumentorder-' + key + '-' + uploadedDocumentOrderAttributeName).attr('style', 'border-color:#ef9ba8;');
                        }
                    }
                }
            }
        }
    });
});

var $newRows = 1;
$(document).on('click', '#uploaded-documents-form .button-add-line .btn-add-line-table', function () {
    $newRows++;
    var $table = $(this).closest('.table');
    var $newRow = $table.find('.order-row:last').clone();
    $newRow.find('input').val('').removeAttr('style');
    $newRow.find('input, select').each(function () {
        $(this).attr('name', 'UploadedDocumentOrder[new][' + $newRows + '][' + $(this).data('id') + ']');
        $(this).attr('id', 'uploadeddocumentorder-new-' + $newRows + '-' + $(this).data('id'));
    });
    $table.find('.order-row:last').after($newRow);
});

$('.preview-document-photo').imagezoomsl({
    innerzoommagnifier: true,
    classmagnifier: window.external ? window.navigator.vendor === 'Yandex' ? '' : 'round-loupe' : '',
    magnifierborder: '5px solid #F0F0F0',
    zoomrange: [2, 8],
    zoomstart: 2,
    magnifiersize: [300, 300]
});

// $('.is_complete-document:not(.md-check, .md-radiobtn)').uniform();

$(document).on('change', '#uploadeddocuments-nds_view_type_id', function () {
    var $ndsAmount = $('#uploadeddocuments-total_amount_nds');
    if ($(this).val() == 2) {
        $ndsAmount.val('');
        $ndsAmount.attr('disabled', true);
    } else {
        $ndsAmount.attr('disabled', false);
    }
});

var $defaultUploadedDocumentsOrderInBlock = null;

$(document).on('click', '.update-uploaded-documents-order', function () {
    var $uploadedDocumentInBlock = $('.uploaded-documents-order-in');
    $defaultUploadedDocumentsOrderInBlock = $uploadedDocumentInBlock.clone();
    $defaultUploadedDocumentsOrderInBlock.find('.select2').remove();
    $(this).hide();
    $('.undo-update-uploaded-documents-order').show();
    $('.save-upload-documents-order').show();
    $('.uploaded-documents-order-out').hide();
    $uploadedDocumentInBlock.show();
    $('.requisites-block').hide();
    $('.invoice-number-block').hide();
    $('.main-buttons-block').hide();
});

$(document).on('click', '.undo-update-uploaded-documents-order', function () {
    $('.undo-update-uploaded-documents-order').hide();
    $('.save-upload-documents-order').hide();
    $('.update-uploaded-documents-order').show();
    $('.uploaded-documents-order-out').show();
    $('.uploaded-documents-order-in').replaceWith($defaultUploadedDocumentsOrderInBlock).hide();
    if ($("select").is(".nds-select")) {
        $(".nds-select").select2({
            minimumResultsForSearch: Infinity
        });
    }
    $('.requisites-block').show();
    $('.invoice-number-block').show();
    $('.main-buttons-block').show();
});

$(document).on('click', '.remove-uploaded-document-order', function () {
    var $tbody = $(this).closest('tbody');
    var $count = $tbody.find('.order-row').length;
    var $tr = $(this).closest('tr');
    if ($count > 1) {
        $tr.remove();
    } else {
        $tr.find('input, select').val('');
    }
});

var $defaultUploadedDocumentsRequisitesInBlock = null;
$(document).on('click', '.update-uploaded-documents-requisites', function () {
    var $block = $(this).closest('.uploaded-documents-block');
    $defaultUploadedDocumentsRequisitesInBlock = $block.clone();
    $block.find('.out').hide();
    $block.find('.in').show();
    $('.orders-block').hide();
    $('.invoice-number-block').hide();
    $('.main-buttons-block').hide();
    $block.find('td').css('background-color', '').css('opacity', '');
});

$(document).on('click', '.undo-update-uploaded-documents-requisites', function () {
    var $block = $(this).closest('.uploaded-documents-block');
    $block.replaceWith($defaultUploadedDocumentsRequisitesInBlock);
    $('.orders-block').show();
    $('.invoice-number-block').show();
    $('.main-buttons-block').show();
});

$(document).ready(function () {
    var $previewUploadedDocumentImg = $('.preview-img-with-loupe');
    if ($previewUploadedDocumentImg.length > 0) {
        var distanceTop = $previewUploadedDocumentImg.offset().top - 45;
        var $width = $previewUploadedDocumentImg.width();
        $(window).scroll(function () {
            if ($(document).scrollTop() > distanceTop) {
                $previewUploadedDocumentImg.width($width + 'px');
                $previewUploadedDocumentImg.addClass('fix');
            } else {
                $previewUploadedDocumentImg.removeClass('fix');
                $previewUploadedDocumentImg.width('29%');
            }
        });
    }
});

$(document).on('click', '.close-modal-button', function () {
    $(this).closest('.modal').modal('hide');
});

$('.btn-toggle').click(function () {
    $(this).find('.btn').toggleClass('active');
});

$('.notification-item .switch .btn').click(function () {
    var $status = 0;
    if ($(this).hasClass('active')) {
        $status = $(this).siblings('.btn').attr('status');
    } else {
        $status = $(this).attr('status');
    }
    $.post($(this).data('url'), {status: $status}, function () {

    });
});

$('.how-to-earn-more').click(function () {
    $('.more-affiliate_program-information').fadeIn();
});

$(document).on('click', '.copy-affiliate-link', function () {
    var affiliateLink = document.querySelector('.affiliate_invite');
    var range = document.createRange();
    range.selectNode(affiliateLink);
    window.getSelection().addRange(range);
    try {
        document.execCommand('copy');
    } catch (err) {
    }
    window.getSelection().removeAllRanges();
});

$('.generate-affiliate-link').click(function () {
    $.post($(this).data('url'), null, function (data) {
        if (data.page == 'company') {
            $('.affiliate-program-ooo').empty().append(data.html);
            $('.affiliate-program-ip').empty().append(data.html);
            $('.more-affiliate_program-information').show();
        } else {
            $('.affiliate-program-subscribe').closest('.wrap').replaceWith(data.html);
        }
    });
});

$(document).on('change', '#uploadeddocuments-document_number, #uploadeddocuments-document_date', function () {
    var $postData = {};
    $postData[$(this).data('id')] = $(this).val();
    var $input = $(this);
    $.post($(this).data('url'), $postData, function (data) {
        if (data.result == false) {
            $input.attr('style', 'border-color:#ef9ba8;');
        } else {
            $input.removeAttr('style');
        }
    });
});

$('input.switch').change(function () {
    var $block20Stock = $('.block-20stock');
    var $blockNoStock = $('.block-no-stock');
    var $blockMoreStock = $('.block-more-stock');
    var $buttons = $('.bill, .pay-card');

    $('.pay-now').toggleClass('checked');
    $('.default-tariff').toggleClass('checked');
    if ($('.switch-block .checked').data('value') == 1) {
        $block20Stock.hide();
        $blockNoStock.show();
        $blockMoreStock.hide();
        $buttons.show();
    } else {
        $blockNoStock.hide();
        $block20Stock.show();
    }
});

$('#servicemorestock-activities_id').change(function () {
    var $companyTypeText = $('.field-servicemorestock-company_type_text');
    if ($(this).val() == -1) {
        $companyTypeText.show();
    } else {
        $companyTypeText.hide();
        $companyTypeText.find('input').val('');
    }
});

$('.more-discount').click(function () {
    var $block20Stock = $('.block-20stock');
    var $blockNoStock = $('.block-no-stock');
    var $blockMoreStock = $('.block-more-stock');
    var $buttons = $('.bill, .pay-card');
    $block20Stock.hide();
    $blockNoStock.hide();
    $blockMoreStock.show();
    $buttons.hide();
});

$(document).on('submit', 'form#service-more-stock', function () {
    $.post($(this).data('url'), $(this).serialize(), function (data) {
        if (data.result == 1) {
            var $modal = $('#service-stock.modal');
            var $buttons = $('.bill, .pay-card');
            var $blockMoreStock = $('.block-more-stock');

            $blockMoreStock.hide();
            $buttons.show();
            $modal.find('.modal-header h1').text('Спасибо за ответы!');
            $modal.find('.modal-body div.text').text('Скидка 30% при оплате КУБ сегодня');
            $modal.find('.block-20stock .discount').text('30% СКИДКА').closest('.block-20stock').show();
            $modal.find('.tariff-preview-img.block-20stock').remove();
            $modal.find('.tariff-preview-img.block-30stock').removeClass('block-30stock').addClass('block-20stock').show();
            $modal.find('.block-20stock .more-discount').closest('.block-20stock').remove();
            $modal.addClass('stock-30-finish');
            $modal.find('.block-no-stock .free-trial').attr('style', 'line-height: 1.4;width: 25%;');
        }
    });
});

$('#service-stock.modal').on('hidden.bs.modal', function (e) {
    if ($(this).hasClass('stock-30-finish')) {
        $('.modal#reminder30stock').modal();
    }
});

/** Ladda button */
$(document).ready(function() {

    if ($('#never-stop-ladda-button').length)
        return;

    $(document).ajaxComplete(function (event, request, settings) {
        /** return if the request is form ajax validation */
        if (settings.type === 'POST' && settings.data) {
            let getData = new URLSearchParams(settings.url);
            let postData = new URLSearchParams(settings.data);
            if (postData.get('ajax') !== null || getData.get('ajax') !== null)
                return;
        }
        if (request.getResponseHeader('x-redirect') !== null) {
            return;
        }
        Ladda.stopAll();
    });
    $(document).on('afterValidate', 'form', function(event, messages, errorAttributes) {
        if (errorAttributes.length > 0) {
            Ladda.stopAll();
        }
    });
});
$(document).on('click', '.ladda-button:not(.payment-type-choose):not(.ladda-custom)', function (e) {
    e.preventDefault();
    if ($(this).hasClass('disabled') || $(this).is(':disabled')) {
        e.stopPropagation();
    } else {
        if (!$(this).attr('data-style')) {
            $(this).attr('data-style', 'expand-right');
        }
        var l = Ladda.create(this);
        $(this).show();
        l.start();
        if (this.form && this.form.length > 0) {
            $(this.form).trigger('submit');
        } else if ($(this).attr('href') !== undefined) {
            location.href = $(this).attr('href');
        }
        if ($(this).closest(".modal").length > 0) {
            $(this).closest(".modal").on('hidden.bs.modal', function () {
                l.stop();
            });
        }
    }
});


function initUploadFileChat($block) {
    var _uploadAreaSelector = '.upload-area-chat';
    var _uploadButtonSelector = '.upload-file-chat';
    var _$uploadArea = $block.find(_uploadAreaSelector);
    var _$uploadButton = _$uploadArea.find(_uploadButtonSelector);
    var uploadUrl = _$uploadArea.data('upload-url');
    var csrfParameter = _$uploadArea.data('csrf-parameter');
    var csrfToken = _$uploadArea.data('csrf-token');
    var data = {};
    data[csrfParameter] = csrfToken;

    var uploaderChatFile = new ss.SimpleUpload({
        button: _$uploadButton[0],
        url: uploadUrl,
        data: data,
        maxSize: 3072,
        encodeCustomHeaders: true,
        responseType: 'json',
        name: 'ChatFile',
        onComplete: function (filename, response) {
            //    if (response.result == true && response.sendToSocket == false) {
            //        var $scroller = $('div.scroller');
            //        var $template = $//('.message-template');
            //        var $chat = $('ul.chats');
            //        $template.find('.name').empty().append('<a class="name" href="/employee/view?id=' + response.from_user_id + '">' + response.name + '</a>');
            //        $template.find('.datetime').text(response.createdAt);
            //        $template.find('.body').html(response.message);
            //        $template.find('.avatar').attr('src', response.fromEmployeePhoto);
            //        $chat.append(checkTodayDelimiter());
            //        $chat.append('<li class="out">' + $template.html() + '</li>');
            //        if (response.fromEmployeeHasSong == 1) {
            //            new Audio('/frontend/web/songs/chat/chat-send.mp3').play();
            //        }
            //        $scroller.slimScroll({
            //            scrollTo: $scroller[0].scrollHeight,
            //        });
            //    }
        }
    });
}

$(document).ready(function () {
    var $chatForm = $('.chat-form');
    if ($chatForm.length > 0) {
        initUploadFileChat($chatForm);
    }
});

var isCtrl = false;
$(document).on('keyup', 'textarea.message', function (e) {
    if (e.which == 17) {
        isCtrl = false;
    }
});

$(document).on('keydown', 'textarea.message', function (e) {
    if (e.which == 17) {
        isCtrl = true;
    }
    if (e.which == 13 && isCtrl == true) {
        $(this).val($(this).val() + '\r\n');
        return false;
    } else if (e.which == 13 && isCtrl == false) {
        if ($(this).val().trim() !== '') {
            $(this).closest('form').submit();
        }
        return false;
    }
});

$(document).on('change', '#contractor-physical_no_patronymic', function () {
    if ($(this).is(':checked')) {
        $('#contractor-physical_patronymic').attr('readonly', true).val('');
    } else {
        $('#contractor-physical_patronymic').attr('readonly', false);
    }
});

$('select#flowoffundsreportsearch-year, #profitsearch-year, #paymentcalendarsearch-year, #planfactsearch-year, #balancesearch-year, ' +
    '#incomesearch-year, #expensessearch-year, #profitandlosssearchmodel-year, #productanalysisabc-year, #debtreportsearch2-year, ' +
    '#stockssearch-year, #oddssearch-year, #breakevensearchmodel-yearmonth, #operationalefficiencysearchmodel-year').change(function () {
    location.href = $(this).closest('form').attr('action') + '?' + $(this).closest('form').serialize();
});

$('table.flow-of-funds tbody tr.expenditure_type .flow-of-funds-type').change(function () {
    $('table.flow-of-funds tbody').find('tr[data-id="' + $(this).closest('tr').attr('id') + '"]').toggleClass('hidden');
    $('table.flow-of-funds tbody').find('tr[data-id="' + $(this).closest('tr').attr('id') + '"] td').each(function () {
        $(this).css('width', $(this).outerWidth(true));
    });
});

$('.balance-report .balance-type').change(function () {
    var $class = $(this).closest('tr').data('children');
    var $val = $(this).val();

    $('.balance-report tr.' + $class).toggleClass('hidden');
    if ($('.update-balance-item-panel:visible').length > 0) {
        $('.undo-update-balance-item').click();
    }
    if ($class == 'current-assets') {
        $('tr[data-children="receivable"] td .balance-type, tr[data-children="cash"] td .balance-type').each(function (e) {
            if ($val == 1) {
                $(this).closest('tr').addClass('hidden');
                if ($(this).val() == 0) {
                    $(this).click();
                }
            } else {
                $(this).closest('tr').removeClass('hidden');
                if ($(this).val() == 1) {
                    $(this).click();
                }
            }
        });
    }
    if ($class == 'shorttermLiabilities') {
        $('tr[data-children="shorttermAccountsPayable"] td .balance-type').each(function (e) {
            if ($val == 1) {
                $(this).closest('tr').addClass('hidden');
                if ($(this).val() == 0) {
                    $(this).click();
                }
            } else {
                $(this).closest('tr').removeClass('hidden');
                if ($(this).val() == 1) {
                    $(this).click();
                }
            }
        });
    }
    if ($class == 'capital') {
        $('tr[data-children="capital-undestributed-profits"] td .balance-type').each(function (e) {
            if ($val == 1) {
                $(this).closest('tr').addClass('hidden');
                if ($(this).val() == 0) {
                    $(this).click();
                }
            } else {
                $(this).closest('tr').removeClass('hidden');
                if ($(this).val() == 1) {
                    $(this).click();
                }
            }
        });
    }
});

$('table.profit-and-loss-table .profit_and_loss-type').change(function () {
    $('table.profit-and-loss-table tr.' + $(this).closest('tr').data('children')).toggleClass('hidden');
});

$('table.profit-report tbody tr.consumption-total-row .profit-consumption-type').change(function () {
    $('table.profit-report tbody').find('tr.profit-consumption-type-hidden').toggleClass('hidden');
    $('table.profit-report tbody').find('tr.profit-consumption-type-hidden td').each(function () {
        $(this).css('width', $(this).outerWidth(true));
    });
});

//$('#header_chat_bar .dropdown-menu > li ul').slimScroll({
//    height: '270px',
//    start: 'bottom',
//});

$('.quarters-flow-of-funds .quarter-checkbox, .quarters-profit-report .quarter-checkbox').change(function () {
    var th = $(this).closest('th');
    var label = th.find('label');
    var labelText = label.text().trim();
    if ($(this).val() == 0) {
        label.text(labelText.replace('кв.', 'квартал'));
        th.css('min-width', '180px');
        $('.' + $(this).data('month-class')).show();
        $('.' + $(this).data('total-quarter-class')).hide();
        $(this).closest('th').attr('colspan', 3);
    } else {
        label.text(labelText.replace('квартал', 'кв.'));
        th.css('min-width', '172px');
        $('.' + $(this).data('month-class')).hide();
        $('.' + $(this).data('total-quarter-class')).show();
        $(this).closest('th').attr('colspan', 1);
    }
});

$('#articles-income button, #articles-expense button').click(function () {
    $(this).hide();
    $(this).siblings('button').show();
    if ($(this).data('action') == 'collapse') {
        $(this).siblings('ol').hide();
    } else {
        $(this).siblings('ol').show();
    }
});

$('#articles-income .dd-handle, #articles-expense .dd-handle').mouseover(function () {
    $(this).find('.glyphicon-trash, .glyphicon-pencil').show();
}).mouseout(function () {
    $(this).find('.glyphicon-trash, .glyphicon-pencil').hide();
});

$('.article-box .nav-item').click(function () {
    var $siblingsTab = $(this).siblings('.nav-item');
    var $showItemID = $(this).find('a').data('item-id');
    var $hideItemID = $(this).siblings('.nav-item').find('a').data('item-id');
    $(this).find('a').toggleClass('active');
    $siblingsTab.find('a').toggleClass('active');

    $('#' + $showItemID).show();
    $('#' + $hideItemID).hide();
});

$(document).on('keyup change', 'input#article-search', function (e) {
    var $activeList = $('.article-box div.dd:visible');
    var $searchArticle = $(this).val().toLowerCase();

    $activeList.find('button#expand:visible').click();

    if ($searchArticle !== '') {
        $activeList.find('.article-name').each(function () {
            if ($(this).text().toLowerCase().search($searchArticle.toLowerCase()) == -1) {
                $(this).closest('.dd-handle').hide();
                $(this).closest('.dd-handle').siblings('button#collapse').hide();
            } else {
                $(this).closest('.dd-handle').show();
                $(this).closest('.dd-handle').siblings('button#collapse').show();
            }
        });
    } else {
        $activeList.find('.article-name').closest('.dd-handle').show();
        $activeList.find('.article-name').closest('.dd-handle').siblings('button#collapse').show();
    }
});

$('#article-type').change(function () {
    var $articleType = $(this).find('input:checked').val();
    var $articleInput = $(this).closest('form').find('#article-article_id');
    $articleInput.find('option').remove();
    var $articles = {};
    if ($articleType == 1) {
        $articles = $articleInput.data('income-items');
    } else {
        $articles = $articleInput.data('expense-items');
    }
    $articleInput.append(new Option('', ''));
    for (var $id in $articles) {
        var $option = new Option($articles[$id], $id);
        $articleInput.append($($option));
    }
});

$(document).on("click", ".new-product-show", function () {
    $(".new-product-row").removeClass("hidden");
});

$(document).on("click", ".new-product-hide", function () {
    $(".new-product-row").addClass("hidden");
});

$(document).on("click", ".selected-product-delete", function () {
    $(this).closest(".selected-product-row").remove();
});

$(document).on("change", ".flow-reason-field", function () {
    var $reasonBox = $(".other-reason-box", this.form);
    if (this.value == $reasonBox.data("other")) {
        $reasonBox.show();
    } else {
        $reasonBox.hide();
    }
});

$(document).on("change", "#cashorderflows-contractorinput, #cashemoneyflows-contractor_id-", function () {
    $(".internal_flows", this.form).collapse({toggle: false});
    $(".internal_flows", this.form).collapse("hide");
    $(".internal_flows select", this.form).val("").trigger("change");
    if (this.value.substr(0, 4) == "bank") {
        $(".flow-rs-selector select").prop("selectedIndex", 1).trigger("change"); // 0 - empty
        $(".flow-rs-selector", this.form).collapse("show");
    }
    else if (this.value.substr(0, 6) == "emoney") {
        $(".flow-emoney-selector select").prop("selectedIndex", 1).trigger("change"); // 0 - empty
        $(".flow-emoney-selector", this.form).collapse("show");
    }
    else if (this.value.substr(0, 5) == "order") {
        if ($(this).attr('name') === "CashOrderFlows[contractorInput]") { // order to order
            $(".flow-order-selector").find("[type=checkbox]").prop("checked", true).uniform("refresh");
        } else {
            $(".flow-order-selector select").prop("selectedIndex", 1).trigger("change"); // 0 - empty
            $(".flow-order-selector", this.form).collapse("show");
            $(".flow-order-selector").find("[type=checkbox]").prop("checked", false).uniform("refresh");
        }
    }
});

$(document).ready(function () {
/*
    var $flowOfFundsTable = $('table.flow-of-funds tbody');
    if ($flowOfFundsTable.length > 0) {
        $flowOfFundsTable.sortable({
            cancel: 'tr.not-drag',
            handle: ".sortable-row-icon",
            start: function (event, ui) {
                ui.item.find('td').each(function () {
                    $(this).css('border', '1px solid #ddd');
                });
            },
            stop: function (event, ui) {
                var $item = ui.item;
                var $itemID = $item.data('item_id');
                var $prevItem = $item.prev();
                var $blockTypeID = null;
                var $type = 'expense';
                var $year = null;
                var $reportType = null;

                if ($('select#flowoffundsreportsearch-year').length > 0) {
                    $year = $('select#flowoffundsreportsearch-year').val();
                    $reportType = 'odds';
                } else if ($('select#paymentcalendarsearch-year').length > 0) {
                    $reportType = 'paymentCalendar';
                    $year = $('select#paymentcalendarsearch-year').val();
                }
                if ($prevItem.hasClass('cancel-drag') || $prevItem[0] == undefined) {
                    $flowOfFundsTable.sortable('cancel');
                } else {
                    if ($item.hasClass('income')) {
                        $type = 'income';
                    }
                    if (($prevItem.hasClass('income') && $type == 'expense') ||
                        ($prevItem.hasClass('expense') && $type == 'income')) {
                        $flowOfFundsTable.sortable('cancel');
                    } else {
                        if ($prevItem.hasClass('expenditure_type')) {
                            $blockTypeID = $prevItem.attr('id');
                            if ($prevItem.find('input').val() == 1) {
                                $prevItem.find('input').click();
                            }
                        } else {
                            $blockTypeID = $prevItem.data('id');
                            if ($prevItem.is(':hidden')) {
                                $('.expenditure_type#' + $blockTypeID + ' input').click();
                            }
                        }
                        if (+$blockTypeID == +$item.attr('data-id')) {
                            $flowOfFundsTable.sortable('cancel');
                        } else {
                            $('#hellopreloader').show();
                            $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);

                            $.post('/analytics/finance/change-block', {
                                item_id: $itemID,
                                block_type_id: $blockTypeID,
                                type: $type,
                                year: $year,
                                reportType: $reportType
                            }, function ($data) {
                                if ($data.result == true) {
                                    $item.attr('data-id', $blockTypeID);
                                    var $html = $($.parseHTML($data.html));
                                    $html.find('table.flow-of-funds tbody tr').each(function(i, tr) {
                                        $(tr).find('td').each(function(j, td) {
                                            $('table.flow-of-funds tbody').find('tr').eq(i).find('td').eq(j).html($(td).html());
                                        });
                                    });
                                    //$html.find('table.flow-of-funds tbody td:not(.checkbox-td, .tooltip-td)').each(function () {
                                    //    var $className = 'td.' + $(this).attr('class').trim().replace(/\s/ig, '.');
                                     //   $($className).html($(this).html());
                                    //});
                                    var hellopreloader = document.getElementById("hellopreloader_preload");
                                    fadeOutPreloader(hellopreloader);
                                } else {
                                    $flowOfFundsTable.sortable('cancel');
                                    var hellopreloader = document.getElementById("hellopreloader_preload");
                                    fadeOutPreloader(hellopreloader);
                                }
                            });
                        }
                    }
                }
            }
        }).disableSelection();
    }
*/
    var $balanceTable = $('table.balance-report-table tbody');
    if ($balanceTable.length > 0) {
        $balanceTable.sortable({
            cancel: 'tr.not-drag',
            handle: "img.sortable-row-icon",
            start: function (event, ui) {
                ui.item.find('td').each(function () {
                    $(this).css('border', '1px solid #ddd');
                });
            },
            stop: function (event, ui) {
                var $item = ui.item;
                var $prevItem = $item.prev();
                var $class = $item.attr('class');
                var $data = {};

                if (!$prevItem.hasClass($item.attr('class')) && $prevItem.attr('data-children') !== $item.attr('class')) {
                    $balanceTable.sortable('cancel');
                } else {
                    $('tr.' + $class).each(function (i) {
                        $data[$(this).data('itemid')] = ++i;
                    });
                    $('#hellopreloader').show();
                    $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);

                    $.post('/analytics/finance/change-balance-block', {'items': $data}, function ($data) {
                        if (!$data) {
                            $balanceTable.sortable('cancel');
                        }
                        var hellopreloader = document.getElementById("hellopreloader_preload");
                        fadeOutPreloader(hellopreloader);
                    });
                }
            }
        }).disableSelection();
    }

    var $profitAndLossTable = $('table.profit-and-loss-table tbody');
    if ($profitAndLossTable.length > 0) {
        $profitAndLossTable.sortable({
            cancel: 'tr.not-drag',
            handle: "img.sortable-row-icon",
            start: function (event, ui) {
                ui.item.find('td').each(function () {
                    $(this).css('border', '1px solid #ddd');
                });
            },
            stop: function (event, ui) {
                var $item = ui.item;
                var $prevItem = $item.prev();

                if (!$prevItem.hasClass('expenses-block')) {
                    $profitAndLossTable.sortable('cancel');
                } else {
                    $('#hellopreloader').show();
                    $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);

                    $.post('/analytics/finance/change-profit-and-loss-block', {
                        item_id: $item.data('id'),
                        expense_type: $prevItem.data('block'),
                        year: $('#profitandlosssearchmodel-year').val()
                    }, function ($data) {
                        if (!$data.result) {
                            $profitAndLossTable.sortable('cancel');
                        } else {
                            $item.data('block', $prevItem.data('block'));
                            var $html = $($.parseHTML($data.html));

                            $html.find('table.profit-and-loss-table tbody td.value-cell').each(function ($i) {
                                var $className = 'td.' + $(this).attr('class').trim().replace(/\s/ig, '.');
                                $($className).html($(this).html());
                            });
                        }
                        var hellopreloader = document.getElementById("hellopreloader_preload");
                        fadeOutPreloader(hellopreloader);
                    });
                }
            }
        }).disableSelection();
    }
});

function fadeOutPreloader(el) {
    el.style.opacity = 1;
    var interPreloader = setInterval(function () {
        el.style.opacity = el.style.opacity - 0.05;
        if (el.style.opacity <= 0.05) {
            clearInterval(interPreloader);
            hellopreloader.style.display = "none";
        }
    }, 16);
}

$(document).ready(function () {
    var $select = $('.project-multiple-select');
    $select.each(function () {
        var $checkedDefault = [];
        $(this).closest('.form-group').find('.selected-items-list li').each(function () {
            $checkedDefault.push(String($(this).data('id')));
        });
        $(this).val($checkedDefault).trigger('change');
    });
    $select.on('select2:open', function () {
        var $select2DropDown = $('.select2-container--open .select2-dropdown');
        if ($('.select2-container--open .select2-dropdown .project-select2-search').length == 0) {
            $select2DropDown.prepend('<span class="project-select2-search select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox"></span>');
        }
        $('.select2-container--open .select2-dropdown .project-select2-search input').val('');
    });
    $('#project-expenseitems').on('select2:open', function () {
        var $select2DropDown = $('.select2-container--open .select2-dropdown');
        if ($('.select2-container--open .select2-dropdown .add-new-expenditure-item-form').length == 0) {
            $select2DropDown.append('<span class="add-new-expenditure-item-form" style="display: inline-block; position: relative; width: 100%; padding: 5px 52px 5px 5px; border-top: 1px solid #aaa;">' +
                '<button class="btn yellow project-new-expenditure-item-submit" style="position: absolute; right: 12px; top: 5px; padding: 0 7px; font-size: 24px;">' +
                '<span class="fa icon fa-plus-circle"></span>' +
                '</button>' +
                '<input type="text" class="form-control" name="name" maxlength="45" placeholder="Добавить статью расходов" style="display: inline-block; width: 100%;">' +
                '</span>');
        }
    });

    $(document).on('click', '.project-new-expenditure-item-submit', function () {
        var input = $(this).parent().children('input');
        $.post('/site/add-expenditure-item', input.serialize(), function (data) {
            if (data.itemId && data.itemName) {
                var $input = $('#project-expenseitems');
                var newOption = new Option(data.itemName, data.itemId, false, false);
                $input.select2("close");
                $input.append(newOption).trigger('change');
                input.val('');
            }
        });
    });

    $(document).on('keyup change', '.project-select2-search input', function (e) {
        var $select2Container = $(this).closest('.select2-container');
        var $results = $select2Container.find('.select2-results .select2-results__options');
        var $searchText = $(this).val();

        if ($searchText !== '') {
            $results.find('li').each(function () {
                if ($(this).text().toLowerCase().search($searchText.toLowerCase()) == -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
            if ($results.find('li:visible').length == 0) {
                $results.prepend('<li role="treeitem" aria-live="assertive" class="select2-not-found select2-results__option select2-results__message">Совпадений не найдено</li>');
            } else {
                $results.find('.select2-not-found').remove();
            }
        } else {
            $results.find('li').show();
            $results.find('.select2-not-found').remove();
        }
    });
    $select.on('select2:select', function (e) {
        if (e.params.data.id !== 'add-modal-contractor') {
            $(this).siblings('.selected-items-list').append(
                '<li class="select2-selection__choice item-' + e.params.data.id + '" title="' + e.params.data.text + '" data-id="' + e.params.data.id + '">' +
                '<span class="select2-selection__choice__remove" role="presentation">×</span>' +
                e.params.data.text +
                '</li>');
        }
    });
    $select.on('select2:unselect', function (e) {
        $(this).siblings('.selected-items-list').find('.select2-selection__choice.item-' + e.params.data.id).remove();
    });
    $(document).on('click', '.selected-items-list .select2-selection__choice .select2-selection__choice__remove', function () {
        var $input = $('#' + $(this).closest('.selected-items-list').data('input_id'));
        var $value = $(this).closest('.select2-selection__choice').data('id');
        var $checked = $input.val();

        $checked.splice(+$checked.indexOf($value + ''), 1);
        $input.val($checked).trigger('change');
        $(this).closest('.select2-selection__choice').remove();
    });

    $('#description_project_update').click(function () {
        var $fieldProjectDescription = $(this).closest('.field-project-description');
        $fieldProjectDescription.find('.inp_one_line-product').toggleClass('hidden');
        $fieldProjectDescription.find('textarea').focus();
    });

    $('.project-analyse-checkbox').change(function () {
        $('tr.' + $(this).attr('id') + '-item').toggleClass('hidden');
    });

    $('.change-order-document .previous, .change-order-document .next').click(function () {
        location.href = $(this).data('url');
    });
    $('.submit-product-form').click(function () {
        $('form#product-form').submit();
    });

    var $productImage = $('.update-product #product-form .field-product-image');
    if ($productImage.length > 0) {
        var $label = $productImage.find('label:first').clone();
        $label.css('padding-left', 0);
        $label.css('width', '120px');
        $label.css('padding-top', '2px');
        $productImage.find('label').remove();
        setTimeout(function () {
            $productImage.find('.fileinput-new div:first').css('margin-top', '-14px').prepend($label);
        }, 1000);
    }
});

var emptyGridTableCollspan = function () {
    $("table > tbody > tr > td > div.empty").each(function () {
        var $emptyCell = $(this).closest("td");
        var $emptyTable = $emptyCell.closest("table");
        var currentColspan = parseInt($emptyCell.attr("colspan"));
        var rightCollspan = $("thead > tr > th:not(.hidden)", $emptyTable).length;
        if (currentColspan && currentColspan != rightCollspan) {
            $emptyCell.attr("colspan", rightCollspan);
        }
    });
}

emptyGridTableCollspan();

$(document).on('pjax:complete', function () {
    emptyGridTableCollspan();
});

$(document).on('click', '#modal-loader-items .link', function () {
    location.href = $(this).data('url');
});
$(document).on('click', '#modal-loader-items .btn-video', function () {
    $('#modal-loader-items .hide-video, #modal-loader-items .show-video').toggle();
});
$(document).on('click', '#modal-loader-items .btn-ok, #modal-loader-items .not-show', function () {
    $(this).closest('.modal').modal('hide');
});

var $defaultSettingsStoreForm = null;
$(document).ready(function () {
    var $settingsForm = $('.form-store-settings');
    if ($settingsForm.length > 0) {
        $defaultSettingsStoreForm = $settingsForm.clone();
    }
});

$('.update-store-settings').click(function () {
    $(this).addClass('hidden');
    $(this).closest('.tab-pane').find('.form-actions').removeClass('hidden');
    $(this).closest('.settings-block').find('input:not(.default-disabled), select:not(.default-disabled)').removeAttr('disabled');
    $('a.action-buttons').removeClass('hidden');
    $.uniform.update(':not(.md-check, .md-radiobtn)');
});

$('.settings-block a.submit-settings').click(function () {
    $(this).siblings('form').submit();
})

$(document).on('click', '.undo-update-store-settings', function () {
    $('.form-actions').addClass('hidden');
    $('a.action-buttons').addClass('hidden');
    $('.update-store-settings').removeClass('hidden');
    if ($defaultSettingsStoreForm !== null) {
        $('.form-store-settings').replaceWith($defaultSettingsStoreForm);
        $defaultSettingsStoreForm = $('.form-store-settings').clone();
        $('input[type="checkbox"]:not(.md-check), input[type="radio"]:not(.md-radiobtn)').uniform();
    }
});

$(document).on('change', '#company-store_where_empty_products_type', function () {
    var $disabledInput = $('#company-store_where_empty_products_text_type');
    if ($(this).find('input:checked').val() == 2) {
        $disabledInput.attr('disabled', false);
    } else {
        $disabledInput.attr('disabled', true);
    }
});

$(document).on('change', '#company-store_show_novelty_button', function () {
    var $disabledInput = $('#company-store_novelty_product_by_days_count');
    if ($(this).is(':checked') == true) {
        $disabledInput.attr('disabled', false);
    } else {
        $disabledInput.attr('disabled', true);
        $disabledInput.val('');
    }
});

$(document).on('change', '#company-store_has_discount', function () {
    var $disabledInput = $('#company-store_discount_from_amount');
    var $disabledRadioInputs = $('#company-store_discount_type input:text');
    if ($(this).is(':checked') == true) {
        $disabledInput.attr('disabled', false);
        $('#company-store_discount_type span.checked').closest('div.radio').siblings('input:text').attr('disabled', false);
    } else {
        $disabledInput.attr('disabled', true);
        $disabledRadioInputs.attr('disabled', true);
        $disabledInput.val('');
    }
});

$(document).on('change', '#company-store_discount_type', function () {
    if ($('#company-store_has_discount').is(':checked')) {
        var $disabledPercentInput = $('#company-store_discount');
        var $disabledAnotherInput = $('#company-store_discount_another');
        if ($(this).find('input:checked').val() == 0) {
            $disabledPercentInput.attr('disabled', false);
            $disabledAnotherInput.attr('disabled', true);
        } else {
            $disabledAnotherInput.attr('disabled', false);
            $disabledPercentInput.attr('disabled', true);
        }
    }
});

$(document).on('click', '.send-phone-block a.send-phone', function () {
    $phone = $(this).siblings('[name="phone"]').val();
    $question = $(this).siblings('[name="question"]').val();
    $numberCount = $phone.length - $phone.replace(/\d/gm, '').length;
    $modal = $(this).closest('.modal');

    if ($numberCount == 11) {
        $.post($(this).data('url'), {
            'phone': $phone,
            'question': $question,
            'type': $(this).data('type')
        }, function (data) {
            if (data.result == true) {
                $modal.modal('hide');
            }
        });
    }
});

var $modal = $("#modal-loader-items.modal");
$(".next-modal:not(.expose-invoice)").click(function () {
    $(this).closest(".modal").modal("hide");
    loadPageByItem($(this).data("type"), true);
});
$(".expose-invoice").click(function () {
    var $url = $(this).data("url");
    $.ajax({
        url: "/site/get-modal-registration/?type=1",
        type: "get",
        success: function (data) {
            location.href = $url;
        }
    });

});
$(document).on("click", ".loadPage", function () {
    var $type = $(this).data("page");
    loadPageByItem($type, false);
});

function loadPageByItem(type, showModal) {
    var $url = null;
    if (type == "") {
        $url = "/site/get-modal-registration";
    } else {
        $url = "/site/get-modal-registration/?type=" + type;
    }
    $.ajax({
        url: $url,
        type: "get",
        success: function (data) {
            if (data.result == true) {
                if (data.showMainModal == true) {
                    $(".modal#modal-loader-items").modal("hide");
                    $(".modal#modal-kub-do-it").modal();
                }
                $modal.find("h2.header-name").html(data.header);
                $modal.find("#modal-loader").html(data.html);
                $("input#not-show:not(.md-check, .md-radiobtn)").uniform();
                if (showModal) {
                    $modal.first().modal();
                }
            }
        }
    });
}

$("#modal-kub-do-it .next-modal").mouseover(function () {
    $(this).addClass("border4");
});
$("#modal-kub-do-it .next-modal").mouseleave(function () {
    $(this).removeClass("border4");
});
$(document).on("change", "input#not-show", function () {
    $(this).closest(".modal").modal("hide");
    $.post("/site/cancel-modal-show", {type: $(this).data("type")}, function (data) {
    });
});
$('.benefit-block .list-header').click(function () {
    var $visibleList = $(this).closest('.benefit-block').find('.list-body:not(.hidden)');
    if ($visibleList[0] != $(this).next('.list-body')[0]) {
        $visibleList.addClass('hidden');
    }
    $(this).next('.list-body').toggleClass('hidden');
});
$('.update-store-settings').click(function () {
    $(this).addClass('hidden');
    $(this).closest('.tab-pane').find('.form-actions').removeClass('hidden');
});
$('.undo-update-store-settings').click(function () {
    $(this).closest('.form-actions').addClass('hidden');
    $('.update-store-settings').removeClass('hidden');
});
$(document).on('click', 'td.activate-store-account-row', function () {
    $(this).find('input').trigger('click');
});
$('.profile-form-tabs .main-tabs li').click(function () {
    var href = $(this).find('a').attr('href');
    if (!$(this).hasClass('active')) {
        $('.profile-form-tabs .tab-pane' + href + ' .nav-form-tabs li:first a').click();
    }
});
$(document).on('keyup change', '.message-template-body_editable_block', function () {
    $(this).siblings('#paymentremindermessage-message_template_body:hidden').val($(this).html());
});

$(document).ready(function () {
    $('.menu-settings a').click(function () {
        if ($(this).data('first_show') == 1) {
            $.post($(this).data('url'), null, function (data) {
            });
        }
    });
    if ($('.menu-settings a').data('pulsate') == 1) {
        $('.menu-settings a i').pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    }
    $('.modal#modal-loader-items').each(function () {
        if ($(this).find('.submodal-content').length == 0) {
            $(this).remove();
        }
    });
});

$(document).ready(function () {
    $(".payment-order .reload-status").click(function () {
        var date = new Date();
        var status = null;

        date = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
        if ($(this).hasClass('import')) {
            status = 'Экспортировано в банк';
        } else if ($(this).hasClass('print')) {
            status = 'Распечатано';
        }
        if (status !== null) {
            $('.payment-order .status-name').text(status);
            $('.payment-order .status-date').text(date);
        }
    });
});

$(document).on('change', '.main-checkbox-side', function () {
    if ($(this).val() == 1) {
        $(".flow-of-funds-type, .balance-type, .profit_and_loss-type").each(function () {
            if ($(this).val() == 0) {
                $(this).click();
            }
        });
    } else {
        $(".flow-of-funds-type, .balance-type, .profit_and_loss-type").each(function () {
            if ($(this).val() == 1) {
                $(this).click();
            }
        });
    }
});

$(document).on('change', '.selling-contractor-main-checkbox', function () {
    if ($(this).val() == 1) {
        $(".selling-contractor-checkbox").each(function () {
            if ($(this).val() == 0) {
                $(this).click();
            }
        });
    } else {
        $(".selling-contractor-checkbox").each(function () {
            if ($(this).val() == 1) {
                $(this).click();
            }
        });
    }
});

$(document).on('change', '.analytics-contractor-main-checkbox', function () {
    if ($(this).val() == 1) {
        $(".analytics-contractor-checkbox").each(function () {
            if ($(this).val() == 0) {
                $(this).click();
            }
        });
    } else {
        $(".analytics-contractor-checkbox").each(function () {
            if ($(this).val() == 1) {
                $(this).click();
            }
        });
    }
});

$('.invoice-block .dropdown a#create-invoice').click(function () {
    $('#invoice-is_invoice_contract').val(0);
    $('.invoice-block .dropdown .form-title-link').text($(this).text());
    $(this).addClass('active');
    $('.invoice-block .dropdown ul a#create-invoice-contract').removeClass('active');
    $('.field-invoice_contract-essence').addClass('hidden');
    $('.field-invoice_comment').removeClass('hidden');
    $('.field-invoice-essence').removeClass('hidden');
    $(this).parents('.dropdown-menu').removeClass('show');
    $(this).parents('.dropdown').removeClass('show');
});

$('.invoice-block .dropdown a#create-invoice-contract').click(function () {
    $('#invoice-is_invoice_contract').val(1);
    $('.invoice-block .dropdown .form-title-link').text($(this).text());
    $(this).addClass('active');
    $('.invoice-block .dropdown ul a#create-invoice').removeClass('active');
    $('.field-invoice_contract-essence').removeClass('hidden');
    $('.field-invoice_comment').addClass('hidden');
    $('.field-invoice-essence').addClass('hidden');
    $(this).parents('.dropdown-menu').removeClass('show');
    $(this).parents('.dropdown').removeClass('show');
});

$('.autoinvoice-block .dropdown a#create-invoice').click(function () {
    $('#invoice-is_invoice_contract').val(0);
    $('.autoinvoice-block .dropdown .form-title-link').text($(this).text());
    $(this).addClass('active');
    $('.autoinvoice-block .dropdown ul a#create-invoice-contract').removeClass('active');
    $('.field-invoice_contract-essence').addClass('hidden');
    $('.field-invoice_comment').removeClass('hidden');
    $('.field-invoice-essence').removeClass('hidden');
    $(this).parents('.dropdown-menu').removeClass('show');
    $(this).parents('.dropdown').removeClass('show');
});

$('.autoinvoice-block .dropdown a#create-invoice-contract').click(function () {
    $('#invoice-is_invoice_contract').val(1);
    $('.autoinvoice-block .dropdown .form-title-link').text($(this).text());
    $(this).addClass('active');
    $('.autoinvoice-block .dropdown ul a#create-invoice').removeClass('active');
    $('.field-invoice_contract-essence').removeClass('hidden');
    $('.field-invoice_comment').addClass('hidden');
    $('.field-invoice-essence').addClass('hidden');
    $(this).parents('.dropdown-menu').removeClass('show');
    $(this).parents('.dropdown').removeClass('show');
});

$(document).on('keyup change', '#invoice-contract_essence, #invoiceauto-contract_essence', function () {
    var $contractEssenceIsChecked = $('#invoicecontractessence-is_checked');
    if ($contractEssenceIsChecked.is(':checked')) {
        $contractEssenceIsChecked.click();
        $contractEssenceIsChecked.closest('.field-invoice_contract-essence').find('.col-xs-11 label').css('color', '#f3565d;');
    }
});

$('#invoicecontractessence-is_checked').click(function () {
    $(this).closest('.field-invoice_contract-essence').find('.col-xs-11 label').css('color', '#333333;');
});

$(document).on('keyup change', '#invoice-comment, #invoiceauto-comment', function () {
    var $essenceIsChecked = $('#invoiceessence-is_checked');
    if ($essenceIsChecked.is(':checked')) {
        $essenceIsChecked.click();
        $essenceIsChecked.closest('.field-invoice-essence').find('.col-xs-11 label').css('color', '#f3565d;');
    }
});

$('#invoiceessence-is_checked').click(function () {
    $(this).closest('.field-invoice-essence').find('.col-xs-11 label').css('color', '#333333;');
});

$(document).on('keyup change', '#invoicesendform-emailtext', function () {
    var $emailTextIsChecked = $('#invoiceemailtext-is_checked');
    if ($emailTextIsChecked.is(':checked')) {
        $emailTextIsChecked.click();
        $emailTextIsChecked.closest('.field-invoicesendform-invoice_email_text').find('.col-xs-11 label').css('color', '#f3565d;');
    }
});

$('#invoiceemailtext-is_checked').click(function () {
    $(this).closest('.field-invoicesendform-invoice_email_text').find('.col-xs-11 label').css('color', '#333333;');
});
$(document).on('change', 'table#analytics-contractor-table .analytics-contractor-checkbox, table#selling-contractor-table .selling-contractor-checkbox', function () {
    var $chart = $(this).closest('tr').next('tr').find('.high-chart');
    if ($(this).val() == 1) {
        $chart.hide();
        $(this).closest('tr').next('tr').hide();
    } else {
        $chart.show();
        $(this).closest('tr').next('tr').show();
    }
});

function number_format(number, decimals, decPoint, thousandsSep) {
    // eslint-disable-line camelcase
    //  discuss at: http://locutus.io/php/number_format/
    // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // improved by: davook
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Theriault (https://github.com/Theriault)
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // bugfixed by: Michael White (http://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (http://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    //  revised by: Luke Smith (http://lucassmith.name)
    //    input by: Kheang Hok Chin (http://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (http://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56)
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ')
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '')
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.')
    //   returns 4: '67,00'
    //   example 5: number_format(1000)
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2)
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1)
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.')
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0)
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2)
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4)
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3)
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ')
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '')
    //  returns 14: '0.00000001'

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

$(document).on('click', '#get-invoices-pjax a', function (e) {
    e.preventDefault();
    $.pjax.reload('#get-invoices-pjax', {
        url: $(this).attr('href'),
        type: 'post',
        push: false,
        replace: false,
        timeout: 5000
    });
});

$(document).ready(function () {
    var $userBankDropDown = $('ul#user-bank-dropdown');
    var $block = $('.dashboard-stat._yellow:first');

    if ($userBankDropDown.length > 0 && $block.length > 0) {
        $userBankDropDown.css('width', $block.width() + 'px');
    }
});

$(document).on('submit', '#statement-request-form', function (e) {
    $('button[type=submit]:not([data-loading])', this).each(function (i, b) {
        $(b).addClass('ladda-button ladda-right');
        Ladda.create(b).start();
    });
});

$('.employee-click_trigger').click(function () {
    $.post($(this).data('url'), null, function (data) {

    });
});

$(".trigger-menu_checkbox").change(function (e) {
    var $menuItems = null;
    var $checkedItems = [];
    var $value = $(this).prop('checked') + 0;

    $.post('/site/menu-item', {
        attribute: $(this).attr('id'),
        value: $value
    }, function (data) {
        if (data == true) {
            $(".trigger-menu_checkbox").each(function (e) {
                $menuItems = $($(this).data("menu_item"));
                if (!$(this).prop('checked')) {
                    $menuItems.addClass("hidden");
                } else {
                    $checkedItems.push($(this));
                }
            });
            $checkedItems.forEach(function (item, i, arr) {
                $menuItems = $(item.data("menu_item"));
                $menuItems.removeClass("hidden");
            });
        }
    });
});

$(".nav-item_accordeon .no-rules").click(function (e) {
    $("#menu-no-rules").modal();
});

$('table.flow-of-funds tbody td.can-hover:not(.blur)').mouseover(function () {
    $(this).addClass('hover');
}).mouseout(function () {
    $(this).removeClass('hover');
});

var $itemIDs = {};
var $coloredItemsCoords = [];
$(document).on('click', 'table.flow-of-funds tbody td.can-hover:not(.blur)', function () {
    /////////////////////////////////////////////////
    var $refreshUrl = '/analytics/finance/item-list';
    /////////////////////////////////////////////////
    var $tr = $(this).closest('tr');
    var $itemsTable = $('.items-table-block');
    var $serialNumber = $(this).index();
    var $nextTr = null;
    var $prevTd = null;
    var $prevTr = null;
    var $i = 0;
    var $text = 'Детализация';
    var $period = {};
    var $year = null;
    var $yearNumber = null;
    var byDays = $('#active-period-size').val() || 0;

    $coloredItemsCoords = [];

    if ($('#flowoffundsreportsearch-year').length > 0) {
        $yearNumber = $('#flowoffundsreportsearch-year').val();
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $yearNumber;
        $itemIDs['reportType'] = 'odds';
        $itemIDs['payment_type'] = $(this).closest('tr').data('payment_type');
    } else if ($('#oddssearch-year').length > 0) {
        $yearNumber = $('#oddssearch-year').val();
        $year = "OddsSearch%5Byear%5D=" + $yearNumber;
        $itemIDs['reportType'] = 'odds';
        $itemIDs['payment_type'] = $(this).closest('tr').data('payment_type');
        $itemIDs['wallet_id'] = $(this).closest('tr').data('wallet_id');

        switch ($('#oddssearch-tab').val()) {
            case '5': // По валютам
                $itemIDs['currency_payment_type'] = $itemIDs['payment_type'];
                $itemIDs['payment_type'] = '';
                break;
            case '4': // По точкам продаж
                $itemIDs['sale_point_id'] = $itemIDs['payment_type'];
                $itemIDs['payment_type'] = '';
                break;
            case '3': // По направлениям
                $itemIDs['industry_id'] = $itemIDs['payment_type'];
                $itemIDs['payment_type'] = '';
                break;
            default:
                break;
        }
        // detailing pages
        const pageFilter = $("#pageFilter");
        if ($(pageFilter).length) {
            if (String($(pageFilter).data('industry')).length) {
                $itemIDs['industry_id'] = $(pageFilter).data('industry');
            }
            if (String($(pageFilter).data('sale_point')).length) {
                $itemIDs['sale_point_id'] = $(pageFilter).data('sale_point');
            }
            if (String($(pageFilter).data('project')).length) {
                $itemIDs['project_id'] = $(pageFilter).data('project');
            }
        }

        // projects page
        // todo: need refactoring
        if ($('#odds-project-id').length) {
            $refreshUrl = '/project/odds-item-list';
            $itemIDs['project_id'] = $('#odds-project-id').val();
        }

    } else if ($('#paymentcalendarsearch-year').length > 0) {
        $yearNumber = $('#paymentcalendarsearch-year').val();
        $year = "PaymentCalendarSearch%5Byear%5D=" + $yearNumber;
        $itemIDs['reportType'] = 'paymentCalendar';
        $itemIDs['payment_type'] = $(this).closest('tr').data('payment_type');
        $itemIDs['wallet_id'] = $(this).closest('tr').data('wallet_id');

        switch ($('#paymentcalendarsearch-tab').val()) {
            case '15': // По точкам продаж
                $itemIDs['sale_point_id'] = $itemIDs['payment_type'];
                $itemIDs['payment_type'] = '';
                break;
            case '14': // По направлениям
                $itemIDs['industry_id'] = $itemIDs['payment_type'];
                $itemIDs['payment_type'] = '';
                break;
            default:
                break;
        }

    } else if ($('#expensessearch-year').length > 0) {
        $yearNumber = $('#expensessearch-year').val();
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $yearNumber;
        $itemIDs['reportType'] = 'expenses';
        $itemIDs['payment_type'] = $(this).closest('tr').data('payment_type');
    } else if ($('#incomesearch-year').length > 0) {
        $yearNumber = $('#incomesearch-year').val();
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $yearNumber;
        $itemIDs['reportType'] = 'income';
        $itemIDs['payment_type'] = $(this).closest('tr').data('payment_type');
    }

    // currency diff row
    if ($tr.data('hover_text')) {
        $refreshUrl = null;
        $('#odds-items_pjax').html('<div class="wrap">' + $tr.data('hover_text') + '</div>');
    }

    $('table.flow-of-funds tbody td.can-hover.hover-checked').removeClass('hover-checked');
    $(this).addClass('hover-checked');
    $coloredItemsCoords.push({tr: $(this).closest('tr').index(), td: $(this).index()});

    if ($(this).hasClass('quarter-block')) {
        $prevTd = $(this).prev('td');
        do {
            if ($prevTd.hasClass('can-hover')) {
                $prevTd.addClass('hover-checked');
                $coloredItemsCoords.push({tr: $($prevTd).closest('tr').index(), td: $($prevTd).index()});
            }
            $prevTd = $prevTd.prev('td');
            $i++
        }
        while ($i < 3);
    }
    if ($tr.hasClass('main-block')) {
        $text += ' «' + $tr.find('td:first').text().trim() + '»';
        $nextTr = $tr.next('tr');
        do {
            $prevTd = $nextTr.find('td:eq(' + $serialNumber + ')');
            if ($prevTd.hasClass('can-hover')) {
                $prevTd.addClass('hover-checked');
                $coloredItemsCoords.push({tr: $($prevTd).closest('tr').index(), td: $($prevTd).index()});
            }
            $nextTr = $nextTr.next('tr');
            if ($prevTd.hasClass('quarter-block')) {
                $prevTd = $prevTd.prev('td');
                $i = 0;
                do {
                    if ($prevTd.hasClass('can-hover')) {
                        $prevTd.addClass('hover-checked');
                        $coloredItemsCoords.push({tr: $($prevTd).closest('tr').index(), td: $($prevTd).index()});
                    }
                    $prevTd = $prevTd.prev('td');
                    $i++
                }
                while ($i < 3);
            }
        }
        while ($nextTr.hasClass('sub-block') || $nextTr.hasClass('item-block'));
    } else if ($tr.hasClass('sub-block')) {
        $text += ' «' + $tr.find('td:first').text().trim() + '»';
        $prevTr = $tr;
        do {
            $prevTr = $prevTr.prev('tr');
        }
        while (!$prevTr.hasClass('main-block'));
        $text += ' по «' + $prevTr.find('td:first').text().trim() + '»';
        $nextTr = $tr.next('tr');
        do {
            if ($nextTr.find('td:eq(' + $serialNumber + ')').hasClass('can-hover')) {
                $nextTr.find('td:eq(' + $serialNumber + ')').addClass('hover-checked');
                $coloredItemsCoords.push({tr: $($nextTr).closest('tr').index(), td: $($nextTr.find('td:eq(' + $serialNumber + ')')).index()});
            }
            $nextTr = $nextTr.next('tr');
        }
        while ($nextTr.hasClass('item-block'));
    } else {
        if ($('select#paymentcalendarsearch-year').length > 0 || $('select#flowoffundsreportsearch-year').length > 0 || $('select#oddssearch-year').length > 0) {
            $prevTr = $tr;
            do {
                $prevTr = $prevTr.prev('tr');
            }
            while (!$prevTr.hasClass('main-block'));
            $text += ' по «' + $prevTr.find('td:first').text().trim() + '» по';
        }
        $text += ' «' + $tr.find('td:first').text().trim() + '»';
    }

    // data
    if ($(this).hasClass('quarter-block')) {
        $text += ' за ' + $('.flow-of-funds .quarters-flow-of-funds th:eq(' + Math.ceil(($serialNumber - 1) / 4) + ')').text().trim() + 'г.';
        var $quarter = $('.flow-of-funds .quarters-flow-of-funds th:eq(' + Math.ceil(($serialNumber - 1) / 4) + ')').data('quarter');
        var $start = (+$quarter * 3) - 2;
        var $end = +$quarter * 3;
        $start = $start < 10 ? ('0' + $start) : $start;
        $end = $end < 10 ? ('0' + $end) : $end;
        $period = {
            start: $start,
            end: $end
        };
    } else if ($(this).hasClass('total-block')) {
        $text += ' за ' + $yearNumber + 'г.';
        $period = {
            start: '01',
            end: '12'
        };
    } else if ($(this).hasClass('day-block')) {
        var _month;
        var _day;
        var _ruMonths = ['', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

        if ($('.flow-of-funds thead tr:first-child th:first-child').attr('rowspan') > 1) {
            _month = $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber - 1) + ')').data('month');
            _day = $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber - 1) + ')').data('day');
        } else {
            _month = $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber) + ')').data('month');
            _day = $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber) + ')').data('day');
        }

        console.log(_month, _day, _ruMonths)

        $text += ' за ' + _day + ' ' + _ruMonths[parseInt(_month)] + ' ' + $yearNumber + 'г.';
        $period = {
            start: _month,
            end: _month,
            day: _day,
        };
    } else {

        var block = $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber - 1) + ')');
        var monthName = $(block).data('month-name') || $(block).text();

        $text += ' за ' + monthName.trim().toLowerCase() + ' ' +
            $yearNumber + 'г.';
        $period = {
            start: $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber - 1) + ')').data('month'),
            end: $('.flow-of-funds tr:not(.quarters-flow-of-funds) th:eq(' + ($serialNumber - 1) + ')').data('month')
        };
    }
    $itemsTable.find('.caption').text($text);
    var $income = {};
    var $expense = {};
    $('table.flow-of-funds td.can-hover.hover-checked').each(function (e) {
        $tr = $(this).closest('tr');
        if ($tr.hasClass('item-block')) {
            if ($tr.hasClass('income')) {
                $income[$tr.data('item_id')] = $tr.data('item_id');
            } else {
                $expense[$tr.data('item_id')] = $tr.data('item_id');
            }
        }
    });
    if ($(this).closest('table').hasClass('by_purse')) {
        $itemIDs['flow_type'] = $(this).closest('tr').data('flow_type');
    }
    $itemIDs['activeTab'] = $('#active-tab_report').val();
    $itemIDs['income'] = $income;
    $itemIDs['expense'] = $expense;
    $itemIDs['period'] = $period;
    $('.items-table-block .filter').removeClass('hidden');

    if ($refreshUrl) {
        $('#hellopreloader').show();
        $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
        jQuery.pjax({
            url: $refreshUrl + '?' + $year,
            type: 'POST',
            data: {'items': JSON.stringify($itemIDs)},
            container: '#odds-items_pjax',
            timeout: 5000,
            push: false,
            scrollTo: false
        });
    }

    $('.items-table-block').removeClass('hidden');
    $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");
});
$(document).on('click', 'table.flow-of-funds-double tbody td.can-hover:not(.blur)', function () {
    /////////////////////////////////////////////////
    let refreshUrl = '/analytics/finance/item-list';
    /////////////////////////////////////////////////

    const TABLE = $('table.flow-of-funds-double tbody');
    const ITEMS_TABLE = $('.items-table-block');
    const TD = $(this);
    const TR = $(this).closest('tr');
    const TH = $('.flow-of-funds-double .th-flow-of-funds-double th:eq(' + (TD.index() - 1) + ')');
    let   YEAR;
    let   title = 'Детализация ';
    let   selectionLeft, selectionRight, selectionTop, selectionBottom = 0;

    $itemIDs.activeTab = $('#active-tab_report').val();
    $itemIDs.payment_type = TR.data('payment_type');
    $itemIDs.income = {};
    $itemIDs.expense = {};

    if ($('#expensessearch-year').length > 0) {
        YEAR = $('#expensessearch-year').val();
        $itemIDs.reportType = 'expenses';
        refreshUrl += "?FlowOfFundsReportSearch[year]=" + YEAR;
    } else if ($('#incomesearch-year').length > 0) {
        YEAR = $('#incomesearch-year').val();
        $itemIDs.reportType = 'income';
        refreshUrl += "?FlowOfFundsReportSearch[year]=" + YEAR;
    }

    title += ' «' + TR.find('td:first').text().trim() + '»';
    if (TR.hasClass('sub-block')) {
        title += ' по «' + TR.prev('tr').find('td:first').text().trim() + '»';
    }

    // Колонки Квартала
    if (TD.hasClass('quarter-block')) {

        let prevTh = TH;
        let nextTh = TH;

        do {
            prevTh = prevTh.prev('th');
        }
        while (prevTh.data('quarter') === TH.data('quarter') || Math.ceil(prevTh.data('month') / 3) === Math.ceil(TH.data('quarter')));
        do {
            nextTh = nextTh.next('th');
        }
        while (nextTh.data('quarter') === TH.data('quarter'));

        selectionLeft = prevTh.index() + 1;
        selectionRight = nextTh.index() - 1;
        selectionTop = TR.index();
        selectionBottom = TR.index();

        $itemIDs.period = {
            start: String(Math.ceil(TH.data('quarter') * 3) - 3 + 1).padStart(2, '0'),
            end: String(Math.ceil(TH.data('quarter') * 3)).padStart(2, '0'),
        };

        title += ' за ' + TH.data('quarter') + ' кв ' + YEAR + 'г.';

        console.log(selectionLeft, selectionRight, selectionTop, selectionBottom);
    }

    // Ячейки Года
    else if (TD.hasClass('total-block')) {

        let prevTh = TH;

        do {
            prevTh = prevTh.prev('th');
        }
        while (prevTh.data('year') === TH.data('year'));

        selectionLeft = prevTh.index() + 1;
        selectionRight = TH.closest('tr').find('th').length - 1;
        selectionTop = TR.index();
        selectionBottom = TR.index();

        $itemIDs.period = {
            start: "01",
            end: "12"
        };

        title += ' за ' + YEAR + 'г.';

        console.log(selectionLeft, selectionRight, selectionTop, selectionBottom);
    }

    // Ячейки Месяца
    else {

        let prevTh = TH;
        let nextTh = TH;

        do {
            prevTh = prevTh.prev('th');
        }
        while (prevTh.data('month') === TH.data('month'));
        do {
            nextTh = nextTh.next('th');
        }
        while (nextTh.data('month') === TH.data('month'));

        selectionLeft = prevTh.index() + 1;
        selectionRight = nextTh.index() - 1;
        selectionTop = TR.index();
        selectionBottom = TR.index();

        $itemIDs.period = {
            start: TH.data('month'),
            end: TH.data('month')
        };

        title += ' за ' + TH.data('month-name') + ' ' + YEAR + 'г.';

        console.log(selectionLeft, selectionRight, selectionTop, selectionBottom);
    }

    // Строки Раздела
    if (TR.hasClass('sub-block') || TR.hasClass('main-block')) {

        let nextTr = TR;

        do {
            nextTr = nextTr.next('tr');
        }
        while (nextTr.hasClass('item-block'));

        selectionBottom = nextTr.index() - 1;

        console.log(selectionLeft, selectionRight, selectionTop, selectionBottom);
    }

    // Закращиваем ячейки
    TABLE.find('td.hover-checked').removeClass('hover-checked');

    let markTr = TR;
    for (let y = selectionTop; y <= selectionBottom; y++) {

        let markTd = markTr.find('td').eq(selectionLeft + 1);

        for (let x = selectionLeft; x <= selectionRight; x++) {
            markTd.addClass('hover-checked');
            markTd = markTd.next('td');
        }

        if (markTr.data('item_id')) {
            if (markTr.hasClass('income')) {
                $itemIDs.income[markTr.data('item_id')] = markTr.data('item_id');
            } else {
                $itemIDs.expense[markTr.data('item_id')] = markTr.data('item_id');
            }
        }

        markTr = markTr.next('tr');
    }

    $('#hellopreloader').show();
    $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);

    $.pjax({
        url: refreshUrl,
        type: 'POST',
        data: {'items': JSON.stringify($itemIDs)},
        container: '#odds-items_pjax',
        timeout: 5000,
        push: false,
        scrollTo: false
    });

    ITEMS_TABLE.removeClass('hidden').find('.caption').text(title);
    $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");

    return false;
});

$('.items-table-block a.close-odds').click(function (e) {
    $('table.flow-of-funds tbody td.can-hover.hover-checked').removeClass('hover-checked');
    if ($('.items-table-block').find('table.plan-item-table').length > 0) {
        $('.items-table-block').find('.portlet-title .caption').text('Реестр плановых операций');
        delete $itemIDs.income;
        delete $itemIDs.expense;
        delete $itemIDs.period;
        delete $itemIDs.group;
        $('#hellopreloader').show();
        $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
        $('.items-table-block .filter').addClass('hidden');
        jQuery.pjax({
            url: '/analytics/finance/item-list?PaymentCalendarSearch%5Byear%5D=' + $('select#paymentcalendarsearch-year').val(),
            type: 'POST',
            data: {'items': JSON.stringify($itemIDs)},
            container: '#odds-items_pjax',
            timeout: 5000,
            push: false,
            scrollTo: false
        });
    } else {
        $('.items-table-block').addClass('hidden');
    }
});

$(document).on('click', '#odds-items_pjax a:not(.update-item-payment-calendar, .delete-item-payment-calendar, .update-flow-item, .delete-flow-item)', function (e) {
    e.preventDefault();
    if (!$(this).find('span').hasClass('cash-bank-need-clarify')) {
        $itemIDs['activeTab'] = $('#active-tab_report').val();
        jQuery.pjax({
            url: $(this).attr('href'),
            type: 'POST',
            data: {'items': JSON.stringify($itemIDs)},
            container: '#odds-items_pjax',
            timeout: 5000,
            push: false,
            scrollTo: false
        });
    }
});
$('#odds-items_pjax').on('pjax:beforeSend', function (event, xhr, settings) {
    if (settings.data == undefined) {
        $itemIDs['activeTab'] = $('#active-tab_report').val();
        jQuery.pjax({
            url: settings.url,
            type: 'POST',
            data: {'items': JSON.stringify($itemIDs)},
            container: '#odds-items_pjax',
            timeout: 5000,
            push: false,
            scrollTo: false
        });

        return false;
    }
});
$('#odds-items_pjax').on("pjax:complete", function () {
    var hellopreloader = document.getElementById("hellopreloader_preload");
    fadeOutPreloader(hellopreloader);
    $('div.actions.joint-operations').hide();
});
//$(document).on("hide.bs.modal", ".odds-model-movement, #ajax-modal-box", function (event) {
//    if ($('#odds-items_pjax').length) {
//        $(this).find(".modal-body").empty();
//        $(this).find("#js-modal_update_title").empty();
//        $('#hellopreloader').show();
//        $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
//        $itemIDs['activeTab'] = $('#active-tab_report').val();
//        jQuery.pjax({
//            url: '/analytics/finance/item-list?FlowOfFundsReportSearch%5Byear%5D=' + $('#flowoffundsreportsearch-year').val(),
//            type: 'POST',
//            data: {'items': JSON.stringify($itemIDs)},
//            container: '#odds-items_pjax',
//            timeout: 5000,
//            push: false,
//            scrollTo: false
//        });
//    }
//});
$(document).on('click', '.items-table-block .portlet-title .filter a:not(.active, .close-odds)', function (e) {
    $('.items-table-block .portlet-title .filter a.active').removeClass('active');
    $(this).addClass('active');
    $('#hellopreloader').show();
    $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
    $itemIDs['group'] = $(this).data('attr');
    $itemIDs['activeTab'] = $('#active-tab_report').val();
    var $year = null;
    if ($('select#flowoffundsreportsearch-year').length > 0) {
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $('select#flowoffundsreportsearch-year').val();
        $itemIDs['reportType'] = 'odds';
    } else if ($('select#paymentcalendarsearch-year').length > 0) {
        $year = "PaymentCalendarSearch%5Byear%5D=" + $('select#paymentcalendarsearch-year').val();
        $itemIDs['reportType'] = 'paymentCalendar';
    } else if ($('select#expensessearch-year').length > 0) {
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $('select#expensessearch-year').val();
        $itemIDs['reportType'] = 'expenses';
    }
    jQuery.pjax({
        url: '/analytics/finance/item-list?' + $year,
        type: 'POST',
        data: {'items': JSON.stringify($itemIDs)},
        container: '#odds-items_pjax',
        timeout: 5000,
        push: false,
        scrollTo: false
    });
});
$(document).on('click', '.items-table-block .portlet-title .filter a.active', function (e) {
    $(this).removeClass('active');
    $('#hellopreloader').show();
    $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
    delete $itemIDs.group;
    $itemIDs['activeTab'] = $('#active-tab_report').val();
    var $year = null;
    if ($('select#flowoffundsreportsearch-year').length > 0) {
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $('select#flowoffundsreportsearch-year').val();
        $itemIDs['reportType'] = 'odds';
    } else if ($('select#paymentcalendarsearch-year').length > 0) {
        $year = "PaymentCalendarSearch%5Byear%5D=" + $('select#paymentcalendarsearch-year').val();
        $itemIDs['reportType'] = 'paymentCalendar';
    } else if ($('select#expensessearch-year').length > 0) {
        $year = "FlowOfFundsReportSearch%5Byear%5D=" + $('select#expensessearch-year').val();
        $itemIDs['reportType'] = 'expenses';
    }
    jQuery.pjax({
        url: '/analytics/finance/item-list?' + $year,
        type: 'POST',
        data: {'items': JSON.stringify($itemIDs)},
        container: '#odds-items_pjax',
        timeout: 5000,
        push: false,
        scrollTo: false
    });
});

$(document).on('change', 'select.cash_contractor_id_select', function (e) {
    let form = this.form;
    let $itemInput = $('select.cash_income_item_id_select:visible', form);
    if ($itemInput) {
        if ($(this).val() == 'balance') {
            if ($itemInput.val() != '15') {
                $itemInput.val('15').trigger('change');
            }
        } else if ($itemInput.val() == '15') {
            $itemInput.val('').trigger('change');
        }
    }
});

$(document).on('change', 'select.cash_income_item_id_select', function (e) {
    let form = this.form;
    let $contractorInput = $('select.cash_contractor_id_select:visible', form);
    if ($contractorInput) {
        if ($(this).val() == '15') {
            if ($contractorInput.val() !== 'balance') {
                $contractorInput.val('balance').trigger('change');
            }
        } else if ($contractorInput.val() == 'balance') {
            $contractorInput.val('').trigger('change');
        }
    }
});

// Scroll down
$(document).on("click", ".banking-modal-scroll-down", function (e) {
    e.preventDefault();
    $("#banking-module-modal").animate({scrollTop: $("#banking-module-modal .modal-dialog").height()}, 250);
});

$(document).on("click", ".banking_select_items_by_flow_type", function (e) {
    let form = $('#vidimus_form_ajax');
    $('input.select_item', form).prop('checked', false);
    $($(this).data('target'), form).prop('checked', true);
    $('input.select_item', form).uniform('refresh');
});

$(document).on("click", "td.link-view", function (e) {
    const link = $(this).closest("tr").find("td.document_number a");
    if (link.length)
        location.href = link.attr('href');
});

function invoiceTemplateResult(data, container, max_name_length) {

    var kpp = $(data.element).attr('data-kpp');

    if (kpp) {
        if (data.text.length > max_name_length) {
            $(container).attr('title', kpp);
            $(container).addClass('opt-tooltip-4');
            $(container).attr('data-placement', 'right');
            $(container).parents('.select2-dropdown').css('overflow', 'visible');
            container.innerHTML = data.text;
        } else {
            container.innerHTML =
                '<div class="opt-contractor">' +
                '<div class="name">' + data.text + '</div>' +
                '<div class="kpp">' + kpp.substr(0, 8) + '</div>' +
                '</div>';
        }
    } else {
        container.innerHTML = data.text;
    }

    return container;
}

function invoiceContractorMatcher(params, data) {

    var term = params.term || "";
    var text = data.text || "";
    var element = data.element;

    if ($.trim(term) === "")
        return data;

    if (text.toUpperCase().indexOf(term.toUpperCase()) > -1)
        return data;

    if ($(element).attr('data-inn') && $(element).attr('data-inn').indexOf(term) > -1)
        return data;

    return null;
}

$(document).on("select2:open",
    "select#invoice-contractor_id, " +  // invoices
    "select#customer_contractor_id, " + // bank
    "select#seller_contractor_id, " +   // bank
    "select#cashorderflows-contractorinput, " + // orders
    "select#cashemoneyflows-contractor_id-, " + // e-money
    "select#packinglist-consignee_id, " + // packing-list
    "select#upd-consignor_id, " + // upd
    "select#upd-consignee_id", // upd
    function() {

        function tultip() {
            $(".opt-tooltip-4").tooltipster({
                theme: ["tooltipster-kub"],
                trigger: "hover",
                side: "right",
            });
        }
        setTimeout(tultip, 300);
});
// m3956
$(document).ready(function(){
    if($('.trg-add-client').length) {
	    $('.trg-add-client').click(function(e){
	        e.preventDefault();
	        //
		    $.get('/crm/clients/add-modal-client',
			    function (data) {
				    if (data) {
				        $('#add-new-crm').modal();
					    if (data.header !== undefined) {
						    $('#add-new-crm .modal-header').html(data.header);
						    $('#block-modal-new-product-form').html(data.body);
						    if($('[id="crmclient-itn"]').length) {
						        console.info('setting up suggestions...',
							    $('[id="crmclient-itn"]').suggestions({
								    serviceUrl: 'https://dadata.ru/api/v2',
								    token: '78497656dfc90c2b00308d616feb9df60c503f51',
								    type: 'PARTY',
								    count: 5,

								    onSelect: function (suggestion) {
									    var companyType = {
										    'ИП': 1,
										    'ООО': 2,
										    'ЗАО': 3,
										    'ПАО': 4,
										    'АО': 5,
										    'АНО': 6,
										    'ФГУП': 7,
										    'ОАО': $('#crmclient-itn').data('prod') == 1 ? 10 : 8
									    };

									    var modalContainer = $('#block-modal-new-product-form').parents('.modal');

									    console.log(suggestion.data);
									    var companyTypeId = '-1';
									    if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
										    companyTypeId = companyType[suggestion.data.opf.short];
									    }
									    $('#crmclient-lead_name', modalContainer).val(suggestion.data.name.full);
									    $('#crmclient-itn', modalContainer).val(suggestion.data.inn);
									    //$('#crmclient-account', modalContainer).val(suggestion.data.kpp);
									    //$('#crmclient-bik', modalContainer).val(suggestion.data.ogrn);
									    $('#crmclient-legal_address', modalContainer).val(suggestion.data.address.value);
									    //$('#crmclient-actual_address', modalContainer).val(suggestion.data.address.value);
									    if (!empty(suggestion.data.management)) {
										    $('#crmclient-director_personal_name', modalContainer).val(suggestion.data.management.name);
										    $('#crmclient-director_role', modalContainer).val(suggestion.data.management.post);
									    }
									    $('#crmclient-company_type_id', modalContainer).val(companyTypeId);
									    if ($('#crmclient-company_type_id', modalContainer).val() == companyType['ИП']) {
										    $('#crmclient-director_personal_name', modalContainer).val(suggestion.data.name.full);
										    //$('.field-contractor-ppc', modalContainer).hide();
									    }
									    /*else {
									        $('.field-contractor-ppc', modalContainer).show();
                                        }*/
								    }
							    }));
						    }
						    $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
						    $("#contractor-face_type").on("change", "input", function () {
							    if (!$('.page-content.payment-order').length) {
								    $('.error-summary').remove();
							    }
							    $('.forContractor').toggleClass('selectedDopColumns');
							    //$('.date-picker').datepicker({
								//    keyboardNavigation: false,
								//    forceParse: false,
								//    language: 'ru',
								//    autoclose: true
							    //}).on("change.dp", dateChanged);

							    function dateChanged(ev) {
								    if (ev.bubbles == undefined) {
									    var $input = $("[name='" + ev.currentTarget.name + "']");
									    if (ev.currentTarget.value == "") {
										    if ($input.data("last-value") == null) {
											    $input.data("last-value", ev.currentTarget.defaultValue);
										    }
										    var $lastDate = $input.data("last-value");
										    console.log($lastDate);
										    $input.datepicker("setDate", $lastDate);
									    } else {
										    $input.data("last-value", ev.currentTarget.value);
									    }
								    }
							    }
						    });
						    $('a.btn').click(function () {
							    $('#add-new-crm').modal('hide');
						    });
					    }/* else if (data.hasOwnProperty('is_payment_order_page')) {
						    $('#add-new').modal('hide');
						    // Contractor info
						    $('.contractor_inn_button').remove();
						    $('input[name="PaymentOrder[contractor_name]"]').val(data.contractor_name);
						    $('input[name="PaymentOrder[contractor_inn]"]').val(data.contractor_inn);
						    $('input[name="PaymentOrder[contractor_kpp]"]').val(data.contractor_kpp);
						    // Contractor bank info
						    if (data.contractor_bik && data.contractor_bank_name && data.contractor_current_account) {
							    $('.contractor_bank_button').remove();
							    $('input[name="PaymentOrder[contractor_bank_name]"]').val(data.contractor_bank_name);
							    $('input[name="PaymentOrder[contractor_bik]"]').val(data.contractor_bik).show();
							    $('input[name="PaymentOrder[contractor_current_account]"]').val(data.contractor_current_account);
							    $('input[name="PaymentOrder[contractor_corresponding_account]"]').val(data.contractor_corresponding_account);
						    }

						    $('#payment_order_new_contractor_id').val(data.id);

					    } else {
						    $('#add-new').modal('hide');

						    if (data['update-contractor']) {
							    $('#contractor_update_button').val(data['name-short'] + ' ' + data['name']);
						    } else if (data['name-short'].length > 0) {
							    $contractorInput.append('<option value="' + data.id + '">' + data['name-short'] + ' ' + data.name + '</option>');
						    } else {
							    $contractorInput.append('<option value="' + data.id + '">' + data.name + '</option>');
						    }
						    $("#add-first-contractor").hide();
						    $("#select-existing-contractor").show();
						    $contractorInput.val(data.id).trigger("change");
					    }*/
					    //INVOICE.recalculateInvoiceReady();
				    }
			    }
		    );
        });
	    $(document).on('submit', '#new-client-crm-form', function (event) {
		    event.preventDefault();
		    //var url = 'add-modal-client';
		    var form = $('#new-client-crm-form').serialize();
		    //INVOICE.addNewContractor(url, form);
		    $.post('/crm/clients/add-modal-client',form,function(data){
		        if(data){
			        $('#add-new-crm').modal();
			        if (data.header !== undefined) {
				        $('#add-new-crm .modal-header').html(data.header);
				        $('#block-modal-new-product-form').html(data.body);
				        $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
				        $("#contractor-face_type").on("change", "input", function () {
					        if (!$('.page-content.payment-order').length) {
						        $('.error-summary').remove();
					        }
					        $('.forContractor').toggleClass('selectedDopColumns');
					        //$('.date-picker').datepicker({
						    //    keyboardNavigation: false,
						    //    forceParse: false,
						    //    language: 'ru',
						    //    autoclose: true
					        //}).on("change.dp", dateChanged);

					        function dateChanged(ev) {
						        if (ev.bubbles == undefined) {
							        var $input = $("[name='" + ev.currentTarget.name + "']");
							        if (ev.currentTarget.value == "") {
								        if ($input.data("last-value") == null) {
									        $input.data("last-value", ev.currentTarget.defaultValue);
								        }
								        var $lastDate = $input.data("last-value");
								        console.log($lastDate);
								        $input.datepicker("setDate", $lastDate);
							        } else {
								        $input.data("last-value", ev.currentTarget.value);
							        }
						        }
					        }
				        });
				        $('a.btn').click(function () {
					        $('#add-new-crm').modal('hide');
				        });
			        } else {
				        $('#add-new-crm').modal('hide');
			        }
                }
            });
	    });
    }
});
//

let isInitCollapse = false;

$(document).on("show.bs.collapse", "#autoload-form-container", function(e) {
    if (isInitCollapse === false) {
        isInitCollapse = true;
        $("#statement-history").removeClass("in");
    } else {
        $("#statement-history").collapse("show");
    }
});

$(document).on("hide.bs.collapse", "#autoload-form-container", function(e) {
    $("#statement-history").collapse("hide");
});

$(document).on("click", ".save-autoload", function(e) {
    e.preventDefault();
    $.post($(this).data('url'), $("#autoload-mode-selector input:checked").serialize(), function(data) {
        $("#autoload_save_report").show();
        setTimeout(function() {
            $("#autoload_save_report").hide();
        }, 3000);
    });
});

$(document).on("submit", "#upload-yandex-direct-form, #upload-google-words-form, #upload-vk-ads-form", function(e) {
    isInitCollapse = false;
});

$(document).on("hide.bs.modal", "#upload-yandex-direct-modal, #upload-google-words-modal, #upload-vk-ads-modal", function(e) {
    if (!$(e.target).hasClass('date-picker')) {
        isInitCollapse = false;
    }
});

$(document).on("click", ".upload-yandex-direct", function(e) {
    e.preventDefault();
    $.pjax({
        url: $(this).data("url"),
        container: "#upload-yandex-direct-pjax",
        push: false
    });
    $(document).on("pjax:success", "#upload-yandex-direct-pjax", function() {
        $("#upload-yandex-direct-modal").modal();
    });
});

$(document).on("click", ".upload-google-words", function(e) {
    e.preventDefault();
    $.pjax({
        url: $(this).data("url"),
        container: "#upload-google-words-pjax",
        push: false
    });
    $(document).on("pjax:success", "#upload-google-words-pjax", function() {
        $("#upload-google-words-modal").modal();
        if ($("#getoperationsform-datefrom").length > 0) {
            $('.disconnect-google-words').show();
        }
    });
});

$(document).on("click", ".upload-vk-ads", function(e) {
    e.preventDefault();
    $.pjax({
        url: $(this).data("url"),
        container: "#upload-vk-ads-pjax",
        push: false
    });
    $(document).on("pjax:success", "#upload-vk-ads-pjax", function() {
        $("#upload-vk-ads-modal").modal();
    });
});

$(document).on("click", ".connect-google-analytics", function(e) {
    e.preventDefault();
    $.pjax({
        url: $(this).data("url"),
        container: "#google-analytics-connect-pjax",
        push: false
    });
    $(document).on("pjax:success", "#google-analytics-connect-pjax", function() {
        $("#google-analytics-connect-modal").modal();
    });
});

$(document).on('click', '.togle-project-columns-button', function (e) {
    let btn = $(this);
    let attr = btn.data('attribute');
    let value = parseInt(btn.data('value'));
    let formData = {Config:{}};
    formData["Config"][attr] = value;
    $.post("/site/config", formData, function(data) {
        console.log(data[attr]);
        if (typeof data[attr] !== "undefined" && data[attr] == value) {
            $(btn.data('true-target')).toggleClass(btn.data('toggle-class'), true);
            $(btn.data('false-target')).toggleClass(btn.data('toggle-class'), false);
        }
    })
});