$.pjax.defaults.timeout = 20000;

$(document).on('keypress', 'input.date-picker', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        var v = $(this).val().split('.');
        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            var picker = $(this).data('datepicker');
            if (picker) {
                var curDate = picker.selectedDates[0];
                if (!curDate || curDate.getTime() != date.getTime()) {
                    picker.selectDate(date);
                }
            }
        }
    }
});

// MXFI functions
function empty(mixed_var) {
    return (
        mixed_var === '' ||
        mixed_var === 0 ||
        mixed_var === '0' ||
        mixed_var === null ||
        mixed_var === false ||
        mixed_var === 'false' ||
        mixed_var === undefined
    );
}

function initDatepicker() {

    $('.date-picker:not(.hasDatepicker)').each(function(i, dp) {

        if ($(dp).hasClass('hasDatepicker'))
            return;

        var v = $(dp).val().split('.');

        $(dp).addClass('hasDatepicker').attr('autocomplete', 'off').inputmask({"mask": "9{2}.9{2}.9{4}"});

        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            if (date.getTime() === date.getTime()) {
                $(dp).data('datepicker').selectDate(date);
            }
        }

    });
}
function initAutocompleteOff() {
    $(".dictionary-bik").attr('autocomplete', 'off');
}
function tooltipstering() {
    $('[title]:not(.tooltipstered)').filter(':not([data-toggle="tooltip"])').each(function() {
        $(this).tooltipster({
            theme: 'tooltipster-kub',
            maxWidth: 500,
            "contentAsHTML": !!$(this).attr('title-as-html'),
        });
    });
}

function refreshDatepicker(container) {
    $('.date-picker:not(.hasDatepicker)', container || document).each(function(i, dp) {
        var v = $(dp).val().split('.');

        $(dp).datepicker(kubDatepickerConfig);

        $(dp).addClass('hasDatepicker').attr('autocomplete', 'off').inputmask({"mask": "9{2}.9{2}.9{4}"});

        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            if (date.getTime() === date.getTime()) {
                $(dp).data('datepicker').selectDate(date);
            }
        }

    });
}

function refreshUniform(container) {

    if (typeof $.fn.uniform === 'undefined') {
        return;
    }

    $("input:checkbox:not(.md-check)", container || document).uniform('refresh');
    $("input:radio:not(.md-radio)", container || document).uniform('refresh');
}


$(document).ready(function() {
    tooltipstering();
    refreshDatepicker();
    initAutocompleteOff();
});

$(document).on('click', '.date-picker-icon', function(evt) {
    evt.stopPropagation();
    var $input = $(this).closest('.date-picker-wrap').find('input');
    if ($input.length && !$input.prop('disabled')) {
        $input.datepicker().data('datepicker').show();
    }
});

$.fn.serializeFormAsObject = function() {
    var data = {};

    function buildInputObject(arr, val) {
        if (arr.length < 1)
            return val;
        var objkey = arr[0];
        if (objkey.slice(-1) == "]") {
            objkey = objkey.slice(0,-1);
        }
        var result = {};
        if (arr.length == 1){
            result[objkey] = val;
        } else {
            arr.shift();
            var nestedVal = buildInputObject(arr,val);
            result[objkey] = nestedVal;
        }
        return result;
    }

    $.each(this.serializeArray(), function() {
        var val = this.value;
        var c = this.name.split("[");
        var a = buildInputObject(c, val);
        $.extend(true, data, a);
    });

    return data;
};

function createSimpleSelect2(id)
{
    if (!$("#" + id).length)
        return false;

    // Create Select2 in banking modal
    var select2_banking_modal = {
        "allowClear":false,
        "placeholder":"",
        "language":{},
        "theme":"krajee-bs4",
        "width":"100%",
        "minimumResultsForSearch":Infinity
    };

    var s2options_xxx_banking_modal = {
        "themeCss":".select2-container--krajee-bs4",
        "sizeCss":"",
        "doReset":true,
        "doToggle":false,
        "doOrder":false
    };

    if (jQuery("#" + id).data("select2")) { jQuery("#" + id).select2("destroy"); }
    jQuery.when(jQuery("#" + id).select2(select2_banking_modal)).done(initS2Loading(id,"s2options_xxx_banking_modal"));
}

window._offsetMCSLeft = 0;
$('.products-scroll-table').mCustomScrollbar({
    horizontalScroll: true,
    alwaysShowScrollbar: 0,
    axis:"x",
    scrollInertia: 300,
    advanced:{
        autoExpandHorizontalScroll: true,
        updateOnContentResize: true,
        updateOnImageLoad: false
    },
    mouseWheel:{ enable: false },
    callbacks: {
        whileScrolling:function(){
            window._offsetMCSLeft = this.mcs.left;
        }
    }
});

/**
 * Fix scrolling when a modal is opened
 */
window._bodyScrollTop;
$(document).on("show.bs.modal", function() {
    window._bodyScrollTop = window.pageYOffset || document.documentElement.scrollTop;
});
$(document).on("shown.bs.modal", function() {
    if (window._bodyScrollTop) {
        window.scrollTo({
            top: window._bodyScrollTop,
        });
    }
});
$(document).on("hidden.bs.modal", function() {
    if (window._bodyScrollTop) {
        window.scrollTo({
            top: window._bodyScrollTop,
        });
    }
});

/**
 * Refresh Uniform after replacing HTML content loaded from the server
 */
$(document).on('pjax:complete', function(e) {
    $(".dictionary-bik", e.target).attr('autocomplete', 'off');
    refreshDatepicker(e.target);
    refreshUniform(e.target);
    $('[data-toggle=dropdown]', e.target).dropdown();
    //$("input:checkbox:not(.md-check), input:radio:not(.md-radio)", e.target).uniform('refresh');
    //$('.date-picker', e.target).each(function(i, dp) {
    //    let v = $(dp).val().split('.');
    //    $(dp).datepicker(kubDatepickerConfig);
    //    $(dp).addClass('hasDatepicker').attr('autocomplete', 'off').inputmask({"mask": "9{2}.9{2}.9{4}"});
    //    if (v.length === 3) {
    //        let date = new Date(v[2], v[1] - 1, v[0]);
    //        if (date.getTime() === date.getTime()) {
    //            $(dp).data('datepicker').selectDate(date);
    //        }
    //    }
    //});
});
$(document).ajaxComplete(function() {
    tooltipstering();
});

/**
 * Prevent default redirection behavior
 */
$(document).on('pjax:timeout, pjax:error', function(event) {
    event.preventDefault();
    console.log(event.type);
});

$(document).on('click', '.form-errors-fixed > span', function () {
    $(this).parent().children('.alert').hide();
});
$(document).on('click', '[data-dismiss-single="modal"]', function(e) {
    e.preventDefault();
    $(this).closest(".modal").modal("hide");
});

$(document).on('click', 'a.document-many-send, .modal-many-delete, .modal-many-create-act, .modal-many-charge, ' +
    '.create-one-act, .create-one-upd, .create-one-invoice-facture, .modal-many-change-responsible, .modal-many-change-payment-delay,' +
    '.modal-many-change-status, .modal-many-change-article, .modal-not-need-document-submit, .modal-move-files-to, .modal-many-restore,' +
    'a.document-many-send-with-docs, .modal-many-change-payment-priority, .modal-many-change-project, .modal-many-change-cashbox,' +
    '.modal-many-change-sale-point, .modal-many-change-company-industry', function () {
    if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
    $('#ajax-loading').show();
    var $this = $(this);
    $('#many-send-error .form-body .row').remove();
    if (!$this.hasClass('clicked')) {
        if ($('.joint-operation-checkbox:checked').length > 0) {
            $this.addClass('clicked');
            $.post($this.data('url'), $('.joint-operation-checkbox, .modal-document-date, #contractor-responsible_employee,' +
                '#contractor-status, #contractor-payment_delay, #contractor-payment_priority, #contractor-article,' +
                '#operation-many-project, #operation-many-sale-point, .operation-many-cashbox-field, #operation-many-company-industry, ' +
                '#contractor-many-company-industry, #contractor-many-sale-point, #project-end-date').serialize(), function (data) {
                $('#ajax-loading').hide();
                if (data.notSend !== undefined) {
                    $('.joint-operation-checkbox:checked').click();
                    $('#many-send-error .form-body').append('<div class="row">' + data.notSend + '</div>');
                    $('#many-send-error').modal();
                    $this.removeClass('clicked');
                }
                if (data.message !== undefined) {

                    window.toastr.success(data.message, "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 1000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false,
                    });
                }
            });
        }
    }
});

$(document).on('tcw.change', '.mCustomScrollbar table', function (e) {
    $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
});

/**
 * If `dropdoun-menu` clipped by container with `overflow: hidden` add css class `clipped_by_container` to `dropdown` element
 */
$('.dropdown.clipped_by_container').on('show.bs.dropdown', function () {
    var dropdown = $(this);
    $('body').append(dropdown.css({
        'position':'absolute',
        'left':dropdown.offset().left,
        'top':dropdown.offset().top
    }).detach());
});
$('.dropdown.clipped_by_container').on('hidden.bs.dropdown', function () {
    var dropdown = $(this);
    var parentSelector = dropdown.data('parent');
    $(parentSelector).append(dropdown.css({
        'position':'static',
        'left':'auto',
        'top':'auto'
    }).detach());
});

$(document).on('click', '.toggle-password-input-type', function () {
    var wrapper = $(this).parent();
    var input = wrapper.children('input');
    var isVisible = input.attr('type') == 'text';
    wrapper.toggleClass('pass-visible', !isVisible);
    input.attr('type', isVisible ? 'password' : 'text');
});

$(document).on('change', '#registrationform-companytype input[type=radio]', function() {
    if ($('#registrationform-companytype input[type=radio][value=0]').is(':checked')) {
        $('.field-registrationform-taxationtypeosno input[type=checkbox]')
            .prop({checked: false, disabled: true})
            .uniform('refresh');
    } else {
        $('.field-registrationform-taxationtypeosno input[type=checkbox]')
            .prop({disabled: false})
            .uniform('refresh');
    }
});

// Plus Menu
var plusDropdownTimer;
$(window).on("load", function() {
    $(".dropdown_plus").hover(
        function() {
            clearInterval(plusDropdownTimer);
            var $this = $(".dropdown_plus");
            $this.addClass("show");
            $this.find(".dropdown-toggle").attr("aria-expanded", "true");
            $this.find(".dropdown-menu").addClass("show");
        },
        function() {
            plusDropdownTimer = setTimeout(
            'var $this = $(".dropdown_plus");' +
            '$this.removeClass("show");'+
            '$this.find(".dropdown-toggle").attr("aria-expanded", "false");'+
            '$this.find(".dropdown-menu").removeClass("show");', 500);
        }
    );
});

$(document).on('change', 'select[data-tax-kbk]', function () {
    var kbk = $(this).data('tax-kbk')[this.value];
    if (kbk) {
        $('#cashbankflowsform-kbk, #cashbankflows-kbk', this.form).val(kbk);
    }
});

// Forms
$(document).on('change', 'select[data-select2-id]', function (e) {
    let $el = $(this).parent().find('span.select2-selection__rendered.tooltipstered');
    if ($.tooltipster.instances($el).length) {
        $el.tooltipster('destroy');
    }
});
$(document).on('change', '.form-autosubmit', function () {
    $($(this).closest('form').submit());
});
$(document).on('change', '#cashorderflows-reason_id', function () {
    var target = $(this).data('related-target');
    if (target) {
        if (this.value == $(this).data('related-value')) {
            $(target).collapse('show');
        } else {
            $(target).collapse('hide');
        }
    }
});
// Collapse save state to userConfig
$('.jsSaveStateCollapse').on('shown.bs.collapse', function() {
    $.post("/site/change-table-view-mode", {
        'attribute': $(this).attr('data-attribute'),
        'invert-attribute': $(this).attr('data-invert-attribute'),
    }, function (data) {});
});
$('.jsSaveStateCollapse').on('hide.bs.collapse', function() {
    $.post("/site/change-table-view-mode", {
        'attribute': $(this).attr('data-attribute'),
        'invert-attribute': $(this).attr('data-invert-attribute'),
    }, function (data) {});
});

// ODDS

var OddsTable = {
  fillFixedColumn: function() {

      var tableBlock = $('#cs-table-1');
      var columnBlock = $('#cs-table-first-column');
      // CLEAR
      $(columnBlock).find('thead').html('');
      $(columnBlock).find('tbody').html('');
      $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());

      // CLONE FIRST COLUMN
      $('.odds-table thead tr > th:first-child').each(function(i,v) {
          var col = $(v).clone();
          $('.table-fixed-first-column thead').append($('<tr/>').append(col));

          return false;
      });
      $('.odds-table tbody tr > td:first-child').each(function(i,v) {
          var col = $(v).clone();
          var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
          var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
          $(col).find('.sortable-row-icon').html('');
          $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
      });

      // ADD "+" EVENTS
      $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
          var target = $(this).data('target');
          $('#cs-table-1').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
      });
      $('#cs-table-1 [data-collapse-row-trigger]').click(function() {
          var target = $(this).data('target');
          $('#cs-table-first-column').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
      });

      // ADD "+" EVENT TOGGLE ALL
      function tableFixedColumnToggleAll(syncTable, trigger) {
          var row = $(syncTable).find('tr[data-id]');
          console.log(syncTable, trigger)
          if ( $(trigger).hasClass('active') ) {
              row.removeClass('d-none');
          } else {
              row.addClass('d-none');
          }
      }
      $('#cs-table-first-column [data-collapse-all-trigger]').click(function(e) {
          var syncTable = $('#cs-table-1');
          var trigger = $(syncTable).find('[data-collapse-all-trigger]');
          $(trigger).toggleClass('active');
          tableFixedColumnToggleAll(syncTable, trigger);
      });
      $('#cs-table-1 [data-collapse-all-trigger]').click(function(e) {
          var syncTable = $('#cs-table-first-column');
          var trigger = $(syncTable).find('[data-collapse-all-trigger]');
          $(trigger).toggleClass('active');
          tableFixedColumnToggleAll(syncTable, trigger);
      });
  }
};

// from main.js
bindTablePlusEvents = function() {
    $('[data-collapse-trigger]').click(function () {
        var target = $(this).data('target');
        var collapseCount = $(this).data('columns-count') || 3;
        var uncollapseCount = $(this).data('columns-count-collapsed') || 1;
        $(this).toggleClass('active');
        $('[data-id="' + target + '"][data-collapse-cell]').toggleClass('d-none');
        $('[data-id="' + target + '"][data-collapse-cell-total]').toggleClass('d-none');
        if ($(this).hasClass('active')) {
            $('[data-id="' + target + '"][data-collapse-cell-title]').attr('colspan', collapseCount);
        } else {
            $('[data-id="' + target + '"][data-collapse-cell-title]').attr('colspan', uncollapseCount);
        }
        $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
        // console.log('lalalal');
    });
    $('[data-collapse-row-trigger]').click(function () {
        var target = $(this).data('target');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('[data-id="' + target + '"]').removeClass('d-none');
        } else {
            // level 1
            $('[data-id="' + target + '"]').addClass('d-none');
            $('[data-id="' + target + '"]').find('[data-collapse-row-trigger]').removeClass('active');
            $('[data-id="' + target + '"]').each(function (i, row) {
                // level 2
                $('[data-id="' + $(row).find('[data-collapse-row-trigger]').data('target') + '"]').addClass('d-none');
                $('[data-id="' + $(row).find('[data-collapse-row-trigger]').data('target') + '"]').find('[data-collapse-row-trigger]').removeClass('active');
                $('[data-id="' + $(row).find('[data-collapse-row-trigger]').data('target') + '"]').each(function (i, row) {
                    $('[data-id="' + $(row).find('[data-collapse-row-trigger]').data('target') + '"]').addClass('d-none');
                });
            });
        }
        if ($('[data-collapse-row-trigger].active').length <= 0) {
            $('[data-collapse-all-trigger]').removeClass('active');
        } else {
            $('[data-collapse-all-trigger]').addClass('active');
        }
    });
    $('[data-collapse-all-trigger]').click(function () {
        var _this = $(this);
        var table = $(this).closest('.table');
        var row = table.find('tr[data-id]');
        _this.toggleClass('active');
        if (_this.hasClass('active')) {
            row.removeClass('d-none');
            $(table).find('tbody .table-collapse-btn').addClass('active');
        } else {
            row.addClass('d-none')
            $(table).find('tbody .table-collapse-btn').removeClass('active');
        }
    });
    $("[data-collapse-trigger-days]").click(function() {
        var collapseBtn = this;
        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 250);
        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).data('columns-count') || 3;

            $(collapseBtn).toggleClass('active');
            $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            if ( $(collapseBtn).hasClass('active') ) {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
            } else {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
            }
            $(collapseBtn).closest('.custom-scroll-table').mCustomScrollbar("update");
        };
        _collapseToggle(collapseBtn);
    });
};

flowOfFundsTableSortable = function() {
    const $flowOfFundsTable = $('table.flow-of-funds tbody');
    $flowOfFundsTable.sortable({
        cancel: 'tr.not-drag',
        handle: ".sortable-row-icon",
        start: function (event, ui) {
            //ui.item.find('td').each(function () {
            //    $(this).css('border', '1px solid #ddd');
            //});
        },
        stop: function (event, ui) {
            var $item = ui.item;
            var $itemID = $item.data('item_id');
            var $prevItem = $item.prev();
            var $blockTypeID = null;
            var $type = 'expense';
            var $year = null;
            var $reportType = null;
            var $periodSize = $('#active-period-size').val() || 'months';

            if ($('select#oddssearch-year').length > 0) {
                $year = $('select#oddssearch-year').val();
                $reportType = 'odds';
            } else if ($('select#flowoffundsreportsearch-year').length > 0) {
                $year = $('select#flowoffundsreportsearch-year').val();
                $reportType = 'odds';
            } else if ($('select#paymentcalendarsearch-year').length > 0) {
                $reportType = 'paymentCalendar';
                $year = $('select#paymentcalendarsearch-year').val();
            }
            if ($prevItem.hasClass('cancel-drag') || $prevItem[0] == undefined) {
                $flowOfFundsTable.sortable('cancel');
            } else {
                if ($item.hasClass('income')) {
                    $type = 'income';
                }
                if (($prevItem.hasClass('income') && $type == 'expense') ||
                    ($prevItem.hasClass('expense') && $type == 'income')) {
                    $flowOfFundsTable.sortable('cancel');
                } else {
                    if ($prevItem.hasClass('expenditure_type')) {
                        $blockTypeID = $prevItem.attr('id');
                        //if (!$prevItem.find('.table-collapse-btn').hasClass('active')) {
                        //    $prevItem.find('.table-collapse-btn').click();
                        //}
                    } else {
                        $blockTypeID = $prevItem.data('type_id');
                        //if (!$prevItem.find('.table-collapse-btn').hasClass('active')) {
                        //    $prevItem.find('.table-collapse-btn').click();
                        //}
                    }
                    if ($blockTypeID == $item.attr('data-type_id')) {
                        $flowOfFundsTable.sortable('cancel');
                    } else {
                        $('#hellopreloader').show();
                        $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);

                        var debugCurrTime = Date.now();

                        const analyticsUrlPart = $('#isAnalyticsModule').length ? 'analytics' : 'reports';

                        let floorMap = {};
                        $('table.flow-of-funds').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-days]').each(function (i, v) {
                            floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
                        });

                        $.post('/' + analyticsUrlPart + '/finance/change-block', {
                            item_id: $itemID,
                            block_type_id: $blockTypeID,
                            type: $type,
                            year: $year,
                            reportType: $reportType,
                            periodSize: $periodSize,
                            floorMap: floorMap
                        }, function ($data) {
                            if ($data.result == true) {
                                $item.attr('data-id', 'first-floor-' + $blockTypeID);
                                $item.attr('data-type_id', $blockTypeID);
                                if ($prevItem.hasClass('d-none')
                                    || ($prevItem.find('.table-collapse-btn').length && !$prevItem.find('.table-collapse-btn').hasClass('active'))) {
                                    $item.addClass('d-none');
                                }

                                console.log('get data: ' + 1 / 1000 * (Date.now() - debugCurrTime));
                                debugCurrTime = Date.now();
                                var $html = $($.parseHTML($data.html));
                                console.log('parse html: ' + 1 / 1000 * (Date.now() - debugCurrTime));
                                debugCurrTime = Date.now();
                                var debugCurrTime2 = Date.now();

                                // JQUERY - 2,7s
                                // var $origTable = $('table.flow-of-funds tbody');
                                // $html.find('table.flow-of-funds tbody tr').each(function (i, tr) {
                                //     var $origTr = $origTable.find('tr').eq(i);
                                //     $(tr).find('td').each(function (j, td) {
                                //         if ($(td).hasClass('checkbox-td') || $(td).hasClass('tooltip-td')) {
                                //             return true;
                                //         }
                                //         var $origTD = $origTr.find('td').eq(j);
                                //         $origTD.html($(td).html());
                                //     });
                                //     console.log('insert tr: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                                // });

                                // JS - 0,37s
                                //let html = new DOMParser().parseFromString($data.html, "text/html");
                                //let trs = html.querySelectorAll('table.flow-of-funds tbody tr');
                                //let origTrs = document.querySelectorAll('table.flow-of-funds tbody tr');
                                //trs.forEach(function(trValue, trIndex, trsObj) {
                                //    let tds = trValue.querySelectorAll('td');
                                //    let origTds = origTrs[trIndex].querySelectorAll('td');
                                //    tds.forEach(function(tdValue, tdIndex, tdsObj) {
                                //        if (tdValue.classList.contains('checkbox-td') || tdValue.classList.contains('tooltip-td'))
                                //            return;
                                //        origTds[tdIndex].innerHTML = tdValue.innerHTML;
                                //    });
                                //    // console.log('insert tr: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                                //});

                                // JQUERY - change whole table - 0,024s
                                const html = new DOMParser().parseFromString($data.html, "text/html");
                                const table = $(html).find('table.flow-of-funds');
                                const origTable = $('table.flow-of-funds');
                                $(origTable).replaceWith(table);

                                console.log('insert in table: ' + 1 / 1000 * (Date.now() - debugCurrTime2));

                                // fixed first column
                                if ($('#cs-table-first-column').length) {
                                    OddsTable.fillFixedColumn();
                                    $("#cs-table-2").mCustomScrollbar("update");
                                }

                                flowOfFundsTableSortable();
                                bindTablePlusEvents();

                                //$html.find('table.flow-of-funds tbody td:not(.checkbox-td, .tooltip-td)').each(function () {
                                //    var $className = 'td.' + $(this).attr('class').trim().replace(/\s/ig, '.');
                                //   $($className).html($(this).html());
                                //});

                                var hellopreloader = document.getElementById("hellopreloader_preload");
                                fadeOutPreloader(hellopreloader);
                            } else {
                                $flowOfFundsTable.sortable('cancel');
                                var hellopreloader = document.getElementById("hellopreloader_preload");
                                fadeOutPreloader(hellopreloader);
                            }
                        });
                    }
                }
            }
        }
    }).disableSelection();
};

$(document).ready(function () {
    if ($('table.flow-of-funds').length > 0) {
        flowOfFundsTableSortable();
    }
});

$('.nstSlider').nstSlider({
    "left_grip_selector": ".leftGrip",
    "value_changed_callback": function(cause, leftValue, rightValue) {
        $(this).parent().find('.leftGrip-value').text(leftValue);

        if ($(this).hasClass('plan-fact')) {
            var tableItems = $("table.flow-of-funds tbody td.tooltip2-deviation-td");
            var $filterDeviationVal = +leftValue;
            tableItems.each(function (e) {
                let $deviation = Math.abs(parseFloat($(this).data("deviation")));
                if (!isNaN($deviation) && $deviation < $filterDeviationVal) {
                    $(this).addClass("filter-deviation");
                } else if (!isNaN($deviation) && $deviation > $filterDeviationVal) {
                    $(this).removeClass("filter-deviation");
                }
            });
        }
    }
});

$(document).ready(function () {
    sortableArticleList();
});

function sortableArticleList() {
    let articleList = $('ul.articles-list');

    if (articleList.length > 0) {

        if ($(articleList).hasClass('articles-list-sorting-disabled'))
            return;

        $('ul.subCategory, ul.articles-list').sortable({
            connectWith: $('ul.subCategory, ul.articles-list'),
            handle: ".sortable-row-icon",
            stop: function (event, ui) {
                let contractor = ui.item;
                let contractorId = +contractor.data('id');
                let contractorType = +contractor.data('type');
                let prevItem = contractor.prev();
                let nextItem = contractor.next();
                let parent = contractor.parent('.subCategory');
                let subList = null;
                let preloadModal = $('#preloader_modal');

                if (prevItem.hasClass('subCategory') && prevItem.data('type') !== undefined && prevItem.data('id') !== undefined) {
                    subList = prevItem;
                } else if (nextItem.hasClass('subCategory') && nextItem.data('type') !== undefined && nextItem.data('id') !== undefined) {
                    subList = nextItem;
                } else if (parent.data('type') !== undefined && parent.data('id') !== undefined) {
                    subList = parent;
                } else {
                    $('ul.subCategory').sortable('cancel');
                    return;
                }

                if (+subList.data('type') === contractorType) {
                    let preloadButton = preloadModal.find('.ladda-button');
                    let laddaButton = Ladda.create(preloadButton[0]);

                    preloadModal.find('.modal-title').text('Производим изменения в настройках. Это может занять около минуты');
                    preloadModal.modal();
                    preloadModal.on('shown.bs.modal', function(e) {
                        laddaButton.start();
                        preloadModal.off('shown.bs.modal');
                    });

                    $("#blockScreen").show();

                    $.post('/reference/articles/change-contractor', {
                        articleId: +subList.data('id'),
                        type: +subList.data('type'),
                        contractorId: contractorId,
                        filterType: $('ul.articles-list').data('type')
                    }, function(data) {
                        if (data.result === true) {
                            let html = $($.parseHTML(data.html)).find('ul.articles-list');
                            let openedCategories = [];
                            let newCategory = $('ul.articles-list .category.item-' + subList.data('id'));

                            if (!newCategory.hasClass('open')) {
                                $('ul.articles-list .category.item-' + subList.data('id')).click();
                            }

                            $('ul.articles-list .category').each(function(i, e) {
                                 if ($(this).hasClass('open')) {
                                     openedCategories.push($(this).attr("id"));
                                 }
                            });
                            $('ul.articles-list').replaceWith(html);

                            $('ul.articles-list .category').each(function(i, e) {
                                if ($.inArray($(this).attr("id"), openedCategories) !== -1) {
                                    $(e).click();
                                }
                            });

                            Ladda.stopAll();
                            $("#blockScreen").hide();

                            sortableArticleList();

                        } else {
                            $('ul.subCategory').sortable('cancel');

                            Ladda.stopAll();
                            $("#blockScreen").hide();
                        }
                    });
                } else {
                    $('ul.subCategory').sortable('cancel');
                }
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
window.PACKING_LIST = {
    $table: $('#packinglist_table'),
    //$addToOrderDocumentButton: $('#add-to-invoice-button'),
    init: function() {
        this.bindEvents();
        //this.surchargeInvoiceModal.bindEvents();
    },
    bindEvents: function() {
        $(document).on('change', 'input.quantity', function() {
            PACKING_LIST.recalculateTable();
        });
        $(document).on('change', 'input.price', function() {
            this.value = Number($(this).val().replace(',', '.').replace(/[^0-9\.]/g, '')).toFixed(2);
            $(this).closest('tr').find('input.quantity').attr('price', 100 * (this.value || 0));
            PACKING_LIST.recalculateTable();
        });
    },
    getItems: function () {
        return this.$table.find('tbody > tr.order');
    },
    getFloatPrice: function (price) {
        return (price / 100).toFixed(2);
    },
    recalculateTable: function () {
        const ndsViewId = parseInt($('.nds-view-type').val());
        let totalPrice = 0;
        let totalTax = 0;
        let totalWeight = 0;
        this.getItems().each(function () {
            let $this = $(this);
            let $input = $(this).find('input.quantity');
            let quantity = parseFloat($input.val() || '0');
            let basePrice = parseFloat($input.attr('data-price') || '0') / 100;
            let taxRate = parseFloat($input.attr('data-tax-rate') || '0');
            let weight =  parseFloat(1) * quantity;

            let productTotal = basePrice * quantity;
            let productNoNds = 0;

            if (ndsViewId === 2) {
                // no nds
                productNoNds = productTotal;
                totalTax = 0;
            } else if (ndsViewId === 1) {
                // nds out
                productNoNds = productTotal - (productTotal * taxRate / (1 + taxRate));
                totalTax += productTotal * taxRate / (1 + taxRate);
            } else {
                // nds in
                productNoNds = productTotal - (productTotal * taxRate / (1 + taxRate));
                totalTax += productTotal * taxRate / (1 + taxRate);
            }

            totalPrice += productNoNds;
            totalWeight += weight;
            $this.find('.product-amount').text(number_format(productTotal, 2, ',', ' '));
        });

        $('#total_price').text(number_format(totalPrice, 2, ',', ' '));
        $('#total_nds').text(number_format(totalTax, 2, ',', ' '));
        $('#total_amount').text(number_format(totalPrice + (ndsViewId !== 2 ? totalTax : 0), 2, ',', ' '));
        $('#total_weight').text(number_format(totalWeight, (totalWeight === parseInt(totalWeight)) ? 0 : 3, ',', ' '));

        //
        //PACKING_LIST.surchargeInvoiceModal.checkAvailableQuantity();
    },
    // addProductToTable: function (items)
    // {
    //     if (items) {
    //         const docType = parseInt($('.document-type').val());
    //         const viewType = parseInt($('.nds-view-type').val());
    //         const $template = PACKING_LIST.$table.find('.template').clone();
    //         $template.removeClass('template').addClass('order');
    //         $template.find('input').attr('disabled', false);
    //         let $item;
    //
    //         for (let i = 0; i < items.length; i++) {
    //             let item = items[i];
    //             let unitName = item.productUnit ? item.productUnit.name : '---';
    //             let priceForSellNds = Number(item.priceForSellNds.rate);
    //             let priceForBuyNds = Number(item.priceForBuyNds.rate);
    //             let priceForSell = (viewType === 1) ? Math.round(Number(item.price_for_sell_with_nds) / (priceForSellNds + 1)) : Number(item.price_for_sell_with_nds);
    //             let priceForBuy  = (viewType === 1) ? Math.round(Number(item.price_for_buy_with_nds)  / (priceForBuyNds + 1))  : Number(item.price_for_buy_with_nds);
    //             let price = (docType === 1) ? priceForBuy : priceForSell;
    //             let taxRate = (docType === 1) ? priceForBuyNds : priceForSellNds;
    //             let taxRateName = (docType === 1) ? item.priceForBuyNds.name : item.priceForSellNds.name;
    //
    //             $item = $template.clone();
    //             $item.find('.product-title').text(item.title);
    //             $item.find('.product-unit-name').text(unitName);
    //             if (unitName === '---') {
    //                 $item.find('.product-count').remove();
    //             } else {
    //                 $item.find('.product-no-count').remove();
    //             }
    //             if ($item.find('.product-tax_rate')) {
    //                 $item.find('.product-tax_rate').text(taxRateName);
    //             }
    //             $item.find('.product-price input.price')
    //                 .attr('name', 'surchargeProduct[' + item.id + '][price]')
    //                 .val(PACKING_LIST.getFloatPrice(parseInt(price)));
    //             $item.find('.product-amount').text(PACKING_LIST.getFloatPrice(parseInt(price)));
    //             $item.find('.product-quantity input.quantity')
    //                 .attr('name', 'surchargeProduct[' + item.id + '][quantity]')
    //                 .attr('price', price)
    //                 .attr('tax-rate', taxRate)
    //                 .val(1);
    //
    //             $item.find('input.selected-product-id').val(item.id);
    //
    //             $item.show().insertBefore('#from-new-add-row');
    //         }
    //
    //         PACKING_LIST.recalculateTable();
    //
    //         return true;
    //     }
    //
    //     return false;
    // },
    // addNewProduct: function (url, form) {
    //     $.post(
    //         url,
    //         form,
    //         function (data) {
    //             $('#add-new').modal();
    //             if (data) {
    //                 if (data.header !== undefined) {
    //                     $('#add-new .modal-title').html(data.header);
    //                     $('#block-modal-new-product-form').html(data.body);
    //                     $('#block-modal-new-product-form input').uniform('refresh');
    //                     $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
    //                     $("#contractor-face_type").on("change", "input", function () {
    //                         if (!$('.page-content.payment-order').length) {
    //                             $('.error-summary').remove();
    //                         }
    //                         $('.forContractor').toggleClass('selectedDopColumns');
    //                     });
    //                     $('a.btn').click(function () {
    //                         $('#add-new').modal('hide');
    //                     });
    //
    //                     $('.forProduct').addClass('selectedDopColumns');
    //                     $('.forService').removeClass('selectedDopColumns');
    //                     $('#product-goods-group_id').attr('disabled', true);
    //                     $('#product-service-group_id').removeAttr('disabled');
    //                     $('.field-product-product_unit_id').removeClass('required');
    //                     $('.import-products').hide();
    //                     $('.new_product_group_input').attr('placeholder', 'Добавить новую группу услуг');
    //
    //                 } else if (data.items !== undefined) {
    //
    //                     $('.order-add-static-items .service-count-value').text(data.services);
    //                     if (data.services > 0) {
    //                         $('li.select2-results__option.add-modal-services').removeClass('hidden');
    //                     }
    //                     //$('.order-add-static-items .product-count-value').text(data.goods);
    //                     //if (data.goods > 0) {
    //                     //    $('li.select2-results__option.add-modal-products').removeClass('hidden');
    //                     //}
    //
    //                     PACKING_LIST.addProductToTable(data.items);
    //
    //                     $('#from-new-add-row').hide();
    //
    //                     $('#add-new').modal('hide');
    //                 } else {
    //                     $('#add-new').modal('hide');
    //                 }
    //             }
    //         }
    //     );
    // },
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
window.ACT = {
    $table: $('#product-table-act'),
    //$addToOrderDocumentButton: $('#add-to-invoice-button'),
    init: function() {
        this.bindEvents();
        this.surchargeInvoiceModal.bindEvents();
    },
    bindEvents: function() {
        $(document).on('change', 'input.quantity', function() {
            ACT.recalculateTable();
        });
        $(document).on('change', 'input.price', function() {
            this.value = Number($(this).val().replace(',', '.').replace(/[^0-9\.]/g, '')).toFixed(2);
            $(this).closest('tr').find('input.quantity').attr('price', 100 * (this.value || 0));
            ACT.recalculateTable();
        });
    },
    getItems: function () {
        return this.$table.find('tbody > tr.order');
    },
    getFloatPrice: function (price) {
        return (price / 100).toFixed(2);
    },
    recalculateTable: function () {
        const ndsViewId = parseInt($('.nds-view-type').val());
        let totalPrice = 0;
        let totalTax = 0;
        this.getItems().each(function () {
            let $this = $(this);
            let $input = $(this).find('input.quantity');
            let quantity = parseFloat($input.val() || '0');
            let basePrice = parseFloat($input.attr('price') || '0') / 100;
            let taxRate = parseFloat($input.attr('tax-rate') || '0');

            let productTotal = basePrice * quantity;

            if (ndsViewId === 2) {
                totalTax += 0;
            } else if (ndsViewId === 1) {
                totalTax += productTotal * taxRate;
            } else {
                totalTax += productTotal * taxRate / (1 + taxRate);
            }

            totalPrice += productTotal;
            $this.find('.product-amount').text(number_format(productTotal, 2, ',', ' '));
        });

        $('#total_price').text(number_format(totalPrice, 2, ',', ' '));
        $('#total_nds').text(number_format(totalTax, 2, ',', ' '));
        $('#total_amount').text(number_format(totalPrice + (ndsViewId === 1 ? totalTax : 0), 2, ',', ' '));

        //
        ACT.surchargeInvoiceModal.checkAvailableQuantity();
    },
    addProductToTable: function (items)
    {
        if (items) {
            const docType = parseInt($('.document-type').val());
            const viewType = parseInt($('.nds-view-type').val());
            const $template = ACT.$table.find('.template').clone();
            $template.removeClass('template').addClass('order');
            $template.find('input').attr('disabled', false);
            let $item;

            for (let i = 0; i < items.length; i++) {
                let item = items[i];
                let unitName = item.productUnit ? item.productUnit.name : '---';
                let priceForSellNds = Number(item.priceForSellNds.rate);
                let priceForBuyNds = Number(item.priceForBuyNds.rate);
                let priceForSell = (viewType === 1) ? Math.round(Number(item.price_for_sell_with_nds) / (priceForSellNds + 1)) : Number(item.price_for_sell_with_nds);
                let priceForBuy  = (viewType === 1) ? Math.round(Number(item.price_for_buy_with_nds)  / (priceForBuyNds + 1))  : Number(item.price_for_buy_with_nds);
                let price = (docType === 1) ? priceForBuy : priceForSell;
                let taxRate = (docType === 1) ? priceForBuyNds : priceForSellNds;
                let taxRateName = (docType === 1) ? item.priceForBuyNds.name : item.priceForSellNds.name;

                $item = $template.clone();
                $item.find('.product-title').text(item.title);
                $item.find('.product-unit-name').text(unitName);
                if (unitName === '---') {
                    $item.find('.product-count').remove();
                } else {
                    $item.find('.product-no-count').remove();
                }
                if ($item.find('.product-tax_rate')) {
                    $item.find('.product-tax_rate').text(taxRateName);
                }
                $item.find('.product-price input.price')
                    .attr('name', 'surchargeProduct[' + item.id + '][price]')
                    .val(ACT.getFloatPrice(parseInt(price)));
                $item.find('.product-amount').text(ACT.getFloatPrice(parseInt(price)));
                $item.find('.product-quantity input.quantity')
                    .attr('name', 'surchargeProduct[' + item.id + '][quantity]')
                    .attr('price', price)
                    .attr('tax-rate', taxRate)
                    .val(1);

                $item.find('input.selected-product-id').val(item.id);

                $item.show().insertBefore('#from-new-add-row');
            }

            ACT.recalculateTable();

            return true;
        }

        return false;
    },
    addNewProduct: function (url, form) {
        $.post(
            url,
            form,
            function (data) {
                $('#add-new').modal();
                if (data) {
                    if (data.header !== undefined) {
                        $('#add-new .modal-title').html(data.header);
                        $('#block-modal-new-product-form').html(data.body);
                        $('#block-modal-new-product-form input').uniform('refresh');
                        $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                        $("#contractor-face_type").on("change", "input", function () {
                            if (!$('.page-content.payment-order').length) {
                                $('.error-summary').remove();
                            }
                            $('.forContractor').toggleClass('selectedDopColumns');
                        });
                        $('a.btn').click(function () {
                            $('#add-new').modal('hide');
                        });

                        $('.forProduct').addClass('selectedDopColumns');
                        $('.forService').removeClass('selectedDopColumns');
                        $('#product-goods-group_id').attr('disabled', true);
                        $('#product-service-group_id').removeAttr('disabled');
                        $('.field-product-product_unit_id').removeClass('required');
                        $('.import-products').hide();
                        $('.new_product_group_input').attr('placeholder', 'Добавить новую группу услуг');

                    } else if (data.items !== undefined) {

                        $('.order-add-static-items .service-count-value').text(data.services);
                        if (data.services > 0) {
                            $('li.select2-results__option.add-modal-services').removeClass('hidden');
                        }
                        //$('.order-add-static-items .product-count-value').text(data.goods);
                        //if (data.goods > 0) {
                        //    $('li.select2-results__option.add-modal-products').removeClass('hidden');
                        //}

                        ACT.addProductToTable(data.items);

                        $('#from-new-add-row').hide();

                        $('#add-new').modal('hide');
                    } else {
                        $('#add-new').modal('hide');
                    }
                }
            }
        );
    },

    surchargeInvoiceModal: {
        showModal: false,
        modal: $('#modal-add-invoice'),
        form: $('#edit-act'),
        formSubmit: $('#edit-act [type="submit"]'),
        amount: 0,
        bindEvents: function() {
            const that = this;
            $(document).on('change', 'input.quantity', function() {
                that.checkAvailableQuantity();
            });
            $(document).on('submit', '#edit-act', function(e) {
                if (that.showModal) {
                    that.modal.modal("show");
                    return false;
                }

                return true;
            });
            $(document).on('click', '#modal-add-invoice .yes', function() {
                that.showModal = false;
                that.form.submit();
            });
        },
        checkAvailableQuantity: function()
        {
            const that = this;
            that.showModal = false;
            that.amount = 0;

            const ndsViewId = parseInt($('.nds-view-type').val());

            ACT.getItems().each(function () {
                let $input = $(this).find('input.quantity');
                let quantity = parseFloat($input.val() || '0');
                let quantityAvailable = parseFloat($input.attr('available') || '0');
                let basePrice = parseFloat($input.attr('price') || '0') / 100;
                let taxRate = parseFloat($input.attr('tax-rate') || '0');
                let productTax = 0;

                if (quantity > quantityAvailable) {
                    that.showModal = true;

                    let productTotal = basePrice * (quantity - quantityAvailable);

                    if (ndsViewId === 2) {
                        productTax += 0;
                    } else if (ndsViewId === 1) {
                        productTax += productTotal * taxRate;
                    } else {
                        productTax += productTotal * taxRate / (1 + taxRate);
                    }

                    that.amount += productTotal + (ndsViewId === 1 ? productTax : 0);
                }
            });

            that.modal.find('.diff-amount').html(number_format(that.amount, 2, ',', ' '));

            if (that.showModal) {
                that.formSubmit.removeClass('ladda-button');
                $('#total_amount').css({color:'#f00'});
            } else {
                that.formSubmit.addClass('ladda-button');
                $('#total_amount').css({color:'#333'});
            }
        }
    }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
window.UPD = {
    $table: $('#product-table-upd'),
    //$addToOrderDocumentButton: $('#add-to-invoice-button'),
    init: function() {
        this.bindEvents();
        this.surchargeInvoiceModal.bindEvents();
    },
    bindEvents: function() {
        $(document).on('change', 'input.quantity', function() {
            UPD.recalculateTable();
        });
        $(document).on('change', 'input.price', function() {
            const $tr = $(this).closest('tr');
            const $input = $tr.find('input.quantity');
            let taxRate = parseFloat($input.attr('tax-rate') || '0');

            this.value = Number($(this).val().replace(',', '.').replace(/[^0-9\.]/g, '')).toFixed(2);
            let quantity = parseFloat($input.val() || '0');
            let basePrice = parseFloat(this.value || '0');
            let productTotal = basePrice * quantity;

            $tr.find('input.quantity').attr('price', 100 * basePrice);
            $tr.find('.product-amount_no_nds').text(number_format(productTotal, 2, ',', ' '));
            $tr.find('.product-amount').text(number_format((1+taxRate) * productTotal, 2, ',', ' '));
            UPD.recalculateTable();
        });
    },
    getItems: function () {
        return this.$table.find('tbody > tr.order');
    },
    getFloatPrice: function (price) {
        return number_format(price / 100, 2, ',', ' ');
    },
    recalculateTable: function () {
        const ndsViewId = parseInt($('.nds-view-type').val());
        let totalPrice = 0;
        let totalPriceWithNds = 0;
        let totalTax = 0;
        let totalQuantity = 0;
        let quantityPrecision = 0;
        this.getItems().each(function () {
            let $this = $(this);
            let $input = $(this).find('input.quantity');
            let quantity = parseFloat($input.val() || '0');
            let basePrice = parseFloat($input.attr('price') || '0') / 100;
            let priceWithNds = parseFloat($input.attr('price_with_nds') || '0') / 100;
            let taxRate = parseFloat($input.attr('tax-rate') || '0');

            let productTotal = basePrice * quantity;
            let productTotalWithNds = priceWithNds * quantity;
            let productTax;

            if (ndsViewId === 2) {
                productTax = 0;
            } else if (ndsViewId === 1) {
                productTax = productTotal * taxRate;
            } else {
                productTax = productTotal * taxRate;
            }

            totalTax += productTax;
            totalPrice += productTotal;
            totalPriceWithNds += productTotalWithNds;
            totalQuantity += quantity;

            $this.find('.product-amount_no_nds').text(number_format(productTotal, 2, ',', ' '));
            $this.find('.product-tax_amount').text(number_format(productTax, 2, ',', ' '));
            $this.find('.product-amount').text(number_format((ndsViewId === 1) ? (productTotal + productTax) : productTotalWithNds, 2, ',', ' '));

            if (parseFloat($input.val() || '0') > parseInt($input.val() || '0'))
                quantityPrecision = 3;
        });

        $('#total_price').text(number_format(totalPrice, 2, ',', ' '));
        $('#total_nds').text(number_format(totalTax, 2, ',', ' '));
        $('#total_amount').text(number_format((ndsViewId === 1) ? (totalPrice + totalTax) : (totalPriceWithNds), 2, ',', ' '));
        $('#total_quantity').text(number_format(totalQuantity, quantityPrecision, ',', ' '));

        //
        UPD.surchargeInvoiceModal.checkAvailableQuantity();
    },
    addProductToTable: function (items)
    {
        if (items) {
            const docType = parseInt($('.document-type').val());
            const viewType = parseInt($('.nds-view-type').val());
            const $template = UPD.$table.find('.template').clone();
            $template.removeClass('template').addClass('order');
            $template.find('input').attr('disabled', false);
            let $item;

            for (let i = 0; i < items.length; i++) {
                let item = items[i];
                let unitName = item.productUnit ? item.productUnit.name : '---';
                let codeOkei = item.productUnit ? item.productUnit.code_okei : '---';
                let basePrice = (docType === 1) ? Number(item.price_for_buy_with_nds) : Number(item.price_for_sell_with_nds);
                let priceForSellNds = Number(item.priceForSellNds.rate);
                let priceForBuyNds = Number(item.priceForBuyNds.rate);
                let priceForSell = Math.round(Number(item.price_for_sell_with_nds) / (priceForSellNds + 1));
                let priceForBuy  = Math.round(Number(item.price_for_buy_with_nds)  / (priceForBuyNds + 1));
                let price = (docType === 1) ? priceForBuy : priceForSell;
                let taxRate = (docType === 1) ? priceForBuyNds : priceForSellNds;
                let taxRateName = (docType === 1) ? item.priceForBuyNds.name : item.priceForSellNds.name;
                let taxAmount = taxRate * price;
                let fullAmount = basePrice;

                $item = $template.clone();
                $item.find('.product-title').text(item.title);
                $item.find('.product-unit-name').text(unitName);
                if (unitName === '---') {
                    $item.find('.product-count').remove();
                } else {
                    $item.find('.product-no-count').remove();
                }

                $item.find('.product-quantity input.quantity')
                    .attr('name', 'surchargeProduct[' + item.id + '][quantity]')
                    .attr('price', price)
                    .attr('tax-rate', taxRate)
                    .val(1);
                $item.find('.product-country select')
                    .attr('name', 'surchargeProduct[' + item.id + '][country_id]').val(item.country_origin_id);
                $item.find('.product-custom_declaration_number input')
                    .attr('name', 'surchargeProduct[' + item.id + '][custom_declaration_number]').val(item.customs_declaration_number);

                $item.find('.product-type_code').text(item.item_type_code || '---');
                $item.find('.product-code').text(item.code || '---');
                $item.find('.product-code_okei').text(codeOkei);
                $item.find('.product-box_type').text(item.box_type || '---');
                $item.find('.product-count_in_place').text(item.count_in_place || '---');
                $item.find('.product-place_count').text(item.place_count || '---');
                $item.find('.product-mass_gross').text(item.mass_gross || '---');
                $item.find('.product-price input.price')
                    .attr('name', 'surchargeProduct[' + item.id + '][price]')
                    .val(UPD.getFloatPrice(parseInt(price)));
                $item.find('.product-amount_no_nds').text(UPD.getFloatPrice(parseInt(price)));
                $item.find('.product-tax_rate').text(taxRateName);
                $item.find('.product-tax_amount').text(UPD.getFloatPrice(parseInt(taxAmount)));
                $item.find('.product-amount').text(UPD.getFloatPrice(parseInt(fullAmount)));
                $item.find('input.selected-product-id').val(item.id);

                $item.find('.simple-select-2').attr('id', 'product_country_' + item.id);
                $item.show().insertBefore('#from-new-add-row');
                createSimpleSelect2('product_country_' + item.id);
            }

            UPD.recalculateTable();

            return true;
        }

        return false;
    },
    addNewProduct: function (url, form) {
        $.post(
            url,
            form,
            function (data) {
                $('#add-new').modal();
                if (data) {
                    if (data.header !== undefined) {
                        $('#add-new .modal-title').html(data.header);
                        $('#block-modal-new-product-form').html(data.body);
                        $('#block-modal-new-product-form input').uniform('refresh');
                        $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                        $("#contractor-face_type").on("change", "input", function () {
                            if (!$('.page-content.payment-order').length) {
                                $('.error-summary').remove();
                            }
                            $('.forContractor').toggleClass('selectedDopColumns');
                        });
                        $('a.btn').click(function () {
                            $('#add-new').modal('hide');
                        });

                        $('.forProduct').addClass('selectedDopColumns');
                        $('.forService').removeClass('selectedDopColumns');
                        $('#product-goods-group_id').attr('disabled', true);
                        $('#product-service-group_id').removeAttr('disabled');
                        $('.field-product-product_unit_id').removeClass('required');
                        $('.import-products').hide();
                        $('.new_product_group_input').attr('placeholder', 'Добавить новую группу услуг');

                    } else if (data.items !== undefined) {

                        $('.order-add-static-items .service-count-value').text(data.services);
                        if (data.services > 0) {
                            $('li.select2-results__option.add-modal-services').removeClass('hidden');
                        }
                        $('.order-add-static-items .product-count-value').text(data.goods);
                        if (data.goods > 0) {
                            $('li.select2-results__option.add-modal-products').removeClass('hidden');
                        }

                        UPD.addProductToTable(data.items);

                        $('#from-new-add-row').hide();

                        $('#add-new').modal('hide');
                    } else {
                        $('#add-new').modal('hide');
                    }
                }
            }
        );
    },

    surchargeInvoiceModal: {
        showModal: false,
        modal: $('#modal-add-invoice'),
        form: $('#edit-upd'),
        formSubmit: $('#edit-upd [type="submit"]'),
        amount: 0,
        bindEvents: function() {
            const that = this;
            $(document).on('change', 'input.quantity', function() {
                that.checkAvailableQuantity();
            });
            $(document).on('submit', '#edit-upd', function(e) {
                if (that.showModal) {
                    that.modal.modal("show");
                    return false;
                }

                return true;
            });
            $(document).on('click', '#modal-add-invoice .yes', function() {
                that.showModal = false;
                that.form.submit();
            });
        },
        checkAvailableQuantity: function()
        {
            const that = this;
            that.showModal = false;
            that.amount = 0;

            const ndsViewId = parseInt($('.nds-view-type').val());

            UPD.getItems().each(function () {
                let $input = $(this).find('input.quantity');
                let quantity = parseFloat($input.val() || '0');
                let quantityAvailable = parseFloat($input.attr('available') || '0');
                let basePrice = parseFloat($input.attr('price') || '0') / 100;
                let taxRate = parseFloat($input.attr('tax-rate') || '0');
                let productTax = 0;

                if (quantity > quantityAvailable) {
                    that.showModal = true;

                    let productTotal = basePrice * (quantity - quantityAvailable);

                    if (ndsViewId === 2) {
                        productTax += 0;
                    } else if (ndsViewId === 1) {
                        productTax += productTotal * taxRate;
                    } else {
                        productTax += productTotal * taxRate;
                    }

                    that.amount += productTotal + productTax;
                }
            });

            that.modal.find('.diff-amount').html(number_format(that.amount, 2, ',', ' '));

            if (that.showModal) {
                that.formSubmit.removeClass('ladda-button');
                $('#total_amount').css({color:'#f00'});
            } else {
                that.formSubmit.addClass('ladda-button');
                $('#total_amount').css({color:'#333'});
            }
        }
    }
};

window.showToastr = function(message)
{
    if (typeof window.toastr !== 'undefined') {
        window.toastr.success(message, "", {
            "closeButton": true,
            "showDuration": 1000,
            "hideDuration": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 1000,
            "escapeHtml": false
        });
    }
};

$(document).on('hidden.bs.modal', '.modal', function(e) {
    $('body').toggleClass('modal-open', $('.modal:visible').length > 0);
});

$(document).on('click', 'button > a.client_new_tab_link', function(e) {
    e.preventDefault();
    $(this).parent().trigger('click');
});

/**
 * ofd_receipt details modal
 */
$(document).on("click", ".show_receipt_item[data-url]", function (e) {
    let url = $(this).data("url");
    $.ajax({
        url: url,
        success: function (content) {
            $("#ofd_receipt_view_modal .modal-content").html(content);
        }
    });
    $("#ofd_receipt_view_modal").modal("show");
});
$(document).on("hidden.bs.modal", "#ofd_receipt_view_modal", function (e) {
    $("#ofd_receipt_view_modal .modal-content").html("");
});
/* end */

function copyTextToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();

    window.toastr.success("Ссылка скопирована в буфер обмена", "", {
        "closeButton": true,
        "showDuration": 1000,
        "hideDuration": 1000,
        "timeOut": 3000,
        "extendedTimeOut": 1000,
        "escapeHtml": false,
    });
}