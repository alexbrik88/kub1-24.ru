var productGroupDropdownItemsTemplate = function(data, container) {
    container.setAttribute('data-id', data.id);
    if ($(data.element).data('editable') == '1') {
        let deleteButton = '';
        if ($(data.element).data('deletable') == '1') {
            deleteButton = '<i class="link pull-right del-exp-item mr-1" data-id="' + data.id + '" title="Удалить"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg></i>';
        }

        let content = '<div class="expenditure-item-name-label">'
            + '<i class="link pull-right edit-exp-item" data-id="'+data.id+'" title="Изменить"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg></i>'
            + deleteButton
            + '<div class="item-name">'+data.text+'</div>'
            + '</div>';

        container.innerHTML = content;
    } else {
        container.innerHTML = data.text;
    }
    return container;
}
var dleteProductGroup = function(input, target) {
    editProductGroupCancel();
    var itemId = $(target).data("id");
    var modalSelector = '#' + $(input).attr('id') + '-del-modal';
    $(modalSelector).on('show.bs.modal', function() {
        $(input).select2("close");
    });
    $(modalSelector).modal('show');
    $(modalSelector + ' .item-name').text($(target).parent().children('.item-name').text());
    $(modalSelector + ' .js-item-delete').on('click', function() {
        $.post($(input).data('delurl'), {"id":itemId}, function(data) {
            if (data.success) {
                $(input).find('option[value="'+itemId+'"]').remove();
                $(target).closest(".select2-results__option").remove();
                if ($(input).val() == itemId) {
                    $(input).val(null).trigger("change");
                }
            }
        });
    });
}
var editProductGroup = function(input, target) {
    editProductGroupCancel();
    var itemId = $(target).data('id');
    var $container = $(target).closest('.select2-results__option');
    var text = $container.find('div.item-name').html();
    var form = '<form class="expenditure-item-name-form" data-id="'+itemId+'" data-input="'+input.id+'">'
             + '<i class="link pull-right edit-exp-item-apply"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/img/svg/svgSprite.svg#check"></use></svg></i>'
             + '<i class="link pull-right edit-exp-item-cancel mr-1"><svg class="svg-icon" style="pointer-events: none!important;"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg></i>'
             + '<div class="input-wrapper">'
             + '<input type="hidden" name="id" value="'+itemId+'"/>'
             + '<input type="text" name="title" value="'+text+'" class="item-name-input"/>'
             + '</div>'
             + '</form>';
    $container.addClass('editable-expenditure-item');
    $container.children('.expenditure-item-name-label').hide();
    $container.append(form);
    $container.find('form').on('submit', function(e) {
        e.preventDefault();
        return productGroupFormSubmit(input, $(this));
    });
}
var editProductGroupCancel = function() {
    $('form.expenditure-item-name-form').remove();
    $('div.expenditure-item-name-label').show();
    $('.editable-expenditure-item').removeClass('editable-expenditure-item');
}
var productGroupFormSubmit = function(input, $form) {
    var itemId = $form.data('id')
    var $container = $form.parent();
    var $input = $(input);
    $.post($input.data('editurl'), $form.serialize(), function(data) {
        if (data.success) {
            var $option = $input.find('option[value="'+itemId+'"]');
            $container.find('.item-name').text(data.name);
            $option.text(data.name);
            editProductGroupCancel();
            if ($input.data('select2')) {
                $input.select2('destroy');
            }
            $input.select2(eval($input.attr('data-krajee-select2')));
            $.when($input.select2(eval($input.attr('data-krajee-select2')))).done(initS2Loading($input.attr('id'), $input.attr('data-s2-options')));
            $input.val(itemId).trigger('change');
        } else if (data.message) {
            alert(data.message);
        } else {
            alert('Ошибка сохранения изменений.');
        }
    });
    return false;
}
var editProductGroupApply = function(input, target) {
    return productGroupFormSubmit(input, $(target).parent());
}
