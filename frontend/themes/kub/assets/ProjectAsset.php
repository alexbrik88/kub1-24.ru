<?php

namespace frontend\themes\kub\assets;

use yii\web\AssetBundle;

/**
 * DocSendAsset
 */
class ProjectAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/kub/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
        'js/project_task_form.js',
        'js/ajax-form.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
