<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\permissions\Product;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */
/* @var $actualSubscription \common\models\service\Subscribe */
/* @var $lastActiveSubscribe \common\models\service\Subscribe */

$this->title = $model->name;

$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE, [
    'model' => $model,
]);

$backUrl = 'step-price-lists';
?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, ['class' => 'link mb-2']); ?>
<?php endif; ?>

    <div class="wrap wrap_padding_small">
        <div class="page-in row">
            <div class="page-in-content column">
                <div class="page-border">
                    <?= DocumentLogWidget::widget([
                        'model' => $model,
                        'toggleButton' => [
                            'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                            'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                            'title' => 'Последние действия',
                        ]
                    ]); ?>

                    <?php if (!$model->is_archive && Yii::$app->user->can(frontend\rbac\permissions\Product::UPDATE, ['model' => $model])): ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'update-card',
                            'id' => $model->id
                        ], [
                            'title' => 'Редактировать',
                            'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 ',
                        ]) ?>
                    <?php endif; ?>
                    <div class="doc-preview">
                        <div class="doc-container">
                            <?= $this->render('parts_view/_template', [
                                'model' => $model,
                                'canUpdate' => $canUpdate
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-in-sidebar column">
                <?= $this->render('parts_view/_status', [
                    'model' => $model,
                    'canUpdate' => $canUpdate
                ]); ?>
            </div>
        </div>
    </div>

<?= $this->render('parts_view/_actions_buttons', [
    'model' => $model,
    'actualSubscription' => $actualSubscription,
    'lastActiveSubscribe' => $lastActiveSubscribe
]); ?>

<?php /*
<div class="page-content-in" style="padding-bottom: 30px;">

    <div>
        <?= \yii\helpers\Html::a('Назад к списку', ['index'], [
                'class' => 'back-to-customers',
            ]) ?>
    </div>

    <div class="col-xs-12 pad0">

        <div class="col-xs-12 col-lg-7 pad0">
            <?= $this->render('parts_view/_template', [
                'model' => $model,
                'canUpdate' => $canUpdate
            ]); ?>
        </div>

        <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">

            <div class="col-xs-12" style="padding-right:0px !important;">

                <?= $this->render('parts_view/_status', [
                    'model' => $model,
                    'canUpdate' => $canUpdate
                ]); ?>

            </div>

        </div>

    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('parts_view/_actions_buttons', [
            'model' => $model,
            'canUpdate' => $canUpdate
        ]); ?>
    </div>

</div>
*/ ?>

<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>