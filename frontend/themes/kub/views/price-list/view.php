<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.10.2018
 * Time: 13:08
 */

use common\components\helpers\ArrayHelper;
use common\models\product\PriceList;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\product\Product;
use common\components\helpers\Html;
use common\components\image\EasyThumbnailImage;
use common\models\product\ProductCategory;
use common\models\product\ProductField;
use frontend\models\PriceListSearch;
use frontend\themes\kub\components\Icon;
use yii\bootstrap\Nav;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model PriceList
 * @var $priceListOrders \common\models\product\PriceListOrder[]
 */

$this->title = 'Прайс-лист "' . $model->name . '"';
$isProduct = $model->production_type == Product::PRODUCTION_TYPE_GOODS || $model->production_type == Product::PRODUCTION_TYPE_ALL;

$homeUrl = Yii::$app->user->isGuest && YII_ENV_PROD ? Yii::$app->params['serviceSite'] : Yii::$app->homeUrl;

$author = $model->priceListContact ?: $model->author;
$contacts = [];
if (!empty($author->phone)) $contacts[] = 'Тел: ' . $author->phone;
if (!empty($author->email)) $contacts[] = 'E-mail: ' . $author->email;
if (!empty($author->site))  $contacts[] = 'Сайт: ' . $author->site;
$contacts = implode(', ', $contacts);

// changing columns
$isName = $model->include_name_column;
$isArticle = $model->include_article_column;
$isGroup = $model->include_product_group_column;
$isReminder = $model->include_reminder_column;
$isUnit = $model->include_product_unit_column;
$isPrice = $model->include_price_column;
$isWeight = $model->include_weight_column;
$isNetto = $model->include_netto_column;
$isBrutto = $model->include_brutto_column;
$isCustomField = $model->include_custom_field_column;
$isCountInPackage = $model->include_count_in_package_column;
$isBoxType = $model->include_box_type_column;

// for modal
$isDescription = $model->include_description_column;
$isImage = $model->include_image_column;

$emptyMessage = 'Ничего не найдено';

$searchModel = new \frontend\models\PriceListOrderSearch();
$searchModel->ownerPriceList = $model;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

$downloadPdfUrl = Url::to(['download', 'type' =>'pdf', 'uid' => $model->uid]);
$downloadXlsUrl = Url::to(['download', 'type' =>'xls', 'uid' => $model->uid]);
$checkoutUrl    = Url::to(["/out/order-document/{$model->uid}"]);

/** @var ProductField $productField */
$productField = ProductField::findOne(['company_id' => $model->company_id]);
$productCategoryNames = Product::getCategoryNames();
$productCategoryFields = Product::getCategoryFieldNames();
$productCategoryColumns = [];
foreach ($model->productCategoryColumns as $fieldId) {
    $productCategoryColumns[] =
        [
            'attribute' => '_category_product_' . $fieldId,
            'label' => ArrayHelper::getValue($productCategoryFields[ProductCategory::TYPE_BOOK_PUBLISHER], $fieldId),
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
            ],
            'format' => 'raw',
            'value' => function ($model) use ($fieldId) {
                $productCategoryItems = $model->product->getCategoryFieldValues(ProductCategory::TYPE_BOOK_PUBLISHER);
                return ArrayHelper::getValue($productCategoryItems, $fieldId) ?: '---';
            },
        ];
}

if ($isImage || $isDescription) {
    $this->registerJs(<<<JS
    
    const hasTouchscreen = 'ontouchstart' in window;
    
    if (hasTouchscreen) {
        
        if (!sessionStorage.getItem("tooltip-image-modal-content")) {
            $('#pricelist-product-help-modal').modal('show');
            sessionStorage.setItem("tooltip-image-modal-content", "1");
        }
        
    } else {
        
        sessionStorage.removeItem("tooltip-image-modal-content");
        $(".show-pricelist-modal").tooltipster({
            "theme": ["tooltipster-dark"],
            "trigger": "hover",
            "side": "left",
            "contentAsHTML": true,
            "contentCloning": true
        });
        $("#pricelist-image-modal").one("shown.bs.modal", function() {
            sessionStorage.setItem("tooltip-image-modal-content", "1");
            $(".show-pricelist-modal").tooltipster("destroy");
        });
    }
JS
); } ?>
<div class="header" style="margin-top:10px;">
    <table style="width: 100%">
        <tr>
            <td class="company-logo" style="width: 20%">
                <?php if (is_file($path = $model->company->getImage('logoImage'))) : ?>
                    <?= ImageHelper::getThumb($path, [200, 54.4], [
                        'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                    ]); ?>
                <?php endif; ?>
            </td>
            <td class="main-text">
                <?= $model->company->getTitle(true); ?><br>
                <?= $author->getFio(); ?><br>
                <?= $contacts ?>
            </td>
            <td style="width: 20%">
                <?php if (($model->can_checkout || $model->can_checkout_invoice) && !$model->is_archive && !$model->is_deleted): ?>
                    <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'basket']) . '<span>Оформить заказ</span>', $checkoutUrl, [
                            'class' => 'button-clr button-regular button-regular_red',
                            'title' => 'Оформить заказ',
                            'data' => ['pjax' => 0],
                            'style' => 'display:block; float:right;'
                        ]
                    ); ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
</div>

<div class="document">
    <?php if ($model->is_deleted || $model->is_archive): ?>
        <div class="text-center price-list_deleted">
            Данный прайс-лист не действителен. <br>
            Запросите новый.
        </div>
    <?php else: ?>

        <?php $num = 0; ?>

        <?php \yii\widgets\Pjax::begin(['id' => 'price-list-pjax']); ?>

        <div class="table-settings row row_indents_s">
            <div class="col-6">
                <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'exel']), $downloadXlsUrl, [
                        'class' => 'button-list button-hover-transparent button-clr mr-2',
                        'title' => 'Скачать в Excel',
                        'data' => ['pjax' => 0]
                    ]
                ); ?>
                <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'pdf']), $downloadPdfUrl, [
                        'class' => 'button-list button-hover-transparent button-clr mr-2',
                        'title' => 'Скачать в PDF',
                        'data' => ['pjax' => 0],
                        'target' => '_blank'
                    ]
                ); ?>
            </div>
            <div class="col-6">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'options' => [
                        'class' => 'd-flex flex-nowrap align-items-center',
                        'data-pjax' => true
                    ],
                ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Поиск по названию' . ($isArticle ? ' и артикулу' : ''),
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>

        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => $emptyMessage,
            'tableOptions' => [
                'class' => 'table table-style table-count-list table-compact',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'class' => 'show-pricelist-modal',
                    'data' => [
                        'key' => 'order_' . $model->id,
                        'tooltip-content' => '#tooltip-image-modal-content'
                    ]
                ];
            },
            'columns' => array_merge([
                [
                    'headerOptions' => [
                        'width' => '1%',
                    ],
                    'format' => 'raw',
                    'value' => function ($model) use (&$num, $dataProvider) {
                        return ++$num + ($dataProvider->pagination->page * $dataProvider->pagination->pageSize);
                    },
                ],
                [
                    'attribute' => 'name',
                    'label' => 'Наименование',
                    'headerOptions' => [
                        'width' => '20%',
                        'class' => ($isName ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isName ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->name;
                    },
                ]],
                $productCategoryColumns, [
                [
                    'attribute' => 'article',
                    'label' => 'Артикул',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isArticle ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isArticle ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->article;
                    },
                ],
                [
                    'attribute' => 'product_group_id',
                    'label' => 'Группа',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isGroup ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isGroup ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->productGroup ? $model->productGroup->title : '---';
                    },
                    'filter' => $searchModel->getFilterGroups(),
                    's2width' => '200px'
                ],
                [
                    'attribute' => 'product.weight',
                    'label' => 'Вес (кг)',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isWeight ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isWeight ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->weight ?: '---';
                    },
                ],
                [
                    'attribute' => 'product.mass_net',
                    'label' => 'Масса нетто',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isNetto ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isNetto ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->mass_net ?: '---';
                    },
                ],
                [
                    'attribute' => 'product.mass_gross',
                    'label' => 'Масса брутто',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isBrutto ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isBrutto ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->mass_gross ?: '---';
                    },
                ],
                [
                    'attribute' => 'product.custom_field_value',
                    'label' => $productField->title ?? 'Ваше название',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isCustomField ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isCustomField ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->custom_field_value ?: '---';
                    },
                ],
                [
                    'attribute' => 'quantity',
                    'label' => 'Остаток',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isReminder ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isReminder ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        $isDecimal = ($model->quantity != (int)$model->quantity);
                        return ($model->quantity > 0) ? TextHelper::numberFormat($model->quantity, $isDecimal ? 2 : 0) : 0;
                    },
                ],
                [
                    'attribute' => 'product.box_type',
                    'label' => 'Вид упаковки',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isBoxType ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isBoxType ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->box_type ?: '---';
                    },
                ],
                [
                    'attribute' => 'product.count_in_package',
                    'label' => 'Количество в упаковке',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isCountInPackage ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isCountInPackage ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->product->count_in_package ?: '---';
                    },
                ],
                [
                    'attribute' => 'product_unit_id',
                    'label' => 'Ед. измер-я',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isUnit ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => ($isUnit ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->productUnit ? $model->productUnit->name : '---';
                    },
                ],
                [
                    'attribute' => 'price_for_sell',
                    'label' => 'Цена',
                    'headerOptions' => [
                        'width' => '10%',
                        'class' => ($isPrice ? '' : ' hidden')
                    ],
                    'contentOptions' => [
                        'class' => 'text-right ' . ($isPrice ? '' : ' hidden')
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return TextHelper::invoiceMoneyFormat($model->price_for_sell, 2);
                    },
                ],
            ]),
        ]); ?>
        <?php \yii\widgets\Pjax::end(); ?>

    <?php endif; ?>
</div>

<?php /* $this->render('_create_order', ['model' => $model]) */ ?>

<?php

$sortAttr = $model->getSortAttributeName();
$priceListOrders = $model->getPriceListOrders()
    ->joinWith('productGroup')
    ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
    ->indexBy('id')
    ->all();

echo $this->render('_clickable_product_modal', [
    'priceList' => $model,
    'priceListOrders' => $priceListOrders,
    'showModalBtn' => '.show-pricelist-modal'
]) ?>
<div style="display: none">
    <div id="tooltip-image-modal-content">Кликните<br/> на строку,<br/>чтобы получить<br/>информацию<br/>о товаре</div>
</div>

<?php \yii\bootstrap4\Modal::begin(['id' => 'pricelist-product-help-modal', 'toggleButton' => false]); ?>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="text-left">
        Кликните<br/> на строку,<br/>чтобы получить<br/>информацию<br/>о товаре
    </div>
<?php \yii\bootstrap4\Modal::end(); ?>