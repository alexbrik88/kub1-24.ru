<?php

use common\models\Company;
use common\models\service\SubscribeTariffGroup;
use frontend\themes\kub\components\Icon;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View  $this
 * @var Company $company
 * @var SubscribeTariffGroup $tariffGroup
 * @var array $tariffList
 */

$this->title = 'Инструкция по созданию Прайс-листа';

$baseUrl = Yii::$app->params['kubAssetBaseUrl'];
?>

<?= $this->render('_style') ?>
<?= $this->render('_steps', ['step' => 2]) ?>
<?= $this->render('_modal_instruction', [
    'title' => 'Видео инструкция 1',
    'src' => 'https://www.youtube.com/embed/_pJ7QW25S6Q'
]) ?>

<h4 class="mb-2">Прайс-лист</h4>
<div class="row mb-4" style="position: relative">
    <div class="column">
        <span style="font-size: 18px;">Создайте уникальный прайс-лист, который <b>ПРОДАЁТ за ВАС!</b></span>
    </div>
    <div style="position: absolute;right:15px; top:-23px;">
        <?= Html::button($this->render('//svg-sprite', ['ico' => 'video-instruction']).'<span>Видео инструкция 1</span>', [
            'class' => "button-regular button-regular_red",
            'data-toggle' => 'modal',
            'data-target' => "#modal-instruction",
        ]); ?>
    </div>
</div>

<div class="wrap">
    <h4 class="mb-3">Ваши выгоды</h4>

    <div class="row bblock">
        <div class="col-4" style="display: flex">
            <div class="st-tile medium">
                <img class="ico" src="/img/price_list/1.svg">
                <div class="title title-top-pad"><b>УДИВЛЯЙ</b></div>
                <div class="subtitle">
                    Вместо скучных Excel таблиц, отправляйте клиентам продающий прайс-лист с описанием товара, количеством, артикулом, стоимостью и фото. Ваш прайс-лист — это мини интернет-магазин!
                </div>
            </div>
        </div>
        <div class="col-4" style="display: flex">
            <div class="st-tile medium">
                <img class="ico" src="/img/price_list/2.svg">
                <div class="title title-top-pad"><b>ВЫДЕЛЯЙСЯ</b></div>
                <div class="subtitle">
                    Клиент получит ссылку на ваш прайс-лист с логотипом и вашими контактами. Он сможет открыть или скачать прайс-лист в любом удобном ему формате - WEB, PDF, XLS.
                </div>
            </div>
        </div>
        <div class="col-4" style="display: flex">
            <div class="st-tile medium">
                <img class="ico" src="/img/price_list/3.svg">
                <div class="title title-top-pad"><b>ПРОДАВАЙ</b></div>
                <div class="subtitle">
                    Ваш клиент сможет выбрать необходимое кол-во товара и оформить заказ сразу в прайс-листе! Он автоматически получит счет на оплату с возможностью мгновенно его оплатить. А вы получите информацию о заказе.
                </div>
            </div>
        </div>
    </div>

    <h4 class="mt-3 mb-3">Ваши преимущества</h4>

    <div class="row bblock">
        <div class="col-8">
            <div class="row">
                <div class="col-6" style="display: flex">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/21.svg">
                        <div class="title"><b>Уведомление <br/> о прочтении</b></div>
                        <div class="subtitle">
                            Как только клиент откроет прайс-лист, вам придет уведомление! Позвоните клиенту в тот момент, когда он изучает ваш прайс-лист. Вы его приятно удивите и, сразу сможете ответить на все его вопросы и проконсультировать по товару.
                        </div>
                    </div>
                </div>
                <div class="col-6" style="display: flex">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/22.svg">
                        <div class="title"><b>Прайс-лист <br/> онлайн</b></div>
                        <div class="subtitle">
                            Клиент может пользоваться им постоянно, пока вы его не отключите. Нужно поменять цену, кол-во, описание? Меняйте всё онлайн — данные в отправленном прайс-листе обновятся автоматически. Теперь не нужно после любой правки рассылать новые прайс-листы!
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6" style="display: flex">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/23.svg">
                        <div class="title"><b>Продающий <br/> прайс-лист</b></div>
                        <div class="subtitle">
                            Вам не нужно оформлять заказ и выставлять счета! Теперь заказ товара и счет на его оплату формируется из прайс-листа. Клиент это может сделать самостоятельно в любое время суток. Потенциальные клиенты обязательно оценят такой подход.
                        </div>
                    </div>
                </div>
                <div class="col-6" style="display: flex">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/24.svg">
                        <div class="title"><b>Индивидуальные <br/> предложения</b></div>
                        <div class="subtitle">
                            Создавайте индивидуальные прайс-листы под конкретного клиента с наценкой или скидкой. Добавляйте нужные столбцы и убирайте лишние.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="bg_1"></div>
        </div>
    </div>

    <h4 class="mt-3 mb-3">Как сделать <b>Прайс-лист</b></h4>

    <div class="row bblock">
        <div class="col-3" style="display: flex">
            <div class="st-tile">
                <div class="title"><b>ЛЕГКО</b><br/>создать</div>
                <div class="subtitle">
                    Достаточно один раз загрузить ваши товары и услуги в КУБ24. Загрузите список товаров с ценами, остатками, артикулами, описанием и даже фотографиями.
                </div>
                <div class="bottom-img">
                    <img src="/img/price_list/women_1.png" style="bottom:-10px;">
                </div>
            </div>
        </div>
        <div class="col-3" style="display: flex">
            <div class="st-tile">
                <div class="title"><b>УДОБНО</b><br/>редактировать</div>
                <div class="subtitle">
                    Загруженные товары и услуги можно использовать постоянно, включая или исключая нужные позиции в конкретном прайс-листе.
                </div>
                <div class="bottom-img">
                    <img  src="/img/price_list/women_2.png" style="bottom:0px;">
                </div>
            </div>
        </div>
        <div class="col-3" style="display: flex">
            <div class="st-tile">
                <div class="title"><b>БЫСТРО</b><br/>добавить индивидуальности</div>
                <div class="subtitle">
                    Загрузите ваш логотип, добавьте реквизиты компании и вашу контактную информацию.
                </div>
                <div class="bottom-img">
                    <img  src="/img/price_list/women_3.png" style="bottom:-10px;">
                </div>
            </div>
        </div>
        <div class="col-3" style="display: flex">
            <div class="st-tile">
                <div class="title"><b>ПРОСТО</b><br/>отправить</div>
                <div class="subtitle">
                    Отправьте удобным для клиента способом: по e-mail, в мессенджере или в соцсети. Прайс-лист будет аккуратен, и читаем как на компьютере, так и на телефоне.
                </div>
                <div class="bottom-img">
                    <img  src="/img/price_list/women_4.png" style="bottom:0px;">
                </div>
            </div>
        </div>
    </div>

    <h4 class="mt-3 mb-3">Тарифы</h4>

    <?= $this->render('_tariff', [
        'user' => $user,
        'company' => $user->company,
        'tariffGroup' => $tariffGroup,
        'tariffList' => $tariffList,
    ]) ?>

</div>
<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-end">
        <div class="column">
        </div>
        <div class="column">
            <?= Html::a('Попробовать БЕСПЛАТНО', ['step-company'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
            ]); ?>
        </div>
    </div>
</div>