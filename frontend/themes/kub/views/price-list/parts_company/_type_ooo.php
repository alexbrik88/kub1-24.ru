<?php

use yii\helpers\Html;
use common\models\company\CompanyType;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$typeArray = ['' => ''] + CompanyType::find()->inContractor()
        ->select(['name_short', 'id'])
        ->orderBy([
            'IF([[id]] < 5, [[id]], 5)' => SORT_ASC,
            'name_short' => SORT_ASC,
        ])
        ->indexBy('id')
        ->column();

?>

<?= Html::activeHiddenInput($model, 'name_short'); ?>
<?= Html::activeHiddenInput($model, 'ogrn'); ?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'inn')->textInput([
                    'maxlength' => true,
                ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'kpp')->textInput([
                    'maxlength' => true,
                ]) ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'taxRegistrationDate')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control date-picker ico',
                    'autocomplete' => 'off',
                    'value' => $model->taxRegistrationDate,
                    'disabled' => !empty($model->taxRegistrationDate),
                ])->label('Дата регистрации '.$model->companyType->name_short) ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <div class="form-filter">
                    <?= $form->field($model, 'company_type_id')->dropDownList($typeArray)->label('Форма собственности'); ?>
                </div>
            </div>
            <div class="col-6 form-group mb-3">
                <?= $form->field($model, 'name_full')->label('Название')->textInput(); ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-6 form-group mb-3">
                <?= $form->field($model, 'address_legal')->label('Юридический адрес')->textInput(); ?>
            </div>
        </div>
    </div>
</div>
<strong class="small-title d-block mb-3 pt-3">Руководитель</strong>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3">
                <?= $form->field($model, 'chief_post_name')->label('Должность')->textInput(); ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'chief_lastname')->label('Фамилия')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'chief_firstname')->label('Имя')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'chief_patronymic')->label('Отчество')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3 pt-4 pl-0">
                <div class="pt-1">
                    <div class="checkbox d-block mb-3 mt-2">
                        <?= $form->field($model, 'has_chief_patronymic')->checkbox([
                            'data-target' => '#company-chief_patronymic',
                        ], true) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
