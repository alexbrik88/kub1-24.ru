<?php
use yii\bootstrap4\Modal;
use yii\helpers\Url;

Modal::begin([
    'id' => 'modal-instruction',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>

    <?php if (!empty($title)): ?>
        <h4 class="modal-title"><?= $title ?></h4>
    <?php endif; ?>

    <?php if (!empty($src)): ?>
        <iframe width="560" height="315" src="<?= $src ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <?php endif; ?>

<?php Modal::end(); ?>
