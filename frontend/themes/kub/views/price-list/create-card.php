<?php

use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\ProductField;
use frontend\models\Documents;
use frontend\modules\telegram\widgets\CodeModalWidget;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use common\models\product\PriceListStatus;
use yii\helpers\Url;
use common\models\product\ProductCategory;

/** @var $model \common\models\product\PriceList */
/** @var $modelContact \common\models\product\PriceListContact */
/** @var $actualSubscription \common\models\service\Subscribe */
/** @var $lastActiveSubscribe \common\models\service\Subscribe */
/** @var $tariffRules array */

$this->title = ($model->isNewRecord) ? 'Создать Прайс-Лист' : 'Редактировать Прайс-Лист';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);

$form = ActiveForm::begin([
    'id' => 'price-list-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);

$storeList = $user->getStores()
    ->select(['name'])
    ->orderBy(['is_main' => SORT_DESC])
    ->indexBy('id')
    ->column();

$contactModel = $model->contact ?: $modelContact;

if ($model->discount == round($model->discount))
    $model->discount = round($model->discount);
if ($model->markup == round($model->markup))
    $model->markup = round($model->markup);
if ($model->discount_from_sum_percent == round($model->discount_from_sum_percent))
    $model->discount_from_sum_percent = round($model->discount_from_sum_percent);
if ($model->markup_from_sum_percent == round($model->markup_from_sum_percent))
    $model->markup_from_sum_percent = round($model->markup_from_sum_percent);

$disableChangeDiscountMarkup = $model->status_id == PriceListStatus::STATUS_SEND && !$model->auto_update_prices;

/** @var ProductField $productField */
$productField = ProductField::findOne(['company_id' => Yii::$app->user->identity->company->id]);

$categoryNames = Product::getCategoryNames();
$categoryFields = Product::getCategoryFieldNames();
?>

<?= $form->errorSummary($model); ?>

<?= Html::hiddenInput('documentType', Documents::IO_TYPE_OUT, ['id' => 'documentType']); // for products table ?>
<?= Html::hiddenInput('PriceList[is_custom_price_list]', 1) ?>

<div class="wrap">
    <div class="row row_indents_s flex-nowrap align-items-center">
        <div class="column pr-3" style="flex:1">
            <div class="form-title">
                Название
            </div>
        </div>
        <div class="column pr-3" style="flex:10">
            <?= $form->field($model, 'name', ['options' => ['class' => '', 'style' => 'padding-top: 2px;']])->textInput([
                'maxlength' => true,
            ])->label(false) ?>
        </div>
        <div class="column" style="flex:3; min-width: 212px;">
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'video-instruction']).'<span>Видео инструкция 2</span>', [
                'class' => "button-regular button-regular_red w-100",
                'data-toggle' => 'modal',
                'data-target' => "#modal-instruction",
            ]); ?>
        </div>
    </div>
</div>

<div class="wrap p-4">

    <strong class="small-title d-block mb-3">Контактные данные</strong>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3">
                    <?= $form->field($contactModel, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]) ?>
                </div>
                <div class="form-group col-3 mb-3">
                    <?= $form->field($contactModel, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-3 mb-3">
                    <?= $form->field($contactModel, 'site')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 form-group mb-3">
                    <?= $form->field($contactModel, 'lastname')->label('Фамилия')->textInput(); ?>
                </div>
                <div class="col-3 form-group mb-3">
                    <?= $form->field($contactModel, 'firstname')->label('Имя')->textInput(); ?>
                </div>
                <div class="col-3 form-group mb-3">
                    <?= $form->field($contactModel, 'patronymic')->label('Отчество')->textInput(); ?>
                </div>
                <div class="col-3 form-group mb-3 pt-4 pl-0">
                    <div class="pt-1">
                        <div class="checkbox d-block mb-3 mt-2">
                            <?= $form->field($contactModel, 'no_patronymic')->checkbox([
                            ], true) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-3 form-group mb-3">
                    <strong class="small-title d-block mb-3">
                        Столбцы в прайс-листе
                    </strong>
                    <div class="include-columns_list form-group">
                        <?= \common\components\helpers\Html::activeCheckbox($model, 'include_name_column', [
                            'label' => 'Наименование',
                            'class' => 'disabled-always toggle_products_column',
                            'data-target' => '.col_name',
                            'disabled' => true
                        ]); ?>
                        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS || $productionType == Product::PRODUCTION_TYPE_ALL): ?>
                            <?= Html::activeCheckbox($model, 'include_article_column', [
                                'label' => 'Артикул',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_article'
                            ]); ?>
                        <?php endif; ?>
                        <?= Html::activeCheckbox($model, 'include_product_group_column', [
                            'label' => 'Группа ',
                            'class' => 'toggle_products_column',
                            'data-target' => '.col_group'
                        ]); ?>
                        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS || $productionType == Product::PRODUCTION_TYPE_ALL): ?>
                            <?= Html::activeCheckbox($model, 'include_weight_column', [
                                'label' => 'Вес (кг)',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_weight'
                            ]); ?>
                            <?= Html::activeCheckbox($model, 'include_netto_column', [
                                'label' => 'Масса нетто',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_netto'
                            ]); ?>
                            <?= Html::activeCheckbox($model, 'include_brutto_column', [
                                'label' => 'Масса брутто',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_brutto'
                            ]); ?>
                        <?php endif; ?>
                        <div style="position: relative">
                            <?= Html::activeCheckbox($model, 'include_custom_field_column', [
                                'label' => $productField->title ?? 'Ваше название',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_custom_field'
                            ]); ?>
                            <?= Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'style' => 'position:absolute;',
                                'data-tooltip-content' => '#tooltip_custom_field',
                            ]) ?>
                        </div>
                        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS || $productionType == Product::PRODUCTION_TYPE_ALL): ?>
                            <?= Html::activeCheckbox($model, 'include_reminder_column', [
                                'id' => 'include_reminder_column',
                                'label' => 'Остаток',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_quantity'
                            ]); ?>
                            <?= Html::activeCheckbox($model, 'include_box_type_column', [
                                'label' => 'Вид упаковки',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_box_type'
                            ]); ?>
                            <?= Html::activeCheckbox($model, 'include_count_in_package_column', [
                                'label' => 'Количество в упаковке',
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_count_in_package'
                            ]); ?>
                        <?php endif; ?>
                        <?= Html::activeCheckbox($model, 'include_product_unit_column', [
                            'label' => 'Ед. измерения',
                            'class' => 'disabled-always toggle_products_column',
                            'data-target' => '.col_unit_name',
                            'disabled' => true
                        ]); ?>
                        <?= Html::activeCheckbox($model, 'include_price_column', [
                            'label' => 'Цена (продажи)',
                            'class' => 'toggle_products_column',
                            'data-target' => '.col_price'
                        ]); ?>
                    </div>
                    <div style="position: absolute; width: 275px;">
                        Максимальное кол-во столбцов <span class="js-include-columns-count-limit"></span>.
                    </div>
                </div>

                <div class="col-3 form-group mb-3">
                    <strong class="small-title d-block mb-3">
                        Сортировка по столбцу
                    </strong>
                    <?= $form->field($model, 'sort_type')
                        ->radioList([
                            PriceList::SORT_BY_NAME => 'Наименование',
                            PriceList::SORT_BY_GROUP => 'Группа ',
                            PriceList::SORT_CUSTOM => 'Вручную'
                        ])->label(false); ?>
                </div>

                <div class="col-6 form-group mb-3 price-list-options" style="position: relative">
                    <strong class="small-title d-block mb-3">
                        Дополнительно
                    </strong>
                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'can_checkout_invoice', [
                            'label' => 'Оформление заказа и счета из прайс-листа',
                            'disabled' => !$tariffRules['has_checkout']
                        ]); ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'style' => 'position:absolute;',
                            'data-tooltip-content' => '#tooltip_can_checkout_invoice',
                        ]) ?>
                    </div>
                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'can_checkout', [
                            'label' => 'Оформление только заказа из прайс-листа',
                            'disabled' => !$tariffRules['has_checkout']
                        ]); ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'style' => 'position:absolute;',
                            'data-tooltip-content' => '#tooltip_can_checkout',
                        ]) ?>
                    </div>
                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'include_description_column', [
                            'label' => 'Добавить описание',
                            'disabled' => !$tariffRules['has_product_view']
                        ]); ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'style' => 'position:absolute;',
                            'data-tooltip-content' => '#tooltip_image_description',
                        ]) ?>
                    </div>
                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'include_image_column', [
                            'label' => 'Добавить изображение',
                            'disabled' => !$tariffRules['has_product_view']
                        ]); ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'style' => 'position:absolute;',
                            'data-tooltip-content' => '#tooltip_image_description',
                        ]) ?>
                    </div>

                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'has_discount', [
                            'class' => 'toggle_products_column toggle_in_form_only discount-percent',
                            'data-target' => '.col_discount',
                            'label' => 'Добавить скидку на весь товар',
                            'data-value' => (int)$model->has_discount,
                            'disabled' => $disableChangeDiscountMarkup,
                        ]); ?>
                        <div class="discount_block <?= (!$model->has_discount) ? 'hide' : '' ?>">
                            <?= $form->field($model, 'discount', ['options' => ['tag' => false]])
                                ->textInput([
                                    'id' => 'all-discount',
                                    'data-value' => $model->discount,
                                    'disabled' => $disableChangeDiscountMarkup
                                ])
                                ->label('%%', ['style' => '']) ?>
                            <div class="discount_hint">В прайс-листе цена будет с учетом скидки</div>
                        </div>
                    </div>

                    <div style="position:relative;">
                        <?= Html::activeCheckbox($model, 'has_markup', [
                            'class' => 'toggle_products_column toggle_in_form_only markup-percent',
                            'data-target' => '.col_markup',
                            'label' => 'Добавить наценку на весь товар',
                            'data-value' => (int)$model->has_markup,
                            'disabled' => $disableChangeDiscountMarkup
                        ]); ?>
                        <div class="markup_block <?= (!$model->has_markup) ? 'hide' : '' ?>">
                            <?= $form->field($model, 'markup', ['options' => ['tag' => false]])
                                ->textInput([
                                    'id' => 'all-markup',
                                    'data-value' => $model->markup,
                                    'disabled' => $disableChangeDiscountMarkup
                                ])
                                ->label('%%', ['style' => '']) ?>
                            <div class="markup_hint">В прайс-листе цена будет с учетом наценки</div>
                        </div>
                    </div>

                    <?= Html::activeCheckbox($model, 'auto_update_prices', [
                        'label' => 'Автоматически обновлять цены',
                        'disabled' => !$tariffRules['has_auto_update']
                    ]); ?>
                    <?= Html::activeCheckbox($model, 'auto_update_quantity', [
                        'label' => 'Автоматически обновлять остатки',
                        'disabled' => !$tariffRules['has_auto_update']
                    ]); ?>
                    <div style="position: relative">
                        <?= Html::activeCheckbox($model, 'use_notifications', [
                            'label' => 'Получать уведомления об открытии',
                            'disabled' => !$tariffRules['has_notification']
                        ]); ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'style' => 'position:absolute;',
                            'data-tooltip-content' => '#tooltip_notifications',
                        ]) ?>
                    </div>

                    <?= Html::activeCheckbox($model, 'is_contractor_physical', [
                        'label' => 'Продавать только физ. лицам',
                    ]); ?>

                    <div style="position: relative; margin-top: 27px;">
                        <?= Html::activeCheckbox($model, 'has_discount_from_sum', [
                            'label' => 'Скидка при заказе<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;на сумму',
                            'data-value' => (int)$model->has_discount_from_sum,
                            'disabled' => $disableChangeDiscountMarkup,
                        ]); ?>
                        <div class="discount_from_sum_block <?= (!$model->has_discount_from_sum) ? 'hide' : '' ?>">
                            <div class="row">
                                <div class="col-7 pl-0">
                                <?= $form->field($model, 'discount_from_sum', ['options' => ['tag' => false]])
                                    ->textInput([
                                        'disabled' => $disableChangeDiscountMarkup,
                                        'value' => round($model->discount_from_sum / 100, 2)
                                    ])
                                    ->label('Сумма', ['style' => '']) ?>
                                </div>
                                <div class="col-5 pl-0">
                                <?= $form->field($model, 'discount_from_sum_percent', ['options' => ['tag' => false]])
                                    ->textInput([
                                        'disabled' => $disableChangeDiscountMarkup
                                    ])
                                    ->label('%%', ['style' => '']) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="position: relative;">
                        <?= Html::activeCheckbox($model, 'has_markup_from_sum', [
                            'label' => 'Наценка при заказе<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;на сумму',
                            'data-value' => (int)$model->has_markup_from_sum,
                            'disabled' => $disableChangeDiscountMarkup,
                        ]); ?>
                        <div class="markup_from_sum_block <?= (!$model->has_markup_from_sum) ? 'hide' : '' ?>">
                            <div class="row">
                                <div class="col-7 pl-0">
                                    <?= $form->field($model, 'markup_from_sum', ['options' => ['tag' => false]])
                                        ->textInput([
                                            'disabled' => $disableChangeDiscountMarkup,
                                            'value' => round($model->markup_from_sum / 100, 2)
                                        ])
                                        ->label('Сумма', ['style' => '']) ?>
                                </div>
                                <div class="col-5 pl-0">
                                    <?= $form->field($model, 'markup_from_sum_percent', ['options' => ['tag' => false]])
                                        ->textInput([
                                            'disabled' => $disableChangeDiscountMarkup
                                        ])
                                        ->label('%%', ['style' => '']) ?>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    
                </div>

                <div class="col-6 form-group mb-3 pb-3">
                    <button class="forProduct link link_collapse link_bold button-clr mb-2 pb-1 collapsed" type="button" data-toggle="collapse" data-target="#forProductCategory" aria-expanded="true" aria-controls="forProductCategory">
                        <span class="link-txt" style="border-bottom: 2px dotted #0097fd">Доп. столбцы для издательств</span>
                        <svg class="link-shevron svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>

                    <div id="forProductCategory" class="collapse">

                        <?php foreach ($categoryFields[ProductCategory::TYPE_BOOK_PUBLISHER] as $fieldId => $fieldName): ?>
                            <?= Html::checkbox('include_product_category_columns['.$fieldId.']',
                                in_array($fieldId, $model->productCategoryColumns), [
                                'label' => $fieldName,
                                'class' => 'toggle_products_column',
                                'data-target' => '.col_product_category_column_' . $fieldId
                            ]); ?><br/>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-6 form-group mb-3 pb-3">
                    <button
                            class="forProduct link link_collapse link_bold button-clr mb-2 pb-1 collapsed"
                            type="button"
                            data-toggle="collapse"
                            data-target="#notificationSettings"
                            aria-expanded="true"
                            aria-controls="notificationSettings">
                        <span class="link-txt" style="border-bottom: 2px dotted #0097fd">Настройки уведомлений</span>
                        <svg class="link-shevron svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>

                    <div id="notificationSettings" class="collapse">
                        <div style="position: relative">
                            <?= Html::activeCheckbox($model, 'notify_to_telegram', [
                                'disabled' => !$tariffRules['has_notification'],
                                'onchange' => 'return Telegram.CheckBox.onchange(this);'
                            ]); ?>
                            <?= Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'style' => 'position:absolute;',
                                'data-tooltip-content' => '#tooltip_notify_to_telegram',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-3 form-group">
                    <strong class="small-title d-block mb-3">Включить в прайс-лист</strong>
                    <?= $form->field($model, '_price_list_type_id')
                        ->widget(\kartik\select2\Select2::class, [
                            'hideSearch' => true,
                            'data' => PriceList::$includeTypes,
                            'options' => [
                                'value' => $model->getPriceListType(),
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ])
                        ->label(false); ?>
                </div>
                <div class="col-9 form-group price-list-stores">
                    <div class="collapse <?= $model->include_reminder_column ? 'show' : '' ?>" id="stockCollapse">
                        <strong class="small-title d-block mb-3">Учитывать остатки товара на складах</strong>
                        <?= Html::checkbox('storeId[]', true, [
                            'class' => 'choose_store store_all',
                            'label' => 'Все склады',
                            'value' => 'all'
                        ]); ?>
                        <?php foreach ($storeList as $id => $storeName): ?>
                            <?= Html::checkbox('storeId[]', false, [
                                'class' => 'choose_store',
                                'label' => $storeName,
                                'value' => $id
                            ]); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap custom_products_block mb-2 <?= $model->priceListType != PriceList::FOR_SELECTIVELY ? 'hidden' : '' ?>">
    <?= $this->render('parts_create/_product_table', [
        'model' => $model,
        'company' => $company,
        'productField' => $productField
    ]); ?>
</div>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?php if ($actualSubscription || !$lastActiveSubscribe): ?>
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                    'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            <?php else: ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Сохранить',
                        'class' => 'button-width button-clr button-regular button-regular_red'
                    ],
                    'confirmUrl' => Url::to([
                        '/subscribe'
                    ]),
                    'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                        'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                        'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', ($model->isNewRecord) ? ['step-price-lists'] : ['view-card', 'id' => $model->id], [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>


<?php $form->end(); ?>

<div class="tooltip-templates" style="display: none">
    <div id="tooltip_image_description">
        При клике на строку прайс-листа откроется<br/>
        карточка товара с описанием и изображением
    </div>
    <div id="tooltip_can_checkout_invoice">
        Это позволит вашему клиенту из прай-листа сделать заказ и выставить счет.<br/>
        Заказ и счет создадутся у вас в КУБе
    </div>
    <div id="tooltip_can_checkout">
        Это позволит вашему клиенту из прай-листа сделать заказ.<br/>
        Заказ создастся у вас в КУБе
    </div>
    <div id="tooltip_notifications">
        Когда клиент откроет ваш прайс-лист<br/>
        вы получите уведомление. Так же<br/>
        в карточке прайс-листа будут отмечены<br/>
        все клиенты, которые открыли его.
    </div>
    <div id="tooltip_notify_to_telegram">
        Подключив уведомления в Телеграмм, вы<br/>
        сможете получить сообщения о новом заказе<br/>
        и счете выставленном через прайс лист
    </div>
    <div id="tooltip_custom_field">
        В карточке товара вы можете<br/>
        изменить это название<br/>
        на нужное вам.
    </div>
</div>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_existed', [
    'model' => $model,
]); ?>

<?= $this->render('parts_create/_confirm_modals') ?>

<?= $this->render('_modal_instruction', [
    'title' => 'Видео инструкция 2',
    'src' => 'https://www.youtube.com/embed/ZjQwc3FKtZY'
]) ?>

<?= CodeModalWidget::widget(); ?>

<script>

    $('#price-list-form').on('submit', function() {
        const form = $(this);
        const selectPriceListType = $('#pricelist-_price_list_type_id');
        const productsTable = $('#table-product-list-body');
        if (selectPriceListType.val() === "<?= PriceList::FOR_SELECTIVELY ?>") {
            form.find('.sort-hidden-input').remove();
            productsTable.find('tr.product-row').each(function(i,v) {
                $('<input/>', {
                    type: 'hidden',
                    name: 'sort[]',
                    class: 'sort-hidden-input',
                    value: $(v).attr('id').replace(/\D+/,'')
                }).appendTo(form);
            });
        }
        return true;
    });

    $('#include_reminder_column').on('click', function() {
        $('#stockCollapse').collapse($(this).prop('checked') ? 'show' : 'hide');
    });
    // toggle checkboxes
    $('#pricelist-has_discount').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.price-list-options').find('.discount_block').removeClass('hide');
            $('#pricelist-has_markup').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.price-list-options').find('.discount_block').addClass('hide');
        }
    });
    $('#pricelist-has_markup').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.price-list-options').find('.markup_block').removeClass('hide');
            $('#pricelist-has_discount').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.price-list-options').find('.markup_block').addClass('hide');
        }
    });
    // toggle checkboxes
    $('#pricelist-can_checkout').on('change', function() {
        if ($(this).prop('checked')) {
            $('#pricelist-can_checkout_invoice').prop('checked', false).uniform('refresh').trigger('change');
        }
    });
    $('#pricelist-can_checkout_invoice').on('change', function() {
        if ($(this).prop('checked')) {
            $('#pricelist-can_checkout').prop('checked', false).uniform('refresh').trigger('change');
        }
    });
    // toggle checkboxes
    $('#pricelist-has_discount_from_sum').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.price-list-options').find('.discount_from_sum_block').removeClass('hide');
            $('#pricelist-has_markup_from_sum').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.price-list-options').find('.discount_from_sum_block').addClass('hide');
        }
    });
    $('#pricelist-has_markup_from_sum').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.price-list-options').find('.markup_from_sum_block').removeClass('hide');
            $('#pricelist-has_discount_from_sum').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.price-list-options').find('.markup_from_sum_block').addClass('hide');
        }
    });

    $('.choose_store').on('click', function() {
        if ($('.choose_store').filter(':checked').length == 0) {
            if ($(this).hasClass('store_all'))
                $('.choose_store').not('.store_all').first().prop('checked', true).uniform('refresh');
            else
                $('.store_all').prop('checked', true).uniform('refresh');
        } else if ($(this).val() == 'all') {
            $('.choose_store').not('.store_all').prop('checked', false).uniform('refresh');
        } else {
            $('.store_all').prop('checked', false).uniform('refresh');
        }
        window.PriceListForm.recalculateQuantities();
    });

    $('#pricelist-_price_list_type_id').on('change', function() {
        var $productBlock = $('.custom_products_block');
        if ($(this).val() == "<?= PriceList::FOR_SELECTIVELY ?>")
            $productBlock.removeClass('hidden');
        else
            $productBlock.addClass('hidden');
    });

    $('.toggle_products_column').on('change', function() {
        var targetBlock = $(this).data('target');
        if ($(this).prop('checked')) {
            $(targetBlock).removeClass('hidden');
        } else {
            $(targetBlock).addClass('hidden');
        }

        window.PriceListForm.checkIncludeColumnsCountLimit();
    });

    $(document).ready(function() {
        window.PriceListForm.checkIncludeColumnsCountLimit();
        $('.js-include-columns-count-limit').text(window.PriceListForm.COLUMNS_LIMIT);
    });

    $('#add-from-exists').on("hidden.bs.modal", function () {
        setTimeout("window.PriceListForm.recalculateQuantities()", 500);
    });
    $('#add-new').on("hidden.bs.modal", function () {
        setTimeout("window.PriceListForm.recalculateQuantities()", 500);
    });

    window.PriceListForm = {
        COLUMNS_LIMIT: 10,
        recalculateQuantities: function()
        {
            $('.order-param-value.product-quantity').each(function(j, column) {
                var totalQuantity = 0;
                $('.choose_store').filter(':checked').each(function(i, checkbox) {
                    var storeId = $(checkbox).val();
                    var quantity = $(column).attr('data-store-' + storeId) || 0;
                    totalQuantity += parseFloat(quantity);
                });
                $(column).text(totalQuantity);
            });
        },
        refreshMarkupDiscount: function()
        {
            $("#table-for-invoice tbody .discount-input").val($('#all-discount').val()).each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
            $("#table-for-invoice tbody .markup-input").val($('#all-markup').val()).each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
        },
        checkIncludeColumnsCountLimit: function() {
            let $message = $('.js-include-columns-count-limit').parent();
            let $checkboxes = $('.toggle_products_column').filter(':not(.toggle_in_form_only)');
            if ($checkboxes.filter(':checked').length >= this.COLUMNS_LIMIT) {
                $checkboxes.filter(':not(:checked)').prop('disabled', true).uniform('refresh');
                $message.css('color', '#e30611');
            } else if ($checkboxes.filter(':not(:checked):disabled').length) {
                $checkboxes.filter(':not(:checked)').prop('disabled', false).uniform('refresh');
                $message.css('color', '#001424');
            }
        }
    };

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });

    $('#all-discount').on('change', function() {
        var $input = $("input#all-discount");
        var val = Math.max(Math.min(parseFloat($input.val()), 99.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#table-for-invoice tbody .markup-input").val(0);
        $("#table-for-invoice tbody .discount-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        // INVOICE.recalculateInvoiceTable();
    });

    $('#all-markup').on('change', function() {
        var $input = $("input#all-markup");
        var val = Math.max(Math.min(parseFloat($input.val()), 9999.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#table-for-invoice tbody .discount-input").val(0);
        $("#table-for-invoice tbody .markup-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        // INVOICE.recalculateInvoiceTable();
    });

    $('#pricelist-has_discount').on('change', function() {
        window.PriceListForm.refreshMarkupDiscount();
    });
    $('#pricelist-has_markup').on('change', function() {
        window.PriceListForm.refreshMarkupDiscount();
    });

    <?php if ($model->status_id == PriceListStatus::STATUS_SEND): ?>
        $('#pricelist-auto_update_prices').on('change', function() {
            var has_discount = $('#pricelist-has_discount');
            var has_markup = $('#pricelist-has_markup');
            var discount = $('#all-discount');
            var markup = $('#all-markup');

            if ($(this).prop('checked')) {
                $('#modal-auto-update-prices').modal('show');
            } else {
                $(has_discount).prop('checked', $(has_discount).data('value')).trigger('change').attr('disabled', 'disabled');
                $(has_markup).prop('checked', $(has_markup).data('value')).trigger('change').attr('disabled', 'disabled');
                $(discount).val($(discount).data('value')).attr('disabled', 'disabled');
                $(markup).val($(markup).data('value')).attr('disabled', 'disabled');
            }
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
        $('#modal-auto-update-prices button.yes').on('click', function() {
            $('#pricelist-has_discount').removeAttr('disabled');
            $('#pricelist-has_markup').removeAttr('disabled');
            $('#all-discount').removeAttr('disabled');
            $('#all-markup').removeAttr('disabled');
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
        $('#modal-auto-update-prices button.no').on('click', function() {
            $('#pricelist-has_discount').attr('disabled', 'disabled');
            $('#pricelist-has_markup').attr('disabled', 'disabled');
            $('#all-discount').attr('disabled', 'disabled');
            $('#all-markup').attr('disabled', 'disabled');
            $('#pricelist-auto_update_prices').prop('checked', false);
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
    <?php endif; ?>

</script>