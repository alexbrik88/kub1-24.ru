<?php

use common\components\widgets\StepMenu;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\models\Company;

$company = Yii::$app->user->identity->company;
$type = $company->companyType->name_short;
$isIp = $company->company_type_id == CompanyType::TYPE_IP;
?>

<div class="menu-nav-tabs nav-tabs-row pb-3 mb-1">
    <?= StepMenu::widget([
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 justify-content-around',
        ],
        'itemOptions' => [
            'class' => 'nav-item pl-2 pr-2 d-flex flex-column',
            'style' => 'flex: 1 1 0;',
        ],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => 'Инструкция',
                'url' => ['/price-list/step-instruction'],
                'template' => $this->render('_step_link', ['i' => 'description']),
            ],
            [
                'label' => 'Реквизиты вашего ' . $company->companyType->name_short,
                'url' => ['/price-list/step-company'],
                'template' => $this->render('_step_link', ['i' => $isIp ? 'man' : 'bag']),
            ],
            [
                'label' => 'Товары',
                'url' => ['/price-list/step-products'],
                'template' => $this->render('_step_link', ['i' => 'buildings']),
            ],
            [
                'label' => 'Услуги',
                'url' => ['/price-list/step-services'],
                'template' => $this->render('_step_link', ['i' => 'tools']),
            ],
            [
                'label' => 'Прайс-листы',
                'url' => ['/price-list/step-price-lists'],
                'template' => $this->render('_step_link', ['i' => 'price-list']),
            ],
        ],
    ]); ?>
</div>