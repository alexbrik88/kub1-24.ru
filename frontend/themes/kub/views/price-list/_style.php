<style>

    .total-sum.total-txt-foot {
        display: none;
    }

    /** INSTRUCTION **/

    /* blocks */
    .t-tile .title { min-height: 60px; font-size: 18px; color: #4679ae; }
    .t-tile .title > .img { float:left; }
    .t-tile .title > .txt {  }
    .t-tile .title > span {}
    .t-tile .list {}
    .t-tile .list ul { padding-left: 0; }
    .t-tile .list ul li { margin-left: 15px; }
    .st-tile { border-radius: 4px!important; border:1px solid #ddd; padding:20px;  position: relative; }
    .st-tile .title { font-size: 18px; color: #4679ae; }
    .st-tile .subtitle { width: 50%; font-size: 16px; }
    .bblock { margin: 0 -5px; }
    .bblock .col-3, .bblock .col-4, .bblock .col-6 { padding: 0 5px; }
    .subcaption { font-size:18px; font-weight: bold; }
    .st-tile { margin-bottom: 10px; line-height: 1.2; }
    .st-tile .ico { position: absolute; top:20px; left:20px; }
    .st-tile .title { margin-bottom: 20px; }
    .st-tile .ico + .title { margin-left: 55px; min-height: 40px; }
    .st-tile .subtitle { width: 100%; }
    .st-tile.small .subtitle {font-size: 12px; color: #001424; }
    .st-tile.medium .subtitle {font-size: 13px; color: #001424; }
    .st-tile .subtitle { font-size: 15px; }
    .st-tile .bottom-img { text-align: center; height: 150px; }
    .st-tile .bottom-img > img { display: block; width: 85.905%; position: absolute; }
    .st-tile .title-top-pad { padding-top: 9px; }
    .bg_1 {
        border: 1px solid #ddd;
        background-image: url(/img/price_list/man_flying_money.png);
        background-repeat: no-repeat;
        background-position: 100% 100%;
        height: calc(100% - 10px);
    }

    /** TARIFF */

    div.tariff-months-block .tmb-item {
        padding: 7px 0 0 0!important;
    }
    table.tariff-table button.tariff {
        margin-bottom: 15px;
    }
    .button-hover-transparent-blue {
        color: #4679ae;
        border-color: #4679ae;
    }
    .button-hover-transparent-blue:hover {
        color: #4679ae;
    }
    div.tariff-months-block .tmb-item .discount {
        padding: 1px 5px !important;
    }
    .tariff-price-td:first-child {
        padding-top: 32px!important;
    }

</style>