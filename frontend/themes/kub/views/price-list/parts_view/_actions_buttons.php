<?php

use frontend\rbac\permissions;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Html;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\product\PriceList;
 * @var $actualSubscription \common\models\service\Subscribe
 * @var $lastActiveSubscribe \common\models\service\Subscribe
 */

$canUpdate = Yii::$app->user->can(permissions\Product::CREATE);
$printUrl = ['print', 'id' => $model->id, 'filename' => $model->name.'.html'];
$sendUrl = Url::to(['/price-list/send', 'id' => $model->id]);
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate && !$model->is_deleted && !$model->is_archive) : ?>
                <?= \yii\helpers\Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && !$model->is_archive): ?>
                <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', $printUrl, [
                    'target' => '_blank',
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && !$model->is_archive): ?>
                <div class="dropup">
                    <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                        'class' => 'button-clr button-regular button-hover-transparent w-full no-after',
                        'data-toggle' => 'dropdown',
                    ]); ?>
                    <?= \yii\bootstrap4\Dropdown::widget([
                        'options' => [
                            'style' => '',
                            'class' => 'form-filter-list list-clr',
                        ],
                        'items' => [
                            [
                                'label' => '<span style="display: inline-block;">PDF</span> файл',
                                'encode' => false,
                                'url' => ['download', 'type' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName()],
                                'linkOptions' => [
                                    'target' => '_blank',
                                ]
                            ],
                            [
                                'label' => '<span style="display: inline-block;">Excel</span> файл',
                                'encode' => false,
                                'url' => ['download', 'type' => 'xls', 'id' => $model->id, 'filename' => $model->getPdfFileName()],
                                'linkOptions' => [
                                    'target' => '_blank',
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && !$model->is_archive): ?>
                <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => $this->render('//svg-sprite', ['ico' => 'copied']) . '<span>Копировать</span>',
                            'class' => 'button-clr button-regular button-hover-transparent w-full',
                        ],
                        'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                        'message' => 'Вы уверены, что хотите скопировать этот прайс-лист?',
                    ]); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($actualSubscription): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => (!$model->is_archive) ?
                            'В архив' :
                            'Из архива',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to([
                        'update-is-archive',
                        'id' => $model->id,
                        'value' => (!$model->is_archive) ? 1 : 0
                    ]),
                    'message' => (!$model->is_archive) ?
                        'Вы уверены, что хотите перенести прайс-лист в архив?' :
                        'Вы уверены, что хотите извлечь прайс-лист из архива?'
                ]); ?>
            <?php else: ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Из архива',
                        'class' => 'button-clr button-regular button-hover-transparent w-full'
                    ],
                    'confirmUrl' => Url::to([
                        '/subscribe'
                    ]),
                    'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                        'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                        'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            &nbsp;
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\Product::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
                    'message' => 'Вы уверены, что хотите удалить этот прайс-лист?',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>