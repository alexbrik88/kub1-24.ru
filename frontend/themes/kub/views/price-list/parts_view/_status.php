<?php

use common\components\date\DateHelper;
use common\models\product\PriceListNotification;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\product\PriceListStatus;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$statusIcon = [
    PriceListStatus::STATUS_CREATED => 'check-2',
    PriceListStatus::STATUS_SEND => 'envelope',
];
$statusColor = [
    PriceListStatus::STATUS_CREATED => '#26cd58',
    PriceListStatus::STATUS_SEND => '#FAC031',
];
$icon = ArrayHelper::getValue($statusIcon, $model->status_id);
$color = ArrayHelper::getValue($statusColor, $model->status_id);

if ($model->is_archive) {
    $color = '#666';
    $icon = 'archiev';
}

if ($model->discount == round($model->discount))
    $model->discount = round($model->discount);
if ($model->markup == round($model->markup))
    $model->markup = round($model->markup);
if ($model->discount_from_sum_percent == round($model->discount_from_sum_percent))
    $model->discount_from_sum_percent = round($model->discount_from_sum_percent);
if ($model->markup_from_sum_percent == round($model->markup_from_sum_percent))
    $model->markup_from_sum_percent = round($model->markup_from_sum_percent);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);

?>

    <div class="sidebar-title d-flex flex-wrap align-items-center">
        <div class="column flex-grow-1 mt-1 mt-xl-0">
            <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
                    background-color: <?= $color ?>;
                    border-color: <?= $color ?>;
                    color: #ffffff;
                    ">
                <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
                <span class="ml-3">
                    <?php
                    if ($model->is_archive)
                        echo 'В архиве';
                    else
                        echo $model->status->name
                    ?>                
                </span>
                <span class="ml-auto mr-1"><?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?></span>
            </div>
        </div>
    </div>

    <div class="about-card mb-3 mt-1">
        <div class="about-card-item">
            <span class="text-grey">Вид прайс-листа для клиента:</span>
            <br/>

            <?= Html::a($model->name,
                ['out-view', 'uid' => $model->uid],
                ['target' => '_blank']) ?>

            <?= Html::a($this->render('//svg-sprite', ['ico' => 'copied']) . Html::hiddenInput('link', Url::to(['out-view', 'uid' => $model->uid], true)), '#', [
                'title' => 'Копировать ссылку на прайс-лист',
                'aria-label' => 'Копировать ссылку на прайс-лист',
                'class' => 'copy-link',
                'style' => 'padding-left:6px',
                'data' => [
                    'pjax' => 0
                ]
            ]) ?>

        </div>
        <?php if ($model->has_discount): ?>
            <div class="about-card-item">
                <span class="text-grey">Скидка на весь товар:</span>
                <?= $model->discount ?>%
            </div>
        <?php endif; ?>
        <?php if ($model->has_markup): ?>
            <div class="about-card-item">
                <span class="text-grey">Наценка на весь товар:</span>
                <?= $model->markup ?>%
            </div>
        <?php endif; ?>
        <?php if ($model->has_discount_from_sum): ?>
            <div class="about-card-item">
                <span class="text-grey">Скидка при заказе на сумму:</span>
                <br/>
                <?= TextHelper::invoiceMoneyFormat($model->discount_from_sum) ?> ₽ / <?= $model->discount_from_sum_percent ?>%
            </div>
        <?php endif; ?>
        <?php if ($model->has_markup_from_sum): ?>
            <div class="about-card-item">
                <span class="text-grey">Наценка при заказе на сумму:</span>
                <br/>
                <?= TextHelper::invoiceMoneyFormat($model->markup_from_sum) ?> ₽ / <?= $model->markup_from_sum_percent ?>%
            </div>
        <?php endif; ?>
        <?php if ($model->auto_update_prices): ?>
            <div class="about-card-item">
                <span class="text-grey">Цены обновляются автоматически</span>
            </div>
        <?php endif; ?>
        <?php if ($model->auto_update_quantity): ?>
            <div class="about-card-item">
                <span class="text-grey">Остатки обновляются автоматически</span>
            </div>
        <?php endif; ?>
        <?php if ($model->use_notifications): ?>
            <div class="about-card-item">
                <span class="text-grey">Уведомления об открытии</span>
            </div>
        <?php endif; ?>

        <div class="about-card-item">
            <span class="text-grey">Уведомления в Telegram:</span>
            <?= ($model->notify_to_telegram ? 'ВКЛ.' : 'ВЫКЛ.') ?>
        </div>

        <?php if ($model->is_contractor_physical): ?>
            <div class="about-card-item">
                <span class="text-grey">Продавать только физ. лицам</span>
            </div>
        <?php endif; ?>
    </div>

    <div class="about-card mb-3 mt-1">
        <div class="about-card-item">
            <span style="font-weight: bold;">Комментарий</span>
            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                'id' => 'comment_internal_update',
                'style' => 'cursor: pointer;',
            ]); ?>
            <div id="comment_internal_view" style="margin-top:5px" class="">
                <?= Html::encode($model->comment_internal) ?>
            </div>
            <?php if ($canUpdate) : ?>
                <?= Html::beginTag('div', [
                    'id' => 'comment_internal_form',
                    'class' => 'hidden',
                    'style' => 'position: relative;',
                    'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
                ]) ?>
                <?= Html::tag('i', '', [
                    'id' => 'comment_internal_save',
                    'class' => 'fa fa-floppy-o',
                    'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
                ]); ?>
                <?= Html::textarea('comment_internal', $model->comment_internal, [
                    'id' => 'comment_internal_input',
                    'rows' => 3,
                    'maxlength' => true,
                    'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
                ]); ?>
                <?= Html::endTag('div') ?>
            <?php endif ?>
        </div>
    </div>

    <?php $viewedNotifications = PriceListNotification::find()->where(['price_list_id' => $model->id, 'is_viewed' => 1])->all() ?>
    <?php if ($viewedNotifications): ?>
    <div class="about-card mb-3 mt-1">
        <div class="about-card-item">
            <span class="text-grey">Прайс-лист был открыт:</span><br/>
            <?php /** @var PriceListNotification $notification */ ?>
            <?php foreach ($viewedNotifications as $notification): ?>
                <div><?= $notification->contractor ?
                    Html::a($notification->contractor->getShortName(), ['/contractor/view', 'type' => 2, 'id' => $notification->contractor_id]) :
                    $notification->contractor_email ?></div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}


$this->registerJs('

    function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).val()).select();
      document.execCommand("copy");
      $temp.remove();
    }

    $(".copy-link").bind("click", function(e) {
       e.preventDefault();
       var linkElement = $(this).find("input[name=\'link\']");
       copyToClipboard(linkElement);
    
       window.toastr.success("Ссылка скопирована в буфер обмена", "", {
            "closeButton": true,
            "showDuration": 500,
            "hideDuration": 1000,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            "escapeHtml": false,
        });
    });
');

?>