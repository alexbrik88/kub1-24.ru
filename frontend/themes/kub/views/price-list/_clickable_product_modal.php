<?php
use common\models\product\PriceList;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\Modal;
use yii\web\View;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $priceList PriceList
 * @var $priceListOrders \common\models\product\PriceListOrder[]
 * @var $showModalBtn string
 */

// for modal
$isDescription = $priceList->include_description_column;
$isImage = $priceList->include_image_column;


if ($isImage || $isDescription): ?>
    <script>

        const hasTouchscreen = 'ontouchstart' in window;

        function swipedetect(el, callback){

            var touchsurface = el,
                swipedir,
                startX,
                startY,
                distX,
                distY,
                threshold = 150, //required min distance traveled to be considered swipe
                restraint = 100, // maximum distance allowed at the same time in perpendicular direction
                allowedTime = 300, // maximum time allowed to travel that distance
                elapsedTime,
                startTime,
                handleswipe = callback || function(swipedir){}

            touchsurface.addEventListener('touchstart', function(e){
                var touchobj = e.changedTouches[0]
                swipedir = 'none'
                dist = 0
                startX = touchobj.pageX
                startY = touchobj.pageY
                startTime = new Date().getTime() // record time when finger first makes contact with surface
                e.preventDefault()
            }, false)

            touchsurface.addEventListener('touchmove', function(e){
                e.preventDefault() // prevent scrolling when inside DIV
            }, false)

            touchsurface.addEventListener('touchend', function(e){
                var touchobj = e.changedTouches[0]
                distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
                distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
                elapsedTime = new Date().getTime() - startTime // get time elapsed
                if (elapsedTime <= allowedTime){ // first condition for awipe met
                    if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                        swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
                    }
                    else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
                        swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
                    }
                }
                handleswipe(swipedir)
                e.preventDefault()
            }, false)
        }

        $(document).ready(function() {

            if (hasTouchscreen) {
                $('.pic-part.row').find('.product-description').each(function() {
                    $(this).parent().parent().append($(this));
                });
                $('.product-single-image .image > img').css({'max-height':'140px', 'width':'auto'});
                $('.product-many-image .image-small > img').css({'max-height':'140px', 'width':'auto'});

                let $arrows = $('#pricelist-image-modal .modal-arrow')
                ;
                swipedetect(document.getElementById('pic-desc-block'), function(swipedir){
                    if (swipedir === 'right') $arrows.filter('[data-move="left"]').click();
                    if (swipedir === 'left')  $arrows.filter('[data-move="right"]').click();
                });
                swipedetect(document.getElementById('pic-block'), function(swipedir){
                    if (swipedir === 'right') $arrows.filter('[data-move="left"]').click();
                    if (swipedir === 'left')  $arrows.filter('[data-move="right"]').click();
                });
                swipedetect(document.getElementById('desc-block'), function(swipedir){
                    if (swipedir === 'right') $arrows.filter('[data-move="left"]').click();
                    if (swipedir === 'left')  $arrows.filter('[data-move="right"]').click();
                });
            }
        });

        // filter by group
        $('#price-list-pjax').on('pjax:beforeSend', function (e, jqXHR, settings) {
            let group_id = null;
            let r = decodeURIComponent(settings.url).match(/\[product_group_id\]=(\d+)/);
            if (r)
                group_id = parseInt(r[1]);

            if (!group_id) {
                window.priceListOrder = window.origPriceListOrder;
            } else {
                window.priceListOrder = {};
                for (let key in origPriceListOrder) {
                    if (origPriceListOrder.hasOwnProperty(key)) {
                        if (group_id === origPriceListOrder[key].group_id) {
                            window.priceListOrder[key] = origPriceListOrder[key];
                        }
                    }
                }
            }
        });

        window.origPriceListOrder =
        window.priceListOrder = {};
        <?php foreach ($priceListOrders as $key => $priceListOrder): ?>
            <?php $images = [];
            // new theme images
            for ($i=1; $i<=4; $i++)
                if ($src = $priceListOrder->product->getPictureThumb($i, PriceList::IMAGE_THUMB_WIDTH, PriceList::IMAGE_THUMB_HEIGHT))
                    $images[] = urlencode($src);
            // old theme image
            if (empty($images)) {
                if ($src = $priceListOrder->product->getImageThumb(PriceList::IMAGE_THUMB_WIDTH, PriceList::IMAGE_THUMB_HEIGHT))
                    $images[] = urlencode($src);
            }

            ?>
            window.origPriceListOrder['order_<?=($key)?>'] =
            window.priceListOrder['order_<?=($key)?>'] = {
                'image': <?= json_encode($images) ?>,
                'name':  <?= json_encode((string)$priceListOrder->name) ?>,
                'group_id': <?= json_encode($priceListOrder->productGroup ? $priceListOrder->productGroup->id : 0) ?>,
                'group': <?= json_encode($priceListOrder->productGroup ? (string)$priceListOrder->productGroup->title : '---') ?>,
                'price': <?= json_encode((string)TextHelper::invoiceMoneyFormat($priceListOrder->price_for_sell)) ?>,
                'description': <?= json_encode((string)nl2br($priceListOrder->description)) ?>
            };
        <?php endforeach; ?>

        window.priceListToggleArrows = function(key) {
            let $modal = $('#pricelist-image-modal');
            let pOrderKeys = Object.keys(window.priceListOrder);
            let nextIndex = pOrderKeys[pOrderKeys.indexOf(key) + 1];
            let prevIndex = pOrderKeys[pOrderKeys.indexOf(key) - 1];

            if (nextIndex) {
                $modal.find('.modal-arrow[data-move="right"]').removeClass('hide');
            } else {
                $modal.find('.modal-arrow[data-move="right"]').addClass('hide');
            }
            if (prevIndex) {
                $modal.find('.modal-arrow[data-move="left"]').removeClass('hide');
            } else {
                $modal.find('.modal-arrow[data-move="left"]').addClass('hide');
            }
        }

        window.priceListShowImage = function(key) {

            let pOrder = window.priceListOrder[key];
            let $modal = $('#pricelist-image-modal');

            let isImage = <?= (int)$isImage ?>;
            let isDescription = <?= (int)$isDescription ?>;

            if (isImage && isDescription) {
                $modal.find('.block').hide().filter('.pic-desc-block').show();
            } else if (isImage) {
                $modal.find('.block').hide().filter('.pic-block').show();
            } else if (isDescription) {
                $modal.find('.block').hide().filter('.desc-block').show();
            } else {
                $modal.find('.block').hide();
                return;
            }
            
            if (pOrder.image.length > 0) {
                // one pic
                if (pOrder.image.length === 1) {
                    $modal.find('.product-single-image').show();
                    $modal.find('.product-many-image').hide();
                    $modal.find('.product-single-image img').attr('src', decodeURIComponent(pOrder.image[0]))
                    // many pics
                } else {
                    $modal.find('.product-many-image').show();
                    $modal.find('.product-single-image').hide();
                    for (i=0; i<=4; i++) {
                        if (pOrder.image[i])
                            $modal.find('.product-many-image .image-small[data-id="' + i + '"] img').attr('src', decodeURIComponent(pOrder.image[i])).show();
                        else
                            $modal.find('.product-many-image .image-small[data-id="' + i + '"] img').hide();
                    }
                }
                // no pic
            } else {
                $modal.find('.block').hide().end().find('.desc-block').show();
            }

            $modal.find('.product-image').html(pOrder.image);
            $modal.find('.product-name').html(pOrder.name);
            $modal.find('.product-group').html(pOrder.group);
            $modal.find('.product-price').html(pOrder.price);
            $modal.find('.product-description').html(pOrder.description);
            $modal.find('.modal-arrow').data('key', key);

            if (pOrder.description)
                $modal.find('.caption-description').show();
            else
                $modal.find('.caption-description').hide();
        };

        // show modal
        $(document).on('click', '<?=($showModalBtn)?>', function(e) {

            if ($(e.target).closest('td.td-no-clickable').length)
                return;

            let $modal = $('#pricelist-image-modal');
            let key = $(this).attr('data-key');

            priceListShowImage(key);
            priceListToggleArrows(key);

            $modal.modal('show');
        });

        // move left|right
        $(document).on('click', '#pricelist-image-modal .modal-arrow', function() {
            let $modal = $('#pricelist-image-modal');
            let key = $(this).data('key');
            let pOrderKeys = Object.keys(window.priceListOrder);
            let nextIndex = pOrderKeys[pOrderKeys.indexOf(key) + 1];
            let prevIndex = pOrderKeys[pOrderKeys.indexOf(key) - 1];

            if ($(this).data('move') === 'left' && prevIndex) {
                priceListShowImage(prevIndex);
                priceListToggleArrows(prevIndex);
            }
            if ($(this).data('move') === 'right' && nextIndex) {
                priceListShowImage(nextIndex);
                priceListToggleArrows(nextIndex);
            }
        });

    </script>

    <?php Modal::begin(['id' => 'pricelist-image-modal', 'toggleButton' => false]); ?>

    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>

    <div class="modal-arrow link cursor-pointer" data-move="left" data-key="" style="position:absolute; left:5px; top:27px; z-index:2; font-size:2rem">
        <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
    </div>
    <div class="modal-arrow link cursor-pointer" data-move="right" data-key="" style="position:absolute; right:5px; top:27px; z-index:2; font-size:2rem">
        <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
    </div>

    <div id="pic-desc-block" class="pic-desc-block block" style="display: none">
        <div class="pic-part row">
            <div class="col-6">
                <div class="product-single-image">
                    <div class="image"><img src=""/></div>
                </div>
                <div class="product-many-image" style="display: none">
                    <div style="width: 100%">
                        <div class="image-small" data-id="0"><img src=""/></div>
                        <div class="image-small" data-id="1"><img src=""/></div>
                    </div>
                    <div style="width: 100%">
                        <div class="image-small" data-id="2"><img src=""/></div>
                        <div class="image-small" data-id="3"><img src=""/></div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="product-name"></div>
                <div class="product-caption">Группа: </div><div class="product-group"></div><br/>
                <div class="product-caption">Цена: </div><div class="product-price"></div><div class="product-price-r">
                    <img width="15" src="/img/ruble.png"/>
                </div><br/>
                <div class="product-caption caption-description">Описание: </div><br/>
                <div class="product-description"></div>
            </div>
        </div>
    </div>

    <div id="desc-block" class="desc-block block" style="display: none">
        <div class="desc-part row" style="margin: 0">
            <div class="col-12">
                <div class="product-name"></div>
                <div class="product-caption">Группа: </div><div class="product-group"></div><br/>
                <div class="product-caption">Цена: </div><div class="product-price"></div><div class="product-price-r">
                    <img width="15" src="/img/ruble.png"/>
                </div><br/>
                <div class="product-caption caption-description">Описание: </div><br/>
                <div class="product-description"></div>
            </div>
        </div>
    </div>

    <div id="pic-block" class="pic-block block" style="display: none">
        <div class="pic-part row">
            <div class="col-6">
                <div class="product-single-image">
                    <div class="image"><img src=""/></div>
                </div>
                <div class="product-many-image" style="display: none">
                    <div style="width: 100%">
                        <div class="image-small" data-id="0"><img src=""/></div>
                        <div class="image-small" data-id="1"><img src=""/></div>
                    </div>
                    <div style="width: 100%">
                        <div class="image-small" data-id="2"><img src=""/></div>
                        <div class="image-small" data-id="3"><img src=""/></div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="product-name"></div>
                <div class="product-caption">Группа: </div><div class="product-group"></div><br/>
                <div class="product-caption">Цена: </div><div class="product-price"></div><div class="product-price-r">
                    <img width="15" src="/img/ruble.png"/>
                </div><br/>
            </div>
        </div>
    </div>

    <?php Modal::end(); ?>

<?php endif; ?>