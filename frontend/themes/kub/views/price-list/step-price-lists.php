<?php

use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\models\product\PriceListStatus;
use common\models\service\SubscribeTariffGroup;
use frontend\rbac\permissions;
use common\models\product\PriceList;

/* @var $actualSubscription \common\models\service\Subscribe */
/* @var $lastActiveSubscribe \common\models\service\Subscribe */

$this->title = 'Прайс-листы';

$emptyMessage = 'У вас пока не создано ни одного прайс-листа.';

$canDelete = Yii::$app->getUser()->can(permissions\Product::DELETE);
$canSend = FALSE; // todo: сделать модалку отправки; Yii::$app->getUser()->can(permissions\Product::UPDATE);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_price_list');
?>

<?= $this->render('_style') ?>
<?= $this->render('_steps', ['step' => 5, 'company' => $company]) ?>

<div class="stop-zone-for-fixed-elems">

        <div class="row mb-3">
            <div class="column">
                <h4 class="pt-2">Прайс-листы: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column ml-auto mb-2 pb-1">
                <?php if ($actualSubscription || !$lastActiveSubscribe): ?>
                    <?= Html::a(Icon::get('add-icon', ['class' => 'mr-1',]).' <span class="ml-2">Добавить</span>', [
                        'create-card',
                    ], [
                        'class' => 'button-clr button-regular button-regular_red pl-3 pr-3'
                    ]); ?>
                <?php else: ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<span class="ml-2">Добавить</span>',
                            'class' => 'button-width button-clr button-regular button-regular_red'
                        ],
                        'confirmUrl' => Url::to([
                            '/subscribe/default/index',
                        ]),
                        'confirmParams' => [
                            'tariff_group_id[]' => SubscribeTariffGroup::PRICE_LIST,
                        ],
                        'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                            'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                            'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>

    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_price_list']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= \yii\bootstrap4\Html::activeTextInput($searchModel, 'find_by', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list ' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', [
            'totalCount' => $dataProvider->totalCount,
            'scroll' => true,
        ]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::checkbox('PriceList[' . $model->id . '][checked]', false, [
                            'class' => 'joint-operation-checkbox',
                        ]) . Html::hiddenInput('price', 0, ['class' => 'price', 'disabled' => true]);

                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Дата',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'name',
                'label' => 'Название',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, ['view-card', 'id' => $model->id], ['class' => 'link', 'data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'comment_internal',
                'label' => 'Комментарий',
                'headerOptions' => [
                    'width' => '40%',
                ],
                'value' => function ($model) {
                    return $model->comment_internal ?: '';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($model) {
                    return ($model->is_archive) ? 'В архиве' : $model->status->name;
                },
                'format' => 'raw',
                'filter' => [
                    '' => 'Все',
                    PriceListStatus::STATUS_CREATED => 'Действующий',
                    PriceListStatus::STATUS_SEND => 'Отправлен',
                    PriceListStatus::STATUS_ARCHIVE => 'В архиве',
                ],
                's2width' => '150px'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'price-list',
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '6%',
                ],
                'contentOptions' => [
                    'class' => 'text-left'
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Icon::get('pencil'), ['/price-list/update-card', 'id' => $model->id], [
                            'class' => 'button-clr link ml-1',
                            'title' => 'Изменить',
                            'aria-label' => 'Изменить',
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'button-clr link ml-1',
                                'title' => 'Удалить',
                                'aria-label' => 'Удалить',
                                'data-pjax' => '0',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => Url::to(['/price-list/delete', 'id' => $model->id, 'productionType' => $model->production_type]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить прайс-лист?',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canSend ? Html::a(\frontend\components\Icon::get('envelope').' <span>Отправить</span>', 'javascript:void(0)', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-url' => Url::to(['many-send']),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php /*
<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['services'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
            ]) ?>
        </div>
    </div>
</div>*/ ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'title' => 'Вы уверены, что хотите удалить выбранные прайс-листы?',
        'id' => 'many-delete',
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'headerOptions' => [
            'class' => 'text-center'
        ],
        'options' => [
            'class' => 'confirm-modal fade'
        ],
    ]); ?>
    <div class="text-center">
        <?= \yii\bootstrap4\Html::a('Да', null, [
            'id' => 'confirm-many-delete',
            'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
            'data-url' => Url::to(['many-delete']),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
    </div>
    <?php Modal::end(); ?>
<?php endif; ?>