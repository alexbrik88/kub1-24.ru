<div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                <div class="text-center pad5">
                    <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                    <div>
                        <p>
                            Счет отправлен на
                            <br>
                            <strong id="send-to-email"></strong>.
                        </p>
                        <p>
                            Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                            <br>
                            <span>support@kub-24.ru</span>
                        </p>
                    </div>

                    <a class="button-list button-clr button-regular button-hover-transparent" data-dismiss="modal" style="color: #FFF;"> ОК </a>
                </div>
            </div>
        </div>
    </div>
</div>