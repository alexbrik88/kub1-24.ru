<?php

use common\models\service\PaymentType;
use yii\helpers\Html;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;

/** @var $tariff SubscribeTariff */
/** @var $tariffGroup SubscribeTariffGroup */

?>

<?php if ($tariff->price == 0): ?>
    <?= Html::a('ПОПРОБОВАТЬ', ['/price-list/step-company'], [
        'class' => 'tariff button-clr button-regular button-hover-transparent button-hover-transparent-blue w-full',
    ]) ?>
<?php else: ?>
    <?= Html::button('ОПЛАТИТЬ', [
        'class' => 'tariff tooltip2-pay button-clr button-regular button-regular_red w-full',
        'data-tooltip-content' => '#tooltip_pay_' . $tariff->id,
    ]) ?>
<?php endif; ?>

<div class="hidden">
    <div id="tooltip_pay_<?=$tariff->id?>" class="tooltip_pay_type"
         style="display: inline-block; text-align: center;">
        <div class="pay-tariff-wrap mt-2 mb-2">
            <strong>
                Тариф:
            </strong>
            <br>
            "<span class="pay-tariff-name"><?= $tariffGroup->name ?></span>"
        </div>
        <div class="pay-tariff-wrap mt-2 mb-2">
            <strong>
                Период:
            </strong>
            <br>
            <span class="pay-tariff-period"><?= $tariff->getReadableDuration() ?></span>
        </div>
        <div class="pay-tariff-wrap mt-2 mb-2">
            <strong>
                Количество прайс-листов:
            </strong>
            <br>
            <span class="pay-tariff-period"><?= $tariff->tariff_limit ?></span>
        </div>
        <div style="font-size: 16px; margin: 10px 0;  text-align: center;">
            Способ оплаты
        </div>
        <div class="">
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'id' => 'tariff-pay-form-'.$tariff->id.'-'.PaymentType::TYPE_ONLINE,
                'class' => 'tariff-group-payment-form mb-3',
                'style' => 'margin-right: 1px;'
            ]) ?>
            <?= Html::activeHiddenInput($model, 'tariffId', [
                'value' => $tariff->id,
            ]) ?>
            <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                'value' => PaymentType::TYPE_ONLINE,
            ]) ?>
            <?= Html::submitButton('Картой', [
                'class' => 'button-regular button-hover-transparent w-100 ladda-button',
            ]) ?>
            <div class="hidden submit-response-content"></div>
            <?= Html::endForm() ?>
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'id' => 'tariff-pay-form-'.$tariff->id.'-'.PaymentType::TYPE_INVOICE,
                'class' => 'tariff-group-payment-form mb-2',
                'style' => 'margin-left: 1px;'
            ]) ?>
            <?= Html::activeHiddenInput($model, 'tariffId', [
                'value' => $tariff->id,
            ]) ?>
            <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                'value' => PaymentType::TYPE_INVOICE,
            ]) ?>
            <?= Html::submitButton('Выставить счет', [
                'class' => 'button-regular button-hover-transparent w-100 ladda-button',
            ]) ?>
            <div class="hidden submit-response-content"></div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>
