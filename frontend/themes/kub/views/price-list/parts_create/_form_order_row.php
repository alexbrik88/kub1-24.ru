<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\product\Product;
use common\models\product\ProductCategory;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\ImageHelper;
use common\components\helpers\ArrayHelper;
/* @var \common\models\product\PriceList $model */
/* @var \common\models\product\PriceListOrder $order */
/* @var integer $number */
/* @var integer $precision */

$baseName = 'orderArray[' . $number . ']';
$isExist = Order::find()->where(['id' => $order->id])->exists();
$prodType = $order->productionType;
if ($order->quantity != intval($order->quantity)) {
    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
}

$quantityData = [];
$quantityStoresAll = 0;
$quantityByStores = $order->product->getQuantity();
if (!empty($quantityByStores)) {
    foreach ($quantityByStores as $storeId => $quantity) {
        $quantityData[] = 'data-store-' . $storeId . '="' . TextHelper::numberFormat($quantity, ($quantity!=(int)$quantity)? 3:0, '.') . '"';
        $quantityStoresAll += $quantity;
    }
}

$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
?>

<tr id="model_<?= $order->product_id; ?>" class="product-row" role="row">
    <td class="product-delete delete-column-left" style="white-space: nowrap;">

        <input type="hidden" class="product-count"
               name="<?= $baseName; ?>[count]" value="1"/>
        <input type="hidden" class="order-id"
               name="<?= $baseName; ?>[id]" value="<?= $order->id; ?>"/>
        <input type="hidden" class="product-id selected-product-id"
               name="<?= $baseName; ?>[product_id]" value="<?= $order->product_id; ?>"/>
        <input type="hidden" class="product-production-type"
               name="<?= $baseName; ?>[production_type]" value="<?= $order->production_type; ?>"/>

        <button class="remove-product-from-invoice button-clr" type="button">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
            </svg>
        </button>
        <button class="sortable-row-icon button-clr" type="button">
            <svg class="table-count-icon table-count-icon_small svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
            </svg>
        </button>

    </td>
    <td class="col_name" style="position: relative;">
        <input type="text" class="product-title form-control tooltip-product"
               style="padding-right: 25px; width: 100%;"
               name="<?= $baseName; ?>[title]"
               data-value = "<?= $order->name; ?>"
               value="<?= $order->name; ?>">
    </td>

    <!-- Category Fields -->
    <?php $productCategoryItems = $order->product->getCategoryFieldValues(ProductCategory::TYPE_BOOK_PUBLISHER); ?>
    <?php foreach ($productCategoryFields[ProductCategory::TYPE_BOOK_PUBLISHER] as $fieldId => $fieldName): ?>
        <td class="col_product_category_column_<?= $fieldId ?> <?= in_array($fieldId, $model->productCategoryColumns) ? '' : 'hidden' ?>" style="text-align: right">
            <?= ArrayHelper::getValue($productCategoryItems, $fieldId) ?: '---' ?>
        </td>
    <?php endforeach; ?>
    
    <td class="col_article <?= $model->include_article_column ? '' : 'hidden' ?>">
        <div class="order-param-value">
            <?= $order->article ?>
        </div>
    </td>
    <td class="col_group <?= $model->include_product_group_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-group">
            <?= $order->productGroup->title ?>
        </div>
    </td>
    <td class="col_weight <?= $model->include_weight_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-weight">
            <?= $order->product->weight ?>
        </div>
    </td>
    <td class="col_netto <?= $model->include_netto_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-netto">
            <?= $order->product->mass_net ?>
        </div>
    </td>
    <td class="col_brutto <?= $model->include_brutto_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-brutto">
            <?= $order->product->mass_gross ?>
        </div>
    </td>
    <td class="col_custom_field <?= $model->include_custom_field_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-custom_field">
            <?= $order->product->custom_field_value ?>
        </div>
    </td>
    <td class="col_quantity <?= $model->include_reminder_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-quantity" data-store-all="<?= $quantityStoresAll ?>" <?= implode(' ', $quantityData) ?>>
            <?= TextHelper::numberFormat($order->quantity, ($order->quantity != (int)$order->quantity)? 3:0, '.') ?>
        </div>
    </td>
    <td class="col_box_type <?= $model->include_box_type_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-box_type">
            <?= $order->product->box_type ?>
        </div>
    </td>
    <td class="col_count_in_package <?= $model->include_count_in_package_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-count_in_package">
            <?= $order->product->count_in_package ?>
        </div>
    </td>
    <?php /*
    <td class="col_comment <?= $model->include_description_column ? '' : 'hidden' ?>">
        <div class="order-param-value product-comment">
            <?= $order->description ?>
        </div>
    </td>*/ ?>
    <td class="col_unit_name">
        <div class="order-param-value product-unit-name">
            <?php echo $order->productUnit ? $order->productUnit->name : Product::DEFAULT_VALUE; ?>
        </div>
    </td>
    <td class="col_price <?= $model->include_price_column ? '' : 'hidden' ?> price-one">
        <?= Html::input('number', $baseName . '[price]', TextHelper::moneyFormatFromIntToFloat($order->price, $precision), [
            'class' => 'form-control price-input',
            'data-value' => TextHelper::moneyFormatFromIntToFloat($order->price, $precision),
            'min' => 0,
            'max' => Order::MAX_PRICE,
            'step' => 'any',
        ]); ?>
    </td>
    <td class="discount col_discount <?= $model->has_discount ? '' : 'hidden'; ?>">
        <div class="order-param-value product-discount">
            <span class="price-one-with-nds">
                <?= TextHelper::moneyFormatFromIntToFloat($order->price_for_sell, $precision) ?>
            </span>
            <?= Html::input('hidden', $baseName . '[discount]', $order->priceList->discount, [
                'class' => 'form-control discount-input',
                'data-value' => $order->priceList->discount
            ]); ?>
        </div>
    </td>
    <td class="markup col_markup <?= $model->has_markup ? '' : 'hidden'; ?>">
        <div class="order-param-value product-markup">
            <span class="price-one-with-nds">
                <?= TextHelper::moneyFormatFromIntToFloat($order->price_for_sell, $precision) ?>
            </span>
            <?= Html::input('hidden', $baseName . '[markup]', $order->priceList->markup, [
                'class' => 'form-control markup-input',
                'data-value' => $order->priceList->markup
            ]); ?>
        </div>
    </td>
</tr>
