<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\models\document\Invoice;
use common\models\document\NdsViewType;
use common\models\product\Product;
use common\models\product\ProductCategory;
use common\models\product\ProductField;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\ImageHelper;
use common\components\TextHelper;
use frontend\modules\documents\components\InvoiceHelper;
use common\models\product\PriceList;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */
/* @var $company common\models\Company */
/* @var $document string */

SortableAsset::register($this);

$serviceCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale(false)->count();
$goodsCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale(false)->count();
$productCount = $serviceCount + $goodsCount;

$priceOneCss = ($model->has_discount ? ' has_discount' : '') . ($model->has_markup ? ' has_markup' : '');
$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}

$productCategoryNames = Product::getCategoryNames();
$productCategoryFields = Product::getCategoryFieldNames();

$js = <<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: ".sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS;
$this->registerJs($js);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-need-product',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);
?>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
        'style' => 'margin-bottom: 0;',
    ],
]); ?>
<div style="position: relative">
<table id="table-for-invoice" class="table table-style table-count" style="margin-bottom: 0;">
    <thead>
    <tr class="heading" role="row">
        <th class="delete-column-left" width="5%" tabindex="0" rowspan="1" colspan="1">
        </th>
        <th class="col_name" width="30%" tabindex="0" rowspan="1" colspan="1">
            Наименование
        </th>
        <!-- Category Fields -->
        <?php foreach ($productCategoryFields[ProductCategory::TYPE_BOOK_PUBLISHER] as $fieldId => $fieldName): ?>
            <th class="col_product_category_column_<?= $fieldId ?> <?= in_array($fieldId, $model->productCategoryColumns) ? '' : 'hidden' ?>">
                <?= $fieldName ?>
            </th>
        <?php endforeach; ?>
        <th class="col_article <?= $model->include_article_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Артикул
        </th>
        <th class="col_group <?= $model->include_product_group_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Группа
        </th>
        <th class="col_weight <?= $model->include_weight_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Вес (кг)
        </th>
        <th class="col_netto <?= $model->include_netto_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Масса нетто
        </th>
        <th class="col_brutto <?= $model->include_brutto_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Масса брутто
        </th>
        <th class="col_custom_field <?= $model->include_custom_field_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            <?= $productField->title ?? 'Ваше название' ?>
        </th>
        <th class="col_quantity <?= $model->include_reminder_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Остаток
        </th>
        <th class="col_box_type <?= $model->include_box_type_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Вид упаковки
        </th>
        <th class="col_count_in_package <?= $model->include_count_in_package_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Количество в упаковке
        </th>
        <?php /*
        <th class="col_comment <?= $model->include_description_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Описание
        </th>*/ ?>
        <th class="col_unit_name" width="8%" tabindex="0" rowspan="1" colspan="1" style="min-width: 70px">
            Ед. измер-я
        </th>
        <th class="col_price <?= $model->include_price_column ? '' : 'hidden' ?>" width="15%" tabindex="0" rowspan="1" colspan="1" style="position: relative; min-width:122px">
            Цена
        </th>
        <th class="col_discount <?= $model->has_discount ? '' : 'hidden'; ?>" width="5%" tabindex="0" rowspan="1" colspan="1">
            Цена со скидкой
        </th>
        <th class="col_markup <?= $model->has_markup ? '' : 'hidden'; ?>" width="5%" tabindex="0" rowspan="1" colspan="1">
            Цена с наценкой
        </th>
    </tr>
    </thead>

    <tbody id="table-product-list-body" data-number="<?= count($model->priceListOrders) ?>">
    <?php if (count($model->priceListOrders) > 0 && $model->priceListType == PriceList::FOR_SELECTIVELY) : ?>
        <?php
        $sortAttr = $model->getSortAttributeName();
        $orders = $model->getPriceListOrders()->joinWith('productGroup')->orderBy([
            'production_type' => SORT_DESC,
            $sortAttr => SORT_ASC,
        ])->all();
        foreach ($orders as $key => $order) {
            echo $this->render('_form_order_row', [
                'price' => $model,
                'order' => $order,
                'number' => $key,
                'model' => $model,
                'precision' => 2,
                'productField' => $productField ?? null,
                'productCategoryNames' => $productCategoryNames,
                'productCategoryFields' => $productCategoryFields,
            ]);
        }
        ?>
    <?php endif; ?>
    <?= $this->render('_form_add_order_row', [
        'hasOrders' => count($model->priceListOrders) > 0,
        'serviceCount' => $serviceCount,
        'goodsCount' => $goodsCount,
        'productCount' => $productCount
    ]) ?>
    </tbody>

    <tfoot>
    <tr class="template disabled-row" role="row">
        <td class="product-delete delete-column-left" style="white-space: nowrap;">

            <input disabled="disabled" type="hidden" class="product-count" name="orderArray[][count]" value="1"/>
            <input disabled="disabled" type="hidden" class="order-id" name="orderArray[][id]" value=""/>
            <input disabled="disabled" type="hidden" class="product-id selected-product-id" name="orderArray[][product_id]" value=""/>
            <input disabled="disabled" type="hidden" class="product-production-type" name="orderArray[][production_type]" value=""/>

            <button class="remove-product-from-invoice button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
            <button class="sortable-row-icon button-clr" type="button">
                <svg class="table-count-icon table-count-icon_small svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                </svg>
            </button>

        </td>
        <td class="col_name">
            <input disabled="disabled" type="text" class="product-title form-control tooltip-product"
                   name="orderArray[][title]"
                   style="width: 100%;">
        </td>

        <!-- Category Fields -->
        <?php foreach ($productCategoryFields[ProductCategory::TYPE_BOOK_PUBLISHER] as $fieldId => $fieldName): ?>
            <td class="col_product_category_column_<?= $fieldId ?> <?= in_array($fieldId, $model->productCategoryColumns) ? '' : 'hidden' ?>" style="text-align: right">
                ---
            </td>
        <?php endforeach; ?>

        <td class="col_article <?= $model->include_article_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-article">&nbsp;</div>
        </td>
        <td class="col_group <?= $model->include_product_group_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-group-name">&nbsp;</div>
        </td>
        <td class="col_weight <?= $model->include_weight_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-weight">&nbsp;</div>
        </td>
        <td class="col_netto <?= $model->include_netto_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-netto">&nbsp;</div>
        </td>
        <td class="col_brutto <?= $model->include_brutto_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-brutto">&nbsp;</div>
        </td>
        <td class="col_custom_field <?= $model->include_custom_field_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-custom_field">&nbsp;</div>
        </td>
        <td class="col_quantity <?= $model->include_reminder_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-quantity" data-store-all="0">&nbsp;</div>
        </td>
        <td class="col_box_type <?= $model->include_box_type_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-box_type">&nbsp;</div>
        </td>
        <td class="col_count_in_package <?= $model->include_count_in_package_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-count_in_package">&nbsp;</div>
        </td>
        <?php /*
        <td class="col_comment <?= $model->include_description_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-comment">&nbsp;</div>
        </td>*/ ?>
        <td class="col_unit_name">
            <div class="order-param-value product-unit-name">&nbsp;</div>
        </td>
        <td class="col_price <?= $model->include_price_column ? '' : 'hidden' ?> price-one">
            <?= Html::input('number', 'orderArray[][price]', 0, [
                'class' => 'form-control price-input',
                'disabled' => 'disabled',
                'min' => 0,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="discount col_discount <?= $model->has_discount ? '' : ' hidden'; ?>">
            <div class="order-param-value product-discount">
                <?= Html::input('hidden', 'orderArray[][discount]', 0, [
                    'class' => 'discount-input'
                ]); ?>
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
        <td class="markup col_markup <?= $model->has_markup ? '' : ' hidden'; ?>">
            <div class="order-param-value product-markup">
                <?= Html::input('hidden', 'orderArray[][markup]', 0, [
                    'class' => 'markup-input'
                ]); ?>
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
    </tr>
    <tr class="button-add-line" role="row">
        <td class="delete-column-left" colspan="2" style="border-bottom: none; border-left: none; border-right: none; position: relative;display: table-cell;">
            <span class="btn-add-line-table button-regular button-hover-content-red mt-3 <?= !$productCount ? 'tooltip-need-product' : '' ?>" data-tooltip-content="#tooltip-need-product">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </span>
        </td>
    </tr>
    </tfoot>
</table>
<div style="display: none">
    <div id="tooltip-need-product">Сначала необходимо добавить товары/услуги</div>
</div>
</div>
<?php Pjax::end(); ?>