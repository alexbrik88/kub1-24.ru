<?php

use yii\helpers\Html;

?>
<!-- remove product -->
<div id="modal-remove-one-product" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить эту позицию из прайс-листа?
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-auto-update-prices" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Если поставите галочку, то цены изменятся <br/> и в уже отправленных прайс-листах.
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2 no',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>