<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\company\CheckingAccountant;
use common\models\EmployeeCompany;
use frontend\models\CashboxSearch;
use frontend\themes\kub\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CashboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => "{items}\n{pager}",

    'columns' => [
        [
            'attribute' => 'name',
            'header' => 'Название',
            'headerOptions' => [
            ],
        ],
        [
            'attribute' => 'currency_id',
            'label' => 'Валюта',
            'value' => function ($model) {
                return $model->currency->name;
            },
        ],
        [
            'attribute' => 'accessible',
            'class' => DropDownSearchDataColumn::class,
            'headerOptions' => [
            ],
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->accessible_to_all) {
                    return 'Всем';
                }
                $e = EmployeeCompany::find()->andWhere([
                    'employee_id' => $model->responsible_employee_id,
                    'company_id' => $model->company_id,
                ])->one();

                return $e ? $e->getFio(true) : '';
            },
        ],
        [
            'attribute' => 'is_accounting',
            'format' => 'boolean',
        ],
        [
            'label' => 'Тип',
            'value' => function ($model) {
                return $model->is_main ? 'Основная' : ($model->is_closed ? 'Закрыта' : 'Дополнительная');
            },
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'controller' => '/cashbox',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $data) {
                    return Html::a(Icon::get('pencil'), $url, [
                        'class' => 'link ajax-modal-btn',
                        'data-pjax' => '0',
                        'data-title' => 'Изменить кассу',
                        'title' => Yii::t('yii', 'Изменить'),
                        'aria-label' => Yii::t('yii', 'Изменить'),
                    ]);
                },
                'delete' => function ($url, $data) {
                    if ($data->is_main) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Основную кассу удалить нельзя'
                        ]);
                    } elseif (!$data->canDelete()) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Кассу нельзя удалить, т.к. есть операции по кассе.<br/>
                                Удалите операции, после можно удалить кассу',
                            'title-as-html' => 1
                        ]);
                    } else {
                        return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'link',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить кассу?',
                        ]);
                    }
                },
            ],
        ],
    ],
]);
