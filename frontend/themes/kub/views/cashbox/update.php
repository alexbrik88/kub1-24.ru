<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$this->title = 'Изменить Кассу: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кассы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cashbox-update">

    <?php if (ArrayHelper::getValue($_params_, 'success', false)) : ?>
        <?= Html::script('
                if ($("#cashbox-pjax-container").length) {
                    $.pjax.reload("#cashbox-pjax-container", {timeout: 10000});
                    $(".modal.show").modal("hide");
                } else {
                    $(".modal.show").modal("hide");                
                    location.href = location.href;
                }
            ', ['type' => 'text/javascript']); return; ?>
    <?php endif ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
