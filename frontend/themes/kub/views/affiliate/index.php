<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Autoinvoice;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\ContractorSearch;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\rbac\permissions\document\Invoice;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use frontend\widgets\TableViewWidget;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;
use yii\bootstrap4\Modal;
use common\models\Company;
use common\components\ImageHelper;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\widgets\Pjax;
use common\models\employee\EmployeeClick;
use yii\bootstrap\Dropdown;
use common\components\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $searchModel frontend\models\ContractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */
/* @var $prompt backend\models\Prompt */
/* @var $company Company */
/* @var $user Employee */

$user = Yii::$app->user->identity;
$company = $user->company;

$this->title = $company->companyType->name . ' ' . $company->name_short;

$tabFile = ArrayHelper::remove($tabData, 'tabFile');
$tab = isset($tab) ? $tab : "clients";

?>

<div class="affiliate-index">

    <div class="wrap d-flex p-1 mt-0 mb-1">
        <div class="col-12 pt-1 pb-1 pl-1 pr-1">
            <div class="col-9 pl-1 pr-0 text-left">
                <h4 class="pr-3 pl-0 mt-1 pb-0 pull-left">Кабинет партнера</h4>
                <?= Html::button('Инструкция', [
                    'class' => 'button-regular button-hover-transparent button-width pull-left',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'help',
                ]); ?>
            </div>
            <div class="col-3 mr-0 pr-0 pull-right">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>

    <?= Nav::widget([
        'items' => [
            [
                'label' => 'Клиенты',
                'url' => ['index', 'tab' => 'clients'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ($tab == 'clients' ? ' active' : '')],
                'active' => $tab == 'clients'
            ],
            [
                'label' => 'Счета',
                'url' => ['index', 'tab' => 'invoice'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ((!$tab || $tab == 'invoice') ? ' active' : '')],
                'active' => $tab == 'invoice',
            ],
            [
                'label' => 'Выплаты',
                'url' => ['index', 'tab' => 'reward-payments'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ($tab == 'reward-payments' ? ' active' : '')],
                'active' => $tab == 'reward-payments'
            ],
            [
                'label' => 'Реферальные ссылки',
                'url' => ['index', 'tab' => 'affiliate-link'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ($tab == 'affiliate-link' ? ' active' : '')],
                'active' => $tab == 'affiliate-link'
            ],
            [
                'label' => '%% Вознаграждения',
                'url' => ['index', 'tab' => 'rewards'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ($tab == 'rewards' ? ' active' : '')],
                'active' => $tab == 'rewards'
            ],
            [
                'label' => 'Потенциальные клиенты',
                'url' => ['index', 'tab' => 'leads'],
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link' . ($tab == 'leads' ? ' active' : '')],
                'active' => $tab == 'leads'
            ],
        ],
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100'],
    ]); ?>

    <?php if ($tabFile) : ?>
        <div class="tab-content">
            <div id="tab1" class="tab-pane invoice-tab active">
                <div class="pt-2">
                    <div class="pt-1">
                        <?= $this->render("{$tabFile}", $tabData); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<?= $this->render('_partials/help-panel'); ?>