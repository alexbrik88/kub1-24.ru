<?php

use common\widgets\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\web\View;

Modal::begin([
    'id' => 'modal_approve_agreement',
    'closeButton' => false,
    'toggleButton' => false,
]);

?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]); ?>

    <div class="modal-title">
        <h4>Кабинет партнера</h4>
    </div>
    <button type="button" class="modal-close button-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="form-group">
            <p>Для продолжения работы в партнерской программе сервиса КУБ24, примите
                <?= Html::a('условия оферты', 'https://kub-24.ru/TermsOfUsePartners/TermsOfUsePartners.pdf', ['target' => '_blank']); ?>
            </p>
        </div>
        <div class="form-group">
            <?= Html::checkbox('is_approve_agreement', false, [
                'id' => 'approve_status',
                'label' => 'Я принимаю условия оферты',
            ]); ?>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('<span class="ladda-label">Принять</span><span class="ladda-spinner"></span>', [
                'class' => 'tooltip3 disabled approve_offer button-regular button-width button-regular_red button-clr',
                'data-tooltip-content' => '#approve_offer_tooltip'
            ]); ?>
            <button type="button" class="button-clr button-close button-width button-regular button-hover-transparent"
                    data-dismiss="modal">Отменить
            </button>
        </div>
    </div>
<?php Modal::end(); ?>

<div style="display: none">
    <span id="approve_offer_tooltip">
        Чтобы продолжить дальше, нужно согласится с условиями оферты.
    </span>
</div>

<?php

$this->registerJs(<<<JS
    $('.approve_offer').click(function(e) {
        e.preventDefault();
        var approve_offer = $('#uniform-approve_status').find('.checked').length;

        if (!approve_offer) {
            return false;
        }
        
        $.post('/affiliate/offer-approvement', {'offer': approve_offer}, function(data) {
            if ('true' === data.success) {
                window.location = '/affiliate/index';
            }
        }, 'json');
        
    });

    $(document).on('click', '#uniform-approve_status', function(e) {
        var isChecked = $('#uniform-approve_status').find('.checked').length;
        
        if (isChecked) {
            $('.approve_offer').removeClass('disabled');
            $('#approve_offer_tooltip').text('Нажмите принять, чтобы принять условия оферты.');
        } else {
            $('.approve_offer').addClass('disabled');
            $('#approve_offer_tooltip').text('Чтобы продолжить дальше, нужно согласится с условиями оферты.');
        }
    });

    $('.button-close').click(function(e){
        e.preventDefault();
        var referrer = document.referrer;
        
        window.location = referrer ? referrer : '/';
    })

    $('#modal_approve_agreement').modal('show');
JS, View::POS_READY
);