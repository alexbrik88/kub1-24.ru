<?php
use common\models\company\CompanyAffiliateSearch;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $color string
 * @var $isMoney boolean
 * @var $array array
 * @var $status integer
 * @var $sum string
 * @var $count integer
 */
?>

<div class="col-6 col-xl-3">
    <div class="count-card count-card_<?= $color; ?> wrap">
        <div class="count-card-main mb-1">
            <span class="details-sum"
                  <?= $isMoney ? 'data-value="'.$sum.'"' : '' ?>><?= $isMoney ? TextHelper::invoiceMoneyFormat($sum, 2) . ' ₽' : intval($sum); ?>
            </span>
        </div>
        <div class="count-card-title"><?= $array[$status]; ?></div>
        <hr />
        <div class="count-card-foot"><?= isset($countLabel) ? $countLabel . ': ' : ''; ?><?= isset($count) ? $count : '&nbsp;'; ?></div>
    </div>
</div>
