<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyAffiliateLinkSearch;
use common\models\service\ServiceModule;
use common\widgets\Modal;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\web\View;

/** @var ActiveDataProvider $dataProvider */
/** @var CompanyAffiliateLinkSearch $searchModel */

?>
<div class="table-settings row mt-0">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_affiliate_affiliate_links']) ?>
    </div>
    <div class="col-6 text-right">
        <?= Html::button('<svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                          </svg>
                          <span>Добавить</span>',
                [
                    'class' => 'button-regular button-regular_red button-width ml-auto create_affiliate_link',
                ]
        ); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'options' => [
        'class' => 'overflow-x',
    ],
    'tableOptions' => [
        'class' => 'table table-style table-count-list affiliate-clients-table',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],

    'columns' => [
        [
            'attribute' => 'creationDate',
            'label' => 'Дата создания',
            'headerOptions' => [
                'width' => '15%',
            ],
            'value' => function (CompanyAffiliateLink $model) {
                return DateHelper::format(
                    date(DateHelper::FORMAT_DATE, $model->created_at),
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                );
            },
        ],
        [
            'attribute' => 'product',
            'label' => 'Продукт',
            'headerOptions' => [
                'width' => '15%',
            ],
            'value' => function (CompanyAffiliateLink $model) {
                return ServiceModule::$serviceModuleLabel[$model->service_module_id];
            },
        ],
        [
            'attribute' => 'link',
            'label' => 'Ссылка для ваших клиентов',
            'headerOptions' => [
                'width' => '40%',
            ],
            'format' => 'html',
            'value' => function (CompanyAffiliateLink $model) {
                $link = Yii::$app->params['siteModules'][$model->service_module_id] . '?req=';

                return Html::tag('span', $link . $model->link, [
                    'class' => 'clipboard-affiliate-link-id-' . $model->id,
                ]);
            },
        ],
        [
            'attribute' => 'countAffiliateCompanies',
            'label' => 'Количество регистраций',
            'headerOptions' => [
                'width' => '20%',
            ],
            'format' => 'html',
            'value' => function (CompanyAffiliateLink $model) {
                return $model->countAffiliateCompanies ?: 'Нет приглашенных клиентов';
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '10%',
            ],
            'format' => 'html',
            'value' => function (CompanyAffiliateLink $model) {
                return $model->name ?: '-';
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{copy}',
            'buttons' => [
                'copy' => function ($url, $data) {
                    return Html::a('Копировать', '#copy', [
                        'title' => 'Копировать',
                        'class' => 'edit-affiliate-link-copy link pl-1 pr-1',
                        'data' => [
                            'affiliate-link-id' => $data['id']
                        ],
                    ]);
                },
            ],
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{edit}',
            'buttons' => [
                'edit' => function ($url, $data) {
                    return Html::a(Icon::get('pencil'), '#edit', [
                        'title' => 'Редактировать',
                        'class' => 'update_affiliate_link link pl-1 pr-1 ' . (!$data['name'] ? 'hidden' : ''),
                        'data' => [
                            'affiliate-link-id' => $data['id']
                        ],
                    ]);
                },
            ],
        ],
    ],
]); ?>

<?php Modal::begin([
    'id' => 'modal_update_link',
    'header' => '<h4>Добавить ссылку</h4>',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<?= $this->render('../_edit_links_form', [
    'model' => (new CompanyAffiliateLink()),
    'products' => ServiceModule::$serviceModuleLabel,
]); ?>

<?php Modal::end(); ?>

<?php

$this->registerJs(<<<JS
    $(document).on('click', '.edit-affiliate-link-copy', function(e) {
        e.preventDefault();
        
        var id = $(this).data('affiliate-link-id'),
            span = $('.clipboard-affiliate-link-id-' + id),
            textarea = $('<textarea id="clipboard"></textarea>'),
            body = $('body');
        
        textarea.html(span.text());
        body.append(textarea);

        textarea.select();

        document.execCommand('copy');

        $('#clipboard').remove();
        
        window.toastr.success('Партнерская ссылка скопирована', "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
    });

    $('.create_affiliate_link').on('click', function(e) {
        e.preventDefault();
        
        $.pjax({
            url: 'create-custom-link',
            container: '#modal-item-pjax',
            push: false,
            timeout: 5000
        })
        
        $(document).on("pjax:success", function() {
            $('#modal_update_link').modal('show');
        });
    });
    
    $('.update_affiliate_link').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data('affiliate-link-id');
        
        $.pjax({
            url: 'update-affiliate-link',
            method: 'POST',
            data: {'id': id},
            container: '#modal-item-pjax',
            push: false,
            timeout: 5000
        })
        
        $(document).on("pjax:success", function() {
            $('.modal-header h4').html('Редактировать ссылку');
            $('#modal_update_link').modal('show');
        });
    });
JS, View::POS_READY);