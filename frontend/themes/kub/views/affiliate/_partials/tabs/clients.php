<?php

use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\ActiveSubscribe;
use common\models\company\CompanyAffiliateSearch;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\components\date\DateHelper;
use common\models\service\ServiceModule;
use common\models\service\Subscribe;
use frontend\components\StatisticPeriod;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\db\Expression;
use yii\widgets\ActiveForm;

/** @var $this yii\web\View */
/** @var Company $model */
/** @var CompanyAffiliateSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$user = Yii::$app->user->identity;
$model = $company = $user->company;

$period = StatisticPeriod::getSessionName();

$exists = Company::find()->andWhere(['invited_by_company_id' => $company->id])->exists();

if ($exists) {
    $emptyMessage = "В выбранном периоде «{$period}», у вас нет привлеченных клиентов. Измените период, чтобы увидеть привелеченных клиентов.";
} else {
    $emptyMessage = 'У вас пока нет привлеченных клиентов.';
}

?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]); ?>

<div class="wrap wrap_count">
    <div class="row">
        <?= $this->render('dashboard_block', [
            'color' => 'yellow',
            'array' => CompanyAffiliateSearch::$payedStatusArray,
            'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE,
            'isMoney' => false,
            'sum' => $searchModel->getBaseQuery()
                ->groupBy('`company`.`id`')
                ->count(),
        ]); ?>
        <?= $this->render('dashboard_block', [
            'color' => 'turquoise',
            'array' => CompanyAffiliateSearch::$payedStatusArray,
            'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_GET_INVOICE,
            'isMoney' => false,
            'sum' => $searchModel->getBaseQuery()
                    ->joinWith('invoices', true, 'INNER JOIN')
                    ->andWhere(['and',
                        ['IS NOT', Payment::tableName() . '.id', new Expression('NULL')],
                        [Invoice::tableName() . '.is_deleted' => 0],
                    ])
                    ->groupBy('`company`.`id`')
                    ->count(),
        ]); ?>
        <?= $this->render('dashboard_block', [
            'color' => 'red',
            'array' => CompanyAffiliateSearch::$payedStatusArray,
            'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_INVOICE_PAYED,
            'isMoney' => false,
            'sum' => $searchModel->getBaseQuery()
                ->joinWith('invoices', true, 'INNER JOIN')
                ->andWhere(['and',
                    ['is_confirmed' => 1],
                ])
                ->groupBy('`company`.`id`')
                ->count(),
            ]); ?>
        <?= $this->render('dashboard_block', [
            'color' => 'green',
            'array' => CompanyAffiliateSearch::$payedStatusArray,
            'status' => CompanyAffiliateSearch::SUBSCRIBE_AFFILIATE_PAYED,
            'isMoney' => true,
            'sum' => $searchModel->getBaseQuery()
                    ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                    ->sum('paidInvoicesSum') * 100,
            'count' => $searchModel->getBaseQuery()
                ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                ->count(),
        ]); ?>
    </div>
</div>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_affiliate_affiliate_clients']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'action' => ['index', 'tab' => 'clients'],
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'options' => [
        'class' => 'overflow-x',
    ],
    'tableOptions' => [
        'class' => 'table table-style table-count-list affiliate-clients-table',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'rowOptions' =>
        function ($data) {
            /* @var $data Company */
            return ['class' => ($data->blocked == Company::BLOCKED) ? 'danger' : ''];
        },
    'columns' => [
        [
            'attribute' => 'registration_date',
            'label' => 'Дата регистрации',
            'value' => function (Company $model) {
                return DateHelper::format(date(DateHelper::FORMAT_DATE, $model->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'name_short',
            'value' => function (Company $model) {
                $companyName = trim($model->name_short);
                return $companyName ? $model->companyType->name . ' ' . $companyName : 'Не заполнено';
            },
        ],
        [
            'attribute' => 'email',
            'format' => 'raw',
            'value' => function (Company $model) {
                $companyEmail = substr($model->email, 0, mb_strpos($model->email, '@'));
                $tooltipster = Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip2',
                    'data-tooltip-content' => '#tooltip_email',
                ]);
                return Html::tag('div', Html::encode($companyEmail) . ' ' . $tooltipster, [
                    'style' => 'min-width: 120px',
                ]);
            },
        ],
        [
            'attribute' => 'product',
            'label' => 'Продукт',
            's2width' => '200px',
            'filter' => $searchModel->getAffiliateProductFilter(),
            'value' => function (Company $model) {
                return ServiceModule::$serviceModuleLabel[
                    ServiceModule::$serviceModuleArray[$model->product]
                ];
            },
        ],
        [
            'attribute' => 'activeTariff',
            'label' => 'Тарифный план',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'style' => 'min-width: 150px',
            ],
            's2width' => '200px',
            'filter' => $searchModel->getTariffFilter(),
            'format' => 'raw',
            'value' => function (Company $model) {
                /** @var Subscribe[] $activeTariffs */
                $activeTariffs = $model->activeSubscribes;
                $content = "";

                /** @var Subscribe $activeTariff */
                foreach ($activeTariffs as $activeTariff) {
                    $content .= $activeTariff->tariffGroup->name;
                    $content .= ' ';
                    $content .= $activeTariff->getTariffName();
                    $content .= '; ';
                }

                return $content ? Html::encode($content) : 'Нет';
            },
        ],
        [
            'attribute' => 'paidCount',
            'label' => 'Количество счетов',
            'value' => function (Company $model) {
                return $model->paidCount ?: '-';
            },
        ],
        [
            'attribute' => 'paidSum',
            'label' => 'Сумма счетов',
            'headerOptions' => [
                'style' => 'min-width: 100px'
            ],
            'value' => function (Company $model) {
                return TextHelper::invoiceMoneyFormat($model->paidSum * 100, 2);
            },
        ],

        [
            'attribute' => 'paidInvoicesCount',
            'label' => 'Количество оплат',
            'headerOptions' => [
                'style' => 'min-width: 80px'
            ],
            'value' => function (Company $model) {
                return $model->getPayments()->andWhere(['is_confirmed' => 1])->count() ?: '-';
            },
        ],

        [
            'attribute' => 'paidInvoicesSum',
            'label' => 'Сумма оплат',
            'headerOptions' => [
                'style' => 'min-width: 120px'
            ],
            'value' => function (Company $model) {
                $sum = $model->getPayments()->andWhere(['is_confirmed' => 1])->sum('sum');
                return TextHelper::invoiceMoneyFormat($sum * 100, 2);
            },
        ],
    ],
]); ?>


<div class="tootip-template" style="display: none">
    <span id="tooltip_email" style="display: inline-block; text-align: center;">Согласно закону о защите персональных данных email отображается не полностью</span>
</div>