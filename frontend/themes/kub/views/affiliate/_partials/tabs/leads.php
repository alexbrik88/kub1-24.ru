<?php

use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\company\CompanyAffiliateLeads;
use common\models\company\CompanyAffiliateLeadsSearch;
use common\components\date\DateHelper;
use common\widgets\Modal;
use frontend\models\company\CompanyInviteForm;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;

/** @var $this View */
/** @var $searchModel CompanyAffiliateLeadsSearch */
/** @var $dataProvider ActiveDataProvider */

$emptyMessage = 'У вас пока нет ни одного потенциального клиента. Чтобы добавить первого клиента нажмите "Добавить"';

?>

<div class="table-settings row row_indents_s">
    <div class="col-6 pt-2">
        <?= TableViewWidget::widget(['attribute' => 'table_affiliate_affiliate_clients']) ?>
    </div>
    <div class="col-6 pt-2">
        <div class="form-group pull-right">
            <?= Html::button('Добавить', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red lead-update',
            ]) ?>
        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'options' => [
        'class' => 'overflow-x',
    ],
    'tableOptions' => [
        'class' => 'table table-style table-count-list affiliate-clients-table',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'columns' => [
        [
            'attribute' => 'date',
            'value' => function (CompanyAffiliateLeads $model) {
                return DateHelper::format(date(DateHelper::FORMAT_DATE, $model->date), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'name',
            'value' => function (CompanyAffiliateLeads $model) {
                return $model->name;
            },
        ],
        [
            'attribute' => 'username',
            'headerOptions' => [
                'width' => '15%',
            ],
            'value' => function (CompanyAffiliateLeads $model) {
                return $model->username;
            },
        ],
        [
            'attribute' => 'email',
            'value' => function (CompanyAffiliateLeads $model) {
                return $model->email;
            },
        ],
        [
            'attribute' => 'phone',
            'headerOptions' => [
                'width' => '15%',
            ],
            'value' => function (CompanyAffiliateLeads $model) {
                return $model->phone ?: '-';
            },
        ],
        [
            'attribute' => 'comment',
            'headerOptions' => [
                'width' => '20%',
            ],
            'value' => function (CompanyAffiliateLeads $model) {
                return $model->comment ?: '-';
            },
        ],
        [
            'attribute' => 'send',
            'label' => 'Отправить приглашение',
            'format' => 'raw',
            'value' => function(CompanyAffiliateLeads $model) {
                return Html::button('Отправить', [
                    'class' => 'button-regular button-regular_padding_bigger button-hover-content-red lead-invite',
                    'data' => [
                        'id' => $model->id,
                    ]
                ]);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), $url, [
                        'title' => Yii::t('yii', 'Update'),
                        'class' => 'link lead-update',
                        'data-title' => 'Редактировать клиента',
                        'data-id' => $model->id,
                    ]);
                },
                'delete' => function ($url, $model) {
                    return ConfirmModalWidget::widget([
                        'theme' => 'gray',
                        'toggleButton' => [
                            'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                            'class' => 'button-clr link',
                            'tag' => 'button',
                        ],
                        'confirmUrl' => Url::to([$url, 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить операцию?',
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                return Url::to(["lead-{$action}"]);
            },
        ],
    ],
]); ?>

<?php

Modal::begin([
    'header' => '<h4>Добавить клиента</h4>',
    'id' => 'modal-lead',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);

echo $this->render('../_edit_leads_form', [
    'model' => (new CompanyAffiliateLeads()),
]);

Modal::end(); ?>


<?php


echo $this->render('../send-invite-panel', [
    'model' => (new CompanyInviteForm())
]);

$this->registerJs(<<<JS
    $('.lead-update').click(function(e){
        e.preventDefault();
        var id = $(this).data('id'),
            lead = $('#modal-lead'),
            header = "";
        
        $.pjax({
            'url': id ? 'lead-update' : 'lead-create',
            'type': 'post',
            'data': {'id': id},
            'container': '#modal-item-pjax',
            'push': false
        });
        
        header = id ? "Редактировать клиента" : "Добавить клиента";
        
        $(document).on('pjax:complete', '#modal-item-pjax', function (e) {
            lead.find('.modal-header h4').html(header);
            lead.modal('show');
        });
    })
    
    $('.lead-invite').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        
        $.pjax({
            'url': 'get-invite-lead-form',
            'type': 'post',
            'data': {'id': id},
            'container': '#modal-send-invite-pjax',
            'push': false,
            'timeout': 5000
        });
        
        $(document).on('pjax:complete', '#modal-send-invite-pjax', function (e) {
            $('[data-id="send-invite"]').toggleClass('visible show');
        });
    });
JS, View::POS_READY
);

