<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\company\CompanyAffiliateLeadRewards;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyAffiliateLinkSearch;
use common\models\service\ServiceModule;
use common\widgets\Modal;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\web\View;

/** @var ActiveDataProvider $dataProvider */
/** @var CompanyAffiliateLinkSearch $searchModel */

?>
<div class="wrap wrap_padding_none">
    <div class="row">
        <div class="col-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => "Вознаграждения не установлены",
                'tableOptions' => [
                    'class' => 'table table-style table-count-list affiliate-links-table',
                ],
                'rowOptions' => function () {
                    return [];
                },
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "<div class=\"scroll-table-wrapper\">{items}</div>\n{pager}",
                'columns' => [
                    [
                        'attribute' => 'rewards_range',
                        'label' => 'Общая сумма оплат (от начала сотрудничества)',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (CompanyAffiliateLeadRewards $model) {
                            $bottom = $model->bottom_line ?: '';
                            $top = $model->top_line ?: '';
                            return ($model->bottom_line ? ' от ' : ' до ')
                                . (is_numeric($bottom) ? TextHelper::invoiceMoneyFormat($bottom * 100, 2) : $bottom)
                                . ($model->top_line && $model->bottom_line ? ' до ' : '')
                                . (is_numeric($top) ? TextHelper::invoiceMoneyFormat($top * 100, 2) : $top);
                        },
                    ],
                    [
                        'attribute' => 'rewards',
                        'label' => 'Размер партнерского вознаграждения, %',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (CompanyAffiliateLeadRewards $model) {
                            return $model->rewards;
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>