<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyInvoiceSearch;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\StoreOutInvoiceTariff;
use frontend\components\StatisticPeriod;
use frontend\widgets\TableViewWidget;
use php_rutils\RUtils;
use yii\web\View;
use yii\widgets\ActiveForm;

/** @var CompanyInvoiceSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$user = Yii::$app->user->identity;
$model = $company = $user->company;

$period = StatisticPeriod::getSessionName();

$exists = Company::find()->andWhere(['invited_by_company_id' => $company->id])->exists();

if ($exists) {
    $emptyMessage = "В выбранном периоде «{$period}», у вас нет привлеченных клиентов. Измените период, чтобы увидеть привелеченных клиентов.";
} else {
    $emptyMessage = 'У вас пока нет привлеченных клиентов.';
}

?>


<div class="wrap wrap_count">
    <div class="row">
        <?= $this->render('dashboard_block_extended', [
            'color' => 'yellow',
            'array' => CompanyInvoiceSearch::$subscribeStatusArray,
            'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_CREATED,
            'isMoney' => true,
            'sum' => $searchModel->getBaseQuery()
                ->andWhere(['is_confirmed' => 0])
                ->sum('total_amount_with_nds'),
            'countLabel' => 'Количество счетов',
            'count' => $searchModel->getBaseQuery()
                ->andWhere(['is_confirmed' => 0])
                ->count(),
        ]); ?>
        <?= $this->render('dashboard_block_extended', [
            'color' => 'red',
            'array' => CompanyInvoiceSearch::$subscribeStatusArray,
            'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE,
            'isMoney' => true,
            'sum' => $searchModel->getBaseQuery()
                ->andWhere(['and',
                    ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                    ['is_confirmed' => 0],
                ])
                ->sum('total_amount_with_nds'),
            'countLabel' => 'Количество счетов',
            'count' => $searchModel->getBaseQuery()
                ->andWhere(['and',
                    ['>', 'DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(`' . Payment::tableName() . '`.`created_at`)))', 10],
                    ['is_confirmed' => 0],
                ])
                ->count(),
        ]); ?>
        <?= $this->render('dashboard_block_extended', [
            'color' => 'turquoise',
            'array' => CompanyInvoiceSearch::$subscribeStatusArray,
            'status' => CompanyInvoiceSearch::SUBSCRIBE_STATUS_PAYED,
            'isMoney' => true,
            'sum' => $searchModel->getBaseQuery()
                ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                ->sum('total_amount_with_nds'),
            'countLabel' => 'Количество счетов',
            'count' => $searchModel->getBaseQuery()
                ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                ->count(),
        ]); ?>
        <?= $this->render('dashboard_block_extended', [
            'color' => 'green',
            'array' => CompanyInvoiceSearch::$subscribeStatusArray,
            'status' => CompanyInvoiceSearch::SUBSCRIBE_REWARDS,
            'isMoney' => true,
            'sum' => $searchModel->getBaseQuery()
                ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                ->sum('`rewards_amount`'),
            'countLabel' => 'Количество',
            'count' => $searchModel->getBaseQuery()
                ->andWhere([Payment::tableName() . '.is_confirmed' => 1])
                ->count(),
        ]); ?>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_affiliate_affiliate_invoice']) ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'action' => ['index', 'tab' => 'invoice'],
            'method' => 'GET',
            'fieldConfig' => [
                'template' => "{input}\n{error}",
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ],
        ]); ?>
        <div class="form-group flex-grow-1 col-10 pull-left">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск по ID, названию, ИНН или E-mail',
                'class' => 'form-control',
            ]); ?>
        </div>
        <div class="form-group pull-right">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'options' => [
            'class' => 'overflow-x',
        ],
        'tableOptions' => [
            'class' => 'table table-style table-count-list affiliate-invoice-table',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'columns' => [
            [
                'attribute' => 'document_date',
                'label' => 'Дата счёта',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'value' => function (Invoice $model) {
                    return date(DateHelper::FORMAT_USER_DATE, $model->payment->created_at);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => '№ счёта',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'value' => function (Invoice $model) {
                    return $model->getFullNumber();
                },
            ],
            [
                'attribute' => 'total_amount_with_nds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'style' => 'min-width: 100px',
                ],
                'value' => function (Invoice $model) {
                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                },
            ],
            [
                'attribute' => 'paymentType',
                'class' => DropDownSearchDataColumn::className(),
                'selectPluginOptions' => [
                    'width' => '180px',
                ],
                'label' => 'Тип платежа',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getPaymentTypeFilter(),
                'value' => function (Invoice $model) {
                    return $model->payment->type->name;
                },
            ],
            [
                'attribute' => 'contractorName',
                'class' => DropDownSearchDataColumn::className(),
                'selectPluginOptions' => [
                    'width' => '180px',
                ],
                'label' => 'Компания',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getContractorFilter(),
                'value' => function (Invoice $model) {
                    $companyName = $model->company->name_short;
                    return $companyName ? $model->company->companyType->name . ' ' . $companyName : "";
                },
            ],
            [
                'attribute' => 'subscribeStatus',
                'class' => DropDownSearchDataColumn::className(),
                'selectPluginOptions' => [
                    'width' => '180px',
                ],
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getInvoiceStatusFilter(),
                'value' => function (Invoice $model) {
                    $dateDiff = floor((time() - $model->payment->created_at) / 60 / 60 / 24);
                    if ($model->payment) {
                        if ($model->payment->is_confirmed) {
                            return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_PAYED];
                        }
                    }
                    if ($dateDiff >= 10) {
                        return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_OVERDUE];

                    }

                    return CompanyInvoiceSearch::$subscribeStatusArray[CompanyInvoiceSearch::SUBSCRIBE_STATUS_CREATED];
                },
            ],
            [
                'attribute' => 'payment_date',
                'label' => 'Дата оплаты',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'value' => function (Invoice $model) {
                    if ($model->payment) {
                        if ($model->payment->payment_date) {
                            if ($model->payment->type_id == PaymentType::TYPE_ONLINE) {
                                return date(DateHelper::FORMAT_USER_DATETIME, $model->payment->payment_date);
                            }

                            return date(DateHelper::FORMAT_USER_DATE, $model->payment->payment_date);
                        }
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'tariffType',
                'class' => DropDownSearchDataColumn::className(),
                'selectPluginOptions' => [
                    'width' => '200px',
                ],
                'label' => 'Тип подписки',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getTariffFilter(),
                'format' => 'html',
                'value' => function (Invoice $model) {
                    if ($model->payment->storeTariff) {
                        return $model->payment->storeTariff->cabinets_count . ' ' .
                            RUtils::numeral()->choosePlural($model->payment->storeTariff->cabinets_count, [
                                'кабинет', //1
                                'кабинета', //2
                                'кабинетов' //5
                            ]);
                    } elseif ($model->payment->outInvoiceTariff) {
                        if ($model->payment->outInvoiceTariff->id == StoreOutInvoiceTariff::UNLIM_TARIFF) {
                            return $model->payment->outInvoiceTariff->links_count . ' ссылок';
                        }
                        return $model->payment->outInvoiceTariff->links_count . ' ' .
                            RUtils::numeral()->choosePlural($model->payment->outInvoiceTariff->links_count, [
                                'ссылка', //1
                                'ссылки', //2
                                'ссылок' //5
                            ]);
                    } elseif ($model->payment->payment_for == Payment::FOR_ODDS) {
                        return 'Настройка финансовых отчетов';
                    } elseif ($model->payment->payment_for == Payment::FOR_ADD_INVOICE) {
                        return $model->payment->invoiceTariff->invoice_count . ' ' .
                            RUtils::numeral()->choosePlural($model->payment->invoiceTariff->invoice_count, [
                                'счет', //1
                                'счета', //2
                                'счетов' //5
                            ]);
                    } elseif ($tariff = $model->payment->tariff) {
                        return $tariff->tariffGroup->name . '<br>' . $tariff->getTariffName();
                    }

                    return '';
                },
            ],
            [
                'label' => '%%',
                'value' => function (Invoice $model) {
                    return $model->rewards_percent ?: "20";
                },
            ],
            [
                'attribute' => 'rewards_amount',
                'label' => 'Сумма вознаграждения',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'value' => function (Invoice $model) {
                    $rewardsAmount = $model->rewards_amount;
                    if (!$rewardsAmount && $model->payment->is_confirmed) {
                        $rewardsAmount =  ($model->total_amount_with_nds / 100) * 20;
                    }
                    return TextHelper::invoiceMoneyFormat($rewardsAmount, 2);
                },
            ],
        ],
    ]); ?>