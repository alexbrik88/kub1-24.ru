<?php

use common\components\helpers\Html;
use common\models\Company;
use common\models\company\CompanyAffiliateLeads;
use common\models\employee\Employee;
use common\models\service\ServiceModule;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/** @var CompanyAffiliateLeads $model */
/** @var Employee $employee */
/** @var Company $company */
/** @var array $products */

?>
<?php
Pjax::begin([
    'id' => 'modal-item-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
?>
<?php $form = ActiveForm::begin([
    'id' => 'leads-form',
    'method' => 'POST',
    'validationUrl' => Url::to(['custom-link-validate']),
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label text-bold',
        ],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

    <?= Html::hiddenInput('id', $model->id); ?>
    <div class="form-group mt-3">
        <?= $form->field($model, 'service_module_id', [
                'options' => [
                    'class' => 'form-group',
                ],
            ])->widget(Select2::class, [
                'hideSearch' => true,
                'data' => $products,
                'options' => [
                    'placeholder' => '',
                    'value' => !empty($model->service_module_id) ? $model->service_module_id : ServiceModule::SERVICE_INVOICES,
                    'disabled' => !empty($model->service_module_id),
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
    </div>
    <div class="form-group mt-3">
        <?= $form->field($model, 'name')->textInput([
            'maxlength' => 128,
            'class' => 'form-control col-12',
        ]); ?>
    </div>
    <?php if ($model->link): ?>
    <div class="form-group mt-3">
        <?= $form->field($model, 'link')->textInput([
            'maxlength' => 128,
            'class' => 'form-control col-12',
            'disabled' => !empty($model->link),
            'value' => Yii::$app->params['siteModules'][$model->service_module_id] . '?req=' . $model->link,
        ]); ?>
    </div>
    <?php endif; ?>
    <div class="mt-4 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>