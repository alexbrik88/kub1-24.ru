<?php

use common\components\helpers\Html;
use common\models\Company;
use common\models\company\CompanyAffiliateLeads;
use common\models\employee\Employee;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/** @var CompanyAffiliateLeads $model */
/** @var Employee $employee */
/** @var Company $company */

?>
<?php
Pjax::begin([
    'id' => 'modal-item-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
?>
<?php $form = ActiveForm::begin([
    'id' => 'leads-form',
    'method' => 'POST',
    'validationUrl' => Url::to(['lead-validate']),
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label text-bold',
        ],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<?= Html::hiddenInput('id', $model->id); ?>
    <div class="form-group mt-3">
        <?= $form->field($model, 'name')->textInput([
            'maxlength' => 128,
            'class' => 'form-control col-12',
        ]); ?>
    </div>
    <div class="form-group mt-3">
        <?= $form->field($model, 'username')->textInput([
            'maxlength' => 128,
            'class' => 'form-control col-12',
        ]); ?>
    </div>
    <div class="row">
        <div class="col-6 pull-left mt-1">
            <?= $form->field($model, 'email')->textInput([
                'maxlength' => 128,
                'class' => 'form-control col-12',
            ]); ?>
        </div>
        <div class="col-6 pull-right mt-1">
            <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
                'name' => 'phone',
                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => '+7(___) ___-__-__',
                    'style' => 'width: 100%',
                ],
            ]); ?>
        </div>
    </div>
    <div class="form-group mt-2">
        <?= $form->field($model, 'comment')
            ->textarea([
                'rows' => 7,
                'maxlength' => 500,
            ]); ?>
    </div>
    <div class="mt-4 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>