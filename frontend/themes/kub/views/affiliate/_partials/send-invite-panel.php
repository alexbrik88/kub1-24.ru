<?php

use common\components\helpers\Html;
use frontend\models\company\CompanyInviteForm;
use kartik\widgets\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/** @var CompanyInviteForm $model */

Pjax::begin([
    'id' => 'modal-send-invite-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);

?>

<div class="invoice-wrap" data-id="send-invite">
    <div class="invoice-wrap-in invoice-wrap-scroll">
        <button class="invoice-wrap-close invite-panel-close button-clr" type="button">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>

        <?php
            $form = ActiveForm::begin([
                'id' => 'invite-leads-form',
                'action' => Url::to(['send-invite-lead-form']),
                'method' => 'POST',
                'validationUrl' => Url::to(['invite-lead-form-validate']),
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,
                'validateOnSubmit' => true,
                'fieldConfig' => [
                    'labelOptions' => [
                        'class' => 'label text-bold',
                    ],
                ],
                'options' => [
                    'data-pjax' => true,
                ],
            ]);
        ?>

            <div class="main-block">
                <p class="bold" style="font-size:19px;">
                    Партнерская программа
                </p>

                <p class="bold">
                    Пригласите потенциального клиента.
                </p>

                <?= $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group email-recipient',
                    ],
                ])
                ->textInput(); ?>

                <?= $form->field($model, 'emailRecipient', [
                    'options' => [
                        'class' => 'form-group tooltip-email disabled',
                        'data-tooltip-content' => '#tooltip-email',
                    ],
                ])->hiddenInput()->label(false); ?>

                <?= $form->field($model, 'product', [
                    'options' => [
                        'class' => 'form-group tooltip-message',
                        'data-tooltip-content' => '#send-from-tooltip',
                    ],
                ])->widget(Select2::class, [
                    'hideSearch' => true,
                    'data' => $model->inviteProduct,
                    'value' => array_key_first($model->inviteProduct),
                    'options' => [
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]); ?>

                <?= $form->field($model, 'subject', [
                    'options' => [
                        'class' => 'form-group tooltip-message',
                        'data-tooltip-content' => '#send-from-tooltip',
                    ],
                ])->textInput([
                    'value' => 'Приглашаю зарегестрироваться в сервисе Куб!',
                ]); ?>

                <?= $form->field($model, 'body', [
                    'options' => [
                        'class' => 'form-group',
                    ],
                ])
                ->textarea([
                    'rows' => 5,
                    'value' => $model->textBody
                ])
                ->label('Сообщение <sup>*</sup>'); ?>

                <div class="mt-4 mb-5">
                    <?= Html::submitButton('Отправить', [
                        'class' => 'button-regular button-width button-regular_red button-clr',
                        'data-style' => 'expand-right',
                    ]); ?>
                </div>

                <p class="label bold mt-2 mb-2">
                    <sup>*</sup> Ссылка на рекомендуемый продукт будет добавлена к письму автоматически.
                </p>

                <div class="mt-4">
                    <p>
                        <strong>Остались вопросы?</strong><br/>
                        <strong>Напишите нам: support@kub-24.ru</strong>
                    </p>
                </div>
            </div>

        <?php
            ActiveForm::end();
        ?>

    </div>
</div>

<div class="tooltip-template" style="display: none">
    <span class="tooltip-from-email">
        Вы не можете менять email отправителя.
    </span>
    <span class="tooltip-email">
        Чтобы поменять email получателя отредактируйте поле в таблице.
    </span>
</div>

<?php

$this->registerJs(<<<JS
    $('.invite-panel-close').on('click', function (e) {
        $('[data-id="send-invite"]').toggleClass('visible show');
    });
JS, View::POS_READY);

Pjax::end();