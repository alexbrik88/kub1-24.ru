<?php

$this->registerCss(<<<CSS
    .line {
        border-bottom: 1px solid #e4e4e4;
    }
CSS
);

?>

<div class="invoice-wrap" data-id="help">
    <div class="invoice-wrap-in invoice-wrap-scroll">
        <button class="invoice-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="help">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>

        <div class="main-block">
            <p class="bold" style="font-size:19px;">
                Партнерская программа
            </p>

            <p class="bold">
                Как начать зарабатывать вместе с куб?
            </p>

            <p>
                <strong>1</strong>. Чтобы передать ссылку клиенту для регистрации в КУБ24, нажмите на кнопку копировать напротив ссылки, которую хотите передать клиенту .
            </p>
            <p>
                <img style="width:100%" src="/img/partners/kub/copy-link_kub.jpg"/>
            </p>
            <p class="line"></p>

            <p>
                <strong>2</strong>. Когда клиент пройдет регистрацию по вашей ссылке, вы увидите данную регистрацию во вкладке «Клиенты».
            </p>
            <p>
                <img style="width:100%" src="/img/partners/kub/tab-clients_kub.jpg"/>
            </p>

            <p class="line"></p>

            <p>
                <strong>3</strong>. После того как клиент сформирует счет на оплату сервиса КУБ24, вы также увидите эту информацию, во вкладке «Счета»
            </p>
            <p>
                <img style="width:100%" src="/img/partners/kub/tab-invoices_kub.jpg"/>
            </p>

            <br/>
            <p>
                <strong>Остались вопросы?</strong><br/>
                <strong>Напишите нам: support@kub-24.ru</strong>
            </p>
            <br/>
        </div>

    </div>
</div>