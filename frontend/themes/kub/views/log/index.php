<?php

use common\components\date\DateHelper;
use frontend\components\PageSize;
use frontend\models\log\Log;
use frontend\models\log\LogSearch;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Действия';
?>

<?= Html::a('Назад', ['/site/index'], [
    'class' => 'back-to-customers link mb-2',
]); ?>

<div class="wrap wrap-activities">
    <div class="portlet-title">
        <h4 class="">
            <?= $this->title; ?>
        </h4>

        <div class="actions">
            <div class="btn-group widget-home-popup">
                <?= $this->render('_form_entity', [
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="feeds-index">
        <?php $date = null; ?>
        <?= \yii\widgets\ListView::widget([
            'options' => [
                'class' => 'list-view',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => "{items}\n{pager}",
            'dataProvider' => $dataProvider,
            'itemView' => function (Log $model) use (&$date) {
                $currentDate = date(DateHelper::FORMAT_USER_DATE, $model->created_at);

                $output = '';

                if ($date != $currentDate) {
                    $date = $currentDate;
                    $output .= '<span class="date vert-middle-pos">' . \php_rutils\RUtils::dt()->ruStrFTime([
                            'format' => 'd F',
                            'monthInflected' => true,
                            'date' => $model->created_at,
                        ]) . '</span>';
                }

                return $output . $this->render('_view', [
                    'model' => $model,
                ]);
            },
        ]); ?>
        <div class="clearfix"></div>
    </div>
</div>
