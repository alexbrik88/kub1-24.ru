<?php

use common\models\currency\Currency;
use common\models\employee\Employee;
use kartik\select2\Select2;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Emoney */
/* @var $form yii\widgets\ActiveForm */

$currency = Currency::find()->select(['name', 'id'])->indexBy('id')->column();
if (empty($model->currency_id)) {
    $model->currency_id = Currency::DEFAULT_ID;
}

$isRuble = $model->currency_id == Currency::DEFAULT_ID;
$currencyTag = Html::tag('span', $currency[$model->currency_id] ?? '', [
    'class' => 'currency_name_tag',
]);
$isMainLabel = 'Основной'.Html::tag('span', ' для '.$currencyTag, [
    'class' => 'ruble_hidden'.($isRuble ? ' hidden' : ''),
]);

$templateDateInput = "{label}<div class='date-picker-wrap' style='width: 130px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

$helpIsAccounting = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Если операции по этому кошельку отображаются в бухгалтерии, поставьте тут галочку. Это поможет вам сверяться с бух. отчетностью',
    'title-as-html' => 1
]);
$helpStartBalance = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Если у вас в кошельке есть сумма на момент заведения ее в КУБ24,<br/> то укажите начальный остаток',
    'title-as-html' => 1
]);
?>

<div class="emoney-form">
    <?php $form = ActiveForm::begin([
        'id' => 'emoney-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => $enableAjaxValidation ?? false,
        'action' => $actionUrl ?? Yii::$app->request->url,
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
            'checkOptions' => [
                'class' => '',
                'labelOptions' => [
                    'class' => 'label'
                ],
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
            'data-ruble' => (int) $isRuble,
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
                'data' => $currency,
                'options' => [
                    'data-currency' => $currency,
                    'class' => 'select-accountant-currency_id',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'minimumResultsForSearch' => 10,
                ]
            ]); ?>
        </div>
    </div>

    <?php if (!$model->is_main) : ?>
        <?= $form->field($model, 'is_main')->checkbox([], true)->label($isMainLabel, [
            'class' => 'label mb-0',
        ]) ?>
    <?php endif ?>

    <?php if (!$model->isNewRecord && !$model->is_main) : ?>
        <?= $form->field($model, 'is_closed')->checkbox([
            'labelOptions' => [
                'class' => 'label mb-0',
            ],
        ], true) ?>
    <?php endif ?>

    <div class="row">
        <?php if ($model->isNewRecord) : ?>
            <div class="col-6">
                <?= $form->field($model, 'createStartBalance')->checkbox([
                    'class' => 'account_start_balance_checkbox',
                    'labelOptions' => [
                        'class' => 'label mb-0',
                    ],
                ], true)
                    ->label('Указать начальный остаток' . $helpStartBalance) ?>
            </div>
        <?php endif ?>
        <div class="col-6">
            <?= $form->field($model, 'is_accounting')->checkbox([
                'labelOptions' => [
                    'class' => 'label mb-0',
                ],
            ], true)
                ->label('Для учета в бухгалтерии' . $helpIsAccounting) ?>
        </div>
    </div>

    <?php if ($model->isNewRecord) : ?>
        <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : null; ?>">
            <div class="col">
                <?= $form->field($model, 'startBalanceAmount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                ]) ?>
            </div>
            <div class="col">
                <?= $form->field($model, 'startBalanceDate', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                ])->label('на дату') ?>
            </div>
        </div>
    <?php endif ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-width button-clr',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
            'data-dismiss' => 'modal'
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= (Yii::$app->request->isAjax || Yii::$app->request->isPjax) ?
Html::script('
    $("#emoney-form input:checkbox:not(.md-check)").uniform();
    refreshDatepicker();
', [
    'type' => 'text/javascript',
]) : ''; ?>
