<?php

use common\models\cash\Emoney;
use common\models\Company;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;

/* @var $emoney Emoney */
/* @var $title string */
/* @var $id string */
/* @var $company Company|null */

?>

<?php Modal::begin([
    'id' => $id,
    'title' => $title,
    'toggleButton' => false,
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>

<?= $this->render('_form', [
    'company' => $company,
    'model' => $emoney,
    'actionUrl' => $actionUrl ?? null,
    'enableAjaxValidation' => $enableAjaxValidation ?? false,
]); ?>

<?php Modal::end() ?>
