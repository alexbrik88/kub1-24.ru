<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Emoney */

?>

<div class="emoney-create">
    <?php \yii\widgets\Pjax::begin([
        'id' => 'emoney-form-pjax',
        'formSelector' => '#emoney-form',
        'enablePushState' => false,
    ]); ?>

        <?php if (ArrayHelper::getValue($_params_, 'success', false)) : ?>
            <?= Html::script('
                if ($("#emoney-pjax-container").length) {
                    $.pjax.reload("#emoney-pjax-container", {timeout: 10000});
                    $(".modal.show").modal("hide");
                } else {
                    $(".modal.show").modal("hide");                
                    location.href = location.href;
                }
            ', ['type' => 'text/javascript']); ?>
        <?php endif ?>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    <?php \yii\widgets\Pjax::end(); ?>
</div>
