<?php

use common\components\grid\GridView;
use frontend\models\EmoneySearch;
use frontend\themes\kub\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => "{items}\n{pager}",

    'columns' => [
        [
            'attribute' => 'name',
            'headerOptions' => [
            ],
        ],
        [
            'attribute' => 'currency_id',
            'label' => 'Валюта',
            'value' => function ($model) {
                return $model->currency->name;
            },
        ],
        [
            'attribute' => 'is_accounting',
            'format' => 'boolean',
        ],
        [
            'label' => 'Тип',
            'value' => 'typeLabel',
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'controller' => '/emoney',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $data) {
                    return Html::a(Icon::get('pencil'), $url, [
                        'class' => 'link ajax-modal-btn',
                        'data-pjax' => '0',
                        'data-title' => 'Изменить E-money',
                        'title' => Yii::t('yii', 'Изменить'),
                        'aria-label' => Yii::t('yii', 'Изменить'),
                    ]);
                },
                'delete' => function ($url, $data) {

                    if ($data->is_main) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Основной кошелек удалить нельзя'
                        ]);
                    } elseif (!$data->canDelete()) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Кошелек нельзя удалить, т.к. есть операции по кошельку.<br/>
                                Удалите операции, после можно удалить кошелек',
                            'title-as-html' => 1
                        ]);
                    } else {
                        return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'link',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить E-money?',
                        ]);
                    }
                },
            ],
            'visible' => ArrayHelper::getValue($_params_, 'actionVisible', false),
        ],
    ],
]);
