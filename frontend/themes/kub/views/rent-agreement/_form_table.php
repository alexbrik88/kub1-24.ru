<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\models\RentAgreementForm */
/* @var $form yii\bootstrap4\ActiveForm */

$totalAmount = 0;

yii2tooltipster::widget([
    'options' => [
        'class' => '.prise_modify_toggle',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true,
        'repositionOnScroll' => true,
        'side' => ['bottom', 'left', 'top', 'right'],
    ],
]);
?>

<table id="rent-agreemment-items" class="table table-style table-count">
    <thead>
        <tr class="heading">
            <th width="30px"></th>
            <th width="80%">Объект аренды</th>
            <th width="10%">Цена</th>
            <th width="10%" class="discount_column<?= $hasDiscount ? '': ' hidden'; ?>">
                <?= Html::tag('span', 'Скидка' . ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'), [
                    'id' => 'all_discount_toggle',
                    'class' => 'prise_modify_toggle',
                    'data-tooltip-content' => '#invoice_all_discount',
                    'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                ]) ?>
            </th>
            <th width="20%" class="discount_column<?= $hasDiscount ? '': ' hidden'; ?>">Цена со скидкой</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->getItems() as $key => $item) : ?>
            <?php $totalAmount += $item->amount; ?>
            <?= $this->render('_form_table_row', [
                'item' => $item,
                'key' => $key,
                'hasDiscount' => $hasDiscount,
            ]) ?>
        <?php endforeach ?>
    </tbody>
    <tfoot id="rent_agreemment_item_row" class="<?= count($model->getItems()) == 0 ? '' : 'hidden'; ?>">
        <tr>
            <td class="text-center">
                <?= Icon::get('circle-close', [
                    'class' => 'cursor-pointer',
                    'onclick' => '$(this).closest("tfoot").toggleClass("hidden", true);',
                ]) ?>
            </td>
            <td class="title p-1">
                <?= Select2::widget([
                    'id' => 'rent_agreement_entity_select',
                    'name' => 'entity_id',
                    'options' => [
                        'data-item-url' => Url::to(['item']),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0,
                        'ajax' => [
                            'url' => Url::to(['entity-list']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression("
                                function(params) {
                                    params.excludeIds = $('#rent-agreemment-items tr.rent_agreement_item')
                                        .map(function(){return $(this).data('entity');})
                                        .get();
                                    params.hasDiscount = $('#rentagreementform-has_discount').val();
                                    return params;
                                }
                            "),
                        ],
                        'escapeMarkup' => new JsExpression("
                            function(text) {
                                let escape = document.createElement('textarea');
                                escape.textContent = text;
                                return escape.innerHTML;
                            }
                        "),
                        'templateSelection' => new JsExpression("
                            function(data, container) {return data.title;}
                        "),
                        'templateResult' => new JsExpression("
                            function(data, container) {
                                container.innerHTML = data.title;
                                return container;
                            }
                        "),
                        'width' => '100%'
                    ],
                ]) ?>
            </td>
            <td class="price">
            </td>
            <td class="discount discount_column<?= $hasDiscount ? '': ' hidden'; ?>">
            </td>
            <td class="amount discount_column<?= $hasDiscount ? '': ' hidden'; ?>">
            </td>
        </tr>
    </tfoot>
</table>
<div class="d-flex align-items-start justify-content-between">
    <?= Html::button(Icon::get('add-icon').'<span>Добавить</span>', [
        'id' => 'rent_agreement_item_add',
        'class' => 'button-regular button-hover-content-red',
    ]) ?>
    <div class="ml-auto">
        <table id="rent-agreemment-summary">
            <tr>
                <td class="p-1 total-txt">Итого</td>
                <td class="p-1 pr-3 total_amount"><?= number_format($model->amount, 2, '.', '') ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="hidden">
    <!-- Discount popup -->
    <div id="invoice_all_discount" style="display: inline-block; width: 370px; overflow: hidden;">
        <div class="form-group mb-3">
            <div class="label">Скидка для всех позиций счета, %</div>
            <br/>
            <div class="row">
                <div class="col-6">
                    <?= Html::input('number', 'all-discount', 0, [
                        'id' => 'all-discount',
                        'min' => 0,
                        'max' => 99.9999,
                        'step' => 'any',
                        'style' => '',
                        'class' => 'form-control form-control-number',
                        'disabled' => $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE,
                        'data-old-value' => 0,
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="form-group mb-3">
            <label class="label" for="checkbox-discount-rub">
                Использовать формат скидки
            </label>
            <?= Html::activeRadioList($model, 'discount_type', [
                Invoice::DISCOUNT_TYPE_PERCENT => 'В процентах',
                Invoice::DISCOUNT_TYPE_RUBLE => 'В рублях',
            ], [
                'id' => 'config-discount_type',
                'data-rub' => Invoice::DISCOUNT_TYPE_RUBLE,
                'itemOptions' => [
                    'labelOptions' => [
                        'class' => 'label mr-3',
                    ],
                ],
            ]) ?>
        </div>

        <div class="form-group mb-3">
            <label class="label">
                <?= Html::checkbox('discount_hidden', $model->is_hidden_discount, [
                    'id' => 'config-is_hidden_discount',
                    'class' => 'discount-hidden',
                ]); ?>
                Скидка видна только в режиме редактирования
                <div class="tooltip-box tooltip-box_side-margins">
                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom"
                            title="Используйте, когда нужно посчитать скидку от текущей цены, но клиент не должен знать, что есть скидка. В счете для клиента столбцов со скидкой не будет, но цена будет указана со скидкой.">
                        <svg class="tooltip-question-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                        </svg>
                    </button>
                </div>
            </label>
        </div>

        <?= Html::button('Применить', [
            'id' => 'discount_submit',
            'class' => 'button-regular button-hover-grey button-width rent_agreement',
        ]) ?>
    </div>
</div>
