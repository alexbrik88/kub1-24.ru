<?php

use common\components\TextHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $item common\models\rent\RentAgreementItem */

?>

<tr class="rent_agreement_item">
    <td class="number">
        <?= $item->number ?>
    </td>
    <td class="title">
        <?= Html::a(Html::encode($item->entity->title), ['/rent/entity-view', 'id' => $item->entity->id]) ?>
    </td>
    <td class="title">
        <?= $model->getPaymentPeriod() ?>
    </td>
    <td class="amount text-right nowrap">
        <?= TextHelper::invoiceMoneyFormat($item->amount, 2); ?>
    </td>
</tr>