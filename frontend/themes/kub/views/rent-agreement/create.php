<?php

use frontend\assets\RentAsset;

/* @var $model frontend\models\RentAgreementForm */

$this->title = 'Аренда / Добавление договора аренды';

RentAsset::register($this);
?>

<?= $this->render('_form', ['model' => $model]) ?>