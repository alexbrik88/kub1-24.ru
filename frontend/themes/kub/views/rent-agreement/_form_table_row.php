<?php

use frontend\components\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $item frontend\models\RentAgreementItemForm */
/* @var $key int */

?>

<tr class="rent_agreement_item" data-entity="<?= $item->rent_entity_id ?>">
    <td class="text-center px-3">
        <?= Icon::get('circle-close', [
            'class' => 'rent_agreement_item_remove cursor-pointer',
            'title' => 'удалить позицию',
        ]) ?>
    </td>
    <td class="title px-3">
        <?= $item->entity->title ?>
        <?= Html::activeHiddenInput($item, sprintf('[%s]rent_entity_id', $key)) ?>
    </td>
    <td class="price p-1">
        <?= Html::activeTextInput($item, sprintf('[%s]price', $key), ['class' => 'form-control form-control_small']) ?>
    </td>
    <td class="discount p-1 discount_column<?= $hasDiscount ? '': ' hidden'; ?>">
        <?= Html::activeTextInput($item, sprintf('[%s]discount', $key), ['class' => 'form-control form-control_small discount-input']) ?>
    </td>
    <td class="amount text-right px-3 discount_column<?= $hasDiscount ? '': ' hidden'; ?>">
        <?= number_format($item->amount, 2, '.', '') ?>
    </td>
</tr>