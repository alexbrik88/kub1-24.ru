<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\rbac\UserRole;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\employee\EmployeeRole;
use frontend\widgets\BoolleanSwitchWidget;
use frontend\models\RentAgreementInvoiceSearch;
use yii\data\ActiveDataProvider;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Url;

/**
 * @var RentAgreementInvoiceSearch  $searchModel
 * @var ActiveDataProvider $dataProvider
 */

$period = StatisticPeriod::getSessionName();
$canView = false;
$user = Yii::$app->user->identity;
$company = $user->company;

$userConfig = $user->config;
$isContractor = $this->context->id == 'contractor';
$tabConfig = [
    'scan' => $isContractor ? $userConfig->contr_inv_scan : $userConfig->invoice_scan,
    'paydate' => $isContractor ? $userConfig->contr_inv_paydate : $userConfig->invoice_paydate,
    'paylimit' => $isContractor ? $userConfig->contr_inv_paylimit : $userConfig->invoice_paylimit,
    'act' => $isContractor ? $userConfig->contr_inv_act : $userConfig->invoice_act,
    'paclist' => $isContractor ? $userConfig->contr_inv_paclist : $userConfig->invoice_paclist,
    // 'waybill' => $isContractor ? $userConfig->contr_inv_waybill : $userConfig->invoice_waybill,
    'invfacture' => $isContractor ? $userConfig->contr_inv_invfacture : $userConfig->invoice_invfacture,
    'upd' => $isContractor ? $userConfig->contr_inv_upd : $userConfig->invoice_upd,
    'author' => $isContractor ? $userConfig->contr_inv_author : $userConfig->invoice_author,
    'comment' => $isContractor ? $userConfig->contr_inv_comment : $userConfig->invoice_comment,
];
if (!isset($tabViewClass)) {
    $tabViewClass = $userConfig->getTableViewClass('table_view_document');
}

$isFilter = (boolean)($searchModel->search);

$useContractor = isset($useContractor) ? $useContractor : false;
$docsTypeUpd = $company->isDocsTypeUpd;

echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'click',
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-currency',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]); ?>
<style>
    .update-attribute-tooltip-content {
        text-align: center;
    }
</style>

<div class="table-settings row row_indents_s mt-3">
    <div class="col-6">
        <?= Html::a(
            '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
            array_merge(['get-xls'], Yii::$app->request->queryParams),
            [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]
        ); ?>
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'invoice_scan',
                ],
                [
                    'attribute' => 'invoice_paylimit',
                ],
                [
                    'attribute' => 'invoice_paydate',
                ],
                [
                    'attribute' => 'invoice_act',
                ],
                [
                    'attribute' => 'invoice_paclist',
                ],
                //[
                //    'attribute' => 'invoice_waybill',
                //],
                [
                    'attribute' => 'invoice_invfacture',
                ],
                [
                    'attribute' => 'invoice_upd',
                ],
                [
                    'attribute' => 'invoice_author',
                    'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
                ],
                [
                    'attribute' => 'invoice_comment',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_document', 'tableSelector' => 'table#invoice_list_grid']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>

        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Номер счета, название или ИНН контрагента',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="wrap">
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'id' => 'invoice_list_grid',
            'class' => 'table table-style table-count-list invoice-table' . $tabViewClass,
        ],
        'rowOptions' => function (Invoice $data) {
            return $data->isAllStatusesComplete ? ['class' => 'invoice-payed'] : [];
        },
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
            $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
        'columns' => [
            [
                'attribute' => 'document_date',
                'label' => 'Дата счёта',
                'headerOptions' => [
                    //'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'document_number',
                'label' => '№ счёта',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (Invoice $data) use ($useContractor) {
                    if ($data->canView) {
                        $link = Html::a($data->fullNumber, ['/documents/invoice/view',
                            'type' => $data->type,
                            'id' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], ['class' => 'link']);
                    } else {
                        $link = $data->fullNumber;
                    }
                    $fullName = '№' . $data->document_number . ' от ' . DateHelper::format(
                            $data->document_date,
                            DateHelper::FORMAT_USER_DATE,
                            DateHelper::FORMAT_DATE
                        );

                    return Html::tag('span', $link, ['data-full-name' => $fullName]);
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'col_invoice_scan col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_scan link-view col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return DocumentFileWidget::widget(['model' => $model]);
                },
            ],
            [
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                    'style' => 'white-space: nowrap;',
                ],
                'attribute' => 'total_amount_with_nds',
                'format' => 'raw',
                'value' => function (Invoice $model) {
                    $viewPrice = TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
                    $rubPrice = TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                    $currency = $model->currency_name == Currency::DEFAULT_NAME ? '' : ' ' . $model->currency_name;
                    $content = Html::tag('span', $viewPrice . $currency, [
                        'class' => 'price' . ($currency ? ' tooltip-currency' : ''),
                        'data' => [
                            'price' => str_replace(" ", "", $rubPrice),
                            'tooltip-content' => $currency ? '#tooltip_currency-' . $model->id : null,
                            'remaining-amount' => str_replace(" ", "", TextHelper::invoiceMoneyFormat($model->remaining_amount, 2)),
                        ],
                    ]);
                    if ($currency) {
                        $text = "Сумма счета в RUB: {$rubPrice}<br/>";
                        $text .= "Курс обмена: {$model->currency_amount} {$model->currency_name} = {$model->currency_rate} RUB";
                        $tooltip = Html::tag('span', $text, ['id' => 'tooltip_currency-' . $model->id]);
                        $content .= "\n" . Html::tag('div', $tooltip, ['class' => 'hidden']);
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контр&shy;агент',
                'encodeLabel' => false,
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Invoice $model) {
                    $name = Html::encode($model->contractor->getNameWithType());
                    return Html::a($name, [
                        '/contractor/view',
                        'type' => $model->type,
                        'id' => $model->contractor_id,
                    ], [
                        'class' => 'link',
                        'title' => html_entity_decode($name),
                        'data' => [
                            'id' => $model->contractor_id,
                        ]
                    ]);
                },
                'visible' => ($useContractor ? false : true),
            ],
            [
                'attribute' => 'invoice_status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => function (Invoice $model) {
                    $options = [
                        'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap;',
                        'class' => '',
                    ];
                    if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                        $options['class'] .= ' tooltip2';
                        $options['data-tooltip-content'] = '#tooltip_invoice-sum-' . $model->id;
                    }

                    return $options;
                },
                'selectPluginOptions' => [
                    'templateResult' => new JsExpression('function(data, container) {
                        return $.parseHTML(data.text);
                    }'),
                    'templateSelection' => new JsExpression('function(data, container) {
                        return $.parseHTML(data.text);
                    }'),
                ],
                's2width' => '170px',
                'format' => 'raw',
                'value' => function (Invoice $model) {
                    $tooltipAmount = '';
                    if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                        $tooltipAmount = '<span class="tooltip-template"  style="display:none;"><span id="tooltip_invoice-sum-' . $model->id . '">
                        <span style="color: #45b6af;">Оплачено: ' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . ' руб.</span></br>
                        <span style="color: #f3565d;">Задолженность: ' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . ' руб.</span>
                        </span></span>';
                    }
                    $icon = $model->invoice_status_id == InvoiceStatus::STATUS_PAYED ?
                        Html::tag('span', $model->getPaymentIcon(), ['style' => 'color: #0097FD; font-size: 14px;']) : '';

                    return $model->invoiceStatus->name . " $icon" . $tooltipAmount;
                },
            ],
            [
                'attribute' => 'payment_limit_date',
                'format' => 'date',
                'headerOptions' => [
                    'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
                ],
                'visible' => true,
            ],
            [
                'attribute' => 'payDate',
                'label' => 'Дата оплаты',
                'headerOptions' => [
                    'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
                ],
                'value' => function (Invoice $model) {
                    return $model->payDate ? date('d.m.Y', $model->payDate) : '';
                },
                'visible' => true,
            ],
            [
                'attribute' => 'has_act',
                'label' => 'Акт',
                'headerOptions' => [
                    'class' => 'col_invoice_act col_contr_inv_act' . ($tabConfig['act'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_act col_contr_inv_act' . ($tabConfig['act'] ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                    $content = '';
                    if (!$data->need_act && $data->canAddAct) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            return BoolleanSwitchWidget::widget([
                                'id' => 'add-act_' . $data->id,
                                'action' => [
                                    '/documents/act/create',
                                    'type' => $data->type,
                                    'invoiceId' => $data->id,
                                    'contractorId' => ($useContractor ? $data->contractor_id : null),
                                ],
                                'inputName' => 'add_act',
                                'inputValue' => 1,
                                'label' => 'Выставить Акт?',
                                'addFalseForm' => false,
                                'toggleButton' => [
                                    'tag' => 'span',
                                    'label' => 'Без Акта',
                                    'class' => 'color-link cursor-pointer',
                                ],
                                'closeByFalseLabel' => true,
                                'options' => [
                                    'class' => 'dropdown-popup-in text-center',
                                ],
                                'labelOptions' => [
                                    'class' => 'dropdown-price-title pt-3 pb-3',
                                ],
                                'buttonBoxOptions' => [
                                    'class' => 'd-flex flex-nowrap justify-content-center',
                                ],
                                'buttonOptions' => [
                                    'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                                ],
                            ]);
                        } else {
                            return 'Без Акта';
                        }
                    }
                    foreach ($data->acts as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/act/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? 'link' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddAct) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/act/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'has_packing_list',
                'label' => 'Товарная накладная',
                'headerOptions' => [
                    'class' => 'col_invoice_paclist col_contr_inv_paclist' . ($tabConfig['paclist'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_paclist col_contr_inv_paclist' . ($tabConfig['paclist'] ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                    $content = '';
                    if (!$data->need_packing_list && $data->canAddPackingList) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            return BoolleanSwitchWidget::widget([
                                'id' => 'add-packing-list_' . $data->id,
                                'action' => [
                                    '/documents/packing-list/create',
                                    'type' => $data->type,
                                    'invoiceId' => $data->id,
                                    'contractorId' => ($useContractor ? $data->contractor_id : null),
                                ],
                                'inputName' => 'add_packing_list',
                                'inputValue' => 1,
                                'label' => 'Выставить ТН?',
                                'addFalseForm' => false,
                                'toggleButton' => [
                                    'tag' => 'span',
                                    'label' => 'Без ТН',
                                    'class' => 'color-link cursor-pointer',
                                ],
                                'closeByFalseLabel' => true,
                                'options' => [
                                    'class' => 'dropdown-popup-in text-center',
                                ],
                                'labelOptions' => [
                                    'class' => 'dropdown-price-title pt-3 pb-3',
                                ],
                                'buttonBoxOptions' => [
                                    'class' => 'd-flex flex-nowrap justify-content-center',
                                ],
                                'buttonOptions' => [
                                    'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                                ],
                            ]);
                        } else {
                            return 'Без ТН';
                        }
                    } else {
                        foreach ($data->packingLists as $doc) {
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/packing-list/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ], [
                                'class' => $data->canView ? 'link' : 'no-rights-link',
                            ]);

                            $fileLink = DocumentFileWidget::widget([
                                'model' => $doc,
                                'cssClass' => 'pull-right',
                            ]);

                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($data->canAddPackingList) {
                            $canCreate = $data->canCreate;
                            $content .= Html::a('Добавить', [
                                '/documents/packing-list/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ], [
                                'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                            ]);
                        }
                    }

                    return $content;
                }
            ],
            [
                'attribute' => 'has_invoice_facture',
                'label' => 'Счёт-фактура',
                'headerOptions' => [
                    'class' => 'col_invoice_invfacture col_contr_inv_invfacture' . ($tabConfig['invfacture'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_invfacture col_contr_inv_invfacture' . ($tabConfig['invfacture'] ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd) {
                    $content = '';

                    if (!$data->hasNds) {
                        return $content;
                    }
                    /* @var $doc InvoiceFacture */
                    foreach ($data->invoiceFactures as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/invoice-facture/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? 'link' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddInvoiceFacture) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/invoice-facture/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }

                    return $content;
                },
                'visible' => true,
            ],
            [
                'attribute' => 'has_upd',
                'label' => 'УПД',
                'headerOptions' => [
                    'class' => 'col_invoice_upd col_contr_inv_upd' . ($tabConfig['upd'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_upd col_contr_inv_upd' . ($tabConfig['upd'] ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                    $content = '';
                    if (!$data->need_upd && $data->canAddUpd) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            return BoolleanSwitchWidget::widget([
                                'id' => 'add-upd_' . $data->id,
                                'action' => [
                                    '/documents/upd/create',
                                    'type' => $data->type,
                                    'invoiceId' => $data->id,
                                    'contractorId' => ($useContractor ? $data->contractor_id : null),
                                ],
                                'inputName' => 'add_upd',
                                'inputValue' => 1,
                                'label' => 'Выставить УПД?',
                                'addFalseForm' => false,
                                'toggleButton' => [
                                    'tag' => 'span',
                                    'label' => 'Без УПД',
                                    'class' => 'color-link cursor-pointer',
                                ],
                                'closeByFalseLabel' => true,
                                'options' => [
                                    'class' => 'dropdown-popup-in text-center',
                                ],
                                'labelOptions' => [
                                    'class' => 'dropdown-price-title pt-3 pb-3',
                                ],
                                'buttonBoxOptions' => [
                                    'class' => 'd-flex flex-nowrap justify-content-center',
                                ],
                                'buttonOptions' => [
                                    'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                                ],
                            ]);
                        } else {
                            return 'Без УПД';
                        }
                    } else {
                        /* @var $doc Upd */
                        foreach ($data->upds as $doc) {
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/upd/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ], [
                                'class' => $data->canView ? 'link' : 'no-rights-link',
                            ]);

                            $fileLink = DocumentFileWidget::widget([
                                'model' => $doc,
                                'cssClass' => 'pull-right',
                            ]);

                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($data->canAddUpd) {
                            $canCreate = $data->canCreate;
                            $content .= Html::a('Добавить', [
                                '/documents/upd/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ], [
                                'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                            ]);
                        }
                    }

                    return $content;
                },
            ],

            [
                'attribute' => 'document_author_id',
                'label' => 'От&shy;вет&shy;ствен&shy;ный',
                'encodeLabel' => false,
                'headerOptions' => [
                    'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:150px'
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
                ],
                's2width' => '200px',
                'format' => 'raw',
                'value' => function (Invoice $model) {
                    return $model->author ? $model->author->getFio(true) : '';
                },
                'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
            ],
            [
                'attribute' => 'comment_internal',
                'label' => 'Ком&shy;мен&shy;та&shy;рий',
                'encodeLabel' => false,
                'headerOptions' => [
                    'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                    'width' => '10%',
                    'style' => 'min-width:150px'
                ],
                'contentOptions' => function (Invoice $model) use ($tabConfig) {
                    $options = [
                        'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                        'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap; max-width:90px;',
                    ];
                    if ($model->comment_internal) {
                        $options['class'] .= ' tooltip2';
                        $options['data-tooltip-content'] = "#tooltip_invoice_comment_{$model->id}";
                    }

                    return $options;
                },
                'format' => 'raw',
                'value' => function (Invoice $model) {
                    $tooltip = '';
                    if ($model->comment_internal) {
                        $tooltip = '
                        <span class="tooltip-template" style="display:none;">
                            <span id="tooltip_invoice_comment_' . $model->id . '">
                            ' . $model->comment_internal . '
                            </span>
                        </span>';
                    }

                    return $model->comment_internal . $tooltip;
                },
            ],
        ],
    ]) ?>
</div>
