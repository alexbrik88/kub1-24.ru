<?php

use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\rent\Category;
use common\models\rent\RentAgreement;
use frontend\assets\RentAsset;
use frontend\components\Icon;
use frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\rent\RentAgreement */

RentAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];

$this->title = "Аренда / Договор аренды {$model->id}"
?>

<a class="link mb-2" href="<?= Url::to(['/rent-agreement/index']); ?>">Назад к списку</a>

<div class="wrap p-3">
    <div class="page-in row">
        <div class="col-9">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h4 class="mb-2">
                    Договор аренды № <?= $model->document_number ?>
                    <?= $model->document_additional_number ?>
                    от <?= Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y') ?>
                </h4>
                <div class="">
                    <?= Html::button(Icon::get('info'), [
                        'class' => 'button-regular button-regular_red button-clr w-44 mr-2',
                        'title' => 'Последние действия',
                        'data-toggle' => 'modal',
                        'data-target' => '#basic',
                    ]) ?>
                    <?= Html::a(Icon::get('pencil'), ['update', 'id' => $model->id], [
                        'class' => 'button-regular button-regular_red button-clr w-44',
                        'title' => 'Изменить',
                    ]) ?>
                </div>
            </div>

            <div class="row">
                <div class="column mb-4 pb-2 col-sm-3">
                    <div class="label weight-700 mb-3">Арендатор</div>
                    <div><?= Html::a($model->contractor->getShortName(true), ['contractor/view',
                            'id' => $model->contractor_id,
                            'type' => $model->type
                        ]) ?></div>
                </div>
                <div class="column mb-4 pb-2 col-sm-3">
                    <div class="label weight-700 mb-3">Дата начала</div>
                    <div><?= DateHelper::format($model->date_begin, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></div>
                </div>
                <div class="column mb-4 pb-2 col-sm-3">
                    <div class="label weight-700 mb-3">Дата завершения</div>
                    <div><?= DateHelper::format($model->date_end, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></div>
                </div>
                <div class="column mb-4 pb-2 col-sm-3">
                    <div class="label weight-700 mb-3">Осталось дней</div>
                    <div><?= $model->isActive ? $model->daysLeft : '---' ?></div>
                </div>
            </div>
            <div>
                <div class="label weight-700 mb-3">Комментарий</div>
                <div>
                    <?= $model->comment ?: '---' ?>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="pb-2 mb-1">
                <span class="button-regular button-regular_padding_medium w-100"><?= $model->statusLabel ?></span>
            </div>

            <div class="pb-2 mb-1">
                <?= Html::a(Icon::get('add-icon').'<span class="ml-2">СЧЕТ</span>', ['create-invoice', 'id' => $model->id], [
                    'class' => 'button-regular button-regular_red w-100',
                ]) ?>
            </div>

            <div class="pb-2 mb-1">
                <div class="about-card mb-3">
                    <div class="about-card-item">
                        <span class="text-grey">Арендатор:</span>
                        <span><?= Html::a($model->contractor->getShortName(true), ['contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type]) ?></span>
                    </div>

                    <div class="about-card-item">
                        <?= DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Tabs::widget([
    'options' => [
        'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
    ],
    'linkOptions' => [
        'class' => 'nav-link',
    ],
    'headerOptions' => [
        'class' => 'nav-item',
    ],
    'items' => [
        [
            'label' => 'Объекты',
            'content' => $this->render('_view_tab_items', [
                'model' => $model,
            ]),
        ],
        [
            'label' => 'Счета',
            'content' => $this->render('_view_tab_invoices', [
                'model' => $model,
                'searchModel' => $invoiceSearchModel,
                'dataProvider' => $invoiceDataProvider,
            ]),
        ],
        [
            'label' => 'Деньги',
            'content' => $this->render('_view_tab_cash', [
                'user' => Yii::$app->user->identity,
                'model' => $model,
                'searchModel' => $cashSearchModel,
                'dataProvider' => $cashDataProvider,
            ]),
        ],
    ],
]) ?>

<div class="wrap wrap_padding_small wrap_btns check-condition visible">
    <div class="row align-items-center justify-content-end">
        <div class="column">
            <?= Html::a('Копировать', ['create', 'clone' => $model->id], [
                'class' => 'button-regular button-width button-hover-transparent',
            ]) ?>
        </div>
        <div class="column">
            <button type="button" class="button-clr button-regular button-width button-hover-transparent" data-toggle="modal" data-target="#delete-confirm">
                <svg class="svg-icon" viewBox="0 0 14 16">
                    <path d="M12.7 10.333V4H14V2.667h-3.9V0H3.9v2.667H0V4h1.3v12h11.4v-5.667zm-7.5-9h3.6v1.334H5.2V1.333zm6.2 13.334H2.6v-1.334h2.6V12H2.6V4h8.8v8H6.5v1.333h4.9v1.334z"></path>
                </svg>
                <span>Удалить</span>
            </button>
        </div>
    </div>
</div>

<?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
    'message' => 'Вы уверены, что хотите удалить этот договор аренды?',
]); ?>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>
    <div class="created-by">
        <?= date('d.m.Y', $model->created_at) ?>
        Создал
        <?= implode(' ', array_filter([$model->employee->lastname, $model->employee->firstname, $model->employee->patronymic])); ?>
    </div>
<?php Modal::end(); ?>