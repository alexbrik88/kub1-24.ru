<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $user common\models\employee\Employee
 * @var $searchModel frontend\models\RentAgreementCashSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$company = $user->company;
$strictMode = !trim($company->name_short) || !trim($company->inn);
?>

<div class="table-settings row row_indents_s mt-3">
    <div class="col-6">
        <?= frontend\widgets\TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'cash_column_income_expense',
                    'invert_attribute' => 'invert_cash_column_income_expense'
                ],
                [
                    'attribute' => 'cash_column_wallet'
                ],
                [
                    'attribute' => 'cash_column_paying'
                ],
                [
                    'attribute' => 'cash_column_project'
                ],
                [
                    'attribute' => 'cash_column_sale_point'
                ],
                [
                    'attribute' => 'cash_column_industry'
                ],
                [
                    'attribute' => 'cash_column_priority'
                ],
            ],
        ]); ?>
        <?= frontend\widgets\TableViewWidget::widget(['attribute' => 'table_view_cash']) ?>
    </div>
</div>

<div class="wrap">
    <?= $this->render('@frontend/modules/cash/views/default/_partial/table', [
        'user' => $user,
        'company' => $company,
        'strictMode' => $strictMode,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
