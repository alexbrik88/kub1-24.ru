<?php

use common\models\rent\RentAgreement;
use frontend\assets\RentAsset;
use frontend\models\RentSearch;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\helpers\Url;

/**
 * @var View               $this
 * @var RentSearch         $searchModel
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Договоры аренды';

RentAsset::register($this);
?>

<?= $this->render('_list', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'showEntity' => true,
    'isTab' => false,
]) ?>
