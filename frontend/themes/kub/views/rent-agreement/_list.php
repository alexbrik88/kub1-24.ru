<?php

use common\components\TextHelper;
use common\components\grid\GridView;
use common\models\employee\Config;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use frontend\components\Icon;
use frontend\models\RentEntity;
use frontend\models\RentSearch;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/**
 * @var RentSearch         $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var bool               $showEntity
 * @var bool               $isTab
 */

if (!isset($userConfig)) {
    /** @var Config $userConfig */
    $userConfig = Yii::$app->user->identity->config;
}

$categoryList = $searchModel->categoryList();
$contractorList = $searchModel->contractorFilterList();
$employeeList = $searchModel->employeeList();
?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'rent_categories',
                ],
                [
                    'attribute' => 'rent_statuses'
                ],
                [
                    'attribute' => 'rent_responsible',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_rent_list', 'tableSelector' => 'table#rent_list_grid']) ?>
        <span class="ml-3 mr-auto">
            Список договоров: <strong><?= $dataProvider->totalCount; ?></strong>
        </span>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
        <div class="d-flex flex-nowrap align-items-center">
            <?php if ($showEntity) { ?>
                <div class="dropdown popup-dropdown popup-dropdown_filter cash-filter <?= $searchModel->hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
                    <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="button-txt">Фильтр</span>
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="filter">
                        <div class="popup-dropdown-in p-3">
                            <div class="p-1">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Категория</div>
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($searchModel, 'category_id', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->widget(Select2::class, [
                                                        'hideSearch' => true,
                                                        'data' => ['0' => 'Все'] + $categoryList,
                                                        'options' => ['placeholder' => 'Все'],
                                                        'pluginOptions' => ['width' => '100%'],
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Объект аренды</div>
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($searchModel, 'entity', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->widget(Select2::class, [
                                                        'data' => ['0' => 'Все'] + $searchModel->entityActualList(),
                                                        'options' => ['placeholder' => 'Все'],
                                                        'pluginOptions' => ['width' => '100%'],
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Контрагент</div>
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($searchModel, 'contractor', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->widget(Select2::class, [
                                                        'data' => ['0' => 'Все'] + $contractorList,
                                                        'options' => ['placeholder' => 'Все'],
                                                        'pluginOptions' => ['width' => '100%'],
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Ответственный</div>
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($searchModel, 'employee', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->widget(Select2::class, [
                                                        'data' => ['0' => 'Все'] + $employeeList,
                                                        'options' => ['placeholder' => 'Все'],
                                                        'pluginOptions' => ['width' => '100%'],
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-6 pr-0">
                                        <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                            <span>Применить</span>
                                        </button>
                                    </div>
                                    <div class="col-6 pl-0 text-right">
                                        <a href="<?= Url::to([Yii::$app->requestedRoute]) ?>" class="button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                            <span>Сбросить</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'keyword', [
                    'type' => 'search',
                    'placeholder' => 'Поиск по наименованию объекта',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'id' => 'rent_list_grid',
        'class' => 'table table-style table-count-list'.($userConfig->table_rent_list ? ' table-compact' : ''),
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (RentAgreement $rent) {
                return Html::checkbox('RentAgreement[]', false, [
                    'value' => $rent->id,
                    'class' => 'joint-operation-checkbox',
                    'data-clone-url' => Url::to(['/rent-agreement/create', 'clone' => $rent->id]),
                ]);
            },
        ],
        [
            'attribute' => 'document_number',
            'value' => function(RentAgreement $rent) {
                return Html::a(Html::encode($rent->getFullNumber()), ['/rent-agreement/view', 'id' => $rent->id]);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:d.m.Y']
        ],
        [
            'attribute' => 'entity_id',
            'label' => 'Объект',
            'headerOptions' => ['class' => ($showEntity ? '' : ' hidden')],
            'contentOptions' => ['class' => ($showEntity ? '' : ' hidden')],
            'value' => function (RentAgreement $rent) {
                $pieces = [];
                foreach ($rent->entities as $entity) {
                    $pieces[] = Html::a(Html::encode($entity->title), ['/rent/entity-view', 'id' => $entity->id]);
                }

                return implode('<br>', $pieces);
            },
            'format' => 'raw',
            'filter' => ['' => 'Все'] + $searchModel->entityList(),
            'hideSearch' => false,
            's2width' => '200px'
        ],
        [
            'attribute' => 'category_id',
            'label' => 'Категория',
            'headerOptions' => [
                'class' => 'col_rent_categories ' . ($userConfig->rent_categories ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_categories ' . ($userConfig->rent_categories ? '' : ' hidden')
            ],
            'value' => function (RentAgreement $rent) {
                return ArrayHelper::getValue($rent, ['entity', 'category', 'title']);
            },
            'filter' => ['' => 'Все'] + $categoryList,
            'hideSearch' => false,
            's2width' => '200px'
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Арендатор',
            'value' => function (RentAgreement $rent) {
                $contractor = $rent->contractor;
                return Html::a(Html::encode($contractor->getNameWithType()), ['/contractor/view', 'type' => $contractor->type, 'id' => $contractor->id]);
            },
            'format' => 'raw',
            'filter' => ['' => 'Все'] + $contractorList,
            'hideSearch' => false,
            's2width' => '200px'
        ],
        [
            'attribute' => 'date_begin',
            'format' => ['date', 'php:d.m.Y']
        ],
        [
            'attribute' => 'date_end',
            'format' => ['date', 'php:d.m.Y']
        ],
        [
            'attribute' => 'days_left',
            'value' => function(RentAgreement $rent) {
                return $rent->daysLeft >= 0 ? $rent->daysLeft : '-';
            }
        ],
        [
            'attribute' => 'amount',
            'label' => 'Стоимость',
            'value' => function (RentAgreement $rent) {
                $pieces = [
                    TextHelper::invoiceMoneyFormat($rent->amount, 2) . ' руб.',
                    $rent->getPaymentPeriod(),
                ];
                return implode('/', array_filter($pieces));
            }
        ],
        [
            'attribute' => 'status',
            'label' => 'Статус',
            'headerOptions' => [
                'class' => 'col_rent_statuses ' . ($userConfig->rent_statuses ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_statuses ' . ($userConfig->rent_statuses ? '' : ' hidden')
            ],
            'value' => function (RentAgreement $rent) {
                return RentAgreement::$statuses[$rent->status];
            },
            'format' => 'raw',
            'filter' => ['' => 'Все'] + RentAgreement::$statuses,
            'hideSearch' => false,
            's2width' => '200px'
        ],
        [
            'attribute' => 'invoice_id',
            'label' => 'Последний счет',
            'value' => function (RentAgreement $rent) {
                if ($rent->invoice === null) {
                    return Html::a('Добавить', ['invoice', 'id' => $rent->id]);
                } else {
                    return Html::a('№' . $rent->invoice->fullNumber . ' от ' . date_format(date_create($rent->invoice->document_date), 'd.m.Y'), ['/documents/invoice/view', 'type' => 2, 'id' => $rent->invoice->id]);
                }
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'document_author_id',
            'label' => 'Ответственный',
            'headerOptions' => [
                'class' => 'col_rent_responsible ' . ($userConfig->rent_responsible ? '' : 'hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_responsible ' . ($userConfig->rent_responsible ? '' : 'hidden')
            ],
            'value' => function(RentAgreement $model) {
                return $model->employee->getShortFio();
            },
            'filter' => ['' => 'Все'] + $employeeList,
            'hideSearch' => false,
            's2width' => '150px'
        ],
    ]
]);
?>

<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox select-on-check-all" id="Allcheck" type="checkbox" name="count-list-table"/>
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано:
                <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto"></div>
        <div class="column">
            <?= Html::button('Копировать', [
                'id' => 'rent-clone-element',
                'class' => 'button-clr button-regular button-width button-hover-transparent hidden',
            ]) ?>
            <?= Html::button(Icon::get('garbage') . ' <span>Удалить</span>', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-target' => '#many-delete',
            ]) ?>
        </div>
    </div>
</div>

<?php if (!$isTab): ?>

<div id="many-delete" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные объекты (<span class="total-count">0</span>)?</h4>
            <div class="text-center">
                <?= Html::button('Да', [
                    'id' => 'rent-many-delete',
                    'class' => 'button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['/rent-agreement/many-delete']),
                ]); ?>
                <button class="button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>

<?php $this->registerJs('
    rentRentListPrepare();
') ?>
