<?php

use frontend\assets\RentAsset;

/* @var $model frontend\models\RentAgreementForm */

$this->title = 'Аренда / Редактирование договора аренды №"' . $model->rentAgreement->fullNumber . '"';

RentAsset::register($this);
?>

<?= $this->render('_form', ['model' => $model]) ?>