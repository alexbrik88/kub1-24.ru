<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\models\rent\RentAgreement
 */

?>

<div class="wrap mt-3">
    <?= $this->render('_view_table', [
        'model' => $model,
    ]) ?>
</div>
