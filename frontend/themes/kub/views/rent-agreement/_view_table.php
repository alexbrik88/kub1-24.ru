<?php

use common\components\TextHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\rent\RentAgreement */

?>

<table id="rent-agreemment-items" class="table table-bordered table-style table-count">
    <thead>
        <tr class="heading">
            <th width="1%">№</th>
            <th width="50%">Объект аренды</th>
            <th width="10%">Период оплаты</th>
            <th width="10%">Цена</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->items as $item) : ?>
            <?= $this->render('_view_table_row', [
                'hasDiscount' => $model->has_discount,
                'model' => $model,
                'item' => $item,
            ]) ?>
        <?php endforeach ?>
    </tbody>
</table>
<div class="d-flex align-items-start justify-content-between">
    <div> </div>
    <div class="ml-auto">
        <table id="rent-agreemment-summary">
            <tr>
                <td class="p-1 total-txt">Итого</td>
                <td class="p-1 nowrap">
                    <?= TextHelper::invoiceMoneyFormat($model->amount, 2); ?>
                </td>
            </tr>
        </table>
    </div>
</div>