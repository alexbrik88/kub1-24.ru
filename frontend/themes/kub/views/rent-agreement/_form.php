<?php

use common\models\Contractor;
use common\models\rent\RentAgreement;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model frontend\models\RentAgreementForm */

$hasAdditionals = $model->has_discount || $model->comment;

$this->registerCss(<<<CSS
    .select2-container .select2-results__option:first-child {
        color: #4679AE;
        font-weight: bold;
    }
CSS);

?>

<?php $form = ActiveForm::begin([
    'id' => 'rent-agreemment-form',
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

<?= $form->errorSummary($model); ?>
<?= $form->field($model, 'items')->hiddenInput(['value' => ''])->label(false)->error(false) ?>

<div class="wrap">
    <div class="d-flex flex-nowrap align-items-center">
        <div class="form-title mr-2">Договор аренды №</div>

        <?= $form->field($model, 'document_number', ['options' => ['class' => 'mr-2']])->textInput([
            'class' => 'form-control form-control_small',
            'placeholder' => 'номер',
        ])->label(false)->error(false) ?>

        <?= $form->field($model, 'document_additional_number', ['options' => ['class' => 'mr-2']])->textInput([
            'class' => 'form-control form-control_small',
            'placeholder' => 'доп. номер',
        ])->label(false)->error(false) ?>

        <span class="d-inline-block mr-2">от</span>

        <?= $form->field($model, 'document_date', ['options' => ['class' => 'date-picker-wrap']])->textInput([
            'class' => 'form-control form-control_small date-picker ico',
            'data' => ['date-viewmode' => 'years'],
        ])->label(false)->error(false) ?>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
                'contractorType' => Contractor::TYPE_CUSTOMER,
                'staticData' => [
                    'add-modal-contractor' => Icon::get('add-icon') . ' Добавить арендатора',
                ],
                'options' => [
                    'placeholder' => '',
                    'data-type' => Contractor::TYPE_CUSTOMER,
                    'data-doctype' => Documents::IO_TYPE_OUT,
                    'data-createurl' => Url::to(['add-modal-contractor']),
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]) ?>

            <div class="d-flex">
                <?= $form->field($model, 'date_begin', ['options' => ['class' => 'form-group date-picker-wrap mr-3']])->textInput([
                    'class' => 'form-control form-control_small date-picker ico rent_agreement_period_input',
                    'data' => ['date-viewmode' => 'years'],
                ])->label('Начало аренды') ?>

                <?= $form->field($model, 'date_end', ['options' => ['class' => 'form-group date-picker-wrap mr-3']])->textInput([
                    'class' => 'form-control form-control_small date-picker ico rent_agreement_period_input',
                    'data' => ['date-viewmode' => 'years'],
                ])->label('Окончание аренды') ?>

                <div class="form-group ml-auto">
                    <label class="label">Срок</label>
                    <?= Html::textInput(null, sprintf('%s дн.', $model->getPeriodDays()), [
                        'id' => 'rentagreementform-period_days',
                        'class' => 'form-control form-control_small',
                        'data-value' => $model->getPeriodDays(),
                        'readonly' => true,
                    ]) ?>
                </div>
            </div>

            <div class="d-flex justify-content-between">
                <div class="" style="width: 260px;">
                    <div style="margin-right: -1rem;">
                        <?= $form->field($model, 'payment_period')->widget(Select2::class, [
                            'data' => RentAgreement::$paymenPeriodList,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="label">Кол-во оплат</label>
                    <?= Html::textInput(null, $model->getPaymentCount(), [
                        'id' => 'rentagreementform-pay_count',
                        'class' => 'form-control form-control_small',
                        'readonly' => true,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::tag('span', '<span class="link-txt">Дополнительные параметры</span> '.Icon::get('shevron', [
            'class' => 'link-shevron',
        ]), [
            'class' => 'link link_collapse link_bold'.($hasAdditionals ? '' : ' collapsed'),
            'data-toggle' => 'collapse',
            'data-target' => '#moreDetails',
            'aria-expanded' => 'true',
            'aria-controls' => 'moreDetails',
        ]) ?>
    </div>
    <div id="moreDetails" class="collapse<?= $hasAdditionals ? ' show' : ''; ?>">
        <?= $form->field($model, 'has_discount')->checkbox() ?>

        <?= $form->field($model, 'comment')->textarea() ?>
    </div>
</div>

<div class="wrap">
    <?= $this->render('_form_table', [
        'form' => $form,
        'model' => $model,
        'hasDiscount' => $model->has_discount,
    ]) ?>
</div>

<div class="wrap wrap_btns check-condition visible mb-0" style="z-index: 99;">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitInput('Сохранить', [
                'class' => 'button-regular button-width button-regular_red'
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a(
                'Отменить',
                ['index'],
                ['class' => 'button-width button-regular button-hover-transparent']
            ); ?>
        </div>
    </div>
</div>

<?= Html::activeHiddenInput($model, 'discount_type'); ?>
<?= Html::activeHiddenInput($model, 'is_hidden_discount'); ?>

<?php $form::end() ?>

<!-- Add new -->
<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <?= Icon::get('close') ?>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>
