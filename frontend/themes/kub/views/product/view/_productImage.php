<?php

use devgroup\dropzone\DropZoneAsset;
use yii\helpers\Html;
use yii\helpers\Url;

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="pt-3 product-pics">
    <div class="row row_indents_s">
        <?php for ($number=1; $number<=4; $number++): ?>
            <?php $src = $model->getPictureThumb($number, 193, 136); ?>
            <div class="wrap-pic col-6 col-xl-3 d-flex flex-column mb-1 pb-2">
                <div data-number="<?= $number ?>" class="<?= !$src ? 'hidden' : '' ?> view-pic page-border page-border_grey page-border_min-height_234 d-flex flex-column justify-content-center align-items-center pt-4">
                    <div class="flex-shrink-0">
                        <?= Html::img($src, ['alt' => '']) ?>
                    </div>
                </div>
                <div data-number="<?= $number ?>" class="<?= $src ? 'hidden' : '' ?> upload-pic">
                    <div id="dz-pic<?= $number ?>" class="dz-upload-product-image">
                        <div class="dz-help"><span class="link">Выберите файл</span><br>или перетащите сюда</div>
                        <div class="dz-drag" style="display: none">Перетащите файл сюда</div>
                    </div>
                    <div class="dz-upload-product-tip">Рекомендуемый размер: 200x200.<br>Форматы: JPG, JPEG, PNG</div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>

<?= $this->render('@frontend/themes/kub/views/product/partial/jsDropzone', [
    'model' => $model,
    'immediately' => true
]) ?>