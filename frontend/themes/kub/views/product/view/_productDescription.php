<?php

use yii\helpers\Html;

?>
<div class="pt-3 pb-2">
    <?= nl2br(Html::encode($model->comment_photo)); ?>
</div>
