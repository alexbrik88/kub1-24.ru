<?php


use common\components\helpers\Month;
use common\models\document\Invoice;
use common\models\document\Order;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;use yii\widgets\Pjax;
use common\models\product\ProductStore;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();
$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;

$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
?>

<div class="nav-tabs-row">
    <ul class="nav nav-tabs nav-tabs_border_transparent w-100 mr-3" id="myTabIn" role="tablist">
        <?php if ($isGoods): ?>
        <li class="nav-item">
            <a class="nav-link <?= $isGoods ? 'active' : 'hidden' ?>" id="stocks-balance-tab" data-toggle="tab" href="#stocks-balance" role="tab" aria-controls="stocks-balance" aria-selected="false">Неснижаемый остаток</a>
        </li>
        <?php endif; ?>
        <li class="nav-item">
            <a class="nav-link <?= !$isGoods ? 'active' : '' ?>" id="stocks-indicators-tab" data-toggle="tab" href="#stocks-indicators" role="tab" aria-controls="stocks-indicators" aria-selected="true">Ключевые показатели</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="stocks-analytic-tab" data-toggle="tab" href="#stocks-analytic" role="tab" aria-controls="stocks-analytic" aria-selected="false">Аналитика</a>
        </li>
    </ul>
</div>
<div class="tab-content" id="myTabContentIn">
    <!-- TAB 1 -->
    <div class="tab-pane fade <?= $isGoods ? 'show active' : 'hidden' ?>" id="stocks-balance" role="tabpanel" aria-labelledby="stocks-balance-tab">
        <div class="page-border p-3">
            <div class="pb-4">
                <table class="table table-style mb-0">
                    <thead>
                    <tr>
                        <th class="align-middle pl-1 pb-2 pt-1">Название склада</th>
                        <th class="align-middle pb-2 pt-1">Остаток</th>
                        <th class="align-middle pb-2 pt-1">Неснижаемый остаток</th>
                        <th class="align-middle pb-2 pt-1">Действие, если остаток меньше</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($storeArray as $store): ?>
                    <?php /* @var $productStore ProductStore */
                    $productStore = $model->getProductStoreByStore($store->id);
                    $productInitQuantity = $model->getInitQuantity($productStore->store_id);
                    $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
                    $totalStoreQuantity += (int)$productStore->quantity;
                    $totalIrreducibleQuantity += $productIrreducibleQuantity;
                    $totalAmountForBuy += (int)$model->price_for_buy_with_nds * $productStore->quantity;
                    $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
                    <tr>
                        <td><div class="table-count-cell pl-0"><?= Html::encode($store->name) ?></div></td>
                        <td><div class="table-count-cell">
                                <?= $productStore->quantity ?>
                            </div></td>
                        <td><div class="table-count-cell">
                                <?= $productIrreducibleQuantity; ?>
                            </div></td>
                        <td><div class="table-count-cell">Отправить сообщение на почту <span style="color:#4679AE">(Скоро)</span></div></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- TAB 2 -->
    <div class="tab-pane fade <?= !$isGoods ? 'show active' : '' ?>" id="stocks-indicators" role="tabpanel" aria-labelledby="stocks-indicators-tab">
        <div class="page-border p-3">
            <div class="pb-4">
                <table class="table table-style mb-0">
                    <thead>
                    <tr>
                        <th class="align-middle pl-1 pb-2 pt-1">Название показателя</th>
                        <th class="align-middle pb-2 pt-1">Значение </th>
                        <th class="align-middle pb-2 pt-1">Контрольное значение </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><div class="table-count-cell pl-0">Тип <?= ($isGoods) ? 'товара' : 'услуги' ?> по АВС-XYZ анализу</div></td>
                        <td><div class="table-count-cell">AX</div></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><div class="table-count-cell pl-0">Маржинальность </div></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php if ($isGoods): ?>
                    <tr>
                        <td><div class="table-count-cell pl-0">Оборачиваемость</div></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- TAB 3 -->
    <div class="tab-pane fade" id="stocks-analytic" role="tabpanel" aria-labelledby="stocks-analytic-tab">

        <?php
        
        function calculatePercentPercent($valPrev, $valCurr) {
            return ($valPrev > 0) ?
                TextHelper::numberFormat(($valCurr - $valPrev) / $valPrev * 100) : ($valCurr > 0 ? 100 : 0);
        }

        function calculateAverage($values) {
            $a = array_filter($values);
            return (count($a) > 0) ?
                TextHelper::numberFormat(array_sum($a)/count($a)) : '';
        }

        $startDate = new DateTime(date('Y-m-01'));
        $periods = [];

        // for charts
        $x = [];
        $data = [
            'productSellsCount' => [],
            'productSellsSum' => [],
            'storeQuantity' => [],
            'turnover' => [],
            'buyersCount' => [],
            'sellsCount' => []
        ];
        $dataName = [
            'productSellsCount' => 'Продано (шт)',
            'productSellsSum' => 'Продано (₽)',
            'storeQuantity' => 'Остаток на складе (шт)',
            'turnover' => 'Оборачиваемость (дн)',
            'buyersCount' => 'Покупателей (шт)',
            'sellsCount' => 'Продаж (шт)'
        ];
        $dataUnits = [
            'productSellsCount' => 'шт',
            'productSellsSum' => '₽',
            'storeQuantity' => 'шт',
            'turnover' => 'дн',
            'buyersCount' => 'шт',
            'sellsCount' => 'шт'
        ];

        for ($i=11; $i>=0; $i--) {
            $monthNumber = $startDate->format('m');
            $periods[$i] = [
                'date_from' => $startDate->format('Y-m-01'),
                'date_to' => $startDate->format('Y-m-t'),
                'month_name' => Month::$monthShortByFull[$monthNumber],
                'month_name_full' => Month::$monthFullRU[(int)$monthNumber],
                'year' => $startDate->format('Y'),
            ];

            $startDate->modify("-1 month");
        }
        ksort($periods);

        $num = 0;
        foreach ($periods as $period)
        {
            $sellsQuery = $model->getInvoices()
                    ->joinWith('orders')
                    ->andWhere([Order::tableName() . '.product_id' => $model->id])
                    ->andWhere(['type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere(['between',
                        Invoice::tableName() . '.document_date',
                        $period['date_from'],
                        $period['date_to']
                    ]);

            $x[] = $period['month_name'] . '<br>' . $period['year'];
            $data['productSellsCount'][] = (clone $sellsQuery)->select('SUM('.Order::tableName().'.quantity)')->scalar() ?: 0;
            $data['productSellsSum'][] = (clone $sellsQuery)->select('SUM('.Order::tableName().'.amount_sales_with_vat)')->scalar() ?: 0;
            $model->setDateStart(new DateTime(date($period['date_from'])));
            $model->setDateEnd(new DateTime(date($period['date_to'])));
            $data['storeQuantity'][] = $model->balanceAtDate($num == 0, [$model->id]);
            $data['turnover'][] = 0;
            $data['buyersCount'][] = (new \yii\db\Query())->select('COUNT(*)')->from((clone $sellsQuery)->select(Invoice::tableName() . '.contractor_id')->groupBy(Invoice::tableName() . '.contractor_id'))->scalar() ?: 0;
            $data['sellsCount'][]  = (new \yii\db\Query())->select('COUNT(*)')->from((clone $sellsQuery)->select(Invoice::tableName() . '.id')->groupBy(Invoice::tableName() . '.id'))->scalar() ?: 0;
            $num++;
        }

        $data['productSellsSum'] = array_map(function($v) { return $v/100; }, $data['productSellsSum']);
        ?>

        <?php $indexCurr = count($periods) - 1; ?>
        <?php $indexPrev = count($periods) - 2; ?>
        <table id="table-stocks-analytics" class="table table-style table-count-list mt-3">
            <thead>
            <tr>
                <th class="align-middle pb-2 pt-1">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1"></span>
                    </button>
                </th>
                <th class="align-middle pb-2 pt-1"><?= $periods[$indexPrev]['month_name_full'].' '.$periods[$indexPrev]['year'] ?></th>
                <th class="align-middle pb-2 pt-1"><?= $periods[$indexCurr]['month_name_full'].' '.$periods[$indexCurr]['year'] ?></th>
                <th class="align-middle pb-2 pt-1">%%</th>
                <th class="align-middle pb-2 pt-1">Среднее (12 мес.)</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $key => $value): ?>
                    <?php if (!$isGoods && in_array($key, ['storeQuantity', 'turnover'])) continue; ?>
                    <tr>
                        <td>
                            <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-row-trigger data-target="first-floor-<?= $key ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-dark weight-700 text_size_14 ml-1"><?= $dataName[$key] ?></span>
                            </button>
                        </td>
                        <td><?= TextHelper::numberFormat($value[$indexPrev]) ?></td>
                        <td><?= TextHelper::numberFormat($value[$indexCurr]) ?></td>
                        <td><?= calculatePercentPercent($value[$indexPrev], $value[$indexCurr]) ?></td>
                        <td><?= calculateAverage($value) ?></td>
                    </tr>
                    <tr class="d-none" data-id="first-floor-<?= $key ?>">
                        <td colspan="5">
                            <?= $this->render('__productStocksChart', [
                                'x' => $x,
                                'y' => $value,
                                'units' => $dataUnits[$key]
                            ]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php if (Yii::$app->request->get('_pjax') == '#product-view-tabs-pjax') {

    $this->registerJs(<<<JS
		$('[data-collapse-row-trigger]').click(function() {
			var target = $(this).data('target');
			$(this).toggleClass('active');
			if ( $(this).hasClass('active') ) {
				$('[data-id="'+target+'"]').removeClass('d-none');
			} else {
                $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
				$('[data-id="'+target+'"]').each(function(i, row) {
					$('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
				});
				$('[data-id="'+target+'"]').addClass('d-none');
			}
			if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
				$('[data-collapse-all-trigger]').removeClass('active');
			} else {
				$('[data-collapse-all-trigger]').addClass('active');
			}
		});
		$('[data-collapse-all-trigger]').click(function() {
			var _this = $(this);
			var table = $(this).closest('.table');
			var row = table.find('tr[data-id]');
			_this.toggleClass('active');
			if ( _this.hasClass('active') ) {
				row.removeClass('d-none');
				$(table).find('tbody .table-collapse-btn').addClass('active');
			} else {
				row.addClass('d-none')
				$(table).find('tbody .table-collapse-btn').removeClass('active');
			}
		});
JS
);

} ?>