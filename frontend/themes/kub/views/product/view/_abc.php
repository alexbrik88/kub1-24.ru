<?php

use frontend\modules\reports\models\AbstractFinance;
use frontend\modules\reports\models\ProductAnalysisABC;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

/** @var \common\models\product\Product $model */

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->product_abc_help ?? false;
$showChartPanel = $userConfig->product_abc_chart ?? false;

$searchModel = new \common\models\product\ProductABC([
    'company' => Yii::$app->user->identity->company,
    'product_id' => $model->id,
]);
$searchModel->load(Yii::$app->request->get());

$PERIODS = array_reverse($searchModel::_getPeriodsFromY(11, 0, $searchModel->year));
if (date('Y') == $searchModel->year) {
    $PERIODS = array_merge([date('Ym')], $PERIODS);
}

// CHARTS /////////////////////////
$tableData = [];
$x = $labelX = $labelCategoryXYZ = [];
$labelCategoryABC = [
    'margin' => [],
    'margin_percent' => [],
    'sells_quantity' => [],
];
$y = [
    'margin' => [],
    'margin_percent' => [],
    'sells_quantity' => [],
    'demand' => [],
    'stock_quantity' => [],
    'turnover' => [],
];
///////////////////////////////////////
// CHANGE RECOMMENDATIONS MONTH ONLY //
if (Yii::$app->request->isPjax)
    $PERIODS = [];
///////////////////////////////////////

$_idxChart = 0;
foreach ($PERIODS as $period) {

    if (substr($period, 0, 4) != $searchModel->year) {
        break;
    }

    $searchModel->yearMonth = $period;
    $products = $searchModel->search();
    $searchModel->removeTmpTables();

    $model->setDateStart(new DateTime($searchModel::_getDateFromYM($period)));
    $model->setDateEnd(new DateTime($searchModel::_getDateFromYM($period, true)));
    $balanceAtStart = $model->balanceAtDate(true, [$model->id], true, Yii::$app->db2);
    $balanceAtEnd = $model->balanceAtDate(false, [$model->id], true, Yii::$app->db2);
    $irreducibleQuantity = array_sum($model->getIrreducibleQuantity(null, Yii::$app->db2));

    if ($products) {
        $product = &$products[0];
        $product['balance_at_start'] = $balanceAtStart;
        $product['balance_at_end'] = $balanceAtEnd;
        $product['irreducible_quantity'] = $irreducibleQuantity;
        $tableData[] = $product;
    } else {
        $product = null;
    }

    if ($_idxChart < 6) {

        $x[] = ArrayHelper::getValue(['ЯНВ', 'ФЕВ', 'МАРТ', 'АПР', 'МАЙ', 'ИЮН', 'ИЮЛ', 'АВГ', 'СЕН', 'ОКТ', 'НОЯ', 'ДЕК'], -1 + substr($period, -2, 2), $period);
        $labelX[] = ArrayHelper::getValue(AbstractFinance::$month, substr($period, -2, 2)) . ' ' . substr($period, 0, 4);

        // charts
        if ($product) {
            $labelCategoryABC['margin'][] = str_replace([1,2,3], ['A', 'B', 'C'], substr($product['abc'], 0,1));
            $labelCategoryABC['margin_percent'][] = str_replace([1,2,3], ['A', 'B', 'C'], substr($product['abc'], 1,1));
            $labelCategoryABC['sells_quantity'][] = str_replace([1,2,3], ['A', 'B', 'C'], substr($product['abc'], 2,1));

            $labelCategoryXYZ[] = ($product['variation_coefficient'] <= 10) ? 'X' :
                ($product['variation_coefficient'] > 10 && $product['variation_coefficient'] <= 25 ? 'Y' : 'Z');

            $y['margin'][] = (float)$product['margin'] / 100;
            $y['margin_percent'][] = (float)$product['margin_percent'] * 100;
            $y['sells_quantity'][] = (float)$product['selling_quantity'];
            $y['demand'][] = (float)$product['variation_coefficient'];
            $y['stock_quantity'][] = (float)$balanceAtEnd;
            $y['turnover'][] = 0; // todo
        } else {
            $labelCategoryABC['margin'][] = 'C';
            $labelCategoryABC['margin_percent'][] = 'C';
            $labelCategoryABC['sells_quantity'][] = 'C';
            $labelCategoryXYZ[] = 'Z';
            $y['margin'][] = 0;
            $y['margin_percent'][] = 0;
            $y['sells_quantity'][] = 0;
            $y['demand'][] = 0;
            $y['stock_quantity'][] = (float)$balanceAtEnd;
            $y['turnover'][] = 0;
        }
    }

    $_idxChart++;
}

$x = array_reverse($x);
$labelX = array_reverse($labelX);
$labelCategoryABC['margin'] = array_reverse($labelCategoryABC['margin']);
$labelCategoryABC['margin_percent'] = array_reverse($labelCategoryABC['margin_percent']);
$labelCategoryABC['sells_quantity'] = array_reverse($labelCategoryABC['sells_quantity']);
$labelCategoryXYZ = array_reverse($labelCategoryXYZ);
$y['margin'] = array_reverse($y['margin']);
$y['margin_percent'] = array_reverse($y['margin_percent']);
$y['sells_quantity'] = array_reverse($y['sells_quantity']);
$y['demand'] = array_reverse($y['demand']);
$y['stock_quantity'] = array_reverse($y['stock_quantity']);
$y['turnover'] = array_reverse($y['turnover']);

?>
<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2">ABC и XYZ анализ</h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= Html::beginForm(\yii\helpers\Url::current(), 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="product_abc_chart">
    <div class="pt-4 pb-3">
        <div class="row">
            <div class="col-4">
                <?php
                echo $this->render('abc/chart', [
                    'searchModel' => $searchModel,
                    'title' => 'Маржа',
                    'chartId' => 'margin',
                    'x' => $x,
                    'y' => $y['margin'],
                    'labels' => [
                        'x' => $labelX,
                        'category' => $labelCategoryABC['margin']
                    ],
                    'color' => '#36c3b0'
                ]);
                ?>
            </div>
            <div class="col-4">
                <?php
                echo $this->render('abc/chart', [
                    'searchModel' => $searchModel,
                    'title' => '% Маржи',
                    'chartId' => 'margin_percent',
                    'x' => $x,
                    'y' => $y['margin_percent'],
                    'labels' => [
                        'x' => $labelX,
                        'category' => $labelCategoryABC['margin_percent'],
                    ],
                    'color' => '#36c3b0'
                ]);
                ?>
            </div>
            <div class="col-4">
                <?php
                echo $this->render('abc/chart', [
                    'searchModel' => $searchModel,
                    'title' => 'Объем продаж',
                    'chartId' => 'sells_quantity',
                    'x' => $x,
                    'y' => $y['sells_quantity'],
                    'labels' => [
                        'x' => $labelX,
                        'category' => $labelCategoryABC['sells_quantity'],
                    ],
                    'color' => '#36c3b0'
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="pt-2 pb-3">
        <div class="row">
            <div class="col-4">
                <?php
                echo $this->render('abc/chart', [
                    'searchModel' => $searchModel,
                    'title' => 'Спрос',
                    'chartId' => 'demand',
                    'x' => $x,
                    'y' => $y['demand'],
                    'labels' => [
                        'x' => $labelX,
                        'category' => $labelCategoryXYZ
                    ],
                    'color' => '#336d9a'
                ]);
                ?>
            </div>
            <div class="col-4">
                <?php
                echo $this->render('abc/chart', [
                    'searchModel' => $searchModel,
                    'title' => 'Остатки',
                    'chartId' => 'stock_quantity',
                    'x' => $x,
                    'y' => $y['stock_quantity'],
                    'labels' => [
                        'x' => $labelX,
                        'category' => $labelCategoryXYZ,
                        'showOnPoint' => false
                    ],
                    'color' => '#336d9a'
                ]);
                ?>
            </div>
            <div class="col-4">
                <div class="ht-caption"><?= 'Оборачиваемость' ?></div>
                <div style="height: 200px; border: 1px solid #e0f1f8; border-radius: 4px;"></div>
                <?php
                //echo $this->render('abc/chart', [
                //    'searchModel' => $searchModel,
                //    'title' => 'Оборачиваемость',
                //    'chartId' => 'turnover',
                //    'x' => $x,
                //    'y' => $y['turnover'],
                //    'labels' => [
                //        'x' => $labelX,
                //        'category' => $labelCategoryXYZ,
                //        'showOnPoint' => false
                //    ],
                //    'color' => '#336d9a'
                //]);
                ?>
            </div>
        </div>
    </div>
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('abc/recomendations', [
            //'product' => $recommendationsProduct,
            'searchModel' => $searchModel
        ]);
        ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="product_abc_help">
    <div class="pt-4 pb-3">
        ...
        <?php
        //echo $this->render('_partial/odds-panel')
        ?>
    </div>
</div>


<?= $this->render('abc/table', [
        'searchModel' => $searchModel,
        'data' => $tableData
    ]);
?>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
    $('#productabc-year').on('change', function() {
        location.href = $(this).closest('form').attr('action') + '?' + $(this).closest('form').serialize();
    });
</script>

<?php
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);