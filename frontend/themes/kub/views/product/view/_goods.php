<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductStore;
use frontend\models\Documents;
use yii\bootstrap\Tabs;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\rbac\permissions;
use common\models\product\ProductToSupplier;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */
/* @var $canABC boolean */
/* @var $abcModalTitle string|null */

$canViewPriceForBuy = Yii::$app->user->can(\frontend\rbac\permissions\Product::PRICE_IN_VIEW, [
    'model' => $model,
]);
$storeArray = Yii::$app->user->identity
    ->getStores()
    ->orderBy(['is_main' => SORT_DESC])
    ->all();
$fromStoreArray = [];
foreach ($storeArray as $store) {
    if ($model->getQuantity($store->id) > 0)
    $fromStoreArray[] = $store;
}

$company = $model->company;

$posibleTabs = [
    '_productInfo',
    '_productPrice',
    '_productQuantity',
    '_productSupplier',
    '_productImage',
    '_productDescription',
    '_productTurnover',
    '_productStocks',
];
$tabActive = Yii::$app->request->get('tab');
$modeActive = Yii::$app->request->get('mode', 'view');
if (preg_match('|/product/turnover|', Yii::$app->request->referrer)) {
    $tabActive = '_productTurnover';
}
if (!$tabActive || !in_array($tabActive, $posibleTabs)) {
    $tabActive = '_productInfo';
}

$suppliers = [];
$productToSupplier = ProductToSupplier::find()->where(['product_id' => $model->id])->orderBy('price')->all();
if ($productToSupplier) {
    foreach ($productToSupplier as $pts) {
        $suppliers[] = Html::a($pts->supplier->getShortName(),
            ['/contractor/view', 'type' => Contractor::TYPE_SELLER, 'id' => $pts->supplier->id],
            ['target' => '_blank', 'class' => 'nowrap']
        );
    }
}

$isMoveable = $model->production_type == Product::PRODUCTION_TYPE_GOODS && max(0, $model->quantity) > 0;

$quantity = $model->getQuantity();
array_walk($quantity, function ($val) {
    $val = max(0, $val);
});
$hint = Html::tag('div', '', [
    'class' => 'quantity_hint quantity_hint_',
]);
foreach ($quantity as $key => $value) {
    $hint .= Html::tag('div', 'Количество на складе: ' . $value, [
        'class' => 'hidden quantity_hint quantity_hint_' . $key,
    ]);
}

if (preg_match('|/product/turnover|', Yii::$app->request->referrer)) {
    $tabActive = '_productTurnover';
}
?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-1 single-product-page">
    <div class="pl-1">
        <div class="page-in row">
            <div class="col-9 pr-4">
                <div class="pr-2">
                    <div class="row align-items-start justify-content-between pb-3">
                        <h4 class="column mb-2 break-word-title">
                            <?= Html::encode($model->title); ?>
                        </h4>
                        <div style="position: absolute;right: 31px;top: 1px;min-width: 105px;z-index:2;">
                            <a class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                title="Последние действия"
                                data-toggle="modal"
                                href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                </svg>
                            </a>
                            <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                                <a href="<?= Url::toRoute(['product/update', 'productionType' => $model->production_type, 'id' => $model->id,]); ?>"
                                   title="Редактировать"
                                   class="edit-product button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                                   data-tab="">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div style="position: absolute;right: 31px;top: 53px;min-width: 105px;z-index:2;">
                            <?php if ($canABC) : ?>
                                <?= Html::button('ABC анализ', [
                                    'class' => 'w-100 button-regular '.($modeActive == 'abc' ? 'button-regular_red' : 'button-hover-transparent'),
                                    'data-style' => 'expand-right',
                                    'title' => 'ABC анализ',
                                    'data-href' => Url::current(['mode' => ($modeActive == 'abc' ? null : 'abc')]),
                                    'onclick' => 'location.href = $(this).data(\'href\');',
                                ]) ?>
                            <?php elseif (!empty($abcModalTitle)) : ?>
                                <?= Html::button('ABC анализ', [
                                    'class' => 'w-100 button-regular button-hover-transparent',
                                    'title' => 'ABC анализ',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#abc-denied-modal',
                                ]) ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-11">
                            <div class="row">
                                <div class="col-3 mb-3">
                                    <div class="label weight-700 mb-2">Цена продажи</div>
                                    <div><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?> ₽</div>
                                </div>
                                <div class="col-3 mb-3">
                                    <div class="label weight-700 mb-2">НДС</div>
                                    <div><?= $model->priceForSellNds->name; ?></div>
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="label weight-700 mb-2">Группа товара</div>
                                    <div><?= $model->group ? Html::encode($model->group->title) : Product::DEFAULT_VALUE; ?></div>
                                </div>
                                <?php if ($modeActive == 'view'): ?>
                                    <div class="col-3 mb-3">
                                        <div class="label weight-700 mb-2">Код</div>
                                        <div><?= Html::encode($model->code ?: Product::DEFAULT_VALUE); ?></div>
                                    </div>
                                    <div class="col-3 mb-3">
                                        <div class="label weight-700 mb-2">Артикул</div>
                                        <div><?= Html::encode($model->article ?: Product::DEFAULT_VALUE); ?></div>
                                    </div>
                                    <div class="col-4 mb-3">
                                        <div class="label weight-700 mb-2">Штрих код</div>
                                        <div><?=  Html::encode($model->barcode ?: Product::DEFAULT_VALUE); ?></div>
                                    </div>
                                    <div class="col-3 mb-3">
                                        <div class="label weight-700 mb-2">Ед. изм.</div>
                                        <div><?= $model->productUnit ? Html::encode($model->productUnit->name) : Product::DEFAULT_VALUE; ?></div>
                                    </div>
                                    <div class="col-3 mb-3">
                                        <div class="label weight-700 mb-2">Объем (м3)</div>
                                        <div><?= $model->volume ? $model->volume : Product::DEFAULT_VALUE; ?> </div>
                                    </div>
                                    <div class="col-4 mb-3">
                                        <div class="label weight-700 mb-2">Вес (кг)</div>
                                        <div><?= $model->weight ? $model->weight : Product::DEFAULT_VALUE; ?> </div>
                                    </div>
                                    <div class="col-3 mb-3">
                                        <div class="label weight-700 mb-2">Остатки</div>
                                        <div>
                                            <?= $model->getBalanceOnDate() . ' ' . ($model->productUnit ? $model->productUnit->name : '') ?>
                                        </div>
                                    </div>
                                    <div class="col-9 mb-3">
                                        <div class="label weight-700 mb-2">Поставщик</div>
                                        <div>
                                            <?= ($suppliers) ? implode(', ', $suppliers) :  Product::DEFAULT_VALUE ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($model->status == Product::ARCHIVE): ?>
            <div class="col-3 pl-0">
                <div class="button-regular mb-3 w-100 btn-status-archive">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#archiev"></use>
                    </svg>
                    <span>В архиве c <?= date('d.m.Y', $model->archived_at) ?></span>
                </div>
            </div>
            <?php else: ?>
            <div class="col-3 pl-0">
                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)
                ) : ?>
                    <div class="pb-2 dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductSales"
                           <?php if ($modeActive == 'view'): ?>data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" <?php else: ?>title="Закройте режим ABC-анализа"<?php endif; ?>>
                            <span>Продажа</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductSales">
                            <div class="popup-dropdown-in overflow-hidden overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('Выставить счёт', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2]),
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Выставить счёт-договор', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2, 'isContract' => 1]),
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Создать заказ', null, [
                                            'class' => 'nowrap add-to-order',
                                            'data-action' => Url::to(['documents/order-document/create',]),
                                        ]); ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Списать товар', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/goods-cancellation/create']),
                                        ]) ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">Накладная на возраст</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_IN)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)) : ?>
                    <div class="pb-2 dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductPurchase"
                            <?php if ($modeActive == 'view'): ?> data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" <?php else: ?>title="Закройте режим ABC-анализа"<?php endif; ?>>
                            <span>Покупка</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductPurchase">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('По счёту', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 1]),
                                        ]) ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">По приходному ордеру</a></li>
                                    <li><a class="nowrap" href="#">Без документов</a></li>
                                    <li><a class="nowrap" href="#">Создать заказ</a></li>
                                    <li><a class="nowrap" href="#">Начальные остатки</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($modeActive == 'view'): ?>
                    <?php if (count($storeArray) > 1): ?>
                        <a class="button-regular w-100 button-hover-content-red" role="button" href="#move_to_store_modal" data-toggle="modal" <?= !$isMoveable ? 'disabled' : '' ?>>
                            <span>Переместить на склад</span>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($modeActive == 'view'): ?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?php Pjax::begin([
                'id' => 'product-view-tabs-pjax',
                'linkSelector' => '#product-view-tabs > .nav-item > .nav-link',
                'timeout' => 10000,
                'options' => [
                    'style' => 'width:100%'
                ]
            ]); ?>

            <?= Tabs::widget([
                'id' => 'product-view-tabs',
                'options' => [
                    'class' => 'nav nav-tabs w-100 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane pl-3 pr-3',
                    'style' => 'width:100%'
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Информация',
                        'url' => Url::current(['tab' => null]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productInfo' ? ' active' : ''),
                            'data-tab' => '_productInfo'
                        ],
                        'active' => (!$tabActive || $tabActive == '_productInfo')
                    ],
                    [
                        'label' => 'Цены',
                        'url' => Url::current(['tab' => '_productPrice']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productPrice' ? ' active' : ''),
                            'data-tab' => '_productPrice'
                        ],
                        'active' => ($tabActive == '_productPrice')
                    ],
                    [
                        'label' => 'Остатки',
                        'url' => Url::current(['tab' => '_productQuantity']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productQuantity' ? ' active' : ''),
                            'data-tab' => '_productQuantity'
                        ],
                        'active' => ($tabActive == '_productQuantity')
                    ],
                    [
                        'label' => 'Поставщики',
                        'url' => Url::current(['tab' => '_productSupplier']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productSupplier' ? ' active' : ''),
                            'data-tab' => '_productSupplier'
                        ],
                        'active' => ($tabActive == '_productSupplier')
                    ],
                    [
                        'label' => 'Изображение',
                        'url' => Url::current(['tab' => '_productImage']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productImage' ? ' active' : ''),
                            'data-tab' => '_productImage'
                        ],
                        'active' => ($tabActive == '_productImage')
                    ],
                    [
                        'label' => 'Описание',
                        'url' => Url::current(['tab' => '_productDescription']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productDescription' ? ' active' : ''),
                            'data-tab' => '_productDescription'
                        ],
                        'active' => ($tabActive == '_productDescription')
                    ],
                    [
                        'label' => 'Оборот',
                        'url' => Url::current(['tab' => '_productTurnover']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productTurnover' ? ' active' : ''),
                            'data-tab' => '_productTurnover'
                        ],
                        'active' => ($tabActive == '_productTurnover')
                    ],
                    [
                        'label' => 'Управление запасами',
                        'url' => Url::current(['tab' => '_productStocks']),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productStocks' ? ' active' : ''),
                            'data-tab' => '_productStocks'
                        ],
                        'active' => ($tabActive == '_productStocks')
                    ],
                ],
            ]); ?>

            <div class="pr-3 tab-content">
                <?= $this->render($tabActive, [
                    'model' => $model,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]) ?>
            </div>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?= $this->render('move_to_store', [
    'fromStoreList' => ArrayHelper::map($fromStoreArray, 'id', 'name'),
    'storeList' => ArrayHelper::map($storeArray, 'id', 'name'),
    'store' => null,
    'hint' => $hint
]); ?>

<!-- Create Invoice With Product -->
<?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
<?= Html::hiddenInput('selection[]', $model->id); ?>
<?= Html::endForm(); ?>
<script>
    $(document).on("click", ".add-to-invoice, .add-to-order", function(e) {
        e.preventDefault();
        $("#product_checker_form").attr("action", this.dataset.action).submit();
    });
    $(document).on("click", ".edit-product", function(e) {
        e.preventDefault();
        var activeTab = $(".nav-link.active").data('tab');
        var url = $(this).attr("href");
        if (activeTab)
            url += "&tab=" + activeTab;

        location.href = url;

        return false;
    });
</script>

<?php elseif ($modeActive == 'abc'): ?>

    <?= $this->render('_abc', [
        'model' => $model,
    ]) ?>

<?php endif; ?>
