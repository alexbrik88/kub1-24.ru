<?php

use common\models\product\Product;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

?>

<div class="pt-3 pb-2">
    <?= \common\components\helpers\Html::encode($model->comment ? : Product::DEFAULT_VALUE); ?>
</div>