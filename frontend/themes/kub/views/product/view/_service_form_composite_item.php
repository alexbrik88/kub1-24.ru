<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $simpleProduct common\models\product\Product */
/* @var $quantity int|float */

?>

<tr class="composite_product_item">
    <td class="composite_product_item_label py-2 px-3" style="width: 90%;">
        <?= Html::a(Html::encode($simpleProduct->title), [
            '/product/view',
            'productionType' => $simpleProduct->production_type,
            'id' => $simpleProduct->id,
            'data-pjax' => 0,
        ]) ?>
    </td>
    <td class="composite_product_item_price text-nowrap py-2 px-3 text-right" data-price="<?= intval($simpleProduct->price_for_buy_with_nds)/100 ?>">
        <?= TextHelper::invoiceMoneyFormat($simpleProduct->price_for_buy_with_nds, 2) ?>
    </td>
    <td class="composite_product_item_nds text-nowrap py-2 px-3 text-right">
        <?= $simpleProduct->priceForBuyNds->name ?? 'Без НДС' ?>
    </td>
    <td class="composite_product_item_inputs text-nowrap py-2 px-3 text-right">
        <?= +$quantity ?>
    </td>
    <td class="composite_product_item_cost_price text-nowrap py-2 px-3 text-right">
        <?= TextHelper::invoiceMoneyFormat($simpleProduct->price_for_buy_with_nds * $quantity, 2) ?>
    </td>
</tr>
