<?php
use yii\web\JsExpression;
/**
 * @var $searchModel
 * @var $chartId string
 * @var $x array
 * @var $y array
 * @var $labels array
 * @var $title string
 * @var $color string
 */

$showLabelX = (int)($labels['showOnPoint'] ?? true);
?>

<script>
    var chart_label_x_<?=($chartId)?> = <?= json_encode($labels['x']); ?>;
    var chart_label_category_<?=($chartId)?> = <?= json_encode($labels['category']); ?>;
</script>

<style>
    #chart_<?= $chartId ?> { height: 200px; border: 1px solid #e0f1f8; border-radius: 4px; }
    #chart_<?= $chartId ?> .highcharts-axis-labels { z-index: -1!important; }
</style>

<div class="ht-caption">
    <?= $title ?>
</div>

<div class="kub-chart">
<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart_'.$chartId,
    'scripts' => [
        //'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'credits' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'line',
            'events' => [],
        ],
        'legend' => [
            'enabled' => false
        ],
        'exporting' => [
            'enabled' => false
        ],
        'tooltip' => [
            'useHTML' => true,
            'shared' => false,
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'formatter' => new jsExpression("
                function(args) {
                    
                    var showLabelX = $showLabelX;
                    var index = this.point.index;
                    var labelX = window.chart_label_x_{$chartId}[index] || '?';
                    var labelCategory = window.chart_label_category_{$chartId}[index] || '?';
                    var content = '';
                    
                    content += '<span class=\'title\'>' + labelX + '</span>';
                    if (this.point.y >= 0) {
                        content += '<br/>' + '<span class=\'gray-text\'>' + args.chart.series[0].name + ': ' + '</span>' + '<b class=\'gray-text-b\'>' + Highcharts.numberFormat(this.point.y, 0, ',', ' ') + '</b>';
                    } else {
                        content += '<br/>' + '<span class=\'red-text\'>' + args.chart.series[0].name + ': ' + '</span>' + '<b class=\'red-text-b\'>' + Highcharts.numberFormat(this.point.y, 0, ',', ' ') + '</b>';
                    }
                    if (showLabelX)
                        content += '<br/>' + '<span class=\'gray-text\'>' + 'Категория' + ': ' + '</span>' + '<b class=\'gray-text-b\'>' + labelCategory + '</b>';
                                        
                    return content;                           
                }
            ")
        ],
        'title' => ['text' => ''],
        'yAxis' => [
            //'min' => 0,
            'index' => 0,
            'title' => '',
            'labels' => false,
            'minorGridLineWidth' => 0,
            'gridLineWidth' => 0
        ],
        'xAxis' => [
            'categories' => $x,
            'labels' => [
                'formatter' => new JsExpression("function() { 
                    return '<span class=\"x-axis\">' + this.value + '</span>'; 
                }"),
                'useHTML' => true,
            ],
            'minorGridLineWidth' => 0,
            'gridLineWidth' => 0
        ],
        'series' => [
            [
                'name' => $title,
                'data' => $y,
                'color' => $color,
                'negativeColor' => 'rgba(231,76,60,1)',
            ],
        ],
        'plotOptions' => [
            'series' => [
                'dataLabels' => [
                    'enabled' => $showLabelX,
                    'useHTML' => true,
                    'formatter' => new JsExpression("function() {
                        var index = this.point.index;
                        var labelCategory = window.chart_label_category_{$chartId}[index] || '?';                         
                        return labelCategory; 
                    }")
                ]
            ]
        ],
    ],
]); ?>
</div>
