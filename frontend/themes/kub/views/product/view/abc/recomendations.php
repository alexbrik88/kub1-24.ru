<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.06.2018
 * Time: 18:14
 */

use common\components\helpers\Html;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use \common\models\product\ProductABC;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $recomendations []
 * @var $searchModel ProductABC
 */

$searchModel->yearMonth = $searchModel->year . $searchModel->recommendationsMonth;
$products = $searchModel->search();
$searchModel->removeTmpTables();

if ($products) {
    $product = &$products[0];
} else {
    // no sells
    $product = [
        'abc' => "333",
        'variation_coefficient' => 100
    ];
}

if ($product) {
    $currentMonthRecomendations = ProductABC::getRecommendations($product['abc'], $product['variation_coefficient']);
} else {
    $currentMonthRecomendations = [];
}

$recomendations = [
    1 => [
        'title' => 'Неснижаемый остаток',
        'descr' => 'Сколько нужно на складе?',
        'msgKey' => 'irreducible_quantity_txt'
    ],
    2 => [
        'title' => 'Реклама',
        'descr' => 'Необходима или нет?',
        'msgKey' => 'advertising_txt'
    ],
    3 => [
        'title' => 'Цена Продажи',
        'descr' => 'Можно ли увеличить?',
        'msgKey' => 'selling_price_txt'
    ],
    4 => [
        'title' => 'Акции',
        'descr' => 'Купи 3 по цене 2-х',
        'msgKey' => 'action_txt'
    ],
    5 => [
        'title' => 'Скидки',
        'descr' => 'Делать или нет?',
        'msgKey' => 'discount_txt'
    ],
    6 => [
        'title' => 'Цена Закупки',
        'descr' => 'Нужно ли снижать?',
        'msgKey' => 'purchase_price_txt'
    ],
    7 => [
        'title' => 'Вывод из ассортимента',
        'descr' => 'Убрать или заменить аналогом?',
        'msgKey' => 'out_from_range_txt'
    ],
]

?>

<?php Pjax::begin([
    'id' => 'product-recommendations-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>
<?= Html::beginForm(\yii\helpers\Url::current(), 'GET', [
    'id' => 'recommendations-form',
    'validateOnChange' => true,
    'data-pjax' => 1
]); ?>
<div class="wrap pt-0 pl-0 pr-0 pb-0 mt-2">
    <div class="pt-2 pr-2 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">Рекомендации</h4>
            </div>
            <div class="column ml-auto" style="margin-top:-4px;">
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'recommendationsMonth',
                    'data' => $searchModel->getRecommendationsMonthFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
        </div>
        <hr class="devider mb-0 mt-1 border-color-light-grey">
        <div class="row">
            <?php $number = 1; ?>
            <?php foreach ($recomendations as $key => $data): ?>
                <div class="column warning-box pt-2 pb-4 col-4">
                    <div class="pb-2">
                        <div style="display: inline-block;">
                            <span class="pt-1 pb-1 pl-2 pr-2 mb-2 mt-1 button-regular button-regular_red" style="min-height: 44px; min-width: 52px;">
                                <?= \yii\helpers\Html::img("/img/abc/r_{$key}.svg", ['width' => ($key==5) ? 18 : ($key==3 ? 28 : 32)]); ?>
                            </span>
                        </div>
                        <div style="display: inline-block; vertical-align: top; padding-top: 17px;">
                            <span class="pl-2 bold"><?= $data['title'] ?></span><br/>
                        </div>
                        <div class="text-dark-alternative line-height-1-38">
                            <span class="pb-2 text-grey"><?= $data['descr'] ?></span><br/>
                            <?= ArrayHelper::getValue($currentMonthRecomendations, $data['msgKey']) ?: '-' ?>
                        </div>
                    </div>
                </div>
                <?php if ($key % 3 == 0): ?>
                    </div><hr class="devider mb-0 mt-1 border-color-light-grey"><div class="row">
                <?php endif; ?>
                <?php $number++; ?>
            <?php endforeach; ?>
        </div>
        <hr class="devider mb-0 mt-1 border-color-light-grey">
    </div>
</div>
<?= Html::endForm(); ?>
<?php Pjax::end(); ?>
<script>
    $(document).on('change', '#productabc-recommendationsmonth', function() {
        $('#recommendations-form').submit();
    });
</script>
