<?php

use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\reports\models\ProductAnalysisABC;
use common\components\TextHelper;
use common\components\helpers\ArrayHelper;
use frontend\widgets\TableConfigWidget;
use \common\models\product\ProductGroup;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisABC */
/* @var $data array */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_abc');

$colors = [
    ProductAnalysisABC::GROUP_A => 'rgba(38, 205, 88, 0.25)',
    ProductAnalysisABC::GROUP_B => 'rgba(200, 205, 38, 0.25)',
    ProductAnalysisABC::GROUP_C => 'rgba(227, 6, 17, 0.25)',
]
?>

<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_view_report_abc']) ?>
    </div>
    <div class="col-6"></div>
</div>

<div id="abc-analysis-grid">
    <div class="wrap wrap_padding_none_all" style="position:relative!important;">

        <div id="cs-table-2" class="custom-scroll-table-double cs-top">
            <table style="width: 1010px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
        </div>

        <!-- FIXED COLUMN -->
        <div id="cs-table-first-column">
            <table class="table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>

        <div id="cs-table-1" class="custom-scroll-table-double">
            <div class="table-wrap">
                <table class="table table-style table-count-list analysis-table <?= $tabViewClass ?>" aria-describedby="datatable_ajax_info" role="grid">
                    <thead>
                    <tr>
                        <th rowspan="2">
                            <?= $this->render('sort_arrows', ['title' => 'Период', 'attr' => 'period']) ?>
                        </th>
                        <th colspan="4" class="height-30">
                            <button id="collapse-cell-data"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-data"
                                    data-colspan-close="3"
                                    data-colspan-open="4">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Данные</span>
                            </button>
                        </th>
                        <th colspan="2">
                            <button id="collapse-cell-result"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-result"
                                    data-colspan-close="1"
                                    data-colspan-open="2">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Результат</span>
                            </button>
                        </th>
                        <th colspan="7">
                            <button id="collapse-cell-guidance"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-guidance"
                                    data-colspan-close="4"
                                    data-colspan-open="7">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Рекомендации</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button id="collapse-cell-store"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-store"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Склад</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button id="collapse-cell-order"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-order"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Заказ</span>
                            </button>
                        </th>
                        <th rowspan="2">Комментарий</th>
                    </tr>
                    <tr id="abc-analysis-grid-filters" class="filters">

                        <!--<th width="10%">Название</th>-->

                        <!-- DATA -->
                        <th width="10%" data-collapse-cell data-id="cell-data" class="height-30">
                            <?= $this->render('sort_arrows', ['title' => 'Выручка', 'attr' => 'turnover']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('sort_arrows', ['title' => 'Маржа ₽', 'attr' => 'margin']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('sort_arrows', ['title' => '% Маржи', 'attr' => 'margin_percent']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('sort_arrows', ['title' => 'Объем продаж', 'attr' => 'selling_quantity']) ?>
                        </th>

                        <!-- RESULTS -->
                        <th width="10%">
                            <?= $this->render('sort_arrows', ['title' => 'АВС', 'attr' => 'abc']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-result">
                            <?= $this->render('sort_arrows', ['title' => 'XYZ', 'attr' => 'variation_coefficient']) ?>
                        </th>

                        <!-- GUIDANCE -->
                        <th width="10%">
                            Наличие
                        </th>
                        <th width="10%">
                            Реклама
                        </th>
                        <th width="10%">
                            Скидки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Акции
                        </th>
                        <th width="10%">
                            Цена продажи
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Цена закупки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Вывод из ассортимента
                        </th>

                        <!-- STORE -->
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Остаток на начало
                        </th>
                        <th width="10%">
                            Остаток на конец
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Неснижаемый остаток
                        </th>

                        <!-- ORDER -->
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Мин. рекомендуе&shy;мый заказ (ед.)
                        </th>
                        <th width="10%">
                            Заказ (ед.)
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Сумма заказа
                        </th>

                    </tr>
                    </thead>
                    <?php if (!empty($data)): ?>
                    <tbody>
                        <?php $sortedData = $searchModel->sort($data); ?>
                        <?php foreach ($sortedData as $key => &$product): ?>
                            <?php $recommendations = $searchModel::getRecommendations($product['abc'], $product['variation_coefficient']); ?>
                            <?= $this->render('table_row', [
                                'key' => $key,
                                'product' => $product,
                                'colors' => $colors,
                                'recommendations' => $recommendations
                            ]); ?>
                        <?php endforeach; ?>

                    </tbody>
                    <?php else: ?>
                        <tr>
                            <td colspan="11">Нет данных за выбранный период</td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div>

    </div>
</div>

<?php
//$pjax->end();
?>

<?php

$this->registerJs(<<<JS

    _activeMCS = 0;
    _offsetMCSLeft = 0;
    _firstCSTableColumn = $('#cs-table-first-column');
    
    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS === 1)
                    window._activeMCS = 0;
                
                if (this.mcs.left === 0 && $(_firstCSTableColumn).is(':visible')) 
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS === 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible')) 
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });
    
    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });
    
    $("[data-collapse-trigger-abc]").click(function() {
    
        var collapseBtn = this;
    
        setTimeout(function() { 
            $("#cs-table-2 table").width($("#cs-table-1 table").width()); 
            $("#cs-table-2").mCustomScrollbar("update");
        }, 240);
        
        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).hasClass('active') ? $(collapseBtn).data('colspan-close') : $(collapseBtn).data('colspan-open');
    
            $(collapseBtn).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $('.analysis-table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
        };
    
        _collapseToggle(collapseBtn);
        
        $("#cs-table-1").mCustomScrollbar("update");
    });
    
    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width()); 
        $("#cs-table-2").mCustomScrollbar("update");
    });
JS
);

if (!empty($data)) {

    $this->registerJs(<<<JS

    var ProductAnalysisTable = {
      fillFixedColumn: function() {
    
          var tableBlock = $('#cs-table-1');
          var columnBlock = $('#cs-table-first-column');
          // CLEAR
          $(columnBlock).find('thead').html('');
          $(columnBlock).find('tbody').html('');
          $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());
    
          // CLONE FIRST COLUMN
          $('.analysis-table thead tr > th:first-child').each(function(i,v) {
              var col = $(v).clone();
              $('.table-fixed-first-column thead').append($('<tr/>').append(col));
    
              return false;
          });
          $('.analysis-table tbody tr > td:first-child').each(function(i,v) {
              var col = $(v).clone();
              var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
              var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
              $(col).find('.sortable-row-icon').html('');
              $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
          });
    
          // ADD "+" EVENTS
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-1').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });
          $('#cs-table-1 [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-first-column').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });
    
          // main.js
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $(this).toggleClass('active');
              if ( $(this).hasClass('active') ) {
                  $('[data-id="'+target+'"]').removeClass('d-none');
              } else {
                  // level 1
                  $('[data-id="'+target+'"]').addClass('d-none');
                  $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
              }
              if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                  $('[data-collapse-all-trigger]').removeClass('active');
              } else {
                  $('[data-collapse-all-trigger]').addClass('active');
              }
		  });
          $('#cs-table-first-column [data-collapse-all-trigger]').click(function() {
              var _this = $(this);
              var table = $(this).closest('.table');
              var row = table.find('tr[data-id]');
              _this.toggleClass('active');
              if ( _this.hasClass('active') ) {
                  row.removeClass('d-none');
                  $(table).find('tbody .table-collapse-btn').addClass('active');
                  // row.find('.table-collapse-btn').addClass('active');
              } else {
                  row.addClass('d-none');
                  $(table).find('tbody .table-collapse-btn').removeClass('active');
              }
          });
          // end main.js
          
        // ADD "+" EVENT TOGGLE ALL
        function tableFixedColumnToggleAll(syncTable, trigger) {
            var row = $(syncTable).find('tr[data-id]');
            console.log(syncTable, trigger)
            if ( $(trigger).hasClass('active') ) {
                row.removeClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').removeClass('active');
            }
        }
        $('#cs-table-first-column [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-1');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });
        $('#cs-table-1 [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-first-column');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });          

      }
    };

    // FIXED FIRST COLUMN
    $(document).ready(function() {
        ProductAnalysisTable.fillFixedColumn();
    });
JS
    );
}