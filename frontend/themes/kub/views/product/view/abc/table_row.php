<?php
/**
  * @var $key int
  * @var $product array
  * @var $recommendations array
 */

use common\components\TextHelper;
use frontend\modules\reports\models\AbstractFinance;
use yii\helpers\ArrayHelper; ?>

<tr data-key="<?= $key ?>">
    <!--Название-->
    <td class="col-name">
        <?= ArrayHelper::getValue(AbstractFinance::$month, substr($product['period'], -2, 2)) . ' ' . substr($product['period'], 0, 4); ?>
    </td>

    <!--Оборот-->
    <td class="text-right" data-collapse-cell data-id="cell-data">
        <?= TextHelper::moneyFormat(1/100 * $product['turnover'], 2) ?>
    </td>
    <!--Маржа Р-->
    <td class="text-right"  style="background-color: <?= null //$colors[substr($product['abc'], 0, 1)] ?? null ?>">
        <?= TextHelper::moneyFormat(1/100 * $product['margin'], 2) ?>
    </td>
    <!--% Маржи-->
    <td class="text-right"  style="background-color: <?= null //$colors[substr($product['abc'], 1, 1)] ?? null ?>">
        <?= TextHelper::moneyFormat(100 * $product['margin_percent'], 2) ?>
    </td>
    <!--Объем продаж-->
    <td class="text-right"  style="background-color: <?= null //$colors[substr($product['abc'], 2, 1)] ?? null ?>">
        <?= TextHelper::moneyFormat($product['selling_quantity'], 2) ?>
    </td>

    <!--АВС комбинация-->
    <td class="text-right">
        <?= str_replace([1,2,3], ['A', 'B', 'C'], $product['abc']) ?>
    </td>
    <!--XYZ значение-->
    <td class="text-right" data-collapse-cell data-id="cell-result" data-variation-coefficient="<?= $product['variation_coefficient'] ?>">
        <?php
        if ($product['variation_coefficient'] <= 10)
            echo 'X';
        elseif ($product['variation_coefficient'] > 10 && $product['variation_coefficient'] <= 25)
            echo 'Y';
        else
            echo 'Z';
        ?>
    </td>

    <!-- GUIDANCE -->
    <td>
        <?= ArrayHelper::getValue($recommendations, 'irreducible_quantity') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'advertising') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'discount') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'action') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'selling_price') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'purchase_price') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'out_from_range') ?>
    </td>
    <!-- STORE -->
    <td data-collapse-cell data-id="cell-store">
        <?= ArrayHelper::getValue($product, 'balance_at_start') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($product, 'balance_at_end') ?>
    </td>
    <td data-collapse-cell data-id="cell-store">
        <?= ArrayHelper::getValue($product, 'irreducible_quantity') ?>
    </td>

    <!-- ORDER -->
    <td data-collapse-cell data-id="cell-order">
    </td>
    <td>
    </td>
    <td data-collapse-cell data-id="cell-order">
    </td>

    <!--Комментарий-->
    <td>
    </td>

</tr>
