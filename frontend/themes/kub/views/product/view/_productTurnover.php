<?php

use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\ofd\OfdReceipt;
use frontend\components\StatisticPeriod;
use frontend\models\OlapProductSearch;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\document\GoodsCancellation;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
$period = StatisticPeriod::getSessionPeriod();
$searchModel = new OlapProductSearch([
    'company_id' => $model->company_id,
    'product_id' => $model->id,
    'periodFrom' => date_create($period['from']),
    'periodTo' => date_create($period['to']),
]);
//$searchModel::$productType
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();
$isGoods = ($model->production_type == Product::PRODUCTION_TYPE_GOODS);
$statisticData = $model->getQuantityStatisticForPeriod(date_create($period['from']), date_create($period['to']));
?>

<?php
//Pjax::begin([
//    'id' => 'productTurnover-pjax',
//    'timeout' => 10000,
//]); ?>

<div class="flex-column pt-3 pr-3 pb-2">
    <div class="wrap wrap_count pl-0 pr-0 pt-0 pb-0 mb-0">
        <div class="row">
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main"><?= ($isGoods) ? $statisticData['balance1']*1 : '—' ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Остаток на начало</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_turquoise wrap">
                    <div class="count-card-main"><?= $statisticData['purchase2']*1 ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Закуплено всего</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_grey wrap">
                    <div class="count-card-main"><?= $statisticData['sale2']*1 ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Продано всего</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main"><?= ($isGoods) ? $statisticData['balanceTotal']*1 : '—' ?></div>
                    <hr class="mt-3">
                    <div class="count-card-foot">Остаток на конец</div>
                </div>
            </div>
            <div class="count-card-column count-card-column_tabs col-6 flex-column flex-grow-1 flex-shrink-0">
                <div class="d-flex flex-column ml-auto mr-0" style="width: 100%; max-width: 250px;">
                    <?= frontend\widgets\RangeButtonWidget::widget([
                        'cssClass' => 'doc-gray-button btn_select_days btn_row',
                        'pjaxSelector' => '#productTurnover-pjax',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page-border pl-3 pr-3 pt-3 pb-0 mb-1 mr-1">
        <div class="pb-3">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                //'showFooter' => true,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                //'options' => [
                //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                //],

                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

                'columns' => [
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'footer' => 'Итого',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            return DateHelper::format($data->date, 'd.m.Y', 'Y-m-d');
                        },
                    ],
                    [
                        'attribute' => 'coming',
                        'label' => 'Закуплено',
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            $content = '';
                            if ($data->type == Documents::IO_TYPE_IN) {
                                $quantity = str_replace('.', ',', $data->quantity * 1);
                                $unitName = $data->product->unit->name ?? Product::DEFAULT_VALUE;
                                $totalAmount = TextHelper::invoiceMoneyFormat($data->total_amount);
                                $content .= '<b>'.$quantity . ' ' . $unitName.'</b>';
                                $content .= '<br>';
                                $content .= '<span class="text-grey">' . $totalAmount . ' ₽</span>';
                            }

                            return $content;
                        },
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                    ],
                    [
                        'attribute' => 'selling',
                        'label' => 'Продано',
                        'format' => 'html',
                        'value' => function ($data) use ($model) {
                            $content = '';
                            if ($data->type == Documents::IO_TYPE_OUT) {
                                $quantity = str_replace('.', ',', $data->quantity * 1);
                                $unitName = $data->product->unit->name ?? Product::DEFAULT_VALUE;
                                $totalAmount = TextHelper::invoiceMoneyFormat($data->total_amount);
                                $content .= '<b>'.$quantity . ' ' . $unitName.'</b>';
                                $content .= '<br>';
                                $content .= '<span class="text-grey">' . $totalAmount . ' ₽</span>';
                            }

                            return $content;
                        },
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                    ],
                    [
                        //'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'filter' => ['' => 'Все'],
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-ellipsis'
                        ],
                        's2width' => '300px',
                        'format' => 'html',
                        'value' => function ($data) {
                            $content = '';

                            if (($doc = $data->document) instanceof GoodsCancellation) {
                                $content .= 'Списание товара<br>';
                            } elseif ($contractor = $data->contractor) {
                                $inn = $contractor->ITN;
                                $kpp = $contractor->PPC;
                                $content .= Html::beginTag('div', ['style' => 'max-width:300px']).Html::a($contractor->getShortName(), [
                                    '/contractor/view',
                                    'type' => $contractor->type,
                                    'id' => $contractor->id,
                                ]) . '<br>' . '<span class="text-grey">ИНН ' . $contractor->ITN . ($contractor->PPC ? (', КПП ' . $contractor->PPC) : '') . '</span>'.Html::endTag('div');
                            } elseif (($doc = $data->document) instanceof OfdReceipt) {
                                $content .= 'Розничная продажа<br>';
                                if ($kkt = $doc->kkt) {
                                    $content .= 'Точка продажи: '.$kkt->name;
                                }
                            } elseif ($data->document_table == 'initial_balance') {
                                $content .= 'Начальные остатки';
                            }

                            return $content;
                        }
                    ],
                    [
                        'attribute' => 'basis',
                        'label' => 'Документ',
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) use ($model) {
                            $content = '';
                            if ($doc = $data->document) {
                                $name = Html::encode($doc->printablePrefix.' №'.$doc->getFullNumber());
                                if (isset($doc->urlPart)) {
                                    $content .= Html::a($name, [
                                        "/documents/{$doc->urlPart}/view",
                                        'type' => $doc->type,
                                        'id' => $doc->id,
                                    ]);
                                } elseif ($doc instanceof OfdReceipt) {
                                    $date = date_create($doc->date_time);
                                    $content .= Html::a($name, [
                                        "/retail/receipt/view",
                                        'id' => $doc->id,
                                    ], [
                                        'class' => 'ajax-modal-btn',
                                        'data-title' => "Чек № {$doc->document_number} от {$date->format("d.m.Y")} в {$date->format("H:i")}",
                                    ]);
                                } else {
                                    $content .= $name;
                                }

                                $content .= '<br>';
                                $content .= '<span class="text-grey">Дата ' . DateHelper::format($data->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . ', </span>';
                                $content .= '<span class="text-grey">Сумма ' . TextHelper::invoiceMoneyFormat($data->total_amount, 2) . ' ₽</span>';
                            }

                            return $content;
                        }
                    ],
                    [
                        'attribute' => 'invoice',
                        'label' => 'Счет',
                        'footer' => ' ',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            $content = "";
                            if (($doc = $data->document) instanceof OfdReceipt) {
                                $content .= 'Розничная продажа<br>';
                                $content .= 'Дата оплаты: '.date_create($doc->date_time)->format('d.m.Y');
                            } elseif (($doc = $data->document) instanceof GoodsCancellation) {
                                // empty cell
                            } elseif ($invoice = $data->invoice) {
                                $cashFlowData = [];
                                $invoiceStatus = null;
                                if ($invoice->cashBankFlows) {
                                    $dateArray = ArrayHelper::getColumn($invoice->cashBankFlows, 'date');
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по Банку';
                                }
                                if ($invoice->cashOrderFlows) {
                                    $dateArray = ArrayHelper::getColumn($invoice->cashOrderFlows, 'date');
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по Кассе';
                                }
                                if ($invoice->cashEmoneyFlows) {
                                    $dateArray = ArrayHelper::getColumn($invoice->cashEmoneyFlows, 'date');
                                    $cashFlowData[] = implode(', ', $dateArray) . ' по E-money';
                                }

                                $invoiceStatus = (
                                    $invoice->invoice_status_id == InvoiceStatus::STATUS_OVERDUE ||
                                    $invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL
                                ) ? '<span style="color:red">'.$invoice->invoiceStatus->name.'</span>' : (
                                    $invoice->getIsFullyPaid() ?
                                    '<span style="color:#26cd58">'.$invoice->invoiceStatus->name.'</span>' :
                                    $invoice->invoiceStatus->name
                                );
                                $name = Html::encode($invoice->getTitle());

                                $content .= Html::a($name, [
                                        "/documents/invoice/view",
                                        'type' => $data['type'],
                                        'id' => $data['invoice_id'],
                                    ], ['target' => '_blank', 'data-pjax' => 0]) . '<br/>';

                                if ($cashFlowData) {
                                    $content .= '<span class="text-grey">' . $invoiceStatus . '. ';
                                    $content .= implode(', ', $cashFlowData) . '</span>';
                                } elseif ($invoice->isOverdue()) {
                                    $content .= '<span class="text-grey">' . $invoiceStatus . '. ';
                                    $content .= 'Оплатить до ' . DateHelper::format($invoice->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . '</span>';
                                }
                            }

                            return $content;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php // Pjax::end(); ?>
