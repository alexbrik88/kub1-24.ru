<?php

use common\models\product\Product;
use common\models\product\ProductCategory;
use common\models\product\ProductCategoryField;
use common\models\product\ProductCategoryItem;
use common\models\product\ProductField;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;

$categoryNames = Product::getCategoryNames();
$categoryFields = Product::getCategoryFieldNames();
$categoryItems = $model->getCategoryFieldValues();
$productField = ProductField::findOne(['company_id' => $model->company_id]);
?>

<div class="row pt-3">
    <div class="col-12">
        <div class="row">
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Группа <?= ($isGoods) ? 'товара' : 'услуги' ?></div>
                <div><?= $model->group ? Html::encode($model->group->title) : Product::DEFAULT_VALUE; ?></div>
            </div>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3">Категория</div>
                <div><?= $model->category ? Html::encode($model->category->name) : '---' ?></div>
            </div>
            <?php if ($isGoods): ?>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Товар подлежит прослеживаемости</div>
                    <div><?= $model->is_traceable ? 'Да' : 'Нет' ?></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($isGoods): ?>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Код</div>
                    <div><?= Html::encode($model->code ?: Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Артикул</div>
                    <div><?= Html::encode($model->article ?: Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Штрих код</div>
                    <div><?=  Html::encode($model->barcode ?: Product::DEFAULT_VALUE); ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Единица измерения</div>
                    <div><?= $model->productUnit ? $model->productUnit->name : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Вес (кг)</div>
                    <div><?= $model->weight ? $model->weight : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Объем (м3)</div>
                    <div><?= $model->volume ? $model->volume : Product::DEFAULT_VALUE; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Вид упаковки</div>
                    <div><?= Html::encode($model->box_type ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <?php /*
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Количество в одном месте</div>
                    <div><?= Html::encode($model->count_in_place ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Количество мест, штук</div>
                    <div><?= Html::encode($model->place_count ? : Product::DEFAULT_VALUE); ?></div>
                </div>*/ ?>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Количество в упаковке</div>
                    <div><?= Html::encode($model->count_in_package ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Масса нетто</div>
                    <div><?= Html::encode($model->mass_net ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Масса брутто</div>
                    <div><?= Html::encode($model->mass_gross ? : Product::DEFAULT_VALUE); ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Страна происхождения товара</div>
                    <div><?= $model->countryOrigin ? $model->countryOrigin->name_short : Product::DEFAULT_VALUE; ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Номер таможенной декларации</div>
                    <div><?= Html::encode($model->customs_declaration_number ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Код вида товара</div>
                    <div><?= Html::encode($model->item_type_code ? : Product::DEFAULT_VALUE); ?></div>
                </div>
                <?php if ($productField): ?>
                    <div class="col-3 mb-4 pb-2">
                        <div class="label weight-700 mb-3"><?= $productField->title ?></div>
                        <div><?= Html::encode($model->custom_field_value ? : Product::DEFAULT_VALUE); ?></div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-9 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Название на английском языке (Name in English)</div>
                    <div><?= $model->title_en ? Html::encode($model->title_en) : '---' ?></div>
                </div>
                <div class="col-3 mb-4 pb-2">
                    <div class="label weight-700 mb-3">Код ТН ВЭД</div>
                    <div><?= $model->code_tn_ved ? Html::encode($model->code_tn_ved) : '---' ?></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if ($model->category_id): ?>

    <?php

    foreach ($categoryNames as $categoryId => $categoryName): ?>
    <div class="row category-field-group <?= $model->category_id == $categoryId ? '' : 'hide' ?>" data-category="<?= $categoryId ?>">
        <?php foreach ($categoryFields[$categoryId] as $fieldId => $fieldName): ?>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3" for=""><?= $fieldName ?></div>
                <div><?= $categoryItems[$fieldId] ?? "" ?></div>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endforeach; ?>

<?php endif; ?>
