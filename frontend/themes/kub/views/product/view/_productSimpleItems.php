<?php

use common\models\product\Product;
use frontend\components\Icon;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\product\Product */

$servicesMaps = $goodsMaps = [];
foreach ($model->compositeMaps as $compositeMap) {
    $simpleProduct = $compositeMap->simpleProduct;
    if ($simpleProduct->production_type == Product::PRODUCTION_TYPE_SERVICE) {
        $servicesMaps[] = $compositeMap;
    } elseif ($simpleProduct->production_type == Product::PRODUCTION_TYPE_GOODS) {
        $goodsMaps[] = $compositeMap;
    }
}
?>

<div class="nav-tabs-row">
    <ul class="nav nav-tabs nav-tabs_border_transparent w-100 mr-3" id="myTabIn" role="tablist">
        <li class="nav-item">
            <a id="simple-services-tab"
                class="nav-link active"
                data-toggle="tab"
                href="#simple-services-items"
                role="tab"
                aria-controls="simple-services-items"
                aria-selected="true">
                Услуги/Работы
            </a>
        </li>
        <li class="nav-item">
            <a id="simple-goods-tab"
                class="nav-link"
                data-toggle="tab"
                href="#simple-goods-items"
                role="tab"
                aria-controls="simple-goods-items"
                aria-selected="true">
                Материалы
            </a>
        </li>
    </ul>
</div>
<div class="tab-content" id="nav-tabContent" style="width: 100%;">
    <div class="tab-pane fade show active" id="simple-services-items" role="tabpanel" aria-labelledby="simple-services-tab">
        <div class="page-border p-3">
            <div class="composite_product_simple_items mt-2">
                <table class="table table-style mb-0 table-price">
                    <thead>
                        <tr>
                            <th class="text-nowrap py-2 px-3">Название услуги/работы, входящей в составную услугу</th>
                            <th class="text-nowrap py-2 px-3">Цена покупки</th>
                            <th class="text-nowrap py-2 px-3">НДС</th>
                            <th class="text-nowrap py-2 px-3">Кол-во</th>
                            <th class="text-nowrap py-2 px-3">Итого затраты</th>
                        </tr>
                    </thead>
                    <tbody class="exist_item_list">
                        <?php foreach ($servicesMaps as $item) : ?>
                            <?= $this->render('_service_form_composite_item', [
                                'simpleProduct' => $item->simpleProduct,
                                'quantity' => $item->quantity,
                            ]) ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="simple-goods-items" role="tabpanel" aria-labelledby="simple-goods-tab">
        <div class="page-border p-3">
            <div class="composite_product_simple_items mt-2">
                <table class="table table-style mb-0 table-price">
                    <thead>
                        <tr>
                            <th class="text-nowrap py-2 px-3">Название материала, входящего в составную услугу</th>
                            <th class="text-nowrap py-2 px-3">Цена покупки</th>
                            <th class="text-nowrap py-2 px-3">НДС</th>
                            <th class="text-nowrap py-2 px-3">Кол-во</th>
                            <th class="text-nowrap py-2 px-3">Итого затраты</th>
                        </tr>
                    </thead>
                    <tbody class="exist_item_list">
                        <?php foreach ($goodsMaps as $item) : ?>
                            <?= $this->render('_service_form_composite_item', [
                                'simpleProduct' => $item->simpleProduct,
                                'quantity' => $item->quantity,
                            ]) ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
