<?php

use yii\helpers\Json;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $data array */

$jsonData = Json::encode($data);
?>

<?php Pjax::begin([
    'id' => 'simple_product_form_pjax',
    'linkSelector' => false,
    'formSelector' => '#simple_product_form',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<script type="text/javascript">
    window.createdCompositeItemData = <?= $jsonData ?>;
    $('#simple_product_form_pjax').trigger('createdCompositeItemSuccess');
</script>

<?php Pjax::end(); ?>
