<?php

use common\models\product\Product;
use frontend\components\Icon;
use frontend\models\Documents;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<h4 class="modal-title"><?= $title ?? ''; ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <?= Icon::get('close') ?>
</button>

<?php Pjax::begin([
    'id' => 'simple_product_form_pjax',
    'linkSelector' => false,
    'formSelector' => '#simple_product_form',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'simple_product_form',
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

    <div class="form-body">
        <?= $this->render('@frontend/modules/documents/views/invoice/partial/_mainForm', [
            'model' => $model,
            'form' => $form,
            'documentType' => Documents::IO_TYPE_OUT,
            'document' => null,
            'types' => false,
        ]); ?>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'id' => 'product-form-submit',
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            ]) ?>
            <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
        </div>

    </div>

<?php ActiveForm::end() ?>

<?php Pjax::end(); ?>
