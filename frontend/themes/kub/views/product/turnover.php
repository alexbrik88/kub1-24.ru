<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\DataColumn;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\product\ProductSearch;
use frontend\widgets\RangeButtonWidget;
use yii\widgets\ActiveForm;
use \common\models\product\ProductInitialBalance;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */

//$this->title = Product::$productionTypes[$productionType].', оборот.';
$this->title = 'Оборот товаров';
$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Количество товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Количество услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';

if (!isset($turnoverType))
    $turnoverType = Yii::$app->request->getQueryParam('turnoverType');

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_product_turnover');

$tableConfigItems = [
    [
        'attribute' => 'turnover_group_id',
        'label' => 'Группа товара',
    ],
    [
        'attribute' => 'turnover_unit_id',
        'label' => 'Ед.измерения',
    ],
];
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $showItems = [
        [
            'attribute' => 'product_zeroes_turnover',
            'label' => 'Товары с нулевыми оборотами',
        ]
    ];
} else {
    $showItems = [];
}
?>

<div class="stop-zone-for-fixed-elems product-index">
    <div class="page-head d-flex flex-wrap align-items-center">

        <h4><?= Html::encode($this->title) . ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? ' в закупочных ценах ₽' : null); ?></h4>

        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE)): ?>
            <a href="<?= Url::toRoute(['create', 'productionType' => $productionType]) ?>"
               class="button-regular button-regular_padding_medium button-regular_red ml-auto mb-2">
                <svg class="svg-icon mr-2">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span class="ml-1">Добавить</span>
            </a>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="count-card-column col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceStart(), 2); ?>
                        <?php else: ?>
                            <?= TextHelper::numberFormat($searchModel->getBalanceStart()) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Остаток на начало периода</div>
                    <hr>
                    <div class="count-card-foot"><?= $searchModel->dateStart->format('d.m.Y') ?></div>
                </div>
            </div>
            <div class="count-card-column col-6">
                <div class="count-card count-card_turquoise wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceIn(), 2); ?>
                        <?php else: ?>
                            <?= TextHelper::numberFormat($searchModel->getBalanceIn()) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Закуплено всего</div>
                    <hr>
                    <div class="count-card-foot">&nbsp;</div>
                </div>
            </div>
            <div class="count-card-column col-6">
                <div class="count-card count-card_grey wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceOut(), 2); ?>
                        <?php else: ?>
                            <?= TextHelper::numberFormat($searchModel->getBalanceOut()) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Продано всего</div>
                    <hr>
                    <div class="count-card-foot">&nbsp;</div>
                </div>
            </div>
            <div class="count-card-column col-6">
                <div class="count-card count-card_yellow wrap">
                    <div class="count-card-main">
                        <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                            <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceEnd(), 2) ?>
                        <?php else: ?>
                            <?= TextHelper::numberFormat($searchModel->getBalanceEnd()) ?>
                        <?php endif; ?>
                    </div>
                    <div class="count-card-title">Остаток на конец периода</div>
                    <hr>
                    <div class="count-card-foot"><?= $searchModel->dateEnd->format('d.m.Y') ?></div>
                </div>
            </div>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>

    <?= Html::beginForm(['turnover', 'productionType' => $productionType, 'turnoverType' => $turnoverType], 'GET'); ?>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <div class="row align-items-center">
                <div class="sub-title column" style="padding-top:10px;"><?= $tableHeader[$productionType] ?>: <strong><?= $dataProvider->totalCount; ?></strong></div>
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                    <?= Html::a(Icon::get('exel'), Url::current(['xls' => 1]), [
                        'class' => 'get-xls-link button-regular button-regular_red button-clr w-44 mr-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
                    <?= TableConfigWidget::widget([
                        'items' => $tableConfigItems,
                        'showItems' => $showItems,
                        'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2'
                    ]); ?>
                    <?= TableViewWidget::widget(['attribute' => 'table_view_product_turnover']) ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'title', [
                        'type' => 'search',
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list table-responsive' . $tabViewClass,
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],

        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'showFooter' => false,
        'footerRowOptions' => ['class' => 'padding-left-10'],
        'columns' => [
            [
                'attribute' => 'title',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'footer' => 'Итого единиц ',
                'value' => function ($data) use ($searchModel) {
                    $title = Html::encode($data['title']);
                    $content = (Yii::$app->user->can(\frontend\rbac\permissions\Product::VIEW)) ? Html::a($title, [
                        'view',
                        'productionType' => $searchModel->production_type,
                        'id' => $data['id'],
                    ]) : $title;

                    return Html::tag('div', $content, ['class' => 'product-title-wide-cell']);
                },
            ],
            [
                'attribute' => 'group_id',
                'enableSorting' => false,
                'label' => 'Группа товара',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_turnover_group_id' . ($userConfig->turnover_group_id ? '' : ' hidden'),
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'col_turnover_group_id' . ($userConfig->turnover_group_id ? '' : ' hidden'),
                ],
                'filter' => ArrayHelper::merge([null => 'Все'], $searchModel->getGroupFilter()),
                'value' => function ($data) use ($searchModel) {
                    return $searchModel->getGroupFilter()[$data['group_id']] ?? '';
                },
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                's2width' => '200px'
            ],
            [
                'attribute' => 'product_unit_id',
                'enableSorting' => false,
                'label' => 'Ед.измерения',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_turnover_unit_id' . ($userConfig->turnover_unit_id ? '' : ' hidden'),
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'col_turnover_unit_id' . ($userConfig->turnover_unit_id ? '' : ' hidden'),
                ],
                'filter' => ArrayHelper::merge([null => 'Все'], $searchModel->getUnitFilter()),
                'value' => function ($data) use ($searchModel) {
                    return $searchModel->getUnitFilter()[$data['product_unit_id']] ?? '';
                },
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE || $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? false : true),
            ],
            [
                'attribute' => 'balance_start',
                'label' => 'Остаток на начало периода',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model['balance_start'] * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'balance_in',
                'label' => 'Закуплено',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model['balance_in'] * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'balance_out',
                'label' => 'Продано',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model['balance_out'] * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
            [
                'attribute' => 'balance_end',
                'label' => 'Остаток на конец периода',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) use ($searchModel) {
                    $result = $model['balance_end'] * 1;

                    return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                },
            ],
        ],
    ]); ?>

</div>