<?php

use common\models\TaxRate;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\product\Product */
/* @var $compositeModel frontend\models\CompositeServiceForm|null */

$taxItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');
$units = ProductUnit::findSorted()->andWhere(['services' => 1])->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}
?>

<?php $form = ActiveForm::begin([
    'id' => 'product-service-form',
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'data-pjax' => true,
    ],
]); ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'title')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'group_id')->widget(ProductGroupDropdownWidget::class, [
                'loadAssets' => false,
                'productionType' => Product::PRODUCTION_TYPE_SERVICE,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'price_for_sell_with_nds', [
                'options' => [
                    'class' => 'form-group required',
                ],
            ])->textInput([
                'class' => 'product-form-price-input form-control js_input_to_money_format',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'price_for_sell_nds_id')->radioList($taxItems, [
                'class' => 'd-flex mx-n2 pt-2',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, ['class' => 'px-2']);
                },
            ])->label('Включая НДС'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'product_unit_id')->widget(Select2::class, [
                'data' => ArrayHelper::map($units, 'id', 'name'),
                'options' => [
                    'prompt' => '',
                    'options' => $unitsOptions,
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
    </div>

    <?php if (isset($compositeModel)) : ?>
        <?= $this->render('_service_form_composite', [
            'company' => $company,
            'form' => $form,
            'model' => $model,
            'compositeModel' => $compositeModel,
        ]) ?>
    <?php else : ?>
        <?= $this->render('_service_form_additional', [
            'company' => $company,
            'form' => $form,
            'model' => $model,
            'taxItems' => $taxItems,
        ]) ?>
    <?php endif ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'id' => 'product-form-submit',
            'class' => 'button-regular button-width button-regular_red ladda-button',
        ]) ?>
        <button type="button" class="button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

<?php ActiveForm::end() ?>
