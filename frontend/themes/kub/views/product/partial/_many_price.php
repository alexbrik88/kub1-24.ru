<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $productionType int */

$model = new ProductPriceForm($company);
$unitTemplate = "{label}<div class=\"form-filter\">\n{input}\n{hint}\n{error}\n</div>";
$unitConf = [
    'template' => $unitTemplate,
    'labelOptions' => [
        'class' => 'label'
    ]
];

$inputTemplate1 = "<div class=\"price-inp-wrap\">\n<label class=\"label\">Цена покупки</label>\n{input}\n</div>\n{hint}\n{error}";
$inputConf1 = [
    'template' => $inputTemplate1,
];
$inputTemplate11 = "<div class=\"price-inp-wrap\">\n<label class=\"label\">Цена продажи</label>\n{input}\n</div>\n{hint}\n{error}";
$inputConf11 = [
    'template' => $inputTemplate11,
];
$inputTemplate2 = "<div class=\"price-inp-wrap\">\n<label class=\"label\">на</label>\n{input}\n</div>\n{hint}\n{error}";
$inputConf2 = [
    'template' => $inputTemplate2,
];

$this->registerJs('
var resetManyPriceForm = function() {
    $("#productpriceform-unit input:radio").prop("checked", false).uniform("refresh");
    $("#many-price-form select, #many-price-form :text").val("").prop("disabled", true);
    $("#productpriceform-form_error").val("")
    $("#product-id-inputs").html("");
    $("#many-price-form").yiiActiveForm("resetForm");
}
$(document).on("show.bs.modal", "#many-price-modal", function() {
    resetManyPriceForm();
    $("input.product_checker:checked").each(function(i, item) {
        $(item).clone().attr({
            "type" : "hidden",
            "name" : "ProductPriceForm[product_id][]",
        }).appendTo("#product-id-inputs");
    });
});
$(document).on("hide.bs.modal", "#many-price-modal", function() {
    resetManyPriceForm();
});
$(document).on("change", "#productpriceform-unit input:radio", function() {
    $("#many-price-form select").prop("disabled", false);
    $("#many-price-form .change_value_units").html(
        $("#productpriceform-unit input:radio:checked").val() == "1" ? "%" : "руб."
    );
});
$(document).on("change", "#productpriceform-buy_type, #productpriceform-sell_type", function() {
    $("#productpriceform-form_error")
        .val($("#productpriceform-buy_type").val() + $("#productpriceform-sell_type").val())
        .trigger("change");
    var changeValue = $(this).closest(".input_group").find(".change_value input:text");
    if (!$(this).val()) {
        changeValue.val("").prop("disabled", true);
        $("#many-price-form").yiiActiveForm("validateAttribute", changeValue.attr("id"));
    } else {
        changeValue.prop("disabled", false);
    }
});
$(document).on("keyup", "#many-price-form :text", function() {
    var value = $(this).val().replace(/,/g, ".").replace(/[^\d.]/g, "").match( /\d+(\.\d{0,2})?/ );
    $(this).val(value ? value[0] : "");
});
');
?>

<?php Modal::begin([
    'id' => 'many-price-modal',
    'options' => [
        'data-url' => Url::to(['many-price', 'productionType' => $productionType]),
    ]
]); ?>

    <h4 class="modal-title">Изменить цены по выбранным <?= ($productionType == 1 ? 'товарам' : 'услугам') ?></h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>

    <?php $form = ActiveForm::begin([
        'id' => 'many-price-form',
        'action' => ['many-price', 'productionType' => $productionType],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <div class="form-group">
        <?= $form->field($model, 'unit', $unitConf)->radioList(ProductPriceForm::$unitItems, [
            'class' => 'd-flex flex-wrap',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value'  => $value,
                    'label'  => '<span class="radio-txt-bold">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'label mr-3 mt-2',
                    ],
                ]);
            }
        ]); ?>
    </div>

    <div class="form-group">
        <div class="row input_group">
            <div class="col-6 change_type" style="padding-right: 0;">
                <?= $form->field($model, 'buy_type', $inputConf1)->label(false)->widget(Select2::class, [
                    'data' => ProductPriceForm::$typeItems,
                    'disabled' => true,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="col-6 change_value">
                <?= $form->field($model, 'buy_value', $inputConf2)->label(false)->textInput([
                    'disabled' => true,
                ]); ?>
                <div class="change_value_units"></div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row input_group">
            <div class="col-6 change_type" style="padding-right: 0;">
                <?= $form->field($model, 'sell_type', $inputConf11)->label(false)->widget(Select2::class, [
                    'data' => ProductPriceForm::$typeItems,
                    'disabled' => true,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="col-6 change_value">
                <?= $form->field($model, 'sell_value', $inputConf2)->label(false)->textInput([
                    'disabled' => true,
                ]); ?>
                <div class="change_value_units"></div>
            </div>
        </div>
    </div>

    <div id="product-id-inputs"></div>

    <?= $form->field($model, 'form_error')->label(false)->hiddenInput(); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data' => [
                'url' => Url::to(['/product/to-store']),
            ],
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>