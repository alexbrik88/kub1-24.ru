<?php

use common\models\product\Product;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\product\Product */
/* @var $compositeModel frontend\models\CompositeServiceForm */

$canAdd = Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE);
?>

<style type="text/css">
    .composite_product_item_price,
    .composite_product_item_nds,
    .composite_product_item_cost_price {
        display: none;
    }
</style>
<div class="row">
    <div class="col-6 composite_product_simple_items form-group required">
        <?= $form->field($compositeModel, 'services', ['options' => ['class' => '']])->hiddenInput(['value' => '']) ?>
        <table style="width: 100%;">
            <tbody class="exist_item_list">
                <?php foreach ($compositeModel->servicesMaps as $item) : ?>
                    <?= $this->render('_composite_product_simple_item', [
                        'company' => $company,
                        'compositeModel' => $compositeModel,
                        'canAdd' => $canAdd,
                        'simpleProduct' => $item->simpleProduct,
                        'quantity' => $item->quantity,
                    ]) ?>
                <?php endforeach ?>
            </tbody>
        </table>
        <div class="composite_product_item_select hidden">
            <?= $this->render('_product_composite_item_select', [
                'company' => $company,
                'canAdd' => $canAdd,
                'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            ]) ?>
        </div>
        <?= Html::button(Icon::get('add-icon').'<span>Добавить</span>', [
            'class' => 'button-regular button-hover-content-red show_product_composite_item_select',
        ]) ?>
    </div>
    <div class="col-6 composite_product_simple_items">
        <?= $form->field($compositeModel, 'goods', ['options' => ['class' => '']])->hiddenInput(['value' => '']) ?>
        <table style="width: 100%;">
            <tbody class="exist_item_list">
                <?php foreach ($compositeModel->goodsMaps as $item) : ?>
                    <?= $this->render('_composite_product_simple_item', [
                        'company' => $company,
                        'compositeModel' => $compositeModel,
                        'simpleProduct' => $item->simpleProduct,
                        'quantity' => $item->quantity,
                    ]) ?>
                <?php endforeach ?>
            </tbody>
        </table>
        <div class="composite_product_item_select hidden">
            <?= $this->render('_product_composite_item_select', [
                'company' => $company,
                'canAdd' => $canAdd,
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
            ]) ?>
        </div>
        <?= Html::button(Icon::get('add-icon').'<span>Добавить</span>', [
            'class' => 'button-regular button-hover-content-red show_product_composite_item_select',
        ]) ?>
    </div>
</div>