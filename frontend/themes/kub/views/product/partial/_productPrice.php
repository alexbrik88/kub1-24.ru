<?php

use common\components\TextHelper;
use common\models\Contractor;
use common\models\ContractorAgentPaymentType;
use common\models\product\ProductToSupplier;
use common\models\TaxRate;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\product\Product;
use common\models\product\CustomPrice;
use common\models\product\CustomPriceGroup;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$taxType = $model->company->companyTaxationType;
$taxItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');

$checkbox = Html::label('Не для продажи ' . Html::activeCheckbox($model, 'not_for_sale', ['label' => false]), null, [
    'class' => 'control-label bold-text content-right',
    'style' => 'width: 100%; text-align: right !important;',
]);
$notForSaleTemplate = "{label}\n{beginWrapper}\n<div class=\"row\">\n";
$notForSaleTemplate .= "<div class=\"col-xs-6\">{input}</div>\n";
$notForSaleTemplate .= "<div class=\"col-xs-6\">{$checkbox}</div>\n";
$notForSaleTemplate .= "</div>\n{error}\n{hint}\n{endWrapper}\n";
$notForSaleConf = [
    'template' => $notForSaleTemplate,
];

$priceTemplate = ['options' => ['class' => 'form-control form-control_width_110']];
$number = 0;

$customPrices = $model->loadedCustomPrices ?: $model->customPrices;
?>

<div class="flex-column">
    <div class="page-border pl-3 pr-3 pt-3">
        <div class="pb-3">
            <div class="pb-3">
                <table class="table table-style mb-0 table-price">
                    <thead>
                        <tr>
                            <th class="pl-1" style="width: 45%">Название цены</th>
                            <th style="width: 20%">Цена ₽</th>
                            <th style="width: 10%">НДС</th>
                            <th style="width: 10%">Маржа %</th>
                            <th style="width: 10%">Маржа ₽</th>
                            <th style="width: 5%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($model->unit_type == Product::UNIT_TYPE_SIMPLE) : ?>
                            <tr>
                                <td>Цена покупки</td>
                                <td>
                                    <?= $form->field($model, 'price_for_buy_with_nds')->textInput([
                                        'value' => $canViewPriceForBuy ?
                                            ($model->isNewRecord && !$model->price_for_buy_with_nds ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds)) :
                                            ($model->isNewRecord && !$model->price_for_buy_with_nds ? '0' : Product::DEFAULT_VALUE),
                                        'disabled' => !$canViewPriceForBuy,
                                        'placeholder' => 'Цена',
                                        'class' => 'form-control js_input_to_money_format buy_input',
                                    ]); ?>
                                </td>
                                <td>
                                    <?= $form->field($model, 'price_for_buy_nds_id')->widget(Select2::class, [
                                        'data' => $taxItems,
                                        'options' => [
                                            'placeholder' => '%',
                                        ],
                                        'pluginOptions' => [
                                            'minimumResultsForSearch' => -1,
                                            'width' => '100%'
                                        ],
                                    ]); ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td>&nbsp;</td>
                            </tr>
                        <?php endif ?>
                        <tr>
                            <td>Цена продажи</td>
                            <td>
                                <?= $form->field($model, 'price_for_sell_with_nds')->textInput([
                                    'placeholder' => 'Цена',
                                    'class' => 'form-control js_input_to_money_format for_sale_input sell_input',
                                    'disabled' => (boolean)$model->not_for_sale,
                                    'value' => ($model->isNewRecord && !$model->price_for_sell_with_nds) ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds)
                                ]); ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'price_for_sell_nds_id')->widget(Select2::class, [
                                    'data' => $taxItems,
                                    'options' => [
                                        'placeholder' => '%',
                                    ],
                                    'pluginOptions' => [
                                        'minimumResultsForSearch' => -1,
                                        'width' => '100%'
                                    ],
                                ]); ?>
                            </td>
                            <td class="margin-percent"></td>
                            <td class="margin"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php if ($model->unit_type == Product::UNIT_TYPE_SIMPLE) : ?>
                            <?php foreach ($customPrices as $cp): ?>
                                <?php $baseName = 'custom_price[' . $number . ']'; ?>
                                <tr class="custom-price-block group-<?= $cp->price_group_id ?>">
                                    <td><?= $cp->priceGroup->name ?></td>
                                    <td>
                                        <input type="text" name="<?= $baseName . '[price]' ?>" value="<?= TextHelper::moneyFormatFromIntToFloat($cp->price) ?>" class="form-control sell_input js_input_to_money_format">
                                    </td>
                                    <td>
                                        <?= Select2::widget([
                                            'name' => $baseName . '[nds_id]',
                                            'data' => $taxItems,
                                            'options' => [
                                                'placeholder' => '',
                                            ],
                                            'value' => $cp->nds_id,
                                            'pluginOptions' => [
                                                'minimumResultsForSearch' => -1,
                                                'width' => '100%'
                                            ],
                                        ]); ?>
                                    </td>
                                    <td class="margin-percent"></td>
                                    <td class="margin"></td>
                                    <td>
                                        <a href="javascript:;" class="remove-price-group link mb-2 ml-1" type="button" data-price_group_id="<?= $cp->price_group_id ?>">
                                            <svg class="svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#garbage"></use>
                                            </svg>
                                        </a>
                                        <input type="hidden" name="<?= $baseName . '[id]' ?>" value="<?= $cp->price_group_id ?>">
                                    </td>
                                </tr>
                                <?php $number++ ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php if ($model->unit_type == Product::UNIT_TYPE_SIMPLE) : ?>
            <div class="pl-1 pr-1">
                <div class="mb-3">
                    <div class="checkbox">
                        <?= Html::activeCheckbox($model, 'not_for_sale', ['label' => false]) ?>
                        <label class="checkbox-label" for="template1">
                            <span class="checkbox-txt ml-2">Не для продажи</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="pt-3 pb-3 new-price-add">
                <button class="button-regular button-clr button-hover-content-red button-width collapsed" type="button" data-toggle="collapse" data-target="#addCustomPrice" aria-expanded="false" aria-controls="addCustomPrice">
                    <svg class="svg-icon mr-2">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-1">Добавить</span>
                </button>
            </div>
            <div class="collapse" id="addCustomPrice">
                <div class="pt-3 pb-3">
                    <div class="page-border p-4">
                        <div class="pt-2 pl-2 pr-2">
                            <div class="row">
                                <div class="form-group mb-0 col-12">
                                    <div class="row">
                                        <div class="form-filter col-6 mb-4 pb-2">
                                            <label for="testTabsInput" class="label">Название цены <span class="important">*</span></label>
                                            <?php echo \frontend\widgets\ProductPriceDropdownWidget::widget([
                                                'id' => 'custom_price_group',
                                                'name' => 'custom_price_group',
                                                //'data' => ArrayHelper::map(CustomPriceGroup::find()->where(['company_id' => $model->company_id])->all(), 'id', 'name'),
                                                'hideSearch' => true,
                                                'options' => [
                                                    'class' => 'form-control',
                                                    'placeholder' => '',
                                                ],
                                                'pluginOptions' => [
                                                    'width' => '100%',
                                                    'language' => [
                                                        'noResults' =>  new \yii\web\JsExpression('
                                                            function(){ return $("<span />").html("Вы еще не добавили ни одной цены"); }
                                                        ')
                                                    ]
                                                ],
                                            ]); ?>
                                            <p class="help-block-empty help-block help-block-error hidden">Необходимо заполнить «Название цены».</p>
                                            <p class="help-block-present help-block help-block-error hidden">Цена уже есть в списке.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row justify-content-between">
                                        <div class="form-group column mb-2">
                                            <button class="add-custom-price button-clr button-regular button-regular_red button-width_123" type="button" data-number="<?= $number ?>">Добавить</button>
                                        </div>
                                        <div class="form-group column mb-2">
                                            <button class="button-clr button-regular button-hover-grey button-width_123" type="button" data-toggle="collapse" data-target="#addCustomPrice" aria-expanded="false" aria-controls="addCustomPrice">Отменить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- template -->
<div style="display: none!important;">
    <table>
    <tr class="custom-price-block custom-price-template">
        <td class="cp-name"></td>
        <td class="cp-price">
            <input type="text" name="new_custom_price_name" value="" class="form-control sell_input js_input_to_money_format">
        </td>
        <td class="cp-nds">
            <?= Html::dropDownList('new_custom_price_nds',null, $taxItems, ['id' => 'new_custom_price_nds']); ?>
        </td>
        <td class="margin-percent"></td>
        <td class="margin"></td>
        <td>
            <a href="javascript:;" class="remove-price-group link mb-2 ml-1" type="button" data-price_group_id="0">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#garbage"></use>
                </svg>
            </a>
            <input type="hidden" name="new_custom_price_id" class="new_custom_price_id">
        </td>
    </tr>
    </table>
</div>

<script>
    function calculateMargin()
    {
        var table = $('.table-price');
        var buy_price = $(table).find('input.buy_input').val();
        $(table).find('tr').each(function() {
            var margin_col = $(this).find('.margin');
            var margin_percent_col = $(this).find('.margin-percent');

            if (margin_col.length) {
                var sell_price = $(this).find('.sell_input').val();

                //console.log(buy_price, sell_price)

                if (!isNaN(buy_price) && !isNaN(sell_price) && buy_price > 0 && sell_price > 0) {
                    $(margin_col).html((sell_price - buy_price).toFixed(2));
                    $(margin_percent_col).html(((sell_price - buy_price) / sell_price * 100).toFixed(2) + '%');
                } else {
                    $(margin_col).html('');
                    $(margin_percent_col).html('');
                }
            }
        });
    }

    $(document).on('blur', '.buy_input, .sell_input', function() {
        calculateMargin();
    });
    $(document).ready(function() {
        calculateMargin();
    });
    $(document).on('click', '.remove-price-group', function(e) {
        e.preventDefault();
        $('#delete-price-group').find('.modal-delete-price-group').data('price_group_id', $(this).data('price_group_id'));
        $('#delete-price-group').modal('show');
    });
    $(document).on('click', '.modal-delete-price-group', function(e) {
        e.preventDefault();
        $('.custom-price-block').filter('.group-' + $(this).data('price_group_id')).remove();
        $('#delete-price-group').modal('hide');
    })

    function validateNewPrice()
    {
        $('#addCustomPrice').find('.help-block').addClass('hidden');
        var price_group = $('#custom_price_group');

        var isValidate = true;

        if (!$(price_group).val()) {
            $(price_group).siblings('.help-block-empty').removeClass('hidden');
            isValidate = false;
        }
        if ($('.custom-price-block.group-' + $(price_group).val()).length) {
            $(price_group).siblings('.help-block-present').removeClass('hidden');
            isValidate = false;
        }

        return isValidate;
    }

    $(document).on('click', '.add-custom-price', function(e)
    {
        if (validateNewPrice()) {
            var button = $(this);
            var select = $('#custom_price_group');
            var number = $(button).data('number') + 1;
            var basis_name = 'custom_price[' + number + ']';
            var group_id = $(select).val();
            var group_name = $(select).find('option:selected').html();

            var template = $('.custom-price-template').clone();
            $(template).addClass('group-' + group_id);
            $(template).find('.cp-name').html(group_name);
            $(template).find('.new_custom_price_id').attr('name', basis_name + '[id]').val(group_id);
            $(template).find('.cp-price').find('input').attr('name', basis_name + '[price]');
            $(template).find('.remove-price-group').data('price_group_id', group_id);

            var select2id = 'new_custom_price_nds' + '_' + number;
            $(template).find('.cp-nds').find('select').attr('id', select2id).attr('name', basis_name + '[nds_id]');

            $('.table-price').find('tr').last().after(template);
            $(template).show();

            $(select).val('').trigger('change');
            $('#addCustomPrice').collapse('hide');

            $(button).data('number', number);

            createSimpleSelect2(select2id);
        }
    });
</script>