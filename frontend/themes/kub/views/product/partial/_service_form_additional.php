<?php

use common\models\product\Product;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\product\Product */
/* @var $taxItems array */

$btnLabel = '<span class="link-txt">Дополнительные поля</span> ' . Icon::get('shevron', ['class' => 'link-shevron']);
?>

<?= Html::tag('span', $btnLabel, [
    'class' => 'link link_collapse link_bold mb-4 collapsed',
    'data-toggle' => 'collapse',
    'data-target' => '#service_additional_fields',
    'aria-expanded' => 'false',
    'aria-controls' => 'service_additional_fields',
]) ?>

<div id="service_additional_fields" class="collapse">
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'price_for_buy_with_nds')->textInput([
                'class' => 'product-form-price-input form-control js_input_to_money_format',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'price_for_buy_nds_id')->radioList($taxItems, [
                'class' => 'd-flex mx-n2 pt-2',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, ['class' => 'px-2']);
                },
            ])->label('Включая НДС'); ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->request->isAjax) : ?>
<script type="text/javascript">
    $('[data-toggle=collapse]', $('#product-service-form')).collapse({toggle: false});
</script>
<?php endif ?>