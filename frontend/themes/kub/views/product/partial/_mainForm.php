<?php

use common\models\product\Product;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model common\models\product\Product */
/* @var $isService bool */
/* @var $isGoods bool */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$thinFieldOptions = [
    'template' => "{input}\n{error}"
];

$thinFieldInlineOptions = [
    'template' => "{input}\n{error}"
];


$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;
$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$js = <<<JS
    $(document).on("change", "#product-not_for_sale", function() {
        $("input.for_sale_input").prop("disabled", this.checked);
        $("input.for_sale_input:radio:not(.md-radiobtn)").uniform("refresh");
        if (this.checked) {
            $("#product-price_for_sell_with_nds").val("");
            $(".field-product-price_for_sell_with_nds").removeClass("has-error");
            $(".field-product-price_for_sell_with_nds .help-block-error").html("");
        }
    });

    $(document).on("change", "#product-is_alco", function() {
        if(this.checked) {
            $("li#tab-alco").show();
            $("li#tab-alco a").trigger("click");
        } else {
            $("li#tab-alco").hide();
            $("li#tab-price a").trigger("click");
        }
    });

    $(document).ready(function() {
        if($('#product-is_alco').is(':checked') ){
            $("li#tab-alco").show();
        }
    });


JS;
$this->registerJs($js);
?>

<?= Html::activeHiddenInput($model, 'production_type', [
    'id' => 'production_type_input',
]); ?>

<?= $form->errorSummary($model); ?>

<?php if ($isGoods) : ?>
    <?= $this->render('_productParams', [
        'company' => $company,
        'model' => $model,
        'form' => $form,
        'thinFieldOptions' => $thinFieldOptions,
        'thinFieldInlineOptions' => $thinFieldInlineOptions,
        'canViewPriceForBuy' => $canViewPriceForBuy,
        'productField' => $productField,
        'compositeModel' => $compositeModel ?? null,
    ]); ?>
<?php else : ?>
    <?= $this->render('_serviceParams', [
        'company' => $company,
        'model' => $model,
        'form' => $form,
        'thinFieldOptions' => $thinFieldOptions,
        'canViewPriceForBuy' => $canViewPriceForBuy,
        'productField' => $productField,
        'compositeModel' => $compositeModel ?? null,
    ]); ?>
<?php endif ?>