<?php

use common\models\Contractor;
use kartik\select2\Select2;

?>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Поставщик</label>
                </div>
                    <?= $form->field($model, 'provider_id')->widget(Select2::classname(), [
                        'data' => Contractor::getAllContractorListArray(Contractor::TYPE_SELLER),
                        'options' => [
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'minimumResultsForSearch' => 10,
                            'allowClear' => true,
                            'width' => '100%'
                        ],
                    ]); ?>
            </div>
        </div>
    </div>
</div>
