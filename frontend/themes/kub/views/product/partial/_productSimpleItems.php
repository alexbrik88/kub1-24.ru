<?php

use common\models\product\Product;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\product\Product */
/* @var $compositeModel frontend\models\CompositeServiceForm */

$canAdd = Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE);
$company = Yii::$app->user->identity->company;
?>

<div class="nav-tabs-row">
    <ul class="nav nav-tabs nav-tabs_border_transparent w-100 mr-3" id="myTabIn" role="tablist">
        <li class="nav-item">
            <a id="simple-services-tab"
                class="nav-link active"
                data-toggle="tab"
                href="#simple-services-items"
                role="tab"
                aria-controls="simple-services-items"
                aria-selected="true">
                Услуги/Работы
            </a>
        </li>
        <li class="nav-item">
            <a id="simple-goods-tab"
                class="nav-link"
                data-toggle="tab"
                href="#simple-goods-items"
                role="tab"
                aria-controls="simple-goods-items"
                aria-selected="true">
                Материалы
            </a>
        </li>
    </ul>
</div>
<div class="tab-content" id="nav-tabContent" style="width: 100%;">
    <div class="tab-pane fade show active" id="simple-services-items" role="tabpanel" aria-labelledby="simple-services-tab">
        <div class="page-border p-3">
            <div class="composite_product_simple_items mt-2">
                <?= $form->field($compositeModel, 'services', [
                    'options' => ['class' => ''],
                ])->hiddenInput(['value' => ''])->label(false) ?>
                <table class="table table-style mb-0 table-price">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-nowrap py-2 px-3">
                                Название услуги/работы, входящей в составную услугу
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Цена покупки
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon',
                                    'title' => 'Изменить можно, отредактировав соответствующую услугу/работу.'
                                ]) ?>
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                НДС
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon',
                                    'title' => 'Изменить можно, отредактировав соответствующую услугу/работу.'
                                ]) ?>
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Кол-во
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Итого затраты
                            </th>
                        </tr>
                    </thead>
                    <tbody class="exist_item_list">
                        <?php foreach ($compositeModel->servicesMaps as $item) : ?>
                            <?= $this->render('_service_form_composite_item', [
                                'compositeModel' => $compositeModel,
                                'simpleProduct' => $item->simpleProduct,
                                'quantity' => $item->quantity,
                            ]) ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-6 composite_product_item_select hidden pt-2">
                        <?= $this->render('_product_composite_item_select', [
                            'company' => $company,
                            'canAdd' => $canAdd,
                            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                        ]) ?>
                    </div>
                </div>
                <?= Html::button(Icon::get('add-icon').'<span>Добавить</span>', [
                    'class' => 'button-regular button-hover-content-red show_product_composite_item_select mt-2',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="simple-goods-items" role="tabpanel" aria-labelledby="simple-goods-tab">
        <div class="page-border p-3">
            <div class="composite_product_simple_items mt-2">
                <?= $form->field($compositeModel, 'goods', [
                    'options' => ['class' => ''],
                ])->hiddenInput(['value' => ''])->label(false) ?>
                <table class="table table-style mb-0 table-price">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-nowrap py-2 px-3">
                                Название материала, входящего в составную услугу
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Цена покупки
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon',
                                    'title' => 'Изменить можно, отредактировав соответствующий товар.'
                                ]) ?>
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                НДС
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon',
                                    'title' => 'Изменить можно, отредактировав соответствующий товар.'
                                ]) ?>
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Кол-во
                            </th>
                            <th class="text-nowrap py-2 px-3">
                                Итого затраты
                            </th>
                        </tr>
                    </thead>
                    <tbody class="exist_item_list">
                        <?php foreach ($compositeModel->goodsMaps as $item) : ?>
                            <?= $this->render('_service_form_composite_item', [
                                'compositeModel' => $compositeModel,
                                'simpleProduct' => $item->simpleProduct,
                                'quantity' => $item->quantity,
                            ]) ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-6 composite_product_item_select hidden pt-2">
                        <?= $this->render('_product_composite_item_select', [
                            'company' => $company,
                            'canAdd' => $canAdd,
                            'production_type' => Product::PRODUCTION_TYPE_GOODS,
                        ]) ?>
                    </div>
                </div>
                <?= Html::button(Icon::get('add-icon').'<span>Добавить</span>', [
                    'class' => 'button-regular button-hover-content-red show_product_composite_item_select mt-2',
                ]) ?>
            </div>
        </div>
    </div>
</div>