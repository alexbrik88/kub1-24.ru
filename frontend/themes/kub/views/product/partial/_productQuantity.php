<?php

use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\product\ProductStore;
use common\components\TextHelper;
use common\models\product\Product;

/* @var $model common\models\product\Product */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$storeArray = $model->company->getStores()->orderBy([
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->all();

$growingPriceForBuy = $model->price_for_buy_with_nds;

$totalStoreQuantity = 0;
$totalIrreducibleQuantity = 0;
$totalPriceForBuy = 0;
$totalAmountForBuy = 0;
$totalPriceForSell = 0;
$totalAmountForSell = 0;
?>

<?php Pjax::begin([
    'id' => 'productStore-pjax-container',
    'enablePushState' => false,
]); ?>

<div class="table-responsive">
    <table class="table table-style table-quantity">
        <thead>
        <tr class="heading va-top">
            <th>Склад</th>
            <th>Начальное количество</th>
            <th>Остаток</th>
            <th>Неснижаемый остаток</th>
            <th>Цена продажи</th>
            <th>Стоимость продажи</th>
            <th>Цена покупки</th>
            <th>Стоимость покупки</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($storeArray as $store): ?>
            <?php /* @var $productStore ProductStore */
            $productStore = $model->getProductStoreByStore($store->id);
            $productInitQuantity = $model->getInitQuantity($productStore->store_id);
            $productIrreducibleQuantity = $model->getIrreducibleQuantity($productStore->store_id);
            $totalStoreQuantity += (int)$productStore->quantity;
            $totalIrreducibleQuantity += $productIrreducibleQuantity;
            $totalAmountForBuy += (int)$growingPriceForBuy * $productStore->quantity;
            $totalAmountForSell += (int)$model->price_for_sell_with_nds * $productStore->quantity; ?>
            <tr class="store-row">
                <td style="vertical-align: middle; min-width: 100px;"><?= Html::encode($store->name) ?></td>
                <td class="store-initial-quantity"
                    style="text-align: right; vertical-align: middle; border-right-width: 0;">
                    <?= Html::activeTextInput($model, "initQuantity[$productStore->store_id]", [
                        'class' => 'form-control product-store-quantity',
                        'style' => 'width: 100%; text-align: right; max-width:150px;',
                    ]); ?>
                </td>
                <td style="text-align: right; vertical-align: middle;"><?= $productStore->quantity ?></td>
                <td class="store-irreducible-quantity" style="text-align: right; vertical-align: middle;">
                    <?php /*
                    <?= Html::activeTextInput($model, "irreducibleQuantity[$productStore->store_id]", [
                        'class' => 'form-control product-store-irreducible-quantity',
                        'style' => 'width: 100%; text-align: right;',
                    ]); ?>*/ ?>
                    <?= (isset($model->irreducibleQuantity[$productStore->store_id])) ? $model->irreducibleQuantity[$productStore->store_id] : '' ?>
                </td>
                <td class="price-for-sell" data-value="<?= $model->price_for_sell_with_nds; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?>
                </td>
                <td class="amount-for-sell"
                    data-value="<?= $model->price_for_sell_with_nds * $productStore->quantity; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds * $productStore->quantity, 2); ?>
                </td>
                <td class="price-for-buy" data-value="<?= $growingPriceForBuy; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($growingPriceForBuy ?: ($model->initialBalance ? $model->initialBalance->price : 0), 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
                <td class="amount-for-buy"
                    data-value="<?= $growingPriceForBuy * $productStore->quantity; ?>"
                    style="text-align: right; vertical-align: middle;">
                    <?= $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($growingPriceForBuy * $productStore->quantity, 2) :
                        Product::DEFAULT_VALUE; ?>
                </td>
            </tr>
        <?php endforeach ?>
        <tr class="total-row text-grey" style="font-weight: bold;">
            <td colspan="2" style="vertical-align: middle;">Итого</td>
            <td class="total-store-quantity" style="text-align: right; vertical-align: middle;">
                <?= $totalStoreQuantity; ?>
            </td>
            <td class="total-irreducible-quantity" style="text-align: right; vertical-align: middle;">
                <?= $totalIrreducibleQuantity; ?>
            </td>
            <td class="total-price-for-sell" style="text-align: right; vertical-align: middle;"></td>
            <td class="total-amount-for-sell" style="text-align: right; vertical-align: middle;">
                <?= TextHelper::invoiceMoneyFormat($totalAmountForSell, 2); ?>
            </td>
            <td class="total-price-for-buy" style="text-align: right; vertical-align: middle;"></td>
            <td class="total-amount-for-buy" style="text-align: right; vertical-align: middle;">
                <?= $canViewPriceForBuy ?
                    TextHelper::invoiceMoneyFormat($totalAmountForBuy, 2) :
                    Product::DEFAULT_VALUE; ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<?php if ($initialBalanceModel = $model->loadedInitialBalance ?: $model->initialBalance): ?>
    <div class="mt-3 mb-2">
        <b>Начальные остатки</b>
    </div>
    <div class="row">
        <div class="column">
            <div class="form-group mb-0">
                <div class="form-group d-inline-block mb-0">
                    <label class="label">На дату</label>
                    <div class="date-picker-wrap top-date-picker">
                        <?= Html::activeTextInput($initialBalanceModel, 'date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-position' => 'top left',
                            'value' => DateHelper::format($initialBalanceModel->date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                    <div style="margin-top: 0.25rem; font-size: 80%; color: #dc3545;" <?= Html::error($initialBalanceModel, 'date'); ?>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="form-group mb-0">
                <label class="label">Цена покупки</label>
                <?= Html::activeTextInput($initialBalanceModel, "price", [
                    'class' => 'form-control js_input_to_money_format',
                    'style' => 'width: 100%; text-align: right; max-width:150px;',
                    'value' => round($initialBalanceModel->price / 100, 2)
                ]); ?>
                <div style="margin-top: 0.25rem; font-size: 80%; color: #dc3545;" <?= Html::error($initialBalanceModel, 'price'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>
<?php $this->registerJs('
    $(document).on("keyup change", "input.product-store-irreducible-quantity", function() {
        recalculateProductStoreTable();
    });

    function recalculateProductStoreTable() {
        var $items = $(".product-store-table .store-row");
        var $totalRow = $(".product-store-table .total-row");
        var $totalIrreducibleQuantity = 0;
        $items.each(function () {
            $irreducibleQuantity = +$(this).find("td.store-irreducible-quantity input").val();
            if (!isNaN($irreducibleQuantity)) {
                $totalIrreducibleQuantity += $irreducibleQuantity;
            }
        });
        $totalRow.find(".total-irreducible-quantity").text($totalIrreducibleQuantity);
    }
'); ?>
