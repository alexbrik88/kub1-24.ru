<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use kartik\select2\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\widgets\ProductGroupDropdownWidget;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$units = ProductUnit::findSorted()
    ->andWhere(['services' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}
if ($model->isNewRecord && !$model->group_id) {
    $model->group_id = ProductGroup::WITHOUT;
}

$tabList = [
    '_productInfo',
    '_productPrice',
    isset($compositeModel) ? '_productSimpleItems' : '_productSupplier',
    '_productDescription',
    '_productStocks',
];
$tabActive = Yii::$app->request->get('tab');
if (!in_array($tabActive, $tabList)) {
    $tabActive = '_productInfo';
}
?>

<?= Html::input('hidden', 'tab', $tabActive) ?>

<div class="wrap p-4">
    <div class="p-2">
        <form>
            <div class="form-group mb-0">
                <?= $form->field($model, 'title')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите наименование услуги'
                ]); ?>
            </div>
        </form>
    </div>
</div>

<!-- KUB -->
<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?= Tabs::widget([
                'id' => 'product_params_nav',
                'options' => [
                    'class' => 'nav nav-tabs w-100 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane pr-3',
                    'style' => 'width:100%'
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Информация',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productInfo', [
                            'model' => $model,
                            'form' => $form
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productInfo' ? ' active' : ''),
                            'data-tab' => '_productInfo',
                        ],
                        'active' => $tabActive == '_productInfo',
                    ],
                    [
                        'label' => isset($compositeModel) ? 'Цена' : 'Цены',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productPrice', [
                            'model' => $model,
                            'form' => $form,
                            'canViewPriceForBuy' => $canViewPriceForBuy,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productPrice' ? ' active' : ''),
                            'data-tab' => '_productPrice',
                        ],
                        'active' => $tabActive == '_productPrice',
                    ],
                    isset($compositeModel) ? [
                        'label' => 'Составляющие',
                        'content' => $this->render('_productSimpleItems', [
                            'company' => $company,
                            'model' => $model,
                            'compositeModel' => $compositeModel,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productSimpleItems' ? ' active' : ''),
                            'data-tab' => '_productSimpleItems',
                        ],
                        'active' => $tabActive == '_productSimpleItems',
                    ] : [
                        'label' => 'Поставщики',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productSupplier', [
                            'company' => $company,
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productSupplier' ? ' active' : ''),
                            'data-tab' => '_productSupplier',
                        ],
                        'active' => $tabActive == '_productSupplier',
                    ],
                    [
                        'label' => 'Описание',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productDescription', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productDescription' ? ' active' : ''),
                            'data-tab' => '_productDescription',
                        ],
                        'active' => $tabActive == '_productDescription',
                    ],
                    [
                        'label' => 'Аналитика',
                        'content' => $this->render('_productStocks', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productStocks' ? ' active' : ''),
                            'data-tab' => '_productStocks',
                        ],
                        'active' => $tabActive == '_productStocks',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="delete-supplier" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;" >
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить поставщика?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'modal-delete-supplier button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-supplier_id' => ''
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<div id="delete-price-group" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;" >
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить цену?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'modal-delete-price-group button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-price_group_id' => ''
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php
$tab = Yii::$app->request->get('tab') ?: '_productInfo';
$js = <<<JS
    $(document).on("click", ".nav-item", function(e) {
        var form = $("#product-form");
        var cancelBtn = $(form).find('.button-cancel-edit');
        var newTab = $(this).find('.nav-link').attr('data-tab');
        if (newTab.length && cancelBtn.length) {
            $(form).attr("action", $(form).attr("action").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + newTab));
            $(cancelBtn).attr("href", $(cancelBtn).attr("href").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + newTab));
        }
        return false;
    });
JS;
$this->registerJs($js);
?>