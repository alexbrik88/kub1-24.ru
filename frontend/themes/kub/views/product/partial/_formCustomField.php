<?php

use common\models\product\ProductField;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $productField ProductField */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company Company|null */

$form = ActiveForm::begin([
    'id' => 'change-custom-field-name-form',
    'action' => 'change-custom-field-name',
    'enableClientValidation' => true,
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
    ],
]);
?>
<div class="form-body">
    <div class="row">
           <div class="col-12">
            <?= $form->field($productField, 'title')->textInput([
            ]); ?>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-width button-regular button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-clr button-regular button-hover-transparent back',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>
