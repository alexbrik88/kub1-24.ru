<?php

use common\components\TextHelper;
use common\models\product\Product;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $simpleProduct common\models\product\Product */
/* @var $compositeModel frontend\models\CompositeServiceForm */

if ($simpleProduct->production_type == Product::PRODUCTION_TYPE_GOODS) {
    $attribute = 'goods';
} elseif ($simpleProduct->production_type == Product::PRODUCTION_TYPE_SERVICE) {
    $attribute = 'services';
} else {
    $attribute = null;
}
if (!isset($quantity)) {
    $quantity = 1;
}
$idAttribute = sprintf('%s[%s]', $attribute, $simpleProduct->id);
$quantityAttribute = sprintf('%s_quantity[%s]', $attribute, $simpleProduct->id);
?>

<?php if ($attribute) : ?>
<tr class="composite_product_item">
    <td class="pb-2">
        <?= Icon::get('circle-close', [
            'class' => 'svg-icon_grey mr-2 cursor-pointer composite_product_item_remove',
        ]) ?>
    </td>
    <td class="pb-2 composite_product_item_label" style="width: 90%;">
        <?= Html::a(Html::encode($simpleProduct->title), [
            '/product/view',
            'productionType' => $simpleProduct->production_type,
            'id' => $simpleProduct->id,
            'data-pjax' => 0,
        ]) ?>
    </td>
    <td class="pb-2 composite_product_item_price" data-price="<?= intval($simpleProduct->price_for_buy_with_nds)/100 ?>">
        <?= Html::textInput(null, TextHelper::invoiceMoneyFormat($simpleProduct->price_for_buy_with_nds, 2), [
            'class' => 'form-control width-100 text-right',
            'disabled' => true,
        ]) ?>
    </td>
    <td class="pb-2 composite_product_item_nds">
        <?= Html::textInput(null, $simpleProduct->priceForBuyNds->name ?? 'Без НДС', [
            'class' => 'form-control width-100 text-right',
            'disabled' => true,
        ]) ?>
    </td>
    <td class="pb-2 composite_product_item_inputs">
        <?= Html::activeHiddenInput($compositeModel, $idAttribute, [
            'class' => 'composite_product_item_id',
            'value' => $simpleProduct->id,
        ]) ?>
        <?= Html::activeInput('number', $compositeModel, $quantityAttribute, [
            'class' => 'form-control composite_product_item_quantity text-right',
            'value' => +$quantity,
            'style' => 'width: 80px !important;',
        ]) ?>
    </td>
    <td class="pb-2 composite_product_item_cost_price text-right">
        <?= TextHelper::invoiceMoneyFormat($simpleProduct->price_for_buy_with_nds * $quantity, 2) ?>
    </td>
</tr>
<?php endif ?>