<?php

use common\models\product\Product;
use frontend\components\Icon;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $production_type int|null */
/* @var $canAdd bool */

$id = uniqid();
$staticId = $id.'_static_items';
$addProductTypesText = null;
if ($production_type == Product::PRODUCTION_TYPE_SERVICE) {
    $addProductTypesText = 'услугу/работу';
    $createUrl = Url::to(['/product/create-simple-product', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]);
} elseif ($production_type == Product::PRODUCTION_TYPE_GOODS) {
    $addProductTypesText = 'товар/материал';
    $createUrl = Url::to(['/product/create-simple-product', 'productionType' => Product::PRODUCTION_TYPE_GOODS]);
}
if (!isset($queryParams) || !is_array($queryParams)) {
    $queryParams = [];
}
$queryParams['q'] = new JsExpression('params.term');
$queryParams['unit_type'] = Product::UNIT_TYPE_SIMPLE;
$queryParams['production_type'] = $production_type;
$data = Json::encode($queryParams);
?>

<?= Select2::widget([
    'id' => $id,
    'name' => 'addOrder',
    'initValueText' => '',
    'options' => [
        'placeholder' => '',
        'class' => 'form-control product_composite_item_select',
        'data-static' => '#'.$staticId,
    ],
    'pluginOptions' => [
        'allowClear' => false,
        'width' => '100%',
        'minimumInputLength' => 1,
        'dropdownCssClass' => 'product-search-dropdown',
        'ajax' => [
            'url' => "/product/search",
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression(sprintf("function(params) { return %s; }", $data)),
            'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(product) { return product.text; }'),
        'templateSelection' => new JsExpression('function (product) { return product.text; }'),
    ],
]); ?>

<div id="<?=$staticId ?>" class="hidden">
    <ul class="product_composite_item_select_static_items select2-results__options">
        <?php if ($canAdd && !empty($createUrl)) : ?>
            <?= Html::beginTag('li', [
                'class' => 'select2-results__option _create_simple_product red',
                'aria-selected' => 'false',
                'data-select' => '#'.$id,
                'data-url' => $createUrl,
            ]) ?>
                <?= Icon::get('add-icon') . " Добавить {$addProductTypesText}" ?>
            <?= Html::endTag('li') ?>
        <?php endif ?>
        <?php if ($production_type == Product::PRODUCTION_TYPE_SERVICE) : ?>
            <?php $serviceCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
                'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            ])->notForSale(false)->count() ?>
            <?= Html::beginTag('li', [
                'class' => 'select2-results__option _add_simple_product',
                'aria-selected' => 'false',
                'data-select' => '#'.$id,
                'data-url' => Url::to(['/product/simple-index', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]),
            ]) ?>
                Перечень ваших услуг
                <?php if (isset($serviceCount)) : ?>
                    (<span class="service-count-value"><?= $serviceCount ?></span>)
                <?php endif ?>
            <?= Html::endTag('li') ?>
        <?php endif ?>
        <?php if ($production_type == Product::PRODUCTION_TYPE_GOODS) : ?>
            <?php $goodsCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
            ])->notForSale(false)->count() ?>
            <?= Html::beginTag('li', [
                'class' => 'select2-results__option _add_simple_product',
                'aria-selected' => 'false',
                'data-select' => '#'.$id,
                'data-url' => Url::to(['/product/simple-index', 'productionType' => Product::PRODUCTION_TYPE_GOODS]),
            ]) ?>
                Перечень ваших товаров
                <?php if (isset($goodsCount)) : ?>
                    (<span class="product-count-value"><?= $goodsCount ?></span>)
                <?php endif ?>
            <?= Html::endTag('li') ?>
        <?php endif ?>
    </ul>
</div>
