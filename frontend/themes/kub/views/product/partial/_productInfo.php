<?php

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use common\models\product\ProductCategory;
use common\models\product\ProductCategoryField;
use common\models\product\ProductCategoryItem;
use yii\bootstrap4\Html;

/* @var $model common\models\product\Product */
/* @var $productField common\models\product\ProductField */
/* @var $thinFieldOptions array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

$shortFieldOptions = [
    'inputOptions' => [
        //'class' => 'form-control form-control_width_140'
        'class' => 'form-control'
    ]
];
$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;

if ($isGoods) {
    $units = ProductUnit::findSorted()->andWhere(['goods' => 1])->all();
} else {
    $units = ProductUnit::findSorted()->andWhere(['services' => 1])->all();
}

$categoryNames = Product::getCategoryNames();
$categoryFields = Product::getCategoryFieldNames();
$categoryItems = $model->getCategoryFieldValues();

$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);
?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Группа <?= ($isGoods) ? 'товара' : 'услуги' ?></label>
                </div>
                <?= $form->field($model, 'group_id', [
                    'options' => [
                        'class' => ''
                    ],
                ])->widget(ProductGroupDropdownWidget::classname(), [
                    'loadAssets' => false,
                    'addDeleteModal' => false,
                    'productionType' => $model->production_type,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Категория</label>
                </div>
                <?= $form->field($model, 'category_id', [
                    'options' => [
                        'class' => 'required'
                    ],
                ])->widget(Select2::classname(), [
                    'hideSearch' => true,
                    'data' => ['' => '--'] + $categoryNames,
                    'options' => [
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip2',
                    'style' => 'position:absolute; top:0; left:75px;',
                    'data-tooltip-content' => '#tooltip_category',
                ]) ?>
                <input type="hidden" id="hidden-product-category_id" name="Product[category_id]" value="<?= $model->category_id ?>"/>
            </div>
            <?php if ($isGoods): ?>
                <div class="form-group col-3 mb-3 pb-2">
                    <div class="checkbox" style="padding-top: 37px">
                        <?= Html::activeCheckbox($model, 'is_traceable', ['label' => false]) ?>
                        <label class="checkbox-label" for="product-is_traceable">
                            Товар подлежит прослеживаемости
                        </label><?=
                            \yii\helpers\Html::tag('span', Icon::QUESTION, ['class' => 'tooltip2', 'data-tooltip-content' => '#tooltip_is_traceable',])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if ($isService): ?>
        <div class="col-12">
            <div class="row">
                <div class="col-3">
                    <div class="form-filter">
                        <label class="label weight-700" for="cause">Единица измерения</label>
                    </div>
                    <?= $form->field($model, 'product_unit_id', [
                        'options' => [
                            'class' => 'required'
                        ],
                    ])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($units, 'id', 'name'),
                        'options' => [
                            'prompt' => '',
                            'options' => $unitsOptions,
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($isGoods): ?>
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <label class="label weight-700" for="">Код</label>
                <?= $form->field($model, 'code')->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <label class="label weight-700" for="">Артикул</label>
                <?= $form->field($model, 'article')->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="form-group col-2 mb-3 pb-2">
                <label class="label weight-700" for="">Штрих код</label>
                <?= $form->field($model, 'barcode', $shortFieldOptions)->textInput([
                    'placeholder' => Product::DEFAULT_VALUE,
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <div class="form-filter">
                        <label class="label weight-700" for="cause">Единица измерения</label>
                    </div>
                    <?= $form->field($model, 'product_unit_id', [
                        'options' => [
                            'class' => 'required'
                        ],
                    ])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($units, 'id', 'name'),
                        'options' => [
                            'prompt' => '',
                            'options' => $unitsOptions,
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                </div>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Вес (кг)</label>
                    <?= $form->field($model, 'weight', ['enableClientValidation' => false])->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700" for="">Объем (м3)</label>
                    <?= $form->field($model, 'volume', ['enableClientValidation' => false])->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Вид упаковки</label>
                    <?= $form->field($model, 'box_type')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <?php /*
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Количество в одном месте</label>
                    <?= $form->field($model, 'count_in_place')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group column mb-3 pb-2">
                    <label class="label weight-700" for="">Количество мест, штук</label>
                    <?= $form->field($model, 'place_count', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>*/ ?>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Количество в упаковке</label>
                    <?= $form->field($model, 'count_in_package')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700" for="">Масса нетто</label>
                    <?= $form->field($model, 'mass_net', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                        'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
                    ]); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700" for="">Масса брутто</label>
                    <?= $form->field($model, 'mass_gross', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                        'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-3 mb-3 pb-2">
                    <div class="form-filter">
                        <label class="label weight-700" for="cause">Страна происхождения товара</label>
                    </div>
                    <?= $form->field($model, 'country_origin_id')
                        ->widget(Select2::class, [
                            'data' => ArrayHelper::map(Country::getCountries(), 'id', 'name_short'),
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>

                </div>
                <div class="form-group col-3 mb-3 pb-2">
                    <label class="label weight-700" for="">Номер таможенной декларации</label>
                    <?= $form->field($model, 'customs_declaration_number')->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700" for="">Код вида товара</label>
                    <?= $form->field($model, 'item_type_code', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700 custom-field-name-label" style="max-width: 120px;"><?= $productField->title ?></label>
                    <?= $this->render('//svg-sprite', ['ico' => 'pencil', 'class' => "label link change-custom-field-name-btn ml-1", 'style' => 'cursor:pointer']) ?>
                    <?= $form->field($model, 'custom_field_value', $shortFieldOptions)->textInput([
                        'placeholder' => Product::DEFAULT_VALUE,
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-8 mb-3 pb-2">
                    <label class="label weight-700">Название на английском языке (Name in English)</label>
                    <?= $form->field($model, 'title_en')->textInput(); ?>
                </div>
                <div class="form-group col-2 mb-3 pb-2">
                    <label class="label weight-700">Код ТН ВЭД</label>
                    <?= $form->field($model, 'code_tn_ved')->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php $i=0; ?>
<?php foreach ($categoryNames as $categoryId => $categoryName): ?>
    <div class="row category-field-group <?= $model->category_id == $categoryId ? '' : 'hide' ?>" data-category="<?= $categoryId ?>">
        <?php foreach ($categoryFields[$categoryId] as $fieldId => $fieldName): ?>
            <?php if ($i==0): ?><div class="col-12"><div class="row"><?php endif; ?>
            <div class="form-group <?= (++$i < 3) ? 'col-3' : 'column' ?> mb-3 pb-2">
                <label class="label weight-700" for=""><?= $fieldName ?></label>
                <?= Html::input('text', "category_items[{$categoryId}][{$fieldId}]", $categoryItems[$fieldId] ?? "", [
                    'class' => 'category-field form-control' . ($i >= 3 ? ' form-control_width_140' : '')
                ]) ?>
            </div>
            <?php if ($i >= 3) { $i=0; ?></div></div><?php } ?>
        <?php endforeach; ?>
    <?php if ($i > 0) { ?></div></div><?php } ?>
    </div>
<?php endforeach; ?>

<div class="tooltip-templates" style="display: none">
    <div id="tooltip_category">
        Выберите категорию, чтобы<br/>
        появились дополнительные поля<br/>
        для товаров данной категории.<br/>
        Поля появятся в самом низу.
    </div>
    <div id="tooltip_is_traceable">
        Поставьте галочку, чтобы в счет-фактуре<br/>
        или УПД заполнялись дополнительные столбцы<br/>
        (11,12, 12а, 13) для товаров, которые <br/>
        подлежат прослеживанию.
    </div>
</div>

<script>
    function checkCategorySelect() {
        const $select = $('#product-category_id');
        const $fields = $('.category-field');
        let empty = true;
        $fields.each(function() {
            if ($(this).val())
                empty = false;
        });
        if (empty) {
            $select.prop('disabled', false);
        } else {
            $select.prop('disabled', true);
        }
    }
    $('#product-category_id').on('change', function() {
        let $fields = $('.category-field-group');
        $fields.filter(':visible').slideToggle( "fast" );
        $fields.filter('[data-category="' + $(this).val() + '"]').slideToggle( "fast" );
        $('#hidden-product-category_id').val($(this).val());
        checkCategorySelect();
    });
    $('.category-field').on('change', function() {
        checkCategorySelect();
    });
    <?php if ($model->category_id): ?>
    $(document).ready(function() {
        checkCategorySelect();
    });
    <?php endif; ?>
</script>