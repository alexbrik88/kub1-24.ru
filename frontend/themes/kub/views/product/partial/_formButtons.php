<?php
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

// B2B return
if ('B2B' == Yii::$app->request->get('backTo'))
    $returnUrl = ['/b2b/products'];
else
    $returnUrl = null;

$tabActive = Yii::$app->request->get('tab');

/* @var $model common\models\product\Product */
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-clr button-regular button-regular_red pl-4 pr-4 ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $model->isNewRecord
                ? ($returnUrl ?: ['product/index', 'productionType' => $model->production_type,])
                : ['product/view', 'productionType' => $model->production_type, 'id' => $model->id, 'tab' => $tabActive],
                [
                    'class' => 'button-width button-clr button-regular button-hover-grey pl-4 pr-4' . ($model->isNewRecord ? '' : ' button-cancel-edit'),
                    'data-style' => 'expand-right',
                ]); ?>
        </div>
    </div>
</div>