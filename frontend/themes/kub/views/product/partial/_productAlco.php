<?php
use common\models\address\Country;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use \common\models\product\EgaisType;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

$options = [
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line-product',
        'style' => 'padding-left: 0px;',
    ],
];

$modelAlco = $model->getModelAlco();

?>
<style type="text/css">
    .field-product-provider_id .select2-selection__clear {
        position: absolute !important;
    }
    .form-control{
        width:100%;
    }
</style>


<?= $form->field($modelAlco, 'egais_type_id', $options)->widget(Select2::classname(), [
    'data' => ArrayHelper::map(EgaisType::getData(), 'id', 'title'),
    'options' => [
        'prompt' => '',
    ]
]); ?>

<?=$form->field($modelAlco, 'alco_strength', $options)->textInput([
    'type' => 'number'
])?>

<?=$form->field($modelAlco, 'volume', $options)->textInput([
    'type' => 'number'
])?>

<?= $form->field($model, 'country_origin_id', $options)
    ->dropDownList(ArrayHelper::map(Country::getCountries(), 'id', 'name_short')); ?>

<?= $form->field($modelAlco, 'manufacturer', $options)->textInput([
    'type' => 'string'
]); ?>

<?=$form->field($modelAlco, 'inn', $options)->textInput([
    'type' => 'number'
])?>

<?=$form->field($modelAlco, 'alco_code', $options)->textInput([
    'type' => 'number'
])?>

