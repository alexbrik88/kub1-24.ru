<?php
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use dosamigos\fileinput\BootstrapFileInput;
use dosamigos\fileinput\FileInput;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$units = ProductUnit::findSorted()
    ->andWhere(['goods' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}

$storeArray = $model->company->getStores()->orderBy(['is_main' => SORT_DESC])->all();

if ($model->isNewRecord && !$model->group_id) {
    $model->group_id = ProductGroup::WITHOUT;
}

$tabList = [
    '_productInfo',
    '_productPrice',
    '_productQuantity',
    '_productSupplier',
    '_productImage',
    '_productDescription',
    '_productStocks',
    '_productMarketplaces',
];
$tabActive = Yii::$app->request->get('tab');
if (!in_array($tabActive, $tabList)) {
    $tabActive = '_productInfo';
}
?>

<?= Html::input('hidden', 'tab', $tabActive) ?>

<div class="wrap p-4">
    <div class="p-2">
        <form>
            <div class="form-group mb-0">
                <?= $form->field($model, 'title')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите наименование товара'
                ]); ?>
            </div>
        </form>
    </div>
</div>

<!-- KUB -->
<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?= Tabs::widget([
                'id' => 'product_params_nav',
                'options' => [
                    'class' => 'nav nav-tabs w-100 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane pr-3',
                    'style' => 'width:100%',
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Информация',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productInfo', [
                            'model' => $model,
                            'form' => $form,
                            'productField' => $productField
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productInfo' ? ' active' : ''),
                            'data-tab' => '_productInfo',
                        ],
                        'active' => $tabActive == '_productInfo',
                    ],
                    [
                        'label' => 'Цены',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productPrice', [
                            'model' => $model,
                            'form' => $form,
                            'canViewPriceForBuy' => $canViewPriceForBuy,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productPrice' ? ' active' : ''),
                            'data-tab' => '_productPrice',
                        ],
                        'active' => $tabActive == '_productPrice',
                    ],
                    [
                        'label' => 'Остатки',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productQuantity', [
                            'model' => $model,
                            'form' => $form,
                            'canViewPriceForBuy' => $canViewPriceForBuy,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productQuantity' ? ' active' : ''),
                            'data-tab' => '_productQuantity',
                        ],
                        'active' => $tabActive == '_productQuantity',
                    ],
                    [
                        'label' => 'Поставщики',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productSupplier', [
                            'company' => $company,
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productSupplier' ? ' active' : ''),
                            'data-tab' => '_productSupplier',
                        ],
                        'active' => $tabActive == '_productSupplier',
                    ],
                    [
                        'label' => 'Изображение',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productImage', [
                            'model' => $model,
                            'form' => $form,
                            'thumbnail' => ($src = $model->getImageThumb()) ?
                                Html::img($src, [
                                    'class' => 'product-photo-preview-form',
                                    'alt' => '',
                                ]) :
                                null,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productImage' ? ' active' : ''),
                            'data-tab' => '_productImage',
                        ],
                        'active' => $tabActive == '_productImage',
                    ],
                    [
                        'label' => 'Описание',
                        'options' => [
                            'class' => 'pt-3',
                        ],
                        'content' => $this->render('_productDescription', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productDescription' ? ' active' : ''),
                            'data-tab' => '_productDescription',
                        ],
                        'active' => $tabActive == '_productDescription',
                    ],
                    [
                        'label' => 'Управление запасами',
                        'content' => $this->render('_productStocks', [
                            'model' => $model,
                            'form' => $form,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link'.($tabActive == '_productStocks' ? ' active' : ''),
                            'data-tab' => '_productStocks',
                        ],
                        'active' => $tabActive == '_productStocks',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="delete-supplier" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;" >
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить поставщика?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'modal-delete-supplier button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-supplier_id' => ''
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<div id="delete-price-group" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;" >
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить цену?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'modal-delete-price-group button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-price_group_id' => ''
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php
$massGrossId = \yii\helpers\Html::getInputId($model, 'mass_gross');
$massNetId = \yii\helpers\Html::getInputId($model, 'mass_net');
$productUnitId = \yii\helpers\Html::getInputId($model, 'product_unit_id');
$countableUnits = json_encode(array_keys(ProductUnit::$countableUnits));
$js = <<<JS
    var productMassGross = $('#$massGrossId');
    var productMassNet = $('#$massNetId');
    var countableUnits = $countableUnits;
    $('#$productUnitId').on('change init', '', function (e) {
        productMassGross.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
        productMassNet.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
    }).trigger('init');
    $(document).on("click", "#comment_photo_update", function () {
        $("#comment_photo_view").toggleClass("hidden");
        $("#comment_photo_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_photo_save", function () {
        $("#comment_photo_view").text($('#product-comment_photo').val().trim());
        $("#comment_photo_form").addClass("hidden");
        $("#comment_photo_view").removeClass("hidden");
    });
JS;
$this->registerJs($js);
?>
