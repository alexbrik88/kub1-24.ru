<?php

use common\models\product\Product;
use common\models\service\SubscribeTariffGroup;
use frontend\components\BusinessAnalyticsAccess;
use frontend\rbac\permissions;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model common\models\product\Product */
/** @var $canViewPriceForBuy boolean */
/** @var $estimate boolean */
/** @var $canABC boolean */
/** @var $abcModalTitle string|null */

$this->title = Html::encode($model->title);

$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';

$isMoveable = $model->production_type == Product::PRODUCTION_TYPE_GOODS && max(0, $model->quantity) > 0;
switch ($model->status) {
    case Product::DELETED:
        $statusText = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товар удалён' : 'Услуга удалена';
        break;

    case Product::ARCHIVE:
        $statusText = 'Находится в архиве';
        break;

    default:
        $statusText = '';
        break;
}

$iconArchive = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#archiev"></use></svg>';
$iconDelete = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>';

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);

$backUrl = empty($estimate) ? Url::previous('product_list') : Yii::$app->request->referrer;

?>

<div class="page-head row justify-content-between align-items-end pb-1">
    <?= Html::a('Назад к списку', $backUrl ?: [
        'index',
        'productionType' => $model->production_type,
    ], [
        'class' => 'link mb-2 column back-to-customers',
    ]) ?>
</div>

<?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS): ?>
    <?= $this->render('view/_goods', [
        'model' => $model,
        'canViewPriceForBuy' => $canViewPriceForBuy,
        'canABC' => $canABC,
        'abcModalTitle' => $abcModalTitle,
    ]) ?>

    <?php if (!empty($abcModalTitle)) : ?>
        <?php Modal::begin([
            'id' => 'abc-denied-modal',
            'title' => $abcModalTitle,
            'toggleButton' => [
                'label' => 'ABC анализ',
                'class' => 'w-100 button-regular button-hover-transparent',
            ]
        ]) ?>
            <div class="d-flex justify-content-center">
                <?= Html::a('Да', ['/subscribe/default/index'], [
                    'class' => 'button-regular button-hover-transparent button-width mr-1',
                ]) ?>
                <?= Html::button('Нет', [
                    'class' => 'button-regular button-hover-transparent button-width ml-1',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        <?php Modal::end() ?>
    <?php endif ?>
<?php elseif ($model->production_type == Product::PRODUCTION_TYPE_SERVICE): ?>
    <?= $this->render('view/_service', [
        'model' => $model,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]) ?>
<?php endif; ?>


<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-end">
        <div class="column">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>',
                    Url::to(['create', 'productionType' => $model->production_type, 'fromProduct' => $model->id]), [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                ]) ?>
            <?php endif ?>
        </div>
        <div class="column">
            <button class="print-product button-clr button-regular button-width button-hover-transparent w-100" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#print"></use>
                </svg>
                <span>Печать</span>
            </button>
        </div>
        <?php if ($canUpdate): ?>
            <div class="column">
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'folder']).' <span>В группу</span>', '#move_to_group_modal', [
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                'data-toggle' => 'modal',
                ]) ?>
            </div>
        <?php endif; ?>
        <div class="column">
            <?php if (in_array($model->status, [Product::ACTIVE, Product::ARCHIVE])
                && Yii::$app->user->can(\frontend\rbac\permissions\Product::UPDATE)
            ) : ?>
                <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $iconArchive . ($model->status == Product::ACTIVE ? '<span>В архив</span>' : '<span>Извлечь из архива</span>'),
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::to(['archive', 'productionType' => $model->production_type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => $model->status == Product::ACTIVE ? 'Перенести в архив?' : 'Извлечь из архива?',
                ]);
                ?>
            <?php endif; ?>

        </div>
        <div class="column">
            <?php if (!$model->is_deleted && Yii::$app->user->can(\frontend\rbac\permissions\Product::DELETE)): ?>
                <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $iconDelete . '<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::to(['delete', 'productionType' => $model->production_type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
                ]);
                ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

    <div class="created-by">
        <?= date('d.m.Y', $model->created_at) ?>
        <?php if ($model->is_import_1c): ?>
            <?= $model->is_import_1c ? 'Загрузка из 1С' : '' ?>
        <?php else: ?>
            Создал
            <?= $model->creator ? $model->creator->fio : ''; ?>
        <?php endif; ?>
    </div>

<?php Modal::end(); ?>

<?= $this->render('index/move_to_group', [
    'productionType' => $model->production_type,
]); ?>
<div class="move_to_group_product_id" style="display: none!important;"><input type="checkbox" class="product_checker" checked value="<?= $model->id ?>"></div>

<style>
    @media print {
        .header, .footer, .page-sidebar, .page-head, .button-regular, .nav-tabs, .button-clr, .wrap_btns {
            display: none;
        }
        .tab-pane {
            display: block!important;
        }
        .wrap {
            margin:0 !important;
        }
    }
</style>

<script>
    $('.print-product').on('click', function(e) {
        e.preventDefault();
        window.print();
    });
    $(document).on('pjax:success', '#product-view-tabs-pjax', function() {
        // fix calendar
        $('#product-view-tabs-pjax').find('[data-toggle="toggleVisible"]').click(function(e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('[data-id="'+target+'"]').toggleClass('visible show');
            if ( $(this).closest('.dropdown').length ) {
                $(this).closest('.invoice-wrap').removeClass('stub');
            }
        });
        $('.date-picker-calendar').each(function() {
            var date = this.value.split(".");
            var min = String($(this).data('mindate')).split(".");
            var datepicker = $(this).datepicker({
                autoClose: false,
                inline: true,
                minDate: min.length == 3 ? new Date(min[2], min[1] - 1, min[0]) : "",
                prevHtml:   '<svg class="svg-icon">'+
                    '<use xlink:href="/img/svg/svgSprite.svg#shevron"></use>'+
                    '</svg>',
                nextHtml:   '<svg class="svg-icon">'+
                    '<use xlink:href="/img/svg/svgSprite.svg#shevron"></use>'+
                    '</svg>',
                onSelect: function(formattedDate, date, inst) {
                    var $el = $(inst.el);
                    if ($el.hasClass('date-from')) {
                        var $container = $el.closest('.range-box-datepicker');
                        var picker = $('input.date-picker-calendar.date-till', $container).data('datepicker');
                        if (picker) {
                            if (picker.selectedDates[0] && picker.selectedDates[0] < date) {
                                var newDate = new Date(date.getTime());
                                newDate.setDate(newDate.getDate() + 14);
                                picker.selectDate(newDate);
                            }
                            picker.update('minDate', date);
                        }
                    }
                    $(inst.el).trigger('change');
                },
            }).data('datepicker');
            if (date.length == 3) {
                var dateObject = new Date(date[2], date[1] - 1, date[0]);
                datepicker.date = dateObject;
                datepicker.selectDate(dateObject);
            }
        });
        $('.date-picker-calendar').click(function() {
            $(this).closest('.popup-dropdown_calendar').find('.datepicker-inline').addClass('visible')
        });
    });
</script>