<?php

use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $header string
 * @var $xlsText string
 * @var $moyskladText string
 * @var $formData string
 * @var $uploadXlsTemplateUrl string
 */

?>
<div class="modal fade" id="import-products" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title mb-3"><?= $header; ?></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <ul id="import-products-tab" class="av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 mb-3 row nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" href="#tab-import-excel" data-toggle="tab" role="tab" aria-controls="tab-import-excel" aria-selected="true">Excel</a></li>
                    <li class="nav-item"><a class="nav-link" href="#tab-import-moysklad" data-toggle="tab" role="tab" aria-controls="tab-import-moysklad" aria-selected="false">Мой склад</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-import-excel" class="tab-pane active">
                        <div class="row">
                            <div class="col-6">
                                <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить файл</span>', 'javascript:;', [
                                    'class' => 'button-regular button-width button-hover-content-red button-clr add-xls-file',
                                    'style' => 'width:160px'
                                ]); ?>
                            </div>
                            <div class="col-6 upload-xls-button ta-r">
                                <?= Html::a('<span>Загрузить</span>', 'javascript:;', [
                                    'class' => 'button-regular button-width button-hover-content-red button-clr disabled',
                                    'data-tooltip-content' => '#upload-xls-tooltip',
                                    'data-tooltipster' => Json::encode([
                                        'contentAsHTML' => true,
                                    ]),
                                    'style' => 'width:160px'
                                ]); ?>
                                <div class="hidden">
                                    <div id="upload-xls-tooltip">
                                        Сначала добавьте файл для загрузки
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row xls-title">
                            <div class="gray-alert col-12 mt-2">
                                <?= $xlsText; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="margin-top: 20px;">
                                <div class="xls-error"></div>
                                <div class="row">
                                    <?php ActiveForm::begin([
                                        'action' => '#',
                                        'method' => 'post',
                                        'id' => 'xls-ajax-form',
                                        'options' => [
                                            'enctype' => 'multipart/form-data',
                                        ],
                                    ]); ?>
                                    <?= $formData; ?>
                                    <?= Html::hiddenInput('createdModels', null, [
                                        'class' => 'created-models',
                                    ]); ?>
                                    <div class="file-list col-12">
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
                                    'class' => 'upload-xls-template-url',
                                ]); ?>
                            </div>
                            <div class="row action-buttons buttons-fixed xls-buttons" style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
                                <div class="button-bottom-page-lg col-3" style="text-align: left;width: 50%">
                                    <?= Html::a('Сохранить', 'javascript:;', [
                                        'class' => 'button-regular button-width button-regular_red button-clr undelete-models',
                                    ]); ?>
                                </div>
                                <div class="button-bottom-page-lg col-6" style="text-align: right;width: 50%;">
                                    <?= Html::a('Отменить', 'javascript:;', [
                                        'class' => 'button-clr button-width button-regular button-hover-transparent xls-close',
                                    ]); ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div id="progressBox"></div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-import-moysklad" class="tab-pane">
                        <div class="row">
                            <div class="col-12">
                                <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить файл</span>', [
                                    'class' => 'button-regular button-width button-hover-content-red button-clr show-moysklad-import-modal',
                                    'style' => 'width:160px'
                                ]); ?>
                            </div>
                        </div>
                        <div class="row xls-title">
                            <div class="gray-alert col-12 mt-2">
                                <?= $moyskladText; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="margin-top: 20px;">
                                <button class="link link_collapse link_bold button-clr collapsed mb-1" type="button" data-toggle="collapse" data-target="#moreMoyskladInfo" aria-expanded="false" aria-controls="moreMoyskladInfo">
                                    <span class="link-txt">Инструкция, как скачать xml файл</span>
                                    <svg class="link-shevron svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                    </svg>
                                </button>
                                <div class="collapse" id="moreMoyskladInfo">
                                    <div class="font-14 mt-2">
                                        <div class="mb-1">
                                            Из сервиса Мой склад можно выгрузить следующие данные:<br/>
                                            <ul class="pl-3 mb-0">
                                                <li>Справочник контрагентов</li>
                                                <li>Справочник номенклатуры</li>
                                                <li>Счета, Акты, Накладные и УПД</li>
                                            </ul>
                                            Чтобы выгрузить данные, нужно сформировать экспортный xml файл, для 1С. КУБ24 тоже понимает файл для 1С.
                                        </div>
                                        <div class="mb-2">
                                            1. Зайдите в свой кабинет сервиса Мой склад
                                        </div>
                                        <div class="mb-2">
                                            2. Далее нажмите по профилю и выберите пункт «Настройки»
                                            <br/>
                                            <img class="pt-1" src="/img/moysklad/instruction_1.jpg"/>
                                        </div>
                                        <div class="mb-2">
                                            3. В настройках, в левом меню выберите раздел «Экспорт», затем нажмите по кнопке «Экспортировать» и нажмите по пункту «1С:Бухгалтерия»
                                            <br/>
                                            <img class="pt-1" src="/img/moysklad/instruction_2.jpg"/>
                                        </div>
                                        <div class="mb-2">
                                            4. В появившемся окне, укажите настройки экспорта. Период выгрузки данных, по какой организации(если в вашем аккануте в Моем складе больше одной компании), и формат выгрузки. В формате выгрузке обязательно указывайте «1C:Бухгалтерия 3.0», далее нажмите по кнопке «Экспортировать»
                                            <br/>
                                            <img class="pt-1" src="/img/moysklad/instruction_3.jpg"/>
                                        </div>
                                        <div class="mb-2">
                                            5. Дождитесь окончания подготовки файла экспорта и нажмите по кнопке «Скачать файл». Обратите внимание на дату и период выгрузки, чтобы скачать актуальный файл.
                                            <br/>
                                            <img class="pt-1" src="/img/moysklad/instruction_4.jpg"/>
                                        </div>
                                    </div>
                                    <div class="mt-2 font-14">
                                        <strong>Технологии рулят :)</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
