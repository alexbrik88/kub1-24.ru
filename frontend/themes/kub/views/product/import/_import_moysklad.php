<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\product\Store;
use common\components\moysklad\MoyskladImport;

$company = Yii::$app->user->identity->company;
$storeData = ArrayHelper::map(
    Store::find()
        ->where(['company_id' => $company->id])
        ->orderBy(['is_main' => SORT_DESC])
        ->all(), 'id', 'name');
$formatData = [
    MoyskladImport::DOCUMENT_FORMAT_UPD => 'Создавать УПД',
    MoyskladImport::DOCUMENT_FORMAT_ACT_WITH_PACKING_LIST => 'Создавать отдельно Акт и ТН'
];
/** @var MoyskladImport $lastImport */
$lastImport = MoyskladImport::find()
    ->where(['company_id' => $company->id])
    ->orderBy(['created_at' => SORT_DESC])
    ->one();

if ($lastImport) {
    $lastStoreId = $lastImport->store_id;
    $lastFormatId = $lastImport->document_format_id;
} else {
    $lastStoreId = reset($storeData);
    $lastFormatId = reset($formatData);
}
?>
<div class="modal fade" id="import-moysklad" tabindex="-1" role="modal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title mb-3">Импорт товаров и документов из</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

                <div class="bank-form">
                    <div class="row pb-3">
                        <div class="col-5 d-flex flex-column align-items-center justify-content-center">
                            <div class="logo-border text-center">
                                <img src="/img/moysklad/moysklad_logo.png" class="flex-shrink-0" style="margin-top:-12px" width="200" height="200"/>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="bank-form-notify p-3">
                                <div class="p-1">
                                    Для обеспечения безопасности данных используется
                                    протокол зашифрованного соединения. <br/>
                                    SSL - надежный протокол для передачи конфиденциальной
                                    банковской информации и соблюдаются требования
                                    международного стандарта PCI DSS по хранению и передаче
                                    конфиденциальной информации в банковской сфере.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-6">
                        <label class="label">Выберите склад<span class="important">*</span></label>
                        <?= Select2::widget([
                            'hideSearch' => true,
                            'name' => 'MoyskladImport[store_id]',
                            'data' => $storeData,
                            'value' => $lastStoreId,
                            'options' => [
                                'id' => 'moysklad_store_id',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-6">
                        <label class="label">Выберите формат документов<span class="important">*</span></label>
                        <?= Select2::widget([
                            'hideSearch' => true,
                            'name' => 'MoyskladImport[document_format_id]',
                            'data' => $formatData,
                            'value' => $lastFormatId,
                            'options' => [
                                'id' => 'moysklad_document_format_id',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]) ?>
                    </div>
                </div>

                <div class="row mt-3 hide-on-process">
                    <div class="form-group col-12 mb-0">
                        <button class="add-xml-file-2 button-hover-content-red button-regular button-regular_padding_bigger button-clr"
                                type="button" data-modal-id="#import-moysklad">
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                            </svg>
                            <span>Добавить файл xml</span>
                        </button>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12" style="min-height:44px">
                        <div class="xml-error-2 mb-2"></div>
                        <div class="row">
                            <?php ActiveForm::begin([
                                'action' => '#',
                                'method' => 'post',
                                'id' => 'xml-ajax-form-2',
                                'options' => [
                                    'enctype' => 'multipart/form-data',
                                    'class' => 'w-100'
                                ],
                            ]); ?>
                            <div class="file-list-2 col-12"></div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="progress-status process-shift-info w-100 ml-0" style="display:none;">
                            <label>
                                <span class="progress-description font-14"></span>
                            </label>
                            <div class="progress progress-striped active export-progressbar">
                                <div class="progress-bar" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0"
                                     aria-valuemax="100" style="width:0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="show-on-process hidden">
                    <div class="mt-2">
                        <button class="link link_collapse link_bold button-clr font-14 collapsed" type="button" data-toggle="collapse" data-target="#html_statistics_collapser" aria-expanded="true" aria-controls="forProduct2">
                            <span class="link-txt">Детали загрузки</span>
                            <svg class="link-shevron svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div id="html_statistics_collapser" class="collapse">
                            <div class="html_statistics w-100"><!-- js --></div>
                        </div>
                    </div>

                    <div class="mt-2">
                        <button class="link link_collapse link_bold button-clr font-14 collapsed" type="button" data-toggle="collapse" data-target="#html_validation_collapser" aria-expanded="true" aria-controls="forProduct2">
                            <span class="link-txt">Ошибки валидации</span>
                            <svg class="link-shevron svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div id="html_validation_collapser" class="collapse">
                            <div class="html_validation_objects w-100 mt-3"><!-- js --></div>
                            <div class="html_validation_documents w-100 mt-3"><!-- js --></div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3 d-flex justify-content-between hide-on-process hide-on-success">
                    <div class="upload-xml-button-2 column">
                        <a href="#" class="button-clr button-regular button-regular_red width-160 pull-right disabled">
                            Загрузить
                        </a>
                    </div>
                    <div class="column">
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">
                            Отменить
                        </button>
                    </div>
                </div>

                <div class="row mt-3 justify-content-end hide-on-process show-on-success hidden">
                    <div class="show-on-success">
                        <div class="column">
                            <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal" onclick="location.reload(); return false">
                                Ок
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
