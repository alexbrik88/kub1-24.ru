<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.12.2017
 * Time: 18:15
 */

use yii\bootstrap\Dropdown;
use frontend\models\ProductXyzSearch;
use yii\data\ArrayDataProvider;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

/* @var $this yii\web\View
 * @var $searchModel ProductXyzSearch
 * @var $dataProvider ArrayDataProvider
 */

$this->title = 'XYZ Анализ';

$this->registerJs('
    $(document).on("click", ".result-table .result-row", function() {
        $("#product-xyz-grid-filters select, #product-xyz-grid-filters input").val("");
        $("#productxyzsearch-group").val($(this).data("group"));
        $("#product-xyz-grid").yiiGridView("applyFilter");
    });
');
?>
<div class="xyz-content container-fluid"
     style="padding: 0; margin-top: -10px;">
    <?php NavBar::begin([
        'brandLabel' => 'Отчет',
        'brandUrl' => null,
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'items' => [
            ['label' => 'XYZ Анализ', 'url' => ['/product/xyz']],
        ],
        'options' => ['class' => 'navbar-nav navbar-right'],
    ]);
    NavBar::end(); ?>
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-md-7 created-scroll">
            <table class="table table-striped table-bordered table-hover result-table xyz-result-table">
                <tbody>
                <tr class="heading">
                    <th>Группа товаров</th>
                    <th>Кол-во типов товаров</th>
                    <th>% товаров</th>
                    <th>Кол-во товаров на складе</th>
                    <th>% товаров на складе</th>
                </tr>
                <tr class="result-row" data-group="<?= ProductXyzSearch::GROUP_X; ?>">
                    <td>С самым устойчивым спросом (X)</td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_X]['countProductTypes']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_X]['percentProductTypes'], 2) . '%'; ?>
                    </td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_X]['countProductInStock']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_X]['percentProductInStock'], 2) . '%'; ?>
                    </td>
                </tr>
                <tr class="result-row" data-group="<?= ProductXyzSearch::GROUP_Y; ?>">
                    <td>С изменчивым объемом продаж (Y)</td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_Y]['countProductTypes']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_Y]['percentProductTypes'], 2) . '%'; ?>
                    </td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_Y]['countProductInStock']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_Y]['percentProductInStock'], 2) . '%'; ?>
                    </td>
                </tr>
                <tr class="result-row" data-group="<?= ProductXyzSearch::GROUP_Z; ?>">
                    <td>Имеющие случайный спрос (Z)</td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_Z]['countProductTypes']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_Z]['percentProductTypes'], 2) . '%'; ?>
                    </td>
                    <td class="text-right">
                        <?= $searchModel->headerTableData[ProductXyzSearch::GROUP_Z]['countProductInStock']; ?>
                    </td>
                    <td class="text-right">
                        <?= round($searchModel->headerTableData[ProductXyzSearch::GROUP_Z]['percentProductInStock'], 2) . '%'; ?>
                    </td>
                </tr>
                <tr class="result-row" data-group="" style="font-weight: bold;">
                    <td>Итого</td>
                    <td class="text-right"><?= $searchModel->headerTableData['totalProductTypes']; ?></td>
                    <td class="text-right">100%</td>
                    <td class="text-right"><?= $searchModel->headerTableData['totalProductInStock']; ?></td>
                    <td class="text-right">100%</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-3">
            <div style="position: relative;">
                <div class="btn default p-t-7 p-b-7" data-toggle="dropdown" style="width: 100%">
                    <?= $searchModel->periodName ?> <b class="fa fa-angle-down"></b>
                </div>
                <?= Dropdown::widget([
                    'items' => $searchModel->periodItems,
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-sm-3">
                Товары
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'id' => 'product-xyz-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => array_merge(
                        [
                            [
                                'class' => DropDownSearchDataColumn::className(),
                                'attribute' => 'product_title',
                                'label' => 'Название',
                                'headerOptions' => [
                                    'class' => 'text-center nowrap-normal max10list',
                                    'rowspan' => 2,
                                    'colspan' => 1,
                                ],
                                'filter' => $searchModel->getProductTitleFilter(),
                            ],
                            [
                                'label' => 'Продажи (единицы)',
                                'headerOptions' => [
                                    'type' => 'header',
                                    'class' => 'text-center',
                                    'rowspan' => 1,
                                    'colspan' => count($searchModel->monthArray),
                                    'style' => 'border-bottom: 1px solid #ddd;',
                                ],
                            ],
                        ],
                        $searchModel->monthColumns,
                        [
                            [
                                'attribute' => 'coefficientVariation',
                                'label' => 'Коэффициент вариации',
                                'headerOptions' => [
                                    'class' => 'text-center sorting',
                                    'rowspan' => 2,
                                    'colspan' => 1,
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'value' => function ($data) {
                                    return $data['coefficientVariation'] . '%';
                                },
                            ],
                            [
                                'attribute' => 'group',
                                'label' => 'Группа',
                                'headerOptions' => [
                                    'class' => 'text-center dropdown-filter',
                                    'rowspan' => 2,
                                    'colspan' => 1,
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'filter' => $searchModel->getGroupFilter(),
                                'format' => 'html',
                            ],
                        ]
                    ),
                ]); ?>
            </div>
        </div>
    </div>
</div>