<?php

use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

$this->title = 'Добавить ' . ($model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'товар' : 'услугу');
$this->context->layoutWrapperCssClass = 'page-good';

// B2B return
if ('B2B' == Yii::$app->request->get('backTo'))
    $returnUrl = ['/b2b/products'];
else
    $returnUrl = null;

?>
<div style="border-bottom: 1px solid #eee;margin-bottom: 10px;">
    <h4 class="page-title">
        <?= $this->title ?>
    </h4>
</div>

<?= $this->render('_form', [
    'model' => $model,
    'canViewPriceForBuy' => $canViewPriceForBuy,
    'productField' => $productField,
]) ?>
