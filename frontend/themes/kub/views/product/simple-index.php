<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $company common\models\Company*/
/* @var $searchModel frontend\models\SimpleProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $productionType int */

if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $title = 'Выбрать товар из списка';
    $emptyText = 'Все товары уже добавлены';
} elseif ($productionType == Product::PRODUCTION_TYPE_SERVICE) {
    $title = 'Выбрать услугу из списка';
    $emptyText = 'Все услуги уже добавлены';
}
?>

<?php Pjax::begin([
    'id' => 'simple_product_index_pjax',
    //'linkSelector' => false,
    'formSelector' => '#simple_product_index_form',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<h4 class="modal-title"><?= $title ?? ''; ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <?= Icon::get('close') ?>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'simple_product_index_form',
    'method' => 'get',
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="d-flex flex-nowrap">
    <?= $form->field($searchModel, 'search', [
        'options' => [
            'class' => 'form-group flex-grow-1 mr-2',
        ],
    ])->textInput()->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton('Найти', [
            'class' => 'button-clr button-red button-red_small',
            'style' => 'padding: 15px;',
        ]); ?>
    </div>
</div>

<?= GridView::widget([
    'id' => 'simple-product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $searchModel->exclude && !empty($emptyText) ? $emptyText : 'Ничего не найдено.',
    'formatter' => ['class' => 'common\components\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-goods table-style',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr nav-pagination list-clr justify-content-end',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout_no_perpage'),
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'simple_product_selection',
            'cssClass' => 'simple_product_selection',
            'headerOptions' => [
                'width' => '1%',
                'style' => 'padding: 5px;'
            ],
        ],
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'headerOptions' => [
                'width' => '60%',
            ],
        ],
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'headerOptions' => [
                'width' => '20%',
            ],
            'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
        ],
        [
            'attribute' => 'product_unit_id',
            'label' => 'Ед.изм.',
            'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
            'value' => 'productUnit.name',
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа',
            'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
            'value' => 'group.title',
            's2width' => '200px',
        ],
        [
            'attribute' => 'price_for_sell_with_nds',
            'label' => 'Цена продажи',
            'format' => 'money',
            'contentOptions' => [
                'class' => 'text-right',
            ],
        ],
    ],
]) ?>

<?php ActiveForm::end() ?>

<?php Pjax::end(); ?>

<div class="d-flex justify-content-between mt-2">
    <?= Html::button('Добавить отмеченные', [
        'id' => 'add_simple_selected_to_composite',
        'class' => 'button-regular button-regular_red',
    ]) ?>
</div>