<?php

use common\models\product\Product;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

if ($model->production_type == Product::PRODUCTION_TYPE_SERVICE) {
    frontend\assets\CompositeProductAsset::register($this);
}

$this->title = 'Редактировать ' . ($model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'товар' : 'услугу');

$this->context->layoutWrapperCssClass = 'page-good';

echo $this->render('_form', [
    'model' => $model,
    'canViewPriceForBuy' => $canViewPriceForBuy,
    'productField' => $productField,
    'compositeModel' => $compositeModel ?? null,
]);

?>

<?php if ($model->production_type == Product::PRODUCTION_TYPE_SERVICE): ?>
    <?php Modal::begin([
        'id' => 'simple_product_form_modal',
        'closeButton' => false,
        'toggleButton' => false,
    ]); ?>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'simple_product_index_modal',
        'closeButton' => false,
        'toggleButton' => false,
    ]); ?>
    <?php Modal::end(); ?>
<?php endif; ?>