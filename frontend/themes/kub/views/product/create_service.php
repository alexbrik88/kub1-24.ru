<?php

use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\product\Product */
/* @var $compositeModel frontend\models\CompositeServiceForm|null */

$currentTitle = isset($compositeModel) ? 'Добавить составную услугу' : 'Добавить услугу';
$currentLabel = isset($compositeModel) ? 'Составная услуга' : 'Услуга';
?>

<?php Pjax::begin([
    'id' => 'create_service_pjax',
    'enablePushState' => false,
]) ?>

    <div class="d-flex justify-content-between align-items-center mb-4">
        <h4 class="modal-title mb-0"><?= $currentTitle ?></h4>
        <div class="ml-auto">
            <?= Icon::get('question', [
                'class' => 'tooltip-question-icon',
                'title-as-html' => 'true',
                'title' => '«Простая» - Обычная услуга. Например «доставка».
                    <br>
                    «Составная» - Многокомпонентная услуга, которая включает в себя две или более простых услуг или одну, но оказывают её больше одного раза, а также расходные материалы.
                    Например «Уборка помещения 100 м2» состоит из «Услуга уборка» – 2 шт (два сотрудника), расходные материалы: тряпки, моющие средства, пакеты для мусора.
                    <br>
                    Ваш клиент видит только название Составной услуги, а у вас в учете отображается себестоимость за нормочасы по  сотрудникам и стоимость расходных материалов идет в себестоимость.
                    Фактически это технологическая карта данной услуги.',
            ]) ?>
        </div>
        <div class="create_service_type_wrap ml-2">
            <?= Select2::widget([
                'id' => 'create_service_type_select',
                'name' => 'create_service',
                'value' => Yii::$app->controller->action->id,
                'data' => [
                    'create-service' => 'Услуга',
                    'create-service-composite' => 'Составная услуга',
                ],
                'options' => [
                    'prompt' => '',
                    'options' => [
                        'create-service' => [
                            'data-url' => Url::to(['/product/create-service']),
                        ],
                        'create-service-composite' => [
                            'data-url' => Url::to(['/product/create-service-composite']),
                        ],
                    ],
                ],
                'pluginOptions' => [
                    'width' => '200px',
                    'minimumResultsForSearch' => -1,
                ]
            ]); ?>
            <?= Html::a('', Url::current(), [
                'class' => 'hidden create_service_pjax_link',
            ]) ?>
        </div>
    </div>

    <?= $this->render('partial/_service_form', [
        'company' => $company,
        'model' => $model,
        'compositeModel' => $compositeModel ?? null,
        'dataPjax' => true,
    ]) ?>

<?php Pjax::end() ?>