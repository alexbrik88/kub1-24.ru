<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

frontend\assets\CompositeProductAsset::register($this);

if (!isset($canViewPriceForBuy)) {
    $canViewPriceForBuy = true;
}
?>
<div class="product-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'product-form',
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
        ],
    ])); ?>
    <div class="form-body update-product">
        <?= $this->render('partial/_mainForm', [
            'model' => $model,
            'form' => $form,
            'canViewPriceForBuy' => $canViewPriceForBuy,
            'productField' => $productField,
            'compositeModel' => $compositeModel ?? null,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<!-- Add new supplier -->
<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>
<div id="product-group_id-del-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">
                Вы уверены, что хотите удалить группу товара
            </h4>
            <div class="text-center">
                <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 js-item-delete ladda-button',
                    'data-style' => 'expand-right',
                ]) ?>
                <?= Html::button('Нет', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<!-- Change custom field name -->
<?php Modal::begin([
    'id' => 'change-custom-field-name',
    'title' => 'Изменить название дополнительной колонки',
    'toggleButton' => false,
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>

<?= $this->render('partial/_formCustomField', [
    'productField' => $productField,
]); ?>

<?php Modal::end() ?>

<?php $this->registerJs(<<<JS
    $('.change-custom-field-name-btn').on('click', function(e) {
        $('#change-custom-field-name').modal('show');
    });
    $(document).on('submit', '#change-custom-field-name-form', function() {
        $.post($(this).attr('action'), $(this).serialize(), function (data) {

            if (data.result) {
                $('#product-form').find('.custom-field-name-label').text(data.title);
            }

            window.toastr.success(data.result ? "Сохранено" : "Ошибка сохранения", "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 1000,
                "extendedTimeOut": 1000,
                "escapeHtml": false,
            });

            $('#change-custom-field-name').modal('hide');

            Ladda.stopAll();
        });

        return false;
    });

    $('#product-form').on('afterValidateAttribute', function(event, attr, msg) {
        if (msg.length) {

            window.toastr.success(msg[0], "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });

            if (attr.name === 'price_for_sell_with_nds' || attr.name === 'price_for_buy_with_nds') {
                $('.nav-tabs a[data-tab="_productPrice"]').tab('show')
            }
        }
        console.log(attr);
        console.log(msg);
    });

    $('#product_params_nav a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let activeTab = $(this).data('tab');
        let form = $(this).closest('form');
        let cancelBtn = $('.button-cancel-edit', form);
        console.log(activeTab);
        form.attr("action", form.attr("action").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + activeTab));
        cancelBtn.attr("href", cancelBtn.attr("href").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + activeTab));
        $('input[name=tab]', form).val(activeTab);
    });
JS); ?>
