<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.10.2018
 * Time: 11:25
 */

use common\components\helpers\Html;

?>
<div id="delete-confirm-price-list" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        Вы уверены, что хотите удалить прайс-лист <span class="price-list-name"></span>?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', null, [
                            'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button delete-price-list-modal',
                            'data-style' => 'expand-right',
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::button('НЕТ', [
                            'class' => 'btn darkblue',
                            'data-dismiss' => 'modal',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
