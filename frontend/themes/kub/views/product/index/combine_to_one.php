<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$articleCss = $userConfig->product_article ? '' : ' hidden';
$view = ($productionType == Product::PRODUCTION_TYPE_GOODS) ? '_combine_to_one_product' : '_combine_to_one_service';
$jsProductionType = (int)$productionType;

$JS = <<<JS
$(document).on('click', 'button.combine_to_one_apply:not(.disabled)', function() {
    if ($(this).data('units') == '1') {
        $.ajax({
            type: 'post',
            url: $(this).data('url'),
            data: {
                target_id: $('input[name=target]:checked').val(),
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
                production_type: {$jsProductionType}
            },
            success: function() {
                location.reload();
            }
        });
    } else {
        $('.same-units-hint').css('color', 'red');
    }
})
$(document).on('show.bs.modal', '#combine_to_one_modal', function() {
    if ($("input.product_checker:checkbox:checked").length > 1) {
        var unitArray = {};
        $('.quantity_correct', this).removeClass('hidden');
        $("input.product_checker:checkbox:checked").each(function(i, el) {
            var prod = $('#product-grid tr[data-key='+el.value+']');
            var target = $('tr.template').clone().removeClass('hidden template');
            var unit = prod.find('td.col_product_unit').html();
            unitArray[unit] = unit;
            target.find('td.control').html('<input type="radio" name="target" value="'+el.value+'">');
            target.find('td.col_product_article').html(prod.find('.col_product_article').html());
            target.find('td.col_product_name').html(prod.find('td.col_product_name a').html());
            target.find('td.col_product_group').html(prod.find('td.col_product_group').html());
            target.find('td.col_product_unit').html(unit);
            target.appendTo('tbody.combine_product_items', this);
            $('button.combine_to_one_apply').data('units', Object.keys(unitArray).length);
        });
        $('input:radio', this).uniform();
    } else {
        $('.quantity_fail', this).removeClass('hidden');
    }
});
$(document).on('hidden.bs.modal', '#combine_to_one_modal', function() {
    $('.quantity_correct, .quantity_fail', this).addClass('hidden');
    $('tbody.combine_product_items', this).html('');
    $('button.combine_to_one_apply').addClass('disabled');
    $('.same-units-hint').css('color', '#333');
});
$(document).on('click', '.combine_target_row', function() {
    $('input:radio', this).prop('checked', true);
    $('.combine_target_row input:radio').uniform('refresh');
    $('button.combine_to_one_apply').removeClass('disabled');
});
JS;
?>


<?php Modal::begin([
    'id' => 'combine_to_one_modal',
]); ?>

    <?= $this->render($view, ['articleCss' => $articleCss]); ?>

<?php Modal::end(); ?>
<?php $this->registerJs($JS); ?>