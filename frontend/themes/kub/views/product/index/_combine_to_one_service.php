<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<h4 class="modal-title">Объединить в одну услугу</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="move_to_group">
    <div class="quantity_fail hidden">
        Для объединения услуг в одну, необходимо выбрать больше одной услуги.
    </div>
    <div class="quantity_correct hidden">
        <div>
            Выберите услугу, к которому будут объеденены другие услуги.
            <div class="same-units-hint">
                Услуги объединяются только с одинаковой единицей измерения.
            </div>
        </div>
        <table class="combine_product_table table table-style table-count-list" style="width: 100%;">
            <thead>
            <tr class="">
                <th></th>
                <th>Наименование</th>
                <th>Группа</th>
                <th>Единица измерения</th>
            </tr>
            </thead>
            <tbody class="combine_product_items">
            </tbody>
            <tfoot class="hidden">
            <tr class="template combine_target_row" style="cursor: pointer; border-top: 1px solid #ccc;">
                <td class="control"></td>
                <td class="col_product_name"></td>
                <td class="col_product_group"></td>
                <td class="col_product_unit"></td>
            </tr>
            </tfoot>
        </table>
    </div>

</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr combine_to_one_apply quantity_correct',
        'data' => [
            'units' => '1',
            'url' => Url::to(['/product/combine']),
        ],

    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]) ?>
</div>