<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */

$this->title = 'Создание контрагента';
$this->context->layoutWrapperCssClass = 'create-counterparty';
$model->contact_is_director = true;
$model->chief_accountant_is_director = true;


echo $this->render('form/_form', [
    'model' => $model,
    'face_type_opt' => 0,
    'clientForm' => null,
    'contactForms' => null,
    'isCrmView' => false,
]);

// add article modal toggle
$contractorArticleType = ($model->type == Contractor::TYPE_SELLER) ? 'expenditure' : 'income';
echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
    'type' => $contractorArticleType,
    'inputId' => "contractor-invoice_{$contractorArticleType}_item_id",
]);

