<?php

use common\models\Contractor;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $model Contractor */
/* @var $duplicates Contractor[] */

$this->registerJs(<<<JS
    $(document).on("change", "#merge-duplicates-form .merge_item", function() {
        let form = this.form;
        if ($(this).is(":checked")) {
            $($(this).data("id"), form).prop("disabled", false).uniform("refresh");
        } else {
            $($(this).data("id"), form).prop("checked", false).prop("disabled", true).uniform("refresh");
        }
        if ($(".merge_item:checked", form).length > 1) {
            $(".merge_from_error", form).toggleClass("hidden", true);
        }
    });
    $(document).on("change", "#merge-duplicates-form .merge_item_to", function() {
        let form = this.form;
        if ($(".merge_item_to:checked", form).length == 1) {
            $(".merge_to_error", form).toggleClass("hidden", true);
        }
    });
    $(document).on("submit", "#merge-duplicates-form", function(e) {
        if ($(".merge_item:checked", this).length < 2) {
            $(".merge_from_error", this).toggleClass("hidden", false);
            e.preventDefault();
        }
        if ($(".merge_item_to:checked", this).length == 0) {
            $(".merge_to_error", this).toggleClass("hidden", false);
            e.preventDefault();
        }
    });
JS
);

$company = Yii::$app->user->identity->company;
?>

<?= Html::beginForm([
    '/contractor/merge',
    'type' => $type,
    'id' => $model->id,
], 'post', [
    'id' => 'merge-duplicates-form',
]) ?>
    <div class="mb-3">
        <div>
            У вас к компании
        </div>
        <div class="mt-2">
            <?= Html::tag('strong', $model->getShortTitle()) ?>
            Создано <?= date('d.m.Y', $model->created_at) ?>.
            Счетов: <?= $model->getInvoices()->andWhere(['is_deleted' => false])->count() ?> шт.
            Операций по деньгам: <?= $company->getAllAnyFlows([], ['contractor_id' => $model->id])->count() ?> шт.
        </div>
        <div class="my-2">
            Есть дубли:
        </div>
        <?php foreach ($duplicates as $contractor) : ?>
            <div>
                <?= Html::tag('strong', $contractor->getShortTitle()) ?>
                Создано <?= date('d.m.Y', $contractor->created_at) ?>.
                Счетов: <?= $contractor->getInvoices()->andWhere(['is_deleted' => false])->count() ?> шт.
                Операций по деньгам: <?= $company->getAllAnyFlows([], ['contractor_id' => $contractor->id])->count() ?> шт.
            </div>
        <?php endforeach ?>
    </div>
    <div class="mb-3 merge_from_list">
        <label class="label">Выберите компании, которые надо объединить в одну</label>
        <div class="mt-2">
            <?= Html::checkbox('merge_from[]', false, [
                'value' => $model->id,
                'id' => 'merge_from_'.$model->id,
                'data-id' => '#merge_to_'.$model->id,
                'class' => 'merge_item',
                'label' => implode(' ', [
                    Html::tag('strong', $model->getShortTitle()),
                    'Создано '.date('d.m.Y', $model->created_at).'.',
                    ($r = $model->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
                ]),
            ]) ?>
        </div>
        <?php foreach ($duplicates as $contractor) : ?>
            <div>
                <?= Html::checkbox('merge_from[]', false, [
                    'value' => $contractor->id,
                    'id' => 'merge_from_'.$contractor->id,
                    'data-id' => '#merge_to_'.$contractor->id,
                    'class' => 'merge_item',
                    'label' => implode(' ', [
                        Html::tag('span', $contractor->getShortTitle()),
                        'Создано '.date('d.m.Y', $contractor->created_at).'.',
                        ($r = $contractor->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
                    ]),
                ]) ?>
            </div>
        <?php endforeach ?>
        <div class="merge_from_error hidden" style="margin-top: 0.25rem; font-size: 80%; color: #dc3545;">
            Для объединения необходимо выбрать хотя бы двух контрагентов.
        </div>
    </div>
    <div class="mb-3 merge_to_list">
        <label class="label">Выберите компанию, в которую перенесутся все документы и операции по деньгам</label>
        <div class="mt-2">
            <?= Html::radio('merge_to', false, [
                'value' => $model->id,
                'id' => 'merge_to_'.$model->id,
                'class' => 'merge_item_to',
                'disabled' => true,
                'label' => implode(' ', [
                    Html::tag('strong', $model->getShortTitle()),
                    'Создано '.date('d.m.Y', $model->created_at).'.',
                    ($r = $model->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
                ]),
            ]) ?>
        </div>
        <?php foreach ($duplicates as $contractor) : ?>
            <div>
                <?= Html::radio('merge_to', false, [
                    'value' => $contractor->id,
                    'id' => 'merge_to_'.$contractor->id,
                    'class' => 'merge_item_to',
                    'disabled' => true,
                    'label' => implode(' ', [
                        Html::tag('span', $contractor->getShortTitle()),
                        'Создано '.date('d.m.Y', $contractor->created_at).'.',
                        ($r = $contractor->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
                    ]),
                ]) ?>
            </div>
        <?php endforeach ?>
        <div class="merge_to_error hidden" style="margin-top: 0.25rem; font-size: 80%; color: #dc3545;">
            Необходимо выбрать одного контрагента.
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <button type="submit" class="button-regular button-width button-regular_red" data-style="expand-right">Сохранить</button>
        <button type="button" class="button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?= Html::endForm(); ?>
