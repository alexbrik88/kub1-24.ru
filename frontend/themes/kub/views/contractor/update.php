<?php

use common\models\Contractor;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ContactForm;

/* @var $this yii\web\View */
/* @var $model Contractor */

/**
 * @var ContactForm[] $contactForms
 * @var ClientForm $clientForm
 */

$this->title = 'Обновить контрагента: ' . ' ' . $model->name;

echo $this->render('form/_form', [
    'model' => $model,
    'type' => $type,
    'clientForm' => $clientForm,
    'contactForms' => $contactForms,
    'isCrmView' => false,
    'face_type_opt' => 1,
]);

// add article modal toggle
$contractorArticleType = ($model->type == Contractor::TYPE_SELLER) ? 'expenditure' : 'income';
echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
    'type' => $contractorArticleType,
    'inputId' => "contractor-invoice_{$contractorArticleType}_item_id",
]);

echo $this->render('form/_partial_modals', [
    'model' => $model
]);