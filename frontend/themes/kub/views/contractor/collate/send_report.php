<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */
/* @var $sent integer */

$company = $model->company;
$contractor = $model->contractor;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<h3 class="modal-title">Отправка акта сверки</h3>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="form-group">
    Отправлено писем: <?= $sent ?>
</div>

<div class="text-center mt-3">
    <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
</div>

<?php $pjax->end(); ?>
