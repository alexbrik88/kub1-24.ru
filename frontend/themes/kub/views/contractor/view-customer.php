<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\store\StoreCompanyContractor;
use frontend\components\WebUser;
use frontend\models\Documents;
use frontend\modules\crm\models\Client;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\document\Collate;
use frontend\rbac\permissions\document\Invoice;
use frontend\themes\kub\components\Icon;
use frontend\widgets\RangeButtonWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $activeTab integer */
/* @var $analyticsMonthNumber string */
/* @var $analyticsYearNumber string */
/* @var $sellingMonthNumber string */
/* @var $sellingYearNumber string */
/* @var $abcGroup integer */
/* @var $disciplineGroup integer */

/**
 * @var WebUser $user
 */

$this->title = $model->nameWithType;
$this->context->layoutWrapperCssClass = 'customer';

$isLegal = ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
$isPhysical = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
$isForeign = ($model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON);

if (!isset($user)) {
    $user = Yii::$app->user;
}
if (!isset($type)) {
    $type = $model->type;
}
if (!isset($ioType)) {
    $ioType = $model->type;
}
$tabLink = Url::toRoute([
    '/contractor/view',
    'type' => $type,
    'id' => $model->id,
    'per-page' => Yii::$app->request->get('per-page')
]);

$operationsLink = Url::toRoute([
    '/contractor/operations',
    'type' => $ioType,
    'id' => $model->id,
    'per-page' => Yii::$app->request->get('per-page')
]);

$tabFile = ArrayHelper::remove($tabData, 'tabFile');
$tabData = array_merge($tabData, [
    'isLegal' => $isLegal,
    'isPhysical' => $isPhysical,
    'isForeign' => $isForeign,
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'click',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-store',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'side' => 'top'
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

$employee = $user->identity;
$company = $employee->company;
$isStoreActive = $model->getStoreCompanyContractors()
    ->andWhere(['status' => StoreCompanyContractor::STATUS_ACTIVE])
    ->exists();
$hasStore = $model->getStoreCompanyContractors()->one();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$disabled = !$canAddStoreCabinet && !$isStoreActive;
$invCount = $company->getInvoiceLeft();

$contractorNavColumnClass = 'nav-item';

$invoiceItems = [
    [
        'label' => 'Исходящий',
        'url' => [
            $model->getIsForeignCurrency() ?
            '/documents/foreign-currency-invoice/create' :
            '/documents/invoice/create',
            'type' => Documents::IO_TYPE_OUT,
            'contractorId' => $model->id,
        ],
    ],
    [
        'label' => 'Входящий',
        'url' => [
            $model->getIsForeignCurrency() ?
            '/documents/foreign-currency-invoice/create' :
            '/documents/invoice/create',
            'type' => Documents::IO_TYPE_IN,
            'contractorId' => $model->id,
        ],
    ],
];

$duplicates = $model->getDuplicateQuery()->all();

$moreItems = [];
if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)) {
    /*$moreItems[] = [
        'label' => 'Акт сверки',
        'url' => [
            'create-autoinvoice',
            'type' => Documents::IO_TYPE_OUT,
            'id' => $model->id,
        ],
        'linkOptions' => [
            'id' => 'collate-report-button',
            'data-toggle' => 'modal',
            'data-target' => '#modal-collate-report'
        ]
    ];*/
}
if ($isLegal) {
    /*$moreItems[] = [
        'label' => ($model->verified ? 'Досье ' : 'Проверить ') . ($model->type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика'),
        'url' => ['/dossier', 'id' => $model->id],
        'linkOptions' => [
            'data-toggle' => 'modal',
            'data-target' => '#popup-confirm'
        ]
    ];*/
}
if ($model->is_customer && !$isForeign) {
    if ((YII_ENV_DEV || in_array($employee->company->id, [486, 1, 2031, 7642, 1871])) && in_array($employee->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])) {
        /*$moreItems[] = [
            'label' => 'АвтоСбор Долгов',
            'url' => ['/payment-reminder/index']
        ];*/
    }
}
if ($duplicates && Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)) {
    $moreItems[] = [
        'label' => 'Объединение дублей',
        'url' => '#merge-duplicates-modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ]
    ];
}

if ($model->is_customer) {
    if ($tab == 'autoinvoice') {
        $label = '<span class="ia-tab pr-2" data-href="' . $tabLink . '&tab=autoinvoice' . '">АвтоСчета</span>';
    } else {
        $label = '<span class="ia-tab pr-2" data-href="' . $tabLink . '">Счета</span>';
    }
    $tabInvoice = [
        'label' => $label . '<span class="ia-drop">' . Icon::get('shevron', ['class' => 'dropdown-btn-shevron']) . '</span>',
        'dropDownOptions' => ['class' => 'form-filter-list list-clr'],
        'url' => $tabLink . ($tab === 'autoinvoice' ? '&tab=autoinvoice' : ''),
        //'active' => $tab == null || $tab == 'autoinvoice',
        'options' => [
            'class' => $contractorNavColumnClass . ' dropdown tab-label-dropdown ',
        ],
        'linkOptions' => [
            'class' => 'invoice-autoinvoice nav-link dropdown-btn' . ($tab == '' || $tab == 'autoinvoice' ? ' active' : ''),
            'data-toggle' => 'dropdown',
        ],
        'encode' => false,
        'items' => [
            ['label' => 'Счета', 'url' => $tabLink],
            ['label' => 'АвтоСчета', 'url' => $tabLink . '&tab=autoinvoice'],
        ],
    ];
} else {
    $tabInvoice = [
        'label' => 'Счета',
        'url' => $tabLink,
        'options' => [
            'class' => $contractorNavColumnClass,
        ],
        'linkOptions' => [
            'class' => 'nav-link' . ($tab == null ? ' active' : '')
        ],
    ];
}

$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->getInvoicesAuto()->exists();

$dossierAllowed = $isLegal;
//$needDossierConfirm = $company->needDossierConfirm($model);

$this->registerJs('
    $(document).on("change", "#activate_store_account:not(:checked)", function() {
        changeCabinet($("#activate_store_account").serialize());
    });
    $(document).on("click", "#add-store-account .yes", function() {
        changeCabinet({store_account: 1});
    });
        $(document).on("click", "#add-store-account .no", function() {
        $("#activate_store_account").prop("checked", false);
    });

    function changeCabinet(val) {
        var isChecked = $("#activate_store_account").prop("checked");
        $.post("' . Url::to(['store-account', 'id' => $model->id]) . '", val, function(data) {
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value != undefined) {
                if (data.value == false) {
                    $icon = "error";
                }
                $("#activate_store_account").prop("checked", data.value);
            } else {
                $("#activate_store_account").prop("checked", !isChecked);
            }
            var $value = $("#activate_store_account").is(":checked");
            if ($value) {
                $("#activate_store_account").removeAttr("data-toggle");
                $("#activate_store_account").removeAttr("data-target");
            } else if (data.canAddStoreCabinet == 1) {
                $("#activate_store_account").attr("data-toggle", "modal");
                $("#activate_store_account").attr("data-target", "#add-store-account");
            }

            if (data.label != undefined) {
                $(".contractor-store-label").text(data.label);
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    }

    var $customerBlockIn = $(".contractor-in-block");
    var $customerBlockOut = $(".contractor-out-block");
    $(".update-contractor").click(function (e) {
        if ($(".contractor-info-tab").hasClass("active")) {
            $customerBlockIn.show();
            $customerBlockOut.hide();
        } else {
            location.href = $(".contractor-info-tab > a").attr("href") + "&edit=1";
        }
    });

    $(".undo-contractor").click(function (e) {
        $customerBlockIn.hide();
        $customerBlockOut.show();
    });
');

$backUrl = Url::to(['/contractor/index', 'type' => $type]);
$referrer = Yii::$app->request->referrer;
$parts = parse_url($referrer);
if ($parts['path'] === '/project/view') {
    $backUrl = $referrer;
}
?>

<?php if ($duplicates) : ?>
    <?php Modal::begin([
        'id' => 'merge-duplicates-modal',
        'title' => 'Объединение дублей',
        'toggleButton' => false,
        'closeButton' => [
            'label' => frontend\components\Icon::get('close'),
            'class' => 'modal-close close',
        ],
    ]) ?>
        <?= $this->render('_merge_form', [
            'type' => $type,
            'model' => $model,
            'duplicates' => $duplicates,
        ]) ?>
    <?php Modal::end() ?>
<?php endif ?>
<div id="contractor-header">
    <a class="link mb-2" href="<?= $backUrl ?>">
        Назад к списку
    </a>

    <!-- KUB -->
    <div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <div class="pr-2">
                        <div class="row align-items-center justify-content-between mb-3">
                            <h4 class="column mb-2" style="max-width: 550px;"><?= Html::encode($this->title); ?></h4>
                            <div class="column" style="margin-bottom: auto;">
                                <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                        type="button"
                                        data-toggle="modal"
                                        title="Последние действия"
                                        href="#basic">
                                    <svg class="svg-icon svg-icon_size_">
                                        <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                    </svg>
                                </button>
                                <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::UPDATE, [
                                    'model' => $model,
                                ])
                                ): ?>
                                    <a href="<?= Url::to(['/contractor/update', 'id' => $model->id, 'type' => $type, 'tab' => $tab]) ?>"
                                       class="button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                                       title="Изменить">
                                        <svg class="svg-icon">
                                            <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                        </svg>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column mb-4">
                                <div class="label weight-700 mb-3">Контакт</div>
                                <div><?= $model->getRealContactName() ?: '—' ?></div>
                                <div class="label weight-700 mt-4 mb-3">
                                    <?= ($model->type == Contractor::TYPE_CUSTOMER) ?
                                        'Отсрочка оплаты по счету' : 'Отсрочка оплаты' ?>
                                </div>
                                <div>
                                    <?= ($model->type == Contractor::TYPE_CUSTOMER) ?
                                        $model->customer_payment_delay : $model->seller_payment_delay ?> дн.
                                </div>

                                <?php if ($model->isSellerCustomer): ?>
                                    <div class="label weight-700 mt-4 mb-3">Отсрочка оплаты в днях</div>
                                    <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                                        <div><?= $model->seller_payment_delay ?: '—' ?></div>
                                    <?php endif; ?>
                                    <?php if ($model->type == Contractor::TYPE_SELLER): ?>
                                        <div><?= $model->customer_payment_delay ?: '—' ?></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="column mb-4">
                                <div class="label weight-700 mb-3">Телефон</div>
                                <div><?= $model->getRealContactPhone() ?: '—' ?></div>
                                <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                                    <div class="label weight-700 mt-4 mb-3">Статья приходов</div>
                                    <div><?= $model->income->name ?? '—' ?></div>
                                <?php else: ?>
                                    <div class="label weight-700 mt-4 mb-3">Статья расходов</div>
                                    <div><?= $model->expenditure->name ?? '—' ?></div>
                                <?php endif; ?>

                                <?php if ($model->isSellerCustomer): ?>
                                    <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                                        <div class="label weight-700 mt-4 mb-3">Статья расходов</div>
                                        <div><?= $model->expenditure->name ?? '—' ?></div>
                                    <?php endif; ?>
                                    <?php if ($model->type == Contractor::TYPE_SELLER): ?>
                                        <div class="label weight-700 mt-4 mb-3">Статья приходов</div>
                                        <div><?= $model->income->name ?? '—' ?></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="column mb-4">
                                <div class="label weight-700 mb-3">E-mail</div>
                                <div>
                                    <?php if ($model->getRealContactEmail()): ?>
                                        <a class="link" href="mailto:<?= $model->getRealContactEmail() ?>"><?= $model->getRealContactEmail() ?></a>
                                    <?php else: ?>
                                        —
                                    <?php endif; ?>
                                </div>
                                <?php if ($model->discount > 0) : ?>
                                    <div class="label weight-700 mt-4 mb-3">Фиксированная скидка на всё, %</div>
                                    <div><?= $model->discount ?></div>
                                <?php endif ?>
                                <div class="label weight-700 mt-4 mb-3">Не разнесенные оплаты</div>
                                <div>
                                    <?php if ($prepaidAmount = $model->getPrepaidAmount()): ?>
                                        <?= TextHelper::invoiceMoneyFormat($prepaidAmount, 2) ?> ₽
                                    <?php else: ?>
                                        —
                                    <?php endif; ?>
                                </div>
                                <?php if ($model->isSellerCustomer): ?>
                                    <div class="label weight-700 mt-4 mb-3">Не разнесенные оплаты</div>
                                    <div>
                                    <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                                        <?php if ($prepaidAmount = $model->getPrepaidAmount(Contractor::TYPE_SELLER)): ?>
                                            <?= TextHelper::invoiceMoneyFormat($prepaidAmount, 2) ?> ₽
                                        <?php else: ?>
                                            —
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if ($model->type == Contractor::TYPE_SELLER): ?>
                                        <?php if ($prepaidAmount = $model->getPrepaidAmount(Contractor::TYPE_CUSTOMER)): ?>
                                            <?= TextHelper::invoiceMoneyFormat($prepaidAmount, 2) ?> ₽
                                        <?php else: ?>
                                            —
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if ($model->isSellerCustomer): ?>
                                <div class="column mb-4 seller-customer-label">
                                    <div class="label weight-700 mb-3">Покупатель и поставщик</div>
                                    <div><input type="checkbox" disabled checked/></div>
                                </div>
                            <?php endif; ?>
                            <?php /* if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="column">
                                    <?= $hasStore ?
                                        ($isStoreActive ? 'Отключить кабинет' : 'Включить кабинет')
                                        : ($model->type == Contractor::TYPE_CUSTOMER ?
                                            Html::a('Создать кабинет покупателя', Url::to(['/contractor/sale-increase'], ['class' => 'link'])) :
                                            'Создать кабинет'); ?>
                                </div>
                                <div class="column">
                                    <div class="d-flex flex-nowrap align-items-center">

                                        <div class="switch <?= $disabled ? ' tooltip2-store' : ''; ?>" data-tooltip-content="<?= $disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : ''; ?>">
                                            <?= \yii\bootstrap4\Html::checkbox('store_account', $isStoreActive, [
                                                'id' => 'activate_store_account',
                                                'class' => 'switch switch-input input-hidden md-check',
                                                'disabled' => $disabled,
                                                'data-toggle' => $employee->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                    (!$isStoreActive && !$disabled ?
                                                        (empty($model->director_email) ? 'modal' : 'modal') : null) :
                                                    'modal',
                                                'data-target' => $employee->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                    !$isStoreActive && !$disabled ?
                                                        (empty($model->director_email) ?
                                                            '#empty-contractor-email' :
                                                            '#add-store-account') : null :
                                                    '#no-rights-store-account',
                                            ]) ?>
                                            <label class="switch-label" for="activate_store_account">
                                                <span class="switch-pseudo">&nbsp;</span>
                                            </label>
                                            <?php if ($disabled): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                    <span id="tooltip_pay-to-add-cabinet-<?= $model->id; ?>"
                                                          style="display: inline-block; text-align: center;">
                                                        Кабинет можно добавить после
                                                        <?= Html::a('оплаты', Url::to(['/contractor/sale-increase',
                                                            'activeTab' => Contractor::TAB_PAYMENT,
                                                        ])); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div class="tooltip-box ml-2 mr-2">
                                            <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title="Самый быстрый способ увеличить продажи товара – упростить процесс заказа товара. Для этого подключите покупателей к вашему складу!">
                                                <svg class="tooltip-question-icon svg-icon">
                                                    <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; */ ?>
                        </div>
                    </div>
                </div>
                <div class="col-3 column pl-0">
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, ['ioType' => $ioType]) && $model->company_id !== null): ?>
                        <div class="pb-2 mb-1">
                            <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                                <?php if ($model->isSellerCustomer) : ?>
                                    <div class="dropdown">
                                        <?= Html::a(Icon::get('add-icon', [
                                            'class' => 'mr-2',
                                        ]).' СЧЕТ', '#', [
                                            'class' => 'button-regular button-clr button-regular_red w-100',
                                            'data-toggle' => 'dropdown',
                                            'aria-expanded' => 'false'
                                        ]); ?>

                                        <?= Dropdown::widget([
                                            'options' => [
                                                'class' => 'dropdown-menu form-filter-list list-clr w-100',
                                            ],
                                            'items' => $invoiceItems,
                                        ]); ?>
                                    </div>
                                <?php else : ?>
                                    <?= Html::a(Icon::get('add-icon', [
                                        'class' => 'mr-2',
                                    ]).' СЧЕТ', [
                                        $model->getIsForeignCurrency() ?
                                        '/documents/foreign-currency-invoice/create' :
                                        '/documents/invoice/create',
                                        'type' => $model->is_seller ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT,
                                        'contractorId' => $model->id,
                                    ], [
                                        'class' => 'button-regular button-clr button-regular_red w-100'
                                    ]) ?>
                                <?php endif; ?>
                            <?php else : ?>
                                <button class="button-regular button-clr button-regular_red w-100 action-is-limited"
                                    <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                                    <svg class="svg-icon ml-auto">
                                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                                    </svg>
                                    <span class="mr-auto" style="margin-left:8px">СЧЕТ</span>
                                </button>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div class="pb-2 mb-1">
                        <?= frontend\widgets\RangeButtonWidget::widget(['zIndex' => null]); ?>
                    </div>
                    <?php if ($model->is_customer && $tab != 'autoinvoice' && !$isForeign) : ?>
                        <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT) && !$hasAutoinvoice) : ?>
                            <div class="pb-2 mb-1">
                                <?= Html::a('АвтоСчета', [
                                    'create-autoinvoice',
                                    'type' => Documents::IO_TYPE_OUT,
                                    'id' => $model->id,
                                ], [
                                    'class' => 'button-regular button-hover-content-red button-clr w-100',
                                ]) ?>
                            </div>
                        <?php endif ?>
                    <?php endif; ?>
                    <?php /* todo
                <?php if ($model->type == Contractor::TYPE_CUSTOMER && !in_array($employee->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_ACCOUNTANT, EmployeeRole::ROLE_ASSISTANT])): ?>
                <div class="pb-2 mb-1">
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index', 'PaymentReminderMessageContractorSearch' => [
                            'contractor' => $model->id,
                        ]]), [
                            'class' => 'button-regular button-hover-content-red button-clr w-100',
                        ]); ?>
                </div>
                <?php endif; ?>
                */ ?>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)): ?>
                        <div class="pb-2 mb-1">
                            <?= Html::tag('span', 'Акт сверки', [
                                'id' => 'collate-report-button',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-collate-report',
                                'class' => 'button-regular button-hover-content-red button-clr w-100',
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($moreItems): ?>
                    <div class="dropdown">
                        <?= Html::a('<span>Еще</span>' . Icon::get('shevron', ['class' => 'svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute']), '#', [
                            'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => 'false'
                        ]); ?>

                        <?= Dropdown::widget([
                            'options' => [
                                'class' => 'dropdown-menu form-filter-list list-clr w-100'
                            ],
                            'items' => $moreItems
                        ]); ?>
                    </div>
                    <?php endif; ?>

                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)): ?>
                        <div>
                            <?php
                            $url = Url::to(['/contractor/collate', 'step' => 'form', 'id' => $model->id]);
                            Modal::begin([
                                'id' => 'modal-collate-report',
                                'closeButton' => false,
                                'size' => 'modal-dialog-centered',
                                //'toggleButton' => [
                                //    'tag' => 'button',
                                //    'id' => 'collate-report-button',
                                //    'label' => ' АКТ СВЕРКИ',
                                //    'class' => 'button-regular button-hover-content-red button-clr w-100',
                                //],
                            ]);

                            $pjax = Pjax::begin([
                                'id' => 'collate-pjax-container',
                                'enablePushState' => false,
                                'enableReplaceState' => false,
                                'linkSelector' => '.collate-pjax-link',
                                'timeout' => 5000,
                            ]);

                            $pjax->end();

                            Modal::end();

                            $this->registerJs('
                            $(document).on("click", "#collate-report-button", function() {
                                $.pjax.reload("#collate-pjax-container", {url: ' . json_encode($url) . ', push: false, replace: false, timeout: 5000})
                            });
                            $(document).on("pjax:complete", "#collate-pjax-container", function(event) {
                                refreshDatepicker();
                                refreshUniform();
                                $("#modal-collate-report").find(".dropup > a").first().click();
                            });
                            $(document).on("hidden.bs.modal", "#modal-collate-report", function () {
                                $("#collate-pjax-container").html("");
                            })
                            ');
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="contractor-tabs" class="nav-tabs-row">
        <?= Nav::widget([
            'id' => 'contractor-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                $tabInvoice,
                [
                    'label' => 'Деньги',
                    'url' => $operationsLink,
                    'options' => [
                        'class' => $contractorNavColumnClass,
                        'style' => ($employee->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ? ['display' => 'block'] : ['display' => 'none']),
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'operations' ? ' active' : '')
                    ]
                ],
                [
                    'label' => 'Инвойсы',
                    'url' => $tabLink . '&tab=foreign_invoice',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                        'style' => ($model->getIsForeign() ? ['display' => 'block'] : ['display' => 'none']),
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'foreign_invoice' ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Договоры',
                    'url' => $tabLink . '&tab=agreements',
                    //'active' => $tab == 'agreements',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'agreements' ? ' active' : '')
                    ]
                ],
                [
                    'label' => 'Аналитика',
                    'url' => $tabLink . '&tab=analytics',
                    //'active' => $tab == 'analytics',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'analytics' ? ' active' : '')
                    ],
                    'visible' => $ioType == Contractor::TYPE_CUSTOMER,
                ],
                [
                    'label' => $model->isSellerCustomer ? 'Оборот' : ($ioType == Contractor::TYPE_CUSTOMER ? 'Продажи' : 'Закупки'),
                    'url' => $tabLink . '&tab=selling',
                    //'active' => $tab == 'selling',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'selling' ? ' active' : '')
                    ],
                    'visible' => ($ioType == Contractor::TYPE_CUSTOMER || $ioType == Contractor::TYPE_SELLER && !$model->is_agent)
                ],
                [
                    'label' => 'Агентские отчеты',
                    'url' => $tabLink . '&tab=agent_report',
                    //'active' => $tab == 'agent_report',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'agent_report' ? ' active' : '')
                    ],
                    'visible' => (false && $ioType == Contractor::TYPE_SELLER && $model->is_agent)
                ],
                [
                    'label' => 'Карточка',
                    'url' => $tabLink . '&tab=info',
                    //'active' => $tab == 'info',
                    'options' => [
                        'class' => $contractorNavColumnClass . ' contractor-info-tab',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'info' ? ' active' : '')
                    ],
                ],
                /*[
                    'label' => 'Досье',
                    'url' => str_replace('/contractor/view', '/dossier', $tabLink),
                    'active' => $tab === 'dossier',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'visible' => $dossierAllowed && !$needDossierConfirm,
                ],*/
                /*[
                    'label' => 'Досье',
                    'active' => $tab === 'dossier',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'data-toggle' => 'modal',
                        'data-target' => '#popup-confirm'
                    ],
                    'visible' => $dossierAllowed && $needDossierConfirm,
                ],*/
                [
                    'label' => 'CRM',
                    'url' => Url::to([
                        '/crm/client/tasks',
                        'contractor_id' => $model->id,
                        'contractor' => true,
                    ]),
                    'active' => $tab === 'crm',
                    'options' => [
                        'class' => $contractorNavColumnClass,
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'crm' ? ' active' : '')
                    ],
                    'visible' => $user->can(ClientAccess::VIEW, ['contractor' => $model]),
                ],
            ],
        ]); ?>
    </div>
<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane view-contractor-tab active">
            <div class="pt-2">
                <div class="pt-1">
                    <?= $this->render("{$tabFile}", $tabData); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

    <div id="add-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12 text-center">
                                Вы уверены, что хотите создать личный кабинет для <br>
                                данного покупателя? После нажатия на кнопку ДА, <br>
                                покупателю будет отослано на e-mail письмо <br>
                                с приглашением в личный кабинет. <br>
                                В письме также будет логин и пароль для входа.
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <button type="button" class="yes button-regular button-width button-regular_red button-clr mr-3" data-dismiss="modal">Да</button>
                        <button type="button" class="no button-regular button-clr button-width button-hover-transparent" data-dismiss="modal">Нет</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="empty-contractor-email" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12 text-center">
                                У данного покупателя не заполнено поле с e-mail. <br>
                                Заполните в закладке "Карточка покупателя".
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="no-rights-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12 text-center">
                                У вас не достаточно прав для создания кабинета. Обратитесь к руководителю.
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

    <div class="created-by">
        <?= date('d.m.Y', $model->created_at) ?>
        <?php if ($model->employee) : ?>
            Создал <?= $model->employee->fio ?>
        <?php else : ?>
            Автосоздание
        <?php endif ?>
    </div>

<?php Modal::end(); ?>

<?= $this->render('../dossier/_popup-confirm', [
    'link' => Url::toRoute(['/dossier', 'id' => $model->id]),
]) ?>

    <div style="display: none">
        <?php /* preload styles */ ?>
        <?= Select2::widget([
            'name' => 'empty',
            'data' => []
        ]); ?>
    </div>

<?php $this->registerJs(<<<JS

$('.invoice-autoinvoice').on('click', function(e) {
    if ($(e.target).hasClass('ia-tab')) {
        $('#contractor-menu .dropdown-menu').remove();
        location.href = $(e.target).attr('data-href');
        return false;
    }
});

JS
);
