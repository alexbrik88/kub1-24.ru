<?php
/* @var $model common\models\Contractor */
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
$textInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$addressInputConfig = [
    'options' => [
        'class' => 'form-group col-12',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$textInputConfig2 = [
    'options' => [
        'class' => 'form-group  col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
        'autocomplete' => 'off'
    ],
    'template' => "{label}\n{input}\n{error}",
];
?>

<button class="link link_collapse link_bold button-clr mb-4 collapsed" type="button" data-toggle="collapse" data-target="#dopColumns2" aria-expanded="false" aria-controls="dopColumns2">
    <span class="link-txt">Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
    </svg>
</button>

<div id="dopColumns2" class="collapse dopColumns">
    <div class="row face_type_depend face_type_0 face_type_2<?= ($isLegal || $isForeign ? '' : ' hidden') ?>">
        <?= $form->field($model, 'director_post_name', $textInputConfig)->textInput([
            'disabled' => !$isLegal && !$isForeign,
        ])->label('Должность руководителя'); ?>
        <?= $form->field($model, 'director_name', $textInputConfig)->textInput([
            'disabled' => !$isLegal && !$isForeign,
        ])->label('ФИО руководителя'); ?>
        <?= $form->field($model, 'legal_address', $addressInputConfig)->textInput([
            'maxlength' => true,
            'disabled' => !$isLegal && !$isForeign,
        ]); ?>
    </div>
    <div class="row face_type_depend face_type_0<?= ($isLegal ? '' : ' hidden') ?>">
        <?= $form->field($model, 'current_account', $textInputConfig2)->textInput([
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'BIC', $textInputConfig2)->widget(\common\components\widgets\BikTypeahead::classname(), [
            'remoteUrl' => Url::to(['/dictionary/bik']),
            'related' => [
                '#' . Html::getInputId($model, 'bank_name') => 'name',
                '#' . Html::getInputId($model, 'bank_city') => 'city',
                '#' . Html::getInputId($model, 'corresp_account') => 'ks',
            ],
        ])->textInput([
            'placeHolder' => 'Нужен для Актов и Товарных накладных',
            'autocomplete' => 'off',
            'disabled' => !$isLegal,
        ]); ?>

        <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
            'maxlength' => true,
            'readonly' => true,
            'disabled' => !$isLegal,
        ]); ?>
        <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
            'maxlength' => true,
            'readonly' => true,
            'disabled' => !$isLegal,
        ]); ?>
        <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
            'maxlength' => true,
            'readonly' => true,
            'disabled' => !$isLegal,
        ]); ?>
    </div>
    <div class="row face_type_depend face_type_2<?= ($isForeign ? '' : ' hidden') ?>">
        <?= $form->field($model, 'current_account', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-current_account',
            'placeholder' => '0123456789',
            'disabled' => !$isForeign,
            'maxlength' => '35',
        ]); ?>

        <?= $form->field($model, 'BIC', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-bic',
            'placeholder' => 'ALFABANK',
            'disabled' => !$isForeign,
        ])->label('SWIFT'); ?>

        <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-bank_name',
            'placeholder' => 'ALFABANK',
            'disabled' => !$isForeign,
        ]); ?>
        <?= $form->field($model, 'bank_address', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-bank_address',
            'disabled' => !$isForeign,
            'placeholder' => 'Пример: USA, NY, street, etc',
        ])->label('Адрес банка'); ?>
        <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-bank_city',
            'disabled' => !$isForeign,
        ]); ?>
        <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-corresp_account',
            'disabled' => !$isForeign,
        ])->label('Correspondent bank account №'); ?>
        <?= $form->field($model, 'corr_bank_name', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-corr_bank_name',
            'disabled' => !$isForeign,
        ]); ?>
        <?= $form->field($model, 'corr_bank_address', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-corr_bank_address',
            'disabled' => !$isForeign,
        ]); ?>
        <?= $form->field($model, 'corr_bank_swift', $textInputConfig)->textInput([
            'id' => 'foreign_contractor-corr_bank_swift',
            'disabled' => !$isForeign,
        ]); ?>
    </div>
</div>