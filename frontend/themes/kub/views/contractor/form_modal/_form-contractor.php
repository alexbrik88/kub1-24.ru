<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use frontend\models\Documents;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */
$textInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$checkboxConfig = [
    'options' => [
        'class' => 'form-group col-6',
        'style' => 'padding-top: 36px'
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

if ($model->face_type === null || $model->face_type === '') {
    $model->face_type = Contractor::TYPE_LEGAL_PERSON;
}

if ($model->isNewRecord) {
    $faceTypeList = [
        Contractor::TYPE_LEGAL_PERSON => 'Юр. лицо',
        Contractor::TYPE_PHYSICAL_PERSON => 'Физ. лицо',
        Contractor::TYPE_FOREIGN_LEGAL_PERSON => 'Иностр. юр. лицо',
    ];
} else {
    $faceTypeList = [
        $model->face_type => $faceTypeList[$model->face_type],
    ];
}

$showContractorType = Yii::$app->request->post('show_contractor_type');
$innPlaceholder = 'Автозаполнение по ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ? ' покупателя' : ' продавца') : '');
$innLabel = 'ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ?  ' покупателя' : ' продавца') : '');
$isLegal = $model->face_type == Contractor::TYPE_LEGAL_PERSON;
$isPhysical = $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
$isForeign = $model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
$typeLabel = $model->type == Contractor::TYPE_SELLER ? 'Тип поставщика ' : 'Тип покупателя ';
?>

<?php yii\bootstrap4\Modal::begin([
    'id' => 'new-contractor-duplicates-modal',
    'title' => $model->isNewRecord ? 'Дубли Контрагента' : 'Объединить покупателя и поставщика',
    'toggleButton' => false,
    'closeButton' => [
        'label' => frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>
<?php yii\bootstrap4\Modal::end() ?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => '',
        'data-find-duplicate' => Url::to([
            '/contractor/find-duplicate',
            'id' => $model->id,
            'type' => $documentType,
        ]),
    ],
    'id' => 'new-contractor-invoice-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]); ?>
<?= Html::hiddenInput('documentType', $documentType); ?>
<?php if (!$model->isNewRecord) {
    $invoiceId = Yii::$app->request->post('invoiceId');
    echo Html::hiddenInput('contractorId', $model->id);
    echo Html::hiddenInput('invoiceId', $invoiceId);
} ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'face_type', [
            'labelOptions' => [
                'class' => 'label',
            ],
            'radioOptions' => [
                'class' => '',
            ],
        ])->label($typeLabel)->radioList($faceTypeList, [
            'itemOptions' => [
                'wrapperOptions' => [
                    'class' => 'd-flex',
                    'style' => 'padding: 0 10px;',
                ],
            ],
            'class' => 'd-flex align-items-center',
            'style' => 'margin-left: -10px; margin-right: -10px; height: 44px;'
        ]) ?>
    </div>
    <?= $form->field($model, 'ITN', [
        'options' => [
            'class' => 'form-group col-6 face_type_depend face_type_0'.($isLegal ? '' : ' hidden'),
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control form-control_placeholder_red',
        ],
        'template' => "{label}\n{input}\n{error}",
    ])->label($innLabel)->textInput([
        'placeholder' => $innPlaceholder,
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'prod' => YII_ENV_PROD ? 1 : 0,
        ],
        'readonly' => !$model->isNewRecord,
        'disabled' => !$isLegal,
    ]); ?>
    <?= $form->field($model, 'ITN', [
        'options' => [
            'class' => 'form-group col-6 face_type_depend face_type_2'.($isForeign ? '' : ' hidden'),
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'template' => "{label}\n{input}\n{error}",
    ])->label('ИНН/ITN')->textInput([
        'id' => 'foreign_'.Html::getInputId($model, 'ITN'),
        'placeholder' => 'Пример: 0123456789',
        'maxlength' => true,
        'readonly' => !$model->isNewRecord,
        'disabled' => !$isForeign,
    ]); ?>
</div>

<div class="face_type_depend face_type_0 face_type_2<?= ($isLegal || $isForeign ? '' : ' hidden') ?>">
    <?= $this->render('_partial_main_info', [
        'model' => $model,
        'form' => $form,
        'class' => 'required',
        'textInputConfigDirector' => $partialDirectorTextInputConfig,
        'face_type_opt' => $face_type_opt,
        'textInputConfig' => $textInputConfig,
        'documentType' => $documentType,
        'isLegal' => $isLegal,
        'isForeign' => $isForeign,
    ]) ?>
    <div class="clearfix"></div>
    <?= $this->render('_partial_details', [
        'model' => $model,
        'form' => $form,
        'isLegal' => $isLegal,
        'isForeign' => $isForeign,
    ]) ?>
</div>
<div class="face_type_depend face_type_1<?= ($isPhysical ? '' : ' hidden') ?>">
    <div class="row">
        <?= $form->field($model, 'physical_lastname', $textInputConfig)->label('Фамилия')->textInput([
            'maxlength' => true,
            'disabled' => !$isPhysical,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_firstname', $textInputConfig)->label('Имя')->textInput([
            'maxlength' => true,
            'disabled' => !$isPhysical,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_patronymic', $textInputConfig)->label('Отчество')->textInput([
            'maxlength' => true,
            'disabled' => !$isPhysical,
            'readonly' => (boolean)$model->physical_no_patronymic,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>

        <?= $form->field($model, 'physical_no_patronymic', $checkboxConfig)->label('Нет отчества')->checkbox([
            'disabled' => !$isPhysical,
        ]); ?>

        <?= $form->field($model, 'physical_address', $textInputConfig)->label('Адрес регистрации')->textInput([
            'maxlength' => true,
            'disabled' => !$isPhysical,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]);
        ?>
        <?= $form->field($model, 'contact_email', $textInputConfig)
            ->label('Email покупателя (для отправки счетов)</span>')
            ->textInput([
                'maxlength' => true,
                'disabled' => !$isPhysical,
            ]); ?>

        <div class="col-12">
            <button class="link link_collapse link_bold button-clr mb-4 collapsed refreshDatepicker" type="button" data-toggle="collapse" data-target="#dopColumns3" aria-expanded="false" aria-controls="dopColumns3">
                <span class="link-txt">Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры</span>
                <svg class="link-shevron svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                </svg>
            </button>
        </div>

    </div>

    <div id="dopColumns3" class="collapse dopColumns">
        <div class="row">
            <?= $form->field($model, 'physical_passport_series', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{2} 9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XX XX',
                    'disabled' => !$isPhysical,
                ],
            ]); ?>
            <?= $form->field($model, 'physical_passport_number', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{6}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XXXXXX',
                    'disabled' => !$isPhysical,
                ],
            ]); ?>
            <div class="form-group col-6">
                <label class="label">Дата выдачи</label>
                <div class="date-picker-wrap">
                    <?= Html::activeTextInput($model, 'physical_passport_date_output', [
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                        'value' => DateHelper::format($model->physical_passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'disabled' => !$isPhysical,
                    ]); ?>
                    <svg class="date-picker-icon svg-icon input-toggle">
                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                    </svg>
                </div>
            </div>
        </div>

    </div>

</div>
<div class="clearfix"></div>

<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    function contractorIsIp() {
        return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
    }
    function contractorIsNotIp() {
        return $('#contractor-companytypeid').val() && !contractorIsIp();
    }

    function contractorIsPhysical() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
    }
    function contractorIsLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
    }
    function contractorIsForeignLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
    }
    function contractorIsAgent() {
        return $("#contractor_is_agent_input:checked").length;
    }

    $('#contractor-companytypeid').change(function () {
        if ($(this).val() == "<?= CompanyType::TYPE_IP ?>") {
            $('.field-contractor-ppc').hide();
        } else {
            $('.field-contractor-ppc').show();
        }
        $('#new-contractor-invoice-form').yiiActiveForm('validateAttribute', 'contractor-itn');
    });
    $('#contractor-name').on('input', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('#contractor-companytypeid').on('change', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
</script>
