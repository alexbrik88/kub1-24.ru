<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */
?>

<?= $model->isNewRecord ? 'Добавить' : 'Редактировать' ?> <?= $model->type == Contractor::TYPE_SELLER ? 'поставщика' : 'покупателя'; ?>