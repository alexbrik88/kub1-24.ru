<?php

use common\models\company\CheckingAccountant;
use common\models\companyStructure\SalePoint;
use frontend\rbac\permissions;
use yii\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\cash\models\CashSearch;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel CashSearch
 * @var $user Employee
 * @var $company Company
 * @var $currentRsModel CheckingAccountant|null
 * @var $widgetWallet string|null
 */

$this->title = 'Операции факт';
$this->params['breadcrumbs'][] = $this->title;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate && Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate && \common\models\company\CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$bankIds =  array_keys($searchModel->bankItems);
$cashboxIds = array_keys($searchModel->cashboxItems);
$emoneyIds = array_keys($searchModel->emoneyItems);
$userConfig = $user->config;
?>

<div class="stop-zone-for-fixed-elems cash-operations-index">
    <?= $this->render('@frontend/modules/cash/views/default/_partial/head', [
        'searchModel' => $searchModel,
        'userConfig' => $userConfig,
        'showCalendar' => false,
        'showHelpButton' => false,
        'showBalanceChart' => false,
        'showInstructionButton' => false
    ]) ?>
    <?= $this->render('@frontend/modules/cash/views/default/_partial/table-filter', [
        'searchModel' => $searchModel,
        'filterUrl' => \common\components\helpers\Url::to(['operations',
            'type' => $contractorType,
            'id' => $contractorId
        ]),
        'showContractorFilter' => false,
        'showCreateButtonInline' => true,
        'contractorId' => $contractorId,
        'type' => ($contractorType == Contractor::TYPE_CUSTOMER)
            ? CashFlowsBase::FLOW_TYPE_INCOME
            : CashFlowsBase::FLOW_TYPE_EXPENSE
    ]) ?>

    <?= $this->render('@frontend/modules/cash/views/default/_partial/table', [
        'user' => $user,
        'company' => $user->company,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>

<?= $this->render('@frontend/modules/cash/views/default/_partial/table-summary-select', [
    'showButtonsAsDropdown' => $canProjectUpdate || $canSalePointUpdate || $canIndustryUpdate,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ],
    'bankIds' => $bankIds,
    'cashboxIds' => $cashboxIds,
    'emoneyIds' => $emoneyIds
]); ?>

<?= $this->render('@frontend/modules/cash/views/default/_partial/table-modals', ['searchModel' => $searchModel]) ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-project'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-sale-point'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-company-industry'); ?>

<?php
$this->registerJs(<<<JS
// CATCH CHECKBOX
$(document).on("click", ".joint-checkbox-td, .joint-main-checkbox-td", function(e) {
    console.log(e.target);
    if ($(e.target).is("td") || $(e.target).is("th")) {
        const checkbox = $(this).find("input:checkbox");
        console.log(checkbox);
        if (checkbox.length === 1)
            checkbox.click();
    }
});

/////////////////////////////////
if (window.CashModalUpdatePieces)
    CashModalUpdatePieces.init();
/////////////////////////////////

JS, \yii\web\View::POS_READY);
?>