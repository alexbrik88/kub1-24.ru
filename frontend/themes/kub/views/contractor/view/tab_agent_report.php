<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\Invoice;
use common\models\document\AgentReport;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use yii\widgets\Pjax;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\AgentReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \common\models\Contractor */
/* @var $ioType int */
/* @var $message Message */

$this->title = 'Список Агентских Отчетов ' . $model->getShortName();

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = true;

$isFilter = (boolean) ($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет отчетов. Измените период, чтобы увидеть имеющиеся отчеты.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного отчета.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);


echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

?>

<div class="wrap">
    <div class="portlet-title">
        <div class="table-settings row row_indents_s">
            <div class="col-6">
                <h4>Список агентских отчетов</h4>
            </div>
            <div class="col-6">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'options' => [
                        'class' => 'd-flex flex-nowrap align-items-center',
                    ],
                ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер отчета',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <div>
                    <?php Pjax::begin([
                        'id' => 'pjax-agent-report',
                        'enablePushState' => false,
                        'timeout' => 5000
                    ]) ?>
                    <?= common\components\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'emptyText' => $emptyMessage,
                        'tableOptions' => [
                            'class' => 'table table-style',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],

                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $model) {
                                    return Html::checkbox('AgentReport[' . $model->id . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox',
                                    ]);

                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата отчета',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '15%',
                                ],
                                'contentOptions' => [
                                    'class' => 'link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ отчета',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '12%',
                                ],
                                'contentOptions' => [
                                    'class' => 'document_number link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                        'model' => $data,
                                    ])
                                        ? Html::a($data->fullNumber, ['/documents/agent-report/view', 'type' => $data->type, 'id' => $data->id], ['data-pjax' => 0])
                                        : $data->fullNumber;
                                },
                            ],
                            [
                                'attribute' => 'total_sum',
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '15%',
                                ],
                                'contentOptions' => [
                                    'class' => 'link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->total_sum, 2);
                                    return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                                },
                            ],
                            [
                                'attribute' => 'pay_up_date',
                                'label' => 'Оплатить до',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '15%',
                                ],
                                'contentOptions' => [
                                    'class' => 'link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return DateHelper::format($data->pay_up_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],
                            [
                                'attribute' => 'status_out_id',
                                'label' => 'Статус',
                                'class' => DropDownDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                    'width' => '10%',
                                ],
                                'filter' => $searchModel->getStatusArray(),
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return ($data->statusOut) ? $data->statusOut->name : '';
                                },
                            ],
                            [
                                'attribute' => 'has_invoice',
                                'label' => 'Счёт',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '13%',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) use ($canCreate) {
                                    $content = '';
                                    if ($data->invoice) {
                                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])) {
                                            $content .= Html::a($data->invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => Documents::IO_TYPE_IN,
                                                'id' => $data->invoice->id
                                            ], ['data-pjax' => 0, 'target' => '_blank']);
                                        } else {
                                            $content .= $data->invoice->fullNumber;
                                        }
                                    } else {
                                        $totalSum = $data->total_sum;
                                        if (!$totalSum) {
                                            $content .= Html::tag('span', 'Добавить', [
                                                'class' => 'btn btn-sm yellow no-sum tooltip2',
                                                'data-tooltip-content' => '#tooltip2_text',
                                                'style' => ($content ? 'margin-top: 5px;' : ''),
                                            ]);
                                        } else {
                                            $content .= Html::a('Добавить', [
                                                '/documents/agent-report/create-invoice',
                                                'reportId' => $data->id,
                                            ], [
                                                'class' => 'btn btn-sm yellow' . ($canCreate ? '' : ' no-rights-link'),
                                                'style' => ($content ? 'margin-top: 5px;' : ''),
                                                'data-pjax' => 0
                                            ]);
                                        }
                                    }

                                    return $content;
                                },
                                'filter' => ['' => 'Все', 1 => 'Есть счет', 0 => 'Нет счета']
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '15%',
                                ],
                                'contentOptions' => [
                                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                                ],
                                'attribute' => 'document_author_id',
                                'label' => 'Ответст&shy;венный',
                                'encodeLabel' => false,
                                'class' => DropDownSearchDataColumn::className(),
                                'value' => function ($data) {
                                    $employee = \common\models\employee\Employee::findOne([
                                        'id' => $data['document_author_id']
                                    ]);

                                    return (!empty($employee)) ? $employee->getShortFio() : '';
                                },
                                'format' => 'raw',
                                'filter' => $searchModel->getResponsibleEmployees()
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display:none">
    <div id="tooltip2_text">
        Сумма по Отчету агента равна нулю,<br/>
        поэтому нельзя создать счет
    </div>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            '/documents/agent-report/many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['/documents/agent-report/many-send', 'type' => $ioType]),
        ]) : null,
    ],
]);?>
