<?php

use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $analyticsMonthNumber string */
/* @var $analyticsYearNumber string */
/* @var $abcGroup integer */
/* @var $disciplineGroup integer */

?>

<?php if (ArrayHelper::getValue(Yii::$app->user, ['identity', 'currentEmployeeCompany', 'document_access_own_only'], true)) : ?>
    <div class="alert alert-danger" role="alert">
        У вас не достаточно прав, для просмотра данной страницы
    </div>
<?php else : ?>
    <?= $this->render('tab_analytics_view', $_params_) ?>
<?php endif ?>
