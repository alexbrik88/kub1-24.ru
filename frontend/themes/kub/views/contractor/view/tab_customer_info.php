<?php

use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use frontend\modules\crm\models\Contact;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $company common\models\Company */

/**
 * @var Contact[] $contacts
 * @var string $activeTab
 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$isSeller = $model->type == Contractor::TYPE_SELLER || $model->is_seller;
$isCustomer = $model->type == Contractor::TYPE_CUSTOMER || $model->is_customer;
$accountArray = $model->getContractorAccounts()->orderBy(['is_main' => SORT_DESC])->all();
$crmClient = $model->crmClient;

// SalePoint
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
// Industry
$hasCompanyIndustry = CompanyIndustry::find()->where(['company_id' => $company->id])->exists();
?>

<?= $this->render('customer_info/_partial_main', [
    'model' => $model,
    'isLegal' => $isLegal,
    'isPhysical' => $isPhysical,
    'isForeign' => $isForeign,
]) ?>

<div class="wrap pl-2 pr-2 pt-3 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <div class="nav-tabs-row">
        <?= Tabs::widget([
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'headerOptions' => ['class' => 'nav-item'],
            'items' => array_merge([
                [
                    'label' => $isPhysical ? 'Данные паспорта'.(empty($accountArray) ? '' : ', р/с') : 'Реквизиты',
                    'linkOptions' => ['class' => 'nav-link active'],
                    'content' => $this->render('customer_info/_partial_details', [
                        'model' => $model,
                        'isLegal' => $isLegal,
                        'isPhysical' => $isPhysical,
                        'isForeign' => $isForeign,
                        'accountArray' => $accountArray,
                    ]),
                ],
                [
                    'label' => 'Настройки',
                    'linkOptions' => ['class' => 'nav-link'],
                    'content' => $this->render('customer_info/_partial_comment', [
                        'model' => $model,
                    ]),
                    'visible' => false,
                ],
                [
                    'label' => 'Настройки Покупателя',
                    'linkOptions' => ['class' => 'nav-link'],
                    'content' => $this->render('customer_info/_partial_conf_customer', [
                        'model' => $model,
                        'hasSalePoint' => $hasSalePoint,
                        'hasCompanyIndustry' => $hasCompanyIndustry,
                    ]),
                    'visible' => $isCustomer,
                ],
                [
                    'label' => 'Настройки Поставщика',
                    'linkOptions' => ['class' => 'nav-link'],
                    'content' => $this->render('customer_info/_partial_conf_seller', [
                        'model' => $model,
                        'hasSalePoint' => $hasSalePoint,
                        'hasCompanyIndustry' => $hasCompanyIndustry,
                    ]),
                    'visible' => $isSeller,
                ],
                [
                    'label' => 'Настройки CRM',
                    'linkOptions' => ['class' => 'nav-link'],
                    'content' => $this->render('customer_info/_partial_crm_conf', [
                        'client' => $crmClient,
                    ]),
                    'visible' => $crmClient !== null,
                ],
            ], (FALSE && $model->type == Contractor::TYPE_SELLER) ?
                [
                    [
                        'label' => 'Агент',
                        'linkOptions' => ['class' => 'nav-link'],
                        'content' => $this->render('customer_info/_partial_agent', [
                            'model' => $model,
                        ]),
                    ]
                ] : []),
        ]); ?>
        </div>
    </div>
</div>

<?php if ($contacts): ?>
    <div>
        <?= Tabs::widget([
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-4'],
            'items' => [
                [
                    'label' => 'Для документов',
                    'headerOptions' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => ($activeTab == 'default') ? 'nav-link active' : 'nav-link',
                    ],
                    'content' => $this->render('customer_info/_partial_default_tab', [
                        'model' => $model,
                    ]),
                    'active' => ($activeTab == 'default'),
                ],
                [
                    'label' => 'CRM контакты',
                    'headerOptions' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => ($activeTab == 'crm') ? 'nav-link active' : 'nav-link',
                    ],
                    'content' => $this->render('customer_info/_partial_crm_tab', [
                        'contractor' => $model,
                        'contacts' => $contacts,
                    ]),
                    'active' => ($activeTab == 'crm'),
                ],
            ],
        ]) ?>
    </div>
<?php else: ?>
    <?= $this->render('customer_info/_partial_default_tab', [
        'model' => $model,
    ]) ?>
<?php endif; ?>
