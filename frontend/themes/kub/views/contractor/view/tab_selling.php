<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.07.2018
 * Time: 16:18
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\themes\kub\components\Icon;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use yii\helpers\Json;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\components\helpers\Html;
use common\components\helpers\Url;
use yii\bootstrap\Dropdown;
use yii\bootstrap4\Nav;
use common\components\date\DateHelper;
use miloschuman\highcharts\HighchartsAsset;
use frontend\models\Documents;
use yii\web\JsExpression;
use \common\models\currency\Currency;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $sellingMonthNumber string */
/* @var $sellingYearNumber string */

$productsChartOptions = $model->getProductHighChartsOptions($ioType, $sellingMonthNumber, $sellingYearNumber, $dataProvider->models);

$dateItems = [];
foreach ($model->getAnalyticsDateItems($ioType) as $key => $value) {
    $dateItems[] = [
        'label' => mb_strtolower($value) . ' ' . DateHelper::format($key, 'Y', 'Y-m'),
        'url' => Url::current([
            'sellingMonthNumber' => DateHelper::format($key, 'm', 'Y-m'),
            'sellingYearNumber' => DateHelper::format($key, 'Y', 'Y-m'),
        ]),
        'linkOptions' => ['class' => $key == ($sellingYearNumber . '-' . $sellingMonthNumber) ? 'active' : ''],
    ];
}
$previousMonthNumber = date('m', strtotime('-1 month', strtotime("01.{$sellingMonthNumber}." . $sellingYearNumber)));
$previousYear = date('Y', strtotime('-1 month', strtotime("01.{$sellingMonthNumber}." . $sellingYearNumber)));

$byProductId = Yii::$app->request->get('productId');

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>
<div class="wrap pt-3">
    <?php if ($model->isSellerCustomer) : ?>
        <div class="mb-4">
            <?= Nav::widget([
                'id' => 'contractor-menu-invoices',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Продажи',
                        'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_OUT]),
                        'options' => [
                            'class' => 'nav-item',
                        ],
                    ],
                    [
                        'label' => 'Закупки',
                        'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_IN]),
                        'options' => [
                            'class' => 'nav-item',
                        ],
                    ],
                ],
            ]); ?>
        </div>
    <?php endif ?>
    <div class="row">
        <div class="col-12 text-left pr-1">
            <div class="row">
                <div class="col-6">
                    <div class="bold" style="font-size: 16px; padding-top: 14px;">
                        <?= ($ioType == Contractor::TYPE_CUSTOMER) ? 'Проданные' : 'Купленные' ?> товары / услуги за
                        <div style="display: inline-block;">
                            <div class="dropdown">
                                <?= Html::tag('div', mb_strtolower(FlowOfFundsReportSearch::$month[$sellingMonthNumber]) . ' ' . $sellingYearNumber, [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; border-bottom: 1px dashed #333333; cursor: pointer;',
                                ]) ?>
                                <?= Dropdown::widget([
                                    'id' => 'employee-rating-dropdown',
                                    'options' => [
                                        'class' => 'dropdown-menu form-filter-list list-clr',
                                        'style' => 'max-height: 294px; overflow-y: auto;'
                                    ],
                                    'items' => $dateItems,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6" style="padding-right: 17px">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'options' => [
                            'class' => 'd-flex flex-nowrap align-items-center',
                        ],
                    ]); ?>
                    <div class="form-group flex-grow-1 mr-2">
                        <?= \yii\helpers\Html::textInput('productTitle', $searchModel->productTitle, [
                            'type' => 'search',
                            'placeholder' => 'Поиск по товарам и услугам',
                            'class' => 'form-control'
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Найти', [
                            'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                        ]) ?>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>

            <table id="selling-contractor-table" class="table table-style table-sells-list" style="table-layout: fixed;" role="grid">
                <thead>
                <tr class="odd" role="row">
                    <th width="37%" style="padding-left: 0;  position: relative">
                        <?= CheckboxX::widget([
                            'id' => "product-main-checkbox",
                            'name' => "product-main-checkbox",
                            'value' => true,
                            'options' => [
                                'class' => 'selling-contractor-main-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '+',
                                'iconUnchecked' => '−',
                            ],
                        ]) ?>
                        <div id="contractor-sells-toggle" class="th-title filter filter-open <?= $byProductId ? 'active' : '' ?>" style="display: inline-block;">
                            <span class="th-title-name">Товары / Услуги</span>
                    </th>
                    <th width="15%" class="text-right">
                        <?= FlowOfFundsReportSearch::$month[$previousMonthNumber] . ' ' . $previousYear; ?>
                    </th>
                    <th width="15%" class="text-right">
                        <?= FlowOfFundsReportSearch::$month[$sellingMonthNumber] . ' ' . $sellingYearNumber; ?>
                    </th>
                    <th width="8%" class="text-right">
                        %%
                    </th>
                    <th width="25%" class="text-right">
                        Среднее (12 месяцев)
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($productsChartOptions as $productID => $productChartOption): ?>
                    <?php
                    $productChartOption['chartInfo']['credits'] = [
                        'enabled' => false
                    ];
                    $productChartOption['chartInfo']['tooltip'] = [
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1'
                    ];
                    $productChartOption['chartInfo']['yAxis'][1]['title']['text'] = 'Кол-во (шт)';
                    $productChartOption['chartInfo']['series'][0]['borderRadius'] = '3';
                    //$productChartOption['chartInfo']['series'][0]['color'] = '#0079f1';
                    $highChartsProductOptionsJson = Json::encode($productChartOption['chartInfo']);
                    $currentProductCount = $model->getProductSumSellingData($ioType, $sellingMonthNumber, $sellingYearNumber, $productID);
                    $previousMonthProductCount = $model->getProductSumSellingData($ioType, $previousMonthNumber, $previousYear, $productID);
                    $averageProductAmount = 0;
                    if (empty($previousMonthProductCount)) {
                        $invoiceProductPercent = 100;
                        if (empty($currentProductCount)) {
                            $invoiceProductPercent = 0;
                        }
                    } else {
                        $invoiceProductPercent = $previousMonthProductCount > 0 ?
                            (round(($currentProductCount * 100 / $previousMonthProductCount), 2) - 100) : -100;
                    };
                    $seriesProductData = $productChartOption['chartInfo']['series'][0]['data'];
                    foreach ($seriesProductData as $oneAmount) {
                        $averageProductAmount += $oneAmount * 100;
                    }
                    $averageProductAmount /= 12; ?>
                    <tr data-product_id="<?= $productID ?>">
                        <td width="37%" style="padding-right: 0;padding-left: 0;">
                            <?= CheckboxX::widget([
                                'id' => "product-{$productID}-checkbox",
                                'name' => "product-{$productID}-checkbox",
                                'value' => true,
                                'options' => [
                                    'class' => 'selling-contractor-checkbox',
                                ],
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'threeState' => false,
                                    'inline' => false,
                                    'iconChecked' => '+',
                                    'iconUnchecked' => '−',
                                ],
                            ]) . '<span class="product-title">' . $productChartOption['productTitle'] . ' (' . Currency::$currencySymbols[$model->currency_id] . ')</span>'; ?>
                        </td>
                        <td width="15%" class="text-right">
                            <?= TextHelper::invoiceMoneyFormat($previousMonthProductCount, 2); ?>
                        </td>
                        <td width="15%" class="text-right">
                            <?= TextHelper::invoiceMoneyFormat($currentProductCount, 2); ?>
                        </td>
                        <td width="8%" class="text-right">
                            <?= $invoiceProductPercent > 0 ? ('+' . $invoiceProductPercent) : ($invoiceProductPercent) ?>
                        </td>
                        <td width="25%" class="text-right">
                            <?= TextHelper::invoiceMoneyFormat($averageProductAmount, 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="pad0">
                            <div id="high-charts-container-product-<?= $productID ?>"
                                 class="high-chart contractor-selling-chart"
                                 style="height: 200px;display: none;"></div>
                            <span class="product-unit-label-chart" style="display: none;">
                                КОЛ-ВО <?= $productChartOption['productUnitTitle'] ?
                                    ('(' . $productChartOption['productUnitTitle'] . ')') : null; ?>
                            </span>
                        </td>
                    </tr>
                    <?php $this->registerJs('
                        Highcharts.chart("high-charts-container-product-" + ' . $productID . ', ' . $highChartsProductOptionsJson . ');
                    '); ?>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{pager}",
            ]); ?>
        </div>
    </div>
</div>
