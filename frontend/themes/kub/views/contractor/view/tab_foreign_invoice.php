<?php

use common\components\debts\DebtsHelper;
use common\components\helpers\Url;
use common\components\TextHelper;
use common\models\Company;
use common\models\currency\Currency;
use common\models\document\ForeignCurrencyInvoice;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel frontend\modules\documents\models\ForeignCurrencyInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */

$user = Yii::$app->user;
$employee = $user->identity;
$company = $employee->company;
$userConfig = $employee->config;

$canChief = $user->can(UserRole::ROLE_CHIEF);
$canIndex = $user->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = $user->can(permissions\document\Document::CREATE, ['ioType' => $ioType]) &&
    $user->can(permissions\document\Document::STRICT_MODE);
$canDelete = $user->can(permissions\document\Document::DELETE, ['ioType' => $ioType]) &&
    $user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    $user->can(permissions\document\Document::UPDATE_STATUS) &&
    $user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    $user->can(permissions\document\Document::VIEW, ['ioType' => $ioType]);
$canPay = $user->can(permissions\Cash::CREATE, ['ioType' => $ioType]) &&
    $user->can(permissions\document\Invoice::ADD_CASH_FLOW) &&
    $user->can(permissions\document\Document::UPDATE_STATUS);

$dropItems = [];

$sendDropItems = [];
if ($canSend) {
    $sendDropItems[] = [
        'label' => 'Счета',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['/documents/foreign-currency-invoice/many-send', 'type' => $ioType]),
        ],
    ];
}

list($stat1, $stat2, $stat3) = InvoiceStatistic::getForeignCurrencyStatisticInfo($dataProvider->query, $ioType);

$currencyTabs = [];
foreach ($currencyNameArray as $name) {
    $currencyTabs[] = [
        'label' => $name,
        'url' => [
            '/contractor/view',
            'type' => $ioType,
            'id' => $model->id,
            'tab' => 'foreign_invoice',
            'currency' => $name,
        ],
        'options' => ['class' => 'nav-item'],
        'linkOptions' => ['class' => 'nav-link pt-0'],
        'active' => $currencyName == $name,
    ];
}
$currencySymbol = Currency::$currencySymbols[$searchModel->currency_name] ?? '';
?>

<?php if (count($currencyNameArray) > 1) : ?>
    <div class="nav-tabs-row mb-2 pb-1">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => $currencyTabs,
        ]) ?>
    </div>
<?php endif ?>

<div class="wrap wrap_count">
    <?php if ($model->isSellerCustomer) : ?>
        <div class="mb-4">
            <?= Nav::widget([
                'id' => 'contractor-menu-invoices',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Продажи',
                        'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_OUT]),
                        'options' => [
                            'class' => 'nav-item',
                        ],
                    ],
                    [
                        'label' => 'Закупки',
                        'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_IN]),
                        'options' => [
                            'class' => 'nav-item',
                        ],
                    ],
                ],
            ]); ?>
        </div>
    <?php endif ?>
    <div class="row">
        <div class="col-6 col-xl-3">
            <div class="count-card wrap">
                <div class="count-card-main">
                    <?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?>
                    <?= $currencySymbol ?>
                </div>
                <div class="count-card-title">
                    Не оплачено ВСЕГО
                </div>
                <hr>
                <div class="count-card-foot">
                    Количество инвойсов: <?= $stat1['count'] ?>
                </div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_red wrap">
                <div class="count-card-main">
                    <?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?>
                    <?= $currencySymbol ?>
                </div>
                <div class="count-card-title">
                    Не оплачено в срок
                </div>
                <hr>
                <div class="count-card-foot">
                    Количество инвойсов: <?= $stat2['count'] ?>
                </div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap">
                <div class="count-card-main">
                    <?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?>
                    <?= $currencySymbol ?>
                </div>
                <div class="count-card-title">
                    Оплачено
                </div>
                <hr>
                <div class="count-card-foot">
                    Количество инвойсов: <?= $stat3['count'] ?>
                </div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap p-3 text_size_14">
                <div class="mb-1 weight-700 text-red">Просрочено на:</div>
                <div class="line-height-1-5">
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>0-10 дней</span>
                        <span class="weight-700">
                            <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                                $company,
                                $currencyName,
                                DebtsHelper::PERIOD_0_10,
                                $searchModel->type,
                                false,
                                true,
                                $model->id
                            ); ?>
                            <?= number_format($sum/100, 2, ',', ' ') ?>
                        </span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>11-30 дней </span>
                        <span class="weight-700">
                            <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                                $company,
                                $currencyName,
                                DebtsHelper::PERIOD_11_30,
                                $searchModel->type,
                                false,
                                true,
                                $model->id
                            ); ?>
                            <?= number_format($sum/100, 2, ',', ' ') ?>
                        </span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>31-60 дней</span>
                        <span class="weight-700">
                            <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                                $company,
                                $currencyName,
                                DebtsHelper::PERIOD_31_60,
                                $searchModel->type,
                                false,
                                true,
                                $model->id
                            ); ?>
                            <?= number_format($sum/100, 2, ',', ' ') ?>
                        </span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>61-90 дней </span>
                        <span class="weight-700">
                            <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                                $company,
                                $currencyName,
                                DebtsHelper::PERIOD_61_90,
                                $searchModel->type,
                                false,
                                true,
                                $model->id
                            ); ?>
                            <?= number_format($sum/100, 2, ',', ' ') ?>
                        </span>
                    </div>
                    <div class="d-flex flex-nowrap justify-content-between">
                        <span>Больше 90 дней</span>
                        <span class="weight-700">
                            <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                                $company,
                                $currencyName,
                                DebtsHelper::PERIOD_MORE_90,
                                $searchModel->type,
                                false,
                                true,
                                $model->id
                            ); ?>
                            <?= number_format($sum/100, 2, ',', ' ') ?>
                        </span>
                    </div>
                </div>
                <div class="d-flex flex-nowrap justify-content-between mt-1 weight-700 text-red">
                    <span>Просрочено итого:</span>
                    <span>
                        <?php $sum = DebtsHelper::getForeignCurrencyDebtsSum(
                            $company,
                            $currencyName,
                            DebtsHelper::PERIOD_MORE_90,
                            $searchModel->type,
                            true,
                            true,
                            $model->id
                        ); ?>
                        <?= number_format($sum/100, 2, ',', ' ') ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'contr_inv_scan',
                ],
                [
                    'attribute' => 'contr_inv_paylimit',
                ],
                [
                    'attribute' => 'contr_inv_paydate',
                ],
                [
                    'attribute' => 'contr_inv_author',
                    'visible' => (
                        $user->can(UserRole::ROLE_CHIEF) ||
                        $user->can(UserRole::ROLE_SUPERVISOR) ||
                        $user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
                    ),
                ],
                [
                    'attribute' => 'contr_inv_comment',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_contractor']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'byNumber', [
                'type' => 'search',
                'placeholder' => 'Номер счета, название или ИНН контрагента',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['/documents/foreign-currency-invoice/generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

<?= $this->render('@frontend/modules/documents/views/foreign-currency-invoice/_invoices_table', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'useContractor' => true,
    'type' => $ioType,
    'id' => $model->id,
    'tabViewClass' => $userConfig->getTableViewClass('table_view_contractor')
]); ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_contractor-debt" style="display: inline-block; text-align: center;">
        Расшифровка «Не оплачено в срок»<br>
        за весь период работы с клиентом.<br>
        В блоках слева, данные за период, указанный выше.
    </span>
    <span id="tooltip_doc_upd" style="display: inline-block; text-align: center;">
        У вас нет ни одной УПД.<br>
        Что бы начать формировать УПД,<br>
        измените настройки в профиле компании
    </span>
</div>

<?php Modal::begin([
    'id' => 'store-account-modal',
    'closeButton' => false,
]); ?>
    <h4 id="store-account-modal-content"></h4>
    <div style="text-align: center; margin-top: 30px;">
        <button type="button" class="btn darkblue text-white"
                data-dismiss="modal">OK
        </button>
    </div>
<?php Modal::end(); ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            '/documents/foreign-currency-invoice/many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? (Html::a(Icon::get('envelope').' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width document-many-send',
            'data-url' => Url::to([
                '/documents/foreign-currency-invoice/many-send',
                'type' => $ioType,
            ]),
        ])  . ($sendDropItems ? Html::tag('div', Html::a(Icon::get('envelope').' <span>Отправить</span>', '#', [
            'id' => 'dropdownMenu3',
            'class' => 'button-regular button-hover-transparent button-width',
            'data-toggle' => 'dropdown',
        ]) . Dropdown::widget([
            'items' => $sendDropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), [
            'class' => 'dropup document-many-send-dropdown hidden',
        ]) : null)) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<span>Платежка</span>', '#many-charge', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a($this->render('//svg-sprite', ['ico' => 'check-2']).' <span>Оплачены</span>', '#many-paid-modal', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span class="pr-2">Еще</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'class' => 'form-filter-list list-clr dropdown-menu-right'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?php if ($canPay) : ?>
    <?php Modal::begin([
        'id' => 'many-paid-modal',
        'closeButton' => false,
        'options' => [
            'data-url' => Url::to(['/documents/foreign-currency-invoice/add-flow-form', 'type' => $ioType]),
        ]
    ]); ?>
    <div id="many-paid-content"></div>
    <?php Modal::end(); ?>

    <?php $this->registerJs('
        $(document).on("shown.bs.modal", "#many-paid-modal", function() {
            var container = this;
            var idArray = $(".joint-operation-checkbox:checked").map(function () {
                return $(this).closest("tr").data("key");
            }).get();
            var url = this.dataset.url + "&id=" + idArray.join();
            $.post(url, function(data) {
                $("#many-paid-content", container).html(data);
                $("input:checkbox, input:radio", container).uniform();
                $(".date-picker", container).datepicker(kubDatepickerConfig);
            })
        });
        $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
            $("#many-paid-content").html("");
        });
    ') ?>
<?php endif ?>

<?php if ($ioType == Documents::IO_TYPE_IN) : ?>
    <?php Modal::begin([
        'id' => 'many-charge',
        'title' => 'Вы уверены, что хотите подготовить платежки в банк:',
        'closeButton' => false,
    ]); ?>
        <div class="many-charge-text" style="font-size: 15px;">
            <div class="total-row" style="font-weight: bold;">
                Итого: счета <span class="total-invoice-count"></span> шт.,
                сумма <span class="total-summary"></span>
                <i class="fa fa-rub"></i>
            </div>
        </div>
        <div class="col-12 text-left many-charge-text payment-invoices-block mb-2" style="font-size: 15px;display: none;">
            <div>
                Эти счета уже оплачены:
            </div>
        </div>

        <div class="payment-invoices-block" style="display: none;">
            <div class="col-12 text-center">
                <button type="button" class="button-clr button-regular button-hover-transparent button-width-medium" data-dismiss="modal">
                    ОК
                </button>
            </div>
        </div>
        <div class="main-block text-center mt-5">
            <?= Html::a('Да', null, [
                'class' => 'button-regular button-hover-transparent button-width-medium mr-2 modal-many-charge ladda-button',
                'data-url' => Url::to(['/documents/payment-order/many-create',]),
                'data-style' => 'expand-right',
            ]); ?>
            <button type="button" class="button-regular button-hover-transparent button-width-medium" data-dismiss="modal">
                НЕТ
            </button>
        </div>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $("#many-charge").on("show.bs.modal", function () {
            var countChecked = 0;
            var totalSummary = 0;
            var payedInvoices = [];
            var mainBlock = $(".main-block", this);
            var paymentInvoiceBlock = $(".payment-invoices-block", this);
            var $totalRow = $(".total-row", this);

            (".generated-row", this).remove();
            mainBlock.show();
            paymentInvoiceBlock.hide();

            $(".joint-operation-checkbox:checked").each(function(){
                var price = $(this).closest("tr").find(".price").data("remaining-amount");
                var invoiceNumber = $(this).closest("tr").find(".document_number span").data("full-name");

                console.log(invoiceNumber);
                price = parseFloat(price.replace(",", "."));
                if (price > 0) {
                    totalSummary += price;
                    countChecked++;
                } else {
                    payedInvoices.push(invoiceNumber);
                }
            });

            $totalRow.find(".total-invoice-count").text(countChecked);
            $totalRow.find(".total-summary").text(number_format(totalSummary, 2, ",", " "));

            if (countChecked == 0) {
                mainBlock.hide();
            }
            if (payedInvoices.length > 0) {
                paymentInvoiceBlock.show();
                payedInvoices.forEach(function (invoice) {
                    paymentInvoiceBlock.append("<div class=generated-row>" + invoice + "</div>");
                });
            }
        });
    '); ?>
<?php endif ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'title' => 'Вы уверены, что хотите удалить выбранные счета?',
        'closeButton' => false,
        'toggleButton' => false,
        'options' => [
            'class' => 'confirm-modal modal fade',
        ],
        'titleOptions' => [
            'class' => 'ta-c',
        ],
    ]); ?>
        <div class="text-center">
            <?= Html::button('Да', [
                'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                'data-url' => Url::to([
                    '/documents/foreign-currency-invoice/many-delete',
                    'type' => $ioType,
                ]),
            ]); ?>
            <?= Html::button('Нет', [
                'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
<?php endif; ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => true,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE
    ]); ?>
<?php endif; ?>
