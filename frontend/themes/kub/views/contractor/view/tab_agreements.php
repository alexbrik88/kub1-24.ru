<?php

use common\components\grid\GridView;
use common\models\Agreement;
use common\models\EmployeeCompany;
use common\models\file\widgets\FileUpload;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\widgets\BtnConfirmModalWidget;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\bootstrap4\Dropdown;

use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Contractor */
/* @var $searchAgreementModel \frontend\modules\documents\models\AgreementSearch */
/* @var $dataAgreementProvider yii\data\ActiveDataProvider */

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_contractor');

$addItems = [
    [
        'label' => 'С покупателем',
        'url' => '#',
        'linkOptions' => [
            'class' => 'agreement-modal-link',
            'data-url' => Url::to([
                '/documents/agreement/create',
                'contractor_id' => $model->id,
                'type' => Contractor::TYPE_CUSTOMER,
                'returnTo' => 'contractor',
            ]),
        ],
    ],
    [
        'label' => 'С поставщиком',
        'url' => '#',
        'linkOptions' => [
            'class' => 'agreement-modal-link',
            'data-url' => Url::to([
                '/documents/agreement/create',
                'contractor_id' => $model->id,
                'type' => Contractor::TYPE_SELLER,
                'returnTo' => 'contractor',
            ]),
        ],
    ],
];

$agreemmmentTypesArray = [
    Documents::IO_TYPE_OUT => 'С покупателем',
    Documents::IO_TYPE_IN => 'С поставщиком'
];
?>

<div class="row">
    <div class="col-12 mb-3" style="text-align: right">
        <?php if ($model->isSellerCustomer) : ?>
            <div class="dropdown button-width ml-auto">
                <?= Html::a(Icon::get('add-icon', [
                    'class' => 'mr-2',
                ]).' Договор', '#', [
                    'class' => 'button-regular button-regular_red button-width ml-auto',
                    'data-toggle' => 'dropdown',
                    'aria-expanded' => 'false'
                ]); ?>

                <?= Dropdown::widget([
                    'options' => [
                        'class' => 'dropdown-menu form-filter-list list-clr',
                        'style' => 'min-width: auto;',
                    ],
                    'items' => $addItems,
                ]); ?>
            </div>
        <?php else : ?>
            <?= Html::a(Icon::get('add-icon', [
                'class' => 'mr-2',
            ]).' Договор', '#', [
                'class' => 'agreement-modal-link button-regular button-regular_red button-width ml-auto',
                'data-url' => Url::to([
                    '/documents/agreement/create',
                    'contractor_id' => $model->id,
                    'type' => $model->is_seller ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER,
                    'returnTo' => 'contractor',
                ]),
            ]) ?>
        <?php endif; ?>

    </div>
</div>

<?php Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'timeout' => 5000
]) ?>

<div class="wrap">
    <?php if ($model->isNewRecord) : ?>
        <h3>Возможность добавить договор появится после сохранения контрагента</h3>
    <?php else : ?>

        <?= GridView::widget([
            'dataProvider' => $dataAgreementProvider,
            'filterModel' => $searchAgreementModel,
            'emptyText' => 'Еще не добавлено ни одного договора',
            'tableOptions' => [
                'class' => 'table table-style table-contractor-agreement' . $tabViewClass,
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
                $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataAgreementProvider->totalCount, 'scroll' => false]),
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                [
                    'attribute' => 'document_number',
                    'label' => 'Номер',
                    'headerOptions' => ['style' => 'width:100px;'],
                    'format' => 'raw',
                    'value' => function ($data) {
                        $full_number = ($data['document_additional_number']) ?
                            $data['document_additional_number'].'-'.$data['document_number'] :
                            $data['document_number'];
                        $can_view = Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                            'model' => Agreement::findOne($data['id']),
                        ]);

                        return  ($can_view && $data['tid']) ? // if has template
                            Html::a($full_number, ['/documents/agreement/view', 'id' => $data['id'], 'contractor' => $data['cid']]) :
                            $full_number;
                    },
                ],
                [
                    'attribute' => 'document_date',
                    'label' => 'Дата',
                    'headerOptions' => ['style' => 'width:100px;'],
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'filter' => $searchAgreementModel->getAgreementTypeFilterItems(),
                    's2width' => '200px',
                    'attribute' => 'document_type_id',
                    'label' => 'Тип документа',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data['agreementType'];
                    },
                ],
                [
                    'attribute' => 'type',
                    'label' => 'С кем',
                    'filter' => ['' => 'Все'] + $agreemmmentTypesArray,
                    's2width' => '200px',
                    'value' => function ($data) use ($agreemmmentTypesArray) {
                        return $agreemmmentTypesArray[$data['type']] ?? null;
                    },
                    'visible' => $model->isSellerCustomer,
                ],
                [
                    'attribute' => 'document_name',
                    'label' => 'Название',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data['document_name'];
                    },
                ],
                [
                    'attribute' => 'agreement_template_id',
                    'label' => 'Шаблон',
                    'filter' => $searchAgreementModel->getAgreementTemplates(),
                    's2width' => '200px',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data['tid']) {
                            $tpl = \common\models\AgreementTemplate::findOne($data['tid']);
                            return '№ '.$tpl->document_number.' от '.date('d.m.Y', strtotime($tpl->document_date));
                        }
                        return '---';
                    },
                ],
                [
                    'attribute' => 'has_file',
                    'label' => 'Скан',
                    'format' => 'html',
                    'headerOptions' => ['style' => 'width:60px;'],
                    'content' => function ($data) {
                        $agreement = Agreement::findOne($data['id']);
                        $file = ($agreement && $agreement->files) ? $agreement->files[0] : null;
                        return $file ? Html::a($this->render('//svg-sprite', ['ico' => 'clip', 'class' => 'svg-icon mr-2',]), [
                            '/contractor/agreement-file-get',
                            'id' => $agreement->id,
                            'file-id' => $file->id,
                        ], [
                            'class' => 'file-link',
                            'target' => '_blank',
                            'download' => '',
                            'data-pjax' => 0,
                        ]) : '';
                    },
                ],
                [
                    'attribute' => 'created_by',
                    'label' => 'Ответственный',
                    'headerOptions' => [
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'text-left text-ellipsis',
                    ],
                    'value' => function ($data) use ($searchAgreementModel) {
                        $employee = EmployeeCompany::findOne([
                            'employee_id' => $data['created_by'],
                            'company_id' => $searchAgreementModel->company_id
                        ]);

                        return (!empty($employee)) ? $employee->getShortFio() : '';
                    },
                    'format' => 'raw',
                    'filter' => $searchAgreementModel->getAgreementEmployees(),
                    's2width' => '200px',
                ],
                [
                    'class' => ActionColumn::className(),
                    'template' => '{update} {delete}',
                    'headerOptions' => [
                        'width' => '5%',
                    ],
                    'buttons' => [
                        'update' => function ($url, $data) {
                            return Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), '#', [
                                'data-url' => $url,
                                'class' => 'agreement-modal-link link',
                                'title' => Yii::t('yii', 'Редактировать'),
                                'aria-label' => Yii::t('yii', 'Редактировать'),
                            ]);
                        },
                        'delete' => function ($url) {
                            return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                'theme' => 'gray',
                                'toggleButton' => [
                                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']),
                                    'class' => 'button-clr link',
                                    'tag' => 'button',
                                    'title' => Yii::t('yii', 'Удалить'),
                                ],
                                'confirmUrl' => $url,
                                'confirmParams' => [],
                                'message' => 'Вы уверены, что хотите удалить договор?',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = 0;
                        switch ($action) {
                            case 'update':
                                $url = '/documents/agreement/update';
                                break;
                            case 'delete':
                                $url = 'agreement-delete';
                                break;
                        }

                        Yii::$app->session->set('return_from_agreement', 'contractor');

                        return Url::to([$url, 'id' => $model['id'], 'type' => $model['type'], 'contractor_id' => $model['cid'], 'old_record' => 1]);
                    },
                ],
            ],
        ]); ?>

    <?php endif ?>
</div>

<?php Pjax::end(); ?>



<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();
$successFlashMessageDelete = "<div id='w2-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Договор удален</div>";

$this->registerJs('
$(document).on("click", ".agreement-modal-link", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).data("url"), container: "#agreement-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#agreement-modal-header").html($("[data-header]").data("header"));
    })
    $("#agreement-modal-container").modal("show");
});
$(document).on("show.bs.modal", "#agreement-modal-container", function(event) {
        $(".alert-success").remove();
});
$(document).on("click", ".btn-confirm-yes", function(){
    var $this = $(this);
    $.ajax({
        url: $this.data("url"),
        type: $this.data("type"),
        data: $this.data("params"),
        success: function(data) {
            $.pjax.reload("#agreement-pjax-container", {push: false, timeout: 5000});
            //$(".page-content").prepend("' . $successFlashMessageDelete . '");
        }
    });
    return false;
});
');
