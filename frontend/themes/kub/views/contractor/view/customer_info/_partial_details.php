<?php
/* @var $model common\models\Contractor */

use common\models\company\CompanyType;
use common\models\Contractor;
use yii\helpers\Html;

?>

<?php if (!empty($accountArray) || !$isPhysical): ?>
    <div class="pl-2 pr-2 my-3 py-3">
        <div class="row pl-1 pr-1">
            <div class="col-12">
                <table class="table table-style table-count-list page-border">
                    <thead>
                        <tr>
                            <th>Наименование банка</th>
                            <th><?= $isForeign ? 'SWIFT' : 'БИК' ?></th>
                            <th>Валюта</th>
                            <th>Р/с</th>
                            <?php if (!$isForeign) : ?>
                                <th>К/с</th>
                            <?php endif ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($accountArray as $account) : ?>
                            <tr>
                                <td><?php echo $account->bank_name ?: '—'; ?></td>
                                <td><?php echo $account->bik ?: '—'; ?></td>
                                <td><?php echo $account->currency->name ?? '—'; ?></td>
                                <td><?php echo $account->rs ?: '—'; ?></td>
                                <?php if (!$isForeign) : ?>
                                    <td><?php echo $account->ks ?: '—'; ?></td>
                                <?php endif ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($isPhysical): ?>
    <div class="pl-2 pr-2">
        <div class="row pl-1 pr-1">
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Паспорт</div>
                        <div><?= ($model->physical_passport_isRf == 0) ? 'не РФ' : 'РФ'; ?></div>
                    </div>
                    <?php if ($model->physical_passport_isRf == 0) : ?>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Страна</div>
                        <div><?= $model->physical_passport_country ?: '—' ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Серия</div>
                        <div><?php echo $model->physical_passport_series ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Номер</div>
                        <div><?php echo $model->physical_passport_number ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Код подразделения</div>
                        <div><?php echo $model->physical_passport_department ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Кем выдан</div>
                        <div><?php echo $model->physical_passport_issued_by ?: '—'; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Дата выдачи</div>
                        <div><?= $model->physical_passport_date_output ?
                            date('d.m.Y', strtotime($model->physical_passport_date_output)): '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Адрес регистрации</div>
                        <div><?php echo $model->physical_address ?: '—'; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="pl-2 pr-2">
        <div class="row pl-1 pr-1">
            <?php if ($isLegal) : ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 mb-3 pb-3">
                            <div class="label weight-700 mb-3">КПП</div>
                            <div><?php echo $model->PPC ?: '—'; ?></div>
                        </div>
                        <div class="col-3 mb-3 pb-3">
                            <div class="label weight-700 mb-3"><?= ($model->company_type_id == CompanyType::TYPE_IP) ? 'ОГРНИП' : 'ОГРН' ?></div>
                            <div><?php echo $model->BIN ?: '—'; ?></div>
                        </div>

                        <div class="col-3 mb-3 pb-3">
                            <div class="label weight-700 mb-3">ОКПО</div>
                            <div><?php echo $model->okpo ?: '—'; ?></div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Юридический адрес</div>
                    <div><?php echo $model->legal_address ?: '—'; ?></div>
                </div>
            </div>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Фактический адрес</div>
                    <div><?php echo $model->actual_address ?: '—'; ?></div>
                </div>
            </div>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Адрес для почты</div>
                    <div><?php echo $model->postal_address ?: '—'; ?></div>
                </div>
            </div>
            <?php if ($isLegal) : ?>
                <div class="col-12">
                    <div class="mb-3 pb-3">
                        <div class="label weight-700 mb-3">Адрес в строке Грузополучатель</div>
                        <div><?= $model->consignee_address_same ? 'Совпадает с юр. адресом' : Html::encode($model->consignee_address); ?></div>
                    </div>
                    <div class="mb-3 pb-3">
                        <div class="label weight-700 mb-3">КПП в строке Грузополучатель</div>
                        <div><?= $model->consignee_kpp_same ? 'Совпадает с основным КПП' : Html::encode($model->consignee_kpp); ?></div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php endif; ?>
