<?php

use common\models\Contractor;
use yii\helpers\Html;

?>

<div class="wrap pl-4 pr-4 pt-4 pb-0">
    <div class="pl-2 pt-2">

            <div class="row justify-content-left">
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">ИНН<?= $isForeign ? '/ITN' : ''; ?></div>
                    <div><?php echo $model->ITN; ?></div>
                </div>
                <?php if (!$isForeign) : ?>
                    <div class="column pb-3 mb-3">
                        <div class="label weight-700 mb-3">Юр/Физ лицо</div>
                        <div><?= $isPhysical ? 'Физ. лицо' : 'Юр. лицо' ?></div>
                    </div>
                <?php endif ?>
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">Название <?= ($model->type == 1 ? 'Поставщика' : 'Покупателя') ?></div>
                    <div><?php echo $model->name; ?></div>
                </div>
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">Форма</div>
                    <div><?php echo $model->getCompanyTypeName(); ?></div>
                </div>
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">Налогообложение</div>
                    <div><?php echo $model->taxation_system == Contractor::WITH_NDS ? 'С НДС' : 'Без НДС' ?></div>
                </div>
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">Статус</div>
                    <div><?= $model->status == Contractor::ACTIVE ? 'Активен' : 'Неактивен' ?></div>
                </div>
                <div class="column pb-3 mb-3">
                    <div class="label weight-700 mb-3">Не для бухгалтерии</div>
                    <div class="d-flex flex-nowrap align-items-center">
                        <div class="switch line-height-1 mr-1">
                            <input class="switch-input input-hidden md-check" type="checkbox" id="switcher2" <?php if ($model->not_accounting) echo 'checked'; ?> disabled>
                            <label class="switch-label" for="switcher2">
                                <span class="switch-pseudo">&nbsp;</span>
                            </label>
                        </div>
                        <div class="tooltip-box ml-2 mr-2">
                            <button type="button" class="button-clr" title="Документы по такому клиенту не попадают в выгрузку для 1с и видны только для руководителя">
                                <svg class="tooltip-question-icon svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Вид деятельности</div>
                    <div>
                        <?php if ($model->activity !== null) : ?>
                            <?= Html::encode($model->activity->name) ?>
                        <?php else : ?>
                            —
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-3 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Источник</div>
                    <div><?php echo $model->campaign->name ?? '—'; ?></div>
                </div>
                <div class="col-5 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Комментарии</div>
                    <div><?php echo $model->comment ?: '—'; ?></div>
                </div>
            </div>
    </div>
</div>
