<?php

use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use common\models\Contractor;
use \common\models\document\InvoiceIncomeItem;
use \common\models\document\InvoiceExpenditureItem;
use yii\helpers\Html;

$hasSalePoint = $hasSalePoint ?? false;
$hasCompanyIndustry = $hasCompanyIndustry ?? false;
?>

<div class="pl-2 pr-2  mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Отсрочка оплаты в днях</div>
                    <div><?= $model->seller_payment_delay; ?></div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">
                        Отсрочка выполнения обязательств в днях
                        <?= Html::tag('span', Icon::QUESTION, [
                            'title' => 'Количество дней на оказание услуги или отгрузку товара после предоплаты (аванса)',
                        ]) ?>
                    </div>
                    <div>
                        <?= $model->seller_guaranty_delay; ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Фиксированная скидка на всё</div>
                    <div><?= ($model->seller_discount > 0) ? "{$model->seller_discount}%" : '0' ?></div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">
                        Статья расходов
                    </div>
                    <div>
                        <?php $expenseItem = InvoiceExpenditureItem::findOne($model->invoice_expenditure_item_id) ?>
                        <?= $expenseItem ? $expenseItem->name : '—' ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Приоритет в оплате</div>
                    <div>
                        <?php switch ($model->payment_priority) {
                            case Contractor::PAYMENT_PRIORITY_HIGH:
                                echo '1 - Большой';
                                break;
                            case Contractor::PAYMENT_PRIORITY_MEDIUM:
                                echo '2 - Средний';
                                break;
                            case Contractor::PAYMENT_PRIORITY_LOW:
                                echo '3 - Наименьший';
                                break;
                        }; ?>
                    </div>
                </div>
                <?php if ($hasCompanyIndustry): ?>
                    <div class="col-4 mb-3 pb-3">
                        <div class="label weight-700 mb-3">
                            Направление
                        </div>
                        <div>
                            <?php $industry = $model->getIndustry(Contractor::TYPE_SELLER)->one() ?>
                            <?= $industry ? $industry->name : '—'; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($hasSalePoint): ?>
                    <div class="col-4 mb-3 pb-3">
                        <div class="label weight-700 mb-3">
                            Точка продаж
                        </div>
                        <div>
                            <?php $salePoint = $model->getSalePoint(Contractor::TYPE_SELLER)->one() ?>
                            <?= $salePoint ? $salePoint->name : '—'; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">
                        Группа
                    </div>
                    <div>
                        <? $nameGroup =  $model->getGroupName(1);
                        echo(!is_null($nameGroup)) ? Html::encode($nameGroup) : '—'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>