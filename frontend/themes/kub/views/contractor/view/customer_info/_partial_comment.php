<?php

use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use common\models\Contractor;
use \common\models\document\InvoiceIncomeItem;
use \common\models\document\InvoiceExpenditureItem;
use yii\helpers\Html;


?>

<div class="pl-2 pr-2  mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Ответственный сотрудник</div>
                    <div>
                        <?php if ($model->responsibleEmployee !== null) : ?>
                            <?= $model->responsibleEmployee['lastname'] .' '.
                                $model->responsibleEmployee['firstname'] .' '.
                                $model->responsibleEmployee['patronymic']; ?>
                        <?php else : ?>
                            --
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Отсрочка оплаты в днях</div>
                    <div><?php echo $model->payment_delay; ?></div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">
                        Отсрочка выполнения обязательств в днях
                        <?= Html::tag('span', Icon::QUESTION, [
                            'title' => 'Количество дней на оказание услуги или отгрузку товара после предоплаты (аванса)',
                        ]) ?>
                    </div>
                    <div>
                        <?= ($model->type == Contractor::TYPE_SELLER) ? $model->seller_guaranty_delay : $model->customer_guaranty_delay; ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Фиксированная скидка на всё</div>
                    <div><?php echo ($model->discount > 0) ? "{$model->discount}%" : '0' ?></div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">
                        <?= ($model->type == Contractor::TYPE_CUSTOMER) ? 'Статья приходов' : 'Статья расходов' ?>
                    </div>
                    <div>
                        <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                            <?php $incomeItem = InvoiceIncomeItem::findOne($model->invoice_income_item_id) ?>
                            <?php echo $incomeItem ? $incomeItem->name : '—'; ?>
                        <?php else: ?>
                            <?php $expenseItem = InvoiceExpenditureItem::findOne($model->invoice_expenditure_item_id) ?>
                            <?php echo $expenseItem ? $expenseItem->name : '—' ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3 <?=($model->type == 1) ? '' : 'hidden' ?>">
                    <div class="label weight-700 mb-3">Приоритет в оплате</div>
                    <div>
                        <?php switch ($model->payment_priority) {
                            case Contractor::PAYMENT_PRIORITY_HIGH:
                                echo '1 - Большой';
                                break;
                            case Contractor::PAYMENT_PRIORITY_MEDIUM:
                                echo '2 - Средний';
                                break;
                            case Contractor::PAYMENT_PRIORITY_LOW:
                                echo '3 - Наименьший';
                                break;
                        }; ?>
                    </div>
                </div>
                <div class="col-4 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Источник</div>
                    <div><?php echo $model->campaign->name ?? '—'; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-12 mb-3 pb-3">
                    <div class="label weight-700 mb-3">Комментарии</div>
                    <div><?php echo $model->comment ?: '—'; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>