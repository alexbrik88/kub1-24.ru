<?php
/* @var $model common\models\Contractor */
?>

<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="pl-2 pr-2 pt-2 d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Руководитель</h4>
            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">Должность</div>
                <div><?php echo $model->director_post_name ?: '—'; ?></div>
            </div>
            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">ФИО</div>
                <div><?php echo $model->director_name ?: '—'; ?></div>
            </div>
            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">E-mail</div>
                <div>
                    <?php if ($model->director_email): ?>
                        <a class="link link_bold" href="mailto:<?php echo $model->director_email; ?>"><?php echo $model->director_email; ?></a>
                    <?php else: ?>
                        —
                    <?php endif; ?>
                </div>
            </div>
            <div class="pb-3">
                <div class="label weight-700 mb-3">Телефон</div>
                <div>
                    <?php if ($model->director_phone): ?>
                        <a class="link link_dark" href="tel:<?php echo $model->director_phone; ?>"><?php echo $model->director_phone; ?></a>
                    <?php else: ?>
                        —
                    <?php endif; ?>
                </div>
            </div>
            <div class="pb-3">
                <label class="checkbox-inline match-with-leader" style="height: 39px; pointer-events: none;">
                    <input value="1" <?php if ($model->director_in_act) echo 'checked' ?> disabled type="checkbox">
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>