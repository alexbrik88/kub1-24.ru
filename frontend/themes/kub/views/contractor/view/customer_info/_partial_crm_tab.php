<?php

namespace frontend\views\contractor;

use common\models\Contractor;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\MessengerTypeList;
use yii\web\View;

/**
 * @var View $view
 * @var Contractor $contractor
 * @var Contact[] $contacts
 */

$typeList = new MessengerTypeList();

?>

<div class="row">
<?php if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON): ?>
<?php $contact = reset($contacts); ?>
<div class="col d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="pl-2 pr-2 pt-2 d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Контакт</h4>

            <div class="pb-3 mb-3 row">
                <div class="col-3">
                    <div class="label weight-700 mb-3">Телефон</div>
                    <div>
                        <?php if ($contact->primary_phone_number): ?>
                            <a class="link link_dark" href="tel:<?= $contact->primary_phone_number ?>">
                                <?= $contact->primary_phone_number; ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-3">
                    <div class="label weight-700 mb-3">Мессенджер</div>
                    <div>
                        <?= $typeList->getItems()[$contact->primary_messenger_type] ?? '—' ?>
                    </div>
                </div>

                <div class="col-3">
                    <div class="label weight-700 mb-3">E-mail</div>
                    <div>
                        <?php if ($contact->email_address): ?>
                            <a class="link link_bold" href="mailto:<?= $contact->email_address ?>">
                                <?= $contact->email_address ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-3">
                    <div class="label weight-700 mb-3">Skype</div>
                    <div>
                        <?php if ($contact->skype_account): ?>
                            <a class="link link_bold" href="skype:<?= $contact->skype_account ?>?call">
                                <?= $contact->skype_account ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="pb-3 mb-3 row">
                <div class="col-3">
                    <div class="label weight-700 mb-3">Телефон 2</div>
                    <div>
                        <?php if ($contact->secondary_phone_number): ?>
                            <a class="link link_dark" href="tel:<?= $contact->secondary_phone_number ?>">
                                <?= $contact->secondary_phone_number; ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-3">
                    <div class="label weight-700 mb-3">Мессенджер</div>
                    <div>
                        <?= $typeList->getItems()[$contact->secondary_messenger_type] ?? '—' ?>
                    </div>
                </div>

                <div class="col-6">
                    <div class="label weight-700 mb-3">Ссылка на соц. сеть</div>
                    <div>
                        <?php if ($contact->social_account): ?>
                            <a class="link link_bold" href="<?= $contact->social_account ?>">
                                <?= $contact->social_account ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php else: ?>
<?php foreach ($contacts as $index => $contact): ?>
<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="pl-2 pr-2 pt-2 d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Контакт <?= ($index + 1) ?></h4>

            <div class="pb-3" style="margin-bottom: 23px; pointer-events: none;">
                <label style="margin-right: 20px;">
                    <input type="radio" value="1" disabled<?php if ($contact->contact_default) echo ' checked' ?>> Выводить в карточке
                </label>
            </div>

            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">Должность:</div>
                <div><?= $contact->position->name ?? '—' ?></div>
            </div>

            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">ФИО</div>
                <div><?= $contact->contact_name ?? '—' ?></div>
            </div>

            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">E-mail</div>
                <div>
                <?php if ($contact->email_address): ?>
                    <a class="link link_bold" href="mailto:<?= $contact->email_address ?>">
                        <?= $contact->email_address ?>
                    </a>
                <?php else: ?>
                    —
                <?php endif; ?>
                </div>
            </div>

            <div class="pb-3 mb-3 row">
                <div class="col-6">
                    <div class="label weight-700 mb-3">Телефон</div>
                    <div>
                        <?php if ($contact->primary_phone_number): ?>
                            <a class="link link_dark" href="tel:<?= $contact->primary_phone_number ?>">
                                <?= $contact->primary_phone_number; ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-6">
                    <div class="label weight-700 mb-3">Мессенджер</div>
                    <div>
                        <?= $typeList->getItems()[$contact->primary_messenger_type] ?? '—' ?>
                    </div>
                </div>
            </div>

            <div class="pb-3 mb-3 row">
                <div class="col-6">
                    <div class="label weight-700 mb-3">Телефон 2</div>
                    <div>
                        <?php if ($contact->secondary_phone_number): ?>
                            <a class="link link_dark" href="tel:<?= $contact->secondary_phone_number ?>">
                                <?= $contact->secondary_phone_number; ?>
                            </a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="label weight-700 mb-3">Мессенджер</div>
                    <div>
                        <?= $typeList->getItems()[$contact->secondary_messenger_type] ?? '—' ?>
                    </div>
                </div>
            </div>

            <div class="pb-3 mb-3">
                <div class="label weight-700 mb-3">Skype</div>
                <div>
                    <?php if ($contact->skype_account): ?>
                        <a class="link link_bold" href="skype:<?= $contact->skype_account ?>?call">
                            <?= $contact->skype_account ?>
                        </a>
                    <?php else: ?>
                        —
                    <?php endif; ?>
                </div>
            </div>

            <div class="pb-3">
                <div class="label weight-700 mb-3">Ссылка на соц. сеть</div>
                <div>
                    <?php if ($contact->social_account): ?>
                        <a class="link link_bold" href="<?= $contact->social_account ?>">
                            <?= $contact->social_account ?>
                        </a>
                    <?php else: ?>
                        —
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>
<?php endif; ?>
</div>
