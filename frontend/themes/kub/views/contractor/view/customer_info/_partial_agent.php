<?php

use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\contractor\ContractorAgentBuyer;

/** @var $model \common\models\Contractor */

?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Агент</div>
                    <div>
                        <?= Html::checkbox('is_agent_input_info', $model->is_agent, [
                            'label' => false,
                            'disabled' => true
                        ]); ?>
                    </div>
                </div>
            </div>
            <?php if ($model->is_agent): ?>
                <div class="row">
                    <div class="column mb-3 pb-3 mr-4">
                        <div class="label weight-700 mb-3">Комиссионное вознаграждение</div>
                        <div><?php echo TextHelper::moneyFormat($model->agent_payment_percent, 2); ?> %</div>
                    </div>
                    <div class="column mb-3 pb-3 mr-4">
                        <div class="label weight-700 mb-3">Тип платежа</div>
                        <div><?php echo ($model->agentPaymentType) ? $model->agentPaymentType->name : ''; ?></div>
                    </div>
                    <div class="column mb-3 pb-3 mr-4">
                        <div class="label weight-700 mb-3">Дата начала расчетов</div>
                        <div><?php echo DateHelper::format($model->agent_start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .' г.'; ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="column mb-3 pb-3 mr-4">
                        <div class="label weight-700 mb-3">Покупатели</div>
                        <div>
                            <?php foreach ($model->agentBuyersInfo as $buyerInfo): ?>
                                <?php $start_date = DateHelper::format($buyerInfo->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                <?= $start_date.' г. '.Html::a($buyerInfo->buyer->getShortName(),  Url::to(['/contractor/view', 'type' => Contractor::TYPE_CUSTOMER, 'id' => $buyerInfo->buyer->id]), [
                                    'target' => '_blank'
                                ]) ?><br/>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>