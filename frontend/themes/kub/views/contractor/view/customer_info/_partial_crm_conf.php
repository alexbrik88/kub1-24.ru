<?php

use yii\helpers\Html;

/* @var $client frontend\modules\crm\models\Client */

?>

<?php if ($client !== null) : ?>
<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="mb-3 pb-3">
                <div class="label weight-700 mb-3">Чек</div>
                <div><?= $client->check ? Html::encode($client->check->name) : '—'; ?></div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-3 pb-3">
                <div class="label weight-700 mb-3">Тип сделки</div>
                <div><?= $client->deal ? Html::encode($client->deal->name) : '—'; ?></div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-3 pb-3">
                <div class="label weight-700 mb-3">Тип обращения</div>
                <div><?= $client->reason ? Html::encode($client->reason->name) : '—'; ?></div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-3 pb-3">
                <div class="label weight-700 mb-3">Сайт</div>
                <div><?= $client->site_url ? Html::a(Html::encode($client->site_url), $client->site_url) : '—'; ?></div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>
