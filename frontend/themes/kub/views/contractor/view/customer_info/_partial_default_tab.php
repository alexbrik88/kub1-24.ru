<?php

namespace frontend\views\contractor;

use common\models\Contractor;

/**
 * @var Contractor $model
 */

?>

<div class="row">
    <?= $this->render('_partial_director', [
        'model' => $model,
    ]) ?>

    <?= $this->render('_partial_chief_accountant', [
        'model' => $model,
    ]) ?>

    <?= $this->render('_partial_contact', [
        'model' => $model,
    ]) ?>
</div>
