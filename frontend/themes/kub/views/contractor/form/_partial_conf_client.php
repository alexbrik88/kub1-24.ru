<?php

namespace frontend\views\contractor;

use Closure;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\widgets\ButtonListFactory;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveForm $form
 * @var ClientForm $clientForm
 * @var array $textInputConfig
 * @var Closure $tooltipInputConfig
 */

?>

<?php if ($clientForm !== null) : ?>
<div class="mt-3 pt-3">
    <div class="row">
        <div class="col-4">
            <?= $form->field($clientForm, 'client_check_id', $textInputConfig)->widget(Select2::class, [
                'data' => (new ButtonListFactory($this))->createCheckList()->getItems(),
                'pluginOptions' => [
                    'placeholder' => '',
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                ],
            ]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($clientForm, 'deal_type_id', $tooltipInputConfig('#dealTypeTooltip'))->widget(Select2::class, [
                'data' => (new ListFactory)->createDealList()->getItems(),
                'pluginOptions' => [
                    'placeholder' => '',
                    'width' => '100%',
                ],
            ]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($clientForm, 'client_reason_id', $textInputConfig)->widget(Select2::class, [
                'data' => (new ButtonListFactory($this))->createReasonList()->getItems(),
                'pluginOptions' => [
                    'placeholder' => '',
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <?= $form->field($clientForm, 'site_url', $textInputConfig) ?>
        </div>
    </div>
</div>
<?php endif ?>
