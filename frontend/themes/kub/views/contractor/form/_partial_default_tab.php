<?php

namespace frontend\views\contractor;

use common\models\Contractor;
use yii\bootstrap\ActiveForm; // TODO: old bootstrap 3?
use yii\web\View;

/**
 * @var View $this
 * @var Contractor $model
 * @var ActiveForm $form
 * @var array $textInputConfig
 * @var array $partialDirectorTextInputConfig
 * @var string $legalForeignCss
 * @var string $physicalCss
 */
?>

<!-- LEGAL -->
<div class="face_type legal foreign row <?= $legalForeignCss ?>">
    <?= $this->render('_partial_director', [
        'model' => $model,
        'form' => $form,
        'textInputConfig' => $partialDirectorTextInputConfig,
    ]) ?>

    <?= $this->render('_partial_chief_accountant', [
        'model' => $model,
        'form' => $form,
        'textInputConfig' => $partialDirectorTextInputConfig,
    ]) ?>

    <?= $this->render('_partial_contact', [
        'model' => $model,
        'form' => $form,
        'textInputConfig' => $partialDirectorTextInputConfig,
    ]) ?>
</div>
<!-- PHYSICAL -->
<div class="face_type physical wrap <?= $physicalCss ?>">
    <div class="row">
        <div class="col-12">
            <h4 class="mb-3 pb-3">Контактные данные</h4>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                        'id' => 'physical-director_email',
                        'maxlength' => true,
                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'director_phone', $textInputConfig)->textInput([
                        'id' => 'physical-director_phone',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
