<?php

use common\models\Contractor;
use common\components\ImageHelper;
use common\components\TextHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use \common\assets\SortableAsset;

SortableAsset::register($this);

$js = <<<JS
$("#table-for-agent").sortable({
    containerSelector: "table",
    handle: "img.sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    }
});
JS;
$this->registerJs($js);

/* @var $this yii\web\View */
/* @var $model Contractor */
?>
<table id="table-for-agent" class="table table-style table-count-list" style="margin-bottom: 0;">
    <thead>
        <tr class="heading" role="row">
            <th class="delete-column-left" width="5%" tabindex="0" rowspan="1" colspan="1">
            </th>
            <th class="" tabindex="0" rowspan="1" colspan="1">
                Покупатель
            </th>
            <th width="150" class="" tabindex="0" rowspan="1" colspan="1" style="min-width: 145px!important;">
                Дата привлечения
            </th>
            <th class="delete-column-right" width="5%" tabindex="0" rowspan="1" colspan="1">
            </th>
        </tr>
    </thead>
    <tbody data-number="<?= count($model->agentBuyersInfo) ?>">
        <?php $number = 0; ?>
        <?php if (!$model->hasAgentBuyers) : ?>
            <?php
            foreach ($model->agentBuyersInfo as $key => $buyerInfo) {
                $number++;
                echo $this->render('_partial_agent_table_form_row', [
                    'agent' => $model,
                    'buyerInfo' => $buyerInfo,
                    'number' => $number,
                ]);
            } ?>
        <?php elseif (count($model->hasAgentBuyers) > 0) : ?>
            <?php
            foreach ($model->hasAgentBuyers as $key => $buyerArr) {
                $number++;
                echo $this->render('_partial_agent_table_new_row', [
                    'agent' => $model,
                    'buyerArr' => $buyerArr,
                    'number' => $number,
                ]);
            }
            ?>
        <?php endif; ?>

        <?= $this->render('_partial_agent_table_add_row', [
            'agent' => $model,
            'hasBuyers' => (bool)$number
        ]) ?>

        <tr class="template disabled-row" role="row" style="display: none">
            <td class="delete-column-left" style="white-space: nowrap;">
                <span class="icon-close remove-buyer"></span>
                <?= ImageHelper::getThumb('img/menu-humburger.png', [20, 14], [
                    'class' => 'sortable-row-icon',
                    'style' => 'padding-bottom: 9px;',
                ]); ?>
            </td>
            <td class="buyer-name">
                <?php echo Html::hiddenInput('agentBuyer__contractor_id') ?>
                <span class="form-control" style="width:100%"></span>
            </td>
            <td class="buyer-start-date">
                <?php echo Html::input('text', 'agentBuyer__start_date]', date('d.m.Y'), [
                        'class' => 'form-control date-picker',
                        'autocomplete' => 'off',
                    ]) . Html::tag('i', '', [
                        'class' => 'fa fa-calendar',
                        'style' => 'position: absolute; top: 15px; right: 25px; color: #cecece; cursor: pointer;',
                    ]) ?>
            </td>
            <td class="delete-column-right">

            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr role="row">
            <td class="delete-column-left" colspan="2" style="border: none; position: relative; border-top:1px solid #ddd;">
                <div class="portlet pull-left control-panel button-width-table" style="text-align:left">
                    <div class="btn-group pull-right" style="display: inline-block; padding-top: 7px">
                        <span class="btn-add-line button-regular button-hover-content-red pl-3">
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                            </svg>
                            <span>Добавить</span>
                        </span>
                    </div>
                </div>
            </td>
            <td style="border: none; border-top:1px solid #ddd;"></td>
            <td style="border: none; border-top:1px solid #ddd;"></td>
        </tr>
    </tfoot>
</table>

<!-- Modal Add Contractor -->
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
<!-- Modal Remove Buyer -->
<div id="modal-remove-buyer" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;" >
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить этого покупателя?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<script>

    var agentBuyerRow = <?= $number + 1 ?>

    $(document).on('click', '#table-for-agent .btn-add-line', function () {

        if ($('#from-new-add-row').is(':hidden')) {

            $('#from-new-add-row').show();

        } else {

            var $table = $('#table-for-agent');
            var $template = $table.find('.template');
            var $newRow = $template.clone();
            var $editedRow = $table.find('#from-new-add-row');
            var $select2 = $('select#agent-buyer');
            var data = $select2.select2('data');

            if (data && $select2.val() > 0 && $editedRow.find('.date-picker').val()) {
                var item = {
                    id: $select2.val(),
                    name: data[0].text || '',
                    date: $editedRow.find('.date-picker').val()
                };

                console.log(item);

                $newRow.removeClass('template').addClass('buyer-row');
                $newRow.find('.buyer-name').find('input').attr('name', 'agentBuyer['+agentBuyerRow+'][contractor_id]').val(item.id);
                $newRow.find('.buyer-start-date').find('input').attr('name', 'agentBuyer['+agentBuyerRow+'][start_date]').val(item.date);
                $newRow.find('.buyer-name').find('span.form-control').html(item.name);
                $table.find('.from-new-add').before($newRow);
                $newRow.find('.hasDatepicker').removeClass('hasDatepicker');
                $newRow.show();

                $select2.find('option:selected').remove();
                $select2.val('').trigger('change');
                $editedRow.find('input').val('');

                agentBuyerRow++;

                reinitializeDatePicker();
            }
        }
    });

    // fix two contractor forms
    $('#add-new').on('hidden.bs.modal', function() {
        $('#add-new .modal-header').html('');
        $('#block-modal-new-product-form').html('');
    })

    var removeBuyerBtn;
    $('#table-for-agent').on('click', '.remove-buyer', function () {
        $('#modal-remove-buyer').modal("show");
        removeBuyerBtn = this;
    });

    $('#modal-remove-buyer').on('click', '.yes', function() {

        if ($(removeBuyerBtn).hasClass('from-new')) {
            $('#from-new-add-row').hide();
            $('select#agent-buyer').val('').trigger('change');
        } else {
            var $row = $(removeBuyerBtn).parents('tr');
            var contractor = {
                id: $row.find('.buyer-name').find('input').val() || null,
                name: $row.find('.buyer-name').find('span.form-control').html() || null
            }

            $row.remove();

            console.log(contractor);
            if (contractor.id && contractor.name) {
                $('select#agent-buyer').append("<option value='"+ contractor.id +"'>"+ contractor.name +"</option>")
            }
        }
    });

    reinitializeDatePicker = function() {
        refreshDatepicker();
    }

    $(document).on('click', '.buyer-start-date .fa-calendar', function() {
        $(this).parents('.buyer-start-date').find('.date-picker').datepicker('show');
    });

</script>