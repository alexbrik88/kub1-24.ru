<?php

use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\widgets\AddItemDialogWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\bootstrap4\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */
/**
 * @var ClientForm $clientForm
 * @var ContactForm[] $contactForms
 * @var bool $isCrmView TRUE - если открыли из CRM
 */

yii\widgets\MaskedInputAsset::register($this);

$tasks = Task::findByContractor($model);
$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-delete',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$tooltipInputConfig = function (string $selector) use ($textInputConfig): array {
    $icon = Html::tag(
        'span',
        '<svg class="tooltip-question-icon svg-icon mx-1"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>',
        [
            'class' => 'tooltip2',
            'style' => '',
            'data-tooltip-content' => $selector,
        ]
    );

    $textInputConfig['template'] = str_replace('{label}', "{label}\n{$icon}", $textInputConfig['template']);

    return $textInputConfig;
};

$isLegal = ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
$isPhysical = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
$isForeign = ($model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON);
$isSeller = $model->type == Contractor::TYPE_SELLER || $model->is_seller;
$isCustomer = $model->type == Contractor::TYPE_CUSTOMER || $model->is_customer;

$physicalCss = $isPhysical ? '' : 'hidden';
$foreignCss = $isForeign ? '' : 'hidden';
$legalCss = $isPhysical || $isForeign ? 'hidden' : '';
$legalForeignCss = $isPhysical ? 'hidden' : '';

$tabLabel = Html::tag('span', 'Реквизиты', [
    'class' => 'face_type legal foreign '.$legalForeignCss,
]) . Html::tag('span', 'Данные паспорта, р/с', [
    'class' => 'face_type physical '.$physicalCss,
]);

$tab = Yii::$app->request->get('tab');
$subTab = Yii::$app->request->get('sub_tab');
if (!in_array($subTab, ['requisites', 'comments', 'agent']))
    $subTab = false;

// SalePoint
$hasSalePoint = SalePoint::find()->where(['company_id' => $model->company->id])->exists();
$salePoints = ($hasSalePoint) ? SalePoint::getSelect2Data() : [];
// Industry
$hasCompanyIndustry = CompanyIndustry::find()->where(['company_id' => $model->company->id])->exists();
$companyIndustries = ($hasCompanyIndustry) ? CompanyIndustry::getSelect2Data() : [];
?>

<?= AddItemDialogWidget::widget() ?>

<div class="hidden">
    <div id="campaignTooltip">
        Укажите источник, из которого клиент пришел к вам
    </div>
    <div id="activityTooltip">
        Укажите вид деятельности клиента, это поможет<br>группировать клиентов для сегментирования
    </div>
    <div id="dealTypeTooltip">
        Укажите тип сделки (воронку продаж),<br>по которой вы ведете клиента к продаже
    </div>
    <div id="clientTypeTooltip">
        В этом поле укажите тип отношений с клиентом.<br>Например: Клиент или Партнер
    </div>
</div>

<?= ConfirmModalWidget::widget([ // TODO: Если эту переснести ниже, то она сабмитить всю форму!?
    'options' => [
        'id' => 'deletion-confirm-dialog',
    ],
    'toggleButton' => false,
    'confirmUrl' => $isCrmView
        ? Url::to(['delete', 'contractor_id' => $model->id])
        : Url::to(['delete', 'type' => $type ?? $model->type, 'id' => $model->id]),
    'message' => $isCrmView
        ? 'Вы уверены, что хотите удалить клиента?'
        : 'Вы уверены, что хотите удалить контрагента?',
]) ?>

<?php Modal::begin([
    'id' => 'contractor-duplicates-modal',
    'title' => $model->isNewRecord ? 'Дубли Контрагента' : 'Объединить покупателя и поставщика',
    'toggleButton' => false,
    'closeButton' => [
        'label' => frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>
<?php Modal::end() ?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ?
        Url::to(['create', 'type' => $type ?? $model->type]) :
        Url::to(['update', 'id' => $model->id,
            'type' => $type ?? $model->type,
            'tab' => Yii::$app->request->get('tab', 'info'),
            'sub_tab' => Yii::$app->request->get('sub_tab'),
        ]),
    'id' => 'update-contractor-form',
    'class' => 'form-horizontal',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => [
        'data-type' => $model->type,
        'data-find-duplicate' => Url::to([
            '/contractor/find-duplicate',
            'id' => $model->id,
            'type' => Yii::$app->request->get('type'),
        ]),
    ],
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

<?php

if ($isCrmView) {
    $form->action = null;
}

?>

    <div class="form-body form-horizontal">
        <?php if (isset($showErrorSummary)) {
            echo $form->errorSummary($model);
        } ?>
        <?= Html::activeHiddenInput($model, 'type'); ?>
        <?= Html::hiddenInput(null, $model->id, [
            'id' => 'contractor-id',
        ]); ?>
        <?= Html::hiddenInput(null, $model->isNewRecord, [
            'id' => 'contractor-is_new_record',
        ]); ?>

        <?= $this->render('_partial_main_info', [
            'model' => $model,
            'type' => $type ?? $model->type,
            'clientForm' => $clientForm,
            'form' => $form,
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
            'isLegal' => $isLegal,
            'isPhysical' => $isPhysical,
            'isForeign' => $isForeign,
            'tooltipInputConfig' => $tooltipInputConfig,
        ]) ?>
        <div class="clearfix"></div>

        <div class="wrap mb-3">
            <div class="profile-form-tabs">
            <?= Tabs::widget([
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'headerOptions' => ['class' => 'nav-item'],
                'items' => array_merge([
                    [
                        'label' => $tabLabel,
                        'encode' => false,
                        'linkOptions' => ['class' => 'nav-link' . ((!$subTab && !$isCrmView) || $subTab == 'requisites' ? ' active' : '')],
                        'content' => $this->render('_partial_details', [
                            'model' => $model,
                            'form' => $form,
                            'isLegal' => $isLegal,
                            'isPhysical' => $isPhysical,
                            'isForeign' => $isForeign,
                        ]),
                    ],
                    [
                        'label' => 'Настройки Покупателя',
                        'options' => [
                            'class' => 'conf_customer'.($isCustomer ? '' : ' hidden'),
                        ],
                        'headerOptions' => [
                            'class' => 'nav-item conf_customer'.($isCustomer ? '' : ' hidden'),
                        ],
                        'linkOptions' => ['class' => 'nav-link' .  ($subTab == 'comments' ? ' active' : '')],
                        'content' => $this->render('_partial_conf_customer',
                            compact('model', 'form', 'hasSalePoint', 'salePoints', 'hasCompanyIndustry', 'companyIndustries')),
                    ],
                    [
                        'label' => 'Настройки Поставщика',
                        'options' => [
                            'class' => 'conf_seller'.($isSeller ? '' : ' hidden'),
                        ],
                        'headerOptions' => [
                            'class' => 'nav-item conf_seller'.($isSeller ? '' : ' hidden'),
                        ],
                        'linkOptions' => ['class' => 'nav-link' .  ($subTab == 'comments' ? ' active' : '')],
                        'content' => $this->render('_partial_conf_seller',
                            compact('model', 'form', 'hasSalePoint', 'salePoints', 'hasCompanyIndustry', 'companyIndustries')),
                    ],
                    [
                        'label' => 'Настройки CRM',
                        'linkOptions' => ['class' => 'nav-link' .  ($isCrmView ? ' active' : '')],
                        'content' => $this->render('_partial_conf_client', [
                            'form' => $form,
                            'model' => $model,
                            'clientForm' => $clientForm,
                            'textInputConfig' => $textInputConfig,
                            'tooltipInputConfig' => $tooltipInputConfig,
                        ]),
                        'visible' => $clientForm !== null,
                        'active' => $isCrmView,
                    ],
                ], (FALSE && $model->type == Contractor::TYPE_SELLER && !$model->isNewRecord) ?
                    [
                        [
                            'label' => 'Агент',
                            'linkOptions' => ['class' => 'nav-link' . ($subTab == 'agent' ? ' active' : '')],
                            'content' => $this->render('_partial_agent', [
                                'model' => $model,
                                'form' => $form,
                            ]),
                        ]
                    ] : [])
            ]); ?>
            </div>
        </div>

        <div class="clearfix"></div>

<?php if ($contactForms): ?>
    <div>
    <?= Tabs::widget([
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-4'],
        'items' => [
            [
                'label' => 'Для документов',
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => $isCrmView ? 'nav-link' : 'nav-link active',
                ],
                'content' => $this->render('_partial_default_tab', [
                    'model' => $model,
                    'form' => $form,
                    'textInputConfig' => $textInputConfig,
                    'partialDirectorTextInputConfig' => $partialDirectorTextInputConfig,
                    'legalForeignCss' => $legalForeignCss,
                    'physicalCss' => $physicalCss,
                ]),
                'active' => !$isCrmView,
            ],
            [
                'label' => 'CRM контакты',
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => $isCrmView ? 'nav-link active' : 'nav-link',
                ],
                'content' => $this->render('_partial_crm_tab', [
                    'contactForms' => $contactForms,
                    'contractor' => $model,
                    'widget' => $form,
                    'textInputConfig' => $textInputConfig,
                    'tooltipInputConfig' => $tooltipInputConfig,
                ]),
                'active' => $isCrmView,
            ],
        ],
    ]) ?>
    </div>
<?php else: ?>
    <?= $this->render('_partial_default_tab', [
        'model' => $model,
        'form' => $form,
        'textInputConfig' => $textInputConfig,
        'partialDirectorTextInputConfig' => $partialDirectorTextInputConfig,
        'legalForeignCss' => $legalForeignCss,
        'physicalCss' => $physicalCss,
    ]) ?>
<?php endif; ?>

    <div class="wrap wrap_btns check-condition visible mb-0 mt-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                    'data-style' => 'expand-right',
                ]) ?>
            </div>
            <div class="column">
                <span style="padding: 0 6px;">
                    <?php if (!$model->isNewRecord
                        && Yii::$app->authManager
                        && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE, ['model' => $model])
                    ): ?>
                        <?php if ($model->getDeleteItemCondition() && !$model->hasCashFlows() && !count($tasks)): ?>
                            <?= Html::button('Удалить', [
                                'class' => 'button-regular button-width button-hover-content-red',
                                'data-toggle' => 'modal',
                                'data-target' => '#deletion-confirm-dialog',
                            ]) ?>
                        <?php else: ?>
                            <?= Html::button('Удалить', [
                                'class' => 'button-clr button-width button-regular button-hover-transparent tooltip2-delete',
                                'data-tooltip-content' => '#tooltip_not_delete',
                            ]); ?>
                            <div style="display:none">
                                <span id="tooltip_not_delete">
                                    <?php if (!$model->getDeleteItemCondition()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, у которого есть счета, удалить нельзя.
                                        <br>
                                    <?php endif; ?>
                                    <?php if ($model->hasCashFlows()): ?>
                                        <?= $model->type == Contractor::TYPE_CUSTOMER ? 'Покупателя' : 'Поставщика'; ?>, по которому есть операции в разделе Деньги, удалить нельзя.
                                    <?php endif; ?>
                                    <?php if (count($tasks)): ?>
                                        Клиента, по которому есть задачи в разделе CRM, удалить нельзя.
                                    <?php endif; ?>
                                </span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
                <span style="padding: 0 6px;">
                <?php if ($isCrmView): ?>
                    <?= Html::a('Отменить', ['client/index', 'contractor_id' => $model->id], [
                        'class' => 'button-clr button-width button-regular button-hover-transparent undo-contractor'
                    ]) ?>
                <?php else: ?>
                    <?= Html::a('Отменить', $model->isNewRecord ? Url::to(['index', 'type' => $model->type]) : Url::to(['view', 'type' => $model->type, 'id' => $model->id, 'tab' => $tab]), [
                        'class' => 'button-clr button-width button-regular button-hover-transparent undo-contractor',
                    ]); ?>
                <?php endif ?>
                </span>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

    <script>
        function chiefAccountantIsNotDirector(attribute, value) {
            return !$('#chief_accountant_is_director_input').is(':checked');
        }

        function contactIsNotDirector(attribute, value) {
            return !$('#contact_is_director_input').is(':checked');
        }

        function contractorIsIp() {
            return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsNotIp() {
            return $('#contractor-companytypeid').val() != "<?= CompanyType::TYPE_IP ?>";
        }

        function contractorIsPhysical() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
        }

        function contractorIsLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
        }

        function contractorIsForeignLegal() {
            return $("#contractor_face_type").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
        }
        function contractorIsAgent() {
            return $("#contractor_is_agent_input:checked").length;
        }
    </script>

<?php
$typeArray = json_encode(array_flip($model->getTypeArray()));
$this->registerJs(<<<JS
    var checkDirectorPostName = function() {
        if ($( "#contractor_face_type" ).val() != '1' && $('#contractor-companytypeid').val() != '1') {
            $('#contractor-director_post_name').prop('disabled', false);
            $('.field-contractor-director_post_name').show();
        } else {
            $('#contractor-director_post_name').prop('disabled', true);
            $('.field-contractor-director_post_name').hide();
        }
    }
    var checkFaceType = function(value) {
        switch (value) {
            case '1':
                $('.face_type:not(.physical)').toggleClass('hidden', true);
                $('.face_type.physical').toggleClass('hidden', false);
                $('.face_type:not(.physical) input').prop('disabled', true);
                $('.face_type.physical input').prop('disabled', false);

                $('#contractor-legal_address').attr('placeholder','');
                $('#contractor-actual_address').attr('placeholder','');
                $('#contractor-postal_address').attr('placeholder','');

                break;
            case '2':
                $('.face_type:not(.foreign)').toggleClass('hidden', true);
                $('.face_type.foreign').toggleClass('hidden', false);
                $('.face_type:not(.foreign) input').prop('disabled', true);
                $('.face_type.foreign input').prop('disabled', false);

                $('#contractor-legal_address').attr('placeholder','Пример: USA, NY, street, etc');
                $('#contractor-actual_address').attr('placeholder','Пример: USA, NY, street, etc');
                $('#contractor-postal_address').attr('placeholder','Пример: USA, NY, street, etc');
                $('.bank_account.face_type.legal').find('input').prop('disabled', true);
                $('.bank_account.face_type.foreign').find('input').prop('disabled', false);

                break;
            case '0':
            default:
                $('.face_type:not(.legal)').toggleClass('hidden', true);
                $('.face_type.legal').toggleClass('hidden', false);
                $('.face_type:not(.legal) input').prop('disabled', true);
                $('.face_type.legal input').prop('disabled', false);

                $('#contractor-legal_address').attr('placeholder','');
                $('#contractor-actual_address').attr('placeholder','');
                $('#contractor-postal_address').attr('placeholder','');
                $('.bank_account.face_type.foreign').find('input').prop('disabled', true);
                $('.bank_account.face_type.legal').find('input').prop('disabled', false);

                break;
        }
    };
    var findContractorDuplicates = function(form) {
        let hasDuplicate = false;
        let url = $(form).data('find-duplicate');
        $.ajax({
            type: "POST",
            url: url,
            async : false,
            data: $(form).serialize(),
            success: function(data) {
                if (data) {
                    hasDuplicate = true;
                    $("#contractor-duplicates-modal .modal-body").html(data);
                    $("#contractor-duplicates-modal").modal("show");
                }
            }
        });

        return hasDuplicate;
    };
    $(document).on("hidden.bs.modal", "#contractor-duplicates-modal", function (e) {
        $("#contractor-duplicates-modal .modal-body").html("");
    });
    $(document).on("submit", "#update-contractor-form", function (e) {
        if ($("#contractor-is_new_record", this).val()) {
            let hasDuplicate = findContractorDuplicates(this);
            return !hasDuplicate;
        } else {
            const changeRuleModal = $('#modal-payment-delay-update-rule');
            const ruleCustomer = $("input[name=customer-payment-delay-update-rule]", this);
            const ruleSeller = $("input[name=seller-payment-delay-update-rule]", this);
            if (changeRuleModal.length) {
                if (ruleCustomer.length && !ruleCustomer.val() || ruleSeller.length && !ruleSeller.val()) {
                    changeRuleModal.modal('show');
                    return false;
                }
            }
        }

        return true;
    });

    $(document).on("change", "#contractor-opposite", function () {
        let checked = this.checked;
        let form = this.form;
        let type = $(form).data("type");
        let isSeller = type == "1" || checked;
        let isCustomer = type == "2" || checked;
        $(".conf_seller", form).toggleClass('hidden', !isSeller);
        $(".conf_customer", form).toggleClass('hidden', !isCustomer);
        if (checked) {
            findContractorDuplicates(form);
        }
    });
    $(document).on("change", "#contractor-companytypeid", function () {
        if ($("#contractor-itn").val()) {
            $(this).closest("form").yiiActiveForm("validateAttribute", "contractor-itn");
        }
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
        checkDirectorPostName();
    });
    $(document).on("change", "#chief_accountant_is_director_input, #contact_is_director_input", function () {
        var wrap = $(this).closest(".wrap");
        wrap.find("input:text,select[name]").prop("disabled", $(this).is(":checked"));
        if ($(this).is(":checked")) {
            wrap.find("input.fio_in_act").prop("checked", false).prop("disabled", true);
            wrap.find('select[name]').val('');
        } else {
            wrap.find("input.fio_in_act").prop("disabled", false);
        }
        wrap.find("input.fio_in_act").uniform("refresh");
        wrap.find('select[name]').trigger('change');
    });
    $(document).on("change", ".fio_in_act", function() {
        if ($(this).prop("checked")) {
            $(".fio_in_act").not(this).prop("checked", false).uniform("refresh");
        }
    });
    $(document).on("input", "#contractor-bic", function(){
        if ($(this).val() === "") {
            $("#contractor-bank_name").val("");
            $("#contractor-bank_city").val("");
            $("#contractor-corresp_account").val("");
        }
    });
    var companyType = $typeArray;
    var directorEmail;
    var contractorIsNewRecord = $("#contractor-is_new_record").val();

    $('#contractor-itn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        beforeRender: function (container) {
            $(".field-contractor-itn p.exists-contractor").empty();
            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: this.value,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
        },
        onSelect: function(suggestion) {
            console.log(suggestion);
            var companyTypeId = '-1';

            if ($('#contractor-itn').val() != suggestion.data.inn) {
                $('#legal-director_email').val('');
                $('#legal-director_phone').val('');
                $('#contractor-chief_accountant_email').val('');
                $('#contractor-chief_accountant_email').val('');
                $('#contractor-contact_email').val('');
                $('#contractor-contact_phone').val('');
            }
            $('#contractor-itn').val(suggestion.data.inn);
            if (contractorIsNewRecord) {
                $.post("check-availability", {
                    inn: suggestion.data.inn,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId).trigger('change');
            if (companyTypeId == 1) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
                $('.field-contractor-bin label').text('ОГРНИП');
            } else {
                $('.field-contractor-ppc').show();
                $('.field-contractor-bin label').text('ОГРН');
            }
            var address = '';
            if (suggestion.data.address.data !== null && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
    $(document).ready(function() {
        checkFaceType($("#contractor_face_type").val());
        checkDirectorPostName();
    });

    $(document).on('change','#contractor_face_type', function() {
        checkFaceType(this.value);
        checkDirectorPostName();
    });

    $('#contractor-name').on('input', function(){
        if ($('#contractor-companytypeid').val() == 1) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    
    $('#contractor-customer_payment_delay').on('change', function() {
        const form = $('#update-contractor-form');
        const isUpdate = $('#contractor-id').val() > 0;
        const isRuleAdded = $("[name=customer-payment-delay-update-rule]", form).length; 
        if (isUpdate)
            if (!isRuleAdded)
                form.append('<input type="hidden" name="customer-payment-delay-update-rule"/>');
    });
    
    $('#contractor-seller_payment_delay').on('change', function() {
        const form = $('#update-contractor-form');
        const isUpdate = $('#contractor-id').val() > 0;
        const isRuleAdded = $("[name=seller-payment-delay-update-rule]", form).length;
        if (isUpdate)
            if (!isRuleAdded)
                form.append('<input type="hidden" name="seller-payment-delay-update-rule"/>');
    });
    
    $('#modal-payment-delay-update-rule .ok').on('click', function() {
        const form = $('#update-contractor-form');
        const ruleCustomer = $("input[name=customer-payment-delay-update-rule]", form);
        const ruleSeller = $("input[name=seller-payment-delay-update-rule]", form);
        const userSelected = $('.radio-payment-delay-update-rule:checked').val() || 0;
        if (ruleCustomer.length)
            ruleCustomer.val(userSelected);
        if (ruleSeller.length)
            ruleSeller.val(userSelected);
        
        form.submit();
    });
    
    $('#modal-payment-delay-update-rule').on('hidden.bs.modal', function() {
        Ladda.stopAll();
    });
JS
);
