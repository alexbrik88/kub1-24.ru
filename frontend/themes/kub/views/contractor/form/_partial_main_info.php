<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\widgets\ButtonListFactory;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\themes\kub\helpers\Icon;
use common\models\address\Country;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/**
 * @var ClientForm $clientForm
 * @var Closure $tooltipInputConfig
 */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$checkboxConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
        'style' => 'padding-top: 8px;'
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$textAreaConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$inputListConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];

$helpAccounting = Html::tag('span', '', [
    'class' => 'tooltip2 ico-question',
    'style' => 'display: inline-block; margin: -6px 5px; vertical-align: middle;',
    'data-tooltip-content' => '#tooltip_not_accounting',
]);

$getTitle = function () use ($model, $type, $clientForm) {
    if ($clientForm) {
        return 'Редактировать клиента';
    }

    return implode(' ', [
        ($model->isNewRecord) ? 'Добавить' : 'Редактировать',
        (($type ?? $model->type) == Contractor::TYPE_SELLER) ? 'Поставщика' : 'Покупателя',
    ]);
};

$physicalCss = $isPhysical ? '' : 'hidden';
$foreignCss = $isForeign ? '' : 'hidden';
$legalCss = $isPhysical || $isForeign ? 'hidden' : '';
$legalForeignCss = $isPhysical ? 'hidden' : '';

$innLabel = Html::tag('span', '/ITN', [
    'class' => 'face_type foreign '.$foreignCss,
]);

$responsibleEmployers = ArrayHelper::map($model->company->getEmployeeCompanies()
    ->joinWith('employee')
    ->andWhere(($model->responsible_employee_id)
    ? ['or', ['employee_company.is_working' => true], ['employee_company.employee_id' => $model->responsible_employee_id]]
    : ['employee_company.is_working' => true])
    ->andWhere(['employee.is_deleted' => false])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');
?>

    <div class="wrap">
        <div class="row">
            <div class="col-12 mb-1">
                <h4>
                    <?= Html::encode($getTitle()) ?>
                </h4>
            </div>
            <div class="face_type legal col-6 <?=$legalCss?>">
                <!-- INN LEGAL -->
                <?= $form->field($model, 'ITN', array_merge($textInputConfig, [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}<p class='exists-contractor'></p>\n{endWrapper}",
                ]))->label('ИНН')->textInput([
                    'placeholder' => 'Автозаполнение по ИНН' ,
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                        'prod' => YII_ENV_PROD ? 1 : 0,
                    ],
                ]); ?>
            </div>
            <div class="face_type foreign col-6 <?=$foreignCss?>">
                <?= $form->field($model, 'ITN', $textInputConfig)->label('ИНН/ITN')->textInput([
                    'id' => 'foreign_'.Html::getInputId($model, 'ITN'),
                    'placeholder' => 'Пример: 0123456789',
                ]); ?>
            </div>
            <div class="face_type physical col-6 <?=$physicalCss?>">
                <!-- LastName Physical -->
                <?= $form->field($model, 'physical_lastname', $textInputConfig)->label('Фамилия:')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-6">
                <!-- face_type -->
                <?= $form->field($model, 'face_type', $textInputConfig)->widget(Select2::class, [
                    'options' => [
                        'id' => 'contractor_face_type',
                    ],
                    'data' => Contractor::$contractorFaceType,
                    'hideSearch' => true,
                    'disabled' => !empty($face_type_opt),
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
        </div>
        <div class="face_type physical row <?=$physicalCss?>">
            <div class="col-3">
                <?= $form->field($model, 'physical_firstname', $textInputConfig)->label('Имя:')->textInput([
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                    ],
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'physical_patronymic', $textInputConfig)->label('Отчество:')->textInput([
                    'maxlength' => true,
                    'readonly' => (boolean) $model->physical_no_patronymic,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                    ],
                ]); ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'physical_no_patronymic', $checkboxConfig)->label('Нет отчества:')->checkbox([], false); ?>
            </div>
        </div>
        <div class="row face_type legal <?=$legalCss?>" style="display: flex">
            <div class="col-12 legal <?=$legalCss?>">
                <?= $form->field($model, 'name', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                        'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
                    ],
                ])->label('Название '.($model->type == 1 ? 'Поставщика' : 'Покупателя').':'); ?>
            </div>
            <div class="face_type legal col-6 <?=$legalCss?>">
                <?= $form->field($model, 'companyTypeId', $textInputConfig)->widget(Select2::class, [
                    'data' => $model->getTypeArray(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])->label('Форма'); ?>
            </div>
            <?php if ($clientForm !== null) : ?>
                <div class="face_type legal physical col-3">
                    <?= $form->field($clientForm, 'client_type_id', $tooltipInputConfig('#clientTypeTooltip'))->widget(Select2::class, [
                        'data' => (new ButtonListFactory($this))->createClientTypeList()->getItems(),
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%',
                            'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                        ],
                    ])->label('Тип отношений'); ?>
                </div>
            <?php endif ?>
        </div>
        <div class="row face_type foreign <?=$foreignCss?>">
            <div class="col-12">
                <?= $form->field($model, 'name', $textInputConfig)->textInput([
                    'id' => 'foreign_'.Html::getInputId($model, 'name'),
                    'maxlength' => true,
                    'placeholder' => 'Пример: Company name',
                ])->label('Название '.(($type ?? $model->type) == 1 ? 'Поставщика' : 'Покупателя')); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'foreign_legal_form', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Пример: LCC',
                ])->label('Форма'); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'decoding_legal_form', $textInputConfig)->textInput([
                    'maxlength' => true,
                ])->label('Pacшифровка формы'); ?>
            </div>
        </div>
<?php if (!$model->type != Contractor::TYPE_POTENTIAL_CLIENT): ?>
        <div class="row">
            <div class="col-3 face_type legal foreign <?=$legalForeignCss?>">
                <?= $form->field($model, 'taxation_system', $inputListConfig)->label()->radioList(
                    [
                        Contractor::WITH_NDS => 'С НДС',
                        Contractor::WITHOUT_NDS => 'Без НДС',
                    ],
                    [
                        'class' => 'radio-list inp_one_line-contractor',
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::tag('label', Html::radio($name, $checked, [
                                    'value' => $value
                                ]) . $label, [
                                'class' => 'mr-3',
                            ]);
                        },
                    ]
                ); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'status', $inputListConfig)->label()->radioList(
                    [
                        Contractor::ACTIVE => 'Активен',
                        Contractor::INACTIVE => 'Не активен',
                    ], [
                    'class' => 'radio-list inp_one_line-contractor',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, [
                                'value' => $value,
                            ]) . $label, [
                            'class' => 'radio-inline width120 marg_left mrg_lf_checkbox',
                        ]);
                    },
                ]); ?>
            </div>
            <div class="col-3">
                <?php if ($model->getCanHasOpposite()) : ?>
                    <?php
                    $model->opposite = (int) $model->getHasOpposite();
                    $helpOpposite = Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'style' => '',
                        'data-tooltip-content' => '#tooltip_has_opposite',
                    ]);
                    ?>
                    <?= $form->field($model, 'opposite', [
                        'template' => "{label}\n{beginWrapper}\n{input}{$helpOpposite}\n{error}\n{endWrapper}",
                    ])->checkbox([
                        'disabled' => $model->getHasOpposite(),
                    ], false); ?>
                    <div class="hidden">
                        <span id="tooltip_has_opposite">
                            Поставив галочку, данный контрагент будет отображаться и в Покупателях и Поставщиках
                        </span>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-3">
                <?php
                $helpAccounting = Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip2',
                    'style' => '',
                    'data-tooltip-content' => '#tooltip_not_accounting',
                ]);
                ?>
                <?= $form->field($model, 'not_accounting', [
                    'options' => [
                        'class' => '',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}{$helpAccounting}\n{error}\n{endWrapper}",
                ])->checkbox([], false); ?>
                <div class="hidden">
                    <span id="tooltip_not_accounting">
                        Документы по такому клиенту не попадают в выгрузку для 1с и видны только для руководителя
                    </span>
                </div>
            </div>
        </div>
<?php endif; ?>
        <div class="row face_type foreign <?=$foreignCss?>">
            <div class="col-6">
                <?= $form->field($model, 'order_currency', $textInputConfig)->widget(Select2::class, [
                    'data' => \common\models\currency\Currency::getAllCurrency(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ])->label('Выставлять счета в'); ?>
                <div class="text-left contractor-currency-disclaimer">
                    <i>Конвертация в RUB происходит автоматически, на день оплаты по курсу банка России.</i>
                </div>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'country_id', $textInputConfig)->widget(Select2::class, [
                    'data' => Country::find()->select(['name_short', 'id'])->indexBy('id')->column(),
                    //'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ])->label('Страна'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'responsible_employee_id', $textInputConfig)->widget(Select2::class, [
                    'data' => $responsibleEmployers,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field(
                    $model,
                    'campaign_id',
                    $tooltipInputConfig('#campaignTooltip')
                )->widget(Select2::class, [
                    'data' => (new ButtonListFactory($this))->createCampaignList()->getItems(),
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                    ],
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'activity_id', $tooltipInputConfig('#activityTooltip'))->widget(Select2::class, [
                    'data' => (new ButtonListFactory($this))->createActivityList()->getItems(),
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                    ],
                ]) ?>
            </div>
        </div>

        <?= $form->field($model, 'comment', $textAreaConfig)->textarea([
            'maxlength' => true,
            'style' => 'width: 100%;',
        ]) ?>
    </div>
