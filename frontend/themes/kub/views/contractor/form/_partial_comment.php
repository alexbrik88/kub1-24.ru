<?php

namespace views;

use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$delayInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$textAreaConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$discountInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
];

$responsibleEmployers = ArrayHelper::map($model->company->getEmployeeCompanies()
    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');
?>

<div class="mt-3 pt-3">
        <div class="row legal">
            <div class="col-6">
                <?= $form->field($model, 'responsible_employee_id', $textInputConfig)->widget(Select2::class, [
                    'data' => $responsibleEmployers,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]); ?>
            </div>

            <div class="col-6">
                <?= $form->field($model, 'payment_delay', $delayInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                    'type' => 'number',
                    'min' => 0,
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                    <?php echo $form->field($model, 'invoice_income_item_id')->widget(ExpenditureDropdownWidget::class, [
                        'loadAssets' => false,
                        'income' => true,
                        'exclude' => ['items' => InvoiceIncomeItem::ITEM_OWN_FOUNDS],
                        'options' => [
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Статья приходов');
                    echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                        'inputId' => 'contractor-invoice_income_item_id',
                        'type' => 'income',
                    ]) ?>
                <?php else: ?>
                    <?php echo $form->field($model, 'invoice_expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                        'loadAssets' => false,
                        'exclude' => ['items' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS],
                        'options' => [
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Статья расходов');
                    echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                        'inputId' => 'contractor-invoice_income_item_id',
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row legal">
            <div class="col-6">
                <?= $form->field($model, 'source', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'guaranty_delay', $delayInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                    'type' => 'number',
                    'min' => 0,
                ])->label('Отсрочка выполнения обязательств в днях ' . Html::tag('span', Icon::QUESTION, [
                    'title' => 'Количество дней на оказание услуги или отгрузку товара после предоплаты (аванса)'
                ])); ?>
            </div>
        </div>
        <div class="row legal">
            <div class="col-6">
                <?= $form->field($model, 'discount', $discountInputConfig)->input('number', [
                    'min' => 0,
                    'max' => 99.99,
                    'step' => 'any',
                    'style' => 'width: 100%;',
                ])->label('Фиксированная скидка на всё, %'); ?>
            </div>
            <div class="col-6 <?=($model->type == 1) ? '' : 'hidden' ?>">
                <?= $form->field($model, 'payment_priority', $textInputConfig)->widget(Select2::class, [
                    'data' => [
                        Contractor::PAYMENT_PRIORITY_HIGH => '1 - Большой',
                        Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - Средний',
                        Contractor::PAYMENT_PRIORITY_LOW => '3 - Наименьший',
                    ],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])->label('Приоритет в оплате ' . Html::tag('span', Icon::QUESTION, [
                    'title' => 'Приоритетность нужна для Платежного Календаря, чтобы в случае кассового разрыва видеть, какие платежи можно сдвинуть, а какие нужно оплачивать.'])); ?>
            </div>
        </div>
    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'comment', $textAreaConfig)->textarea([
                'maxlength' => true,
                'style' => 'width: 100%;',
            ]) ?>
        </div>
    </div>
</div>
