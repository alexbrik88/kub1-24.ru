<?php

use common\components\widgets\BikTypeahead;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\rbac\permissions;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $contractor common\models\Contractor */
/* @var $isForeign boolean */

$canUpdate = Yii::$app->user->can(permissions\Contractor::UPDATE, [
    'model' => $contractor,
]);

$dataProvider = new ActiveDataProvider([
    'query' => $contractor->getContractorAccounts()->orderBy(['is_main' => SORT_DESC]),
]);
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'rs-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-style',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],

    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => "{items}",
    'columns' => [
        [
            'attribute' => 'bank_name',
            'enableSorting' => false,
            'label' => 'Банк',
            'headerOptions' => [
                'width' => '25%',
            ],
        ],
        [
            'attribute' => 'bik',
            'label' => $isForeign ? 'SWIFT' : 'БИК',
            'enableSorting' => false,
            'headerOptions' => [
                'width' => '15%',
            ],
        ],
        [
            'attribute' => 'currency_id',
            'headerOptions' => [
                'width' => '15%',
            ],
            'value' => 'currency.name',
        ],
        [
            'attribute' => 'ks',
            'enableSorting' => false,
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->ks;
            },
            'visible' => !$isForeign,
        ],
        [
            'attribute' => 'rs',
            'label' => $isForeign ? 'Account №' : 'Р/с',
            'enableSorting' => false,
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
            ],
        ],
        [
            'attribute' => 'is_main',
            'enableSorting' => false,
            'headerOptions' => [
                'width' => '5%',
            ],
            'format' => 'boolean',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), $url, [
                        'title' => Yii::t('yii', 'Update'),
                        'class' => 'ajax-modal-btn link',
                        'data-title' => $model->isForeign ? 'Изменить валютный счет' : 'Изменить счет',
                        'data-url' => $url,
                    ]);
                },
                'delete' => function ($url, $model) use ($contractor) {
                    return $this->render('_partial_account_delete', [
                        'model' => $model,
                        'contractor' => $contractor,
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) use ($contractor) {
                return Url::to([
                    "/contractor/account-{$action}",
                    'cid' => $contractor->id,
                    'id' => $model->id,
                    'showModal' => 1
                ]);
            },
            'visible' => $canUpdate,
        ],
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<?php if ($canUpdate) : ?>
    <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить счет</span>', [
        'class' => 'button-regular button-hover-content-red ajax-modal-btn',
        'data-title' => 'Добавить банковский счет',
        'data-url' => Url::to(['/contractor/account-create', 'cid' => $contractor->id, 'showModal' => 1]),
    ]); ?>
    <?php $this->registerJs(<<<JS
        $(document).on('submit', 'form.contractor-account-form', function (e) {
            e.preventDefault();
            var form = $(this);
            $.post(this.action, form.serialize(), function (data) {
                form.closest('.modal').modal('hide');
                Ladda.stopAll();
                $.pjax.reload('#rs-pjax-container', {"push":false,"replace":false,"timeout":5000,"scrollTo":false});
            })
        });
        $(document).on('click', 'button.contractor-account-delete', function (e) {
            e.preventDefault();
            var button = $(this);
            $.post(button.data('url'), button.data('params'), function (data) {
                button.closest('.modal').modal('hide');
                Ladda.stopAll();
                $.pjax.reload('#rs-pjax-container', {"push":false,"replace":false,"timeout":5000,"scrollTo":false});
            })
        });
JS
    ) ?>
<?php endif ?>
