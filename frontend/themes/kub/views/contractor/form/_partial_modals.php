<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use common\models\Contractor;
use frontend\controllers\ContractorController;

/** @var Contractor $model */

Modal::begin([
    'id' => 'modal-payment-delay-update-rule',
    'title' => 'Правило применения новой отсрочки платежа',
    'toggleButton' => false,
    'closeButton' => [
        'label' => frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>

    <ul class="list-clr" role="menu">
        <li style="white-space: nowrap;">
            <?= Html::radio('radio-payment-delay-update-rule', true, [
                'label' => 'Применять ТОЛЬКО к новым счетам',
                'class' => 'radio-payment-delay-update-rule',
                'value' => ContractorController::PAYMENT_DELAY_NO_UPDATE
            ]); ?>
        </li>
        <li style="white-space: nowrap;">
            <?= Html::radio('radio-payment-delay-update-rule', false, [
                'label' => 'Применять к новым счетам + изменить отсрочку по существующим НЕ ОПЛАЧЕННЫМ счетам',
                'class' => 'radio-payment-delay-update-rule',
                'value' => ContractorController::PAYMENT_DELAY_UPDATE_UNPAID
            ]); ?>
        </li>
        <li style="white-space: nowrap;">
            <?= Html::radio('radio-payment-delay-update-rule', false, [
                'label' => 'Применять к новым счетам + изменить отсрочку по ВСЕМ существующим счетам',
                'class' => 'radio-payment-delay-update-rule',
                'value' => ContractorController::PAYMENT_DELAY_UPDATE_ALL
            ]); ?>
        </li>
    </ul>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'ok button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">
            Отменить
        </button>
    </div>

<?php Modal::end() ?>