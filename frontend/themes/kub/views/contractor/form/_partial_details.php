<?php
/* @var $model common\models\Contractor */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$templateDateInput = "<div class='date-picker-wrap'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

$physicalCss = $isPhysical ? '' : 'hidden';
$foreignCss = $isForeign ? '' : 'hidden';
$legalCss = $isPhysical || $isForeign ? 'hidden' : '';
$legalForeignCss = $isPhysical ? 'hidden' : '';
$legalPhysicalCss = $isForeign ? 'hidden' : '';

if (!isset($model->consignee_address_same)) {
    $model->consignee_address_same = 1;
}
if (!isset($model->consignee_kpp_same)) {
    $model->consignee_kpp_same = 1;
}
?>

    <div class="face_type legal foreign mt-3 pt-3 <?=$legalForeignCss?>">
        <div class="row">
            <div class="col-12">
                <div class="face_type legal <?=$legalCss?>">
                    <div class="row">
                        <?php if ($model->company_type_id !== CompanyType::TYPE_IP): ?>
                            <div class="col-4">
                                <?= $form->field($model, 'PPC', $textInputConfig)->textInput([
                                    'maxlength' => false,
                                ]); ?>
                            </div>
                        <?php endif; ?>
                        <div class="col-4">
                            <?= $form->field($model, 'BIN', \yii\helpers\ArrayHelper::merge($textInputConfig, [
                                'enableClientValidation' => false,
                            ]))->textInput([
                                'maxlength' => true,
                            ])->label($model->company_type_id == \common\models\company\CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'); ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'okpo', $textInputConfig)->textInput([
                                'maxlength' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <?= $form->field($model, 'legal_address', $textInputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'actual_address', $textInputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'postal_address', $textInputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
    </div>
    <div class="mt-3">
        <label class="control-label label-width">Для Товарной накладной и УПД</label>
        <div class="row">
            <div class="col-8">
                <?= $form->field($model, 'consignee_address', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => boolval($model->consignee_address_same),
                ]); ?>
            </div>
            <div class="col-4">
                <div><label class="label">&nbsp;</label></div>
                <div class="pt-3">
                    <?= $form->field($model, 'consignee_address_same')->checkbox([
                        'onchange' => "$('#contractor-consignee_address', this.form).attr('disabled', this.checked);"
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <?= $form->field($model, 'consignee_kpp', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => boolval($model->consignee_kpp_same),
                ]); ?>
            </div>
            <div class="col-8">
                <div><label class="label">&nbsp;</label></div>
                <div class="pt-3">
                    <?= $form->field($model, 'consignee_kpp_same')->checkbox([
                        'onchange' => "$('#contractor-consignee_kpp', this.form).attr('disabled', this.checked);"
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="face_type physical mt-3 pt-3 <?=$physicalCss?>">
        <div class="row">
            <div class="col-6">
                <div class="form-group field-contractor-physical_passport_isrf">
                    <?= $form->field($model, 'physical_passport_isRf', array_merge($textInputConfig, ['labelOptions' => ['class' => 'label mb-3']]))
                        ->radioList(['1' => 'РФ', '0' => 'не РФ'], [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return Html::tag('label',
                                    Html::radio($name, $checked, ['value' => $value]) . $label,
                                    [
                                        'class' => 'radio-inline mr-3',
                                    ]);
                            },
                        ]); ?>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group field-physical-passport-country <?= $model->physical_passport_isRf == 1 ? 'hide' : '' ?>">
                    <?= $form->field($model, 'physical_passport_country', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',

                        ],
                    ]))
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group field-contractor-ppc">
                    <?=
                    $form->field($model, 'physical_passport_series', array_merge($textInputConfig, ['options' => ['class' => '']]))
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => ($model->physical_passport_isRf == 1) ? '9{2} 9{2}' : '[9|a| ]{1,25}',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ($model->physical_passport_isRf == 1) ? 'XX XX' : '',
                            ],
                        ]);
                    ?>
                </div>
                <div class="form-group field-contractor-bin">
                    <?=
                    $form->field($model, 'physical_passport_issued_by', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->label('Кем выдан:')->textInput([
                        'maxlength' => false,
                        'data' => [
                            'toggle' => 'popover',
                            'trigger' => 'focus',
                            'placement' => 'bottom',
                        ],
                    ]);
                    ?>
                </div>
                <div class="form-group field-contractor-legal_address">
                    <?=
                    $form->field($model, 'physical_passport_department', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->physical_passport_isRf == 1) ? '9{3}-9{3}' : '[9|a| ]{1,255}' ,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->physical_passport_isRf == 1) ? 'XXX-XXX' : '',
                        ],
                    ]);
                    ?>
                </div>
                <div class="form-group form-contractor-physical_address">
                    <?= $form->field($model, 'physical_address', array_merge($textInputConfig, [
                        'options' => [
                            'class' => 'physical required',
                        ],
                    ]))->label('Адрес регистрации:')->textInput([
                        'maxlength' => true,
                        'data' => [
                            'toggle' => 'popover',
                            'trigger' => 'focus',
                            'placement' => 'bottom',
                        ],
                    ]); ?>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group field-contractor-current_account">
                    <?=
                    $form->field($model, 'physical_passport_number', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->physical_passport_isRf == 1) ? '9{6}' : '[9|a| ]{1,25}',
                        //'regex'=> "[0-9]*",
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->physical_passport_isRf == 1) ? 'XXXXXX' : '',
                        ],
                    ]);
                    ?>
                </div>

                <div class="form-group field-contractor-current_account">
                    <div class="field-contractor-physical_passport_number">
                        <label for="under-date" class="label">Дата выдачи:</label>

                        <div class="w-130">

                            <?= $form->field($model, 'physical_passport_date_output', [
                                'template' => $templateDateInput,
                                'labelOptions' => [
                                    'class' => 'label',
                                ],
                            ])->textInput([
                                'class' => 'form-control form-control_small date-picker',
                                'value' => $model->physical_passport_date_output ?
                                    date('d.m.Y', strtotime($model->physical_passport_date_output)): '',
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row mt-3">
        <?php if ($model->isNewRecord) : ?>
            <div class="col-8">
                <div class="bank_account face_type foreign <?=$foreignCss?>">
                    <?= $form->field($model, 'current_account', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-current_account',
                        'placeholder' => '0123456789',
                        'disabled' => !$isForeign,
                        'maxlength' => '35',
                    ]); ?>

                    <?= $form->field($model, 'BIC', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-bic',
                        'placeholder' => 'ALFABANK',
                        'disabled' => !$isForeign,
                    ])->label('SWIFT'); ?>

                    <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-bank_name',
                        'placeholder' => 'ALFABANK',
                        'disabled' => !$isForeign,
                    ]); ?>
                    <?= $form->field($model, 'bank_address', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-bank_address',
                        'disabled' => !$isForeign,
                        'placeholder' => 'Пример: USA, NY, street, etc',
                    ])->label('Адрес банка'); ?>
                    <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-bank_city',
                        'disabled' => !$isForeign,
                    ]); ?>
                    <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
                        'id' => 'foreign_contractor-corresp_account',
                        'disabled' => !$isForeign,
                    ])->label('Correspondent bank account №'); ?>
                    <?= $form->field($model, 'corr_bank_name')->textInput([
                        'id' => 'foreign_contractor-corr_bank_name',
                        'disabled' => !$isForeign,
                    ]); ?>
                    <?= $form->field($model, 'corr_bank_address')->textInput([
                        'id' => 'foreign_contractor-corr_bank_address',
                        'disabled' => !$isForeign,
                    ]); ?>
                    <?= $form->field($model, 'corr_bank_swift')->textInput([
                        'id' => 'foreign_contractor-corr_bank_swift',
                        'disabled' => !$isForeign,
                    ]); ?>
                </div>
                <div class="bank_account face_type legal physical <?=$legalPhysicalCss?>">
                    <?= $form->field($model, 'current_account', $textInputConfig)->textInput([
                        'placeHolder' => 'Нужен для Актов и Товарных накладных',
                        'autocomplete' => 'off',
                        'maxlength' => '20',
                    ]); ?>

                    <?= $form->field($model, 'BIC', $textInputConfig)->widget(\common\components\widgets\BikTypeahead::classname(), [
                        'remoteUrl' => Url::to(['/dictionary/bik']),
                        'related' => [
                            '#' . Html::getInputId($model, 'bank_name') => 'name',
                            '#' . Html::getInputId($model, 'bank_city') => 'city',
                            '#' . Html::getInputId($model, 'corresp_account') => 'ks',
                        ],
                    ])->textInput([
                        'placeHolder' => 'Нужен для Актов и Товарных накладных',
                        'autocomplete' => 'off'
                    ]); ?>

                    <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                    <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                    <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                    ]); ?>
                </div>
            </div>
        <?php else : ?>
            <div class="col-12">
                <label class="control-label label-width">Банковские реквизиты</label>
                <?= $this->render('_partial_account', [
                    'contractor' => $model,
                    'isForeign' => $isForeign,
                ]) ?>
            </div>
        <?php endif ?>
    </div>

    <script>
        var Passport_isRf = (function () {
            var inputs = {
                'contractor-physical_passport_series': {"mask": "9{2} 9{2}"},
                'contractor-physical_passport_department': {"mask": "9{3}-9{3}"},
                'contractor-physical_passport_number': {"mask": "9{6}"}
            };
            var placeholders = {
                'contractor-physical_passport_series': 'XX XX',
                'contractor-physical_passport_department': 'XXX-XXX',
                'contractor-physical_passport_number': 'XXXXXX'
            };
            var inputsFonNotRf = {
                'contractor-physical_passport_series': {"mask": "[9|a| ]{1,25}"},
                'contractor-physical_passport_department': {"mask": "[9|a]{1,255}"},
                'contractor-physical_passport_number': {"mask": "[9|a]{1,25}"}
            };

            var init = function(){

            };

            var hideInputsMask = function () {
                $.each(inputsFonNotRf, function (o, val) {
                    if ($("#" + o).inputmask) {
                        $("#" + o).inputmask(val);
                        $("#" + o).attr('placeholder','');
                    }
                });
            };

            var showInputsMask = function () {
                $.each(inputs, function (o, val) {
                    var inp = document.getElementById(o);
                    if (inp) {
                        $(inp).inputmask(val);
                        $(inp).attr('placeholder',placeholders[o]);
                    }
                });
            };


            return {hideInputsMask: hideInputsMask, showInputsMask: showInputsMask, init: init}
        })();



        window.addEventListener('load', function () {
            $(".radio-inline").click(function () {
                var radio = $(this).find('input:radio');

                if (radio.length > 0) {
                    radio = radio[0];
                    if (radio.name == 'Contractor[physical_passport_isRf]') {
                        if (radio.value == 0) {
                            $('.field-physical-passport-country').removeClass('hide');
                            Passport_isRf.hideInputsMask();
                        }
                        else {
                            $('.field-physical-passport-country').addClass('hide');
                            Passport_isRf.showInputsMask();
                        }
                    }
                }
            });
        });

    </script>

