<?php

use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorAccount */
/* @var $contractor common\models\Contractor */

?>

<?php Modal::begin([
    'id' => 'contractor-account-delete-modal-' . $model->id,
    'closeButton' => false,
    'options' => [
        'class' => 'confirm-modal fade',
    ],
    'toggleButton' => [
        'tag' => 'a',
        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']),
        'title' => 'Удалить',
        'class' => 'pl-1 link'
    ],
]); ?>

<h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить расчетный счет?</h4>
<div class="text-center">
    <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
        'class' => 'contractor-account-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
        'data' => [
            'style' => 'expand-right',
            'url' => Url::to([
                '/contractor/account-delete',
                'cid' => $contractor->id,
                'id' => $model->id,
            ]),
            'params' => [
                '_csrf' => Yii::$app->request->csrfToken,
            ],
        ],
    ]); ?>
    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
</div>

<?php Modal::end(); ?>
