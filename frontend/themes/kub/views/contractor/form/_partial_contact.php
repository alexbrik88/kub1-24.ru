<?php

use yii\helpers\Html;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $textInputConfig array */

$requiredInputConfig = $textInputConfig;
if (isset($requiredInputConfig['options']['class'])) {
    $requiredInputConfig['options']['class'] .= ' required';
} else {
    $requiredInputConfig['options']['class'] = ' required';
}

?>

<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Контакт</h4>
            <div class="form-group" style="margin-top:35px; margin-bottom: 6px;">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'contact_is_director', [
                        'id' => 'contact_is_director_input',
                        'label' => false,
                    ]); ?>
                    Совпадает с руководителем
                </label>
            </div>
            <?= $form->field($model, 'contact_name', $requiredInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'contact_email', $textInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'contact_phone', $textInputConfig)->textInput([
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'contact_in_act', [
                        'class' => 'fio_in_act',
                        'label' => false,
                        'disabled' => $model->contact_in_act? true: false,
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>
