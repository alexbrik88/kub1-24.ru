<?php
/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $textInputConfig string */

use yii\helpers\Html; ?>

<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Руководитель</h4>
            <?= $form->field($model, 'director_post_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                'id' => 'legal-director_email',
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_phone', $textInputConfig)->textInput([
                'id' => 'legal-director_phone',
            ]); ?>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'director_in_act', [
                        'class' => 'fio_in_act',
                        'label' => false,
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>