<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$requiredInputConfig = $textInputConfig;
if (isset($requiredInputConfig['options']['class'])) {
    $requiredInputConfig['options']['class'] .= ' required';
} else {
    $requiredInputConfig['options']['class'] = ' required';
}
?>

<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Главный бухгалтер</h4>
            <div class="form-group" style="margin-top:35px; margin-bottom: 6px;">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'chief_accountant_is_director', [
                        'id' => 'chief_accountant_is_director_input',
                        'label' => false,
                    ]); ?>
                    Совпадает с руководителем
                </label>
            </div>

            <?= $form->field($model, 'chief_accountant_name', $requiredInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->chief_accountant_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'chief_accountant_email', $textInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->chief_accountant_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'chief_accountant_phone', $textInputConfig)->textInput([
                'disabled' => $model->chief_accountant_is_director? true: false,
            ]); ?>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'chief_accountant_in_act', [
                        'class' => 'fio_in_act',
                        'label' => false,
                        'disabled' => $model->chief_accountant_in_act? true: false,
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>
