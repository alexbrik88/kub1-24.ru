<?php
/* @var $model common\models\Contractor */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\ContractorAgentPaymentType;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$textInputConfig2 = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
        'autocomplete' => 'off'
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$agreementArray = $model->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$agreementDropDownList = ['add-modal-agreement' => \frontend\themes\kub\helpers\Icon::PLUS . ' Добавить договор'] + ArrayHelper::map($agreementArray, 'id', 'listItemName');

$templateDateInput = "<div class='date-picker-wrap' style='width:100%'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>{error}";
/** @var $model \common\models\Contractor */
?>

    <div class="pl-2 pr-2 mt-3 pt-3 legal">
        <div class="pl-1 pr-1">
            <div class="row">
                <div class="col-6">
                    <div class="form-group field-contractor-is_agent">
                        <label class="label" for="contractor-is_agent">Агент</label>
                        <div class="field-width inp_one_line">
                            <?= Html::activeCheckbox($model, 'is_agent', [
                                'id' => 'contractor_is_agent_input',
                                'label' => false,
                            ]); ?>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_payment_type_id">
                        <label class="labe" for="contractor-agent_payment_type_id">Агентский договор</label>
                        <div class="field-width inp_one_line">
                            <?php
                            echo Select2::widget([
                                'model' => $model,
                                'attribute' => 'agent_agreement_id',
                                'data' => $agreementDropDownList,
                                'hideSearch' => true,
                                'options' => [
                                    'class' => 'agent-field',
                                ],
                                'pluginOptions' => [
                                    'placeholder' => '',
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1,
                                    'language' => [],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                ],
                            ]);
                            ?>
                        </div>
                    </div>


                    <div class="form-group field-contractor-agent_payment_percent">
                        <label class="label" for="contractor-agent_payment_percent">Комиссионное вознаграждение</label>
                        <div class="field-width inp_one_line" style="max-width:145px;display:table;">
                            <?= Html::activeTextInput($model, 'agent_payment_percent', [
                                'class' => 'form-control agent-field',
                                'type' => 'number',
                                'maxlength' => true,
                                'min' => 0.01,
                                'max' => 99.99,
                                'step' => '0.01',
                            ]) ?>
                            <span style="display:table-cell;padding-left:10px;vertical-align:middle;">%</span>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_payment_type_id">
                        <label class="label" for="contractor-agent_payment_type_id">Тип платежа</label>
                        <div class="field-width inp_one_line">
                            <?= Select2::widget([
                                'model' => $model,
                                'attribute' => 'agent_payment_type_id',
                                'data' => ArrayHelper::map(ContractorAgentPaymentType::find()->where(['id' => 1])->all(), 'id', 'name'),
                                'options' => [
                                    'class' => 'form-control agent-field',
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ],
                            ]); ?>
                        </div>
                    </div>

                    <div class="form-group field-contractor-agent_start_date">
                        <label class="label" for="contractor-agent_start_date">Дата начала расчетов</label>
                        <div class="field-width inp_one_line" style="max-width:130px">
                            <?= $form->field($model, 'agent_start_date', [
                                'template' => $templateDateInput,
                                'labelOptions' => [
                                    'class' => 'label',
                                ],
                            ])->textInput([
                                'class' => 'form-control form-control_small date-picker agent-field',
                                'value' => DateHelper::format($model->agent_start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                'style' => 'width: 100%'
                            ]) ?>
                        </div>
                    </div>

                </div>
            </div>

            <?= $this->render('_partial_agent_table', [
                 'model' => $model
            ]) ?>

        </div>
    </div>

<?= $this->render('_partial_agent_agreement', [
    'model' => $model
]) ?>

<?php
$this->registerJS(<<<JS
$('#contractor_is_agent_input').on('change', function() {
    if ($(this).prop('checked'))
        $('.agent-field').removeAttr('disabled');
    else
        $('.agent-field').attr('disabled', 'disabled');
});
JS
);

if (!$model->is_agent) {
    $this->registerJS("
    $(document).ready(function () {
        $('.agent-field').attr('disabled', 'disabled');
    });");
}