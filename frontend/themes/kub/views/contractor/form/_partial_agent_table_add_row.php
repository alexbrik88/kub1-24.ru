<?php

use common\components\date\DateHelper;
use frontend\rbac\permissions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\Contractor;
use \common\models\contractor\ContractorAgentBuyer;

/** @var $agent Contractor */

$company = Yii::$app->user->identity->company;
$canAdd = Yii::$app->user->can(permissions\Contractor::CREATE);
$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
];
$agent_ids = Contractor::find()->where(['company_id' => $agent->company_id, 'is_agent' => true])->select('id')->asArray()->column();
$except_buyer_ids = ContractorAgentBuyer::find()->where(['agent_id' => $agent_ids])->select('buyer_id')->asArray()->column();
?>

<tr id="from-new-add-row" class="from-new-add buyer-row" role="row" <?= $hasBuyers ? 'style="display: none;"': ''; ?> >
    <td class="delete-column-left">
        <span class="icon-close remove-buyer from-new"></span>
    </td>
    <td class="">
        <?php echo Select2::widget([
            'id' => 'agent-buyer',
            'name' => 'agentBuyer[0][contractor_id]',
            'data' => ['add-modal-contractor' => \frontend\themes\kub\helpers\Icon::PLUS . ' Добавить покупателя'] + Contractor::getALLContractorList(Contractor::TYPE_CUSTOMER, false, $except_buyer_ids),
            'options' => [
                'class' => 'form-control customer agent-field',
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'width' => '100%',
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            ]
        ]); ?>
    </td>
    <td class="" style="position: relative;">
        <?= Html::textInput('agentBuyer[0][start_date]', date('d.m.Y'), [
            'id' => 'document_date',
            'class' => 'form-control date-picker ico agent-field',
            'style' => 'width:130px',
            'data-date-viewmode' => 'years',
        ]); ?>
    </td>
    <td class="delete-column-right">

    </td>
</tr>