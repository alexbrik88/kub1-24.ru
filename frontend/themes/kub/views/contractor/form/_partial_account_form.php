<?php

use common\components\widgets\BikTypeahead;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorAccount */
/* @var $contractor common\models\Contractor */

$isRuble = $model->currency_id == Currency::DEFAULT_ID;
$isForeign = $model->is_foreign_bank;
$currencyData = Currency::find()->select('name')->orderBy([
    'name' => SORT_ASC,
])->indexBy('id')->column();
$rubCssClass = 'currency_ruble'.($isRuble ? '' : ' hidden');
$curCssClass = 'currency_foreign'.($isRuble ? ' hidden' : '');
$russianCssClass = 'russian_bank'.($isForeign ? ' hidden' : '');
$foreignCssClass = 'foreign_bank'.($isForeign ? '' : ' hidden');
$noMaskCssClass = 'no_mask_wrap'.($isRuble && !$isForeign ? '' : ' hidden');
?>

<?php $form = ActiveForm::begin([
    'action' => [$model->isNewRecord ? 'account-create' : 'account-update', 'cid' => $contractor->id, 'id' => $model->id],
    'options' => [
        'class' => 'contractor-account-form',
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="row">
    <div class="col-6 <?= $rubCssClass ?>">
        <?= $form->field($model, 'rs')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => $model->no_mask ? '9{20}' : '9{5}8109{12}',
            'options' => [
                'class' => 'form-control',
                'disabled' => !$isRuble ? true : null,
            ],
        ]); ?>
    </div>
    <div class="col-6 <?= $curCssClass ?>">
        <?= $form->field($model, 'rs')->textInput([
            'id' => Html::getInputId($model, 'rs').'-foreign',
            'maxlength' => true,
            'disabled' => $isRuble ? true : null,
        ]); ?>
    </div>
    <div class="col-2">
        <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
            'data' => $currencyData,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                //'allowClear' => true,
                'width' => '100%',
            ],
        ]); ?>
    </div>
    <div class="col-4">
        <label class="label">&nbsp;</label>
        <?= $form->field($model, 'is_foreign_bank')->checkbox([
            'class' => '',
        ], true)->label(null, [
            'class' => 'label pt-3',
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-6 <?= $russianCssClass ?>">
        <?= $form->field($model, 'bik')->widget(\common\components\widgets\BikTypeahead::classname(), [
            'remoteUrl' => Url::to(['/dictionary/bik']),
            'related' => [
                '#' . Html::getInputId($model, 'bank_name') => 'name',
                '#' . Html::getInputId($model, 'bank_city') => 'city',
                '#' . Html::getInputId($model, 'ks') => 'ks',
            ],
        ])->textInput([
            'maxlength' => true,
            'autocomplete' => 'off',
            'disabled' => $isForeign ? true : null,
        ])->label('БИК'); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'bik')->textInput([
            'id' => Html::getInputId($model, 'bik').'-foreign',
            'disabled' => !$isForeign ? true : null,
        ])->label('SWIFT'); ?>
    </div>
    <div class="col-6 <?= $russianCssClass ?>">
        <?= $form->field($model, 'ks')->textInput([
            'maxlength' => true,
            'readonly' => !$isForeign ? true : null,
            'disabled' => $isForeign ? true : null,
        ])->label('Корреспондентский счет'); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'ks')->textInput([
            'id' => Html::getInputId($model, 'ks').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ])->label('Счет в банке-корреспонденте / Account №'); ?>
    </div>
</div>
<div class="row">
    <div class="col-6 <?= $russianCssClass ?>">
        <?= $form->field($model, 'bank_name')->textInput([
            'maxlength' => true,
            'readonly' => !$isForeign ? true : null,
            'disabled' => $isForeign ? true : null,
        ]); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'bank_name')->textInput([
            'id' => Html::getInputId($model, 'bank_name').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ]); ?>
    </div>
    <div class="col-6 <?= $russianCssClass ?>">
        <?= $form->field($model, 'bank_city')->textInput([
            'readonly' => true,
            'disabled' => $isForeign ? true : null,
        ]); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'bank_address')->textInput([
            'id' => Html::getInputId($model, 'bank_address').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'corr_bank_swift')->textInput([
            'id' => Html::getInputId($model, 'corr_bank_swift').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ])->label('SWIFT банка-корреспондента / Intermediary Bank'); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'corr_bank_name')->textInput([
            'id' => Html::getInputId($model, 'corr_bank_name').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ])->label('Банк-корреспондент / Intermediary Bank'); ?>
    </div>
    <div class="col-6 <?= $foreignCssClass ?>">
        <?= $form->field($model, 'corr_bank_address')->textInput([
            'id' => Html::getInputId($model, 'corr_bank_address').'-foreign',
            'maxlength' => true,
            'disabled' => !$isForeign ? true : null,
        ])->label('Адрес банка-корреспондента'); ?>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'is_main')->checkbox(['class' => 'uniform-input'], true); ?>
    </div>
    <div class="col-6 <?= $noMaskCssClass ?>">
        <?= $form->field($model, 'no_mask')->checkbox([
            'class' => 'uniform-input',
            'label' => $model->getAttributeLabel('no_mask').' '.Icon::get('question', [
                'class' => 'tooltip-question-icon ml-2',
                'title' => 'Это нужно для казначейских/бюджетных счетов',
            ]),
        ], true) ?>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= \yii\bootstrap4\Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<?= Html::script(
<<<JS
$('input.uniform-input', $('form.contractor-account-form')).uniform();
JS
) ?>