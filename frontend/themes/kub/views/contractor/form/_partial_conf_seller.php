<?php

use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\crm\models\ClientForm;
use frontend\widgets\ContractorGroupDropdownWidget;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */
/* @var $companyIndustries array */
/* @var $salePoints array */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$delayInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$textAreaConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$discountInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
];

$responsibleEmployers = ArrayHelper::map($model->company->getEmployeeCompanies()
    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');

$hasSalePoint = $hasSalePoint ?? false;
$hasCompanyIndustry = $hasCompanyIndustry ?? false;
?>

<div class="mt-3 pt-3">
    <div class="row legal">
        <div class="col-6">
            <?= $form->field($model, 'invoice_expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'exclude' => ['items' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS],
                'options' => [
                    'prompt' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ])->label('Статья расходов'); ?>
            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'contractor-invoice_expenditure_item_id',
            ]) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'seller_payment_delay', $delayInputConfig)->textInput([
                'maxlength' => true,
                'style' => 'width: 100%;',
                'type' => 'number',
                'min' => 0,
            ])->label('Отсрочка оплаты по счету в днях'); ?>
        </div>
    </div>
    <div class="row legal">
        <div class="col-6">
            <?= $form->field($model, 'seller_discount', $discountInputConfig)->input('number', [
                'min' => 0,
                'max' => 99.99,
                'step' => 'any',
                'style' => 'width: 100%;',
            ])->label('Фиксированная скидка на всё, %'); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'seller_guaranty_delay', $delayInputConfig)->textInput([
                'maxlength' => true,
                'style' => 'width: 100%;',
                'type' => 'number',
                'min' => 0,
            ])->label('Отсрочка выполнения обязательств в днях ' . Html::tag('span', Icon::QUESTION, [
                'title' => 'Количество дней на оказание услуги или отгрузку товара после предоплаты (аванса)'
            ])); ?>
        </div>
    </div>

    <div class="row legal">
        <div class="col-6">
            <?= $form->field($model, 'payment_priority', $textInputConfig)->widget(Select2::class, [
                'data' => [
                    Contractor::PAYMENT_PRIORITY_HIGH => '1 - Большой',
                    Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - Средний',
                    Contractor::PAYMENT_PRIORITY_LOW => '3 - Наименьший',
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ])->label('Приоритет в оплате ' . Html::tag('span', Icon::QUESTION, [
                'title' => 'Приоритетность нужна для Платежного Календаря, чтобы в случае кассового разрыва видеть, какие платежи можно сдвинуть, а какие нужно оплачивать.'])); ?>
        </div>
    </div>
    <?php if ($hasCompanyIndustry || $hasSalePoint): ?>
        <div class="row">
            <?php if ($hasCompanyIndustry): ?>
                <div class="col-6">
                    <?= $form->field($model, 'seller_industry_id')->widget(Select2::class, [
                        'model' => $model,
                        'data' => $companyIndustries,
                        'hideSearch' => count($companyIndustries) === 1,
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'pluginOptions' => [
                            'placeholder' => 'Без направления',
                            'width' => '100%',
                            'allowClear' => true,
                        ]
                    ])->label('Направление'); ?>
                </div>
            <?php endif; ?>
            <?php if ($hasSalePoint): ?>
                <div class="col-6">
                    <?= $form->field($model, 'seller_sale_point_id')->widget(Select2::class, [
                        'model' => $model,
                        'data' => $salePoints,
                        'hideSearch' => count($salePoints) === 1,
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'pluginOptions' => [
                            'placeholder' => 'Без точки продаж',
                            'width' => '100%',
                            'allowClear' => true,
                        ]
                    ])->label('Точка продаж'); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="row legal">
        <div class="col-6">
            <?= $form->field($model, 'seller_group_id', [
                'options' => [
                    'class' => 'form-group',
                ],
            ])->widget(ContractorGroupDropdownWidget::classname(), [
                'type' => 1,
            ])->label('Группа поставщика');?>
        </div>
    </div>
</div>
