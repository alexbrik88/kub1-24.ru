<?php

namespace frontend\views\contractor;

use Closure;
use common\models\Contractor;
use frontend\models\ContactForm;
use frontend\modules\crm\models\MessengerTypeList;
use frontend\modules\crm\widgets\ButtonListFactory;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveForm $widget
 * @var ContactForm[] $contactForms
 * @var Contractor $contractor
 * @var array $textInputConfig
 * @var Closure $tooltipInputConfig
 */

?>

<div class="row">

<?php if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON): ?>
    <?php
        $index = array_key_first($contactForms);
        $form = $contactForms[$index];
    ?>

    <div class="hidden">
        <div id="skypeTooltip<?= $index ?>">
            Укажите Skype клиента для связи
        </div>
        <div id="primaryMessengerTypeTooltip<?= $index ?>">
            Укажите мессенджер, в котором клиент предпочитает переписываться
        </div>
        <div id="secondaryMessengerTypeTooltip<?= $index ?>">
            Укажите мессенджер, в котором клиент предпочитает переписываться
        </div>
    </div>

    <div class="col-12 d-flex flex-column">
        <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
            <div class="d-flex flex-column flex-grow-1">
                <h4 class="mb-3 pb-3">Контакт</h4>

                <div class="row">
                    <div class="col-3">
                        <?= $widget->field($form, "[{$index}]primary_phone_number", $textInputConfig)->textInput([
                            'id' => "primary-phone-number-{$index}",
                        ]) ?>
                    </div>

                    <div class="col-3">
                        <?= $widget->field(
                            $form,
                            "[{$index}]primary_messenger_type",
                            $tooltipInputConfig("#primaryMessengerTypeTooltip{$index}")
                        )->widget(Select2::class, [
                            'id' => "primary-messenger-type-{$index}",
                            'data' => (new MessengerTypeList)->getItems(),
                            'pluginOptions' => [
                                'placeholder' => '',
                                'width' => '100%',
                            ],
                        ]) ?>
                    </div>

                    <div class="col-3">
                        <?= $widget->field($form, "[{$index}]email_address", $textInputConfig)->textInput([
                            'id' => "email-address-{$index}",
                        ]) ?>
                    </div>

                    <div class="col-3">
                        <?= $widget->field(
                            $form,
                            "[{$index}]skype_account",
                            $tooltipInputConfig("#skypeTooltip{$index}")
                        )->textInput([
                            'id' => "skype-account-{$index}",
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <?= $widget->field($form, "[{$index}]secondary_phone_number", $textInputConfig)->textInput([
                            'id' => "secondary-phone-number-{$index}",
                        ]) ?>
                    </div>

                    <div class="col-3">
                        <?= $widget->field(
                            $form,
                            "[{$index}]secondary_messenger_type",
                            $tooltipInputConfig("#secondaryMessengerTypeTooltip{$index}")
                        )->widget(Select2::class, [
                            'id' => "secondary-messenger-type-{$index}",
                            'data' => (new MessengerTypeList)->getItems(),
                            'pluginOptions' => [
                                'placeholder' => '',
                                'width' => '100%',
                            ],
                        ]) ?>
                    </div>

                    <div class="col-6">
                        <?= $widget->field($form, "[{$index}]social_account", $textInputConfig)->textInput([
                            'id' => "social-account-type-{$index}",
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
<?php foreach ($contactForms as $index => $form): ?>
    <div class="hidden">
        <div id="skypeTooltip<?= $index ?>">
            Укажите Skype клиента для связи
        </div>
        <div id="primaryMessengerTypeTooltip<?= $index ?>">
            Укажите мессенджер, в котором клиент предпочитает переписываться
        </div>
        <div id="secondaryMessengerTypeTooltip<?= $index ?>">
            Укажите мессенджер, в котором клиент предпочитает переписываться
        </div>
    </div>

    <div class="col-4 d-flex flex-column">
        <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
            <div class="d-flex flex-column flex-grow-1">
                <h4 class="mb-3 pb-3">Контакт <?= ($index + 1) ?></h4>

                <?= $widget->field($form, "[{$index}]contact_default")->radioList(
                    [1 => $form->getAttributeLabel('contact_default')],
                    [
                        'item' => fn ($index, $label, $name, $checked, $value) =>
                            Html::tag('label', Html::radio($name, $checked, ['value' => $value, 'class' => 'js-contact-default']) . $label, [
                                //'class' => 'label',
                                'style' => 'margin-right: 20px;',
                            ]),
                    ]
                )->label(false) ?>

                <?= $widget->field($form, "[{$index}]client_position_id", $textInputConfig)->widget(Select2::class, [
                    'data' => (new ButtonListFactory($this))->createPositionList()->getItems(),
                    'pluginOptions' => [
                        'placeholder' => '',
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
                    ],
                ]) ?>

                <?= $widget->field($form, "[{$index}]contact_name", $textInputConfig)->textInput([
                    'id' => "contact-name-{$index}",
                ]) ?>

                <?= $widget->field($form, "[{$index}]email_address", $textInputConfig)->textInput([
                    'id' => "email-address-{$index}",
                ]) ?>

                <div class="row">
                    <div class="col">
                    <?= $widget->field($form, "[{$index}]primary_phone_number", $textInputConfig)->textInput([
                        'id' => "primary-phone-number-{$index}",
                    ]) ?>
                    </div>

                    <div class="col">
                    <?= $widget->field(
                        $form,
                        "[{$index}]primary_messenger_type",
                        $tooltipInputConfig("#primaryMessengerTypeTooltip{$index}")
                    )->widget(Select2::class, [
                        'id' => "primary-messenger-type-{$index}",
                        'data' => (new MessengerTypeList)->getItems(),
                        'pluginOptions' => [
                            'placeholder' => '',
                            'width' => '100%',
                        ],
                    ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                    <?= $widget->field($form, "[{$index}]secondary_phone_number", $textInputConfig)->textInput([
                        'id' => "secondary-phone-number-{$index}",
                    ]) ?>
                    </div>

                    <div class="col">
                    <?= $widget->field(
                        $form,
                        "[{$index}]secondary_messenger_type",
                        $tooltipInputConfig("#secondaryMessengerTypeTooltip{$index}")
                    )->widget(Select2::class, [
                        'id' => "secondary-messenger-type-{$index}",
                        'data' => (new MessengerTypeList)->getItems(),
                        'pluginOptions' => [
                            'placeholder' => '',
                            'width' => '100%',
                        ],
                    ]) ?>
                    </div>
                </div>
                <?= $widget->field(
                    $form,
                    "[{$index}]skype_account",
                    $tooltipInputConfig("#skypeTooltip{$index}")
                )->textInput([
                    'id' => "skype-account-{$index}",
                ]) ?>

                <?= $widget->field($form, "[{$index}]social_account", $textInputConfig)->textInput([
                    'id' => "social-account-type-{$index}",
                ]) ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>

</div>

<?php

$this->registerJs(<<<JS
    $(document).off('change', '.js-contact-default');
    $(document).on('change', '.js-contact-default', function () {
        const target = $(this);
        const form = target.closest('form').first();
        
        form.find('.js-contact-default').each(function () {
            const element = $(this);
            
            if (!element.is(target)) {
                element.prop('checked', false);
                element.uniform('refresh');
            }
        });
    });
JS);

?>
