<?php

use common\models\Contractor;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $model Contractor */
/* @var $contractorArray Contractor[] */

if ($model->type == Contractor::TYPE_SELLER) {
    $listName = $model->id ? 'Покупателей' : 'Контрагентов';
    $typeName = 'продавца';
} else {
    $listName = $model->id ? 'Продавцов' : 'Контрагентов';
    $typeName = 'покупателя';
}
$count = count($contractorArray);
?>

<div>
    У вас уже есть в списке <?= $listName ?> компании
    <br>
    <?php if ($model->face_type == Contractor::TYPE_LEGAL_PERSON) : ?>
        с таким ИНН <?= $model->ITN ?>
        <?php if ($model->PPC) : ?>
            и КПП <?= $model->PPC ?>
        <?php endif ?>
    <?php else : ?>
        с таким названием <?= $model->name ?>
    <?php endif ?>
</div>

<?php if ($model->id) : ?>
    <div class="mb-3">
        Выберите компанию, с которой объединить вашего <?= $typeName ?>
    </div>
    <?= Html::beginForm([
        '/contractor/merge',
        'type' => $type,
    ]) ?>
        <?= Html::hiddenInput('opposite', $model->opposite) ?>
        <?= Html::hiddenInput('merge_from', $model->id) ?>
        <?php foreach ($contractorArray as $contractor) : ?>
            <?= Html::radio('merge_to', $count == 1, [
                'value' => $contractor->id,
                'label' => implode(' ', [
                    Html::tag('strong', $contractor->getShortTitle()),
                    'Создано '.date('d.m.Y', $contractor->created_at).'.',
                    ($r = $contractor->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
                ]),
            ]) ?>
        <?php endforeach ?>
        <div class="mt-3 d-flex justify-content-between">
            <button type="submit" class="button-regular button-width button-regular_red" data-style="expand-right">Сохранить</button>
            <button type="button" class="button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
        </div>
    <?= Html::endForm(); ?>
<?php else : ?>
    <div class="my-3">
        Перейти в профиль:
    </div>
    <?php foreach ($contractorArray as $contractor) : ?>
        <?php
        $type = $contractor->getIsSellerCustomer() ? $type : $contractor->type;
        if (!in_array($type, [Contractor::TYPE_CUSTOMER, Contractor::TYPE_SELLER])) {
            $type = ($contractor->is_customer || !$contractor->is_seller) ? Contractor::TYPE_CUSTOMER : Contractor::TYPE_SELLER;
        }
        ?>
        <div>
            <?= Html::a(Html::tag('strong', $contractor->getShortTitle()), [
                '/contractor/view',
                'type' => $type,
                'id' => $contractor->id,
            ]) ?>
            <?= implode(' ', [
                'Создано '.date('d.m.Y', $contractor->created_at).'.',
                ($r = $contractor->responsibleEmployeeCompany) ? 'Ответственный '.$r->getShortFio() : null,
            ]) ?>
        </div>
    <?php endforeach ?>
    <div class="mt-3 d-flex flex-row-reverse">
        <button type="button" class="button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php endif ?>