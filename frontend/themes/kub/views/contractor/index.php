<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\Autoinvoice;
use common\models\EmployeeCompany;
use frontend\components\StatisticPeriod;
use frontend\models\ContractorSearch;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;
use yii\bootstrap4\Modal;
use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\components\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\company\CompanyType;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\contractor\ContractorGroup;

/* @var $this yii\web\View */
/* @var $type int */
/* @var $searchModel frontend\models\ContractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */
/* @var $prompt backend\models\Prompt */
/* @var $company Company */
/* @var $user Employee */

$this->title = Contractor::$contractorTitle[$type];


$countContractor = Contractor::find()->where(['and',
    ['employee_id' => Yii::$app->user->identity->id],
    ['type' => $type],
])->count();

$query = clone $dataProvider->query;

$contractorIdArray = $query->column();
if ($searchModel->duplicate) {
    $emptyMessage = 'Поиск дублей по ИНН - дубли не обнаружены';
} else {
    if ($searchModel->name) {
        $emptyMessage = 'Ничего не найдено';
    }
    elseif ($type == Contractor::TYPE_CUSTOMER) {
        $emptyMessage = 'Вы еще не добавили ни одного покупателя';
    } elseif ($type == Contractor::TYPE_SELLER) {
        $emptyMessage = 'Вы еще не добавили ни одного поставщика';
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$user = Yii::$app->user->identity;
$userConfig = $user->config;
$company = $user->company;

$hasFilters = $searchModel->activityStatus != ContractorSearch::ACTIVITY_STATUS_ACTIVE
    || (bool) $searchModel->filterTaxation
    || (bool) $searchModel->filterIsAccounting;
$autoinvoiceFiterItems = array_merge(['' => 'Все', 'no' => 'Нет'], Autoinvoice::$STATUS);
unset($autoinvoiceFiterItems[Autoinvoice::DELETED]);
///////////////////////////////////////////////////////////////////////////////////////////////
$invCount = $company->getInvoiceLeft();
$dateRange = StatisticPeriod::getSessionPeriod();
///////////////////////////////////////////////////////////////////////////////////////////////
$stat1 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::NOT_PAID,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
///////////////////////////////////////////////////////////////////////////////////////////////
$stat2 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::NOT_PAID_IN_TIME,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
///////////////////////////////////////////////////////////////////////////////////////////////
$stat3 = InvoiceStatistic::getStatisticInfo(
    $type,
    $company->id,
    InvoiceStatistic::PAID,
    $dateRange,
    null,
    null,
    null,
    $searchModel
);
///////////////////////////////////////////////////////////////////////////////////////////////
$filterStatusItems = [
    ContractorSearch::ACTIVITY_STATUS_ALL => 'Все',
    ContractorSearch::ACTIVITY_STATUS_ACTIVE => 'Активные',
    ContractorSearch::ACTIVITY_STATUS_INACTIVE => 'Неактивные',
];
///////////////////////////////////////////////////////////////////////////////////////////////
$filterTaxationItems = [
    ContractorSearch::TAXATION_ALL => 'Все',
    ContractorSearch::TAXATION_NDS => 'С НДС',
    ContractorSearch::TAXATION_NO_NDS => 'Без НДС'
];
///////////////////////////////////////////////////////////////////////////////////////////////
$fieldGroup = [1=>"seller_group_id",2=>"customer_group_id"];
$filterGroup = [0 => 'Все'] + ArrayHelper::map(
        ContractorGroup::find()->where(['or',
            [
                'and',
                ['type' => $type],
                ['company_id' => $company],
            ],
            [
                'and',
                ['type' => ContractorGroup::TYPE_WITHOUT],
                ['id' => ContractorGroup::ID_WITHOUT],
            ],
        ])->all(), 'id', 'title');
///////////////////////////////////////////////////////////////////////////////////////////////
$filterFormItems = ['' => 'Все'] + ArrayHelper::map(
        CompanyType::find()->where([
            'id' => Contractor::find()
                ->select('company_type_id')
                ->distinct()->where([
                    'company_id' => $company->id,
                    'type' => $type
                ])->column()
        ])->all(), 'id', 'name_short');
///////////////////////////////////////////////////////////////////////////////////////////////
$filterIsAccountingItems = [
    ContractorSearch::NO_ACCOUNTING_ALL => 'Все',
    ContractorSearch::NO_ACCOUNTING_YES => 'Да',
    ContractorSearch::NO_ACCOUNTING_NO => 'Нет'
];
///////////////////////////////////////////////////////////////////////////////////////////////
$tabViewClass = $userConfig->getTableViewClass('table_view_contractor_list');
$canViewBalanceByDocuments = $searchModel::canViewBalanceByDocuments();
///////////////////////////////////////////////////////////////////////////////////////////////
yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip_in_modal',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub', 'tooltipster-in-modal'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
])
?>

<div class="stop-zone-for-fixed-elems contractor-index">

    <?php /*
    <?php if (!$company->getCashBankStatementUploads()->exists() && Yii::$app->user->identity->showAlert) : ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×
            </button>
            <a href="<?= Url::to(['cash/bank/index']) ?>">Загрузите выписку</a>
            из банка и все имеющиеся у вас Покупатели и Поставщики будут созданы автоматически.
        </div>
        <?php $this->registerJs('
            $(document).on("click", "#contractor_alert_close", function() {
                $.ajax({"url": "' . Url::to(['alert-close']) . '"});
            });
        '); ?>
    <?php endif; ?> */ ?>

    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if (\frontend\rbac\permissions\Contractor::CREATE): ?>
        <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
            '/contractor/create',
            'type' => $type,
        ]) ?>">
            <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
            <span>Добавить</span>
        </a>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="count-card wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено ВСЕГО</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat1['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_red wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено в срок</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat2['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_green wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Оплачено</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat3['count'] ?></div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <?php /* todo
                <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) &&
                    in_array($user->currentEmployeeCompany->employee_role_id, [
                        EmployeeRole::ROLE_CHIEF,
                        EmployeeRole::ROLE_SUPERVISOR
                    ])
                ): ?>

                    <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                        'class' => 'button-regular w-100 button-hover-content-red'
                    ]); ?>
                <?php endif; ?>*/ ?>
                <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
                    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php else : ?>
                    <?php /*
                    <?= Html::a('Увеличение онлайн продаж', ['/contractor/sale-increase'], [
                        'class' => 'employee-click_trigger button-regular w-100 button-hover-content-red',
                        'data-url' => Url::to(['/site/employee-click', 'type' => EmployeeClick::SALE_INCREASE_TYPE])
                    ]) */ ?>
                <?php endif ?>
                <?php if ($type == Documents::IO_TYPE_OUT) { ?>
                    <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) && in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])) { ?>
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                            'class' => 'button-regular w-100 button-hover-content-red',
                        ]); ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="table-settings row row_indents_s">
        <div class="col-3">
            <?= TableConfigWidget::widget([
                'items' => array_filter([
                    [
                        'attribute' => 'contractor_contact',
                    ],
                    [
                        'attribute' => 'contractor_group_name',
                    ],
                    [
                        'attribute' => 'contractor_phone',
                    ],
                    [
                        'attribute' => 'contractor_agreement',
                    ],
                    [
                        'attribute' => 'contractor_payment_delay',
                    ],
                    [
                        'attribute' => ($type == Contractor::TYPE_CUSTOMER)
                            ? 'contractor_income_item'
                            : 'contractor_expenditure_item',
                    ],
                    [
                        'attribute' => 'contractor_industry',
                    ],
                    [
                        'attribute' => 'contractor_sale_point',
                    ],
                    [
                        'attribute' => 'contractor_notpaidcount',
                    ],
                    [
                        'attribute' => 'contractor_notpaidsum',
                    ],
                    [
                        'attribute' => 'contractor_prepaymentsum',
                    ],
                    [
                        'attribute' => 'contractor_balancesum',
                    ],
                    [
                        'attribute' => 'contractor_docs_balancesum',
                        'need_pay' => !$canViewBalanceByDocuments,
                        'need_pay_modal' => '#modal_contractor_docs_balancesum'
                    ],
                    [
                        'attribute' => 'contractor_paidcount',
                    ],
                    [
                        'attribute' => 'contractor_paidsum',
                    ],
                    [
                        'attribute' => 'contractor_new_invoice',
                    ],
                    [
                        'attribute' => $type == Contractor::TYPE_CUSTOMER ? 'contractor_autionvoice' : null,
                    ],
                    [
                        'attribute' => 'contractor_responsible',
                    ],
                ]),
            ]); ?>
            <?= Html::a(
                '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                Url::current(['xls' => 1]),
                [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                    'title' => 'Скачать в Excel',
                ]
            ); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_contractor_list']) ?>
        </div>
        <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
            <?= Html::a('Дубли', Url::current([
                'ContractorSearch' => [
                    'duplicate' => $searchModel->duplicate ? null : 1,
                ],
            ]), [
                'class' => 'button-regular button-clr duplicate-contractor' . (
                    $searchModel->duplicate ? ' active button-regular_red' : ' button-hover-transparent'
                ),
                'title' => 'Показать дубли ' . ($type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков')
            ]); ?>
        </div>
        <div class="col-6">
            <?= Html::beginForm(Url::current(), 'get', [
                'id' => 'contractor-filter-form',
                'class' => 'd-flex flex-nowrap align-items-center',
            ]); ?>

            <!-- filter -->
            <div class="dropdown popup-dropdown popup-dropdown_filter products-filter <?= $hasFilters ? 'itemsSelected' : '' ?>">
                <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="button-txt">Фильтр</span>
                    <svg class="svg-icon svg-icon-shevron">
                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                    </svg>
                </button>
                <div class="dropdown-menu keep-open" aria-labelledby="filter">
                    <div class="popup-dropdown-in p-3">
                        <div class="p-1">
                            <div class="row">
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown1" data-default="<?= ContractorSearch::ACTIVITY_STATUS_ACTIVE ?>">
                                        <div class="label">Статус</div>
                                        <?= Html::activeHiddenInput($searchModel, 'activityStatus') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                                    <span class="drop-title">
                                                        <?= \yii\helpers\ArrayHelper::getValue($filterStatusItems, (int)$searchModel->activityStatus) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown1">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterStatusItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->activityStatus) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown2">
                                        <div class="label">Налогообложение</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterTaxation') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterTaxationItems, (int)$searchModel->filterTaxation) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown2">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterTaxationItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterTaxation) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown3">
                                        <div class="label">Форма</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterForm') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                    <span class="drop-title">
                                                        <?= \yii\helpers\ArrayHelper::getValue($filterFormItems, $searchModel->filterForm) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown3">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterFormItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterForm) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6 mb-3">
                                    <div class="dropdown-drop" data-id="dropdown4">
                                        <div class="label">Не для бухгалтерии</div>
                                        <?= Html::activeHiddenInput($searchModel, 'filterIsAccounting') ?>
                                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown4">
                                                    <span class="drop-title">
                                                        <?= ArrayHelper::getValue($filterIsAccountingItems, (int)$searchModel->filterIsAccounting) ?>
                                                    </span>
                                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                            </svg>
                                        </a>
                                        <div class="dropdown-drop-menu" data-id="dropdown4">
                                            <div class="dropdown-drop-in">
                                                <ul class="form-filter-list list-clr">
                                                    <?php foreach ($filterIsAccountingItems as $key=>$item): ?>
                                                        <li>
                                                            <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterIsAccounting) ?>"><?= $item ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mt-3">
                                    <div class="row justify-content-between">
                                        <div class="form-group column">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                <span>Применить</span>
                                            </button>
                                        </div>
                                        <div class="form-group column">
                                            <button id="product_filters_reset" class="button-regular button-hover-content-red button-width-medium button-clr" data-clear="dropdown" type="button">
                                                <span>Сбросить</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group flex-grow-1 ml-2 mr-2">
                <?= Html::activeTextInput($searchModel, 'name', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list' . $tabViewClass,
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '1%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::checkbox('Contractor[' . $data->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                                'data-industry' => ($data->type == Contractor::TYPE_CUSTOMER) ? $data->customer_industry_id : $data->seller_industry_id,
                                'data-sale_point' => ($data->type == Contractor::TYPE_CUSTOMER) ? $data->customer_sale_point_id : $data->seller_sale_point_id,
                                'data-project' => null,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'contractor-cell-main'
                        ],
                        'format' => 'raw',
                        'value' => function ($data) use ($type) {
                            return Html::a(Html::encode($data->getTitle(true)), [
                                'contractor/view',
                                'type' => $type,
                                'id' => $data->id,
                            ]);
                        },
                    ],


                    [
                        'attribute' => 'group_name',
                        'label' => 'Группа',
                        'headerOptions' => [
                            'class' => 'col_contractor_group_name' . ($userConfig->contractor_group_name ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'contact-cell col_contractor_group_name' . ($userConfig->contractor_group_name ? '' : ' hidden'),
                        ],
                        //'filter' => $searchModel->getContactItemsByQuery($dataProvider->query),
                        'filter' => $filterGroup,

                        'format' => 'raw',
                        /*'value' => function ($data) {
                            return $data->realContactName ?: '';
                        },*/
                        's2width' => '250px'
                    ],



                    [
                        'attribute' => 'contact',
                        'label' => 'Контакт',
                        'headerOptions' => [
                            'class' => 'col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'contact-cell col_contractor_contact' . ($userConfig->contractor_contact ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getContactItemsByQuery($dataProvider->query),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->realContactName ?: '';
                        },
                        's2width' => '250px'
                    ],
                    [
                        'attribute' => 'phone',
                        'label' => 'Телефон',
                        'headerOptions' => [
                            'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_phone' . ($userConfig->contractor_phone ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->realContactPhone ?: '';
                        },
                    ],
                    [
                        'attribute' => 'agree',
                        'label' => 'Договор',
                        'headerOptions' => [
                            'class' => 'col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => 'agreement-cell col_contractor_agreement' . ($userConfig->contractor_agreement ? '' : ' hidden'),
                        ],
                        'format' => 'html',
                        'value' => 'lastAgreementStr',
                    ],
                    [
                        'attribute' => 'payment_delay',
                        'label' => 'Отсрочка<br/>дн.',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_payment_delay' . ($userConfig->contractor_payment_delay ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_payment_delay' . ($userConfig->contractor_payment_delay ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) use ($type) {
                            $delay = $type == Contractor::TYPE_CUSTOMER ? $data->customer_payment_delay : $data->seller_payment_delay;

                            return TextHelper::numberFormat($delay);
                        },
                    ],
                    [
                        'attribute' => ($type == Contractor::TYPE_CUSTOMER) ? 'invoice_income_item_id' : 'invoice_expenditure_item_id',
                        'label' => ($type == Contractor::TYPE_CUSTOMER) ? 'Статья приходов' : 'Статья расходов',
                        'headerOptions' => [
                            'class' => ($type == Contractor::TYPE_CUSTOMER)
                                ? 'agreement-cell col_contractor_income_item' . ($userConfig->contractor_income_item ? '' : ' hidden')
                                : 'agreement-cell col_contractor_expenditure_item' . ($userConfig->contractor_expenditure_item ? '' : ' hidden'),
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'class' => ($type == Contractor::TYPE_CUSTOMER)
                                ? 'agreement-cell col_contractor_income_item' . ($userConfig->contractor_income_item ? '' : ' hidden')
                                : 'agreement-cell col_contractor_expenditure_item' . ($userConfig->contractor_expenditure_item ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getArticleItemsByQuery($type, $dataProvider->query),
                        's2width' => '300px',
                        'format' => 'html',
                        'value' => function (Contractor $data) use ($type) {

                            if ($data->isKub()) {
                                $data = $data->getKubItem();
                            }

                            return ($type == Contractor::TYPE_CUSTOMER)
                                ? ($data->income ? $data->income->name : '---')
                                : ($data->expenditure ? $data->expenditure->name : '---');
                        },
                    ],
                    [
                        'attribute' => ($type == Contractor::TYPE_CUSTOMER) ? 'customer_industry_id' : 'seller_industry_id',
                        'label' => 'Направление',
                        'headerOptions' => [
                            'class' => 'col_contractor_industry' . ($userConfig->contractor_industry ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_industry' . ($userConfig->contractor_industry ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getIndustryItemsByQuery($type, $dataProvider->query),
                        'hideSearch' => false,
                        's2width' => '300px',
                        'format' => 'raw',
                        'value' => function (Contractor $data) use ($type) {
                            $industry = $data->getIndustry($type)->one();
                            return ($industry) ? Html::encode($industry->name) : '';
                        },
                    ],
                    [
                        'attribute' => ($type == Contractor::TYPE_CUSTOMER) ? 'customer_sale_point_id' : 'seller_sale_point_id',
                        'label' => 'Точка продаж',
                        'headerOptions' => [
                            'class' => 'col_contractor_sale_point' . ($userConfig->contractor_sale_point ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_sale_point' . ($userConfig->contractor_sale_point ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getSalePointItemsByQuery($type, $dataProvider->query),
                        'hideSearch' => false,
                        's2width' => '300px',
                        'format' => 'raw',
                        'value' => function (Contractor $data) use ($type) {
                            $salePoint = $data->getSalePoint($type)->one();
                            return ($salePoint) ? Html::encode($salePoint->name) : '';
                        },
                    ],                    
                    [
                        'attribute' => 'not_paid_count',
                        'label' => 'Счетов<br>не<br>оплачено',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_notpaidcount' . ($userConfig->contractor_notpaidcount ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::moneyFormat($data->not_paid_count);
                        },
                    ],
                    [
                        'attribute' => 'not_paid_sum',
                        'label' => 'Не оплачено счетов на сумму',
                        'headerOptions' => [
                            'class' => 'col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell col_contractor_notpaidsum' . ($userConfig->contractor_notpaidsum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->not_paid_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'prepayment_sum',
                        'label' => 'Оплата без привязки',
                        'headerOptions' => [
                            'class' => 'col_contractor_prepaymentsum' . ($userConfig->contractor_prepaymentsum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell col_contractor_prepaymentsum' . ($userConfig->contractor_prepaymentsum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->prepayment_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'balance_sum',
                        'label' => 'Сальдо по<br>счетам',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_balancesum' . ($userConfig->contractor_balancesum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell col_contractor_balancesum' . ($userConfig->contractor_balancesum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->balance_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'balance_docs_sum',
                        'label' => 'Сальдо по<br>Актам/ТН/УПД',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_docs_balancesum' . ($userConfig->contractor_docs_balancesum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell col_contractor_docs_balancesum' . ($userConfig->contractor_docs_balancesum ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $data) use ($canViewBalanceByDocuments) {
                            return ($canViewBalanceByDocuments)
                                ? TextHelper::invoiceMoneyFormat($data->balance_docs_sum, 2)
                                : Html::tag('span', 'нет данных', [
                                    'title' => 'Доступно только на тарифах ФинДиректор, <br/>ФинДиректор PRO, ФинДиректор Все включено',
                                    'title-as-html' => 1,
                                    'style' => "color:#CCC"
                                ]);
                        },
                    ],
                    [
                        'attribute' => 'paid_count',
                        'label' => 'Счетов оплачено',
                        'headerOptions' => [
                            'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_paidcount' . ($userConfig->contractor_paidcount ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::moneyFormat($data->paid_count);
                        },
                    ],
                    [
                        'attribute' => 'paid_sum',
                        'label' => 'Оплачено счетов на сумму',
                        'headerOptions' => [
                            'class' => 'col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:120px'
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell col_contractor_paidsum' . ($userConfig->contractor_paidsum ? '' : ' hidden'),
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->paid_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'autoinvoice',
                        'label' => 'АвтоСчет',
                        'headerOptions' => [
                            'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                            'width' => '5%',
                            'style' => 'max-width:80px'
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_autionvoice' . ($userConfig->contractor_autionvoice ? '' : ' hidden'),
                        ],
                        'filter' => $autoinvoiceFiterItems,
                        'value' => function (Contractor $data) {
                            return ArrayHelper::getValue(Autoinvoice::$STATUS, $data->autoinvoice, '');
                        },
                        'visible' => $type == Contractor::TYPE_CUSTOMER,
                        's2width' => '200px'
                    ],
                    ($searchModel->duplicate) ?
                        [
                            'header' => 'Дубли',
                            'headerOptions' => [
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                                'style' => 'max-width:120px; color:#f3565d;',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $data) use ($searchModel) {
                                return $data->getDuplicateDifference();
                            },
                        ] :
                        [
                            'header' => 'Выставить счет',
                            'headerOptions' => [
                                'width' => '1%',
                                'class' => 'col_contractor_new_invoice' . ($userConfig->contractor_new_invoice ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_contractor_new_invoice' . ($userConfig->contractor_new_invoice ? '' : ' hidden'),
                                'style' => 'max-width:120px; padding-right:10px;',
                            ],
                            'format' => 'raw',
                            'value' => function (Contractor $data) use ($searchModel) {
                                if ($data->company_id !== null) {
                                    return Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span class="pl-1">Счёт</span>', [
                                        'documents/invoice/create',
                                        'type' => $data->type,
                                        'contractorId' => $data->id,
                                    ], [
                                        'class' => 'button-regular button-hover-content-red',
                                        'style' => 'width:81px;'
                                    ]);
                                }

                                return '';
                            },
                            'visible' => Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                'ioType' => $type, // same value
                            ]),

                        ],
                    [
                        'attribute' => 'responsible_employee_id',
                        'label' => 'От&shy;вет&shy;ствен&shy;ный',
                        'encodeLabel' => false,
                        'headerOptions' => [
                            'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_contractor_responsible' . ($userConfig->contractor_responsible ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getResponsibleItemsByQuery($dataProvider->query),
                        'value' => function (Contractor $data) use ($company) {
                            $employee = EmployeeCompany::findOne([
                                'employee_id' => $data->responsible_employee_id,
                                'company_id' => $company->id,
                            ]);

                            return $employee ? $employee->getFio(true) : '';
                        },
                        's2width' => '250px'
                    ],
                ],
            ]); ?>

    <?php /* if ($countContractor == 0) : ?>
        <?= \frontend\widgets\PromptWidget::widget([
            'prompt' => $prompt,
        ]); ?>
    <?php endif; */ ?>

</div>

<?= \frontend\themes\kub\widgets\SummarySelectContractorWidget::widget([
    'buttons' => [
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-width button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => array_filter([
                    [
                        'label' => 'Ответственный',
                        'url' => '#change_responsible_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Статья ' . ($type == Contractor::TYPE_SELLER ? 'расхода' : 'прихода'),
                        'url' => '#change_article_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Отсрочка платежа',
                        'url' => '#change_payment_delay_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Направление',
                        'url' => '#many-company-industry',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Точка продаж',
                        'url' => '#many-sale-point',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    ($type == Contractor::TYPE_SELLER) ? [
                        'label' => 'Приоритет в оплате',
                        'url' => '#change_payment_priority_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ] : []
                ]),
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align']),
    ],
]); ?>


<?php Modal::begin([
    'title' => false,
    'id' => 'change_responsible_modal',
]); ?>
<h4 class="modal-title">Изменить ответственного</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Ответственный
            </label>
            <?= Select2::widget([
                'id' => 'contractor-responsible_employee',
                'name' => 'responsibleEmployee',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => ArrayHelper::map($company->getEmployeeCompanies()
                    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                    ->orderBy([
                        'lastname' => SORT_ASC,
                        'firstname' => SORT_ASC,
                        'patronymic' => SORT_ASC,
                    ])->all(), 'employee_id', 'fio'),
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-responsible button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/contractor/change-responsible', 'type' => $type]),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => false,
    'id' => 'change_article_modal',
]); ?>
<h4 class="modal-title">Изменить статью <?= ($type == Contractor::TYPE_SELLER ? 'расхода' : 'прихода') ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Статья <?= ($type == Contractor::TYPE_SELLER ? 'расхода' : 'прихода') ?>
            </label>
            <?= ExpenditureDropdownWidget::widget([
                'id' => 'contractor-article',
                'name' => 'article',
                'loadAssets' => false,
                'income' => !($type == Contractor::TYPE_SELLER),
                'exclude' => ['items' => ($type == Contractor::TYPE_SELLER) ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : InvoiceIncomeItem::ITEM_OWN_FOUNDS],
                'options' => [
                    'prompt' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'contractor-article',
                'type' => ($type != Contractor::TYPE_SELLER) ? 'income' : 'expenditure',
            ]) ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-article button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/contractor/change-article', 'type' => $type]),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => false,
    'id' => 'change_status_modal',
]); ?>
<h4 class="modal-title">Изменить статус</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">

    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Статус
            </label>
            <?= Html::radioList('status', Contractor::ACTIVE, [
                Contractor::ACTIVE => 'Активный',
                ContractorSearch::INACTIVE => 'Не активный',
            ], [
                'id' => 'contractor-status',
            ]); ?>
        </div>
    </div>

</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/contractor/change-status', 'type' => $type]),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => false,
    'id' => 'change_payment_delay_modal',
]); ?>
    <h4 class="modal-title">Изменить отсрочку платежа</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Отсрочка платежа в днях
                </label>
                <?= Html::input('number', 'paymentDelay', '', [
                    'id' => 'contractor-payment_delay',
                    'class' => 'form-control',
                    'max' => 365,
                    'min' => 0,
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-payment-delay button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/contractor/change-payment-delay', 'type' => $type]),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => false,
    'id' => 'change_payment_priority_modal',
]); ?>
    <h4 class="modal-title">Изменить приоритет в оплате</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных <?= $type == Contractor::TYPE_CUSTOMER ? 'покупателей' : 'поставщиков'; ?> изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Приоритет в оплате
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip_in_modal',
                        'data-tooltip-content' => '#tooltip_in_modal_payment_priority',
                    ]) ?>
                </label>
                <?= Select2::widget([
                    'id' => 'contractor-payment_priority',
                    'data' => [
                        Contractor::PAYMENT_PRIORITY_HIGH => '1 - Большой',
                        Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - Средний',
                        Contractor::PAYMENT_PRIORITY_LOW => '3 - Наименьший',
                    ],
                    'hideSearch' => true,
                    'name' => 'paymentPriority',
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => 'Выберите приоритет',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-payment-priority button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/contractor/change-payment-priority', 'type' => $type]),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
    <div style="display: none">
        <div id="tooltip_in_modal_payment_priority">
            Приоритетность нужна для Платежного Календаря, чтобы в случае кассового разрыва<br/> видеть, какие платежи можно сдвинуть, а какие нужно оплачивать.
        </div>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => null,

    'id' => 'modal_contractor_docs_balancesum',
    'toggleButton' => false,
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ],
]); ?>
    <h4 class="modal-title text-center">
        Данная опция доступна только на тарифах<br/>
        ФинДиректор, ФинДиректор PRO, ФинДиректор Все включено.
    </h4>
    <h4 class="modal-title text-center">
        Перейти к оплате ?
    </h4>
    <div class="form-body"></div>
    <div class="mt-3 d-flex justify-content-center">
        <a href="<?= Url::to(['/subscribe']) ?>" class="button-clr button-regular button-hover-transparent button-width-medium mr-2">Да</a>
        <button type="button" class="button-clr button-regular button-hover-transparent button-width-medium ml-2" data-dismiss="modal">Нет</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-company-industry',
]); ?>
    <h4 class="modal-title mb-4">Изменить направление</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных операций изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Направление
                </label>
                <?= \kartik\widgets\Select2::widget([
                    'id' => 'contractor-many-company-industry',
                    'name' => 'industry',
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'data' => CompanyIndustry::getSelect2Data(),
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= \common\components\helpers\Html::submitButton('<span class="ladda-label pr-3">Сохранить</span>', [
            'class' => 'modal-many-change-company-industry button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['change-industry', 'type' => $type]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-sale-point',
]); ?>
    <h4 class="modal-title mb-4">Изменить точку продаж</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных операций изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Точка продаж
                </label>
                <?= \kartik\widgets\Select2::widget([
                    'id' => 'contractor-many-sale-point',
                    'name' => 'sale_point',
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'data' => SalePoint::getSelect2Data(),
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label pr-3">Сохранить</span>', [
            'class' => 'modal-many-change-sale-point button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['change-sale-point', 'type' => $type]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php /* todo
<?php if ($company->show_popup_sale_increase && $type == Contractor::TYPE_CUSTOMER): ?>
    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Повышение продаж
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_BILL,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 6,
            'description' => 'ПРОДАВАЙТЕ БОЛЬШЕ без УЧАСТИЯ ПРОДАВЦОВ<br>
                    Ваш сайт сможет самостоятельно выставлять счета покупателям.<br>
                    Создайте личные ОНЛАЙН-КАБИНЕТЫ для покупателей с доступом к складу.',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/contractor/sale-increase']),
            'image' => ImageHelper::getThumb('img/modal_registration/block-6.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => 5,
            'nextModal' => null,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
*/ ?>

<?php $this->registerJs('

    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });

    // SEARCH
    $("input#contractorsearch-name").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $("#contractor-filter-form").submit();
      }
    });

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }

    $("#contractor-filter-form").find(".filter-item").on("click", function(e) {
        e.preventDefault();
            var pop =  $(this).parents(".popup-dropdown_filter");
            var drop = $(this).parents(".dropdown-drop");
            var value = $(this).data("id");
            $(drop).find("input").val(value);
            $(drop).find(".drop-title").html($(this).text());
            $(drop).find(".dropdown-drop-menu").removeClass("visible show");

            refreshProductFilters();
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");

            if (drop.attr("data-default")) {
                a_first = $(drop).find("li").eq(drop.attr("data-default")).find("a");
            }

            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

    // MANY INDUSTRY MODAL
    $(document).on("show.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {

        let defaultVal = null;
        let defaults = {industry: [], sale_point: [], project: []};
        const unique = function(array){
            return array.filter(function(el, index, arr) {
                return index === arr.indexOf(el);
            });
        };

        $(".joint-operation-checkbox:checked").each(function(i,v) {
            defaults.industry.push($(v).data("industry") || 0);
            defaults.sale_point.push($(v).data("sale_point") || 0);
            defaults.project.push($(v).data("project") || 0);
        });
        
        switch ($(this).attr("id")) {
            case "many-company-industry":
                defaultVal = (unique(defaults.industry).length === 1) ? defaults.industry[0] : 0;
                break;
            case "many-sale-point":
                defaultVal = (unique(defaults.sale_point).length === 1) ? defaults.sale_point[0] : 0;
                break;
            case "many-project":
                defaultVal = (unique(defaults.project).length === 1) ? defaults.project[0] : 0;
                break;
        }

        $(this).find("select").val(defaultVal).trigger("change");
    });

    $(document).on("hidden.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {
        $(this).find("select").val(null).trigger("change");
    });    

') ?>