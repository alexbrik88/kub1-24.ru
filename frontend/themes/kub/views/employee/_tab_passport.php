<?php
/* @var $model \common\models\EmployeeCompany */
use common\components\date\DateHelper;
use common\models\address\Country;
use common\models\company\CompanyType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{input}\n{error}",
];

if ($model->passport_isRf === null) {
    $model->passport_isRf = 1;
}

$templateDateInput = "<div class='date-picker-wrap' style='width:100%'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>";
$templateDateInputTop = "<div class='date-picker-wrap date-picker-wrap-top' style='width:100%'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>{error}";
?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="col-3">
                    <div class="label">Паспорт</div>
                    <?= $form->field($model, 'passport_isRf', array_merge($textInputConfig, ['options' => ['class' => '']]))
                        ->radioList(['1' => 'РФ', '0' => 'не РФ'], [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return Html::tag('label',
                                    Html::radio($name, $checked, ['value' => $value]) . $label,
                                    [
                                        'class' => 'radio-inline pr-3 radio-padding',
                                    ]);
                            },
                        ]); ?>
                </div>
                <div class="col-3 field-physical-passport-country <?= $model->passport_isRf == 1 ? 'hide' : '' ?>">
                    <div class="label">Страна</div>
                    <?= $form->field($model, 'passport_country')
                        ->widget(Select2::class, [
                            'data' => ArrayHelper::map(Country::getCountries(), 'name_short', 'name_short'),
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="label">Серия</div>
                    <?=
                    $form->field($model, 'passport_series', array_merge($textInputConfig, ['options' => ['class' => '']]))
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => ($model->passport_isRf == 1) ? '9{2} 9{2}' : '[9|a| ]{1,25}',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ($model->passport_isRf == 1) ? 'XX XX' : '',
                            ],
                        ]);
                    ?>
                </div>
                <div class="col-3">
                    <div class="label">Номер</div>
                    <?=
                    $form->field($model, 'passport_number', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->passport_isRf == 1) ? '9{6}' : '[9|a| ]{1,25}',
                        //'regex'=> "[0-9]*",
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->passport_isRf == 1) ? 'XXXXXX' : '',
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-3">
                    <div class="label">Дата выдачи</div>
                    <?= $form->field($model, 'passport_date_output', [
                        'template' => $templateDateInputTop,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control form-control_small date-picker',
                        'style' => 'width:100%',
                        'value' => DateHelper::format($model->passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'data-position' => 'top left'
                    ]) ?>
                </div>
            </div>
            <div class="row mb-3 pb-3">
                <div class="col-3">
                    <div class="label">Кем выдан</div>
                    <?=
                    $form->field($model, 'passport_issued_by', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->label(false)->textInput([
                        'maxlength' => false,
                    ]);
                    ?>
                </div>
                <div class="col-3">
                    <div class="label">Код подразделения</div>
                    <?=
                    $form->field($model, 'passport_department', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->passport_isRf == 1) ? '9{3}-9{3}' : '[9|a| ]{1,255}' ,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->passport_isRf == 1) ? 'XXX-XXX' : '',
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-3">
                    <div class="label">Адрес регистрации</div>
                    <?= $form->field($model, 'passport_address', array_merge($textInputConfig, [
                        'options' => [
                            'class' => 'required',
                        ],
                    ]))->label(false)->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var Passport_isRf = (function () {
        var inputs = {
            'employeecompany-passport_series': {"mask": "9{2} 9{2}"},
            'employeecompany-passport_department': {"mask": "9{3}-9{3}"},
            'employeecompany-passport_number': {"mask": "9{6}"}
        };
        var placeholders = {
            'employeecompany-passport_series': 'XX XX',
            'employeecompany-passport_department': 'XXX-XXX',
            'employeecompany-passport_number': 'XXXXXX'
        };
        var inputsFonNotRf = {
            'employeecompany-passport_series': {"mask": "[9|a| ]{1,25}"},
            'employeecompany-passport_department': {"mask": "[9|a]{1,255}"},
            'employeecompany-passport_number': {"mask": "[9|a]{1,25}"}
        };

        var init = function(){

        };

        var hideInputsMask = function () {
            $.each(inputsFonNotRf, function (o, val) {
                if ($("#" + o).inputmask) {
                    $("#" + o).inputmask(val);
                    $("#" + o).attr('placeholder','');
                }
            });
        };

        var showInputsMask = function () {
            $.each(inputs, function (o, val) {
                var inp = document.getElementById(o);
                if (inp) {
                    $(inp).inputmask(val);
                    $(inp).attr('placeholder',placeholders[o]);
                }
            });
        };


        return {hideInputsMask: hideInputsMask, showInputsMask: showInputsMask, init: init}
    })();



    window.addEventListener('load', function () {
        $(".radio-inline").click(function () {
            var radio = $(this).find('input:radio');

            if (radio.length > 0) {
                radio = radio[0];
                if (radio.name == 'EmployeeCompany[passport_isRf]') {
                    if (radio.value == 0) {
                        $('.field-physical-passport-country').removeClass('hide');
                        Passport_isRf.hideInputsMask();
                    }
                    else {
                        $('.field-physical-passport-country').addClass('hide');
                        Passport_isRf.showInputsMask();
                    }
                }
            }
        });
    });

</script>