<?php

use common\models\employee\Employee;
use common\models\employee\EmployeeSalary;
use frontend\models\SalaryConfigForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SalaryConfigForm */

$employeeCompany = $model->getEmployeeCompany();

$this->title = 'Настройка расчета зарплаты по ' . $employeeCompany->getFio();

$fieldConf = [
    'options' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control text-right',
        'style' => 'width: 100%; min-width: 65px;',
    ],
    'template' => "{input}",
];

$dayRange = range(1, 31);
$dayList = ['' => ''] + array_combine($dayRange, $dayRange);
$result = $model->getFormatCalculatedData();

$js = <<<JS
$(document).on('change', '#salary-config-form input', function () {
    var form = this.form;
    $.post($(form).data('calc-url'), $(form).serialize(), function(data) {
    console.log(data.result);
        if (data.result) {
            $.each(data.result, function(index, value) {
                if ($('td.'+index, form).length) {
                    $('td.'+index, form).html(value);
                }
            });
        }
    });
})
JS;

$this->registerJs($js);
?>

<style type="text/css">
table.employee-salary-table tbody td {
    vertical-align: middle;
}
</style>

<div class="employee-salary">
    <div class="portlet box">
        <h4 class="page-title"><?= Html::encode($this->title); ?></h4>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'salary-config-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnChange' => false,
        'validateOnBlur' => false,
        'options' => [
            'data' => [
                'calc-url' => Url::to(['/salary/config-calc', 'id' => $model->getEmployeeCompany()->employee_id]),
            ]
        ]
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <table class="employee-salary-table table table-bordered">
        <thead>
            <tr class="heading">
                <th rowspan="2"></th>
                <th class="text-center" rowspan="2">Вид расчета</th>
                <th class="text-center" rowspan="2">Сумма на руки (руб.)</th>
                <th class="text-center" rowspan="2">НДФЛ</th>
                <th class="text-center" rowspan="2">Соц. налоги</th>
                <th class="text-center" colspan="2" style="border: 1px solid #ddd;">Аванс</th>
                <th class="text-center" colspan="2" style="border: 1px solid #ddd;">Зарплата</th>
                <th class="text-center" rowspan="2">Итого затраты для компании</th>
            </tr>
            <tr class="heading">
                <th class="text-center">Дата</th>
                <th class="text-center">Сумма</th>
                <th class="text-center">Дата</th>
                <th class="text-center">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $form->field($model, 'has_salary_1', $fieldConf)->checkbox(['label' => false]) ?>
                </td>
                <td>
                    Оклад 1
                </td>
                <td>
                    <?= $form->field($model, 'salary1Amount', $fieldConf)->textInput() ?>
                </td>
                <td class="text-center">
                    <?= EmployeeSalary::TAX_NDFL ?>%
                </td>
                <td class="text-center">
                    <?= EmployeeSalary::TAX_SOCIAL ?>%
                </td>
                <td>
                    <?= $form->field($model, 'salary_1_prepay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td>
                    <?= $form->field($model, 'salary1PrepaySum', $fieldConf)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($model, 'salary_1_pay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td class="text-right nowrap salary_1_pay_sum">
                    <?= $result['salary_1_pay_sum'] ?>
                </td>
                <td class="text-right nowrap salary_1_expenses">
                    <?= $result['salary_1_expenses'] ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $form->field($model, 'has_bonus_1', $fieldConf)->checkbox(['label' => false]) ?>
                </td>
                <td>
                    Премия 1
                </td>
                <td>
                    <?= $form->field($model, 'bonus1Amount', $fieldConf)->textInput() ?>
                </td>
                <td class="text-center">
                    <?= EmployeeSalary::TAX_NDFL ?>%
                </td>
                <td class="text-center">
                    <?= EmployeeSalary::TAX_SOCIAL ?>%
                </td>
                <td>
                    <?= $form->field($model, 'bonus_1_prepay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td>
                    <?= $form->field($model, 'bonus1PrepaySum', $fieldConf)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($model, 'bonus_1_pay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td class="text-right nowrap bonus_1_pay_sum">
                    <?= $result['bonus_1_pay_sum'] ?>
                </td>
                <td class="text-right nowrap bonus_1_expenses">
                    <?= $result['bonus_1_expenses'] ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $form->field($model, 'has_salary_2', $fieldConf)->checkbox(['label' => false]) ?>
                </td>
                <td>
                    Оклад 2
                </td>
                <td>
                    <?= $form->field($model, 'salary2Amount', $fieldConf)->textInput() ?>
                </td>
                <td class="text-center">
                    -
                </td>
                <td class="text-center">
                    -
                </td>
                <td>
                    <?= $form->field($model, 'salary_2_prepay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td>
                    <?= $form->field($model, 'salary2PrepaySum', $fieldConf)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($model, 'salary_2_pay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td class="text-right nowrap salary_2_pay_sum">
                    <?= $result['salary_2_pay_sum'] ?>
                </td>
                <td class="text-right nowrap salary_2_expenses">
                    <?= $result['salary_2_expenses'] ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $form->field($model, 'has_bonus_2', $fieldConf)->checkbox(['label' => false]) ?>
                </td>
                <td>
                    Премия 2
                </td>
                <td>
                    <?= $form->field($model, 'bonus2Amount', $fieldConf)->textInput() ?>
                </td>
                <td class="text-center">
                    -
                </td>
                <td class="text-center">
                    -
                </td>
                <td>
                    <?= $form->field($model, 'bonus_2_prepay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td>
                    <?= $form->field($model, 'bonus2PrepaySum', $fieldConf)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($model, 'bonus_2_pay_day', $fieldConf)->dropDownList($dayList) ?>
                </td>
                <td class="text-right nowrap bonus_2_pay_sum">
                    <?= $result['bonus_2_pay_sum'] ?>
                </td>
                <td class="text-right nowrap bonus_2_expenses">
                    <?= $result['bonus_2_expenses'] ?>
                </td>
            </tr>
        </tbody>
        <tfoot class="text-bold">
            <tr class="summary">
                <td>
                </td>
                <td>
                    Итого
                </td>
                <td class="text-right nowrap total_amount">
                    <?= $result['total_amount'] ?>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td class="text-right nowrap total_prepay_sum">
                    <?= $result['total_prepay_sum'] ?>
                </td>
                <td>
                </td>
                <td class="text-right nowrap total_pay_sum">
                    <?= $result['total_pay_sum'] ?>
                </td>
                <td class="text-right nowrap total_expenses">
                    <?= $result['total_expenses'] ?>
                </td>
            </tr>
        </tfoot>
    </table>

    <div class="form-group">
        Настроенные данные буду использоваться при расчете ежемесячной ЗП
    </div>

    <div class="form-actions">
        <div class="row action-buttons">
            <div class="spinner-button col-sm-3">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]) ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ]) ?>
            </div>
            <div class="spinner-button col-sm-3 pull-right">
                <?= Html::a('Отменить', ['/employee/view', 'id' => $employeeCompany->employee_id], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undo-contractor',
                ]); ?>
                <?= Html::a('<i class="fa fa-reply fa-2x"></i>', ['/employee/view', 'id' => $employeeCompany->employee_id], [
                    'title' => 'Отменить',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ]); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>