<?php /* NOT USED

use common\components\date\DateHelper;
use common\models\document\DocumentType;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

$initialConfig = [
    'wrapperOptions' => [
        'class' => 'col-md-2 inp_one_line',
    ],
];
$yaMetrika = $completeRegistration ? ['onsubmit' => 'yaCounter34333825.reachGoal("regfinish"); return true;'] : [];


echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'employee-form',
    'method' => 'POST',
    'options' => array_merge([
        'class' => 'form-horizontal b-shadow-hide',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ], $yaMetrika),
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-2 control-label bold-text width-212',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7 inp_one_line',
        ],
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => 'col-md-offset-2',
            'hint' => 'col-md-7',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],

    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
])); ?>

<div class="form-body form-body_width">
    <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'email', [
            'hintOptions' => ['tag' => 'div',],
        ])->widget(\common\components\widgets\EmployeeTypeahead::className(), [
            'remoteUrl' => Url::to(['/dictionary/employee']),
            'related' => [
                '#' . Html::getInputId($model, 'lastname') => 'lastname',
                '#' . Html::getInputId($model, 'firstname') => 'firstname',
                '#' . Html::getInputId($model, 'firstname_initial') => 'firstname_initial',
                '#' . Html::getInputId($model, 'patronymic') => 'patronymic',
                '#' . Html::getInputId($model, 'patronymic_initial') => 'patronymic_initial',
                '#' . Html::getInputId($model, 'time_zone_id') => 'time_zone_id',
                '#' . Html::getInputId($model, 'sex') => 'sex',
                '#' . Html::getInputId($model, 'birthday') => 'birthday_format',
                '#' . Html::getInputId($model, 'date_hiring') => 'date_hiring_format',
                '#' . Html::getInputId($model, 'date_dismissal') => 'date_dismissal_format',
                '#' . Html::getInputId($model, 'position') => 'position',
                '#' . Html::getInputId($model, 'employee_role_id') => 'employee_role_id',
                '#' . Html::getInputId($model, 'phone') => 'phone',
            ],
        ])->textInput([
            'placeholder' => 'Автозаполнение по email',
        ]); ?>

    <?= $form->field($model, 'lastname')->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($model, 'firstname')->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($model, 'patronymic')->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'time_zone_id')->dropDownList(
        ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>

    <?= $model->scenario !== Employee::SCENARIO_CONTINUE_REGISTRATION ? $form->field($model, 'sex')
        ->radioList(Employee::$sex_message, [
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                $radio = Html::radio($name, $checked, [
                    'checked' => $checked,
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline m-l-n-md',
                    ],
                ]);

                return Html::tag('div', $radio, [
                    'class' => 'col-r col-xs-4 m-l-n',
                ]);
            },
        ]) : ''; ?>

    <?= $model->scenario !== Employee::SCENARIO_CONTINUE_REGISTRATION ? $form->field($model, 'birthday', [
        'wrapperOptions' => [
            'class' => 'col-md-2 inp_one_line',
        ],
        'template' => Yii::$app->params['formDatePickerTemplate'],
    ])->textInput([
        'class' => 'form-control date-picker min_wid_picker',
        'value' => DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
    ]) : ''; ?>

    <?php if ($model->scenario !== Employee::SCENARIO_CONTINUE_REGISTRATION): ?>
        <div class="form-group">
            <?= $form->field($model, 'date_hiring', [
                'options' => [
                    'class' => '',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>

            <?= $form->field($model, 'date_dismissal', [
                'options' => [
                    'class' => 'one_line_date_dismissal',
                ],
                'labelOptions' => [
                    'class' => 'col-md-3 control-label bold-text label-width',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'position')->textInput([
        'placeholder' => $model->getAttributeLabel('position'),
    ]); ?>
    <?= $form->field($model, 'employee_role_id', [
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n" .
            Html::tag('span', '', [
                'class' => 'tooltip2  ico-question valign-middle',
                'data-tooltip-content' => '#tooltip_employye_role',
                'style' => 'position: absolute; top: 2px; right: -20px;'
            ]) . "\n{endWrapper}",
    ])->dropDownList(
        ArrayHelper::map(\common\models\employee\EmployeeRole::find()->actual()->all(), 'id', 'name'),
        [
            'prompt' => '',
            'disabled' => $model->scenario === Employee::SCENARIO_CONTINUE_REGISTRATION,
        ]
    ); ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>

    <div class="form-group">
        <label class="col-md-2 control-label bold-text width-212">
            Право подписи на документах
        </label>
        <div class="col-md-7 inp_one_line" style="line-height: 35px;">
            <?= Html::activeCheckbox($model, 'can_sign', ['label' => false, 'style' => 'margin-right: 10px;']); ?>
            <span class="employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>">
                <?= Html::activeDropDownList($model, 'sign_document_type_id', DocumentType::find()->select(['name', 'id'])->indexBy('id')->column(), [
                    'prompt' => '',
                    'class' => 'form-control ',
                    'style' => 'display: inline-block; width: 170px; margin-right: 10px;'
                ]); ?>
                <span>№</span>
                <?= Html::activeTextInput($model, 'sign_document_number', [
                    'class' => 'form-control ',
                    'style' => 'display: inline-block; width: 170px; margin-right: 10px;'
                ]); ?>
                <span>от</span>
                <div class="input-icon" style="display: inline-block;">
                    <i class="fa fa-calendar"></i>
                    <?= Html::activeTextInput($model, 'sign_document_date', [
                        'class' => 'form-control date-picker min_wid_picker',
                        'style' => 'display: inline-block; width: 170px;',
                        'value' => DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]); ?>
                </div>
            </span>
        </div>
    </div>
    <div class="form-group employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>">
        <label class="col-md-2 control-label bold-text width-212">
            Подпись
        </label>
        <div class="col-md-7 inp_one_line">
            <?php if ($model->link->employeeSignature) : ?>
                <div class="form-group">
                <?= ImageHelper::getThumb(
                    $model->link->employeeSignature->file,
                    ImageHelper::THUMB_MEDIUM,
                    [
                        'class' => 'signature_image',
                    ]
                ); ?>
                </div>
            <?php endif ?>
            <?= $form->field($model, 'sign_file', [
                'wrapperOptions' => [
                    'class' => '',
                ],
                'hintOptions' => [
                    'tag' => 'p',
                    'class' => 'text-muted',
                ],
                'horizontalCssClasses' => [
                    'offset' => '',
                    'hint' => 'col-md-7',
                ],
            ])->fileInput([
                'class' => 'js_filetype',
                'data-button-text' => 'Добавить подпись',
                'accept' => 'image/gif, image/jpeg, image/png, image/bmp, ',
            ])->label(false); ?>
        </div>
    </div>
</div>

<?php \yii\bootstrap\ActiveForm::end(); ?>

<div class="tooltip_templates">
    <span id="tooltip_employye_role"
          style="display: inline-block;">
        <b>Руководитель</b> – это максимальный уровень доступа.<br>
        <b>Руководитель отдела</b> - видит все счета выставляемые менеджерами,<br>
        может редактировать и удалять счета, акты, накладные и счета-фактуры.<br>
        Не видит блоки, связанные с финансами.<br>
        <b>Руководитель отдела (только просмотр)</b> – видит все счета выставляемые менеджерами.<br>
        Не видит блоки, связанные с финансами.<br>
        <b>Менеджер</b> – может выставлять счета, видит только свои счета.<br>
        Не видит блоки, связанные с финансами.<br>
        <b>Бухгалтер</b> – видит все, кроме операций, которые помечены «Не для бухгалтерии».<br>
        <b>Помощник</b> – может вносить документы от ваших поставщиков.<br>
        Счета вашим клиентам - не видит.<br>
        <b>Без доступа в КУБ</b> – сотрудник, не имеющий доступа в сервис КУБ24.<br>
    </span>
</div>

<?php $this->registerJs('
$(document).on("change", "#employeeform-can_sign", function () {
    if ($(this).is(":checked")) {
        $(".employee_signature_element").removeClass("hidden_element");
    } else {
        $(".employee_signature_element").addClass("hidden_element");
    }
});
') ?>
*/