<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\document\DocumentType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use common\components\image\EasyThumbnailImage;

/* @var yii\web\View $this */
/* @var common\models\employee\Employee $model */

/* @var \yii\bootstrap\ActiveForm $form */
/* @var $completeRegistration boolean */

$initialConfig = [
    'wrapperOptions' => [
        'class' => '',
    ],
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);

$showChBxRoles = EmployeeCompany::$addFlowRolesNeedConfig;
$payInvoiceCss = in_array($model->employee_role_id, $showChBxRoles) ? '' : 'hidden';

$templateDateInput = "<div class='date-picker-wrap' style='width:100%'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>{error}";
$templateDateInputTop = "<div class='date-picker-wrap date-picker-wrap-top' style='width:100%'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>{error}";
?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12 mtm-l">
            <div class="row">
                <div class="col-3">
                    <div class="label">Часовой пояс</div>
                    <?= $form->field($model, 'time_zone_id')->widget(\kartik\select2\Select2::class, [
                        'data' => ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone'),
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                </div>
                <div class="col-3">
                    <div class="label">Дата рождения</div>
                    <?= $form->field($model, 'birthday', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control form-control_small date-picker',
                        'value' => DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'style' => 'width: 100%'
                    ]) ?>
                </div>
                <div class="col-3">
                    <div class="label">Пол</div>
                    <?= $form->field($model, 'sex')->radioList(Employee::$sex_message, [
                        'uncheck' => null,
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $radio = Html::radio($name, $checked, [
                                'checked' => $checked,
                                'value' => $value,
                                'label' => $label,
                                'labelOptions' => [
                                    'class' => 'radio-inline m-l-n-md',
                                ],
                            ]);

                            return Html::tag('div', $radio, [
                                'class' => 'col-r col-xs-4 m-l-n',
                            ]);
                        },
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12 mtm-l">
            <div class="row">
                <div class="col-3">
                    <div class="label">Дата приёма на работу</div>
                    <?= $form->field($model, 'date_hiring', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control form-control_small date-picker',
                        'value' => DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'style' => 'width: 100%'
                    ]) ?>
                </div>
                <div class="col-3">
                    <div class="label">Дата увольнения</div>
                    <?= $form->field($model, 'date_dismissal', [
                        'template' => $templateDateInputTop,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control form-control_small date-picker',
                        'value' => DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'style' => 'width: 100%',
                        'data-position' => 'top left'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-12" style="margin-top:8px;">
            <div class="row">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'document_access_own_only', [
                        'data-initvalue' => $model->document_access_own_only,
                        'data-default-on' => EmployeeCompany::$docAccessOwnOnly,
                        'data-always-off' => EmployeeCompany::$docAccessAll,
                        'disabled' => in_array($model->employee_role_id, EmployeeCompany::$docAccessAll) ? true : null,
                    ]); ?>
                </div>
            </div>

            <div class="col-12 mb-3" style="padding: 0!important;">
                <?= Html::activeCheckbox($model, 'can_sign', ['label' => 'Право подписи на документах']); ?>
            </div>

            <div class="employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>" style="margin-top:-0.5rem;">
                <div class="row">
                    <div class="col-3">
                        <label class="label">Наименование документа</label>
                        <?= $form->field($model, 'sign_document_type_id')->widget(\kartik\select2\Select2::class, [
                            'hideSearch' => true,
                            'data' => DocumentType::find()->select(['name', 'id'])->indexBy('id')->column(),
                            'class' => 'form-control ',
                            'options' => [
                                'prompt' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">Номер</label>
                        <?= $form->field($model, 'sign_document_number')->textInput([
                            'maxlength' => true,
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">Дата</label>
                        <?= $form->field($model, 'sign_document_date', [
                            'template' => $templateDateInput,
                            'labelOptions' => [
                                'class' => 'label',
                            ],
                        ])->textInput([
                            'class' => 'form-control form-control_small date-picker',
                            'value' => DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            'style' => 'width: 100%'
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-3 mb-3 employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>">
                    <div class="label">Подпись </div>

                    <div class="flex-shrink-0">

                        <?= Html::input('hidden', 'apply',
                            Yii::$app->request->post('apply', 0),
                            ['id' => 'apply-signature-to-all-documents']) ?>

                        <?php
                        $attr = 'signature_file';
                        $uploadUrl = Url::toRoute(['/employee/img-upload', 'id' => $model->employee_id, 'attr' => $attr]);
                        $imgPath = $model->getTmpImage($attr);
                        if (!$imgPath && $model->employeeSignature) {
                            $imgPath = $model->employeeSignature->getImage();
                        }
                        ?>
                        <div id="signature_file-image-result" class="<?= !$imgPath ? 'hidden' : '' ?>">
                            <table class="company-image-preview">
                                <tr>
                                    <td>
                                        <?php
                                        if (is_file($imgPath)) : ?>
                                            <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                            <div class="del-link link del-company-image" data-attr="signature_file" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                        </div>
                        <div id="signature_file-image-upload" class="<?= $imgPath ? 'hidden' : '' ?>">
                            <div id="company_img_<?= $attr ?>" class="dz-upload-company-image dropzone mb-1">
                                <div id="progress_<?= $attr ?>" class="progress progress-striped active" style="display: none">
                                    <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                                </div>
                            </div>
                            <div class="dz-upload-product-tip">
                                <?= EmployeeCompany::$imageDataArray['signature_file']['dummy_text'] ?>. <?= 'Форматы: JPG, JPEG, PNG.' ?>
                            </div>
                            <?= $this->render('_jsDropzone', [
                                'model' => $model,
                                'attr' => $attr,
                                'uploadUrl' => $uploadUrl,
                            ])?>
                        </div>
                        <div style="display: none!important;">
                            <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteSignature_file', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                                'id' => 'signature_file-delete'
                            ]) : ''; ?>
                        </div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate): ?>
                            <div id="signature_file-modification-date" class="text-grey text-left">
                                Дата добавления<br>
                                <?= date(DateHelper::FORMAT_USER_DATE, $modificationDate); ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php /*
                    <div class="">
                        <div class="<?= ($model->employeeSignature) ? '' : 'hidden' ?> mb-3" id="signature_file-image-result">
                            <?php if ($model->employeeSignature): ?>
                                <?= EasyThumbnailImage::thumbnailImg($model->employeeSignature->file, 165, 60, EasyThumbnailImage::THUMBNAIL_INSET, [
                                    'class' => 'signature_image',
                                ]); ?>
                            <?php else: ?>
                                <img class="signature_image" src="" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <?= Html::button('Добавить подпись', [
                                'class' => 'button-regular button-hover-content-red',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-employeeSignatureImage',
                                'style' => 'margin-right: 20px;',
                            ]); ?>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <?= $model->employeeSignature ? Html::activeCheckbox($model, 'deleteSignature_file', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]) : ''; ?>
                        </div>
                    </div>
                    */ ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'can_view_price_for_buy', ['label' => 'Видит закупочные цены']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'is_product_admin', ['label' => 'Может добавлять номенклатуру']); ?>
                </div>
            </div>
            <div class="row employeecompany-can_invoice_add_flow <?= $payInvoiceCss; ?>">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'can_invoice_add_flow', ['label' => 'Может добавлять оплату по счетам']); ?>
                </div>
            </div>
            <div class="row can_product_write_off_wrap <?= $model->employee_role_id == EmployeeRole::ROLE_MANAGER ? '' : 'hidden' ?>">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'can_product_write_off', [
                        //'label' => 'Товар',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <?= Html::activeCheckbox($model, 'can_business_analytics', [
                        'label' => 'Доступ к ФинДиректор',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3 employee_business_analytics<?= $model->can_business_analytics ? '' : ' hidden_element'; ?>">
                    <div class="row mb-3">
                        <div class="col-12">
                            <?= Html::activeCheckbox($model, 'can_ba_all', [
                                'label' => 'Все блоки',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="column">
                            <?= Html::activeCheckbox($model, 'can_ba_finance', [
                                'label' => 'Финансы',
                            ]); ?>
                            <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'data-tooltip-content' => '#tooltip_ba_finance',
                            ]) ?>
                        </div>
                        <div class="column">
                            <?= Html::activeCheckbox($model, 'can_ba_marketing', [
                                'label' => 'Маркетинг',
                            ]); ?>
                            <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'data-tooltip-content' => '#tooltip_ba_marketing',
                            ]) ?>
                        </div>
                        <div class="column">
                            <?= Html::activeCheckbox($model, 'can_ba_sales', [
                                'label' => 'Продажи',
                            ]); ?>
                            <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'data-tooltip-content' => '#tooltip_ba_sales',
                            ]) ?>
                        </div>
                        <div class="column">
                            <?= Html::activeCheckbox($model, 'can_ba_product', [
                                'label' => 'Товар',
                            ]); ?>
                            <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip2',
                                'data-tooltip-content' => '#tooltip_ba_product',
                            ]) ?>
                        </div>
                    </div>
                    <?php if ($baError = $model->getFirstError('can_business_analytics')) : ?>
                        <div class="text-danger">
                            <p class="help-block help-block-error "><?= $baError ?></p>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none">
    <div id="tooltip_ba_finance">
        Сотрудник сможет видеть отчеты,<br/>
        которые входят только в блок Финансы
    </div>
    <div id="tooltip_ba_marketing">
        Сотрудник сможет видеть отчеты,<br/>
        которые входят только в блок Маркетинг
    </div>
    <div id="tooltip_ba_product">
        Сотрудник сможет видеть отчеты,<br/>
        которые входят только в блок Товары
    </div>
    <div id="tooltip_ba_sales">
        Сотрудник сможет видеть отчеты,<br/>
        которые входят только в блок Продажи
    </div>
</div>