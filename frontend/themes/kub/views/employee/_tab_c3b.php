<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.06.2019
 * Time: 12:28
 */

use yii\helpers\Html;
use yii\widgets\MaskedInput;

/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{input}",
];
?>


<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="col-3 mb-3">
                    <div class="label">ИНН сотрудника</div>
                    <?=
                    $form->field($model, 'inn')->textInput([
                        'maxlength' => false,
                    ]);
                    ?>
                </div>
                <div class="col-3 mb-3">
                    <div class="label">СНИЛС сотрудника</div>
                    <?=
                    $form->field($model, 'snils')
                        ->widget(MaskedInput::className(), [
                            'mask' => '9{3}-9{3}-9{3} 9{2}',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => 'ХХХ-ХХХ-ХХХ ХХ',
                            ],
                        ]);
                    ?>
                </div>
                <div class="col-6 mb-3 mt-2" style="padding-top: 28px">
                    <?= Html::activeCheckbox($model, 'in_szvm', ['label' => 'Добавить сотрудника в отчет']); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portlet box darkblue employeecompany-passport-info-wrapper">
    <div class="portlet-body min-width-container-passport">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group field-employeecompany-inn">

                </div>
                <div class="form-group field-employeecompany-snils">

                </div>
                <div class="form-group field-employeecompany-in_szvm">

                </div>
            </div>
        </div>
    </div>
</div>
