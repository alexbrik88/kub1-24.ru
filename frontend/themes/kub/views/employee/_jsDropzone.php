<?php
$fileTypeText = 'Форматы: JPG, JPEG, PNG.';
$dummyText = "<span class=\"link\">Выберите файлы</span><br/> или перетащите сюда.";
$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';
?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzone_<?= $attr ?> = new Dropzone("#company_img_<?= $attr ?>", {
        'paramName': '<?= $attr ?>',
        'url': '<?= $uploadUrl ?>',
        'dictDefaultMessage': '<?= $dummyText ?>',
        'dictInvalidFileType': '<?= $fileTypeText ?>',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png",
        'params': {'<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
        'previewTemplate': "<?= $DZpreviewTemplate ?>"
    });
    dropzone_<?= $attr ?>.on('thumbnail', function (f, d) {
        $('#progress_<?= $attr ?>').show();
    });
    dropzone_<?= $attr ?>.on('success', function (f, d) {
        $('#modal-employeeSignatureImage').modal('show');
        pjaxImgFormKUB('/employee/img-form', '<?= $model->employee_id ?>', '<?= $attr ?>', 'crop');
        this.removeAllFiles(true);
        dragEnd();
    });
    dropzone_<?= $attr ?>.on('error', function (f, d) {
        alert(d);
        this.removeAllFiles(true);
        $('#progress_<?= $attr ?>').hide();
        $('#progress_bar_<?= $attr ?>').width(0);
        dragEnd();
    });
    dropzone_<?= $attr ?>.on('totaluploadprogress', function (progress) {
        $('#progress_bar_<?= $attr ?>').width(progress + '%');
    });
    dropzone_<?= $attr ?>.on("addedfile", function(f) {
        dragEnd();
    });
    dropzone_<?= $attr ?>.on("dragover", function(e) {
        dragStart();
    });
    dropzone_<?= $attr ?>.on("dragleave", function(e) {
        dragEnd();
    });

    /////////////////////////////////////////////////////////////////////////////////////

    function jcropImageDeleteKUB(pjaxUrl, formUrl, companyId, attr) {
        jQuery.post(pjaxUrl, {'id': companyId, 'attr': attr}, function (data) {
            pjaxImgFormKUB(formUrl, companyId, attr, 'view');
        });
    }
    function jcropImageAcceptedKUB(attr) {
        var edit = $('#' + attr + '-image-edit');
        $('#' + attr + '-image-upload').addClass('hidden');
        $('#' + attr + '-image-result td').html(edit.find('td').html());
        $('#' + attr + '-image-result').removeClass('hidden');
        if ($('#'+attr+'-delete').length) {
            $('#'+attr+'-delete').prop('checked', false);
        }

        $('#modal-employeeSignatureImage').modal('hide');
    }
    function pjaxImgFormKUB(pjaxUrl, companyId, attr, action, apply) {
        apply = apply || 0;
        jQuery.pjax({
            url: pjaxUrl,
            data: {'id': companyId, 'attr': attr, 'action': action, 'apply': apply},
            container: '#' + attr + '-pjax-container',
            timeout: 5000,
            push: false,
            scrollTo: false
        });
    }
    function jcrop_ajaxRequestKUB(pjaxUrl, formUrl, companyId, attr, el) {
        var hasApply = el !== undefined;
        var tab = hasApply ? $(el).closest('.jcrop-image-container') : null;
        var apply = tab !== null ? $('input.apply-image-radio:checked', tab).val() : '0';
        var ajaxData = {};
        ajaxData[attr + '_x'] = $('#' + attr + '_x').val();
        ajaxData[attr + '_x2'] = $('#' + attr + '_x2').val();
        ajaxData[attr + '_y'] = $('#' + attr + '_y').val();
        ajaxData[attr + '_y2'] = $('#' + attr + '_y2').val();
        ajaxData[attr + '_h'] = $('#' + attr + '_h').val();
        ajaxData[attr + '_w'] = $('#' + attr + '_w').val();
        ajaxData['id'] = companyId;
        ajaxData['attr'] = attr;
        ajaxData['width'] = $('#' + attr).width();
        ajaxData['apply'] = apply;
        if ($('#company-tmpid').lenght) {
            ajaxData['tmpId'] = $('#company-tmpid').val();
        }
        jQuery.ajax({
            type: 'post',
            url: pjaxUrl,
            data: ajaxData,
            success: function (data) {
                pjaxImgFormKUB(formUrl, companyId, attr, 'view', apply);
            }
        });
    }

    $(document).on('click', '.del-company-image', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        var itemName = 'подпись';

        delModal.find('.modal-title > span').html(itemName);
        delModal.find('.del-company-image-ok').data('attr', attr);
        delModal.modal('show');

        return false;
    });

    $(document).on('click', '.del-company-image-ok', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        if (attr) {
            $('#' + attr + '-image-result').addClass('hidden');
            $('#' + attr + '-image-upload').removeClass('hidden');
            // form input
            if ($('#' + attr + '-delete').length) {
                $('#' + attr + '-delete').prop('checked', true);
            }
            $('#' + attr + '-modification-date').remove();
        }
        delModal.modal('hide');
    });

    $(document).on('hide.bs.modal', '#modal-employeeSignatureImage', function() {
        $('.progress-bar').width(0);
        $('.progress-bar').parent().hide();
    });

    /* DRAG */
    function dragStart() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#e8eaf7', 'border-color': '#0048aa'});
        $(blocks).find('.dz-message').html('<span>Перетащите файл сюда.</span>');
    }
    function dragEnd() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#fff', 'border-color': '#ddd'});
        $(blocks).find('.dz-message').html('<span><span class="link">Выберите файлы</span><br> или перетащите сюда.</span>');
    }

    $(document).on("dragstart, dragover", function (e) {
        dragStart();
    });
    $(document).on("dragend, dragleave", function (e) {
        dragEnd();
    });

</script>