<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.06.2019
 * Time: 12:28
 */

use yii\widgets\MaskedInput;

/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pl-2 pr-2 mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-9">
            <div class="row">
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">ИНН сотрудника</div>
                    <div><?= $model->inn ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">СНИЛС сотрудника</div>
                    <div><?= $model->snils ?: '—' ?></div>
                </div>
                <div class="column mb-3 pb-3 col-4">
                    <div class="label weight-700 mb-3">Добавить сотрудника в отчет</div>
                    <div><?= $model->in_szvm ? 'да' : 'нет' ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
