<?php

use common\components\notification\NotificationFlash;
use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\rbac\UserRole;
use frontend\themes\kub\assets\KubAsset;
use frontend\themes\kub\widgets\Alert;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\widgets\AjaxModalWidget;
use frontend\widgets\InvoicePaymentModal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\PriceListNotificationsWidget;
use frontend\widgets\QueueNotificationsWidget;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);

$user = Yii::$app->user->identity;
$company = ArrayHelper::getValue($user, 'company');
$controller = Yii::$app->controller->id;
$firstModule = Yii::$app->controller->getModules()[1]->id ?? null;
$module = Yii::$app->controller->module->id;
$action = Yii::$app->controller->action->id;
$route = Yii::$app->controller->getRoute();
$paramType = Yii::$app->request->getQueryParam('type');
$paramDocument = Yii::$app->request->getQueryParam('document');
$hideCarrot = ArrayHelper::getValue($this->params, 'hideCarrot', false);

if ($module == 'documents' || in_array($route, ['export/one-s/index', 'export/files/index'])) {
    $active = 'documents';
} elseif ($module == 'cash') {
    $active = 'cash';
} elseif ($controller == 'contractor') {
    $active = 'contractor';
} elseif ($controller == 'product') {
    $active = 'product';
} else {
    $active = null;
}

$isKUBbank = Yii::$app->params['isKUBbank'];
$footerText = 'ООО «КУБ»';
$isPreviewScanFullscreen = Yii::$app->controller->module->id == 'documents' && Yii::$app->controller->action->id == 'preview-scans';
$isPreviewScan = (Yii::$app->controller->module->id == 'documents' && Yii::$app->request->get('mode') == 'previewScan');
$previewScanClass = ($isPreviewScan || $isPreviewScanFullscreen ? 'preview-scan' : '') . ($isPreviewScanFullscreen ? ' full-screen' : '');
$isDemo = Yii::$app->user->id == (Yii::$app->params['service']['demo_employee_id'] ?? -1);

$bodyCssClass = ['theme-kub'];
$userConf = $user->config ?? null;
if ($userConf->theme_wide ?? 0) {
    $bodyCssClass[] = 'theme-kub_wide';
}
if ($userConf->minimize_side_menu ?? 0) {
    $bodyCssClass[] = 'theme-kub_sidebar_collapsed';
}
$blackScreenShow = $this->params['black-screen-show'] ?? false;
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
      <meta charset="<?= Yii::$app->charset ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?= Html::csrfMetaTags() ?>
      <title><?= Html::encode($this->title) ?></title>
      <?php $this->head() ?>
      <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">

      <style>
          .table-style:not(.table-bleak) td a:not(.link-bleak),
          .table-style:not(.table-bleak) td .link:not(.link-bleak),
          .table-style:not(.table-bleak) td button:not(.link-bleak),
          .table-style:not(.table-bleak).table-compact td a:not(.link-bleak),
          .table-style:not(.table-bleak).table-compact td .link:not(.link-bleak),
          .table-style:not(.table-bleak).table-compact td button:not(.link-bleak)
          {
              color:<?= ($user) ? $user->getLinksColor() : \common\models\employee\Employee::$linkColors[0] ?>!important;
          }
      </style>

	</head>
	<body class="<?= implode(' ', $bodyCssClass) ?>">
      <div class="black-screen <?= $blackScreenShow ? 'show' : ''; ?>"></div>
      <div class="wrapper__in">
          <?php $this->beginBody() ?>

          <?= AjaxModalWidget::widget(); ?>

          <?= $this->render('_main_menu', [
              'user' => $user,
              'company' => $company,
              'controller' => $controller,
              'module' => $module,
              'action' => $action,
              'route' => $route,
              'paramType' => $paramType,
              'active' => $active,
              'isKUBbank' => $isKUBbank,
              'isDemo' => $isDemo,
          ]) ?>

          <div id="ajax-loading" class="ajax-loader-wrapper">
              <img src="/img/loading.gif">
          </div>

          <div class="wrapper__content">
						<div class="page-grid">
							<div class="container-fluid">
									<div class="row flex-nowrap">
											<?php if ($isPreviewScanFullscreen): ?>
													<?php // fullscreen mode ?>
											<?php elseif ($isPreviewScan): ?>
													<?= $this->render('@frontend/themes/kub/views/layouts/_widget_side_menu_preview_scan', [
															'isFullScreen' => false
													]);
													?>
											<?php elseif (strpos(Yii::$app->request->url, 'upload-manager')): ?>
													<?= $this->render('_widget_side_menu_documents', [
															'user' => $user,
															'company' => $company,
															'controller' => $controller,
															'module' => $module,
															'action' => $action,
															'route' => $route,
															'paramType' => $paramType,
															'paramDocument' => $paramDocument,
															'active' => $active,
															'isDemo' => $isDemo,
													]) ?>
											<?php else: ?>
													<?= $this->render('_side_menu', [
															'user' => $user,
															'company' => $company,
															'controller' => $controller,
															'firstModule' => $firstModule,
                              'module' => $module,
															'action' => $action,
															'route' => $route,
															'paramType' => $paramType,
															'paramDocument' => $paramDocument,
															'active' => $active,
															'isDemo' => $isDemo,
													]) ?>
											<?php endif; ?>

											<div class="page-content column <?= $previewScanClass ?>">
													<?php // $this->render('//layouts/_service_kub_subscribe'); ?>

													<?= NotificationFlash::widget([
															'options' => [
																	'closeButton' => true,
																	'showDuration' => 1000,
																	'hideDuration' => 1000,
																	'timeOut' => 5000,
																	'extendedTimeOut' => 1000,
																	'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
																	'escapeHtml' => false,
															],
													]); ?>

													<?= Alert::widget(); ?>

													<?= $content ?>

													<?php if (\Yii::$app->user && Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
															<?= BankingModalWidget::widget() ?>
															<?= frontend\modules\ofd\widgets\OfdModalWidget::widget() ?>
													<?php endif; ?>

											</div>
									</div>
							</div>
						</div>
          </div>
          <footer class="footer">
              <div class="container-fluid">
                  <div class="row align-items-center">
                      <div class="copy column"> © <?= date('Y') ?> <?= $footerText ?></div>
                      <div class="column ml-5">
                      </div>
                      <div class="copy column ml-auto">Служба поддержки: 8 800 500 54 36. Бесплатно по России. E-mail: support@kub-24.ru</div>
                  </div>
              </div>
          </footer>
      </div>

      <?php /* if (Yii::$app->user->can(frontend\rbac\permissions\Subscribe::INDEX)) {
          echo SubscribeReminderWidget::widget(['company' => Yii::$app->user->identity->company]);
      } */ ?>

      <?= frontend\widgets\IfFreeTariffWidget::widget([
          'modalId' => 'if-free-tariff-xls',
          'linkSelector' => '.get-xls-link',
          'text' => 'Выгрузить данные в Excel можно только на платном тарифе.',
      ]); ?>

      <?= frontend\widgets\IfFreeTariffWidget::widget([
          'modalId' => 'if-free-tariff-word',
          'linkSelector' => '.get-word-link',
          'text' => 'Скачать документ в Word можно только на платном тарифе.',
      ]); ?>

      <?php // $this->render('//layouts/modal/modal_after_registration'); ?>

      <?= $this->render('@frontend/modules/tax/views/robot/parts_bank/modal-kub-worth', ['onStartPage' => true]) ?>

      <?= $this->render('//layouts/modal/notify_modal'); ?>

      <?php // $this->render('//layouts/_service_video_modal'); ?>

      <?= $this->render('modal/can_not_delete_article_modal'); ?>

      <?= $this->render('modal/add_new_article_modal'); ?>

      <?= $this->render('modal/preload_modal'); ?>

      <?= $this->render('modal/ofd_receipt_view_modal'); ?>

      <?= !\Yii::$app->user->isGuest ? $this->render('//layouts/inquirer/_modal') : null; ?>

      <?= (false && YII_ENV === 'prod') ? $this->render('//layouts/script/jivosite') : null; ?>

      <?= InvoicePaymentModal::widget(); ?>
      <?= PriceListNotificationsWidget::widget() ?>

      <?= $this->render('@frontend/widgets/views/modal/menu_no_rules'); ?>

      <?= $this->render('@frontend/widgets/views/modal/_create_company', [
          'newCompany' => new Company([
              'company_type_id' => CompanyType::TYPE_OOO,
              'scenario' => Company::SCENARIO_CREATE_COMPANY,
          ]),
          'companyTaxation' => new CompanyTaxationType(),
      ]); ?>

      <?= ConfirmModalWidget::widget([
          'toggleButton' => false,
          'options' => [
              'id' => 'logout-confitm-modal',
          ],
          'confirmUrl' => Url::to(['/site/logout']),
          'message' => 'Вы уверены, что хотите выйти?',
      ]) ?>
      
      <?= QueueNotificationsWidget::widget() ?>

      <div id="blockScreen"></div>

      <?php $this->endBody(); ?>

      <script>
          jQuery(document).ready(function () {
              Metronic.init(); // init metronic core components
              ChartsFlotcharts.init();
              ChartsFlotcharts.initCharts();
              ChartsFlotcharts.initBarCharts();
          });
      </script>

  </body>

  <?php if (YII_ENV === 'prod') : ?>
    <script src="https://cdn.optimizely.com/js/6093251051.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFPYASVmcsXKqaa1Z2m9tAQgXvdw0CdvM"></script>

    <?= (!$hideCarrot) ? $this->render('//layouts/_carrot') : null; ?>

    <!-- Google Tag Manager -->
    <script>
      (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
          'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-WBNX29');
    </script>
    <!-- End Google Tag Manager -->

    <?= $this->render('//layouts/_metrics'); ?>

    <?php if (false && empty(Yii::$app->params['hide_experrto_assistant'])): ?>
      <script src='https://cdn.experrto.io/client/experrto.js'></script>
      <script>
          Experrto.identify("25f5a3dc3c67023c0b27e3d9d7754a3e5e4d0093");
      </script>
    <?php endif; ?>
  <?php endif; ?>
</html>
<?php $this->endPage(); ?>