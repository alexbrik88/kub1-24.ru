<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.10.2018
 * Time: 13:15
 */

use frontend\assets\AppAsset;
use common\components\helpers\Html;
use frontend\themes\kub\assets\KubAsset;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=1" type="image/x-icon">
</head>
<body class="out-view-page theme-kub">
    <div class="wrapper__in">
        <?php $this->beginBody() ?>
        <div class="wrapper__content">
            <div class="container">
                <div class="price-list-view">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <?php $this->endBody(); ?>
    </div>
</body>
</html>
<?php $this->endPage(); ?>
