<?php $dirClass = (mb_strlen($dir->name, 'utf-8') > 16) ? ' tooltip-upload-dir' : ''; ?>
<div class="sidebar-menu-dir sidebar-menu-head" style="position: relative">
    <a class="sidebar-menu-btn <?= $dirClass ?> button-clr" href="{url}" data-tooltip-content="#tooltip-upload-dir-<?= $dir->id ?>">
        <svg class="sidebar-menu-icon svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#folder"></use>
        </svg>
        <span class="sidebar-menu-btn-txt ellipsis">{label}</span>
    </a>
    <i class="sidebar-menu-btn directory-options">
        <svg class="sidebar-menu-icon sidebar-menu-icon-right svg-icon tooltip-dir-options" data-tooltip-content="#dir-options-<?= $dir->id ?>">
            <use xlink:href="/img/svg/svgSprite.svg#cog-1"></use>
        </svg>
    </i>
    <?php if (count($dir->files)): ?>
        <?= \common\components\helpers\Html::tag('span', count($dir->files), ['class' => 'badge']) ?>
    <?php endif; ?>
</div>