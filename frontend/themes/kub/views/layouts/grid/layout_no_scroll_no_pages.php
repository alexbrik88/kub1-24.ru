<?php
use frontend\components\PageSize;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}
?>

<div class="wrap wrap_padding_none">
        <div class="table-wrap">
            {items}
        </div>
</div>
<?php /*
<div class="table-settings-view row align-items-center">
    <div style="width: 100%">
        {pager}
    </div>
</div>*/ ?>