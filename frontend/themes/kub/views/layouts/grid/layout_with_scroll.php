<?php

use frontend\components\PageSize;
use yii\helpers\ArrayHelper;

$pageSizeParam = ArrayHelper::getValue($_params_, 'pageSizeParam', 'per-page');
$showExtraLink = ArrayHelper::getValue($_params_, 'showExtraLink', false);
?>

<style>
    th {
        white-space: nowrap;
    }
</style>

<div class="wrap wrap_padding_none">
    <div class="table-wrap" style="overflow-x: scroll;">
        {items}
    </div>
</div>
<div class="table-settings-view row align-items-center">
    <div class="col-8">
        <nav>
            {pager}
        </nav>
    </div>
    <div class="col-4">
        <?= $this->render('perPage', [
            'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => $pageSizeParam,
        ]) ?>
    </div>
</div>