<?php

use frontend\components\PageSize;
use frontend\themes\kub\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}

$perPage = PageSize::get($pageSizeParam);
$items = PageSize::$items;
?>

<div class="page-size-selector form-group d-flex flex-nowrap align-items-center justify-content-end">
    <label class="label mr-3 mb-0" for="view">Отображать по</label>
    <div class="dropdown dropdown-view">
        <?= Html::button($perPage.Icon::get('shevron', [
            'class' => 'form-control-shevron svg-icon input-toggle',
            'style' => 'position: absolute;',
        ]), [
            'id' => 'view',
            'class' => 'button-regular button-hover-transparent w-100 text-left px-3',
            'style' => 'position: relative; font-size: 16px; font-weight: normal; padding: 10px 40px 10px 15px;',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
        ]) ?>
        <div class="form-filter-drop dropdown-menu dropdown-menu-right" aria-labelledby="view">
            <ul class="form-filter-list list-clr">
                <?php foreach ($items as $key => $value) : ?>
                    <?= Html::tag('li', Html::a($value, Url::current(['page' => null, $pageSizeParam => $key])), [
                        'class' => $perPage == $key ? 'active': null,
                    ]) ?>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>
