<?php

use frontend\components\PageSize;
use yii\helpers\ArrayHelper;

?>

<div class="wrap wrap_padding_none">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            {items}
        </div>
    </div>
</div>
<div class="table-settings-view row align-items-center">
    <div class="col-12">
        <nav>
            {pager}
        </nav>
    </div>
</div>