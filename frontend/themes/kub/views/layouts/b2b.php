<?php

use common\components\widgets\StepMenu;
use common\models\company\CompanyType;
use yii\bootstrap4\Html;
use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$company = Yii::$app->user->identity->company;
$type = $company->companyType->name_short;
$isIp = $company->company_type_id == CompanyType::TYPE_IP;
?>

<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>

<div class="menu-nav-tabs nav-tabs-row pb-3 mb-1">
    <?= StepMenu::widget([
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 justify-content-around',
        ],
        'itemOptions' => [
            'class' => 'nav-item pl-2 pr-2 d-flex flex-column',
            'style' => 'flex: 1 1 0;',
        ],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => 'Реквизиты вашего ' . $company->companyType->name_short,
                'url' => ['/b2b/company'],
                'template' => $this->render('_b2b_step_link', ['i' => $isIp ? 'man' : 'bag']),
            ],
            [
                'label' => 'Описание В2В эквайринга',
                'url' => ['/b2b/description'],
                'template' => $this->render('_b2b_step_link', ['i' => 'description']),
            ],
            [
                'label' => 'Товары',
                'url' => ['/b2b/products'],
                'template' => $this->render('_b2b_step_link', ['i' => 'buildings']),
            ],
            [
                'label' => 'Услуги',
                'url' => ['/b2b/services'],
                'template' => $this->render('_b2b_step_link', ['i' => 'tools']),
            ],
            [
                'label' => 'В2В эквайринг',
                'url' => ['/b2b/module'],
                'template' => $this->render('_b2b_step_link', ['i' => 'bank']),
            ],
        ],
    ]); ?>
</div>

<?= $content ?>

<?php $this->endContent(); ?>
