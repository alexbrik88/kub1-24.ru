<?php
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;

$type = Yii::$app->request->get('type');
$id = Yii::$app->request->get('id');
$tab = Yii::$app->request->get('tab', 'index');
$contractor = $this->params['dossierContractor'];
$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <h4>Досье <?= ($contractor) ? $contractor->getShortName(true) : '' ?></h4>
    <?php
    echo Nav::widget([
        'items' => [
            ['label' => 'Главная', 'url' => ['/dossier', 'type' => $type, 'id' => $id], 'active' => $tab == 'index'],
            ['label' => 'Учредители', 'url' => ['/dossier/founders', 'id' => $id], 'active' => $tab == 'founders'],
            ['label' => 'Связи', 'url' => ['/dossier/connections', 'id' => $id], 'active' => $tab == 'connections'],
            ['label' => 'Надежность', 'url' => ['/dossier/reliability', 'id' => $id], 'active' => $tab == 'reliability'],
            ['label' => 'Финансы', 'url' => ['/dossier/finance', 'id' => $id], 'active' => $tab == 'finance'],
            ['label' => 'Суды', 'url' => ['/dossier/arbitr', 'id' => $id], 'active' => $tab == 'arbitr'],
            ['label' => 'Виды деят.', 'url' => ['/dossier/okved', 'id' => $id], 'active' => $tab == 'okved'],
            ['label' => 'Отчетность', 'url' => ['/dossier/accounting', 'id' => $id], 'active' => $tab == 'accounting']
        ],
        'options' => ['class' => 'nav nav-tabs w-100 mr-3'],
    ]);
    ?>

    <?php echo $content ?>

</div>

<?php $this->endContent(); ?>
