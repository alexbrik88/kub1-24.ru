<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.04.2020
 * Time: 1:18
 */

use common\components\helpers\Html;

?>
<div id="article-is-used" class="fade modal" role="modal" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 30px;text-align: center;font-size: 20px;">
                    Статью нельзя удалить, так как она используется минимум в одной операции.
                </div>
                <div class="text-center">
                    <?= Html::button('ОК', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
