<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.03.2018
 * Time: 7:09
 */

use common\components\ImageHelper;
use yii\helpers\Url;
use frontend\models\Documents;
use yii\helpers\Html;

/* @var $description string
 * @var $link string
 * @var $image string
 * @var $video string
 * @var $previousModal integer
 * @var $nextModal integer
 * @var $type integer
 */
?>
<div class="col-xs-12 pad0 submodal-content">
    <h4 class="description" style="text-align: center;font-size: 17px;color: #00618e;">
        <?= $description; ?>
    </h4>

    <div class="col-xs-12" style="padding: 10px 20px!important;text-align: center">
        <div class="loadPage" data-page="<?= $previousModal; ?>" style="padding-top: 8px;">
            <?= ImageHelper::getThumb('img/icons/before-page.png', [32, 32]); ?>
        </div>
        <?= $image; ?>
        <iframe class="show-video" width="636" height="314" src="<?= $video; ?>"
                frameborder="0" allowfullscreen style="display: none;">
        </iframe>
        <div class="loadPage" data-page="<?= $nextModal; ?>" style="padding-top: 9px;">
            <?= ImageHelper::getThumb('img/icons/next-page.png', [32, 32]); ?>
        </div>
    </div>
    <div class="col-xs-12 pad0"
         style="margin-top: 25px;text-align: left;font-size: 14px;padding: 5px 100px;">
        <div class="col-xs-1" style="width: 11.8%;"></div>
        <div class="col-xs-9 pad0" style="margin-top: 3px;text-align: center;">
            <div class="btn-video" style="display: none!important;">
                <div style="display: table-cell;vertical-align: middle;text-transform: uppercase;color: white; text-align: center;">
                    <?php ImageHelper::getThumb('img/icons/play-button.png', [30, 30], [
                        'style' => 'height: 25px;padding: 0 5px 2px 5px;',
                    ]); ?>
                    Видеоинструкция
                </div>
            </div>
            <div class="btn-start link" style="float: none;"
                 data-url="<?= $link; ?>">
                <div
                    style="display: table-cell;vertical-align: middle;text-transform: uppercase;color: white; text-align: center;">
                    <?= ImageHelper::getThumb('img/icons/cursor.png', [30, 30], [
                        'style' => 'height: 25px;padding: 0 38px 2px 10px;',
                    ]); ?>
                    Приступить
                </div>
            </div>
        </div>
        <div class="col-xs-1" style="width: 11.8%;"></div>
    </div>
    <div class="col-xs-12 not-show-check text-center" style="padding-top: 15px;padding-bottom: 5px;">
        <?= Html::checkbox(null, false, [
            'id' => 'not-show',
            'data-type' => $type,
            'label' => 'Спасибо, все понятно',
        ]); ?>
    </div>
</div>

