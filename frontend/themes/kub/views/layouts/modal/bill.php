<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.03.2018
 * Time: 7:09
 */

use common\components\ImageHelper;
use yii\helpers\Url;

?>
<div class="col-xs-12 pad0">
    <div class="col-xs-12 pad0">
        <?= ImageHelper::getThumb('img/modal_registration/block-6.jpg', [680, 340], [
            'class' => 'hide-video',
            'style' => 'max-width: 100%',
        ]); ?>
        <iframe class="show-video" width="680" height="340" src="https://www.youtube.com/embed/jXesTUBlxl4"
                frameborder="0" allowfullscreen style="display: none;">
        </iframe>
    </div>
    <div class="col-xs-12 pad0"
         style="margin-top: 25px;text-align: left;font-size: 14px;padding: 5px 100px;border-bottom: 2px dashed #f3f3f3;">
        <div class="col-xs-2 loadPage " style="padding-top: 10px" data-page="5">
            <?= ImageHelper::getThumb('img/icons/before-page.png', [32, 25]); ?>
        </div>
        <div class="col-xs-8 pad0">
            Пусть постоянные клиенты покупают без участия продавцов
        </div>
        <div class="col-xs-2 loadPage text-right" style="padding-top: 10px" data-page="1">
            <?= ImageHelper::getThumb('img/icons/next-page.png', [32, 25]); ?>
        </div>
        <div class="col-xs-12" style="padding: 40px 0px; ">
            <div class="btn-video hide-video">
                <div style="display: table-cell;vertical-align: middle;text-transform: uppercase;color: white; text-align: center;">
                    <?= ImageHelper::getThumb('img/icons/video-1.png', [113, 61], [
                        'style' => 'height: 25px;padding-right: 5px;',
                    ]); ?>
                    Как это работает?
                </div>
            </div>
            <div class="btn-start-work show-video link" data-url="<?= Url::to(['/out-invoice/create']); ?>" style="display: none;">
                <div style="display: table-cell;vertical-align: middle;text-transform: uppercase;color: white; text-align: center;">
                    Начать работу
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12" style="padding: 25px 0px">
        <div class="btn-ok text-center">
            Спасибо мне все понятно
        </div>
        <div class="text-center" style="margin-top: 10px; text-decoration: underline"><i> Больше не показывать это
                сообщение </i></div>
    </div>
</div>