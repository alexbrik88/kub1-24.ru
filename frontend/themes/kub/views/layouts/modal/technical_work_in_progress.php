<?php

$needShow = false;

$isViewed = Yii::$app->request->cookies->getValue('technical_work_in_progress');


if (!\Yii::$app->user->isGuest && !$isViewed) {

    $needShow = true;

    Yii::$app->response->cookies->add(new \yii\web\Cookie([
        'name' => 'technical_work_in_progress',
        'value' => 1,
        'expire' => time() + 3600 * 24
    ]));
}
?>

<?php if ($needShow): ?>

    <div id="modal_technical_work_in_progress" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-content">
                <h4 class="modal-title">Ведутся технические работы по обновлению сервиса</h4>
                При работе с сервисом возможны временные ошибки.<br/>
                Если у вас выдает ошибку, нужно одновременно нажать кнопки "CTRL" и "F5".<br/>
                Приносим извинения за доставленные неудобства.
            </div>
        </div>
    </div>
    
    <script>
        $('document').ready(function() { $('#modal_technical_work_in_progress').modal('show'); });
    </script>

    <style>
        #modal_technical_work_in_progress { z-index: 10051; }
        #modal_technical_work_in_progress .modal-header { display: none; }
    </style>

<?php endif; ?>