<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.04.2020
 * Time: 23:10
 */

use yii\widgets\Pjax;

?>
<div class="modal fade" id="article-dropdown-modal-container" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <span class="modal-header-description">
                Перед тем как добавить свою статью, проверьте, есть ли такая статья в предустановленных,
                но еще не используемых вами
            </span>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php Pjax::begin([
                    'id' => 'article-dropdown-form-container',
                    'enablePushState' => false,
                    'linkSelector' => false,
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
