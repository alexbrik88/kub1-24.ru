<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 09.04.2020
 * Time: 21:25
 */

use common\components\helpers\Html;
?>
<div id="preloader_modal" class="confirm-modal modal fade show" role="dialog" tabindex="-1"
     aria-modal="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4"></h4>
                <div class="text-center">
                    <?= Html::button('<span class="ladda-label">ОК</span><span class="ladda-spinner"></span>', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1 ladda-button ladda-custom',
                        'data-dismiss' => 'modal',
                        'data-style' => 'expand-right',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
