<?php

use common\models\service\ServiceModule;

use frontend\rbac\permissions\BusinessAnalytics;
use frontend\modules\crm\Module as CrmModule;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $currentModule string */
/** @var $currentController string */
/** @var $canBuhIP boolean */
/** @var $isDemo boolean */

$user = Yii::$app->user->identity;

$companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);
$allowedIds = ArrayHelper::getValue(Yii::$app->params, ['prjectSectionAllowedIds'], []);
$projectSectionAllowed = $companyId && $allowedIds && in_array($companyId, $allowedIds);

?>

<div class="dropdown-popup-in">
    <div class="products-list-title">Доступные продукты</div>
    <ul class="products-list list-clr border-bottom">
        <?php if (Yii::$app->user->can(BusinessAnalytics::START) || Yii::$app->user->can(BusinessAnalytics::INDEX)) : ?>
            <li class="<?= ($currentModule == ServiceModule::ANALYTICS) ? 'active' : '' ?>">
                <a class="products-list-link" href="<?= Url::to(['/site/change-product', 'product' => ServiceModule::ANALYTICS]) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('svg/analytics') ?>
                </span>
                    <span class="products-link-txt">ФинДиректор</span>
                </a>
            </li>
        <?php endif ?>
        <?php if (!$isDemo) : ?>
            <li class="<?= ($currentModule == ServiceModule::STANDART) ? 'active' : '' ?>">
                <a class="products-list-link" href="<?= Url::to(['/site/change-product', 'product' => ServiceModule::STANDART]) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('svg/invoicing') ?>
                </span>
                    <span class="products-link-txt">Выставление счетов</span>
                </a>
            </li>
        <?php endif ?>
        <?php if (!$isDemo && $canBuhIP) : ?>
            <li class="<?= ($currentModule == ServiceModule::IP_USN_6) ? 'active' : '' ?>">
                <a class="products-list-link" href="<?= Url::to(['/site/change-product', 'product' => ServiceModule::IP_USN_6]) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'mix']) ?>
                </span>
                    <span class="products-link-txt">
                    Налоги и Декларация ИП
                </span>
                </a>
            </li>
        <?php endif ?>
        <?php if (!$isDemo) : ?>
            <li class="<?= ($currentModule == ServiceModule::B2B_PAYMENT) ? 'active' : '' ?>">
                <a class="products-list-link" href="<?= Url::to(['/site/change-product', 'product' => ServiceModule::B2B_PAYMENT]) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'bank']) ?>
                </span>
                    <span class="products-link-txt">Бизнес платежи</span>
                </a>
            </li>
        <?php endif ?>
    </ul>
    <ul class="products-list list-clr">
        <?php if (!$isDemo && (YII_ENV_DEV || $projectSectionAllowed)) : ?>
            <li>
                <a class="products-list-link" href="<?= Url::to(['/project/index']) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'project']) ?>
                </span>
                    <span class="products-link-txt">Проекты</span>
                </a>
            </li>
        <?php endif ?>
        <?php if (!$isDemo && (YII_ENV_DEV || $projectSectionAllowed)) : ?>
            <li>
                <a class="products-list-link" href="<?= Url::to(['/rent/calendar']) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'time']) ?>
                </span>
                    <span class="products-link-txt">Объекты аренды</span>
                </a>
            </li>
        <?php endif ?>
        <?php if (!$isDemo && CrmModule::isEnabled()) : ?>
            <li>
                <a class="products-list-link" href="<?= Url::to(['/crm/deal/index']) ?>">
                <span class="notif-marker notif-marker_red">
                    <?= $this->render('//svg-sprite', ['ico' => 'crm']) ?>
                </span>
                    <span class="products-link-txt">CRM</span>
                </a>
            </li>
        <?php endif ?>
    </ul>
</div>