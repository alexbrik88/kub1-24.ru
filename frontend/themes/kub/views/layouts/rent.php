<?php

use frontend\assets\RentAsset;
use frontend\components\Icon;
use yii\bootstrap4\Tabs;
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\RentChart;

/* @var $this yii\web\View*/

RentAsset::register($this);

$this->beginContent('@frontend/views/layouts/main.php');

$route = Yii::$app->controller->action->getUniqueId();

$company = Yii::$app->user->identity->company;
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->marketing_help ?? false;
$showChartPanel = $userConfig->marketing_chart ?? false;
$chartModel = new RentChart($company);
?>

<?php /*
<div class="stop-zone-for-fixed-elems product-index">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>

        <?= Html::a(Icon::get('add-icon').'<span class="ml-1">Договор аренды</span>', ['/rent-agreement/create'], [
            'class' => 'button-regular button-regular_padding_medium button-regular_red ml-auto mb-2',
        ]) ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-xl-9"></div>

            <div class="col-6 col-xl-3 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
</div>*/ ?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0 select2-wrapper" style="margin-top:-9px">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="marketing_chart">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('@frontend/views/rent/chart/chart', [
            'model' => $chartModel
        ]); ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="marketing_help">
        <div class="pt-4 pb-3">
            <span class="label">В разработке</span>
        </div>
    </div>

<div class="row" style="margin-top:12px">

    <div class="column">
        <?= Tabs::widget([
            'options' => [
                'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            ],
            'linkOptions' => [
                'class' => 'nav-link',
            ],
            'tabContentOptions' => [
                'style' => 'width:100%; padding-bottom:12px!important'
            ],
            'headerOptions' => [
                'class' => 'nav-item',
            ],
            'items' => [
                [
                    'label' => 'Календарь',
                    'url' => ['/rent/calendar'],
                    'linkOptions' => [
                        'data-tab' => '_productInfo'
                    ],
                    'active' => $route == 'rent/calendar',
                ],
                [
                    'label' => 'Договоры аренды',
                    'url' => ['/rent-agreement/index'],
                    'linkOptions' => [
                        'data-tab' => '_productSupplier'
                    ],
                    'active' => $route == 'rent-agreement/index',
                ],
                [
                    'label' => 'Объекты',
                    'url' => ['/rent/entity-list'],
                    'linkOptions' => [
                        'data-tab' => '_productSupplier'
                    ],
                    'active' => $route == 'rent/entity-list',
                ],
            ],
        ]) ?>
    </div>
    <div class="column ml-auto">
        <?= Html::a(Icon::get('add-icon').'<span class="ml-1">Договор аренды</span>', ['/rent-agreement/create'], [
            'class' => 'button-regular button-regular_padding_medium button-regular_red ml-auto mb-2',
        ]) ?>
    </div>
</div>

<?= $content ?>

<?php $this->endContent(); ?>
