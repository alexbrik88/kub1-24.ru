<?php

use common\models\Inquirer;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Inquirer */
/* @var $form ActiveForm */

$model = new Inquirer;
?>

<div class="site-inquirer">
    <?php $form = ActiveForm::begin([
        'id' => 'inquirer-form',
        'action' => Url::to(['/site/inquirer']),
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <div class="form-group">
        Меня зовут Арслан, я технический директор КУБ.
        Мы постоянно работаем над улучшением взаимодействия пользователей с нашим сайтом.
        Расскажите, пожалуйста, что вы думаете о нашем сайте ниже.
    </div>

    <div class="form-group">
        <div class="bold mb-2">Рейтинг</div>
        <?= $form->field($model, 'rating')->label(false)
            ->radioList([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::radio($name, $checked, [
                            'value' => $value,
                            'label' => $label,
                            'labelOptions' => [
                                'class' => 'radio-inline mr-3 mb-0',
                            ],
                        ]);
                },
            ])
            ->hint(Html::tag(
                'div',
                Html::tag('span', 'Плохо', ['class' => 'label', 'style' => 'margin-left: 0; margin-top: 3px']) .
                Html::tag('span', 'Отлично', ['class' => 'label', 'style' => 'margin-left: 170px; margin-top: 3px'])
            )) ?>
    </div>

    <div class="inquirer-field-group low-rating-group" style="display: none">
        <div class="mb-2 bold">
            Будем признательны, если напишите, с какими трудностями вы столкнулись
        </div>
        <?= $form->field($model, 'difficulty')->textArea()->label(false) ?>
    </div>

    <div class="inquirer-field-group high-rating-group" style="display: none">
        <div class="mb-2 bold">
            Планируете ли вы использовать КУБ в будущем?
        </div>
        <?= $form->field($model, 'plan_id')->label(false)->radioList(Inquirer::$planLabels, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline mr-3',
                    ],
                ]);
            },
        ]) ?>
    
        <div class="mb-2 bold">
            Сервис помогает решить вашу проблему? Кстати какую?
        </div>
        <?= $form->field($model, 'problem')->textArea(['placeholder' => 'Приятно будет услышать от вас пару слов:)'])->label(false) ?>
    </div>

    <div class="d-flex flex-wrap justify-content-between mt-5">
        <button class="button-regular button-regular_red button-width" type="submit">
            Отправить
        </button>
        <button class="button-regular button-hover-transparent button-width" data-toggle="toggleVisible" data-target="inquirer">
            Отменить
        </button>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- site-inquirer -->