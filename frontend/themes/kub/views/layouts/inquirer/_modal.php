<?php

$company = \Yii::$app->user->identity->company;

if (Yii::$app->session->remove('show_inquirer_popup') && !$company->getInquirers()->exists()): ?>

    <div id="inquirer-modal" class="right-modal-wrap" data-id="inquirer">
        <div class="right-modal-wrap-in right-modal-wrap-scroll" style="padding-bottom: 0;">
            <button class="right-modal-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="inquirer">
                <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
            </button>
            <div class="right-modal-wrap-title">Здравствуйте!</div>
            <?= $this->render('_form') ?>
        </div>
    </div>

    <?php $this->registerJs('
    
        var needShowInquirer = true;
        
        $(document).mouseleave(function () {
            if (needShowInquirer) {
                $("#inquirer-modal").toggleClass("visible show");
                needShowInquirer = false;
            }
        });
        
        $(document).on("change", "input[name=\"Inquirer[rating]\"]", function (e) {
            console.log(this.value);
            $(".inquirer-field-group").hide();
            if ($.inArray(this.value, ["1", "2", "3"]) > -1) {
                $(".low-rating-group").show();
                console.log("low");
            } else if ($.inArray(this.value, ["4", "5"]) > -1) {
                $(".high-rating-group").show();
                console.log("high");
            }
        });
    '); ?>

<?php endif; ?>
