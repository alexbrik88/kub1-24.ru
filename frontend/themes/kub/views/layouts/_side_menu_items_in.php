<div id="item<?=$id?>Content" class="sidebar-menu-collapse-in collapse" aria-labelledby="item<?=$id?>" data-parent="<?=$parent?>">
    <div class="sidebar-menu-body">
        <ul class="sidebar-menu-list list-clr <?= $listClass ?? '' ?>" <?=(isset($ul_id)) ? "id=\"{$ul_id}\"" : "" ?>>
            {items}
        </ul>
    </div>
</div>