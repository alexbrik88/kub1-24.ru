<?php

use backend\models\Bank;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\rbac\permissions;
use common\components\ImageHelper;

if (!Yii::$app->user->isGuest) : ?>
    <?php

    $linkText = '';
    /* @var $user Employee */
    $user = Yii::$app->user->identity;
    $company = $user->company;
    $canCreateCash = $company->strict_mode != Company::ON_STRICT_MODE && Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
    $canCreateProduct = $company->strict_mode != Company::ON_STRICT_MODE && Yii::$app->user->can(\frontend\rbac\permissions\Product::CREATE);
    $actualSubscribes = $company ? SubscribeHelper::getPayedSubscriptions($company->id) : [];
    $expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
    $expireDays = SubscribeHelper::getExpireLeftDays($expireDate);
    $isTrial = (count($actualSubscribes) === 1 && $actualSubscribes[0]->tariff_id == SubscribeTariff::TARIFF_TRIAL);
    $canViewAutoAct = $canViewAutoInvoice = Yii::$app->user->can(permissions\document\Document::INDEX, [
        'ioType' => Yii::$app->request->get('type', null),
    ]);
    $canViewPaymentReminder = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_MANAGER, EmployeeRole::ROLE_SUPERVISOR, EmployeeRole::ROLE_SUPERVISOR_VIEWER,]);
    $canViewSaleIncrease = Yii::$app->user->can(permissions\Contractor::INDEX, [
        'type' => Contractor::TYPE_CUSTOMER,
    ]);
    $canActivate = $canViewAutoAct || $canViewAutoInvoice || $canViewPaymentReminder || $canViewSaleIncrease;
    $canCreateDocument = Yii::$app->user->can(permissions\document\Document::CREATE);
    $canBusinessAnalyticsStart = Yii::$app->user->can(permissions\BusinessAnalytics::START);
    $canCashIndex = Yii::$app->user->can(frontend\rbac\permissions\Cash::INDEX);
    $canProductIndex = Yii::$app->user->can(\frontend\rbac\permissions\Product::INDEX);
    $canInvoiceIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => Documents::IO_TYPE_OUT]);

    echo Html::hiddenInput(null, Yii::$app->user->identity->id, [
        'class' => 'identity_employee_id',
    ]);
    echo Html::hiddenInput(null, YII_ENV_DEV ? 8085 : 8086, [
        'class' => 'chat-port',
    ]);

    $banking = [
        'logo' => '',
    ];

    $bankItems = [];
    /* @var $bankArray CheckingAccountant[] */
    $bankArray = $company->getCheckingAccountants()
        ->groupBy('rs')
        ->indexBy('rs')
        ->orderBy('type')
        ->all();
    if ($bankArray && count($bankArray) == 1) {
        foreach ($bankArray as $account) {
            if (($bankingClass = Banking::classByBik($account->bik)) !== null) {
                if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
                    $banking['logo'] = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29], [
                        'class' => 'ml-1',
                    ]);
                    break;
                }
            }
        }
    }
    $isAnalyticsMenu = Yii::$app->controller->module->id == 'analytics' || Yii::$app->user->identity->currentEmployeeCompany->at_business_analytics_now ?? false;
    $mainCashbox = $company->getCashboxes()->where(['is_main' => 1])->select('id')->one();
    ?>

    <div id="quick_access_unit" class="dropdown dropdown_plus">
        <button class="button-clr dropdown-btn dropdown-toggle" type="button" id="dropdownPlus" aria-haspopup="true" aria-expanded="false">
            <?= $this->render('//svg-sprite', ['ico' => 'increase-icon', 'class' => 'header-plus-icon']) ?>
        </button>
        <div class="dropdown-popup dropdown-popup_plus dropdown-menu header-plus-menu">
            <?php if (!$isAnalyticsMenu): ?>
                <!-- STD MENU -->
                <ul class="header-plus-list columns-2 list-clr">
                <li class="title">Добавить</li>
                <li class="title"></li>
                <li>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                        'ioType' => Documents::IO_TYPE_OUT,
                    ])
                    ): ?>
                        <?= Html::a('Счет покупателю', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]), [
                            'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT) ?
                                'btn-p' :
                                'btn-p action-is-limited',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if ($canCreateCash): ?>
                        <?= Html::a('Операцию по банку', [
                            '/cash/bank/index',
                            'show_add_modal' => 1
                        ], [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ])
                    ): ?>
                        <?= Html::a('Счет от поставщика', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_IN]), [
                            'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_IN) ?
                                'btn-p' :
                                'btn-p action-is-limited',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if ($canCreateCash): ?>
                        <?= Html::a('Операцию по кассе', [
                            '/cash/order/index',
                            'show_add_modal' => 1
                        ], [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if ($canCreateProduct): ?>
                        <?= Html::a('Товар', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_GOODS]), [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_CUSTOMER])): ?>
                        <?= Html::a('Покупателя', Url::to(['/contractor/create', 'type' => Contractor::TYPE_CUSTOMER]), [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if ($canCreateProduct): ?>
                        <?= Html::a('Услугу', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]), [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_SELLER])): ?>
                        <?= Html::a('Поставщика', Url::to(['/contractor/create', 'type' => Contractor::TYPE_SELLER]), [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE)): ?>
                        <?= Html::a('Договор',
                            Url::to(['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT, 'modal' => 1]
                            ), [
                                'class' => 'btn-p',
                                'data-url' => '/documents/agreement/index?modal=1'
                            ]); ?>
                    <?php endif; ?>
                </li>
                <li></li>
                <li>
                    <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Employee::CREATE)): ?>
                        <?= Html::a('Сотрудника', Url::to(['/employee/create']), [
                            'class' => 'btn-p',
                        ]); ?>
                    <?php endif; ?>
                </li>
                </ul>
                <ul class="header-plus-list list-clr">
                    <?php if ($canCreateCash || $canCreateProduct) : ?>
                        <li class="title title-pad2">Загрузить</li>
                        <li class="title title-pad2"></li>
                        <?php if ($canCreateCash): ?>
                            <li class="btn-line">
                                <?= Html::a('Выписку из банка', [
                                    '/cash/banking/default/index',
                                    'p' => Banking::routeEncode(['/cash/bank/index']),
                                ], [
                                    'class' => 'btn-p',
                                ]); ?>
                                <?= $banking['logo'] ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canCreateCash): ?>
                            <li>
                                <?= Html::a('Операции по кассе из Excel', [
                                    '/cash/order/index',
                                    'show_xls_modal' => 1
                                ], [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canCreateCash): ?>
                            <li class="btn-line">
                                <?= Html::a('Данные из ОФД', [
                                    '/cash/ofd/default/index',
                                    'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode(['/cash/order/index']),
                                    'cashbox_id' => ($mainCashbox) ? $mainCashbox->id : null
                                ], [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canCreateProduct) : ?>
                            <li class="btn-line">
                                <?= Html::a('Товары из таблицы Excel', Url::to(['/product/index', 'productionType' => 1, 'modal' => true]), [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canCreateProduct) : ?>
                            <li class="btn-line">
                                <?= Html::a('Услуги из таблицы Excel', Url::to(['/product/index', 'productionType' => 0, 'modal' => true]), [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif ?>
                    <?php endif ?>
                </ul>
                <ul class="header-plus-list list-clr">
                    <?php if ($canActivate): ?>
                        <li class="title title-pad2">Включить</li>
                        <li class="title title-pad2"></li>
                        <?php if ($canViewAutoInvoice): ?>
                            <li class="btn-line">
                                <?= Html::a('Рассылку АвтоСчетов', Url::to(['/documents/invoice/index-auto',]), [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canViewAutoAct): ?>
                            <li class="btn-line">
                                <?= Html::a('Выставление АвтоАктов', Url::to(['/documents/act/index', 'type' => Documents::IO_TYPE_OUT, 'autoActModal' => true]), [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($canViewPaymentReminder && false): ?>
                            <li class="btn-line">
                                <?= Html::a('Рассылку писем должникам', Url::to(['/payment-reminder/index',]), [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                        <?php endif; ?>
                        <?php /* if ($canViewSaleIncrease): ?>
                        <li class="btn-line">
                            <?= Html::a('Увеличение онлайн продаж', Url::to(['/contractor/sale-increase',]), [
                                'class' => 'btn-p',
                            ]); ?>
                        </li>
                    <?php endif; */ ?>
                    <?php endif; ?>
                </ul>
            <?php else: ?>
                <!-- ANALYTICS MENU -->
                <div class="row m-0">
                    <div class="col-8 pt-0 pb-0 pl-0 pr-0" style="border-right: 1px solid #e2e5eb">
                        <ul class="header-plus-list list-clr columns-2">
                            <?php if ($isDemo || $canCreateCash || $canCreateProduct) : ?>
                                <li class="title title-pad2">Загрузить данные</li>
                                <li class="title title-pad2"></li>
                                <?php if ($isDemo || $canCreateCash): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Выписку из банка', [
                                            '/cash/banking/default/index',
                                            'p' => Banking::routeEncode(['/cash/bank/index']),
                                        ], [
                                            'class' => 'btn-p',
                                        ]); ?>
                                        <?= $banking['logo'] ?>
                                    </li>
                                <?php endif; ?>
                                <li class="btn-line">
                                    <?= Html::a('Данные из 1С', Url::to(['/import/one-s/index']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                                <?php if ($isDemo || $canCreateCash): ?>
                                    <li>
                                        <?= Html::a('Операции по кассе из Excel', [
                                            '/cash/order/index',
                                            'show_xls_modal' => 1
                                        ], [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($isDemo || $canCreateCash): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Данные из ОФД', [
                                            '/cash/ofd/default/index',
                                            'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode(['/cash/order/index']),
                                            'cashbox_id' => ($mainCashbox) ? $mainCashbox->id : null
                                        ], [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($isDemo || $canCreateProduct) : ?>
                                    <li class="btn-line">
                                        <?= Html::a('Товары из таблицы Excel', Url::to(['/product/index', 'productionType' => 1, 'modal' => true]), [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <li class="btn-line">
                                    <?= Html::a('Данные из рекл. каналов', Url::to(['/analytics/marketing/default/upload-data']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                                <?php if ($isDemo || $canCreateProduct) : ?>
                                    <li class="btn-line">
                                        <?= Html::a('Услуги из таблицы Excel', Url::to(['/product/index', 'productionType' => 0, 'modal' => true]), [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif ?>
                            <?php endif ?>
                        </ul>
                        <ul class="header-plus-list columns-2 list-clr">
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li class="title">Добавить</li>
                            <li class="title"></li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ])
                                ): ?>
                                    <?= Html::a('Счет покупателю', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]), [
                                        'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT) ?
                                            'btn-p' :
                                            'btn-p action-is-limited',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || $canCreateCash): ?>
                                    <?= Html::a('Операцию по банку', [
                                        '/cash/bank/index',
                                        'show_add_modal' => 1
                                    ], [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ])
                                ): ?>
                                    <?= Html::a('Счет от поставщика', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_IN]), [
                                        'class' => \Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_IN) ?
                                            'btn-p' :
                                            'btn-p action-is-limited',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || $canCreateCash): ?>
                                    <?= Html::a('Операцию по кассе', [
                                        '/cash/order/index',
                                        'show_add_modal' => 1
                                    ], [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || $canCreateProduct): ?>
                                    <?= Html::a('Товар', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_GOODS]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?= Html::a('Плановый платеж', [
                                    '/analytics/finance/payment-calendar',
                                    'show_add_modal' => 1
                                ], [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                            <li>
                                <?php if ($isDemo || $canCreateProduct): ?>
                                    <?= Html::a('Услугу', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?= Html::a('Статью Прихода/Расхода', [
                                    '/reference/articles/index',
                                    'type' => 1,
                                    'show_add_modal' => 1
                                ], [
                                    'class' => 'btn-p',
                                ]); ?>
                            </li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(\frontend\rbac\permissions\Contractor::CREATE)): ?>
                                    <?= Html::a('Договор',
                                        Url::to(['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT, 'modal' => 1]
                                        ), [
                                            'class' => 'btn-p',
                                            'data-url' => '/documents/agreement/index?modal=1'
                                        ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(\frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_CUSTOMER])): ?>
                                    <?= Html::a('Покупателя', Url::to(['/contractor/create', 'type' => Contractor::TYPE_CUSTOMER]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(\frontend\rbac\permissions\Employee::CREATE)): ?>
                                    <?= Html::a('Сотрудника', Url::to(['/employee/create']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($isDemo || Yii::$app->user->can(\frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_SELLER])): ?>
                                    <?= Html::a('Поставщика', Url::to(['/contractor/create', 'type' => Contractor::TYPE_SELLER]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4 pt-0 pb-0 pl-4 pr-0">
                        <ul class="header-plus-list list-clr">
                            <li class="title title-pad2">Перейти в</li>
                            <?php if ($isDemo || $canBusinessAnalyticsStart): ?>
                                <li class="btn-line">
                                    <?= Html::a('Уровни Бизнес Аналитики', Url::to(['/analytics/options/levels']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php if ($isDemo || $canCashIndex): ?>
                                <li class="btn-line">
                                    <?= Html::a('Реестр операций по Банку', Url::to(['/cash/bank/index']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php if ($isDemo || $canCashIndex): ?>
                                <li class="btn-line">
                                    <?= Html::a('Реестр операций по Кассе', Url::to(['/cash/order/index']), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php if ($isDemo || $canProductIndex): ?>
                                <li class="btn-line">
                                    <?= Html::a('Список Товаров', Url::to(['/product/index', 'productionType' => 1, 'ProductSearch[filterStatus]' => 2]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php if ($isDemo || $canInvoiceIndex): ?>
                                <li class="btn-line">
                                    <?= Html::a('Список Счетов', Url::to(['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT]), [
                                        'class' => 'btn-p',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <ul class="header-plus-list list-clr">
                            <?php if ($canActivate): ?>
                                <li class="title title-pad2">Включить</li>
                                <?php if ($isDemo || $canViewAutoInvoice): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Рассылку АвтоСчетов', Url::to(['/documents/invoice/index-auto',]), [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($isDemo || $canViewAutoAct): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Выставление АвтоАктов', Url::to(['/documents/act/index', 'type' => Documents::IO_TYPE_OUT, 'autoActModal' => true]), [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($isDemo || $canViewPaymentReminder && false): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Рассылку писем должникам', Url::to(['/payment-reminder/index',]), [
                                            'class' => 'btn-p',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if ($isAnalyticsMenu): // wide menu ?>
        <style>
            .dropdown-popup_plus { width:680px!important; right:-213px!important; }
            .dropdown_plus .dropdown-popup::after { right: 222px !important; }
        </style>
    <?php endif; ?>
<?php endif; ?>