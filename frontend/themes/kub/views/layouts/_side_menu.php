<?php

use common\components\image\EasyThumbnailImage;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\document\SalesInvoice;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\crm\Module as CrmModule;
use frontend\modules\crm\controllers\SettingControllerInterface;
use frontend\modules\crm\widgets\CrmHotTasksWidget;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;
use \frontend\components\BusinessAnalyticsAccess;

$visibleOutInvoiceFacture = false;
$visibleInInvoiceFacture = false;
$isIp = false;
$logo = $this->render('svg/company');
$companyItems = [];
$userItems = [];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-side_menu',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'position' => 'right',
    ],
]);

if ($user) {
    if ($company) {
        $visibleByTaxationType = $company->companyTaxationType->osno;
        $visibleSalesInvoices = SalesInvoice::billExists($company->id);
        $visibleOutInvoiceFacture = $visibleByTaxationType ||
            InvoiceFacture::find()
                ->alias('if')
                ->joinWith('invoice invoice')
                ->where([
                    'invoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                    'if.type' => Documents::IO_TYPE_OUT
                ])
                ->exists();
        $visibleInInvoiceFacture = $visibleByTaxationType ||
            InvoiceFacture::find()
                ->alias('if')
                ->joinWith('invoice invoice')
                ->where([
                    'invoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                    'if.type' => Documents::IO_TYPE_IN
                ])
                ->exists();
        $isIp = $company->company_type_id == CompanyType::TYPE_IP;


        $imgPath = $company->getImage('logoImage');
        if (is_file($imgPath)) {
            $logo = EasyThumbnailImage::thumbnailImg($imgPath, 22, 22, EasyThumbnailImage::THUMBNAIL_INSET, [
                'style' => 'max-width: 100%; max-height: 100%;',
            ]);
        }

        // todo:temp
        $visibleAnalyticsDevModule = (YII_ENV_DEV || in_array($company->id, [112, 486, 628, 11270, 23083, 53146]));
    }

    $companyId = $user->company ? $user->company->id : null;
    $employeeCompanyArray = $user->getEmployeeCompany()
        ->joinWith('company.companyType')
        ->andWhere([
            'employee_company.is_working' => true,
            'company.blocked' => false,
        ])->orderBy([
            new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
            "company_type.name_short" => SORT_ASC,
            "company.name_short" => SORT_ASC,
        ])->all();
    $imgPath = $company->getImage('logoImage');
    if (is_file($imgPath)) {
        $mainLogo = EasyThumbnailImage::thumbnailImg($imgPath, 40, 40, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $mainLogo = $this->render('svg/company');
    }
    $companyArray = ArrayHelper::getColumn($employeeCompanyArray, 'company');
    foreach ($companyArray as $item) {
        if ($item->id != $companyId) {
            $companyItems[] = [
                'label' => Html::encode($item->shortTitle),
                'encode' => false,
                'url' => [
                    '/site/change-company',
                    'id' => $item->id,
                ],
                'active' => $item->id == $companyId,
            ];
        }
    }

    if (!trim($company->name_short) || !trim($company->inn)) {
        $companyItems[] = [
            'label' => Icon::get('add-icon', ['class' => 'add-button-icon mr-2']).'Добавить компанию',
            'encode' => false,
            'url' => ['/company/update'],
        ];
    } else {
        $companyItems[] = [
            'label' => Icon::get('add-icon', ['class' => 'add-button-icon mr-2']).'Добавить компанию',
            'encode' => false,
            'url' => '#create-company',
            'template' => '<a data-toggle="modal" href="{url}">{label}</a>'
        ];
    }

    $userItems = [
        [
            'label' => 'Профиль',
            'url' => ['/profile/index'],
        ],
        //[
        //    'label' => 'Мои документы',
        //    'url' => '#',
        //],
        [
            'label' => 'Помощь',
            'url' => '#',
        ],
    ];

    $analyticsActivated = $company->analytics_module_activated ?? false;
    $canAnalytics = Yii::$app->user->can(permissions\BusinessAnalytics::INDEX);
    $startAnalytics = Yii::$app->requestedRoute == 'analytics/options/start';
    $canMarketing =  Yii::$app->user->can(UserRole::ROLE_MARKETER);
    $hideForMarketer = Yii::$app->user->can(UserRole::ROLE_MARKETER); // todo: temp
    $isAnalyticsMenu = $module == 'analytics' || Yii::$app->user->identity->currentEmployeeCompany->at_business_analytics_now ?? false;
    $isPartner = (bool)$company->is_partner;
    $isPartnerRelationsManager = Yii::$app->user->can(UserRole::ROLE_PARTNER_RELATIONS_MANAGER);
    if ($isPartnerRelationsManager) {
        array_pop($companyItems);
    }

    $companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);
    $allowedIds = ArrayHelper::getValue(Yii::$app->params, ['prjectSectionAllowedIds'], []);
    $projectSectionAllowed = $companyId && $allowedIds && in_array($companyId, $allowedIds);
    if (!$isAnalyticsMenu) {

    $items = [
        [
            'label' => 'Мои компании',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => "company1", 'i' => 'home']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'items' => $companyItems,
            'visible' => !$isDemo,
        ],
        [
            'label' => $company ? Html::encode($company->shortTitle) : '',
            'encode' => false,
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => null, 'img' => $mainLogo]),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'url' => ['/site/index'],
            'active' => $controller == 'site' && $action == 'index',
        ],
        //=================================================================================================
        [
            'label' => 'Модуль В2В',
            'url' => ['/b2b/index'],
            'template' => $this->render('_side_menu_link', ['i' => 'chart']),
            'active' => $controller == 'b2b',
            'options' => [
                'class' => 'sideabar-menu-item b2b-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.b2b_item') ? '' : ' hidden'),
            ],
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
        ],
        [
            'label' => 'CRM',
            'url' => ['/crm/deal/index'],
            'template' => $this->render('_side_menu_btn', [
                'i' => 'crm',
                'id' => 'crm-items',
                'append' => Html::tag('span', CrmHotTasksWidget::widget(), [
                    'class' => 'sidebar-menu-append expand_hide ml-1',
                ]),
            ]),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'crm-items']),
            'active' => ($module == 'crm'),
            'options' => [
                'class' => 'sideabar-menu-item crm-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.crm_item') ? '' : ' hidden'),
            ],
            'items' => [
                [
                    'label' => 'Сделки',
                    'url' => ['/crm/deal/index'],
                    'active' => ($controller == 'deal'),
                ],
                [
                    'label' => Html::tag('span', '<span>Задачи</span>'.Html::tag('span', CrmHotTasksWidget::widget(), [
                        'class' => 'sidebar-menu-append ml-auto mr-1',
                    ]), [
                        'class' => 'd-flex w-100',
                        'style' => 'padding-right: 14px;',
                    ]),
                    'encode' => false,
                    'url' => ['/crm/task/index'],
                    'active' => ($controller == 'task'),
                ],
                [
                    'label' => 'Клиенты',
                    'url' => ['/crm/clients/index'],
                    'active' => ($controller == 'clients' || $controller == 'client'),
                ],
                [
                    'label' => 'Отчеты',
                    'url' => ['/crm/reports/refusal'],
                    'active' => (Yii::$app->controller->getUniqueId() == 'crm/reports'),
                ],
                [
                    'label' => 'Настройки',
                    'url' => ['/crm/deal-type/index'],
                    'active' => (Yii::$app->controller instanceof SettingControllerInterface),
                ],
            ],
            'visible' => CrmModule::isEnabled(),
        ],
        [
            'label' => 'Покупатели',
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_CUSTOMER],
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'basket']),
            'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_CUSTOMER,
            'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_CUSTOMER,
            ]),
        ],
        [
            'label' => 'Поставщики',
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_SELLER],
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'bagagge']),
            'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_SELLER,
            'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_SELLER,
            ]),
        ],
        [
            'label' => 'Продажи',
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 1, 'i' => 'sales']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 1]),
            'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_OUT,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'invoice' && !$paramDocument) && $paramType != Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Товарные накладные',
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'ТТН',
                    'url' => ['/documents/waybill/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'waybill' || $controller == 'invoice' && $paramDocument == 'waybill') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'УПД',
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Инвойсы',
                    'url' => ['/documents/foreign-currency-invoice/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller == 'foreign-currency-invoice' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Расходные накладные',
                    'url' => ['/documents/sales-invoice/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'sales-invoice' || $paramDocument == 'sales-invoice') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => $visibleSalesInvoices,
                ],
                [
                    'label' => 'Счета-фактуры',
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => $visibleOutInvoiceFacture,
                ],
                [
                    'label' => 'Заказы',
                    'url' => ['/documents/order-document/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => $controller === 'order-document' && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Договоры',
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]),
                ],
                [
                    'label' => 'Розничные продажи',
                    'url' => ['/retail/dashboard/index'],
                    'active' => $firstModule === 'retail',
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                ],
            ],
        ],
        [
            'label' => 'Покупки',
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 2, 'i' => 'purchase']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 2]),
            'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_IN,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'invoice' && !$paramDocument) && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Товарные накладные',
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'УПД',
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Инвойсы',
                    'url' => ['/documents/foreign-currency-invoice/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller == 'foreign-currency-invoice' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Счета-фактуры',
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_IN,
                    'visible' => $visibleInInvoiceFacture,
                ],
                [
                    'label' => 'Заказы',
                    'url' => ['/documents/order-document/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'order-document' && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Платёжные поручения',
                    'url' => ['/documents/payment-order/index'],
                    'active' => $controller === 'payment-order',
                    'visible' => Yii::$app->user->can(permissions\document\PaymentOrder::INDEX),
                ],
                [
                    'label' => 'Распознавание сканов',
                    'url' => ['/documents/upload-documents/index'],
                    'active' => $controller === 'upload-documents',
                    'visible' => false,
                ],
                [
                    'label' => 'Договоры',
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_IN,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_SELLER,
                    ])
                ],
                [
                    'label' => 'Доверенности',
                    'url' => ['/documents/proxy/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'proxy' && $paramType == Documents::IO_TYPE_IN,
                ],
            ],
        ],
        [
            'label' => 'Деньги',
            'encode' => false,
            'url' => 'javascript:;',
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item accountant-trigger_menu-item ' . (
                    ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.analytics_item') ||
                    ArrayHelper::getValue($user, 'menuItem.accountant_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                    ),
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 'money1', 'i' => 'ruble-2']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'money1']),
            'active' => $module == 'cash' && !($controller == 'default' && $action == 'operations'),
            'visible' => Yii::$app->user->can(permissions\Cash::INDEX) ||
                Yii::$app->user->can(permissions\CashOrder::INDEX),
            'items' => [
                [
                    'label' => 'Банк',
                    'url' => ['/cash/bank/index'],
                    'active' => $module == 'cash' && $controller == 'bank',
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                ],
                [
                    'label' => 'Касса',
                    'url' => ['/cash/order/index'],
                    'active' => $module == 'cash' && $controller == 'order',
                    'visible' => Yii::$app->user->can(permissions\CashOrder::INDEX),
                ],
                [
                    'label' => 'Эквайринг',
                    'url' => ['/acquiring'],
                    'active' => $controller == 'acquiring',
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                ],
                [
                    'label' => 'Карты',
                    'url' => ['/cards/default/index'],
                    'active' => ($module == 'cards'),
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                ],
                [
                    'label' => 'E-money',
                    'url' => ['/cash/e-money/index'],
                    'active' => $module == 'cash' && $controller == 'e-money',
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                ],
                [
                    'label' => 'Итого',
                    'url' => ['/cash/default/index'],
                    'active' => $module == 'cash' && $controller == 'default' && $action != 'operations',
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                ],
            ]
        ],
        [
            'label' => 'Проекты',
            'options' => [
                'class' => 'sideabar-menu-item project-trigger_menu-item ' . (
                    ArrayHelper::getValue($user, 'menuItem.project_item') ? '' : ' hidden'
                ),
            ],
            'url' => ['/project/index'],
            'template' => $this->render('_side_menu_link', ['i' => 'project']),
            'active' => $controller == 'project',
            'visible' => Yii::$app->user->can(permissions\Project::INDEX),
        ],
        [
            'label' => 'Услуги',
            'url' => [
                '/product/index',
                'productionType' => Product::PRODUCTION_TYPE_SERVICE,
                'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
            ],
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'tools']),
            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_SERVICE,
            'visible' => Yii::$app->user->can(permissions\Product::INDEX) && !$hideForMarketer && !$isPartnerRelationsManager,
        ],
        [
            'label' => 'Товары',
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item' .
                    (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 3, 'i' => 'buildings']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 3]),
            'visible' => Yii::$app->user->can(permissions\Product::INDEX) && !$hideForMarketer && !$isPartnerRelationsManager,
            'items' => [
                [
                    'label' => 'Склад',
                    'url' => [
                        '/product/index',
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                    ],
                    'active' => $controller == 'product' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        in_array($action, ['index', 'view', 'create', 'update']),
                ],
                [
                    'label' => 'Прайс-листы',
                    'encode' => false,
                    'url' => ['/price-list', 'per-page' => 20],
                    'active' => $controller == 'price-list',
                    'visible' => !$isDemo,
                ],
                [
                    'label' => 'Оборот товара',
                    'url' => [
                        '/product/turnover',
                        'per-page' => 20,
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'turnoverType' => ProductSearch::TURNOVER_BY_COUNT,
                    ],
                    'active' => $controller == 'product' &&
                        $action == 'turnover' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_COUNT,
                ],
                [
                    'label' => 'Оборот ₽',
                    'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                        && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_AMOUNT,
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ]),
                ],
                [
                    'label' => 'Списание товара',
                    'url' => '/documents/goods-cancellation/index',
                    'active' => $controller == 'goods-cancellation',
                    'visible' => Yii::$app->user->can(permissions\Product::WRITE_OFF),
                ],
            ],
        ],
        [
            'label' => 'Логистика',
            'options' => [
                'class' => 'sideabar-menu-item invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                    ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                    ),
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 7, 'i' => 'logistics-icon']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 7]),
            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
            'visible' => YII_ENV_DEV,
            'items' => [
                [
                    'label' => '<span class="title">Заявки</span>',
                    'encode' => false,
                    'url' => ['/logistics/request/index'],
                    'active' => $module == 'logistics' && $controller == 'request' && ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                ],
                [
                    'label' => '<span class="title">ТТН</span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                ],
                [
                    'label' => '<span class="title">Путевые листы</span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                ],
                [
                    'label' => '<span class="title">Отчеты</span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                ],
                [
                    'label' => '<span class="title">Справочники</span>',
                    'encode' => false,
                    'url' => ['/logistics/driver/index'],
                    'active' => $module == 'logistics' && ($controller == 'driver' || $controller == 'vehicle' || $controller == 'address') &&
                        ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                ],
            ],
        ],
        [
            'label' => 'Объекты аренды',
            'url' => ['/rent/calendar'],
            'options' => [
                'class' => 'sideabar-menu-item rent-trigger_menu-item ' . (
                    ArrayHelper::getValue($user, 'menuItem.rent_item') ? '' : ' hidden'
                ),
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'time']),
            'active' => $controller == 'rent',
            'visible' => Yii::$app->user->can(permissions\Rent::VIEWER),
        ],
        [
            'label' => 'Отчеты',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 4, 'i' => 'chart']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 4]),
            'visible' => Yii::$app->user->can(permissions\Reports::VIEW),
            'items' => [
                [
                    'label' => 'Финансы',
                    'url' => ['/reports/finance/odds'],
                    'visible' => Yii::$app->user->can(permissions\Reports::FINANCE),
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && !in_array($action, ['profit-and-loss', 'balance', 'debtor', 'breakeven-point'])
                        || $module == 'reference' && $controller == 'articles',
                ],
                [
                    'label' => 'Отчеты',
                    'visible' => Yii::$app->user->can(permissions\Reports::REPORTS),
                    'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                        ['/reports/analysis/index'] :
                        ['/reports/debt-report/debtor'],
                    'active' => $module == 'reports' && in_array($controller, [
                        'debtor',
                        'debt-report',
                        'debt-report-seller',
                        'analysis',
                        'discipline',
                        'employees',
                        'invoice-report',
                        'selling-report',
                    ]),
                ],
            ],
        ],
        [
            'label' => 'ЮрОтдел',
            'url' => ['/urotdel/default'],
            'template' => $this->render('_side_menu_link', ['i' => 'scales']),
            'active' => $module == 'urotdel',
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        [
            'label' => 'Настройки',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 5, 'i' => 'cog']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 5]),
            'items' => [
                [
                    'label' => 'Профиль компании',
                    'url' => ['/company/index'],
                    'active' => $controller == 'company',
                    'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
                ],
                [
                    'label' => 'Сотрудники',
                    'url' => ['/employee/index'],
                    'active' => $controller == 'employee',
                    'visible' => Yii::$app->user->can(permissions\Employee::INDEX),
                ],
                [
                    'label' => 'Оплатить КУБ24',
                    // 'url' => ['/kub/subscribe/index'], // TEMP
                    'url' => ['/subscribe'],
                    'active' => $module == 'subscribe' && $controller == 'default',
                    'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
                ],
                [
                    'label' => 'Кабинет партнера',
                    'url' => ['/affiliate'],
                    'active' => $controller == 'affiliate',
                    'visible' => ($isPartner && Yii::$app->user->can(UserRole::ROLE_CHIEF)) || $isPartnerRelationsManager,
                ],
                [
                    'label' => 'Автосбор долгов',
                    'url' => ['/payment-reminder/index'],
                    'options' => [
                        'class' => 'invoice-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ? '' : ' hidden')
                    ],
                    'active' => $controller == 'payment-reminder',
                    'visible' => Yii::$app->user->can(permissions\PaymentReminder::INDEX),
                ],
            ],
        ],
        [
            'label' => 'Загрузка / Выгрузка',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => '1c1', 'i' => 'download']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => '1c1']),
            'items' => [
                [
                    'label' => '1С',
                    'url' => ['/export/one-s/index'],
                    'active' => $controller == 'one-s',
                ],
                [
                    'label' => 'Выгрузка документов',
                    'url' => ['/export/files/index'],
                    'active' => $module == 'export' && $controller == 'files',
                ]
            ],
            'visible' => !$hideForMarketer && !$isPartnerRelationsManager,
        ],
        [
            'label' => 'Бухгалтерия',
            'options' => [
                'class' => 'sideabar-menu-item accountant-trigger_menu-item ' . (
                    ArrayHelper::getValue($user, 'menuItem.accountant_item') ? '' : ' hidden'
                    ),
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 6, 'i' => 'mix']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 6]),
            'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) && Yii::$app->user->can(UserRole::ROLE_CHIEF),
            'items' => [
                [
                    'label' => '<span class="title">Бухгалтерия для ИП</span>',
                    'encode' => false,
                    'url' => ['/tax/robot/index'],
                    'active' => $controller == 'robot',
                    'visible' => $company && $company->getCanTaxModule(),
                ],
                //[
                //    'label' => '<span class="title">Бухгалтерия для ООО</span>',
                //    'encode' => false,
                //    'url' => ['/tax/declaration-osno/index'],
                //    'active' => $controller == 'declaration-osno' && \Yii::$app->controller->action->id != 'index-list',
                //    'visible' => $company && $company->getCanOOOTaxModule(),
                //],
                [
                    'label' => '<span class="title">Налоговый календарь</span>',
                    'encode' => false,
                    'url' => ['/notification/index', 'month' => date('m'), 'year' => date('Y')],
                    'active' => $controller == 'notification',
                    'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()),
                ],
                [
                    'label' => '<span class="title">Отчетность</span>',
                    'encode' => false,
                    'url' => [($isIp) ? '/tax/declaration/index' : '/tax/declaration-osno/index-list'],
                    'active' => Yii::$app->controller->id == 'declaration' || Yii::$app->controller->id == 'kudir' ||
                        (Yii::$app->controller->id == 'declaration-osno' && \Yii::$app->controller->action->id == 'index-list'),
                    'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) && (
                            $company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
                            $company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6) ||
                            $company->getHasActualSubscription(SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING)
                        ),
                ],
            ],
        ],
        [
            'label' => 'Еще',
            'encode' => false,
            'url' => 'javascript:;',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 'more1', 'i' => 'more', 'style' => 'font-size:20px;padding-left:2px;']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'more1']),
            'items' => [
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.invoice_item'), [
                        'id' => 'invoice_item',
                        'label' => 'КУБ24.Счета',
                        'class' => 'invoice-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.invoice-trigger_menu-item',
                        'visible' => !Yii::$app->user->can(permissions\document\Document::SOME_ACCESS),
                        'disabled' => Yii::$app->user->can(UserRole::ROLE_DEMO),
                    ]),
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.b2b_item'), [
                        'id' => 'b2b_item',
                        'label' => 'КУБ24.Модуль В2В',
                        'class' => 'b2b-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.b2b-trigger_menu-item',
                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    ]),
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]',
                        ArrayHelper::getValue($user, 'menuItem.logistics_item'), [
                            'id' => 'logistics_item',
                            'label' => 'КУБ24.Логистика',
                            'class' => 'logistics-trigger_menu trigger-menu_checkbox',
                            'data-menu_item' => '.logistics-trigger_menu-item',
                            'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                        ]),
                    'visible' => YII_ENV_DEV
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.accountant_item'), [
                        'id' => 'accountant_item',
                        'label' => 'КУБ24.Бухгалтерия',
                        'class' => 'accountant-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.accountant-trigger_menu-item',
                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    ]),
                    'options' => [
                        'class' => (!Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null) . (
                            Yii::$app->user->identity &&
                            Yii::$app->user->identity->company &&
                            (Yii::$app->user->identity->company->canTaxModule || Yii::$app->user->identity->company->canOOOTaxModule)
                                ? null : ' tooltip-side_menu'
                            ),
                        'data-tooltip-content' => Yii::$app->user->identity &&
                        Yii::$app->user->identity->company &&
                        (Yii::$app->user->identity->company->canTaxModule || Yii::$app->user->identity->company->canOOOTaxModule)
                            ? null : '#accountant_menu-tooltip',
                    ],
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.crm_item'), [
                        'id' => 'crm_item',
                        'label' => 'КУБ24.CRM',
                        'class' => 'crm-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.crm-trigger_menu-item',
                        'disabled' => Yii::$app->user->can(UserRole::ROLE_DEMO),
                    ]),
                    'visible' => CrmModule::isEnabled(),
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.project_item'), [
                        'id' => 'project_item',
                        'label' => 'КУБ24.Проекты',
                        'class' => 'project-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.project-trigger_menu-item',
                        'disabled' => !Yii::$app->user->can(permissions\Project::INDEX) || Yii::$app->user->can(UserRole::ROLE_DEMO),
                    ]),
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.product_item'), [
                        'id' => 'product_item',
                        'label' => 'КУБ24.Склад',
                        'class' => 'product-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.product-trigger_menu-item',
                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    ]),
                    'visible' => YII_ENV_DEV
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.rent_item'), [
                        'id' => 'rent_item',
                        'label' => 'КУБ24.Аренда',
                        'class' => 'rent-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.rent-trigger_menu-item',
                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    ]),
                ],
                [
                    'url' => 'javascript:;',
                    'encode' => false,
                    'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.business_analytics_item'), [
                        'id' => 'business_analytics_item',
                        'label' => Html::tag('span', 'КУБ24.ФинДиректор', [
                            'style' => 'width: 83.5%; display: block; float: right'
                        ]),
                        'class' => 'business_analytics-trigger_menu trigger-menu_checkbox',
                        'data-menu_item' => '.business_analytics-trigger_menu-item',
                        'disabled' => !(
                            Yii::$app->user->can(permissions\BusinessAnalytics::START) ||
                            Yii::$app->user->can(permissions\BusinessAnalytics::INDEX)
                        ) || Yii::$app->user->can(UserRole::ROLE_DEMO),
                    ]),
                    'visible' => $canAnalytics,
                ],
            ],
            'visible' => !$isPartnerRelationsManager,
        ],
    ];

    } else {

    $items = [
        [
            'label' => 'Мои компании',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => "company1", 'i' => 'home']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'items' => $companyItems,
            'visible' => !$isDemo,
        ],
        [
            'label' => $company ? Html::encode($company->shortTitle) : '',
            'encode' => false,
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => null, 'img' => $mainLogo]),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'url' => ['/site/index'],
            'active' => $controller == 'site' && $action == 'index',
        ],
        [
            'label' => 'Операции',
            'url' => ['/cash/default/operations'],
            'options' => [
                'class' => 'sideabar-menu-item'
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'ruble-2']),
            'active' => $module == 'cash' && $controller == 'default' && $action == 'operations',
            'visible' => BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_FINANCE),
        ],
        [
            'label' => 'Финансы',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 'finance_top_menu', 'i' => 'chart']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'finance_top_menu']),
            'visible' => BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_FINANCE),
            'items' => [
                [
                    'label' => 'Контроль денег',
                    'url' => ['/analytics/finance/odds'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && !in_array($action, ['profit-and-loss', 'balance', 'debtor', 'stocks', 'loans', 'break-even-point', 'operational-efficiency'])
                        && !in_array($controller, ['balance-articles', 'planning', 'credit', 'finance-model', 'finance-plan', 'detailing'])
                        || $module == 'reference' && $controller == 'articles',
                ],
                [
                    'label' => 'Контроль долгов',
                    'url' => ['/analytics/finance/debtor', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($this->params['is_finance_reports_module'] ?? false)
                        && (in_array($action, ['debtor', 'loans']) || in_array($controller, ['credit'])),
                ],
                [
                    'label' => 'Прибыль',
                    'url' => ['/analytics/finance/profit-and-loss'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && in_array($action, ['profit-and-loss', 'break-even-point', 'operational-efficiency']),
                ],
                [
                    'label' => 'Баланс',
                    'url' => ['/analytics/finance/balance'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false)
                        && (in_array($action, ['balance', 'stocks']) || in_array($controller, ['balance-articles'])),
                ],
                [
                    'label' => 'Детализация',
                    'url' => ['/analytics/detailing/index'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false)
                        && (in_array($controller, ['detailing'])),
                ],
                //[
                //    'label' => 'Финмодель',
                //    'url' => ['/analytics/finance-model'],
                //    'active' => ($this->params['is_finance_reports_module'] ?? false) && ($controller == 'finance-model'),
                //],
                [
                    'label' => 'Финмодель',
                    'url' => ['/analytics/finance-plan'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && ($controller == 'finance-plan'),
                ],
                [
                    'label' => 'Точки роста',
                    'url' => ['/analytics/planning/what-if'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && (in_array($action, ['what-if', 'scenario']) && $controller == 'planning'),
                ],
                [
                    'label' => 'Планирование',
                    'url' => ['/analytics/planning/index'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && (in_array($action, ['index']) && $controller == 'planning'),
                    'visible' => YII_ENV_DEV
                ],
            ],
        ],
        [
            'label' => 'Маркетинг',
            'template' => $this->render('_side_menu_btn', ['id' => 'marketing_advertising', 'i' => 'marketing']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'marketing_advertising']),
            'visible' => BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_MARKETING),
            'options' => ['class' => 'sideabar-menu-item'],
            'items' => [
                [
                    'label' => 'Реклама',
                    'url' => ['/analytics/marketing/default'],
                    'active' => ($module === 'marketing' && $controller !== 'planning'),
                ],
                [
                    'label' => 'Планирование',
                    'url' => ['/analytics/marketing/planning'],
                    'active' => ($module === 'marketing' && $controller == 'planning'),
                ],
            ],
        ],
        [
            'label' => 'Продажи',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 'sale_top_menu', 'i' => 'voronka']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 'sale_top_menu']),
            'visible' => BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_SALES),
            'items' => [
                [
                    'label' => 'Воронка продаж',
                    'url' => ['/analytics/planning/sales-funnel'],
                    'active' => ($this->params['is_finance_reports_module'] ?? false) && (in_array($action, ['sales-funnel']) && $controller == 'planning'),
                ],
                [
                    'label' => 'Отчеты',
                    'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                        ['/analytics/analysis/index'] :
                        ['/analytics/debt-report/debtor'],
                    'active' => $module == 'analytics' && in_array($controller, [
                        'debtor',
                        'debt-report',
                        'analysis',
                        'discipline',
                        'employees',
                        'invoice-report',
                        'selling-report',
                    ]),
                ],
                [
                    'label' => 'Розничные продажи',
                    'url' => ['/retail/dashboard/index'],
                    'active' => $firstModule === 'retail',
                ],
            ],
        ],
        [
            'label' => 'Товары',
            'url' => ['/analytics/product-analysis/dashboard'],
            'template' => $this->render('_side_menu_link', ['i' => 'pie-chart']),
            'active' => $controller == 'product-analysis',
            'visible' => BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_PRODUCTS),
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        //=================================================================================================
        [
            'label' => 'Заметки',
            'url' => ['/analytics/notes/index'],
            'template' => $this->render('_side_menu_link', ['i' => 'notes']),
            'active' => Yii::$app->requestedRoute == 'analytics/notes/index',
            'visible' => Yii::$app->user->can(permissions\BusinessAnalytics::INDEX) && Yii::$app->user->can(UserRole::ROLE_CHIEF),
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        [
            'label' => 'Настройки',
            'options' => [
                'id' => 'sideabar_menu_settings',
                'class' => 'sideabar-menu-item  border-bottom-blue',
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 5, 'i' => 'cog']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 5]),
            'items' => [
                [
                    'label' => 'Профиль компании',
                    'url' => ['/company/index'],
                    'active' => $controller == 'company',
                    'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
                ],
                [
                    'label' => 'Сотрудники',
                    'url' => ['/employee/index'],
                    'active' => $controller == 'employee',
                    'visible' => Yii::$app->user->can(permissions\Employee::INDEX),
                ],
                [
                    'label' => 'Оплатить КУБ24',
                    // 'url' => ['/kub/subscribe/index'], // TEMP
                    'url' => ['/subscribe'],
                    'active' => $module == 'subscribe' && $controller == 'default',
                    'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
                ],
                [
                    'label' => 'Старт',
                    'url' => ['/analytics/options/start'],
                    'active' => Yii::$app->requestedRoute == 'analytics/options/start',
                    'visible' => Yii::$app->user->can(permissions\BusinessAnalytics::START),
                ],
                [
                    'label' => 'Кабинет партнера',
                    'url' => ['/affiliate'],
                    'active' => $controller == 'affiliate',
                    'visible' => $isPartner && Yii::$app->user->can(UserRole::ROLE_CHIEF),
                ],
                [
                    'label' => 'Автосбор долгов',
                    'url' => ['/payment-reminder/index'],
                    'options' => [
                        'class' => 'invoice-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ? '' : ' hidden')
                    ],
                    'active' => $controller == 'payment-reminder',
                    'visible' => Yii::$app->user->can(permissions\PaymentReminder::INDEX),
                ],
            ],
        ],
        //===================================================================================================
        [
            'label' => 'Дополнительно',
            'options' => [
                'class' => 'sideabar-menu-item'
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 'extra1', 'i' => null]),
            'submenuTemplate' => $this->render('_side_menu_items_in', ['ul_id' => 'sidebarCollapseIn', 'id' => 'extra1', 'parent' => '#sidebarCollapse', 'listClass' => 'sidebar-submenu-list']),
            'items' => [
                [
                    'label' => 'Модуль В2В',
                    'url' => ['/b2b/index'],
                    'template' => $this->render('_side_menu_link', ['i' => 'chart']),
                    'active' => $controller == 'b2b',
                    'options' => [
                        'class' => 'sideabar-menu-item-in b2b-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.b2b_item') ? '' : ' hidden'),
                    ],
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                ],
                [
                    'label' => 'CRM',
                    'url' => ['/crm/deal/index'],
                    'template' => $this->render('_side_menu_btn', [
                        'i' => 'crm',
                        'id' => 'crm-items',
                        'append' => Html::tag('span', CrmHotTasksWidget::widget(), [
                            'class' => 'sidebar-menu-append expand_hide ml-1',
                        ]),
                    ]),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 'crm-items', 'parent' => '.sideabar-menu-item-in']),
                    'active' => ($module == 'crm'),
                    'options' => [
                        'class' => 'sideabar-menu-item-in crm-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.crm_item') ? '' : ' hidden'),
                    ],
                    'items' => [
                        [
                            'label' => 'Сделки',
                            'url' => ['/crm/deal/index'],
                            'active' => ($controller == 'deal'),
                        ],
                        [
                            'label' => 'Задачи',
                            'url' => ['/crm/task/index'],
                            'active' => ($controller == 'task'),
                        ],
                        [
                            'label' => 'Клиенты',
                            'url' => ['/crm/clients/index'],
                            'active' => ($controller == 'clients' || $controller == 'client'),
                        ],
                        [
                            'label' => 'Отчеты',
                            'url' => ['/crm/reports/refusal'],
                            'active' => (Yii::$app->controller->getUniqueId() == 'crm/reports'),
                        ],
                        [
                            'label' => 'Настройки',
                            'url' => ['/crm/deal-type/index'],
                            'active' => (Yii::$app->controller instanceof SettingControllerInterface),
                        ],
                    ],
                    'visible' => CrmModule::isEnabled(),
                ],
                [
                    'label' => 'Покупатели',
                    'url' => ['/contractor/index', 'type' => Contractor::TYPE_CUSTOMER],
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_link', ['i' => 'basket']),
                    'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_CUSTOMER,
                    'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]),
                ],
                [
                    'label' => 'Поставщики',
                    'url' => ['/contractor/index', 'type' => Contractor::TYPE_SELLER],
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_link', ['i' => 'bagagge']),
                    'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_SELLER,
                    'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                        'type' => Contractor::TYPE_SELLER,
                    ]),
                ],
                [
                    'label' => 'Продажи',
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 1, 'i' => 'sales']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 1, 'parent' => '#sidebarCollapseIn']),
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_OUT,
                    ]),
                    'items' => [
                        [
                            'label' => 'Счета',
                            'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'invoice' && !$paramDocument) && $paramType != Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Акты',
                            'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Товарные накладные',
                            'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'ТТН',
                            'url' => ['/documents/waybill/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'waybill' || $controller == 'invoice' && $paramDocument == 'waybill') && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'УПД',
                            'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Инвойсы',
                            'url' => ['/documents/foreign-currency-invoice/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller == 'foreign-currency-invoice' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Расходные накладные',
                            'url' => ['/documents/sales-invoice/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'sales-invoice' || $paramDocument == 'sales-invoice') && $paramType == Documents::IO_TYPE_OUT,
                            'visible' => $visibleSalesInvoices,
                        ],
                        [
                            'label' => 'Счета-фактуры',
                            'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_OUT,
                            'visible' => $visibleOutInvoiceFacture,
                        ],
                        [
                            'label' => 'Заказы',
                            'url' => ['/documents/order-document/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'order-document' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Договоры',
                            'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_OUT,
                            'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                                'type' => Contractor::TYPE_CUSTOMER,
                            ]),
                        ],
                        [
                            'label' => 'Розничные продажи',
                            'url' => ['/retail/dashboard/index'],
                            'active' => $firstModule === 'retail',
                            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                        ],
                    ],
                ],
                [
                    'label' => 'Покупки',
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 2, 'i' => 'purchase']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 2, 'parent' => '#sidebarCollapseIn']),
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ]),
                    'items' => [
                        [
                            'label' => 'Счета',
                            'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'invoice' && !$paramDocument) && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Акты',
                            'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Товарные накладные',
                            'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'УПД',
                            'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Инвойсы',
                            'url' => ['/documents/foreign-currency-invoice/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller == 'foreign-currency-invoice' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Счета-фактуры',
                            'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_IN,
                            'visible' => $visibleInInvoiceFacture,
                        ],
                        [
                            'label' => 'Заказы',
                            'url' => ['/documents/order-document/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'order-document' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Платёжные поручения',
                            'url' => ['/documents/payment-order/index'],
                            'active' => $controller === 'payment-order',
                            'visible' => Yii::$app->user->can(permissions\document\PaymentOrder::INDEX),
                        ],
                        [
                            'label' => 'Распознавание сканов',
                            'url' => ['/documents/upload-documents/index'],
                            'active' => $controller === 'upload-documents',
                            'visible' => false,
                        ],
                        [
                            'label' => 'Договоры',
                            'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_IN,
                            'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                                'type' => Contractor::TYPE_SELLER,
                            ])
                        ],
                        [
                            'label' => 'Доверенности',
                            'url' => ['/documents/proxy/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'proxy' && $paramType == Documents::IO_TYPE_IN,
                        ],
                    ],
                ],
                [
                    'label' => 'Деньги',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item accountant-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.analytics_item') ||
                            ArrayHelper::getValue($user, 'menuItem.accountant_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                            ),
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 'money1', 'i' => 'ruble-2']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 'money1', 'parent' => '#sidebarCollapseIn']),
                    'active' => $module == 'cash' && !($controller == 'default' && $action == 'operations'),
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX) ||
                        Yii::$app->user->can(permissions\CashOrder::INDEX),
                    'items' => [
                        [
                            'label' => 'Банк',
                            'url' => ['/cash/bank/index'],
                            'active' => $module == 'cash' && $controller == 'bank',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => 'Касса',
                            'url' => ['/cash/order/index'],
                            'active' => $module == 'cash' && $controller == 'order',
                            'visible' => Yii::$app->user->can(permissions\CashOrder::INDEX),
                        ],
                        [
                            'label' => 'Эквайринг',
                            'url' => ['/acquiring'],
                            'active' => $controller == 'acquiring',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => 'Карты',
                            'url' => ['/cards/default/index'],
                            'active' => ($module == 'cards'),
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => 'E-money',
                            'url' => ['/cash/e-money/index'],
                            'active' => $module == 'cash' && $controller == 'e-money',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => 'Итого',
                            'url' => ['/cash/default/index'],
                            'active' => $module == 'cash' && $controller == 'default' && $action != 'operations',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                    ]
                ],
                [
                    'label' => 'Проекты',
                    'options' => [
                        'class' => 'sideabar-menu-item-in project-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.project_item') ? '' : ' hidden'
                        ),
                    ],
                    'url' => ['/project/index'],
                    'template' => $this->render('_side_menu_link', ['i' => 'project']),
                    'active' => $controller == 'project',
                    'visible' => Yii::$app->user->can(permissions\Project::INDEX),
                ],
                [
                    'label' => 'Услуги',
                    'url' => [
                        '/product/index',
                        'productionType' => Product::PRODUCTION_TYPE_SERVICE,
                        'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                    ],
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_link', ['i' => 'tools']),
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_SERVICE,
                    'visible' => Yii::$app->user->can(permissions\Product::INDEX) && !$hideForMarketer,
                ],
                [
                    'label' => 'Товары',
                    'options' => [
                        'class' => 'sideabar-menu-item-in invoice-trigger_menu-item logistics-trigger_menu-item' .
                            (ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden')
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 3, 'i' => 'buildings']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 3, 'parent' => '#sidebarCollapseIn']),
                    'visible' => Yii::$app->user->can(permissions\Product::INDEX) && !$hideForMarketer,
                    'items' => [
                        [
                            'label' => 'Склад',
                            'url' => [
                                '/product/index',
                                'productionType' => Product::PRODUCTION_TYPE_GOODS,
                                'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                            ],
                            'active' => $controller == 'product' &&
                                Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                                in_array($action, ['index', 'view', 'create', 'update']),
                        ],
                        [
                            'label' => 'Прайс-листы',
                            'encode' => false,
                            'url' => ['/price-list', 'per-page' => 20],
                            'active' => $controller == 'price-list',
                            'visible' => !$isDemo,
                        ],
                        [
                            'label' => 'Оборот товара',
                            'url' => [
                                '/product/turnover',
                                'per-page' => 20,
                                'productionType' => Product::PRODUCTION_TYPE_GOODS,
                                'turnoverType' => ProductSearch::TURNOVER_BY_COUNT,
                            ],
                            'active' => $controller == 'product' &&
                                $action == 'turnover' &&
                                Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                                Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_COUNT,
                        ],
                        [
                            'label' => 'Оборот ₽',
                            'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT],
                            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                                && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_AMOUNT,
                            'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                                'ioType' => Documents::IO_TYPE_IN,
                            ]),
                        ],
                        [
                            'label' => 'Списание товара',
                            'url' => '/documents/goods-cancellation/index',
                            'active' => $controller == 'goods-cancellation',
                            'visible' => Yii::$app->user->can(permissions\Product::WRITE_OFF),
                        ],
                    ],
                ],
                [
                    'label' => 'Логистика',
                    'options' => [
                        'class' => 'sideabar-menu-item-in ' . (
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                            ),
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 7, 'i' => 'logistics-icon']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 7, 'parent' => '#sidebarCollapseIn']),
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
                    'visible' => YII_ENV_DEV,
                    'items' => [
                        [
                            'label' => '<span class="title">Заявки</span>',
                            'encode' => false,
                            'url' => ['/logistics/request/index'],
                            'active' => $module == 'logistics' && $controller == 'request' && ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                        [
                            'label' => '<span class="title">ТТН</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Путевые листы</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Отчеты</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Справочники</span>',
                            'encode' => false,
                            'url' => ['/logistics/driver/index'],
                            'active' => $module == 'logistics' && ($controller == 'driver' || $controller == 'vehicle' || $controller == 'address') &&
                                ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                    ],
                ],
                [
                    'label' => 'Объекты аренды',
                    'url' => ['/rent/calendar'],
                    'options' => [
                        'class' => 'sideabar-menu-item-in rent-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.rent_item') ? '' : ' hidden'
                        ),
                    ],
                    'template' => $this->render('_side_menu_link', ['i' => 'time']),
                    'active' => $controller == 'rent',
                    'visible' => Yii::$app->user->can(permissions\Rent::VIEWER),
                ],
                [
                    'label' => 'Отчеты',
                    'options' => ['class' => 'sideabar-menu-item-in'],
                    'template' => $this->render('_side_menu_btn', ['id' => 4, 'i' => 'chart']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 4, 'parent' => '#sidebarCollapseIn']),
                    'visible' => Yii::$app->user->can(permissions\Reports::VIEW),
                    'items' => [
                        [
                            'label' => 'Финансы',
                            'url' => ['/reports/finance/odds'],
                            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            'active' => ($this->params['is_finance_reports_module'] ?? false) && !in_array($action, ['profit-and-loss', 'balance', 'debtor', 'breakeven-point'])
                                || $module == 'reference' && $controller == 'articles',
                        ],
                        [
                            'label' => 'Отчеты',
                            'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                                ['/reports/analysis/index'] :
                                ['/reports/debt-report/debtor'],
                            'active' => $module == 'reports' && in_array($controller, [
                                'debtor',
                                'debt-report',
                                'debt-report-seller',
                                'analysis',
                                'discipline',
                                'employees',
                                'invoice-report',
                                'selling-report',
                            ]),
                        ],
                    ],
                ],
                [
                    'label' => 'ЮрОтдел',
                    'url' => ['/urotdel/default'],
                    'template' => $this->render('_side_menu_link', ['i' => 'scales']),
                    'active' => $module == 'urotdel',
                    'options' => [
                        'class' => 'sideabar-menu-item-in',
                    ],
                ],
                [
                    'label' => 'Загрузка / Выгрузка',
                    'options' => ['class' => 'sideabar-menu-item-in'],
                    'template' => $this->render('_side_menu_btn', ['id' => '1c1', 'i' => 'download']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => '1c1', 'parent' => '#sidebarCollapseIn']),
                    'items' => [
                        [
                            'label' => '1С',
                            'url' => ['/export/one-s/index'],
                            'active' => $controller == 'one-s',
                        ],
                        [
                            'label' => 'Выгрузка документов',
                            'url' => ['/export/files/index'],
                            'active' => $module == 'export' && $controller == 'files',
                        ]
                    ],
                    'visible' => !$hideForMarketer
                ],
                [
                    'label' => 'Бухгалтерия',
                    'options' => [
                        'class' => 'sideabar-menu-item-in accountant-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.accountant_item') ? '' : ' hidden'
                            ),
                    ],
                    'template' => $this->render('_side_menu_btn', ['id' => 6, 'i' => 'mix']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 6, 'parent' => '#sidebarCollapseIn']),
                    'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) && Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    'items' => [
                        [
                            'label' => '<span class="title">Бухгалтерия для ИП</span>',
                            'encode' => false,
                            'url' => ['/tax/robot/index'],
                            'active' => $controller == 'robot',
                            'visible' => $company && $company->getCanTaxModule(),
                        ],
                        [
                            'label' => '<span class="title">Бухгалтерия для ООО</span>',
                            'encode' => false,
                            'url' => ['/tax/declaration-osno/index'],
                            'active' => $controller == 'declaration-osno' && \Yii::$app->controller->action->id != 'index-list',
                            'visible' => $company && $company->getCanOOOTaxModule(),
                        ],
                        [
                            'label' => '<span class="title">Налоговый календарь</span>',
                            'encode' => false,
                            'url' => ['/notification/index', 'month' => date('m'), 'year' => date('Y')],
                            'active' => $controller == 'notification',
                            'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()),
                        ],
                        [
                            'label' => '<span class="title">Отчетность</span>',
                            'encode' => false,
                            'url' => [($isIp) ? '/tax/declaration/index' : '/tax/declaration-osno/index-list'],
                            'active' => Yii::$app->controller->id == 'declaration' || Yii::$app->controller->id == 'kudir' ||
                                (Yii::$app->controller->id == 'declaration-osno' && \Yii::$app->controller->action->id == 'index-list'),
                            'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) && (
                                    $company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
                                    $company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6) ||
                                    $company->getHasActualSubscription(SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING)
                                ),
                        ],
                    ],
                ],
                [
                    'label' => 'Еще',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'options' => ['class' => 'sideabar-menu-item-in'],
                    'template' => $this->render('_side_menu_btn', ['id' => 'more1', 'i' => 'more', 'style' => 'font-size:20px;padding-left:2px;']),
                    'submenuTemplate' => $this->render('_side_menu_items_in', ['id' => 'more1', 'parent' => '#sidebarCollapseIn']),
                    'items' => [
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.invoice_item'), [
                                'id' => 'invoice_item',
                                'label' => 'КУБ24.Счета',
                                'class' => 'invoice-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.invoice-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(permissions\document\Document::SOME_ACCESS) || Yii::$app->user->can(UserRole::ROLE_DEMO),
                            ]),
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.b2b_item'), [
                                'id' => 'b2b_item',
                                'label' => 'КУБ24.Модуль В2В',
                                'class' => 'b2b-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.b2b-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            ]),
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.logistics_item'), [
                                'id' => 'logistics_item',
                                'label' => 'КУБ24.Логистика',
                                'class' => 'logistics-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.logistics-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            ]),
                            'visible' => YII_ENV_DEV
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.accountant_item'), [
                                'id' => 'accountant_item',
                                'label' => 'КУБ24.Бухгалтерия',
                                'class' => 'accountant-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.accountant-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            ]),
                            'options' => [
                                'class' => (!Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null) . (
                                    Yii::$app->user->identity &&
                                    Yii::$app->user->identity->company &&
                                    (Yii::$app->user->identity->company->canTaxModule || Yii::$app->user->identity->company->canOOOTaxModule)
                                        ? null : ' tooltip-side_menu'
                                    ),
                                'data-tooltip-content' => Yii::$app->user->identity &&
                                Yii::$app->user->identity->company &&
                                (Yii::$app->user->identity->company->canTaxModule || Yii::$app->user->identity->company->canOOOTaxModule)
                                    ? null : '#accountant_menu-tooltip',
                            ],
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.crm_item'), [
                                'id' => 'crm_item',
                                'label' => 'КУБ24.CRM',
                                'class' => 'crm-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.crm-trigger_menu-item',
                                'disabled' => Yii::$app->user->can(UserRole::ROLE_DEMO),
                            ]),
                            'visible' => CrmModule::isEnabled(),
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.project_item'), [
                                'id' => 'project_item',
                                'label' => 'КУБ24.Проекты',
                                'class' => 'project-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.project-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(permissions\Project::INDEX) || Yii::$app->user->can(UserRole::ROLE_DEMO),
                            ]),
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.product_item'), [
                                'id' => 'product_item',
                                'label' => 'КУБ24.Склад',
                                'class' => 'product-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.product-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            ]),
                            'visible' => YII_ENV_DEV
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.rent_item'), [
                                'id' => 'rent_item',
                                'label' => 'КУБ24.Аренда',
                                'class' => 'rent-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.rent-trigger_menu-item',
                                'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                            ]),
                        ],
                        [
                            'url' => 'javascript:;',
                            'encode' => false,
                            'label' => Html::checkbox('module-trigger_menu[]', ArrayHelper::getValue($user, 'menuItem.business_analytics_item'), [
                                'id' => 'business_analytics_item',
                                'label' => Html::tag('span', 'КУБ24.ФинДиректор', [
                                    'style' => 'width: 85%; display: block; float: right'
                                ]),
                                'class' => 'business_analytics-trigger_menu trigger-menu_checkbox',
                                'data-menu_item' => '.business_analytics-trigger_menu-item',
                                'disabled' => !(
                                    Yii::$app->user->can(permissions\BusinessAnalytics::START) ||
                                    Yii::$app->user->can(permissions\BusinessAnalytics::INDEX)
                                ) || Yii::$app->user->can(UserRole::ROLE_DEMO),
                            ]),
                            'visible' => $canAnalytics
                        ],
                    ],
                ],
            ],
        ],

    ];

    }

} else {
    $items = [];
}

$this->registerJs("
$(document).ready(function () {
    $('.sideabar-menu-item .collapse:has(.active)').collapse('show');
    $('.business_analytics-trigger_menu').on('change', function(){
        console.log(1);
        if ($(this).closest('span').hasClass('checked') && $('#tooltipster_finance').hasClass('hidden')) {
            $('#tooltipster_finance')
                .removeClass('hidden')
                .addClass('tooltipster-fade')
                .addClass('tooltipster-show');
        }
    });
    $('.user-help-stop').on('click', function(){
        $('#tooltipster_finance')
            .addClass('hidden')
            .removeClass('tooltipster-fade')
            .removeClass('tooltipster-show');
        });
    });
");
?>

<div class="page-sidebar column">
	<div class="page-sidebar__sticky">

        <?php \yii\widgets\Pjax::begin([
				'id' => 'sidemenu-pjax-container',
				'enablePushState' => false,
				'linkSelector' => false,
				'options' => [
						'class' => 'sidebar',
						// 'class' => 'sidebar sidebar_fixed',
				],
		]); ?>

		<?php if ($user) : ?>
				<?= Menu::widget([
						//'activateParents' => true,
						'items' => $items,
						'options' => [
								'id' => 'sidebarCollapse',
								'class' => 'sidebar-menu list-clr',
						]
				]) ?>
		<?php endif ?>

		<?php \yii\widgets\Pjax::end(); ?>

	</div>
</div>
<div class="hidden">
    <span id="accountant_menu-tooltip" style="display: inline-block;">
        Данный блок только для<br>
        1) ИП на УСН Доходы,<br>
        2) ООО на ОСНО с нулевой отчетностью.
    </span>
</div>

<div class="tooltipster-base tooltipster-sidetip tooltipster-user-help tooltipster-bottom hidden"
     id="tooltipster_finance"
     style="position: fixed; top: 75px; left: 20px; max-width: 250px; z-index: 10; height: 65px; animation-duration: 350ms; transition-duration: 350ms;">
    <div class="tooltipster-box">
        <div class="tooltipster-content">
            <span class="user-help-stop">×</span>
            <div class="user-help-content">Нажмите по логотипу КУБ24. Выберите продукт "ФинДиректор"
            </div>
        </div>
    </div>
    <div class="tooltipster-arrow" style="left: 43%">
        <div class="tooltipster-arrow-uncropped">
            <div class="tooltipster-arrow-border"></div>
        </div>
    </div>
</div>