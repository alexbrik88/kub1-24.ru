<?php

use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\file\FileDir;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$employee = Yii::$app->user->identity;
$company = Yii::$app->user->identity->company;
$isChief = Yii::$app->user->identity->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF;
$allowedDirsIds = ArrayHelper::getColumn(FileDir::getEmployeeAllowedDirs(), 'id');

$isFullScreen = $isFullScreen ?: false;
$filesIds = isset($filesIds) ? $filesIds : (array)Yii::$app->request->get('files');
$startFileId = isset($startFileId) ? $startFileId : ($filesIds ? reset($filesIds) : 0);
// get images
$query = File::find()->where([
    'company_id' => $company->id,
    'id' => $filesIds,
]);
if (!$isChief) {
    $query->andWhere(['or', ['directory_id' => $allowedDirsIds], ['created_at_author_id' => $employee->id]]);
}
$files = $query->asArray()->all();

$images = [];
foreach ($files as $file) {
    $images[] = [
        'id' => $file['id'],
        'ext' => $file['ext'],
        'filename_full' => $file['filename_full'],
        'filesize' => $file['filesize'],
        'src' => UploadManagerHelper::getImgPreviewUrl($file),
    ];
}
////////
$dropDirs = [];
$dropDocs = [];

$canIndex = $canCreate = $canDelete = $canUpdate = $canRestore =
    Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
    Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
    Yii::$app->user->can(UserRole::ROLE_SUPERVISOR);

if ($isFullScreen) {
    if ($dirs = FileDir::getEmployeeAllowedDirs()) {

        $dropDirs[] = [
            'label' => 'В папку',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'group-attach-files-to',
            ],
        ];

        /** @var FileDir $dir */
        foreach ($dirs as $dir) {
            $dropDirs[] = [
                'label' => $dir->name,
                'url' => '#move-files-to',
                'linkOptions' => [
                    'class' => 'move-files-to',
                    'data-directory_id' => $dir->id
                ],
            ];
        }
    }

    $docTypesList = UploadManagerHelper::getDocumentTypesList($company);
    foreach ($docTypesList as $label => $docTypes) {
        $dropDocs[] = [
            'label' => $label,
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'group-attach-files-to',
            ],
        ];
        foreach ($docTypes as $key => $value) {
            $dropDocs[] = [
                'label' => $value,
                'url' => '#attach-files-to',
                'linkOptions' => [
                    'class' => 'attach-files-to',
                    'data-doc_type' => $key
                ],
            ];
        }
    }
}

// Styles
echo $this->render('@frontend/themes/kub/modules/documents/views/upload-manager/_partial/_style_full_screen');

?>

<?php if ($isFullScreen): ?>
    <div class="wrap page-scan-preview">
        <div class="wrap_btns_100">
            <div class="row align-items-center justify-content-start">
                <div class="column">
                    <?php if ($canUpdate && $dropDirs) : ?>
                        <div class="dropdown">
                            <?= Html::a($this->render('//svg-sprite', ['ico' => 'folder']) . '<span class="pr-2">Переместить</span>' . $this->render('//svg-sprite', ['ico' => 'shevron']), null, [
                                'class' => 'button-regular button-more-2-icons button-hover-transparent button-clr dropdown-toggle',
                                'id' => 'dropdownMenu1',
                                'data-toggle' => 'dropdown',
                                'aria-expanded' => true,
                            ]); ?>
                            <?= Dropdown::widget([
                                'items' => $dropDirs,
                                'options' => [
                                    'class' => 'form-filter-list list-clr dropdown-menu-right',
                                ],
                            ]); ?>
                        </div>
                    <?php endif ?>
                </div>
                <div class="column">
                    <?php if ($canUpdate && $dropDocs) : ?>
                        <div class="dropdown">
                            <?= Html::a($this->render('//svg-sprite', ['ico' => 'clip']) . '<span class="pr-2">Прикрепить</span>' . $this->render('//svg-sprite', ['ico' => 'shevron']), null, [
                                'class' => 'button-regular button-more-2-icons button-hover-transparent button-clr dropdown-toggle btn-many-attach',
                                'id' => 'dropdownMenu2',
                                'data-toggle' => 'dropdown',
                                'aria-expanded' => true,
                                'style' => 'display:none',
                            ]); ?>
                            <?= Dropdown::widget([
                                'items' => $dropDocs,
                                'options' => [
                                    'class' => 'form-filter-list list-clr dropdown-menu-right',
                                ],
                            ]); ?>
                        </div>
                    <?php endif ?>
                </div>
                <div class="column scan-info-column ml-auto mr-auto">
                    <div class="inline-block">
                        <div class="slide-file-name">
                            <div class="file-name"><?= str_repeat('&nbsp;', 15) ?></div>
                        </div>
                        <div class="num-page-wrap text-center">
                            <span class="num-page">1</span> из <?= count($images) ?>
                        </div>
                    </div>
                    <div class="prevPage">
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'shevron']), "#", ['class' => 'link']) ?>
                    </div>
                    <div class="nextPage">
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'shevron']), "#", ['class' => 'link']) ?>
                    </div>
                </div>
                <div class="column">
                    <?php if ($canIndex): ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']), 'javascript:;', [
                            'class' => 'print-scan button-clr button-regular button-list-width button-hover-transparent',
                            'title' => 'Печать'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <div class="column">
                    <?php if ($canIndex): ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'download']), 'javascript:;', [
                            'class' => 'download-scan button-clr button-regular button-list-width button-hover-transparent',
                            'download' => 'download',
                            'title' => 'Скачать'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <div class="column">
                    <?php if ($canDelete) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'garbage']), '#many-delete', [
                            'class' => 'button-clr button-regular button-list-width button-hover-transparent',
                            'data-toggle' => 'modal',
                            'title' => 'Удалить'
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="column">
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'close']) . ' <span>Закрыть</span>',
                        Url::previous('lastPreviewScansPage'), [
                        'class' => 'button-clr button-regular button-hover-transparent',
                        'title' => 'Закрыть'
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="slider-body">
            <div id="slide-img" class="slide issuu-container issuu-img-container img" style="display: none; min-height: 551px;"></div>
            <div id="slide-pdf" class="slide issuu-container issuu-embed-container pdf" style="display: none; padding-bottom: 851px;"></div>
            <div id="slide-other" class="slide issuu-container" style="display: none; padding-bottom: 551px; padding-top: 15px; padding-left: 20px;"></div>
        </div>

        <?= Html::tag('span', '<i class="fa fa-plus"></i>', [
            'class' => 'zoomIn'
        ]); ?>
        <?= Html::tag('span', '<i class="fa fa-minus"></i>', [
            'class' => 'zoomOut'
        ]); ?>

    </div>

    <?= $this->render('@documents/views/upload-manager/_partial/_joint_operations_modals', [
        'canUpdate' => $canUpdate,
        'canRestore' => $canRestore,
        'canDelete' => $canDelete
    ]) ?>

    <?= $this->render('@frontend/modules/documents/views/upload-manager/_partial/_kub_doc_js') ?>

    <?= $this->render('@frontend/modules/documents/views/upload-manager/_partial/_invoices_modal', [
        'company' => $company,
    ]) ?>

<?php else: ?>

    <div class="wrap page-scan-preview">

        <div class="row align-items-center justify-content-start">
            <div class="column scan-info-column ml-auto mr-auto text-center" style="width: 95%">
                <div class="inline-block" style="width: 100%">
                    <div class="slide-file-name" style="width: 100%">
                        <div class="file-name"><?= str_repeat('&nbsp;', 15) ?></div>
                    </div>
                    <div class="num-page-wrap text-center">
                        <span class="num-page">1</span> из <?= count($images) ?>
                    </div>
                </div>
                <div class="prevPage">
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'shevron']), "#", ['class' => 'link']) ?>
                </div>
                <div class="nextPage">
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'shevron']), "#", ['class' => 'link']) ?>
                </div>
            </div>
        </div>

        <div class="slider-body">
            <div id="slide-img" class="slide issuu-container issuu-img-container img" style="display: none; min-height: 551px;"></div>
            <div id="slide-pdf" class="slide issuu-container issuu-embed-container pdf" style="display: none; padding-bottom: 851px;"></div>
            <div id="slide-other" class="slide issuu-container" style="display: none; padding-bottom: 551px; padding-top: 15px; padding-left: 20px;"></div>
        </div>

        <?= Html::tag('span', '<i class="fa fa-plus"></i>', [
            'class' => 'zoomIn'
        ]); ?>
        <?= Html::tag('span', '<i class="fa fa-minus"></i>', [
            'class' => 'zoomOut'
        ]); ?>

    </div>

    <style>
        .page-scan-preview {
            max-width: 35%;
            width: 35%;
            flex: 0 0 35%;
            margin-bottom: 40px;
        }
        .page-content.preview-scan {
            max-width: 65%;
            width: 65%;
            flex: 0 0 65%;
        }
        .page-scan-preview .file-name {
            width: 80%;
        }
        #slide-img {
            overflow: auto;
        }
        #slide-img img {
            max-width: unset!important;
        }
        .scan-info-column .prevPage {
            left: 0;
        }
        .scan-info-column .nextPage {
            right: 0;
        }
        .page-scan-preview .file-name {
            width: calc(100% - 80px);
            margin-left: auto;
            margin-right: auto;
        }
        .wrap_btns {
            min-width: 625px;
        }
        /** FIX */
        #table-for-invoice > tbody td {
            vertical-align: top;
        }
        input[type=search]::-webkit-search-cancel-button {
            position:relative;
            right:0px;
            -webkit-appearance: none;
            height: 10px;
            width: 10px;
            background-image:url('data:image/svg+xml;charset=UTF-8,<svg style="fill:rgb(170,170,170)" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 47.971 47.971"><path d="M28.228 23.986L47.092 5.122a2.998 2.998 0 0 0 0-4.242 2.998 2.998 0 0 0-4.242 0L23.986 19.744 5.121.88a2.998 2.998 0 0 0-4.242 0 2.998 2.998 0 0 0 0 4.242l18.865 18.864L.879 42.85a2.998 2.998 0 1 0 4.242 4.241l18.865-18.864L42.85 47.091c.586.586 1.354.879 2.121.879s1.535-.293 2.121-.879a2.998 2.998 0 0 0 0-4.242L28.228 23.986z"></path></svg>');
        }
        .table-style a.link.unknown_product,
        .table-style a.link.unknown_product:hover {
            color: red!important;
        }
    </style>

    <script>
        $(document).ready(function() {
            var returnUrl = '<?= \yii\helpers\Url::previous('lastUploadManagerPage') ?>';
            $('.page-content.preview-scan').prepend('<a class="link mb-2" href="' + returnUrl + '">Назад к списку</a>');
        });
    </script>

<?php endif; ?>

<script>
    window.previewScansFromId = <?= (int)$startFileId ?>;
    window.previewScansList = {};
    window.previewScansPos = 0;
    window.previewScansTotal = <?= count($images) ?>;

    <?php $i = 0; foreach ($images as $imageInfo): ?>
    <?php if ($imageInfo['ext'] == 'pdf') {
        $showIn = '#slide-pdf';
        $img = Html::tag('embed', '', [
            'class' => 'source',
            'src' => Url::to($imageInfo['src']),
            'name' => 'plugin',
            'type' => 'application/pdf',
        ]);
    } elseif (in_array($imageInfo['ext'], ['gif', 'png', 'jpg', 'jpeg'])) {
        $showIn = '#slide-img';
        $img = Html::img($imageInfo['src'], [
            'class' => 'source',
        ]);
    } else {
        $showIn = '#slide-other';
        $img = 'Изображение не поддерживается';
    }
    $filename = htmlspecialchars($imageInfo['filename_full']) .' '
        . '<span>'.UploadManagerHelper::formatSizeUnits($imageInfo['filesize']).'</span>';
    ?>

    previewScansList[<?= $i ?>] =
        {
            'file_id':  <?= (int)$imageInfo['id'] ?>,
            'showIn':   '<?= addcslashes($showIn, "'") ?>',
            'filename': '<?= addcslashes($filename, "'") ?>',
            'html':     '<?= addcslashes($img, "'") ?>',
            'src':      '<?= addcslashes(Url::to($imageInfo['src']), "'") ?>'
        };

    <?php if ($startFileId == $imageInfo['id']): ?>
    window.previewScansPos = <?= (int)$i ?>;
    <?php endif; ?>


    <?php $i++; endforeach; ?>

    window.currScan = window.previewScansList[window.previewScansPos];

    ///////////////////////////////////////////////////////////////
    // console.log(previewScansList);
    ///////////////////////////////////////////////////////////////

    $('.move-files-to').on('click', function(e) {
        e.preventDefault();
        $('#move-files-to').find('.modal-move-files-to').attr('data-url', '/documents/upload-manager/move-files-to?directory_id=' + $(this).data('directory_id'));
        $('#move-files-to').modal('show');
    });
    $(document).on('click', '.modal-move-files-to', function () {
        if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
        $('#ajax-loading').show();
        var $this = $(this);

        if (!$this.hasClass('clicked')) {
            $this.addClass('clicked');
            $.post($this.data('url'), {'File[]': $('.page-scan-preview').find('.slide:visible').attr('data-id')}, function (data) {
                $('#ajax-loading').hide();
                if (data.message !== undefined) {
                    if ($('.alert-success').length > 0) {
                        $('.alert-success').remove();
                    }
                    $('.page-content').prepend('<div id="w2-success-0" class="alert-success alert fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        data.message + '</div>');
                }
            });
        }
    });

    $(document).on("change", ".btn-attach-image", function (e) {
        e.preventDefault();
        var addToDoc = $(this).val();
        var files = [$(this).parents('tr').data('file_id')];

        if (!addToDoc)
            return false;

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
            $(this).val('').trigger('change');
        }
    });

    $(document).on('click', '.group-attach-files-to', function(e) {
        return false;
    });

    $(document).on("click", ".attach-files-to", function (e) {
        e.preventDefault();
        var addToDoc = $(this).data('doc_type');
        var files = [ $('.page-scan-preview').find('.slide:visible').attr('data-id') ];

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
        }

    });

    $('.modal-attach-image-yes').on('click', function () {
        $('#invoices-list').modal();
        $('#modal-attach-image').modal('hide');
        $.pjax({
            url: '/documents/upload-manager/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            },
            push: false,
            timeout: 5000
        });
    });

    $('.modal-attach-image-no').on('click', function () {
        $.post('/documents/upload-manager/redirect-to-create-document', {
            type: KubDoc.currentType,
            doc_type: KubDoc.currentDocType,
            files: KubDoc.currentFiles
        }, function(data) {
            $('#modal-attach-image').modal('hide');
            if (data['redirect_url']) {
                location.href = data['redirect_url'];
            }
        });
    });

</script>


<!-- side-menu-preview-scan.js -->
<script>
    $(document).ready(function() {
        var container = $('.page-scan-preview');
        $(container).find('.file-name').html(window.currScan.filename);
        $(container).find('.num-page').html(window.previewScansPos + 1);
        if (!$(container).find('.slide').find('img').length) {
            $(container).find('.zoomIn, .zoomOut').hide();
        }

        previewScansRefreshPage();
    });

    function previewScansRefreshPage()
    {
        var container = $('.page-scan-preview');
        window.currScan = window.previewScansList[window.previewScansPos];

        $('.issuu-container').hide().html("");
        $(window.currScan.showIn).html(window.currScan.html).show();

        $(container).find('.file-name').html(window.currScan.filename);
        $(container).find('.num-page').html(window.previewScansPos + 1);
        if (!$(container).find('.slide').find('img').length) {
            $(container).find('.zoomIn, .zoomOut').hide();
        } else {
            $(container).find('.zoomIn, .zoomOut').show();
        }
        $(container).find('.slide:visible').attr('data-id', window.currScan.file_id);

        $.post('/documents/upload-manager-ajax/can-attach-file?file_id=' + window.currScan.file_id, function(data) {
            if (data.result) {
                $('.btn-many-attach').show();
            } else {
                $('.btn-many-attach').hide();
            }
        });
    }

    // slide scans
    $('.nextPage').on('click', function(e) {
        window.previewScansPos++;
        if (window.previewScansPos > window.previewScansTotal - 1) {
            window.previewScansPos = 0;
        }

        previewScansRefreshPage();
    });
    $('.prevPage').on('click', function(e) {
        window.previewScansPos--;
        if (window.previewScansPos < 0) {
            window.previewScansPos = window.previewScansTotal - 1;
        }

        previewScansRefreshPage();
    });

    // zoom
    $('.zoomIn, .zoomOut').on('click', function(e) {
        var container = $('.page-scan-preview');
        var activeSlide = $(container).find('.slide:visible').first();
        var image = $(activeSlide).find('img');
        var currentZoom = $(image).css('width').replace('px', '');
        if ($(this).hasClass('zoomIn'))
            $(image).css({'width': Math.ceil(1.05 * currentZoom) + 'px'});
        if ($(this).hasClass('zoomOut'))
            $(image).css({'width': Math.ceil(0.95 * currentZoom) + 'px'});
    });

    // print
    $('.print-scan').on('click', function(e) {
        e.preventDefault();
        var container = $('.page-scan-preview');
        var activeSlide = $(container).find('.slide:visible').first();
        var id = $(activeSlide).attr('id');
        if ($(activeSlide).hasClass('pdf')) {
            printPDF(id);
        } else {
            printImg(id);
        }
    });

    // download
    $('.download-scan').on('mousedown', function(e) {
        var filename = window.currScan.src;
        $(this).attr('href', filename);
    });

    function printImg(id) {
        var divToPrint = document.getElementById(id);
        wnd = window.wnd && window.wnd.close() || window.open('', "Print-Window");
        wnd.document.open();
        wnd.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        wnd.document.close();
    }

    function printPDF(id) {
        var printUrl = $('#'+id).find('embed').attr('src').replace('/file/', '/print/');
        wnd = window.wnd && window.wnd.close() || window.open(printUrl, "Print-Window");
    }
</script>