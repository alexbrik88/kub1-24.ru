<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$route = Yii::$app->controller->getRoute();
$kubUser = ArrayHelper::getValue(\Yii::$app->user, 'identity.kubUser');

if ($kubUser && $kubUser->getKubSubscribes()->exists() && $route != 'kub/subscribe/index') {
    echo \yii\bootstrap4\Alert::widget([
        'body' => 'У Вас есть оплаченные подписки, ожидающие активации. ' . Html::a('Активировать', [
            '/kub/subscribe/index',
        ]),
        'closeButton' => ['tag' => 'span'],
        'options' => [
            'id' => 'kub-subscribe-alert',
            'class' => 'alert-info',
        ],
    ]);
}
