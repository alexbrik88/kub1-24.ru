<?php

use frontend\components\Icon;

?>
<div class="sidebar-menu-head">
    <a class="sidebar-menu-btn button-clr" href="{url}">
        <?php if (isset($img)): ?>
            <?= \yii\bootstrap4\Html::tag('span', $img, [
                'class' => 'side-menu-logo',
                'style' => 'margin-right: 17px;'
            ]); ?>
        <?php else: ?>
            <?= Icon::get($i, ['class' => 'sidebar-menu-icon']) ?>
        <?php endif; ?>
        <span class="sidebar-menu-btn-txt">{label}</span>
        <?php if (isset($i_right)): ?>
            <?= Icon::get($i_right, ['class' => 'sidebar-menu-icon sidebar-menu-icon-right']) ?>
        <?php endif; ?>
    </a>
    <?php if (!empty($count)): ?>
        <?= \common\components\helpers\Html::tag('span', $count, ['class' => 'badge badge-blue badge-line']) ?>
    <?php endif; ?>
</div>