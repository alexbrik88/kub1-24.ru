<?php

use common\models\employee\EmployeeRole;
use common\models\notification\Notification;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\ServiceModule;
use frontend\components\Icon;
use frontend\components\NotificationHelper;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\themes\kub\widgets\NewsWidget;
use frontend\widgets\AccountBalanceWidget;
use frontend\widgets\CompanyLimitsWidget;
use frontend\widgets\CompanySubscribesWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

/* @var $this \yii\web\View */
/* @var $isKUBbank bool */

$theme = $this->theme;
$company = null;
$companyItems = [];
$userItems = [
    [
        'label' => 'Профиль компании',
        'url' => ['/company/index'],
        'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
    ],
    [
        'label' => 'Визитка компании',
        'url' => ['/company/visit-card'],
        'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
    ],
    [
        'label' => 'Ваш профиль',
        'url' => ['/profile/index'],
        'visible' => Yii::$app->user->can(permissions\User::PROFILE_EDIT),
    ],
    [
        'label' => 'Оплатить КУБ24',
        'style' => 'color:#4679AE',
        'url' => ['/subscribe'],
        'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
    ],
];
$notifications = [];
if ($user) {
    $company = $user->company;

    if ($company !== null) {
        $notifications = (new NotificationHelper([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->getNotifications($company);
    }
}
$marketingNotifications = [];
if ($user) {
    $company = $user->company;

    if ($company !== null) {
        $marketingNotifications = \common\modules\marketing\models\MarketingNotificationMessage::find()
            ->where(['company_id' => $company->id])
            ->andWhere(['status' => 1])
            ->andWhere(['DATE_FORMAT(FROM_UNIXTIME(created_at), "%Y-%m-%d")' => date('Y-m-d')])
            ->all();
    }
}

$module = Yii::$app->controller->module->id;
$controller = Yii::$app->controller->id;

if (Yii::$app->user->isGuest) {
    $logoText = '';
    $logoUrl  = '#';
    $currentModule = null;
} elseif ($module == 'analytics' || Yii::$app->user->identity->currentEmployeeCompany->at_business_analytics_now ?? false) {
    $logoText = 'ФинДиректор';
    $logoUrl  = Url::to(['/reports/finance/odds']);
    $currentModule = ServiceModule::ANALYTICS;
} elseif ($module == 'tax' || ($module == 'app-frontend' && $controller == 'notification')) {
    $logoText = 'Бухгалтерия ИП';
    $logoUrl = Url::to(['/tax/robot/index']);
    $currentModule = ServiceModule::IP_USN_6;
} elseif ($module == 'app-frontend' && $controller == 'b2b') {
    $logoText = 'Бизнес платежи';
    $logoUrl = Url::to(['/b2b/module']);
    $currentModule = ServiceModule::B2B_PAYMENT;
} else {
    $logoText = 'Выставление счетов';
    $logoUrl  = Url::to(['/documents/invoice/index', 'type' => 2]);
    $currentModule = ServiceModule::STANDART;
}

$canBuhIP = $company && $company->getCanTaxModule() && Yii::$app->user->can(UserRole::ROLE_CHIEF);
$notificationsUrl = Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]);
$hasNewNotifications = false;
if (Yii::$app->controller->action->id != 'first-create') {
    foreach ($notifications as $notification) {
        if (date('Y-m-d H:i:s', $notification->updated_at) > Yii::$app->user->identity->view_notification_date) {
            $hasNewNotifications = true;
            break;
        }
    }
}
if ($marketingNotifications)
    $hasNewNotifications = true;

$canThemeSwitch = true; /* YII_ENV_DEV || (
    $company && in_array($company->id, (array) ArrayHelper::getValue(Yii::$app->params, 'canThemeSwitch'))
); */
$userId = Yii::$app->user->identity->id ?? null;
$demoUserId = Yii::$app->params['service']['demo_employee_id'] ?? null;
$isPartnerRelationsManager = (Yii::$app->user->identity->currentEmployeeCompany->employee_role_id ?? null) == EmployeeRole::ROLE_PARTNER_RELATIONS_MANAGER;
if ($isPartnerRelationsManager) {
    $canThemeSwitch = false;
}
$onboarding = Yii::$app->controller->action->getUniqueId() == 'cash/default/operations'
    && boolval($company->show_cash_operations_onboarding);
?>
<style type="text/css">
.dropdown-popup_products.dropdown-logo {
    left: 0 !important;
    right: auto !important;
}
.dropdown-popup_products.dropdown-logo:after {
    left: 5px !important;
    right: auto !important;
}
.theme-kub_wide .dropdown-popup_products.dropdown-logo {
    left: 20px !important;
}
</style>
<header class="header">
    <div class="container-fluid">
        <div class="row" style="min-height: 40px;">
            <div class="col-7">
                <div class="d-flex align-items-center">
                    <div class="dropdown pr-3" style="position: relative; min-width: 240px;">
                        <div class="header-logo-wrap">
                            <a class="header-logo d-inline-flex align-items-center" href="#" id="logoDropdownProducts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/img/fav.svg" style="height: 40px; margin-right: 10px;">
                                <span class="logo-txt"><?= $logoText ?></span>
                            </a>
                            <div class="hamburger-wrap text-center">
                                <div class="hamburger hamburger--arrow is-active">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-popup dropdown-popup_products dropdown-menu dropdown-logo" aria-labelledby="logoDropdownProducts">
                                <?= $this->render('_available_products', [
                                    'currentModule' => $currentModule,
                                    'module' => $module,
                                    'currentController' => $controller,
                                    'canBuhIP' => $canBuhIP,
                                    'isDemo' => $isDemo,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <?php if ($isDemo || Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                            <?= AccountBalanceWidget::widget(['company' => $company, 'employee' => Yii::$app->user->identity]) ?>
                        <?php endif ?>
                    </div>
                    <?php if ($userId && $demoUserId && $userId == $demoUserId) : ?>
                        <style type="text/css">
                            #theme-switcher-form .switch-input:checked + label .switch-pseudo:after {
                                background-color: #4679AE;
                            }
                        </style>
                        <?= Html::beginForm(['/site/demo-logout'], 'post', [
                            'id' => 'theme-switcher-form',
                            'class' => 'ml-4',
                        ]) ?>
                            <div class="switch ml-4" onclick="$('#theme-switcher-form').submit();">
                                <?= Html::checkbox('t', true, [
                                    'id' => 'theme-switcher-checkbox',
                                    'class' => 'md-check switch-input input-hidden hidden',
                                ]) ?>
                                <label class="d-flex align-items-center switch-label pl-0" for="theme-switcher-checkbox"
                                    style="cursor: pointer;">
                                    <span class="switch-pseudo position-relative">&nbsp;</span>
                                    <span class="ml-2" style="line-height: 20px; color: #9198a0;">
                                        Выйти из
                                        <br>
                                        Демо аккаунта
                                    </span>
                                </label>
                            </div>
                        <?= Html::endForm() ?>
                        <script type="text/javascript">
                            (function( $ ) {
                                $("#theme-switcher-checkbox").on("change", function (e) {
                                    this.form.submit();
                                });
                            })(jQuery);
                        </script>
                    <?php elseif ($userId && $demoUserId && $currentModule == ServiceModule::ANALYTICS) : ?>
                        <style type="text/css">
                            #theme-switcher-form .switch-input:checked + label .switch-pseudo:after {
                                background-color: #4679AE;
                            }
                        </style>
                        <?= Html::beginForm(['/site/demo-login'], 'post', [
                            'id' => 'theme-switcher-form',
                            'class' => 'ml-4',
                        ]) ?>
                            <div class="switch ml-4">
                                <?php if ($onboarding) : ?>
                                    <?= Html::beginTag('div', [
                                        'id' => 'onboarding_help_demo_account',
                                        'class' => 'onboarding_help_item',
                                        'data-tooltip-content' => '#tooltip_content_3',
                                        'style' => 'background-color: #fff;'
                                    ]); ?>
                                    <div class="hidden">
                                        <div id="tooltip_content_3">
                                            <div class="mb-2">
                                                В демоаккаунте заполнены данные.
                                                <br>
                                                Удобно смотреть все отчеты и графики.
                                            </div>
                                            <div class="mt-3">
                                                <?= Html::button('Отлично', [
                                                    'id' => 'onboarding_help_demo_account_ok',
                                                    'class' => 'button-regular button-regular_red px-4',
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <div onclick="$('#theme-switcher-form').submit();">
                                <?= Html::checkbox('t', false, [
                                    'id' => 'theme-switcher-checkbox',
                                    'class' => 'md-check switch-input input-hidden hidden',
                                ]) ?>
                                <label class="d-flex align-items-center switch-label pl-0" for="theme-switcher-checkbox"
                                    style="cursor: pointer;">
                                    <span class="switch-pseudo position-relative">&nbsp;</span>
                                    <span class="ml-2" style="line-height: 20px; color: #9198a0;">
                                        Перейти в
                                        <br>
                                        Демо аккаунт
                                    </span>
                                </label>
                                </div>
                                <?php if ($onboarding) : ?>
                                    <div class="onboarding_help_cover"></div>
                                    <?= Html::endTag('div') // onboarding_help_demo_account ?>
                                <?php endif ?>
                            </div>
                        <?= Html::endForm() ?>
                        <script type="text/javascript">
                            (function( $ ) {
                                $("#theme-switcher-checkbox").on("change", function (e) {
                                    this.form.submit();
                                });
                            })(jQuery);
                        </script>
                    <?php elseif ($canThemeSwitch) : ?>
                        <?= Html::beginForm(['/site/change-theme'], 'get', [
                            'id' => 'theme-switcher-form',
                            'class' => 'ml-4',
                        ]) ?>
                            <?= Html::hiddenInput('theme', 'old') ?>
                            <div class="switch ml-4" onclick="$('#theme-switcher-form').submit();">
                                <?= Html::checkbox('t', true, [
                                    'id' => 'theme-switcher-checkbox',
                                    'class' => 'md-check switch-input input-hidden hidden',
                                ]) ?>
                                <label class="d-flex align-items-center switch-label pl-0" for="theme-switcher-checkbox"
                                    style="cursor: pointer;">
                                    <span class="switch-pseudo position-relative">&nbsp;</span>
                                    <span class="ml-2" style="line-height: 20px; color: #9198a0;">
                                        Вернуться в
                                        <br>
                                        старый дизайн
                                    </span>
                                </label>
                            </div>
                        <?= Html::endForm() ?>
                        <script type="text/javascript">
                            (function( $ ) {
                                $("#theme-switcher-checkbox").on("change", function (e) {
                                    this.form.submit();
                                });
                            })(jQuery);
                        </script>
                    <?php endif ?>
                </div>
            </div>
            <?php if (!Yii::$app->user->isGuest) : ?>
                <div class="col-5">
                    <div class="header-others">
                        <div class="header-others-row row align-items-center  <?= !$isPartnerRelationsManager ? 'justify-content-between' : 'justify-content-end' ?>">
                            <?php if (!$isPartnerRelationsManager): ?>
                                <div class="header-others-column column" style="margin-right: auto;">
                                    <?= $this->render('_plus_menu', [
                                        'isDemo' => $isDemo,
                                    ]) ?>
                                </div>

                                <?= NewsWidget::widget(['employee' => $user]) ?>

                                <?php if (YII_ENV_DEV): ?>
                                    <div class="header-others-column column">
                                        <div class="dropdown_products">
                                        <?php $isUploadManagerActive = strpos(Yii::$app->request->url, 'upload-manager'); ?>
                                        <?php $newFilesCount = $company ? \common\models\document\ScanDocument::getNewScansCount($company) : 0; ?>
                                        <?php echo yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'folder', 'class' => 'dropdown-btn-icon svg-icon']) .
                                            ($newFilesCount ? ('<span class="badge badge-blue">'.$newFilesCount.'</span>') : ''),
                                            ($isUploadManagerActive) ?
                                                Url::previous('UploadManagerReturn') :
                                                Yii::$app->urlManager->createUrl('documents/upload-manager/index'), [
                                                'class' => 'button-clr dropdown-btn ' . ($isUploadManagerActive ? 'active':''),
                                                'title' => 'Документы',
                                                'style' => "padding-left: 5px;",
                                            ]); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if ($company) : ?>
                                    <?= CompanyLimitsWidget::widget(['company' => $company]) ?>
                                <?php endif; ?>

                                <div class="header-others-column column">
                                    <div class="dropdown dropdown_products">
                                        <button class="button-clr dropdown-btn" type="button" id="dropdownProducts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-left: 2px;">
                                            <?= $this->render('svg/tiles') ?>
                                        </button>
                                        <div class="dropdown-popup dropdown-popup_products dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?= $this->render('_available_products', [
                                                'currentModule' => $currentModule,
                                                'module' => $module,
                                                'currentController' => $controller,
                                                'canBuhIP' => $canBuhIP,
                                                'isDemo' => $isDemo,
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($company) : ?>
                                    <?= CompanySubscribesWidget::widget(['company' => $company]) ?>
                                <?php endif; ?>

                                <div class="header-others-column column">
                                    <div class="dropdown">
                                        <button class="button-clr dropdown-btn" type="button" id="dropdownNotif" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?= Icon::get('bell2', ['class' => 'dropdown-btn-icon']) ?>
                                            <span class="dropdown-btn-indicator indicator <?= ($hasNewNotifications ? 'active' : '') ?>">&nbsp;</span>
                                        </button>
                                        <div class="dropdown-popup dropdown-popup_notifications dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="dropdown-popup-in">
                                                <ul class="notification-list list-clr">
                                                    <?php foreach ($notifications as $notification) : ?>
                                                        <li>
                                                            <a class="notification-link" href="<?= $notificationsUrl ?>">
                                                                <span class="notif-marker">
                                                                    <?= Icon::get('attention') ?>
                                                                </span>
                                                                <span class="notif-details">
                                                                    <span class="notification-message" title="<?= $notification->title; ?>">
                                                                        <?= $notification->title; ?>
                                                                    </span>
                                                                    <br>
                                                                    <span class="notification-time">
                                                                        <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                                                            'format' => 'd F',
                                                                            'monthInflected' => true,
                                                                        ]); ?>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                    <?php foreach ($marketingNotifications as $notification) : ?>
                                                        <li>
                                                            <a class="notification-link" href="<?= \common\modules\marketing\models\MarketingChannel::getUrl($notification->channel_id) ?>">
                                                                <span class="notif-marker">
                                                                    <?= Icon::get('attention') ?>
                                                                </span>
                                                                <span class="notif-details">
                                                                    <span class="notification-message" title="<?= $notification->message; ?>">
                                                                        <?= $notification->channel->name; ?><br/>
                                                                        <span style="font-size: 14px">
                                                                            <?= $notification->message ?>
                                                                        </span>
                                                                    </span>
                                                                    <span class="notification-time">
                                                                        <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                                                            'format' => 'd F',
                                                                            'monthInflected' => true,
                                                                            'date' => $notification->created_at,
                                                                        ]); ?>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                                <a class="all-notifications link link_bold" href="<?= $notificationsUrl ?>">Все уведомления</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="header-others-column column">
                                <div class="dropdown">
                                    <button class="button-clr dropdown-btn d-flex align-items-center" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 40px;">
                                        <?= Icon::get('user', [
                                            'class' => 'dropdown-btn-icon',
                                            'style' => 'font-size: 40px;'
                                        ]) ?>
                                    </button>
                                    <div class="dropdown-popup dropdown-menu dropdown-user-menu" aria-labelledby="dropdownMenuButton">
                                        <?php if ($user) : ?>
                                            <div class="dropdown-popup-in">
                                                <div class="dropdown-popup-head">
                                                    <div class="dropdown-user-name">
                                                        <?= $user->shortFio ?>
                                                    </div>
                                                    <a class="dropdown-user-mail" href="mailto:<?= $user->email ?>">
                                                        <?= $user->email ?>
                                                    </a>
                                                </div>
                                                <ul class="dropdown-list list-clr">
                                                    <?php foreach ($userItems as $item) : ?>
                                                        <?php if ($item['visible']) : ?>
                                                            <li>
                                                                <?= Html::a($item['label'], $item['url'], [
                                                                    'class' => 'dropdown-list-link',
                                                                    'style' => $item['style'] ?? null
                                                                ]) ?>
                                                            </li>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                </ul>
                                                <div class="dropdown-popup-foot">
                                                    <?= Html::a('Выход', '#logout-confitm-modal', [
                                                        'class' => 'dropdown-list-link',
                                                        'data-toggle' => 'modal',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</header>

<?php if (!Yii::$app->user->isGuest) : ?>
    <?php Modal::begin([
        'id' => 'freeTariffNotify',
        'toggleButton' => false,
    ]) ?>

        <h4 class="modal-title">
            <?php if (Yii::$app->user->identity->company->freeTariffNotified) : ?>
                Текущая подписка - БЕСПЛАТНО
            <?php else : ?>
                Ваш аккаунт переведен на тариф БЕСПЛАТНО
            <?php endif ?>
        </h4>

        <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#close"></use>
            </svg>
        </button>
        <div style="font-weight: 400; font-size: 16px; color: #333;">
            <p>
                Ограничения на тарифе БЕСПЛАТНО:
            </p>
            <ul>
                <li>5 счетов в месяц</li>
                <li>Только 1 организация</li>
                <li>Не больше 3-х сотрудников</li>
                <li>Место на диске - 1 ГБ</li>
            </ul>
            <p>
                Что бы работать без ограничений нужно <?= Html::a('перейти на платный тариф', ['/subscribe']) ?>.
            </p>
        </div>

    <?php Modal::end() ?>

    <?php $this->registerJs('
        $(document).on("click", ".action-is-limited", function(e) {
            e.preventDefault();
            $("#freeTariffNotify").modal("show");
        });
    ') ?>
    <?php $this->registerJs('
        $("#dropdownNotif").parent().on("shown.bs.dropdown", function () {
        var notificationMenu = $(this);
            if (notificationMenu.find(".indicator.active").length > 0) {
                $.post("/employee/view-notification", { id : '. Yii::$app->user->id .' }, function() {
                    notificationMenu.find(".indicator.active").removeClass("active");
                });
            }
        });
    ') ?>
<?php endif ?>
