<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
   <?= Nav::widget([
        'id' => 'one-s-menu',
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            'style' => 'margin-bottom: 20px;'
        ],
        'items' => [
            [
                'label' => 'Выгрузка в 1С',
                'url' => ['/export/one-s/index'],
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . (Yii::$app->controller->module->id == 'export' ? ' active' : '')
                ]
            ],
            [
                'label' => 'Загрузка из 1С',
                'url' => ['/import/one-s/index'],
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . (Yii::$app->controller->module->id == 'import' ? ' active' : '')
                ]
            ],
        ],
    ]);
    ?>
    <?= $content; ?>
</div>

<?php $this->endContent(); ?>
