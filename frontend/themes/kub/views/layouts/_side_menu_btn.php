<div class="sidebar-menu-head" id="item<?=$id?>">
    <button class="sidebar-menu-btn button-clr collapsed d-flex align-items-center" type="button"
        data-toggle="collapse" data-target="#item<?=$id?>Content"
        aria-expanded="false" aria-controls="item<?=$id?>Content">
        <svg class="sidebar-menu-icon svg-icon" <?= (!empty($style)) ? 'style="'.$style.'"' : '' ?>>
            <use xlink:href="/img/svg/svgSprite.svg#<?=$i?>"></use>
        </svg>
        <span class="sidebar-menu-btn-txt">{label}</span>
        <span class="ml-auto d-flex align-items-center flex-nowrap">
            <?php if (!empty($append)) : ?>
                <?= $append ?>
            <?php endif ?>
            <svg class="sidebar-menu-angle svg-icon ml-1">
                <use xlink:href="/img/svg/svgSprite.svg#arrow"></use>
            </svg>
        </span>
    </button>
</div>