<?php

use common\components\image\EasyThumbnailImage;
use common\models\file\FileDir;
use frontend\widgets\BtnConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\widgets\Pjax;

$visibleOutInvoiceFacture = false;
$visibleInInvoiceFacture = false;
$isIp = false;
$logo = $this->render('svg/company');
$companyItems = [];
$userAllowedDirs = FileDir::getEmployeeAllowedDirs();
$folderItems = [];
foreach ($userAllowedDirs as $dir) {
    /** @var $dir FileDir */
    $folderItems[] = [
        'label' => $dir->name,
        'options' => ['id' => 'dir-'.$dir->id, 'class' => 'sideabar-menu-item directory-wrapper'],
        'url' => ['/documents/upload-manager/directory/', 'id' => $dir->id],
        'template' => $this->render('_side_menu_dir', ['dir' => $dir]),
        'active' => $action == 'directory' && Yii::$app->request->get('id') == $dir->id,
        'encode' => false
    ];
}

if ($user) {
    if ($company) {
        $imgPath = $company->getImage('logoImage');
        if (is_file($imgPath)) {
            $logo = EasyThumbnailImage::thumbnailImg($imgPath, 22, 22, EasyThumbnailImage::THUMBNAIL_INSET, [
                'style' => 'max-width: 100%; max-height: 100%;',
            ]);
        }
    }

    $companyId = $user->company ? $user->company->id : null;
    $employeeCompanyArray = $user->getEmployeeCompany()
        ->joinWith('company.companyType')
        ->andWhere([
            'employee_company.is_working' => true,
            'company.blocked' => false,
        ])->orderBy([
            new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
            "company_type.name_short" => SORT_ASC,
            "company.name_short" => SORT_ASC,
        ])->all();
    $imgPath = $company->getImage('logoImage');
    if (is_file($imgPath)) {
        $mainLogo = EasyThumbnailImage::thumbnailImg($imgPath, 40, 40, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $mainLogo = $this->render('svg/company');
    }
    $companyArray = ArrayHelper::getColumn($employeeCompanyArray, 'company');
    $newFilesCount = $company ? \common\models\document\ScanDocument::getNewScansCount($company) : 0;

    foreach ($companyArray as $item) {
        if ($item->id != $companyId) {
            $companyItems[] = [
                'label' => Html::encode($item->shortTitle),
                'encode' => false,
                'url' => [
                    '/site/change-company',
                    'id' => $item->id,
                ],
                'active' => $item->id == $companyId,
            ];
        }
    }

    $companyItems[] = [
        'label' => 'Добавить компанию',
        'encode' => false,
        'url' => '#create-company',
        'template' => '<a data-toggle="modal" href="{url}">'.\frontend\themes\kub\helpers\Icon::PLUS . '<span class="ml-2">{label}</span></a>'
    ];


    $documentItems = [
        [
            'label' => 'Все документы',
            'url' => ['/documents/upload-manager/all-documents'],
            'template' => $this->render('_side_menu_link', ['i' => 'files']),
            'active' => $action === 'all-documents',
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        [
            'label' => 'Распознавание',
            'url' => ['/documents/upload-manager/upload'],
            'template' => $this->render('_side_menu_link', ['i' => 'recognize', 'count' => $newFilesCount]),
            'active' => $action === 'upload',
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        [
            'label' => 'Папки',
            'options' => [
                'class' => 'sideabar-menu-item add-folder'
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'folder', 'i_right' => 'increase-icon']),
        ],
    ];

    $items = [
        [
            'label' => 'Мои компании',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => "company1", 'i' => 'home']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'items' => $companyItems,
            'visible' => !$isDemo,
        ],
        [
            'label' => $company ? Html::encode($company->shortTitle) : '',
            'encode' => false,
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => null, 'img' => $mainLogo]),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => "company1"]),
            'url' => ['/site/index'],
        ],
        //=================================================================================================
        [
            'label' => 'Назад в меню',
            'url' => \yii\helpers\Url::previous('UploadManagerReturn') ?: '/site/index',
            'template' => $this->render('_side_menu_link', ['i' => 'arrow-left']),
            'options' => [
                'class' => 'sideabar-menu-item',
            ],
        ],
        [
            'label' => 'Документы',
            'options' => [
                'class' => 'sideabar-menu-item sideabar-menu-item-docs'
            ],
            'template' => $this->render('_side_menu_btn', ['id' => 1, 'i' => 'folder']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 1]),
            'items' => array_merge($documentItems, $folderItems)
        ],
        [
            'label' => 'Корзина',
            'options' => [
                'class' => 'sideabar-menu-item'
            ],
            'template' => $this->render('_side_menu_link', ['i' => 'garbage']),
            'url' => ['/documents/upload-manager/trash'],
        ]
    ];

} else {
    $items = [];
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-upload-dir',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'zIndex' => 10000,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-dir-options',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'triggerOpen' => ['click' => true],
        'triggerClose' => ['originClick' => true, 'click' => true],
        'trackOrigin' => true,
        'trackerInterval' => 50,
        'delay' => 0,
        'animationDuration' => 0,
        'contentAsHTML' => true,
        'debug' => false,
        'zIndex' => 10000,
        'position' => 'bottom',
        'interactive' => true,
        'functionReady' => new \yii\web\JsExpression('function(origin, tooltip) { $(origin._$origin).parent().addClass("active-opt") }'),
        'functionAfter' => new \yii\web\JsExpression('function(origin, tooltip) { $(origin._$origin).parent().removeClass("active-opt") }')
    ],
]);

/* modals ----------------------------- */

Modal::begin([
    'id' => 'add-directory-modal',
    'size' => 'modal-dialog_width-535'
]);
Pjax::begin([
    'id' => 'add-directory-form',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();


Modal::begin([
    'id' => 'unable-delete-folder-modal',
    'class' => 'confirm-modal ',
    'headerOptions' => ['style' => 'display:none'],
    'footerOptions' => ['style' => 'border:none; padding-top:0; margin:auto;'],
    'footer' => Html::button('Ок', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]),
]);
$header = 'Нельзя удалить папку';

echo Html::beginTag('div', ['class' => 'text-center']);
echo Html::tag('h4', 'Перед удалением папки, просьба удалить все документы из папки или перенести их в другую папку', ['class' => 'modal-title']);
echo Html::endTag('div');
Modal::end();


/* end modals --------------------------*/

?>

<div class="tooltip_templates" style="display: none;">
    <?php foreach($userAllowedDirs as $dir): ?>
        <?php $dirFilesCount = count($dir->files); ?>
        <span id="<?= "tooltip-upload-dir-{$dir->id}" ?>" style="display: inline-block;">
            <?= $dir->name; ?>
        </span>
        <span id="<?= "dir-options-{$dir->id}" ?>">

            <?= Html::a('Редактировать', '#', [
                'class' => 'dir-opts edit-folder link',
                'data-id' => $dir->id
            ]) ?>

            <?php if ($dirFilesCount == 0): ?>
                <?= \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Удалить',
                        'class' => 'dir-opts remove-folder link',
                        'tag' => 'span',
                    ],
                    'confirmUrl' => Url::to(['/documents/upload-manager/remove-dir', 'id' => $dir->id]),
                    'message' => 'Вы уверены, что хотите удалить папку?',
                ]); ?>
            <?php else: ?>
                <?= Html::a('Удалить', '#unable-delete-folder-modal', [
                    'class' => 'dir-opts remove-folder link',
                    'data-toggle' => 'modal',
                ]); ?>
            <?php endif; ?>

            <?= Html::a('Вверх', '#', [
                'class' => 'dir-opts move-folder link',
                'data-direction' => 'up',
                'data-id' => $dir->id
            ]) ?>

            <?= Html::a('Вниз', '#', [
                'class' => 'dir-opts move-folder link',
                'data-direction' => 'down',
                'data-id' => $dir->id
            ]) ?>
        </span>

    <?php endforeach; ?>
</div>

<div class="page-sidebar column">
    <div class="sidebar sidebar_fixed">
        <?php if ($user) : ?>
            <?php /*<div class="sidebar-scroll custom-scroll">*/ ?>
                <?= Menu::widget([
                    //'activateParents' => true,
                    'items' => $items,
                    'options' => [
                        'id' => 'sidebarCollapse',
                        'class' => 'sidebar-menu list-clr',
                    ]
                ]) ?>
            <?php /*</div>*/ ?>
        <?php endif ?>
    </div>
</div>


<?php
$this->registerJs('
$(document).ready(function () {
    $(".sideabar-menu-item .collapse:has(.active)").collapse("show");
});

// Add folder
$(document).on("click", ".add-folder", function(e) {
    e.preventDefault();
    $(".tooltip-dir-options").tooltipster("hide");
    $.pjax({url: "/documents/upload-manager/create-dir", container: "#add-directory-form", push: false});
    $("#add-directory-modal").modal("show")
        .find(".modal-header > h1").text("Новая папка");
});

// Edit folder
$(".tooltip_templates .edit-folder").on("click", function(e) {
    e.preventDefault();
    $(".tooltip-dir-options").tooltipster("hide");
    var id = $(this).attr("data-id");
    $.pjax({url: "/documents/upload-manager/update-dir/?id=" + id, container: "#add-directory-form", push: false});
    $("#add-directory-modal").modal("show")
        .find(".modal-header > h1").text("Редактировать папку");
});

// Remove folder
$(document).on("click", "button.btn-confirm-yes", function(){
    var $this = $(this);
    $.ajax({
        url: $this.data("url"),
        type: $this.data("type"),
        data: $this.data("params"),
        success: function(data) {
            //$(".page-content").prepend(successFlashMessageDelete);
        }
    });
    return false;
});

// Move folder up/down
$(".tooltip_templates .move-folder").on("click", function(e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    var direction = $(this).attr("data-direction");
    $.post("/documents/upload-manager/move-dir", {id:id, direction:direction}, function (data) {
        if (data.swap_id) {
            if (direction === "up")
                $("#dir-" + data.swap_id).before($("#dir-" + id));
            else
                $("#dir-" + data.swap_id).after($("#dir-" + id));
        }

    })
});

');
?>
