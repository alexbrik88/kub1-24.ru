<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PaymentReminderReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по отправленным письмам';

?>

<?= $this->render('_menu', ['currentTab' => 'report']) ?>

<div class="report-index">


    <div class="wrap">
        <div class="row">
            <div class="col-6 col-xl-9">
                <h3 class="page-title m-0"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'У вас пока нет отправленных писем',
        'tableOptions' => [
            'class' => 'table table-style table-count-list rent-calendar',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            'date:date',
            [
                'attribute' => 'contractor_id',
                'filter' => $searchModel->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '300px',
                'value' => 'contractor.nameWithType',
            ],
            [
                'attribute' => 'debt_amount',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model->debt_amount, 2);
                }
            ],
            'invoice:ntext',
            'days_overdue',
            [
                'attribute' => 'is_sent',
                'format' => 'html',
                'filter' => $searchModel->getIsSentFilterItems(),
                's2width' => '140px',
                'value' => function ($model) {
                    return Html::tag('span', $model->getIsSentText(), [
                        'class' => $model->is_sent ? '' : 'text-danger',
                    ]);
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => $searchModel->getCategoryFilterItems(),
                's2width' => '200px',
                'value' => 'category.name',
            ],
            'event:html',
            'subject',
        ],
    ]); ?>
</div>
