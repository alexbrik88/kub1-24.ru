<?php

use common\components\date\DateHelper;
use common\models\paymentReminder\PaymentReminderMessage;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model PaymentReminderMessage */
/* @var $isFreeTariff boolean */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
Modal::begin([
    'id' => 'modal-update-payment-reminder-' . $model->id,
]);
$form = ActiveForm::begin([
    'action' => Url::to(['update', 'id' => $model->id]),
    'options' => [
        'id' => 'form-payment-reminder-message-' . $model->id,
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'checkOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => '',
            ],
        ],
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized',],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<h4 class="modal-title"><?= 'Настройка шаблона письма №' . $model->number ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="form-body">
    <div class="form-group warning-text hidden">Текст и параметры шаблона измените под ваши требования</div>
    <?= Html::activeHiddenInput($model, 'activate'); ?>
    <?= Html::activeHiddenInput($model, 'status'); ?>
    <?= Html::activeHiddenInput($model, 'number'); ?>
    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="label">
                Условие отправки:
            </label>
            <div class="">
                <?= $model->event_header; ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [6, 7, 8, 9])): ?>
        <div class="form-group required">
            <label class="label" for="checkingaccountant-rs">
                Условие отправки:
            </label>
            <div class="">
                Сумма просроченного долга больше суммы: <?= Html::activeTextInput($model, 'debt_sum', [
                    'class' => 'form-control',
                    'style' => 'display: inline-block; width: 70px; background-image: none; padding-right: 15px;',
                ]); ?>
                <span id="tooltip_payment-reminder-debt-sum-<?= $model->id; ?>"
                      class="tooltip3 ico-question valign-middle"
                      data-tooltip-content="#tooltip_debt-sum-reminder-<?= $model->id; ?>"
                      style="margin-left: 10px;cursor: pointer;"></span>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Условие отправки:
            </label>
            <div class="">
                Счет оплачен до истечения срока оплаты.
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number == 1): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "За {input} дней до окончания срока отсрочки платежа по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline-block; width: 70px; background-image: none; padding-right: 15px;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input} день просрочки оплаты по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline-block; width: 70px; background-image: none; padding-right: 15px;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif ((int)$model->number == 6): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>
            <div class="">
                В день превышения суммы всей просроченной задолженности над указанной выше суммой.
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [7, 8, 9])): ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>

            <div class="">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input}  день превышения суммы всей просроченной задолженности над указанной выше суммой. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline; width: 70px; background-image: none; padding-right: 15px;',
                ])->label(false); ?>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                День отправки письма:
            </label>

            <div class="">
                В день изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'message_template_subject', $config)->textInput([
        'maxlength' => true,
    ])->label('Тема письма:'); ?>

    <div class="form-group required">
        <label class="label">
            Текст письма:
        </label>

        <div class="">
            <div class="message-template-body_editable_block" contenteditable="true"
                 style="border: solid 2px #e2e5eb;width: 100%;padding: 6px 12px;border-radius: 4px;">
                <?= $model->message_template_body; ?>
            </div>
            <?= Html::activeHiddenInput($model, 'message_template_body'); ?>
        </div>
    </div>

    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 6, 7, 8, 10])): ?>
        <div class="form-group required">
            <label class="label">
                Кому:
            </label>
            <div class="row">
                <div class="col-8 send_for_inputs">
                    <div>
                        <?= $form->field($model, 'send_for_chief', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'options' => [
                                'style' => 'width: 30%;display: inline-block;',
                            ],
                        ])->checkbox([], true)->label('Руководителю'); ?>
                    </div>
                    <div>
                        <?= $form->field($model, 'send_for_contact', [
                            'options' => [
                                'class' => '',
                                'style' => 'display: inline-block;',
                            ],
                        ])->checkbox([], true)->label('Контактному лицу'); ?>
                    </div>
                    <div>
                        <?= $form->field($model, 'send_for_all', [
                            'options' => [
                                'class' => '',
                            ],
                        ])->checkbox([], true)->label('Всем, кто есть в карточке Покупателя'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Кому: <span id="tooltip_payment-reminder-send-to-<?= $model->id; ?>"
                      class="tooltip3 ico-question valign-middle"
                      data-tooltip-content="#tooltip_send-to-reminder-<?= $model->id; ?>"
                      style="cursor: pointer;"></span>
            </label>
            <div class="" style="max-width: 300px;">
                <?= $form->field($model, 'email1')->label(false) ?>
                <?= $form->field($model, 'email2')->label(false) ?>
                <?= $form->field($model, 'email3')->label(false) ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number == 10): ?>
        <div class="form-group required">
            <label class="label">
                Не отправлять письмо:
            </label>

            <div class="mt-2">
                <?= Html::activeCheckbox($model, 'not_send_where_order_payment', [
                    'label' => 'При оплате по кассе',
                    'labelOptions' => [
                        'class' => 'mr-4',
                    ],
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_emoney_payment', [
                    'label' => 'При оплате по e-money',
                    'labelOptions' => [
                        'class' => 'mr-4',
                    ],
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_partial_payment', [
                    'label' => 'При частичной оплате счета',
                ]); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number !== 10): ?>
        <?php if (!$model->timeInput) {
            $model->timeInput = '09:45';
        } ?>
        <?php $time = explode(':', $model->timeInput) ?>
        <?= $form->field($model, 'timeInput', [
            'template' => "{label}\n<div class=\"date-picker-wrap button-width\">
                {input}
            </div>\n{hint}\n{error}"
        ])->textInput([
            'class' => 'form-control time-picker ico time-picker-' . $model->id,
            'readonly' => true,
            'style' => 'background-color: #ffffff;',
        ])->label('Время отправки письма') ?>
        <script>
            jQuery(document).ready(function () {
                let cfg = kubDatepickerConfig;
                cfg.timepicker = true;
                cfg.onlyTimepicker = true;
                cfg.timeFormat = 'hh:ii';
                cfg.minutesStep = 5;
                cfg.startDate = new Date(null, null, null,<?= $time[0] ?? 'null' ?>,<?= $time[1] ?? 'null'?>);
                $('.time-picker-<?=$model->id?>').datepicker(cfg);
            });
        </script>
    <?php else: ?>
        <div class="form-group required">
            <label class="label">
                Время отправки письма:
            </label>
            <div class="">
                После изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>
    <div class="mt-3 d-flex justify-content-between">
        <?php if (!$isFreeTariff || $model->number == 1): ?>
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        <?php else: ?>
            <?= Html::button('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr no-rules-update',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#activate-modal-no-rules-' . $model->id,
                ],
            ]); ?>
            <?= $modalNoRules = $this->render('no_rules_modal', [
                'model' => $model,
            ]); ?>
        <?php endif; ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent back',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>
<?php Modal::end(); ?>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_send-to-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Поставьте e-mail нашего юриста: urist@kub-24.ru <br>
        При получении письма он свяжется с вами, что бы подготовить претензию. <br>
        Стоимость подготовки претензии 3000 рублей
    </span>
    <span id="tooltip_debt-sum-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Укажите сумму, если сумма всей просроченной <br>
        задолженности ее превысит, то отправится <br>
        письмо
    </span>
</div>
<?php $this->registerJs('
    $(".no-rules-update").click(function () {
        $(this).closest(".modal").modal("hide");
    });
'); ?>

