<?php

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Settings;
use frontend\assets\PaymentReminderAsset;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\models\PaymentReminderMessageContractorSearch;
use frontend\modules\documents\components\FilterHelper;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$isFreeTariff = $user->company->isFreeTariff;

$activeTemplates = PaymentReminderMessage::find()
    ->andWhere(['company_id' => $user->company_id])
    ->andWhere(['status' => PaymentReminderMessage::STATUS_ACTIVE])
    ->indexBy('number')
    ->all();

$hasActiveTemplate = !empty($activeTemplates);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');

$columns = [
    [
        'attribute' => 'contractor',
        'label' => 'Покупатель',
        'headerOptions' => [
            'rowspan' => 2,
            'colspan' => 1,
            'width' => '15%',
            'class' => 'fixed-column',
        ],
        'contentOptions' => [
            'class' => 'fixed-column',
        ],
        's2width' => '300px',
        'hideSearch' => false,
        'filter' => $searchModel->getContractorFilter(),
        'value' => function (PaymentReminderMessageContractor $model) {
            return $model->contractor->getTitle(true);
        }
    ],
    [
        'attribute' => 'invoiceSum',
        'label' => 'Долг',
        'headerOptions' => [
            'rowspan' => 2,
            'colspan' => 1,
            'class' => 'col_payment_reminder_message_contractor_invoice_sum' .
                ($user->config->payment_reminder_message_contractor_invoice_sum ? null : ' hidden'),
            'width' => '8%',
        ],
        'contentOptions' => [
            'class' => 'sorting col_payment_reminder_message_contractor_invoice_sum' .
                ($user->config->payment_reminder_message_contractor_invoice_sum ? null : ' hidden'),
        ],
        'value' => function (PaymentReminderMessageContractor $model) {
            return DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, false, true, $model->contractor_id);
        }
    ],
    [
        'label' => 'Письма',
        'headerOptions' => [
            'type' => 'header',
            'class' => 'text-center',
            'rowspan' => 1,
            'colspan' => 10,
            'style' => 'border-bottom: 1px solid #ddd;',
        ],
    ],
];

foreach (range(1, 10) as $i) {
    $selectedAll = true;
    foreach ($dataProvider->models as $item) {
        $selectedAll = $selectedAll && $item->{'message_' . $i};
    }
    $isActive = isset($activeTemplates[$i]);
    $columns[] = [
        'attribute' => 'message_'.$i,
        'header' => Html::checkbox(null, $selectedAll, [
            'class' => "paymentremindermessagecontractor-message-all messagecontractor_{$i}_all",
            'label' => '№'.$i,
            'labelOptions' => [
                'style' => 'font-weight: 600; margin-right: 5px;',
            ],
            'data' => [
                'can-update' => $isActive,
                'number' => 1,
                'target' => "input:checkbox.messagecontractor_$i",
            ],
            'disabled' => true,
        ]),
        'headerOptions' => [
            'rowspan' => 1,
            'colspan' => 1,
            'class' => "tooltip2 col_payment_reminder_message_contractor_message_$i" .
                ($isActive ? ' can_update' : '') .
                ($user->config->{"payment_reminder_message_contractor_message_$i"} ? null : ' hidden'),
            'data-tooltip-content' => $isActive ? '#template_body-header-'.$i : null,
            'title' => $isActive ? null : 'Данное письмо не активно, включите его на шаге "Настройка шаблонов писем"',
            'width' => '1%',
        ],
        'contentOptions' => [
            'title' => $isActive ? null : 'Данное письмо не активно, включите его на шаге "Настройка шаблонов писем"',
            'class' => "col_payment_reminder_message_contractor_message_$i" .
                ($isActive ? ' can_update' : '') .
                ($user->config->{"payment_reminder_message_contractor_message_$i"} ? null : ' hidden'),
        ],
        'format' => 'raw',
        'value' => function (PaymentReminderMessageContractor $model) use ($user, $i, $isActive) {
            $lastDate = DateHelper::format($model->{"last_message_date_$i"}, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

            return Html::activeCheckbox($model, "message_$i", [
                'id' => "messagecontractor_{$i}_{$model->contractor_id}",
                'class' => "paymentremindermessagecontractor-message messagecontractor_{$i}",
                'label' => false,
                'name' => "PaymentReminderMessageContractor[message_{$i}][{$model->contractor_id}]",
                'disabled' => true,
                'data' => [
                    'can-update' => $isActive,
                    'number' => $i,
                    'target' => "input:checkbox.messagecontractor_$i",
                ],
            ]) .
                '<span class="payment-reminder-tooltip"
                    data-contractor_email="' . $model->{"last_message_contractor_email_$i"} . '"
                    data-date="' . $lastDate . '">' . $lastDate . '</span>';
        }
    ];
}

$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'template' => '{delete}',
    'headerOptions' => [
        'rowspan' => 2,
        'colspan' => 1,
        'width' => '3%',
    ],
    'buttons' => [
        'delete' => function ($url, $model, $key) {
            return Html::a(Icon::get('garbage'), '#reminder-comtractor-delete-'.$model->id, [
                'data-toggle' => 'modal',
            ]);
        },
    ],
];
?>
<style type="text/css">
    .page-size-selector {
        margin-bottom: 12px !important;
    }
</style>
<?php Pjax::begin([
    'id' => 'payment-reminder-message-contractor-pjax',
    'enablePushState' => false,
    'timeout' => 10000,
    'options' => [
        'class' => 'pb-0',
    ],
]); ?>
    <div class="row mb-0">
        <div class="col-4">
            <div class="mb-2">
                <?= TableConfigWidget::widget([
                    'items' => [
                        [
                            'attribute' => 'payment_reminder_message_contractor_invoice_sum',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_1',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_2',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_3',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_4',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_5',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_6',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_7',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_8',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_9',
                        ],
                        [
                            'attribute' => 'payment_reminder_message_contractor_message_10',
                        ],
                    ],
                ]); ?>
                <?= TableViewWidget::widget(['attribute' => 'table_view']) ?>
            </div>
        </div>
        <div class="col-8">
            <div class="actions float-l pensil-box text-right">
                <?= Html::button('Добавить', [
                    'class' => 'button-regular button-width button-regular_red',
                    'title' => 'Добавить покупателя в список',
                    'data-toggle' => 'modal',
                    'data-target' => '#add_message_contractor_modal',
                ]); ?>
                <?= Html::button('Редактировать', [
                    'class' => 'update-payment-reminder-message-contractor button-regular button-width button-regular_red' .
                                ($hasActiveTemplate ? null : ' tooltip3 disabled'),
                    'data-tooltip-content' => $hasActiveTemplate ? null : '#tooltip-active-template',
                    'data-tooltipster' => $hasActiveTemplate ? null : "{'trigger':'hover'}",
                ]); ?>
                <?= Html::submitButton('Сохранить', [
                    'form' => 'form-payment-reminder-message-contractor',
                    'class' => 'button-regular button-width button-regular_red hidden ladda-button',
                ]); ?>
                <?= Html::button('Отменить', [
                    'class' => 'undo-payment-reminder-message-contractor hidden button-regular button-width button-hover-transparent',
                ]); ?>
            </div>
            <div class="tooltip-template hidden">
                <span class="text-left" id="tooltip-active-template" style="display: inline-block;">
                    У вас нет активных писем.<br>
                    Необходимо включить хотя бы одно письмо.
                </span>
            </div>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'form-payment-reminder-message-contractor',
            'action' => Url::to(['update-message-contractor']),
            'options' => [
                'class' => 'col-12 form-payment-reminder-message-contractor',
                'data-pjax' => 1,
            ],
        ]); ?>
            <?= GridView::widget([
                'id' => 'payment-reminder-message-contractor-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list mb-0 scrollable-table'.$tabViewClass,
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout_no_scroll', [
                    'totalCount' => $dataProvider->totalCount,
                ]),
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'columns' => $columns,
            ]); ?>
        <?php ActiveForm::end(); ?>
    </div>

<?php foreach ($dataProvider->models as $model) : ?>
    <?php Modal::begin([
        'id' => 'reminder-comtractor-delete-'.$model->id,
        'options' => [
            'class' => 'confirm-modal fade',
        ],
        'closeButton' => false,
        'toggleButton' => false,
    ]); ?>
        <h4 class="modal-title text-center mb-4">
            <?= Html::encode("Удалить {$model->contractor->getTitle(true)} из списка?") ?>
        </h4>
        <?= Html::beginForm([
            'customer-delete',
            'id' => $model->id
        ], 'post', [
            'class' => 'form-actions row reminder-comtractor-delete-form',
        ]) ?>
            <div class="col">
                <?= Html::submitButton('ДА', [
                    'class' => 'button-regular button-hover-transparent button-width-medium pull-right',
                ]); ?>
            </div>
            <div class="col">
                <?= Html::button('НЕТ', [
                    'class' => 'button-regular button-hover-transparent button-width-medium',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        <?= Html::endForm() ?>
    <?php Modal::end() ?>
<?php endforeach ?>

<?php Pjax::end(); ?>
