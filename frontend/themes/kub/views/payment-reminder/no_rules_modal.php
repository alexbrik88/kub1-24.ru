<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.05.2018
 * Time: 6:26
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="activate-modal-no-rules-<?= $model->id; ?>" class="confirm-modal fade modal" role="dialog" tabindex="-1"
     aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">
                Только для платных тарифов. Перейти к оплате?
            </h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', Url::to(['/subscribe']), [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
    </div>
</div>