<?php

use frontend\rbac\permissions\PaymentReminder;
use frontend\components\Icon;
use yii\helpers\Url;

/**
 * @var string $currentTab
 */
$menu = [
    [
        'title' => 'Описание',
        'tab' => 'index',
        'icon' => 'file',
        'visible' => Yii::$app->user->can(PaymentReminder::INDEX),
    ],
    [
        'title' => 'Настройка шаблонов писем',
        'tab' => 'template',
        'icon' => 'templates',
        'visible' => Yii::$app->user->can(PaymentReminder::TEMPLATE),
    ],
    [
        'title' => 'Настройка автоматической рассылки писем',
        'tab' => 'settings',
        'icon' => 'email-setting',
        'visible' => Yii::$app->user->can(PaymentReminder::SETTINGS),
    ],
    [
        'title' => 'Отчёт',
        'tab' => 'report',
        'icon' => 'email-result',
        'visible' => Yii::$app->user->can(PaymentReminder::REPORT),
    ],
];
?>
<div class="menu-nav-tabs nav-tabs-row pb-3 mb-1">
    <ul class="nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 justify-content-around">
        <?php foreach ($menu as $i => $item) { ?>
            <?php if ($item['visible']) : ?>
                <li class="nav-item pl-2 pr-2 d-flex flex-column<?php if($item['tab']===$currentTab) echo ' active'; ?>">
                    <a class="nav-link d-flex flex-column flex-grow-1 align-items-center text-center pb-2" href="<?= Url::toRoute([$item['tab']]) ?>">
                        <div class="mb-2 pb-1">
                            <?= Icon::get($item['icon']) ?>
                        </div>
                        <div class="flex-grow-1 d-flex flex-column justify-content-center pb-1"><?= $item['title'] ?></div>
                    </a>
                </li>
            <?php endif ?>
        <?php } ?>
    </ul>
</div>