<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$contractorItems = ['' => 'Все'] + Contractor::getAllContractorList(Contractor::TYPE_CUSTOMER, false);
?>

<?php Pjax::begin([
    'id' => 'add_message_contractor_pjax',
    'enablePushState' => false,
    'timeout' => 10000,
]); ?>

    <?= Html::beginForm(null, null, [
        'class' => 'd-flex mb-3',
        'data-pjax' => true,
    ]) ?>

        <?= Html::activeTextInput($searchModel, 'search', [
            'type' => 'search',
            'class' => 'form-control flex-grow-1',
            'placeholder' => 'Поиск...',
        ]) ?>

        <?= Html::submitButton('Найти', [
            'class' => 'ml-1 button-regular button-regular_red min-w-130',
        ]); ?>

    <?= Html::endForm() ?>

    <?= GridView::widget([
        'id' => 'add_message_contractor_grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'data-current-url' => Url::current(),
        ],
        'tableOptions' => [
            'class' => 'table table-style table-count-list m-0 mb-4 scrollable-table table-compact',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'layout' => $this->render('//layouts/grid/layout_no_scroll', [
            'totalCount' => $dataProvider->totalCount,
        ]),
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    $exists = PaymentReminderMessageContractor::find()->andWhere([
                        'contractor_id' => $model->id,
                    ])->exists();

                    return [
                        'value' => $model->id,
                        'checked' => $exists,
                        'disabled' => $exists,
                    ];
                }
            ],
            [
                'attribute' => 'id',
                'label' => 'Покупатель',
                'filter' => $contractorItems,
                'hideSearch' => false,
                's2width' => '250px',
                'value' => 'nameWithType',
            ],
            [
                'attribute' => 'debt_count',
                'label' => 'Просрочено счетов',
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
            [
                'attribute' => 'debt_sum',
                'label' => 'Долг',
                'headerOptions' => [
                    'class' => 'text-right',
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model->debt_sum, 2);
                }
            ],
        ],
    ]); ?>

    <div class="row mt-4">
        <div class="col-6">
            <?= Html::button('Сохранить', [
                'id' => 'add_message_contractor_btn',
                'class' => 'button-regular button-regular_red button-width',
                'data-url' => Url::to(['payment-reminder/customer-add']),
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-6">
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-hover-transparent button-width pull-right',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

<?php Pjax::end(); ?>
