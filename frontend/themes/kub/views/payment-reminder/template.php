<?php

use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessage;
use frontend\assets\PaymentReminderAsset;
use frontend\models\PaymentReminderMessageSearch;
use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\web\View;

/* @var View $this
 * @var Employee                     $user
 * @var PaymentReminderMessageSearch $paymentReminderMessageSearchModel
 * @var ActiveDataProvider           $paymentReminderMessageDataProvider
 */

PaymentReminderAsset::register($this);
$this->title = 'Шаблоны писем должникам';

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-message-body'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'maxWidth' => 500,
    ],
]);

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-contractor-count',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-contractor-count'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
$isFreeTariff = $user->company->isFreeTariff;
echo $this->render('_menu', ['currentTab' => 'template']);
?>

<div class="wrap">
    <h4>Настройка шаблонов писем</h4>
    <div class="mt-3 mb-3">
        Настройте шаблоны писем, которые будут отправляться вашим должникам.
        Правила рассылки писем клиентам на следующем шаге «Настройка рассылки».
    </div>
    <?= GridView::widget([
        'id' => 'payment-reminder-message-grid',
        'dataProvider' => $paymentReminderMessageDataProvider,
        'filterModel' => $paymentReminderMessageSearchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list w-100',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => "{items}\n{pager}",
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'options' => [
            'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
        ],
        'columns' => [
            [
                'attribute' => 'number',
                'label' => '№',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) use ($user) {
                    if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                        return Html::a($model->number, null, [
                            'title' => Yii::t('yii', 'Редактировать'),
                            'aria-label' => Yii::t('yii', 'Редактировать'),
                            'data-pjax' => 0,
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#modal-update-payment-reminder-' . $model->id,
                                'pjax' => 0,
                            ],
                        ]);
                    }

                    return $model->number;
                }
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Категория',
                'headerOptions' => [
                    'width' => '7%',
                    'class' => 'dropdown-filter category-header',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'filter' => $paymentReminderMessageSearchModel->getCategoryFilter(),
                'value' => function (PaymentReminderMessage $model) {
                    return $model->category->name;
                },
                's2width' => '200px'
            ],
            [
                'attribute' => 'event_header',
                'label' => 'Событие',
                'headerOptions' => [
                    'width' => '27%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) {
                    return '<b>' . $model->event_header . '</b><br>' . $model->event_body;
                }
            ],
            [
                'attribute' => 'days',
                'label' => '"X" дней',
                'headerOptions' => [
                    'width' => '7%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) {
                    return $model->days ? $model->days : '-';
                }
            ],
            [
                'attribute' => 'message_template_subject',
                'label' => 'Тема шаблона письма',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) {
                    return Html::tag('span', $model->message_template_subject, [
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-update-payment-reminder-' . $model->id,
                    ]);
                }
            ],
            [
                'attribute' => 'message_template_body',
                'label' => 'Текст шаблона письма',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) {
                    return '<span class="tooltip2" data-tooltip-content="#template_body-' . $model->id . '">' .
                        mb_substr($model->message_template_body, 0, 35) .
                        '...</span>' .
                        '<div class="tooltip-template hidden">
                                    <span class="tooltip-payment-reminder-body text-left" id="template_body-' . $model->id . '" style="display: inline-block;">' .
                        $model->message_template_body .
                        '</span>
                                </div>';
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                    $title = ($model->status ? 'Oключить' : 'Включить') . ' письмо';
                    $id = 'template_status_'.$model->number;
                    $target = ($isFreeTariff && $model->number != 1) ?
                        '#activate-modal-no-rules-' . $model->id :
                        '#modal-update-payment-reminder-' . $model->id;
                    $content = Html::beginForm([
                        'template-status',
                        'id' => $model->id,
                    ], 'post', [
                        'id' => 'form_'.$id,
                        'class' => 'inline-block template_status_form',
                        'title' => $title,
                    ]);
                    $content .= Html::activeCheckbox($model, 'status', [
                        'id' => $id,
                        'class' => 'kub-switch template_status_switch',
                        'label' => null,
                        'data-target' => $target,
                    ]);
                    $content .= Html::endForm();

                    return $content;
                }
            ],
            [
                'label' => 'Кол-во покупателей',
                'headerOptions' => [
                    'class' => 'tooltip2-contractor-count',
                    'data-tooltip-content' => '#template_contractor_count',
                    'width' => '10%',
                    'style' => 'cursor: pointer;',
                ],
                'contentOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentReminderMessage $model) {
                    return $model->getContractorCount();
                }
            ],
            [
                'headerOptions' => [
                    'width' => '5%',
                ],
                'class' => ActionColumn::className(),
                'template' => '{edit}',
                'buttons' => [
                    'edit' => function ($url, PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                        if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                            $updateModal = $this->render('update_modal', [
                                'model' => $model,
                                'isFreeTariff' => $isFreeTariff,
                            ]);

                            return Html::a(Icon::get('pencil'), null, [
                                    'class' => 'update-payment-reminder-message link',
                                    'title' => Yii::t('yii', 'Редактировать'),
                                    'aria-label' => Yii::t('yii', 'Редактировать'),
                                    'data-pjax' => 0,
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#modal-update-payment-reminder-' . $model->id,
                                        'pjax' => 0,
                                    ],
                                ]) . $updateModal;
                        }

                        return null;
                    },
                ],
            ],
        ],
    ]); ?>

    <div class="tooltip-template hidden">
        <?php foreach (PaymentReminderMessage::find()->andWhere(['company_id' => $user->company_id])->all() as $paymentReminderMessage): ?>
            <span class="tooltip-payment-reminder-body text-left"
                  id="template_body-header-<?= $paymentReminderMessage->number; ?>"
                  style="display: inline-block;">
                <?= $paymentReminderMessage->message_template_body; ?>
            </span>
        <?php endforeach; ?>
        <span class="tooltip-payment-reminder-contractor-count text-left" id="template_contractor_count"
              style="display: inline-block;">
                Кол-во покупателей,<br>
                которым подключен<br>
                данный шаблон
            </span>
    </div>

</div>
