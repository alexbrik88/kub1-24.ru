<?php

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

?>

<?= \frontend\widgets\Alert::widget(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= $form->field($model, 'username') ?>

<?= $form->field($model, 'password', [
    'wrapperOptions' => [
        'class' => 'form-filter password-input-wrapper',
    ],
    'parts' => [
        '{input}' => Html::activePasswordInput($model, 'password', [
            'class' => 'form-control'
        ]) . Icon::get('eye', [
            'class' => 'toggle-password-input-type to-visible',
            'title' => 'Показать пароль',
        ]) . Icon::get('eye-ban', [
            'class' => 'toggle-password-input-type to-invisible',
            'title' => 'Скрыть пароль',
        ]),
    ],
]) ?>

<?= $form->field($model, 'rememberMe')->checkbox() ?>

<div class="form-group">
    <?= Html::submitButton('Войти', [
        'class' => 'button-regular button-regular_red w-100',
    ]) ?>
</div>

<?php ActiveForm::end(); ?>

<div class="label">
    Если Вы забыли пароль,
    <br>
    то Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
</div>
