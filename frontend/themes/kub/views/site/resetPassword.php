<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h4><?= Html::encode($this->title) ?></h4>

    <p>Пожалуйста введите новый пароль:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'reset-password-form',
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>
        <div class="row">
            <div class="col-5">
                <?= $form->field($model, 'password')->passwordInput(); ?>
                <?= Html::submitButton('Сохранить', ['class' => 'button-regular button-regular_red width-120']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
