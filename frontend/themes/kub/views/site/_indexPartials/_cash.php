<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;
use yii\widgets\Pjax;

/**
 * @var $id
 * @var $cashStatisticInfo array
 * @var $foraignCashStatisticInfo array
 */

$widgetDate = StatisticPeriod::getSessionPeriod()['to'];
$cashStatisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');
$pjaxId = "{$id}_pjax";
?>
<div id="<?=($id)?>" class="wrap wrap-block" style="overflow: visible">
    <div class="d-flex align-items-center pb-2">
        <div class="ht-caption pb-0">
            <b>ДЕНЬГИ</b> <span style="text-transform: none"> на </span> <?= DateHelper::format($cashStatisticsDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
        </div>
        <?= $this->render('_cash_config', ['pjaxId' => $pjaxId]) ?>
    </div>
    <?php Pjax::begin([
        'id' => $pjaxId,
        'linkSelector' => false,
        'options' => [
            'style' => 'overflow:hidden'
        ]
    ]) ?>
    <table class="ht-table dashboard-cash">
        <?php foreach ($cashStatisticInfo as $key => $item) : ?>
            <?php $isSubstring = in_array($item['cssClass'], ['cash-bank-substring', 'cash-order-substring', 'cash-emoney-substring', 'cash-acquiring-substring', 'cash-card-substring']); ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <?= $item['typeName']; ?>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <?= TextHelper::invoiceMoneyFormat($item['endBalance'] ?? 0, 2); ?>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php foreach ($foraignCashStatisticInfo as $currencyId => $currencyData) : ?>
            <?php $currencyName = $currencyData['name']; ?>
            <?php foreach ($currencyData['items'] as $itemType => $itemData) : ?>
                <?php if (count($itemData['items']) > 1) : ?>
                    <?php $cssClass = "cash-{$currencyName}-{$itemType}-substring" ?>
                    <tr class="toggle-string">
                        <td>
                            <div>
                                <span class="ht-empty-wrap flow-label link" data-target=".<?= $cssClass ?>">
                                    <?= $itemData['name']; ?>
                                    <?= $currencyName ?>
                                </span>
                            </div>
                        </td>
                        <td class="nowrap">
                            <span class="ht-empty-wrap">
                                <?= TextHelper::invoiceMoneyFormat($itemData['balance'], 2); ?>
                            </span>
                        </td>
                    </tr>
                    <?php foreach ($itemData['items'] as $accountId => $account) : ?>
                        <tr class="<?= $cssClass ?>" style="display:none">
                            <td style="padding-left: 5px;">
                                <div>
                                    <span class="ht-empty-wrap flow-label">
                                        <?= empty($account['name']) ? '---' : $account['name']; ?>
                                        <i class="ht-empty left"></i>
                                    </span>
                                </div>
                            </td>
                            <td class="nowrap">
                                <span class="ht-empty-wrap">
                                    <?= TextHelper::invoiceMoneyFormat($account['balance'], 2); ?>
                                    <i class="ht-empty right"></i>
                                </span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr class="single-string">
                        <td>
                            <div>
                                <span class="ht-empty-wrap flow-label">
                                    <?= $itemData['name']; ?>
                                    <?= $currencyName ?>
                                    <i class="ht-empty right"></i>
                                </span>
                            </div>
                        </td>
                        <td class="nowrap">
                            <span class="ht-empty-wrap">
                                <?= TextHelper::invoiceMoneyFormat($itemData['balance'], 2); ?>
                                <i class="ht-empty right"></i>
                            </span>
                        </td>
                    </tr>
                <?php endif ?>
            <?php endforeach; ?>
            <?php if (count($currencyData['items']) > 1) : ?>
                <tr class="summ">
                    <td>
                        <div>
                            <span class="ht-empty-wrap flow-label">
                                Итого <?= $currencyName ?>
                                <i class="ht-empty left">
                            </span>
                        </div>
                    </td>
                    <td class="nowrap">
                        <span class="ht-empty-wrap">
                            <?= TextHelper::invoiceMoneyFormat($currencyData['balance'], 2); ?>
                            <i class="ht-empty right"></i>
                        </span>
                    </td>
                </tr>
            <?php endif ?>
        <?php endforeach; ?>
    </table>
    <?php Pjax::end() ?>
</div>

<script>
    $(document).on("click", "#<?=($id)?> table.dashboard-cash .toggle-string .flow-label", function() {
        $("#<?=($id)?> table.dashboard-cash " + $(this).data("target") + "").slideToggle(150);
    });
    $(document).on("change", "input.debts-cash-config-item", function (e) {
        var input = this;
        var form = this.form;
        console.log($(form).serialize());
        $.post($(input).data('url'), $(form).serialize(), function(data) {
            $(input).prop('checked', data[$(input).data('attribute')] == 1).uniform('refresh');
            $.pjax.reload($(input).data('target'), {});
        });
    });
</script>