<?php

use common\components\helpers\ArrayHelper;
use common\models\cash;
use frontend\rbac\permissions;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var \yii\web\View $this */

$model = new \frontend\models\FundingFlowForm();

$fundingTypeArray = ArrayHelper::getAssoc(cash\CashFundingType::find()->all(), 'id');

$fundingArray = array_fill_keys(array_keys($fundingTypeArray), 0);

$companyId = Yii::$app->user->identity->company->id;
foreach ([
             cash\CashBankFlows::getFundingArray($companyId),
             cash\CashEmoneyFlows::getFundingArray($companyId),
             cash\CashOrderFlows::getFundingArray($companyId),
         ] as $flowFunding) {
    foreach ($fundingArray as $key => $funding) {
        if (isset($flowFunding[$key])) {
            $fundingArray[$key] += $flowFunding[$key];
        }
    }
}

$fundingAllowed = Yii::$app->getUser()->can(permissions\Service::HOME_FINANCES)
    && Yii::$app->user->can(permissions\Cash::STRICT_MODE, ['throw' => false,]);
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'cash-funding-pjax',
    'formSelector' => '.add-funding-flow-form',
    'enablePushState' => false,
    'enableReplaceState' => false,
]) ?>

    <div class="widget-funding widget-home portlet box darkblue ">
        <div class="portlet-title">
            <div class="caption">
                Финансирование
            </div>
            <div class="tools arrow-tools">
                <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
            </div>
        </div>
        <div class="portlet-body portlet-body-checkall">
            <table class="table table-striped table-bordered table-hover" style="width: 98%; margin: 0 auto 20px;">
                <thead class="not-fixed">
                <tr class="heading" role="row">
                    <th width="50%">Источник</th>
                    <th width="15%">Сумма</th>
                    <?php if ($fundingAllowed): ?>
                        <th width="8%" colspan="2">Действия</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($fundingArray as $key => $sum): ?>
                    <tr class="odd" role="row"
                        data-funding-type-id="<?= $key; ?>"
                        >
                        <td><?= $fundingTypeArray[$key]->name; ?></td>
                        <td><?= \common\components\TextHelper::invoiceMoneyFormat($sum, 2); ?></td>
                        <?php if ($fundingAllowed): ?>
                            <td class="text-center">
                                <a href="#money-popup-plus" class="funding-flow-button">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#money-popup-minus" class="funding-flow-button">
                                    <i class="fa fa-minus"></i>
                                </a>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>

                <tr class="even" role="row">
                    <td class="bold">Итого</td>
                    <td class="bold"><?= \common\components\TextHelper::invoiceMoneyFormat(array_sum($fundingArray), 2); ?></td>
                    <?php if ($fundingAllowed): ?>
                        <td></td>
                        <td></td>
                    <?php endif; ?>
                </tr>
                </tbody>
            </table>
        </div>

        <?php
        echo $this->render('_finances_form', [
            'modalId' => 'money-popup-minus',
            'modalTitle' => 'Уменьшить на',
            'model' => $model,
            'flowType' => cash\CashFlowsBase::FLOW_TYPE_EXPENSE,
        ]);
        echo $this->render('_finances_form', [
            'modalId' => 'money-popup-plus',
            'modalTitle' => 'Увеличить на',
            'model' => $model,
            'flowType' => cash\CashFlowsBase::FLOW_TYPE_INCOME,
        ]);
        ?>
    </div>

<?php \yii\widgets\Pjax::end(); ?>

<?php
$js = <<<JS
$(function () {
    $('#cash-funding-pjax').on('click', '.funding-flow-button', function (e) {
        e.preventDefault();
        var that = $(this);
        var row = that.parents('tr:first');
        var modal = $(that.attr('href'));

        modal.find('.funding-type-input').val(row.data('funding-type-id'));
        modal.modal('show');
    });

    $('#cash-funding-pjax').on('pjax:complete', function (e) {
        $('.modal').modal('hide');
        $('.add-funding-flow-form').yiiActiveForm('resetForm');
    });
});
JS;
$this->registerJs($js);
?>
