<?php
/**
 * Created by konstantin.
 * Date: 14.7.15
 * Time: 16.08
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var \yii\web\View $this */

/* @var string $modalId */
/* @var string $modalTitle */

/* @var \frontend\models\FundingFlowForm $model */
/* @var integer $flowType */

Modal::begin([
    'options' => [
        'id' => $modalId,
    ],
    'header' => Html::tag('h4', $modalTitle, ['class' => 'modal-title',]),
]);

$form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'add-funding-flow-' . (int) $flowType,
    'action' => ['add-funding-flow'],
    'method' => 'POST',

    'options' => [
        'class' => 'add-funding-flow-form',
        'data-pjax' => true,
    ],

    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-4 control-label',
        ],
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]));

echo Html::activeHiddenInput($model, 'flowType', [
    'value' => $flowType,
]);
echo Html::activeHiddenInput($model, 'fundingTypeId', [
    'class' => 'funding-type-input',
]);
echo $form->field($model, 'flow')->dropDownList($model::$flowArray, [
    'prompt' => '',
]);
echo $form->field($model, 'sum')->textInput([
    'class' => 'form-control js_input_to_money_format',
]);

?>
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton('ОК', [
                'class' => 'btn blue pull-right',
            ]); ?>
        </div>
    </div>
<?php

$form->end();

Modal::end();
