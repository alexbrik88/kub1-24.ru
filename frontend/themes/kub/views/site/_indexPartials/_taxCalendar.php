<?php

use frontend\models\NotificationSearch;
use frontend\themes\kub\components\Icon;
use php_rutils\RUtils;
use yii\grid\GridView;
use common\models\notification\Notification;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

$month = Yii::$app->request->get('month', date('m'));
$year = Yii::$app->request->get('year', date('Y'));

$notifications = (new NotificationSearch([
    'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
]))->searchIndex(Yii::$app->request->get(), $month, $year, \Yii::$app->user->identity->company, 8);

$monthInt = (int) $month;
$prevMonth = $monthInt > 1 ? $monthInt - 1 : 12;
$prevMonth = strlen($prevMonth) == 1 ? '0' . $prevMonth : $prevMonth;
$prevYear = $monthInt > 1 ? $year : $year - 1;

$nextMonth = $month < 12 ? $monthInt + 1 : 1;
$nextMonth = strlen($nextMonth) == 1 ? '0' . $nextMonth : $nextMonth;
$nextYear = $month < 12 ? $year : $year + 1;

$period = [
    'month' => $month,
    'year' => $year,
    'prevMonth' => $prevMonth,
    'prevYear' => $prevYear,
    'nextMonth' => $nextMonth,
    'nextYear' => $nextYear,
];

?>
<?php \yii\widgets\Pjax::begin([
    'options' => [
        'id' => 'tax-calendar-pjax',
    ],
    'enableReplaceState' => false,
    'enablePushState' => false,
]); ?>
<div class="wrap wrap-tax-calendar-p-r">
    <div class="ht-caption">
        НАЛОГОВЫЙ КАЛЕНДАРЬ
    </div>

    <?= GridView::widget([
            'dataProvider' => $notifications,
            'tableOptions' => [
                'class' => 'table table-style table-index-notifications',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],
            //'options' => [
            //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            //],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination nav-pagination-no-pages list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout_no_scroll_no_pages', ['totalCount' => $notifications->totalCount]),

            'showHeader' => false,

            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper breakall-td tax-calendar-p-r calend_hov_non',
            ],
            'emptyText' => 'Нет событий по налоговому календарю.',
            'columns' => [
                [
                    'label' => '',
                    'contentOptions' => [
                        'class' => 'tag',
                    ],
                    'format' => 'raw',
                    'value' => function (Notification $model) {
                        return $this->render('_taxCalendar_view', [
                            'model' => $model,
                        ]);
                    },
                ],
                [
                    'attribute' => 'text',
                    'format' => 'raw',
                    'value' => function (Notification $model) {
                        return Html::a($model->title, Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]), ['class' => 'link']) .
                        '<br/><span class="novelty-text">
                            <b>Кто: </b>' . $model->text .
                        '</span>';
                    },
                ],
            ],
        ]); ?>

    <div class="all-novelty-in-month">
        <div style="display: flex;">
            <?= Html::a(Icon::get('shevron', ['style' => 'transform: rotate(90deg);']), [
                'index',
                'month' => $period['prevMonth'],
                'year' => $period['prevYear']
            ], [
                'class' => 'tc-left link',
            ]); ?>
            <span class="pl-2 pr-2" style="flex: 1; text-align: center;">
                    <?= Html::a('Все события в ' . Notification::$monthHelper[(int)$month],
                    Url::to(['/notification/index', 'month' => $month, 'year' => $year]), [
                        'class' => 'link',
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]); ?>
                </span>
            <?= Html::a(Icon::get('shevron', ['style' => 'transform: rotate(270deg);']), [
                'index',
                'month' => $period['nextMonth'],
                'year' => $period['nextYear']
            ], [
                'class' => 'tc-right link',
            ]); ?>
        </div>
    </div>


</div>
<?php \yii\widgets\Pjax::end() ?>
