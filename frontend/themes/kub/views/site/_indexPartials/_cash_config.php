<?php

use frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $pjaxId */

$config = Yii::$app->user->identity->config;
$items = [
    [
        'label' => 'Не выводить<br>строки с нулем',
        'attribute' => 'debts_cash_config_hide_zero',
        'checked' => $config->debts_cash_config_hide_zero,
    ],
    [
        'label' => 'Выводить в <br>разрезе счетов',
        'attribute' => 'debts_cash_config_show_by_account',
        'checked' => $config->debts_cash_config_show_by_account,
    ],
];
?>

<form>
    <div class="dropdown dropdown-settings d-inline-block debts-cash-config-dropdown" style="position: relative;">
        <?= Html::button(Icon::get('cog'), [
            'class' => 'marketing-dropdown-btn-icon',
            'data-toggle' => 'dropdown',
        ]) ?>
        <ul id="debts-cash-config" class="debts-cash-config dropdown-popup dropdown-menu py-2 px-3" role="menu">
            <li class="pb-2" style="border-bottom: 1px solid #ddd;">
                <label class="bold nowrap m-0">Настройки</label>
            </li>
            <?php foreach ($items as $item): ?>
                <li class="d-flex flex-nowrap align-items-start pt-2">
                    <?= Html::activeCheckbox($config, $item['attribute'], [
                        //'uncheck' => 0,
                        'class' => 'debts-cash-config-item mr-1',
                        'id' => $item['attribute'],
                        'data-target' => '#'.$pjaxId,
                        'data-attribute' => $item['attribute'],
                        'data-url' => Url::to(['/site/config']),
                        'label' => Html::tag('div', $item['label']),
                        'labelOptions' => [
                            'class' => 'd-flex flex-nowrap align-items-start'
                        ],
                    ]); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</form>

<style>
    #debts-cash-config {
        left: auto !important;
        right: -5px !important;
        margin-top: 10px;
    }
    #debts-cash-config::after {
        left: auto !important;
        right: 10px !important;
    }
</style>