<?php
use common\models\Contractor;
use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\DebtReportSearch2;

$color1 = '#36c3b0';

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-debt').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = 54;
        var symbOneOffset = (data.dataLabel.text.textStr.match(/1/g)||[]).length;
        var left = chart.plotWidth - data.dataLabel.width + offset;

        //console.log(data.dataLabel, data.dataLabel.text.textStr);
        data.dataLabel.attr({
            x: left
        });
    });
}
JS;

$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX + 10,y:point.plotY + 15}; }
JS;

$htmlHeader = <<<HTML

    <table class="ht-in-table">
        <tr>
            <th class="red">Просрочка на</th>
            <th colspan="2"></th>
        </tr>
HTML;
$htmlData = <<<HTML
        <tr>
            <td><div class="ht-title">{period}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart green" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
HTML;

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

// OLD & SLOW
//
//$top5_debit = DebtsHelper::getTop(5);
//
//$categories = $debt = [];
//foreach ($top5_debit as $contractorID => $sum) {
//    $categories[] = Contractor::findOne($contractorID)->getShortName();
//    $debt[] = round(bcdiv($sum, 100), 2);
//}
//
//// tooltip debit
//$jsTopData = [];
//foreach ($top5_debit as $contractorId => $sum) {
//    $sum_0_10 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, false, $contractorId);
//    $sum_11_30 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, false, $contractorId);
//    $sum_31_60 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, false, $contractorId);
//    $sum_61_90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, false, $contractorId);
//    $sum_more_90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, false, $contractorId);
//    $max_sum = max($sum_0_10, $sum_11_30, $sum_31_60, $sum_61_90, $sum_more_90);
//
//    $jsTopData[] = [
//        ['period' => '0-10 дней', 'percent' => $max_sum ? round(100 * $sum_0_10 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_0_10)],
//        ['period' => '11-30 дней', 'percent' => $max_sum ? round(100 * $sum_11_30 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_11_30)],
//        ['period' => '31-60 дней', 'percent' => $max_sum ? round(100 * $sum_31_60 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_31_60)],
//        ['period' => '61-90 дней', 'percent' => $max_sum ? round(100 * $sum_61_90 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_61_90)],
//        ['period' => 'Больше 90 дней', 'percent' => $max_sum ? round(100 * $sum_more_90 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_more_90)],
//    ];
//}
//
//$jsTopData = json_encode($jsTopData);

$categories = ['Нет данных'];
$debt = [0];
$topData = [];

try {

    $model = new DebtReportSearch2();
    $model->search_by = DebtReportSearch2::REPORT_TYPE_MONEY;
    $model->report_type = DebtReportSearch2::SEARCH_BY_INVOICES;
    $model->findDebtor([], Contractor::TYPE_CUSTOMER);

    $topData = $model->getTopByContractors(5);

    if ($topData) {
        $categories = $debt = $contractorsIds = [];
        foreach ($topData as $data) {
            $categories[] = ($c = Contractor::findOne($data['id'])) ? $c->getNameWithType() : $data['name'];
            $debt[] = round($data['debt_all_sum'] / 100, 0);
        }
    }

} catch (Throwable $e) {}

// tooltip credit
$jsTopData = [];
foreach ($topData as $data) {

    $max_sum = max(1, $data['debt_0_10_sum'], $data['debt_11_30_sum'], $data['debt_31_60_sum'], $data['debt_61_90_sum'], $data['debt_more_90_sum']);

    $jsTopData[] = [
        ['period' => '0-10 дней', 'percent' => $max_sum ? round(100 * $data['debt_0_10_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_0_10_sum'], 0)],
        ['period' => '11-30 дней', 'percent' => $max_sum ? round(100 * $data['debt_11_30_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_11_30_sum'], 0)],
        ['period' => '31-60 дней', 'percent' => $max_sum ? round(100 * $data['debt_31_60_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_31_60_sum'], 0)],
        ['period' => '61-90 дней', 'percent' => $max_sum ? round(100 * $data['debt_61_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_61_90_sum'], 0)],
        ['period' => 'Больше 90 дней', 'percent' => $max_sum ? round(100 * $data['debt_more_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_more_90_sum'], 0)],
    ];
}

$jsTopData = json_encode($jsTopData);


$jsFormatterFunc = <<<JS
function (a) {

      if(1 == this.point.series.index)
          return false;

      var idx = this.point.index;
      var tooltipHtml;

      var arr = $jsTopData;

      tooltipHtml = '$htmlHeader';
      if (arr[idx] != undefined) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '$htmlData'
               .replace("{period}", data.period)
               .replace("{percent}", data.percent)
               .replace("{sum}", data.sum);
          });
      } else {
          tooltipHtml += '<tr><td colspan="3">Нет данных</td></tr>';
      }

      tooltipHtml += '$htmlFooter';

      return tooltipHtml.replace();
    }
JS;

?>
<div style="height: 150px!important;">
<?php
echo \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-debt',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
        'modules/pattern-fill'
    ],
    'options' => [
        'title' => [
            'text' => '',
            'align' => 'left',
            'floating' => false,
            'style' => [
                'font-size' => '15px',
                'color' => '#9198a0',
            ],
            'x' => 0,
        ],
        'credits' => [
            'enabled' => false
        ],
        'legend' => [
            'enabled' => false
        ],
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'inverted' => true,
            'height' => max(30 * count($categories), 30),
            'spacing' => [0,0,0,0],
            'marginRight' => 50,
            'events' => [
                'load' => new \yii\web\JsExpression($jsLoadFunc),
                'redraw' => new \yii\web\JsExpression($jsLoadFunc),
            ]
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                    'position' => 'left'
                ],
                'grouping' => false,
                'shadow' => false,
                'borderWidth' => 0
            ]
        ],
        'tooltip' => [
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'positioner' => new \yii\web\JsExpression($jsPositionerFunc),
            'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
            'useHTML' => true,
            'shape' => 'rect',
        ],
        'yAxis' => [
            'min' => 0,
            'index' => 0,
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'title' => '',
            'labels' => false,
        ],
        'xAxis' => [
            'categories' => $categories,
            'labels' => [
                //'useHTML' => true,
                'align' => 'left',
                'reserveSpace' => true,
                'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
            ],
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'lineWidth' => 0,
            'offset' => 0
        ],
        'series' => [
            [
                'name' => 'Сумма долга',
                'data' => $debt,
                'color' => $color1,
                'states' => [
                    'hover' => [
                        'color' => [
                            'pattern' => [
                                'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                'color' => $color1,
                                'width' => 5,
                                'height' => 5
                            ]
                        ]
                    ]
                ],
                'pointWidth' => 18,
                'borderRadius' => 3
            ],
        ]
    ],
]);
?>
</div>

<?php /*<a href="<?= \yii\helpers\Url::to(['/reports/debt-report/debtor']) ?>" class="link" style="font-size: 13px;padding:10px 0;">Посмотреть все</a>*/ ?>
