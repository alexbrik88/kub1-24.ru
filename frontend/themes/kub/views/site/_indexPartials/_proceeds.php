<?php

use frontend\components\StatisticPeriod;

?>

<div class="portlet box darkblue widget-coming widget-home">
    <div class="portlet-title">
        <div class="caption">
            Счета за
            <div class="statistic-period-btn" style="display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;">
                <?= mb_strtolower(StatisticPeriod::getSessionName()); ?>
            </div>
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall accounts-list" style="display: block;">
        <div class="table-container tabbable-line" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <tbody>
                <tr role="row" class="even">
                    <td>
                        <?= frontend\modules\documents\widgets\StatisticWidget::widget([
                            'type' => \frontend\models\Documents::IO_TYPE_OUT,
                            'outerClass' => 'col-md-4 col-sm-4',
                            'clickable' => true,
                        ]); ?>
                    </td>
                </tr>
                </tbody>
            </div>
        </div>
    </div>
</div>