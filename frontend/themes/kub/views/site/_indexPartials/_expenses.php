<?php
use common\assets\MorrisChartAsset;
use frontend\components\StatisticPeriod;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var array $inInvoiceStatistics */
/* @var array $invoiceTextVariants */

MorrisChartAsset::register($this);

$expenses_type = (int) Yii::$app->request->post('expenses_type', 1);

$this->registerJs('
$(document).on("change", "#expenses_type-select", function () {
    $.pjax.reload("#expenses-pjax", {data: {expenses_type: $(this).val()}, type: "post"});
});
');
?>

<?php Pjax::begin([
    'id' => 'expenses-pjax',
    'enableReplaceState' => false,
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<div class="portlet box darkblue widget-expense widget-home">
    <div class="portlet-title">
        <div class="caption">
            Расход за
            <div class="statistic-period-btn" style="display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;">
                <?= mb_strtolower(StatisticPeriod::getSessionName()); ?>
            </div>
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
        </div>
        <div class="actions p-t-5 p-b-0">
            <div class="btn-group widget-home-popup">
                <?= Html::dropDownList('expenses_type', $expenses_type, [1 => 'По оплатам', 2 => 'По счетам'], [
                    'id' => 'expenses_type-select',
                    'class' => 'form-control',
                    'style' => 'height: 28px; padding: 0 10px; width: 130px; margin: 2px;',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall">
        <?php if ($expenses_type == 1) : ?>
            <?= $this->render('_expenses_payment') ?>
        <?php elseif ($expenses_type == 2) : ?>
            <?= $this->render('_expenses_invoice', [
                'inInvoiceStatistics' => $inInvoiceStatistics,
                'invoiceTextVariants' => $invoiceTextVariants,
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<?php Pjax::end() ?>
