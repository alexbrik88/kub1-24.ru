<?php
use common\components\date\DateHelper;
use \common\components\helpers\ArrayHelper;

$currDate = ($currency) ? ArrayHelper::getValue($currency, '0.update_date') : null;
?>
<div class="wrap">
    <div class="ht-caption">
        КУРСЫ ВАЛЮТ <?php if ($currDate): ?><span style="text-transform: none!important;"> на </span> <?= DateHelper::format($currDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?><?php endif; ?>
    </div>
    <table class="ht-table">
        <?php

        foreach ($currency as $item) :
            if ($item->current_value > $item->old_value) {
                $cssClass = 'green';
                $iconClass = 'fa-caret-up';
            } elseif ($item->current_value < $item->old_value) {
                $cssClass = 'red';
                $iconClass = 'fa-caret-down';
            } else {
                $cssClass = '';
                $iconClass = 'fa-minus';
            }
            ?>
            <tr>
                <td>
                    <i class="fa <?= $iconClass . ' ' . $cssClass; ?>"></i>
                </td>
                <td>
                    <?= $item->name; ?> ЦБ
                </td>
                <td class="<?= $cssClass; ?>">
                    <?= round($item->current_value - $item->old_value, 4); ?>
                </td>
                <td class="bold">
                    <?= $item->current_value; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>