<?php

use php_rutils\RUtils;
use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use yii\helpers\ArrayHelper;

/* @var $model \frontend\modules\reports\models\PaymentCalendarSearch */

/////////////////////////////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';

$CENTER_DATE = date('Y-m-d');
$inTooltipMaxRows = 5;
/////////////////////////////////////////

$datePeriods = $model->getFromCurrentDaysPeriods(6, 6);
$prevYearDatePeriods = $model->getFromCurrentDaysPeriods(6, 6, -1);
$currDayPos = -1;
$daysPeriods = [];
$chartPlanFactLabelsX = [];
$chartFreeDays = [];
foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $day = $dateArr[2];
    $daysPeriods[] = $day;
    $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0,6]));
    $chartPlanFactLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $date['from'],
    ]);
    if ($CENTER_DATE == $date['from'])
        $currDayPos = $i;
}

$mainData = $model->getPlanFactSeriesData($datePeriods);
$prevMainData = $model->getPlanFactSeriesData($prevYearDatePeriods);
$incomeFlowsFactPlan = &$mainData['incomeFlowsFact'];
$outcomeFlowsFactPlan = &$mainData['outcomeFlowsFact'];
$incomeFlowsPlan = &$mainData['incomeFlowsPlan'];
$outcomeFlowsPlan = &$mainData['outcomeFlowsPlan'];
$balanceFact = &$mainData['balanceFact'];
$balanceFactPrevYear = &$prevMainData['balanceFact'];
$balancePlan = [];

// chart1
for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
    $incomeFlowsFactPlan[$i] = $incomeFlowsPlan[$i];
    $outcomeFlowsFactPlan[$i] = $outcomeFlowsPlan[$i];
    $incomeFlowsPlan[$i] = null;
    $outcomeFlowsPlan[$i] = null;
}

// chart2: connect plan/fact edges
for ($i = 0; $i < $currDayPos; $i++) {
    $balancePlan[$i] = null;
}
$balancePlan[$currDayPos] = $balanceFact[$currDayPos];
for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
    $balanceFact[$i] = null;
    $balancePlan[$i] = $balancePlan[$i - 1] + $incomeFlowsFactPlan[$i] - $outcomeFlowsFactPlan[$i];
}

//////////////// TOOLTIP ///////////////////////
$dateFrom = $datePeriods[0]['from'];
$dateTo = $datePeriods[count($datePeriods)-1]['to'];
$incomeTooltipData = $model->getTooltipStructureByContractors($dateFrom, $dateTo, null, CashFlowsBase::FLOW_TYPE_INCOME);
$outcomeTooltipData = $model->getTooltipStructureByContractors($dateFrom, $dateTo, null, CashFlowsBase::FLOW_TYPE_EXPENSE);

$arrIncome = [];
$arrOutcome = [];
$arrIncomeTotal = [];
$arrOutcomeTotal = [];
$arrDate = [];
foreach ($datePeriods as $pos => $date) {
    $amountKey = ($pos <= $currDayPos) ? 'amountFact' : 'amountPlan';
    $countKey = ($pos <= $currDayPos) ? 'countFact' : 'countPlan';

    $arrIncome[$pos] = null;
    $arrOutcome[$pos] = null;
    $arrIncomeTotal[$pos] = null;
    $arrOutcomeTotal[$pos] = null;
    $arrDate[$pos] = null;

    if ($income = ArrayHelper::getValue($incomeTooltipData, $date['from'])) {
        $maxAmount = max(array_column($income, $amountKey)) ?: 9E9;
        $row = 0;
        foreach ($income as $i) {
            if ($i[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
            $arrIncome[$pos][] = [
                'name' => $i['name'],
                'percent' => round($i[$amountKey] / $maxAmount * 100),
                'sum' => $i[$amountKey]
            ];
        }
        $total = array_sum(array_column($income, $countKey));
        $arrIncomeTotal[$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
    }

    if ($outcome = ArrayHelper::getValue($outcomeTooltipData, $date['from'])) {
        $maxAmount = max(array_column($outcome, $amountKey));
        $row = 0;
        foreach ($outcome as $o) {
            if ($o[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
            $arrOutcome[$pos][] = [
                'name' => $o['name'],
                'percent' => round($o[$amountKey] / $maxAmount * 100),
                'sum' => $o[$amountKey]
            ];
        }
        $total = array_sum(array_column($outcome, $countKey));
        $arrOutcomeTotal[$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
    }

    $dateArr = explode('-', $date['from']);
    $year = $dateArr[0];
    $month = $dateArr[1];
    $day = $dateArr[2];
    $arrDate[$pos] = (int)$day . ' ' . mb_strtolower(ArrayHelper::getValue(Month::$monthGenitiveRU, substr($date['from'], 5, 2)));
}
$jsArrIncome = json_encode($arrIncome);
$jsArrOutcome = json_encode($arrOutcome);
$jsArrIncomeTotal = json_encode($arrIncomeTotal);
$jsArrOutcomeTotal = json_encode($arrOutcomeTotal);
$jsArrDate = json_encode($arrDate);
//var_dump($arrIncome, $arrOutcome); exit;


$htmlHeaderFact = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
        <tr>
            <th>Факт</th>
            <th></th>
            <th>{fact.y} ₽</th>
        </tr>
HTML;
$htmlHeaderPlan = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
HTML;
$htmlData = <<<HTML
        <tr>
            <td><div class="ht-title">{name}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart {color_css}" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum} ₽</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
    <table class="ht-in-footer-table">
        <tr>
            <td><strong>Всего {total}</strong></td>
        </tr>
    </table>
HTML;

$htmlHeaderFact = str_replace(["\r", "\n", "'"], "", $htmlHeaderFact);
$htmlHeaderPlan = str_replace(["\r", "\n", "'"], "", $htmlHeaderPlan);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

$jsFormatterFunc = <<<JS

function (a) {

      var idx = this.point.index;
      var isIncome = (this.series.index == 0 || this.series.index == 2);
      var isPlanPeriod = (idx > {$currDayPos});

      if((this.series.index == 0 || this.series.index == 2) && this.series.chart.series[0].points[idx].y == 0)
          return false;
      if ((this.series.index == 1 || this.series.index == 3) && this.series.chart.series[1].points[idx].y == 0)
          return false;
      
      var arrIncome = {$jsArrIncome};
      var arrOutcome = {$jsArrOutcome};
      var arrIncomeTotal = {$jsArrIncomeTotal};
      var arrOutcomeTotal = {$jsArrOutcomeTotal};
      var arrDate = {$jsArrDate};
      
      var arr = (isIncome) ? arrIncome : arrOutcome;
      var arrTotals = (isIncome) ? arrIncomeTotal : arrOutcomeTotal;
      var chart = this.series.chart;
      var planY = (isIncome) ? chart.series[2].points[idx].y : chart.series[3].points[idx].y;
      var factY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
      
      if (isPlanPeriod) {
          factY = 0;
          planY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
      }
      
      var tooltipHtml = (isPlanPeriod) ? '{$htmlHeaderPlan}' : '{$htmlHeaderFact}';
      
      tooltipHtml = tooltipHtml
          .replace("{title}", isIncome ? 'Приход за' : 'Расход за')
          .replace("{point.x}", arrDate[idx])
          .replace("{plan.y}", Highcharts.numberFormat(planY, 0, ',', ' '))
          .replace("{fact.y}", Highcharts.numberFormat(factY, 0, ',', ' '))
      
      if (arr[idx]) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '{$htmlData}'
               .replace("{name}", data.name)
               .replace("{percent}", data.percent)
               .replace("{sum}", Highcharts.numberFormat(data.sum, 0, ',', ' '))
               .replace("{color_css}", (isIncome ? 'blue' : 'yellow') + (isPlanPeriod ? ' plan' : ''))              
          });
      } else {
          tooltipHtml += '<tr><td colspan="3"></td></tr>';
      }
      if (arrTotals[idx]) {
          tooltipHtml += '{$htmlFooter}'.replace("{total}", arrTotals[idx]);          
      }

      return tooltipHtml;
    }

JS;
////////////////////////////////////////////////

?>
<style>
    #chart-plan-fact { height: 200px; }
    #chart-plan-fact-2 { height: 200px; }
    #chart-plan-fact  .highcharts-axis-labels, #chart-plan-fact-2 .highcharts-axis-labels { z-index: -1!important; }
    #chart-plan-fact, #chart-plan-fact .highcharts-container, #chart-plan-fact .highcharts-container > svg { overflow: visible!important; }
    #chart-plan-fact .highcharts-container { z-index: 1!important; }
</style>

<div class="row">
    <div style="width: 100%; z-index: 1;">
        <div class="ht-caption" style="margin-left:7px">
            Приход-Расход
            <?php if (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [112, 486, 628, 11270, 23083, 53146])): ?>
                <div class="site-index-more-link" style="margin-right:7px;"><?= \yii\helpers\Html::a('Подробнее', ["/reports/finance/payment-calendar"], ['class' => 'link']) ?></div>
            <?php endif; ?>
        </div>
        <div style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('redrawPlanMonths()')
                        ],
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX + 69,y:point.plotY - 45}; }'),
                        'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        'min' => 1,
                        'max' => 11,
                        'categories' => $daysPeriods,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chartCurrDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                (window.chartFreeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                            'useHTML' => true,
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Приход Факт',
                            'data' => $incomeFlowsFactPlan,
                            'color' => $color1,
                            'borderColor' => 'rgba(46,159,191,.3)',
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Расход Факт',
                            'data' => $outcomeFlowsFactPlan,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Приход План',
                            'data' => $incomeFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(21,67,96,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => -0.225,
                            'stickyTracking' => false,
                        ],
                        [
                            'name' => 'Расход План',
                            'data' => $outcomeFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(50,50,50,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0.22,
                            'stickyTracking' => false,
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div class="ht-caption" style="margin-left:7px">
            Остаток денег
        </div>
        <div style="min-height:125px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-2',
                'scripts' => [
                   // 'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHtml' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                                function(args) {
                                    var index = this.series.data.indexOf( this.point );
                                    var series_index = this.series.index;
                                    var factColor = args.chart.series[2].data[index].y >= 0 ? 'green-text' : 'red-text';
                                    var factColorB = args.chart.series[2].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                                    var planColor = args.chart.series[1].data[index].y >= 0 ? 'green-text' : 'red-text';
                                    var planColorB = args.chart.series[1].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                                    
                                    var toggleFactLines = args.chart.series[0].data[index].y > args.chart.series[2].data[index].y; 
                                    var togglePlanLines = args.chart.series[0].data[index].y > args.chart.series[1].data[index].y;

                                    var titleLine = '<span class=\"title\">' + window.chartPlanFactLabelsX[this.point.index] + '</span>';
                                    var factLine = '<br/>' + '<span class=\"' + factColor + '\">' + args.chart.series[2].name + ': ' + '</span>' + '<b class=\"' + factColorB + '\">' + Highcharts.numberFormat(args.chart.series[2].data[index].y, 0, ',', ' ') + ' ₽</b>';
                                    var planLine = '<br/>' + '<span class=\"' + planColor + '\">' + args.chart.series[1].name + ': ' + '</span>' + '<b class=\"' + planColorB + '\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 0, ',', ' ') + ' ₽</b>';
                                    var prevYearLine = '<br/>' + '<span class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</span>' + '<b class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 0, ',', ' ') + ' ₽</b>';

                                    if (index > (window.chartCurrDayPos)) {
                                        return titleLine + (togglePlanLines ? (prevYearLine + planLine) : (planLine + prevYearLine));
                                    }
                                        
                                    return titleLine + (toggleFactLines ? (prevYearLine + factLine) : (factLine + prevYearLine));
                                }
                            ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        'min' => 1,
                        'max' => 11,
                        'categories' => $daysPeriods,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chartCurrDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                (window.chartFreeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                            'useHTML' => true,
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток Факт (предыдущий год)',
                            'data' => $balanceFactPrevYear,
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 2
                        ],
                        [
                            'name' => 'Остаток План',
                            'data' => $balancePlan,
                            'color' => 'rgba(26,184,93,1)',
                            'negativeColor' => 'red',
                            'fillColor' => [
                                'pattern' => [
                                    //'image' => '/img/pattern1.png',
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#27ae60',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'negativeFillColor' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#e74c3c',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'marker' => [
                                'symbol' => 'square'
                            ],
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 1
                        ],
                        [
                            'name' => 'Остаток Факт',
                            'data' => $balanceFact,
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    var chartCurrDayPos = <?= (int)$currDayPos; ?>;
    var chartPlanFactLabelsX = <?= json_encode($chartPlanFactLabelsX) ?>;
    var chartFreeDays = <?= json_encode($chartFreeDays) ?>;

    function redrawPlanMonths() {

        var custom_pattern = function (color) {
            return {
                pattern: {
                    path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                    width: 10,
                    height: 10,
                    color: color
                }
            }
        }

        var chartToLoad = window.setInterval(function () {
            var chart = $('#chart-plan-fact').highcharts();
            if (typeof(chart) !== 'undefined') {

                for (var i = (1+chartCurrDayPos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color1 ?>");
                    chart.series[1].points[i].color = custom_pattern("<?= $color2 ?>");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();

                window.clearInterval(chartToLoad);
            }

        }, 100);
    }

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });

    ChartPlanFactIndex = {

    };

</script>