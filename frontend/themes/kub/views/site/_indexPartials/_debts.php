<?php

use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\reports\models\PaymentCalendarSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\rbac\permissions;
use common\components\date\DateHelper;

/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.01.2017
 * Time: 8:22
 */

/**
 * @var $cashStatisticInfo array
 * @var $foraignCashStatisticInfo array
 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);

$dateRange = StatisticPeriod::getSessionPeriod();
$cashStatisticsDate = ($dateRange['to'] < date('Y-m-d')) ? $dateRange['to'] : date('Y-m-d');
?>

<div class="row in-row-wrap-p-20" style="margin: 0 -10px">

    <!-- left part -->
    <div class="col-8" style="padding:0 5px;">
        <div class="row">
            <?php if (Yii::$app->user->can(permissions\Service::HOME_PROCEEDS)): ?>
            <div class="col-12">
                <div class="wrap">
                    <?php // Chart
                    $pcSearchModel = new PaymentCalendarSearch();
                    echo $this->render('_chart_plan_fact', [
                        'model' => $pcSearchModel,
                    ]); ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if (Yii::$app->user->can(permissions\Service::HOME_CASH)): ?>
            <div class="col-6">
                <div class="wrap">
                    <div class="ht-caption">
                        НАМ ДОЛЖНЫ
                        <?php if (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [112, 486, 628, 11270, 23083, 53146])): ?>
                            <div class="site-index-more-link"><?= \yii\helpers\Html::a('Подробнее', ['/reports/debt-report/debtor'], ['class' => 'link']) ?></div>
                        <?php endif; ?>
                    </div>
                    <table class="ht-table">
                        <tr>
                            <td class="gray bold size15">Текущая задолженность</td>
                            <td class="bold nowrap size15"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL); ?></td>
                        </tr>
                        <tr>
                            <td class="red bold">Просрочка дней</td>
                            <td></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">0-10 дней<i class="ht-empty left"></i></span></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">11-30 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">31-60 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">61-90 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">Больше 90 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr>
                            <td class="red bold">Просроченая задолженность</td>
                            <td class="nowrap red bold"><?=DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_OVERDUE, false, true) ?></td>
                        </tr>
                        <tr>
                            <td class="gray bold size15">ИТОГО задолженность</td>
                            <td class="bold nowrap size15">
                                <?= TextHelper::invoiceMoneyFormat(
                                DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false) +
                                      DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL, false, false), 2) ?>
                            </td>
                        </tr>
                    </table>

                    <div class="devider mt-3 mb-3"></div>

                    <div class="ht-caption">
                        ТОП 5 Должников
                    </div>

                    <?= $this->render('_debts_chart_debit') ?>

                    <div class="devider mt-3 mb-3"></div>

                    <div class="ht-caption">
                        Динамика по дебиторке
                    </div>

                    <?= $this->render('_debts_chart_dynamic_debit') ?>

                </div>
            </div>
            <div class="col-6">
                <div class="wrap">
                    <div class="ht-caption">
                        МЫ ДОЛЖНЫ
                        <?php if (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [112, 486, 628, 11270, 23083, 53146])): ?>
                            <div class="site-index-more-link"><?= \yii\helpers\Html::a('Подробнее', ['/reports/debt-report-seller/debtor'], ['class' => 'link']) ?></div>
                        <?php endif; ?>
                    </div>
                    <table class="ht-table">
                        <tr>
                            <td class="gray bold size15">Текущая задолженность</td>
                            <td class="bold nowrap size15"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL, false, true, null, 1); ?></td>
                        </tr>
                        <tr>
                            <td class="red bold">Просрочка дней</td>
                            <td></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">0-10 дней<i class="ht-empty left"></i></span></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, null, 1); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">11-30 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, null, 1); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">31-60 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, null, 1); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">61-90 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, null, 1); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr class="bb">
                            <td><span class="ht-empty-wrap">Больше 90 дней<i class="ht-empty left"></i></td>
                            <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, null, 1); ?><i class="ht-empty right"></i></span></td>
                        </tr>
                        <tr>
                            <td class="red bold">Просроченая задолженность</td>
                            <td class="nowrap red bold"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_OVERDUE, false, true, null, 1) ?></td>
                        </tr>
                        <tr>
                            <td class="gray bold size15">ИТОГО задолженность</td>
                            <td class="bold nowrap size15">
                                <?= TextHelper::invoiceMoneyFormat(
                                    DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false, null, 1) +
                                    DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL, false, false, null, 1), 2) ?>
                            </td>
                        </tr>
                    </table>

                    <div class="devider mt-3 mb-3"></div>

                    <div class="ht-caption">
                        ТОП 5 Кредиторов
                    </div>

                    <?= $this->render('_debts_chart_credit') ?>

                    <div class="devider mt-3 mb-3"></div>

                    <div class="ht-caption">
                        Динамика по кредиторке
                    </div>

                    <?= $this->render('_debts_chart_dynamic_credit') ?>

                </div>
            </div>
            <?php endif; ?>
            <div class="col-12">
                <?php echo $this->render('_employee_rating') ?>
            </div>
        </div>
    </div>

    <!-- right part -->
    <div class="col-4" style="padding:0 5px;">
        <?php if (Yii::$app->user->can(permissions\Service::HOME_CASH)): ?>
            <?= $this->render('_cash', [
                'id' => 'chart_34',
                'cashStatisticInfo' => $cashStatisticInfo,
                'foraignCashStatisticInfo' => $foraignCashStatisticInfo,
            ]) ?>
        <?php endif; ?>

        <?php if (!empty($currency) && Yii::$app->user->can(permissions\Service::HOME_EXCHANGE_RATE)) {
            echo $this->render('_exchangeRate', ['currency' => $currency]);
        } ?>

        <?php echo $this->render('_taxCalendar'); ?>
    </div>
</div>
