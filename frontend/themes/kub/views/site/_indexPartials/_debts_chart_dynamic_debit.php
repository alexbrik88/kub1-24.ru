<?php

use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use frontend\modules\reports\models\PaymentCalendarSearch;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;

/* @var $pcModel \frontend\modules\reports\models\PaymentCalendarSearch
 */
$LEFT_DAYS = 7;
$CENTER_DATE = date('Y-m-d');

$pcModel = new PaymentCalendarSearch();
$datePeriods = $pcModel->getFromCurrentDaysPeriods($LEFT_DAYS, 0);
$prevYearDatePeriods = [];
$daysPeriods = [];
$chartDebtDebitLabelsX = [];
$daysMin = $LEFT_DAYS;
$mainData = [];
$chartDebtDebitFreeDays = [];
$currDayPos = -1;
foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $prevYearDatePeriods[] = $dateArr[0] -1 .'-'. $dateArr[1] .'-'. $dateArr[2];
    $day = $dateArr[2];
    $daysPeriods[] = $day;
    $chartDebtDebitFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0,6]));

    $chartDebtDebitLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
            'format' => 'F',
            'monthInflected' => true,
            'date' => $date['from'],
        ]);

    $query = DebtsHelper::getOverdueDebtsByPeriod(['min' => $daysMin]);
    $mainData[] = 1/100 * (float)(new Query)->from(['t' => $query])->sum('sum');
    $daysMin--;

    if ($CENTER_DATE == $date['from'])
        $currDayPos = $i;
}

$color1 = '#36c3b0';
?>

<style>
    #chart-debt-debit {
        height: 200px;
    }
    #chart-debt-debit  .highcharts-axis-labels {
        z-index: -1!important;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-debt-debit',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'line',
                        'events' => [],
                    ],
                    'legend' => [
                        'enabled' => false
                    ],
                    'exporting' => [
                        'enabled' => false
                    ],
                    'tooltip' => [
                        'useHtml' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'formatter' => new jsExpression("
                            function(args) {
                            
                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                        
                                return '<span class=\'title\'>' + window.chartDebtDebitLabelsX[this.point.index] + '</span>' +
                                       '<br/>' + '<span class=\'gray-text\'>' + args.chart.series[0].name + ': ' + '</span>' + '<b class=\'gray-text-b\'>' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 0, ',', ' ') + ' ₽</b>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'labels' => false,
                        'minorGridLineWidth' => 0,
                        'gridLineWidth' => 0
                    ],
                    'xAxis' => [
                        'categories' => $daysPeriods,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chartDebtDebitCurrDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                (window.chartDebtDebitFreeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                            'useHTML' => true,
                        ],
                        'minorGridLineWidth' => 0,
                        'gridLineWidth' => 0
                    ],
                    'series' => [
                        [
                            'name' => 'Просроченая задолженность',
                            'data' => $mainData,
                            'color' => $color1,
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ]
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    var chartDebtDebitCurrDayPos = <?= (int)$currDayPos; ?>;
    var chartDebtDebitLabelsX = <?= json_encode($chartDebtDebitLabelsX) ?>;
    var chartDebtDebitFreeDays = <?= json_encode($chartDebtDebitFreeDays) ?>;
</script>