<?php

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход';

?>

<?php if (Yii::$app->params['isKUBbank']) : ?>
    <?= $this->render('_kubbank', [
        'view' => '_login',
        'model' => $model,
        'typeItems' => [],
    ]) ?>
<?php else : ?>
    <?= \frontend\widgets\Alert::widget(); ?>

    <h4><?= Html::encode($this->title) ?></h4>

    <div class="row">
        <div class="col-5">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            ]); ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password', [
                'wrapperOptions' => [
                    'class' => 'form-filter password-input-wrapper',
                ],
                'parts' => [
                    '{input}' => Html::activePasswordInput($model, 'password', [
                        'class' => 'form-control'
                    ]) . Icon::get('eye', [
                        'class' => 'toggle-password-input-type to-visible',
                        'title' => 'Показать пароль',
                    ]) . Icon::get('eye-ban', [
                        'class' => 'toggle-password-input-type to-invisible',
                        'title' => 'Скрыть пароль',
                    ]),
                ],
            ]) ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                Если Вы забыли пароль, то Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
            </div>
            <div class="form-group">
                <?= Html::submitButton('Войти', [
                    'class' => 'button-regular button-regular_red width-120',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="form-group row text-bold">
        <div class="col-sm-12">
            Войти с помощью аккаунта
            <?= Html::a('МТС', ['/auth/login', 'authclient' => 'kub']) ?>
        </div>
    </div>
<?php endif ?>

