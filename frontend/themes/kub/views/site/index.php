<?php
/* @var $this yii\web\View */

use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use common\models\currency\Currency;
use common\models\employee\EmployeeRole;
use common\models\notification\Notification;
use common\models\retail\RetailPoint;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\components\Message;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\rbac\permissions;
use frontend\rbac\permissions\Service;
use frontend\rbac\UserRole;
use frontend\widgets\RangeButtonWidget;
use Intervention\Image\ImageManagerStatic as Image;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use common\components\TaxRobotHelper;
use common\models\company\CompanyType;

/* @var $inInvoiceStatistics Array */
/* @var $pieData Array */
/* @var $cashStatisticInfo Array */
/* @var $notifications common\models\notification\Notification[] */
/* @var $currency common\models\currency\Currency[] */
/* @var $activitiesSearchModel \frontend\models\log\LogSearch */
/* @var $activitiesDataProvider \yii\data\ActiveDataProvider */
/* @var $invoicesSearchModel \frontend\modules\documents\models\InvoiceSearch */

$this->title = 'Личный кабинет';
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order home-page';
$invoiceTextVariants = ['счёт', 'счёта', 'счетов',];

$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$company = Yii::$app->user->identity->company;

//
$ioType = Documents::IO_TYPE_OUT;
$invCount = $company->getInvoiceLeft($ioType);
$dateRange = StatisticPeriod::getSessionPeriod();
$invoiceStatusId = array_filter(is_array($invoicesSearchModel->invoice_status_id) ?
    $invoicesSearchModel->invoice_status_id :
    explode(',', $invoicesSearchModel->invoice_status_id));
$stat1 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID,
    $dateRange,
    $invoicesSearchModel->contractor_id,
    null,
    $invoiceStatusId,
    $invoicesSearchModel
);
$stat2 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID_IN_TIME,
    $dateRange,
    $invoicesSearchModel->contractor_id,
    null,
    $invoiceStatusId,
    $invoicesSearchModel
);
$stat3 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::PAID,
    $dateRange,
    $invoicesSearchModel->contractor_id,
    null,
    $invoiceStatusId,
    $invoicesSearchModel
);
$url1 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][$ioType]
    ),
]);
$url2 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID_IN_TIME][$ioType]
    ),
]);
$url3 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::PAID][$ioType]
    ),
]);

\frontend\themes\kub\helpers\KubHelper::setStatisticsFontSize(max($stat1['sum'], $stat2['sum'], $stat3['sum']) );
?>
<div class="stop-zone-for-fixed-elems site-index">

    <div class="wrap d-flex flex-wrap align-items-center" style="padding: 12px 24px">
        <h4>
            <?= $company->getTitle(true) ?>
            <?= Html::a(\frontend\themes\kub\helpers\Icon::get('cog'), Url::to(['/company/index']), [
                'id' => 'update-company_profile',
                'class' => 'company-cog-btn dropdown-btn-icon ml-1'
            ]); ?>
            <?php if (Yii::$app->user->can(permissions\Company::UPDATE) &&
                (!trim($company->name_short) || !trim($company->inn))
            ) : ?>
                <?= Html::a('Заполнить реквизиты вашей компании', ['/company/update'], [
                    'class' => 'link ml-1',
                    'style' => 'font-size: 1rem; font-weight: normal;',
                ]); ?>
            <?php endif ?>
        </h4>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Service::HOME_LOGO)): ?>
            <div class="ml-auto">
                <?php if (is_file($path = $company->getImage('logoImage'))) : ?>
                    <img src="<?= EasyThumbnailImage::thumbnailSrc($path, $logoWidth, $logoHeight, EasyThumbnailImage::THUMBNAIL_INSET, []) ?>"
                         style="max-height: 90px"/>
                <?php endif ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count" style="margin-top:20px;">
        <div class="row">
            <div class="col-6 col-xl-3">
                <a href="<?= $url1 ?>" style="text-decoration: none;" class="count-card wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено ВСЕГО</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat1['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3">
                <a href="<?= $url2 ?>" style="text-decoration: none;" class="count-card count-card_red wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено в срок</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat2['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3">
                <a href="<?= $url3 ?>" style="text-decoration: none;" class="count-card count-card_green wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Оплачено</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat3['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                    <a href="javascript:;"
                       onclick="window.location.href='<?= Url::toRoute(['documents/invoice/create', 'type' => 2]); ?>'"
                       class="button-regular w-100 button-hover-content-red">
                        <?= \frontend\themes\kub\helpers\Icon::get('add-icon') ?><span class="ml-1">Счет</span>
                    </a>
                <?php else : ?>
                    <a href="javascript:;" class="button-regular w-100 button-hover-content-red action-is-limited">
                        <?= \frontend\themes\kub\helpers\Icon::get('add-icon') ?><span class="ml-1">Счет</span>
                    </a>
                <?php endif ?>

            </div>
        </div>
    </div>

    <?php
        echo $this->render('_indexPartials/_debts', [
            'cashStatisticInfo' => $cashStatisticInfo,
            'foraignCashStatisticInfo' => $foraignCashStatisticInfo,
            'currency' => !empty($currency) ? $currency : null,
            'notifications' => $notifications
        ]); ?>

    <?php if (Yii::$app->user->can(permissions\Service::HOME_ACTIVITIES)) {
        echo $this->render('_indexPartials/_activities', ['activitiesSearchModel' => $activitiesSearchModel,
            'activitiesDataProvider' => $activitiesDataProvider,]);
    } ?>

</div>
<?php
$periodRanges = StatisticPeriod::getRangesJs();
$periodStart = StatisticPeriod::getStartEndDateJs('from');
$periodEnd = StatisticPeriod::getStartEndDateJs('to');

$this->registerJs(<<<JS
$('.details-sum').each(function() {
    $(this).numerator({
        easing: 'linear', // easing options.
        duration: 3000, // the length of the animation.
        delimiter: ' ',
        rounding: 2, // decimal places.
        toValue: $(this).attr('data-value')/100
    });
});

$(document).on("change", ".period-step-select", function () {
    $.pjax.reload("#period-pjax-container", {data: {
        "step": $(this).val(),
        "expenses_type": $("#expenses_type-select").val(),
    }, type: "post"});
});

$(document).on('pjax:success', '#period-pjax-container', function() {
});

$(document).on('hidden.bs.modal', '#retail-point-modal', function () {
    $.pjax.reload({container: '#retail-point-pjax'});
});
JS
    , $this::POS_READY
);
