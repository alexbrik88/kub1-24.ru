<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2017
 * Time: 19:52
 */

use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\notification\Notification;
use php_rutils\RUtils;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model Notification
 *
 * @var $user Employee
 */

$user = Yii::$app->user->identity;
$isPassed = ArrayHelper::getValue($model, 'companyNotification.status') == Notification::STATUS_PASSED;
$canSwitch = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_ACCOUNTANT]);
?>
<div class="notification-item mb-3 pb-3">
    <div class="link">
        <?= RUtils::dt()->ruStrFTime([
            'date' => $model->event_date,
            'format' => 'd F Y',
            'monthInflected' => true,
        ]); ?>
    </div>

    <div class="notification-item-header">
        <div class="notification-item-title">
            <h5 class="title">
                <?= $model->title; ?>
            </h5>
        </div>
        <?php if ($canSwitch): ?>
            <div class="notification-item-switch">
                <div class="btn-group btn-toggle switch pull-right">
                    <?= Html::button('Сделано', [
                        'class' => 'btn button-regular button-hover-transparent width-100 passed ' . ($isPassed ? 'active' : ''),
                        'data-url' => Url::to([
                            'change-status',
                            'id' => $model->id,
                        ]),
                        'status' => Notification::STATUS_PASSED,
                    ]); ?>
                    <?= Html::button('Не сделано', [
                        'class' => 'btn button-regular button-hover-transparent width-100 not-passed ' . ($isPassed ? '' : 'active'),
                        'data-url' => Url::to([
                            'change-status',
                            'id' => $model->id,
                        ]),
                        'status' => Notification::STATUS_NOT_PASSED,
                    ]); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div>
        <b><i>Срок:</i></b> до <?= RUtils::dt()->ruStrFTime([
            'date' => $model->event_date,
            'format' => 'd F Y',
            'monthInflected' => true,
        ]); ?>
    </div>
    <div>
        <b><i>Кто:</i></b> <?= $model->text; ?>
    </div>
    <div>
        <b><i>Штраф:</i></b> <?= $model->fine; ?>
    </div>
</div>
