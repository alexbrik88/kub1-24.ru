<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2017
 * Time: 18:58
 */

use common\models\notification\Notification;
use frontend\themes\kub\components\Icon;
use php_rutils\RUtils;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View
 * @var $period []
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Налоговый календарь';
?>
<div class="notification-index">
    <div class="notification-item-header ">
        <div class="notification-item-title">
            <h4 class="page-title"><?= $this->title; ?></h4>
        </div>

        <div class="notification-item-switch" style="width: 260px;">
            <h4 style="display: flex;">
                <?= Html::a(Icon::get('shevron', ['style' => 'transform: rotate(90deg);']), [
                    'index',
                    'month' => $period['prevMonth'],
                    'year' => $period['prevYear']
                ], [
                    'class' => 'pl-2 pr-2',
                ]); ?>
                <span class="pl-2 pr-2" style="flex: 1; text-align: center;">
                    <?= RUtils::dt()->ruStrFTime([
                        'date' => '01-' . $period['month'] . '-' . $period['year'],
                        'format' => 'F Y',
                    ]); ?>
                </span>
                <?= Html::a(Icon::get('shevron', ['style' => 'transform: rotate(270deg);']), [
                    'index',
                    'month' => $period['nextMonth'],
                    'year' => $period['nextYear']
                ], [
                    'class' => 'pl-2 pr-2',
                ]); ?>
            </h4>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title row-fluid">
        </div>
        <div class="portlet-body accounts-list">
            <?= ListView::widget([
                'options' => [
                    'class' => 'list-view',
                ],
                'layout' => "{items}\n{pager}",
                'dataProvider' => $dataProvider,
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'emptyText' => 'В ' . Notification::$monthHelper[(int) $period['month']] . ' нет событий по налоговому календарю.',
                'itemView' => function (Notification $model) {
                    return $this->render('view', [
                        'model' => $model,
                    ]);
                },
            ]); ?>
        </div>
    </div>
</div>
