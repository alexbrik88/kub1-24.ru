<?php

use common\components\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\document\UserEmail $userEmail */

?>

<div class="who-send-container">
    <div class="mb-2">
        <div class="checkbox">
            <?= Html::checkbox('InvoiceSendForm[sendToUser][' . $userEmail->id . ']', false, [
                'id' => 'invoicesendform-sendtouser',
                'data-value' => $userEmail->email,
                'label' => $userEmail->fio ? '<span class="email-label">' . $userEmail->fio .
                    '<br><span style="padding-left: 25px">' . $userEmail->email . '</span></span>' :
                    '<span class="email-label">' . $userEmail->email . '</span>',
            ]); ?>
        </div>
        <div class="update-user-email-block" style="display: none">
            <?= Html::textInput('fio', $userEmail->fio, [
                'class' => 'form-control',
                'id' => 'user-fio',
                'placeholder' => 'Фамилия И.О.',
                'style' => 'display: inline-block'
            ]); ?>
            <?= Html::textInput('email', $userEmail->email, [
                'class' => 'form-control',
                'id' => 'user-email',
                'placeholder' => 'E-mail',
                'style' => 'display: inline-block'
            ]); ?>
        </div>
        <div class="email-actions">
            <span class="update-user-email link">
                <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>
            </span>

            <span class="delete-user-email link user-email-<?= $userEmail->id; ?>"
                  data-url="<?= Url::to(['/email/delete-user-email', 'id' => $userEmail->id]); ?>"
                  data-id="<?= $userEmail->id; ?>" data-toggle="modal"
                  data-target="#delete-confirm-user-email">
                  <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>
            </span>

            <span class="save-user-email link" style="display: none;"
                  data-url="<?= Url::to(['/email/update-user-email', 'id' => $userEmail->id]); ?>">
                  <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#check"></use></svg>
            </span>
            <span class="undo-user-email link" style="display: none;">
                <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close"></use></svg>
            </span>
        </div>
    </div>
</div>