<?php

use common\models\rent\RentAgreement;
use frontend\models\RentEntity;
use frontend\assets\RentAsset;
use frontend\models\RentSearch;
use common\components\grid\GridView;
use frontend\themes\kub\assets\KubAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var ActiveDataProvider $provider
 * @var RentSearch         $searchModel
 * @var string[]           $categoryList
 * @var RentAgreement         $entityRent
 * @var string[]           $entities
 * @var string[]           $contractors
 * @var array[]            $quarterRanges
 * @var array[]            $monthRanges
 */

$this->title = 'Объекты аренды';

RentAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];

Url::remember('lastPage');

$isMonthMode = $searchModel->calendarMode == RentSearch::CALENDAR_MODE_MONTH;
$dateFrom = strtotime($searchModel->calendarRange[0]);
$dateTo = strtotime($searchModel->calendarRange[1]);
$columns = [];
if ($isMonthMode) {
    $monthLabels = [
        1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель', 5 => 'Май', 6 => 'Июнь',
        7 => 'Июль', 8 => 'Август', 9 => 'Сентябрь', 10  => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь'
    ];

    for ($date = new DateTime($searchModel->calendarRange[0]); $date->getTimestamp() <= $dateTo;) {
        list ($month, $year) = explode('-', $date->format('n-Y'));
        $quarter = $isMonthMode ? (int)ceil($month / 3) : 0;

        $columns[] = [
            'label' => $monthLabels[$month],
            'headerOptions' => ['data-quarter' => $quarter, 'data-year' => $year],
            'contentOptions' => function(RentEntity $entity) use ($year, $quarter, $month, $date) {
                $date = $date->getTimestamp();
                $options = ['colspan' => 1, 'data-quarter' => $quarter, 'data-year' => $year, 'data-rents' => []];

                foreach ($entity->rents as $rent) {
                    $rentFrom = strtotime(substr($rent->date_begin, 0, 7) . '-01');
                    $rentTo = strtotime(substr($rent->date_end, 0, 7) . '-01');

                    if ($rentFrom <= $date && $rentTo >= $date) {
                        $options['class'] = 'rented';
                        $options['data-rents'][$rent->id] = [
                            'title' => $rent->contractor->getShortName(true),
                            'url' => Url::to(['/rent-agreement/view', 'id' => $rent->id]),
                            'begin' => $rent->date_begin,
                            'end' => $rent->date_end,
                            'tooltip' => [
                                'title' => $rent->contractor->getShortName(true),
                                'agreement' => 'Договор №: ' . $rent->document_number . ' от ' . $rent->document_date,
                                'period' => 'Период: с ' . $rent->date_begin . ' по ' . $rent->date_end
                            ]
                        ];
                    }
                }

                $options['data-rents'] = Json::encode($options['data-rents']);

                return $options;
            },
            'value' => function() { return null; }
        ];

        $date = (clone $date)->modify('+1 month');
    }
} else {
    for ($date = new DateTime($searchModel->calendarRange[0]); $date->getTimestamp() <= $dateTo;) {
        $columns[] = [
            'label' => $date->format('d.m.Y'),
            'headerOptions' => ['data-date' => $date->getTimestamp()],
            'contentOptions' => function (RentEntity $entity) use ($date) {
                $date = $date->getTimestamp();
                $options = ['colspan' => 1, 'data-date' => $date, 'data-rents' => [], 'class' => []];

                foreach ($entity->rents as $rent) {
                    $rentFrom = strtotime($rent->date_begin);
                    $rentTo = strtotime($rent->date_end);

                    if ($rentFrom <= $date && $rentTo >= $date) {
                        $options['class'][] = 'rented';
                        $options['data-rents'][$rent->id] = [
                            'title' => $rent->contractor->getNameWithType(),
                            'url' => Url::to(['rent-view', 'id' => $rent->id]),
                            'begin' => $rent->date_begin,
                            'end' => $rent->date_end
                        ];
                    }
                }

                $options['data-rents'] = Json::encode($options['data-rents']);
                $options['class'] = implode(' ', $options['class']);

                return $options;
            },
            'value' => function (RentEntity $entity) { return null; },
            'format' => 'raw'
        ];

        $date = (clone $date)->modify('+1 day');
    }
}
?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => [
            'tooltipster-kub'
        ],
        'trigger' => 'hover',
    ],
]); ?>

<div class="table-settings row row_indents_s">
    <div class="col-10">
        <div class="row align-items-center">
            <div class="sub-title column">Список объектов: <strong><?= $provider->totalCount; ?></strong></div>
        </div>
    </div>

    <div class="col-2">
        <div class="d-flex flex-nowrap">
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($searchModel->calendarMode == 'day') ? 'button-regular_red' : '' ?> pl-4 pr-4 mb-0" href="<?= Url::to(['calendar', 'mode' => 'day']) ?>">День</a>
            </div>
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($searchModel->calendarMode == 'month') ? 'button-regular_red' : '' ?> pl-3 pr-3 mb-0" href="<?= Url::to(['calendar', 'mode' => 'month']) ?>">Месяц</a>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $provider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => ['class' => 'table table-style table-count-list rent-calendar', 'data-mode' => $searchModel->calendarMode],
    'pager' => ['options' => ['class' => 'nav-pagination list-clr']],
    'layout' => $this->render('//layouts/grid/layout_responsive', ['totalCount' => $provider->totalCount]),
    'columns' => array_merge([
        [
            'attribute' => 'title',
            'label' => 'Объект',
            'value' => function(RentEntity $entity) {
                return Html::a($entity->title, ['entity-view', 'id' => $entity->id]);
            },
            'format' => 'raw',
        ]
    ], $columns),
]); ?>

<script>rentIndexPrepare();</script>