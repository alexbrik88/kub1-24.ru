<?php

use common\components\TextHelper;
use common\models\rent\RentAgreement;
use frontend\assets\RentAsset;
use frontend\models\RentEntity;
use yii\bootstrap\Tabs;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use common\models\rent\Category;
use yii\data\ActiveDataProvider;
use frontend\models\RentSearch;
use frontend\models\RentInvoiceSearch;
use yii\bootstrap4\Modal;
use frontend\components\Icon;
use yii\helpers\Html;

/**
 * @var RentEntity         $entity
 * @var array              $attributes
 * @var array              $attributeValue
 * @var RentAgreement         $entityRent
 * @var string[]           $contractors
 * @var string[]           $employees
 * @var string[]           $categoryList
 * @var RentSearch         $rentSearch
 * @var ActiveDataProvider $rentProvider
 * @var RentInvoiceSearch  $invoiceSearch
 * @var ActiveDataProvider $invoiceProvider
 * @var string             $tabActive
 */


RentAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];
$this->title = "Аренда / Объект аренды {$entity->title}";
$lastPage = Url::to(['/rent/entity-list']);

$isTab = true;
$userConfig = Yii::$app->user->identity->config;
$userConfig->rent_categories = 0;
?>

<a class="link mb-2" href="<?= $lastPage ?>">Назад к списку</a>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2"><?= $entity->title ?></h4>
                        <div class="column">
                            <a class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                               title="Последние действия"
                               data-toggle="modal" href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                </svg>
                            </a>
                            <a href="<?= Url::to(['entity-form', 'id' => $entity->id]) ?>" class="button-regular button-regular_red button-clr w-44 mb-2 ml-1" title="Изменить">
                                <svg class="svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="column mb-4 pb-2 col-sm-3">
                            <div class="label weight-700 mb-3">Категория</div>
                            <div><?= $entity->category->title ?></div>
                        </div>
                        <?php if ($entity->category_id == Category::ID_REALTY) { ?>
                            <div class="column mb-4 pb-2 col-sm-3">
                                <div class="label weight-700 mb-3">Тип</div>
                                <div><?= $entity->attributeValue('type') ?></div>
                            </div>

                            <div class="column mb-4 pb-2 col-sm-3">
                                <div class="label weight-700 mb-3">Площадь (м2)</div>
                                <div><?= $entity->attributeValue('area') ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($entity->category_id == Category::ID_TECH || $entity->category_id == Category::ID_WAGON_HOUSE) { ?>
                            <div class="column mb-4 pb-2 col-sm-3">
                                <div class="label weight-700 mb-3">Гос номер</div>
                                <div><?= $entity->attributeValue('stateNumber') ?></div>
                            </div>

                            <div class="column mb-4 pb-2 col-sm-3">
                                <div class="label weight-700 mb-3">Срок действия ТО</div>
                                <div><?= $entity->attributeValue('validityPeriod') ?></div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="column mb-4 pb-2 col-sm-3">
                            <div class="label weight-700 mb-3">Цена,
                                <?= $entity->attributeValue('unit') ?>/<?= $entity->attributeValue('paymentPeriod') ?>
                            </div>
                            <div>
                                <?php if ($price = (float)$entity->attributeValue('price')): ?>
                                <?= TextHelper::invoiceMoneyFormat($price * 100, 2) ?> <?= $entity->attributeValue('currency') ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2 col-sm-3">
                            <div class="label weight-700 mb-3">Тип собственности</div>
                            <div><?= $entity->ownershipLabel ?></div>
                        </div>
                        <?php if ((Category::ID_REALTY == $entity->category_id) && $entity->attributeValue('lessor')) { ?>
                            <div class="column mb-4 pb-2 col-sm-3">
                                <div class="label weight-700 mb-3">Арендодатель</div>
                                <div><?= $entity->attributeValue('lessor') ?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-3 column pl-0">
                <div class="pb-2 mb-1">
                    <a href="<?= Url::to(['/rent-agreement/create', 'entity_id' => $entity->id]) ?>" class="button-regular button-regular_padding_medium button-regular_red w-100">
                        <svg class="svg-icon mr-2">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span class="ml-1">Договор аренды</span>
                    </a>
                </div>
                <div class="pb-2 mb-1">
                    <span class="button-regular button-regular_padding_medium w-100"><?= $entity->statusLabel ?></span>
                </div>
                <?php if ($additionalStatus = $entity->getAdditionalStatus()) : ?>
                    <div class="pb-2 mb-1">
                        <?= Html::tag('span', $entity->additionalStatusLabel, [
                            'class' => 'button-regular button-regular_padding_medium w-100',
                            'style' => $entity->additionalStatusStyle,
                        ]) ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<div class="nav-tabs-row">
    <?php
    /** @noinspection PhpUnhandledExceptionInspection */
    echo Tabs::widget([
        'options' => [
            'class' => 'nav nav-tabs w-100 mb-3 mr-3',
        ],
        'linkOptions' => [
            'class' => 'nav-link',
        ],
        'tabContentOptions' => [
            'class' => 'tab-pane',
            'style' => 'width:100%'
        ],
        'headerOptions' => [
            'class' => 'nav-item',
        ],
        'items' => [
            [
                'label' => 'Договоры аренды',
                'content' => $this->render('entity-view/_tab_rent', [
                    'categoryList' => $categoryList,
                    'contractors' => $contractors,
                    'employees' => $employees,
                    'rentSearch' => $rentSearch,
                    'rentProvider' => $rentProvider,
                    'isTab' => $isTab,
                    'userConfig' => $userConfig,
                ]),
                'linkOptions' => [
                    'class' => ($tabActive == 'rent') ? 'nav-link active' : 'nav-link'
                ],
                'active' => $tabActive == 'rent'
            ],
            [
                'label' => 'Счета',
                'content' => $this->render('entity-view/_tab_invoices', [
                    'contractors' => $contractors,
                    'employees' => $employees,
                    'searchModel' => $invoiceSearch,
                    'provider' => $invoiceProvider,
                ]),
                'linkOptions' => [
                    'class' => ($tabActive == 'invoices') ? 'nav-link active' : 'nav-link'
                ],
                'active' => $tabActive == 'invoices'
            ],
            [
                'label' => 'Информация',
                'content' => $this->render('entity-view/_tab_info', [
                    'entity' => $entity,
                    'attributes' => $attributes['info'],
                    'attributeValue' => $attributeValue
                ]),
                'linkOptions' => [
                    'class' => 'nav-link'
                ],
            ],
            [
                'label' => 'Цена',
                'content' => $this->render('entity-view/_tab_price', [
                    'attributes' => $attributes['price'],
                    'attributeValue' => $attributeValue
                ]),
                'linkOptions' => [
                    'class' => 'nav-link'
                ],
            ],
            [
                'label' => 'Изображения',
                'content' => $this->render('entity-view/_tab_image', ['entity' => $entity]),
                'linkOptions' => [
                    'class' => 'nav-link'
                ],
            ],
            [
                'label' => 'Описание',
                'content' => $this->render('entity-view/_tab_description', ['entity' => $entity]),
                'linkOptions' => [
                    'class' => 'nav-link'
                ],
            ],
        ],
    ]);
    ?>
</div>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>
<div class="created-by">
    <?= date('d.m.Y', $entity->created_at) ?>
    Создал
    <?= implode(' ', array_filter([$entity->employee->lastname, $entity->employee->firstname, $entity->employee->patronymic])); ?>
</div>
<?php Modal::end(); ?>

<?php if ($isTab): ?>

    <div id="many-delete" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные объекты (<span class="total-count">0</span>)?</h4>
                <?php ActiveForm::begin(['action' => ['rent-delete']]); ?>
                <?= Html::hiddenInput('id', null, ['class' => 'selected-ids']) ?>
                <div class="text-center">
                    <?= \yii\bootstrap4\Html::submitButton('Да', [
                        'id' => 'confirm-many-delete',
                        'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-action' => Url::to(['entity-delete']),
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <script>rentRentListPrepare();</script>

<?php endif; ?>