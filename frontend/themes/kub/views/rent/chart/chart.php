<?php

use frontend\themes\kub\components\Icon;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\models\RentChart;
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\ChartHelper;

/** @var \frontend\models\RentChart $model */

///////////////// dynamic vars ///////////
$customPeriod = $customPeriod ?? "months";
$customOffset = $customOffset ?? 1;
$customObjectType = $customObjectType ?? null;
/////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 18;
$RIGHT_DATE_OFFSET = 0;
$MOVE_OFFSET = 6;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

$currDayPos = null;
if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = ChartHelper::getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

elseif ($customPeriod == "days") {

    $datePeriods = ChartHelper::getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $monthPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($monthPrev != $month && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                'next' => Month::$monthFullRU[$month]
            ];
            // emulate align right
            switch ($month) {
                case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
            }
        }

        $monthPrev = $month;

        $daysPeriods[] = $day;
        $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
        $chartLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);
        if ($CENTER_DATE == $date['from'])
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');
}

$data = $model->getSingleChartData($datePeriods, $customPeriod, $customObjectType);
$dataLeased = &$data['leased'];
$dataReturn = &$data['return'];
$dataTotalCount = &$data['total_count'];
$dataRentCount = &$data['rent_count'];

if (Yii::$app->request->post('chart-rent-ajax')) {

    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataLeased,
                ],
                [
                    'data' => $dataReturn,
                ]
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataTotalCount,
                ],
                [
                    'data' => $dataRentCount,
                ],
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-rent, #chart-rent-2 { height: 235px; }
</style>

<div style="position: relative; margin-right:20px;">
    <div style="width: 100%;">

        <div class="chart-rent-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:286px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-rent-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:286px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-rent-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-rent-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">

            <div class="d-flex">
            <div class="mr-2">
                <ul class="nav nav-tabs" role="tablist" style="border-bottom: none; float: right; margin-top:11px;">
                    <li class="nav-item pr-2">
                        <a class="chart-rent-period nav-link <?= $customPeriod == 'days' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="days" data-toggle="tab">День</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="chart-rent-period nav-link <?= $customPeriod == 'months' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="months" data-toggle="tab">Месяц</a>
                    </li>
                </ul>
            </div>

            <div class="select2-wrapper">
            <?= \kartik\select2\Select2::widget([
                'id' => 'chart-rent-select2',
                'name' => 'selectObjectTypes',
                'data' => RentChart::getSelect2Data(),
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '170px'
                ]
            ]); ?>
            </div>
            </div>

        </div>


        <div class="clearfix"></div>

        <?= $this->render('_chart_body', [
            'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
            'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
            'daysPeriods' => $daysPeriods,
            'dataLeased' => $dataLeased,
            'dataReturn' => $dataReturn,
            'dataTotalCount' => $dataTotalCount,
            'dataRentCount' => $dataRentCount,
        ]) ?>

    </div>
</div>

<script>
    // MOVE CHART
    RentChart = {
        chart: 'main',
        offset: {
            days: <?= (int)$customOffset ?>,
            months: <?= (int)$customOffset ?>
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        byObjectType: "<?= $customObjectType ?>",
        period: '<?= $customPeriod ?>',
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            // move chart
            $(document).on('click', '.chart-rent-arrow', function() {
                // prevent double-click
                if (RentChart._inProcess) {
                    return false;
                }
                if ($(this).data('move') === 'left') {
                    window.RentChart.offset[RentChart.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.RentChart.offset[RentChart.period] += <?= $MOVE_OFFSET ?>;
                }
                RentChart.redrawByClick();
            });
            // change period
            $('.chart-rent-period').on('show.bs.tab', function (e) {
                RentChart.period = $(this).data('period');
                RentChart.redrawByClick();
            });
            // select object type
            $(document).on('change', '#chart-rent-select2', function() {
                RentChart.byObjectType = $(this).val();
                //RentChart.setTitle($(this).find('option:selected').data('chart-title'));
                RentChart.redrawByClick();
            });
        },
        //setTitle: function(title) {
        //    $('.object-type-title').html(title);
        //},
        redrawByClick: function() {

            return RentChart._getData().done(function() {

                $('#chart-rent').highcharts().update(RentChart.chartPoints);
                $('#chart-rent-2').highcharts().update(RentChart.chartPoints2);
                RentChart._inProcess = false;
            });
        },
        _getData: function() {
            RentChart._inProcess = true;
            return $.post('/rent/ajax-chart-data', {
                    "chart-rent-ajax": true,
                    "chart": RentChart.chart,
                    "period": RentChart.period,
                    "offset": window.RentChart.offset[RentChart.period],
                    "object-type": RentChart.byObjectType,
                },
                function(data) {
                    data = JSON.parse(data);
                    RentChart.freeDays = data.freeDays;
                    RentChart.currDayPos = data.currDayPos;
                    RentChart.labelsX = data.labelsX;
                    RentChart.wrapPointPos = data.wrapPointPos;
                    RentChart.chartPoints = data.optionsChart;
                    RentChart.chartPoints2 = data.optionsChart2;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-rent').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (RentChart.period === 'months') ? 0.705 : 0.625;

            if (RentChart.wrapPointPos[x + 1]) {
                name = RentChart.wrapPointPos[x + 1].prev;
                left = RentChart.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (RentChart.wrapPointPos[x]) {
                name = RentChart.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        getSerieName: function() {
            return $('#chart-rent-select2').find('option:selected').data('chart-title');
        }
    };

    //////////////////////////////
    RentChart.init();
    //////////////////////////////

    // COLLAPSES
    $(document).ready(function() {
        $("#chartCollapse").on("show.bs.collapse", function() {
            $("#helpCollapse").collapse("hide");
            $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
        });
        $("#chartCollapse").on("hide.bs.collapse", function() {
            $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
        });
        $("#helpCollapse").on("show.bs.collapse", function() {
            $("#chartCollapse").collapse("hide");
            $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
        });
        $("#helpCollapse").on("hide.bs.collapse", function() {
            $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
        });
    });
</script>