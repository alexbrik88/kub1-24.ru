<?php

use yii\web\JsExpression;

/**
 * @var $LEFT_DATE_OFFSET
 * @var $RIGHT_DATE_OFFSET
 * @var $daysPeriods
 * @var $dataLeased
 * @var $dataReturn
 * @var $dataTotalCount
 * @var $dataRentCount
 */

///////////////// colors /////////////////
$color = 'rgba(46,159,191,1)';
$color_opacity = 'rgba(46,159,191,.5)';
$color2 = 'rgba(243,183,46,1)';
$color2_opacity = 'rgba(243,183,46,.5)';
//////////////////////////////////////////
?>
<div class="ht-caption noselect" style="margin-bottom: 16px">
    СДАЧА <span style="text-transform: lowercase">в</span> АРЕНДУ <span style="text-transform: lowercase">и</span> ВОЗВРАТ
    <span class="plan-title"></span>
</div>
<div class="finance-charts-group" style="min-height:235px;">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-rent',
        'class' => 'finance-charts',
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'events' => [],
                'spacing' => [0,0,0,0],
                'marginBottom' => '50',
                'marginLeft' => '55',
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                ]
                //'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'shared' => false,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                            function(args) {

                                const index = this.series.data.indexOf( this.point );
                                const y0 = args.chart.series[0].data[index].y || 0;
                                const y1 = args.chart.series[1].data[index].y || 0;

                                return '<span class=\"title\">' + RentChart.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">Сдано: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(y0, 0, ',', ' ') + ' шт</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">Возврат: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(y1, 0, ',', ' ') + ' шт</td></tr>') +
                                    '</table>';

                            }
                        ")
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                [
                    'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'useHTML' => true,
                        'style' => [
                            'fontWeight' => '300',
                            'fontSize' => '13px',
                            'whiteSpace' => 'nowrap'
                        ]
                    ]
                ],
            ],
            'xAxis' => [
                [
                    'min' => 1,
                    'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                    'categories' => $daysPeriods,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == RentChart.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (RentChart.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (RentChart.wrapPointPos) {
                                            result += RentChart.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                        'useHTML' => true,
                        'autoRotation' => false
                    ],
                ],
            ],
            'series' => [
                [
                    'name' => 'Сдано',
                    'data' => $dataLeased,
                    'color' => $color,
                    'borderColor' => $color_opacity,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ],
                    ]
                ],
                [
                    'name' => 'Возврат',
                    'data' => $dataReturn,
                    'color' => $color2,
                    'borderColor' => $color2_opacity,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color2,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ],
                    ]
                ],
            ],
            'plotOptions' => [
                'series' => [
                    'pointWidth' => 20,
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                    'groupPadding' => 0.05,
                    'pointPadding' => 0.1,
                    'borderRadius' => 3,
                    'borderWidth' => 1
                ]
            ],
        ],
    ]); ?>
</div>

<div class="ht-caption noselect mt-3">
    ЗАГРУЖЕННОСТЬ ОБЪЕКТОВ
    <span class="object-type-title"></span>
</div>
<div class="finance-charts-group" style="min-height:235px;">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-rent-2',
        'scripts' => [
            // 'modules/exporting',
            'themes/grid-light',
            //'modules/pattern-fill'
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'line',
                'events' => [
                    'load' => null
                ],
                'spacing' => [0,0,0,0],
                'marginBottom' => '50',
                'marginLeft' => '55',
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif'
                ]
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                            function(args) {

                                const index = this.series.data.indexOf( this.point );
                                const totalCount = args.chart.series[0].data[index].y || 0;
                                const rentCount = args.chart.series[1].data[index].y || 0;
                                const percent = (totalCount > 0) ? rentCount / totalCount : 0;

                                return '<span class=\"title\">' + RentChart.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">Всего объектов: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(totalCount, 0, ',', ' ') + ' шт</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">Сдано в аренду: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(rentCount, 0, ',', ' ') + ' шт</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">%% загруженности: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * percent, 2, ',', ' ') + ' %</td></tr>') +
                                    '</table>';

                            }
                        ")
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
                'labels' => [
                    'useHTML' => true,
                    'style' => [
                        'fontWeight' => '300',
                        'fontSize' => '13px',
                        'whiteSpace' => 'nowrap'
                    ]
                ]
            ],
            'xAxis' => [
                'min' => 1,
                'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                'categories' => $daysPeriods,
                'title' => '',
                'minorGridLineWidth' => 0,
                'labels' => [
                    'formatter' => new \yii\web\JsExpression("
                                function() {
                                    result = (this.pos == window.RentChart.currDayPos) ?
                                        ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                        (window.RentChart.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                    if (window.RentChart.wrapPointPos) {
                                        result += window.RentChart.getWrapPointXLabel(this.pos);
                                    }

                                    return result;
                                }"),
                    'useHTML' => true,
                    'autoRotation' => false
                ],
            ],
            'series' => [
                [
                    'name' => 'Кол-во имеющихся',
                    'data' => $dataTotalCount,
                    'color' => 'rgba(46,159,191,1)',
                    'fillColor' => 'rgba(46,159,191,1)',
                    'negativeColor' => '#e74c3c',
                    'negativeFillColor' => '#e74c3c',
                    'dataLabels' => [
                        'enabled' => false
                    ],
                    'legendIndex' => 0
                ],
                [
                    'name' => 'Кол-во в аренде',
                    'data' => $dataRentCount,
                    'color' => 'rgba(46,204,113,1)',
                    'fillColor' => 'rgba(46,204,113,1)',
                    'negativeColor' => '#e74c3c',
                    'negativeFillColor' => '#e74c3c',
                    'dataLabels' => [
                        'enabled' => false
                    ],
                    'legendIndex' => 0
                ],
            ],
            'plotOptions' => [
                'line' => [
                    'fillOpacity' => .9,
                    'marker' => [
                        'enabled' => false,
                        'symbol' => 'circle',
                    ],
                    'dataLabels' => [
                        'enabled' => true
                    ],
                ],
                'series' => [
                    'stickyTracking' => false,
                ]
            ],
        ],
    ]); ?>
</div>
