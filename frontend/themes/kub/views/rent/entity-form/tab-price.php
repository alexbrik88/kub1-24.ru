<?php

use common\models\rent\Attribute;
use common\models\rent\AttributeValue;
use common\models\rent\Entity;
use yii\helpers\Url;

/**
 * @var Entity           $entity
 * @var Attribute[]      $attributes
 * @var AttributeValue[] $attributeValue
 */
?>
<div id="ajax-price" class="row">
    <?= $this->render('ajax-attribute', [
        'entity' => $entity,
        'attributes' => $attributes,
        'attributeValue' => $attributeValue,
        'class' => 'col-3'
    ]) ?>
</div>
