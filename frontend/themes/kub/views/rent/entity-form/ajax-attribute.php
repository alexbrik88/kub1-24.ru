<?php

use common\models\rent\Attribute;
use common\models\rent\AttributeValue;
use common\models\rent\Entity;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View             $this
 * @var Attribute[]      $attributes
 * @var Entity           $entity
 * @var AttributeValue[] $attributeValue
 * @var string           $class
 * @var string           $ownerName
 */

$ownerName = isset($ownerName) ? $ownerName : '';


foreach ($attributes as $item): ?>
    <?php $class = sprintf('col-%s', $item->grid_cols); ?>
    <?php isset($attributeValue[$item->id]) ? $attributeValue[$item->id]->hook($item->alias == 'lessor', $ownerName) : false; ?>
    <div class="form-group <?= $class ?? null ?> field-rententity-attr-<?= $item->id ?><?php if ($item->required === true) { echo ' required'; } ?> <?=$item->containerCSS?>">
        <label class="label" for="field-<?= $item->id ?>"><?= $item->title; ?> <?= !empty($item->unit) ? ', ' . $item->unit : ''; ?></label>
        <?= $this->render('_attribute-' . $item->type, ['attribute' => $item, 'value' => $attributeValue[$item->id]->value ?? ($entity->attr['attr-'.$item->id] ?? $item->default)]) ?>
        <p class="help-block help-block-error"><?= $entity->getFirstError('attr-' . $item->id) ?></p>
    </div>

    <?php if ($item->alias == 'VIN') : ?>
        <?= $this->render('_additional-status', ['entity' => $entity]) ?>
    <?php endif ?>
<?php endforeach; ?>

<?php if ($isAjax??false) : ?>
    <script type="text/javascript">
        refreshDatepicker($('#ajax-category'));
    </script>
<?php endif ?>
