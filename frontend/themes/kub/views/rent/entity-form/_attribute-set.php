<?php

use common\models\rent\Attribute;
use common\components\helpers\Html;

/**
 * @var Attribute   $attribute
 * @var string|null $value
 */
echo Html::checkboxList(
    'RentEntity[attr][' . $attribute->id . ']',
    $value,
    array_combine($attribute->data, $attribute->data), [
        'id' => 'attr-' . $attribute->id,
    ]
);