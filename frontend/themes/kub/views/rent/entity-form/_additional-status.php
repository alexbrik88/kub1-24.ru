<?php

use common\models\rent\Entity;
use yii\helpers\Html;
use yii\web\View;

/** @var $this yii\web\View */
/** @var $entity common\models\rent\Entity */

?>
<?php if ($data = $entity->getAdditionalStatusList()) : ?>
    <div class="form-group col-3">
        <label class="label">Дополнительные статусы</label>
        <?= Html::textInput('rent-entity-additional-status', $entity->getAdditionalStatusLabel(), [
            'id' => 'rent-entity-additional-status',
            'class' => 'form-control'.($entity->getAdditionalStatus() == 2 ? ' color-red' : ''),
            'readonly' => true,
            'data-list' => $data,
        ]) ?>
    </div>
<?php endif ?>
