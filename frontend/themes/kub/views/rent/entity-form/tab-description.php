<?php

use common\models\rent\Entity;
use yii\bootstrap\ActiveForm;

/**
 * @var ActiveForm $form
 * @var Entity     $entity
 */
echo $form->field($entity, 'comment')->textarea();