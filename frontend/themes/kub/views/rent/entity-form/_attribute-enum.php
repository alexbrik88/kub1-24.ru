<?php

use common\models\rent\Attribute;
use kartik\select2\Select2;
use yii\helpers\Json;

/**
 * @var Attribute   $attribute
 * @var string|null $value
 */

echo Select2::widget([
    'id' => 'rent-entity-attr-' . $attribute->id,
    'name' => 'RentEntity[attr][' . $attribute->id . ']',
    'value' => $value,
    'data' => array_combine((array)$attribute->data, (array)$attribute->data),
    'hideSearch' => true,
    'pluginLoading' => false,
    'options' => [
        'placeholder' => '',
    ],
    'pluginOptions' => [
        'width' => '100%'
    ],
]);
