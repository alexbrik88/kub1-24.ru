<?php

use common\models\Company;
use common\models\rent\Attribute;
use common\models\rent\AttributeValue;
use common\models\rent\Entity;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use frontend\widgets\ContractorDropdown;
use common\models\Contractor;
use common\components\date\DateHelper;

/**
 * @var ActiveForm       $form
 * @var Entity           $entity
 * @var string[]         $ownerList Список поставщиков (id=>title)
 * @var string[]         $categoryList Список доступных категорий (id=>title)
 * @var Attribute[]      $attributes
 * @var AttributeValue[] $attributeValue
 * @var Contractor       $defaultContractor
 */

$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';
?>
<div class="row">
    <div class="col-6">
        <?= $form->field($entity, 'category_id')->widget(Select2::class, [
            'hideSearch' => true,
            'data' => $categoryList,
            'options' => ['placeholder' => ''],
            'pluginOptions' => ['width' => '100%'],
        ]) ?>
    </div>

    <div class="col-6">
        <?= $form->field($entity, 'owner_id')->widget(ContractorDropdown::class, [
            'id' => 'ownerId',
            'contractorType' => Contractor::TYPE_SELLER,
            'company' => Yii::$app->user->identity->company,
            'staticData' => [$defaultContractor->id => $defaultContractor->company->getShortName()],
            'excludeIds' => [$defaultContractor->id ?? null],
            'options' => ['data-default_owner_id' => $defaultContractor->id ?? null],
            'pluginOptions' => ['allowClear' => false]
        ]) ?>
    </div>
</div>

<div id="ajax-category" class="row">
    <?= $this->render('ajax-attribute', [
        'entity' => $entity,
        'attributes' => $attributes,
        'attributeValue' => $attributeValue,
        'class' => 'col-3',
        'ownerName' => $entity->owner->type != Contractor::TYPE_FOUNDER ? $entity->owner->getNameWithType() : '',
    ]) ?>
</div>

<div id="block-additional-fields">
    <div class="flex-row">
        <button class="additionalFields link link_collapse link_bold button-clr mb-4 collapsed" type="button" data-toggle="collapse" data-target="#additionalFields" aria-expanded="true" aria-controls="additionalFields">
            <span class="link-txt">Информация для учета в балансе</span>
            <svg class="link-shevron svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </button>
    </div>

    <div id="additionalFields" class="collapse additionalFields">
        <div class="flex-row">
            <?= $form->field($entity, 'useful_life_in_month', ['options' => ['class' => 'form-group left']]) ?>
            <?= $form->field($entity, 'count', ['options' => ['class' => 'form-group right']]) ?>
        </div>

        <div class="flex-row">
            <?= $form->field($entity, 'purchased_price', ['options' => ['class' => 'form-group left']]) ?>
            <?= $form->field($entity, 'purchased_at', [
                'wrapperOptions' => [
                    'class' => 'form-filter date-picker-wrap',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
                'options' => ['class' => 'form-group right']
            ])->textInput([
                'class' => 'form-control date-picker',
                'data' => ['date-viewmode' => 'years'],
                'value' => DateHelper::format($entity->purchased_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
            ]) ?>
        </div>

        <div class="flex-row">
            <?= $form->field($entity, 'description')->textarea(); ?>
        </div>
    </div>
</div>

