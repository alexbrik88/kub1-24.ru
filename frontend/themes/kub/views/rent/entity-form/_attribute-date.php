<?php

use common\models\rent\Attribute;
use common\components\helpers\Html;

/**
 * @var Attribute   $attribute
 * @var string|null $value
 */
echo Html::textInput(
    'RentEntity[attr][' . $attribute->id . ']',
    $value,
    [
        'id' => 'rententity-attr-' . $attribute->id,
        'class' => 'form-control date-picker ico entity_attribute_' . $attribute->alias,
        'readonly' => true,
        'style' => 'background-color: #fff;',
    ]
);