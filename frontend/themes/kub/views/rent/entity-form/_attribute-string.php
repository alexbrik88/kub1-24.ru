<?php

use common\models\rent\Attribute;
use common\components\helpers\Html;

/**
 * @var Attribute   $attribute
 * @var string|null $value
 */
echo Html::textInput(
    'RentEntity[attr][' . $attribute->id . ']',
    $value,
    [
        'id' => 'rententity-attr-' . $attribute->id,
        'class' => 'form-control entity_attribute_' . $attribute->alias,
    ]
);