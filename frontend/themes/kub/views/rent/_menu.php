<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var string $activeTab
 */

/** @noinspection PhpUnhandledExceptionInspection */
echo Tabs::widget([
    'options' => [
        'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
    ],
    'linkOptions' => [
        'class' => 'nav-link',
    ],
    'tabContentOptions' => [
        'class' => 'tab-pane pl-3 pt-3 pr-3',
        'style' => 'width:100%'
    ],
    'headerOptions' => [
        'class' => 'nav-item',
    ],
    'items' => [
        [
            'label' => 'Календарь',
            'url' => Url::toRoute(['calendar']),
            'linkOptions' => [
                'class' => 'nav-link' . ($activeTab === 'index' ? ' active' : ''),
                'data-tab' => '_productInfo'
            ],
            'active' => $activeTab === 'index'
        ],
        [
            'label' => 'Договоры аренды',
            'url' => Url::toRoute(['rent-list']),
            'linkOptions' => [
                'class' => 'nav-link' . ($activeTab === 'list' ? ' active' : ''),
                'data-tab' => '_productSupplier'
            ],
            'active' => $activeTab === 'list'
        ],
        [
            'label' => 'Объекты',
            'url' => Url::toRoute(['entity-list']),
            'linkOptions' => [
                'class' => 'nav-link' . ($activeTab === 'entities' ? ' active' : ''),
                'data-tab' => '_productSupplier'
            ],
            'active' => $activeTab === 'entities'
        ],
    ],
]);