<?php

use common\models\rent\Attribute;
use common\models\rent\AttributeValue;
use frontend\assets\RentAsset;
use frontend\models\RentEntity;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Contractor;

/**
 * @var View             $this
 * @var RentEntity       $entity
 * @var string[]         $ownerList Список поставщиков (id=>title)
 * @var Company          $defaultOwner
 * @var string[]         $categoryList Список доступных категорий (id=>title)
 * @var Attribute[]      $attributes
 * @var AttributeValue[] $attributeValue
 * @var Contractor       $defaultContractor
 */
RentAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];
if ($entity->isNewRecord === true) {
    $this->title = 'Аренда / Добавление объекта аренды';
} else {
    $this->title = 'Аренда / Редактирование объекта аренды "' . $entity->title . '"';
}
?>
<div class="row align-items-center justify-content-between mb-3">
    <h4 class="column mb-2"><?= $entity->isNewRecord === true ? 'Добавить объект аренды' : 'Редактирование объекта аренды "' . $entity->title . '"'?></h4>
</div>

<?php $form = ActiveForm::begin([
    'class' => 'form-horizontal',
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'options' => [
        'name' => 'rentEntity',
        'data-ajax-attribute-url' => Url::to(['ajax-attribute'])
    ],
    'fieldConfig' => [
        'template' => "{label}\n{input}",
        'labelOptions' => [
            'class' => 'label',
        ]
    ]
]); ?>
<?= $form->errorSummary($entity); ?>

<div class="wrap wrap_padding_small">
    <?= $form->field($entity, 'id', ['template' => '{input}'])->hiddenInput() ?>
    <?= $form->field($entity, 'title')->textInput(['placeholder'=>'Название объекта'])->label(false) ?>
</div>
<div class="wrap">
    <?php
    /** @noinspection PhpUnhandledExceptionInspection */
    echo Tabs::widget([
        'options' => [
            'class' => 'nav nav-tabs w-100 mb-3 mr-3',
        ],
        'linkOptions' => [
            'class' => 'nav-link',
        ],
        'tabContentOptions' => [
            'class' => 'tab-pane pl-3 pt-3 pr-3',
            'style' => 'width:100%'
        ],
        'headerOptions' => [
            'class' => 'nav-item',
        ],
        'items' => [
            [
                'label' => 'Информация',
                'content' => $this->render('entity-form/tab-info', [
                    'form' => $form,
                    'entity' => $entity,
                    'ownerList' => $ownerList,
                    'categoryList' => $categoryList,
                    'attributes' => $attributes['info'],
                    'attributeValue' => $attributeValue,
                    'defaultContractor' => $defaultContractor
                ]),
                'linkOptions' => [
                    'class' => 'nav-link active',
                    'data-tab' => '_productInfo'
                ],
                'active' => true
            ],
            [
                'label' => 'Цена',
                'content' => $this->render('entity-form/tab-price', [
                    'form' => $form,
                    'entity' => $entity,
                    'attributes' => $attributes['price'],
                    'attributeValue' => $attributeValue
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                    'data-tab' => '_productPrice'
                ],
                'active' => false
            ],
            [
                'label' => 'Изображения',
                'content' => $this->render('entity-form/tab-image', ['entity' => $entity]),
                'linkOptions' => [
                    'class' => 'nav-link',
                    'data-tab' => '_productInfo'
                ],
                'active' => false
            ],
            [
                'label' => 'Описание',
                'content' => $this->render('entity-form/tab-description', [
                    'form' => $form,
                    'entity' => $entity
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                    'data-tab' => '_productSupplier'
                ],
                'active' => false
            ],
        ],
    ]);
    ?>
</div>
<div class="wrap wrap_btns visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitInput('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr move_to_group_apply'
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a(
                'Отменить',
                $entity->isNewRecord !== true ? Url::to(['entity-view', 'id' => $entity->id]) : Url::to(['entity-list']),
                ['class' => 'button-clr button-width button-regular button-hover-transparent undo-contractor']
            ); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<script>rentEntityFormPrepare();</script>