<?php

use frontend\models\RentEntity;

/**
 * @var array $attributes
 * @var array $attributeValue
 */
?>
<div class="wrap">
    <div class="row">
        <?php foreach ($attributes as $attribute) { ?>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3"><?=$attribute->title?></div>
                <div><?= $attributeValue[$attribute->id]->value ?? '---' ?></div>
            </div>
        <?php } ?>
    </div>
</div>