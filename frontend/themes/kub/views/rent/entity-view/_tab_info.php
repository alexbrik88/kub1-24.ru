<?php

use frontend\models\RentEntity;
use yii\helpers\ArrayHelper;

/**
 * @var RentEntity $entity
 * @var array $attributes
 * @var array $attributeValue
 */
?>

<div class="wrap">
    <div class="row">
        <div class="col-3 mb-4 pb-2">
            <div class="label weight-700 mb-3">Категория</div>
            <div><?= $entity->category->title ?></div>
        </div>
        <div class="col-3 mb-4 pb-2">
            <div class="label weight-700 mb-3">Название</div>
            <div><?= $entity->title ?></div>
        </div>
        <div class="col-3 mb-4 pb-2">
            <div class="label weight-700 mb-3">Тип собственности</div>
            <div><?= $entity->ownershipLabel ?></div>
        </div>
        <?php foreach ($attributes as $attribute) { ?>
            <div class="col-3 mb-4 pb-2">
                <div class="label weight-700 mb-3"><?=$attribute->title?><?= $attribute->unit ? ', ' . $attribute->unit : '' ?></div>
                <div><?= ArrayHelper::getValue($attributeValue, [$attribute->id, 'value']) ?: '---' ?></div>
            </div>
        <?php } ?>
    </div>
</div>

