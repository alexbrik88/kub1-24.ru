<?php

use frontend\models\RentEntity;

/**
 * @var RentEntity $entity
 */
?>
<div class="wrap">
    <div class="row">
        <div class="col-12 mb-4 pb-2">
            <div class="label weight-700 mb-3">Комментарий:</div>
            <div><?= $entity->comment ?></div>
        </div>
    </div>
</div>