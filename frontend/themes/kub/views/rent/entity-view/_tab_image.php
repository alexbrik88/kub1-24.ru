<?php

use frontend\models\RentEntity;
use yii\helpers\Html;

/**
 * @var RentEntity $entity
 */
?>
<div class="wrap">
    <div class="row">
        <?php for ($i = 0; $i <= 4; $i++) {
            $url = $entity->pictureThumb($i);
            if ($url === null) {
                continue;
            }
            echo Html::tag('div', Html::img($url), ['class' => 'col-3 mb-4 pb-2']);
        } ?>
    </div>
</div>
