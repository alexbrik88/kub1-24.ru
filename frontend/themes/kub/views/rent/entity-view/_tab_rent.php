<?php

use common\models\employee\Config;
use frontend\models\RentSearch;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * @var string[]           $categoryList
 * @var string[]           $contractors
 * @var string[]           $employees
 * @var RentSearch         $rentSearch
 * @var ActiveDataProvider $rentProvider
 * @var bool               $isTab
 */
?>
<?= $this->render('//rent-agreement/_list', [
    'searchModel' => $rentSearch,
    'dataProvider' => $rentProvider,
    'showEntity' => false,
    'isTab' => $isTab,
]) ?>