<?php

use common\models\rent\RentAgreement;
use frontend\assets\RentAsset;
use frontend\models\RentSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var RentAgreement         $entityRent
 * @var string[]           $categoryList
 * @var string[]           $statusList
 * @var string[]           $ownershipList
 * @var string[]           $employeeList
 * @var string[]           $contractors
 * @var string[]           $entities
 * @var RentSearch         $searchModel
 * @var ActiveDataProvider $provider
 */
$this->title = 'Объекты аренды';
?>

<?= $this->render('_entity-list', [
    'categoryList' => $categoryList,
    'statusList' => $statusList,
    'ownershipList' => $ownershipList,
    'employeeList' => $employeeList,
    'searchModel' => $searchModel,
    'provider' => $provider
]) ?>
