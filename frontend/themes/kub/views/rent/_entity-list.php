<?php

use common\components\grid\GridView;
use common\models\rent\Entity;
use frontend\models\RentSearch;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var string[]           $categoryList
 * @var string[]           $statusList
 * @var string[]           $ownershipList
 * @var string[]           $employeeList
 * @var RentSearch         $searchModel
 * @var ActiveDataProvider $provider
 */

$userConfig = Yii::$app->user->identity->config;
?>

<?php $form = ActiveForm::begin(['action' => Url::to(Yii::$app->controller->action->id), 'method' => 'GET']); ?>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'rent_entity_id',
                ],
                [
                    'attribute' => 'rent_entity_created',
                ],
                [
                    'attribute' => 'rent_entity_category',
                ],
                [
                    'attribute' => 'rent_entity_additional_status',
                ],
                [
                    'attribute' => 'rent_entity_ownership',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_rent_list', 'tableSelector' => 'table#rent_list_grid']) ?>
        <span class="ml-3 mr-auto">
            Список объектов: <strong><?= $provider->totalCount; ?></strong>
        </span>
    </div>
    <div class="col-6">
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'title', [
                    'type' => 'search',
                    'placeholder' => 'Поиск по наименованию объекта',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red mr-2',
                ]) ?>
            </div>
            <div class="form-group">
                <a href="<?= Url::to(['entity-form']) ?>" class="button-clr button-regular button-regular_padding_bigger button-regular_red">
                    <svg class="svg-icon mr-2">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-1">Объект</span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php $form->end(); ?>

<?= GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $provider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $provider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (Entity $entity) {
                return Html::checkbox('Entity[]', false, [
                    'value' => $entity->id,
                    'class' => 'joint-operation-checkbox',
                    'data-clone-url' => Url::to(['/rent/entity-form', 'copy' => 1, 'id' => $entity->id]),
                ]);
            },
        ],
        [
            'attribute' => 'id',
            'headerOptions' => [
                'class' => 'col_rent_entity_id ' . ($userConfig->rent_entity_id ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_entity_id ' . ($userConfig->rent_entity_id ? '' : ' hidden')
            ],
        ],
        [
            'attribute' => 'created_at',
            'headerOptions' => [
                'class' => 'col_rent_entity_created ' . ($userConfig->rent_entity_created ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_entity_created ' . ($userConfig->rent_entity_created ? '' : ' hidden')
            ],
            'format' => ['date', 'php:d.m.Y']
        ],
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function(Entity $entity) {
                return Html::a(Html::encode($entity->title), Url::to(['entity-view', 'id' => $entity->id]));
            }
        ],
        [
            'attribute' => 'category_id',
            'headerOptions' => [
                'class' => 'col_rent_entity_category ' . ($userConfig->rent_entity_category ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_entity_category ' . ($userConfig->rent_entity_category ? '' : ' hidden')
            ],
            'value' => 'category.title',
            'filter' => ['' => 'Все'] + $categoryList,
            's2width' => '150px'
        ],
        [
            'attribute' => 'status',
            'value' => function(Entity $entity) {
                return Entity::STATUS_LABELS[$entity->status];
            },
            'filter' => ['' => 'Все'] + $statusList,
            's2width' => '150px'
        ],
        [
            'label' => 'Доп. статус',
            'headerOptions' => [
                'class' => 'col_rent_entity_additional_status ' . ($userConfig->rent_entity_additional_status ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_entity_additional_status ' . ($userConfig->rent_entity_additional_status ? '' : ' hidden')
            ],
            'value' => 'additionalStatusLabel',
        ],
        [
            'attribute' => 'ownership',
            'headerOptions' => [
                'class' => 'col_rent_entity_ownership ' . ($userConfig->rent_entity_ownership ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'col_rent_entity_ownership ' . ($userConfig->rent_entity_ownership ? '' : ' hidden')
            ],
            'value' => function(Entity $entity) {
                return Entity::OWNERSHIP_TYPE_LABELS[$entity->ownership];
            },
            'filter' => ['' => 'Все'] + $ownershipList,
            's2width' => '150px'
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Ответственный',
            'value' => function(Entity $entity) {
                return $entity->employee->getShortFio();
            },
            'filter' => ['' => 'Все'] + $employeeList,
            'hideSearch' => false,
            's2width' => '150px'
        ]
    ]
]);
?>

<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox select-on-check-all" id="Allcheck" type="checkbox" name="count-list-table"/>
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано:
                <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto"></div>
        <div class="column">
            <?= Html::a('<span>Копировать</span>', ['entity-form', 'copy' => true, 'id' => 0], [
                'class' => 'button-clr button-regular button-width button-hover-transparent btn-copy',
            ]) ?>
            <?= Html::a('<span>Выбыл</span>', '#many-retired', [
                'class' => 'button-clr button-regular button-width button-hover-transparent btn-retired',
                'data-toggle' => 'modal'
            ]) ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', '#many-delete', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
            ]) ?>
        </div>
    </div>
</div>

<div id="many-delete" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные объекты (<span class="total-count">0</span>)?</h4>
            <?php ActiveForm::begin(['action' => ['entity-delete']]); ?>
            <?= Html::hiddenInput('id', null, ['class' => 'selected-ids']) ?>
            <div class="text-center">
                <?= \yii\bootstrap4\Html::submitButton('Да', [
                    'id' => 'confirm-many-delete',
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-action' => Url::to(['entity-delete']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div id="many-retired" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите сделать выбывшими выбранные объекты (<span class="total-count">0</span>)?</h4>
            <?php ActiveForm::begin(['action' => ['entity-retired']]); ?>
            <?= Html::hiddenInput('ids', null, ['class' => 'selected-ids']) ?>
            <div class="text-center">
                <?= \yii\bootstrap4\Html::submitButton('Да', [
                    'id' => 'confirm-many-retired',
                    'class' => 'modal-many-retired button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php $this->registerJs('rentEntityListPrepare();') ?>
