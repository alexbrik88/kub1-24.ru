<?php

use common\components\TextHelper;
use common\models\currency\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('div', $model->currency_name, [
    'class' => 'currency_name',
    'data' => [
        'initial' => $model->currency_name,
    ],
    'style' => 'display: inline-block;'
]);
$currencyAmount = Html::tag('span', $model->currency_amount, [
    'class' => 'currency_rate_amount',
    'data' => [
        'initial' => $model->currency_amount,
    ],
]);
$currencyRate = Html::tag('span', $model->currency_rate, [
    'class' => 'currency_rate_value',
    'data' => [
        'initial' => $model->currency_rate,
    ],
]);
\common\components\floatLabelField\FloatLabelAsset::register(Yii::$app->getView());
$radioName = Html::getInputName($model, 'currency_rate_type');
$rateText = "{$currencyAmount} {$currencyName} = {$currencyRate} RUB";
?>

<div class="about-card-item">
    <span class="text-grey">Обменный курс:</span>
    <span>
        <?= Html::A($rateText, 'javascript:void;', [
            'class' => 'link invoice-currency-toggle',
            'style' => 'position: relative;',
            'data-target' => 'invoice_currency'
        ]); ?>
    </span>
</div>
<div class="about-card-item">
    <span class="text-grey">Сумма счета в RUB:</span>
    <span>
        <?= TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2); ?>
    </span>
</div>

    <div class="invoice-wrap" data-id="invoice_currency">
        <div class="invoice-wrap-in invoice-wrap-scroll">
            <button class="invoice-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="invoice_currency">
                <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
            </button>
            <div style="position: absolute;left:30px;top:30px;"><h4 class="text_size_20 weight-400 line-height-1 pb-1 mb-2">Установить обменный курс</h4></div>

            <div class="main-block" style="padding-top: 40px;">
                <div class="field-invoice-currency_rate_type">
                    <?= Html::activeHiddenInput($model, 'currency_name', [
                        'data' => [
                            'initial' => $model->currency_name,
                        ],
                    ]); ?>
                    <?= Html::activeHiddenInput($model, 'currency_amount', [
                        'class' => 'currency_amount currency_rate_amount',
                        'data' => [
                            'initial' => $model->currency_amount,
                        ],
                    ]); ?>
                    <?= Html::activeHiddenInput($model, 'currency_rate', [
                        'class' => 'currency_rate currency_rate_value',
                        'data' => [
                            'initial' => $model->currency_rate,
                        ],
                    ]); ?>
                    <?= Html::hiddenInput($radioName, '') ?>
                    <?= Html::beginTag('div', [
                        'id' => Html::getInputId($model, 'currency_rate_type'),
                        'class' => 'row',
                        'data' => [
                            'geturl' => Url::to(['/site/currency-rate']),
                            'seturl' => Url::to(['/documents/invoice/set-currency-rate', 'type' => $model->type, 'id' => $model->id]),
                        ],
                        'style' => 'padding-left: 25px;padding-right: 15px;padding-top: 40px;',
                    ]); ?>
                    <div class="radio-list" style="margin: 0;">
                        <div style="width: 100%;display: inline-block; margin: 0 20px 0 0; border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_SERTAIN_DATE, [
                                'id' => 'radio_currency_rate_ondate',
                                'value' => Currency::RATE_SERTAIN_DATE,
                                'class' => 'currency-rate-type-radio',
                                'data' => [
                                    'initial' => $model->currency_rate_type == Currency::RATE_SERTAIN_DATE,
                                ],
                            ]) ?>
                            <label for="radio_currency_rate_ondate" style="margin: 0;font-size: 15px;">
                                <?= Html::activeHiddenInput($model, 'currency_rate_amount', [
                                    'class' => 'currency_rate_amount',
                                    'data' => [
                                        'initial' => $model->currency_rate_amount,
                                    ],
                                ]) ?>
                                <?= Html::activeHiddenInput($model, 'currency_rate_value', [
                                    'class' => 'currency_rate_value',
                                    'data' => [
                                        'initial' => $model->currency_rate_value,
                                    ],
                                ]) ?>
                                <a href="http://www.cbr.ru" target="_blank" class="no-decoration">Курс ЦБ РФ</a> на
                                <div class="date-picker-wrap" style="display: inline-block; width: 130px; margin: 0 10px;">
                                    <?= Html::activeTextInput($model, 'currency_rateDate', [
                                        'class' => 'invoice-currency_ratedate currency_rate_date form-control',
                                        'data' => [
                                            'default' => $model->document_date,
                                            'initial' => $model->currency_rateDate,
                                        ],
                                        'style' => 'font-size: 15px; display: inline-block;',
                                        'readonly' => true,
                                    ]) ?>
                                    <svg class="date-picker-icon svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                    </svg>
                                </div>
                                 <?= Html::tag('div', $model->currency_rate_amount, [
                                    'class' => 'currency_rate_amount',
                                    'data' => [
                                        'initial' => $model->currency_rate_amount,
                                    ],
                                    'style' => 'display: inline;',
                                ]); ?>
                                <?= $currencyName ?> =
                                <?= Html::tag('div', $model->currency_rate_value, [
                                    'class' => 'currency_rate_value',
                                    'data' => [
                                        'initial' => $model->currency_rate_value,
                                    ],
                                    'style' => 'display: inline;',
                                ]) ?>
                                RUB
                            </label>
                        </div>
                        <div style="width: 100%;display: inline-block; margin: 25px 20px 0 0; border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_PAYMENT_DATE, [
                                'id' => 'radio_currency_rate_onpayment',
                                'value' => Currency::RATE_PAYMENT_DATE,
                                'class' => 'currency-rate-type-radio',
                                'data' => [
                                    'initial' => $model->currency_rate_type == Currency::RATE_PAYMENT_DATE,
                                ],
                            ]) ?>
                            <label for="radio_currency_rate_onpayment" style="margin: 0;font-size: 15px;">
                                <a href="http://www.cbr.ru" target="_blank" class="no-decoration">Курс ЦБ РФ</a> на день оплаты.
                            </label>
                            <div style="padding-left: 28px;">
                                (До оплаты счет учитывается по курсу на <?= Html::tag('div', $model->currency_rateDate, [
                                    'class' => 'currency_rate_date',
                                    'data' => [
                                        'initial' => $model->currency_rateDate,
                                    ],
                                    'style' => 'display: inline;',
                                ]) ?>, при оплате пересчитывается).
                            </div>
                        </div>
                        <div style="width: 100%;display: inline-block; margin: 25px 20px 0 0;border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_CUSTOM, [
                                'id' => 'radio_currency_rate_custom',
                                'value' => Currency::RATE_CUSTOM,
                                'class' => 'currency-rate-type-radio',
                                'data' => [
                                    'initial' => $model->currency_rate_type == Currency::RATE_CUSTOM,
                                ],
                            ]) ?>
                            <label for="radio_currency_rate_custom" style="margin: 0;font-size: 15px;">
                                Установить другой обменный курс:<br>
                            </label>
                            <div style="padding-left: 28px">
                                <?= Html::activeTextInput($model, 'currencyAmountCustom', [
                                    'value' => $model->currency_amount,
                                    'class' => 'currency-custom-input currency_rate_amount form-control',
                                    'style' => 'width: 80px;text-align: center;font-size: 15px; display: inline-block;',
                                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                                    'data' => [
                                        'initial' => $model->currency_amount,
                                    ],
                                ]) ?>
                                <?= $currencyName ?>
                                <div style="display: inline-block;">=</div>
                                <?= Html::activeTextInput($model, 'currencyRateCustom', [
                                    'value' => $model->currency_rate,
                                    'class' => 'currency-custom-input currency_rate_value form-control',
                                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                                    'style' => 'text-align: center;font-size: 15px; display: inline-block; width:120px;',
                                    'data' => [
                                        'initial' => $model->currency_rate,
                                    ],
                                ]) ?>
                                <div style="display: inline-block;">RUB</div>
                            </div>
                        </div>
                    </div>
                    <?= Html::endTag('div'); ?>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'id' => 'currency_rate_apply',
                            'class' => 'button-regular button-width button-regular_red button-clr',
                        ]); ?>
                        <button id="currency_rate_cancel" type="button" class="button-regular button-clr button-width button-regular button-hover-transparent">Отменить</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $js = <<<JS
window.rateCalendarEventsOn = false;
$(document).on("click", ".invoice-currency-toggle", function(e) {
    e.preventDefault();
    var target = $(this).data("target");
    $("[data-id=\""+target+"\"]").toggleClass("visible show");

     $('#invoice-currency_ratedate').prop('readonly', !$('#radio_currency_rate_ondate').is(':checked'));

    window.rateCalendarEventsOn = true;

    refreshDatepicker();
});
$(document).on('click', "#invoice-currency_ratedate", function(e) {
    if ($(this).prop('readonly')) {
        e.stopPropagation();
        return false;
    }
});
$(document).on("change", "input.currency-rate-type-radio", function () {
    $('#invoice-currency_ratedate').prop('readonly', !$('#radio_currency_rate_ondate').is(':checked'));
});

JS;

$this->registerJs($js);

$this->registerJs("

var v = $('#invoice-currency_ratedate').val().split('.');
$('#invoice-currency_ratedate').datepicker({
    keyboardNav: false,
    showOtherMonths: false,
    autoClose: true,
    inline: false,
    prevHtml:   '<svg class=\"svg-icon\">'+
                    '<use xlink:href=\"/img/svg/svgSprite.svg#shevron\"></use>'+
                '</svg>',
    nextHtml:   '<svg class=\"svg-icon\">'+
                    '<use xlink:href=\"/img/svg/svgSprite.svg#shevron\"></use>'+
                '</svg>',
    onShow: function(dp, animationCompleted) {
        if ( !animationCompleted ) {
            dp.el.parentElement.appendChild(dp.\$datepicker[0]);
            dp.el.parentElement.querySelector('.datepicker').classList.add('visible');
            dp.el.parentElement.querySelector('.svg-icon').classList.add('red');
        };
        if ( $(dp.el).closest('.popup-dropdown_calendar').length ) {
            $(dp.el).addClass('date-picker_double');
            $('.date-picker:not(.date-picker_double)').datepicker().data('datepicker').show();
        }
    },
    onHide: function(dp, animationCompleted) {
        if ( !animationCompleted ) {
            dp.el.parentElement.appendChild(dp.\$datepicker[0]);
            dp.el.parentElement.querySelector('.datepicker').classList.remove('visible');
            dp.el.parentElement.querySelector('.svg-icon').classList.remove('red');
        };
        if ( $(dp.el).closest('.popup-dropdown_calendar').length ) {
            $(dp.el).removeClass('date-picker_double');
        }
    },
    onSelect: function(formattedDate, date, inst) {
        if (window.rateCalendarEventsOn)
            $(inst.el).trigger('change');
    }
});

if (v.length == 3) {
    var date = new Date(v[2], v[1] - 1, v[0]);
    if (date.getTime() === date.getTime()) {
        $('#invoice-currency_ratedate').data('datepicker').selectDate(date);
    }
}

");