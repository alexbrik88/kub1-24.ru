<?php

use frontend\themes\kub\components\Icon;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\models\AuthSignupForm */

$this->registerJs("
$(document).on('click', '.toggle-password-input-type', function () {
    var wrapper = $(this).parent();
    var input = wrapper.children('input');
    var isVisible = input.attr('type') == 'text';
    wrapper.toggleClass('pass-visible', !isVisible);
    input.attr('type', isVisible ? 'password' : 'text');
});
");
?>

<?= $form->field($model, 'email') ?>

<?= $form->field($model, 'password', [
    'wrapperOptions' => [
        'class' => 'form-filter password-input-wrapper',
    ],
    'parts' => [
        '{input}' => Html::activePasswordInput($model, 'password', [
            'class' => 'form-control'
        ]) . Icon::get('eye', [
            'class' => 'toggle-password-input-type to-visible',
            'title' => 'Показать пароль',
        ]) . Icon::get('eye-ban', [
            'class' => 'toggle-password-input-type to-invisible',
            'title' => 'Скрыть пароль',
        ]),
    ],
]) ?>

<?= $form->field($model, 'rememberMe')->checkbox() ?>
<div style="color:#999;margin:1em 0">
    Если Вы забыли пароль, то Вы можете
    <?= Html::a('сбросить его', ['site/request-password-reset'], [
        'target' => '_blank',
    ]) ?>.
</div>
<div class="form-group">
    <?= Html::submitButton('Войти', [
        'class' => 'button-regular button-regular_red width-120',
    ]) ?>
</div>
