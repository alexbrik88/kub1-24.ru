<?php

use common\models\company\CompanyType;
use frontend\models\AuthSignupForm;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuthSignupForm */

$typeArray = CompanyType::find()->andWhere(['id' => AuthSignupForm::$typeIds])->all();
$offerLink = ArrayHelper::getValue(Yii::$app->params, 'offerLicenseAgreementLink', '#');
?>

    <?= $form->field($model, 'email')->textInput(); ?>

    <?= $form->field($model, 'companyType', [
        'options' => [
            'class' => 'form-group',
        ],
    ])->radioList(ArrayHelper::map($typeArray, 'id', 'name')); ?>

    <?= $form->field($model, 'taxationTypeOsno', [
        'options' => ['class' => 'form-group'],
        'parts' => [
            '{input}' => Html::tag(
                'div',
                Html::activeCheckbox($model, 'taxationTypeOsno', ['labelOptions' => [
                    'class' => 'label',
                    'style' => 'display: block;',
                ]]) .
                Html::activeCheckbox($model, 'taxationTypeUsn', ['labelOptions' => [
                    'class' => 'label',
                    'style' => 'display: block;',
                ]]) .
                Html::activeCheckbox($model, 'taxationTypeEnvd', ['labelOptions' => [
                    'class' => 'label',
                    'style' => 'display: block;',
                ]]) .
                Html::activeCheckbox($model, 'taxationTypePsn', [
                    'labelOptions' => [
                        'class' => 'label',
                        'style' => 'display: block;',
                    ],
                    'disabled' => true,
                ])
            ),
        ]
    ])->label('Система налогобложения')->render(); ?>

    <?= $form->field($model, 'checkrules')->checkbox([
        'uncheck' => null,
    ])->label('Принимаю условия ' . Html::a('лицензионного соглашения', $offerLink, [
        'target' => '_blank',
        'class' => 'link',
    ])); ?>

    <?= Html::submitButton('Подтвердить', [
        'class' => 'button-regular button-regular_red width-120',
    ]); ?>
</div>
