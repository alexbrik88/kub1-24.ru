<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\company\CheckingAccountant;
use common\models\EmployeeCompany;
use frontend\models\StoreSearch;
use frontend\themes\kub\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => "{items}\n{pager}",

    'columns' => [
        [
            'attribute' => 'name',
            'header' => 'Название',
            'headerOptions' => [
            ],
        ],
        [
            'attribute' => 'responsible_employee_id',
            'class' => DropDownSearchDataColumn::class,
            'headerOptions' => [
            ],
            'format' => 'raw',
            'value' => function ($model) {
                $e = EmployeeCompany::find()->andWhere([
                    'employee_id' => $model->responsible_employee_id,
                    'company_id' => $model->company_id,
                ])->one();

                return $e ? $e->getFio(true) : '';
            },
        ],
        [
            'label' => 'Тип',
            'value' => function ($model) {
                return $model->is_main ? 'Основной' : ($model->is_closed ? 'Закрыт' : 'Дополнительный');
            },
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'controller' => '/store',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $data) {
                    return Html::a(Icon::get('pencil'), $url, [
                        'class' => 'link ajax-modal-btn',
                        'data-pjax' => '0',
                        'data-title' => 'Изменить склад',
                        'title' => Yii::t('yii', 'Изменить'),
                        'aria-label' => Yii::t('yii', 'Изменить'),
                    ]);
                },
                'delete' => function ($url, $data) {


                    if ($data->is_main) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Удалить основной склад нельзя'
                        ]);
                    } elseif (!$data->canDelete()) {
                        return Html::tag('span', Icon::get('garbage'), [
                            'class' => 'ml-1 grey-link disabled-link',
                            'title' => 'Удалить склад нельзя, т.к. либо есть товар на данном складе, либо есть документы, связанные со складом. 
                                Удалите товары и документы, после этого можно удалить склад.'
                        ]);
                    } else {
                        return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'link',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить склад?',
                        ]);
                    }
                },
            ],
        ],
    ],
]);
