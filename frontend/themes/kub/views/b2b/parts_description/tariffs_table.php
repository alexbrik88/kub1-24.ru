<?php

use common\models\service\SubscribeTariffGroup;
use common\models\service\SubscribeTariff;
use common\models\service\DiscountByQuantity;
use php_rutils\RUtils;

$tariffs = SubscribeTariff::find()->actual()
    ->andWhere(['tariff_group_id' => SubscribeTariffGroup::B2B_PAYMENT])
    ->orderBy('price')
    ->asArray()
    ->all();

$tariffsByDuration = \yii\helpers\ArrayHelper::index($tariffs, 'duration_month');

$companiesCount = [1,2,3];

$discount = DiscountByQuantity::find()
    ->asArray()
    ->all();

$discountByCompanies = [];


//var_dump($tariffsByDuration);
?>

<table class="table table-striped table-bordered table-hover">
    <tbody>
    <tr class="heading">
        <th class="text-center" width="25%">Кол-во компаний</th>
        <?php foreach ($tariffsByDuration as $tariff): ?>
            <th class="text-center" width="25%">
                Стоимость <br>
                за <?= RUtils::numeral()->getPlural($tariff['duration_month'], ['месяц', 'месяца', 'месяцев']); ?>
            </th>
        <?php endforeach; ?>
    </tr>
    <?php foreach ($companiesCount as $count): ?>
        <tr>
            <td class="text-center"><?= $count ?></td>
            <?php foreach ($tariffsByDuration as $tariff): ?>
                <?php $discount = ($count > 1) ?
                    DiscountByQuantity::find()
                        ->where(['tariff_id' => $tariff['id']])
                        ->andWhere(['quantity' => $count])
                        ->asArray()
                        ->one() : 0;
                    ?>
                <td class="text-center">
                    <?= round($count * ($tariff['price'] * (1 - ($discount['percent'] ?? 0)/100))) ?> <i class="fa fa-rub"></i>
                </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>