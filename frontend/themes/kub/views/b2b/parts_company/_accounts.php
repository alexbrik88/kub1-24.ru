<?php

use common\models\company\CheckingAccountant;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\BtnConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\Company */
/* @var $account common\models\company\CheckingAccountant[] */

$hasCount = count($accounts);
$newAccounts = [];
for ($i=0; $i < 10; $i++) {
    $newAccounts[$hasCount + $i] = new CheckingAccountant([
        'company_id' => $model->id,
        'type' => CheckingAccountant::TYPE_ADDITIONAL,
    ]);
}
?>

<div id="company-accounts-list">
    <?php foreach ($accounts as $key => $account) : ?>
        <?php $css = $key == 0 ? 'show-first-account-number' . ($hasCount == 1 ? ' hidden' : '') : ''; ?>
        <div class="company-account company-accounts-item" data-id="<?= $account->id ?>">
            <strong class="small-title d-block mb-3 pt-3">
                Расчёт счет №
                <span class="<?= $css ?>">
                    <?= $key + 1 ?>
                </span>

                <?php if ($key > 0): ?>
                <?= BtnConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                        'class' => 'ml-1 link',
                        'style' => 'color: #bbc1c7;',
                        'tag' => 'a'
                    ],
                    'modelId' => $account->id,
                    'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                ]); ?>
                <?php endif; ?>

            </strong>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bik")->textInput([
                                'maxlength' => true,
                                'class' => 'form-control input-sm dictionary-bik',
                                'autocomplete' => 'off',
                                'data' => [
                                    'url' => Url::to(['/dictionary/bik']),
                                    'target-name' => '#' . Html::getInputId($account, "[$key]bank_name"),
                                    'target-city' => '#' . Html::getInputId($account, "[$key]bank_city"),
                                    'target-ks' => '#' . Html::getInputId($account, "[$key]ks"),
                                    'target-rs' => '#' . Html::getInputId($account, "[$key]rs"),
                                ]
                            ])->label('БИК вашего банка') ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]rs")->textInput(['maxlength' => true]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bank_name")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]ks")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bank_city")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
    <?php $i = 0; ?>
    <?php foreach ($newAccounts as $key => $account) : ?>
        <?php $i++; ?>
        <div class="new-account company-account company-accounts-item" data-id="<?= "new-{$i}" ?>" style="display: none">
            <strong class="small-title d-block mb-3 pt-3">
                Расчёт счет №
                <span class=""><?= $key + 1 ?></span>

                <?= BtnConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                        'class' => 'ml-1 link',
                        'tag' => 'a'
                    ],
                    'modelId' => "new-{$i}",
                    'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                ]); ?>

            </strong>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bik")->textInput([
                                'maxlength' => true,
                                'disabled' => true,
                                'class' => 'form-control input-sm dictionary-bik',
                                'autocomplete' => 'off',
                                'data' => [
                                    'url' => Url::to(['/dictionary/bik']),
                                    'target-name' => '#' . Html::getInputId($account, "[$key]bank_name"),
                                    'target-city' => '#' . Html::getInputId($account, "[$key]bank_city"),
                                    'target-ks' => '#' . Html::getInputId($account, "[$key]ks"),
                                    'target-rs' => '#' . Html::getInputId($account, "[$key]rs"),
                                ]
                            ])->label('БИК вашего банка') ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]rs")->textInput([
                                'maxlength' => true,
                                'disabled' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bank_name")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                                'disabled' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]ks")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                                'disabled' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, "[$key]bank_city")->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                                'disabled' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>

<?= Html::button(Icon::get('add-icon', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Добавить расчётный счёт</span>', [
    'class' => 'button-clr button-regular button-regular_red pl-3 pr-3',
    'id' => 'add-new-account',
]); ?>

<?php
$this->registerJs(<<<JS
    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        var item = $('#company-accounts-list .company-accounts-item:hidden').first();
        $('input', item).prop('disabled', false);
        item.show(250);
    });
JS
);
?>
