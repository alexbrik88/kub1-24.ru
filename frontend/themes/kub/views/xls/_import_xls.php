<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.12.2016
 * Time: 8:09
 */

use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $header string
 * @var $text string
 * @var $formData string
 * @var $uploadXlsTemplateUrl string
 */

?>
<div class="modal fade" id="import-xls" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"><?= $header; ?></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить файл</span>', 'javascript:;', [
                            'class' => 'button-regular button-width button-hover-content-red button-clr add-xls-file',
                            'style' => 'width:160px'
                        ]); ?>
                    </div>
                    <div class="col-6 upload-xls-button ta-r">
                        <?= Html::a('<span>Загрузить</span>', 'javascript:;', [
                            'class' => 'button-regular button-width button-hover-content-red button-clr disabled',
                            'data-tooltip-content' => '#upload-xls-tooltip',
                            'data-tooltipster' => Json::encode([
                                'contentAsHTML' => true,
                            ]),
                            'style' => 'width:160px'
                        ]); ?>
                        <div class="hidden">
                            <div id="upload-xls-tooltip">
                                Сначала добавьте файл для загрузки
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row xls-title">
                    <div class="gray-alert col-12 mt-2">
                        <?= $text; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="margin-top: 20px;">
                        <div class="xls-error"></div>
                        <div class="row">
                            <?php ActiveForm::begin([
                                'action' => '#',
                                'method' => 'post',
                                'id' => 'xls-ajax-form',
                                'options' => [
                                    'enctype' => 'multipart/form-data',
                                ],
                            ]); ?>
                            <?= $formData; ?>
                            <?= Html::hiddenInput('createdModels', null, [
                                'class' => 'created-models',
                            ]); ?>
                            <div class="file-list col-12">
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
                            'class' => 'upload-xls-template-url',
                        ]); ?>
                    </div>
                    <div class="row action-buttons buttons-fixed xls-buttons" style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
                        <div class="button-bottom-page-lg col-3" style="text-align: left;width: 50%">
                            <?= Html::a('Сохранить', 'javascript:;', [
                                'class' => 'button-regular button-width button-regular_red button-clr undelete-models',
                            ]); ?>
                        </div>
                        <div class="button-bottom-page-lg col-6" style="text-align: right;width: 50%;">
                            <?= Html::a('Отменить', 'javascript:;', [
                                'class' => 'button-clr button-width button-regular button-hover-transparent xls-close',
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div id="progressBox"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
