<?php

use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\company\CompanyInfoIndustry;
use common\models\company\CompanyIndustry;

/** @var boolean $isSaved */
/** @var boolean $isSavedNewRecord */
/** @var CompanyIndustry $model */

$industryList = ArrayHelper::map(CompanyInfoIndustry::find()
    ->select(['id', 'name'])
    ->orderBy(new Expression('IF (id = '.CompanyInfoIndustry::OTHER.', 1, 0), name'))
    ->asArray()
    ->all(), 'id', 'name');
?>

<h4 class="modal-title">
    <?= ($model->isNewRecord) ? 'Добавить' : 'Редактировать' ?> направление
</h4>

<?php

if (Yii::$app->request->isAjax && $isSaved) {

    echo Html::script('
        window.toastr.success("Направление' . ($isSavedNewRecord ? ' добавлено' : ' изменено') . '");        
        $(".modal.show").modal("hide");
        $.pjax.reload({container: "#company-industry-pjax-container"});
        ',
        ['type' => 'text/javascript']);
}

$form = ActiveForm::begin([
    'id' => 'form-company-industry',
    'method' => 'POST',
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
        'data-pjax' => true,
    ],
]);
?>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'name')->label('Название направления'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'industry_type_id')->widget(Select2::class, [
                        'data' => $industryList,
                        'options' => [
                            'disabled' => !$model->isNewRecord
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                    ])->label('Тип направления') ?>
                    <?php if (!$model->isNewRecord): ?>
                        <?= $form->field($model, 'industry_type_id')->hiddenInput()->label(false) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php if (!$model->is_main) : ?>
                        <?= $form->field($model, 'is_main')->checkbox([], true)->label('Основное направление', [
                            'class' => 'label mb-0',
                        ]) ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>
