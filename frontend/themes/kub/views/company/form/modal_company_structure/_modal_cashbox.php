<?php

use common\models\employee\Employee;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use \common\models\companyStructure\OfdType;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$employeeArray = $model->company->getEmployeeCompanies()->andWhere([
    'or',
    ['is_working' => true],
    ['employee_id' => $model->responsible_employee_id],
])->orderBy([
    'lastname' => SORT_ASC,
    'firstname' => SORT_ASC,
    'patronymic' => SORT_ASC,
])->all();
$ofdTypesArray = ArrayHelper::map(
    OfdType::find()
        ->where(['or',
            ['company_id' => null],
            ['company_id' => $model->id]])
        ->orderBy('sort')
        ->all(), 'id', 'name');

$accessibleList = ['all' => 'Всем'] + ArrayHelper::map($employeeArray, 'employee_id', 'fio');
$templateDateInput = "{label}<div class='date-picker-wrap' style='width: 130px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

if (Yii::$app->request->isAjax && $model->saved) {

    if (isset($wasUpdated)) {
        echo Html::script('
            // $(\'.cashbox-item-modal\').filter(\'[data-id = "'.$model->id.'"]\').find(".cashbox-name").html("'.htmlentities($model->name).'");
            $("#cashbox-modal").modal("hide");
        ', ['type' => 'text/javascript']);
    } else {

        echo Html::script('
        var newState = new Option("' . $model->name . '", ' . $model->id . ', true, true);
        $(".cashbox-item-modal").eq(currentCashboxPos).find("select").append(newState).trigger("change");

        // var template = $(".cashbox-list-modal").find(".template").clone();
        // $(template).removeClass("template");
        // $(template).attr("data-id", "' . $model->id . '");
        // $(template).find(".cashbox-name").html("' . htmlentities($model->name) . '");
        // $(template).append("<input type=\'hidden\' name=\'SalePoint[cashboxes][]\' value=\'' . $model->id . '\'/>");
        // $(template).show();
        // $(".cashbox-list-modal").find(".template").before(template);

        $("#cashbox-modal").modal("hide");',

            ['type' => 'text/javascript']);
    }
} ?>

<h4 class="modal-title">
    <?= ($model->isNewRecord) ? 'Добавить' : 'Редактировать' ?> кассу
</h4>

<?php $form = ActiveForm::begin([
    'id' => 'cashbox-form-2',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
        'data-pjax' => true
    ],
]); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'name')->textInput([
                'placeholder' => 'Например: Касса 1 магазина №1',
                'maxlength' => true
            ])->label('Название') ?>
        </div>

        <?php if (!$model->is_main) : ?>
            <div class="col-6">
                <?= $form->field($model, 'accessible')->widget(Select2::class, [
                    'data' => $accessibleList,
                    'options' => [
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
        <?php endif ?>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'is_accounting', [
                'options' => [
                    'style' => 'padding-top:' . ($model->is_accounting ? '36px' : '0')
                ]])->checkbox([], true) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'ofd_type_id', [
                'options' => ['style' => (!$model->is_accounting) ? 'display:none' : '']
            ])->widget(Select2::class, [
                'data' => $ofdTypesArray,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]) ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord && !$model->is_main) : ?>
        <?= $form->field($model, 'is_closed')->checkbox([], true) ?>
    <?php endif ?>

    <?php if ($model->isNewRecord) : ?>
        <?= $form->field($model, 'createStartBalance')->checkbox([], true) ?>

        <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : null; ?>">
            <div class="col">
                <?= $form->field($model, 'startBalanceAmount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                ]) ?>
            </div>
            <div class="col">
                <?= $form->field($model, 'startBalanceDate', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                ])->label('на дату') ?>
            </div>
        </div>
    <?php endif ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-width button-clr',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
            'data-dismiss' => 'modal'
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>


<?= Html::script('

// REFRESH_UNIFORMS
refreshUniform();
refreshDatepicker();

$("#cashbox-createstartbalance").on("change", function (e) {
    $(".start-balance-block").toggleClass("hidden");
});

$("#cashbox-is_accounting").on("change", function(e) {
    if ($(this).prop("checked")) {
        $(".field-cashbox-is_accounting").css({"padding-top":"36px"});
        $(".field-cashbox-ofd_type_id").show();
    } else {
        $(".field-cashbox-is_accounting").css({"padding-top":"0"});
        $(".field-cashbox-ofd_type_id").hide();
    }
});

', [
'type' => 'text/javascript',
]); ?>
