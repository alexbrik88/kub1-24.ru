<?php
use common\models\companyStructure\SalePoint;
/** @var integer $type_id */

$pointName = 'отделу';
switch ($type_id){
    case SalePoint::TYPE_DEPARTMENT:
        $pointName = 'Отделу продаж';
        break;
    case SalePoint::TYPE_SHOP:
        $pointName = 'магазину';
        break;
    case SalePoint::TYPE_FILIAL:
        $pointName = 'филиалу';
        break;
}

?>
<div style="display:none;">
    <div id="tooltip3_companyName">Юридическое лицо, к которому относится магазин.</div>
    <div id="tooltip3_name">Название магазина – это название, по которому вы будете идентифицировать магазин в КУБе</div>
    <?php if ($type_id == SalePoint::TYPE_SHOP || $type_id == SalePoint::TYPE_RESTAURANT): ?>
        <div id="tooltip3_store">Название склада – это название, по которому вы будете идентифицировать склад в КУБе. <br/>Лучше связать название склада, с названием магазина.</div>
    <?php else: ?>
        <div id="tooltip3_store">Выберите склад или добавьте его, если его еще нет в вашем списке. <br/>Склад, с которого будет списываться товар.</div>
    <?php endif; ?>
    <div id="tooltip3_city">Для отражения на карте</div>
    <div id="tooltip3_address">Для отражения на карте</div>
    <div id="tooltip3_cashbox">Добавьте кассу, которая относится к данному <?= $pointName ?>. <br/>В кассе будут отражаться наличные деньги.</div>
    <div id="tooltip3_employee">Вы можете добавить сотрудников в отдел, что бы видеть продажи каждого. <br/>Сотрудники могут выставлять счета из КУБа, это позволит вам оперативно видеть выставленные счета, <br/>отгруженный товар, дебиторскую задолженность. Сотрудники будут видеть только свои счета <br/>и своих клиентов. Блок ФинДиректор сотрудники не видят.</div>
    <div id="tooltip3_site">Укажите домен вашего интернет магазина</div>
    <div id="tooltip3_online_payment_type">Если вы принимаете оплату на сайте, то укажите сервис. <br/>Данные будут подтягиваться из этого сервиса</div>
</div>
