<?php

use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use \frontend\themes\kub\helpers\Icon;
use common\models\cash\Cashbox;
use common\models\Company;
use common\components\helpers\ArrayHelper;
use yii\web\JsExpression;
use common\models\employee\Employee;
use common\models\companyStructure\SalePoint;

/** @var $company Company */
/** @var $model SalePoint */
/** @var boolean $isSaved */
/** @var boolean $isUpdate */
/** @var Cashbox[] $newCashboxes */
/** @var Employee[] $newEmployers */

$questionIcon = [
    'companyName' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_companyName']),
    'name' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_name']),
    'store' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_store']),
    'city' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_city']),
    'address' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_address']),
    'cashbox' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_cashbox']),
    'employee' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_employee']),
    'site' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_site']),
    'online_payment_type' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_online_payment_type']),
];

$storeList = ['add-modal' => Icon::PLUS . 'Добавить склад'] + ArrayHelper::map($company->stores, 'id', 'name');
$cashboxList = ['add-modal' => Icon::PLUS . 'Добавить кассу'] + ArrayHelper::map($company->cashboxes, 'id', 'name');
$pointCashboxes = ($model->isNewRecord || $isUpdate) ? $newCashboxes : $model->cashboxes;
$pointEmployers = ArrayHelper::getColumn(($model->isNewRecord || $isUpdate) ? $newEmployers : $model->employers, 'id');
if (empty($pointCashboxes)) {
    $pointCashboxes = [new Cashbox(['company_id' => $company->id])];
}

$_excludeEmployers = SalePoint::find()
    ->joinWith('salePointEmployers')
    ->where(['sale_point.company_id' => $company->id])
    ->andFilterWhere(['not', ['sale_point_id' => $model->id]])
    ->select('sale_point_employee.employee_id')
    ->column();
$employeeArray = ['add-modal' => Icon::PLUS . 'Добавить сотрудника'] +
    ArrayHelper::map($model->company->getEmployeeCompanies()
        ->joinWith('employee')
        ->andWhere(['employee_company.is_working' => true])
        ->andWhere(['employee.is_deleted' => false])
        ->andFilterWhere(['not', ['employee.id' => $_excludeEmployers]])
        ->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])->all(), 'employee_id', 'fio');

$onlinePaymentTypesArray = \yii\helpers\ArrayHelper::map(
    \common\models\companyStructure\OnlinePaymentType::find()
        ->where(['or',
            ['company_id' => null],
            ['company_id' => $company->id]])
        ->orderBy('sort')
        ->all(), 'id', 'name');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$needShowFields = [
    'site' => [SalePoint::TYPE_INTERNET_SHOP],
    'online_payment_type' => [SalePoint::TYPE_INTERNET_SHOP],
    'employers' => [SalePoint::TYPE_DEPARTMENT, SalePoint::TYPE_FILIAL]
];
$needHideFields = [
    'city' => [SalePoint::TYPE_INTERNET_SHOP],
    'cashboxes' => [SalePoint::TYPE_INTERNET_SHOP],

];

?>

<h4 class="modal-title">
    <?= ($model->isNewRecord) ? 'Добавить' : 'Редактировать' ?> <?= $model->type->name ?>
</h4>

<?php

if (Yii::$app->request->isAjax && $isSaved) {

    echo Html::script('
        $(".modal.show").modal("hide");
        if ($("#sale-point-pjax-container").length) {
            window.toastr.success("'.htmlentities($model->name).($model->isNewRecord ? ' добавлен' : ' изменен').'");        
            $.pjax.reload({container: "#sale-point-pjax-container"});
        } else {
            location.href = location.href;
        }
        ',
        ['type' => 'text/javascript']);
}

$form = ActiveForm::begin([
    'id' => 'sale-point-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
        'data-pjax' => true,
    ],
]); ?>

<?= Html::activeHiddenInput($model, 'type_id') ?>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label class="label">
                Ваше юридическое лицо
                <?= $questionIcon['companyName'] ?>
            </label>
            <input disabled type="text" class="form-control" name="companyName" value="<?= htmlentities($model->company->getShortName()) ?>">
        </div>
    </div>
    <div class="col-6">
        
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Например: Магазин №1'])->label('Название магазина' . $questionIcon['name']) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'store_id', [
            'options' => [
                'class' => 'form-group'
            ]
        ])->widget(Select2::class, [
            'data' => $storeList,
            'hideSearch' => true,
            'options' => [
                'prompt' => 'Например: Склад магазина №1'
            ],
            'pluginOptions' => [
                'width' => '100%',
                'escapeMarkup' => new JsExpression('function(text) {return text;}')
            ]
        ])->label('Склад' . $questionIcon['store']); ?>
    </div>
</div>
<?php if (!in_array($model->type_id, $needHideFields['city'])): ?>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'city')->textInput(['maxlength' => true])->label('Город' . $questionIcon['city']) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label('Адрес' . $questionIcon['address']) ?>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <?php if (!in_array($model->type_id, $needHideFields['cashboxes'])): ?>
    <div class="col-6">
        <div class="form-group required">
            <label class="label"><?= 'Касса' . $questionIcon['cashbox'] ?></label>
            <div class="cashbox-list-modal">
                <?php for($i = 0; $i < SalePoint::MAX_CASHBOXES; $i++): ?>
                <?php $cashbox = $pointCashboxes[$i] ?? new Cashbox ?>
                    <div class="cashbox-item-modal mb-3" style="<?= $cashbox->company_id ? '' : 'display:none' ?>">
                        <?php echo Select2::widget([
                            'id' => 'salepoint-cashbox-' . ($i + 1),
                            'name' => 'SalePoint[cashboxes][]',
                            'data' => $cashboxList,
                            'value' => $cashbox->id,
                            'hideSearch' => true,
                            'options' => [
                                'class' => 'salepoint-cashbox_ids',
                                'prompt' => 'Например: Касса магазина №1'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'width' => '100%',
                                'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            ]
                        ]); ?>
                    </div>
                <?php endfor; ?>
            </div>
            <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']) . 'Касса', [
                'class' => 'add-cashbox-field button-regular button-hover-transparent button-clr button-width',
                'style' => (count($pointCashboxes) >= SalePoint::MAX_CASHBOXES) ? 'display:none' : '',
            ]); ?>
            <?php if ($cashboxErrorMessage = $model->getFirstError('cashboxes')): ?>
                <div class="invalid-feedback invalid-feedback-cashbox" style="display: block!important;"><?= $cashboxErrorMessage ?></div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php if (in_array($model->type_id, $needShowFields['employers'])): ?>
    <div class="col-6">
        <?php echo $form->field($model, 'employers')
            ->widget(Select2::class,[
                'data' => $employeeArray,
                'hideSearch' => true,
                'options' => [
                    'placeholder' => '',
                    'multiple' => true,
                    'value' => $pointEmployers,
                ],
                'pluginOptions' => [
                    'closeOnSelect' => false,
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}')
                ],
                'toggleAllSettings' => [
                    'selectLabel' => false,
                    'unselectLabel' => false,
                ]
            ])->label('Сотрудники' . $questionIcon['employee']); ?>
    </div>
    <?php endif; ?>

</div>

    <div class="row">

        <?php if (in_array($model->type_id, $needShowFields['site'])): ?>
            <div class="col-6">
                <?= $form->field($model, 'site')->textInput(['maxlength' => true])->label('Сайт' . $questionIcon['site']) ?>
            </div>
        <?php endif; ?>

        <?php if (in_array($model->type_id, $needShowFields['online_payment_type'])): ?>
            <div class="col-6">
                <?= $form->field($model, 'online_payment_type_id')->widget(Select2::class, [
                    'data' => $onlinePaymentTypesArray,
                    'hideSearch' => true,
                    'options' => [
                        'prompt' => ''
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                    ]
                ])->label('Прием платежей на сайте' . $questionIcon['online_payment_type']) ?>
            </div>
        <?php endif; ?>
    </div>

<div class="mt-3 pt-1 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-regular_red button-width button-clr ladda-button',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?= $this->render('_modal_sale_point_help', ['type_id' => $model->type_id]) ?>

<?php ActiveForm::end();