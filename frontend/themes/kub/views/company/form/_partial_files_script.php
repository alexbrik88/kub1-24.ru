<script>
    var tempCompanyImages = [];
    function tempImagesAdd(attr, img) {
        var needToAdd = true;
        $.each(tempCompanyImages, function(i,v) {
            if (v.attr == attr) {
                needToAdd = false;
                return false;
            }
        });
        if (needToAdd) {
            tempCompanyImages.push({'attr': attr, 'img': img});
        }
    }
    function tempImagesClear() {
        tempCompanyImages = [];
    }
    function tempImagesDeleteKUB() {
        $.each(tempCompanyImages, function(i,v) {
            jQuery.post('/company/img-delete', {'id': '<?= $model->id ?>', 'attr': v.attr}, function (data) {});
        });
        $('#modal-companyImages, #modal-chiefAccountantSignatureImage').modal('hide');
    }
    function tempImagesAcceptKUB() {
        $.each(tempCompanyImages, function(i,v) {
            $('#' + v.attr + '-image-upload').addClass('hidden');
            $('#' + v.attr + '-image-result td').html(v.img);
            $('#' + v.attr + '-image-result').removeClass('hidden');
            if ($('#'+v.attr+'-delete').length) {
                $('#'+v.attr+'-delete').prop('checked', false);
            }
        });
        $('#modal-companyImages, #modal-chiefAccountantSignatureImage').modal('hide');
    }

    function pjaxImgFormKUB(pjaxUrl, companyId, attr, action, apply) {
        apply = apply || 0;
        jQuery.pjax({
            url: pjaxUrl,
            data: {'id': companyId, 'attr': attr, 'action': action, 'apply': apply},
            container: '#' + attr + '-pjax-container',
            timeout: 5000,
            push: false,
            scrollTo: false
        });

        if (action == 'view') {
            $(document).on('pjax:complete', function () {
                var img = $('#' + attr + '-image-edit').find('.company-image-preview td').html();
                tempImagesAdd(attr, img);
            });
        }
    }
    function jcropImageDeleteKUB(pjaxUrl, formUrl, companyId, attr) {
        jQuery.post(pjaxUrl, {'id': companyId, 'attr': attr}, function (data) {
            pjaxImgFormKUB(formUrl, companyId, attr, 'view');
        });
    }
    function jcrop_ajaxRequestKUB(pjaxUrl, formUrl, companyId, attr, el) {
        var hasApply = el !== undefined;
        var tab = hasApply ? $(el).closest('.jcrop-image-container') : null;
        var apply = tab !== null ? $('input.apply-image-radio:checked', tab).val() : '0';
        var ajaxData = {};
        ajaxData[attr + '_x'] = $('#' + attr + '_x').val();
        ajaxData[attr + '_x2'] = $('#' + attr + '_x2').val();
        ajaxData[attr + '_y'] = $('#' + attr + '_y').val();
        ajaxData[attr + '_y2'] = $('#' + attr + '_y2').val();
        ajaxData[attr + '_h'] = $('#' + attr + '_h').val();
        ajaxData[attr + '_w'] = $('#' + attr + '_w').val();
        ajaxData['id'] = companyId;
        ajaxData['attr'] = attr;
        ajaxData['width'] = $('#' + attr).width();
        ajaxData['apply'] = apply;
        if ($('#company-tmpid').lenght) {
            ajaxData['tmpId'] = $('#company-tmpid').val();
        }
        jQuery.ajax({
            type: 'post',
            url: pjaxUrl,
            data: ajaxData,
            success: function (data) {
                pjaxImgFormKUB(formUrl, companyId, attr, 'view', apply);
            }
        });
    }

    $(document).on('click', '.del-company-image', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        var itemName;
        if (attr == 'logoImage')
            itemName = 'логотип';
        else if (attr == 'printImage')
            itemName = 'печать компании';
        else if (attr == 'chiefSignatureImage')
            itemName = 'подпись руководителя';
        else if (attr == 'chiefAccountantSignatureImage')
            itemName = 'подпись бухгалтера';
        else
            itemName = 'изображение';

        delModal.find('.modal-title > span').html(itemName);
        delModal.find('.del-company-image-ok').data('attr', attr);
        delModal.modal('show');

        return false;
    });

    $(document).on('click', '.del-company-image-ok', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        if (attr) {
            $('#' + attr + '-image-result').addClass('hidden');
            $('#' + attr + '-image-upload').removeClass('hidden');
            // form input
            if ($('#' + attr + '-delete').length) {
                $('#' + attr + '-delete').prop('checked', true);
            }
            $('#' + attr + '-modification-date').remove();
        }
        delModal.modal('hide');
    });

    $(document).on('hide.bs.modal', '#modal-companyImages, #modal-chiefAccountantSignatureImage', function() {
        $('.progress-bar').width(0);
        $('.progress-bar').parent().hide();
        tempImagesClear();
    });

    /* DRAG */
    function dragStart() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#e8eaf7', 'border-color': '#0048aa'});
        $(blocks).find('.dz-message').html('<span>Перетащите файл сюда.</span>');
    }
    function dragEnd() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#fff', 'border-color': '#ddd'});
        $(blocks).find('.dz-message').html('<span><span class="link">Выберите файлы</span><br> или перетащите сюда.</span>');
    }

    $(document).on("dragstart, dragover", function (e) {
        dragStart();
    });
    $(document).on("dragend, dragleave", function (e) {
        dragEnd();
    });
</script>