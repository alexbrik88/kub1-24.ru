<?php

use common\components\widgets\IfnsTypeahead;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $model common\models\Company */
/* @var $ifns common\models\Ifns */
/* @var $form \yii\bootstrap\ActiveForm */

?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-4">
                <?= $form->field($model, 'ifns_ga')->widget(IfnsTypeahead::className(), [
                    'kppSelector' => '#' . Html::getInputId($model, 'kpp'),
                    'related' => [
                        '#' . Html::getInputId($ifns, 'gb') => 'gb',
                        '#' . Html::getInputId($ifns, 'g1') => 'g1',
                        '#' . Html::getInputId($ifns, 'g2') => 'g2',
                        '#' . Html::getInputId($ifns, 'g4') => 'g4',
                        '#' . Html::getInputId($ifns, 'g6') => 'g6',
                        '#' . Html::getInputId($ifns, 'g7') => 'g7',
                        '#' . Html::getInputId($ifns, 'g8') => 'g8',
                        '#' . Html::getInputId($ifns, 'g9') => 'g9',
                        '#' . Html::getInputId($ifns, 'g11') => 'g11',
                    ],
                ])->textInput([
                    'placeholder' => 'Автозаполнение по Коду ИФНС' .
                        ($model->company_type_id != CompanyType::TYPE_IP ? ' (Первые четыре цифры вашего КПП)' : ''),
                ])->label('Код ИФНС'); ?>
            </div>
            <div class="col-4">
                <?= $form->field($ifns, 'g6')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-4">
                <?= $form->field($ifns, 'g7')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <?= $form->field($ifns, 'gb')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <?= $form->field($ifns, 'g4')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-4">
                <?= $form->field($ifns, 'g9')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-4">
                <?= $form->field($ifns, 'g8')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-4">
                <?= $form->field($ifns, 'g11')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-8">
                <?= $form->field($ifns, 'g1')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-4">
                <?= $form->field($ifns, 'g2')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <br>
    </div>
</div>