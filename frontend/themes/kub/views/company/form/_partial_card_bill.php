<?php

use common\components\grid\GridView;
use common\modules\cards\models\CardBill;
use common\modules\cards\models\CardBillRepository;
use frontend\modules\cards\widgets\ZenmoneyButtonWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\themes\kub\components\Icon;

/* @var $model common\models\Company */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CardBillRepository */

$searchModel = new CardBillRepository([
    'company' => $model
]);
$dataProvider = $searchModel->getDataProvider();
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'card-bill-pjax-container',
                'enablePushState' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                    ],
                    [
                        'attribute' => 'currency_name',
                        'label' => 'Валюта',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                    ],
                    [
                        'attribute' => 'account_id',
                        'label' => 'Аккаунт',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'value' => 'account.fullName',
                    ],
                    [
                        'attribute' => 'is_accounting',
                        'label' => 'Для учета в бухгалтерии',
                        'format' => 'boolean',
                    ],
                    [
                        'class' => \yii\grid\ActionColumn::className(),
                        'template' => '{update}{delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, CardBill $model) {
                                return Html::a(Icon::get('pencil'), $url, [
                                    'class' => 'link ajax-modal-btn',
                                    'data-pjax' => '0',
                                    'data-title' => 'Изменить Карту',
                                    'title' => Yii::t('yii', 'Изменить'),
                                    'aria-label' => Yii::t('yii', 'Изменить'),
                                ]);
                            },
                            'delete' => function ($url, CardBill $model) {
                                if ($model->hasMovement()) {
                                    return Html::tag('span', Icon::get('garbage'), [
                                        'class' => 'ml-1 grey-link disabled-link',
                                        'title' => 'Карту нельзя удалить, т.к. есть операции по карте.<br/>
                                            Удалите операции, после можно удалить карту',
                                        'title-as-html' => 1
                                    ]);
                                } else {
                                    return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => Icon::get('garbage'),
                                            'class' => 'ml-1 link',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'classForJquery' => 'delete-card-bill',
                                        'message' => 'Вы уверены, что хотите удалить карту?',
                                    ]);
                                }
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = '/cards/bill/update';
                                    break;
                                case 'delete':
                                    $url = '/cards/bill/delete';
                                    break;
                            }

                            return Url::to([$url, 'id' => $model->id]);
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>

            <div class="form-group">

                <?= ZenmoneyButtonWidget::widget([
                    'cssClass' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                    'content' => $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить карту</span>',
                    'hasImport' => false,
                    'hasBlock' => false,
                ]) ?>

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
$(document).on('submit', '#card-form', function(e){
    var ajaxModal = $('#ajax-modal-box');
    if (ajaxModal && $(this, ajaxModal).length) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#ajax-modal-content', ajaxModal).html(data);
        });
    }
});
JS
);