<?php

use common\models\company\CompanyType;
use yii\helpers\Json;

$house = \common\models\address\AddressHouseType::TYPE_HOUSE;
$flat = \common\models\address\AddressApartmentType::TYPE_APARTMENT;

$typeOOO = CompanyType::TYPE_OOO;
$typeZAO = CompanyType::TYPE_ZAO;
$typePAO = CompanyType::TYPE_PAO;
$typeOAO = CompanyType::TYPE_OAO;
$typeIp = CompanyType::TYPE_IP;
$likeIp = Json::encode(CompanyType::find()->select('name_short')->where(['like_ip' => 1])->column());

$this->registerJs(<<<JS

var companyType = {
    'ООО' : $typeOOO,
    'ЗАО' : $typeZAO,
    'ПАО' : $typePAO,
    'ОАО' : $typeOAO,
    'ИП': $typeIp
};
var likeIp = {$likeIp};
var body = $('.form-body');
$('#company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            let form = this.form;
            $('#company-inn', form).val(suggestion.data.inn);
            if (suggestion.data.name !== null) {
                $('#company-name_short', form).val(suggestion.data.name.short);
                $('#company-name_full', form).val(suggestion.data.name.full);
            }
            if (suggestion.data.address) {
                if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                    var address = '';
                    if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                        address += suggestion.data.address.data.postal_code + ', ';
                    }
                    address += suggestion.data.address.value;
                    $('#company-address_legal', form).val(address);
                    $('#company-address_actual', form).val(address);
                } else {
                    $('#company-address_legal', form).val('000000, ' + suggestion.data.address.value);
                    $('#company-address_actual', form).val('000000, ' + suggestion.data.address.value);
                }
            } else {
                $('#company-address_legal', form).val('');
                $('#company-address_actual', form).val('');
            }

            if (suggestion.data.address && suggestion.data.address.data !== null) {
                $('#company-oktmo', form).val(suggestion.data.address.data.oktmo);
            }

            $('#company-okpo', form).val(suggestion.data.okpo);
            $('#company-okved', form).val(suggestion.data.okved);

            if (likeIp.includes(suggestion.data.opf.short)) {
                var nameArray = suggestion.data.name.full.split(' ');
                $('#company-ip_lastname', form).val(nameArray[0] || '');
                $('#company-ip_firstname', form).val(nameArray[1] || '');
                $('#company-ip_patronymic', form).val(nameArray[2] || '');
                $('#company-egrip', form).val(suggestion.data.ogrn);
            } else {
                $('#company-company_type_id', form).val(companyType[suggestion.data.opf.short]);
                $('#company-kpp', form).val(suggestion.data.kpp).trigger('change');
                $('#company-ogrn', form).val(suggestion.data.ogrn);
                if (suggestion.data.management) $('#company-chief_post_name', form).val(suggestion.data.management.post);
                var nameArray = suggestion.data.management.name.split(' ');
                $('#company-chief_lastname', form).val(nameArray[0] || '');
                $('#company-chief_firstname', form).val(nameArray[1] || '');
                $('#company-chief_patronymic', form).val(nameArray[2] || '');
            }
            if (typeof isTaxRobot !== "undefined") {
                $.each(['ip_lastname', 'ip_firstname', 'ip_patronymic', 'address_legal', 'egrip', 'okved'], function(i,attr) {
                    $('#company-' + attr, form).addClass('edited').parent().removeClass('has-error');
                });
                if (suggestion.data.address && suggestion.data.address.data !== null) {
                   $('#company-oktmo', form).val(suggestion.data.address.data.oktmo).addClass('edited').parent().removeClass('has-error').addClass('has-success');
                   $('#company-ifns_ga', form).val(suggestion.data.address.data.tax_office_legal).addClass('edited').parent().removeClass('has-error').addClass('has-success');
                } else {
                    $('#company-oktmo', form).val('');
                    $('#company-ifns_ga', form).val('');
                }
                if (typeof suggestion.data.state !== "undefined") {
                    var registration_date = suggestion.data.state.registration_date;
                    if (registration_date) {
                        $('#company-taxregistrationdate', form).val(moment(registration_date).format("DD.MM.YYYY")).addClass('edited').parent().removeClass('has-error');
                    }
                } else {
                    $('#company-taxregistrationdate', form).val('');
                }

            }
        }
    });

JS
);