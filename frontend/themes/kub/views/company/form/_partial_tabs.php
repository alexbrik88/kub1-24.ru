<?php

use yii\bootstrap4\Tabs;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $foreignCurrencyAccountDataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\company\CheckingAccountantSearch */
/* @var $foreignCurrencyAccountSearch common\models\company\CheckingAccountantSearch */
/* @var $banks backend\models\Bank[] */

$tab = Yii::$app->request->get('tab');
?>

<div class="wrap pt-2 pl-4 pr-4 pb-0">
    <div class="pt-1 pl-1 pr-2">
        <?= Tabs::widget([
            'id' => 'add_tabs',
            'options' => [
                'class' => 'nav-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
            ],
            'items' => [
                [
                    'label' => 'Банковские счета',
                    'content' => $this->render('_partial_checking_account', [
                        'model' => $model,
                        'form' => $form,
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'banks' => $banks,
                    ]),
                    'active' => !$tab,
                ],
                /*[
                    'label' => 'Валютные счета',
                    'content' => $this->render('_partial_foreign_currency_account', [
                        'form' => $form,
                        'searchModel' => $foreignCurrencyAccountSearch,
                        'dataProvider' => $foreignCurrencyAccountDataProvider,
                        'company' => $model,
                    ]),
                ],*/
                [
                    'label' => 'Кассы',
                    'content' => $this->render('_partial_cashbox', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'Карты',
                    'content' => $this->render('_partial_card_bill', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'E-money',
                    'content' => $this->render('_partial_emoney', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'Склады',
                    'content' => $this->render('_partial_store', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ],
                [
                    'label' => 'Точки продаж',
                    'content' => $this->render('_partial_company_structure', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $tab === 'sale_point',
                ],
                [
                    'label' => 'Направления',
                    'content' => $this->render('_partial_company_industry', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $tab === 'industry',
                ],
            ],
        ]); ?>
    </div>
</div>

<?php if ($tab): ?>
    <script>
        $('html, body').animate({scrollTop: $('#add_tabs').offset().top - 145}, 1000);
    </script>
<?php endif; ?>
