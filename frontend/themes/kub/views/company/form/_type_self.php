<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $creating boolean */

$templateDateInput = "{label}<div class='date-picker-wrap' style=''>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-address_legal_city, #company-address_legal_street", function () {
            var city = $.trim($("#company-address_legal_city").val());
            var street = $.trim($("#company-address_legal_street").val());
            if (!empty(city) && !empty(street)) {
                $.post("/company/ifns", {"city": city, "street": street}, function(data) {
                    if (!empty(data.oktmo)) {
                        $("#company-oktmo").val(data.oktmo);
                    }
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>

<div class="wrap pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'inn')->textInput([
                    'id' => 'self_employed-inn',
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>

        <h4 class="mb-3" style="padding-bottom: 3px">Самозанятый</h4>

        <div class="row align-items-center">
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
            </div>
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
            </div>
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
            </div>
            <div class="col-3 mb-3" style="margin-top:10px;">
                <div class="form-filter">
                    <?= Html::activeCheckbox($model, 'has_chief_patronymic', [
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <?= Tabs::widget([
            'options' => [
                'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
            ],
            'items' => [
                [   'label' => 'Общие',
                    'encode' => false,
                    'content' => $this->render('tab_ooo/_partial_general', [
                        'form' => $form,
                        'model' => $model,
                        'creating' => $creating,
                    ]),
                    'active' => true,
                ],
                [   'label' => 'Настройки',
                    'encode' => false,
                    'content' => $this->render('tab_ooo/_partial_setting', [
                        'form' => $form,
                        'model' => $model,
                        'creating' => $creating,
                    ]),
                ],
            ],
        ]); ?>
    </div>
</div>

<?= $this->render('_partial_tabs', [
    'model' => $model,
    'form' => $form,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'banks' => $banks,
]) ?>

<div class="wrap pt-2 pl-4 pr-4 pb-0">
    <div class="pt-1 pl-1 pr-2">
        <?= Tabs::widget([
            'options' => [
                'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
            ],
            'items' => [
                [
                    'label' => 'Реквизиты',
                    'encode' => false,
                    'content' => $this->render('_self_requisites', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                    'active' => true,
                ],
                [
                    'label' => 'Реквизиты на английском',
                    'encode' => false,
                    'content' => $this->render('_requisites_en', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                ],
                [
                    'label' => 'Реквизиты вашей налоговой',
                    'content' => $this->render('_partial_ifns', [
                        'form' => $form,
                        'model' => $model,
                        'ifns' => $ifns
                    ]),
                ],
            ],
        ]); ?>
    </div>
</div>

<?= $this->render('_partial_files', [
    'model' => $model,
]); ?>
