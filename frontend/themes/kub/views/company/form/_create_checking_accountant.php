<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 19:09
 */

use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model CheckingAccountant */

?>
<div class="bank-create">
    <div class="portlet box">
        <h4 class="page-title">Добавить расчетный счет</h4>
    </div>

    <?= $this->render('_partial/_formz', [
        'model' => $model,
    ]) ?>
</div>