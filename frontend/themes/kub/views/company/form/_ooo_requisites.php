<?php

use common\components\date\DateHelper;
use common\components\TextHelper;

/**
 * @var $form
 * @var $model
 * @var $templateDateInput
 */
?>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ogrn')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'kpp')->textInput([
                    'maxlength' => true,
                    'ifns-exist' => $model->getIfns()->exists() ? 'true' : 'false',
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okpo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okato')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_legal')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_actual')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'fss')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okud')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okved')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'oktmo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okogu')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okfs')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okopf')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker'
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'capital')->textInput([
                    'maxlength' => true,
                    'value' => TextHelper::invoiceMoneyFormat($model->capital, 2, '.', ''),
                ]); ?>
            </div>
        </div>
        <br>

    </div>
</div>
