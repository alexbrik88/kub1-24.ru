<?php
use common\components\helpers\Html;
use common\models\Company;
use frontend\components\Icon;
?>
<div class="row align-items-top mt-2">
    <div class="col-4">
        <label class="label">
            Формировать PDF с <?= $model->self_employed ? '' : 'печатью и'; ?> подписью
        </label>
        <div class="form-filter">
            <?= Html::activeCheckbox($model, 'pdf_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
    <div class="col-4">
        <label class="label">
            Отправлять с <?= $model->self_employed ? '' : 'печатью и'; ?> подписью
        </label>
        <div class="form-filter">
            <?= Html::activeCheckbox($model, 'pdf_send_signed', [
                'labelOptions' => ['style' => 'width: 110px;'],
                'disabled' => true,
            ]) ?>
            <?= Html::activeCheckbox($model, 'pdf_act_send_signed', ['labelOptions' => ['style' => 'width: 110px;']]) ?>
        </div>
    </div>
    <div class="col-4">
        <?= $form->field($model, 'is_additional_number_before')->radioList(Company::$addNumPositions, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                    //'class' => 'label',
                    'style' => 'margin-right: 20px;',
                ]);
            },
        ]) ?>
    </div>
</div>
<div class="row align-items-top" style="margin-bottom: 15px;">
    <div class="col-4">
        <label class="label">
            Вывести кнопку ТТН в счете?
        </label>
        <div class="form-filter">
            <?= Html::activeCheckbox($model, 'show_waybill_button_in_invoice', [
                'label' => 'Да',
                'labelOptions' => [
                    'style' => 'width: 110px;',
                ],
            ]) ?>
        </div>
    </div>
    <div class="col-4">
        <?= $form->field($model, 'invoice_facture_print_template')->radioList(['1' => 'Вариант 1', '2' => 'Вариант 2'], [
            'class' => 'inp_one_line_company',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, [
                            'value' => $value,
                        ]) . $label, [
                        'class' => 'label',
                        'style' => 'margin-right: 10px;',
                    ]) . 'Пример ' . Html::a('СФ', [
                        '/company/view-example-invoice-facture',
                        'ver' => $value,
                    ], [
                        'class' => 'link',
                        'target' => '_blank',
                    ]) . ' и ' . Html::a('УПД', [
                        '/company/view-example-upd',
                        'ver' => $value,
                    ], [
                        'class' => 'link',
                        'target' => '_blank',
                        'style' => 'margin-right: 20px;',
                    ]);
            },
        ])->label('Шаблон СФ и УПД') ?>
    </div>
    <div class="col-4">
        <div class="form-group">
            <label class="label">
                Отсрочка платежа
                <?= Icon::get('question', [
                    'class' => 'tooltip-question-icon ml-2',
                    'title' => 'Для всех новых контрагентов будут устанавливаться данные отсрочки платежей. '.
                        'В карточке контрагента можно поставить индивидуальную отсрочку.',
                ]) ?>
            </label>
            <div class="d-flex">
                <div class="mr-2" style="width: 50px;">
                    <?= $form->field($model, 'customer_payment_delay', [
                        'options' => ['class' => '']
                    ])->label(false)->textInput([
                        'type' => 'number',
                        'class' => 'w-100 px-1',
                        'style' => 'border: solid 2px #e2e5eb; border-radius: 3px;',
                    ]) ?>
                </div>
                <div>
                    Для покупателей
                </div>
            </div>
            <div class="d-flex">
                <div class="mr-2" style="width: 50px;">
                    <?= $form->field($model, 'seller_payment_delay', [
                        'options' => ['class' => '']
                    ])->label(false)->textInput([
                        'type' => 'number',
                        'class' => 'w-100 px-1',
                        'style' => 'border: solid 2px #e2e5eb; border-radius: 3px;',
                    ]) ?>
                </div>
                <div>
                    Для поставщиков
                </div>
            </div>
        </div>
    </div>
</div>

