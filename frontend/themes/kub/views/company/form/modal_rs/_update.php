<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:16
 */

use common\models\company\CheckingAccountant;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $company Company */

echo $this->render('_partial/_modal_form', [
    'checkingAccountant' => $checkingAccountant,
    'title' => 'Редактировать расчетный счет',
    'id' => 'update-company-rs-' . $checkingAccountant->id,
    'company' => $company,
    'actionUrl' => $actionUrl ?? null
]);
