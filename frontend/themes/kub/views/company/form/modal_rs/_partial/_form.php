<?php

use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use frontend\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company Company|null */

$model = $checkingAccountant;

$createAction = ($actionUrl) ?? 'create-checking-accountant';
$updateAction = ($actionUrl) ?? 'update-checking-accountant';

$actionUrl = $model->isNewRecord ? Url::to([
    $createAction,
    'companyId' => $company ? $company->id : null
]) : Url::to([
    $updateAction,
    'id' => $model->id,
    'companyId' => $company ? $company->id : null
]);

$currency = Currency::find()->select(['name', 'id'])->indexBy('id')->column();
if (empty($model->currency_id)) {
    $model->currency_id = Currency::DEFAULT_ID;
}

$isRuble = $model->currency_id == Currency::DEFAULT_ID;
$isForeign = $model->is_foreign_bank;

$aditionaDatalLabel = '<span class="link-txt">Указать данные необходимые для выставления инвойсов</span>'."\n";
$aditionaDatalLabel .= Icon::get('shevron', ['class' => 'link-shevron']);

$helpStartBalance = Icon::get('attention', [
    'class' => 'label-help text-red ml-1 foreign_hidden'.($isForeign ? ' hidden' : ''),
    'style' => 'font-size:24px',
    'title' => 'При загрузке выписки начальный остаток проставится автоматически',
]);

$currencyTag = Html::tag('span', $currency[$model->currency_id] ?? '', [
    'class' => 'currency_name_tag',
]);
$isMainLabel = 'Основной'.Html::tag('span', ' для '.$currencyTag, [
    'class' => 'ruble_hidden'.($isRuble ? ' hidden' : ''),
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.label-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

?>

<?php $form = ActiveForm::begin([
    'action' => $actionUrl,
    'options' => [
        'class' => 'form-checking-accountant',
        'id' => 'form-checking-accountant-' . $model->id,
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data-ruble' => (int) $isRuble,
        'data-foreign' => (int) $isForeign,
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="form-body">
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'rs')->textInput(); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
                'data' => $currency,
                'options' => [
                    'data-currency' => $currency,
                    'class' => 'select-accountant-currency_id',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'minimumResultsForSearch' => 10,
                ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <label class="label">&nbsp;</label>
            <?= $form->field($model, 'is_foreign_bank')->checkbox([
                'class' => '',
                'disabled' => !empty($model->bik) ? true : null,
            ], true)->label(null, [
                'class' => 'label pt-3',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= Html::activeHiddenInput($model, 'bik', ['value' => '', 'id' => 'bik_hidden']) ?>
            <?= $form->field($model, 'bik')
                ->widget(BikTypeahead::classname(), [
                    'remoteUrl' => Url::to(['/dictionary/bik']),
                    'related' => [
                        '#' . Html::getInputId($model, 'bank_name') => 'name',
                        '#' . Html::getInputId($model, 'bank_city') => 'city',
                        '#' . Html::getInputId($model, 'ks') => 'ks',
                    ],
                ])->textInput([
                    'autocomplete' => 'off',
                    'class' => 'form-control foreign_disabled',
                    'disabled' => $isForeign ? true : null,
                ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'bank_name')->textInput([
                'class' => 'form-control russian_disabled',
                'disabled' => $isForeign ? null : true,
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'ks')->textInput([
                'disabled' => true,
            ]); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'bank_city')->textInput([
                'disabled' => true,
            ]); ?>
        </div>
    </div>

    <div class="row">
        <?php if ($model->isNewRecord) : ?>
            <div class="col-6">
                <?= $form->field($model, 'createStartBalance')->checkbox([
                    'class' => 'account_start_balance_checkbox',
                ])->label('Указать начальный остаток '.$helpStartBalance, [
                    'class' => 'label',
                ]) ?>
            </div>
        <?php endif ?>
        <div class="col-6">
            <?php if ($model->type !== CheckingAccountant::TYPE_MAIN) : ?>
                <?= $form->field($model, 'isMain')->checkbox([
                    'class' => '',
                ])->label($isMainLabel, [
                    'class' => 'label',
                ]); ?>

                <?php if (!$model->isNewRecord && count($company->checkingAccountants) !== 1) : ?>
                    <?= $form->field($model, 'isClosed')->checkbox([
                        'class' => '',
                    ])->label(null, [
                        'class' => 'label',
                    ]); ?>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>

    <?php if ($model->isNewRecord) : ?>
        <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : ''; ?>">
            <div class="col-6">
                <?= $form->field($model, 'startBalanceAmount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'startBalanceDate')->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ])->label('на дату') ?>
            </div>
        </div>
        <?php if (Yii::$app->request->isAjax) : ?>
            <script type="text/javascript">
                refreshDatepicker();
            </script>
        <?php endif ?>
    <?php endif ?>

    <div class="foreign_currency_show <?= $isRuble && !$isForeign ? 'hidden' : ''; ?>">
        <?= Html::button($aditionaDatalLabel, [
            'class' => 'link link_collapse link_bold button-clr mb-4 collapsed',
            'data-toggle' => 'collapse',
            'data-target' => '#accountDopColumns',
            'aria-expanded' => 'false',
            'aria-controls' => 'accountDopColumns',
        ]) ?>

        <div id="accountDopColumns" class="collapse">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'swift')
                        ->textInput(['placeholder' => 'Пример: ALFARUMM']); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'bank_name_en')
                        ->textInput(['placeholder' => 'Пример: ALFA-BANK']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'bank_address_en')
                        ->textInput(['placeholder' => 'Пример: 27 Kalanchevskaya str., Moscow, 107078']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 left-column">
                    <?= $form->field($model, 'corr_rs')
                        ->textInput(['placeholder' => 'Пример: 36310481']); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'iban', [
                        'enableClientValidation' => false,
                    ])->textInput(); ?>
                </div>
            <div class="row">
            </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'corr_swift')
                        ->textInput(['placeholder' => 'Пример: CITIUS33']); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'corr_bank_name')
                        ->textInput(['placeholder' => 'Пример: CITIBANK NA']); ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'corr_bank_address')
                        ->textInput(['placeholder' => 'Пример: 399 Park Avenue, New York, NY 10043, USA']); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-width button-regular button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-clr button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>
