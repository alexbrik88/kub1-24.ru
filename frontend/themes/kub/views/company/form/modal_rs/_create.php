<?php

use common\models\company\CheckingAccountant;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $company Company|null */

if (!isset($company)) {
    $company = null;
}

echo $this->render('_partial/_modal_form', [
    'checkingAccountant' => $checkingAccountant,
    'title' => 'Добавить банковский счет',
    'id' => 'add-company-rs',
    'company' => $company,
    'actionUrl' => $actionUrl ?? null
]);
