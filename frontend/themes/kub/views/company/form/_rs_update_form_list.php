<?php foreach ($model->checkingAccountants as $checkingAccountant) {
    echo $this->render('modal_rs/_update', [
        'checkingAccountant' => $checkingAccountant,
        'company' => $model,
        'actionUrl' => $actionUrl ?? null
    ]);
}; ?>