<?php

use common\components\grid\GridView;
use common\components\ImageHelper;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use common\models\company\ForeignCurrencyAccount;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'fca-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'swift',
                        'label' => 'SWIFT',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->swift;
                        },
                    ],
                    [
                        'attribute' => 'currency_id',
                        'label' => 'Валюта',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->currency->name;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Счет',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return ArrayHelper::getValue($model->typeAccount, $model->type, '');
                        },
                    ],
                    [
                        'class' => \yii\grid\ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, ForeignCurrencyAccount $data) {
                                return Html::a(Icon::get('pencil'), '#update-foreign-account-' . $data->id, [
                                    'class' => 'link',
                                    'data-toggle' => 'modal',
                                    'title' => Yii::t('yii', 'Редактировать'),
                                    'aria-label' => Yii::t('yii', 'Редактировать'),
                                ]);
                            },
                            'delete' => function ($url, ForeignCurrencyAccount $data) {
                                if ($data->getIsDeleteAllowed()) {
                                    return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => Icon::get('garbage'),
                                            'class' => 'link',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'classForJquery' => 'delete-foreign-currency-account',
                                        'message' => 'Вы уверены, что хотите удалить валютный счет?',
                                    ]);
                                }

                                return '';
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = 'update';
                                    break;
                                case 'delete':
                                    $url = 'delete-foreign-currency-account';
                                    break;
                            }

                            return Url::to([$url, 'id' => $model->id]);
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
            <div class="form-group">
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить валютный счет</span>', [
                    'class' => 'button-regular button-regular_red',
                    'data-toggle' => 'modal',
                    'data-target'=> '#add-foreign-account',
                    'href' => '#add-foreign-account',
                ]); ?>
            </div>
        </div>
    </div>
</div>
