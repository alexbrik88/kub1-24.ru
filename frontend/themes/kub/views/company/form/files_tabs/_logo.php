<?php

/* @var $model common\models\Company */
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\models\Company;
use devgroup\dropzone\DropZone;
use newerton\jcrop\jCrop;
use newerton\jcrop\jCropAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

jCropAsset::register($this);
?>
<div class="portlet box darkblue">
    <div class="portlet-body">
        <?php $imgPath = $model->getImagePath($model->logo_link);
        if (is_file($imgPath)) : ?>
            <?php if (empty($crop)) : ?>
                <div class="company_img">
                    <div class="portlet align-center" id="company_logo">
                        <?= ImageHelper::getThumb(
                            $imgPath,
                            ImageHelper::THUMB_MEDIUM,
                            [
                                'class' => 'signature_image',
                            ]
                        ); ?>
                    </div>
                </div>
            <?php else : ?>
                <?php
                ?>
                <div class="row">
                    <div class="col-sm-12 text-grey"  style="padding-bottom: 15px;">
                        Выбранная область с логотипом будет выводиться в счете
                    </div>
                    <div class="col-md-8" style="position: relative; padding-bottom: 20px;">
                        <?= Html::img(['/company/file', 'id' => $model->id, 'attr' => 'logoImage'], [
                            'id' => 'logoImage',
                            'alt' => '',
                            'style' => 'max-width: 100%; max-height: 600px;',
                        ]) ?>
                    </div>
                    <div class="col-md-4" style="position: relative; padding-bottom: 20px;">
                        <div id="preview-pane">
                            <div class="preview-container" style="width: 200px; height: 70px; overflow: hidden; margin-bottom: 10px;">
                                <?= Html::img(['/company/file', 'id' => $model->id, 'attr' => 'logoImage'], [
                                    'alt' => 'Preview',
                                    'class' => 'jcrop-preview',
                                ]) ?>
                            </div>
                            <div class="text-grey pt-3" style="width: 200px;">
                                Так будет выглядеть логотип на ваших документах
                            </div>
                        </div>
                    </div>
                </div>

                <style type="text/css">
                    .jcrop-holder #preview-pane {
                        /*display: block;
                        position: absolute;
                        z-index: 2000;
                        top: 10px;
                        right: -280px;*/
                        padding: 6px;
                        border: 1px rgba(0,0,0,.4) solid;
                        background-color: white;
                    }
                </style>

                <script type="text/javascript">
                    jQuery(function($){
                        // Create variables (in this scope) to hold the API and image size
                        var jcrop_api,
                        boundx,
                        boundy,
                        // Grab some information about the preview pane
                        $preview = $('#preview-pane'),
                        $pcnt = $('#preview-pane .preview-container'),
                        $pimg = $('#preview-pane .preview-container img'),
                        xsize = $pcnt.width(),
                        ysize = $pcnt.height();

                        $('#logoImage').Jcrop({
                            onChange: updatePreview,
                            onSelect: updatePreview,
                            //aspectRatio: xsize / ysize,
                            minSize: [xsize, ysize],
                        },function(){
                            // Use the API to get the real image size
                            var bounds = this.getBounds();
                            boundx = bounds[0];
                            boundy = bounds[1];
                            // Store the API in the jcrop_api variable
                            jcrop_api = this;
                            // Move the preview into the jcrop container for css positioning
                            //$preview.appendTo(jcrop_api.ui.holder);
                        });
                        function updatePreview(c) {
                            if (parseInt(c.w) > 0) {
                                var rx = xsize / c.w;
                                var ry = ysize / c.h;
                                $pimg.css({
                                    width: Math.round(rx * boundx) + 'px',
                                    height: Math.round(ry * boundy) + 'px',
                                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                                });
                            }
                        };
                    });
                </script>
            <?php endif ?>
        <?php else : ?>
            <?= DropZone::widget([
                'model' => $model,
                'attribute' => 'logoImage',
                'url' => Url::toRoute(['/company/upload', 'id' => $model->id, 'attr' => 'logoImage']), // upload url
                'storedFiles' => [], // stores files
                'eventHandlers' => [], // dropzone event handlers
                'sortable' => true, // sortable flag
                'sortableOptions' => [], // sortable options
                'htmlOptions' => [], // container html options
                'options' => [
                    'dictDefaultMessage' => 'Перетащите файл изображения сюда или кликните тут, чтобы выбрать его с вашего компьютера',
                    'uploadMultiple' => false,
                ],
            ]) ?>
        <?php endif; ?>
    </div>
</div>