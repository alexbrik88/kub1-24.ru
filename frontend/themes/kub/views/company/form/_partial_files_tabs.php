<?php

use common\assets\JCropAsset;
use common\models\Company;
use devgroup\dropzone\DropZoneAsset;
use yii\bootstrap4\Tabs;

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
JCropAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

if (empty($action)) {
    $action = 'view';
}
$items = [];
foreach (Company::$imageDataArray as $key => $value) {
    if (($key == 'printImage' && $model->self_employed) || $key == 'chiefAccountantSignatureImage') {
        continue;
    }
    $items[] = [
        'label' => $value['label'],
        'content' => $this->render('_partial_files_tabs_item', [
            'model' => $model,
            'attr' => $key,
            'action' => $action,
        ]),
        'active' => $key === 'logoImage',
    ];
}
?>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'id' => 'company-image-tabs',
        'options' => [
            'class' => 'nav-tabs_indents_alternative w-100 mr-3 row mb-3',
        ],
        'headerOptions' => ['class' => 'col-4'],
        'linkOptions' => ['class' => 'text-center'],
        'items' => $items,
    ]); ?>
</div>

<?= $this->render('//company/form/_partial_files_script', [
    'model' => $model,
]) ?>
