<?php

use common\components\date\DateHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <h4 class="mb-3 pb-3">Реквизиты</h4>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ИНН</div>
                <div><?= $model->inn; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОГРНИП</div>
                <div><?= $model->egrip; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКВЭД</div>
                <div><?= $model->okved; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКПО</div>
                <div><?= $model->okpo; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКТМО</div>
                <div><?= $model->oktmo; ?>&nbsp;</div>
            </div>
        </div>
        <div class="pb-3 mb-3">
            <div class="label weight-700 mb-3">Юридический адрес</div>
            <div><?= $model->getAddressLegalFull(); ?>&nbsp;</div>
        </div>
        <div class="pb-3 mb-3">
            <div class="label weight-700 mb-3">Фактический адрес</div>
            <div><?= $model->getAddressActualFull(); ?>&nbsp;</div>
        </div>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ФСС</div>
                <div><?= $model->fss; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ПФР (как ИП)</div>
                <div><?= $model->pfr_ip; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ПФР (как работодателя)</div>
                <div><?= $model->pfr_employer; ?>&nbsp;</div>
            </div>
            <div class="column pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">Дата постановки на учёт в налоговом органе</div>
                <div>
                    <?= DateHelper::format(
                        $model->tax_authority_registration_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    ); ?>
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>