<?php

use common\models\Company;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

$dataArray = Company::$imageDataArray;
$tmp = in_array(parse_url(Yii::$app->request->referrer, PHP_URL_PATH), [
    '/company/update',
    '/company/continue-create',
]);
$imgUrl = Url::toRoute(['/company/img-file', 'id' => $model->id, 'attr' => $attr, 'v' => time(), 'tmp' => $tmp ? 1: null]);
$width = $dataArray[$attr]['width'];
$height = $dataArray[$attr]['height'];
$ratio = $height / $width * 100;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'side' => 'right',
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="jcrop-image-container">
    <div class="mt-3 mb-2">Выбранная область с подписью будет выводиться в документах</div>
    <table class="jcrop-cropper-table" style="width: 100%; vertical-align: top;">
        <tr>
            <td style="width: 70%; padding-right: 10px;">
                <div id="<?= $attr ?>-image-pane" class="jcrop-image-pane">
                    <?php
                    echo Html::hiddenInput($attr . '_x', 0, ['class' => 'coords', 'id' => $attr . '_x']);
                    echo Html::hiddenInput($attr . '_y', 0, ['class' => 'coords', 'id' => $attr . '_y']);
                    echo Html::hiddenInput($attr . '_w', 0, ['class' => 'coords', 'id' => $attr . '_w']);
                    echo Html::hiddenInput($attr . '_h', 0, ['class' => 'coords', 'id' => $attr . '_h']);
                    echo Html::hiddenInput($attr . '_x2', 0, ['class' => 'coords', 'id' => $attr . '_x2']);
                    echo Html::hiddenInput($attr . '_y2', 0, ['class' => 'coords', 'id' => $attr . '_y2']);
                    echo Html::img($imgUrl, [
                        'id' => $attr,
                        'alt' => 'Crop this image',
                        'style' => 'width: 100%; height: auto;'
                    ]);
                    ?>
                </div>
            </td>
            <td style="width: 30%;">
                <div class="jcrop-preview-pane">
                    <div id="<?= $attr ?>-preview-wrapper" class="jcrop-preview-wrapper">
                        <div id="<?= $attr ?>-preview-container" class="jcrop-preview-container" style="padding-top: <?= $ratio ?>%;">
                            <div id="<?= $attr ?>-preview-inner" class="jcrop-preview-inner">
                                <img src="<?= $imgUrl ?>" class="jcrop-preview" alt="Preview" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jcrop-preview-text mt-1"><?= $dataArray[$attr]['preview_text'] ?></div>
                <?php if ($attr == 'chiefSignatureImage' || $attr == 'printImage'): ?>
                    <div class="jcrop-preview-scan-quality mt-3 p-2" style="border: 2px solid #e7eafa">
                        <div class="mb-2">
                            Какой скан <?=($attr == 'chiefSignatureImage') ? 'подписи' : 'печати' ?> нужен:
                        </div>
                        <div class="tooltip3 mb-1" data-tooltip-content="#tooltip3_photo_2">
                            <span style="color:red"><?= $this->render('//svg-sprite', ['ico' => 'close']) ?></span>
                            <span class="pl-1">Плохое фото</span>
                        </div>
                        <div class="tooltip3 mb-1" data-tooltip-content="#tooltip3_photo_1">
                            <span style="color:red"><?= $this->render('//svg-sprite', ['ico' => 'close']) ?></span>
                            <span class="pl-1">Плохой скан</span>
                        </div>
                        <div class="tooltip3 mb-1" data-tooltip-content="#tooltip3_photo_3">
                            <span style="color:green"><?= $this->render('//svg-sprite', ['ico' => 'check-2']) ?></span>
                            <span class="pl-1">Хороший скан</span>
                        </div>
                    </div>
                <?php endif; ?>
            </td>
        </tr>
    </table>

    <div class="form-group <?= $attr == 'logoImage' ? 'hidden' : '' ?>" id="<?= $attr ?>-apply-box">
        <?= Html::radioList('apply', 0, Company::$applyNewImage, [
            'unselect' => 0,
            'itemOptions' => [
                'class' => 'apply-image-radio',
                'labelOptions' => [
                    'class' => 'control-label',
                    'style' => 'display: block;',
                ]
            ]
        ]); ?>
    </div>

    <div id="<?= $attr ?>_buttons" class="mt-4 d-flex justify-content-between jcrop-buttons">
        <?= Html::button('Применить', [
            'id' => "crop_{$attr}",
            'class' => "button-regular button-width button-regular_red jcrop-crop",
            'onclick' => "jcrop_ajaxRequestKUB('/company/img-crop', '/company/img-form', '{$model->id}', '{$attr}', this)",
        ]) ?>
        <?= Html::button('Отменить', [
            'id' => "cancel_{$attr}",
            'class' => "button-regular button-width button-regular button-hover-transparent jcrop-crop",
            'onclick' => "jcropImageDeleteKUB('/company/img-delete', '/company/img-form', '{$model->id}', '{$attr}', this)",
        ]) ?>
    </div>

    <script type="text/javascript">
        jcrop_initByAttr('<?= $attr ?>');
    </script>

    <div style="display:none">
        <?php if ($attr == 'chiefSignatureImage' || $attr == 'printImage'): ?>
            <div id="tooltip3_photo_1"><img src="/images/company_photo/<?=($attr)?>1.jpg"/></div>
            <div id="tooltip3_photo_2"><img src="/images/company_photo/<?=($attr)?>2.jpg"/></div>
            <div id="tooltip3_photo_3"><img src="/images/company_photo/<?=($attr)?>3.jpg"/></div>
        <?php endif; ?>
    </div>
</div>
