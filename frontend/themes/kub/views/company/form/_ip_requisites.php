<?php

use common\components\date\DateHelper;

/**
 * @var $form
 * @var $model
 * @var $templateDateInput
 */
?>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'egrip')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okved')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okpo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'oktmo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_legal')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_actual')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'fss')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr_ip')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr_employer')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okud')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ip_certificate_number')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'certificateDate', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                ])->label('Дата свидетельства') ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'ip_certificate_issued_by')->textInput([
                    'maxlength' => true,
                ])->label('Кем выдано свидетельство'); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?php if (empty($model->tax_authority_registration_date)) : ?>
                    <?= $form->field($model, 'tax_authority_registration_date', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'value' => DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]) ?>
                <?php else : ?>
                    <?= $form->field($model, 'tax_authority_registration_date')->textInput([
                        'readonly' => true,
                        'value' => DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]) ?>
                <?php endif ?>
            </div>
        </div>
        <br>
    </div>
</div>
