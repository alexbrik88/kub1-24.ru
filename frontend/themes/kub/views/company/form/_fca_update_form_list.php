<?php foreach ($model->foreignCurrencyAccounts as $foreignCurrencyAccount) {
    echo $this->render('modal_foreign_currency_account/_update', [
        'foreignCurrencyAccount' => $foreignCurrencyAccount,
        'company' => $model,
    ]);
}; ?>
