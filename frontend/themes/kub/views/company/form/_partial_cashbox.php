<?php

use common\models\company\CheckingAccountant;
use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\models\CashboxSearch;
use frontend\themes\kub\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new CashboxSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

$this->registerJs(<<<JS
$(document).on('submit', '#cashbox-form', function(e){
    var ajaxModal = $('#ajax-modal-box');
    if (ajaxModal && $(this, ajaxModal).length) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('.ajax-modal-content', ajaxModal).html(data);
        });
    }
});
JS
);
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'cashbox-pjax-container',
                'enablePushState' => false,
            ]); ?>

            <?= $this->render('//cashbox/_table', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>

            <?php \yii\widgets\Pjax::end(); ?>

            <div class="form-group">
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить кассу</span>', [
                    'class' => 'button-regular button-regular_red ajax-modal-btn',
                    'data-title' => 'Добавить кассу',
                    'data-url' => Url::to(['/cashbox/create']),
                ]); ?>
            </div>
        </div>
    </div>
</div>
