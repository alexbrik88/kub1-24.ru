<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\Company;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $company Company */

echo $this->render('_partial/_modal_form', [
    'foreignCurrencyAccount' => $foreignCurrencyAccount,
    'title' => 'Редактировать валютный счет',
    'id' => 'update-foreign-account-' . $foreignCurrencyAccount->id,
    'company' => $company,
]);
