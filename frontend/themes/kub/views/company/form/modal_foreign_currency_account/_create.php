<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\Company;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $company Company|null */

if (!isset($company)) {
    $company = null;
}

echo $this->render('_partial/_modal_form', [
    'foreignCurrencyAccount' => $foreignCurrencyAccount,
    'title' => 'Добавить валютный счет',
    'id' => 'add-foreign-account',
    'company' => $company,
]);
