<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\Company;
use yii\bootstrap4\Modal;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $title string */
/* @var $id string */
/* @var $company Company|null */

?>
<?php Modal::begin([
    'id' => $id,
    'title' => $title,
    'toggleButton' => false,
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>

<?= $this->render('_form', [
    'foreignCurrencyAccount' => $foreignCurrencyAccount,
    'company' => $company,
]); ?>

<?php Modal::end() ?>

