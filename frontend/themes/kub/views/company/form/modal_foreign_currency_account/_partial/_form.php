<?php

use common\models\Company;
use common\models\company\ForeignCurrencyAccount;
use common\models\currency\Currency;
use frontend\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Html;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $company Company|null */

$model = $foreignCurrencyAccount;
$currency = Currency::find()->select(['name', 'id'])->andWhere(['not', ['id' => Currency::DEFAULT_ID]])->indexBy('id')->column();
if (empty($model->currency_id)) {
    $model->currency_id = Currency::DEFAULT_USD;
}

$isMainLabel = 'Основной для '.Html::tag('span', $currency[$model->currency_id] ?? '', [
    'class' => 'currency_name_tag',
]);

$aditionaDatalLabel = '<span class="link-txt">Указать дополнительные данные необходимые для выставления инвойсов</span>'."\n";
$aditionaDatalLabel .= Icon::get('shevron', ['class' => 'link-shevron']);

if (!isset($actionUrl)) {
    $actionUrl = $model->isNewRecord ? Url::to([
        'create-foreign-currency-account',
        'companyId' => $company ? $company->id : null
    ]) : Url::to([
        'update-foreign-currency-account',
        'id' => $model->id,
        'companyId' => $company ? $company->id : null
    ]);
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.label-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);

$form = ActiveForm::begin([
    'action' => $actionUrl,
    'options' => [
        'class' => 'form-foreign-currency-account',
        'id' => 'form-foreign-currency-account-' . $model->id,
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'rs', [
                'enableClientValidation' => false,
            ])->textInput(['placeholder' => 'Пример: 01234567890123456789', 'maxlength' => true]); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
                'data' => $currency,
                'options' => [
                    'data-currency' => $currency,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'minimumResultsForSearch' => 10,
                ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <label class="label">&nbsp;</label>
            <?= $form->field($model, 'is_foreign_bank')->checkbox([
                'class' => '',
            ], true)->label(null, [
                'class' => 'label pt-3',
            ]); ?>
        </div>
    </div>


    <div class="row">
        <?php if ($model->isNewRecord) : ?>
            <div class="col-6">
                <?= $form->field($model, 'createStartBalance')->checkbox([
                    'class' => 'account_start_balance_checkbox',
                ])->label('Указать начальный остаток', [
                    'class' => 'label',
                ]) ?>
            </div>
        <?php endif ?>
        <div class="col-6">
            <?php if ($model->type !== ForeignCurrencyAccount::TYPE_MAIN) : ?>
                <?= $form->field($model, 'isMain')->checkbox([
                    'class' => '',
                ])->label($isMainLabel, [
                    'class' => 'label',
                ]); ?>

                <?php if (!$model->isNewRecord && count(Yii::$app->user->identity->company->foreignCurrencyAccounts) !== 1) : ?>
                    <?= $form->field($model, 'isClosed')->checkbox([
                        'class' => '',
                    ])->label(null, [
                        'class' => 'label',
                    ]); ?>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>

    <?php if ($model->isNewRecord) : ?>
        <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : ''; ?>">
            <div class="col-6">
                <?= $form->field($model, 'startBalanceAmount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'startBalanceDate')->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ])->label('на дату') ?>
            </div>
        </div>
        <?php if (Yii::$app->request->isAjax) : ?>
            <script type="text/javascript">
                refreshDatepicker();
            </script>
        <?php endif ?>
    <?php endif ?>

    <?= Html::button($aditionaDatalLabel, [
        'class' => 'link link_collapse link_bold button-clr mb-4 collapsed',
        'data-toggle' => 'collapse',
        'data-target' => '#accountDopColumns',
        'aria-expanded' => 'false',
        'aria-controls' => 'accountDopColumns',
    ]) ?>

    <div id="accountDopColumns" class="collapse">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'swift')
                    ->textInput(['placeholder' => 'Пример: ALFARUMM']); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'bank_name')
                    ->textInput(['placeholder' => 'Пример: ALFA-BANK']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'bank_address')
                    ->textInput(['placeholder' => 'Пример: 27 Kalanchevskaya str., Moscow, 107078']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 left-column">
                <?= $form->field($model, 'corr_rs')
                    ->textInput(['placeholder' => 'Пример: 36310481']); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'iban', [
                    'enableClientValidation' => false,
                ])->textInput(); ?>
            </div>
        <div class="row">
        </div>
            <div class="col-md-6">
                <?= $form->field($model, 'corr_swift')
                    ->textInput(['placeholder' => 'Пример: CITIUS33']); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'corr_bank_name')
                    ->textInput(['placeholder' => 'Пример: CITIBANK NA']); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'corr_bank_address')
                    ->textInput(['placeholder' => 'Пример: 399 Park Avenue, New York, NY 10043, USA']); ?>
            </div>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between" style="margin-top: 20px">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-width button-regular button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-clr button-regular button-hover-transparent back',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>
