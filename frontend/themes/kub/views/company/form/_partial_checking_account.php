<?php

use backend\models\Bank;
use common\components\grid\GridView;
use common\components\ImageHelper;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */

$dataProvider->pagination->pageSize = 0;

$banks = Bank::find()->andWhere([
    'is_special_offer' => true,
    'is_blocked' => false,
])->all();

$banksOrdered = [];
if ($banks) {
    // custom order banks
    $bikOrder = [
        '044525225', // Sberbank
        '044525974', // Tinkoff
        '044525985', // Otkrytie
        '044525092', // Modulbank
        '044525999', // Tochka
        '044525593'  // Alphabank
    ];
    foreach ($bikOrder as $bik) {
        foreach ($banks as $key => $bank) {
            if ($bik == $bank->bik) {
                $banksOrdered[] = $bank;
                unset($banks[$key]);
                continue;
            }
        }
    }
    foreach ($banks as $key => $bank) {
        $banksOrdered[] = $bank;
    }

    unset($banks);
}
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">

            <div class="custom-scroll-table scroll-table">
                <div class="table-wrap">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'rs-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => "{items}\n{pager}",

                    'columns' => [
                        [
                            'attribute' => 'bank_name',
                            'label' => 'Банк',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Название',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'format' => 'raw',
                            'value' => function (CheckingAccountant $model) {
                                return Html::tag('div', Html::encode($model->name), ['class' => 'title-cell', 'title' => $model->name]);
                            },
                        ],
                        [
                            'attribute' => 'bik',
                            'label' => 'БИК',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                        ],
                        [
                            'attribute' => 'currency_id',
                            'label' => 'Валюта',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                            ],
                            'format' => 'raw',
                            'value' => 'currency.name',
                        ],
                        [
                            'attribute' => 'rs',
                            'label' => 'Р/с',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'contentOptions' => [
                                'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                            ],
                        ],
                        [
                            'attribute' => 'type',
                            'label' => 'Тип',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return ArrayHelper::getValue($model->typeText, $model->type, '');
                            },
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {delete}',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'buttons' => [
                                'update' => function ($url, CheckingAccountant $data) {
                                    return Html::a(Icon::get('pencil'), '#update-company-rs-' . $data->id, [
                                        'class' => 'link',
                                        'data-toggle' => 'modal',
                                        'title' => Yii::t('yii', 'Редактировать'),
                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                    ]);
                                },
                                'delete' => function ($url, CheckingAccountant $data) {
                                    if ($data->type === CheckingAccountant::TYPE_MAIN) {
                                        return Html::tag('span', Icon::get('garbage'), [
                                            'class' => 'ml-1 grey-link disabled-link',
                                            'title' => 'Основной счет удалить нельзя'
                                        ]);
                                    }
                                    elseif ($data->hasMovement()) {
                                        return Html::tag('span', Icon::get('garbage'), [
                                            'class' => 'ml-1 grey-link disabled-link',
                                            'title' => 'Счет нельзя удалить, т.к. есть операции по счету.<br/>
                                                Удалите операции, после можно удалить счет',
                                            'title-as-html' => 1
                                        ]);
                                    } else {
                                        return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => Icon::get('garbage'),
                                                'class' => 'link',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                                        ]);
                                    }
                                },
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                $url = 0;
                                switch ($action) {
                                    case 'update':
                                        $url = 'update';
                                        break;
                                    case 'delete':
                                        $url = 'delete-checking-accountant';
                                        break;
                                }

                                return Url::to([$url, 'id' => $model->id]);
                            },
                        ],
                    ],
                ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить банковский счет</span>', [
                    'class' => 'button-regular button-regular_red',
                    'data-toggle' => 'modal',
                    'href' => '#add-company-rs',
                ]); ?>
            </div>
            <?php if ($banksOrdered && Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                <div class="pt-3 pb-1">
                    <div class="col-12">
                        <div class="label weight-700 mb-3" style="margin-left: -8px;">Открыть расчетный счет со скидкой</div>
                        <div class="row">
                            <?php foreach ($banksOrdered as $key => $bank) : ?>
                                <?php $dir = $bank->getUploadDirectory() . $bank->logo_link; ?>
                                <div class="col-3 mb-3" style="padding: 0 8px;">
                                    <div class="page-border page-border_width_2 page-border_grey">
                                        <?= ImageHelper::getThumb($dir, [246, 146], [
                                            'style' => 'cursor: pointer;',
                                            'data-toggle' => 'modal',
                                            'href' => '#apply-to-the-bank-' . $bank->id,
                                            'alt' => '',
                                        ]) ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
