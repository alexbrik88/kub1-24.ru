<?php

use yii\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/**
 * @var $form
 * @var $inputConfig
 * @var $model
 */

$companyFormExample = [
    ['short' => 'Corp', 'full' => 'Corporation'],
    ['short' => 'Inc', 'full' => 'Incorporated'],
    ['short' => 'IBC', 'full' => 'International Business Company'],
    ['short' => 'IC', 'full' => 'International Company'],
    ['short' => 'LTD', 'full' => 'Limited'],
    ['short' => 'LLC', 'full' => 'Limited Liability Company'],
    ['short' => 'LDC', 'full' => 'Limited Duration Company'],
    ['short' => 'LLP', 'full' => 'Limited Liability Partnership'],
    ['short' => 'LP', 'full' => 'Limited Partnership'],
    ['short' => 'PLC', 'full' => 'Private Limited Company'],
    ['short' => 'SA', 'full' => 'Sosiedad Anonima'],
    ['short' => 'SARL', 'full' => 'Societe a Responsidilite Limitee'],
];

$employeePositionExample = [
    ['short' => 'CAO', 'full' => 'Chief Accounting Officer'],
    ['short' => 'CEO', 'full' => 'Chief Executive Officer'],
    ['short' => 'CFO', 'full' => 'Chief Financial Officer'],
    ['short' => 'CMO', 'full' => 'Chief Marketing Officer'],
    ['short' => 'CTO', 'full' => 'Chief Technology officer'],
    ['short' => 'CVO', 'full' => 'Chief Visionary Officer'],
    ['short' => 'SM', 'full' => 'Senior Manager'],
    ['short' => 'MM', 'full' => 'Middle Manager'],
    ['short' => 'JM', 'full' => 'Junior Manager'],

];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-en-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);
?>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'name_short_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: KUB',
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name_full_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Limited Liability Company «KUB»',
                    ]); ?>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip-en-help',
                        'style' => 'position: absolute; top:0; left: 152px',
                        'data-tooltip-content' => '#tooltip_company_form_en_full',
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'form_legal_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: LLC',
                    ]); ?>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip-en-help',
                        'style' => 'position: absolute; top:0; left: 56px',
                        'data-tooltip-content' => '#tooltip_company_form_en_short',
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'address_legal_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Russia, Moscow, street, etc.',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'address_actual_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Russia, Moscow, street, etc.',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 left-column">
                    <?= $form->field($model, 'chief_post_name_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Chief Executive Officer',
                    ]); ?>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip-en-help',
                        'style' => 'position: absolute; top:0; left: 80px',
                        'data-tooltip-content' => '#tooltip_employee_position_en_full',
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'lastname_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petrov',
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'firstname_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petr',
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'patronymic_en')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petrovich',
                    ]); ?>
                    <?= $form->field($model, 'not_has_patronymic_en',
                        [
                            'options' => [
                                'class' => 'form-check',
                            ],
                            'labelOptions' => [
                                'class' => 'form-check-label control-label',
                            ],
                            'template' => "{input}\n{label}"
                        ]
                    )->checkbox([], false); ?>
                </div>
            </div>
    </div>
</div>

<div style="display: none">
    <div id="tooltip_company_form_en_short">
        <?= implode('<br/>', array_column($companyFormExample, 'short')) ?>
    </div>
    <div id="tooltip_company_form_en_full">
        <?= implode('<br/>', array_column($companyFormExample, 'full')) ?>
    </div>
    <div id="tooltip_employee_position_en_full">
        <?= implode('<br/>', array_column($employeePositionExample, 'full')) ?>
    </div>
</div>
