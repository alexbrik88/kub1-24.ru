<?php
/**
 * @var $form
 * @var $model
 */
?>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <h4 class="">Реквизиты</h4>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_legal')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
    </div>
</div>
