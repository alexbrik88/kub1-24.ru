<?php

use backend\models\Bank;
use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CompanyType;
use common\models\NdsOsno;
use frontend\assets\ContractorDossierAsset;
use frontend\models\AffiliateProgramForm;
use frontend\rbac\permissions as rbac;
use frontend\rbac\UserRole;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $outInvoiceSearch frontend\models\OutInvoiceSearch */
/* @var $outInvoiceProvider yii\data\ActiveDataProvider */

ContractorDossierAsset::register($this);
echo $this->render('../dossier/_style');

$this->title = 'Профиль компании';
$this->context->layoutWrapperCssClass = 'edit-profile';

$view = $company->company_type_id == CompanyType::TYPE_IP ? 'ip' : 'ooo';
?>
    <div class="wrap pt-3 pb-2 pl-4 pr-3">
        <div class="pt-1 pb-1 pl-2">
            <div class="row align-items-center">
                <h4 class="column mb-2"><?= Html::encode($company->shortTitle); ?></h4>
                <div class="column mb-2">
                    <?= Html::a('Визитка компании', ['visit-card'], ['class' => 'link']) ?>
                </div>
                <div class="column mb-2 ml-auto">
                    <?php if (Yii::$app->user->can(rbac\Company::UPDATE)) {
                        $linkOptions = [
                            'id' => 'company-edit-link',
                            'class' => 'button-list button-hover-transparent button-clr',
                        ];

                        if ($company->strict_mode == Company::ON_STRICT_MODE) {
                            $linkOptions['title'] = 'Внесите данные по Вашей компании, чтобы использовать все возможности сервиса.';
                        } else {
                            $linkOptions['title'] = 'Редактировать';
                        }

                        echo Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), ['update'], $linkOptions);
                    } ?>
                </div>
            </div>
        </div>
    </div>



    <div class="wrap pt-2 pl-4 pr-4 pb-0">
        <div class="pt-1 pl-1 pr-1">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
                ],
                'items' => [
                    [
                        'label' => 'Общие',
                        'content' => $this->render('view-profile/_partial_general_general', ['model' => $company]) ,
                    ],
                    [
                        'label' => 'Настройки',
                        'content' =>  $this->render('view-profile/_partial_general_setting', ['model' => $company]),
                    ],
                   /* [
                        'label' => 'Оформления счета',
                        'content' => '3',
                    ],
                    [
                        'label' => 'Оформления кабинета',
                        'content' =>'4',
                    ],*/
                ],
            ]); ?>

        </div>
    </div>
    <div class="wrap pt-2 pl-4 pr-4 pb-0">
        <div class="pt-1 pl-1 pr-2">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
                ],
                'items' => [
                    [
                        'label' => 'Банковские счета',
                        'encode' => false,
                        'content' => $this->render('view-profile/_partial_checking_accountant', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'banks' => $banks,
                        ]),
                        'active' => true,
                    ],
                    [
                        'label' => 'Кассы',
                        'content' => $this->render('view-profile/_partial_cashbox', ['model' => $company]),
                    ],
                    [
                        'label' => 'Карты',
                        'content' => $this->render('view-profile/_partial_card', ['model' => $company]),
                    ],
                    [
                        'label' => 'E-money',
                        'content' => $this->render('view-profile/_partial_emoney', ['model' => $company]),
                    ],
                    [
                        'label' => 'Склады',
                        'content' => $this->render('view-profile/_partial_store', ['model' => $company]),
                    ],
                    // [
                    //     'label' => 'Магазины',
                    //     'content' => $this->render('view-profile/_partial_retail', ['model' => $company]),
                    // ],
                    [
                        'label' => 'Точки продаж',
                        'content' => $this->render('view-profile/_partial_company_structure', ['model' => $company]),
                    ],
                    [
                        'label' => 'Направления',
                        'content' => $this->render('view-profile/_partial_company_industry', ['model' => $company]),
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <div class="wrap pt-2 pl-4 pr-4 pb-0">
        <div class="pt-1 pl-1 pr-2">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 mr-3 row',
                ],
                'items' => [
                    [
                        'label' => 'Реквизиты',
                        'encode' => false,
                        'content' => $this->render("view-profile/_details_{$view}", [
                            'company' => $company,
                        ]),
                        'active' => !(bool)Yii::$app->request->get('tab')
                    ],
                    [
                        'label' => 'Реквизиты на английском',
                        'encode' => false,
                        'content' => $this->render('view-profile/_requisites_en', [
                            'company' => $company,
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => 'Реквизиты налоговой',
                        'content' => $this->render("view-profile/_partial_ifns", [
                            'company' => $company,
                        ]),
                    ],
                    /*[
                        'label' => 'Досье',
                        'id' => 'dossier',
                        'content' => $this->render('_dossier'),
                        'active' => (bool)Yii::$app->request->get('tab')
                    ]*/
                ],
            ]); ?>
        </div>
    </div>

<?= $this->render("view-profile/_partial_files", [
    'company' => $company,
]) ?>

<?php foreach ($banks as $bank) {
    echo $this->render('form/modal_bank/_create', [
        'bank' => $bank,
        'applicationToBank' => $applicationToBank,
        'redirectUrl' => Yii::$app->controller->action->id,
    ]);
} ?>
