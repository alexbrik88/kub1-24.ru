<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var \yii\base\View $this */
/* @var $sendForm \frontend\models\VisitCardSendForm */
/* @var $message string */

?>

<h3 class="modal-title">Отправка визитки</h3>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'send-visit-card-form',
    'action' => Url::to(['send-visit-card',]),
    'method' => 'post',
    'enableAjaxValidation' => true,
]); ?>
<div class="form-body">
    <?= $form->field($sendForm, 'email')->label()->textInput(); ?>
</div>
<div class="form-actions text-center">
    <?= Html::submitButton('ОК', [
        'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
    ]); ?>
</div>
<div class="clr"></div>
<?php $form->end(); ?>

