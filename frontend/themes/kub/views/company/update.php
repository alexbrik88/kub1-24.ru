<?php

use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use backend\models\Bank;
use common\models\company\ApplicationToBank;
use frontend\themes\kub\widgets\ImportDialogWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $backUrl string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $checkingAccountant CheckingAccountant */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */

$this->title = 'Редактирование профиля компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
$model->checkFirstUpdate();
?>
<div class="company-update">
    <?= $this->render('form/_form', [
        'model' => $model,
        'ifns' => $ifns,
        'backUrl' => $backUrl,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'checkingAccountant' => $checkingAccountant,
        'banks' => $banks,
        'applicationToBank' => $applicationToBank,
    ]); ?>
</div>
<?php
$successFlashMessageCreate = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с добавлен</div>";
$successFlashMessageUpdate = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с изменен</div>";
$successFlashMessageDelete = "<div id='w0-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Р/с удален</div>";
?>

<?php $this->registerJs('
    $(document).on("show.bs.modal", "#add-company-rs", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("show.bs.modal", "#add-foreign-account", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("show.bs.modal", ".exists-company-rs", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("show.bs.modal", ".exists-foreign-account", function(event) {
        $(".alert-success").remove();
    });
    $(document).on("submit", "form.form-checking-accountant", function(){
        var btn = this;
        return submitForm(btn, "#rs-pjax-container", "#company_rs_update_form_list");
    });
    $(document).on("submit", "form.form-foreign-currency-account", function(){
        var btn = this;
        return submitForm(btn, "#fca-pjax-container", "#company_fca_update_form_list");
    });
    function submitForm(btn, idPjaxContainer, idUpdateFormList, $flashMsg) {
         var formId = $(btn).attr("id");
         var $isNewRecord = $(btn).attr("is_new_record");
         $.post(
            $("#" + formId).attr("action"),
            $("#" + formId).serialize(),
            function(data){
                if(data.status == 1) {
                   var $message = $isNewRecord == 1 ? "' . $successFlashMessageCreate . '" : "' . $successFlashMessageUpdate . '";
                   $("#form-foreign-currency-account-").find("input:text").val("");
                   $(".modal").modal("hide");
                   $(idUpdateFormList).html(data.rs_form_list);
                   $(".page-content").prepend($message);
                   $.pjax.reload(idPjaxContainer, {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
            } else {
                   $("#" + formId).html( $(data.html).find("#" + formId).html() );
                }
            }
         );
         return false;
    }
    $(document).on("click", "button.delete-card-bill", function(){
        var $this = $(this);
        return deleteItem($this, "#card-bill-pjax-container", null);
    });    
    $(document).on("click", "button.delete-foreign-currency-account", function(){
        var $this = $(this);
        return deleteItem($this, "#fca-pjax-container", "#company_fca_update_form_list");
    });
    $(document).on("click", "button.btn-confirm-yes", function(){
        var $this = $(this);
        deleteItem($this, "#rs-pjax-container", "#company_rs_update_form_list");
        $this.closest(".modal").modal("hide");
        return false;
    });
    function deleteItem($this, idPjaxContainer, idUpdateFormList) {
        $.ajax({
            url: $this.data("url"),
            type: $this.data("type"),
            data: $this.data("params"),
            success: function(data) {
                if(data.status == 1) {
                    if (idUpdateFormList) {
                        $(idUpdateFormList).html(data.rs_form_list);
                    }
                    $(".page-content").prepend("' . $successFlashMessageDelete . '");
                    $.pjax.reload(idPjaxContainer, {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
                }
                if (data.js) {
                    eval(data.js);
                }
            }
        });
    }');

echo ImportDialogWidget::widget();