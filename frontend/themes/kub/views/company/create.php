<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.10.2016
 * Time: 4:49
 */

use common\models\company\CheckingAccountant;
use common\models\company\CheckingAccountantSearch;
use backend\models\Bank;
use common\models\company\ApplicationToBank;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $ifns common\models\Ifns */
/* @var $backUrl string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $checkingAccountant CheckingAccountant */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $submitAction string */

$this->title = 'Создание компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
?>
<div class="company-update">
    <?= $this->render('form/_form', [
        'model' => $model,
        'ifns' => $ifns,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'checkingAccountant' => $checkingAccountant,
        'banks' => $banks,
        'applicationToBank' => $applicationToBank,
        'submitAction' => $submitAction,
    ]) ?>
</div>
<?php $this->registerJs('
    $(document).on("submit", "form.form-checking-accountant", function(){
        var formId = $(this).attr("id");
        $.post(
            $("#" + formId).attr("action"),
            $("#" + formId).serialize(),
            function(data){
                if(data.status == 1) {
                    $("#form-checking-accountant-").find("input:text").val("");
                    $(".modal").modal("hide");
                    $("#company_rs_update_form_list").html(data.rs_form_list);
                    $.pjax.reload("#rs-pjax-container", {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
                } else {
                    $("#" + formId).html( $(data.html).find("#" + formId).html() );
                }
            }
        );
        return false;
    });
    $(document).on("click", "button.btn-confirm-yes", function(){
        var $this = $(this);
        $.ajax({
            url: $this.data("url"),
            type: $this.data("type"),
            data: $this.data("params"),
            success: function(data) {
                if(data.status == 1) {
                    $("#company_rs_update_form_list").html(data.rs_form_list);
                    $.pjax.reload("#rs-pjax-container", {"push": false, "timeout": 5000, "url": $("#form-update-company").attr("action")});
                }
                if (data.js) {
                    eval(data.js);
                }
            }
        });
        return false;
    });
'); ?>
