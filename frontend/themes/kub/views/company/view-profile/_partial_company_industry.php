<?php
use common\components\grid\GridView;
use common\models\company\CompanyIndustry;
use frontend\themes\kub\components\Icon;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;

/** @var \common\models\Company $model */

$dataProvider = new ActiveDataProvider([
    'query' => CompanyIndustry::find()->joinWith('industryType')->where(['company_id' => $model->id]),
    'pagination' => ['pageSize' => 0],
    'sort' => [
        'attributes' => [
            'name',
            'industryType.name',
        ],
        'defaultOrder' => [
            'name' => SORT_ASC,
        ],
    ],

]);

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">

            <div class="custom-scroll-table scroll-table">
                <div class="table-wrap">

                <?php Pjax::begin([
                    'id' => 'company-industry-pjax-container',
                    'enablePushState' => false,
                    'scrollTo' => false
                ]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => "{items}\n{pager}",

                    'columns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Название направления',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (CompanyIndustry $model) {
                                return Html::tag('span', Html::encode($model->name));
                            },
                        ],
                        [
                            'attribute' => 'sale_point_type.name',
                            'label' => 'Тип направления бизнеса',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (CompanyIndustry $model) {
                                return Html::encode($model->industryType->name);
                            },
                        ],
                        [
                            'label' => 'Тип',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'value' => function (CompanyIndustry $model) {
                                return $model->is_main ? 'Основное' : 'Дополнительное';
                            },
                        ],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>