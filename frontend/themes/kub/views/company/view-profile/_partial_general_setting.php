<?php
    use common\models\Company;
?>
<div class="row align-items-center mt-2">
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Отправлять с печатью и подписью</div>
        <div>
            <?= join(', ', array_filter([
                $model->pdf_send_signed ? $model->getAttributeLabel('pdf_send_signed') : '',
                $model->pdf_act_send_signed ? $model->getAttributeLabel('pdf_act_send_signed') : '',
            ])) ?>&nbsp;
        </div>
    </div>
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Формировать PDF с печатью и подписью</div>
        <div>
            <?= join(', ', array_filter([
                $model->pdf_signed ? $model->getAttributeLabel('pdf_signed') : '',
                $model->pdf_act_signed ? $model->getAttributeLabel('pdf_act_signed') : '',
            ])) ?>
        </div>
    </div>
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">К номеру счета Доп номер</div>
        <div>
            <?= (isset(Company::$addNumPositions[$model->is_additional_number_before])) ?
                Company::$addNumPositions[$model->is_additional_number_before] : ''; ?>&nbsp;
        </div>
    </div>
</div>


<div class="row align-items-center mt-2">
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Кнопка ТТН</div>
        <div>
            <?= $model->show_waybill_button_in_invoice ? "Да" : 'Нет'?>&nbsp;
        </div>
    </div>
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Шаблон СФ и УПД</div>
        <div>
            <?
                $invoice_facture_print_template = (empty($model->invoice_facture_print_template))?0:$model->invoice_facture_print_template;
                echo Company::$addTemplateSFandUPD[$invoice_facture_print_template];
            ?>
        </div>
    </div>
    <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Отсрочка платежа</div>
        <table>
            <tr>
                <td>Для покупателя:</td><td><?=$model->customer_payment_delay?></td>
            </tr>
            <tr>
                <td>Для продовца:</td><td> <?=$model->seller_payment_delay?></td>
            </tr>
        </table>
    </div>
</div>

