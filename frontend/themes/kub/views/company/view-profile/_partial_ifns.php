<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

$ifns = $company->ifns;

?>

<?php if ($ifns) : ?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Код ИФНС</div>
                <div><?= $company->ifns_ga ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g6')) ?></div>
                <div><?= $ifns->g6 ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g7')) ?></div>
                <div><?= $ifns->g7 ?>&nbsp;</div>
            </div>
            <div class="col-12 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('gb')) ?></div>
                <div><?= $ifns->gb ?>&nbsp;</div>
            </div>
            <div class="col-12 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g4')) ?></div>
                <div><?= $ifns->g4 ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g9')) ?></div>
                <div><?= $ifns->g9 ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g8')) ?></div>
                <div><?= $ifns->g8 ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g11')) ?></div>
                <div><?= $ifns->g11 ?>&nbsp;</div>
            </div>
            <div class="col-8 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g1')) ?></div>
                <div><?= $ifns->g1 ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3"><?= Html::encode($ifns->getAttributeLabel('g2')) ?></div>
                <div><?= $ifns->g2 ?>&nbsp;</div>
            </div>
        </div>
    </div>
</div>

<?php endif ?>
