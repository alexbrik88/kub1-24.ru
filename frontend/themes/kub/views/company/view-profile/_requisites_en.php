<?php

use yii\helpers\Html;

/**
 * @var $form
 * @var $inputConfig
 * @var $company
 */
?>
<div class="mx-1 pt-3 px-4 pb-4">
    <div class="row pt-3">
        <div class="col-4 pb-3 mb-3">
            <div class="label weight-700 mb-3">Название организации:</div>
            <div><?= Html::encode($company->name_short_en);?>&nbsp;</div>
        </div>
        <div class="col-4 pb-3 mb-3">
            <div class="label weight-700 mb-3">Полное наименование:</div>
            <div><?= Html::encode($company->name_full_en); ?>&nbsp;</div>
        </div>
        <div class="col-4 pb-3 mb-3">
            <div class="label weight-700 mb-3">Форма:</div>
            <div><?= Html::encode($company->form_legal_en); ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pb-3 mb-3">
            <div class="label weight-700 mb-3">Юридический адрес:</div>
            <div><?= Html::encode($company->address_legal_en);?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pb-3">
            <div class="label weight-700 mb-3">Фактический адрес:</div>
            <div><?= Html::encode($company->address_actual_en); ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pb-3">
            <div class="label weight-700 mb-3">Должность:</div>
            <div><?= Html::encode($company->chief_post_name_en); ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-4 pb-3">
            <div class="label weight-700 mb-3">Фамилия:</div>
            <div><?= Html::encode($company->lastname_en); ?></div>
        </div>
        <div class="col-4 pb-3">
            <div class="label weight-700 mb-3">Имя:</div>
            <div><?= Html::encode($company->firstname_en); ?></div>
        </div>
        <div class="col-4 pb-3">
            <div class="label weight-700 mb-3">Отчество:</div>
            <div><?= Html::encode($company->patronymic_en); ?></div>
        </div>
    </div>
</div>

