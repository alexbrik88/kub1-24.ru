<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\company\ForeignCurrencyAccountSearch;
use common\components\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Bank;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $foreignCurrencyAccountProvider ForeignCurrencyAccount */
/* @var $foreignCurrencyAccountSearch ForeignCurrencyAccountSearch */
/* @var $banks Bank[] */

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'fca-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'swift',
                        'label' => 'SWIFT',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->swift;
                        },
                    ],
                    [
                        'attribute' => 'currency_id',
                        'label' => 'Валюта',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->currency->name;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Счет',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'value' => function (ForeignCurrencyAccount  $model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function (ForeignCurrencyAccount $model) {
                            return ArrayHelper::getValue($model->typeAccount, $model->type, '');
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
