<?php
use yii\helpers\Html;
use common\models\NdsOsno;
use yii\helpers\ArrayHelper;
?>
<div class="row align-items-center mt-2">
     <div class="col-4 pb-3 mb-3">
         <div class="label weight-700 mb-3">Краткое наименование организации</div>
         <div><?= Html::encode($model->shortTitle); ?>&nbsp;</div>
     </div>
     <div class="column pb-3 mb-3">
          <div class="label weight-700 mb-3">Полное наименование организации</div>
          <div><?= Html::encode($model->title); ?>&nbsp;</div>
     </div>
 </div>
<div class="row align-items-center">
     <div class="col-4 pb-3 mb-3">
          <div class="label weight-700 mb-3">Система налогообложения</div>
             <div><?= $model->companyTaxationType->name; ?>&nbsp;</div>
          </div>
     <?php if (ArrayHelper::getValue($model, 'companyTaxationType.osno')) : ?>
        <div class="col-4 pb-3 mb-3">
            <div class="label weight-700 mb-3">В счетах цену за товар/услуги указывать</div>
                <div>
                <?php if ($model->nds == NdsOsno::WITH_NDS) : ?>
                    НДС включен в цену
                <?php elseif ($model->nds == NdsOsno::WITHOUT_NDS) : ?>
                    НДС сверху цены
                <?php endif ?>&nbsp;
                </div>
            </div>
     <?php endif ?>
     <div class="col-4 pb-3 mb-3">
        <div class="label weight-700 mb-3">Часовой пояс</div>
        <div><?= ArrayHelper::getValue($model, 'timeZone.out_time_zone') ?>&nbsp;</div>
     </div>
 </div>
<div class="row align-items-center">
                <div class="col-4 pb-3 mb-3">
                    <div class="label weight-700 mb-3">Телефон</div>
                    <div>
                        <?= Html::encode($model->phone); ?>
                    </div>
                </div>
                <div class="col-4 pb-3 mb-3">
                    <div class="label weight-700 mb-3">Электронная почта</div>
                    <div>
                        <?= Html::encode($model->email); ?>
                    </div>
                </div>
</div>