<?php

use common\components\grid\GridView;
use common\modules\cards\models\CardBill;
use common\modules\cards\models\CardBillRepository;

/* @var $model common\models\Company */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CardBillRepository */

$searchModel = new CardBillRepository([
    'company' => $model
]);
$dataProvider = $searchModel->getDataProvider();
$dataProvider->sort = [
    'attributes' => ['name', 'account_id'],
    'defaultOrder' => ['name' => SORT_ASC]
]
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <div class="table-container">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'card-bill-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",

                        'columns' => [
                            [
                                'attribute' => 'name',
                                'label' => 'Название',
                            ],
                            [
                                'attribute' => 'currency_name',
                                'label' => 'Валюта',
                            ],
                            [
                                'attribute' => 'account_id',
                                'label' => 'Аккаунт',
                                'value' => 'account.fullName',
                            ],
                            [
                                'attribute' => 'is_accounting',
                                'label' => 'Для учета в бухгалтерии',
                                'format' => 'boolean',
                            ],
                        ],
                    ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>