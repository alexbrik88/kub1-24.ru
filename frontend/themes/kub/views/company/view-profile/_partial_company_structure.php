<?php
use common\components\grid\GridView;
use common\models\companyStructure\SalePoint;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;

/** @var \common\models\Company $model */

$dataProvider = new ActiveDataProvider([
    'query' => SalePoint::find()->joinWith('type')->where(['company_id' => $model->id]),
    'pagination' => ['pageSize' => 0],
    'sort' => [
        'attributes' => [
            'name',
            'sale_point_type.name',
        ],
        'defaultOrder' => [
            'name' => SORT_ASC,
        ],
    ],
]);

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">

            <div class="custom-scroll-table scroll-table">
                <div class="table-wrap">

                <?php Pjax::begin([
                    'id' => 'sale-point-pjax-container',
                    'enablePushState' => false,
                    'scrollTo' => false
                ]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => "{items}\n{pager}",

                    'columns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Точка продаж',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (SalePoint $model) {
                                return
                                    Html::tag('img', null, ['src' => '/images/'.$model->getIcon()]) .
                                    Html::tag('span', Html::encode($model->name), ['class' => 'ml-2 pl-1']);
                            },
                        ],
                        [
                            'attribute' => 'sale_point_type.name',
                            'label' => 'Тип',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (SalePoint $model) {
                                return Html::encode($model->type->name);
                            },
                        ],
                        [
                            'attribute' => 'cashboxes',
                            'label' => 'Касса',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (SalePoint $model) {
                                $content = [];
                                foreach ($model->cashboxes as $cashbox) {
                                    $content[] = Html::encode($cashbox->name);
                                }

                                return implode('<br/>', $content);
                            },
                        ],
                        [
                            'attribute' => 'store',
                            'label' => 'Склад',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (SalePoint $model) {
                                return $model->store ? Html::encode($model->store->name) : '';
                            },
                        ],
                        [
                            'attribute' => 'employers',
                            'label' => 'Сотрудники',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'format' => 'raw',
                            'value' => function (SalePoint $model) {
                                $content = [];
                                foreach ($model->employers as $employee) {
                                    $content[] = Html::encode($employee->getShortFio());
                                }

                                return implode('<br/>', $content);
                            },
                        ],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>

                </div>
            </div>

        </div>
    </div>
</div>