<div class="row">
    <div class="col-sm-12">
        <div class="wrap">
            <div class="portlet-body accounts-list">
                <div class="company-info" id="anketa">
                    <h4>Что-то пошло не так...</h4>
                    <img src="/img/bg/dossier-exception.png"/>
                    <h4 class="mb-3 mt-3">Мы уже работаем над этим. Напишите в техподдержку.</h4>
                    <div style="height: 50px;">
                        <div style="float:left; width: 185px">
                            <label class="label">Телефон</label>
                            <div>8 800 500 5436</div>
                        </div>
                        <div style="float:left; width: 185px">
                            <label class="label">E-mail</label>
                            <div>support@kub-24.ru</div>
                        </div>
                        <div style="float:left; width: 250px">
                            <label class="label">Чат</label>
                            <div>Кнопка в правом нижнем углу</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>