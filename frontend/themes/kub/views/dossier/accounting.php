<?php

use common\components\zchb\Card;
use common\components\zchb\FinancialStatement;
use yii\web\View;

/**
 * @var View                    $this
 * @var Card                    $card
 * @var FinancialStatement|null $fs
 */
?>
<script>setDossierTab('accounting');</script>
<?php
if ($fs === null) {
    echo '<p style="font-style:italic;">Данных по бухгалтерской отчётности нет.</p>';
    return;
}
$yearList = $fs->yearList();

function tableClass(bool $reset = false): string
{
    static $index;
    if ($reset === true) {
        $index = 0;
        return '';
    }
    $index++;
    $class = 'table-desktop';
    if ($index > 2) {
        $class .= ' table-tablet';
    }
    if ($index > 4) {
        $class .= ' table-mobile';
    }
    return $class;
}

?>

<div class="wrap">
    <div class="caption">Бухгалтерская (финансовая) отчетность</div>
    <div class="portlet-body accounts-list">
        <div class="content-frame__header">
            <div class="content-frame__description">Бухгалтерская отчетность публикуется Федеральной службой
                государственной статистики с задержкой, отчет за предыдущий (<?= $fs->getYear() ?>) год обычно
                становится доступен в
                сентябре-октябре текущего (<?= $fs->getYear() + 1 ?>).
            </div>
            <br/>
            <div class="statement-name"><?= $card->НаимЮЛПолн ?></div>
            <table class="info-table info-table--egrul">
                <tbody>
                <tr>
                    <td class="empty"></td>
                    <td>ОГРН</td>
                    <td><?= $card->ОГРН ?></td>
                </tr>
                <tr>
                    <td class="empty"></td>
                    <td>ИНН<?php if ($card->КПП) {
                            echo ' / КПП';
                        } ?></td>
                    <td><?= $card->ИНН ?><?php if ($card->КПП) {
                            echo ' / ', $card->КПП;
                        } ?></td>
                </tr>
                </tbody>
            </table>
            <br/>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="caption">Бухгалтерский баланс</div>
    <div class="portlet-body accounts-list">
        <p>Все суммы указаны в тысячах рублей</p>
        <?php if (empty($yearList) === true) { ?>
            <div class="tile-item striped-table">
                <div class="accounting-header">
                    <div class="accounting-header__title">
                        <div id="xblock_1" class="accounting-title tile-item__title">Данных бухгалтерской отчётности
                            нет.
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="tile-item striped-table">
                <div class="accounting-table-wrap">
                    <table class="accounting-table">
                        <tbody>
                        <tr>
                            <th>
                                <div class="accounting-table__title">Форма № 1</div>
                            </th>
                            <th>
                                <div class="accounting-table__title">Код</div>
                            </th>
                            <?php foreach ($yearList as $item) { ?>
                                <th class="has-value table-desktop">
                                    <div class="accounting-table__title"><?= $item ?></div>
                                </th>
                            <?php } ?>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="accounting-table-wrap">
                    <table class="accounting-table">
                        <tbody>
                        <?= $this->render('accounting_table_tr', [
                            'fs' => $fs,
                            'title' => 'БАЛАНС (актив)',
                            'yearList' => $yearList,
                            'indicator' => 1600,
                        ]) ?>
                        <?= $this->render('accounting_table_tr', [
                            'fs' => $fs,
                            'title' => 'БАЛАНС (пассив)',
                            'yearList' => $yearList,
                            'indicator' => 1700,
                        ]) ?>
                        </tbody>
                    </table>
                </div>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'balance->active->I. ВНЕОБОРОТНЫЕ АКТИВЫ',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'balance->active->II. ОБОРОТНЫЕ АКТИВЫ',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'balance->passive->III. КАПИТАЛ И РЕЗЕРВЫ',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'balance->passive->IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'balance->passive->V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА',
                    'yearList' => $yearList,
                ]) ?>
            </div>
        <?php } ?>
    </div>
</div>

<div class="wrap">
    <div class="caption">Отчёт о финансовых результатах</div>
    <div class="portlet-body accounts-list">
        <p>Все суммы указаны в тысячах рублей</p>
        <?php if (empty($yearList) === true) { ?>
            <div class="tile-item striped-table">
                <div class="accounting-header">
                    <div class="accounting-header__title">
                        <div id="xblock_1" class="accounting-title tile-item__title">Данных бухгалтерской отчётности
                            нет.
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <?php $yearList = $fs->yearList(false, 2200, true); ?>
            <div class="tile-item striped-table">
                <div class="accounting-table-wrap">
                    <table class="accounting-table">
                        <tbody>
                        <tr>
                            <th>
                                <div class="accounting-table__title">Форма № 2</div>
                            </th>
                            <th>
                                <div class="accounting-table__title">Код</div>
                            </th>
                            <?php foreach ($yearList as $item) { ?>
                                <th class="has-value table-desktop">
                                    <div class="accounting-table__title"><?= $item ?></div>
                                </th>
                            <?php } ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'report->Доходы и расходы по обычным видам деятельности',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'report->Прочие доходы и расходы',
                    'yearList' => $yearList,
                ]) ?>
                <?= $this->render('accounting_table', [
                    'fs' => $fs,
                    'path' => 'report->Прочие доходы и расходы',
                    'yearList' => $yearList,
                ]) ?>
            </div>
        <?php } ?>
    </div>
</div>
