<?php

use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/**
 * @var string     $message Текст сообщения во всплывающем диалоге
 * @var string     $link    Ссылка, на которую нужно перейти в случае подтверждения
 * @var array|null $button
 * @var bool       $newTab  Открывать в новой вкладке
 */
if (isset($button) === true) {
    if (isset($button['label']) === false) {
        $button['label'] = '';
    }
    if (isset($button['class']) === false) {
        $button['class'] = '';
    }
    $button = [
        'tag' => 'a',
        'label' => $button['label'],
        'class' => $button['class'],
        'title' => $button['title'] ?? null,
        'style' => $button['style'] ?? null
    ];
} else {
    $button = false;
}
if (isset($message) === false || !$message) {
    $message = 'Вы уверены, что хотите проверить данного контрагента?';
}
if (isset($newTab) === false) {
    $newTab = substr(Url::current(), 0, 16) !== '/contractor/view';
}

echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'popup-confirm',
    ],
    'toggleButton' => $button,
    'confirmOptions' => $newTab ? ['target' => '_blank', 'onclick' => "$(this).closest('.modal').modal('hide')", 'data' => []] : [],
    'confirmUrl' => $link,
    'confirmParams' => [],
    'message' => $message,
]);