<?php

use common\components\zchb\Card;

/**
 * @var Card $card
 */
?>
<script>setDossierTab('okved');</script>
<div class="wrap">
    <div class="caption">Виды деятельности ОКВЭД (<?= $card->OKVEDCount() ?>)</div>
    <div class="accounts-list">

        <div class="tile-item__title okved-list-title">Основной</div>
        <ul class="okved-list">
            <li class="okved-item okved-item--main">
                <span class="okved-item__num"><?= $card->КодОКВЭД ?></span>
                <span class="okved-item__text"><?= $card->OKVEDTitle ?></span>
            </li>
        </ul>
        <div class="tile-item__title okved-list-title">Дополнительные (<?= count($card->СвОКВЭДДоп) ?>)</div>

        <ul class="okved-list">
            <?php foreach ($card->СвОКВЭДДоп as $item) { ?>
                <li class="okved-item">
                    <div class="okved-item__num"><?= $item['КодОКВЭД'] ?></div>
                    <div class="okved-item__text"><?= $item['НаимОКВЭД'] ?></div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
