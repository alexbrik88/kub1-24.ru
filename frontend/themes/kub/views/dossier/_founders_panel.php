<div class="founders-wrap" data-id="founders">
    <div class="founders-wrap-in founders-wrap-scroll">
        <button class="founders-wrap-close button-clr" type="button" data-target="founders">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>

        <div class="main-block">
        </div>

    </div>
</div>


<script>
    var foundersClicked = false;
    $(document).on('click', '.open-founders-panel', function (e) {
        e.preventDefault();

        if (foundersClicked)
            return false;

        foundersClicked = true;
        var that = this;
        var url = $(that).data('url');
        $(that).siblings('.loader').show();
        $('.founders-wrap').find('.main-block').load(url, function () {
            var target = $(that).data('target');
            $('[data-id="' + target + '"]').toggleClass('visible show');
            if ($(that).closest('.dropdown').length) {
                $(that).closest('.founders-wrap').removeClass('stub');
            }
            $(that).siblings('.loader').hide();
            foundersClicked = false;
        });
    });

    $('.founders-wrap .founders-wrap-close').click(function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('[data-id="' + target + '"]').toggleClass('visible show');
        if ($(this).closest('.dropdown').length) {
            $(this).closest('.invoice-wrap').removeClass('stub');
        }
    });
</script>