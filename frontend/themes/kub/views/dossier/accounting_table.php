<?php

use common\components\zchb\FinancialStatement;
use yii\web\View;

/**
 * @var FinancialStatement $fs
 * @var int[]              $yearList
 * @var string             $path
 * @var View               $this ;
 */
$title = explode('->', $path);
$title = $title[count($title) - 1];
$data = array_keys($fs->valueAllOf($path));
?>
<div class="tile-item__title"><?= $title ?></div>
<div class="accounting-table-wrap">
    <table class="accounting-table">
        <tbody>
        <?php foreach ($data as $item) {
            echo $this->render('accounting_table_tr', [
                'fs' => $fs,
                'title' => $item,
                'yearList' => $yearList,
                'indicator' => $path . '->' . $item,
            ]);
        } ?>
        </tbody>
    </table>
</div>