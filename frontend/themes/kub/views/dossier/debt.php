<?php

use yii\helpers\Url;

/**
 * @var int $modelId
 */
?>
<script>setDossierTab('debt');</script>
<div class="wrap">
    <div class="caption">Исполнительные производства</div>
    <div class="portlet-body accounts-list finance" id="debt">
        <div class="loader"><span></span><span></span><span></span></div>
        <script>
            $('#debt').load('<?=Url::toRoute([
                'ajax-debt',
                'modelId' => $modelId
            ])?>');
        </script>
    </div>
</div>