<?php

use common\components\zchb\CourtArbitration;
use common\components\zchb\CourtArbitrationCard;
use kartik\select2\Select2;
use yii\helpers\Url;

/**
 * @var int                   $modelId
 * @var CourtArbitration|null $court
 */
?>

<script>setDossierTab('arbitr');</script>

<?php if ($court === null) { ?>
    <div class="wrap">
        <p>К сожалению информации о судебных делах нет</p>
    </div>
    <?php
    return;
} ?>

<div class="wrap">
    <div class="caption">Судебные дела (<?= $court->count() ?>)</div>
    <div class="arbitr accounts-list">
        <?php if ($court->count() === 0) { ?>
            <p><b>Судебных дел нет</b></p>
        <?php } else { ?>
            <div class="content-frame__header">
                <div class="content-frame__description" style="display:none;">
                    Сведения об участии организации в судебных процессах: <span></span>.
                </div>
            </div>
            <div class="main-wrap">
                <div class="main-wrap__content">
                    <br/>
                    <div class="row">
                        <div class="col-4">
                            <label class="label">Роль</label>
                            <?= Select2::widget([
                                'name' => 'by_director',
                                'data' => [
                                    '' => 'Все',
                                    $court::ROLE_APPLICANT => 'Истец',
                                    $court::ROLE_DEFENDANT => 'Ответчик',
                                    $court::ROLE_THIRD => 'Третья сторона',
                                    $court::ROLE_OTHER => 'Иное'
                                ],
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'width' => '100%',
                                ],
                                'options' => [
                                    'onchange' => new \yii\web\JsExpression('arbitr({"role": this.value});'),
                                ]
                            ]); ?>
                        </div>
                        <div class="col-4">
                            <label class="label">Статус</label>
                            <?= Select2::widget([
                                'name' => 'by_director',
                                'data' => [
                                    '' => 'Все',
                                    CourtArbitrationCard::STATUS_PROCESS => 'Рассматривается',
                                    CourtArbitrationCard::STATUS_FINISH => 'Завершено',
                                    CourtArbitrationCard::STATUS_APPEAL => 'Апелляция',
                                ],
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'width' => '100%',
                                ],
                                'options' => [
                                    'onchange' => new \yii\web\JsExpression('arbitr({"status": this.value});'),
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <div style="clear:both;"></div>

                    <div id="arbitr">
                        <p></p>
                        <p>Подготовка отчёта...</p>
                    </div>

                </div>
            </div>
            <script>
                var arbitr = function (link) {
                    let container = $('#arbitr');
                    let config = {'date': null, 'status': null, 'role': null, 'sort': null};
                    return function (cfg) {
                        container.html('<div class="loader"><span></span><span></span><span></span></div>');
                        config = $.extend(config, cfg);
                        let lnk = link;
                        if (config.role) lnk += '&role=' + config.role;
                        if (config.status) lnk += '&status=' + config.status;
                        if (config.sort) lnk += '&sort=' + config.sort;
                        $('#arbitr').load(lnk);
                    };
                }('<?=Url::toRoute([
                    'ajax-arbitr',
                    'ogrn' => $court->ogrn,
                    'inn' => $court->inn,
                    'modelId' => $modelId
                ])?>');
                arbitr();
            </script>
        <?php } ?>
    </div>
</div>