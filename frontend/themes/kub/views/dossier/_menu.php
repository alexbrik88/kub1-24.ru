<?php

use yii\bootstrap\Nav;
use yii\web\View;

echo $this->render('_style');

/**
 * @var View $this
 * @var bool $clickable
 */
$tabLink = $this->params['tabLink'];
$tab = $this->params['currentTab'];

/** @noinspection PhpUnhandledExceptionInspection */
echo Nav::widget([
    'items' => [
        [
            'label' => 'Главная',
            'url' => $tabLink('index'),
            'options' => ['class' => 'nav-item dossier-tab-index'],
            'linkOptions' => ['class' => 'nav-link' . ((!$tab || $tab == 'index') ? ' active' : '')],
            'active' => $tab == 'index',
        ],
        [
            'label' => 'Учредители',
            'url' => $tabLink('founders'),
            'options' => ['class' => 'nav-item dossier-tab-founders'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'founders' ? ' active' : '')],
            'active' => $tab == 'founders'
        ],
        [
            'label' => 'Связи',
            'url' => $tabLink('connections'),
            'options' => ['class' => 'nav-item dossier-tab-connections'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'connections' ? ' active' : '')],
            'active' => $tab == 'connections'
        ],
        [
            'label' => 'Надежность',
            'url' => $tabLink('reliability'),
            'options' => ['class' => 'nav-item dossier-tab-reliability'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'reliability' ? ' active' : '')],
            'active' => $tab == 'reliability'
        ],
        [
            'label' => 'Финансы',
            'url' => $tabLink('finance'),
            'options' => ['class' => 'nav-item dossier-tab-finance'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'finance' ? ' active' : '')],
            'active' => $tab == 'finance'
        ],
        [
            'label' => 'Суды',
            'url' => $tabLink('arbitr'),
            'options' => ['class' => 'nav-item dossier-tab-arbitr'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'arbitr' ? ' active' : '')],
            'active' => $tab == 'arbitr'
        ],
        [
            'label' => 'Долги',
            'url' => $tabLink('debt'),
            'options' => ['class' => 'nav-item dossier-tab-debt'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'debt' ? ' active' : '')],
            'active' => $tab == 'debt'
        ],
        [
            'label' => 'Виды деят.',
            'url' => $tabLink('okved'),
            'options' => ['class' => 'nav-item dossier-tab-okved'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'okved' ? ' active' : '')],
            'active' => $tab == 'okved'
        ],
        [
            'label' => 'Отчетность',
            'url' => $tabLink('accounting'),
            'options' => ['class' => 'nav-item dossier-tab-accounting'],
            'linkOptions' => ['class' => 'nav-link' . ($tab == 'accounting' ? ' active' : '')],
            'active' => $tab == 'accounting'
        ],
    ],
    'options' => ['class' => 'nav-dossier nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
]);