<?php

use common\components\zchb\Card;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\components\helpers\ArrayHelper;

/**
 * @var Card    $card
 * @var array[] $connections
 * @var int     $countByDirector
 * @var int     $countByFounder
 */
//Руководители и учредители в одном списке
$all = [];
foreach ($card->directors ?? [] as $item) {
    $all[$item['inn']] = $item['fl'];
}
foreach ($card->founders as $item) {
    if (isset($all[$item['inn']]) === false) {
        $all[$item['inn']] = $item['name'];
    }
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);
?>
<script>setDossierTab('connections');</script>
<div class="wrap">
    <div class="caption">
        Связи (<?= count($connections) ?>)
    </div>
    <div class="connections accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">Сведения об аффилированных организациях, полученные на основании анализа информации из ЕГРЮЛ
                <span class="button-clr tooltip3" data-tooltip-content="#tooltip_source_1">
                    <svg class="tooltip-question-icon svg-icon"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>
                </span>
                <div style="display: none">
                    <div id="tooltip_source_1">Единый государственный реестр юридических лиц ФНС РФ</div>
                </div>
            </div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content noyellow">
                <div class="tabs-line">
                    <div class="tabs-line__wrap">
                        <div class="row">
                            <div class="col-4">
                                <label class="label">По руководителю</label>
                                <?= Select2::widget([
                                    'id' => 'byDirector',
                                    'name' => 'by_director',
                                    'data' => ['' => 'Все'] + ArrayHelper::map($card->directors ?? [], 'inn', 'fl'),
                                    'hideSearch' => true,
                                    'options' => [
                                        'class' => 'director',
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%',
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-4">
                                <label class="label">По учредителю</label>
                                <?= Select2::widget([
                                    'id' => 'byFounder',
                                    'name' => 'by_founder',
                                    'data' => ['' => 'Все'] + ArrayHelper::map($card->founders, 'inn', 'name'),
                                    'hideSearch' => true,
                                    'options' => [
                                        'class' => 'founder',
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <?php foreach ($connections as $item) {
                    $class = 'fl-all-' . $item['flINN'];
                    if ($item['byDirector'] === true) {
                        $class .= ' fl-director-' . $item['flINN'];
                    }
                    if ($item['byFounder'] === true) {
                        $class .= ' fl-founder-' . $item['flINN'];
                    }
                    if ($item['byDirector'] === true) {
                        $s = 'руководителю';
                    } else {
                        $s = '';
                    }
                    if ($item['byFounder'] === true) {
                        if ($s !== '') {
                            $s .= ', ';
                        }
                        $s .= 'учредителю';
                    }
                    ?>
                    <div class="company-item <?= $class ?>">
                        <div class="company-item__title"><?= $item['НаимЮЛСокр'] ?></div>
                        <div class="company-item__text"><span class="green-mark">По <?= $s ?></span></div>
                        <div class="company-item-info"><?= $item['post'] ?> <?= $item['fio'] ?></div>
                        <address class="company-item__text"><span class="yellow-mark"><?= $item['Адрес'] ?></span></address>
                        <?php if ($item['byFounder'] === true) { ?>
                            <div class="company-item-info">Учредитель <?= $item['fio'] ?></div>
                        <?php } ?>
                        <div class="company-item-info" style="margin-top:5px;">
                            <?php if (isset($item['ОГРН']) === true) { ?>
                                <dl>
                                    <dt>ОГРН</dt>
                                    <dd><?= $item['ОГРН'] ?></dd>
                                </dl>
                            <?php } ?>
                            <?php if (isset($item['ИНН']) === true) { ?>
                                <dl>
                                    <dt>ИНН</dt>
                                    <dd><?= $item['ИНН'] ?></dd>
                                </dl>
                            <?php } ?>
                            <?php if (isset($item['registrationDate']) === true) { ?>
                                <dl>
                                    <dt>Дата регистрации</dt>
                                    <dd><?= Yii::$app->formatter->asDate($item['registrationDate'], 'long') ?></dd>
                                </dl>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>
<script>
    $('.director').change(function () {
        let items = $('.company-item');
        if (this.value !== '') {
            items.addClass('hidden');
            let className = '.fl-director-' + this.value;
            items = items.filter(className);
        }
        items.removeClass('hidden');
    });
    $('.founder, .director').change(function () {
        let items = $('.company-item');
        if (this.value !== '') {
            items.addClass('hidden');
            let className = '.fl-founder-' + this.value;
            items = items.filter(className);
        }
        items.removeClass('hidden');
    });
</script>