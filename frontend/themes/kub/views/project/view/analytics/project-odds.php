<?php
use frontend\modules\analytics\models\OddsSearch;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\project\Project;

/////////////////////////////////////////////////////////////////////
$ODDS_PROJECT_ID = $project->id;
$ODDS_PLOJECT_FILTER = ['filters' => ['project_id' => $ODDS_PROJECT_ID]];
/////////////////////////////////////////////////////////////////////

/** @var Project $project */

// actionOdds()
$periodSize = Yii::$app->request->get('periodSize', 'months');
$searchModel = new OddsSearch();
$searchModel->year = $year;
$data = ($periodSize == 'days') ?
    $searchModel->searchByDays($activeTab, $ODDS_PLOJECT_FILTER) :
    $searchModel->search($activeTab, $ODDS_PLOJECT_FILTER);

$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);

// odds2.php
$userConfig = Yii::$app->user->identity->config;
if ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE_DETAILED) {
    $viewTable = '@frontend/themes/kub/modules/analytics/views/finance/_partial/odds2_table_detailed';
} else {
    $viewTable = '@frontend/themes/kub/modules/analytics/views/finance/_partial/odds2_table';
}


?>

    <div class="mt-2">
        <input type="hidden" id="isAnalyticsModule" value="1"/>
        <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
        <?= Html::hiddenInput('periodSize', $periodSize, ['id' => 'active-period-size']); ?>
        <?= Html::hiddenInput('projectId', $project->id, ['id' => 'odds-project-id']); ?>
    </div>

    <div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
        <div class="d-flex flex-nowrap">

            <?php if ($activeTab == OddsSearch::TAB_ODDS): ?>
                <div class="ml-0 mr-1">
                    <?= TableConfigWidget::widget([
                        'mainTitle' => 'Выводить',
                        'items' => [
                            ['attribute' => 'report_odds_row_finance', 'refresh-page' => true],
                            ['attribute' => 'report_odds_row_investments', 'refresh-page' => true],
                            ['attribute' => 'report_odds_own_funds', 'refresh-page' => true]
                        ]
                    ]); ?>
                </div>
            <?php else: ?>
                <?= TableConfigWidget::widget([
                    'mainTitle' => 'Выводить',
                    'items' => [
                        ['attribute' => 'report_odds_row_bank', 'refresh-page' => true],
                        ['attribute' => 'report_odds_row_order', 'refresh-page' => true],
                        ['attribute' => 'report_odds_row_emoney', 'refresh-page' => true],
                        ['attribute' => 'report_odds_row_acquiring', 'refresh-page' => true],
                        ['attribute' => 'report_odds_row_card', 'refresh-page' => true],
                    ],
                    'hideItems' => [
                        ['attribute' => 'report_odds_hide_zeroes', 'refresh-page' => true],
                    ]
                ]); ?>
            <?php endif; ?>

            <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
                <div class="ml-0 mr-1">
                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                        Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year, 'project_id' => $project->id]), [
                            'class' => 'download-odds-xls button-list button-hover-transparent button-clr ml-0 mb-2',
                            'title' => 'Скачать в Excel',
                        ]); ?>
                </div>
            <?php endif; ?>

            <div class="ml-2">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_odds']) ?>
            </div>

        </div>
        <div class="d-flex flex-nowrap ml-auto">
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'days') ? 'button-regular_red' : '' ?> pl-4 pr-4 mb-0" href="<?= Url::current(['periodSize' => 'days']) ?>">День</a>
            </div>
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'months') ? 'button-regular_red' : '' ?> pl-3 pr-3 mb-0" href="<?= Url::current(['periodSize' => 'months']) ?>">Месяц</a>
            </div>

            <div class="radio_join mb-2 ml-2" style="width: 240px">
                <div class="ml-1">
                    <?= Select2::widget([
                        'id' => 'oddssearch-tab',
                        'name' => 'oddssearch-tab',
                        'value' => $activeTab,
                        'data' => [
                            // todo
                            OddsSearch::TAB_ODDS => 'По видам деятельности',
                            OddsSearch::TAB_ODDS_BY_PURSE => 'По типам кошельков',
                            OddsSearch::TAB_ODDS_BY_PURSE_DETAILED => 'По кошелькам'
                        ],
                        'options' => [
                            'options' => [
                                OddsSearch::TAB_ODDS => ['data-url' => Url::current(['activeTab' => OddsSearch::TAB_ODDS])],
                                OddsSearch::TAB_ODDS_BY_PURSE => ['data-url' => Url::current(['activeTab' => OddsSearch::TAB_ODDS_BY_PURSE])],
                                OddsSearch::TAB_ODDS_BY_PURSE_DETAILED => ['data-url' => Url::current(['activeTab' => OddsSearch::TAB_ODDS_BY_PURSE_DETAILED])]
                            ],
                            'onchange' => new \yii\web\JsExpression('location.href = this.options[this.selectedIndex].dataset.url')
                        ],
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>

            <div class="radio_join mb-2 ml-2" style="padding-left: 2px">
                <?= Html::beginForm(['odds', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $yearFilter,
                    'hideSearch' => true,
                    'options' => [
                        'data-url' => Url::current(['year' => null]),
                        'onchange' => new \yii\web\JsExpression('location.href = this.dataset.url + "&year=" + this.value'),
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'value' => $year
                ]); ?>
                <?= Html::endForm(); ?>
            </div>

        </div>
    </div>

    <div class="wrap wrap_padding_none_all" style="margin-bottom: 12px; position: relative">
        <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
        <?= Html::hiddenInput('periodSize', $periodSize, ['id' => 'active-period-size']); ?>

        <?= $this->render($viewTable . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'userConfig' => $userConfig,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'data' => $data['data'],
            'growingData' => $data['growingData'],
            'totalData' => $data['totalData'],
            'floorMap' => []
        ]); ?>

    </div>

<?= $this->render('@frontend/themes/kub/modules/analytics/views/finance/_partial/item_table', ['searchModel' => $searchModel]); ?>

<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial_operations_block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы) в компанию и их возврат. <br/>
        Выплата дивидендов. Предоставление займов и депозитов.<br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_operation_activities_block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br/>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)<br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_investment_activities_block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br/>
        Затраты на новые проекты и поступление выручки от них. <br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_own_funds_block" style="display: inline-block; text-align: center;">
        Перемещение денег между своими счетами. <br/>
        Тут отражаются операции со статьей <br/>
        "Перевод собственных средств". <br/>
        Итого по этому разделу всегда <br/>
        должно быть равно нулю
    </span>
    <span id="tooltip_net_cash_flow" style="display: inline-block; text-align: center;">
        "Чистый денежный поток" – это разница между всеми приходами<br/>
        и всеми расходами, за период указанный в столбце<br/>
        (день, месяц, квартал, год)
    </span>
    <span id="tooltip_block_bank" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Банк
    </span>
    <span id="tooltip_block_order" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Касса
    </span>
    <span id="tooltip_block_emoney" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу E-money
    </span>
    <span id="tooltip_block_acquiring" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Интернет-эквайринг
    </span>
    <span id="tooltip_block_card" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Карты
    </span>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper"></div>
</div>

<script>

    FactItemTable = {
        projectID: <?= $ODDS_PROJECT_ID ?>,
        init: function () {
            FactItemTable.bindEvents();
        },
        bindEvents: function() {
            // delete
            $(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
                let l = Ladda.create(this);
                l.start();

                $.get($(this).data('url'), null, function(data) {
                    FactItemTable.reloadAll(function() {
                        Ladda.stopAll();
                        l.remove();
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash(data.msg);
                    });
                });

                return false;
            });

            //update
            $(document).on('submit', '#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form, #operationUpdateForm', function(e) {
                e.preventDefault();

                $(this).prepend('<input type="hidden" name="fromOdds" value="1">');

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    //let l = Ladda.create(submitBtn);
                    //l.start();
                    FactItemTable.reloadAll(function() {
                        Ladda.stopAll();
                        //l.remove();
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash('Операция обновлена.'); // todo: show errors
                    });
                });

                return false;
            });

            // many delete
            $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                let $this = $(this);
                let l = Ladda.create(this);
                l.start();

                if (!$this.hasClass('clicked')) {
                    if ($('.joint-checkbox:checked').length > 0) {
                        $this.addClass('clicked');
                        $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {
                            FactItemTable.reloadAll(function() {
                                Ladda.stopAll();
                                l.remove();
                                $('.modal:visible').modal('hide');
                                FactItemTable.showFlash(data.msg);
                                $this.removeClass('clicked');
                            });
                        });
                    }
                }
                return false;
            });

            // many item (update articles)
            $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                e.preventDefault();

                var l = Ladda.create($(this).find(".btn-save")[0]);
                var $hasError = false;

                l.start();
                $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                    if ($(this).find("select").val() == "") {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    FactItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash(data.msg);
                        Ladda.stopAll();
                        l.remove();
                    });
                });

                return false;
            });

            $(document).on("shown.bs.modal", "#many-item", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);
                var $header = $modal.find(".modal-header h1");
                var $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                });
            });

            $(document).on("hidden.bs.modal", "#many-item", function () {
                $(".expenditure-item-block").addClass("hidden");
                $(".income-item-block").addClass("hidden");
                $(".additional-header-text").remove();
                $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
            });


            // many cashbox (update cashbox)
            $(document).on('click', '#many-change-cashbox-ajax', function(e) {
                e.preventDefault();

                var modal = $(this).closest('.modal');
                var $hasError = false;
                var data = $('.joint-checkbox, .operation-many-cashbox-field').serialize();
                var l = Ladda.create(this);

                l.start();

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                $.post($(this).data('url'), data, function (data) {
                    FactItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            $(document).on("shown.bs.modal", "#many-cashbox", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);

                if ($includeExpenditureItem) {
                    $(".expense-block", $modal).removeClass("hidden");
                } else {
                    $(".expense-block", $modal).addClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-block", $modal).removeClass("hidden");
                } else {
                    $(".income-block", $modal).addClass("hidden");
                }
            });

            $(document).on("hide.bs.modal", "#many-cashbox", function () {
                var modal = $(this);
                $(".income-block, .expense-block", modal).addClass("hidden");
            });

            // show/hide "change cashbox" button
            $(document).on('change', '.joint-checkbox', function() {
                const table = $(this).closest('table');
                const checkboxes = table.find('.joint-checkbox:checked');
                const summaryContainer = $('#summary-container');
                let showCashboxBtn = true;
                if (checkboxes.length) {
                    $(checkboxes).each(function(i,v){
                        if ($(v).attr('name').indexOf('cash_order_flows') === -1)
                            showCashboxBtn = false;
                    });
                } else {
                    showCashboxBtn = false;
                }

                if (showCashboxBtn) {
                    $(summaryContainer).find('.dropdown-item-cashbox').removeClass('hidden');
                } else {
                    $(summaryContainer).find('.dropdown-item-cashbox').addClass('hidden');
                }
            });

            // many recognition date
            $(document).on("shown.bs.modal", "#many-recognition-date", function () {

                let checkedCheckboxes = $('.joint-checkbox:checked');
                let $includeExpenditureItem = $(checkedCheckboxes).filter('.expense-item').length > 0;
                let $includeIncomeItem = $(checkedCheckboxes).filter('.income-item').length > 0;
                let $modal = $(this);
                let $header = $modal.find("h4.modal-title");

                if ($includeExpenditureItem) {
                    $header.html('Изменить дату признания расхода');
                } else if ($includeIncomeItem) {
                    $header.html('Изменить дату признания дохода');
                }

                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form").prepend($(this).clone().hide());
                });
            });

            // many recognition date
            $(document).on("hidden.bs.modal", "#many-recognition-date", function () {
                $(".additional-header-text").remove();
                $(".modal#many-recognition-date form .joint-checkbox").remove();
            });

            // many recognition date (update dates)
            $(document).on('submit', '#js-cash_flow_update_date_form', function(e) {

                e.preventDefault();

                let l = Ladda.create($(this).find(".btn-save")[0]);
                let $hasError = false;

                l.start();
                $(".js-recognition_date_wrapper:visible").each(function () {

                    let input = $(this).find("input").val();

                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");

                    if (!input.match(/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1-9]|1[0-2])\.(20[0-9][0-9])$/)) {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    FactItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            // many project, industry, salepoint
            $(document).on('click', '.modal-many-change-project, .modal-many-change-sale-point, .modal-many-change-company-industry', function (e) {

                e.preventDefault();

                let l = Ladda.create(this);
                l.start();
                $.post($(this).data('url'), $('.joint-checkbox, #operation-many-project, #operation-many-sale-point, #operation-many-company-industry').serialize(), function (data) {
                    FactItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        FactItemTable.showFlash(data.message);
                        Ladda.stopAll();
                        $('#ajax-loading').hide();
                    });
                });

                return false;
            });

            $(document).on("show.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {

                let defaultVal = null;
                let defaults = {industry: [], sale_point: [], project: []};
                const unique = function(array){
                    return array.filter(function(el, index, arr) {
                        return index === arr.indexOf(el);
                    });
                };

                $('.joint-checkbox:checked').each(function(i,v) {
                    defaults.industry.push($(v).data('industry') || 0);
                    defaults.sale_point.push($(v).data('sale_point') || 0);
                    defaults.project.push($(v).data('project') || 0);
                });

                switch ($(this).attr('id')) {
                    case 'many-company-industry':
                        defaultVal = (unique(defaults.industry).length === 1) ? defaults.industry[0] : 0;
                        break;
                    case 'many-sale-point':
                        defaultVal = (unique(defaults.sale_point).length === 1) ? defaults.sale_point[0] : 0;
                        break;
                    case 'many-project':
                        defaultVal = (unique(defaults.project).length === 1) ? defaults.project[0] : 0;
                        break;
                }

                $(this).find('select').val(defaultVal).trigger('change');
            });

            $(document).on("hidden.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {
                $(this).find('select').val(null).trigger('change');
            });
        },
        reloadAll: function(callback)
        {
            let debugCurrTime = Date.now();
            // todo
            //return FactItemTable._reloadChart().done(function() {
            //    console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
            return FactItemTable._reloadSubTable().done(function() {
                    console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                    FactItemTable._reloadMainTable().done(function() {
                        console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        if (typeof callback === "function") {
                            return callback();
                        }

                    }).catch(function() { FactItemTable.showFlash('Ошибка сервера #1') });
                }).catch(function() { FactItemTable.showFlash('Ошибка сервера #2') });
            //}).catch(function() { FactItemTable.showFlash('Ошибка сервера #3') });
        },
        _reloadSubTable: function()
        {
            const $year = "OddsSearch%5Byear%5D=" + $('#oddssearch-year').val();
            $itemIDs['activeTab'] = $('#active-tab_report').val(); // todo: var from custom.js

            return jQuery.pjax({
                url: '/project/odds-item-list?' + $year + '&project_id=' + this.projectID,
                type: 'POST',
                data: {'items': JSON.stringify($itemIDs)},
                container: '#odds-items_pjax',
                timeout: 10000,
                push: false,
                scrollTo: false
            });
        },
        _reloadMainTable: function ()
        {
            let floorMap = {};
            $('table.flow-of-funds').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-days]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            let data = {
                // OddsSearch: {year: $('#OddsSearch-year').val()},
                floorMap: floorMap
            };
            let activeTab = $('#active-tab_report').val();
            let periodSize = $('#active-period-size').val();

            return $.post('/project/odds-part/?activeTab=' + activeTab + '&periodSize=' + periodSize + '&project_id=' + this.projectID, data, function ($data) {
                let html = new DOMParser().parseFromString($data, "text/html");
                let table = html.querySelector('table.flow-of-funds tbody');
                let origTable = document.querySelector('table.flow-of-funds tbody');
                origTable.innerHTML = table.innerHTML;

                if ($coloredItemsCoords) {
                    $.each($coloredItemsCoords, function(i,v) {
                        $('table.flow-of-funds tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                    });
                }

                FactItemTable._bindMainTableEvents();
            });
        },
        _reloadChart: function() {
            return window.ChartPlanFactDays.redrawByClick();
        },
        _bindMainTableEvents: function() {

            // main.js
            $('[data-collapse-row-trigger]', 'table.flow-of-funds tbody').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    // level 1
                    $('[data-id="'+target+'"]').addClass('d-none');
                    $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        // level 2
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                    $('[data-collapse-all-trigger]').removeClass('active');
                } else {
                    $('[data-collapse-all-trigger]').addClass('active');
                }
            });
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    /////////////////////
    FactItemTable.init();
    /////////////////////

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>

<?php if ($periodSize == 'days') {
    $this->registerJs(<<<JS

    var _activeMCS = 0;
    var _offsetMCSLeft = 0;
    var _firstCSTableColumn = $('#cs-table-first-column');

    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS == 1)
                    window._activeMCS = 0;

                if (this.mcs.left == 0 && $(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: true,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                this.scrollInertia = 1000;
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: true,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $("[data-collapse-trigger-days]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 250);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).data('columns-count') || 3;

            $(collapseBtn).toggleClass('active');
            $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            if ( $(collapseBtn).hasClass('active') ) {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
            } else {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
            }
            $(collapseBtn).closest('.custom-scroll-table').mCustomScrollbar("update");
        };

        _collapseToggle(collapseBtn);

    });

    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width());
        $("#cs-table-2").mCustomScrollbar("update");
    });

JS
    );
} ?>

<?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);
?>