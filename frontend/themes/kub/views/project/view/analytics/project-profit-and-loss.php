<?php

use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use yii\helpers\ArrayHelper;

/** @var \common\models\project\Project $project */

/////////////////////////////////////////////////////////////////////
$PAL_PROJECT_ID = $project->id;
$PAL_PLOJECT_FILTER = ['pageFilters' => ['project_id' => $PAL_PROJECT_ID]];
/////////////////////////////////////////////////////////////////////

ProfitAndLossSearchModel::refreshAnalyticsArticles();

$_get = Yii::$app->request->get();
$activeTab = (string)ArrayHelper::getValue($_get, 'activeTab', ProfitAndLossSearchModel::TAB_ALL_OPERATIONS);

$searchModel = new ProfitAndLossSearchModel($PAL_PLOJECT_FILTER);
$searchModel->activeTab = $activeTab;
$searchModel->year = $year;
$searchModel->setYearFilter($yearFilter);
$searchModel->disableMultiCompanyMode();
$searchModel->load(Yii::$app->request->get());
$data = $searchModel->handleItems();
?>

<div style="margin-top: 12px">
    <?= $this->render('@frontend/modules/analytics/views/finance/profit-and-loss', [
        'searchModel' => $searchModel,
        'pageFilter' => $PAL_PLOJECT_FILTER['pageFilters'],
        'hideSecondLine' => true,
        'activeTab' => $activeTab,
        'data' => $data,
        'show' => [
            'text' => [
                'title' => $project->name,
            ],
            'button' => [
                'multicompany' => 0,
                'chart' => 1,
                'help' => 0,
                'video' => 0,
                'options' => 1,
                'excel' => 0,
                'table_view' => 1,
                'month_group_by' => 0
            ],
            'modal_options' => [
                'part_articles' => 0,
                'part_additional' => 1,
                'part_revenue_analytics' => 0,
                'part_bottom_text' => 0
            ],
            'select' => [
                'year' => 1
            ]
        ],
    ]) ?>
</div>