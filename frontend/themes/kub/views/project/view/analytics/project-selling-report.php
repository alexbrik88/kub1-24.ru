<?php

use common\models\project\Project;
use frontend\modules\analytics\models\SellingReportSearch2;

/** @var Project $project */
//////////////////////////////////////////
$searchModel = new SellingReportSearch2();
$searchModel->project_id = $project->id;
$searchModel->setYearFilter($yearFilter);
$searchModel->year = $year;
//////////////////////////////////////////
try {
    $searchModel->searchBy = (int)Yii::$app->user->identity->config->selling_report_search_by;
    $searchModel->type = (int)Yii::$app->user->identity->config->selling_report_type;
    $searchModel->groupByContractor = (bool)Yii::$app->user->identity->config->selling_report_by_contractor;
} catch (\Throwable $e) {
    $searchModel->searchBy = SellingReportSearch2::BY_INVOICES;
    $searchModel->type = SellingReportSearch2::TYPE_ALL;
    $searchModel->groupByContractor = false;
}
?>

<div class="project-selling-report">
    <?= $this->render('@frontend/modules/analytics/views/selling-report/index', [
        'searchModel' => $searchModel,
        'dataProvider' => $searchModel->search(Yii::$app->request->get()),
        'showMenu' => false,
        'projectId' => $project->id,
    ]) ?>
</div>