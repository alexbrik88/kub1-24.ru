<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectCustomerSearch;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $model Project */

$searchModel = new ProjectCustomerSearch();
//$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

$emptyMessage = 'Вы еще не добавили заказчика.';

?>

<div class="wrap wrap_padding_small mt-3 pl-4 pr-4 pt-2 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div id="projectCustomers" class="row page-in">
            <div class="pt-3 col-12">
                <?= $this->render('../partial/_basis_customer_table', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    //'dataProvider' => $dataProvider,
                    'emptyMessage' => $emptyMessage,
                    'update' => false,
                    'showPayments' => true,
                    'showCheckboxes' => true
                ]); ?>
            </div>
        </div>
    </div>
</div>
