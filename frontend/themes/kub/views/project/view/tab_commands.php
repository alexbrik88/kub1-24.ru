<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\project\ProjectEmployee;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\project\Project */
/* @var $company \common\models\Company */

?>


<div class="wrap mt-3">
    <div class="pb-3">
        <div class="form-group col-5 ml-0 pl-0">
            <div class="label col-12">
                Ответственный
            </div>
            <?php
                $employee = Employee::find()
                    ->where(['id' => $model->responsible, 'company_id' => $company->id])
                    ->one();
            ?>
            <?= $employee->fio ?? '---' ?>
        </div>
        <?php
            $employers = ProjectEmployee::find()
                ->where(['project_id' => $model->id, 'company_id' => $company->id])
                ->all();
            foreach ($employers as $key => $empl):
        ?>
                <div class="form-group col-5 ml-0 pl-0">
                    <div class="label col-12">
                        Сотрудник <?= ($key + 1); ?>
                    </div>
                    <?php
                        $employee = Employee::find()
                            ->where(['id' => $empl->employee_id, 'company_id' => $company->id])
                            ->one();
                    ?>
                    <?= $employee->fio ?? '---' ?>
                </div>
        <?php
            endforeach;
        ?>
    </div>
</div>