<?php

use common\models\Company;
use common\models\project\Project;
use frontend\models\project\RepositoryFactory;
use frontend\modules\crm\widgets\PjaxWidget;
use yii\web\View;

/* @var $this View */
/* @var $model Project */
/* @var $company Company */

?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?= $this->render('tasks/tasks-grid', [
        'repository' => (new RepositoryFactory())->createProjectTaskRepository($model->id),
        'hasClient' => false,
]) ?>

<?php PjaxWidget::end(); ?>
