<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\models\Documents;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\rbac\permissions\Cash;
use \frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use frontend\rbac\permissions;
use yii\helpers\Url;
use common\models\Company;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;
use common\models\cash\CashBankTinChildrenHelper as TinChildrenHelper;

/** @var ActiveDataProvider $dataProvider */
/** @var CashFlowsProjectSearch $searchModel */
/** @var Project $model */
/** @var integer $type */
/** @var array $tabViewClass */
/** @var array $tabConfigClass */
/** @var Company $company */

$company = $model->company;

$canCreate = $canUpdate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate &&
    Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate &&
    SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate &&
    CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$cashTables = [
    CashFlowsProjectSearch::TYPE_PAYMENT_BANK => CashBankFlows::tableName(),
    CashFlowsProjectSearch::TYPE_PAYMENT_ORDER => CashOrderFlows::tableName(),
    CashFlowsProjectSearch::TYPE_PAYMENT_EMONEY => CashEmoneyFlows::tableName(),
    CashFlowsProjectSearch::TYPE_PAYMENT_ACQUIRING => AcquiringOperation::tableName(),
    CashFlowsProjectSearch::TYPE_PAYMENT_CARD => CardOperation::tableName(),
    CashFlowsProjectSearch::TYPE_PLAN => PlanCashFlows::tableName()
];

$slugs = [
    CashFlowsProjectSearch::TYPE_PAYMENT_BANK => 'bank',
    CashFlowsProjectSearch::TYPE_PAYMENT_ORDER => 'order',
    CashFlowsProjectSearch::TYPE_PAYMENT_EMONEY => 'e-money',
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => 'В указанном периоде нет операций',
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('/layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($slugs, $cashTables) {

                // todo
                $flows['is_internal_transfer'] = null;
                $flows['tb'] = $cashTables[$flows['type']] ?? null;

                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $typeCss = 'income-item';
                    $income = bcdiv($flows['amount'], 100, 2);
                    $expense = 0;
                } else {
                    $typeCss = 'expense-item';
                    $income = 0;
                    $expense = bcdiv($flows['amount'], 100, 2);
                }

                $copyUrl = isset($slugs[$flows['type']])
                    ? Url::to(['/cash/'.$slugs[$flows['type']].'/create-copy', 'id' => $flows['id'], 'redirect' => Url::current()])
                    : null;

                return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                    'class' => 'joint-operation-checkbox ' . $typeCss,
                    'value' => $flows['id'],
                    'data' => [
                        'type' => $flows['type'],
                        'income' => $income,
                        'expense' => $expense,
                        'copy-url' => $copyUrl
                    ],
                ]);
            },
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'max-width:50px'
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'recognition_date',
            'label' => 'Дата признания',
            'headerOptions' => [
                'class' => 'sorting ' . $tabConfigClass['recognitionDate'],
                'width' => '10%',
                'style' => 'max-width:50px'
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['recognitionDate']
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'amountIncomeExpense',
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => $tabConfigClass['incomeExpense'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($cashTables) {

                $income = '+ ' . TextHelper::invoiceMoneyFormat($flows['amountIncome'] ?: 0, 2);
                $expense = '- ' . TextHelper::invoiceMoneyFormat($flows['amountExpense'] ?: 0, 2);

                // todo
                $flows['is_internal_transfer'] = null;
                $flows['tb'] = $cashTables[$flows['type']] ?? null;

                if ($flows['amountIncome'] > 0) {
                    $link = CashSearch::getUpdateFlowLink($income, $flows);
                    //if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                    //    $link .= $pieces;
                    return Html::tag('div', $link, ['class' => 'green-link']);
                }
                if ($flows['amountExpense'] > 0) {
                    $link = CashSearch::getUpdateFlowLink($expense, $flows);
                    //if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                    //    $link .= $pieces;
                    return Html::tag('div', $link, ['class' => 'red-link']);
                }

                return '';
            },
        ],
        [
            'attribute' => 'amountIncome',
            'label' => 'Приход',
            'headerOptions' => [
                'class' => $tabConfigClass['income'],
                'width' => '10%',
                'style' => 'max-width:50px;'
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
            ],
            'format' => 'raw',
            'value' => function ($flows) {
                $formattedAmount = TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2);
                return (is_numeric($flows['amountIncome']) && $flows['amountIncome'] > 0)
                    ? Html::tag('span', $formattedAmount, ['class' => $flows['type'] == 'plan' ? 'plan':''])
                    : '-';
            },
        ],
        [
            'attribute' => 'amountExpense',
            'label' => 'Расход',
            'headerOptions' => [
                'class' => $tabConfigClass['expense'],
                'width' => '10%',
                'style' => 'max-width:50px;'

            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
            ],
            'format' => 'raw',
            'value' => function ($flows) {
                $formattedAmount = TextHelper::invoiceMoneyFormat($flows['amountExpense'] ?: 0, 2);
                return (is_numeric($flows['amountExpense']) && $flows['amountExpense'] > 0)
                    ? Html::tag('span', $formattedAmount, ['class' => $flows['type'] == 'plan' ? 'plan':''])
                    : '-';
            },
        ],
        [
            'attribute' => 'type',
            'label' => 'Тип оплаты',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'max-width:50px;'
            ],
            'filter' => [
                '' => 'Все оплаты',
                CashFlowsProjectSearch::TYPE_PAYMENT_BANK => 'Банк',
                CashFlowsProjectSearch::TYPE_PAYMENT_ORDER => 'Касса',
                CashFlowsProjectSearch::TYPE_PAYMENT_EMONEY => 'E-money',
                CashFlowsProjectSearch::TYPE_PAYMENT_ACQUIRING => 'Эквайринг',
                CashFlowsProjectSearch::TYPE_PAYMENT_CARD => 'Карты',
            ],
            'value' => function ($flows) {
                switch ($flows['type']) {
                    case CashFlowsProjectSearch::TYPE_PAYMENT_BANK:
                        return 'Банк';
                    case CashFlowsProjectSearch::TYPE_PAYMENT_ORDER:
                        return 'Касса';
                    case CashFlowsProjectSearch::TYPE_PAYMENT_EMONEY:
                        return 'E-money';
                    case CashFlowsProjectSearch::TYPE_PAYMENT_ACQUIRING:
                        return 'Эквайринг';
                    case CashFlowsProjectSearch::TYPE_PAYMENT_CARD:
                        return 'Карты';
                    case CashFlowsProjectSearch::TYPE_PLAN:
                        switch ($flows['wallet_id']) {
                            case 'plan_'.CashFlowsProjectSearch::TYPE_PAYMENT_BANK:
                                return 'Банк';
                            case 'plan_'.CashFlowsProjectSearch::TYPE_PAYMENT_ORDER:
                                return 'Касса';
                            case 'plan_'.CashFlowsProjectSearch::TYPE_PAYMENT_EMONEY:
                                return 'E-money';
                        }

                }
                return '';
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'format' => 'html',
            'filter' => $searchModel->getContractorFilterItems(),
            'value' => function ($flows) {
                $contractor = Contractor::find()
                    ->andWhere(['id' => $flows['contractor_id']])
                    ->one();
                if ($contractor) {
                    return Html::a(Html::encode($contractor->nameWithType), [
                        '/contractor/view',
                        'type' => $contractor->type,
                        'id' => $contractor->id,
                    ], ['target' => '_blank']);
                } else {
                    $contractor = CashContractorType::findOne(['name' => $flows['contractor_id']]);
                    return Html::tag('span', $contractor->text);
                }
            },
            'hideSearch' => false,
            's2width' => '250px'
        ],
        [
            's2width' => '200px',
            'attribute' => 'project_id',
            'label' => 'Проект',
            'headerOptions' => [
                'class' => $tabConfigClass['project'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
            ],
            'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                    $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'project_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'sale_point_id',
            'label' => 'Точка продаж',
            'headerOptions' => [
                'class' => $tabConfigClass['salePoint'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
            ],
            'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                    $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'sale_point_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'industry_id',
            'label' => 'Направление',
            'headerOptions' => [
                'class' => $tabConfigClass['companyIndustry'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
            ],
            'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                    $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'industry_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            'attribute' => 'description',
            'label' => 'Назначение',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell',
            ],
            'format' => 'html',
            'value' => function ($flows) {
                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
            },
        ],
        [
            'attribute' => 'billPaying',
            'label' => 'Опл. счета',
            'headerOptions' => [
                'class' => $tabConfigClass['billPaying'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['billPaying'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($cashTables) {

                //todo
                $flows['is_internal_transfer'] = null;
                $flows['tb'] = $cashTables[$flows['type']] ?? null;

                switch ($flows['tb']) {
                    case CashBankFlows::tableName():
                        $model = CashBankFlows::findOne($flows['id']);
                        break;
                    case CashOrderFlows::tableName():
                        $model = CashOrderFlows::findOne($flows['id']);
                        break;
                    case CashEmoneyFlows::tableName():
                        $model = CashEmoneyFlows::findOne($flows['id']);
                        break;
                    case PlanCashFlows::tableName():
                        $model = PlanCashFlows::findOne($flows['id']);
                        $currDate = date('Y-m-d');
                        if ($model->first_date < $currDate && $model->date != $model->first_date)
                            return 'Перенос';
                        elseif ($model->date >= $currDate)
                            return 'План';
                        elseif ($model->date < $currDate)
                            return 'Просрочен';
                        else
                            return '';
                    default:
                        return '';
                }

                if ($model->credit_id) {
                    return $model->getCreditPaying();
                }

                $invoicesList = $model->getBillPaying(false);

                if ($model->getAvailableAmount() > 0) {
                    return ($invoicesList ? ($invoicesList . '<br />') : '') .
                        Html::a('<span class="red-link">Уточнить</span>',
                            CashSearch::getUpdateFlowLink(false, $flows, false),
                            [
                                'class' => 'update-flow-item red-link',
                                'title' => 'Прикрепите счета, которые были оплачены',
                                'data' => [
                                    'toggle' => 'modal',
                                    'target' => '#update-movement',
                                    'pjax' => 0
                                ],
                            ]);
                } else {
                    return $invoicesList;
                }
            },
        ],
        [
            'attribute' => 'reason_id',
            'label' => 'Статья',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'clause-cell',
            ],
            'filter' => ['' => 'Все статьи', 'empty' => '-'] + $searchModel->getReasonFilterItems(),
            'hideSearch' => false,
            'value' => function ($flows) {
                $reason = "";

                if (isset($flows['expenseName'])) {
                    $reason = $flows['expenseName'];
                } else if (isset($flows['incomeName'])) {
                    $reason = $flows['incomeName'];
                }

                return $reason;
            },
            's2width' => '200px'
        ],
        [
            'class' => ActionColumn::class,
            'template' => '<div class="table-count-cell" style="min-width:60px">{update}{delete}</div>',
            'headerOptions' => [
                'width' => '5%',
                'style' => 'max-width:60px'
            ],
            'visible' => Yii::$app->user->can(Cash::DELETE),
            'buttons' => [
                'update' => function ($url, $flows, $key) use ($cashTables) {

                    $flows['is_internal_transfer'] = null;
                    $flows['tb'] = $cashTables[$flows['type']] ?? null;

                    $url = CashSearch::getUpdateFlowLink(false, $flows, false);

                    return \yii\helpers\Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', $url, [
                        'title' => 'Изменить',
                        'class' => 'update-flow-item button-clr link mr-1',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#update-movement',
                            'pjax' => 0
                        ],
                    ]);
                },
                'delete' => function ($url, $flows) use ($cashTables) {

                    $flows['is_internal_transfer'] = false;
                    $flows['tb'] = $cashTables[$flows['type']] ?? null;

                    $url = CashSearch::getDeleteFlowLink($flows);

                    return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                            'class' => 'delete-flow-item button-clr link',
                            'tag' => 'a',
                        ],
                        'options' => [
                            'id' => 'delete-flow-item-' . $flows['id'],
                            'class' => 'modal-delete-flow-item',
                        ],
                        'confirmUrl' => $url,
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить операцию?',
                    ]);
                },
            ],
        ],
    ],
]);
?>
<?= SummarySelectWidget::widget([
    'buttonsViewType' => ($canProjectUpdate || $canSalePointUpdate || $canIndustryUpdate) ? SummarySelectWidget::VIEW_DROPDOWN : SummarySelectWidget::VIEW_INLINE,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ]
]); ?>

<?= $this->render('_partial/modal-update-flow') ?>
<?= $this->render('_partial/modal-many-delete', [
    'model' => $model,
]); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
    'bank' => (new CashBankFlows()),
    'order' => (new CashOrderFlows()),
    'emoney' => (new CashEmoneyFlows()),
]); ?>

<?= $this->render('../payment/_modal', [
    'model' => $model,
]); ?>

<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-project'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-sale-point'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-company-industry'); ?>
