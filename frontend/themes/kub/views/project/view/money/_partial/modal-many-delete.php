<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\web\View;

?>

<!-- Modal Many Delete -->
<div id="many-delete" class="modal-many-delete-item confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                    'data-style' => 'expand-right',
                    'data-url' => Url::to(['/cash/default/many-delete-flow-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
// confirm many delete
$(document).on("click", ".modal-many-delete-item .btn-confirm-yes", function() {
    if (!$(this).hasClass('clicked')) {
        if ($('.joint-operation-checkbox:checked').length > 0) {
            $(this).addClass('clicked');
            $.post($(this).data('url'), $('.joint-operation-checkbox').serialize(), function(data) {});
        }
    }
});
JS, View::POS_READY
);
