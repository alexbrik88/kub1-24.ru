<!-- Modal Update Flow -->
<div id="update-movement" class="modal fade modal-update-flow" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 id="js-modal_update_title" class="modal-title">Изменить</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
// update show
$(document).on("show.bs.modal", "#update-movement", function(event) {

    const button = $(event.relatedTarget);
    const modal = $(this);

    $.ajax({
        type: "get",
        url: button.attr("href"),
        data: {},
        success: function(data) {
            data = data.replace(/<link href=\"[^\"]+\" rel=\"stylesheet\">/g, '');
            modal.find(".modal-body").append(data);
        }
    });
});

// update hide
$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
        $("#js-modal_update_title").html("Изменить");
    }
});

// confirm delete
$(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
    let l = Ladda.create(this);
    l.start();
    $.post($(this).data('url'), null, function(data) {});
});

JS
);