<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\models\cash\CashBankFlows;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\web\View;

?>

    <div class="modal fade" id="many-item" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="modal-title mb-4">Изменить статью</h4>
                    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                        </svg>
                    </button>
                    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                        'action' => Url::to(['project/change-many-item']),
                        'options' => [
                            'class' => 'form-horizontal',
                        ],
                        'id' => 'js-cash_flow_update_item_form',
                    ])); ?>
                    <div class="form-body">
                        <div class="item-block income bank hidden">
                            <div class="row col-6">
                                <label class="col-12 ml-0 pl-0" for="cashflowsform-flow_type">
                                    Для типа
                                </label>
                                <div class="col-12 ml-0 pl-0">
                                    <div id="cashflowsform-flow_type" aria-required="true">
                                        <div class="">
                                            <?= Html::radio(null, true, [
                                                'label' => 'Приход',
                                                'labelOptions' => [
                                                    'class' => 'radio-txt-bold font-14',
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <?= $form->field($bank, 'incomeItemIdManyItem', [
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                        'wrapperOptions' => [
                                            'class' => '',
                                        ],
                                        'options' => [
                                            'id' => 'js-income_bank_item_id_wrapper',
                                            'class' => 'form-group col-12 mt-3 ml-0 pl-0',
                                        ],
                                    ])
                                    ->widget(ExpenditureDropdownWidget::classname(), [
                                        'income' => true,
                                        'options' => [
                                            'prompt' => '',
                                            'name' => 'incomeBankManyItem'
                                        ],
                                        'pluginOptions' => [
                                            'width' => '100%',
                                        ]
                                    ])
                                    ->label('Изменить статью прихода на:'); ?>

                            </div>
                        </div>

                        <div class="item-block income order hidden">
                            <div class="row col-6">
                                <label class="col-12 ml-0 pl-0" for="cashflowsform-flow_type">
                                    Для типа
                                </label>
                                <div class="col-12 ml-0 pl-0">
                                    <div id="cashflowsform-flow_type" aria-required="true">
                                        <div class="">
                                            <?= Html::radio(null, true, [
                                                'label' => 'Приход',
                                                'labelOptions' => [
                                                    'class' => 'radio-txt-bold font-14',
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>

                                <?= $form->field($order, 'incomeItemIdManyItem', [
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                        'wrapperOptions' => [
                                            'class' => '',
                                        ],
                                        'options' => [
                                            'id' => 'js-income_order_item_id_wrapper',
                                            'class' => 'form-group col-12 mt-3 ml-0 pl-0',
                                        ],
                                    ])
                                    ->widget(ExpenditureDropdownWidget::classname(), [
                                        'income' => true,
                                        'options' => [
                                            'prompt' => '',
                                            'name' => 'incomeItemOrderItem'
                                        ],
                                        'pluginOptions' => [
                                            'width' => '100%',
                                        ]
                                    ])
                                    ->label('Изменить статью прихода на:'); ?>

                            </div>
                        </div>
                        <div class="item-block expense bank hidden">
                            <div class="row col-6">
                                <label class="col-12 ml-0 pl-0" for="cashflowsform-flow_type">
                                    Для типа
                                </label>
                                <div class="col-12 ml-0 pl-0">
                                    <div id="cashflowsform-flow_type" aria-required="true">
                                        <div class="">
                                            <?= Html::radio(null, true, [
                                                'label' => 'Расход',
                                                'labelOptions' => [
                                                    'class' => 'radio-txt-bold font-14',
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <?= $form->field($bank, 'expenditureItemIdManyItem', [
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                        'wrapperOptions' => [
                                            'class' => '',
                                        ],
                                        'options' => [
                                            'id' => 'js-expenditure_bank_item_id_wrapper',
                                            'class' => 'form-group col-12 mt-3 ml-0 pl-0',
                                        ],
                                    ])
                                    ->widget(ExpenditureDropdownWidget::classname(), [
                                        'options' => [
                                            'prompt' => '',
                                            'name' => 'expenseBankItem',
                                        ],
                                        'pluginOptions' => [
                                            'width' => '100%',
                                        ]
                                    ])
                                    ->label('Изменить статью расхода на:'); ?>

                            </div>
                        </div>
                        <div class="item-block expense order hidden">
                            <div class="row col-6">
                                <label class="col-12 ml-0 pl-0" for="cashflowsform-flow_type">
                                    Для типа
                                </label>
                                <div class="col-12 ml-0 pl-0">
                                    <div id="cashflowsform-flow_type" aria-required="true">
                                        <div class="">
                                            <?= Html::radio(null, true, [
                                                'label' => 'Расход',
                                                'labelOptions' => [
                                                    'class' => 'radio-txt-bold font-14',
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>

                                <?= $form->field($order, 'expenditureItemIdManyItem', [
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                        'wrapperOptions' => [
                                            'class' => '',
                                        ],
                                        'options' => [
                                            'id' => 'js-expenditure_order_item_id_wrapper',
                                            'class' => 'form-group col-12 mt-3 ml-0 pl-0',
                                        ],
                                    ])
                                    ->widget(ExpenditureDropdownWidget::classname(), [
                                        'options' => [
                                            'prompt' => '',
                                            'name' => 'expenseOrderItem',
                                        ],
                                        'pluginOptions' => [
                                            'width' => '100%',
                                        ]
                                    ])
                                    ->label('Изменить статью расхода на:'); ?>
                            </div>
                        </div>
                        <div class="mt-3 d-flex justify-content-between">
                            <?= Html::submitButton('Сохранить', [
                                'class' => 'button-regular button-width button-regular_red button-clr submit-item-button',
                                'style' => 'width: 130px!important;',
                                'data' => [
                                    'url' => 'change-many-item',
                                    'project-id' => $model->id
                                ]
                            ]); ?>
                            <button type="button"
                                    class="button-clr button-width button-regular button-hover-transparent"
                                    data-dismiss="modal">Отменить
                            </button>
                        </div>

                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php

$this->registerJs(<<<JS
    const TYPE_BANK = 1;
    const TYPE_ORDER = 2;
    
    var operationButton = $('.operation-item'),
        location = operationButton.attr('href'),
        data = '';
    
    $('.joint-operation-checkbox').change(function(e) {
        var operationSelected = $('.joint-operation-checkbox:checked'), 
            type = operationSelected.first().data('type'),
            income = operationSelected.first().data('income'),
            ioType = income === 0 ? 'expense' : 'income';
        
        operationButton.attr('href', location);
        operationButton.removeClass('disabled');
        
        data = 'type=' + (TYPE_BANK === type ? TYPE_BANK : TYPE_ORDER);
        
        operationSelected.each(function(index, item) {
            if (type !== $(item).data('type') 
                || (ioType === 'income' && $(item).data('income') === 0)
                || (ioType === 'expense' && $(item).data('expense') === 0)
            ) {
                operationButton.removeAttr('href');
                operationButton.addClass('disabled');
            }
            
            data += '&id[]=' + $(item).val();
        });
        
        if (!operationButton.hasClass('disabled')) {
            var visibleClass = TYPE_BANK === type ? 'bank' : 'order';
            $('.item-block').addClass('hidden');
            $('.item-block.'+ioType+'.'+visibleClass).removeClass('hidden');
        }
    });
    
    $('.submit-item-button').click(function(e){
        e.preventDefault();
        
        data += '&project_id=' + $(this).data('project-id');
        
        $.post($(this).data('url') + '?' + data, $('#js-cash_flow_update_item_form').serialize(), function(data){
            if ('true' === data.success) {
                window.location.href = data.href;
            }
        }, 'json');
    });
    
JS, View::POS_READY
);