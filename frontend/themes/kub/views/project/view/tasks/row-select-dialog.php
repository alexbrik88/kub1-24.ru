<?php

use frontend\modules\crm\assets\RowSelectAsset;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var string $label
 * @var string $input
 * @var string $title
 * @var string $id
 */

RowSelectAsset::register($this);

?>

<?php Modal::begin([
    'options' => ['id' => $id],
    'size' => Modal::SIZE_SMALL,
    'closeButton' => false,
    'title' => $title,
]); ?>

<div class="row">
    <div class="form-group col-12">
        <label class="label"><?= Html::encode($label) ?></label>
        <?= $input ?>
    </div>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Применить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>
