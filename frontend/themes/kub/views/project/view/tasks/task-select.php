<?php

use frontend\models\project\RepositoryFactory;
use frontend\models\project\ProjectTaskSelectFormFactory;
use frontend\themes\kub\assets\RowSelectAsset;
use frontend\models\project\ProjectTask;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 * @var int $project_id
 */

RowSelectAsset::register($this);

$form = (new ProjectTaskSelectFormFactory)->createForm();

?>

<?= Html::beginForm(Url::to(['project/many-update-task']), 'post', ['id' => 'rowSelectForm']); ?>

<?= $content ?>

<div id="rowSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано задач: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto"></div>
        <div class="column">
            <div class="dropup">
                <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                    'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                    'data-toggle' => 'dropdown',
                ]) ?>
                <?= Dropdown::widget([
                    'items' => [
                        [
                            'label' => 'Статус',
                            'url' => '#changeStatusDialog',
                            'linkOptions' => ['data-toggle' => 'modal'],
                        ],
                        [
                            'label' => 'Ответственный',
                            'url' => '#changeEmployeeDialog',
                            'linkOptions' => ['data-toggle' => 'modal'],
                        ],
                    ],
                    'options' => ['class' => 'form-filter-list list-clr dropdown-menu-right-sm'],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('row-select-dialog', [
    'id' => 'changeStatusDialog',
    'title' => 'Изменить статус',
    'label' => $form->getAttributeLabel('status'),
    'input' => Select2::widget([
        'model' => $form,
        'attribute' => 'status',
        'data' => ArrayHelper::merge([null => ''], ProjectTask::STATUS_LIST),
        'pluginOptions' => ['width' => '100%'],
        'hideSearch' => true,
    ]),
]) ?>

<?= $this->render('row-select-dialog', [
    'id' => 'changeEmployeeDialog',
    'title' => 'Назначить ответственного',
    'label' => $form->getAttributeLabel('employee_id'),
    'input' => Select2::widget([
        'model' => $form,
        'attribute' => 'employee_id',
        'data' => (new RepositoryFactory)->createProjectTaskRepository($project_id)->getResponsible(),
        'pluginOptions' => ['width' => '100%'],
        'hideSearch' => true,
    ]),
]) ?>

<?= Html::endForm() ?>
