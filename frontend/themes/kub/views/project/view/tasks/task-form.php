<?php

use common\components\date\DateHelper;
use common\models\project\Project;
use frontend\models\project\ProjectTaskForm;
use frontend\models\project\RepositoryFactory;
use frontend\models\project\ProjectTask;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $this
 * @var ProjectTaskForm $form
 * @var bool $hasProject
 */

$project = Project::findOne(['id' => $form->project_id]);

$taskNumber = ($hasProject && $form->scenario == ProjectTaskForm::SCENARIO_CREATE)
    ? ProjectTaskForm::getNextNumber($form->project_id)
    : $form->task_number;

?>

<?php $widget = ActiveForm::begin([
    'id' => 'taskForm',
    'action' => $form->task_id
        ? ['project/update-task', 'task_id' => $form->task_id]
        : ['project/create-task', 'project_id' => $form->project_id],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="row">

<?= $widget->field($form, 'task_number')->textInput(['value' => $taskNumber]) ?>

<?= $widget->field($form, 'task_name')->textInput() ?>

<?= $widget->field($form, 'project_id', ['options' => ['class' => 'form-group col-12']])
    ->widget(Select2::class, [
        'data' => [$project->id => $project->name],
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'readonly' => true,
            'value' => $project->id,
            'width' => '100%',
            'escapeMarkup' => new JsExpression("function(value) { return value; }"),
            'disabled' => $hasProject ? 'readonly' : null,
        ],
    ]) ?>

<?= $widget->field($form, 'status')->widget(Select2::class, [
    'data' => ProjectTask::STATUS_LIST,
    'pluginOptions' => ['width' => '100%'],
]) ?>

<div class="form-group col-4">
    <label class="label">Дата</label>
    <div class="date-picker-wrap">
        <?= Html::activeTextInput($form, 'date', [
            'id' => 'taskDate',
            'class' => 'form-control date-picker',
            'data-date-viewmode' => 'years',
            'value' => DateHelper::format($form->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>
        <svg class="date-picker-icon svg-icon input-toggle">
            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
        </svg>
    </div>
</div>

<?= $widget->field($form, 'time', ['options' => ['class' => 'form-group col-2']])->widget(MaskedInput::class, [
    'mask' => '9{2}:9{2}',
    'options' => ['class' => 'form-control', 'placeholder' => 'XX:XX'],
]) ?>

<?= $widget->field($form, 'employee_id')->widget(Select2::class, [
    'data' => (new RepositoryFactory)->createProjectTaskRepository($form->project_id)->getResponsible(),
    'pluginOptions' => ['width' => '100%'],
]) ?>

<?= $widget->field($form, 'description', ['options' => ['class' => 'form-group col-12']])->textarea(['rows' => 4]) ?>

</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->assetBundles = []; ?>
