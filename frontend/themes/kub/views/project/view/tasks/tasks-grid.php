<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use frontend\modules\crm\models\ListFactory;
use frontend\models\project\ProjectTaskRepository;
use frontend\models\project\ProjectTask;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var ProjectTaskRepository $repository
 * @var bool $hasClient
 */

$provider = $repository->getProvider();
$canEdit = Yii::$app->user->can(TaskAccess::EDIT);
$emptyText = 'По заданным фильтрам задач не найдено. ';

if (!$provider->totalCount) {
    $emptyText = 'Вы ещё не добавили ни одной задачи. ';

    if ($canEdit) {
        $emptyText .= Html::a('Добавить задачу', '#', [
            'class' => 'ajax-form-button',
            'data-url' => Url::to(['project/create-task', 'project_id' => $repository->project_id]),
            'data-title' => 'Добавить задачу',
        ]);
    }
}

?>

<?php ContentDecorator::begin([
        'viewFile' => __DIR__ . DIRECTORY_SEPARATOR . 'task-select.php',
        'params' => [
            'project_id' => $repository->project_id,
        ],
]); ?>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'dataProvider' => $provider,
    'filterModel' => $repository,
    'emptyText' => $emptyText,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper mt-3',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $provider->totalCount, 'scroll' => true]),
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'ProjectTaskSelectForm[task_id]',
            'visible' => $canEdit,
            'headerOptions' => [
                'style' => 'width: 20px;',
            ],
            'checkboxOptions' => function (ProjectTask $task) {
                return ['value' => $task->task_id];
            },
        ],
        [
            'label' => 'Номер задачи',
            'attribute' => 'task_number',
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
            'format' => 'raw',
            'value' => function(ProjectTask $model) use ($canEdit): string  {
                $name = $model->task_number . $model->task_name;
                if ($canEdit) {
                    return Html::a($name, '#', [
                        'class' => 'ajax-form-button text-decoration-none',
                        'data-url' => Url::to(['project/update-task', 'task_id' => $model->task_id]),
                        'data-title' => 'Редактировать задачу'
                    ]);
                }

                return $name;
            }
        ],
        [
            'label' => 'Дата начала',
            'attribute' => 'date_begin',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
        ],
        [
            'label' => 'Дата окончания',
            'attribute' => 'date_end',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
        ],
        [
            'label' => 'Краткое описание',
            'attribute' => 'description',
            'format' => 'raw',
            'value' => function (ProjectTask $task): string {
                $description = mb_substr($task->description, 0, 32, 'UTF-8') . '...';

                return Html::tag('span', $description, ['title' => $task->description]);
            }
        ],
        [
            'label' => 'Время',
            'attribute' => 'time',
            'format' => ['time', 'php:H:i'],
        ],
        [
            'label' => 'Статус',
            'attribute' => 'status',
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => ArrayHelper::merge([null => 'Все'], ProjectTask::STATUS_LIST),
            'value' => 'statusName',
            's2width' => '200px',
            'enableSorting' => false,
        ],
        [
            'label' => 'Ответственный',
            'attribute' => 'employee_id',
            'value' => 'employeeCompany.shortFio',
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => ['' => 'Все'] + (new ListFactory)->createEmployeeList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}',
            'contentOptions' => ['class' => 'text-nowrap'],
            'visible' => $canEdit,
            'buttons' => [
                'update' => function (string $url, ProjectTask $task): string {
                    return GridButtonWidget::widget([
                        'icon' => 'pencil',
                        'options' => [
                            'title' => 'Изменить',
                            'class' => 'button-clr link ajax-form-button',
                            'data-url' => Url::to(['project/update-task', 'task_id' => $task->task_id]),
                            'data-title' => 'Редактировать задачу'
                        ],
                    ]);
                },
            ],
        ],
    ],
]) ?>
</div>

<?php ContentDecorator::end(); ?>
