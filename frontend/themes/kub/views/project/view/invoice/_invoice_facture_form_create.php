<?php
use yii\helpers\Html;
use yii\helpers\Url;
/** @var \yii\web\View $this */
/** @var \common\models\document\Invoice $invoice */
/** @var $useContractor boolean */
/** @var $form \yii\bootstrap\ActiveForm */
/** @var $model common\models\document\InvoiceFacture */

$invoice = isset($invoice) ? $invoice : null;

$formAction = $invoice === null ? '#'
    : Url::to(['invoice-facture/create', 'type' => $invoice->type, 'invoiceId' => $invoice->id, 'contractorId' => ($useContractor ? $invoice->contractor_id : null)]);
?>

<div class="modal fade" id="create-invoice-facture-modal" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <?= Html::beginForm($formAction, 'post', [
                    'class' => 'add-to-invoice',
                ]); ?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="checkbox-inline">
                                <span class="">
                                    <input type="checkbox" name="has_to_payment_document" value="1">
                                </span>
                                К платежно-расчетному документу
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="to_payment_document" id="" class="form-control" placeholder="№ Платежного поручения">
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <button type="submit" class="btn yellow pull-right">Добавить</button>
                        </div>
                    </div>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

