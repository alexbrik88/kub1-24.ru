<?php

use frontend\models\Documents;
use yii\bootstrap\Nav;

/* @var $tab int
 * @var $this \yii\web\View
 * @var $model \common\models\project\Project
 * @var $searchModel \frontend\models\ProjectSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $company \common\models\Company
 * @var $ioType integer
 */


$this->title = 'Счета';

?>

<div class="nav-tabs-row">
    <?= Nav::widget([
        'id' => 'invoice-outgoing-tabs-pane',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'Продажи',
                'url' => ['/project/view',
                    'id' => $model->id,
                    'tab' => 'invoice-outgoing',
                    'type' => Documents::IO_TYPE_OUT
                ],
                'active' => true,
                'options' => ['class' => 'nav-item'],
                'linkOptions' => [
                    'class' => 'nav-link active'
                ],
            ],
            [
                'label' => 'Покупки',
                'url' => ['/project/view',
                    'id' => $model->id,
                    'tab' => 'invoice-incoming',
                    'type' => Documents::IO_TYPE_IN
                ],
                'active' => false,
                'options' => ['class' => 'nav-item'],
                'linkOptions' => [
                    'class' => 'nav-link'
                ],
            ],
        ],
    ]) ?>
</div>

<?php

$tabData = [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'company' => $company,
    'ioType' => $ioType,
];

?>

<div class="tab-content">
    <div id="tab1" class="tab-pane invoice-tab active">
        <div class="mt-3">
            <?= $this->render("invoice", $tabData); ?>
        </div>
    </div>
</div>