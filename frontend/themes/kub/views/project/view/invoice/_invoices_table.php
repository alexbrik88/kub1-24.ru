<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\rbac\UserRole;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\employee\EmployeeRole;
use frontend\widgets\BoolleanSwitchWidget;
use frontend\modules\documents\models\InvoiceSearch;


/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel InvoiceSearch */
/* @var $company \common\models\Company */
/* @var $useContractor boolean
 * @var $this \yii\web\View
 * @var $user \common\models\employee\Employee
 * @var $type integer
 */

$period = StatisticPeriod::getSessionName();
$canView = false;
$user = Yii::$app->user->identity;
if (empty($company)) {
    $company = $user->company;
}

$userConfig = $user->config;

$existsQuery = Invoice::find()->byCompany($company->id)->byDeleted(false)->byIOType($type);
if (isset($id)) {
    $existsQuery->byContractorId($id);
}
$exists = $existsQuery->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет счётов. Измените период, чтобы увидеть имеющиеся счета.";
    }
} else {
    if ($type == Documents::IO_TYPE_OUT) {
        $emptyMessage = 'Вы еще не создали ни одного счёта. ' . Html::a('Создать счёт.', [
                '/documents/invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } elseif ($type == Documents::IO_TYPE_IN) {
        $emptyMessage = 'Вы еще не загрузили ни одного счета от поставщика. ' . Html::a('Загрузить счёт.', [
                '/documents/invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$useContractor = isset($useContractor) ? $useContractor : false;
$docsTypeUpd = $company->isDocsTypeUpd;

$statusFilterItems = [];
foreach ($searchModel->getStatusItemsByQuery($dataProvider->query) as $key => $value) {
    $statusFilterItems[$key] = $value;
    if ($key == InvoiceStatus::STATUS_PAYED) {
        foreach (Invoice::$paymentTypeData as $typeData) {
            $statusFilterItems["{$key},{$typeData['alias']}"] = Html::tag('span', $value, [
                    'style' => 'padding: 0 10px;'
                ]) . $typeData['icon'];
        }
    }
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-currency',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]); ?>
<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-payed',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]); ?>
<style>
    .update-attribute-tooltip-content {
        text-align: center;
    }
</style>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table ' . ($tabViewClass ?? null),
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],


    'rowOptions' => function (Invoice $data) {
        return $data->isAllStatusesComplete ? ['class' => 'invoice-payed'] : [];
    },

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return Html::checkbox('Invoice[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data' => [
                        'contractor' => $model->contractor_id,
                        'sum' => $model->total_amount_with_nds,
                    ]
                ]);
            },
        ],
        [
            'attribute' => 'document_date',
            'label' => 'Дата счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '78px',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'document_number',
            ],
            'format' => 'html',
            'value' => function (Invoice $data) use ($useContractor) {
                if ($data->canView) {
                    $link = Html::a($data->fullNumber, ['/documents/invoice/view',
                        'type' => $data->type,
                        'id' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ]);
                } else {
                    $link = $data->fullNumber;
                }
                $fullName = '№' . $data->document_number . ' от ' . DateHelper::format(
                        $data->document_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    );

                return Html::tag('span', $link, ['data-full-name' => $fullName]);
            },
        ],
        [
            'label' => 'Скан',
            'headerOptions' => [
                'class' => 'col_invoice_scan col_project_invoices_scan' . ($userConfig->project_invoices_scan ? '' : ' hidden'),
                'width' => '70px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_scan col_project_invoices_scan' . ($userConfig->project_invoices_scan ? '' : ' hidden'),
            ],
            'attribute' => 'has_file',
            'format' => 'html',
            'value' => function ($model) {
                return DocumentFileWidget::widget(['model' => $model]);
            },
        ],
        [
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => 'sorting',
                'style' => 'min-width:100px',
            ],
            'contentOptions' => [
                'class' => '',
                'style' => 'white-space: initial;',
            ],
            'attribute' => 'total_amount_with_nds',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $viewPrice = TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
                $rubPrice = TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                $currency = $model->currency_name == Currency::DEFAULT_NAME ? '' : $model->currency_name;
                $content = Html::tag('span', $viewPrice . $currency, [
                    'class' => 'price' . ($currency ? ' tooltip-currency' : ''),
                ]);
                if ($currency) {
                    $text = "Сумма счета в RUB: {$rubPrice}<br/>";
                    $text .= "Курс обмена: {$model->currency_amount} {$model->currency_name} = {$model->currency_rate} RUB";
                    $tooltip = Html::tag('span', $text, ['id' => 'tooltip_currency-' . $model->id]);
                    $content .= "\n" . Html::tag('div', $tooltip, ['class' => 'hidden']);
                }

                return $content;
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '180px',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'format' => 'html', 'filter' => $searchModel->getContractorItems(),
            's2width' => '200px',
            'value' => function ($data) {
                $contractor = Contractor::find()
                    ->andWhere(['id' => $data['contractor_id']])
                    ->one();
                if ($contractor) {
                    return Html::a(Html::encode($contractor->nameWithType), [
                        '/contractor/view',
                        'type' => $contractor->type,
                        'id' => $contractor->id,
                    ], ['target' => '_blank']);
                } else {
                    $contractor = CashContractorType::findOne(['name' => $data['contractor_id']]);
                    return Html::tag('span', $contractor->text);
                }
            },
        ],
        [
            'attribute' => 'payment_limit_date',
            'format' => 'date',
            'headerOptions' => [
                'class' => 'col_invoice_paylimit col_project_invoices_paylimit' . ($userConfig->project_invoices_paylimit ? '' : ' hidden'),
                'width' => '78px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paylimit col_project_invoices_paylimit' . ($userConfig->project_invoices_paylimit ? '' : ' hidden'),
            ],
            'visible' => true,
        ],
        [
            'attribute' => 'invoice_status_id',
            'label' => 'Статус',
            'headerOptions' => [
                'width' => '150px',
            ],
            'format' => 'raw',
            's2width' => '200px',
            'contentOptions' => [],
            'selectPluginOptions' => [
                'templateResult' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
                'templateSelection' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
            ],
            'filter' => $statusFilterItems,
            'value' => function (Invoice $model) {
                $tooltipAmount = '';
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                    $tooltipAmount = '
                        <span class="hidden">
                            <span id="tooltip-payed-info-'.$model->id.'">
                                <span style="color: #45b6af;">Оплачено: ' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . ' ₽</span>
                                <br />
                                <span style="color: #f3565d;">Задолженность: ' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . ' ₽</span>
                            </span>
                        </span>
                    ';
                }

                return ($tooltipAmount)
                    ? Html::tag('span', $model->invoiceStatus->name . $tooltipAmount, ['class' => 'tooltip-payed', 'data-tooltip-content' => "#tooltip-payed-info-{$model->id}"])
                    : $model->invoiceStatus->name;
            },
        ],
        [
            'attribute' => 'payDate',
            'label' => 'Дата оплаты',
            'headerOptions' => [
                'class' => 'col_invoice_paydate col_project_invoices_paydate' . ($userConfig->project_invoices_paydate ? '' : ' hidden'),
                'width' => '14%',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paydate col_project_invoices_paydate' . ($userConfig->project_invoices_paydate ? '' : ' hidden'),
            ],
            'value' => function (Invoice $model) {
                return $model->payDate ? date('d.m.Y', $model->payDate) : '';
            },
            'visible' => true,
        ],
        [
            'attribute' => 'has_act',
            'label' => 'Акт',
            'headerOptions' => [
                'class' => 'col_invoice_act col_project_invoices_act' . ($userConfig->project_invoices_act ? '' : ' hidden'),
                'style' => 'min-width:100px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_act col_project_invoices_act' . ($userConfig->project_invoices_act ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasActFilter($dataProvider->query),
            's2width' => '120px',
            'format' => 'html',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                foreach ($data->acts as $doc) {
                    $docLink = Html::a($doc->fullNumber, [
                        '/documents/act/view',
                        'type' => $doc->type,
                        'id' => $doc->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => $data->canView ? '' : 'no-rights-link',
                    ]);

                    $fileLink = DocumentFileWidget::widget([
                        'model' => $doc,
                        'cssClass' => 'pull-right',
                    ]);

                    $content .= Html::tag('div', $docLink . $fileLink);
                }
                if ($data->canAddAct) {
                    $canCreate = $data->canCreate;
                    $content .= Html::a('Добавить', [
                        '/documents/act/create',
                        'type' => $data->type,
                        'invoiceId' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red'
                            . ($data->isRejected ? ' disabled' : '')
                            . ($canCreate ? '' : ' no-rights-link'),
                        'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                    ]);
                } elseif (!$content) {
                    $content = Html::tag('span', 'Без акта');
                }

                return $content;
            },
        ],
        [
            'attribute' => 'has_packing_list',
            'label' => 'Товарная накладная',
            'headerOptions' => [
                'class' => 'col_invoice_paclist col_project_invoices_paclist' . ($userConfig->project_invoices_paclist ? '' : ' hidden'),
                'style' => 'min-width:200px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paclist col_project_invoices_paclist' . ($userConfig->project_invoices_paclist ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasPackingListFilter($dataProvider->query),
            's2width' => '200px',
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                if (!$data->need_packing_list && $data->canAddPackingList) {
                    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                        return BoolleanSwitchWidget::widget([
                            'id' => 'add-packing-list_' . $data->id,
                            'action' => [
                                '/documents/packing-list/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ],
                            'inputName' => 'add_packing_list',
                            'inputValue' => 1,
                            'label' => 'Выставить ТН?',
                            'addFalseForm' => false,
                            'toggleButton' => [
                                'tag' => 'span',
                                'label' => 'Без ТН',
                                'class' => 'color-link cursor-pointer',
                            ],
                            'options' => [
                                'style' => 'width: 110px;',
                            ],
                        ]);
                    } else {
                        return 'Без ТН';
                    }
                } else {
                    foreach ($data->packingLists as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/packing-list/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? '' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddPackingList) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/packing-list/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red'
                                . ($data->isRejected ? ' disabled' : '')
                                . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }
                }

                return $content;
            }
        ],
        [
            'attribute' => 'has_invoice_facture',
            'label' => 'Счёт-фактура',
            'headerOptions' => [
                'class' => 'col_invoice_invfacture col_project_invoices_invfacture' . ($userConfig->project_invoices_invfacture ? '' : ' hidden'),
                'style' => 'min-width:150px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_invfacture col_project_invoices_invfacture' . ($userConfig->project_invoices_invfacture ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все', '1' => 'Есть СФ', '0' => 'Нет СФ'],
            's2width' => '150px',
            'format' => 'html',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd) {
                $content = '';

                if (!$data->hasNds) {
                    return $content;
                }
                /* @var $doc InvoiceFacture */
                foreach ($data->invoiceFactures as $doc) {
                    $docLink = Html::a($doc->fullNumber, [
                        '/documents/invoice-facture/view',
                        'type' => $doc->type,
                        'id' => $doc->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => $data->canView ? '' : 'no-rights-link',
                    ]);

                    $fileLink = DocumentFileWidget::widget([
                        'model' => $doc,
                        'cssClass' => 'pull-right',
                    ]);

                    $content .= Html::tag('div', $docLink . $fileLink);
                }
                if ($data->canAddInvoiceFacture) {
                    $canCreate = $data->canCreate;
                    $content .= Html::a('Добавить', [
                        '/documents/invoice-facture/create',
                        'type' => $data->type,
                        'invoiceId' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red'
                            . ($data->isRejected ? ' disabled' : '')
                            . ($canCreate ? '' : ' no-rights-link'),
                        'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                    ]);
                }

                return $content;
            },
        ],
        [
            'attribute' => 'has_upd',
            'label' => 'УПД',
            'headerOptions' => [
                'class' => 'col_invoice_upd col_project_invoices_upd' . ($userConfig->project_invoices_upd ? '' : ' hidden'),
                'style' => 'min-width:80px',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_upd col_project_invoices_upd' . ($userConfig->project_invoices_upd ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasUpdFilter($dataProvider->query),
            's2width' => '100px',
            'format' => 'html',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                if (!$data->need_upd && $data->canAddUpd) {
                    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                        return BoolleanSwitchWidget::widget([
                            'id' => 'add-upd_' . $data->id,
                            'action' => [
                                '/documents/upd/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ],
                            'inputName' => 'add_upd',
                            'inputValue' => 1,
                            'label' => 'Выставить УПД?',
                            'addFalseForm' => false,
                            'toggleButton' => [
                                'tag' => 'span',
                                'label' => 'Без УПД',
                                'class' => 'color-link cursor-pointer',
                            ],
                            'options' => [
                                'style' => 'width: 115px;',
                            ],
                        ]);
                    } else {
                        return 'Без УПД';
                    }
                } else {
                    /* @var $doc Upd */
                    foreach ($data->upds as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/upd/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? '' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddUpd) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/upd/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red'
                                . ($data->isRejected ? ' disabled' : '')
                                . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }
                }


                return $content;
            },
        ],

        [
            'attribute' => 'responsible_employee_id',
            'label' => 'Ответственный',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_project_invoices_responsible' . ($userConfig->project_invoices_responsible ? '' : ' hidden'),
                'style' => 'min-width:150px',
            ],
            'contentOptions' => [
                'class' => 'col_project_invoices_responsible' . ($userConfig->project_invoices_responsible ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getResponsibleEmployeeByQuery($dataProvider->query),
            's2width' => '150px',
            'value' => function (Invoice $model) {
                return $model->author ? $model->author->getFio(true) : '';
            },
            'visible' => (
                Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
            ),
        ],
        [
            'attribute' => 'comment_internal',
            'label' => 'Комментарий',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_invoice_comment col_project_invoices_comment' . ($userConfig->project_invoices_comment ? '' : ' hidden'),
                'style' => 'min-width:150px',
            ],
            'contentOptions' => function (Invoice $model) use ($userConfig) {
                $options = [
                    'class' => 'col_invoice_comment col_project_invoices_comment' . ($userConfig->project_invoices_comment ? '' : ' hidden'),
                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap; max-width:90px;',
                ];
                if ($model->comment_internal) {
                    $options['class'] .= ' tooltip2';
                    $options['data-tooltip-content'] = "#tooltip_invoice_comment_{$model->id}";
                }

                return $options;
            },
            'value' => function (Invoice $model) {
                $tooltip = '';
                if ($model->comment_internal) {
                    $tooltip = '
                    <span class="tooltip-template" style="display:none;">
                        <span id="tooltip_invoice_comment_' . $model->id . '">
                        ' . $model->comment_internal . '
                        </span>
                    </span>';
                }

                return $model->comment_internal . $tooltip;
            },
        ],
    ],
]);

echo $this->render('_invoice_facture_form_create', [
    'useContractor' => $useContractor,
]);

Modal::begin([
    'id' => 'no-rights-modal',
    'closeButton' => false,
    'toggleButton' => false,
]);
echo Html::button('×', ['class' => 'close', 'type' => 'button', 'data-dismiss' => 'modal', 'aria-hidden' => 'true']);
echo Html::tag('span', 'У Вас нет прав на данное действие.', ['class' => 'center-block text-center']);
Modal::end();

$contractorDuplicateLink = Url::to(['/contractor/index', 'type' => $searchModel->type, 'ContractorSearch' => [
    'duplicate' => 1,
]]);
$this->registerJs('
    $(document).on("click", "a.no-rights-link", function(e) {
        e.preventDefault();
        $("#no-rights-modal").modal("show");
    });
    $(".act-file-link-preview, .packing-list-file-link-preview, .invoice-facture-file-link-preview, .upd-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
    $(document).on("change", "#invoicesearch-contractor_id", function (e) {
        if ($(this).val() == "duplicate") {
            location.href = "' . $contractorDuplicateLink . '";
        }
    });
');
?>
