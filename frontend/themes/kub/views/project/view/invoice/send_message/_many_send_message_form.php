<?php

use common\components\helpers\Html;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\company\CompanyType;
use common\models\document\EmailTemplate;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\Waybill;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\document\OrderDocument;
use common\models\product\PriceList;
use common\models\employee\EmployeeRole;
use common\models\Agreement;
use common\models\driver\Driver;
use common\models\vehicle\Vehicle;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\document\AgentReport;
use faryshta\widgets\JqueryTagsInput;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\themes\kub\assets\DocSendAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice|\common\models\document\Act */
/* @var $user \common\models\employee\Employee */
/* @var $useContractor string */
/* @var $showSendPopup integer */
/* @var $requestType integer */

DocSendAsset::register($this);

if (empty($models)) {
    $models = [new Contractor(['company_id' => Yii::$app->user->identity->company->id])];
}

function getEmailSubject(array $models, Company $company, $sendWithDocs = false)
{
    // Set Subject
    if (empty($models) || $models[0] instanceof Contractor) {
        return '';
    }

    $modelsSubject = [];
    foreach ($models as $m) {
        $modelsSubject[] = $m->printablePrefix . ' № ' . $m->fullNumber
            . ' от ' . date_format(date_create($m->document_date), 'd.m.Y');
    }

    if ($sendWithDocs) {
        foreach ($models as $key => $model) {
            foreach ($model->acts as $m) {
                $modelsSubject[] = $m->shortPrefix . ' № ' . $m->fullNumber
                    . ' от ' . date_format(date_create($m->document_date), 'd.m.Y');
            }
            foreach ($model->packingLists as $m) {
                $modelsSubject[] = $m->shortPrefix . ' № ' . $m->fullNumber
                    . ' от ' . date_format(date_create($m->document_date), 'd.m.Y');
            }
            foreach ($model->invoiceFactures as $m) {
                $modelsSubject[] = $m->shortPrefix . ' № ' . $m->fullNumber
                    . ' от ' . date_format(date_create($m->document_date), 'd.m.Y');
            }
            foreach ($model->upds as $m) {
                $modelsSubject[] = $m->shortPrefix . ' № ' . $m->fullNumber
                    . ' от ' . date_format(date_create($m->document_date), 'd.m.Y');
            }
        }
    }

    $modelsSubject = implode(', ', $modelsSubject) . ' от ' . $company->getShortName();

    return $modelsSubject;
}

function getEmailText(array $models)
{
    if (empty($models) || $models[0] instanceof Contractor) {
        return '';
    }
    $text = '';
    foreach ($models as $k=>$m) {
        $text .= ($k == 0) ? $m->emailText : str_replace("Здравствуйте!", "", $m->emailText);
        if ($k < count($models) - 1) $text .= "\r\n\r\n";
    }

    return $text;
}

// MAIN MODEL
$model = $models[0];

$user = Yii::$app->user->identity;
$employeeCompany = $user->currentEmployeeCompany;
$sendForm = new InvoiceSendForm($user->currentEmployeeCompany);
$emailTemplates = $user->company->getEmailTemplates($user->id);
$emailTemplatesArray = ArrayHelper::merge([0 => 'Стандартный'], ArrayHelper::map(EmailTemplate::find()->andWhere(['employee_id' => Yii::$app->user->identity->id])->all(), 'id', 'name'));
$activeEmailTemplate = $user->company->getActiveEmailTemplate($user->id);
$emailTemplatesCount = count($emailTemplates);
$emailSignature = $model->company->getEmailSignature($user->id);
$headerText = null;
$viewDocumentButtonText = null;
$requestType = isset($requestType) ? $requestType : null;
$emailPermanentText = null;
$documentSlug = ArrayHelper::getValue(Documents::$typeToSlug, $typeDocument);
$sendWithDocs = isset($sendWithDocs) ? $sendWithDocs : false;

$permanentAttaches = [];
if (!$models[0] instanceof Contractor && !empty($documentSlug)) {
    foreach ($models as $model) {
        $permanentAttaches[] = [
            'model_id' => $model->id,
            'size' => '≈100',
            'name' => $model->getPrintTitle(),
            'url' => ["/documents/{$documentSlug}/document-print", 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle()]
        ];
    }

    if ($sendWithDocs && $model instanceof Invoice) {
        foreach ($models as $model) {
            foreach ($model->acts as $m) {
                $permanentAttaches[] = [
                    'model_id' => $m->id,
                    'size' => '≈100',
                    'name' => $m->getPrintTitle(),
                    'url' => ["/documents/act/document-print", 'actionType' => 'pdf', 'id' => $m->id, 'type' => $m->type, 'filename' => $m->getPrintTitle()]
                ];
            }
            foreach ($model->packingLists as $m) {
                $permanentAttaches[] = [
                    'model_id' => $m->id,
                    'size' => '≈100',
                    'name' => $m->getPrintTitle(),
                    'url' => ["/documents/packing-list/document-print", 'actionType' => 'pdf', 'id' => $m->id, 'type' => $m->type, 'filename' => $m->getPrintTitle()]
                ];
            }
            foreach ($model->invoiceFactures as $m) {
                $permanentAttaches[] = [
                    'model_id' => $m->id,
                    'size' => '≈100',
                    'name' => $m->getPrintTitle(),
                    'url' => ["/documents/invoice-facture/document-print", 'actionType' => 'pdf', 'id' => $m->id, 'type' => $m->type, 'filename' => $m->getPrintTitle()]
                ];
            }
            foreach ($model->upds as $m) {
                $permanentAttaches[] = [
                    'model_id' => $m->id,
                    'size' => '≈100',
                    'name' => $m->getPrintTitle(),
                    'url' => ["/documents/upd/document-print", 'actionType' => 'pdf', 'id' => $m->id, 'type' => $m->type, 'filename' => $m->getPrintTitle()]
                ];
            }
        }
    }

}

if ($model instanceof Invoice) {
    $headerText = 'счетов';
    $viewDocumentButtonText = null;
    $contractor = $model->contractor;
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    foreach ($models as $key => $model) {
        $emailPermanentText .= $model->getEmailPermanentText();
        if ($key < count($models) - 1) $emailPermanentText .= '<br/><br/>';
    }
    if ($sendWithDocs) {
        $emailPermanentText .= '<br/><br/>';
        foreach ($models as $key => $model) {
            foreach ($model->acts as $m) {
                $emailPermanentText .= str_replace("Здравствуйте!", "", $m->emailText) . '<br/><br/>';
            }
            foreach ($model->packingLists as $m) {
                $emailPermanentText .= str_replace("Здравствуйте!", "", $m->emailText) . '<br/><br/>';
            }
            foreach ($model->invoiceFactures as $m) {
                $emailPermanentText .= str_replace("Здравствуйте!", "", $m->emailText) . '<br/><br/>';
            }
            foreach ($model->upds as $m) {
                $emailPermanentText .= str_replace("Здравствуйте!", "", $m->emailText) . '<br/><br/>';
            }
        }
    }
    if ($activeEmailTemplate) {
        $sendForm->emailText = $activeEmailTemplate->text;
    }
} elseif ($model instanceof Contractor) {
    $headerText = 'Счетов';
    $contractor = $model;
} elseif ($model instanceof Agreement) {
    $headerText = 'договоров';
    $viewDocumentButtonText = null;
    $contractor = $model->contractor;
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);

} elseif ($model instanceof PriceList) {
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);
    $headerText = 'прайс-листов';
    $viewDocumentButtonText = null;
} elseif ($model instanceof Driver) {
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);
    $contractor = $model->contractor;
    $headerText = 'водителей';
    $viewDocumentButtonText = null;
} elseif ($model instanceof Vehicle) {
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);
    $contractor = $model->contractor;
    $headerText = 'транспортное средств';
    $viewDocumentButtonText = null;
} elseif ($model instanceof LogisticsRequest) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->getEmailText($requestType);
    $headerText = 'Договора-заявок';
    $viewDocumentButtonText = null;
    if ($requestType == LogisticsRequest::TYPE_CUSTOMER) {
        $contractor = $model->customer_id;
        if ($model->customerAgreement) {
            $headerText = 'Заявок';
        }
        $headerText .= ' с Заказчиком';
    } else {
        $contractor = $model->carrier;
        if ($model->carrierAgreement) {
            $headerText = 'Заявки';
        }
        $headerText .= ' с Перевозчиком';
    }
} elseif ($model instanceof AgentReport) {
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);
    $contractor = $model->agent;
    $headerText = 'Отчетов';
    $viewDocumentButtonText = null;
} elseif ($model instanceof OrderDocument) {
    $sendForm->textRequired = true;
    $sendForm->emailText = getEmailText($models);
    $contractor = $model->contractor;
    $headerText = 'Заказов';
    $viewDocumentButtonText = null;
} else {
    $contractor = $model->invoice->contractor;
    if ($model instanceof Act) {
        $sendForm->textRequired = true;
        $sendForm->emailText = getEmailText($models);
        $headerText = 'актов';
        $viewDocumentButtonText = null;
    } elseif ($model instanceof PackingList) {
        $sendForm->textRequired = true;
        $sendForm->emailText = getEmailText($models);
        $headerText = 'товарных накладных';
        $viewDocumentButtonText = null;
    } elseif ($model instanceof Waybill) {
        $sendForm->textRequired = true;
        $sendForm->emailText = getEmailText($models);
        $headerText = 'товарно-транспортных накладных';
        $viewDocumentButtonText = null;
    } elseif ($model instanceof InvoiceFacture) {
        $sendForm->textRequired = true;
        $sendForm->emailText = getEmailText($models);
        $headerText = 'счет-фактур';
        $viewDocumentButtonText = null;
    } elseif ($model instanceof Upd) {
        $sendForm->textRequired = true;
        $sendForm->emailText = getEmailText($models);
        $headerText = 'УПД';
        $viewDocumentButtonText = null;
    }
}
if (!($model instanceof PriceList) && !($model instanceof Driver) && !($model instanceof Vehicle)) {
    $directorLabel = '<span class="email-label">' . $contractor->director_name .
        ($contractor->director_email ?
            (' (Руководитель)<br><span style="padding-left: 25px">' . $contractor->director_email . '</span>') :
            null) .
        '</span>';
    $chiefAccountantLabel = '<span class="email-label">' . $contractor->chief_accountant_name .
        ($contractor->chief_accountant_email ?
            (' (Главный бухгалтер)<br><span style="padding-left: 25px">' . $contractor->chief_accountant_email . '</span>') :
            null) .
        '</span>';
    $contactLabel = '<span class="email-label">' . $contractor->contact_name .
        ($contractor->contact_email ?
            (' (Контакт)<br><span style="padding-left: 25px">' . $contractor->contact_email . '</span>') :
            null) .
        '</span>';
}
if (isset($emailSignature) && $emailSignature->text) {
    $signature = $emailSignature->text;
} else {
    $employeeRole = $user->company->company_type_id != CompanyType::TYPE_IP ? $employeeCompany->position : null;
    $signature = <<<EMAIL_TEXT
С уважением,
{$employeeCompany->getFio(true)}
{$employeeRole}
EMAIL_TEXT;
}

$formAction = $model instanceof Contractor ? [
    '/documents/invoice/send-many-in-one-by-contractor',
    'type' => $model->type,
    'contractor' => $model->id,
] : ($model instanceof AgentReport ? [
    '/documents/agent-report/send-many-in-one',
    'type' => $model->type,
    'id' => $model->id,
] : ($model instanceof PriceList ? [
    '/price-list/send-many-in-one',
    'productionType' => $model->production_type,
    'id' => $model->id,
] : ($model instanceof Driver ? [
    '/logistics/driver/send-many-in-one',
    'id' => $model->id,
] : ($model instanceof Vehicle ? [
    '/logistics/vehicle/send-many-in-one',
    'id' => $model->id,
] : ($model instanceof LogisticsRequest ? [
    '/logistics/request/send-many-in-one',
    'id' => $model->id,
    'type' => $requestType,
] : [
    'send-many-in-one',
    'type' => $model->type,
    'id' => $model->id,
    'contractorId' => ($useContractor ? $contractor->id : null),
])))));

$modelsSubject = getEmailSubject($models, $user->company, $sendWithDocs);

?>

<div class="invoice-wrap" data-id="invoice">
    <div class="invoice-wrap-in invoice-wrap-scroll" style="padding-bottom: 0;">
        <?= Html::button(Icon::get('close'), [
            'class' => 'invoice-wrap-close button-clr',
            'data-toggle' => "toggleVisible",
            'data-target' => "invoice",
        ]) ?>
        <?php $form = ActiveForm::begin([
            'id' => 'send-document-form',
            'action' => $formAction,
            'options' => [
                'name' => 'send-document-form',
                'class' => 'invoice-wrap-body',
            ],
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
            'validateOnSubmit' => true,
        ]); ?>

            <?php foreach ($models as $m): ?>
                <?= Html::hiddenInput('ids[]', $m->id); ?>
            <?php endforeach; ?>

            <?php if ($sendWithDocs): ?>
                <?= Html::hiddenInput('send_with_documents', 1); ?>
            <?php endif; ?>

            <div class="invoice-wrap-title">Отправка <?= $headerText; ?> покупателю</div>

            <?= $form->field($sendForm, 'sendFrom', [
                'options' => [
                    'class' => 'form-group tooltip-message',
                    'data-tooltip-content' => '#send-from-tooltip',
                ],
            ])->textInput([
                'value' => "{$employeeCompany->getFio()} ({$user->email})",
                'disabled' => true,
            ]); ?>

            <?= $form->field($sendForm, 'sendTo', [
                'template' => "{label}\n{input}\n{hint}\n{error}" .
                    //$this->render('//svg-sprite', [
                    //    'ico' => 'shevron',
                    //    'class' => 'form-control-shevron svg-icon',
                    //]) .
                    $this->render('_send_message_to', [
                    'model' => $model,
                    'form' => $form,
                    'sendForm' => $sendForm,
                    'user' => $user,
                    'contractor' => isset($contractor) ? $contractor : null,
                    'employeeCompany' => $employeeCompany,
                ]),
                'options' => [
                    'style' => 'position: relative;',
                    'class' => 'form-group dropdown-email open-users',
                ],
            ])->widget(JqueryTagsInput::className(), [
                'clientOptions' => [
                    'width' => '100%',
                    'defaultText' => '',
                    'removeWithBackspace' => true,
                    'onAddTag' => new JsExpression("function (val) {
                        var tagInput = $('#invoicesendform-sendto');
                        var reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                        if (reg.test(val) == false) {
                            tagInput.removeTag(val);
                        } else {
                            $('.who-send-container .container-who-send-label').each(function () {
                                var sendToCheckBox = $(this).find('.checker input');
                                if (sendToCheckBox.data('value') == val) {
                                    if (!sendToCheckBox.is(':checked')) {
                                        sendToCheckBox.click();
                                    }
                                }
                            });
                        }
                        if ($('#invoicesendform-sendto').val() == '') {
                            $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                            .attr('placeholder', 'Укажите e-mail или выберите из списка')
                            .addClass('visible');
                        } else {
                            $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                            .removeAttr('placeholder')
                            .removeClass('visible');
                        }
                        $('#invoicesendform-sendto_tagsinput').find('.tag > a').attr('title', 'Удалить');
                    }"),
                    'onRemoveTag' => new JsExpression("function (val) {
                        $('.who-send-container .container-who-send-label').each(function () {
                            sendToCheckBox = $(this).find('.checker input');
                            if (sendToCheckBox.data('value') == val) {
                                if (sendToCheckBox.is(':checked')) {
                                    sendToCheckBox.click();
                                }
                            }
                        });
                        if ($('#invoicesendform-sendto').val() == '') {
                            $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                            .attr('placeholder', 'Укажите e-mail или выберите из списка')
                            .addClass('visible');
                        } else {
                            $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                            .removeAttr('placeholder')
                            .removeClass('visible');
                        }
                    }"),
                ],
            ]); ?>

            <?= $form->field($sendForm, 'subject')->textInput([
                'value' => $modelsSubject,
            ]); ?>

            <div class="row row_indents_m align-items-center mb-4">
                <div class="block-files-email column mr-auto">
                    <span class="upload-file" data-url="<?= Url::to(['/email/upload-email-file']); ?>"
                          data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
                          data-csrf-token="<?= Yii::$app->request->csrfToken; ?>">
                        <span class="upload-file-email border-b link">
                            <?= $this->render('//svg-sprite', ['ico' => 'clip', 'class' => 'svg-icon mr-2',]) ?>
                            <span>Прикрепить файл</span>
                        </span>
                    </span>
                    <div id="file-ajax-loading" style="display: none;">
                        <img src="/img/loading.gif">
                    </div>
                </div>
                <div class="column">
                    <div class="dropdown email-sign">
                        <a class="link" href="#" role="button" id="signEmail" aria-haspopup="true" aria-expanded="false">Подпись</a>
                        <div class="dropdown-popup dropdown-menu email-sign-dropdown-menu">
                            <div class="form-edit form-group mb-0">
                                <div class="form-edit-result pt-3 pb-3 pl-3 hide show" data-id="form-edit">
                                    <span><?= nl2br($signature) ?></span>
                                    <div class="form-edit-btns">
                                        <button class="button-clr link " data-toggle="toggleVisible" data-target="form-edit">
                                            <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-edit-group hide" data-id="form-edit">
                                    <?= Html::textArea('signature', $signature, [
                                        'class' => 'email-signature form-control',
                                        'rows' => 3,
                                    ]); ?>
                                    <div class="form-edit-btns">
                                        <button class="save-email-signature button-clr link mr-1" data-url="<?= Url::to('/email/email-signature') ?>" data-toggle="toggleVisible" data-target="form-edit">
                                            <?= $this->render('//svg-sprite', ['ico' => 'check-2']) ?>
                                        </button>
                                        <button class="button-clr link" data-toggle="toggleVisible" data-target="form-edit">
                                            <?= $this->render('//svg-sprite', ['ico' => 'close', 'class' => 'svg-icon svg-icon_close']) ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($model instanceof Invoice): ?>
                <div class="column template-text-block">
                    <div class="dropdown">
                        <a class="link" href="#" role="button" id="tplEmail" aria-haspopup="true" aria-expanded="false">Шаблон</a>
                        <div class="dropdown-popup dropdown-menu" aria-labelledby="template">
                            <div class="dropdown-padding-small">

                                <?= Html::radioList('template', $activeEmailTemplate ? $activeEmailTemplate->id : 0, $emailTemplatesArray, [
                                    'class' => 'template-variants',
                                    'id' => 'template-variant-radio',
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                        if ($value) {
                                            /* @var $emailTemplate EmailTemplate */
                                            $emailTemplate = EmailTemplate::findOne($value);
                                            $emailText = $emailTemplate->text;
                                        } else {
                                            $emailText = $model->emailText;
                                        }

                                        $return = '
                                                <div class="row flex-nowrap justify-content-between mb-2">
                                                    <div class="column nowrap">
                                                        <label class="radio-label">
                                                            '.Html::radio($name, $checked, ['data-message' => $emailText]).'<span class="radio-txt nowrap">'.$label.'</span>
                                                        </label>
                                                    </div>';
                                        if ($value) {
                                            $return .= '<div class="column nowrap">
                                                        <button class="update-template link button-clr mr-1" data-url="'.Url::to(['/email/update-template', 'id' => $value]).'">
                                                            '.$this->render('//svg-sprite', ['ico' => 'pencil']).'
                                                        </button><button class="delete-template template-'.$value.' link button-clr" data-url="'.Url::to(['/email/delete-template', 'id' => $value]).'" data-id="'.$value.'" data-toggle="modal" data-target="#delete-confirm-template">
                                                            '.$this->render('//svg-sprite', ['ico' => 'garbage']).'
                                                        </button>
                                                    </div>';
                                        } else {
                                            $return .= '<div class="column nowrap">&nbsp;</div>';
                                        }
                                        $return .= '
                                                </div>';

                                        return $return;
                                    },
                                ]); ?>

                            </div>

                            <?= Html::textInput('templateName', null, [
                                'placeholder' => 'Название шаблона',
                                'class' => 'form-control',
                                'id' => 'new-template-name',
                                'style' => ($emailTemplatesCount < 3) ? '' : 'display:none'
                            ]); ?>
                            <div class="form-actions email-template-buttons  form-edit form-group position-relative mb-0 w-auto" style="<?= ($emailTemplatesCount < 3) ? '' : 'display:none' ?>">
                                <button class="form-edit-btn button-clr link save-email-template" data-url="<?= Url::to(['/email/email-template']); ?>" style="display:none;">
                                    <?= $this->render('//svg-sprite', ['ico' => 'check']) ?>
                                </button>
                                <button class="form-edit-btn button-clr link save-email-template-ico">
                                    <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="email_text_input about-card about-card_letter">
                <?= $form->field($sendForm, 'emailText', [
                    'options' => [
                        'class' => '',
                    ],
                    'hintOptions' => [
                        'tag' => 'div',
                        'class' => '',
                    ],
                ])->textArea([
                    'rows' => $model instanceof Invoice ? 1 : 4,
                    'style' => 'padding: 10px 0; width: 100%; border: 0; overflow-y: auto; color: #001424;',
                ])->hint($this->render('_send_message_text', [
                    'documentText' => $emailPermanentText,
                    'emailSignature' => $emailSignature,
                    'viewDocumentButtonText' => $viewDocumentButtonText,
                    'viewDocumentLink' => false,
                ]))->label(false); ?>
            </div>

            <div class="row email-uploaded-files" style="<?= empty($permanentAttaches) ? 'display: none;' : null; ?>">
                <?= Html::activeHiddenInput($sendForm, 'sendEmailFiles'); ?>
                <div class="one-file col-6 template">
                    <div class="loaded-file-name-wrap">
                        <span class="file-name loaded-file-name mb-1"></span>
                    </div>
                    <span class="file-size loaded-file-size mb-1"></span>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'eye']), null, [
                        'target' => '_blank',
                        'class' => 'download-file loaded-file-link link',
                        'style' => 'display:none'
                    ]); ?>
                </div>
                <?php foreach ($permanentAttaches as $attach): ?>
                    <div class="one-file col-6">
                        <img src="/img/email/emailPdf.png" class="preview-img">
                        <div class="loaded-file-name-wrap">
                            <span class="file-name loaded-file-name mb-1" title="<?= $attach['name'] ?>">
                                <?= $attach['name'] ?>
                            </span>
                        </div>
                        <span class="file-size loaded-file-size mb-1"><?= $attach['size'] . ' КБ'?></span>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'eye']), $attach['url'], [
                            'target' => '_blank',
                            'class' => 'download-file loaded-file-link link',
                        ]); ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="invoice-wrap-foot d-flex flex-wrap justify-content-between mt-5"
                style="position: sticky; bottom: 0; padding-top: 10px; padding-bottom: 30px; background-color: #fff;">
                <?= Html::submitButton('Отправить', [
                    'class' => 'button-regular button-regular_red button-width ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::button('Отменить', [
                    'class' => 'button-regular button-hover-transparent button-width',
                    'data-toggle' => "toggleVisible",
                    'data-target' => "invoice",
                ]); ?>
            </div>

        <?php $form->end(); ?>
    </div>
</div>

<!-- Modals -->
<div id="delete-confirm-template" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить шаблон?</h4>
                <div class="text-center">
                    <?= \yii\bootstrap4\Html::a('Да', null, [
                        'class' => 'delete-template-modal button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-url' => Url::to(['many-delete']),
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="delete-confirm-user-email" class="confirm-modal fade modal" role="dialog" tabindex="-1"
     aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить почту?</h4>
                <div class="text-center">
                    <?= \yii\bootstrap4\Html::a('Да', null, [
                        'class' => 'delete-user-email-modal button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-url' => Url::to(['many-delete']),
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
$(document).ready(function () {

    $("#invoicesendform-sendto_addTag input").addClass("visible");

    $("#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag")
    .attr("placeholder", "Укажите e-mail или выберите из списка")
    .addClass("visible");
});
'); ?>

<?php if (!($model instanceof PriceList)): ?>
    <?php $this->registerJs('
    $("#user-email-dropdown .new-user-email-submit").click(function () {
        var $inputs = $(this).parents(".add-new-user-email-form").find("input");
        var $requiredInput = $(this).parents(".add-new-user-email-form").find("input#user-email");
        var $sendXhr = true;

        if ($requiredInput.val() == "") {
            $sendXhr = false;
        }

        if ($sendXhr) {
            var $this = $(this);
            $.post("/email/add-user-email?contractorID=' . $contractor->id . '", $inputs.serialize(), function (data) {
                if (data.result == true) {
                    $("#user-email-dropdown .who-send-container:last").after(data.html);
                    $("#user-email-dropdown .who-send-container:last checkbox").prop("checked", true).uniform();
                    $inputs.val("");
                    if (!data.canAdd) {
                        $("#user-email-dropdown .add-new-user-email-form").hide();
                    }
                }
            });
        }
    });
'); ?>
<?php else: ?>
    <?php $this->registerJs('
        $(document).on("keyup change", "#user-email-dropdown .search-contractors #search", function (e) {
            e.preventDefault();
            var $search = $(this).val().toLowerCase();

            if ($search !== "") {
                $("#user-email-dropdown .who-send-container").each(function (e) {
                    if ($(this).data("search_name").toLowerCase().search($search) == -1 &&
                    $(this).data("search_fio").toLowerCase().search($search) == -1 &&
                    $(this).data("search_email").toLowerCase().search($search) == -1) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            } else {
                $("#user-email-dropdown .who-send-container").show();
            }
        });
    '); ?>
<?php endif; ?>