<?php

use common\models\Company;
use common\models\project\Project;
use common\models\project\ProjectEstimateSearch;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model Project */
/* @var $searchModel ProjectEstimateSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $company Company */

?>

<?php Pjax::begin(['id' => 'pjaxContent']) ?>

<?= $this->render('estimate/index', [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]) ?>

<?php Pjax::end(); ?>
