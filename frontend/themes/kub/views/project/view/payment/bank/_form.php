<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\project\Project;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use \frontend\themes\kub\helpers\Icon;

$company = Yii::$app->user->identity->company;
$accounArray = $company->getCheckingAccountants()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

//if (Yii::$app->request->post('CashBankFlowsForm') === null) {
//    if ($lastBik = Yii::$app->session->get('lastBik')) {
//        foreach ($accounArray as $acc) {
//            if ($lastBik == $acc->bik) {
//                $model->rs = $acc->rs;
//                break;
//            }
//        }
//    }
//}

$rsArray = ArrayHelper::map($accounArray, 'rs', function ($data) {
    return $data->rs . ', ' . $data->name;
});

if (Yii::$app->request->get('canAddAccount'))
    $rsArray = ['add-checking-accountant' => Icon::PLUS . ' Добавить расч / счет '] + $rsArray;

$flowTypeItems = CashBankFlows::getFlowTypes();
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}

if (Yii::$app->request->get('canAddContractor')) {
    $sellerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить поставщика'];
    $customerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить покупателя'];
} else {
    $sellerStaticItems = [];
    $customerStaticItems = [];
}
$sellerStaticItems = array_merge($sellerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['!=', 'name', $model->getNewCashContractorId()])
    ->andWhere(['!=', 'name', CashContractorType::BALANCE_TEXT])->all(), 'name', 'text'));
$customerStaticItems = array_merge($customerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['!=', 'name', $model->getNewCashContractorId()])->all(), 'name', 'text'));

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($sellerStaticItems as $key => $seller) {
        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($sellerStaticItems[$key]);
        else
            $model->contractor_id = $key;
    }
}

$projects = Project::find()
    ->select('name')
    ->andWhere(['company_id' => $company->id])
    ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS))
    ->andWhere('start_date <= ' . (new Expression('current_date()')))
    ->andWhere('end_date >= ' . (new Expression('current_date()')))
    ->indexBy('id')
    ->column();

if ($model->flow_type === null) {
    $model->flow_type = CashBankFlows::FLOW_TYPE_INCOME;
}
$income = 'income' . ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' движение по банку';

$taxItemsIds = InvoiceExpenditureItem::findTaxItems()->select('id')->column();
$taxFields = in_array($model->expenditure_item_id, $taxItemsIds) ? '' : 'hidden';

$taxpayersStatus = TaxpayersStatus::find()->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->select('name')->indexBy('id')->column();
$paymentDetails = PaymentDetails::find()->select('name')->indexBy('id')->column();
$paymentType = PaymentType::find()->select('name')->indexBy('id')->column();

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'cash-flows-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'modal_cash_bank_flows_form',
    'validateOnSubmit' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'cash_flows_form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data-pjax' => true,
        'data' => [
            'type-income' => CashBankFlows::FLOW_TYPE_INCOME,
            'type-expense' => CashBankFlows::FLOW_TYPE_EXPENSE,
        ]
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="form-body">
    <div class="row">
        <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
            <?= Html::hiddenInput('redirect', $redirect) ?>
        <?php endif ?>
        <div class="col-6">
            <?= $form->field($model, 'flow_type')->radioList($typeItems, [
                'class' => 'd-flex flex-wrap',
                'uncheck' => null,
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::radio($name, $checked, [
                        'class' => 'flow-type-toggle-input',
                        'value' => $value,
                        'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                        'labelOptions' => [
                            'class' => 'label mb-3 mr-3 mt-2',
                        ],
                    ]);
                },
            ])->label('Тип'); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'rs')->widget(Select2::class, [
                'data' => $rsArray,
                'options' => [
                    'class' => '',
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    //'allowClear' => true,
                    'width' => '100%',
                    'escapeMarkup' => new yii\web\JsExpression('function(markup) {
                        return markup;
                    }'),
                    'templateResult' => new yii\web\JsExpression('function(data) {
                        return data.text;
                    }'),
                ],
            ]); ?>
        </div>
    </div>

    <div class="flow-type-toggle <?= $expense ?> row">
        <div class="col-6">
            <?= $form->field($model, 'contractor_id')->label('Поставщик')->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_SELLER,
                'staticData' => $sellerStaticItems,
                'options' => [
                    'id' => 'seller_contractor_id',
                    'class' => 'contractor-items-depend seller',
                    'placeholder' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?php if (Yii::$app->request->get('onlyFNS')): ?>
                <?= $form->field($model, 'expenditure_item_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                        ->where(['id' => [28, 46, 48, 53]])
                        ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                        ->all(), 'id', 'name'),
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        'data' => [
                            'tax-items' => Json::encode($taxItemsIds),
                            'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                        ],
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                    ]
                ]); ?>
            <?php else: ?>
                <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::classname(), [
                    'loadAssets' => false,
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        'data' => [
                            'tax-items' => Json::encode($taxItemsIds),
                            'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                        ],
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                    ]
                ]); ?>
                <?= $this->render('../_expense_item_form', [
                    'inputId' => 'cashbankflowsform-expenditure_item_id',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="flow-type-toggle <?= $income ?> row">
        <div class="col-6">
            <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_CUSTOMER,
                'staticData' => $customerStaticItems,
                'options' => [
                    'id' => 'customer_contractor_id',
                    'class' => 'contractor-items-depend customer',
                    'placeholder' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                    'data' => [
                        'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::classname(), [
                'loadAssets' => false,
                'income' => true,
                'options' => [
                    'class' => 'flow-income-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ]
            ]); ?>
            <?= $this->render('../_expense_item_form', [
                'inputId' => 'cashbankflowsform-income_item_id',
                'type' => 'income',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'amount')
                ->textInput([
                    'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
                    'class' => 'form-control js_input_to_money_format',
            ]); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'payment_order_number', [
                'options' => [
                    'class' => 'form-group'
                ],
            ])
            ->textInput(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'date', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])
                    ->textInput([
                        'class' => 'form-control date-picker ico',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'recognitionDateInput', [
                        'options' => ['class' => 'form-group'],
                        'labelOptions' => [
                            'class' => 'label bold-text',
                            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                    'class' => 'flow-type-toggle ' . $income,
                                ]) . Html::tag('span', 'расхода', [
                                    'class' => 'flow-type-toggle ' . $expense,
                                ]),
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker ico',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                        'disabled' => (bool)$model->is_prepaid_expense
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'is_prepaid_expense', [
                        'options' => [
                            'style' => 'padding-top: 36px',
                        ],
                    ])
                    ->checkbox()
                    ->label('Авансовый платеж', ['class' => 'label text-bold']); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="flow-type-toggle <?= $expense ?>">
        <div class="tax-payment-fields <?= $taxFields ?>">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'taxpayers_status_id')->widget(Select2::classname(), [
                        'data' => $taxpayersStatus,
                        'options' => [
                            'placeholder' => '',
                            'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        ],
                        'pluginOptions' => [
                            //'allowClear' => true,
                            'width' => '100%',
                        ],
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'kbk')->textInput([
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ])->hint('Необходим для точного расчета налогов к уплате'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'oktmo_code')->textInput([
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'payment_details_id')->widget(Select2::classname(), [
                        'data' => $paymentDetails,
                        'options' => [
                            'placeholder' => '',
                            'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        ],
                        'pluginOptions' => [
                            //'allowClear' => true,
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'tax_period_code')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '**.99.9999',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'XX.XX.XXXX',
                            'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        ],
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'document_number_budget_payment')->textInput([
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'dateBudgetPayment', [
                        'inputTemplate' => $inputCalendarTemplate,
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'payment_type_id')->widget(Select2::classname(), [
                        'data' => $paymentType,
                        'options' => [
                            'placeholder' => '',
                            'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        ],
                        'pluginOptions' => [
                            //'allowClear' => true,
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'uin_code')->textInput([
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea([
        'style' => 'resize: none;',
        'rows' => '2',
    ]); ?>

    <?= $form->field($model, 'invoices_list', [
        'wrapperOptions' => [
            'class' => 'row',
        ],
        'options' => [
            'class' => 'mb-0 pb-0',
        ]
    ])->widget(\frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget::class)->label(false); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'project_id')
                ->widget(Select2::classname(), [
                    'data' => $projects,
                    'options' => [
                        'value' => $model->project_id,
                        'disabled' => true,
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])
                ->label('Проект'); ?>
        </div>
    </div>

    <?= $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>',
            [
                'id' => 'modal_cash_flows_form_submitButton',
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]);
        ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                data-dismiss="modal">
            Отменить
        </button>
    </div>

</div>
<?php $form->end(); ?>

<?php
$this->registerJs(<<<JS
    $(document).on("change", "#cashbankflowsform-is_prepaid_expense", function (e) {
        var dateInput = $(this).closest('form').find('#cashbankflowsform-recognitiondateinput');
        if ($(this).is(":checked")) {
            dateInput.val('').attr('disabled', true);
        } else {
            dateInput.removeAttr('disabled');
        }
    });

    $("input[type=checkbox]").uniform('refresh');
JS);
?>

<?php \yii\widgets\Pjax::end(); ?>