<?php

use common\components\helpers\Html;
use common\widgets\Modal;
use yii\web\View;
use yii\widgets\Pjax;

Modal::begin([
    'id' => 'modal-cash',
    'closeButton' => false,
    'size' => 'modal-dialog-centered',
    'header' => "<h4>Добавить движение <span></span></h4>
                 <button type='button' class='modal-close close' data-dismiss='modal' aria-label='Close'>
                    <svg class='svg-icon'>
                        <use xlink:href='/img/svg/svgSprite.svg#close'></use>
                    </svg>
                 </button>",
    'headerOptions' => [
        'class' => 'mt-3 mb-3'
    ],
]);

Pjax::begin([
    'id' => 'cash-flows-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
    function toogleInternalFlows(selectValue) {
        var $internalFlow = $(".toogle_internal_flows");
        var $internalFlowBank = $(".toogle_internal_flows.bank");
        var $internalFlowEmoney = $(".toogle_internal_flows.emoney");
        var $internalFlowOrder = $(".toogle_internal_flows.order");
        $internalFlow.removeClass("show").addClass("hidden");
        switch (selectValue) {
            case "bank":
                $internalFlowBank.removeClass("hidden").addClass("show");
                break;
            case "emoney":
                $internalFlowEmoney.removeClass("hidden").addClass("show");
                break;
            case "order":
                $internalFlowOrder.removeClass("hidden").addClass("show");
                break;
        }
    }

    $(document).on("change", "select#contractorInputExpense", function(e){
        toogleInternalFlows($(this).val());
    });
    
    $(document).on("change", "select#contractorInputIncome", function(e){
        toogleInternalFlows($(this).val());
    });
        
', View::POS_READY);


$this->registerJs(<<<JS
    const MOVEMENT_TYPE_BANK = 1;

    function openModalForm(url, headerPartial) {
        $.pjax({
            url: url,
            container: "#cash-flows-pjax-container",
            push: false,
            timeout: 5000,
        });
        
        $(document).on("pjax:success", function() {
            $(".modal-header h4 span").html(headerPartial);
            $("#modal-cash").modal("show");
        });
    }
    
    $(document).on("click", "#modal_cash_bank_flows_button", function(e) {
        e.preventDefault();
        openModalForm($(this).attr("href"), ' по банку');
    });

    $(document).on("click", "#modal_cash_order_flows_button", function(e) {
        e.preventDefault();
        openModalForm($(this).attr("href"), ' по кассе');
    });

    $(document).on("click", "#modal_cash_flows_button", function(e) {
        e.preventDefault();
        var movement = $(this).data('type') === MOVEMENT_TYPE_BANK ? ' по банку' : ' по кассе'; 
        openModalForm($(this).attr("href"), movement);
    });
    
    $(document).on("hidden.bs.modal", "#modal-cash", function () {
        $("#cash-flows-pjax-container").html("");
    });
JS, View::POS_READY);

$this->registerJs(<<<JS
    $(document).on("pjax:complete", function(event) {
        // REFRESH_UNIFORMS
        refreshUniform();
        refreshDatepicker();
    });
JS
);