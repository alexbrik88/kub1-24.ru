<?php

use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\project\Project;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\document\InvoiceExpenditureItem;

/** @var $model CashOrderFlows */

$company = Yii::$app->user->identity->company;
if ($model->amount) {
    $model->amount /= 100;
}

$flowTypeItems = CashOrderFlows::getFlowTypes();
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
    if (!$model->number) {
        $model->number = $model::getNextNumber($company->id, $model->flow_type, $model->date);
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}

$emoneyArray = $company->getEmoneys()->select('name')->orderBy([
    'is_closed' => SORT_ASC,
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->indexBy('id')->column();

$rsArray = $company->getCheckingAccountants()
    ->select(new \yii\db\Expression('CONCAT(rs, ", ", name)'))
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->indexBy('id')
    ->column();

$userCashboxList = Yii::$app->user->identity->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$cashboxList = $company->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$selfFlowQuery = CashContractorType::find()
    ->andWhere(['not', ['name' => CashContractorType::COMPANY_TEXT]])
    ->andWhere(['not', ['name' => CashContractorType::ORDER_TEXT]]);
if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $selfFlowQuery->andWhere(['not', ['name' => CashContractorType::BALANCE_TEXT]]);
}
$selfFlowResult = $selfFlowQuery->select(['text', 'name'])->indexBy('name')->column();

$contractorListOptions = [];
$contractorData = [];
if (true || Yii::$app->request->get('canAddContractor')) {
    $contractorData += ['add-modal-contractor' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) ?
        (Icon::PLUS . ' Добавить поставщика ') :
        (Icon::PLUS . ' Добавить покупателя ')];
}

if (count($cashboxList) > 1) {
    $contractorData += array_slice($selfFlowResult, 0, 1, true);
    foreach ($cashboxList as $key => $value) {
        $optionKey = CashContractorType::ORDER_TEXT . '.' . $key;
        $contractorData[$optionKey] = $value;
        if ($key == $model->cashbox_id) {
            $contractorListOptions[$optionKey] = ['disabled' => true];
        }
    }
    $contractorData += array_slice($selfFlowResult, 1, null, true);
} else {
    $contractorData += $selfFlowResult;
}

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($contractorData as $key => $seller) {

        if ('add-modal-contractor' == $key)
            continue;

        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($contractorData[$key]);
        else
            $model->contractor_id = $key;
    }
}

$projects = Project::find()
    ->select('name')
    ->andWhere(['company_id' => $company->id])
    ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS))
    ->andWhere('start_date <= ' . (new Expression('current_date()')))
    ->andWhere('end_date >= ' . (new Expression('current_date()')))
    ->indexBy('id')
    ->column();

$income = 'income' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$bank = 'bank' . $model->contractor_id == CashContractorType::BANK_TEXT ? ' show' : ' hidden';
$emoney = 'emoney' . $model->contractor_id == CashContractorType::EMONEY_TEXT ? ' show' : ' hidden';
$order = 'order' . $model->contractor_id == CashContractorType::ORDER_TEXT ? ' show' : ' hidden';

$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Изменить')) . ' операцию по кассе';

$inputCalendarTemplate = '
    <div class="date-picker-wrap">{input}
        <svg class="date-picker-icon svg-icon input-toggle">
            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
        </svg>
    </div>';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

?>

<?php Pjax::begin([
    'id' => 'cash-flows-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'modal_cash_order_flows_form',
    'validateOnSubmit' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'update-movement-form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data-pjax' => true,
        'data' => [
            'type-income' => CashOrderFlows::FLOW_TYPE_INCOME,
            'type-expense' => CashOrderFlows::FLOW_TYPE_EXPENSE,
        ],
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>
<?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
    <?= Html::hiddenInput('redirect', $redirect) ?>
<?php endif ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'flow_type')->radioList($typeItems, [
                'class' => 'd-flex flex-wrap',
                'uncheck' => null,
                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                    $input = Html::radio($name, $checked, [
                        'class' => 'flow-type-toggle-input',
                        'value' => $value,
                        'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                        'labelOptions' => [
                            'class' => 'label mb-3 mr-3 mt-2',
                        ]
                    ]);

                    return Html::tag('div', $input, [
                        'class' => 'col-xs-5 m-l-n',
                        'style' => 'padding: 0;'
                    ]);

                },
            ])->label('Тип'); ?>
        </div>

        <?php if (count($userCashboxList) == 1) : ?>
            <?= $form->field($model, 'cashbox_id')->hiddenInput(['value' => key($userCashboxList)])->label(false) ?>
        <?php else : ?>
            <div class="col-6">
                <?= $form->field($model, 'cashbox_id')->widget(Select2::class, [
                    'data' => $userCashboxList,
                    'options' => [
                        'placeholder' => '',
                        'data' => [
                            'prefix' => CashContractorType::ORDER_TEXT . '.',
                            'target' => '#' . Html::getInputId($model, 'contractorInput'),
                        ],
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="flow-type-toggle col-6 <?= $income ?>">
            <?= $form->field($model, 'contractorInput', [
                'options' => [
                    'class' => 'form-group cash-contractor_input',
                ],
            ])->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_CUSTOMER,
                'staticData' => $contractorData,
                'options' => [
                    'id' => 'contractorInputIncome',
                    'placeholder' => '',
                    'options' => $contractorListOptions,
                    'class' => 'contractor-items-depend customer',
                    'disabled' => $model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE,
                    'data' => [
                        'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                    ],
                ],
            ])->label('Покупатель'); ?>
        </div>
        <div class="flow-type-toggle col-6 <?= $expense ?>">
            <?= $form->field($model, 'contractorInput', [
                'options' => [
                    'class' => 'form-group cash-contractor_input',
                ],
            ])->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_SELLER,
                'staticData' => $contractorData,
                'options' => [
                    'id' => 'contractorInputExpense',
                    'placeholder' => '',
                    'options' => $contractorListOptions,
                    'class' => 'contractor-items-depend seller',
                    'disabled' => $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                    ],
                ],
            ])->label('Поставщик'); ?>
        </div>

        <div class="col-6 internal_flows toogle_internal_flows flow-rs-selector bank hidden">
            <?= $form->field($model, 'other_rs_id')->widget(Select2::classname(), [
                'data' => $rsArray,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%'
                ],
            ]); ?>
            <?php if ($model->isNewRecord): ?>
                <div style="position: absolute; top: 133px;">
                    <?= $form->field($model, 'needBankContractorFlow')
                        ->checkbox()
                        ->label('Отобразить операцию в Банке' .
                            Html::tag('span', Icon::QUESTION, [
                                'class' => 'tooltip3',
                                'data-tooltip-content' => '#tooltip_need_bank_flow'
                            ])); ?>
                </div>
                <div style="display: none">
                    <div id="tooltip_need_bank_flow">
                        Галочку не нужно ставить,<br/> если вы загружаете выписку из банка,<br/> т.к. операция
                        задублируется
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-6 internal_flows toogle_internal_flows flow-emoney-selector emoney hidden">
            <?= $form->field($model, 'other_emoney_id')->widget(Select2::classname(), [
                'data' => $emoneyArray,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%'
                ],
            ]); ?>
            <?php if ($model->isNewRecord): ?>
                <div style="position: absolute; top: 133px;">
                    <?= $form->field($model, 'needEmoneyContractorFlow')->checkbox()->label('Отобразить операцию в E-money' .
                        Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip_need_emoney_flow'])); ?>
                </div>
                <div style="display: none">
                    <div id="tooltip_need_emoney_flow">
                        Галочку не нужно ставить,<br/> если вы загружаете выписку из e-money,<br/> т.к. операция
                        задублируется
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php if ($model->isNewRecord): ?>
            <div class="col-6 internal_flows toogle_internal_flows flow-order-selector order hidden">

                <div style="position: absolute; top: 34px;">
                    <?= $form->field($model, 'needCashboxContractorFlow')->checkbox()->label('Отобразить операцию в Кассе' .
                        Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip_need_cashbox_flow'])); ?>
                </div>
                <div style="display: none">
                    <div id="tooltip_need_cashbox_flow">
                        Галочку не нужно ставить,<br/> если вы загружаете выписку из кассы,<br/> т.к. операция
                        задублируется
                    </div>
                </div>

            </div>
        <?php endif; ?>

        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <div class="flow-type-toggle col-6 <?= $expense ?>">
                <?= $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(Select2::class, [
                    'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                        ->where(['id' => [28, 46, 48, 53]])
                        ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                        ->all(), 'id', 'name'),
                    'options' => [
                        'id' => 'expenditureItemSeller',
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
        <?php else: ?>
            <div class="flow-type-toggle col-6 <?= $expense ?>">
                <?= $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(ExpenditureDropdownWidget::class, [
                    'loadAssets' => false,
                    'options' => [
                        'id' => 'expenditureItemSeller',
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>

            <?= $this->render('../_expense_item_form', [
                'inputId' => 'cashorderflows-expenditure_item_id',
            ]); ?>
        <?php endif; ?>

        <div class="flow-type-toggle col-6 <?= $income ?>">
            <?= $form->field($model, 'income_item_id', [
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form-group',
                    'jsValue' => $model->income_item_id
                ],
            ])->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'income' => true,
                'options' => [
                    'id' => 'expenditureItemCustomer',
                    'class' => 'flow-income-items',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]) ?>
        </div>
    </div>
    <?= $this->render('../_expense_item_form', [
        'inputId' => 'cashorderflows-income_item_id',
        'type' => 'income',
    ]); ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'amount')->textInput([
                // mxfi (not enought zeros!): 'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
                'class' => 'form-control js_input_to_money_format',
            ]); ?>
        </div>
        <div class="flow-type-toggle col-3 <?= $income ?>">
            <?= $form->field($model, 'number', [
                'options' => [
                    'class' => 'form-group'
                ],
            ])->textInput(['id' => 'numberCustomer',])->label('Номер ПКО'); ?>
        </div>
        <div class="flow-type-toggle col-3 <?= $expense ?>">
            <?= $form->field($model, 'number', [
                'options' => [
                    'class' => 'form-group'
                ],
            ])->textInput(['id' => 'numberSeller'])->label('Номер РКО'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'date', [
                        'inputTemplate' => $inputCalendarTemplate,
                        'options' => ['class' => 'form-group'],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'recognitionDateInput', [
                        'inputTemplate' => $inputCalendarTemplate,
                        'options' => ['class' => 'form-group'],
                        'labelOptions' => [
                            'class' => 'label bold-text',
                            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                    'class' => 'flow-type-toggle ' . $income,
                                ]) . Html::tag('span', 'расхода', [
                                    'class' => 'flow-type-toggle ' . $expense,
                                ]),
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                        'disabled' => (bool)$model->is_prepaid_expense
                    ]); ?>

                </div>
                <div class="col-6">
                    <?= $form->field($model, 'is_prepaid_expense', [
                        'options' => [
                            'class' => 'form-group',
                            'style' => 'padding-top: 36px'
                        ],
                    ])->checkbox(); ?>
                </div>
            </div>
        </div>
    </div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<?= $form->field($model, 'invoices_list', [
    'wrapperOptions' => [
        'class' => 'row',
    ],
    'options' => [
        'class' => 'mb-0 pb-0',
    ]
])->widget(\frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget::class, [
    'contractor_attribute' => 'contractorInput',
    'invoicesListUrl' => ['/cash/order/un-payed-invoices'],
])->label(false); ?>

    <div class="row">
        <div class="flow-type-toggle col-6 <?= $income ?>">
            <?= $form->field($model, 'reason_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(
                    CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_INCOME),
                    'id',
                    'name'
                ),
                'options' => [
                    'id' => 'reasonCustomer',
                    'placeholder' => 'Необходимо для печатного документа',
                    'jsValue' => $model->reason_id,
                    'data-related-target' => '#other-reason-collapse',
                    'data-related-value' => CashOrdersReasonsTypes::VALUE_OTHER,
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
        <div class="flow-type-toggle col-6 <?= $expense ?>">
            <?= $form->field($model, 'reason_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(
                    CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_EXPENSE),
                    'id',
                    'name'
                ),
                'options' => [
                    'id' => 'reasonSeller',
                    'placeholder' => 'Необходимо для печатного документа',
                    'jsValue' => $model->reason_id,
                    'data-related-target' => '#other-reason-collapse',
                    'data-related-value' => CashOrdersReasonsTypes::VALUE_OTHER,
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'application')->textInput([
                'placeholder' => 'Необходимо для печатного документа',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
        <?php $show = $model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER ? 'show' : '' ?>
        <div id="other-reason-collapse" class="col-6 collapse <?= $show ?>">
            <?= $form->field($model, 'other')->textInput(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'project_id')
                ->widget(Select2::classname(), [
                    'data' => $projects,
                    'options' => [
                        'value' => $model->project_id,
                        'disabled' => true,
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])
                ->label('Проект'); ?>
        </div>
    </div>

<?= $form->field($model, 'is_accounting')->checkbox(); ?>

<?= $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false); ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>',
        [
            'id' => 'modal_cash_flows_form_submitButton',
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]);
    ?>

    <button type="button" class="button-clr button-width button-regular button-hover-transparent"
            data-dismiss="modal">Отменить
    </button>
</div>

<?php $form->end(); ?>

<?php \yii\widgets\Pjax::end(); ?>
