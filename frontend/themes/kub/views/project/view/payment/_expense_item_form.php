<?php

use frontend\rbac\UserRole;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var string $inputId */

if (empty($type) || $type !== 'income') {
	$type = 'expenditure';
}
?>

<?php if (Yii::$app->getUser()->can(UserRole::ROLE_CHIEF)) : ?>

<div style="display: none;">
    <span class="add-new-<?= $type ?>-item-form"
        style="display: inline-block; position: relative; width: 100%; padding: 5px 52px 0 5px; border-top: 1px solid #aaa;">
        <button class="btn yellow new-<?= $type ?>-item-submit" style="position: absolute; right: 12px; top: 5px; padding: 0 7px; font-size: 24px;">
            <span class="fa icon fa-plus-circle"></span>
        </button>
        <?= Html::textInput('name', null, [
            'class' => 'form-control',
            'maxlength' => 45,
            'placeholder' => $type == 'income' ? 'Добавить статью прихода' : 'Добавить статью расходов',
            'style' => 'display: inline-block; width: 100%;',
        ]) ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().find('.add-new-<?= $type ?>-item-form').length) {
            $('.add-new-<?= $type ?>-item-form').first().clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
    });
    $(document).on('click', '.new-<?= $type ?>-item-submit', function() {
        var input = $(this).parent().children('input');
        $.post('/site/add-<?= $type ?>-item', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                var newOption = new Option(data.itemName, data.itemId, true, true);
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
                input.val('');
            }
        });
    });
</script>

<?php endif ?>