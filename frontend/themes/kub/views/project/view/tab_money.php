<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use common\models\Company;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\project\Project */
/* @var $searchModel \frontend\modules\cash\models\CashFlowsProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */
/* @var $ioType integer */

/////////////////////////////////////////
CashStatisticInfo::calcBankTinChildren();
/////////////////////////////////////////

$dateRange = StatisticPeriod::getSessionPeriod();

$startBalance = CashStatisticInfo::getStartBalanceInfo(
    $company->id,
    $dateRange['from'],
    $model->id
);

$incomeAmount = CashStatisticInfo::getStatisticInfo(
    CashFlowsProjectSearch::TYPE_IO_IN,
    $company->id,
    $dateRange,
    $model->id
);

$outgoingAmount = CashStatisticInfo::getStatisticInfo(
    CashFlowsProjectSearch::TYPE_IO_OUT,
    $company->id,
    $dateRange,
    $model->id
);

$user = Yii::$app->user->identity;
$company = $user->company;
$userConfig = $user->config;

$tabViewClass = $userConfig->getTableViewClass('table_view_project_money');
$tabConfigClass = [
    'billPaying' => 'col_project_money_paying' . ($userConfig->project_money_paying ? '' : ' hidden'),
    'incomeExpense' => 'col_project_money_income_expense' . ($userConfig->project_money_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_project_money_income_expense'  . (!$userConfig->project_money_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_project_money_income_expense' . (!$userConfig->project_money_income_expense ? '' : ' hidden'),
    'project' => 'col_project_money_project' . ($userConfig->project_money_project ? '' : ' hidden'),
    'salePoint' => 'col_project_money_sale_point' . ($userConfig->project_money_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_project_money_industry' . ($userConfig->project_money_industry ? '' : ' hidden'),
    'recognitionDate' => 'col_project_money_recognition_date' . ($userConfig->project_money_recognition_date ? '' : ' hidden'),
];
?>

<div class="wrap wrap_count mt-3">
    <div class="row">
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_yellow wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($startBalance, 2); ?>  ₽</div>
                <div class="count-card-title">Баланс на начало</div>
                <hr>
                <div class="count-card-foot">
                    <?= DateHelper::format($dateRange['from'], 'd.m.Y', 'Y-m-d') ?>
                </div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($incomeAmount['sum'], 2); ?> ₽</div>
                <div class="count-card-title">Приход факт</div>
                <hr>
                <div class="count-card-foot"><?= date('d.m.Y') ?></div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_red wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($outgoingAmount['sum'], 2); ?> ₽</div>
                <div class="count-card-title">Расход факт</div>
                <hr>
                <div class="count-card-foot"><?= date('d.m.Y') ?></div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_yellow wrap">
                <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($startBalance + ($incomeAmount['sum'] - $outgoingAmount['sum']), 2); ?> ₽</div>
                <div class="count-card-title">Баланс на конец</div>
                <hr>
                <div class="count-card-foot">
                    <?= DateHelper::format($dateRange['to'], 'd.m.Y', 'Y-m-d') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget(['items' => [
            [
                'attribute' => 'project_money_income_expense',
                'invert_attribute' => 'invert_project_money_income_expense'
            ],
            [
                'attribute' => 'project_money_recognition_date'
            ],
            [
                'attribute' => 'project_money_paying'
            ],
            //[
            //    'attribute' => 'project_money_project'
            //],
            [
                'attribute' => 'project_money_sale_point'
            ],
            [
                'attribute' => 'project_money_industry'
            ],
            [
                'attribute' => 'cash_index_hide_plan',
                'style' => 'border-top: 1px solid #ddd',
                'refresh-page' => true
            ],
        ]]) ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_project_money']) ?>

    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'POST',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::tag('input', '', [
                'type' => 'text',
                'name' => 'search',
                'placeholder' => 'Имя контрагента',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= $this->render('money/_table', [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'type' => $ioType,
    'tabViewClass' => $tabViewClass,
    'tabConfigClass' => $tabConfigClass
    
]); ?>

<?php
$this->registerJs(<<<JS
/////////////////////////////////
if (window.CashModalUpdatePieces)
    CashModalUpdatePieces.init();
/////////////////////////////////
JS, \yii\web\View::POS_READY);
?>
