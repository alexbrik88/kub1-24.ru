<?php

use common\components\helpers\ArrayHelper;
use common\models\company\CompanyType;
use common\models\document\EmailTemplate;
use common\models\employee\Employee;
use common\models\project\ProjectEstimateItem;
use common\models\project\ProjectEstimateItemSendForm;
use frontend\themes\kub\assets\EmailedReportAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/** @var ProjectEstimateItemSendForm $model */

EmailedReportAsset::register($this);

/** @var Employee $employee */
$employee = Yii::$app->user->identity;
$company = $employee->company;

$emailSignature = $company->getEmailSignature($employee->id);
$employeeCompany = $employee->currentEmployeeCompany;
$emailTemplatesArray = ArrayHelper::merge([0 => 'Стандартный'], ArrayHelper::map(EmailTemplate::find()->andWhere(['employee_id' => Yii::$app->user->identity->id])->all(), 'id', 'name'));
$activeEmailTemplate = $company->getActiveEmailTemplate($employee->id);
$emailTemplates = $company->getEmailTemplates($employee->id);
$emailTemplatesCount = count($emailTemplates);

$viewDocumentButtonText = null;
if ($model instanceof \common\models\project\ProjectEstimateItemSendForm) {
    $headerText = 'Смета';
    $viewDocumentButtonText = 'Открыть смету';
}
if (isset($emailSignature) && $emailSignature->text) {
    $signature = $emailSignature->text;
} else {
    $employeeRole = $company->company_type_id != CompanyType::TYPE_IP ? $employeeCompany->position : null;
    $signature = <<<EMAIL_TEXT
С уважением,
{$employeeCompany->getFio(true)}
{$employeeRole}
EMAIL_TEXT;
}

?>

<div class="invoice-wrap" data-id="project-estimate">
    <div class="invoice-wrap-in invoice-wrap-scroll" style="padding-bottom: 0;">
        <button class="invoice-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="project-estimate">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>
        <?php $form = ActiveForm::begin([
            'id' => 'report-send-form',
            'action' => Url::to(['send-project-estimate', 'id' => $model->projectEstimateId]),
            'validationUrl' => Url::to(['validateEmail']),
            'options' => [
                'class' => 'invoice-wrap-body',
            ],
            'enableClientValidation' => true,
            'validateOnSubmit' => true,
        ]); ?>

        <p class="bold">Воспользуйтесь формой чтобы отправить отчет</p>

        <p>Для отправки нескольких отчетов прикрепите файлы к форме отправки</p>

        <?= $form->field($model, 'emailFrom', [
                'options' => [
                    'class' => 'form-group tooltip-message',
                    'data-tooltip-content' => '#send-from-tooltip',
                ],
            ])
            ->textInput([
                'value' => $company->email,
            ])
            ->label('От кого'); ?>

        <?= $form->field($model, 'emailTo', [
                'options' => [
                    'style' => 'position: relative;',
                    'class' => 'form-group dropdown-email',
                ],
            ])
            ->textInput()
            ->label('Кому'); ?>

        <?= $form->field($model, 'subject')
            ->textInput([
                'value' => 'Смета #' . $model->projectEstimateId,
            ])
            ->label('Тема'); ?>

        <div class="row row_indents_m align-items-center mb-4">
            <div class="block-files-email column mr-auto">
                <span class="upload-file" data-url="<?= Url::to(['/email/upload-email-file']); ?>"
                      data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
                      data-csrf-token="<?= Yii::$app->request->csrfToken; ?>">
                    <span class="upload-file-email border-b link">
                        <?= $this->render('//svg-sprite', ['ico' => 'clip', 'class' => 'svg-icon mr-2',]) ?>
                        <span>Прикрепить файл</span>
                    </span>
                </span>
                <div id="file-ajax-loading" style="display: none;">
                    <img src="/img/loading.gif" alt="">
                </div>
            </div>
            <?php /* todo
            <div class="column">
                <div class="dropdown email-sign">
                    <a class="link" href="#" role="button" id="signEmail" aria-haspopup="true" aria-expanded="false">Подпись</a>
                    <div class="dropdown-popup dropdown-menu email-sign-dropdown-menu">
                        <div class="form-edit form-group mb-0">
                            <div class="form-edit-result pt-3 pb-3 pl-3 hide show" data-id="form-edit">
                                <span><?= nl2br($signature) ?></span>
                                <div class="form-edit-btns">
                                    <button class="button-clr link " data-toggle="toggleVisible" data-target="form-edit">
                                        <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                                    </button>
                                </div>
                            </div>
                            <div class="form-edit-group hide" data-id="form-edit">
                                <?= Html::textArea('signature', $signature, [
                                    'class' => 'email-signature form-control',
                                    'rows' => 3,
                                ]); ?>
                                <div class="form-edit-btns">
                                    <button class="save-email-signature button-clr link mr-1" data-url="<?= Url::to('/email/email-signature') ?>" data-toggle="toggleVisible" data-target="form-edit">
                                        <?= $this->render('//svg-sprite', ['ico' => 'check-2']) ?>
                                    </button>
                                    <button class="button-clr link" data-toggle="toggleVisible" data-target="form-edit">
                                        <?= $this->render('//svg-sprite', ['ico' => 'close', 'class' => 'svg-icon svg-icon_close']) ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column template-text-block">
                <div class="dropdown">
                    <a class="link" href="#" role="button" id="tplEmail" aria-haspopup="true" aria-expanded="false">Шаблон</a>
                    <div class="dropdown-popup dropdown-menu" aria-labelledby="template">
                        <div class="dropdown-padding-small">

                            <?= Html::radioList('template', $activeEmailTemplate ? $activeEmailTemplate->id : 0, $emailTemplatesArray, [
                                'class' => 'template-variants',
                                'id' => 'template-variant-radio',
                                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                    if ($value) {
                                        $emailTemplate = EmailTemplate::findOne($value);
                                        $emailText = $emailTemplate->text;
                                    } else {
                                        $emailText = $model->emailText;
                                    }

                                    $return = '
                                                <div class="row flex-nowrap justify-content-between mb-2">
                                                    <div class="column nowrap">
                                                        <label class="radio-label">
                                                            '.Html::radio($name, $checked, ['data-message' => $emailText]).'<span class="radio-txt nowrap">'.$label.'</span>
                                                        </label>
                                                    </div>';
                                    if ($value) {
                                        $return .= '<div class="column nowrap">
                                                        <button class="update-template link button-clr mr-1" data-url="'.Url::to(['/email/update-template', 'id' => $value]).'">
                                                            '.$this->render('//svg-sprite', ['ico' => 'pencil']).'
                                                        </button><button class="delete-template template-'.$value.' link button-clr" data-url="'.Url::to(['/email/delete-template', 'id' => $value]).'" data-id="'.$value.'" data-toggle="modal" data-target="#delete-confirm-template">
                                                            '.$this->render('//svg-sprite', ['ico' => 'garbage']).'
                                                        </button>
                                                    </div>';
                                    } else {
                                        $return .= '<div class="column nowrap">&nbsp;</div>';
                                    }
                                    $return .= '
                                                </div>';

                                    return $return;
                                },
                            ]); ?>

                        </div>

                        <?= Html::textInput('templateName', null, [
                            'placeholder' => 'Название шаблона',
                            'class' => 'form-control',
                            'id' => 'new-template-name',
                            'style' => ($emailTemplatesCount < 3) ? '' : 'display:none'
                        ]); ?>
                        <div class="form-actions email-template-buttons  form-edit form-group position-relative mb-0 w-auto" style="<?= ($emailTemplatesCount < 3) ? '' : 'display:none' ?>">
                            <button class="form-edit-btn button-clr link save-email-template" data-url="<?= Url::to(['/email/email-template']); ?>" style="display:none;">
                                <?= $this->render('//svg-sprite', ['ico' => 'check']) ?>
                            </button>
                            <button class="form-edit-btn button-clr link save-email-template-ico">
                                <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
        </div>
        <div class="email_text_input about-card about-card_letter">
            <?= $form->field($model, 'emailText', [
                'options' => [
                    'class' => '',
                ],
                'hintOptions' => [
                    'tag' => 'div',
                    'class' => '',
                ],
            ])
            ->textarea([
                'rows' => 1,
                'style' => 'padding: 10px 0; width: 100%; border: 0; overflow-y: auto; color: #001424;',
                'value' => 'Здравствуйте!'
            ])
            ->hint($this->render('_send_message_text', [
                'documentText' => null,
                'emailSignature' => $emailSignature,
                'viewDocumentButtonText' => $viewDocumentButtonText,
                'viewDocumentLink' => 'javascript:;',
            ]))
            ->label('Текст сообщения'); ?>
        </div>

        <div class="row email-uploaded-files" style="<?= empty($permanentFiles) ? 'display: none;' : null; ?>">
            <?= Html::activeHiddenInput($model, 'attachment'); ?>
            <div class="one-file col-6 template mr-auto">
                <div class="loaded-file-name-wrap">
                    <span class="file-name loaded-file-name mb-1"></span>
                </div>
                <span class="file-size loaded-file-size mb-1"></span>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'eye']), null, [
                    'target' => '_blank',
                    'class' => 'download-file loaded-file-link link',
                    'style' => 'display:none'
                ]); ?>
                <span class="delete-file loaded-file-link link">
                    <?= $this->render('//svg-sprite', ['ico' => 'garbage', 'class' => 'loaded-file-link link button-clr']) ?>
                </span>
            </div>
        </div>

        <?php $form->end(); ?>

        <div class="d-flex flex-wrap  mt-5" style="position: sticky; bottom: 0; padding-top: 10px; padding-bottom: 30px; background-color: #fff;">
            <button class="button-regular button-regular_red button-width mr-auto" type="submit" form="report-send-form">
                Отправить
            </button>
            <button class="button-regular button-hover-transparent button-width ml-auto" data-toggle="toggleVisible" data-target="project-estimate">
                Отменить
            </button>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="delete-confirm-template" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить шаблон?</h4>
                <div class="text-center">
                    <?= \yii\bootstrap4\Html::a('Да', null, [
                        'class' => 'delete-template-modal button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-url' => Url::to(['many-delete']),
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

$projectEstimateId = $model->projectEstimateId;
$tableName = ProjectEstimateItem::tableName();

$this->registerJs(<<<JS
    $(document).ready(function () {
        $(document).on('click', '.multiple-send-client, .multiple-send-self', function(){
            let ids = [],
                projectEstimate,
                type = $(this).hasClass('multiple-send-client') ? 1 : 2;
            
            $('.joint-operation-checkbox:checked').each(function() {
                ids.push($(this).data("id"));
                projectEstimate = $(this).data("estimate_id")
            })
            
            $('.one-file').each(function() {
                if (!$(this).hasClass('template')) {
                    $(this).remove();
                }
            });
            
            $.post("/project/get-permanent-files?id=$projectEstimateId", {estimateItems: ids, type: type}, function (data) {
                var needUploadFiles = [];
    
                for (var i = 0; i < data.files.length; i++) {
                    var templateFile = $(".email-uploaded-files .one-file.template").clone(),
                        uploadedFiles = $(".email-uploaded-files");
    
                    templateFile.removeClass("template");
                    templateFile.attr("data-id", data.files[i].id);
                    templateFile.prepend(data.files[i].previewImg);
                    templateFile.find(".file-name").text(data.files[i].name).attr("title", data.files[i].name);
                    templateFile.find(".file-size").text(data.files[i].size + " КБ");
                    templateFile.find(".delete-file").attr("data-url", data.files[i].deleteUrl);
                    if (data.files[i].downloadUrl) {
                        templateFile.find(".download-file").attr("href", data.files[i].downloadUrl).show();
                    }
                    templateFile.show();
                    uploadedFiles.append(templateFile);
                    $("#file-ajax-loading").hide();
                    uploadedFiles.show();
                    if (data.files[i].documentPdf !== undefined) {
                        $(".view-document-link").attr("href", data.files[i].downloadUrl);
                    }
                }
    
                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    needUploadFiles.push($(this).data("id"));
                });
                $("#projectestimateitemsendform-attachment").val(needUploadFiles.join(", "));
            });
        });
    });
JS, View::POS_READY); ?>