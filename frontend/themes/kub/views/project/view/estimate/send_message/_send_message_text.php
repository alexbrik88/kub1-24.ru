<?php

use common\models\company\CompanyType;
use common\components\helpers\Html;

/** @var $emailSignature \common\models\document\EmailSignature */
/** @var $viewDocumentButtonText string */
/** @var $viewDocumentLink string */
/** @var $documentText string */

$employee = Yii::$app->user->identity;
$employeeCompany = $employee->currentEmployeeCompany;
$btnStyle = '
    display: inline-block;
    padding: 8px 16px;
    color: #001424;
    background-color: #fff;
    border: 2px dotted #0097fd;
    text-decoration: none!important;
    font-size: 14px;
';
?>

<?php if (isset($documentText) && $documentText): ?>
<div style="margin-bottom: 20px;">
    <?= $documentText; ?>
</div>
<?php endif; ?>
<div style="margin-bottom: 25px;">
    <?php if ($viewDocumentLink): ?>
        <?= Html::a($viewDocumentButtonText, $viewDocumentLink, [
            'class' => 'view-document-link',
            'target' => '_blank',
            'style' => $btnStyle,
        ]); ?>
    <?php else: ?>
        <?php if ($viewDocumentButtonText) : ?>
            <?= Html::tag('span', $viewDocumentButtonText, [
                'class' => 'view-document-link',
                'style' => $btnStyle,
            ]); ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
<div class="email-signature-message-text" style="">
    <?php if (isset($emailSignature) && $emailSignature->text): ?>
        <?= nl2br($emailSignature->text); ?>
    <?php else: ?>
        С уважением,<br>
        <?= $employeeCompany->getFio(true); ?>
        <?php if ($employee->company->company_type_id != CompanyType::TYPE_IP) : ?>
            <br>
            <?= $employeeCompany->position; ?>
        <?php endif ?>
    <?php endif; ?>
</div>
