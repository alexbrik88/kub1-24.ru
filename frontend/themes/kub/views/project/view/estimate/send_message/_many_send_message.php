<?php

use common\components\helpers\ArrayHelper;
use frontend\models\Documents;

/** @var $typeDocument integer */

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'document-many-send-message',
    'timeout' => 5000,
    'linkSelector' => false,
]); ?>

<?= $this->render('_many_send_message_form', [
    'models' => [],
    'typeDocument' => $typeDocument
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<?php $documentSlug = ArrayHelper::getValue(Documents::$typeToSlug, $typeDocument); ?>

<?php $this->registerJs(<<<JS
    $(document).on('click', '.send-panel-modal', function() {
        var idArray = $('.joint-operation-checkbox:checked')
                .map(function() {
                    return $(this).closest('tr').data('key');
                })
                .get();
        $.pjax({
            type: 'POST',
            push: false,
            url: '/documents/{$documentSlug}/get-many-send-message-panel',
            container: "#document-many-send-message",
            data: {
                ioType: 2, // send only out documents
                typeDocument: {$typeDocument},
                ids: idArray,
                sendWithDocs: 1
            },
        });

        $(document).on('pjax:success', '#document-many-send-message', function() {
            $('[data-id="invoice"]').toggleClass('visible show');
        });
    });

    $(document).on('change', '.joint-operation-checkbox', function () {
        $('.document-many-send').toggleClass('no-ajax-loading').toggleClass('send-panel-modal');
        $('.document-many-send-with-docs').toggleClass('no-ajax-loading').toggleClass('send-panel-modal');
    });

JS
) ?>