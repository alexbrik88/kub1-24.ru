<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\product\Product;
use common\models\product\ProductField;
use common\components\image\EasyThumbnailImage;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateItem;

/* @var $this yii\web\View */
/* @var $model ProjectEstimate */
/* @var $estimateId array */
/* @var $type integer */

$user = Yii::$app->user->identity;
$company = $user->company;

$projectCustomer = current($model->project->customers);
/** @var Contractor $customerCompany */
$customerCompany = $projectCustomer->customer;
$this->title = $model->name;
//$signatureLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
$logoLink = EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$listItem = $model->getListItem($estimateId);

$homeUrl = Yii::$app->user->isGuest && YII_ENV_PROD ? Yii::$app->params['serviceSite'] : Yii::$app->homeUrl;

$contacts = [];
if (!empty($author->phone)) $contacts[] = 'Тел: ' . $company->phone;
if (!empty($author->email)) $contacts[] = 'E-mail: ' . $company->email;
if (!empty($author->site))  $contacts[] = 'Сайт: ' . $company->site;
$contacts = implode(', ', $contacts);

/** @var ProductField $productField */
$productField = ProductField::findOne(['company_id' => $company->id]);
$productCategoryNames = Product::getCategoryNames();
$productCategoryFields = Product::getCategoryFieldNames();

?>

<div class="page-content-in p-center-album pad-pdf-p-album">
    <div style="width: 100%">
        <div class="pull-left p-0" style="float: left; width:20%;">
            <?= $company->getTitle(true); ?><br>
            <?= $user->getFio(); ?><br>
            <?= $contacts ?>
        </div>
        <div style="width: 60%"></div>
        <div class="pull-right p-0" style="float:right; width:20%; text-align: right">
            <?php if (is_file($path = $company->getImage('logoImage'))) : ?>
                <?= ImageHelper::getThumb($path, [200, 54.4], [
                    'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                ]); ?>
            <?php endif; ?>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="text-center p-0" style="margin: 0 auto;">
        <div class="row">
            <strong>Смета № <?= $model->number; ?> от <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at); ?></strong>
        </div>
        <div class="row">
            Заказчик <?= $model->customer->getNameWithType(); ?>
        </div>
        <div class="row">
            Проект "<?= $model->project->name; ?>"
        </div>
    </div>
    <div class="document" style="overflow: auto; margin: 0 auto;">
        <div class="main-text" style="margin-top: 30px;">
            Данные на <?= date('d.m.Y') ?>
        </div>
        <table class="table-price-list bordered text-center" style="width: 100%; border: 2px solid #000000; margin-top:2px;">
            <thead>
            <tr>
                <th>№</th>
                <th>Наимено&shy;вание</th>
                <th>Артикул</th>
                <th>Количество</th>
                <th>Ед. измере&shy;ния</th>
                <?php if (ProjectEstimateItem::TYPE_SELFSIDE == $type): ?>
                    <th>Себестоимость</th>
                <?php endif; ?>
                <th>Цена за ед.</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            <?php $num = 0; ?>
            <?php foreach ($listItem as $key => $items): ?>
                <?php if (is_array($items)): ?>
                    <tr>
                        <td colspan="<?= ProjectEstimateItem::TYPE_CLIENTSIDE == $type ? 6 : 7 ?>"><div class="d-flex text-center" style="padding: 5px 0 5px 0"><?= $key == Product::PRODUCTION_TYPE_GOODS ? 'Товары' : 'Услуги' ?></div></td>
                    </tr>
                    <?php foreach ($items as $item): ?>
                        <tr>
                            <td>
                                <?= (++$num) ?>
                            </td>
                            <td style="text-align: left">
                                <?= $item['name']; ?>
                            </td>
                            <td>
                                <?= $item['article']; ?>
                            </td>
                            <td style="text-align: right;">
                                <?= TextHelper::numberFormat($item['count']); ?>
                            </td>
                            <td style="text-align: right;">
                                <?= $item['unit']; ?>
                            </td>
                            <?php if (ProjectEstimateItem::TYPE_SELFSIDE == $type): ?>
                                <td style="text-align: right; white-space: nowrap">
                                    <?= TextHelper::invoiceMoneyFormat($item['income_sum'], 2); ?>
                                </td>
                            <?php endif; ?>
                            <td style="text-align: right; white-space: nowrap">
                                <?= ($item['count'] > 0) ? TextHelper::invoiceMoneyFormat($item['sale_sum'] / $item['count'], 2) : ''; ?>
                            </td>
                            <td style="text-align: right; white-space: nowrap">
                                <?= TextHelper::invoiceMoneyFormat($item['sale_sum'], 2); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="row" style="width:100%; float: left; margin:15px 0 0 0;">
        <div class="pull-right text-right mt-2 p-0" style="width: 30%; float: right;">
            <div class="row">Итого, руб: <?= TextHelper::invoiceMoneyFormat($listItem['total'], 2); ?></div>
            <div class="row">
                <?php if ($listItem['totalNds'] > 0) : ?>
                    <?php if ($model->nds_out == 1) : ?>
                        НДС сверху:
                    <?php else : ?>
                        В том числе НДС:
                    <?php endif ?>
                <?php else : ?>
                    Без налога (НДС):
                <?php endif ?>
                <?= TextHelper::invoiceMoneyFormat($listItem['totalNds'], 2) ?>
            </div>
            <div class="row">Всего к оплате, руб: <?= TextHelper::invoiceMoneyFormat($listItem['totalWithNds'], 2) ?></div>
        </div>
    </div>

    <div class="row" style="width:100%; float: left; margin:15px 0 0 0;">
        <div class="pull-left text-left mt-2 p-0" style="width: 30%; float: left;">
            <div class="row text-left">Заказчик:</div>
            <div class="row text-left">
                <div class="col-12" style="border-bottom: solid 1px #000;">
                    <?= $customerCompany->director_name; ?>
                </div>
            </div>
            <div class="row text-center">ФИО</div>
        </div>
        <div class="pull-right text-left mt-2 p-0" style="width: 30%; float: right;">
            <div class="row text-left">&nbsp;</div>
            <div class="row text-left">
                <div class="col-12" style="border-bottom: solid 1px #000;">
                    &nbsp;
                </div>
            </div>
            <div class="row text-center">подпись</div>
        </div>
    </div>

</div>