<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Config;
use common\models\product\ProductSearch;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateDocuments;
use common\models\project\ProjectEstimateItem;
use common\models\project\ProjectEstimateItemSearch;
use common\models\project\ProjectEstimateItemSendForm;
use common\models\project\ProjectEstimateSearch;
use common\widgets\SummarySelectProjectEstimateWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use yii\bootstrap4\Dropdown;

/** @var $this yii\web\View */
/** @var $model ProjectEstimate */
/** @var $company Company */
/** @var $searchModel ProjectEstimateSearch */
/** @var $dataProvider ActiveDataProvider */
/** @var Config $userConfig */

$userConfig = Yii::$app->user->identity->config;

$payments = ProjectEstimate::payments($model->id, $model->project->id);
$payments = current($payments);

$emptyMessage = 'В смете нет товаров и услуг';

$estimateDocuments = ProjectEstimateDocuments::findOne(['id' => $model->id]);

$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
$filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';

$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$filterDateItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::FILTER_YESTERDAY => 'Вчера',
    ProductSearch::FILTER_TODAY => 'Сегодня',
    ProductSearch::FILTER_WEEK => 'Неделя',
    ProductSearch::FILTER_MONTH => 'Месяц',
];

$filterCommentItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::HAS_COMMENT => 'Есть',
    ProductSearch::NO_COMMENT => 'Нет',
];

$filterImageItems = [
    ProductSearch::ALL => 'Все',
    ProductSearch::HAS_IMAGE => 'Есть',
    ProductSearch::NO_IMAGE => 'Нет',
];

// for grid summary

$listItem = $model->getListItem();
if ($listItem && $listItem['totalNds'] > 0) {
    if ($model->nds_out == 1) {
        $summaryNdsText = 'НДС сверху:';
    } else {
        $summaryNdsText = 'В том числе НДС:';
    }
} else {
    $summaryNdsText = 'Без налога (НДС):';
}

// payments links
$cashFlowData = [];
foreach ($model->invoices as $i) {
    if ($i->cashBankFlows) {
        $dateArray = ArrayHelper::getColumn($i->cashBankFlows, 'date');
        $idArray = ArrayHelper::getColumn($i->cashBankFlows, 'id');
        $data = Html::beginForm(['/cash/bank/index'], 'post', ['style' => 'display: inline-block;']);
        foreach ($idArray as $flow_id) {
            $data .= Html::hiddenInput('flow_id[]', $flow_id);
        }
        $data .= Html::submitButton(implode(', ', $dateArray) . ' по Банку', ['class' => 'link']);
        $data .= Html::endForm();
        $cashFlowData[] = $data;
    }
    if ($i->cashOrderFlows) {
        $dateArray = ArrayHelper::getColumn($i->cashOrderFlows, 'date');
        $idArray = ArrayHelper::getColumn($i->cashOrderFlows, 'id');
        $cashBox = ArrayHelper::getColumn($i->getCashOrderFlows()->groupBy('cashbox_id')->all(), 'cashbox_id');
        $cashBox = $cashBox ? current($cashBox) : null;
        $data = Html::beginForm(['/cash/order/index', 'cashbox' => $cashBox], 'post', ['style' => 'display: inline-block;']);
        foreach ($idArray as $flow_id) {
            $data .= Html::hiddenInput('flow_id[]', $flow_id);
        }
        $data .= Html::submitButton(implode(', ', $dateArray) . ' по Кассе', ['class' => 'link']);
        $data .= Html::endForm();
        $cashFlowData[] = $data;
    }
    if ($i->cashEmoneyFlows) {
        $dateArray = ArrayHelper::getColumn($i->cashEmoneyFlows, 'date');
        $idArray = ArrayHelper::getColumn($i->cashEmoneyFlows, 'id');
        $data = Html::beginForm(['/cash/e-money/index'], 'post', ['style' => 'display: inline-block;']);
        foreach ($idArray as $flow_id) {
            $data .= Html::hiddenInput('flow_id[]', $flow_id);
        }
        $data .= Html::submitButton(implode(', ', $dateArray) . ' по E-money', ['class' => 'link']);
        $data .= Html::endForm();
        $cashFlowData[] = $data;
    }
}
?>

<style type="text/css">
    button.link {
        border: none;
        background-color: transparent;
        font: inherit;
    }
</style>

<a class="link mb-2" href="<?= Url::to(['/project/view', 'id' => $model->project_id, 'tab' => 'estimate']) ?>">
    Назад к списку
</a>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;">
                            <?= "Смета № {$model->number} {$model->addition_number}&nbsp;&nbsp;&nbsp;{$model->name}"; ?>
                        </h4>
                        <div class="column" style="margin-bottom: auto;">
                            <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                    type="button"
                                    data-toggle="modal"
                                    title="Последние действия"
                                    href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                </svg>
                            </button>
                            <a href="<?= Url::to(['/project/estimate', 'id' => $model->id, 'type' => 'update']) ?>"
                               class="button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                               title="Изменить">
                                <svg class="svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="column col-12">
                            <div class="col-4 ml-0 pl-0 pull-left">
                                <div class="label col-12 mb-3">Сумма для клиента</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->sale_sum, 2) ?></div>
                            </div>
                            <div class="col-4 pull-left">
                                <div class="label col-12 mb-3">Оплачено клиентом</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->customersPaymentSum, 2) ?></div>
                            </div>
                            <div class="col-4 pull-left">
                                <div class="label col-12 mb-3">Долг</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->sale_sum - $payments->customersPaymentSum, 2) ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="column col-12">
                            <div class="col-4 ml-0 pl-0 pull-left">
                                <div class="label col-12 mb-3">Себестоимость</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->income_sum, 2) ?></div>
                            </div>
                            <div class="col-4 pull-left">
                                <div class="label col-12 mb-3">Оплачено поставщику</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->clientsPaymentSum, 2) ?></div>
                            </div>
                            <div class="col-4 pull-left">
                                <div class="label col-12 mb-3">Долг</div>
                                <div class=""><?= TextHelper::invoiceMoneyFormat($payments->income_sum - $payments->clientsPaymentSum, 2) ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="column col-12">
                            <div class="col-4 ml-0 pl-0 pull-left">
                                <div class="label col-12 mb-3">Прибыль / Рентабельность</div>
                                <div>
                                    <span class="nowrap"><?= TextHelper::invoiceMoneyFormat($payments->income_estimate_sum - $payments->expense_estimate_sum, 2) ?> / </span>
                                    <span class="nowrap"><?= TextHelper::invoiceMoneyFormat(($payments->income_estimate_sum - $payments->expense_estimate_sum) / ($payments->income_sum ? $payments->income_sum : 1), 2) . '%' ?></span>
                                </div>
                            </div>
                            <div class="col-4 pull-left">
                                <div class="label col-12 mb-3">Разница по оплатам</div>
                                <?php if ($payments->customersPaymentSum): ?>
                                    <div class=""><?= TextHelper::invoiceMoneyFormat($payments->customersPaymentSumNoNds - $payments->clientsPaymentSumNoNds, 2) ?></div>
                                <?php else: ?>
                                    <div class=""><?= TextHelper::invoiceMoneyFormat(0, 2) ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="col-4 pull-left">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <div class="dropdown project-estimate-doc-status-set mb-2" data-status="<?= $model->status ?>">
                    <?= Html::button(Html::tag('span', ProjectEstimate::$statuses[$model->status], [
                            'class' => 'project-estimate-document-status-name w-100',
                        ]) . '<svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>', [
                        'class' => 'button-regular dropdown-toggle w-100 pr-4 position-relative',
                        'data-toggle' => 'dropdown',
                    ]) ?>

                    <div class="dropdown-menu form-filter-list list-clr w-100" aria-labelledby="documentStatusButton">
                        <?= Html::a('Черновик', 'javascript:;', [
                            'data-id' => $model->id,
                            'data-project-id' => $model->project_id,
                            'data-status' => ProjectEstimate::STATUS_DRAFT,
                            'class' => 'dropdown-item',
                            'style' => 'height: 50px;'
                        ]) ?>
                        <?= Html::a('Плановая', 'javascript:;', [
                            'data-id' => $model->id,
                            'data-project-id' => $model->project_id,
                            'data-status' => ProjectEstimate::STATUS_PLANNED,
                            'class' => 'dropdown-item',
                            'style' => 'height: 50px;'
                        ]) ?>
                        <?= Html::a('Утверждена', 'javascript:;', [
                            'data-id' => $model->id,
                            'data-project-id' => $model->project_id,
                            'data-status' => ProjectEstimate::STATUS_APPROVED,
                            'class' => 'dropdown-item',
                            'style' => 'height: 50px;'
                        ]) ?>
                        <?= Html::a('Подписана', 'javascript:;', [
                            'data-id' => $model->id,
                            'data-project-id' => $model->project_id,
                            'data-status' => ProjectEstimate::STATUS_SIGNED,
                            'class' => 'dropdown-item',
                            'style' => 'height: 50px;'
                        ]) ?>
                    </div>

                </div>
                <div class="pb-2 mb-1 mt-3">
                    <div class="dropdown">
                        <?= Html::a(Icon::get('add-icon') . '<span>Счет</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>', '#', [
                            'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => 'false'
                        ]); ?>

                        <?= Dropdown::widget([
                            'options' => [
                                'class' => 'dropdown-menu form-filter-list list-clr w-100'
                            ],
                            'items' => [
                                [
                                    'label' => 'Счет покупателю',
                                    'url' => Url::to(['/documents/invoice/create',
                                        'type' => Documents::IO_TYPE_OUT,
                                        'project_id' => $model->project_id,
                                        'project_estimate_id' => $model->id,
                                    ]),
                                ],
                                [
                                    'label' => 'Счет поставщику',
                                    'url' => Url::to(['/documents/invoice/create',
                                        'type' => Documents::IO_TYPE_IN,
                                        'project_id' => $model->project_id,
                                        'project_estimate_id' => $model->id,
                                    ]),
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="about-card mb-2">
                    <div class="about-card-item">
                        <span class="text-grey">Заказчик:</span>
                        <span><?= Html::a(
                                $model->customer->getShortName(),
                                Url::to(['contractor/view', 'id' => $model->customer->id, 'type' => Contractor::TYPE_CUSTOMER]),
                                ['class' => 'link']
                            ); ?></span>
                    </div>
                    <div class="about-card-item">
                        <span class="text-grey">Проект:</span>
                        <span>
                            <?= Html::a($model->project->name, Url::to(['project/view', 'id' => $model->project->id, 'tab' => 'estimate'])) ?>
                        </span>
                    </div>
                    <div class="about-card-item">
                        <span class="text-grey">Ответственный:</span>
                        <span>
                            <?php if ($model->employee): ?>
                                <?= $model->employee->getShortFio(); ?>
                            <?php elseif ($model->project->responsibleEmployee): ?>
                                <?= $model->project->responsibleEmployee->getShortFio(); ?>
                            <?php endif; ?>
                        </span>
                    </div>
                    <?php if ($cashFlowData) : ?>
                        <div class="about-card-item">
                            <span class="text-grey">Оплата:</span>
                            <span>
                            <?= implode(', ', $cashFlowData) ?>
                        </span>
                        </div>
                    <?php endif ?>
                    <div class="about-card-item">
                        <?= DocumentFileScanWidget::widget([
                            'model' => $estimateDocuments,
                            'hasFreeScan' => $company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => 'estimate', 'id' => $estimateDocuments->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => 'estimate', 'id' => $estimateDocuments->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => 'estimate', 'id' => $estimateDocuments->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => 'estimate', 'id' => $estimateDocuments->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => 'estimate', 'id' => $estimateDocuments->id]),
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s mt-3">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'project_estimate_item_article',
                ],
                [
                    'attribute' => 'project_estimate_item_income_price',
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
                ],
                [
                    'attribute' => 'project_estimate_item_income_sum',
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
                ],
                [
                    'attribute' => 'project_estimate_item_sum_diff',
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                        Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                        Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_project_estimate_item']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <!-- Filters -->
        <div class="form-group mr-2">
            <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="button-txt">Фильтр</span>
                <svg class="svg-icon svg-icon-shevron">
                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                </svg>
            </button>
            <div class="dropdown-menu keep-open" aria-labelledby="filter">
                <div class="popup-dropdown-in p-3">
                    <div class="p-1">
                        <div class="row">
                            <div class="form-group col-6 mb-3">
                                <div class="dropdown-drop" data-id="dropdown1">
                                    <div class="label">Показывать</div>
                                    <?= Html::activeHiddenInput($searchModel, 'filterStatus') ?>
                                    <a class="button-regular w-100 button-hover-content-red text-left form-control pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                        <span class="drop-title">
                                            <?= ArrayHelper::getValue($filterStatusItems, $searchModel->filterStatus) ?>
                                        </span>
                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                        </svg>
                                    </a>
                                    <div class="dropdown-drop-menu" data-id="dropdown1">
                                        <div class="dropdown-drop-in">
                                            <ul class="form-filter-list list-clr">
                                                <?php foreach ($filterStatusItems as $key=>$item): ?>
                                                    <li>
                                                        <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterStatus) ?>"><?= $item ?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-6 mb-3">
                                <div class="dropdown-drop" data-id="dropdown2">
                                    <div class="label">По дате добавления</div>
                                    <?= Html::activeHiddenInput($searchModel, 'filterDate') ?>
                                    <a class="button-regular w-100 button-hover-content-red text-left form-control pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                        <span class="drop-title">
                                            <?= ArrayHelper::getValue($filterDateItems, $searchModel->filterDate) ?>
                                        </span>
                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                        </svg>
                                    </a>
                                    <div class="dropdown-drop-menu" data-id="dropdown2">
                                        <div class="dropdown-drop-in">
                                            <ul class="form-filter-list list-clr">
                                                <?php foreach ($filterDateItems as $key=>$item): ?>
                                                    <li>
                                                        <a href="#" class="filter-item filter-by-date" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filterDate) ?>"><?= $item ?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <div class="row justify-content-between">
                                    <div class="form-group column">
                                        <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                            <span>Применить</span>
                                        </button>
                                    </div>
                                    <div class="form-group column">
                                        <button id="product_filters_reset" class="button-regular button-hover-content-red button-width-medium button-clr" data-clear="dropdown" type="button">
                                            <span>Сбросить</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск по номеру и названию',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout_with_summary', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                return Html::checkbox('id', false, [
                    'class' => 'joint-operation-checkbox',
                    'data' => [
                        'id' => $model->id,
                        'estimate_id' => $model->id_project_estimate,
                        'sum' => $model->sale_sum,
                    ]
                ]);
            },
        ],
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'headerOptions' => [
                'class' => 'col_project_estimate_item_article ' . ($userConfig->project_estimate_item_article ? "" : 'hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_item_article ' . ($userConfig->project_estimate_item_article ? "" : 'hidden'),
            ],
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                return $model->article;
            },
        ],
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                if ($product = \common\models\product\Product::find()->where(['id' => $model->product_id, 'company_id' => $model->projectEstimate->project->company_id])->one())
                    return Html::a($model->title, Url::to(['product/view', 'id' => $model->product_id, 'productionType' => $product->production_type, 'estimate' => $model->id]));

                return $model->title;
            },
        ],
        [
            'attribute' => 'amount',
            'label' => 'Количество',
            'value' => function (ProjectEstimateItem $model) {
                return $model->count;
            },
        ],
        [
            'attribute' => 'unit',
            'label' => 'Единица измерения',
            'value' => function (ProjectEstimateItem $model) {
                return $model->unit;
            },
        ],
        [
            'attribute' => 'sale_price',
            'label' => 'Цена за единицу',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'width' => '10%',
            ],
            'value' => function (ProjectEstimateItem $model) {
                return TextHelper::invoiceMoneyFormat($model->sale_price, 2);
            },
        ],
        [
            'attribute' => 'sale_sum',
            'label' => 'Сумма',
            'headerOptions' => [
                'width' => '15%',
            ],
            'contentOptions' => [
                'width' => '15%',
            ],
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                return TextHelper::invoiceMoneyFormat($model->sale_sum, 2);
            },
        ],
        [
            'attribute' => 'expense_invoices',
            'label' => 'Исходящие счета',
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                $invoices = ProjectEstimateItemSearch::getInvoicesQuery($model->product_id, $model->projectEstimate->project_id, $model->id_project_estimate, Invoice::IO_TYPE_INCOME)->asArray()->all();

                $result = "";

                foreach ($invoices as $key => $invoice) {
                    $result .= Html::a('№' . $invoice['invoice_number'], Url::to(['documents/invoice/view', 'id' => $invoice['invoice_id'], 'type' => Invoice::IO_TYPE_INCOME]));
                    if ($key !== array_key_last($invoices)) {
                        $result .= ', ';
                    }
                }

                return $result;
            },
        ],
        [
            'attribute' => 'income_price',
            'label' => 'Цена закупки',
            'headerOptions' => [
                'class' => 'col_project_estimate_item_income_price ' . ($userConfig->project_estimate_item_income_price ? "" : 'hidden'),
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_item_income_price ' . ($userConfig->project_estimate_item_income_price ? "" : 'hidden'),
                'width' => '10%'
            ],
            'value' => function (ProjectEstimateItem $model) {
                return TextHelper::invoiceMoneyFormat($model->income_price, 2);
            },
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
        ],
        [
            'attribute' => 'income_sum',
            'label' => 'Сумма закупки',
            'headerOptions' => [
                'class' => 'col_project_estimate_item_income_sum ' . ($userConfig->project_estimate_item_income_sum ? "" : 'hidden'),
                'width' => '15%'
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_item_income_sum ' . ($userConfig->project_estimate_item_income_sum ? "" : 'hidden'),
                'width' => '15%'
            ],
            'value' => function (ProjectEstimateItem $model) {
                return TextHelper::invoiceMoneyFormat($model->income_sum, 2);
            },
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
        ],
        [
            'attribute' => 'income_invoices',
            'label' => 'Входящие счета',
            'format' => 'raw',
            'value' => function (ProjectEstimateItem $model) {
                $invoices = ProjectEstimateItemSearch::getInvoicesQuery($model->product_id, $model->projectEstimate->project_id, $model->id_project_estimate, Invoice::IO_TYPE_EXPENSE)->asArray()->all();

                $result = "";

                foreach ($invoices as $key => $invoice) {
                    $result .= Html::a('№' . $invoice['invoice_number'], Url::to(['documents/invoice/view', 'id' => $invoice['invoice_id'], 'type' => Invoice::IO_TYPE_EXPENSE]));
                    if ($key !== array_key_last($invoices)) {
                        $result .= ', ';
                    }
                }

                return $result;
            },
        ],
        [
            'attribute' => 'diff_sum',
            'label' => 'Разница',
            'headerOptions' => [
                'class' => 'col_project_estimate_item_sum_diff ' . ($userConfig->project_estimate_item_sum_diff ? "" : 'hidden'),
                'width' => '15%'
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_item_sum_diff ' . ($userConfig->project_estimate_item_sum_diff ? "" : 'hidden'),
                'width' => '15%'
            ],
            'value' => function (ProjectEstimateItem $model) {
                return TextHelper::invoiceMoneyFormat($model->diff_sum, 2);
            },
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
        ],
    ],
    'summary' => Html::tag('div', Html::tag('div', Html::tag('table', '' .
        Html::tag('tr',  Html::tag('td', 'Итого, руб:', ['class' => 'pr-3']) . Html::tag('td', TextHelper::invoiceMoneyFormat($listItem['total'] ?? 0, 2))) .
        Html::tag('tr', Html::tag('td',$summaryNdsText, ['class' => 'pr-3']) . Html::tag('td', TextHelper::invoiceMoneyFormat($listItem['totalNds'] ?? 0, 2))) .
        Html::tag('tr', Html::tag('td','Всего к оплате, руб:', ['class' => 'pr-3']) . Html::tag('td', TextHelper::invoiceMoneyFormat($listItem['totalWithNds'] ?? 0, 2))))
    , ['class' => 'p-2 ml-auto pull-right font-14']), ['class' => 'w-100', 'style' => 'min-height:76px'])
]); ?>

<?php
?>


<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

<div class="created-by">
    <div class="row ml-0 pl-0">
        <strong><?= DateHelper::format(date('Y-m-d', $model->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></strong>
        &nbsp;Создал&nbsp;<?= $model->employee ? $model->employee->getShortFio() : $model->project->responsibleEmployee->getShortFio(); ?>
    </div>
    <?php
    /** @var ProjectEstimateItem $model */
    $logs = \common\models\log\Log::find()
        ->asArray()
        ->andWhere(['model_name' => $model::className()])
        ->andWhere(['model_id' => $model->id])
        ->orderBy('created_at ASC')
        ->all();

    foreach ($logs as $log): ?>
        <div class="row ml-0 pl-0 mt-1">
            <strong><?= DateHelper::format(date('Y-m-d', $log['created_at']), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></strong>
            &nbsp;<?= $log['message']; ?>
        </div>
    <?php endforeach; ?>
</div>

<?php Modal::end(); ?>

<div id="many-delete" class="confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные позиции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['/project/many-delete-estimate-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<div id="copy" class="confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите скопировать смету?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to([
                        'estimate',
                        'type' => 'create',
                        'id' => $model->project->id,
                        'clone' => $model->id
                    ]),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?= SummarySelectProjectEstimateWidget::widget([
    'buttons' => [
        [
            Html::tag('div',
                Html::button($this->render('//svg-sprite', ['ico' => 'envelope']) . '<span>Отправить</span>', [
                'class' => 'button-clr button-regular button-hover-transparent button-width',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'form-filter-list list-clr',
                ],
                'items' => [
                    [
                        'label' => 'Для клиента',
                        'encode' => false,
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'document-many-send multiple-send multiple-send-client',
                            'data-toggle' => 'toggleVisible',
                            'data-target' => 'project-estimate'
                        ]
                    ],
                    [
                        'label' => 'Внутреняя',
                        'encode' => false,
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'document-many-send multiple-send multiple-send-self',
                            'data-toggle' => 'toggleVisible',
                            'data-target' => 'project-estimate'
                        ]
                    ],
                ],
            ]), ['class' => 'dropup']),
            Html::tag('div',
            Html::button($this->render('//svg-sprite', ['ico' => 'print']) . ' <span>Печать</span>', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'form-filter-list list-clr',
                ],
                'items' => [
                    [
                        'label' => 'Для клиента',
                        'encode' => false,
                        'url' => [
                            'project-estimate-item-print',
                            'id' => $model->id,
                            'actionType' => 'print',
                            'type' => ProjectEstimateItem::TYPE_CLIENTSIDE,
                        ],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'multiple-print-client',
                        ]
                    ],
                    [
                        'label' => 'Внутреняя',
                        'encode' => false,
                        'url' => [
                            'project-estimate-item-print',
                            'id' => $model->id,
                            'actionType' => 'print',
                            'type' => ProjectEstimateItem::TYPE_SELFSIDE,
                        ],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'multiple-print-self',
                        ]
                    ],
                ],
            ]), ['class' => 'dropup']),

            Html::tag('div',
            Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                'class' => 'button-clr button-regular button-hover-transparent button-width',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'form-filter-list list-clr',
                ],
                'items' => [
                    [
                        'label' => 'Для клиента',
                        'encode' => false,
                        'url' => [
                            'project-estimate-download-pdf',
                            'id' => $model->id,
                            'actionType' => 'pdf',
                            'type' => ProjectEstimateItem::TYPE_CLIENTSIDE,
                        ],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'download-package-client',
                        ]
                    ],
                    [
                        'label' => 'Внутреняя',
                        'encode' => false,
                        'url' => [
                            'project-estimate-download-pdf',
                            'id' => $model->id,
                            'actionType' => 'pdf',
                            'type' => ProjectEstimateItem::TYPE_SELFSIDE,
                        ],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'download-package-self',
                        ]
                    ],
                ],
            ]), ['class' => 'dropup']),
            Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'copied']) . '<span>Копировать</span>', '#copy', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
            ])),
        ],
        [
            Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>', '#many-delete', [
                'class' => 'modal-many-delete-estimate-item button-regular button-width button-regular_padding_bigger button-hover-content-red',
                'data-toggle' => 'modal',
            ])),
        ]
    ],
]); ?>

<?= $this->render('@frontend/themes/kub/views/project/view/estimate/send_message/send-panel.php', [
        'model' => (new ProjectEstimateItemSendForm([
            'projectEstimateId' => $model->id,
        ])),
        'useContractor' => true,
]); ?>


<?php $this->registerJs(' 
    $(document).on("click", ".dropdown-status-menu .dropdown-item", function(e) {
        let status = $(this).data("status"),
            projectEstimateId = $(this).data("id");
            project_id = $(this).data("project-id");
        let text = $(this).text();
        
        if (!project_id && !status) {
            return false;
        }
        
        $.post("project-estimate-change-item", {ProjectEstimateForm: [{projectEstimateId: projectEstimateId, project_id: project_id, status: status}]}, function(data) {
            if ("true" === data.success) {
                $(".project-estimate-document-status-name").text(text);
                $(this).closest(".project-estimate-doc-status-set").attr("data-status", status);
                window.location.reload(true);
            }
        })
    });
    
    $(document).on("click", ".btn-confirm-yes", function() {
        var ids = [];
        let checkboxes = $(".joint-operation-checkbox:checked");
        let url = $(this).data("url");
        
        checkboxes.each(function(){
            ids.push($(this).data("id"));
        })
        
        $.post(url, {ids: ids}, function(data) {
            if ("true" === data.success) {
                window.location.reload(true);
            }
        })
    });
'); ?>