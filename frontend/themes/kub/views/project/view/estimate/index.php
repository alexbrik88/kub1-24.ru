<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateSearch;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use common\components\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Project $model */
/** @var ActiveDataProvider $dataProvider */
/** @var ProjectEstimateSearch $searchModel */

$emptyMessage = "Вы еще не добавили ни одной сметы. " . Html::a('Добавить смету', Url::to(['estimate', 'id' => $model->id, 'type' => 'create']), ['data-pjax' => 0]);

/** @var Employee $user */
$user = Yii::$app->user->identity;
/** @var Company $company */
$company = $user->company;

$userConfig = Yii::$app->user->identity->config;

?>

<div class="table-settings row row_indents_s mt-3">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'project_estimate_name',
                ],
                [
                    'attribute' => 'project_estimate_responsible',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск по номеру и названию',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (ProjectEstimate $model) {
                return Html::checkbox('ProjectEstimateForm[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data' => [
                        'id' => $model->id,
                        'projectEstimateId' => $model->id,
                        'project_id' => $model->project_id,
                    ]
                ]);
            },
        ],
        [
            'attribute' => 'date',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '78px',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function(ProjectEstimate $estimate) {
                return date(DateHelper::FORMAT_USER_DATE, $estimate->date);
            }
        ],
        [
            'attribute' => 'number',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'format' => 'raw',
            'value' => function (ProjectEstimate $estimate) {
                return Html::a($estimate->number . $estimate->addition_number, Url::to(['estimate', 'id' => $estimate->id, 'type' => 'view']), ['data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'name',
            'headerOptions' => [
                'class' => 'sorting col_project_estimate_name ' . ($userConfig->project_estimate_name ? '' : 'hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_name ' . ($userConfig->project_estimate_name ? '' : 'hidden'),
            ],
            'value' => function(ProjectEstimate $estimate) {
                return $estimate->name ?: "Смета № {$estimate->number} {$estimate->addition_number}";
            }
        ],
        [
            'attribute' => 'sale_sum',
            'label' => 'Сумма, без НДС',
            'value' => function (ProjectEstimate $model) {
                return TextHelper::invoiceMoneyFormat($model->sale_sum, 2);
            },
        ],
        [
            'attribute' => 'income_sum',
            'label' => 'Себестоимость, без НДС',
            'value' => function (ProjectEstimate $model) {
                return TextHelper::invoiceMoneyFormat($model->income_sum, 2);
            },
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
        ],
        [
            'attribute' => 'diff_sum',
            'label' => 'Разница, без НДС',
            'value' => function (ProjectEstimate $model) {
                return TextHelper::invoiceMoneyFormat($model->diff_sum, 2);
            },
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_DIRECTOR) ||
                Yii::$app->user->can(UserRole::ROLE_FINANCE_ADVISER) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SALES_MANAGER)
        ],
        [
            'attribute' => 'status',
            'filter' => ['' => 'Все'] + ProjectEstimate::$statuses,
            's2width' => '180px',
            'value' => function (ProjectEstimate $estimate) {
                return ProjectEstimate::$statuses[$estimate->status];
            },
        ],
        [
            'attribute' => 'responsible',
            'headerOptions' => [
                'class' => 'col_project_estimate_responsible ' . ($userConfig->project_estimate_responsible ? '' : 'hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'col_project_estimate_responsible ' . ($userConfig->project_estimate_responsible ? '' : 'hidden'),
            ],
            'filter' => $searchModel->getResponsibleFilter(),
            's2width' => '180px',
            'value' => function (ProjectEstimate $model) {
                if ($model->employee) {
                    return $model->employee->getShortFio();
                }
                if ($model->project->responsibleEmployee) {
                    return $model->project->responsibleEmployee->getShortFio();
                }
            },
        ],

    ],
]); ?>

<?= \frontend\themes\kub\widgets\SummarySelectContractorWidget::widget([
    'buttons' => [
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-width button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Ответственный',
                        'url' => '#change_responsible_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align-sm']),
        Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ])),
    ],
]); ?>

<div id="many-delete" class="confirm-modal fade modal" role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные позиции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['/project/many-delete-project-estimate-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'change_responsible_modal',
]); ?>
<h4 class="modal-title">Изменить ответственного</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Ответственный
            </label>
            <?= Select2::widget([
                'id' => 'project-estimate-responsible',
                'name' => 'responsible',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'class' => 'modal-document-date',
                ],
                'data' => ArrayHelper::map($company->getEmployeeCompanies()
                    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                    ->orderBy([
                        'lastname' => SORT_ASC,
                        'firstname' => SORT_ASC,
                        'patronymic' => SORT_ASC,
                    ])->all(), 'employee_id', 'fio'),
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'project-estimate-change-responsible button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/project/project-estimate-change-responsible']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'change_status_modal',
]); ?>
<h4 class="modal-title">Изменить статус</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-status" class="label">
                Статус
            </label>
            <?= Select2::widget([
                'id' => 'project-estimate-status',
                'name' => 'status',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => ProjectEstimate::$statuses,
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'project-estimate-change-status button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php Modal::end(); ?>

<?php

$this->registerJs(<<<JS
$(document).on('click', '.project-estimate-change-status', function () {
    var data = [];
        
    if ($('.joint-operation-checkbox:checked').length > 0) {
        $(".joint-operation-checkbox:checked").each(function(){
            let row = {};
            row['projectEstimateId'] = $(this).data('id');
            row['project_id'] = $(this).data('project_id');
            row['status'] = $('#project-estimate-status').val();
            data.push(row);
        });
        $('#ajax-loading').show();
        console.log(data);
        $.post('/project/project-estimate-change-item', {ProjectEstimateForm: data}, function (data) {
            $('#ajax-loading').hide();
            if ('true' === data.success) {
                window.location.reload(true);
            }
        });
    }
});
$(document).on('click', '.project-estimate-change-responsible', function () {
    var data = [];
    if ($('.joint-operation-checkbox:checked').length > 0) {
        $(".joint-operation-checkbox:checked").each(function(){
            let row = {};
            row['projectEstimateId'] = $(this).data('projectestimateid');
            row['project_id'] = $(this).data('project_id');
            row['responsible'] = $('#project-estimate-responsible').val();
            data.push(row);
        });
        $('#ajax-loading').show();
        $.post('/project/project-estimate-change-item', {ProjectEstimateForm: data}, function (data) {
            $('#ajax-loading').hide();
            if ('true' === data.success) {
                window.location.reload(true);
            }
        });
    }
});
$(document).on("click", ".btn-confirm-yes", function() {
        var ids = [];
        let checkboxes = $(".joint-operation-checkbox:checked");
        let url = $(this).data("url");
        
        checkboxes.each(function(){
            ids.push($(this).data("id"));
        })
        
        $.post(url, {ids: ids}, function(data) {
            if ("true" === data.success) {
                window.location.reload(true);
            }
        })
    });
JS, \yii\web\View::POS_READY);