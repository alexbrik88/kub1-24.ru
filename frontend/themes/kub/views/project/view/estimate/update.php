<?php

use common\models\project\ProjectEstimate;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var ProjectEstimate $model */

$this->title = 'Редактирование сметы';

?>

<?php $form = ActiveForm::begin(array_replace(Yii::$app->params['formDefaultConfig'], [
//    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
//    'validateOnBlur' => true,
    'fieldConfig' => [
        'template' => "{input}{error}",
        'options' => [
            'class' => '',
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => '-form-control'
        ],
    ],
    'id' => 'project-estimate-form',
    'action' => Url::to(['project/project-estimate-update', 'id' => $model->id]),
    'validationUrl' => Url::to(['project/project-estimate-form-validate', 'id' => $model->id]),
])); ?>

<?= Html::hiddenInput('documentType', \frontend\models\Documents::IO_TYPE_OUT, [
    'id' => 'documentType',
]); ?>

<?= $this->render('_partials/_form_header', [
    'model' => $model,
    'form' => $form,
]); ?>

<?= $this->render('_partials/_form_body', [
    'model' => $model,
    'form' => $form,
]); ?>

<?= $this->render('_partials/_product_table_invoice', [
    'model' => $model,
    'form' => $form,
]); ?>

<?= $this->render('_partials/_form_buttons'); ?>

<?php $form->end(); ?>

<?= $this->render('_partials/_form_product_existed'); ?>
<?= $this->render('_partials/_form_product_new'); ?>

<!-- remove product -->
<div id="modal-remove-one-product" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить эту позицию из счета?
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>