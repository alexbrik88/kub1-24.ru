<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\InvoiceContractEssence;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateItem;
use common\models\TaxRate;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/** @var View $this  */
/** @var ProjectEstimate $model */
/** @var integer $ioType */
/** @var Company $company */
/** @var InvoiceContractEssence $invoiceContractEssence */
/** @var InvoiceContractTemplate $invoiceContractTemplate */
/** @var string $document */

SortableAsset::register($this);

/** @var Company $company */
$company = Yii::$app->user->identity->company;
$userConfig = Yii::$app->user->identity->config;

$ndsCellClass = 'with-nds-item';

$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');

$taxRates = TaxRate::sortedActualArray();

$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];

foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}

$js = <<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: ".sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS;

$this->registerJs($js);

?>

<?php

$this->registerCss(<<<CSS
    .checkbox-txt-bold {
      font-size: 14px!important;
    }

    .checkbox-label {
      padding-left: 0!important;
    }

    #templateList {
        width: 200px!important;
        min-height: 50px!important;
        max-height: 250px!important;
        
        border: none;
        
        overflow-x: hidden;
        overflow-y: auto;
    }

    .form-filter-drop li {
        border: none;
    }

    .invoice-template {
        display: block;
        
        width: 60%;
        float: left;
        
        text-align: left;
        
        margin: 0;
        padding: 0;
    }

    .invoice-template-edit,
    .invoice-template-confirm-delete {
        display: block;
        
        width: 15%;
        float: left;
        
        text-align: center!important;
        
        margin: auto 0!important;
        padding: auto 0!important;
    }

    .invoice-template-edit svg,
    .invoice-template-confirm-delete svg {
        color: #0097fd!important;
        
        margin: auto 0!important;
        padding: auto 0!important;
    }
CSS
);

?>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
        'style' => 'margin-bottom: 0;',
    ],
]); ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.prise_modify_toggle',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true,
        'repositionOnScroll' => true,
        'side' => ['bottom', 'left', 'top', 'right'],
    ],
]); ?>

<div class="wrap" style="position: relative">
    <div class="table-wrap table-responsive">
        <table id="table-for-invoice" class="table table-style table-count" style="position: relative;">
            <thead>
                <tr class="heading" role="row">
                    <th class="delete-column-left" style="min-width:45px; width:1%">
                    </th>
                    <th style="min-width:300px; width: 30%;">
                        Наименование
                    </th>
                    <th style="min-width:100px; width: 10%;" class="">
                        Количество
                    </th>
                    <th style="min-width:75px; width: 7%" class="">
                        Ед. измерения
                    </th>
                    <th style="min-width:75px;  width: 7%;" class="<?= $ndsCellClass ?>">
                        НДС
                    </th>
                    <th style="min-width:100px; width: 10%; position: relative;" class="">
                        <?= Html::tag('span', 'Цена', [
                            'id' => 'price_conf_toggle',
                            'class' => 'prise_modify_toggle',
                            'data-tooltip-content' => '#invoice_price_conf',
                            'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                        ]) ?>
                    </th>
                    <th style="min-width:100px; width: 10%;" class="">
                        Сумма
                    </th>
                </tr>
            </thead>
            <tbody id="table-product-list-body" data-number="<?= count($model->projectEstimateItems) ?>">
                <?php if (!empty($model->projectEstimateItems)) : ?>
                    <?php foreach ($model->projectEstimateItems as $key => $item) : ?>
                        <?= $this->render('_form_order_row', [
                            'item' => $item,
                            'number' => $key,
                            'model' => $model,
                            'ndsCellClass' => $ndsCellClass,
                            'userConfig' => $userConfig,
                        ]); ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?= $this->render('_form_add_order_row', [
                    'hasItem' => !empty($model->projectEstimateItem),
                    'ndsCellClass' => $ndsCellClass,
                    'userConfig' => $userConfig,
                ]) ?>
            </tbody>
            <tfoot>
            <tr class="template disabled-row" role="row" style="display:none">
                <td class="product-delete delete-column-left" style="white-space: nowrap;">
                    <div style="max-width: 45px">
                        <button class="remove-product-from-invoice button-clr" type="button">
                            <svg class="table-count-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                            </svg>
                        </button>
                        <button class="sortable-row-icon button-clr" type="button">
                            <svg class="table-count-icon table-count-icon_small svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                            </svg>
                        </button>
                    </div>
                </td>
                <td style="position: relative;">
                    <!-- FLOAT WIDTH INPUT -->
                    <div>
                        <input type="text" name="orderArray[][title]" class="product-title form-control tooltip-product">
                    </div>
                </td>
                <td>
                    <input disabled="disabled" type="hidden" class="tax-rate" value="0" />
                    <input disabled="disabled" type="hidden" class="order-id" name="orderArray[][id]" value="" />
                    <input disabled="disabled" type="hidden" class="product-id" name="orderArray[][product_id]" value=""/>
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="width: 100px">
                        <input disabled="disabled" type="number" min="0" step="any" lang="en"
                               class="form-control-number product-count" name="orderArray[][count]" value="1"/>
                    </div>
                    <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
                </td>
                <td class="">
                    <div style="width: 90px" class="order-param-value product-unit-name">
                        <?= Html::dropDownList('orderArray[][unit_id]',
                            null,
                            ['' => Product::DEFAULT_VALUE] + $unitItems,
                            [
                                'class' => '',
                                'disabled' => 'disabled'
                            ]
                        ); ?>
                    </div>
                </td>
                <td class="<?= $ndsCellClass ?>">
                    <div style="width: 90px" class="order-param-value price-for-sell-nds-name">
                        <?= Html::dropDownList('orderArray[][purchase_tax_rate_id]',
                            null,
                            $taxItems,
                            [
                                'class' => 'order_product_tax_rate',
                                'options' => $taxOptions,
                                'disabled' => 'disabled'
                            ]); ?>
                    </div>
                </td>
                <td class="price-one">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][price]', '0', [
                            'class' => 'form-control-number price-input',
                            'step' => 'any',
                            'disabled' => 'disabled'
                        ]); ?>
                    </div>
                </td>
                <td class="" style="text-align: right;">
                    <div class="order-param-value price-with-nds">0</div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

    <?= $form->field($model, 'orderArr', ['template' => '{input}{error}'])->textInput(['style' => 'display:none']) ?>

    <div class="row align-flex-start justify-content-between mt-3">
        <div class="column button-add-line">
            <span id="add-one-more-position" class="btn-add-line-table button-regular button-hover-content-red">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </span>
        </div>
        <div class="column">
            <?= $this->render('_total_block', [
                'model' => $model,
                'items' => $model->isNewRecord ? new ProjectEstimateItem() : $model->projectEstimateItems,
            ]) ?>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>

<script type="text/javascript">
    $(document).on('change', 'select.order_product_tax_rate', function () {
        var $row = $(this).closest('tr');
        $row.find('.tax-rate').val($(this).find('option:selected').data('rate'));

        INVOICE.recalculateInvoiceTable();
    });
</script>
