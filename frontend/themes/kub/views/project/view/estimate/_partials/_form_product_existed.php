<?php

use yii\widgets\Pjax;

?>

<div class="modal mobile-modal" id="add-from-exists">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content to-invoice-content">

            <?php Pjax::begin([
                'id' => 'pjax-product-grid',
                'linkSelector' => false,
                'formSelector' => '#products_in_order',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
