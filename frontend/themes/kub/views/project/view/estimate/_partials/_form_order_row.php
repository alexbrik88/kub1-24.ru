<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateItem;
use common\models\TaxRate;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\ImageHelper;

/* @var ProjectEstimate $model */
/* @var ProjectEstimateItem $item */
/* @var integer $ioType */
/* @var integer $number */
/* @var integer $precision */
/* @var string $ndsCellClass */

$baseName = 'orderArray[' . $number . ']';
$isExist = ProjectEstimateItem::find()->where(['id' => $item->id])->exists();
$taxRate = $item->tax;
$taxRateId = $taxRate->id;
$taxRateName = $taxRate->name;
$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedActualArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}
$precision = 2;

?>

<tr id="model_<?= $item->product_id; ?>" class="product-row" role="row">
    <td class="product-delete delete-column-left" style="white-space: nowrap;">
        <div style="max-width: 45px">
            <button class="remove-product-from-invoice button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
            <button class="sortable-row-icon button-clr" type="button">
                <svg class="table-count-icon table-count-icon_small svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                </svg>
            </button>
        </div>
    </td>
    <td style="position: relative;">
        <!-- FLOAT WIDTH INPUT -->
        <div>
        <input type="text" class="product-title form-control tooltip-product"
               style="padding-right: 25px; width: 100%;"
                name="<?= $baseName; ?>[title]"
                data-value = "<?= $item->title; ?>"
                value="<?= $item->title; ?>">
            <span class="product-title-clear">×</span>
        </div>
    </td>
    <td>
        <input type="hidden" class="tax-rate" value="<?= $taxRate->rate; ?>"/>
        <input type="hidden" class="order-id" name="<?= $baseName; ?>[id]" value="<?= $item->id; ?>"/>
        <input type="hidden" class="product-id" name="<?= $baseName; ?>[product_id]" value="<?= $item->product_id; ?>"/>

        <!-- FLOAT WIDTH input -->
        <div style="width: 100px">
            <?= Html::input('text', $baseName . '[count]', $item->count, [
                'class' => 'product-count form-control-number',
                'data-value' => $item->count,
            ]); ?>
        </div>

        <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
    </td>
    <td class="">
        <div style="width: 90px" class="order-param-value product-unit-name">
            <?= Select2::widget([
                'name' => $baseName . '[unit_id]',
                'data' => ['' => Product::DEFAULT_VALUE] + $unitItems,
                'hideSearch' => true,
                'value' => $item->unit_id,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </td>
    <td class="<?= $ndsCellClass ?>">
        <div class="order-param-value price-for-sell-nds-name" data-id="<?= $taxRateId ?>" data-name="<?= $taxRateName ?>">
            <?= Select2::widget([
                'name' => $baseName . '[purchase_tax_rate_id]',
                'data' => $taxItems,
                'options' => [
                    'class' => 'order_product_tax_rate',
                    'options' => $taxOptions,
                ],
                'hideSearch' => true,
                'value' => $item->tax->id,
                'pluginOptions' => [
                    'minimumResultsForSearch' => -1,
                    'width' => '100%',
                ],
            ]); ?>
        </div>
    </td>
    <td class="price-one">
        <!-- FLOAT WIDTH INPUT -->
        <?= Html::input('number', $baseName . '[price]', TextHelper::moneyFormatFromIntToFloat($item->price, $precision), [
            'class' => 'form-control-number price-input',
            'data-value' => TextHelper::moneyFormatFromIntToFloat($item->price, $precision),
            'step' => 'any',
        ]); ?>
    </td>
    <td class="" style="text-align: right;">
        <div class="order-param-value price-with-nds">
            <?= TextHelper::moneyFormatFromIntToFloat($item->count * $item->price, $precision); ?>
        </div>
    </td>
</tr>

<style type="text/css">
    select[readonly].select2-hidden-accessible + .select2-container {
        pointer-events: none;
        touch-action: none;
    }
</style>