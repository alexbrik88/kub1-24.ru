<?php

use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectEstimate;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\rbac\permissions;
use \frontend\themes\kub\helpers\Icon;
use yii\web\View;

/** @var View $this */
/** @var ProjectEstimate $model */
/** @var Employee $user */
/** @var ActiveForm $form */

$user = Yii::$app->user->identity;
/** @var Company $company */
$company = $user->company;
$userConfig = $user->config;

$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
];

$contractorArray = $company->getContractors()
    ->byContractor(Contractor::TYPE_CUSTOMER)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->exists();

if (empty($model->contractor_id)) {
    $contractorDropDownConfig['prompt'] = '';
}

$canAddContractor = Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
    'type' => Contractor::TYPE_CUSTOMER,
]);

$contractorsList = ArrayHelper::map($model->project->getContractors()->all(), 'id', function($c) { return $c->getShortName(); });
?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltipProjectInfo',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'side' => ['top'],
    ],
]); ?>

<?php

if ($hasProject = Yii::$app->user->identity->menuItem->project_item) {
    $projects = Project::find()
        ->select('name')
        ->andWhere(['company_id' => $company->id])
        ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS))
        //->andWhere('start_date <= ' . (new Expression('current_date()')))
        //->andWhere('end_date >= ' . (new Expression('current_date()')))
        ->indexBy('id')
        ->column();
}

$contractorDropDownConfig['options'] = Contractor::getAllContractorSelect2Options(Contractor::TYPE_CUSTOMER, Yii::$app->user->identity->company);

$customer = new ProjectCustomer();
if (!$model->isNewRecord) {
    $customers = $model->project->customers;
    $customer = current($customers);
}

?>

<?php if ($canAddContractor): ?>
    <style>
        #select2-invoice-contractor_id-results .select2-results__option:first-child {
            color: #4679AE;
            font-weight: bold;
        }
    </style>
<?php endif; ?>

<div class="wrap">
    <div class="row d-block">

        <!-- FIRST ADD INVOICE -->
        <div class="col-12">
            <div class="row">
                <div class="col-md-6">
                    <label class="label">Название сметы</label>
                    <?= $form->field($model, 'name', [
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])->textInput([
                        'class' => 'form-control',
                        'placeholder' => 'Необязательное поле',
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <label class="label">Ответственный</label>
                    <?= $form->field($model, 'responsible')->widget(Select2::class, [
                        'id' => 'project',
                        'data' => ArrayHelper::map($company->getEmployeeCompanies()
                            ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                            ->orderBy([
                                'lastname' => SORT_ASC,
                                'firstname' => SORT_ASC,
                                'patronymic' => SORT_ASC,
                            ])->all(), 'employee_id', 'fio'),
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'placeholder' => '',
                            'width' => '100%',
                            'allowClear' => true,
                            'escapeMarkup' => new JsExpression('function(text) {return text;}')
                        ],
                        'options' => [
                            'value' => $model->responsible ?: $model->project->responsible ?: null,
                        ]
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label class="label">Заказчик</label>
                    <?= $form->field($model, 'customer_id')
                            ->widget(Select2::class, [
                                'name' => 'customer_id',
                                'data' => $contractorsList,
                                'options' => $contractorDropDownConfig,
                                'pluginOptions' => [
                                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                                    'width' => '100%'
                                ],
                            ]); ?>
                </div>
                <?php if ($hasProject): ?>
                    <div class="col-6">
                        <div class="form-filter hight-line">
                            <label class="label">Проект</label>
                        </div>
                        <?= $form->field($model, 'project_id')->widget(Select2::class, [
                                'id' => 'project',
                                'data' => $projects,
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'placeholder' => '',
                                    'width' => '100%',
                                    'allowClear' => true,
                                    'language' => [
                                        'noResults' =>  new \yii\web\JsExpression('function(){
                                            return $("<span />").attr("style", "color:red").html("Сначала выберите покупателя");
                                        }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function(text) {return text;}')
                                ],
                                'options' => [
                                    'value' => $model->project_id ?: null,
                                ]
                            ]); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    // sync fields
    $("#autoinvoice-add-monthly").on("change", function() {
        if ($("#autoinvoice-add-quarterly").val() !== $(this).val())
            $("#autoinvoice-add-quarterly").val($(this).val()).trigger("change");
    });
    $("#autoinvoice-add-quarterly").on("change", function() {
        if ($("#autoinvoice-add-monthly").val() !== $(this).val())
            $("#autoinvoice-add-monthly").val($(this).val()).trigger("change");
    });

    $(document).on("select2:selecting", "#invoice-agreement", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-agreement-item")) {
            e.preventDefault();

            $("#invoice-agreement").select2("close");

            $.pjax({
                url: "/documents/agreement/update" +
                    "?id=" + $(target).data("id") +
                    "&contractor_id=" + $("#invoice-contractor_id").val() +
                    "&type=" + $("#documentType").val() +
                    "&returnTo=invoice" +
                    "&container=agreement-select-container" +
                    "&old_record=1" +
                    "&hide_template_fields=1",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });

            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));

            });
            $("#agreement-modal-container").modal("show");
            $("#invoice-agreement").val("").trigger("change");

        }
    });
    $(document).on("select2:open", "#invoice-agreement", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });

    $(document).on("select2:open", "#invoice-company_rs", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });
    $(document).on("select2:selecting", "#invoice-company_rs", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-company_rs-item") || target.classList.contains("ajax-modal-btn")) {
            e.preventDefault();
        }
    });
    $(document).on("keyup", "input.select2-search__field", function (e) {
        $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
            .attr("aria-selected", "false")
            .addClass("selected");
    });

    $(document).on("shown.bs.modal", ".modal#ajax-modal-box", function () {
        $("#invoice-company_rs").select2("close");
    });

    $(document).on("click", "#contractor_update_button", function() {
            var url  = "add-modal-contractor";
            var form = {
                documentType: INVOICE.documentIoType,
                contractorId: $(this).data("contractor-id") || 0,
                invoiceId: $(this).data("invoice-id") || 0,
        };
        INVOICE.addNewContractor("add-modal-contractor", form);
    });

    $("#config-invoice_form_article").on("change", function(e) {
        var input = this;
        var attr = input.id.replace("config-", "");
        $.post("/site/config", {"Config[invoice_form_article]": $("#config-invoice_form_article").prop("checked") ? 1 : 0}, function(data) {
            if (typeof data[attr] !== "undefined") {
                if (data[attr]) {
                    $("." + $(input).data("target")).removeClass("hidden");
                } else {
                    $("." + $(input).data("target")).addClass("hidden");
                }
            }
        })
    });

    $(document).on("shown.bs.modal", "#add-new", function() {
        refreshUniform();
    });

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });
');