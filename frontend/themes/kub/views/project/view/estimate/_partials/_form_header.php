<?php

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\project\ProjectEstimate;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var ProjectEstimate $model */
/** @var Message $message */
/** @var ActiveForm $form */
/** @var boolean $isAuto */
/** @var string $document */

/** @var ActiveForm $form */

if (!$model->date) {
    $model->date = date('Y-m-d');
}

?>

<?= Html::hiddenInput('id', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>

<div class="wrap">
    <div class="row">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-start">

                <div class="form-title d-inline-block column" style="min-width: 240px;">
                    <div class="d-inline-block" style="margin-top: 5px">Смета №</div>
                </div>

                <!-- document_number -->
                <?= $form->field($model, 'number', [
                    'options' => [
                        'class' => 'form-group d-inline-block mb-0 col-xl-2 estimate-header-shift',
                    ],
                ])->textInput([
                    'data-required' => 1,
                    'class' => 'form-control',
                    'autocomplete' => 'off',
                ]); ?>

                <!-- document_additional_number -->
                <?= $form->field($model, 'addition_number', [
                    'options' => [
                        'class' => 'form-group d-inline-block mb-0 col-xl-2 invoice-block estimate-header-shift',
                    ],
                ])->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'доп. номер',
                ]); ?>

                <div class="form-txt d-inline-block mb-0 column estimate-header-shift" style="margin-top: 13px">от</div>
                <div class="form-group d-inline-block mb-0 estimate-header-shift">
                    <div class="date-picker-wrap">
                        <?= $form->field($model, 'date')->textInput([
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker',
                            'size' => 16,
                            'data-date' => date(DateHelper::FORMAT_USER_DATE),
                            'data-date-viewmode' => 'years',
                            'data-is-inited' => 0,
                            'value' => DateHelper::format(date(DateHelper::FORMAT_DATE, false !== strpos($model->date, '-') ? strtotime($model->date) : $model->date), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>