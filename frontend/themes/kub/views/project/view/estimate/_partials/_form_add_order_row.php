<?php

use common\models\product\Product;
use frontend\rbac\permissions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use \frontend\themes\kub\helpers\Icon;

/** @var integer $hasItem */

$company = Yii::$app->user->identity->company;
$serviceCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale(false)->count();
$goodsCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale(false)->count();
$productCount = $serviceCount + $goodsCount;
//$firstCreate = Yii::$app->controller->action->id == 'first-create';
$canAdd = Yii::$app->user->can(permissions\Product::CREATE);
$createdDoc = Yii::$app->request->get('document');
$onlyProducts = $onlyServices = false;
$addProductTypesText = ($onlyServices) ? 'новую услугу' : ($onlyProducts ? 'новый товар' : 'новый товар/услугу');

?>

<tr id="from-new-add-row" class="from-new-add disabled-row" role="row" <?= $hasItem ? 'style="display: none;"': ''; ?> >
    <td class="delete-column-left">
        <span class="icon-close remove-product-from-invoice from-new"></span>
    </td>
    <td class="">
        <div id="invoice_product_select">
            <?php if (!$productCount && $canAdd) : ?>
                <div id="tooltip_add_product_block" style="position: relative">
                    <?= Html::textInput('add_new', '', [
                        'class' => 'add-first-product-button add-modal-produc-new form-control',
                        'style' => 'width: 100%; background-color: #fff; cursor: pointer;',
                        'placeholder' => "Добавить {$addProductTypesText}",
                        'onfocus' => 'this.blur();',
                        'readonly' => true,
                    ]) ?>
                    <a style="position: absolute;top: 0px;right:15px" class="tooltip-first-r" data-tooltip-content="#tooltip_help_add_product">&nbsp;</a>
                    <div class="not-clickable ">
                        &nbsp;
                    </div>
                </div>
                <?= Html::hiddenInput('', 1, ['class' => 'invoice-nds_view_type_id'])?>
            <?php endif ?>
            <div class="add-exists-product<?= $productCount == 0 ? ' hidden' : '' ?>">
                <?php echo Select2::widget([
                    'id' => 'order-add-select',
                    'name' => 'addOrder',
                    'initValueText' => '',
                    'options' => [
                        'placeholder' => '',
                        'class' => 'form-control ',
//                        'data-doctype' => $ioType,
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'width' => '100%',
                        'minimumInputLength' => 1,
                        'dropdownCssClass' => 'product-search-dropdown',
                        'ajax' => [
                            'url' => "/product/search",
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                            'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(product) { return product.text; }'),
                        'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                    ],
                ]); ?>
                <div id="order-add-static-items" style="display: none;">
                    <ul class="order-add-static-items select2-results__options">
                        <?php if ($canAdd) : ?>
                            <li class="select2-results__option add-modal-produc-new red" aria-selected="false">
                                 <?= Icon::PLUS . " Добавить {$addProductTypesText}" ?>
                            </li>
                        <?php endif ?>
                        <li class="select2-results__option add-modal-services<?= $onlyProducts || $serviceCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                        </li>
                        <li class="select2-results__option add-modal-products<?= $onlyServices || $goodsCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                        </li>
                    </ul>
                </div>
                <script type="text/javascript">
                    $('#order-add-select').on('select2:open', function (evt) {
                        var prodItems = $('#select2-order-add-select-results').parent().children('.order-add-static-items');
                        if (prodItems.length) {
                            prodItems.remove();
                        }
                        $('.order-add-static-items').clone().insertAfter('#select2-order-add-select-results');
                    });
                    $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function() {
                        $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                        $(this).addClass('select2-results__option--highlighted');
                    });
                </script>
            </div>
        </div>
    </td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
</tr>