<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateItem;
use yii\web\View;

/** @var View $this */
/** @var ProjectEstimate $model */
/** @var ProjectEstimateItem|ProjectEstimateItem[] $items */

$summary = 0;
$tax = $nds = 0;
$amount = 0;

if (is_array($items)) {
    /** @var ProjectEstimateItem $item */
    foreach ($items as $item) {
        $summary += $item->price * $item->count;
        if (2 !== $model->nds_out) {
            $nds = 0 === $model->nds_out
                ? $item->count * $item->price * ($item->tax ? ((float)$item->tax->rate / (1 + (float)$item->tax->rate)) : 1)
                : $item->count * $item->price * ($item->tax ? ((float)$item->tax->rate) : 1);
        } else {
            $nds = 0;
        }
        $tax += $nds;
        $amount += $item->price * $item->count + ($model->nds_out ? $nds : 0);
    };
}

$this->registerCss('
    #select2-projectestimate-nds_out-container {
        padding-right: 12px!important;
    }
    
    #nds_out-pjax-container .select2-container--krajee-bs4 .select2-selection--single .select2-selection__arrow {
        width: 14px;
        background-image: url(/images/icon_dropdown_blue.png) !important;
    }
    
    #nds_out-pjax-container  .select2-container--krajee-bs4 .select2-selection,
    #nds_out-pjax-container .select2-container--krajee-bs4 .select2-selection:focus
    {
        padding: 4px 10px;
        border: none !important;
        min-height: auto!important;
    }
    
    #select2-projectestimate-nds_out-container {
        color: #0097fd;
    }
    
    #nds_out-pjax-container .select2-container--krajee-bs4.select2-container--open .select2-selection--single .select2-selection__arrow {
        opacity: 0;
    }
');

?>

<div id="invoice-sum" class="total-txt text-right table-resume">
    <div class="mb-1">
        <div>Итого <span class="total_currency_label">руб:</div>
        <div><strong class="total_price"><?= TextHelper::invoiceMoneyFormat($summary, 2); ?></strong></div>
    </div>

    <div class="mb-1">
        <div style="padding-top: 3px">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'nds_out-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
                'options' => ['style' => 'display: inline-block;']
            ]); ?>
            <?= \kartik\select2\Select2::widget([
                'hideSearch' => true,
                'model' => $model,
                'attribute' => 'nds_out',
                'data' => Invoice::$ndsViewList,
                'options' => [
                    'class' => 'form-control nds-select',
                    'data' => ['id' => $tax],
                ],
                'pluginOptions' => [
                    'theme' => "krajee-bs4",
                    'width' => "200px",
                    'minimumResultsForSearch' => -1,
                ]
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
        <div><strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($tax); ?></strong></div>
    </div>

    <div class="mb-1">
        <div>Всего к оплате<span class="total_currency_label">, руб</span>:</div>
        <div><strong class="amount"><?= TextHelper::invoiceMoneyFormat($amount, 2); ?></strong></div>
    </div>
</div>

<?php
$this->registerJs('
    var checkInInvoiceNds = function() {
        if ($("#invoice-nds_out").val() == "2") {
            $(".with-nds-item").addClass("hidden");
            $(".without-nds-item").removeClass("hidden");
        } else {
            $(".with-nds-item").removeClass("hidden");
            $(".without-nds-item").addClass("hidden");
        }
    }
    $(document).on("change", "#projectestimate-nds_out", function() {
        checkInInvoiceNds();
    });
    $(document).on("change", "#projectestimate-contractor_id", function() {
        $.pjax.reload("#nds_out-pjax-container", {"type": "post", "data": $(this).closest("form").serialize()});
        $(document).on("pjax:complete", "#nds_out-pjax-container", function(){
            INVOICE.recalculateInvoiceTable();
            checkInInvoiceNds();
            
            $("select.nds-select").select2({
                theme: "krajee-bs4",
                width: "100%",
                minimumResultsForSearch: -1,
                escapeMarkup: function(markup) {
                    return markup;
                }
            });            
        });
    });
');