<?php

use frontend\modules\analytics\models\OddsSearch;
use yii\bootstrap4\Nav;
use yii\helpers\Url;

/* @var $this yii\web\View */

/* @var $tab string */
/* @var $subtab string */

$tab = $tab ?? 'analytics';
$subtab = $subtab ?? 'odds';

// set year
if (isset($year) && $year > 1970 && $year < 2100) {
    \Yii::$app->session->set('modules.projects.analytics.year', (int)$year);
}
// set odds activeTab
if (isset($activeTab) && in_array($activeTab, [OddsSearch::TAB_ODDS, OddsSearch::TAB_ODDS_BY_PURSE, OddsSearch::TAB_ODDS_BY_PURSE_DETAILED])) {
    \Yii::$app->session->set('modules.projects.analytics.activeTab', (string)$activeTab);
}

$year = \Yii::$app->session->get('modules.projects.analytics.year', date('Y'));
$activeTab = \Yii::$app->session->get('modules.projects.analytics.activeTab', OddsSearch::TAB_ODDS);
$route = [
    '/project/view',
    'id' => $model->id,
    'tab' => $tab
];
?>

<div class="nav-tabs-row">
    <?= Nav::widget([
        'id' => 'contractor-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'ОДДС',
                'url' => array_merge($route, ['year' => $year, 'subtab' => 'odds']),
                'options' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($subtab == 'odds' ? ' active' : '')
                ],
            ],
            [
                'label' => 'ОПиУ',
                'url' => array_merge($route, ['year' => $year, 'subtab' => 'profit-and-loss']),
                'options' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($subtab == 'profit-and-loss' ? ' active' : '')
                ],
            ],
            [
                'label' => 'По продажам',
                'url' => array_merge($route, ['year' => $year, 'subtab' => 'selling-report']),
                'options' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($subtab == 'selling-report' ? ' active' : '')
                ],
            ],
        ],
    ]); ?>
</div>

<?= $this->render('analytics/project-' . $subtab, [
    'project' => $model,
    'year' => $year,
    'yearFilter' => $yearFilter,
    'activeTab' => $activeTab
]) ?>