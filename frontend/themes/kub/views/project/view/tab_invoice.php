<?php

use common\models\Company;
use common\models\project\Project;
use common\models\project\ProjectSearch;
use yii\data\ActiveDataProvider;

/** @var Project $model */
/** @var ProjectSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var Company $company */
/** @var integer $ioType */
/** @var string $tab */

$tabData = [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'company' => $company,
    'ioType' => $ioType,
];
?>

<div class="tab-content">
    <div id="tab1" class="tab-pane invoice-tab active">
        <?= $this->render("invoice/{$tab}", $tabData); ?>
    </div>
</div>