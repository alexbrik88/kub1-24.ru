<?php

use common\models\project\Project;
use common\models\project\ProjectCustomerSearch;
use yii\web\View;

/* @var $this View */
/* @var $model Project */

$searchModel = new ProjectCustomerSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

$emptyMessage = 'Вы еще не добавили заказчика.';

?>

<div class="wrap wrap_padding_small mt-3 pl-4 pr-4 pt-2 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="row page-in">
            <div class="pt-3 pb-4 col-12 pr-2">
                <div class="label col-12 mb-3">
                    Описание
                </div>
                <div class="text-justify">
                    <?= $model->description ?>
                </div>
            </div>
        </div>
    </div>
</div>
