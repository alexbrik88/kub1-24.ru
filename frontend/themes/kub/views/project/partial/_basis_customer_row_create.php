<?php

use common\components\TextHelper;
use frontend\themes\kub\components\Icon;
use yii\helpers\Html;
use common\models\Contractor;
use common\models\Agreement;

/** @var $company_id int */
/** @var $contractor_id int */
/** @var $agreement_id int */
/** @var $amount int */

$contractor = Contractor::findOne([
    'company_id' => $company_id,
    'id' => $contractor_id
]);
$agreement = Agreement::findOne([
    'company_id' => $company_id,
    'id' => $agreement_id
]);

$canDelete = $canDelete ?? false;
?>

<tr id="tr-new-<?=($contractor_id.'-'.$agreement_id)?>">
    <td style="width: 7%!important;">

    </td>
    <td style="width: 30%!important;">
        <div class="info">
            <?= ($contractor) ? $contractor->getNameWithType() : '---'; ?>
            <input type="hidden" name="additional_contractors[<?=($contractor_id)?>]" value="<?= ($contractor) ? $contractor_id : null ?>"/>
        </div>
    </td>
    <td style="width: 30%!important;">
        <div class="info">
            <?= ($agreement) ? $agreement->getTitle() : ''; ?>
            <input type="hidden" name="additional_agreements[<?=($contractor_id)?>]" value="<?= ($agreement) ? $agreement_id : null ?>"/>
        </div>
    </td>
    <td style="width: 12.5%!important;">
        <div class="info">
            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
            <input type="hidden" name="additional_amounts[<?=($contractor_id)?>]" value="<?= ($amount) ? $amount : null ?>"/>
        </div>
    </td>
    <td style="width: 8%!important;">
        <?php if ($canDelete): ?>
            <?= Html::tag('span', Icon::get('garbage'), [
                'title' => 'Удалить',
                'class' => 'new-project-customer-delete link pl-1 pr-1',
                'onclick' => new \yii\web\JsExpression('$(this).closest("tr").remove()')
            ]) ?>
        <?php endif; ?>
    </td>
</tr>