<?php

use common\components\TextHelper;
use common\models\project\ProjectCustomer;
use frontend\themes\kub\components\Icon;
use yii\helpers\Html;

/** @var $item ProjectCustomer */

$canDelete = $canDelete ?? false;
?>

<tr id="tr-<?= $item->id;?>">
    <td style="width: 7%!important;">

    </td>
    <td style="width: 30%!important;">
        <div class="info">
            <?= $item->customerName ?: ($item->customer ? $item->customer->getNameWithType() : ''); ?>
        </div>
    </td>
    <td style="width: 30%!important;">
        <div class="info">
            <?= isset($item->agreement) ? $item->agreement->getTitle() : ''; ?>
        </div>
    </td>
    <td style="width: 12.5%!important;">
        <div class="info">
            <?= TextHelper::invoiceMoneyFormat($item->amount, 2); ?>
        </div>
    </td>
    <td style="width: 8%!important;">
        <?php if ($canDelete): ?>
            <?= Html::a(Icon::get('garbage'), '', [
                'title' => 'Удалить',
                'class' => 'customer-delete link pl-1 pr-1',
                'data' => [
                    'project-customer-id' => $item->id,
                ],
            ]) ?>
        <?php endif; ?>
    </td>
</tr>