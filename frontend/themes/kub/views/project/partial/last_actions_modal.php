<?php

use common\components\date\DateHelper;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>


<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>
<table class="table table-style">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Тип</th>
        <th>Сотрудник</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= DateHelper::format(date('Y-m-d', $model->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
        <td>Создан</td>
        <td><?= $model->getResponsibleEmployee()->one()->fio; ?></td>
    </tr>
    <?php foreach ($logArray as $log): ?>
        <tr>
            <td><?= DateHelper::format(date('Y-m-d', $log->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></td>
            <td><?= ArrayHelper::getValue(DocumentLogWidget::$eventsM, $log->log_event_id) ?></td>
            <td><?= $log->employeeCompany ? $log->employeeCompany->fio : ($log->employee ? $log->employee->fio : '') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php Modal::end(); ?>

<?= AjaxFormDialog::widget() ?>
