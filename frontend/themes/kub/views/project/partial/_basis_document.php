<?php

use common\models\project\Project;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\Pjax;

$companyAgreementID = !empty($companyAgreementID) ? $companyAgreementID : [];
$agreementDropDownList = !empty($agreementDropDownList) ? $agreementDropDownList : [];

$attribute = !empty($attribute) ? $attribute : 'document';

$pjaxId = !empty($pjaxId) ? $pjaxId : 'agreement-pjax-container';
$contractor_id = !empty($contractor_id) ? $contractor_id : '';

Pjax::begin([
    'id' => $pjaxId,
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'class' => $pjaxId,
        'data' => [
            'url' => Url::to(['/project/basis-document']),
            'contractor_id' => $contractor_id,
        ]
    ]
]);

/** @var Project $model */
echo Select2::widget([
    'id' => 'project_document-' . $contractor_id,
    'model' => $model,
    'attribute' => $attribute,
    'name' => 'document',
    'data' => $agreementDropDownList,
    'hideSearch' => true,
    'pluginOptions' => [
        'width' => '100%',
        'allowClear' => true,
        'placeholder' => '',
        'templateResult' => new JsExpression('formatAgreementResult'),
        'language' => [
            'noResults' =>  new JsExpression('function(){
                return $("<span />").attr("style", "color:red").html("Сначала выберите заказчика");
            }'),
        ],
        'escapeMarkup' => new JsExpression('function(text) {return text;}'),
    ],
    'options' => [
        'id' => 'projectcustomer-agreement_id_' . $contractor_id,
        'class' => 'projectcustomer-agreement',
        'disabled' => empty($agreementDropDownList),
        'data' => [
            'delay' => 10,
            'refresh' => false,
            'contractor_id' => $contractor_id,
            'container' => $pjaxId,
        ]
    ]
]);

$this->registerJs('
    var companyAgreementID = ' . json_encode($companyAgreementID) . ';

    function htmlEscape(str) {
        return str
            .replace(/&/g, \'&amp;\')
            .replace(/"/g, \'&quot;\')
            .replace(/\'/g, \'&#39;\')
            .replace(/</g, \'&lt;\')
            .replace(/>/g, \'&gt;\');
    };
    function formatAgreementResult(data, container) {
        if (data.id && data.id !== "add-modal-agreement" && data.disabled !== true) {

            var input = $("#project_agreement");
            var content = \'<div class="agreement-item-name-label">\';

            content += \'<i class="pull-right link" title="Редактировать"> \'
                       + \'<svg class="edit-agreement-item svg-icon" data-id="\' + data.id + \'" >\'
                       + \'<use xlink:href="/img/svg/svgSprite.svg#pencil"></use>\'
                       + \'</svg>\'
                       + \'</i>\';
            
            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            $(container).html(content);
        } else if (data.id) {
            var input = $("#project_agreement");
            $(container).addClass("ajax-modal-btn")
                .attr("title", "Добавить договор")
                .attr("data-url", input.data("create-url"))
                .html(data.text);
        } else {
            $(container).html(data.text);
        }

        return container;
    };
', \yii\web\View::POS_HEAD);

Pjax::end();

$this->registerJs('
if ($("#agreement-pjax-container .select2-container--disabled").length) {
    $("#agreement-pjax-container .select2-container--disabled")
        .attr("data-tooltip-content", "#document-basis-empty")
        .tooltipster({
            theme: ["tooltipster-kub"],
            trigger: "hover",
            side: "top",
        });
}
'); ?>

<div style="display:none">
    <div id="document-basis-empty">
        Укажите заказчика, чтобы добавить договор.
    </div>
</div>