<?php

use common\components\helpers\ArrayHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\project\Project;
use common\models\project\ProjectCustomerSearch;
use frontend\themes\kub\helpers\Icon;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/** @var $model ProjectForm */
/** @var $form ActiveForm */
/** @var $this yii\web\View */

if (!isset($employee)) {
    $employee = Yii::$app->user->identity;
}
$userConfig = $employee->config;
$company = $employee->company;

$agreementDropDownList = [];
$companyAgreementID = [];

$searchModel = new ProjectCustomerSearch();

$employers = ArrayHelper::map($model->getResponsibleData(), 'employee_id', 'fio');
?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'start_date')->textInput([
                    'class' => 'form-control date-picker ico',
                    'data-date-viewmode' => 'years',
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'end_date')->textInput([
                    'class' => 'form-control date-picker ico',
                    'data-date-viewmode' => 'years',
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'status')->widget(Select2::class, [
                    'data' => Project::$statuses,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'responsible')->widget(Select2::class, [
                    'pluginOptions' => [
                        'width' => '100%',
                        'allowClear' => true,
                        'placeholder' => '',
                    ],
                    'data' => $employers,
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'number')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'addition_number')->textInput([
                    'placeholder' => 'Дополнительный номер',
                ])->label('Доп. номер'); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'industry_id')->widget(Select2::class, [
                    'data' => $model->getIndustryData(),
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]) ?>
            </div>
        </div>
        <div class="mt-2">
            <?= $form->field($model, 'projectCustomers', [
                'parts' => [
                    '{label}' => '&nbsp;',
                    '{input}' => $this->render('_project_form_customers', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                ],
            ]); ?>
        </div>
        <div class="row">
            <div class="form-group col-12 mb-3 pb-2">
                <?= $form->field($model, 'description')->textarea([
                    'maxlength' => true,
                    'rows' => 10,
                    'style' => 'width: 100%; resize: vertical;'
                ]); ?>
            </div>
        </div>
    </div>
</div>