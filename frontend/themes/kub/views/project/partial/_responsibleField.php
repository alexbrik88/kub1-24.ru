<?php

use common\models\employee\Employee;
use kartik\widgets\Select2;

?>
<div class="employee form-group pl-0 ml-0">
    <label for="contractor-responsible_employee" class="label">
        Сотрудник&nbsp;<i class="pull-right link remove pointer" title="Удалить" style="cursor: pointer">
                <svg class="edit-agreement-item svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </i>
    </label>
    <?=
        /** @var Employee[] $employers */
        Select2::widget([
            'id' => 'employee' . floor(rand(1, 10000)),
            'name' => 'employee[]',
            'value' => isset($value) ? $value : 0,
            'pluginOptions' => [
                'width' => '100%',
                'allowClear' => true,
                'placeholder' => '',
            ],
            'data' => $employers,
        ]); ?>
</div>

<?php
$this->registerJs('
    $(".remove").click(function(e){
        e.preventDefault();
        $(this).closest(".employee").remove();
    });
', \yii\web\View::POS_READY);
?>
