<?php

use common\models\project\Project;
use yii\widgets\ActiveForm;
use common\models\AgreementType;
use common\models\Company;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\web\View;

/* @var $model Project */
/* @var $form ActiveForm */
/* @var $this yii\web\View */

$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$tabActive = Yii::$app->request->get('tab');
?>

<?= Html::input('hidden', 'tab', $tabActive) ?>

<div class="wrap p-4">
    <div class="p-1">
        <div class="form-group mb-0">
            <?= $form->field($model, 'name')->textInput([
                'maxlength' => true,
                'placeholder' => 'Введите название проекта'
            ]); ?>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'nav nav-tabs w-100 mb-3 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane pl-3 pt-3 pr-3',
                    'style' => 'width:100%'
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Информация',
                        'content' => $this->render('_projectInfo', [
                            'model' => $model,
                            'form' => $form
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . (!$tabActive || $tabActive == '_projectInfo' || $tabActive == '_projectTurnover' ? ' active' : ''),
                            'data-tab' => '_projectInfo'
                        ],
                        'active' => (!$tabActive || $tabActive == '_projectInfo' || $tabActive == '_projectTurnover')
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php
$tab = Yii::$app->request->get('tab') ?: '_projectInfo';
$js = <<<JS
    $(document).on("click", ".nav-item", function(e) {
        var form = $("#product-form");
        var cancelBtn = $(form).find('.button-cancel-edit');
        var newTab = $(this).find('.nav-link').attr('data-tab');
        if (newTab.length && cancelBtn.length) {
            $(form).attr("action", $(form).attr("action").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + newTab));
            $(cancelBtn).attr("href", $(cancelBtn).attr("href").replace(/tab=[a-zA-Z0-9_]+/, 'tab=' + newTab));
        }
        return false;
    });
JS;
$this->registerJs($js);

$this->registerJs('
$(document).on("change", ".projectcustomer-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();
        var customer = $(this).data("contractor_id"),
            container = $(this).data("container");
        
        if (!customer) {
            customer = $("#project-customer_id").val();
        }

        $.pjax({
            url: "/project/create-basis-document?contractor_id=" + customer 
                + "&type=" + ' . AgreementType::TYPE_CUSTOMER . '
                + "&container=agreement-select-container",
            container: "#agreement-form-container",
            push: false,
            timeout: 5000
        });

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
        });
        
        $("#agreement-modal-container").modal("show");
        $("#" + container + " select").val("").trigger("change");
    }
});
$(document).on("select2:selecting", ".projectcustomer-agreement", function(e) {
    var target = e.params.args.originalEvent.target;

    if (target.classList.contains("edit-agreement-item")) {
        e.preventDefault();
        var customer = $(this).data("contractor_id"),
            container = $(this).data("container");
        
        if (!customer) {
            customer = $("#project-customer_id").val();
        }
        
        $(this).select2("close");

        $.pjax({
            url: "/project/update-basis-document?id=" + $(target).data("id")
                + "&type=" + ' . AgreementType::TYPE_CUSTOMER . '
                + "&contractor_id=" + customer
                + "&container=agreement-select-container",
            container: "#agreement-form-container",
            push: false,
            timeout: 10000
        });

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
        });
        
        $("#agreement-modal-container").modal("show");
        $("#" + container + " select").val("").trigger("change");
    }
});

', \yii\web\View::POS_READY);

$this->registerCss(<<<CSS
    ul#select2-project-customer_id-results .select2-results__option:first-child { font-weight: bold; }
    .select2-selection__clear { display: none; } 
CSS, [], View::POS_HEAD
);

