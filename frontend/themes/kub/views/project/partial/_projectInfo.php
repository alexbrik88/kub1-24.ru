<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\project\Project;
use common\models\project\ProjectCustomerSearch;
use common\models\project\ProjectEmployee;
use frontend\themes\kub\helpers\Icon;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model Project */
/** @var $form ActiveForm */
/** @var $this yii\web\View */

$user = Yii::$app->user->identity;
$userConfig = $user->config;
$company = $user->company;

$isEdit = false;
$isNew = false;

$agreementDropDownList = [];
$companyAgreementID = [];

$customerId = $model->customer_id;

if (!empty($customerId)) {
    $agreementArray = Agreement::find()
        ->joinWith('agreementType')
        ->andWhere(['company_id' => $company->id])
        ->andWhere(['contractor_id' => $customerId])
        ->orderBy([
            'agreement_type.name' => SORT_ASC,
            'agreement.document_date' => SORT_DESC,
        ])
        ->all();

    if (!empty($agreementArray)) {
        $agreementDropDownList += ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '];
    }

    /** @var $agreement \common\models\Agreement */
    foreach ($agreementArray as $agreement) {
        $agreementDropDownList[$agreement->id] = '№ ' . $agreement->document_number . ' от '
            . DateHelper::format(
                $agreement->document_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            );
        $companyAgreementID[$agreement->getListItemValue()] = $agreement->document_number;
    }
}

$searchModel = new ProjectCustomerSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$emptyMessage = 'Вы еще не добавили ни одного заказчика. '
    . Html::a('Добавить заказчика', '#project-customer-append', [
        'id' => 'project-customer-append',
        'class' => 'link'
    ]);

$employers = ArrayHelper::map((
    EmployeeCompany::find()
        ->alias('employee_company')
        ->joinWith(['employee', 'company', 'employeeRole'])
        ->andWhere(['employee_company.company_id' => $company->id])
        ->andWhere(['employee.is_deleted' => false])
        ->andWhere(['employee_company.is_working' => true])
        ->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])
        ->all()
    ), 'employee_id', 'fio');

$projectEmployee = ProjectEmployee::find()
    ->andWhere(['company_id' => $company->id])
    ->andWhere(['project_id' => $model->id])
    ->all();
?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Дата начала проекта</label>
                </div>
                <?= $form->field($model, 'start_date', [
                    'options' => [
                        'class' => '',
                    ],
                    'wrapperOptions' => [
                        'class' => '',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker ico',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->start_date ?: date('Y-m-d'),
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Дата завершения проекта</label>
                </div>
                <?= $form->field($model, 'end_date', [
                    'options' => [
                        'class' => '',
                    ],
                    'wrapperOptions' => [
                        'class' => '',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker ico',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->end_date
                        ?: date('Y-m-d', strtotime(date('Y') . '-12-31')),
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Статус</label>
                </div>
                <?= $form->field($model, 'status', [
                    'options' => [
                        'class' => 'required'
                    ],
                ])->widget(Select2::class, [
                    'data' => ['В работе', 'Завершен'],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <label for="contractor-responsible_employee" class="label">
                    Ответственный
                </label>
                <?=
                /** @var Employee[] $employers */
                $form->field($model, 'responsible')
                    ->widget(Select2::class, [
                        'id' => 'responsible',
                        'name' => 'responsible',
                        'options' => [
                            'value' => $model->responsible ?: $userConfig->employee->id,
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'allowClear' => true,
                            'placeholder' => '',
                        ],
                        'data' => $employers,
                    ]); ?>

                <?php /*
                foreach($projectEmployee as $projectEmployer):
                    echo $this->render('_responsibleField', [
                        'value' => $projectEmployer->employee_id,
                        'employers' => $employers,
                    ]);
                endforeach;
                ?>

                <div class="append-employer">
                    <a href="#" class="append-employer__btn">
                        <?= Icon::PLUS ?> Добавить сотрудника</a>
                </div>

                <?php
                $this->registerJs('
                    $(".append-employer__btn").click(function(e){
                        e.preventDefault();
                        $.post("/project/get-employer", {}, function(data){
                            $($(data)).insertBefore(".append-employer")
                        })
                    });
                ', \yii\web\View::POS_READY);
                */ ?>

            </div>



        </div>
        <div class="row">
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Номер проекта</label>
                </div>
                <?= $form->field($model, 'number', [
                    'options' => [
                        'class' => '',
                    ],
                    'wrapperOptions' => [
                        'class' => '',
                    ],
                ])->textInput(['value' => $model->number ?: Project::getNextProjectNumber($company)]); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">&nbsp;</label>
                </div>
                <?= $form->field($model, 'addition_number')
                    ->textInput([
                        'class' => 'form-control',
                        'placeholder' => 'Дополнительный номер',
                    ])
                    ->label(false); ?>
            </div>
            <div class="form-group col-3 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Направление</label>
                </div>
                <?= $form->field($model, 'industry_id')
                    ->widget(Select2::class, [
                        'data' => \common\models\company\CompanyIndustry::getSelect2Data(),
                        'options' => [
                            'placeholder' => 'Без направления',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Направление'); ?>
            </div>
        </div>
        <div class="row mt-2">
            <div class="form-group col-12 mb-3 pb-2">
                <?= $this->render('_basis_customer_table', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'emptyMessage' => $emptyMessage,
                    'update' => true,
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12 mb-3 pb-2">
                <div class="form-filter">
                    <label class="label weight-700" for="cause">Описание</label>
                </div>
                <?= $form->field($model, 'description')->textarea([
                    'maxlength' => true,
                    'rows' => 10,
                    'style' => 'width: 100%;resize: vertical;'
                ]); ?>
            </div>
        </div>
    </div>
</div>