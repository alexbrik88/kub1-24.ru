<?php

/** NOT USED */

/*
use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\project\Project;
use common\models\project\ProjectEmployee;
use frontend\themes\kub\helpers\Icon;
use kartik\widgets\Select2;
use yii\web\View;
use yii\widgets\ActiveForm;

$userConfig = Yii::$app->user->identity->config;
$company = $userConfig->employee->company;

$employers = ArrayHelper::map((
    EmployeeCompany::find()
        ->alias('employee_company')
        ->joinWith(['employee', 'company', 'employeeRole'])
        ->andWhere(['employee_company.company_id' => $company->id])
        ->andWhere(['employee.is_deleted' => false])
        ->andWhere(['employee_company.is_working' => true])
        ->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])
        ->all()
    ), 'employee_id', 'fio');

$projectEmployee = ProjectEmployee::find()
    ->andWhere(['company_id' => $company->id])
    ->andWhere(['project_id' => $model->id])
    ->all();

?>

<div class="pb-3">
    <div class="form-group col-5 pl-0 ml-0">
        <label for="contractor-responsible_employee" class="label">
            Ответственный
        </label>
        <?=

        $form->field($model, 'responsible')
            ->widget(Select2::class, [
                'id' => 'responsible',
                'name' => 'responsible',
                'options' => [
                    'value' => $model->responsible ?: $userConfig->employee->id,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => $employers,
            ]); ?>
    </div>

    <?php
        foreach($projectEmployee as $projectEmployer):
            echo $this->render('_responsibleField', [
               'value' => $projectEmployer->employee_id,
               'employers' => $employers,
            ]);
        endforeach;
    ?>

    <div class="form-group col-5 pl-0 ml-0 append-employer">
        <a href="#" class="append-employer__btn">
            <?= Icon::PLUS ?> Добавить сотрудника</a>
    </div>
</div>



<?php
    $this->registerJs('
        $(".append-employer__btn").click(function(e){
            e.preventDefault();
            $.post("/project/get-employer", {}, function(data){
                $($(data)).insertBefore(".append-employer")
            })
        });
    ', View::POS_READY);
?>