<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectCustomerSearch;
use frontend\modules\documents\widgets\SummarySelectWidget;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/** @var $searchModel ProjectCustomerSearch */
/** @var $dataProvider ActiveDataProvider */
/** @var $emptyMessage string */
/** @var $update boolean */

/** @var Project $model */

/** @var array $customerDropDownList */
/** @var array $agreementDropDownList */

$update = $update ?? false;
$showPayments = $showPayments ?? false;
$showCheckboxes = $showCheckboxes ?? false;

/** @var Employee $user */
$user = Yii::$app->user->identity;
$company = $user->company;

$agreementDropDownList = [];
$companyAgreementID = [];

$customerDropDownList = ['add-modal-contractor' => Icon::PLUS . ' Добавить заказчика '];
$contractors = Contractor::find()
    ->andWhere(['and',
        ['type' => Contractor::TYPE_CUSTOMER],
        ['company_id' => $company->id],
        ['status' => Contractor::ACTIVE],
    ])
    ->all();

//////////////////
$canDelete = true;
//////////////////

/** @var Contractor $contractor */
foreach ($contractors as $contractor) {
    $customerDropDownList[$contractor->id] = $contractor->getNameWithType();
}

$data = $searchModel->getBaseQuery(Yii::$app->request->queryParams, true)->all();

$sorting = ['amount' => '', 'sortAmount' => 'amount', 'paymentSum' => '', 'sortPaymentSum' => 'paymentSum'];

if (false !== array_search('-amount', Yii::$app->request->queryParams)) {
    $sorting['amount'] = 'sort_desc';
    $sorting['sortAmount'] = 'amount';
}
if (false !== array_search('amount', Yii::$app->request->queryParams)) {
    $sorting['amount'] = 'sort_asc';
    $sorting['sortAmount'] = '-amount';
}
if (false !== array_search('-paymentSum', Yii::$app->request->queryParams)) {
    $sorting['paymentSum'] = 'sort_desc';
    $sorting['sortPaymentSum'] = 'paymentSum';
}
if (false !== array_search('paymentSum', Yii::$app->request->queryParams)) {
    $sorting['paymentSum'] = 'sort_asc';
    $sorting['sortPaymentSum'] = '-paymentSum';
}

$this->registerCss('
    .filter-select2-select-container {
        position: absolute;    
        top: 0;
        left:10px;
    }
    
    ul#select2-projectcustomersearch-client--filter-results .select2-results__option {
        font-weight: normal!important;
    }
');

$this->registerJs('
    $(document).on("click", "#projectcustomersearch-client-toggle", function (e) {
        e.preventDefault();
        if (!$("#projectcustomersearch-client--filter").data("select2").isOpen()) {
            $("#projectcustomersearch-client--filter").select2("open");
        }
        return false;
    });
    
    $(document).on("select2:select", "#projectcustomersearch-client--filter", function(e){
        var url = window.location.href,
            separator = (url.indexOf("?")===-1)?"?":"&",
            re = new RegExp(separator + "ProjectCustomerSearch[^&]+", "gi"),
            newParam = separator + "ProjectCustomerSearch[client]=" + $("#projectcustomersearch-client--filter").val();
            newUrl = url.replace(re, "");
            newUrl += newParam;
            window.location.href = newUrl;
    });
', View::POS_READY);

?>

<table id="basis-customer-table" class="table table-style table-count-list border-top border-bottom border-left">
    <thead>
        <tr>
            <?php if ($showCheckboxes): ?>
                <th width="1%"></th>
            <?php endif; ?>
            <th style="width: 7%!important;"><div class="th-title">№№</div></th>
            <th class="dropdown-filter" style="width: 42.5%!important;" >
                <div id="projectcustomersearch-client-toggle" class="th-title filter filter-open <?= $searchModel->client ? 'active' : '' ?>">
                    Заказчик
                    <div class="th-title-btns">
                        <button type="button" class="th-title-btn button-clr">
                            <?= Icon::get('filter', ['class' => 'th-title-icon-filter']); ?>
                        </button>
                        <div class="filter-select2-select-container">
                            <?= kartik\select2\Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'client',
                                'data' => $searchModel->getCustomerFilter(),
                                'options' => [
                                    'id' => 'projectcustomersearch-client--filter'
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'templateResult' => new JsExpression('function(data, container) {return data.text;}'),
                                    'theme' => Select2::THEME_KRAJEE_BS4 . ' select2-container--grid-filter',
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </th>
            <th style="width: 30%!important;"><div class="th-title">Основание</div></th>
            <th style="width: 12.5%!important;">
                <div class="th-title <?= $sorting['amount']; ?>">
                    <a class="th-title-name desc" href="<?= Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => $sorting['sortAmount']])); ?>" data-sort="amount">
                        Сумма по договору
                    </a>
                    <?= Html::beginTag('span', ['class' => 'th-title-btns',]) .
                        Html::a(Icon::get('count-arrow', ['class' => 'th-title-icon th-title-icon_reverse',]),
                            Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => 'amount'])), ['class' => 'th-title-btn icon_asc button-clr',]) .
                        Html::a(Icon::get('count-arrow', ['class' => 'th-title-icon',]),
                            Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => '-amount'])), ['class' => 'th-title-btn icon_desc button-clr',]) .
                        Html::endTag('span'); ?>
                </div>
            </th>
            <?php if ($showPayments): ?>
                <th style="width: 12.5%!important;">
                    <div class="th-title  <?= $sorting['paymentSum']; ?>">
                        <a class="th-title-name desc" href="<?= Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => $sorting['sortPaymentSum']])); ?>" data-sort="paymentSum">
                            Оплачено
                        </a>
                        <?= Html::beginTag('span', ['class' => 'th-title-btns',]) .
                        Html::a(Icon::get('count-arrow', ['class' => 'th-title-icon th-title-icon_reverse',]),
                            Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => 'paymentSum'])), ['class' => 'th-title-btn icon_asc button-clr',]) .
                        Html::a(Icon::get('count-arrow', ['class' => 'th-title-icon',]),
                            Url::to(ArrayHelper::merge([''], Yii::$app->request->queryParams, ['sort' => '-paymentSum'])), ['class' => 'th-title-btn icon_desc button-clr',]) .
                        Html::endTag('span'); ?>
                    </div>
                </th>
            <?php endif; ?>

            <?php if ($update): ?>
                <th style="width: 8%!important;"></th>
            <?php endif; ?>

        </tr>
    </thead>
    <tbody>
    <?php /** @var ProjectCustomer $item */
        $number = 0;
        foreach ($data as $item): ?>
        <tr id="tr-<?= $item->id; ?>">
            <?php if ($showCheckboxes): ?>
                <td>
                    <input type="checkbox" class="joint-operation-checkbox" name="ProjectCustomer[<?= $item->id ?>][checked]" value="1" data-sum="<?= $item->paymentSum ?>"/>
                </td>
            <?php endif; ?>
            <td style="width: 7%!important;">
                <?= ++$number; ?>
            </td>
            <td style="width: 30%!important;">
                <div class="customer-name">
                    <?php
                        $customerName = $item->customerName ?: ($item->customer ? $item->customer->getNameWithType() : '');
                        echo Html::a($customerName, Url::to(['contractor/view', 'id' => $item->customer_id, 'type' => Contractor::TYPE_CUSTOMER]));
                    ?>
                </div>
            </td>
            <td style="width: 30%!important;">
                <div class="info agreement">
                    <?= isset($item->agreement) ? $item->agreement->getTitle() : ''; ?>
                </div>
                <div class="edit hidden">
                    <?php
                        $agreementDropDownList = [];
                        $companyAgreementID = [];

                        if (!empty($item->customer_id)) {
                            $agreementArray = Agreement::find()
                                ->joinWith('agreementType')
                                ->andWhere(['company_id' => $company->id])
                                ->andWhere(['contractor_id' => $item->customer_id])
                                ->orderBy([
                                    'agreement_type.name' => SORT_ASC,
                                    'agreement.document_date' => SORT_DESC,
                                ])
                                ->all();

                            $agreementDropDownList += ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '];

                            /** @var $agreement \common\models\Agreement */
                            foreach ($agreementArray as $agreement) {
                                $agreementDropDownList[$agreement->id] = '№ ' . $agreement->document_number . ' от '
                                    . DateHelper::format(
                                        $agreement->document_date,
                                        DateHelper::FORMAT_USER_DATE,
                                        DateHelper::FORMAT_DATE
                                    );
                                $companyAgreementID[$agreement->getListItemValue()] = $agreement->document_number;
                            }
                        }
                    ?>
                    <?= $this->render('_basis_document', [
                        'model' => $item,
                        'contractor_id' => $item->customer_id,
                        'attribute' => 'agreement_id',
                        'pjaxId' => 'agreement-pjax-container-' . $item->customer_id,
                        'agreementDropDownList' => $agreementDropDownList,
                        'companyAgreementID' => $companyAgreementID,
                    ]); ?>
                </div>
            </td>
            <td style="width: 12.5%!important;">
                <div class="info amount">
                    <?= TextHelper::invoiceMoneyFormat($item->amount, 2); ?>
                </div>
                <div class="edit hidden">
                    <?= Html::activeTextInput($item, 'amount', [
                        'id' => 'projectcustomersearch-amount_' . $item->id,
                        'class' => 'form-control',
                        'type' => 'number',
                        'value' => $item->amount / 100
                    ]); ?>
                </div>
            </td>

            <?php if ($showPayments): ?>
                <td style="width: 12.5%!important;">
                    <div>
                        <?= TextHelper::invoiceMoneyFormat($item->paymentSum, 2); ?>
                    </div>
                </td>
            <?php endif; ?>

            <?php if ($update): ?>
                <td style="width: 8%!important;">
                <div class="info">
                    <?= Html::a(Icon::get('pencil'), '', [
                        'title' => 'Редактировать',
                        'class' => 'customer-edit link pl-1 pr-1',
                        'data' => [
                            'project-customer-id' => $item->id
                        ],
                    ]); ?>
                    <?php if ($canDelete): ?>
                        <?= Html::a(Icon::get('garbage'), '', [
                            'title' => 'Удалить',
                            'class' => 'customer-delete link pl-1 pr-1',
                            'data' => [
                                'project-customer-id' => $item->id,
                                'toggle' => 'modal',
                            ],
                        ]) ?>
                    <?php endif; ?>
                </div>
                <div class="edit hidden">
                    <?= Html::a(Icon::get('save'), '', [
                        'title' => 'Сохранить',
                        'class' => 'customer-save link pl-1 pr-1',
                        'data' => [
                            'project-customer-id' => $item->customer_id,
                            'project-projectcustomer-id' => $item->id
                        ],
                    ]); ?>
                    <?= Html::a(Icon::get('circle-close'), '', [
                        'title' => 'Отмена',
                        'class' => 'customer-cancel link pl-1 pr-1',
                        'data' => [
                            'project-customer-id' => $item->id,
                            'toggle' => 'modal',
                        ],
                    ]) ?>
                </div>
            </td>
            <?php endif; ?>

        </tr>
        <?php endforeach; ?>
        <?= $this->render('_basis_append_customer_row', [
            'model' => $model,
            'update' => $update,
            'customerDropDownList' => $customerDropDownList,
            'agreementDropDownList' => $agreementDropDownList,
            'companyAgreementID' => $companyAgreementID,
        ]); ?>
    </tbody>
</table>

<?php if ($update): ?>
<button type="button" class="button-regular button-hover-content-red mb-2 pl-3 pr-3 mr-auto add_project_customer">
    <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
    <span class="ml-1">Добавить заказчика</span>
</button>
<?php endif; ?>

<?php if ($showCheckboxes): ?>
    <?= SummarySelectWidget::widget([
        'buttons' => [
            $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
            ]) : null
        ]
    ]) ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php \yii\bootstrap4\Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранных заказчиков?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete-project-customer', 'project_id' => $model->id]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php \yii\bootstrap4\Modal::end(); ?>
<?php endif ?>

<?php

$modelId = $model->id ?: 0;

$this->registerJs(<<<JS
    $(document).on('click', '.add_project_customer', function() {
        if ($('.new-customer-row-template').hasClass('hidden')) {
            $('.new-customer-row-template').removeClass('hidden');
        } else if ($('#project-customer_id').val() > 0) {
            $('#new_customer').trigger('click');
        }
    });

    $(document).on('change', '#project-customer_id', function() {
        var customer = $('#project-customer_id').val();
        if (customer && customer !== 'add-modal-contractor') {
            $.pjax({
                'url': 'basis-document',
                'data': {'contractorId': customer},
                'container': '#agreement-pjax-container',
                'push': false,
                'scrollTo': false,
                'timeout': 10000
            })
        }
    });
JS
);

$this->registerJs(<<<JS

    function refreshBasisCustomerTableNumbers()
    {
        let number = 0;
        $('#basis-customer-table tbody').find('tr').each(function() {
            if ($(this).hasClass('new-customer-row-template'))
                return false;
            
            $(this).find('td:first-child').html(++number);
        });
    }

    $('#new_customer').click(function(e){
        e.preventDefault();
        
        if (!$('#project-customer_id').val())
            return;
        
        $.post('append-customer', {
            'project_id': '{$modelId}',
            'customer_id': $('#project-customer_id').val(),
            'agreement_id': $('#projectcustomer-agreement_id_' + $('#project-customer_id').val()).val(),
            'amount': $('#agreement_amount').val() * 100
        }, function(data){
            if (data) {
                $('#project-customer_id').val("").change();
                $('#projectcustomer-agreement_id_').val("").change();
                $('#agreement_amount').val("");
                
                $('.new-customer-row-template').before($(data));
                
                if (!e.isTrigger)
                    $('.new-customer-row-template').addClass('hidden');

                // new project
                if ($('#project-form').data('is_new_record')) {
                    $.pjax({
                        'url': 'basis-document',
                        'data': {'contractorId': null},
                        'container': '#agreement-pjax-container',
                        'push': false,
                        'scrollTo': false,
                        'timeout': 10000
                    })
                }
                
                refreshBasisCustomerTableNumbers();
            }
        })
    });

    $(document).on('click', '.customer-delete', function(e) {
        e.preventDefault();
        const value = $(this).data('project-customer-id');

        $(document).find('#confirm-customer-delete').data('project-customer-id', value);
        $('#customer-delete').modal('show');
        
    });

    $(document).on('click', '#confirm-customer-delete', function(e) {
        e.preventDefault();
        const value = $(document).find('#confirm-customer-delete').data('project-customer-id');

        $.post('project-customer-delete?project_id=' + {$modelId}, {id: value}, function(data){
            if ('true' === data.success) {
                $('#tr-' + value).remove();
                $('#customer-delete').modal('hide');
                
                refreshBasisCustomerTableNumbers();
            }
        });
        
    });

    $(document).on('click', '.customer-cancel', function(e) {
        e.preventDefault();
        var button = $(this);
        button.closest('tr').find('.edit').addClass('hidden');
        button.closest('tr').find('.info').removeClass('hidden');
    });
    
    $(document).on('click', '.customer-edit', function(e) {
        e.preventDefault();
        var button = $(this);
        button.closest('tr').find('.info').addClass('hidden');
        button.closest('tr').find('.edit').removeClass('hidden');
    });
    
    $(document).on('click', '.customer-save', function(e) {
        e.preventDefault();
        var button = $(this), 
            tr = button.closest('tr'),
            projectCustomerModelId = button.data('project-customer-id'),
            projectProjectCustomerModelId = button.data('project-projectcustomer-id');
        $.post('update-project-customer', {
            'id': projectProjectCustomerModelId,
            'project_id': '{$modelId}',
            'customer_id': $('#projectcustomer-customer_id').val(),
            'agreement_id': $('#projectcustomer-agreement_id_' + projectCustomerModelId).val(),
            'amount': $('#projectcustomersearch-amount_' + projectProjectCustomerModelId).val() * 100
        }, function(data){
            if (data) {
                tr.find('.agreement').html(data.agreement);
                tr.find('.amount').html(data.amount);
                
                button.closest('tr').find('.edit').addClass('hidden');
                button.closest('tr').find('.info').removeClass('hidden');
            }
        }, 'json')
    });
    
JS, View::POS_END
);