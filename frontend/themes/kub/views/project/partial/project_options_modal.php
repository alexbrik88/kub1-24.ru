<?php

use common\models\project\ProjectCustomer;
use common\models\ContractorAutoProject;
use common\components\date\DateHelper;
use common\models\project\Project;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/** @var Project $model */
/** @var ContractorAutoProject $modelOptions */

$company = Yii::$app->user->identity->company;

Modal::begin([
    'id' => 'project-options',
    'closeButton' => false,
    'toggleButton' => false,
]);

/** @var ProjectCustomer[] $disabledForProjectCustomers */
$disabledForProjectCustomers = ProjectCustomer::find()->alias('c')
    ->joinWith('project p')
    ->where(['not', ['c.project_id' => $model->id]])
    ->andWhere(['p.status' => Project::STATUS_INPROGRESS])
    ->andWhere(['c.customer_id' => ArrayHelper::getColumn($model->contractors, 'id')])
    ->all();

$title = 'Настройки по проекту';
?>

<h4 class="modal-title mb-4">
    <?= $title ?>
</h4>

<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'project-options-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
    'action' => ['update-options', 'id' => $model->id],
    'options' => [
        'data-pjax' => 0
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="row">
    <div class="col-12 mb-4">
        <table class="no-border">
            <tr>
                <td class="font-14 pr-3" style="padding-bottom: 6px">
                    С даты начала проекта:
                </td>
                <td class="font-14" style="padding-bottom: 6px">
                    <?= DateHelper::format($model->start_date, 'd.m.Y', 'Y-m-d') ?>
                </td>
            </tr>
            <tr>
                <td class="font-14 pr-3">
                    По дату окончания проекта:
                </td>
                <td class="font-14">
                    <?= DateHelper::format($model->end_date, 'd.m.Y', 'Y-m-d') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-2">
        <strong style="text-decoration: underline">По заказчикам проекта</strong>
    </div>
    <div class="col-12">
        <?= $form->field($modelOptions, 'exists_invoices', ['options' => ['class' => '']])->checkbox(['class' => '']) ?>
    </div>
    <div class="col-12">
        <?= $form->field($modelOptions, 'exists_flows', ['options' => ['class' => '']])->checkbox(['class' => '']) ?>
    </div>
    <div class="col-12">
        <?= $form->field($modelOptions, 'new_invoices', ['options' => ['class' => '']])->checkbox(['class' => '']) ?>
    </div>
    <div class="col-12">
        <?= $form->field($modelOptions, 'new_flows', ['options' => ['class' => '']])->checkbox(['class' => '']) ?>
    </div>
</div>

<?php if ($disabledForProjectCustomers): ?>
    <?php $_showed = []; ?>
    <div class="row mt-3">
        <div class="col-12 text_size_14">
            <?php foreach ($disabledForProjectCustomers as $c): ?>
                <?php if (in_array($c->customer_id, $_showed)) continue; else $_showed[] = $c->customer_id; ?>
                <?php $contractorName = ($c->customer)
                    ? $c->customer->getShortName()
                    : "contractorID={$c->customer_id}" ?>
    
                <div class="mb-1" style="color:#e30611">
                    Неприменимо к заказчику проекта <?= $contractorName ?>, т.к. у него более одного незавершенного проекта.
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<script>

    // $('#button-reset-pal-options').on('click', function(e)
    // {
    //     const form = $(this).closest('form');
    //     form.find('[type="submit"]').attr('disabled', true);
    //
    //     let l = Ladda.create(this);
    //     l.start();
    //
    //     $.ajax($(this).attr("data-url"), {
    //         method: "POST",
    //         data: $(this).serialize(),
    //         success: function (data)
    //         {
    //             Ladda.stopAll();
    //             if (data.success) {
    //                 location.href = location.href;
    //             } else {
    //                 window.showToastr("Ошибка обновления");
    //             }
    //         },
    //         error: function ()
    //         {
    //             Ladda.stopAll();
    //             window.showToastr("Ошибка обновления");
    //         }
    //     });
    //
    //     return false;
    // });
    //
    // $('#project-options-form').on('submit', function(e) {
    //     e.preventDefault();
    //     $.ajax($(this).attr("action"), {
    //         method: "POST",
    //         data: $(this).serialize(),
    //         success: function (data)
    //         {
    //             Ladda.stopAll();
    //             if (data.success) {
    //                 location.href = location.href;
    //             } else {
    //                 window.showToastr("Ошибка обновления");
    //             }
    //         },
    //         error: function ()
    //         {
    //             Ladda.stopAll();
    //             window.showToastr("Ошибка обновления");
    //         }
    //     });
    //
    //     return false;
    // });
    //
    // $('#project-options-form .table-collapse-btn').on('click', function()
    // {
    //     if (!$(this).hasClass('active')) {
    //         $(this).closest('form').find('.table-collapse-btn').not(this).each(function() {
    //             if ($(this).hasClass('active'))
    //                 $(this).click();
    //         });
    //     }
    // });
    //
    // $('#project-options-form .pal-options-select').on('change', function() {
    //     const selectWrapper = $(this).closest('.pal-options-select2');
    //     if (Number(this.value) === 0) {
    //         selectWrapper.addClass('black');
    //     } else {
    //         selectWrapper.removeClass('black');
    //     }
    // });
    //
    // $('#project-options-form .pal-revenue-rule-group-radio').on('change', function() {
    //     var rule = $('.pal-revenue-rule-group-radio:checked').val();
    //     $("#pal-revenue-rule-groups .collapse.show").collapse("hide");
    //     if (rule) {
    //         $("#project-options-form .revenue-rule-" + rule).collapse("show");
    //     }
    // });
    //
    // // amortization + ebit checkboxes
    // $('#analyticsarticleform-showamortization').on('change', function() {
    //     if ($(this).prop('checked')) {
    //         $('#analyticsarticleform-showebit').prop('disabled', false).uniform();
    //     } else {
    //         $('#analyticsarticleform-showebit').prop('checked', false).prop('disabled', true).uniform();
    //     }
    // });

</script>