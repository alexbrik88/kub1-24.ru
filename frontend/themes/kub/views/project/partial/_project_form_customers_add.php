<?php

use common\models\Contractor;
use common\models\project\Project;
use frontend\components\Icon;
use frontend\widgets\ContractorDropdown;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $hasCustomers bool */
/* @var $this yii\web\View */

$staticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' <span class="bold">Добавить покупателя</span>'];
?>

<tr class="project_customer_add <?= $hasCustomers ? 'hidden' : ''; ?>">
    <td>
    </td>
    <td class="p-1">
        <?= ContractorDropdown::widget([
            'name' => 'customer_id',
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $staticItems,
            'options' => [
                'id' => 'project_customer_select',
                'placeholder' => '',
                'data' => [
                    'items-url' => Url::to(['/project/add-customer', 'cid' => '_cid_']),
                ],
            ],
        ]) ?>
    </td>
    <td colspan="2">
    </td>
</tr>