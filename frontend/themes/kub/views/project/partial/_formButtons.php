<?php

use common\models\project\Project;
use yii\helpers\Html;
use yii\helpers\Url;

$returnUrl = ['project/index'];

$tabActive = Yii::$app->request->get('tab');

/* @var $model Project */
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <?php if (!$model->isNewRecord): ?>
            <div class="column ml-auto">
                <?= Html::a('Удалить', '#delete', [
                    'class' => 'button-width button-clr button-regular button-hover-grey pl-4 pr-4',
                    'data-toggle' => 'modal'
                ]); ?>
            </div>
        <?php endif; ?>
        <div class="column">
            <?= Html::a('Отменить', $model->isNewRecord
                ? $returnUrl
                : ['project/view', 'id' => $model->id, 'tab' => $tabActive],
                [
                    'class' => 'button-width button-clr button-regular button-hover-grey pl-4 pr-4' . ($model->isNewRecord ? '' : ' button-cancel-edit'),
                ]); ?>
        </div>
    </div>
</div>

<div id="delete" class="confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить проект?</h4>
            <div class="text-center">
                <?= \common\components\helpers\Html::a('Да', ['delete-project', 'id' => $model->id], [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::button('Нет', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        </div>
    </div>
</div>