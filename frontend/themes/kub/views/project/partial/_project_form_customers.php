<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectCustomerSearch;
use frontend\modules\documents\widgets\SummarySelectWidget;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/** @var $model ProjectForm */
/** @var $form ActiveForm */
/** @var $this yii\web\View */

$projectCustomersArray = $model->getProjectCustomers();
?>

<table id="project_form_customers_table" class="table table-style table-count-list border-top border-bottom border-left">
    <thead>
        <tr>
            <th style="width: 5%;">
            </th>
            <th style="width: 45%;" >
                <div class="th-title">Заказчик</div>
            </th>
            <th style="width: 35%;">
                <div class="th-title">Основание</div>
            </th>
            <th style="width: 15%;">
                <div class="th-title">Сумма по договору</div>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($projectCustomersArray as $key => $customer): ?>
            <?= $this->render('_project_form_customers_row', [
                'key' => $key,
                'model' => $model,
                'customer' => $customer,
            ]) ?>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <?= $this->render('_project_form_customers_add', [
            'hasCustomers' => !empty($projectCustomersArray),
        ]); ?>
    </tfoot>
</table>

<?= $form->field($model, 'customers')->hiddenInput()->label(false); ?>

<div class="mt-2 mb-4">
    <?= Html::button(Icon::get('add-icon').'<span class="ml-1">Добавить заказчика</span>', [
        'class' => 'button-regular button-hover-content-red',
        'onclick' => "$('#project_form_customers_table .project_customer_add').toggleClass('hidden', false);"
    ]) ?>
</div>

<?php $this->registerJs(<<<JS
    function checkProjectCustomersKeys() {
        $('#project_form_customers_table tbody tr.project_customer_row').each(function(i) {
            var item = this;
            $(item).find('[name^="ProjectCustomerForm"]').each(function () {
                this.name = this.name.replace(/ProjectCustomerForm\[\d+\]/, 'ProjectCustomerForm[' + i + ']');
            });
        });
        $('#project-form').yiiActiveForm('validate');
    }
    function removeProjectCustomer(row) {
        row.remove();
        if ($('#project_form_customers_table .project_customer_row').length == 0) {
            $('#project_form_customers_table .project_customer_add').toggleClass('hidden', false);
        }
        checkProjectCustomersKeys();
    }
    $(document).on('click', '.project_customer_delete_btn', function(e) {
        $(this).closest('.modal').modal('hide');
        setTimeout(removeProjectCustomer, 600, $(this).closest('.project_customer_row'));
    });
    $(document).on('change', '#project_customer_select', function(e) {
        var value = this.value;
        if (value == '' || value == 'add-modal-contractor') return;
        value = parseInt(value);
        var url = $(this).data("items-url").replace("_cid_", $(this).val());
        $.get(url, function (data) {
            $('#project_form_customers_table tbody').append(data);
            $('#project_form_customers_table .project_customer_add').toggleClass('hidden', true);
            checkProjectCustomersKeys();
        });
        $(this).val('').trigger('change');
    });
JS
) ?>
