<?php

use common\components\helpers\Html;
use common\models\Contractor;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\web\JsExpression;

/** @var ProjectCustomer $model */
/** @var bool $update */
/** @var array $customerDropDownList */
/** @var array $agreementDropDownList */
/** @var array $companyAgreementID */

?>

<tr class="new-customer-row-template hidden">
    <td style="width: 7%!important;"></td>
    <td style="width: 30%!important; max-width: 290px;">
        <?= ContractorDropdown::widget([
            'company' => Yii::$app->user->identity->company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'model' => (new Project()),
            'attribute' => 'customer_id',
//            'data' => $customerDropDownList,
            'staticData' => [
                "add-modal-contractor" => Icon::PLUS . ' Добавить заказчика',
            ],
            'options' => [
                'class' => 'form-control',
            ],
        ]); ?>
    </td>
    <td style="width: 30%!important; max-width: 290px;">
        <div id="agreement_drop_down">
            <?= $this->render('_basis_document', [
                'model' => (new Project()),
                'agreementDropDownList' => [],
                'companyAgreementID' => [],
            ]); ?>
        </div>
    </td>
    <td style="width: 12.5%!important;">
        <?= Html::textInput('agreement_amount', '', ['class' => 'form-control', 'id' => 'agreement_amount']); ?>
    </td>
    <?php if (true || !$model->isNewRecord): ?>
        <?php if ($update): ?>
        <td style="width: 8%!important;">
            <?= Html::a(Icon::get('save'), '', [
                'id' => 'new_customer',
                'title' => 'Сохранить',
                'class' => 'link pl-1 pr-1',
            ]); ?>
        </td>
        <?php endif; ?>
    <?php endif; ?>
</tr>