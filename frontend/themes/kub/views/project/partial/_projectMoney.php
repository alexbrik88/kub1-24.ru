<?php

/** NOT USED */

/*
use common\components\TextHelper;
use common\models\project\Project;
use common\models\project\ProjectEstimateSearch;
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

$estimate_income = (new ProjectEstimateSearch(['project_id' => $model->id]))->getQuery()->sum('income_estimate_sum');
$estimate_expense = (new ProjectEstimateSearch(['project_id' => $model->id]))->getQuery()->sum('expense_estimate_sum');

?>

<div class="project-form">
    <div class="row">
        <div class="form-group col-4 mb-3 pb-2">
            <div class="form-filter">
                <label class="label weight-700" for="cause">Доход (план)</label>
            </div>
            <?= $form->field($model, 'money_amount')->textInput(); ?>
        </div>
        <div class="form-group col-4 mb-3 pb-2">
            <div class="form-filter">
                <label class="label weight-700" for="cause">Доход по смете (план)</label>
            </div>
            <?= Html::textInput('estimate_income', TextHelper::invoiceMoneyFormat($estimate_income, 2), ['class' => 'form-control', 'disabled' => true]); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-4 mb-3 pb-2">
            <div class="form-filter">
                <label class="label weight-700" for="cause">Расход (план)</label>
            </div>
            <?= $form->field($model, 'money_expense')->textInput(); ?>
        </div>
        <div class="form-group col-4 mb-3 pb-2">
            <div class="form-filter">
                <label class="label weight-700" for="cause">Расход по смете (план)</label>
            </div>
            <?= Html::textInput('estimate_expense', TextHelper::invoiceMoneyFormat($estimate_expense, 2), ['class' => 'form-control', 'disabled' => true]); ?>
        </div>
    </div>
</div>