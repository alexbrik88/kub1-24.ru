<?php

use common\models\project\Project;
use frontend\components\Icon;
use kartik\widgets\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $key integer */
/* @var $model ProjectForm */
/* @var $customer ProjectCustomerForm */
/* @var $this yii\web\View */

?>

<tr class="project_customer_row">
    <td>
        <?php Modal::begin([
            'id' => 'modal_'.uniqid(),
            'title' => 'Вы уверены, что хотите удалить заказчика?',
            'closeButton' => false,
            'toggleButton' => [
                'label' => Icon::get('circle-close', ['class' => 'table-count-icon']),
                'tag' => 'span',
            ],
        ]) ?>
        <div class="text-center">
            <?= Html::tag('span', 'Да', [
                'class' => 'button-regular button-hover-transparent button-width-medium mr-2 project_customer_delete_btn',
                'style' => 'color: #001424!important;',
            ]) ?>
            <?= Html::tag('span', 'Нет', [
                'class' => 'button-regular button-hover-transparent button-width-medium ml-2',
                'style' => 'color: #001424!important;',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <?php Modal::end() ?>
    </td>
    <td>
        <?= $customer->getTitle() ?>
        <?= Html::activeHiddenInput($customer, sprintf('[%s]id', $key), [
            'class' => 'form-control',
        ]) ?>
        <?= Html::activeHiddenInput($customer, sprintf('[%s]customer_id', $key), [
            'class' => 'form-control',
        ]) ?>
    </td>
    <td class="p-1">
        <?= Html::activeDropDownList($customer, sprintf('[%s]agreement_id', $key), $customer->getAgreementData(), [
            'class' => 'form-control',
        ]) ?>
    </td>
    <td class="p-1">
        <?= Html::activeTextInput($customer, sprintf('[%s]amount', $key), [
            'class' => 'form-control',
        ]) ?>
    </td>
</tr>