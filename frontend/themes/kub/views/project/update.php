<?php

use common\models\project\Project;

/* @var $this yii\web\View */
/* @var $model Project */

$this->title = 'Изменить проект';

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
