<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectEstimateSearch;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\themes\kub\assets\CashModalAsset;
use frontend\themes\kub\assets\ProjectAsset;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Company;
use common\models\employee\Employee;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;

CashModalAsset::register($this);

/* @var $this yii\web\View */
/* @var $model Project */
/* @var $searchModel CashFlowsProjectSearch */
/* @var $dataProvider ArrayDataProvider */
/* @var $company Company */
/* @var $user Employee */
/* @var $activeTab integer */
/* @var $ioType integer */
/* @var $tab string */
/* @var $modelOptions \common\models\ContractorAutoProject */

$this->title = $model->name;
$this->context->layoutWrapperCssClass = 'project';

$tabLink = Url::toRoute([
    '/project/view',
    'id' => $model->id,
]);

$tabFile = ArrayHelper::remove($tabData, 'tabFile');

$user = Yii::$app->user->identity;

/** @var ProjectCustomer[] $customers */
if ($customers = $model->getCustomers()->all()) {
    $customer = array_shift($customers);
} else {
    $customer = null;
}

$modelId = $model->id;

$estimate_income = (new ProjectEstimateSearch(['project_id' => $model->id]))->getQuery()->sum('income_estimate_sum');
$agreement_income = array_sum(ArrayHelper::getColumn($model->customers, 'amount'));

$logArray = Log::find()
    ->andWhere(['model_name' => $model->getClassName()])
    ->andWhere(['model_id' => $model->id])
    ->orderBy(['id' => SORT_ASC])
    ->all();

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-black',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'position' => 'right',
        'trigger' => 'click',
        'interactive' => true,
        'contentAsHTML' => true,
        'repositionOnScroll' => true,
    ],
]);

ProjectAsset::register($this);
?>
    <a class="link mb-2" href="<?= Url::to(['/project/index']) ?>">
        Назад к списку
    </a>

    <!-- KUB -->
    <div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <div class="pr-2">
                        <div class="row align-items-center justify-content-between mb-3">
                            <h4 class="column mb-2" style="max-width: 550px;"><?= Html::encode($this->title); ?></h4>
                            <div class="column" style="margin-bottom: auto;">
                                <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                        type="button"
                                        data-toggle="modal"
                                        title="Последние действия"
                                        href="#basic">
                                    <svg class="svg-icon svg-icon_size_">
                                        <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                    </svg>
                                </button>
                                <a href="<?= Url::to(['/project/update', 'id' => $model->id]) ?>"
                                   class="button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                                   title="Изменить">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Дата начала</div>
                                    <?= DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Дата завершения</div>
                                    <?= DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Статус</div>
                                    <div id="projectStatuses">
                                        <?= kartik\select2\Select2::widget([
                                            'model' => $model,
                                            'attribute' => 'status',
                                            'options' => [
                                                'id' => 'project_status',
                                            ],
                                            'data' => Project::$statuses,
                                            'pluginOptions' => [
                                                'width' => '150px',
                                            ],
                                            'hideSearch' => true,
                                            'pluginLoading' => false,
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Номер проекта</div>
                                    <?= htmlspecialchars($model->number) . htmlspecialchars($model->addition_number) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Заказчик</div>
                                    <?php if ($customer): ?>
                                        <?= Html::a($customer->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $customer->customer->id, 'type' => Contractor::TYPE_CUSTOMER]), ['class' => 'link']); ?>
                                    <?php endif; ?>
                                    <?php if ($customers): ?>
                                        <div style="margin-bottom: -18px">
                                            <div class="tooltip-black font-14 link pointer" data-tooltip-content="#tooltip_project_other_customers">
                                                + еще <?= count($customers) ?>
                                            </div>
                                            <div style="display:none">
                                                <div id="tooltip_project_other_customers">
                                                    <?php foreach ($customers as $c): ?>
                                                        <div class="pl-2 pr-2 pb-1 pt-1">
                                                            <?= Html::a($c->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $c->customer->id, 'type' => Contractor::TYPE_CUSTOMER]), ['class' => 'link']); ?>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Направление</div>
                                    <?= ($model->industry) ? $model->industry->name : '—'; ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Отвественный</div>
                                    <?php
                                    $employee = \common\models\EmployeeCompany::find()
                                        ->where(['employee_id' => $model->responsible, 'company_id' => $company->id])
                                        ->one();
                                    ?>
                                    <?= ($employee) ? ($employee->lastname . ' '
                                        . $employee->firstname_initial . '.'
                                        . $employee->patronymic_initial . '.') : '--'; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Сумма по договору</div>
                                    <?php if ($agreement_income > 0) {
                                        echo '<br/>' . Html::tag('span', TextHelper::invoiceMoneyFormat($agreement_income, 2) . ' ₽', ['title' => 'Сумма по договорам']);
                                    } ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Сумма по смете</div>
                                    <?php if ($estimate_income > 0) {
                                        echo '<br/>' . Html::tag('span', TextHelper::invoiceMoneyFormat($estimate_income, 2) . ' ₽', ['title' => 'Сумма по сметам с НДС']);
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 column pl-0">
                    <div class="pb-2 mb-1">
                        <div class="dropdown">
                            <?= Html::a(Icon::get('add-icon') . '<span>Добавить</span>
                                <svg class="svg-icon svg-icon_shevron svg-icon_absolute">
                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                </svg>', '#', [
                                'class' => 'button-width button-clr button-regular button-regular_red w-100',
                                'data-toggle' => 'dropdown',
                                'aria-expanded' => 'false'
                            ]); ?>

                            <?= Dropdown::widget([
                                'options' => [
                                    'class' => 'dropdown-menu form-filter-list list-clr w-100 text-center',
                                    'style' => 'z-index:1003'
                                ],
                                'items' => [
                                    [
                                        'label' => 'Операцию по деньгам',
                                        'url' => 'javascript:void(0)',
                                        'linkOptions' => [
                                            'class' => 'ajax-modal-btn dropdown-item',
                                            'data-title' => 'Добавить операцию',
                                            'data-url' => Url::to([
                                                '/cash/multi-wallet/create-bank-flow',
                                                'type' => 1,
                                                'projectId' => $model->id,
                                                'redirectUrl' => Url::to(['view', 'id' => $model->id, 'tab' => 'money'])
                                            ])
                                        ]
                                    ],
                                    [
                                        'label' => 'Счет покупателю',
                                        'url' => Url::to(['/documents/invoice/create',
                                            'type' => Documents::IO_TYPE_OUT,
                                            //'contractorId' => $model->customer_id,
                                            //'fromAgreement' => $model->document,
                                            'project_id' => $model->id,
                                        ]),
                                    ],
                                    [
                                        'label' => 'Счет поставщику',
                                        'url' => Url::to(['/documents/invoice/create',
                                            'type' => Documents::IO_TYPE_IN,
                                            'project_id' => $model->id,
                                        ]),
                                    ],
                                    [
                                        'label' => 'Задачу',
                                        'linkOptions' => [
                                            'id' => 'projectTaskLink',
                                            'data-toggle' => 'modal',
                                            'data-project-id' => $model->id,
                                        ],
                                        'url' => Url::to(['/documents/invoice/create',
                                            'type' => Documents::IO_TYPE_IN,
                                            'project_id' => $model->id,
                                        ]),
                                    ],
                                    [
                                        'label' => 'Смету',
                                        'url' => Url::to(['estimate', 'id' => $model->id, 'type' => 'create']),
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="pb-2 mb-1">
                        <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                    </div>
                    <div class="pb-2 mb-1">
                        <button class="button-regular button-hover-content-red button-clr w-100" type="button" data-toggle="modal" href="#project-options" title="Настройки по проекту">
                            Настройки
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs-row">
        <?= Nav::widget([
            'id' => 'contractor-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Задачи',
                    'url' => $tabLink . '&tab=tasks',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'tasks' ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Счета',
                    'url' => $tabLink . '&tab=invoice&type=' . Contractor::TYPE_CUSTOMER,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . (($tab == 'invoice' || $tab == 'invoice-incoming' || $tab == 'invoice-outgoing') ? ' active' : ''),
                    ],
                ],
                [
                    'label' => 'Деньги',
                    'url' => $tabLink . '&tab=money',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'money' ? ' active' : '')
                    ],
                ],
                [
                    'label' => ' Команда',
                    'url' => $tabLink . '&tab=commands',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'commands' ? ' active' : '')
                    ]
                ],
                [
                    'label' => 'Аналитика',
                    'url' => $tabLink . '&tab=analytics',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'analytics' ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Заказчики',
                    'url' => $tabLink . '&tab=info',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'info' ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Описание',
                    'url' => $tabLink . '&tab=description',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'description' ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Смета',
                    'url' => $tabLink . '&tab=estimate',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == 'estimate' ? ' active' : '')
                    ],
                ],
            ],
        ]); ?>
    </div>
<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab active">
            <?= $this->render("{$tabFile}", $tabData); ?>
        </div>
    </div>
<?php endif ?>

<?= $this->render('view/payment/_modal.php', [
    'model' => $model,
]);?>

<?= $this->render('partial/project_options_modal', [
    'model' => $model,
    'modelOptions' => $modelOptions
]) ?>

<?= $this->render('partial/last_actions_modal', [
    'model' => $model,
    'logArray' => $logArray
]) ?>

<?php

$this->registerJs(<<<JS
    $(document).on('change', '#project_status', function (e) {
        e.preventDefault();
        $.post('change-status', {id:$modelId, status:$(this).val()}, function(data){})
    });
    
    $(document).on('click', '.change_status', function(e) {
        e.preventDefault();
        $('.statuses').removeClass('hidden');
    });
    
    ///////////////////
    MultiWallet.init();
    PlanModal.init();
    /////////////////
    
JS, View::POS_READY);