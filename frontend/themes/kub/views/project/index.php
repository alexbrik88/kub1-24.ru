<?php
/* @var $this yii\web\View */

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\project\Project;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\themes\kub\helpers\Icon;
use common\models\project\ProjectSearch;
use frontend\themes\kub\widgets\SummarySelectProjectWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\widgets\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use yii\web\View;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel ProjectSearch */
/* @var $palModel ProfitAndLossSearchModel */
/* @var $analyticsModel AnalyticsSimpleSearch */
/* @var $userConfig Config */

$this->title = 'Проекты';

$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;
$company = $user->company;

$period = StatisticPeriod::getSessionName();

$exists = Project::find()
    ->andWhere(['company_id' => $company->id])
    ->exists();

if ($exists) {
    $emptyMessage = "В выбранном периоде «{$period}», у вас нет проектов. Измените период, чтобы увидеть имеющиеся проекты.";
} else {
    $emptyMessage = 'Вы еще не добавили ни одного проекта. '
        . Html::a('Создать проект', Url::to(['create']), ['class' => 'link'])
        . '.';
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-project-clients',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'position' => 'right',
        'trigger' => 'click',
        'interactive' => true,
        'contentAsHTML' => true,
        'repositionOnScroll' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentCloning' => true,
    ],
]);

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_document');

$showHelpPanel = $userConfig->report_odds_help ?? false; // todo: rename property
$showChartPanel = $userConfig->report_odds_chart ?? false; // todo: rename property
?>

<div class="stop-zone-for-fixed-elems project-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <h4></h4>
        <span class="button-regular button-regular_red mb-2 pl-3 pr-3 ml-auto add-modal-new-project">
            <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
            <span class="ml-1">Добавить</span>
        </span>
    </div>

    <!-- HEADER -->
    <div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom:12px;">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="column mr-auto">
                    <h4 class="mb-2"><?= $this->title ?></h4>
                </div>
                    <div class="column pl-1 pr-2">
                        <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                            [
                                'id' => 'btnChartCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#chartCollapse',
                                'data-tooltip-content' => '#tooltip_chart_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>
                    <div class="column pl-1 pr-2">
                        <?= Html::button(Icon::get('book'),
                            [
                                'id' => 'btnHelpCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#helpCollapse',
                                'data-tooltip-content' => '#tooltip_help_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>

                    <div class="column pl-1 pr-0 select2-wrapper" style="margin-top:-9px">
                        <?= frontend\widgets\RangeButtonWidget::widget([
                            'byMonths' => true
                        ]); ?>
                    </div>
            </div>
        </div>

        <div style="display: none">
            <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
            <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
        </div>

    </div>

    <div class="jsSaveStateCollapse wrap p-0 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_odds_chart">
        <?= $this->render('chart/_index_charts', [
            'company' => $company,
            'palModel' => $palModel,
            'analyticsModel' => $analyticsModel,
            'onPage' => 'project',
            'currentYear' => $currentYear ?? date('Y'),
        ]) ?>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_odds_help">
        <div class="pt-4 pb-3">
            <span class="text-justify">Добавляйте проекты, что бы вести учет прихода и расхода денег в разрезе каждого
                проекта. Это&nbsp;позволит знать рентабельность каждого проекта, сравнивать проекты между собой
                и&nbsp;понимать какие проекты приносят прибыль, а какие съедают ваши деньги.
            </span>
        </div>
    </div>
    <!-- END OF HEADER -->

    <div class="table-settings row row_indents_s" style="margin-top: -3px;">

        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'project_number',
                    ],
                    [
                        'attribute' => 'project_end_date',
                    ],
                    [
                        'attribute' => 'project_direction',
                    ],
                    [
                        'attribute' => 'project_before_date',
                    ],
                    [
                        'attribute' => 'project_clients',
                    ],

                    [
                        'attribute' => 'project_odds',
                    ],
                    [
                        'attribute' => 'project_opiu',
                    ],
                    [
                        'attribute' => 'project_responsible',
                    ],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'searchProject', [
                        'type' => 'search',
                        'placeholder' => 'Номер проекта, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($model) {

                    $income = round($model['income_sum'] / 100, 2);
                    $expense = round($model['expense_sum'] / 100, 2);
                    $profit = round($model['pal_net_income_loss'] / 100, 2);
                    $revenue = round($model['pal_revenue'] / 100, 2);

                    return Html::checkbox('id[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $model['id'],
                        'data' => [
                            'copy-url' => Url::to(['create', 'copy' => $model['id']]),
                            'income' => $income,
                            'expense' => $expense,
                            'profit' => $profit,
                            'revenue' => $revenue
                        ]
                    ]);
                },
            ],
            [
                'attribute' => 'number',
                'label' => '№ проекта',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'sorting col_project_number' . ($userConfig->project_number ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'document_number col_project_number' . ($userConfig->project_number ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $number =  $model['number'] . $model['addition_number'];
                    return Html::a($number, ['/project/view', 'id' => $model['id']], ['class' => 'link']);
                },
            ],
            [
                'attribute' => 'start_date',
                'label' => 'Дата начала',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'sorting',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'value' => function ($model) {
                    return DateHelper::format($model['start_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'end_date',
                'label' => 'Дата окончания',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'col_project_end_date sorting' . ($userConfig->project_end_date ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_end_date' . ($userConfig->project_end_date ? '' : ' hidden'),
                ],
                'value' => function ($model) {
                    return DateHelper::format($model['end_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'name',
                'label' => 'Название проекта',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'sorting',
                    //'width' => '180px',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model['name'], ['/project/view', 'id' => $model['id']], ['class' => 'link']);
                },
            ],
            [
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'sorting col_project_direction' . ($userConfig->project_direction ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_direction' . ($userConfig->project_direction ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getIndustryFilter(),
                's2width' => '250px',
                'format' => 'html',
                'value' => function ($model) {

                    if ($industry = \common\models\company\CompanyIndustry::findOne($model['industry_id']))
                        return $industry->name;

                    return '';
                },
            ],            
            [
                'attribute' => 'before_date',
                'label' => 'Дней до окончания',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'sorting col_project_before_date sorting' . ($userConfig->project_before_date ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_before_date' . ($userConfig->project_before_date ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model['status'] == Project::STATUS_INPROGRESS) ?
                        Html::tag('span', $model['before_date'], [
                            'style' => ($model['before_date'] < 0) ? 'color: red' : ''
                        ]) : 0;
                },
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    //'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => 'nowrap',
                ],
                'filter' => $searchModel->getStatusFilter(),
                's2width' => '150px',
                'value' => function ($model) {
                    return $model['status'] === Project::STATUS_INPROGRESS
                        ? Project::STRING_INPROGRESS
                        : Project::STRING_CLOSED;
                },
            ],
            [
                'attribute' => 'clients',
                'label' => 'Заказчик',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'col_project_clients' . ($userConfig->project_clients ? '' : ' hidden'),
                    'width' => '200px',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell col_project_clients' . ($userConfig->project_clients ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getClientsFilter(),
                's2width' => '250px',
                'format' => 'raw',
                'value' => function ($model) {

                    $result = '';
                    $clients = $model['clients'];

                    if (empty($clients)) {
                        return $result;
                    }

                    $client = array_shift($clients);

                    $result .= Html::a(htmlspecialchars($client['name']), Url::to(['/contractor/view', 'id' => $client['id'], 'type' => $client['type']]), ['class' => 'link']) . '<br>';

                    if ($clients) {

                        $clientsTooltip = '';
                        foreach ($clients as $c) {
                            $link = Html::a(htmlspecialchars($c['name']), Url::to(['/contractor/view', 'id' => $c['id'], 'type' => $c['type']]), ['class' => 'link']) . '<br>';
                            $clientsTooltip .= Html::tag('div', $link, ['class' => 'pl-2 pr-2 pb-1 pt-1']);

                        }

                        $result .= Html::tag('div', '+ еще ' . count($clients), [
                            'class' => 'tooltip-project-clients font-14 link pointer',
                            'data-tooltip-content' => ".tooltip_project_other_clients[data-project={$model['id']}]"
                        ]);

                        $result .= Html::tag('div', Html::tag('div', $clientsTooltip, [
                            'class' => 'tooltip_project_other_clients',
                            'data-project' => $model['id']
                        ]), ['style' => 'display:none']);
                    }

                    return $result;
                },
            ],
            [
                'label' => Html::tag('span', '<span class="collapse-toggle-icon mr-2"></span><span>ОДДС</span>', [
                    'class' => 'togle-project-columns-button',
                    'data-attribute' => 'project_odds_collapse',
                    'data-value' => '0',
                    'data-toggle-class' => 'el_hide',
                    'data-true-target' => '.project_odds_sm',
                    'data-false-target' => '.project_odds_lg',
                ]),
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'project_odds_sm col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . ($userConfig->project_odds_collapse ? '' : ' el_hide'),
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'project_odds_sm',
                ],
            ],
            [
                'attribute' => 'income_sum',
                'label' => 'Приход',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_odds_sm sorting col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . ($userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_odds_sm nowrap text-right col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . ($userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['income_sum'], 2);
                },
            ],
            [
                'label' => Html::tag('span', '<span class="collapse-toggle-icon mr-2 disclosed"></span><span>ОДДС</span>', [
                    'class' => 'togle-project-columns-button',
                    'data-attribute' => 'project_odds_collapse',
                    'data-value' => '1',
                    'data-toggle-class' => 'el_hide',
                    'data-true-target' => '.project_odds_lg',
                    'data-false-target' => '.project_odds_sm',
                ]),
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'project_odds_lg col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                    'rowspan' => 1,
                    'colspan' => 3,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'project_odds_lg col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
            ],
            [
                'attribute' => 'income_sum',
                'label' => 'Приход',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_odds_lg sorting col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_odds_lg nowrap text-right col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['income_sum'], 2);
                },
            ],
            [
                'attribute' => 'expense_sum',
                'label' => 'Расход',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_odds_lg sorting col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_odds_lg nowrap text-right col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['expense_sum'], 2);
                },
            ],
            [
                'attribute' => 'result_sum',
                'label' => 'Остаток',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_odds_lg sorting col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_odds_lg nowrap text-right col_project_odds' . ($userConfig->project_odds ? '' : ' hidden') . (!$userConfig->project_odds_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['result_sum'], 2);
                },
            ],
            [
                'label' => Html::tag('span', '<span class="collapse-toggle-icon mr-2"></span><span>ОПиУ</span>', [
                    'class' => 'togle-project-columns-button',
                    'data-attribute' => 'project_opiu_collapse',
                    'data-value' => '0',
                    'data-toggle-class' => 'el_hide',
                    'data-true-target' => '.project_opiu_sm',
                    'data-false-target' => '.project_opiu_lg',
                ]),
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'project_opiu_sm col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                    'rowspan' => 1,
                    'colspan' => 2,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_sm' . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
            ],
            [
                'attribute' => 'pal_revenue',
                'label' => 'Выручка',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_opiu_sm sorting col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_sm nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_revenue'], 2);
                },
            ],
            [
                'attribute' => 'pal_profitability',
                'label' => 'Рентабельность',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'width' => '120px',
                    'class' => 'project_opiu_sm sorting col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_sm nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . ($userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'value' => function ($model) {
                    return TextHelper::numberFormat($model['pal_profitability'], 2) . ' %';
                },
            ],
            [
                'label' => Html::tag('span', '<span class="collapse-toggle-icon mr-2 disclosed"></span><span>ОПиУ</span>', [
                    'class' => 'togle-project-columns-button',
                    'data-attribute' => 'project_opiu_collapse',
                    'data-value' => '1',
                    'data-toggle-class' => 'el_hide',
                    'data-true-target' => '.project_opiu_lg',
                    'data-false-target' => '.project_opiu_sm',
                ]),
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'project_opiu_lg col_project_opiu sorting' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                    'rowspan' => 1,
                    'colspan' => 4,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_lg text-right' . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
            ],
            [
                'attribute' => 'pal_revenue',
                'label' => 'Выручка',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_opiu_lg col_project_opiu sorting' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_lg nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_revenue'], 2);
                },
            ],
            [
                'attribute' => 'pal_costs',
                'label' => 'Затраты',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_opiu_lg col_project_opiu sorting' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_lg nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_costs'], 2);
                },
            ],
            [
                'attribute' => 'pal_net_income_loss',
                'label' => 'Прибыль',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'class' => 'project_opiu_lg col_project_opiu sorting' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_lg nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_net_income_loss'], 2);
                },
            ],
            [
                'attribute' => 'pal_profitability',
                'label' => 'Рентабельность',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'width' => '120px',
                    'class' => 'project_opiu_lg col_project_opiu sorting' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'contentOptions' => [
                    'class' => 'project_opiu_lg nowrap text-right col_project_opiu' . ($userConfig->project_opiu ? '' : ' hidden') . (!$userConfig->project_opiu_collapse ? '' : ' el_hide'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::numberFormat($model['pal_profitability'], 2) . ' %';
                },
            ],
            [
                'attribute' => 'responsible',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    'class' => 'col_project_responsible' . ($userConfig->project_responsible ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_responsible' . ($userConfig->project_responsible ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getResponsibleEmployeeFilter(),
                'selectPluginOptions' => [
                    'width' => '150px',
                ],
                'value' => function ($model) use ($userConfig) {
                    /* @var $employee EmployeeCompany */
                    $employee = EmployeeCompany::find()
                        ->joinWith('employee')
                        ->andWhere([
                            EmployeeCompany::tableName() . '.company_id' => $userConfig->employee->company_id,
                            EmployeeCompany::tableName() . '.employee_id' => $model['responsible'],
                            EmployeeCompany::tableName() . '.is_working' => Employee::STATUS_IS_WORKING,
                            Employee::tableName() . '.is_active' => Employee::ACTIVE,
                            Employee::tableName() . '.is_deleted' => Employee::NOT_DELETED,
                        ])->one();
                    if ($employee) {
                        return $employee->getShortFio();
                    }

                    return '';
                },
            ],
        ],
    ]); ?>
</div>

<?= SummarySelectProjectWidget::widget([
    'buttons' => [
        Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'copied']) . '<span>Копировать</span>', '#copy', [
            'class' => 'one-line-button button-clr button-regular button-width button-hover-transparent hidden',
            'data-toggle' => 'modal',
        ])),
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-width button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Дата окончания',
                        'url' => '#change_end_date_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Ответственный',
                        'url' => '#change_responsible_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],

                ],
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align-sm']),
        Html::tag('div', Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>', '#delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ])),
    ],
]); ?>

<div id="delete" class="confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные проекты?</h4>
            <div class="text-center">
                <?= Html::button('Да', [
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                    'data-url' => Url::to(['many-delete-project']),
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::button('Нет', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div id="copy" class="confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите скопировать проект?</h4>
            <div class="text-center">
                <?= Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['copy', 'id' => '']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'change_responsible_modal',
]); ?>
    <h4 class="modal-title">Изменить ответственного</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Ответственный
                </label>
                <?= Select2::widget([
                    'id' => 'contractor-responsible_employee',
                    'name' => 'responsible',
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                        'class' => 'modal-document-date',
                    ],
                    'data' => ArrayHelper::map($company->getEmployeeCompanies()
                        ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                        ->orderBy([
                            'lastname' => SORT_ASC,
                            'firstname' => SORT_ASC,
                            'patronymic' => SORT_ASC,
                        ])->all(), 'employee_id', 'fio'),
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-responsible button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/project/change-responsible']),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'change_status_modal',
]); ?>
    <h4 class="modal-title">Изменить статус</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">

        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-status" class="label">
                    Статус
                </label>
                <?= Select2::widget([
                    'name' => 'status',
                    'options' => [
                        'id' => 'contractor-status',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                    ],
                    'data' => [
                        Project::STATUS_INPROGRESS => 'В работе',
                        Project::STATUS_CLOSED => 'Завершен',
                    ],
                ]); ?>
            </div>
        </div>

    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/project/change-status']),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'change_end_date_modal',
]); ?>
    <h4 class="modal-title">Изменить дату окончания</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">

        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="project-end-date" class="label">
                    Дата окончания
                </label>
                <div class="date-picker-wrap">
                    <?= Html::input('text', 'end_date', null, [
                        'id' => 'project-end-date',
                        'class' => 'form-control form-control_small date-picker',
                        'size' => 16,
                        'data-date' => date(DateHelper::FORMAT_USER_DATE),
                        'data-date-viewmode' => 'years',
                        'data-is-inited' => 0,
                    ]); ?>
                    <svg class="date-picker-icon svg-icon input-toggle" style="left:105px;right:unset;">
                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                    </svg>
                </div>
            </div>
        </div>

    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/project/change-end-date']),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php $this->registerJs('
    // add new project
    $(document).on("click", ".add-modal-new-project", function () {
        window.location.href = "/project/create";
    });
    $(document).on("shown.bs.modal", "#copy", function () {
        let confirmButton = $(this).find(".btn-confirm-yes");

        confirmButton.attr("data-url", $(".joint-operation-checkbox:checked").data("copy-url"));
    });
    $(document).on("click", "#copy .btn-confirm-yes", function () {
        window.location.href = $(this).attr("data-url");
    });
    
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("close"));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("open"));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("close"));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("open"));
    });
        
', View::POS_READY);

