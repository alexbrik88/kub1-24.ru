<?php

use frontend\modules\analytics\models\detailing\DetailingUserConfig;
use frontend\themes\kub\helpers\Icon;
use common\models\employee\Config;
use yii\helpers\Html;

$typeOptions = DetailingUserConfig::$chartTypeName + [DetailingUserConfig::CHART_TYPE_PROFITABILITY => 'Рент-ть'];
?>

<div class="dropdown dropdown-settings d-inline-block" title="Настройка графика" style="z-index: 1001">
    <?= Html::button(Icon::get('cog'), [
        'class' => 'marketing-dropdown-btn-icon',
        'data-toggle' => 'dropdown',
    ]) ?>
    <ul id="marketing_dynamics_chart_config" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
        <li>
            <label class="bold nowrap">По умолчанию открывать</label>
        </li>
        <?php foreach ($typeOptions as $typeId => $typeName): ?>
            <li class="_chart_user_config_option" <?php if (!isset($_bt2)) { echo 'style="border-top: 1px solid #ddd"'; $_bt2 = true; } ?>>
                <?= Html::radio('MarketingUserConfig[dynamics_chart_type]', DetailingUserConfig::getIndexChartType() == $typeId, [
                    'class' => 'chart_user_config_radio',
                    'label' => $typeName,
                    'value' => $typeId
                ]); ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>