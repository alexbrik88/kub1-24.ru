<?php

use frontend\components\StatisticPeriod;
use yii\web\JsExpression;
use common\components\TextHelper;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

/** @var $colors array */
/** @var $customChartType */

$id = $id ?? 'chart_22';
$periodSessionName = StatisticPeriod::getSessionNameMonths();
$symbol = ($customChartType == DetailingUserConfig::CHART_TYPE_PROFITABILITY) ? '%' : '₽';
$chartType = (in_array($customChartType, [DetailingUserConfig::CHART_TYPE_PROFIT, DetailingUserConfig::CHART_TYPE_PROFITABILITY]))
    ? 'column'
    : 'pie'
?>

<div class="kub-chart border" style="min-width:335px; min-height:350px; padding-bottom:33px; padding-top: 55px">
    <div class="p-2">
    <div class="ht-caption" style="position: absolute; top:1.5rem; left:16px">
        <b>СТРУКТУРА</b>
        <span style="text-transform: none; font-weight:400;">
            за <span class="text-pie-chart-period"><?= mb_strtolower($periodSessionName) ?></span>
        </span>
    </div>
    <?php

    echo Highcharts::widget(
        DC::getOptions([
            'id' => $id,
            'options' => [
                'chart' => [
                    'type' => $chartType,
                    'marginTop' => 25,
                    'spacingTop' => 0,
                    'height' => $height ?? 200,
                    'events' => new JsExpression('
                        setTimeout(function() {
                            const legend = $(".detailing-index-chart-legend");
                            legend.appendTo("#'.$id.' > .highcharts-container");
                            if (Number(chart_22.chartType) !== 4 && 5 !== Number(chart_22.chartType)) {
                                legend.fadeIn(250);
                            }
                        }, 500)')
                ],
                'title' => false,
                'series' => [$series],
                'tooltip' => [
                    'useHTML' => true,
                    'formatter' => new JsExpression("
                        function(args) {
                            const serieName = this.key;
                            const periodName = (chart_22.periodText || ['-']);
                            const total = chart_22.total || {$total};

                            if (chart_22.chartType == '5') {
                                return ('<table class=\"indicators\">' +
                                    '<span class=\"title\">' + periodName[0].toUpperCase() + periodName.slice(1) + '</span>' + 
                                    '<tr>' +
                                        '<td class=\"gray-text text-left\" colspan=\"2\">' + serieName + '</td>' +
                                    '</tr>' +                                 
                                    '<tr>' +
                                        '<td class=\"gray-text\">Рент-ть:</td>' +
                                        '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' %</td>' +
                                    '</tr>' +
                                    '</table>');
                            }                            
                            
                            return ('<table class=\"indicators\">' +
                                '<span class=\"title\">' + periodName[0].toUpperCase() + periodName.slice(1) + '</span>' +
                                '<tr>' +
                                    '<td class=\"gray-text text-left\" colspan=\"2\">' + serieName + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td class=\"gray-text\">Сумма:</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td class=\"gray-text\">Доля: ' + '</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * this.y / (total || 9E9), 2, ',', ' ') + ' %</td>' +
                                '</tr>' +
                                '</table>');
                        }
                    "),
                ],
                'legend' => [
                    'enabled' => ($chartType === 'column') ? false : true,
                    'floating' => true,
                    'layout' => 'vertical',
                    'verticalAlign' => 'middle',
                    'align' => 'left',
                    'y' => 30,
                    'x' => 210,
                    'itemMarginBottom' => 5,
                    'labelFormatter' => new JsExpression('function () {
                        const name = (this.name.length > 20) ? (this.name.substr(0, 17) + "...") : this.name;
                        return \'<span class="name-name">\' + name + \'</span>\';
                    }'),
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => false,
                        'borderWidth' => 0,
                        'size' => '90%',
                        'innerSize' => '50%',
                        'cursor' => 'pointer',
                        'dataLabels' => false,
                        'center' => ['80', '100']
                    ],
                    'column' => [
                        'pointWidth' => 20,
                        'grouping' => false,
                        'shadow' => false,
                        'groupPadding' => 0.1,
                        'pointPadding' => 0.14,
                        'borderRadius' => 3,
                    ]
                ],
                'xAxis' => [
                    'type' => 'category',
                    'gridLineWidth' => 1,
                    'tickmarkPlacement' => 'between',
                    'labels' => ($chartType == 'pie') ? [
                        'style' => [
                            'font-size' => '12px',
                            'color' => '#9198a0',
                        ],
                    ] : ['enabled' => false]
                ],
                'yAxis' => [
                    'min' => null
                ]
            ],

        ], DC::CHART_PIE)
    );
    ?>
    </div>
</div>

<?php
if (abs($total) < 1E4) {
    $decimals = 2;
    $decimalsTitle = '';
    $decimalsPostfix = '';
    $formattedAmount = $total;
} elseif (abs($total) < 1E6) {
    $decimals = 0;
    $decimalsTitle = '';
    $decimalsPostfix = '';
    $formattedAmount = $total;
} else {
    $decimals = 1;
    $decimalsTitle = '';
    $decimalsPostfix = 'k';
    $formattedAmount = round($total / 1E3, 1);
}
?>

<?php if ($chartType === 'pie'): ?>
<div class="detailing-pie-chart-hint" style="display: none">
    <div class="pie-label">
        <span class="pie-title"><?= $title ?></div>
    <div class="pie-label mb-2">
        <span class="pie-total bold">
            <?= TextHelper::numberFormat($formattedAmount, $decimals) ?><?= $decimalsPostfix ?>
            <?= $symbol ?>
        </span>
    </div>
</div>
<?php endif; ?>