<?php

use common\models\Company;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateSearch;
use yii\data\ActiveDataProvider;

/** @var string $type */
/** @var ProjectEstimate $model */
/** @var Company $company */
/** @var ProjectEstimateSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Смета №' . $model->fullNumber;

echo $this->render('view/estimate/' . $type, [
    'model' => $model,
    'company' => $company,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]);