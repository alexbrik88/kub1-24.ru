<?php

use common\models\project\Project;

/* @var $this yii\web\View */
/* @var $model Project */

$this->title = 'Добавить проект';

$returnUrl = ['project/index'];

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
