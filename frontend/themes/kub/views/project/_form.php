<?php

use common\models\project\Project;
use common\widgets\Modal;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model Project */

?>
<div class="product-form" style="min-height: 500px">
    <?php $form = ActiveForm::begin(array_replace(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'id' => 'project-form',
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-is_new_record' => $model->isNewRecord ? "1" : ""
        ]
    ])); ?>

    <?= $form->errorSummary($model); ?>

    <div class="form-body">
        <?= $this->render('partial/_project_form', [
            'model' => $model,
            'form' => $form,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<!-- Add new -->
<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>


<?php

Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();
?>
