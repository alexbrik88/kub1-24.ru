<?php

use common\components\date\DateHelper;
use yii\helpers\Url;
use common\models\document\status\TaxDeclarationStatus;
use frontend\modules\documents\widgets\DocumentLogWidget;
use common\models\company\CompanyType;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\TaxDeclaration
 * @var $isIp boolean
 * @var $declarationOsnoHelper \frontend\modules\tax\models\DeclarationOsnoHelper
 */

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
?>
<style>
    .preview-tax-tabs {
    }

    .preview-tax-tabs > .navbar {
        min-height: 0;
        margin-bottom: 0;
        float: right;
    }

    .nav-tax-tabs > li {
        float: left;
        position: relative;
        display: block;
    }

    .nav-tax-tabs > li.active > a, .nav-tax-tabs > li.active > a:hover, .nav-tax-tabs > li.active > a:focus {
        background: #4276a4;
        color: #fff;
    }

    .nav-tax-tabs > li > a {
        padding: 5px 10px;
    }

    .table-preview {
        width: 100%
    }

    .table-preview td.font-size-11 {
        font-size: 12px;
    }

    .table-preview td.font-main {
        font-size: 14px;
        font-weight: bold;
        padding-bottom: 0;
    }

    .table-preview td.border-bottom {
        border-bottom: 1px solid #000;
    }

    .table-preview td.tip {
        padding: 0 0 3px 0;
    }

    .table-preview td.ver-top {
        vertical-align: top;
    }

    .table-preview td.dotbox {
        width: 16px;
        font-size: 16pt;
        border: 1px dotted #333;
        text-align: center;
        padding: 0 2px;
    }

    .table-preview td.hh {
        line-height: 20pt;
    }

    .table-preview td.small-italic {
        font-style: italic;
        font-size: 8pt;
        text-align: center;
    }

    .table-preview tr.middle td {
        vertical-align: middle;
        padding: 4px 0;
    }

    .table-preview td {
        padding: 0 5px;
        margin: 0;
    }

    .preview-page {
        border-bottom: 2px dotted #666;
        padding-bottom: 50px;
        margin-bottom: 50px;
    }

    .table-preview td.text-center {
        text-align: center;
    }

    .table-preview td.text-right {
        text-align: right;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table no_min_h pad0">
    <div class="col-xs-12 pad5 pre-view-table">
        <?php /*
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-12 pad0 font-bold" style="height: inherit">
                <div style="padding-top: 12px">
                    <p style="font-size: 17px; width:100%; text-align: center">
                        <?php if ($isIp): ?>
                            Налоговая декларация за <?= $model->tax_year; ?> год
                        <?php elseif ($model->nds): ?>
                            Декларация на добавленную стоимость за <?= "{$model->tax_quarter}-й квартал {$model->tax_year} года" ?>
                        <?php elseif ($model->org): ?>
                            Декларация на прибыль организации за <?= "{$model->tax_quarter}-й квартал {$model->tax_year} года" ?>
                        <?php elseif ($model->balance): ?>
                            Бухгалтерский баланс за <?= "{$model->tax_year} год" ?>
                        <?php elseif ($model->szvm): ?>
                            <?php $monthName = mb_strtolower(\yii\helpers\ArrayHelper::getValue(\common\components\helpers\Month::$monthFullRU, $model->tax_month)); ?>
                            Сведения о застрахованных лицах за <?= "{$monthName} {$model->tax_year} года" ?>
                        <?php else: ?>
                            Единая (упрощенная) налоговая декларация ОСНО за <?= "{$model->tax_quarter}-й квартал {$model->tax_year} года" ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        */ ?>
        <div class="col-xs-12 pad3 preview-declaration" style="margin-top: 15px;">
            <div class="col-xs-12 pad0" style="font-size: 12px; text-align:justify">
                <?php if ($isIp): ?>
                    <div class="preview-page">
                        <?= $this->render('_viewText-page-1', [
                            'model' => $model,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('_viewText-page-2', [
                            'model' => $model,
                            'quarters' => $model->taxDeclarationQuarters,
                        ]); ?>
                    </div>
                    <div>
                        <?= $this->render('_viewText-page-3', [
                            'model' => $model,
                            'quarters' => $model->taxDeclarationQuarters,
                        ]); ?>
                    </div>
                <?php elseif ($model->nds): ?>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/nds/_viewText-page-1', [
                            'model' => $model,
                            'declarationOsnoHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/nds/_viewText-page-2', [
                            'model' => $model,
                            'declarationOsnoHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                <?php elseif ($model->org): ?>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-1', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-2', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'columnData' => [
                                '010' => $model->company->oktmo,
                                '030' => '18210101011011000110',
                                '040' => 0,
                                '050' => 0,
                                '060' => '18210101011011000110',
                                '070' => 0,
                                '080' => 0
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-3', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '010' => 0,
                                '020' => 0,
                                '030' => 0,
                                '040' => 0,
                                '050' => 0,
                                '060' => 0,
                                '070' => 0,
                                '080' => 0,
                                '100' => 0,
                                '110' => 0,
                                '120' => 0,
                                '130' => 0,
                                '140' => '20.000',
                                '150' => '3.00',
                                '160' => '17.000',
                                '170' => 0,
                                '180' => 0,
                                '190' => 0,
                                '200' => 0,
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-4', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '210' => 0,
                                '220' => 0,
                                '230' => 0,
                                '240' => 0,
                                '250' => 0,
                                '260' => 0,
                                '265' => 0,
                                '266' => 0,
                                '267' => 0,
                                '270' => 0,
                                '271' => 0,
                                '280' => 0,
                                '281' => 0,
                                '290' => 0,
                                '300' => 0,
                                '310' => 0,
                                '320' => 0,
                                '330' => 0,
                                '340' => 0,
                                '350' => 0,
                                '351' => 0,
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-5', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '010' => 0,
                                '011' => 0,
                                '012' => 0,
                                '013' => 0,
                                '014' => 0,
                                '020' => 0,
                                '021' => 0,
                                '022' => 0,
                                '023' => 0,
                                '024' => 0,
                                '027' => 0,
                                '030' => 0,
                                '040' => 0,
                                '100' => 0,
                                '101' => 0,
                                '102' => 0,
                                '103' => 0,
                                '104' => 0,
                                '105' => 0,
                                '106' => 0,
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-6', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '010' => 0,
                                '020' => 0,
                                '030' => 0,
                                '040' => 0,
                                '041' => 0,
                                '042' => 0,
                                '043' => 0,
                                '045' => 0,
                                '046' => 0,
                                '047' => 0,
                                '048' => 0,
                                '049' => 0,
                                '050' => 0,
                                '051' => 0,
                                '052' => 0,
                                '053' => 0,
                                '054' => 0,
                                '055' => 0,
                                '059' => 0,
                                '060' => 0,
                                '061' => 0,
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-7', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '070' => 0,
                                '071' => 0,
                                '072' => 0,
                                '073' => 0,
                                '080' => 0,
                                '090' => 0,
                                '100' => 0,
                                '110' => 0,
                                '120' => 0,
                                '130' => 0,
                                '131' => 0,
                                '132' => 0,
                                '133' => 0,
                                '134' => 0,
                                '135' => '1',
                                '200' => 0,
                                '201' => 0,
                                '202' => 0,
                                '204' => 0,
                                '205' => 0,
                                '206' => 0,
                            ],
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-8', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                            'taxpayerOrganizationCode' => 1,
                            'columnData' => [
                                '300' => 0,
                                '301' => 0,
                                '302' => 0,
                                '400' => 0,
                                '401' => ['year' => '', 'sum' => 0],
                                '402' => ['year' => '', 'sum' => 0],
                                '403' => ['year' => '', 'sum' => 0],
                            ],
                        ]); ?>
                    </div>
                <?php elseif ($model->balance): ?>
                    <style>
                        .table-preview.border,
                        .table-preview.border tr td {
                            border: 1px solid;
                        }

                        .table-preview {
                            margin: 0;
                            padding: 0;
                        }

                        .table-preview td {
                            font-size: 9.5pt;
                            vertical-align: middle;
                            padding: 1px 2px;
                        }

                        .table-preview td.th {
                            padding-top: 5px;
                            padding-bottom: 5px;
                        }

                        .table-preview td.bold {
                            font-weight: bold
                        }

                        .table-preview td.no-bt {
                            border-top: none !important;
                        }

                        .table-preview td.no-bb {
                            border-bottom: none !important;
                        }

                        .table-preview tr.fs-8 td {
                            font-size: 8.5pt;
                        }

                        .table-preview td.fs-10 {
                            font-size: 10pt;
                        }

                        .table-preview td.fs-11 {
                            font-size: 11pt;
                        }

                        .table-preview td.bt {
                            border-top: 1px solid #000 !important;
                        }

                        .table-preview td.bb {
                            border-bottom: 1px solid #000 !important;
                        }

                        .table-preview td.bl {
                            border-left: 1px solid #000 !important;
                        }

                        .table-preview td.br {
                            border-right: 1px solid #000 !important;
                        }

                        .table-preview td.bt2 {
                            border-top: 2px solid #000 !important;
                        }

                        .table-preview td.bb2 {
                            border-bottom: 2px solid #000 !important;
                        }

                        .table-preview td.bl2 {
                            border-left: 2px solid #000 !important;
                        }

                        .table-preview td.br2 {
                            border-right: 2px solid #000 !important;
                        }

                        .table-preview td.tip {
                            padding: 0 0 3px 0;
                            font-size: 8pt;
                            text-align: center;
                        }

                        .table-preview td.pad-3 {
                            padding-top: 3pt;
                            padding-bottom: 3pt;
                        }

                        .table-preview td.ver-bottom {
                            vertical-align: bottom
                        }

                        .table-preview td.ver-top {
                            vertical-align: top
                        }

                        .table-preview td.pad-l {
                            padding-left: 10pt
                        }
                    </style>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/balance/partial/_viewText-page-1', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/balance/partial/_viewText-page-2', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/balance/partial/_viewText-page-3', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                    <div class="preview-page">
                        <?= $this->render('@frontend/modules/tax/views/balance/partial/_viewText-page-4', [
                            'model' => $model,
                            'declarationHelper' => $declarationOsnoHelper,
                        ]); ?>
                    </div>
                <?php elseif ($model->szvm): ?>
                    <style>
                        .table-preview {
                            margin: 0;
                            padding: 0;
                        }

                        .table-preview td {
                            font-size: 11pt;
                            vertical-align: bottom;
                            padding: 4px 2px 0;
                        }

                        .table-preview td.font-big {
                            font-size: 13pt;
                        }

                        .table-preview td.font-medium {
                            font-size: 8.5pt;
                        }

                        .table-preview td.font-small {
                            font-size: 7pt;
                        }

                        .table-preview td.text-underline {
                            text-decoration: underline
                        }

                        .table-preview td.bb {
                            border-bottom: 1px solid #000 !important;
                        }

                        .table-preview td.cc {
                            text-align: center;
                            vertical-align: middle
                        }

                        .table-preview td.bb-data {
                            border-bottom: 1px solid #000;
                            font-weight: bold
                        }

                        .table-preview.border,
                        .table-preview.border tr td {
                            border: 1px solid;
                        }
                    </style>
                    <?= $this->render('@frontend/modules/tax/views/default/_viewText-szvm', [
                        'model' => $model,
                        'declarationHelper' => $declarationOsnoHelper,
                    ]); ?>
                <?php else: ?>
                    <?= $this->render('@frontend/modules/tax/views/declaration/end/_viewText-page-1', [
                        'model' => $model,
                        'declarationOsnoHelper' => $declarationOsnoHelper,
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

