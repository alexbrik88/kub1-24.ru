<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

$companyName = Yii::$app->user->identity->company->getTitle(true);
?>

<div class="taxrobot-start-header">
    <h4 class="counter-toggle-hide">
        Подождите, рассчитываем налоги и заполняем декларацию…
    </h4>
    <h4 class="counter-toggle-hide hidden">
        Готово!
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-3 wait-image"></div>
    <div class="counter-toggle-hide text-center">
        Осталось:
        <strong style="display: inline-block; font-size: 20px; width: 50px">
            <span class="counter-element link">15</span>
        </strong>
        секунд
    </div>
    <div class="counter-toggle-hide hidden text-center">
        <div>
            Налоги по <strong><?= $companyName ?></strong> рассчитаны,
            <br>
            платежки по налогам подготовлены, декларация заполнена.
            <br>
            <strong>
                Для скачивания платежек и декларации, необходимо оплатить сервис
            </strong>
        </div>
        <?= Html::button('Оплатить доступ к сервису', [
            'href' => Url::to(['/tax/robot/calculation', 'reduce' => 'hide']),
            'class' => 'button-regular button-regular_red mt-3 not-ajax ladda-button ladda-custom',
        ]) ?>
    </div>

</div>
