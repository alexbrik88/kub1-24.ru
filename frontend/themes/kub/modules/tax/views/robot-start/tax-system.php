<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$items = TaxrobotStartForm::$taxSystemItems;
$i = 0;
?>

<div class="taxrobot-start-header">
    <h4>
        Ваша система налогообложения?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="row">
        <?php foreach ($items as $key => $value) : ?>
            <?php $form = ActiveForm::begin([
                'id' => 'tax-system-form-'.$key,
                'action' => ['tax-system'],
                'options' => [
                    'class' => 'items-form',
                ],
            ]); ?>

                <?= Html::activeHiddenInput($model, 'tax_system', [
                    'id' => Html::getInputId($model, 'tax_system')."_.$key",
                    'value' => $key,
                ]) ?>

                <?= Html::submitButton($value, [
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>

            <?php ActiveForm::end() ?>
            <?= ++$i == 3 ? "</div>\n<div class=\"row\">" : '' ?>
        <?php endforeach ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['activity']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>