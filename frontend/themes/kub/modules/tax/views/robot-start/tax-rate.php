<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$items = TaxrobotStartForm::$taxRateItems;
?>

<div class="taxrobot-start-header">
    <h4>
        Ваша система налогообложения?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-4">
        <button class="button-regular button-hover-transparent">УСН «Доходы»</button>
    </div>

    <h4>
        <strong>Ваша налоговая ставка?</strong>
    </h4>

    <div class="row mt-4">
        <?php foreach ($items as $key => $value) : ?>
            <?php $form = ActiveForm::begin([
                'id' => 'tax-rate-form-'.$key,
                'action' => ['tax-rate'],
                'options' => [
                    'style' => 'display: inline-block; padding: 0 15px; margin: 0 0 20px;',
                ],
            ]); ?>

                <?= Html::activeHiddenInput($model, 'tax_rate', [
                    'id' => Html::getInputId($model, 'tax_rate')."_.$key",
                    'value' => $key,
                ]) ?>

                <?= Html::submitButton($value, [
                    'class' => 'button-regular button-hover-transparent',
                ]) ?>

            <?php ActiveForm::end() ?>
        <?php endforeach ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['tax-system']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>