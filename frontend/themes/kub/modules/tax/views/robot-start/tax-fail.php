<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$taxName = $model->getTaxName();
$model->subject = 'Программа еще учится работать с вашей системой налогообложения…';
?>

<div class="taxrobot-start-header">
    <h4>
        Ваша система налогообложения?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-4">
        <button class="button-regular button-hover-transparent"><?= $model->getTaxName() ?></button>
    </div>

    <h5>
        <strong>Программа еще учится работать с вашей системой налогообложения…</strong>
    </h5>
    <h5>
        <strong>НО, у нас хорошие новости для вас!</strong>
    </h5>

    <div>
        Пока программа учится, наши бухгалтера помогут рассчитать ваши налоги и сдать отчетность.
        Оставьте ваши контакты, и мы с вами свяжемся
    </div>

    <div class="mt-4">
        <?php $form = ActiveForm::begin([
            'id' => 'tax-fail-form',
            'action' => ['tax-fail'],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        ]); ?>

            <?= Html::activeHiddenInput($model, 'subject') ?>
            <?= Html::activeHiddenInput($model, 'tax_system') ?>

            <div class="row mt-4">
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_name')->textInput(['maxLength' => true]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_phone')->textInput([
                        'maxLength' => true,
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <label class="label">&nbsp;</label>
                    <div>
                        <?= Html::submitButton('Отправить', [
                            'class' => 'button-regular button-hover-transparent min-w-130 ladda-button ladda-custom',
                        ]) ?>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['tax-system']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>