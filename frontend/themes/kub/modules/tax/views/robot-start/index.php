<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

$url = Url::to(['/tax/robot-start/activity']);
?>

<style type="text/css">
.robot-promo-next-arrow {
    position: absolute;
    top: 13px;
    left: -18%;
    width: 35%;
}
</style>

<div class="taxrobot-start-header">
    <h4>
        Как это работает?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="row mb-3">
        <div class="col d-flex flex-column justify-content-end">
            <div>
                <?= Html::img('/img/taxrobot/1.1.png', [
                    'style' => 'width: 100%;',
                    'alt' => '',
                ]) ?>
            </div>
            <div class="mt-3">
                Отвечаете на вопросы
            </div>
        </div>
        <div class="col d-flex flex-column justify-content-end">
            <div>
                <?= Html::img('/img/taxrobot/arrow.svg', [
                    'class' => 'robot-promo-next-arrow',
                    'alt' => '',
                ]) ?>
                <?= Html::img('/img/taxrobot/1.2.png', [
                    'style' => 'width: 100%;',
                    'alt' => '',
                ]) ?>
            </div>
            <div class="mt-3">
                Загружаем выписку
            </div>
        </div>
        <div class="col d-flex flex-column justify-content-end">
            <div>
                <?= Html::img('/img/taxrobot/arrow.svg', [
                    'class' => 'robot-promo-next-arrow',
                    'alt' => '',
                ]) ?>
                <?= Html::img('/img/taxrobot/1.3.png', [
                    'style' => 'width: 100%;',
                    'alt' => '',
                ]) ?>
            </div>
            <div class="mt-3">
                Анализируем данные
            </div>
        </div>
        <div class="col d-flex flex-column justify-content-end">
            <div>
                <?= Html::img('/img/taxrobot/arrow.svg', [
                    'class' => 'robot-promo-next-arrow',
                    'alt' => '',
                ]) ?>
                <?= Html::img('/img/taxrobot/1.4.png', [
                    'style' => 'width: 100%;',
                    'alt' => '',
                ]) ?>
            </div>
            <div class="mt-3">
                Рассчитываем налоги
            </div>
        </div>
        <div class="col d-flex flex-column justify-content-end">
            <div>
                <?= Html::img('/img/taxrobot/arrow.svg', [
                    'class' => 'robot-promo-next-arrow',
                    'alt' => '',
                ]) ?>
                <?= Html::img('/img/taxrobot/1.5.png', [
                    'style' => 'width: 100%;',
                    'alt' => '',
                ]) ?>
            </div>
            <div class="mt-3">
                Заполняем декларацию
            </div>
        </div>
    </div>

    <div class="mb-3" style="height: 360px;">
        <?=  Html::tag('iframe', '', [
            'src' => 'https://www.youtube.com/embed/6J30L5e5_ZU',
            'width' => '100%',
            'height' => '100%',
            'frameborder' => '0',
            'allow' => 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture',
            'allowfullscreen' => true,
        ]) ?>
    </div>
</div>

<div class="next-button">
    <?= Html::button('Далее', [
        'href' => Url::to(['select']),
        'class' => 'button-regular button-regular_red min-w-130 pull-right',
    ]) ?>
</div>
