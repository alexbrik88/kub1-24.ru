<?php

use common\components\ImageHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $bankingData array */

$i = 0;
?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки из банка
    </h4>
</div>

<div class="taxrobot-start-content">
    <h5>
        <strong>Выберите банк</strong>
    </h5>

    <div id="select-bank-buttons">
        <div class="row d-flex align-items-stretch mt-4">
            <?php foreach ($bankingData as $data) : ?>
                <div class="col">
                    <?= Html::tag('div', ImageHelper::getThumb($data['logo'], [250, 150], [
                        'class' => 'bank-logo',
                        'alt' => $data['name'],
                    ]), [
                        'class' => 'button-regular button-hover-transparent w-100 h-100',
                        'href' => Url::to(['account', 'alias' => $data['alias']]),
                    ]) ?>
                </div>
                <?= ++$i == 3 ? "</div>\n<div class=\"row d-flex align-items-stretch mt-4\">" : '' ?>
            <?php endforeach ?>
            <div class="col">
                <?= Html::button('Нет моего банка', [
                    'href' => Url::to(['upload-select']),
                    'class' => 'col button-regular button-hover-transparent w-100 h-100',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['upload-select']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
