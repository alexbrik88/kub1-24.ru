<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки файлом
    </h4>
</div>

<div class="taxrobot-start-content">
    <?= $content ?>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['upload-file']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
