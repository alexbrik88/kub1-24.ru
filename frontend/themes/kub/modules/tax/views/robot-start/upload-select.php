<?php

use frontend\components\Icon;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

?>

<div class="taxrobot-start-header">
    <h4>
        Выберите способ загрузки выписки
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-4">
        Для расчета налогов необходимы данные о доходах и о уже сделанных налоговых платежах
    </div>
    <div class="row mb-4">
        <div class="col-sm-6">
            <?= Html::button(Icon::get('1c', ['class' => 'mr-3']).'Загрузить выписку файлом', [
                'href' => Url::to(['upload-file']),
                'class' => 'button-regular button-hover-transparent w-100',
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::button(Icon::get('bank-3', ['class' => 'mr-3']).'Загрузить выписку из банка', [
                'href' => Url::to(['banking-select']),
                'class' => 'button-regular button-hover-transparent w-100',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            Время - <strong>до 7 минут</strong>
        </div>
        <div class="col-sm-6">
            Время - <strong>1 минута</strong>
        </div>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to([Yii::$app->request->get('return', 'staff')]),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
