<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var $this yii\web\View */
/** @var $route string */
/** @var $alias string */
/** @var $account_id int */

$redirect_url = Url::to([
    '/tax/robot-start/banking-redirect',
    'alias' => $alias,
    'account_id' => $account_id,
]);
$banking_url = Url::to([
    $route,
    'account_id' => $account_id,
    'redirect_url' => $redirect_url,
    'auto_auth' => 1,
]);
?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки из банка
    </h4>
</div>

<div class="taxrobot-start-content">
    <?php Pjax::begin([
        'id' => 'banking-module-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'timeout' => 10000,
        'options' => [
            'data-banking-url' => $banking_url,
        ]
    ]); ?>

    <?php Pjax::end(); ?>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['banking-select']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>

<?php
$this->registerJs(<<<JS
$(document).ready(function () {
    $.pjax({
        container: '#banking-module-pjax',
        url: $('#banking-module-pjax').data('banking-url'),
        push: false
    });
});
JS
);
