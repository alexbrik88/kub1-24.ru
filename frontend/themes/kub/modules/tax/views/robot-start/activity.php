<?php

use common\models\company\CompanyActivity;
use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$items = CompanyActivity::find()->select('name')->indexBy('id')->column();
?>

<div class="taxrobot-start-header">
    <h4>
        Ваш вид деятельности?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="row">
        <?php foreach ($items as $key => $value) : ?>
            <?php $form = ActiveForm::begin([
                'id' => 'activity-form-'.$key,
                'action' => ['activity'],
                'options' => [
                    'class' => 'items-form',
                ],
            ]); ?>

                <?= Html::activeHiddenInput($model, 'company_activity_id', [
                    'id' => Html::getInputId($model, 'company_activity_id')."_.$key",
                    'value' => $key,
                ]) ?>

                <?= Html::submitButton($value, [
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>

            <?php ActiveForm::end() ?>
        <?php endforeach ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['select']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
