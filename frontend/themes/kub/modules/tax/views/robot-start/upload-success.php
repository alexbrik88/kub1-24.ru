<?php

use frontend\widgets\Alert;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки
    </h4>
</div>

<div class="taxrobot-start-content">

    <?= Alert::widget(); ?>

    <div class="row">
        <div class="col-2" style="padding-top: 12px;">
            Загрузить еще?
        </div>
        <div class="col-4">
            <?= Html::button('Да', [
                'href' => Url::to(['upload-select', 'return' => 'upload-success']),
                'class' => 'button-regular button-hover-transparent w-100',
            ]) ?>
        </div>
        <div class="col-4">
            <?= Html::button('Нет', [
                'href' => Url::to(['address']),
                'class' => 'button-regular button-hover-transparent w-100',
            ]) ?>
        </div>
    </div>
</div>
