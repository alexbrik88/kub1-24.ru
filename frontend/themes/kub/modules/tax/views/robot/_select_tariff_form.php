<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\kub\components\Icon;

$company = $taxRobot->company;
$tariffItems = [];
$tariffArray = SubscribeTariff::find()->actual()->andWhere([
    'tariff_group_id' => $groupId,
])->orderBy(['price' => SORT_ASC])->all();
foreach ($tariffArray as $key => $tariff) {
    $tariffItems[] = [
        'label' => $tariff->getTariffName(),
        'url' => '#',
        'linkOptions' => [
            'class' => 'choose-tariff',
            'data' => [
                'tariff-id' => $tariff->id,
            ],
        ],
    ];
}
$baseTariff = reset($tariffArray);
$model = new PaymentForm($company, ['tariffId' => $baseTariff->id]);
?>

<div class="<?= $needSelect ? 'additional-content hidden group_'.$groupId : 'main-content' ?>">
    <?php if ($needSelect) : ?>
        <div class="tariff-block-wrapper mt-0" style="font-size: 18px;">
            <strong>
                <a href="#" class="tariff-select-cancel link">Назад</a>
            </strong>
        </div>
    <?php endif ?>
    <div class="header">
        <table style="width: 100%;">
            <tr>
                <td style="width: 60px; vertical-align: middle; text-align: center;">
                    <?= Html::tag('div', Icon::get(Icon::forTariff($groupId), ['style' => 'font-size: 36px;']), [
                        'style' => '
                            display: inline-flex;
                            align-items: center;
                            justify-content: center;
                            width: 60px;
                            height: 60px;
                            background-color: #4679AE;
                            color: #fff;
                            border-radius: 30px;
                        ',
                    ]) ?>
                </td>
                <td style="padding-left: 10px; font-size: 18px;">
                    <?= implode('<br>ИП', explode(' ИП', $baseTariff->tariffGroup->name)) ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="part-pay">
        <div style="padding-top: 30px;">
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'class' => 'tariff-group-payment-form',
            ]) ?>
                <?= Html::activeHiddenInput($model, 'tariffId', [
                    'class' => 'tariff-id-input',
                ]) ?>
                <div class="text-bold">
                    <div style="display: inline-block; font-size: 18px;">Оплата за </div>
                    <div style="display: inline-block; width: 120px; font-size: 18px;">
                        <?php if (count($tariffItems) == 1) : ?>
                            <?= $tariffItems[0]['label'] ?>
                        <?php else : ?>
                            <div class="dropdown">
                                <?= Html::tag('div', $tariffItems[0]['label'], [
                                    'class' => 'dropdown-toggle link',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                ])?>
                                <?= \yii\bootstrap4\Dropdown::widget([
                                    'id' => 'tax-robot-tariff-dropdown',
                                    'encodeLabels' => false,
                                    'items' => $tariffItems,
                                ])?>
                            </div>
                        <?php endif ?>
                    </div>
                    <?php foreach ($tariffArray as $tariff) : ?>
                        <?php
                        $discount = $company->getMaxDiscount($tariff->id);
                        $discVal = $discount ? $discount->value : 0;
                        $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                        ?>
                        <?= Html::beginTag('div', [
                            'class' => "tariff-price tariff-price-{$tariff->id}" .
                               ($baseTariff->id != $tariff->id ? ' hidden' : ''),
                        ]) ?>
                            <table>
                                <tr>
                                    <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                    <td class="pad-l-10">
                                        <?php if ($discount) : ?>
                                            <div>
                                                <del><?= $tariff->price ?> ₽</del>
                                                <span class="color-red">СКИДКА <?=round($discVal)?>%</span>
                                            </div>
                                            <div class="color-red">
                                                при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                            </div>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?= round($price / $tariff->duration_month) ?>
                                        ₽ / месяц
                                    </td>
                                </tr>
                            </table>
                        <?= Html::endTag('div') ?>
                    <?php endforeach; ?>
                </div>
                <div class="mar-t-20">
                    <p style="font-weight:bold;font-size:18px">Вы получите:</p>
                    <p>Автоматический расчёт налогов.</p>
                    <p style="font-weight:bold">Уменьшение налога в соответствии с законом.</p>
                    <p>Подготовка налоговых платёжек.</p>
                    <?php if ($groupId == SubscribeTariffGroup::TAX_DECLAR_IP_USN_6) : ?>
                        <p>Автоматическая подготовка налоговой декларации.</p>
                        <p>Вся отчётность в налоговую, кроме отчётов за сотрудников.</p>
                        <p>Автоматическая подготовка КУДиР.</p>
                    <?php endif ?>
                    <p style="font-weight:bold;">
                        Тариф "Выставление счетов" не входит в стоимость тарифа
                        "<?= $baseTariff->tariffGroup->name ?>".
                    </p>
                </div>

                <div id="js-form-alert" class="mar-t-20"></div>

                <div class="row mar-t-20">
                    <div class="col-6">
                        <?= Html::submitButton('Картой', [
                            'class' => 'button-regular button-regular_red submit',
                            'name' => Html::getInputName($model, 'paymentTypeId'),
                            'value' => PaymentType::TYPE_ONLINE,
                            'style' => 'width: 100%;',
                            'data-style' => 'zoom-in',
                        ]); ?>
                    </div>
                    <div class="col-6">
                        <?= Html::submitButton('Выставить счет', [
                            'class' => 'button-regular button-regular_red submit',
                            'name' => Html::getInputName($model, 'paymentTypeId'),
                            'value' => PaymentType::TYPE_INVOICE,
                            'style' => 'width: 100%;',
                            'data-style' => 'zoom-in',
                        ]); ?>
                    </div>
                </div>
                <div class="form-submit-result hidden"></div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>
