
<div class="date-picker-wrap">
    <div class="position-relative">
        {input}
        <svg class="date-picker-icon svg-icon input-toggle">
            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
        </svg>
    </div>
</div>