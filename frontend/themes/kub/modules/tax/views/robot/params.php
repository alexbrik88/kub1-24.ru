<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use \php_rutils\RUtils;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Параметры вашего ИП';
$this->params['step'] = 2;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$taxation = $model->companyTaxationType;
$taxation->usn = 1;
$taxation->osno = $taxation->envd = $taxation->psn = 0;
$taxation->usn_type = CompanyTaxationType::INCOME;
$usnType = [
    CompanyTaxationType::INCOME => 'Доходы',
    CompanyTaxationType::INCOME_EXPENSES => 'Доходы минус расходы',
];

if (empty($accounts)) {
    $accounts_title = 'Нет расчетных счетов в банках';
} else {
    $accounts_title = [];
    foreach ($accounts as $value) {
        $accounts_title[] =
            $value['cnt'] . ' ' .
            RUtils::numeral()->choosePlural($value['cnt'], ['расчетный счет', 'расчетных счета', 'расчетных счетов']) .
            ' в ' . $value['bank_name'];
    }
    $accounts_title = implode(', ', $accounts_title);
}
$questions = [
    'no_accounts' => $accounts_title,
    'no_workers' => 'Нет сотрудников',
    'no_foreign_currency' =>  'Нет валютных счетов',
    'no_acquiring' => 'Нет эквайринга',
    'no_cashbox' => 'Нет кассы'
];
$ofds = [
    'Ofd[taxcom]'  => 'Такском',
    'Ofd[2]'  => 'Платформа ОФД',
    'Ofd[3]'  => 'Первый ОФД',
    'Ofd[4]'  => 'Ярус',
    'Ofd[5]'  => 'Петер-Сервис Спецтехнологии (OFD.RU)',
    'Ofd[6]'  => 'Тензор',
    'Ofd[7]'  => 'СКБ Контур',
    'Ofd[8]'  => 'Тандер',
    'Ofd[9]'  => 'Калуга Астрал',
    'Ofd[10]' => 'Яндекс.ОФД',
    'Ofd[11]' => 'ЭнвижнГруп',
    'Ofd[12]' => 'Вымпел-Коммуникации',
    'Ofd[13]' => 'Мультикарта',
];
?>

<?php
$questions_messages['no_accounts'] = '
    <div id="no_accounts_message" style="display:none">
        <p>Если данные по расчетным счетам не соответствуют действительности, то вернитесь на предыдущий шаг и либо добавьте отсутствующий счет, либо удалите счет, которого нет.</p>
        <p>Если у вас есть счет, но по нему нет оборотов, то укажите его.</p>
        <p>Если у вас был счет, но вы его закрыли, то не удаляйте его.</p>
    </div>';
$questions_messages['no_workers'] = '
    <div id="no_workers_message" style="display:none">
        <p>Если у вас есть сотрудники, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность расчета налогов при наличие сотрудников.</p>
    </div>';
$questions_messages['no_foreign_currency'] = '
    <div id="no_foreign_currency_message" style="display:none">
        <p>Если у вас есть валютные счета и по ним были операции, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций по валютным счетам.</p>
    </div>';
$questions_messages['no_acquiring'] = '
    <div id="no_acquiring_message" style="display:none">
        <p>Если у вас есть эквайринг, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций с учётом эквайринга.</p>
    </div>';
$questions_messages['no_cashbox'] = '
    <div id="no_cashbox_message" style="display:none">
        <p>Если у вас есть касса, то пока мы не можем загрузить данные из ОФД (Оператора фискальных данных).
            Вы сможете добавить общую сумму дохода по кассе вручную.
            Мы скоро добавим возможность загружать данные из ОФД.</p>
        <p>Укажите ваш ОФД и мы вам сообщим о возможности загрузки данных из вашего ОФД.</p>
        <div class="row">
        ';
foreach ($ofds as $key => $value) {
    $isChecked = ($key == 'Ofd[taxcom]' && Yii::$app->request->cookies['taxcom_user']);

    $questions_messages['no_cashbox'] .= '<div class="col-4">';
    $questions_messages['no_cashbox'] .= Html::label(Html::checkbox(htmlspecialchars($key), $isChecked, [
        'class' => 'form-group',
    ]) . $value, null, [
        'class' => 'text_size_14'
    ]);
    $questions_messages['no_cashbox'] .= '</div>';
}
        $questions_messages['no_cashbox'] .= '
        </div>
    </div>';

$taxNote = Html::tag('span', '', [
    'style' => 'position: absolute; left: 13px; top: 0; width: 20px; height: 20px;',
    'title' => 'Бухгалтерия ИП работает только для УСН доходы',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-ip-params',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'options' => [
            'class' => ''
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => ''
            ],
        ],
        'checkEnclosedTemplate' => "{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n{error}",
    ],
]); ?>
    <div class="wrap p-4">
        <div class="p-2">
            <div class="d-flex justify-content-between  align-items-start">
                <h3 class="text_size_20 mb-4">Шаг 2: Укажите параметры вашего ИП</h3>
                <div>
                    <?= $this->render('_video_instruction') ?>
                </div>
            </div>
            <strong class="title-small d-block mb-3">Система налогообложения</strong>
            <div class="row pb-3 mb-2">
                <div class="col-12">
                    <div class="row align-items-end">
                        <div class="column mb-3 pos-r">
                            <?= $form->field($taxation, 'osno', [
                                'template' => "{label}\n{input}",
                                'options' => [
                                    'data-tooltip-content' => '#tooltip_osno'
                                ],
                            ])->checkbox([
                                'disabled' => (boolean) $taxation->usn,
                            ], true) ?>
                            <?= $taxNote ?>
                            <?= Html::activeHiddenInput($taxation, 'osno', ['value' => 0]) ?>
                        </div>
                        <div class="column mb-3">
                            <?= $form->field($taxation, 'usn', [
                                'template' => "{label}\n{input}",
                            ])->checkbox([
                                'disabled' => true,
                            ], true) ?>
                            <?= Html::activeHiddenInput($taxation, 'usn', ['value' => 1]) ?>
                        </div>
                        <div class="column mb-3 pos-r">
                            <?= $form->field($taxation, 'envd', [
                                'template' => "{label}\n{input}",
                            ])->checkbox([
                                'disabled' => true,
                            ], true) ?>
                            <?= $taxNote ?>
                            <?= Html::activeHiddenInput($taxation, 'envd', ['value' => 0]) ?>
                        </div>
                        <div class="column mb-3 pos-r">
                            <?= $form->field($taxation, 'psn', [
                                'template' => "{label}\n{input}",
                            ])->checkbox([
                                'disabled' => true,
                            ], true) ?>
                            <?= $taxNote ?>
                            <?= Html::activeHiddenInput($taxation, 'psn', ['value' => 0]) ?>
                        </div>
                    </div>
                </div>
                <?= $form->field($taxation, 'usn_type', [
                    'options' => [
                        'class' => 'column tax-usn-config collapse' . ($taxation->usn ? ' show' : ''),
                    ],
                ])->radioList($usnType, [
                    'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                        $radio = Html::radio($name, $checked, [
                            'id' => 'usn_radio_'.$index,
                            'value' => $value,
                            'label' => $label,
                            'labelOptions' => [
                                'wrapInput' => true,
                            ],
                        ]);
                        $itemRadio = Html::tag('div', $radio, [
                            'class' => 'column mb-2',
                        ]);
                        $percentValue = $checked && $taxation->usn_percent ? $taxation->usn_percent :
                            CompanyTaxationType::$usnDefaultPercent[$value];
                        $percent = Html::textInput('tmp_usn_percent_'.$index, $percentValue, [
                            'class' => 'form-control d-inline-block text-right',
                            'style' => 'width: 75px;'
                        ]) . ' <span class="ml-1">%</span>';
                        $itemPercent = Html::tag('div', $percent, [
                            'class' => 'column mb-2 pl-0',
                        ]);

                        return Html::tag('div', "$itemRadio\n$itemPercent", [
                            'class' => 'row align-items-center justify-content-between',
                        ]);
                    },
                ])->label(false) . "\n"; ?>
                <div class="col-12 tax-patent-config collapse <?=($taxation->psn ? ' show' : '')?>">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group form-md-underline">
                                <label> Субъект РФ, где получен патент: </label>
                                <?= Select2::widget([
                                    'id' => 'select-ip_patent_city',
                                    'name' => 'Company[ip_patent_city]',
                                    'value' => ($taxation->psn) ? $model->ip_patent_city : null,
                                    'options' => [
                                        'placeholder' => '',
                                        'class' => 'form-control js-ifns-search',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'minimumInputLength' => 0,
                                        'language' => [
                                            'inputTooShort' => new JsExpression('function(){ return "Введите первую букву" }')
                                        ],
                                        'ajax' => [
                                            'url' => "/dictionary/fias",
                                            'dataType' => 'json',
                                            'delay' => 250,
                                            'data' => new JsExpression('function(params) { return {q:params.term, level:1, guid: null}; }'),
                                            'processResults' => new JsExpression('
                                        function (data) {
                                            return {
                                                results: [{id:"---", text:"---"}].concat($.map(data, function(obj) {
                                                    return { id: obj.FULLNAME, text: obj.FULLNAME };
                                                }))
                                            };
                                        }
                                    '),
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-4" style="position: relative;">
                            <?= $form->field($model, 'patentDate')->textInput([
                                'maxlength' => true,
                                'class' => 'form-control date-picker ico',
                                'autocomplete' => 'off',
                                'value' => ($taxation->psn) ? $model->patentDate : null,
                            ])->label('Дата начала действия патента') ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'patentDateEnd')->textInput([
                                'maxlength' => true,
                                'class' => 'form-control input-sm date-picker ico',
                                'autocomplete' => 'off',
                                'value' => ($taxation->psn) ? $model->patentDateEnd : null,
                            ])->label('Дата окончания действия патента') ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="message-robot-disabled-by-usn_type" style="display:<?=($taxation->usn && $taxation->usn_type == 1) ? 'block':'none'?>">
                <div class="form-group">
                    <p style="color:red">1. Для системы налогообложения «Доходы минус Расходы» мы не сможем сделать автоматический расчет налогов и подготовку налоговой декларации.</p>
                    <p>2. Напишите нам на почту support@kub-24.ru и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. <br/>Они рассчитают вам стоимость ведения бухгалтерии. От нас вы получите скидку до 20% на их услуги.</p>
                    <p>3. Наш сервис помогает предпринимателям</p>
                    <ul>
                        <li>Выставлять счета, акты ,товарные накладные</li>
                        <li>Контролировать должников</li>
                        <li>Вести управленческий учет</li>
                        <li>Видеть финансовую аналитику по бизнесу</li>
                        <li>Учитывать товар</li>
                    </ul>
                </div>
            </div>

            <div id="message-robot-enabled" style="display:<?=(!$taxation->usn || ($taxation->usn && $taxation->usn_type == 0)) ? 'block':'none'?>">
                <strong class="title-small d-block pb-3 mb-1">
                    Необходимо проверить все пункты ниже,
                    для правильного рассчета налога
                    и только потом переходить на следующий шаг.
                    Подтвердитье, что у вашего ИП:
                </strong>
                <div>
                    <?php foreach ($questions as $key => $value) : ?>
                        <div class="checkbox d-block mb-3">
                            <?= Html::checkbox("{$key}_checkbox", true, [
                                'id' => "{$key}_checkbox",
                                'uncheck' => 0,
                                'label' => $value,
                                'labelOptions' => [
                                    'style' => 'width: 90%;',
                                ],
                                'class' => 'questions-checkboxes',
                                'data-message' => "{$key}_message",
                            ]) ?>
                            <div style="padding-left:30px">
                                <?= isset($questions_messages[$key]) ? $questions_messages[$key] : ''; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap wrap_btns fixed mb-0">
        <div class="row justify-content-between align-items-center">
            <div class="column">
                <?= Html::a('Назад', ['company'], [
                    'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                ]) ?>
            </div>
            <div class="column ml-auto text-grey text_size_12 line-height-1-5 mr-1">
                Нажимая на кнопку “Подтвердить”, вы подтверждаете <br> данные по вашему ИП для расчета налога
            </div>
            <div class="column">
                <?= Html::submitButton(Icon::get('check-double', [
                    'class' => 'svg-icon mr-2',
                ]).'<span class="ml-1">Подтвердить</span>', [
                    'id' => 'ip-params-submit',
                    'class' => 'button-clr button-regular button-regular_red pr-3 pl-3 width-160',
                ]); ?>
            </div>
        </div>
    </div>
<?php $form->end(); ?>

<?= $this->render('tax-fail', [
    'company' => $model,
]) ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_osno" style="display: inline-block; text-align: center;">
        Для ОСНО мы не сможем вам рассчитать налоги<br/> и подготовить декларацию
    </span>
</div>

<?php $this->registerJs(<<<JS
    // Система налогообложения
    function resetDisabledCheckboxes() {
        $('.taxation-system').find('.form-group').removeClass('has-success').removeClass('has-error');
    }
    function submitButtonCanClickCss(can) {
        $('#ip-params-submit').prop('disabled', !can)
            .toggleClass('button-regular_red', can)
            .toggleClass('button-hover-transparent', !can);
    }

    $(document).on("change", "#companytaxationtype-osno", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true);
            $(".nds-view-osno", form).collapse("show");
        } else {
            $("#companytaxationtype-usn", form).prop("disabled", false);
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-usn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true);
            $(".tax-usn-config", form).collapse("show");
        } else {
            $("#companytaxationtype-osno", form).prop("disabled", false);
            $(".tax-usn-config", form).collapse("hide");
            $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#companytaxationtype-usn_percent", form).val("");
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true);
            $(".tax-patent-config", form).collapse("show");
        } else {
            $("#companytaxationtype-envd", form).prop("disabled", false);
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes();
    });

    // Подсказки
    $('.tax-usn-config input[type=radio]').change(function() {
        if (this.value == 1) {
            submitButtonCanClickCss(false);
            $('#message-robot-disabled-by-usn_type').show(250);
            $('#message-robot-enabled').hide(250);
            $('#taxrobot-start-modal').modal('show');
        } else {
            submitButtonCanClickCss(true);
            $('#message-robot-disabled-by-usn_type').hide(250);
            $('#message-robot-enabled').show(250);
        }
    });
    $('.questions-checkboxes').change(function() {
       var msg_id = $(this).attr('data-message');
       if ($(this).prop('checked'))
           $('#'+msg_id).hide(250);
       else
           $('#'+msg_id).show(250);

       var disabled_next_steps = false;
       $('.questions-checkboxes').each(function() {
           if (!$(this).prop('checked') && !($(this).attr('name') == 'no_cashbox_checkbox')) {
               disabled_next_steps = true;
               return false;
           }
       });

       if (disabled_next_steps) {
           submitButtonCanClickCss(false);
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').addClass('disabled');
       } else {
           submitButtonCanClickCss(true);
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').removeClass('disabled');
       }

    });
    $(document).on("click", ".sbs-el", function(e) {
          e.preventDefault();
          if ($(this).hasClass('disabled'))
            return false;
          else
            window.location.href = $(this).attr('href');
    });

    $(document).ready(function() {
        $("#ip-params-submit").parents(".button-pulsate").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    });


JS
)?>
