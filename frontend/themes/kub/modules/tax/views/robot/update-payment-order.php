<?php

use common\components\date\DateHelper;
use common\components\TaxRobotHelper;
use common\components\widgets\BikTypeahead;
use common\models\cash\CashBankFlows;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\OperationType;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $form yii\bootstrap4\ActiveForm */

//$this->title = 'Обновить платёжное поручение: ' . ' ' . $model->document_number;
$this->params['breadcrumbs'][] = ['label' => 'Payment Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$rsItems = [];
$rsOptions = [];
$accountsArray = $model->company->getCheckingAccountants()
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->all();

foreach ($accountsArray as $account) {
    $rsItems[$account->rs] = $account->rs;
    $rsOptions[$account->rs] = [
        'data' => [
            'bank' => $account->bank_name,
            'city' => $account->bank_city,
            'bik' => $account->bik,
            'ks' => $account->ks,
        ]
    ];
}
?>

<style type="text/css">
    #connection-to-account {
        display: none;
    }
</style>

<div class="tax-robot-update-payment-order">
    <div class="p-2">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>

    <?= $this->render('@frontend/modules/documents/views/payment-order/_form', [
        'model' => $model,
        'company' => Yii::$app->user->identity->company,
        'cancellUrl' => [
            'view-payment-order',
            'id' => $model->id,
            'period' => $taxRobot->getUrlPeriodId(),
        ],
    ]) ?>
</div>

<?php $this->registerJs('
    $(document).on("change", "#paymentorder-company_rs", function () {
        var form = this.form;
        var rs = $(":selected", this);
        if ($(this).val() && rs) {
            $("#paymentorder-company_bank_name", form).val(rs.data("bank"));
            $("#paymentorder-company_bank_city", form).val(rs.data("city"));
            $("#paymentorder-company_bik", form).val(rs.data("bik"));
            $("#paymentorder-company_ks", form).val(rs.data("ks"));
        }
    });
'); ?>
