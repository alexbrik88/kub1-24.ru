<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\themes\kub\modules\documents\widgets\PaymentOrderPaid;
use frontend\rbac\permissions;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $model2 common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $searchModel frontend\modules\tax\models\TaxrobotNeedPaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платеж по налогу УСН';
$this->params['step'] = 5;

$sum = [
    'Usn6' => [],
    'Oms' => [],
    'Pfr' => [],
    'Over300' => [],
];
foreach ($dataProvider->getModels() as $model) {
    $type = $taxRobot->getPaymentTypeByPaymentOrder($model);
    $period = $taxRobot->getPeriodLabelByCode($model->tax_period_code, true);
    if (isset($sum[$type][$period])) {
        $sum[$type][$period] += $model->sum;
    } else {
        $sum[$type][$period] = $model->sum;
    }
}
$usn = ArrayHelper::remove($sum, 'Usn6');
$sumTax = array_sum($sum['Oms']) + array_sum($sum['Pfr']) + array_sum($sum['Over300']);
$sumUsn6 = array_sum($usn);

$urlPeriod = $taxRobot->getUrlPeriodId();

$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);

$bankingRoute = ['/tax/robot/payment'];
if (($urlPeriod = $taxRobot->getUrlPeriodId()) !== null) {
    $bankingRoute['period'] = $urlPeriod;
}
$fixedPayments = $taxRobot->getFixedPayments($taxRobot->getYear());
$nextCss = $taxRobot->getIsDeclarationPaid() ? '' : ' taxrobot-pay-panel-trigger';
?>

<div class="tax-robot-payment">

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-payment-pjax',
    'enablePushState' => false
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="d-flex justify-content-between align-items-start">
            <h4 class="mb-2">
                <span>Платежи по взносам и налогу УСН за</span>
                <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
                    <?= Html::a(Html::tag('span', $taxRobot->period->label) . Icon::get('shevron'), '#', [
                        'id' > 'cardProductTitle',
                        'class' => 'link link_title',
                        'role' => 'button',
                        'data-toggle' => 'dropdown',
                    ])?>
                    <div class="dropdown-menu keep-open" aria-labelledby="cardProductTitle">
                        <div class="popup-dropdown-in overflow-hidden">
                            <ul class="form-filter-list list-clr">
                                <?php foreach ($taxRobot->getPeriodDropdownItems('payment') as $key => $item) : ?>
                                    <li style="white-space: nowrap;">
                                        <?= Html::a($item['label'], $item['url']) ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </h4>
            <div>
                <?= $this->render('_video_instruction') ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap p-4 mb-2">
    <div class="p-2">
        <div class="row">
            <div class="col column-61">
                <h4 class="text_size_20 mb-4">
                    <span class="mr-1">Небоходимо оплатить</span>
                </h4>
                <div class="mb-4">
                    Если в списке есть платежи, которые вы уже оплатили, то либо
                    <?= Html::a('загрузите выписку', [
                        '/cash/banking/default/index',
                        'p' => Banking::routeEncode($bankingRoute),
                    ], [
                        'class' => 'banking-module-open-link',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>,
                    чтобы оплата налогов проставилась автоматически,
                    либо поставьте напротив платёжного поручения галочку и с помощью кнопки "Оплачено"
                    измените статус поручания на "Оплачено"
                </div>
                <div>
                    <button class="tax-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center justify-content-between mb-2" type="button" data-toggle="collapse" data-target="#collapseItem2-1" aria-expanded="true" aria-controls="collapseItem2-1">
                        <span class="title-small mr-3 d-block">Страховые взносы за себя</span>
                        <strong><?= number_format($sumTax/ 100, 2, '.', ' ') ?> Р</strong>
                    </button>
                    <div id="collapseItem2-1" class="collapse show">
                        <div class="tax-collapse pt-2 pb-1">
                            <?php foreach ($sum as $type => $data) : ?>
                                <?php foreach ($data as $period => $amount) : ?>
                                    <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                        <div class="mr-3"><?= $taxRobot->{"get{$type}Title"}($period) ?></div>
                                        <div><?= number_format($amount/100, 2, '.', ' ') ?> Р</div>
                                    </div>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div>
                    <button class="tax-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center justify-content-between mb-2 collapsed"  type="button" data-toggle="collapse" data-target="#collapseItem2-2" aria-expanded="false" aria-controls="collapseItem2-2">
                        <span class="title-small mr-3 d-block">Платежи по налогу УСН Доходы</span>
                        <strong><?= number_format($sumUsn6 / 100, 2, '.', ' ') ?> P</strong>
                    </button>
                    <div id="collapseItem2-2" class="collapse">
                        <div class="tax-collapse pt-2 pb-1">
                            <?php foreach ($usn as $period => $amount) : ?>
                                <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                    <div class="mr-3"><?= $taxRobot->getUsn6Title($period) ?></div>
                                    <div><?= number_format($amount/100, 2, '.', ' ') ?> Р</div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col column-39 pl-4">
                <div style="padding: 5px 10px; border-radius: 4px; border: solid 2px #e2e5eb;">
                    <strong class="small-title mb-2 d-block">
                        <div class="text_size_16 mb-2">Фиксированные взносы ИП</div>
                    </strong>
                    <div class="mb-2">
                        <div class="mb-1">
                            Каждый ИП должен заплатить в <?= $taxRobot->getYear() ?> г. фиксированные взносы:
                            <br>
                            В ПФР (пенсионное страхование) - <?= number_format($fixedPayments->pfr/100, 2, '.', ' ') ?> P
                            <br>
                            В ОМС (медицинское страхование) - <?= number_format($fixedPayments->oms/100, 2, '.', ' ') ?> P
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="mb-1">
                            Если ваше ИП зарегистрировано в <?= $taxRobot->getYear() ?> г.,
                            то вы должны заплатить не всю сумму фиксированных взносов, а меньшую сумму,
                            т.к. суммы выше рассчитаны за все дни года.
                        </div>
                    </div>
                    <div class="mb-2">
                        <strong>Сроки уплаты взносов</strong>
                    </div>
                    <div class="mb-2">
                        <div class="mb-1">
                            Суммы фиксированных взносов за <?= $taxRobot->getYear() ?> год,
                            нужно перечислить в бюджет не позднее 31.12.<?= $taxRobot->getYear() ?>.
                        </div>
                    </div>
                    <div class="mb-2">
                        <strong>Важно!</strong>
                    </div>
                    <div class="">
                        <div class="mb-1">
                            Данные платежи уменьшают налог УСН Доходы. Поэтому оплачивая их в начале года, вы сможете уменьшить УСН Доходы и точно не переплатите в бюджет.
                            <br>
                            Также можно разделить суммы на 4 и платить поквартально:
                            <br>
                            В ПФР – <?= number_format($fixedPayments->pfr/100/4, 2, '.', ' ') ?> P в квартал
                            <br>
                            В ОМС – <?= number_format($fixedPayments->oms/100/4, 2, '.', ' ') ?> P в квартал
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pt-1 pb-1 mb-2 d-flex flex-wrap">
    <div class="text_size_20" style="align-self: center;">Список платежных поручений</div>
</div>

<div class="wrap wrap_padding_none">
    <?= common\components\grid\GridView::widget([
        'id' => 'payment-order-search',
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'emptyText' => 'Вы еще не создали ни одного платежного поручения.',
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'layout' => "{items}",
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::checkbox('PaymentOrder[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data' => [
                            'income' => 0,
                            'expense' => bcdiv($model->sum, 100, 2),
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата',
                'headerOptions' => [
                ],
                'value' => function ($model) {
                    return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => '№',
                'headerOptions' => [
                ],
                'format' => 'raw',
                'value' => function ($model) use ($urlPeriod) {
                    return Html::a($model->document_number, [
                        'view-payment-order',
                        'id' => $model->id,
                        'period' => $urlPeriod,
                        'part' => 'unpaid',
                    ], [
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]);
                },
            ],
            [
                'label' => 'Сумма',
                'headerOptions' => [
                    'width' => '9%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'format' => 'html',
                'attribute' => 'sum',
                'value' => function ($model) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($model->sum, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'contractor_name',
                'label' => 'Контрагент',
                'enableSorting' => false,
                'format' => 'html',
                'filter' => $searchModel->getContractorFilterItems(),
                's2width' => '300px',
                'value' => function ($model) {
                    return '<span title="' . Html::encode($model->contractor_name) . '">' . $model->contractor_name . '</span>';
                },
            ],
            [
                'attribute' => 'purpose_of_payment',
                'label' => 'Назначение',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'filter' => $searchModel->getStatusFilterItems(),
                's2width' => '200px',
                'value' => 'paymentOrderStatus.name',
            ],
            [
                'attribute' => 'expenditure_id',
                'label' => 'Статья',
                'filter' => $searchModel->getExpenditureFilterItems(),
                's2width' => '300px',
                'value' => 'expenditureItem.name',
            ],
        ],
    ]) ?>
</div>

<?php /* echo common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('/layouts/grid/pager', ['totalCount' => $dataProvider->totalCount]),
    'columns' => ['id'],
]); */ ?>

<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', [
                'calculation',
                'period' => $taxRobot->getUrlPeriodId(),
            ], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Далее', [
                'declaration',
                'period' => $taxRobot->getUrlPeriodId(),
            ], [
                'id' => 'submit',
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160'.$nextCss,
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('_pay_extra_modal', [
    'taxRobot' => $taxRobot,
]); ?>

<?= \frontend\modules\cash\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a(Icon::get('print') . ' <span>Печать</span>', [
            '/documents/payment-order/many-document-print',
            'actionType' => 'pdf',
            'type' => Documents::IO_TYPE_IN,
            'multiple' => '',
        ], [
            'class' => 'button-clr button-regular button-hover-transparent multiple-print',
            'target' => '_blank',
            'data-pjax' => 0,
        ]) : null,
        $canDelete ? Html::a(Icon::get('garbage') . ' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canPrint ? Html::a('Экспорт в банк', ['/documents/payment-order/import'], [
            'class' => 'button-clr button-regular button-hover-transparent multiple-import-link no-ajax-loading',
            'data-url' => Url::to(['/documents/payment-order/import']),
            'data-pjax' => 0,
        ]) : null,
        $canStatus ? PaymentOrderPaid::widget([
            'id' => 'many-paid',
            'closeButton' => [
                'label' => Icon::get('close'),
                'class' => 'modal-close close',
            ],
            'toggleButton' => [
                'tag' => 'a',
                'label' => Icon::get('check-double') . ' <span>Оплачено</span>',
                'class' => 'button-clr button-regular button-hover-transparent',
                'data-pjax' => 0,
            ],
            'pjaxContainer' => '#tax-robot-payment-pjax',
        ]) : null,
    ],
]); ?>

<?php if (Yii::$app->request->isPjax): ?>
    <script>
        // refresh tab title
        $('.tax-robot .menu-nav-tabs .tab-period-label').html('Доходы за <br/><?= addslashes($taxRobot->getPeriod()->shortLabel) ?>');
    </script>
<?php endif; ?>

<?php \yii\widgets\Pjax::end(); ?>

<?= BankingModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to(['robot/payment', 'period' => $urlPeriod]),
]) ?>

<?php if ($canDelete) : ?>
    <?= $this->render('_many_delete', [
        'text' => '
            Вы уверены, что хотите удалить выбранные платежные поручения?
            <br>
            <span style="font-size: 20px;">
                Платежные поручения после удаления обновятся и создадутся заново,
                <br>
                т.к. их необходимо оплатить, чтобы у вас не было штрафов и пеней от налоговой.
            </span>
        '
    ]); ?>
<?php endif ?>

</div>

<?php $this->registerJs(<<<JS
$(document).on('click', '.my-dropdown', function() {
    var inner = $(this).find('.my-dropdown-body');
    if ($(inner).is(':hidden'))
        $(inner).show(250);
    else
        $(inner).hide(250);
});
JS
)?>
