<?php

use common\models\document\status\TaxDeclarationStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\bootstrap4\Dropdown;
use yii\helpers\Url;

/** @var \frontend\modules\tax\models\TaxDeclaration $model */
/** @var \frontend\modules\tax\models\Kudir $kudir */
/** @var \common\models\Company $company */
/** @var $isEmptyDeclaration boolean */

$this->title = 'Налоговая декларация';
$this->params['step'] = 6;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$taxation = $company->companyTaxationType;
$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-declaration-pjax',
    'enablePushState' => false
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h3 class="mb-2" style="display: flex;">
                    <span class="pr-2">Налоговая декларация за</span>
                    <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
                        <?= Html::a(Html::tag('span', $taxRobot->period->label) . Icon::get('shevron'), '#', [
                            'id' > 'cardProductTitle',
                            'class' => 'link link_title',
                            'role' => 'button',
                            'data-toggle' => 'dropdown',
                        ])?>
                        <div class="dropdown-menu keep-open" aria-labelledby="cardProductTitle">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <?php foreach ($taxRobot->getPeriodDropdownItems('declaration') as $key => $item) : ?>
                                        <li style="white-space: nowrap;">
                                            <?= Html::a($item['label'], $item['url']) ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php if (!preg_match('/^[0-9]{4}_4$/', $taxRobot->period->id)) : ?>
                        <div style="padding: 10px 0 0 20px; font-size:13px; font-style: italic; color: red; line-height: 1;">
                            ВНИМАНИЕ! Декларация сдается<br>ТОЛЬКО за календарный год.
                        </div>
                    <?php endif ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <div class="doc-container">
                    <?= $this->render('parts_declaration/view', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('parts_declaration/viewStatus', [
                'model' => $model,
                'canUpdate' => $canUpdate,
                'canUpdateStatus' => $canUpdateStatus,
                'sumTaxToPay' => $sumTaxToPay,
                'isEmptyDeclaration' => $isEmptyDeclaration,
                'period' => $taxRobot->getUrlPeriodId()
            ]); ?>
        </div>
    </div>
</div>

<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', [($isEmptyDeclaration) ? 'bank' : 'payment', 'period' => $taxRobot->getUrlPeriodId()], [
                'class' => 'button-clr button-regular button-hover-transparent width-160 pr-3 pl-3',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a(Icon::get('print', ['class' => 'mr-1']) . ' <span class="ml-2">Печать</span>', [
                'declaration-print',
                'actionType' => 'print',
                'id' => $model->id,
                'empty' => $isEmptyDeclaration,
                'filename' => $model->getPrintTitle(),
            ], [
                'class' => 'button-regular button-hover-transparent',
                'target' => '_blank',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        </div>
        <div class="column">
            <span class="dropup">
                <?= Html::a(Icon::get('download', ['class' => 'mr-1']) . '<span class="ml-2">Скачать</span>', '#', [
                    'class' => 'button-regular button-hover-transparent',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= \yii\bootstrap4\Dropdown::widget([
                    'items' => [
                        [
                            'label' => 'Скачать PDF файл',
                            'encode' => false,
                            'url' => [
                                'declaration-print',
                                'actionType' => 'pdf',
                                'id' => $model->id,
                                'empty' => $isEmptyDeclaration,
                                'filename' => $model->getPdfFileName(),
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                                'data-pjax' => 0
                            ]
                        ],
                        [
                            'label' => 'Скачать файл для ИФНС',
                            'encode' => false,
                            'url' => [
                                '/tax/declaration/xml',
                                'id' => $model->id,
                                'empty' => $isEmptyDeclaration
                            ],
                            'linkOptions' => [
                                'download' => 'download',
                                'data-pjax' => 0
                            ]
                        ],
                    ],
                ]); ?>
            </span>
        </div>
        <div class="column mr-auto">
            <?= Html::a(Icon::get('archiev', ['class' => 'mr-1']) . '<span class="ml-2">Книга учёта доходов и расходов</span>',
                (!$taxRobot->isPaid) ? '#' : ['/tax/kudir/' . ($taxation->psn ? 'document-print-patent' : 'document-print'),
                    'actionType' => 'pdf',
                    'id' => $kudir->id,
                    'filename' => $kudir->getPdfFileName(),
                    'period' => $taxRobot->period->id
                ], [
                'class' => 'button-regular button-hover-transparent tooltip-hover nowrap',
                'data-tooltip-content' => '#tooltip_kudir',
                'data' => [
                    'pjax' => '0',
                ],
                'target' => '_blank',
                'disabled' => (!$taxRobot->isPaid) ? true : false
            ]); ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->request->isPjax): ?>
    <script>
        // refresh tab title
        $('.tax-robot .menu-nav-tabs .tab-period-label').html('Доходы за <br/><?= addslashes($taxRobot->getPeriod()->shortLabel) ?>');
    </script>
<?php endif; ?>

<?php \yii\widgets\Pjax::end(); ?>

<div class="tooltip-template hidden">
    <span class="tooltip-kudir-body text-top" id="tooltip_kudir">
        КУДиР не нужно отправлять в налоговую, но следует хранить 4 года и <br/>предоставлять по требованию налоговой и других контролирующих органов.<br/>
        Поэтому распечатайте её сейчас либо вы это можете сделать позже.
    </span>
</div>

<?= $this->registerJs('
    $(document).ready(function (e) {
        var $left = Math.ceil((+$(".dropdown-centerjs").width() - +$(".dropdown-linkjs:visible").width()) / 2);
        console.log($left);
        $(".dropdown-centerjs").css("left", "-" + $left + "px");
    });
'); ?>
