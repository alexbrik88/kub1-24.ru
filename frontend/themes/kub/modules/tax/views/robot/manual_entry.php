<?php

use common\components\widgets\BikTypeahead;
use frontend\modules\tax\models\ManualEntryForm;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this  yii\web\View */
/* @var $model  frontend\modules\tax\models\ManualEntryForm */

$company = Yii::$app->user->identity->company;
$flowsArray = $model->getFlowsArray();
?>

<style type="text/css">
#manual_entry_form .form-control.is-valid[value=""] {
    background-image: none;
    border-color: #e2e5eb;
}
</style>
<?php Modal::begin([
    'id' => 'manual_entry_modal',
    'title' => "Укажите суммы доходов и уплаченные налоги за {$model->year} год",
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <div class="mb-4">
        Чтобы правильно рассчитать налоги и подготовить декларацию,
        вам нужно указать суммы полученных доходов по каждому кварталу.
        Так же укажите платежи по налогам, <strong>оплаченные в этих кварталах</strong>,
        даже если они начислены за предыдущий квартал или год.
        Т.е. указываем только оплаченные налоги!
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'manual_entry_form',
        'action' => ['manual-entry', 'year' => $model->year],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

        <table>
            <thead>
                <tr>
                    <th></th>
                    <th class="pb-2 px-1">в 1-м квартале</th>
                    <th class="pb-2 px-1">во 2-м квартале</th>
                    <th class="pb-2 px-1">в 3-м квартале</th>
                    <th class="pb-2 px-1">в 4-м квартале</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (ManualEntryForm::$attributes as $attribute) : ?>
                    <tr>
                        <th style="vertical-align: top;">
                            <?= $model->getAttributeLabel($attribute) ?>
                        </th>
                        <?php foreach (ManualEntryForm::$quarters as $quarter) : ?>
                            <td style="vertical-align: top;">
                                <?= $form->field($model, "{$attribute}_{$quarter}", [
                                    'template' => "{input}\n{error}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->textInput([
                                    'value' => (string) $model->{"{$attribute}_{$quarter}"},
                                    'data-value' => (string) $model->{"{$attribute}_{$quarter}"},
                                    'onchange' => "this.setAttribute('value', this.value);",
                                ]) ?>
                            </td>
                        <?php endforeach ?>
                    </tr>
                <?php endforeach ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <td colspan="4">
                        <?= $form->field($model, 'empty_fields')->label(false)->hiddenInput() ?>
                    </td>
                </tr>
            </tfoot>
        </table>

        <?php if (!$company->mainCheckingAccountant) : ?>
            <div id="manual_entry_account" class="collapse">
                <strong class="my-2">
                    Для подготовки платёжных поручений по налогам, введите банковские реквизиты
                </strong>
                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'rs')->textInput([
                            'maxlength' => true,
                        ]); ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'bik')->widget(BikTypeahead::classname(), [
                            'remoteUrl' => Url::to(['/dictionary/bik']),
                            'related' => [
                                '#' . Html::getInputId($model, 'bank_name') => 'name',
                                '#' . Html::getInputId($model, 'bank_city') => 'city',
                                '#' . Html::getInputId($model, 'ks') => 'ks',
                            ],
                        ])->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'bank_name')->textInput([
                            'disabled' => true,
                        ]); ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'ks')->textInput([
                            'disabled' => true,
                        ]); ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'bank_city')->textInput([
                            'disabled' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <div class="row mt-5">
            <div class="col">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-regular_red min-w-130 ladda-button',
                ]) ?>
            </div>
            <div class="col text-right">
                <?= Html::button('Отменить', [
                    'class' => 'button-regular button-regular_red min-w-130',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        </div>

    <?php $form->end(); ?>

<?php Modal::end(); ?>

<?php $this->registerJs(<<<JS
    $("#manual_entry_modal").on("hidden.bs.modal", function(e) {
        $("input[type=text]", this).each(function() {
            this.value = $(this).data("value") || "";
        });
        $(".validating", this).removeClass("validating");
        $(".is-valid", this).removeClass("is-valid");
        $(".is-invalid", this).removeClass("is-invalid");
        $(".invalid-feedback", this).html("");
    });
    $("#manual_entry_form").on("submit", function (e) {
        if ($("#manual_entry_account").length && !$("#manual_entry_account.show").length) {
            $("#manual_entry_account").collapse("show");
            return false;
        }
        return true;
    });
JS
); ?>