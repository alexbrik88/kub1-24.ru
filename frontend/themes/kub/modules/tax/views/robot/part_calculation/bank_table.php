<?php
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\Company;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\rbac\permissions;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Dropdown;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\widgets\RobotCheckingAccountantFilterWidget;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashBankFlows
 * @var $company Company
 */

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();
$contractorList = $taxRobot->getContractorList($taxRobot->getExpenseSearchQuery());
$taxItems = ArrayHelper::map($taxRobot->getTaxItems(), 'id', 'name');
?>

<div class="row">
    <div class="col-md-3 col-sm-3"></div>
    <div class="col-md-3 col-sm-3"></div>
    <div class="col-md-6 col-sm-6"></div>
</div>

<div class="row">
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption" style="padding-top: 10px;">
            Расходы уменьшающие налог на УСН
        </div>
        <div class="dropdown pull-right" style="margin-top: 4px;">
            <?= Html::tag('span', '<i class="glyphicon glyphicon-plus-sign"></i> Добавить расход, уменьшающий налог ' . Html::tag('i', '', [
                'class' => 'fa fa-chevron-down',
                'style' => 'font-size: 12px;',
            ]), [
                'class' => 'btn darkblue darkblue-invert',
                'style' => 'margin-top:0;margin-bottom:2px;height:33px',
                'data-toggle' => 'dropdown',
            ]) ?>
            <?= Dropdown::widget([
                'id' => 'employee-rating-dropdown',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => 'Добавить по банку',
                        'url' => [
                            '/cash/bank/create',
                            'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                            'redirect' => Url::to(['/tax/robot/calculation', 'period' => $taxRobot->getUrlPeriodId()]),
                            'skipDate' => 1,
                            'canAddContractor' => 1,
                            'onlyFNS' => 1
                        ],
                        'linkOptions' => [
                            'style' => 'padding: 8px 12px;',
                            'class' => 'update-movement-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]
                    ],
                    [
                        'label' => 'Добавить по кассе',
                        'url' => [
                            '/cash/order/create',
                            'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                            'redirect' => Url::to(['/tax/robot/calculation', 'period' => $taxRobot->getUrlPeriodId()]),
                            'skipDate' => 1,
                            'id' => $cashbox->id,
                            'canAddContractor' => 1,
                            'onlyFNS' => 1
                        ],
                        'visible' => (boolean) $cashbox,
                        'linkOptions' => [
                            'style' => 'padding: 8px 12px;',
                            'class' => 'update-movement-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]
                    ],
                ],
                'options' => [
                    'style' => 'width: 100%; margin-top: 0;'
                ]
            ]) ?>
        </div>
        <div class="actions joint-operations col-md-3 col-sm-3"
             style="display:none; width: 220px;">
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>

                <?= $this->render('modal-many-delete'); ?>
            <?php endif ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $taxRobot,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'rowOptions' => function ($data, $key, $index, $grid) {
                    return [
                        'class' => 'flow-row',
                        'data' => [
                            'update-url' => Url::to(['update-movement', 'type' => $data['payment_type'], 'id' => $data['id']]),
                        ]
                    ];
                },
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center no-update-modal',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::checkbox('flowId[]', false, [
                                'class' => 'joint-operation-checkbox',
                                'value' => $data['payment_type'] . '_' . $data['id'],
                                'data' => [
                                    'income' => 0,
                                    'expense' => bcdiv($data['amount'], 100, 2),
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'amountExpense',
                        'label' => 'Расход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'value' => function ($data) {
                            return moneyFormat($data['amount']);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'width' => '12%',
                            'class' => 'nowrap-normal max10list',
                        ],
                        'format' => 'raw',
                        'filter' => ['' => 'Все'] + $contractorList,
                        'value' => function ($data) use ($contractorList) {
                            return ArrayHelper::getValue($contractorList, $data['contractor_id'], $data['contractor_id']);
                        }
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '30%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data['description']) {
                                $description = mb_substr($data['description'], 0, 50) . '<br>';
                                $description .= mb_substr($data['description'], 50, 50);

                                return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                            }

                            return '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'expenditure_item_id',
                        'label' => 'Статья',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'filter' => ['' => 'Все статьи'] + $taxItems,
                        'format' => 'raw',
                        'value' => function ($data) use ($taxItems) {
                            return ArrayHelper::getValue($taxItems, $data['expenditure_item_id'], '-');
                        },
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'controller' => 'cash/bank',
                        'template' => '{update-movement}<br>{delete}',
                        'headerOptions' => [
                            'width' => '3%',
                        ],
                        'contentOptions' => [
                            'class' => 'no-update-modal',
                        ],
                        'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                        'buttons' => [
                            'update-movement' => function ($url, $model) {
                                $options = [
                                    'title' => 'Изменить',
                                    'class' => 'update-movement-link',
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#update-movement-modal',
                                    ],
                                ];

                                return Html::a("<span aria-hidden='true' class='icon-pencil'></span>", $url, $options);
                            },
                            'delete' => function ($url, $model) {
                                return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                        'class' => '',
                                        'tag' => 'a',
                                    ],
                                    'confirmUrl' => $url,
                                    'confirmParams' => ['flowId' => $model['payment_type'] . '_' . $model['id']],
                                    'message' => 'Вы уверены, что хотите удалить операцию?',
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return Url::to(['/tax/robot/' . $action, 'type' => $model['payment_type'], 'id' => $model['id']]);
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
</div>

<div class="modal fade t-p-f modal_scroll_center" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>