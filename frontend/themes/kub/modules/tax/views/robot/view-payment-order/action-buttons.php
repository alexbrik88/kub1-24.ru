<?php

use common\components\ImageHelper;
use common\models\document\status\PaymentOrderStatus;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\modules\documents\widgets\PaymentOrderPaid;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $needPay boolean */

?>

<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['payment', 'period' => $urlPeriod], [
                'class' => 'button-clr button-regular button-hover-transparent width-120 pr-3 pl-3',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Импорт в файл', [
                '/documents/payment-order/import',
                'id' => $model->id,
            ], [
                'class' => 'button-regular button-hover-transparent width-120',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a(Icon::get('print', ['class' => 'mr-2']).'Печать', [
                '/documents/payment-order/document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => Documents::IO_TYPE_IN,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-regular button-hover-transparent width-120 no-reload-status print',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a(Icon::get('pdf', ['class' => 'mr-2']).'PDF', [
                '/documents/payment-order/document-print',
                'actionType' => 'pdf',
                'id' => $model->id,
                'type' => Documents::IO_TYPE_IN,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-regular button-hover-transparent width-120',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
        <?php if ($model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID) : ?>
            <div class="column">
                <?= PaymentOrderPaid::widget([
                    'id' => 'many-paid',
                    'title' => 'Платежное поручение оплачено',
                    'modelId' => $model->id,
                    'pjaxContainer' => null,
                    'closeButton' => [
                        'label' => Icon::get('close'),
                        'class' => 'modal-close close',
                    ],
                    'toggleButton' => [
                        'label' => Icon::get('check-double') . ' <span>Оплачено</span>',
                        'class' => 'button-clr button-regular button-hover-transparent',
                        'data-pjax' => 0,
                    ],
                ]) ?>
            </div>
        <?php endif ?>
        <div class="column mr-auto nowrap">
            <?php if ($needPay &&
                $model->payment_order_status_id != PaymentOrderStatus::STATUS_SENT_TO_BANK &&
                (Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
                ($paymentAccount = $model->company->getBankingPaymentAccountants()->one()) !== null &&
                ($alias = Banking::aliasByBik($paymentAccount->bik)) &&
                ($bank = Banking::getBankByBik($paymentAccount->bik))
            ) : ?>
                <?php $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                    'class' => 'mr-2',
                    'style' => 'display: inline-block;',
                ]);?>
                <?= Html::a($image.'<span>Отправить в ' . $bank->bank_name.'</span>', [
                    "/cash/banking/{$alias}/default/payment",
                    'account_id' => $paymentAccount->id,
                    'po_id' => $model->id,
                    'p' => Banking::routeEncode(['/documents/payment-order/view', 'id' => $model->id]),
                ], [
                    'class' => 'link banking-module-open-link',
                    'data' => [
                        'pjax' => '0',
                    ],
                    'style' => ''
                ]); ?>
                <?= BankingModalWidget::widget([
                    'pageTitle' => $this->title,
                    'pageUrl' => Url::to(['robot/payment', 'period' => $urlPeriod]),
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
