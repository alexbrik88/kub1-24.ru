<?php

use common\components\TaxRobotHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */

$this->params['step'] = 5;

$year = mb_substr($model->tax_period_code, -4);
if ($model->kbk == TaxRobotHelper::$kbkUsn6) {
    $this->title = $taxRobot->getUsn6Title($taxRobot->getPeriodLabelByCode($model->tax_period_code));
    $title = 'Налог УСН Доходы';
    $payment = 'налог УСН за ';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr && mb_strpos($model->purpose_of_payment, '300') !== false) {
    $this->title = $taxRobot->getOver300Title($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в размере 1% за';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr) {
    $this->title = $taxRobot->getPfrTitle($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в ПФР за';
} elseif ($model->kbk == TaxRobotHelper::$kbkOms) {
    $this->title = $taxRobot->getOmsTitle($year);
    $title = 'Взносы в ОМС';
    $payment = 'взносы в ОМС за';
} else {
    $this->title = $model->purpose_of_payment;
    $title = '';
    $payment = '';
}

$needPay = $model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID;
$urlPeriod = $taxRobot->getUrlPeriodId();
?>

<?= Html::a('Назад к списку платежек', [
    'payment',
    'period' => $urlPeriod,
], [
    'class' => 'link mb-2',
]) ?>
<style>
    .showing-1220 {display: none;}
    @media (max-width: 1220px) {
        .hidden-1220 {display: none;}
        .showing-1220 {display: table-cell;}
        .break-word {word-break: break-word;}
    }
</style>
<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">
                    <?= $this->title ?>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\kub\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update-payment-order',
                        'id' => $model->id,
                        'period' => $taxRobot->getUrlPeriodId(),
                    ], [
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 ',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>
                <?php endif; ?>
                <div class="doc-container">
                    <?= $this->render('@frontend/modules/documents/views/payment-order/_view', [
                        'model' => $model,
                        'company' => $model->company,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('view-payment-order/status-block', [
                'model' => $model,
            ]); ?>
            <?= $this->render('view-payment-order/additional-info', [
                'model' => $model,
                'title' => $title,
                'payment' => $payment,
                'taxRobot' => $taxRobot,
                'needPay' => $needPay,
                'urlPeriod' => $urlPeriod,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('view-payment-order/action-buttons', [
    'model' => $model,
    'taxRobot' => $taxRobot,
    'needPay' => $needPay,
    'urlPeriod' => $urlPeriod,
]); ?>
