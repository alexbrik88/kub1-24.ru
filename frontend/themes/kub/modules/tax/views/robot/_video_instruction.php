<?php

if (!isset($btn)) {
    $btn = [
        'class' => 'button-regular button-regular_red',
        'label' => frontend\components\Icon::get('video-instruction', ['class' => 'mr-2']).' Видео инструкция 1',
    ];
}
?>

<?= frontend\widgets\VideoModalWidget::widget([
    'modalOptiions' => [
        'title' => 'Видео инструкция 1',
        'toggleButton' => $btn,
    ],
    'videoUrl' => 'https://www.youtube.com/embed/6J30L5e5_ZU',
]) ?>
