<?php

use common\components\date\DateHelper;
use common\models\document\status\TaxDeclarationStatus;
use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration; */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-doc-date',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub t-doc-date-select'],
        'contentAsHTML' => true,
        'interactive' => true,
        'trigger' => 'custom',
        'triggerOpen' => new JsExpression('{click: true}'),
        'triggerClose' => new JsExpression('{originClick: true}'),
    ],
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass  = $status->getIcon();

$company = $model->company;
$ifns = $company->ifns;

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$pageRoute = ['/tax/robot/declaration', 'id' => $model->id, 'period' => $period];
if ($isEmptyDeclaration)
    $pageRoute['empty'] = (int)$isEmptyDeclaration;

$statusIcon = [
    TaxDeclarationStatus::STATUS_CREATED => 'check-2',
    TaxDeclarationStatus::STATUS_PRINTED => 'print',
    TaxDeclarationStatus::STATUS_SEND => 'envelope',
    TaxDeclarationStatus::STATUS_ACCEPTED => 'check-double',
    TaxDeclarationStatus::STATUS_CORRECTED => 'new-doc',
];
$statusCssClass = [
    TaxDeclarationStatus::STATUS_CREATED => 'paid',
    TaxDeclarationStatus::STATUS_PRINTED => 'paid',
    TaxDeclarationStatus::STATUS_SEND => 'paid',
    TaxDeclarationStatus::STATUS_ACCEPTED => 'paid',
    TaxDeclarationStatus::STATUS_CORRECTED => 'paid',
];
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="pl-2 pr-2 w-100">
        <div class="button-regular <?= $statusCssClass[$status->id] ?> mb-3 pl-3 pr-3 w-100">
            <?= Icon::get($statusIcon[$status->id], ['class' => 'mr-1']) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto">
                <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ? : $model->created_at) ?>
            </span>
        </div>
    </div>
</div>
<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">Налоговая декларация за</span>
        <span><?= $model->tax_year ?> год</span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Дата декларации:</span>
        <?php if ($canUpdate) : ?>
            <span id="date_internal_box" class="nowrap" style="cursor: pointer;">
                <span id="date_internal_view"
                      data-date="<?= strtotime($model->document_date) ?>"
                      data-url="<?= Url::to(['change-declaration-date', 'id' => $model->id]) ?>">
                    <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
                </span>
                <?= Html::tag('button', Icon::get('pencil'), [
                    'id' => 'date_internal_update',
                    'class' => 'button-clr link ml-1',
                ]); ?>
            </span>
            <?= Html::tag('div', null, [
                'id' => 'doc-date-select',
            ]); ?>
        <?php else : ?>
            <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
        <?php endif ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Сумма налогов за год:</span>
        <span>
            <?= \common\components\TextHelper::invoiceMoneyFormat(
                $model->taxDeclarationQuarters[3]['tax_rate'] *
                $model->taxDeclarationQuarters[3]['income_amount'],
                2
            ) ?>
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">В инспекцию:</span>
        <span>
            <?= $company->ifns_ga . ', ' . $ifns->gb ?>
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Адрес инспекции:</span>
        <span>
            <?= $ifns->getAddress() ?>
        </span>
    </div>
</div>
<div class="title-small mb-3">
    <strong class="mr-1">Способы сдать декларацию УСН</strong>
</div>
<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="d-inline-block mb-1 text-grey">1. Отрпавить по почте</span>
        <div>
            Для этого
            <?= Html::a('распечатайте декларацию', [
                '/tax/robot/declaration-print',
                'actionType' => 'pdf',
                'id' => $model->id,
                'empty' => $isEmptyDeclaration,
                'filename' => $model->getPrintTitle().'.pdf',
            ], [
                'class' => 'link',
                'target' => '_blank',
                'data-pjax' => '0',
            ]) ?>
            в 1-м экземпляре и
            <?= Html::a('опись вложения', [
                '/tax/robot/inventory-print',
                'actionType' => 'pdf',
                'id' => $model->id,
                'empty' => $isEmptyDeclaration,
                'filename' => 'Опись_'.$model->getPrintTitle().'.pdf',
            ], [
                'class' => 'link',
                'target' => '_blank',
                'data-pjax' => '0',
            ]) ?>
            в 2-х экземплярах, подпишите их, поставьте на титульном листе декларации печать (если она у вас есть).
            Отправьте отчёт ценным письмом с описью вложения на адрес инспекции.
            У вас на руках должен остаться экземпляр описи с отметкой работника почты, который подтвердит сдачу отчёта.
        </div>
    </div>
    <div class="about-card-item">
        <span class="d-inline-block mb-1 text-grey">2. Отправьте через электронную систему сдачи отчётност</span>
        <div>
            Для этого
            <?= Html::a('скачайте декларацию', [
                '/tax/declaration/xml',
                'id' => $model->id,
                'empty' => $isEmptyDeclaration,
            ], [
                'class' => 'link',
                'target' => '_blank',
                'data-pjax' => '0',
                'download' => true,
            ]) ?>
            в формате для электронной отчетности, загрузите её в вашу систему и подпишите вашей ЭЦП.
        </div>
    </div>
    <div class="about-card-item">
        <?php /* todo
        <?= Html::a('Отправить в TaxCom', [
            '/cash/ofd/taxcom/default/import',
            'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
        ], [
            'class' => 'button-regular button-regular_red w-100 mb-0 ofd-module-open-link',
        ]); ?>
        <?= \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget::widget([
            'pageTitle' => $this->title,
            'pageUrl' => Url::to($pageRoute),
        ]) ?>
        */ ?>
        <?= Html::button('Отправить в TaxCom', [
            'class' => 'button-regular button-regular_red w-100 mb-0',
            'title' => 'Данный функционал временно отключен.<br/>Обратитесь в техподдержку',
            'title-as-html' => 1,
            'disabled' => 1
        ]) ?>
    </div>
    <div class="about-card-item">
        <span class="d-inline-block mb-1 text-grey">3. Отнести отчёт в инспекцию самостоятельно</span>
        <div>
            Для этого
            <?= Html::a('распечатайте декларацию', [
                '/tax/robot/declaration-print',
                'actionType' => 'pdf',
                'id' => $model->id,
                'empty' => $isEmptyDeclaration,
                'filename' => $model->getPrintTitle().'.pdf',
            ], [
                'class' => 'link',
                'target' => '_blank',
                'data-pjax' => '0',
            ]) ?>
            в 2-х экземплярах, подпишите каждый, на титульном листе поставьте печать (если она у вас есть), и отнесите в инспекцию.
        </div>
    </div>

    <div class="about-card-item">
        <a target="_blank" href="https://taxcom.ru/about/promo/test-drive/?utm_source=kub-24_ru">
            <img class="taxcom-banner" data-toggle="modal" data-target="#taxcom_modal" src="/img/taxcom_kub_2.jpg" style="width:100%; margin-top:10px;">
        </a>
    </div>

</div>

<?php Modal::begin([
    'title' => 'ВЫГОДНАЯ ОТЧЕТНОСТЬ ДЛЯ НАШИХ ПОЛЬЗОВАТЕЛЕЙ',
    'id' => 'taxcom_modal',
]); ?>
    <div class="form-body">
        <div class="col-md-12 steps">
            <div style="text-align: center">
                До 31 декабря 2019 года подключите любой тариф отчетности в сервисе Онлайн-Спринтер от компании Такском
                <div style="padding:10px 0; font-size: 16px">
                    И ПОЛУЧИТЕ СКИДКУ 50% НА АБОНЕНТСКОЕ ОБСЛУЖИВАНИЕ НА 1 ГОД
                </div>
            </div>
            <ul style="margin-bottom: 0;">
                <li>Стоимость от 1 500 руб/год</li>
                <li>Электронная подпись уже входит в тариф</li>
                <li>Проверка деклараций перед отправкой</li>
                <li>Мобильное приложение для контроля результатов отчетности и требований от ФНС</li>
            </ul>
            <div style="padding-top:10px">
                При регистрации в поле «Комментарий» обязательно укажите промокод «КУБ».
            </div>
        </div>
        <div class="form-actions col-md-12" style="margin-top: 20px;">
            <div class="row action-buttons" style="padding-right: 15px;padding-left: 15px;">
                <div class="col-md-4" style="padding: 0;">
                    <?= Html::a('Подключить', 'https://taxcom.ru/otchetnost/', [
                        'class' => 'btn darkblue widthe-100',
                    ]); ?>
                </div>
                <div class="col-md-4 text-center" style="padding: 0;">

                </div>
                <div class="col-md-4 text-right" style="padding: 0;">
                    <?= Html::a('Отменить', 'javascript:;', [
                        'class' => 'btn darkblue widthe-100',
                        'data-dismiss' => 'modal',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php Modal::end(); ?>
<?php
$icon = Icon::get('shevron');
if ($canUpdate) {
    $this->registerJs('
        $(document).ready(function() {
            $($("#doc-date-select")).datepicker({
                startDate: new Date($("#date_internal_view").data("date") * 1000),
                prevHtml: \''.$icon.'\',
                nextHtml: \''.$icon.'\',
                inline: true,
                autoClose: true,
                onSelect: function(formattedDate, date, inst) {
                    var dd = date.getDate();
                    var mm = date.getMonth()+1;
                    var yyyy = date.getFullYear();
                    if(dd < 10) {
                        dd = "0" + dd;
                    }
                    if(mm < 10) {
                        mm = "0" + mm;
                    }
                    var dateStr = dd+"."+mm+"."+yyyy;
                    $(".tooltip-doc-date").tooltipster("close");
                    $.post($("#date_internal_view").data("url"), {newDate: dateStr}, function (data) {
                        if (data.value) {
                            $("#date_internal_view").text(data.value);
                            $(".document-date-js").text(data.value);
                        } else {
                            console.log(data);
                            window.toastr.error("Не удалось сохранить дату.", "", {
                                "closeButton": true,
                                "showDuration": 1000,
                                "hideDuration": 1000,
                                "timeOut": 5000,
                                "extendedTimeOut": 1000,
                                "escapeHtml": false,
                            });
                        }
                    });
                },
            });

            var date = $("#date_internal_view").text().split(".");
            if (date.length == 3) {
                $("#doc-date-select").data("datepicker").selectDate(new Date(date[2], date[1] - 1, date[0]));
            }

            $(document).on("click", "#date_internal_box", function () {
                var picker = $("#doc-date-select").datepicker().data("datepicker");
                if ($("#doc-date-select .datepicker.active").length) {
                    picker.hide();
                } else {
                    picker.show();
                }
            });
        });
    ');
}