<?php

use common\components\date\DateHelper;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use common\models\document\status\TaxDeclarationStatus;
use yii\bootstrap4\Nav;
use frontend\rbac\UserRole;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration; */
?>

<style>
    .preview-tax-tabs {}
    .preview-tax-tabs > .navbar {min-height:0;margin-bottom: 0;float:right;}
    .table-preview {border-spacing: 1px; border-collapse: unset;}
    .nav-tax-tabs > li {float:left;position:relative;display:block;}
    .nav-tax-tabs > li.active > a, .nav-tax-tabs > li.active > a:hover, .nav-tax-tabs > li.active > a:focus {background:#4276a4;color:#fff;}
    .nav-tax-tabs > li > a {padding: 5px 10px;}
    .table-preview {width:100%}
    .table-preview td {text-align:left}
    .table-preview td.font-size-11 {font-size:12px;}
    .table-preview td.font-main {font-size:18px;font-weight:bold;padding-bottom:0;}
    .table-preview td.border-bottom {border-bottom:1px solid #000;}
    .table-preview td.tip {padding:0 0 3px 0;}
    .table-preview td.ver-top {vertical-align:top;}
    .table-preview td.dotbox {width:16px; font-size:16pt; border:1px dotted #333;text-align:center;padding:0 2px;}
    .table-preview td.hh {line-height: 20pt;}
    .table-preview td.small-italic {font-style:italic; font-size:8pt;text-align:center;}
    .table-preview tr.middle td {vertical-align:middle;padding:4px 0;}
    .table-preview td {padding:0 5px;margin:0;}
    .table-preview td.text-center {text-align: center;}
    .table-preview td.text-right {text-align: right;}
    .preview-page {border-bottom:2px dotted #666;padding-bottom:50px;margin-bottom:50px;}
    .showing-1220 {display: none;}
    @media (max-width: 1220px) {
        .hidden-1220 {display: none;}
        .showing-1220 {display: block;}
        .table-preview td.dotbox {
            width: 14px;
            font-size: 14pt;
        }
    }
</style>

<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="">

    <div class="col-xs-12 pad5 pre-view-table">

        <div class="col-xs-12 pad3 preview-declaration" style="margin-top: 15px;">
            <div class="col-xs-12 pad0" style="font-size: 12px; text-align:justify;">

                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/partial/_viewText-page-1', [
                        'model' => $model,
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/partial/_viewText-page-2', [
                        'model' => $model,
                        'quarters' => $model->taxDeclarationQuarters,
                    ]); ?>
                </div>
                <div>
                    <?= $this->render('@frontend/modules/tax/views/declaration/partial/_viewText-page-3', [
                        'model' => $model,
                        'quarters' => $model->taxDeclarationQuarters,
                    ]); ?>
                </div>


            </div>
        </div>

    </div>
</div>

