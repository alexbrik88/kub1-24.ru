<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use frontend\modules\tax\widgets\TaxrobotStartModalWidget;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\BtnConfirmModalWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */
/* @var $isFirstVisit buulean */

$this->title = 'Заполните реквизиты';

$noAccount = $model->mainCheckingAccountant === null;

if ($isFirstVisit) {
    $this->params['hideCarrot'] = true;
}
$this->params['step'] = 1;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$this->registerJsFile( '@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );

$buttonFindIfns = Icon::get('search', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Найти код ИФНС и ОКТМО</span>';
$buttonCheckIfns = Icon::get('attention', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Проверить код ИФНС и ОКТМО</span>';

$innHelp = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Данные подтягиваются автоматически при вводе ИНН, изменять не рекоммендуется, только если есть несовпадения',
]);
$canStartModal = TaxrobotStartModalWidget::can($model);
?>

<?= TaxrobotStartModalWidget::widget([
    'company' => $model,
     'isFirstVisit' => $isFirstVisit,
]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'company-update-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'options' => [
            'class' => ''
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
]); ?>
    <div class="wrap p-4 tax-company">
        <div class="p-2">
            <?= Html::activeHiddenInput($model, 'company_type_id'); ?>
            <?= Html::activeHiddenInput($model, 'okato'); ?>
            <?php /*<?= Html::activeHiddenInput($model, 'oktmo'); ?>*/ ?>
            <?php /*<?= Html::activeHiddenInput($model, 'ifns_ga'); ?>*/ ?>
            <?= Html::activeHiddenInput($model, 'address_actual'); ?>
            <?= Html::hiddenInput('not_use_account', 0) ?>
            <div class="d-flex justify-content-between align-items-start">
                <h4 class="text_size_20 mb-4">Шаг 1: Заполните данные по вашему ИП</h4>
                <div class="d-flex align-items-center">
                    <?= $this->render('_video_instruction', [
                        'btn' => [
                            'class' => "button-list button-hover-transparent mr-3",
                            'label' => frontend\components\Icon::get('video-instruction-2'),
                            'title' => 'Видео инструкция',
                        ],
                    ]) ?>
                    <?= $canStartModal ? Html::button('Автоматическое заполнение', [
                        'class' => 'button-regular button-regular_red mr-3',
                        'data-toggle' => 'modal',
                        'data-target' => '#taxrobot-start-modal',
                    ]) : ''; ?>
                </div>
            </div>
            <strong class="small-title d-block mb-3">Реквизиты ИП</strong>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'inn')->textInput([
                                'maxlength' => true,
                            ])->label('<span class="is-empty">Введите ваш </span>ИНН'.$innHelp) ?>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'egrip')->textInput([
                                'maxlength' => true,
                            ]) ?>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'taxRegistrationDate', [
                                'template' => "{label}\n{$this->render('_datepicker_input')}\n{hint}\n{error}"
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'form-control date-picker',
                                'autocomplete' => 'off',
                                'value' => $model->taxRegistrationDate,
                                'disabled' => !empty($model->taxRegistrationDate),
                            ])->label('Дата регистрации ИП') ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
                        </div>
                        <div class="col-3 form-group mb-3 pt-4 pl-0">
                            <div style="margin-top: 13px;">
                                <?= $form->field($model, 'has_chief_patronymic')->checkbox([], true) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 form-group mb-3">
                            <?= $form->field($model, 'address_legal')->label('Адрес по прописке')->textInput(); ?>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'okved')->label('ОКВЭД')->textInput(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'ifns_ga')->label('Код ИФНС')->textInput(); ?>
                            <span class="tooltip2-click valign-middle sbs-tooltip"
                                data-tooltip-content="#tooltip_what_is_ifns"
                                style="position: absolute; left: 85px; top:0;">
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon text_size_16',
                                ]) ?>
                            </span>
                        </div>
                        <div class="col-3 form-group mb-3">
                            <?= $form->field($model, 'oktmo')->label('Код ОКТМО')->textInput(); ?>
                            <span class="tooltip2-click valign-middle sbs-tooltip"
                                data-tooltip-content="#tooltip_what_is_oktmo"
                                style="position: absolute; left: 97px; top:0;">
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon text_size_16',
                                ]) ?>
                            </span>
                        </div>
                        <div class="col-6 form-group mb-3" style="padding-top: 24px;">
                            <?= Html::button(($model->ifns_ga && $model->oktmo) ? $buttonFindIfns : $buttonCheckIfns, [
                                'id' => 'get-fias-ittem',
                                'class' => 'button-clr button-regular button-regular_red' .
                                    (($model->ifns_ga && $model->oktmo) ? '' : ' error'),
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <strong class="small-title d-block mb-3 pt-3">
                Расчёт счет
                <span class="show-first-account-number">№1</span>
            </strong>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, 'bik')->textInput([
                                'maxlength' => true,
                                'class' => 'form-control input-sm dictionary-bik',
                                'autocomplete' => 'off',
                                'data' => [
                                    'url' => Url::to(['/dictionary/bik']),
                                    'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                                    'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                                    'target-ks' => '#' . Html::getInputId($account, 'ks'),
                                    'target-rs' => '#' . Html::getInputId($account, 'rs'),
                                    'target-collapse' => '#company-bank-block',
                                ]
                            ])->label('БИК вашего банка') ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, 'rs')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '9{20}',
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, 'bank_name')->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, 'ks')->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                        <div class="form-group col-3 mb-3">
                            <?= $form->field($account, 'bank_city')->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php $i = 1; ?>
            <?php foreach ($additionalAccounts as $additionalAccount) : ?>
                <?php $i++; ?>
                <div class="company-account" data-id="<?= $additionalAccount->id ?>">
                    <strong class="small-title d-block mb-3 pt-3">
                        Расчёт счет №<span><?=$i?></span>

                        <?= BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                                'class' => 'ml-1 link',
                                'style' => 'color: #bbc1c7;',
                                'tag' => 'a'
                            ],
                            'modelId' => $additionalAccount->id,
                            'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                        ]); ?>
                    </strong>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($additionalAccount, 'bik')->textInput([
                                        'maxlength' => true,
                                        'class' => 'form-control input-sm dictionary-bik',
                                        'autocomplete' => 'off',
                                        'data' => [
                                            'url' => Url::to(['/dictionary/bik']),
                                            'target-name' => '#' . Html::getInputId($additionalAccount, 'bank_name'),
                                            'target-city' => '#' . Html::getInputId($additionalAccount, 'bank_city'),
                                            'target-ks' => '#' . Html::getInputId($additionalAccount, 'ks'),
                                            'target-rs' => '#' . Html::getInputId($additionalAccount, 'rs'),
                                            'target-collapse' => '#company-bank-block',
                                        ]
                                    ])->label('БИК вашего банка') ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($additionalAccount, 'rs')->widget(\yii\widgets\MaskedInput::className(), [
                                        'mask' => '9{20}',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($additionalAccount, 'bank_name')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($additionalAccount, 'ks')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($additionalAccount, 'bank_city')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php foreach ($newAccounts as $newAccount) : ?>
                <?php $i++; ?>
                <div class="new-account company-account" style="display: none" data-id="<?= "new-{$i}" ?>">
                    <strong class="small-title d-block mb-3 pt-3">
                        Расчёт счет №<span><?= $i ?></span>

                        <?= BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                                'class' => 'ml-1 link',
                                'tag' => 'a'
                            ],
                            'modelId' => "new-{$i}",
                            'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                        ]); ?>
                    </strong>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($newAccount, 'bik')->textInput([
                                        'maxlength' => true,
                                        'class' => 'form-control input-sm dictionary-bik',
                                        'autocomplete' => 'off',
                                        'data' => [
                                            'url' => Url::to(['/dictionary/bik']),
                                            'target-name' => '#' . Html::getInputId($newAccount, 'bank_name'),
                                            'target-city' => '#' . Html::getInputId($newAccount, 'bank_city'),
                                            'target-ks' => '#' . Html::getInputId($newAccount, 'ks'),
                                            'target-rs' => '#' . Html::getInputId($newAccount, 'rs'),
                                            'target-collapse' => '#company-bank-block',
                                        ]
                                    ])->label('БИК вашего банка') ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($newAccount, 'rs')->widget(\yii\widgets\MaskedInput::className(), [
                                        'mask' => '9{20}',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($newAccount, 'bank_name')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($newAccount, 'ks')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                                <div class="form-group col-3 mb-3">
                                    <?= $form->field($newAccount, 'bank_city')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="d-flex justify-content-start mt-4">
                <?= Html::button(Icon::get('add-icon', [
                    'class' => 'svg-icon mr-1',
                ]).' <span class="ml-2">Добавить расчётный счёт</span>', [
                    'class' => 'button-clr button-regular pl-3 pr-3'.
                                ($noAccount ? ' button-hover-content-red' : ' button-regular_red'),
                    'id' => 'add-new-account',
                    'style' => 'margin-right: 30px;',
                ]); ?>
                <?php if ($noAccount) : ?>
                    <?= Html::button('У меня нет расчетного счета', [
                        'id' => 'not_use_account',
                        'class' => 'button-clr button-regular button-regular_red pl-3 pr-3 tooltip2-hover',
                        'data-tooltip-content' => '#tooltip_not_use_account',
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="wrap wrap_btns fixed mb-0">
        <div class="row justify-content-between align-items-center">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'button-clr button-width button-regular button-regular_red mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <button class="taxrobot-company-panel-trigger" style="display:none!important"></button>
            </div>
            <div class="column">
                <?= $canStartModal ? Html::button('Автоматическое заполнение', [
                    'class' => 'button-regular button-regular_red',
                    'data-toggle' => 'modal',
                    'data-target' => '#taxrobot-start-modal',
                ]) : ''; ?>
            </div>
        </div>
    </div>
<?php $form->end(); ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_city" style="display: inline-block; text-align: center;">
        Необходимо для Платежного поручения.
    </span>
    <span id="tooltip_what_is_oktmo" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен код ОКТМО? </strong></p>
        <p> ОКТМО — это общероссийский классификатор территорий муниципальных образований. <br/> Коды ОКТМО необходимо указывать в платежных поручениях на уплату налогов и сборов, а так же в налоговых декларациях."</p>
    </span>
    <span id="tooltip_what_is_ifns" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен номер ИФНС? </strong></p>
        <p> Номер ИФНС - это номер Инспекции Федеральной Налоговой Службы, в которой вы регистрировали свое ИП. <br/> Данный номер необходим для заполнения реквизитов налоговой в платежке на уплату налогов.</p>
    </span>
    <span id="tooltip_not_use_account" style="display: inline-block; text-align: center;">
        <p>Расчетный счет нужен для учета дохода.<br/>
           Если у вас нет расч/счета, то возможно вам нужна НУЛЕВАЯ налоговая декларация.<br/>
           Нажмите эту кнопку и далее действуйте по шагам</p>
    </span>
</div>

<?php Modal::begin([
    'id' => 'fias-result-modal',
    'title' => 'Определение кода ИФНС и ОКТМО по адресу прописки',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <div class="fias-result fias-result-success">
        <div class="form-group">
            <label class="label" for="">Адрес по прописке:</label>
            <div class="inp_one_line-product">
                <input class="form-control fias-query-value" id="fias-address" type="text">
                <span class="fias-query-help" style="color:red;font-style:italic;font-size:12px">Начните вводить адрес и выберите нужный из предложенного списка</span>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-4 form-group mb-3">
                <label class="label" for="fias-result-ifns">ИФНС:</label>
                <input id="fias-result-ifns" class="form-control fias-result-value" disabled>
            </div>
            <div class="col-4 form-group mb-3">
                <label class="label" for="fias-result-oktmo">ОКТМО:</label>
                <input id="fias-result-oktmo" class="form-control fias-result-value" disabled>
            </div>
            <div class="col-4 form-group mb-3">
                <label class="label" for="fias-result-index">Индекс:</label>
                <input id="fias-result-index" class="form-control fias-result-value" disabled>
            </div>
        </div>
        <div class="form-group">
            Код ИФНС, ОКТМО и индекс определяются по адресу автоматически.
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Сохранить', [
                'id' => 'copy-fias-result',
                'class' => 'button-regular button-regular_red button-clr',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-regular button-hover-transparent',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    </div>

<?php Modal::end(); ?>

<?= $this->render('//company/form/_company_inn_api') ?>


<?= $this->render('_company_modal') ?>

<?php if (!$isFirstVisit && (Yii::$app->session->remove('show_example_popup') || Yii::$app->request->get('show_example'))) {
    echo $this->render('_first_show_modal');
}
?>

<?php
$this->registerJs(<<<JS
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        beforeRender: function (container, suggestions) {
            if (suggestions.length > 1) {
                container.prepend('<div class="text-truncate text-muted pl-2">Выберите вариант или продолжите ввод</div>');
            }
        },
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            if (!$($(this).data('target-rs')).val())
                $($(this).data('target-rs')).val('40802810').focus();
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
            if (suggestion.city && $(this).data('target-city')) {
                var city = suggestion.city[0].toUpperCase() + suggestion.city.slice(1).toLowerCase();
                $($(this).data('target-city')).addClass('edited').val(city);
            }
        }
    });

    window.isTaxRobot = true;

    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('.show-first-account-number').show();
        $('.new-account:hidden').first().show(250);
        $('#not_use_account').remove();
    });

    $(document).on("click", "#get-fias-ittem", function(e) {
        e.preventDefault();
        $(".fias-result-value").html("");
        $("#fias-result-modal").modal("show");
    });

    $("#fias-address").suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: "78497656dfc90c2b00308d616feb9df60c503f51",
        type: "ADDRESS",
        onSelect: function(suggestion) {
            if (suggestion.data) {
                if (suggestion.data.tax_office_legal) {
                    $('#fias-result-ifns').val(suggestion.data.tax_office_legal);
                } else {
                    $('#fias-result-ifns').val('');
                }
                if (suggestion.data.oktmo) {
                    $('#fias-result-oktmo').val(suggestion.data.oktmo.slice(0,8));
                } else {
                    $('#fias-result-oktmo').val('');
                }
                if (suggestion.data.postal_code) {
                    $('#fias-result-index').val(suggestion.data.postal_code);
                } else {
                    $('#fias-result-index').val('');
                }
            }

            $('#fias-address').parents(".form-group").removeClass("has-error").find(".help-block").remove();

            console.log(suggestion.data);
        }
    });

    $(document).on("click", "#copy-fias-result", function(e) {
        e.preventDefault();
        var form_group = $('#fias-address').parents(".form-group");

        if ($('#fias-address').val().trim() == "") {
            $(form_group).addClass("has-error");
            $(form_group).find('.help-block').remove();
            $(form_group).find('.fias-query-help').after("<div class=\"help-block\">Необходимо заполнить</div>");
            return false;
        }

        if ($("#fias-result-oktmo").val()) {
            $("#company-oktmo").val($("#fias-result-oktmo").val()).addClass('edited').parent().removeClass('has-error');
        }
        if ($("#fias-result-ifns").val()) {
            $("#company-ifns_ga").val($("#fias-result-ifns").val()).addClass('edited').parent().removeClass('has-error');
        }
        if ($("#fias-result-index").val()) {
            $("#company-address_legal").val($("#fias-result-index").val() + ', ' + $("#fias-address").val());
        }

        if ($("#company-ifns_ga").val() && $("#company-oktmo").val()) {
            $("#get-fias-ittem").html("<span class='glyphicon glyphicon-search'></span> Найти код ИФНС и ОКТМО");
        }
        $("#get-fias-ittem").toggleClass('button-regular_red', false).toggleClass('button-hover-content-red', true);

        $('#company-inn').parents('.form-group').find('label').css({'color':'#999'});
        $('#company-address_legal, #company-ifns_ga, #company-oktmo')
            .addClass('edited').parent().removeClass('has-error').addClass('has-success');
        $("#get-fias-ittem").removeClass('error');
        window.ifns_error = 0;
        window.oktmo_error = 0;

        $('#company-update-form').find('.has-error').removeClass('has-error');

        $("#fias-result-modal").modal("hide");

        $("#company-ifns_ga").trigger("change");
    });

    $('#company-has_chief_patronymic').change(function() {
        if ($(this).prop('checked')) {
            $('#company-ip_patronymic').val('').prop('readonly', true).parent().removeClass('has-error');
        } else {
            $('#company-ip_patronymic').prop('readonly', false);
        }
    });

    $(document).on("click", "#show-taxregistrationdate", function() {
        $('#company-taxregistrationdate').datepicker('show');
    });

    $(document).ready(function() {
       if (!$('#company-inn').val()) {
           $('#company-inn').parents('.form-group').find('label').css({'color':'red'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       }
    });

    $('#company-inn').on('change', function() {
       if (!$('#company-inn').val()) {
           $('#company-inn').parents('.form-group').find('label').css({'color':'red'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       } else {
           $('#company-inn').parents('.form-group').find('label').css({'color':'#999'});
       }
    });

    window.ifns_error = 0;
    window.oktmo_error = 0;
    window.show_pulsate = 0;
    $('#company-update-form').on('afterValidateAttribute', function (event, attribute, message) {

        //console.log(attribute);
        if ((attribute.container == '.field-checkingaccountant-rs' && message.length) ||
            (attribute.container == '.field-checkingaccountant-bik' && message.length)
        ) {
            $('#not_use_account').show();
        }
        if ((attribute.container == '.field-checkingaccountant-rs' && !message.length) ||
            (attribute.container == '.field-checkingaccountant-bik' && !message.length)
        ) {
            $('#add-new-account').toggleClass('button-regular_red', true).toggleClass('button-hover-content-red', false);
        }

        if (attribute.container == '.field-company-ifns_ga') {
            window.ifns_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }
        if (attribute.container == '.field-company-oktmo') {
            window.oktmo_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }

        if (window.ifns_error || window.oktmo_error) {
            $("#get-fias-ittem").toggleClass('button-regular_red', true).toggleClass('button-hover-content-red', false);
            if (window.show_pulsate) {
                window.show_pulsate = 0;
                $("#get-fias-ittem").addClass('error').parent().pulsate({
                   color: "#f00",
                   reach: 20,
                   repeat: 3
                });
            }
        } else {
            $("#get-fias-ittem").removeClass('error');
        }
    });

    $('#not_use_account').on('click', function() {
        var form  = $('#company-update-form');
        var input = $(form).find('input[name="not_use_account"]');
        // use accounts
        if ($(input).val() == 1) {
            $(input).val(0);
            $('#add-new-account').removeAttr('disabled');
            $('#checkingaccountant-bik, #checkingaccountant-rs')
                .attr("placeholder", "")
                .toggleClass("is-valid no_account", false)
                .toggleClass("is-invalid", true);
            $('#checkingaccountant-bank_name, #checkingaccountant-bank_city, #checkingaccountant-ks')
                .toggleClass("is-valid no_account", false);
        // not use accounts
        } else {
            $(input).val(1);
            $('.new-account').hide().find('input').val("");
            $('#add-new-account').prop('disabled', true);
            $('#checkingaccountant-bik, #checkingaccountant-rs')
                .attr("placeholder", "Нет счета");
            $('#checkingaccountant-bik, #checkingaccountant-rs, #checkingaccountant-bank_name, #checkingaccountant-bank_city, #checkingaccountant-ks')
                .val("")
                .toggleClass("is-invalid", false)
                .toggleClass("is-valid no_account", true)
                .siblings('.invalid-feedback').html("");
        }
    });

    $('.sbs-el').bind('click', function(e) {
        e.preventDefault();
        $('#company-update-form').yiiActiveForm('validate');

        if ($("#company-update-form").find(".has-error").length) {
            taxrobotCompanyPanelOpen();
        }

        $('#company-update-form').yiiActiveForm('submitForm');
    });

    function repaintAccountNumbers() {
        $('.company-account').each(function(i,v) {
           $(v).find('.small-title > span').html(2+i);
        });
    }

    $(document).on("click", ".btn-confirm-yes", function() {
        var account_id = $(this).data('model_id');
        var account_block = $('.company-account').filter('[data-id="' + account_id + '"]');

        if (account_block.length) {

            if (!$(account_block).find('.dictionary-bik').val().trim())
               $('#add-new-account').removeAttr('disabled');

            if (!$(account_block).hasClass('new-account')) {
                $('#company-update-form').append('<input type="hidden" name="delete_rs[]" value="' + account_id + '" />');
            }

            $(account_block).remove();
            repaintAccountNumbers();
        }

        return false;
    });

JS
);

