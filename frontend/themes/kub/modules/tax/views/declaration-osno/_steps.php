<?php

use yii\helpers\Url;

function getStepClass($itemStep, $step)
{
    return $itemStep < $step ? 'finish' : ($itemStep == $step ? 'active' : '');
}

function getStepImg($itemStep, $step)
{
    $active = $itemStep == $step ? '_active' : '';
    $itemStep = $itemStep < 3 ? $itemStep : 5;
    if ($itemStep)
        return '/img/icons/forma1_' . $itemStep . $active . '.png';
}
?>
<style>
    a.sbs-el {
        width: 33.333%;
    }
</style>
<div class="sbs-menu">
    <a class="sbs-el sbs-step-1 <?= getStepClass(1, $step); ?>" href="<?= Url::to(['company']); ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:32px; height:43px; background-image: url(<?= getStepImg(1, $step); ?>"></div>
                </td>
                <td class="text-block">
                    Реквизиты вашего <?= $company->companyType->name_short; ?>
                </td>
            </tr>
        </table>
    </a>
    <a class="sbs-el sbs-step-2 <?= getStepClass(2, $step); ?>" href="<?= Url::to(['params']); ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:32px; height:43px; background-image: url(<?= getStepImg(2, $step); ?>"></div>
                </td>
                <td class="text-block">
                    Параметры вашего <?= $company->companyType->name_short; ?>
                </td>
            </tr>
        </table>
    </a>
    <?php if ($step <= 3): ?>
        <a class="sbs-el sbs-step-3 <?= getStepClass(3, $step); ?>" href="<?= Url::to(['all']); ?>" data-pjax="0">
            <table>
                <tr>
                    <td class="image-block">
                        <div style="width:32px; height:43px; background-image: url(<?= getStepImg(3, $step); ?>"></div>
                    </td>
                    <td class="text-block">
                        Нулевая отчетность в налоговую и ПФР
                    </td>
                </tr>
            </table>
        </a>
    <?php elseif ($step > 3): ?>
        <?php $action = null; $name = 'Нулевая отчетность в налоговую и ПФР';
        switch ($step) {
            case 4:
                $action = 'declaration';
                //$name = 'Единая (упрощенная) декларация';
                break;
            case 5:
                $action = 'value-added-declaration';
                //$name = 'Декларация на добаленную стоимость';
                break;
            case 6:
                $action = 'profit-declaration';
                //$name = 'Декларация на прибыль организации';
                break;
            case 7:
                $action = 'balance';
                //$name = 'Бухгалтерский баланс';
                break;
            case 8:
                $action = 'szvm';
                //$name = 'Сведения о застрахованных лицах';
        } ?>
        <a class="sbs-el sbs-step-3 <?= getStepClass($step, $step); ?>" href="<?= Url::to([$action]); ?>"
           data-pjax="0">
            <table>
                <tr>
                    <td class="image-block">
                        <div style="width:32px; height:43px; background-image: url(<?= getStepImg($step, $step); ?>"></div>
                    </td>
                    <td class="text-block">
                        <?= $name; ?>
                    </td>
                </tr>
            </table>
        </a>
    <?php endif; ?>
</div>