<?php

use frontend\rbac\permissions;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\document\status\KudirStatus;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\Kudir;
 */
?>

<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('КУДиР (УСН 6%)', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),], [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'target' => '_blank',
            ]);?>
        </div>
        <div class="column mr-auto">
            <?= Html::a('КУДиР (Патент)', ['document-print-patent', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName()], [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'target' => '_blank',
            ]); ?>
        </div>
    </div>
</div>
