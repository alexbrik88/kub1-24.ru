<?php
use \common\components\date\DateHelper;

$bankCount = 0;
$bankItems = [];
$bankArray = $company->getCheckingAccountants()
    ->groupBy('bik')
    ->indexBy('bik')
    ->orderBy('type')
    ->all();

if ($bankArray) {
    foreach ($bankArray as $account) {
        $bankItems[] = "№ {$account->rs} в {$account->bank_name}";
    }
    $bankCount = count($bankItems);
}

if ($company->ip_patent_date && $company->ip_patent_date_end) {
    $startDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
        'date' => $company->ip_patent_date,
        'format' => 'd F Y г.',
        'monthInflected' => true,
    ]);
    $endDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
        'date' => $company->ip_patent_date_end,
        'format' => 'd F Y г.',
        'monthInflected' => true,
    ]);
} else {
    $startDateFormatted = $endDateFormatted = null;
}

$documentStartDate = "{$model->tax_year}-01-01";
if ($tax_registration_date = \DateTime::createFromFormat(DateHelper::FORMAT_USER_DATE, $company->gettaxRegistrationDate())) {
    if ($model->tax_year == $tax_registration_date->format('Y')) {
        $documentStartDate = $tax_registration_date->modify("+ 3 days")->format('Y-m-d');
    }
}
?>
<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
    .page-break  {
        page-break-after: always;
    }
</style>

<style>
    td {font-size:8pt;vertical-align: bottom}
    .table {margin:0; padding:0;}
    .table td.border-bottom {border-bottom:1px solid #000;}
    .table td.tip {padding:0 0 3px 0; font-size:7pt; text-align: center}
    .table td.ver-top {vertical-align:top;}
    .table tr.middle td {vertical-align:middle;padding:4px 0;}
    .table td.codes-h {border:2px solid #000; border-bottom:1px solid #000;}
    .table td.codes-m {border-right:2px solid #000; border-left:2px solid #000; border-bottom:1px solid #000;}
    .table td.inner-table {margin:0;padding:0;vertical-align:top;}
    <?php if (@$_GET['red']) { ?>.table td {border:1px solid red} <?php } ?>
</style>

<div class="page-content-in p-center pad-pdf-p page-break">
    <table class="table no-border">
        <tr>
            <td></td>
            <td width="25%" class="font-size-6 text-right">
                Приложение №3<br/>
                к Приказу Министерства Финансов<br/>
                Российской Федерации<br/>
                от 22.10.2012 N 135н
            </td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td class="font-size-8-bold text-center">
                КНИГА <br/>
                УЧЕТА ДОХОДОВ ИНДИВИДУАЛЬНЫХ ПРЕДПРИНИМАТЕЛЕЙ,<br/>
                ПРИМЕНЯЮЩИХ ПАТЕНТНУЮ СИСТЕМУ НАЛОГООБЛОЖЕНИЯ
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table class="table no-border">
        <tr>

            <td class="inner-table" style="padding-top:3px;">
                <table class="table no-border">
                    <tr>
                        <td colspan="3" class="text-right"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right">Форма по ОКУД</td>
                    </tr>
                    <tr>
                        <td width="35%"></td>
                        <td width="20%" class="border-bottom text-center">на <?= $model->tax_year ?> год</td>
                        <td class="text-right">Дата (год, месяц, число)</td>
                    </tr>
                </table>
                <table class="table no-border" style="margin-right:20px">
                    <tr>
                        <td width="35%">Налогоплательщик (фамилия</td>
                        <td></td>
                        <td width="20px"></td>
                    </tr>
                    <tr>
                        <td>имя, отчество индивидуального</td>
                        <td></td>
                        <td width="20px"></td>
                    </tr>
                    <tr>
                        <td>
                            предпринимателя)
                        </td>
                        <td class="border-bottom ver-bottom">
                            <?= $company->getIpFio() ?>
                        </td>
                        <td width="20px"></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td class="text-right">
                            по ОКПО
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Идентификационный номер налогоплательщика - индивидуального
                        </td>
                    </tr>
                    <tr>
                        <td>
                            предпринимателя (ИНН)
                        </td>
                    </tr>
                </table>
                <table class="table no-border" style="margin:9px 2px">
                    <tr>
                        <?php for ($i=0; $i<22; $i++) : ?>
                            <td style="<?= ($i<12) ? 'border: 1px solid #000;':'' ?> line-height:12pt" class="text-center">
                                <?= ($company->inn && isset($company->inn[$i])) ? $company->inn[$i] : '&nbsp;' ?>
                            </td>
                        <?php endfor; ?>
                        <td style="width:20px"></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td colspan="2">
                            Наименование субъекта Российской Федерации, в котором
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">
                            получен патент
                        </td>
                        <td width="65%" class="border-bottom ver-bottom text-center">
                            <?= $company->ip_patent_city ?>
                        </td>
                        <td class="text-right">по ОКАТО</td>
                    </tr>
                </table>
                <table class="table no-border" style="margin:25px 0">
                    <tr>
                        <td width="33%">
                            Срок, на который выдан патент
                        </td>
                        <td class="border-bottom ver-bottom text-center">
                            <?php if ($startDateFormatted && $endDateFormatted) : ?>
                                c <?= $startDateFormatted ?> по <?= $endDateFormatted ?>
                            <?php endif; ?>
                        </td>
                        <td width="20px"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tip">(дата начала действия патента<br/>и дата окончания действия патента)</td>
                        <td></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td width="21%">Единица измерения:</td>
                        <td>руб.</td>
                        <td class="text-right">по ОКЕИ</td>
                    </tr>
                </table>
            </td>

            <td width="20%" class="inner-table">
                <table class="table no-border">
                    <tr>
                        <td colspan="3" class="text-center font-bold codes-h">
                            Коды
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m">
                            <?= $company->okud ?: '<br/>' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center border-bottom" style="border-left:2px solid #000">
                            <?= date('Y', strtotime($documentStartDate)) ?>
                        </td>
                        <td class="text-center border-bottom" style="border-left:1px solid #000;border-right:1px solid #000;">
                            <?= date('m', strtotime($documentStartDate)) ?>
                        </td>
                        <td class="text-center border-bottom" style="border-right:2px solid #000">
                            <?= date('d', strtotime($documentStartDate)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m">
                            <?= $company->okpo ?: '<br/>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-top:none;border-bottom:none;padding-top:75px;">
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-top:none;border-bottom:none;padding-bottom:92px;">
                            <?= $company->okato ?: '<br/>' ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-top:none;border-bottom:none;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-top:1px solid #000;">
                            383
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td width="52%">Адрес места жительства индивидуального предпринимателя</td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td colspan="2" class="tip"><br/></td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom"><?= $company->getAddressActualFull() ?></td>
        </tr>
        <tr>
            <td colspan="2" class="tip"><br/></td>
        </tr>
        <tr>
            <td>Номера расчетных и иных счетов, открытых в учреждениях банков</td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">(номера расчетных</td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom"> <?php if ($bankItems > 0) echo $bankItems[0] ?> </td>
        </tr>
        <tr>
            <td colspan="2" class="tip">и иных счетов и наименование соответствующих банков)</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <?php if ($bankItems > 1) foreach ($bankItems as $key=>$item) : if ($key == 0) continue; ?>
            <tr>
                <td colspan="2" class="border-bottom"> <?= $item ?> </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>