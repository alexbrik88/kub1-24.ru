<?php

use frontend\components\PageSize;
use frontend\themes\kub\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}

$perPage = PageSize::get($pageSizeParam);
$items = PageSize::$items;
?>

<div class="form-group d-flex flex-nowrap align-items-center justify-content-end">
    <label class="label mr-3 mb-0" for="view">Отображать по</label>
    <div class="dropdown dropdown-view">
        <?= Html::textInput('', $perPage, [
            'id' => 'view',
            'class' => 'form-filter-control',
            'autocomplete' => 'off',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
            'readonly' => true,
        ]) ?>
        <?= Icon::get('shevron', ['class' => 'form-control-shevron svg-icon input-toggle']) ?>
        <div class="form-filter-drop dropdown-menu" aria-labelledby="view">
            <ul class="form-filter-list list-clr">
                <?php foreach ($items as $key => $value) : ?>
                    <?= Html::tag('li', Html::a($value, Url::current(['page' => null, $pageSizeParam => $key])), [
                        'class' => $perPage == $key ? 'active': null,
                    ]) ?>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>
