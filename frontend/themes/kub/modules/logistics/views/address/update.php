<?php

/* @var $this yii\web\View */
/* @var $model \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$this->title = $model->address;
?>
<div class="driver-update">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'newAddressContactPerson' => $newAddressContactPerson,
    ]); ?>
</div>
