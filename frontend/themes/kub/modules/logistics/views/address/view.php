<?php

use common\components\helpers\Html;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use frontend\themes\kub\modules\documents\widgets\CreatedByWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model LoadingAndUnloadingAddress */
/* @var $contactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$this->title = LoadingAndUnloadingAddress::$types[$model->address_type] . ' №' . $model->number;
?>
<?= Html::a('Назад к списку', Url::to(['index']), ['class' => 'link mb-2']); ?>
<div class="wrap wrap_padding_small">
    <div class="page-in row">
        <div class="page-in-content-logistics column">
            <div class="page-border">
                <?= CreatedByWidget::widget([
                    'createdAt' => date("d.m.Y", $model->created_at),
                    'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                ]); ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), Url::to(['update', 'id' => $model->id]), [
                    'title' => 'Редактировать',
                    'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                ]) ?>
                <div class="doc-container doc-preview" style="margin-top: -50px;">
                    <div class="document-template">
                        <table style="margin-top: 60px;">
                            <tr>
                                <td style="font-weight: 600;font-size: 22px;">
                                    <?= $this->title; ?>
                                </td>
                            </tr>
                        </table>
                        <table style="margin-top: 25px;">
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Тип:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= LoadingAndUnloadingAddress::$types[$model->address_type]; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Заказчик:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->contractor ?
                                        Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                        null; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Адрес:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->address; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Грузоотправитель:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?php if ($model->consignor): ?>
                                        <?= Html::a($model->consignor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->consignor_id, 'type' => $model->consignor->type])); ?>
                                    <?php else: ?>
                                        <?= $model->contractor ?
                                            Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                            null; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Грузополучатель:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?php if ($model->consignee): ?>
                                        <?= Html::a($model->consignee->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->consignee_id, 'type' => $model->consignee->type])); ?>
                                    <?php else: ?>
                                        <?= $model->contractor ?
                                            Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                            null; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php foreach ($model->getLoadingAndUnloadingAddressContactPeople()->orderBy(['is_main' => SORT_DESC])->all() as $contactPerson): ?>
                                <tr>
                                    <td class="bold" style="padding-bottom: 5px;">
                                        Контактное лицо:
                                    </td>
                                    <td style="padding-left: 20px;padding-bottom: 5px;">
                                        <span><?= $contactPerson->contact_person; ?></span>
                                        <span style="margin-left: 30px;"><?= "<b>{$contactPerson->phoneType->name}</b>: {$contactPerson->phone}"; ?></span>
                                        <?php if ($contactPerson->is_main): ?>
                                            <span style="padding-left: 10px;">
                                                <i>(Основной)</i>
                                            </span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Статус:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= LoadingAndUnloadingAddress::$statuses[$model->status]; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar-logistics column">
            <div id="map" style="width: 300px;height: 300px;float: right;"></div>
        </div>
    </div>
</div>
<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить адрес?",
            ]); ?>
        </div>
    </div>
</div>
<?php if ($model->map_width && $model->map_longitude): ?>
    <?php $this->registerJs("
    var map, markers;

    function initMap() {
        var center = {
            lat:{$model->map_width},
            lng:{$model->map_longitude}
        }
    
        var mapOptions = {
            center:center,
            scrollwheel:false,
            zoom:15
        }
    
        map = new google.maps.Map(document.getElementById('map'),mapOptions);
        
        var marker = new google.maps.Marker({
            position: center,
            map: map,
        });
    }
    initMap();
"); ?>
<?php endif; ?>
