<?php

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\AddressSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $user \common\models\employee\Employee
 */

use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use frontend\widgets\TableViewWidget;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Адреса Погрузки/Разгрузки';
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$tableViewClass = $user->config->getTableViewClass('table_view_logistics');
?>
<div class="wrap wrap_padding_small pl-4 pr-2 pt-2 pb-0 mb-4">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <h4 class="mt-1 mb-2"><?= $this->title ?></h4>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-1 text-right">
                    <?= Html::a($addIcon . '<span>Добавить</span>', Url::to(['create']), [
                        'class' => 'button-regular button-regular_red button-width',
                        'style' => 'width: 150px;',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_view_logistics']) ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => "table table-style table-count-list table-logistics table-logistics-joint-operations {$tableViewClass}",
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'emptyText' => 'Вы еще не добавили ни одного адреса погрузки/разгрузки. ' . Html::a('Добавить', Url::to(['create'])),
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return Html::checkbox('LoadingAndUnloadingAddress[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'filter' => [null => 'Все'] + LoadingAndUnloadingAddress::$types,
            'hideSearch' => false,
            's2width' => '220px',
            'attribute' => 'address_type',
            'label' => 'Погрузка/Разгрузка',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return Html::a(LoadingAndUnloadingAddress::$types[$model->address_type] . ' №' . $model->number,
                    Url::to(['view', 'id' => $model->id]));
            },
        ],
        [
            'filter' => $searchModel->getContractorFilter(),
            'hideSearch' => false,
            's2width' => '220px',
            'attribute' => 'contractor_id',
            'label' => 'Заказчик',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return $model->contractor ?
                    Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                    '';
            },
        ],
        [
            'attribute' => 'address',
            'label' => 'Адрес',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return $model->address;
            },
        ],
        [
            'filter' => $searchModel->getContactPersonFilter(),
            'hideSearch' => false,
            's2width' => '220px',
            'attribute' => 'mainContactPerson',
            'label' => 'Контактное лицо',
            'headerOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return $model->mainLoadingAndUnloadingAddressContactPerson ?
                    $model->mainLoadingAndUnloadingAddressContactPerson->contact_person :
                    '';
            },
        ],
        [
            'attribute' => 'mainContactPersonPhone',
            'label' => 'Телефон',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return $model->mainLoadingAndUnloadingAddressContactPerson ?
                    $model->mainLoadingAndUnloadingAddressContactPerson->phone :
                    '';
            },
        ],
        [
            'filter' => [null => 'Все'] + LoadingAndUnloadingAddress::$statuses,
            'hideSearch' => false,
            's2width' => '220px',
            'attribute' => 'status',
            'label' => 'Статус',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LoadingAndUnloadingAddress $model) {
                return $model->status ? LoadingAndUnloadingAddress::$statuses[$model->status] : '';
            },
        ],
    ],
]); ?>