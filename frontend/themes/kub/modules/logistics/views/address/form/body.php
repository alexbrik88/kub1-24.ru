<?php

use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\driver\PhoneType;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model LoadingAndUnloadingAddress */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$closeIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg>';
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$model->consignor_id .= '';
$model->consignee_id .= '';
$consignorArray = $model->getConsignorArray();
?>
<div class="wrap body-fields">
    <div class="row d-block">
        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Тип:</label>
                </div>
                <?= $form->field($model, 'address_type', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->radioList(LoadingAndUnloadingAddress::$types, [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                            'class' => 'radio-inline p-o radio-padding',
                        ]);
                    },
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Заказчик:</label>
                </div>
                <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить перевозчика']
                        + $company->sortedContractorList(Contractor::TYPE_CUSTOMER),
                    'options' => [
                        'class' => 'form-control contractor-select',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Адрес:</label>
                </div>
                <?= $form->field($model, 'address', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Грузоотправитель:</label>
                </div>
                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignor_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $consignorArray,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Грузополучатель:</label>
                </div>
                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignee_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $consignorArray,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
        </div>

        <div class="address-contact_person_list">
            <?php foreach ($model->loadingAndUnloadingAddressContactPeople as $loadingAndUnloadingAddressContactPerson): ?>
                <div class="address-contact_person-block row col-12">
                    <div class="form-group col-3 field-loadingandunloadingaddresscontactperson-contact_person">
                        <div class="form-filter" style="display: inline-block;">
                            <label class="label" for="cause">Контактное лицо:</label>
                        </div>
                        <?= $form->field($loadingAndUnloadingAddressContactPerson, 'contact_person', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->textInput([
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][contact_person]",
                            'id' => 'loadingandunloadingaddresscontactperson-contact_person-' . $loadingAndUnloadingAddressContactPerson->id,
                        ]); ?>
                    </div>
                    <div class="form-group col-3" style="padding-top: 22px;">
                        <?= $form->field($loadingAndUnloadingAddressContactPerson, 'phone_type_id', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->dropDownList(ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                            'class' => 'form-control',
                            'id' => 'loadingandunloadingaddresscontactperson-phone_type_id-' . $loadingAndUnloadingAddressContactPerson->id,
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][phone_type_id]",
                        ]); ?>
                    </div>
                    <div class="form-group col-3 field-loadingandunloadingaddresscontactperson-phone" style="padding-top: 22px;">
                        <?= MaskedInput::widget([
                            'model' => $loadingAndUnloadingAddressContactPerson,
                            'attribute' => 'phone',
                            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                            'options' => [
                                'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][phone]",
                                'id' => 'loadingandunloadingaddresscontactperson-phone-' . $loadingAndUnloadingAddressContactPerson->id,
                                'class' => 'form-control',
                                'placeholder' => '+7(___) ___-__-__',
                            ],
                        ]); ?>
                    </div>
                    <div class="form-group col-2" style="padding-top: 31px;">
                        <?= Html::activeCheckbox($loadingAndUnloadingAddressContactPerson, 'is_main', [
                            'class' => '',
                            'label' => 'Основной',
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][is_main]",
                            'id' => 'loadingandunloadingaddresscontactperson-is_main-' . $loadingAndUnloadingAddressContactPerson->id,
                        ]); ?>
                    </div>
                    <div class="col-1" style="padding-top: 26px;">
                        <span class="icon-close delete-contact_person" style="font-size: 20px;cursor: pointer;">
                            <?= $closeIcon ?>
                        </span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?= $form->field($model, 'contactPerson')->hiddenInput()->label(false); ?>

        <div class="form-group col-3">
            <span class="add-contact_person" style="margin-bottom: 15px;">
                <?= "{$addIcon} Добавить Контактное лицо" ?>
            </span>
        </div>

        <div class="template-address_contact_person_block row col-12 hidden">
            <div class="form-group col-3 field-loadingandunloadingaddresscontactperson-contact_person">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Контактное лицо:</label>
                </div>
                <?= $form->field($newAddressContactPerson, 'contact_person', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->textInput([
                    'name' => '',
                ]); ?>
            </div>
            <div class="form-group col-3" style="padding-top: 22px;">
                <?= Html::activeDropDownList($newAddressContactPerson, 'phone_type_id',
                    ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                        'class' => 'form-control',
                        'name' => '',
                    ]); ?>
            </div>
            <div class="form-group col-3 field-loadingandunloadingaddresscontactperson-phone" style="padding-top: 22px;">
                <?= MaskedInput::widget([
                    'model' => $newAddressContactPerson,
                    'attribute' => 'phone',
                    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => '+7(___) ___-__-__',
                        'name' => '',
                    ],
                ]); ?>
            </div>
            <div class="form-group col-2" style="padding-top: 31px;">
                <?= Html::activeCheckbox($newAddressContactPerson, 'is_main', [
                    'class' => 'no-uniform',
                    'label' => 'Основной',
                    'name' => '',
                ]); ?>
            </div>
            <div class="col-1" style="padding-top: 26px;">
                <span class="icon-close delete-contact_person" style="font-size: 20px;cursor: pointer;">
                    <?= $closeIcon ?>
                </span>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Статус:</label>
                </div>
                <?= $form->field($model, 'status', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => LoadingAndUnloadingAddress::$statuses,
                    'options' => [
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Широта:</label>
                </div>
                <?= $form->field($model, 'map_width', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Долгота:</label>
                </div>
                <?= $form->field($model, 'map_longitude', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <?= Html::hiddenInput(null, 'loadingandunloadingaddress-contractor_id', ['id' => 'adding-contractor-from-input']); ?>
    </div>
</div>
<?php $this->registerJs('
    var $contactPersonTmpID = 1;
    $(document).on("click", ".add-contact_person", function (e) {
        var $contactPersonList = $(".address-contact_person_list");
        var $contactPersonListLength = $contactPersonList.find(".address-contact_person-block").length;
        
        if ($contactPersonListLength < 3) {            
            var $addressContactPersonBlock = $(".template-address_contact_person_block").clone();
        
            $addressContactPersonBlock.removeClass("hidden")
                .removeClass("template-address_contact_person_block")
                .addClass("address-contact_person-block");
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-contact_person")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][contact_person]")
                .attr("id", "loadingandunloadingaddresscontactperson-contact_person-new_" + $contactPersonTmpID);
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone_type_id")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][phone_type_id]")
                .attr("id", "loadingandunloadingaddresscontactperson-phone_type_id-new_" + $contactPersonTmpID);
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][phone]")
                .attr("id", "loadingandunloadingaddresscontactperson-phone-new_" + $contactPersonTmpID)
                .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][is_main]")
                .attr("id", "loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID)
                .uniform("refresh");
            
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID)
                .closest("label")
                .attr("for", "loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID);
            
            if ($contactPersonListLength == 0) {
                $addressContactPersonBlock.find("input[type=\'checkbox\']").click();
            }
            $(".address-contact_person_list").append($addressContactPersonBlock);
            $contactPersonTmpID++;
        }
    });
    
    $(document).on("click", ".address-contact_person_list .address-contact_person-block .delete-contact_person", function (e) {        
        if ($(".address-contact_person_list .address-contact_person-block").length > 1) {
            $(this).closest(".address-contact_person-block").remove();
            if ($(".address-contact_person-block input[type=\'checkbox\']:checked").length == 0) {
                $(".address-contact_person-block input[type=\'checkbox\']:first").click();
            }
            checkContactPerson();
        }
    });
    
    $(document).on("change", ".address-contact_person-block input[type=\'checkbox\']", function (e) {
       var $clickedID = $(this).attr("id");
       if ($(this).is(":checked")) {
           $(".address-contact_person-block input[type=\'checkbox\']:checked:not(\'#" + $clickedID + "\')").prop("checked", false).uniform("refresh");
       } else if ($(".address-contact_person-block input[type=\'checkbox\']:checked").length == 0) { 
           $(".address-contact_person-block input[type=\'checkbox\']:first").prop("checked", true).uniform("refresh");
        }
    });
    
    $(document).on("change", ".address-contact_person_list .address-contact_person-block input:text", function (e) {
        checkContactPerson();
    });
    
    function checkContactPerson() {
        var $isSetContactPerson = false;
        $(".address-contact_person_list .address-contact_person-block").each(function () {
            var $contactPersonVal = $(this).find(".field-loadingandunloadingaddresscontactperson-contact_person input").val();
            if ($contactPersonVal !== "" && $(this).find(".field-loadingandunloadingaddresscontactperson-phone input").val() !== "") {
                $("#loadingandunloadingaddress-contactperson").val($contactPersonVal);
                $isSetContactPerson = true;
            }
        });
        if ($isSetContactPerson == false) {
            $("#loadingandunloadingaddress-contactperson").val("");
        }
    }
');
