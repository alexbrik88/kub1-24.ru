<?php

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\RequestSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $company \common\models\Company
 */

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\logisticsRequest\LogisticsRequest;
use frontend\themes\kub\widgets\SummarySelectWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\Url;

$userConfig = Yii::$app->user->identity->config;
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$tableViewClass = $userConfig->getTableViewClass('table_view_logistics');
$this->title = 'Заявки';
?>
<div class="wrap wrap_padding_small pl-4 pr-2 pt-2 pb-0 mb-4">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <h4 class="mt-1 mb-2"><?= $this->title ?></h4>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-1 text-right">
                    <?= Html::a($addIcon . '<span>Добавить</span>', Url::to(['create']), [
                        'class' => 'button-regular button-regular_red button-width',
                        'style' => 'width: 150px;',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'logistics_request_unloading_date',
                ],
                [
                    'attribute' => 'logistics_request_goods',
                ],
                [
                    'attribute' => 'logistics_request_customer_debt',
                ],
                [
                    'attribute' => 'logistics_request_carrier_debt',
                ],
                [
                    'attribute' => 'logistics_request_ttn',
                ],
                [
                    'attribute' => 'logistics_request_author_id',
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_logistics']) ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => "table table-style table-count-list table-logistics {$tableViewClass}",
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'emptyText' => 'Вы еще не добавили ни одной заявки. ' . Html::a('Добавить', Url::to(['create'])),
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return Html::checkbox('LogisticsRequest[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'attribute' => 'document_number',
            'label' => '№',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return Html::a($model->document_number, ['view', 'id' => $model->id]);
            },
        ],
        [
            'attribute' => 'document_date',
            'label' => 'Дата',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'loading.date',
            'label' => 'Дата погрузки',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->loading->date ?
                    DateHelper::format($model->loading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                    '';
            },
        ],
        [
            'attribute' => 'unloading.date',
            'label' => 'Дата разгрузки',
            'headerOptions' => [
                'class' => 'col_logistics_request_unloading_date sorting' . ($userConfig->logistics_request_unloading_date ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_unloading_date' . ($userConfig->logistics_request_unloading_date ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->unloading->date ?
                    DateHelper::format($model->unloading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                    '';
            },
        ],
        [
            'filter' => $searchModel->getStatusFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'status_id',
            'label' => 'Статус',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->status ? $model->status->name : '';
            },
        ],
        [
            'filter' => $searchModel->getCustomerFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'customer_id',
            'label' => 'Заказчик',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->customer ?
                    Html::a($model->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->customer_id, 'type' => $model->customer->type])) :
                    '';
            },
        ],
        [
            'filter' => $searchModel->getCarrierFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'carrier_id',
            'label' => 'Перевозчик',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->carrier ?
                    Html::a($model->carrier->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->carrier_id, 'type' => $model->carrier->type])) :
                    '';
            },
        ],
        [
            'attribute' => 'route',
            'label' => 'Маршрут',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->loading->city . ' - ' . $model->unloading->city;
            },
        ],
        [
            'filter' => $searchModel->getDriverFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'driver_id',
            'label' => 'Водитель',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->driver ?
                    Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id,])) :
                    '';
            },
        ],
        [
            'attribute' => 'goods_name',
            'label' => 'Груз',
            'headerOptions' => [
                'class' => 'sorting col_logistics_request_goods' . ($userConfig->logistics_request_goods ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_goods' . ($userConfig->logistics_request_goods ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->goods_name ?: '';
            },
        ],
        [
            'attribute' => 'customerDolg',
            'label' => 'Долг заказчику',
            'headerOptions' => [
                'class' => 'col_logistics_request_customer_debt' . ($userConfig->logistics_request_customer_debt ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_customer_debt' . ($userConfig->logistics_request_customer_debt ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return '???';
            },
        ],
        [
            'attribute' => 'carrierDolg',
            'label' => 'Долг перевозчику',
            'headerOptions' => [
                'class' => 'col_logistics_request_carrier_debt' . ($userConfig->logistics_request_carrier_debt ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_carrier_debt' . ($userConfig->logistics_request_carrier_debt ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return '???';
            },
        ],
        [
            'attribute' => 'ttn',
            'label' => 'ТТН',
            'headerOptions' => [
                'class' => 'col_logistics_request_ttn' . ($userConfig->logistics_request_ttn ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_ttn' . ($userConfig->logistics_request_ttn ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return '???';
            },
        ],
        [
            'filter' => $searchModel->getAuthorFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'author_id',
            'label' => 'Ответственный',
            'headerOptions' => [
                'class' => 'col_logistics_request_author_id' . ($userConfig->logistics_request_author_id ? '' : ' hidden'),
            ],
            'contentOptions' => [
                'class' => 'col_logistics_request_author_id dropdown-filter' . ($userConfig->logistics_request_author_id ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function (LogisticsRequest $model) {
                return $model->author->getFio();
            },
        ],
    ],
]); ?>
<?= SummarySelectWidget::widget([
    'buttons' => [
        Html::a('Статус', '#status', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
        Html::tag('div', Html::button('<span>Еще</span>  ' . $this->render('//svg-sprite', ['ico' => 'shevron']), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => [
                [
                    'label' => 'Выставить Счет',
                    'url' => '#many-create-invoice',
                    'linkOptions' => [
                        'data-toggle' => 'modal',
                    ],
                ],
                [
                    'label' => 'Создать ТТН',
                    'url' => '#many-create-ttn',
                    'linkOptions' => [
                        'data-toggle' => 'modal',
                    ],
                ],
            ],
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), ['class' => 'dropup']),
        Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
    ],
]); ?>
<div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные заявки?</h4>
            <div class="text-center">
                <?= Html::a('Да', null, [
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['many-delete']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
