<?php

use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\components\date\DateHelper;

/* @var $model \common\models\logisticsRequest\LogisticsRequest
 * @var $contractor \common\models\Contractor
 * @var $attribute string
 * @var $options array
 */

$options = isset($options) ? $options : [];

$agreementDropDownList = ['' => '---'];
if ($contractor) {
    $agreementArray = $contractor->getAgreements()->joinWith('agreementType')->orderBy([
        'agreement_type.name' => SORT_ASC,
        'agreement.document_date' => SORT_DESC,
    ])->all();
    $agreementDropDownList += ['add-modal-agreement' => Icon::PLUS .  'Добавить договор'];
    /** @var $agreement \common\models\Agreement */
    foreach ($agreementArray as $agreement) {
        $agreement_date = DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $key = "{$agreement->agreementType->name}&{$agreement->getFullNumber()}&{$agreement->document_date}&{$agreement->agreementType->id}&{$agreement->id}";
        $value = "{$agreement->agreementType->name} № {$agreement->getFullNumber()} от {$agreement_date}";
        if ($value == $model->$attribute) {
            $model->$attribute = $key;
        }
        $agreementDropDownList[$key] = $value;
    }
}

Pjax::begin([
    'id' => $attribute . '-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['basis-document']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'logistics_request-' . $attribute,
    'model' => $model,
    'attribute' => $attribute,
    'data' => $agreementDropDownList,
    'options' => $options,
    'pluginOptions' => [
        'width' => '100%',
        'escapeMarkup' => new JsExpression('function(text) {return text;}')
    ],
]);

Pjax::end();
