<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\logisticsRequest\LogisticsRequestCondition;
use common\models\logisticsRequest\LogisticsRequestFrom;
use common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use dosamigos\datetimepicker\DateTimePicker;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\RequestAdditionalExpensesDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \common\models\logisticsRequest\LogisticsRequest */
/* @var $company \common\models\Company */
/* @var $logisticsRequestLoading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestUnloading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestContractEssenceCustomerAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCustomerForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */
?>
<?php $form = ActiveForm::begin([
    'id' => 'vehicle-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
]); ?>

<?= $this->render('form/header', [
    'model' => $model,
]); ?>

<div class="nav-tabs-row mb-2" style="z-index: 0;">
    <?= Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 logistic-request-nav-form'],
        'items' => [
            [
                'label' => 'Заказчик',
                'url' => 'javascript:;',
                'active' => true,
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Перевозчик',
                'url' => 'javascript:;',
                'active' => false,
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
        ],
    ]); ?>
</div>

<div class="wrap body-fields">
    <div class="row d-block tab-content-logistics-request">
        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Заказчик:</label>
                </div>
                <?= $form->field($model, 'customer_id', ['template' => "{input}", 'options' => [
                    'class' => 'show-contractor-type-in-fields',
                ]])->widget(Select2::class, [
                    'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить заказчика']
                        + $company->sortedContractorList(Contractor::TYPE_CUSTOMER),
                    'options' => [
                        'class' => 'form-control contractor-select',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Контактное лицо:</label>
                </div>
                <?= $form->field($model, 'customer_contact_person', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Договор:</label>
                </div>
                <div class="field-logisticsrequest-customeragreement">
                    <?= $this->render('form/_basis_document', [
                        'model' => $model,
                        'contractor' => $model->customer,
                        'attribute' => 'customerAgreement',
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="row col-12">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">№ заявки Заказчика</label>
                    <div class="tooltip-box ml-2">
                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                data-original-title="Если у Заказчика свой номер и дата заявки, то укажите их тут">
                            <svg class="tooltip-question-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <?= $form->field($model, 'customer_request_number', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3" style="padding-top: 24px;">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'customer_request_date', [
                                'class' => 'form-control date-picker',
                                'value' => DateHelper::format($model->customer_request_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-6 field-line-block" style="font-size: 25px;">
            Оплата
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Ставка:</label>
                </div>
                <?= $form->field($model, 'customer_rate', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Форма:</label>
                </div>
                <?= $form->field($model, 'customer_form_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(LogisticsRequestFrom::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Условие:</label>
                </div>
                <?= $form->field($model, 'customer_condition_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(LogisticsRequestCondition::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Отсрочка:</label>
                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                            data-original-title="Количество дней от условия">
                        <svg class="tooltip-question-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                        </svg>
                    </button>
                </div>
                <?= $form->field($model, 'customer_delay', ['template' => "{input}<span class='text-bold' style='margin-left: 10px;'>дн.</span>", 'options' => [
                    'class' => '',
                    'style' => 'display: flex;align-items: center;',
                ]]); ?>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Доп расходы:</label>
                </div>
                <?= $form->field($model, 'customer_additional_expenses_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(RequestAdditionalExpensesDropdownWidget::class, [
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
                <?= $this->render('form/expenses_item_form', [
                    'inputId' => 'logisticsrequest-customer_additional_expenses_id',
                    'type' => 'customer',
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Сумма:</label>
                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                            data-original-title="Прибыль будет уменьшаться на сумму доп. расходов">
                        <svg class="tooltip-question-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                        </svg>
                    </button>
                </div>
                <?= $form->field($model, 'customer_amount', ['template' => "{input}", 'options' => [
                    'class' => '',
                    'style' => 'display: flex;align-items: center;',
                ]])->textInput([
                    'class' => 'form-control is_own-disabled',
                    'disabled' => (bool)$model->is_own,
                ]); ?>
            </div>
        </div>
    </div>
    <div class="row d-block tab-content-logistics-request hidden">
        <div class="row col-12">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Перевозчик:</label>
                </div>
                <?= $form->field($model, 'carrier_id', ['template' => "{input}", 'options' => [
                    'class' => 'show-contractor-type-in-fields',
                ]])->widget(Select2::class, [
                    'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить перевозчика']
                        + ['is_own' => 'Собственный транспорт']
                        + $company->sortedContractorList(Contractor::TYPE_SELLER),
                    'options' => [
                        'class' => 'form-control contractor-select',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                    ],
                ]); ?>
                <?= Html::activeHiddenInput($model, 'is_own'); ?>
            </div>
        </div>

        <div class="row col-12">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Контактное лицо:</label>
                </div>
                <?= $form->field($model, 'carrier_contact_person', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Договор:</label>
                </div>
                <div class="field-logisticsrequest-customeragreement">
                    <?= $this->render('form/_basis_document', [
                        'model' => $model,
                        'contractor' => $model->carrier,
                        'attribute' => 'carrierAgreement',
                        'options' => [
                            'class' => 'is_own-disabled',
                            'disabled' => (bool)$model->is_own,
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;width: 170px;">
                    <label class="label" for="cause">№ заявки Перевозчика</label>
                    <div class="tooltip-box ml-2">
                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                data-original-title="Если у Перевозчика свой номер и дата заявки, то укажите их тут">
                            <svg class="tooltip-question-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <?= $form->field($model, 'carrier_request_number', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3" style="padding-top: 24px;">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'carrier_request_date', [
                                'class' => 'form-control date-picker',
                                'value' => DateHelper::format($model->carrier_request_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Водитель:</label>
                </div>
                <?= $form->field($model, 'driver_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map($company->drivers, 'id', 'fio'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
            </div>
            <div class="form-group col-4" style="padding-top: 36px;">
                <span class="driver-phone"></span>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Транспорт:</label>
                </div>
                <?= $form->field($model, 'vehicle_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map($company->getVehicles()
                        ->andWhere(['in', 'vehicle_type_id', [VehicleType::TYPE_TRACTOR, VehicleType::TYPE_WAGON]])->all(), 'id', function (Vehicle $model) {
                        $name = $model->model ? ($model->model . ' ') : null;
                        $name .= $model->state_number;

                        return $name;
                    }),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
            </div>
        </div>
        <div class="form-group col-6 field-line-block" style="font-size: 25px;">
            Оплата
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Ставка:</label>
                </div>
                <?= $form->field($model, 'carrier_rate', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->textInput([
                    'class' => 'form-control is_own-disabled',
                    'disabled' => (bool)$model->is_own,
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Форма:</label>
                </div>
                <?= $form->field($model, 'carrier_form_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(LogisticsRequestFrom::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                        'class' => 'form-control is_own-disabled',
                        'disabled' => (bool)$model->is_own,
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Условие:</label>
                </div>
                <?= $form->field($model, 'carrier_condition_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(LogisticsRequestCondition::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'prompt' => '',
                        'disabled' => (bool)$model->is_own,
                    ],
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Отсрочка:</label>
                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                            data-original-title="Количество дней от условия">
                        <svg class="tooltip-question-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                        </svg>
                    </button>
                </div>
                <?= $form->field($model, 'carrier_delay', ['template' => "{input}<span class='text-bold' style='margin-left: 10px;'>дн.</span>", 'options' => [
                    'class' => '',
                    'style' => 'display: flex;align-items: center;',
                ]])->textInput([
                    'class' => 'form-control is_own-disabled',
                    'disabled' => (bool)$model->is_own,
                    'type' => 'number',
                    'min' => 0,
                    'step' => 'any',
                ]); ?>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Доп расходы:</label>
                </div>
                <?= $form->field($model, 'carrier_additional_expenses_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(RequestAdditionalExpensesDropdownWidget::class, [
                    'options' => [
                        'prompt' => '',
                    ],
                ]); ?>
                <?= $this->render('form/expenses_item_form', [
                    'inputId' => 'logisticsrequest-carrier_additional_expenses_id',
                    'type' => 'carrier',
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Сумма:</label>
                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                            data-original-title="Прибыль будет уменьшаться на сумму доп. расходов">
                        <svg class="tooltip-question-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                        </svg>
                    </button>
                </div>
                <?= $form->field($model, 'carrier_amount', ['template' => "{input}", 'options' => [
                    'class' => '',
                    'style' => 'display: flex;align-items: center;',
                ]])->textInput([
                    'class' => 'form-control is_own-disabled',
                    'disabled' => (bool)$model->is_own,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap body-fields">
    <div class="row d-block">
        <table class="table table-striped table-bordered table-hover dataTable table-logistics-request" role="grid" style="border: none;">
            <thead>
            <tr class="heading">
                <th width="4%" style="border-left: none;">Вид</th>
                <th width="10%">Дата</th>
                <th width="11%">Время</th>
                <th width="10%">Город</th>
                <th width="24%">Адрес</th>
                <th width="10%">Контактное лицо</th>
                <th width="21%">Телефон</th>
                <th width="10%" style="border-right: none;">Способ Погрузки/Разгрузки</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php foreach ($logisticsRequestLoading->attributeLabels() as $attribute => $label): ?>
                    <?php if (in_array($attribute, ['id', 'logistics_request_id'])) continue; ?>
                    <td>
                        <?php if ($attribute == 'type'): ?>
                            <?= LogisticsRequestLoadingAndUnloading::$types[$logisticsRequestLoading->$attribute]; ?>
                        <?php elseif ($attribute == 'time'): ?>
                            <?php $hours = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, 'G', 'H:i:s') : null;
                            $minutes = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, 'i', 'H:i:s') : null;
                            $logisticsRequestLoading->time = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, DateHelper::FORMAT_USER_DATE . ' H:i', 'H:i:s') : null; ?>
                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'style' => 'width: 100%;',
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->widget(DateTimePicker::class, [
                                'language' => 'ru',
                                'size' => 'ms',
                                'template' => '{input}',
                                'pickButtonIcon' => 'glyphicon glyphicon-time',
                                'inline' => false,
                                'options' => [
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                    'class' => 'logisticsrequestLoadingandunloading-time',
                                    'id' => 'logisticsrequestLoadingandunloading-0-time',
                                    'data-hour' => $hours,
                                    'data-minute' => $minutes,
                                ],
                                'clientOptions' => [
                                    'startView' => 1,
                                    'minView' => 0,
                                    'maxView' => 1,
                                    'autoclose' => true,
                                    'minuteStep' => 15,
                                    'format' => 'hh:ii',
                                    'pickerPosition' => 'top-left',
                                    'useCurrent' => true,
                                ],
                            ])->label(false); ?>
                        <?php elseif ($attribute == 'contact_person_phone'): ?>
                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->widget(\yii\widgets\MaskedInput::class, [
                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                'options' => [
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                    'id' => 'logisticsrequestloadingandunloading-' . $logisticsRequestLoading->type . '-contact_person_phone',
                                    'placeholder' => '+7(XXX) XXX-XX-XX',
                                ],
                            ]); ?>
                        <?php else: ?>
                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control' . ($attribute == 'date' ? ' date-picker min_wid_picker' : null),
                                    'style' => 'width: 100%;',
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                    'value' => $attribute == 'date' ?
                                        DateHelper::format($logisticsRequestLoading->$attribute, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                        $logisticsRequestLoading->$attribute,
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->label(false); ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach ($logisticsRequestUnloading->attributeLabels() as $attribute => $label): ?>
                    <?php if (in_array($attribute, ['id', 'logistics_request_id'])) continue; ?>
                    <td>
                        <?php if ($attribute == 'type'): ?>
                            <?= LogisticsRequestLoadingAndUnloading::$types[$logisticsRequestUnloading->$attribute]; ?>
                        <?php elseif ($attribute == 'time'): ?>
                            <?php $hours = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, 'G', 'H:i:s') : null;
                            $minutes = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, 'i', 'H:i:s') : null;
                            $logisticsRequestUnloading->time = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, DateHelper::FORMAT_USER_DATE . ' H:i', 'H:i:s') : null; ?>
                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'style' => 'width: 100%;',
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->widget(DateTimePicker::class, [
                                'language' => 'ru',
                                'size' => 'ms',
                                'template' => '{input}',
                                'pickButtonIcon' => 'glyphicon glyphicon-time',
                                'inline' => false,
                                'options' => [
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                    'class' => 'logisticsrequestLoadingandunloading-time',
                                    'id' => 'logisticsrequestLoadingandunloading-1-time',
                                    'data-hour' => $hours,
                                    'data-minute' => $minutes,
                                ],
                                'clientOptions' => [
                                    'startView' => 1,
                                    'minView' => 0,
                                    'maxView' => 1,
                                    'autoclose' => true,
                                    'minuteStep' => 15,
                                    'format' => 'hh:ii',
                                    'pickerPosition' => 'top-left',
                                    'useCurrent' => true,
                                ],
                            ])->label(false); ?>
                        <?php elseif ($attribute == 'contact_person_phone'): ?>
                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->widget(\yii\widgets\MaskedInput::class, [
                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                'options' => [
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                    'id' => 'logisticsrequestloadingandunloading-' . $logisticsRequestUnloading->type . '-contact_person_phone',
                                    'placeholder' => '+7(XXX) XXX-XX-XX',
                                ],
                            ]); ?>
                        <?php else: ?>
                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control' . ($attribute == 'date' ? ' date-picker min_wid_picker' : null),
                                    'style' => 'width: 100%;',
                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                    'value' => $attribute == 'date' ?
                                        DateHelper::format($logisticsRequestUnloading->$attribute, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                        $logisticsRequestUnloading->$attribute,
                                ],
                                'template' => "{input}\n{error}\n{hint}",
                            ])->label(false); ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-12 row">
            <div class="form-group col-6 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Груз:</label>
                </div>
                <?= $form->field($model, 'goods_name', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="col-12 row">
            <div class="form-group col-2">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Вес (тонн):</label>
                </div>
                <?= $form->field($model, 'goods_weight', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-2">
                <div class="form-filter" style="display: inline-block;width: 85px;">
                    <label class="label" for="cause">Габариты (м):</label>
                </div>
                <?= $form->field($model, 'goods_length', [
                    'template' => "<span class='text-bold' style='margin-right: 5px;'>Д</span>{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
            <div class="form-group col-2" style="padding-top: 22px;">
                <?= $form->field($model, 'goods_width', [
                    'template' => "<span class='text-bold' style='margin-right: 5px;'>Ш</span>{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
            <div class="form-group col-2" style="padding-top: 22px;">
                <?= $form->field($model, 'goods_height', [
                    'template' => "<span class='text-bold' style='margin-right: 5px;'>В</span>{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
        </div>
        <div class="col-12 row">
            <div class="form-group col-3 field-line-block">
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="cause">Кол-во (шт.):</label>
                </div>
                <?= $form->field($model, 'goods_count', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>
        <div class="col-12 row tab-content-logistics-request" style="display: flex;">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Предмет договора с закачиком:</label>
                </div>
                <?= $form->field($model, 'customer_request_essence', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->textarea([
                    'rows' => 6,
                    'value' => $logisticsRequestContractEssenceCustomerAll->is_checked && $model->isNewRecord ?
                        $logisticsRequestContractEssenceCustomerAll->text : $model->customer_request_essence,
                ]); ?>
            </div>
            <div class="form-group field-logisticsrequest-checkboxes col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Сохранить текст для всех договоров-заявок:</label>
                </div>
                <?= Html::activeCheckbox($logisticsRequestContractEssenceCustomerForContractor, 'is_checked', [
                    'class' => '',
                    'label' => 'С данным заказчиком',
                    'name' => "LogisticsRequestContractEssence[customer][contractor][is_checked]",
                    'id' => 'logisticsrequestcontractessence-customer-contractor-is_checked',
                ]); ?>
                <?= Html::activeCheckbox($logisticsRequestContractEssenceCustomerAll, 'is_checked', [
                    'class' => '',
                    'label' => 'С данным заказчиком',
                    'name' => "LogisticsRequestContractEssence[customer][all][is_checked]",
                    'id' => 'logisticsrequestcontractessence-customer-all-is_checked',
                ]); ?>
            </div>
        </div>
        <div class="col-12 row tab-content-logistics-request hidden">
            <div class="col-12 row is_own-hide has_carrier_agreement-hide"
                 style="display: <?= (bool)$model->is_own || $model->carrierAgreement ? 'none' : 'flex'; ?>;">
                <div class="form-group col-6">
                    <div class="form-filter" style="display: inline-block;">
                        <label class="label" for="cause">Предмет договора с перевозчиком:</label>
                    </div>
                    <?= $form->field($model, 'carrier_request_essence', ['template' => "{input}", 'options' => [
                        'class' => '',
                    ]])->textarea([
                        'rows' => 6,
                        'value' => $logisticsRequestContractEssenceCarrierAll->is_checked && $model->isNewRecord ?
                            $logisticsRequestContractEssenceCarrierAll->text : $model->carrier_request_essence,
                    ]); ?>
                </div>
                <div class="form-group field-logisticsrequest-checkboxes col-6">
                    <div class="form-filter" style="display: inline-block;">
                        <label class="label" for="cause">Сохранить текст для всех договоров-заявок:</label>
                    </div>
                    <?= Html::activeCheckbox($logisticsRequestContractEssenceCarrierForContractor, 'is_checked', [
                        'class' => '',
                        'label' => 'С данным перевозчиком',
                        'name' => "LogisticsRequestContractEssence[carrier][contractor][is_checked]",
                        'id' => 'logisticsrequestcontractessence-carrier-contractor-is_checked',
                    ]); ?>
                    <?= Html::activeCheckbox($logisticsRequestContractEssenceCarrierAll, 'is_checked', [
                        'class' => '',
                        'label' => 'С данным перевозчиком',
                        'name' => "LogisticsRequestContractEssence[carrier][all][is_checked]",
                        'id' => 'logisticsrequestcontractessence-carrier-all-is_checked',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', Url::previous('lastPage'), [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>
    <?php Pjax::begin([
        'id' => 'agreement-form-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]); ?>
    <?php Pjax::end(); ?>
<?php Modal::end(); ?>
<?php $this->registerJs('
    $(document).on("change", "#logisticsrequest-customer_id, #logisticsrequest-carrier_id", function() {
        var $customerContractEssence = $(".has_customer_agreement-hide");
        var $carrierContractEssence = $(".has_carrier_agreement-hide");

        if ($(this).attr("id") == "logisticsrequest-customer_id") {
            $pjaxContainer = "#customerAgreement-pjax-container";
            $customerContractEssence.show();
            $type = 0;
        } else {
            $pjaxContainer = "#carrierAgreement-pjax-container";
            $carrierContractEssence.show();
            $type = 1;
        }
        $.post("contractor-essence", {
                contractor_id: $(this).val(),
                type: $type
            }, function (data) {
                if (data.result == true) {
                    if ($type == 0) {
                        $("#logisticsrequest-customer_request_essence").val(data.essence);
                        $("#logisticsrequestcontractessence-customer-contractor-is_checked:not(:checked)").click();
                    } else {
                        $("#logisticsrequest-carrier_request_essence").val(data.essence);
                        $("#logisticsrequestcontractessence-carrier-contractor-is_checked:not(:checked)").click();
                    }
                }
        });
        if ($($pjaxContainer).length) {
            $.pjax({
                url: $($pjaxContainer).data("url"),
                data: {
                    contractorId: $(this).val(),
                },
                container: $pjaxContainer,
                push: false,
                timeout: 10000,
            });
        }
    });
    
    $(document).on("change", "#logisticsrequest-customeragreement, #logisticsrequest-carrieragreement", function(e) {
        var value = $(this).val() || $(this).text();
        
        if ($(this).attr("id") == "logisticsrequest-customeragreement") {
            $contractorID = $("#logisticsrequest-customer_id").val();
            $type = 2;
        } else {
            $contractorID = $("#logisticsrequest-carrier_id").val();
            $type = 1;
        }
        if (value == "add-modal-agreement") {
            e.preventDefault();
    
            $.pjax({
                url: "/documents/agreement/create?contractor_id=" + $contractorID + "&type=" + $type + "&returnTo=request",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });
    
            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));
                $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);
    
                function dateChanged(ev) {
                    if (ev.bubbles == undefined) {
                        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                        if (ev.currentTarget.value == "") {
                            if ($input.data("last-value") == null) {
                                $input.data("last-value", ev.currentTarget.defaultValue);
                            }
                            var $lastDate = $input.data("last-value");
                            $input.datepicker("setDate", $lastDate);
                        } else {
                            $input.data("last-value", ev.currentTarget.value);
                        }
                    }
                };
            });
            $("#agreement-modal-container").modal("show");
            $(this).val("").trigger("change");
        }
    });
    $("#customerAgreement-pjax-container").on("pjax:complete", function() {
        if (window.AgreementValue) {
            $("#logisticsrequest-customeragreement").val(window.AgreementValue).trigger("change");
        }
    });
    $("#carrierAgreement-pjax-container").on("pjax:complete", function() {
        if (window.AgreementValue) {
            $("#logisticsrequest-carrieragreement").val(window.AgreementValue).trigger("change");
        }
    });
    $(document).on("change", "#logisticsrequest-driver_id", function (data) {
        loadDriverPhone($(this).val());
    });
    
    $(document).ready(function () {
        loadDriverPhone($("#logisticsrequest-driver_id").val());
    });
    function loadDriverPhone(driverID) {
        $.post("/logistics/driver/phone", {driver_id: driverID}, function (data) {
            if (data.result) {
                $(".driver-phone").text(data.phone);
            }
        });
    }
    
    $(document).on("change", "#logisticsrequest-carrier_id", function () {
        var $isOwnDisabled = $(".is_own-disabled");
        var $isOwnHide = $(".is_own-hide");
        
        if ($(this).val() == "is_own") {
            $("#logisticsrequest-is_own").val(1);
            $isOwnDisabled.val("").trigger("change").attr("disabled", true);
            $isOwnHide.find("textarea").val("");
            $isOwnHide.find("input:checkbox:checked").click();
            $isOwnHide.hide();       
        } else {
            $("#logisticsrequest-is_own").val(0);
            $isOwnDisabled.removeAttr("disabled");
            $isOwnHide.show();
        }
    });
    
    $(document).on("change", "#logisticsrequest-is_own", function (e) {
        var $isOwnDisabled = $(".is_own-disabled");
        var $isOwnHide = $(".is_own-hide");
        
        if ($(this).is(":checked")) {
            $isOwnDisabled.val("").trigger("change").attr("disabled", true);
            $isOwnHide.find("textarea").val("");
            $isOwnHide.find("input:checkbox:checked").click();
            $isOwnHide.hide();
        } else {
            $isOwnDisabled.removeAttr("disabled");
            $isOwnHide.show();
        }
    });
    
    $(document).on("change", "#logisticsrequest-customeragreement", function (data) {
        var $customerContractEssence = $(".has_customer_agreement-hide");
        
        if ($(this).val() == "" || $(this).val() == null) {
            $customerContractEssence.show();
        } else {
            $customerContractEssence.hide();
        }
    });
    
    $(document).on("change", "#logisticsrequest-carrieragreement", function (data) {
        var $carrierContractEssence = $(".has_carrier_agreement-hide");
        
        if ($(this).val() == "" || $(this).val() == null) {
            $carrierContractEssence.show();
        } else {
            $carrierContractEssence.hide();
        }
    });
    
    var $pickers = $(".datetimepicker .datetimepicker-minutes, .datetimepicker .datetimepicker-hours");
    $pickers.find("thead").remove();
    $pickers.find("tbody tr td").css("width", "185px");
    $("input.logisticsrequestLoadingandunloading-time").each(function () {
        if ($(this).data("hour") !== undefined && $(this).data("minute") !== undefined) {
            $(this).datetimepicker("show");
            $(".datetimepicker-hours:visible").find("tbody span.hour:contains(" + $(this).data("hour") +")").click();
            $(".datetimepicker-minutes:visible").find("tbody span.minute:contains(" + $(this).data("minute") +")").click();
        }
    });
    
    $(document).on("click", ".logistic-request-nav-form a", function (e) {
        if ($(this).hasClass("active")) {
            return;
        }
        $(".logistic-request-nav-form a").toggleClass("active");
        $(".tab-content-logistics-request").toggleClass("hidden");
    });
');
