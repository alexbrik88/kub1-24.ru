<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\vehicle\VehicleType;
use yii\helpers\Url;
use frontend\themes\kub\widgets\file\FileUpload;

/**
 * @var $activeTab int
 * @var $model \common\models\logisticsRequest\LogisticsRequest
 */

$icon = "<svg class='svg-icon'><use xlink:href='/img/svg/svgSprite.svg#{$model->getStatusIconNew()}'></use></svg>";
$createInvoiceText = $activeTab == LogisticsRequest::TYPE_CUSTOMER ? 'Счет' : 'Загрузить счет';
?>
<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
                background-color: #f2f3f7;
                border-color: #f2f3f7;
                color: #000;
                ">
            <span>
                <?= TextHelper::invoiceMoneyFormat(
                    ($activeTab == LogisticsRequest::TYPE_CARRIER ? $model->carrier_rate : $model->customer_rate) * 100,
                    2
                )?> ₽
            </span>
        </div>
    </div>
</div>
<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
                background-color: #f2f3f7;
                border-color: #f2f3f7;
                color: #000;
                ">
            <span>
                <?= date("d.m.Y", $model->status_updated_at); ?>
            </span>
        </div>
    </div>
</div>
<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
                background-color: #f2f3f7;
                border-color: #f2f3f7;
                color: #000;
                ">
            <?= $icon ?>
            <span>
                <?= $model->status->name; ?>
            </span>
        </div>
    </div>
</div>
<?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . Html::tag('span', $createInvoiceText), '#', [
    'class' => 'button-regular button-hover-content-red w-100 text-left',
    'title' => 'Добавить Счет',
]) ?>
<?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . Html::tag('span', 'Акт'), '#', [
    'class' => 'button-regular button-hover-content-red w-100 text-left',
    'title' => 'Добавить Акт',
]) ?>
<?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . Html::tag('span', 'Упд'), '#', [
    'class' => 'button-regular button-hover-content-red w-100 text-left',
    'title' => 'Добавить Упд',
]) ?>
<?php if ($model->company->companyTaxationType->osno): ?>
    <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . Html::tag('span', 'Счет-фактура'), '#', [
        'class' => 'button-regular button-hover-content-red w-100 text-left',
        'title' => 'Добавить СФ',
    ]) ?>
<?php endif; ?>

<div class="about-card mb-3">
    <div class="about-card-item">
        <span class="text-grey">
            Заказчик:
        </span>
        <?= Html::a(
            $model->customer->getNameWithType(),
            Url::to(['/contractor/view', 'id' => $model->customer_id, 'type' => $model->customer->type]),
            ['class' => 'link']
        ); ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Договор с заказчиком №:
        </span>
        <br>
        <?= $model->customer_agreement_document_number; ?> от
        <?= DateHelper::format($model->customer_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
    </div>
    <?php if (!$model->is_own): ?>
        <div class="about-card-item">
            <span class="text-grey">
                Перевозчик:
            </span>
            <br>
            <?= Html::a($model->carrier->getNameWithType(),
                Url::to(['/contractor/view', 'id' => $model->carrier_id, 'type' => $model->carrier->type])); ?>
        </div>
    <?php endif; ?>
    <?php if ($model->carrierAgreement && !$model->is_own): ?>
        <div class="about-card-item">
            <span class="text-grey">
                Договор с перевозчиком №:
            </span>
            <br>
            <?= $model->carrier_agreement_document_number; ?> от
            <?= DateHelper::format($model->carrier_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">
            ТС:
        </span>
        <br>
        <?= Html::a($model->vehicle->model . ', ' . $model->vehicle->state_number,
            Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id])); ?>
        ,
        <?php if ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType): ?>
            <?= Html::a($model->vehicle->semitrailerType->state_number,
                Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle->semitrailerType->id])); ?>
        <?php elseif ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType): ?>
            <?= Html::a($model->vehicle->trailerType->state_number,
                Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle->trailerType->id])); ?>
        <?php endif; ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Водитель:
        </span>
        <br>
        <?= Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id,])); ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Ответственный:
        </span>
        <br>
        <?= $model->author->getFio(); ?>
    </div>
    <div class="about-card-item">
        <?= FileUpload::widget([
            'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
            'listUnderButton' => false,
        ]); ?>
    </div>
</div>
