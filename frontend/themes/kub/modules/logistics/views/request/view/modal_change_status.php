<?php

/** @var $model \common\models\logisticsRequest\LogisticsRequest */

use common\components\date\DateHelper;
use common\components\helpers\Html;
use yii\helpers\Url;

?>
<div class="modal fade" id="change-status" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
                <div class="form-body">
                    <div class="row">
                        <label class="col-4 control-label" style="margin-top: 12px; max-width: 200px;">
                            <span class="status-name"></span> дата:
                        </label>
                        <div class="col-8">
                            <div class="form-group">
                                <?= Html::textInput('date', date(DateHelper::FORMAT_USER_DATE), [
                                    'class' => 'form-control date-picker ico width_date modal-document-date',
                                    'id' => 'under-date',
                                    'data-date-viewmode' => 'years',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row action-buttons">
                        <div class="spinner-button col">
                            <?= Html::a('Сохранить', 'javascript:;', [
                                'class' => 'button-regular button-regular_red change-status_submit',
                                'data-url' => Url::to(['update-status', 'id' => $model->id, 'status' => '']),
                                'style' => 'width: 130px!important;',
                            ]); ?>
                        </div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        </div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        </div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        </div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        </div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        </div>
                        <div class="col">
                            <?= Html::a('Отменить', '#',[
                                'class' => 'button-regular button-width button-hover-transparent',
                                'style' => 'width: 130px!important; float: right;',
                                'data-dismiss' => 'modal',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
