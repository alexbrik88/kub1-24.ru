<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\vehicle\VehicleType;

/* @var $model \common\models\logisticsRequest\LogisticsRequest
 * @var $type integer
 */

if ($type == LogisticsRequest::TYPE_CARRIER) {
    $contractor = $model->carrier;
    $agreement = 'carrierAgreement';
    $prefix = 'carrier';
} else {
    $contractor = $model->customer;
    $agreement = 'customerAgreement';
    $prefix = 'customer';
}
$contactPersonAttribute = $prefix . '_contact_person';
$formID = $prefix . '_form_id';
$form = $prefix . 'Form';
$conditionID = $prefix . '_condition_id';
$condition = $prefix . 'Condition';
$delay = $prefix . '_delay';
$rate = $prefix . '_rate';
$essence = $prefix . '_request_essence';

$directorName = null;
if (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
    $directorName = TextHelper::nameShort($contractor->director_name);
}
$printLink = (!$model->company->print_link) ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 100, 100, EasyThumbnailImage::THUMBNAIL_INSET);
?>
<div class="bold" style="font-size: 15px;">
    <?= $model->getTitle($type); ?>
</div>
<div class="col-12 pad0" style="padding-top: 15px !important;font-size: 14px;">
    <div class="col-12 pad0 bord-dark" style="border-bottom: 0">
        <div class="col-12 pad0" style="display: flex;">
            <div class="col-6 pad5 bord-dark-r">
                <div class="col-12 pad0">
                    <b>Заказчик</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad0">
                        <?= $model->company->getTitle(true); ?>
                    </div>
                    <div class="col-12 pad0">
                        Тел: <?= $model->company->phone; ?>
                    </div>
                    <div class="col-12 pad0">
                        ФИО: <?= $model->company->getChiefFio(); ?>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad0">
                        <?= $contractor->getNameWithType(); ?>
                    </div>
                    <div class="col-12 pad0">
                        Тел: <?= $contractor->getRealContactPhone(); ?>
                    </div>
                    <div class="col-12 pad0">
                        ФИО: <?= $contractor->director_name; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-6 pad5">
                <div class="col-12 pad0">
                    <b>Исполнитель</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad0">
                        <?= $contractor->getNameWithType(); ?>
                    </div>
                    <div class="col-12 pad0">
                        Тел: <?= $contractor->getRealContactPhone(); ?>
                    </div>
                    <div class="col-12 pad0">
                        ФИО: <?= $contractor->director_name; ?>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad0">
                        <?= $model->company->getTitle(true); ?>
                    </div>
                    <div class="col-12 pad0">
                        Тел: <?= $model->company->phone; ?>
                    </div>
                    <div class="col-12 pad0">
                        ФИО: <?= $model->company->getChiefFio(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <b>Маршрут</b>
            </div>
            <div class="col-9 pad5 bord-dark-t min-h-27">
                <?= $model->loading->city . ' - ' . $model->unloading->city; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <b>Погрузка</b>
            </div>
            <div class="col-9 pad5 bord-dark-t min-h-27">
                <?= ($model->loading->date ?
                    ('<b>Дата: </b>' . DateHelper::format($model->loading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)) :
                    null) . ' ' .
                ($model->loading->time ?
                    ('<b>Время: </b>' . DateHelper::format($model->loading->time, 'H:i', 'H:i:s')) : null); ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Адрес
            </div>
            <div class="col-9 pad5 bord-dark-t min-h-27">
                <?= $model->loading->city . ' ' . $model->loading->address; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Контактное лицо
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <?= $model->loading->contact_person; ?>
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Телефон
            </div>
            <div class="col-3 pad5 bord-dark-t min-h-27">
                <?= $model->loading->contact_person_phone; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <b>Разгрузка</b>
            </div>
            <div class="col-9 pad5 bord-dark-t min-h-27">
                <?= ($model->unloading->date ?
                    ('<b>Дата: </b>' . DateHelper::format($model->unloading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)) :
                    null) . ' ' .
                ($model->unloading->time ?
                    ('<b>Время: </b>' . DateHelper::format($model->unloading->time, 'H:i', 'H:i:s')) : null); ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Адрес
            </div>
            <div class="col-9 pad5 bord-dark-t min-h-27">
                <?= $model->unloading->city . ' ' . $model->unloading->address; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Контактное лицо
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <?= $model->unloading->contact_person; ?>
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Телефон
            </div>
            <div class="col-3 pad5 bord-dark-t min-h-27">
                <?= $model->unloading->contact_person_phone; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r" style="height: 70px;padding-top: 23px!important;">
                <b>Груз</b>
            </div>
            <div class="col-9 pad0 bord-dark-t">
                <div class="col-12 pad5">
                    <?= $model->goods_name; ?>
                </div>
                <div class="col-12 pad0" style="display: flex;">
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="height: 43px;">
                        <b>Вес (тонн):</b> <?= $model->goods_weight; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="height: 43px;">
                        <b>Габариты:</b> <?= "{$model->goods_length}x{$model->goods_width}x{$model->goods_height}=" .
                        $model->goods_length * $model->goods_width * $model->goods_height . " м3"; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-t" style="height: 43px;">
                        <b>Кол-во мест:</b> <?= $model->goods_count; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Способ погрузки
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                <?= $model->loading->method; ?>
            </div>
            <div class="col-3 pad5 bord-dark-t bord-dark-r min-h-27">
                Способ разгрузки
            </div>
            <div class="col-3 pad5 bord-dark-t min-h-27">
                <?= $model->unloading->method; ?>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r" style="height: 88px;padding-top: 33px!important;">
                <b>Стоимость услуги</b>
            </div>
            <div class="col-9 pad0 bord-dark-t">
                <div class="col-12 pad5">
                    <b><?= $model->$rate; ?> руб.</b>
                </div>
                <div class="col-12 pad0" style="display: flex;">
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="min-height: 61px;">
                        <b>Форма оплаты:</b> <?= $model->$formID ? $model->$form->name : null; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="min-height: 61px;">
                        <b>Условие оплаты:</b> <?= $model->$conditionID ? $model->$condition->name : null; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-t" style="min-height: 61px;">
                        <b>Оплата:</b>
                        <?php if ($model->$delay): ?>
                            Через <?= $model->$delay; ?>
                            <?php if ($model->$delay == 1): ?>
                                банковский день
                            <?php elseif ($model->$delay > 1 && $model->$delay < 5): ?>
                                банковских дня
                            <?php else: ?>
                                банковских дней
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <?php $hasTrailer = ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType) ||
                ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType); ?>
            <div class="col-3 pad5 bord-dark-t bord-dark-r"
                 style="height: <?= $hasTrailer ? '98px' : '71px' ?>;padding-top: <?= $hasTrailer ? '33px' : '15px' ?>!important;">
                <b>Транспортное средство</b>
            </div>
            <div class="col-9 pad0 bord-dark-t">
                <div class="col-12 pad0" style="display: flex;">
                    <div class="col-4 pad5 bord-dark-r min-h-27">
                        <?= $model->vehicle->vehicleType->name; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-r min-h-27">
                        <?= $model->vehicle->model; ?>
                    </div>
                    <div class="col-4 pad5 min-h-27">
                        <?= $model->vehicle->state_number; ?>
                    </div>
                </div>
                <?php if ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType): ?>
                    <div class="col-12 pad0" style="display: flex;">
                        <div class="col-4 pad5 bord-dark-t bord-dark-r min-h-27">
                            <?= $model->vehicle->semitrailerType->vehicleType->name; ?>
                        </div>
                        <div class="col-4 pad5 bord-dark-t bord-dark-r min-h-27">
                            <?= $model->vehicle->semitrailerType->model; ?>
                        </div>
                        <div class="col-4 pad5 bord-dark-t min-h-27">
                            <?= $model->vehicle->semitrailerType->state_number; ?>
                        </div>
                    </div>
                <?php elseif ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType): ?>
                    <div class="col-12 pad0" style="display: flex;">
                        <div class="col-4 pad5 bord-dark-t bord-dark-r min-h-27">
                            <?= $model->vehicle->trailerType->vehicleType->name; ?>
                        </div>
                        <div class="col-4 pad5 bord-dark-t bord-dark-r min-h-27">
                            <?= $model->vehicle->trailerType->model; ?>
                        </div>
                        <div class="col-4 pad5 bord-dark-t min-h-27">
                            <?= $model->vehicle->trailerType->state_number; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-12 pad0" style="display: flex;">
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="height: 43px;">
                        <b>Тип кузова:</b> <?= $model->vehicle->bodyType ? $model->vehicle->bodyType->name : null; ?>
                    </div>
                    <div class="col-4 pad5 bord-dark-t bord-dark-r" style="height: 43px;">
                        <b>Грузоподъемность:</b> <?= $model->vehicle->getTonnageFull(); ?> т.
                    </div>
                    <div class="col-4 pad5 bord-dark-t" style="height: 43px;">
                        <b>Объем:</b> <?= $model->vehicle->getVolumeFullWithParams(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-3 pad5 bord-dark-t bord-dark-r" style="height: 55px;padding-top: 14px!important;">
                <b>Водитель</b>
            </div>
            <div class="col-9 pad0 bord-dark-t">
                <div class="col-12 pad0" style="display: flex;">
                    <div class="col-8 pad5 bord-dark-r min-h-27">
                        <?= $model->driver->getFio(); ?>
                    </div>
                    <div class="col-4 pad5 min-h-27">
                        <?= $model->driver->mainDriverPhone ? $model->driver->mainDriverPhone->phone : null; ?>
                    </div>
                </div>
                <div class="col-12 pad5 bord-dark-t min-h-27">
                    <?php $identificationDate = DateHelper::format($model->driver->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    <b><?= $model->driver->identificationType->name ?>:</b>
                    <?= "{$model->driver->identification_series}, 
                {$model->driver->identification_number}, 
                {$model->driver->identification_issued_by}, 
                {$identificationDate}"; ?>
                </div>
            </div>
        </div>

        <div class="col-12 pad5 bord-dark-t min-h-27">
            <?= $model->$essence; ?>
        </div>

        <div class="col-12 pad0" style="display: flex;">
            <div class="col-6 pad5 bord-dark-r bord-dark-t">
                <div class="col-12 pad1">
                    <b>Заказчик</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad1">
                        <b><?= $model->company->getTitle(true); ?></b>
                    </div>
                    <div class="col-12 pad1">
                        <b>Юр. адрес:</b> <?= $model->company->getAddressActualFull(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Почт. адрес:</b> <?= $model->company->getAddressLegalFull(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ИНН / КПП:</b> <?= $model->company->inn . ' / ' . $model->company->kpp; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ОГРН:</b> <?= $model->company->ogrn; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Р/с:</b>
                        <?= $model->company->mainCheckingAccountant ?
                            ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Кор/с:</b> <?= $model->company->mainCheckingAccountant->ks; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>БИК:</b> <?= $model->company->mainCheckingAccountant->bik; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Тел:</b> <?= $model->company->phone; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Email:</b> <?= $model->company->email; ?>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad1">
                        <b><?= $contractor->getNameWithType(); ?></b>
                    </div>
                    <div class="col-12 pad1">
                        <b>Юр. адрес:</b> <?= $contractor->legal_address; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Почт. адрес:</b> <?= $contractor->actual_address; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ИНН / КПП:</b> <?= $contractor->ITN . ' / ' . $contractor->PPC; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ОГРН:</b> <?= $contractor->BIN; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Р/с:</b>
                        <?= $contractor->current_account ?
                            ($contractor->current_account . ' в ' . $contractor->bank_name) : null; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Кор/с:</b> <?= $contractor->corresp_account; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>БИК:</b> <?= $contractor->BIC; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Тел:</b> <?= $contractor->getRealContactPhone(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Email:</b> <?= $contractor->getRealContactEmail(); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-6 pad5 bord-dark-t">
                <div class="col-12 pad1">
                    <b>Исполнитель</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad1">
                        <b><?= $contractor->getNameWithType(); ?></b>
                    </div>
                    <div class="col-12 pad1">
                        <b>Юр. адрес:</b> <?= $contractor->legal_address; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Почт. адрес:</b> <?= $contractor->actual_address; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ИНН / КПП:</b> <?= $contractor->ITN . ' / ' . $contractor->PPC; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ОГРН:</b> <?= $contractor->BIN; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Р/с:</b>
                        <?= $contractor->current_account ?
                            ($contractor->current_account . ' в ' . $contractor->bank_name) : null; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Кор/с:</b> <?= $contractor->corresp_account; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>БИК:</b> <?= $contractor->BIC; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Тел:</b> <?= $contractor->getRealContactPhone(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Email:</b> <?= $contractor->getRealContactEmail(); ?>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad1">
                        <b><?= $model->company->getTitle(true); ?></b>
                    </div>
                    <div class="col-12 pad1">
                        <b>Юр. адрес:</b> <?= $model->company->getAddressActualFull(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Почт. адрес:</b> <?= $model->company->getAddressLegalFull(); ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ИНН / КПП:</b> <?= $model->company->inn . ' / ' . $model->company->kpp; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>ОГРН:</b> <?= $model->company->ogrn; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Р/с:</b>
                        <?= $model->company->mainCheckingAccountant ?
                            ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Кор/с:</b> <?= $model->company->mainCheckingAccountant->ks; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>БИК:</b> <?= $model->company->mainCheckingAccountant->bik; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Тел:</b> <?= $model->company->phone; ?>
                    </div>
                    <div class="col-12 pad1">
                        <b>Email:</b> <?= $model->company->email; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-12 pad0 bord-dark-t" style="border-bottom: 1px solid #000000;display: flex;">
            <div class="col-6 pad5 bord-dark-r" style="padding-bottom: 80px!important;">
                <div class="col-12 pad1">
                    <b>Генеральный директор</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad0">
                        <?php if ($printLink): ?>
                            <div style="text-align:center;position:absolute; right: 0; z-index: 0;top:10px;left: 145px">
                                <img class="img-responsive" style="width:150px" src="<?= $printLink; ?>" alt="">
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-12 pad5" style="position: relative;top: 16px;text-align:right;margin-top: 65px;">
                        <?= '/ ' . $model->company->getChiefFio(true) . ' /'; ?>
                    </div>
                    <div class="col-12 pad0"
                         style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                        <div class="col-7 pad5">
                            <div class="bord-dark-t"></div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad5 v_bottom" style="position: relative;top: 16px;text-align: right;margin-top: 65px;">
                        <?= !empty($directorName) ? '/ ' . $directorName . ' /' : ''; ?>
                    </div>
                    <div class="col-12 pad0" style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                        <div class="col-7 pad5">
                            <div class="bord-dark-t"></div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-6 pad5" style="padding-bottom: 80px!important;">
                <div class="col-12 pad1">
                    <b>Генеральный директор</b>
                </div>
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <div class="col-12 pad5" style="position: relative;top: 16px;text-align:right;margin-top: 65px;">
                        <?= !empty($directorName) ? '/ ' . $directorName . ' /' : ''; ?>
                    </div>
                    <div class="col-12 pad0" style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                        <div class="col-7 pad5">
                            <div class="bord-dark-t"></div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-12 pad0">
                        <?php if ($printLink): ?>
                            <div style="text-align:center;position:absolute; right: 0; z-index: 0;left: 145px;top: 10px;">
                                <img class="img-responsive" style="width:150px" src="<?= $printLink; ?>" alt="">
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-12 pad5" style="position: relative;top: 16px;text-align: right;margin-top: 65px;">
                        <?= '/ ' . $model->company->getChiefFio(true) . ' /'; ?>
                    </div>
                    <div class="col-12 pad0"
                         style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                        <div class="col-7 pad5">
                            <div class="bord-dark-t"></div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
