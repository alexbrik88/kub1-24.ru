<?php

use common\components\image\EasyThumbnailImage;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\logisticsRequest\LogisticsRequestStatus;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;

/* @var $this yii\web\View
 * @var $model \common\models\logisticsRequest\LogisticsRequest
 * @var $activeTab integer
 */

$tabItems = [];
$tabItems[] = [
    'label' => 'С заказчиком',
    'url' => Url::to(['view', 'id' => $model->id, 'activeTab' => LogisticsRequest::TYPE_CUSTOMER]),
    'active' => $activeTab == LogisticsRequest::TYPE_CUSTOMER,
    'options' => ['class' => 'nav-item'],
    'linkOptions' => ['class' => 'nav-link'],
];
if (!$model->is_own) {
    $tabItems[] = [
        'label' => 'С перевозчиком',
        'url' => Url::to(['view', 'id' => $model->id, 'activeTab' => LogisticsRequest::TYPE_CARRIER]),
        'active' => $activeTab == LogisticsRequest::TYPE_CARRIER,
        'options' => ['class' => 'nav-item'],
        'linkOptions' => ['class' => 'nav-link'],
    ];
}

$statusItems = [];
/** @var LogisticsRequestStatus $status */
foreach (LogisticsRequestStatus::find()->all() as $status) {
    $statusItems[] = [
        'label' => $status->name,
        'encode' => false,
        'url' => $status->id == LogisticsRequestStatus::STATUS_REJECTED ?
            Url::to(['update-status', 'id' => $model->id, 'status' => $status->id]) :
            '#change-status',
        'linkOptions' => [
            'class' => 'change-status-trigger',
            'data-toggle' => $status->id == LogisticsRequestStatus::STATUS_REJECTED ? null : 'modal',
            'data-id' => $status->id,
        ],
    ];
}

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);
$this->title = $model->getTitle($activeTab);
?>
<?= Html::a('Назад к списку', Url::to(['index']), ['class' => 'link mb-2']); ?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2" style="padding-left: 15px;padding-right: 15px;">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => $tabItems,
        ]); ?>
    </div>
    <div class="wrap wrap_padding_small">
        <div class="page-in row">
            <div class="page-in-content column">
                <div class="page-border">
                    <?= DocumentLogWidget::widget([
                        'model' => $model,
                        'toggleButton' => [
                            'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                            'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                            'title' => 'Последние действия',
                        ],
                    ]); ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), Url::to(['update', 'id' => $model->id]), [
                        'title' => 'Редактировать',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                    ]) ?>
                    <div class="doc-container doc-preview">
                        <div class="col-12 pad0">
                            <div class="<?= $activeTab == LogisticsRequest::TYPE_CARRIER && !$model->is_own ? 'col-7' : 'col-12' ?> pad0"
                                 style="display: inline-block;">
                                <div style="padding-top: 12px;padding-bottom: 10px;">
                                    <span style="font-weight: 600;font-size: 20px;">
                                        <?= $activeTab == LogisticsRequest::TYPE_CARRIER ?
                                            $model->company->getTitle(true) : $model->customer->getNameWithType(); ?>
                                    </span>
                                </div>
                            </div>
                            <?php if ($activeTab == LogisticsRequest::TYPE_CARRIER && !$model->is_own): ?>
                                <div class="col-4 pad0" style="display: inline-block;float: right;text-align: right;">
                                    <div>
                                        <?php if ($logoLink): ?>
                                            <img style="max-height: 70px;max-width: 170px;" src="<?= $logoLink ?>"
                                                 alt="">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?= $this->render('view/_main_info', [
                            'model' => $model,
                            'type' => $activeTab,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="page-in-sidebar column">
                <?= $this->render('view/_status_block', [
                    'model' => $model,
                    'activeTab' => $activeTab,
                ]) ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'envelope']) . '<span>Отправить</span>', [
                'class' => 'button-clr button-regular button-hover-transparent w-full',
                'data-toggle' => 'toggleVisible',
                'data-target' => 'invoice',
            ]) ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php $printUrl = [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $activeTab,
                'filename' => $model->getPrintTitle(),
            ]; ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']) . '<span>Печать</span>', $printUrl, [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => [
                                'document-print',
                                'actionType' => 'pdf',
                                'id' => $model->id,
                                'type' => $activeTab,
                                'filename' => $model->getPrintTitle(),
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => ['docx', 'id' => $model->id, 'type' => $activeTab],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                'message' => 'Вы уверены, что хотите скопировать эту заявку?',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'check-2']) . '<span>Статус</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr status-dropdown',
                    ],
                    'items' => $statusItems,
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить заявку?",
            ]); ?>
        </div>
    </div>
</div>
<?= $this->render('view/modal_change_status', [
    'model' => $model,
]); ?>
<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
    'requestType' => $activeTab,
]); ?>
<?php $this->registerJs('
    $(document).on("click", ".status-dropdown a", function () {
        var $submitStatus = $("#change-status.modal .change-status_submit");
        $("#change-status.modal .status-name").text($(this).text().trim());
        $submitStatus.attr("data-url", $submitStatus.attr("data-url") + $(this).data("id"));
    });
    
    $(document).on("click", "#change-status.modal .change-status_submit", function (e) {
        $.post($(this).data("url"), {date: $("#change-status.modal .modal-document-date").val()}, function (data) {});
    });
'); ?>