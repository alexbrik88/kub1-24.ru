<?php

use yii\bootstrap\Nav;

/** @var $content string */

$this->beginContent('@frontend/views/layouts/main.php');
?>
<div class="debt-report-content nav-finance logistics-block">
    <div class="nav-tabs-row mb-2">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Водители',
                    'url' => ['/logistics/driver/index'],
                    'active' => Yii::$app->controller->id == 'driver',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Транспортные средства',
                    'url' => ['/logistics/vehicle/index'],
                    'active' => Yii::$app->controller->id == 'vehicle',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Адреса Погрузки/Разгрузки',
                    'url' => ['/logistics/address/index'],
                    'active' => Yii::$app->controller->id == 'address',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]); ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>
