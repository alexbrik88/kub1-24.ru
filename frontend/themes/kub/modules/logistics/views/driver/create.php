<?php

/* @var $this yii\web\View */
/* @var $model \common\models\driver\Driver */
/* @var $company \common\models\Company */
/* @var $newDriverPhone \common\models\driver\DriverPhone */

$this->title = 'Добавить водителя';
?>
<div class="driver-create">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'newDriverPhone' => $newDriverPhone,
    ]); ?>
</div>
<?php $this->registerJs('
    $(document).ready(function (e) {
        var $driverPhoneBlock = $(".template-driver_phone_block").clone();
        
        $driverPhoneBlock.find("input[type=\'checkbox\']").uniform("refresh").click();
        $driverPhoneBlock.find("#driverphone-phone-new_0").inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
        
        $driverPhoneBlock.removeClass("hidden")
            .removeClass("template-driver_phone_block")
            .addClass("drive_phone-block");
            
        $driverPhoneBlock.find("#driverphone-phone_type_id-new_0")
            .attr("name", "DriverPhone[new][0][phone_type_id]");
            
        $driverPhoneBlock.find("#driverphone-phone-new_0")
            .attr("name", "DriverPhone[new][0][phone]")
            .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
            
        $driverPhoneBlock.find("input[type=\'checkbox\']")
            .attr("name", "DriverPhone[new][0][is_main]")
            .uniform("refresh");
        
        $(".driver_phone-list").append($driverPhoneBlock);
    });
');