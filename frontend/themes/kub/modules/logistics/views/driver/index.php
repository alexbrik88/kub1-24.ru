<?php

use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\driver\Driver;
use frontend\widgets\TableViewWidget;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\DriverSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $company \common\models\Company
 * @var $user \common\models\employee\Employee
 */

$this->title = 'Водители';
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$tableViewClass = $user->config->getTableViewClass('table_view_logistics');
?>
<div class="wrap wrap_padding_small pl-4 pr-2 pt-2 pb-0 mb-4">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <h4 class="mt-1 mb-2"><?= $this->title ?></h4>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-1 text-right">
                    <?= Html::a($addIcon . '<span>Добавить</span>', Url::to(['create']), [
                        'class' => 'button-regular button-regular_red button-width',
                        'style' => 'width: 150px;',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_view_logistics']) ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => "table table-style table-count-list table-logistics {$tableViewClass}",
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'emptyText' => 'Вы еще не добавили ни одного водителя. ' . Html::a('Добавить', Url::to(['create'])),
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'fio',
            'label' => 'Фамилия Имя Отчество',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Driver $model) {
                return Html::a($model->getFio(), ['view', 'id' => $model->id]);
            },
        ],
        [
            'filter' => $searchModel->getVehicleFilter(),
            'hideSearch' => false,
            's2width' => '220px',
            'attribute' => 'vehicle_id',
            'label' => 'ТС',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (Driver $model) {
                $name = null;
                if ($model->vehicle) {
                    $name .= $model->vehicle->getVehicleTypeText();
                    if ($model->vehicle->model) {
                        $name .= (", {$model->vehicle->model}");
                    }
                    $name .= (" {$model->vehicle->state_number}");
                }
                return Html::a($name, Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id]));
            },
        ],
        [
            'attribute' => 'home_town',
            'label' => 'Город базирования',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Driver $model) {
                return $model->home_town;
            },
        ],
        [
            'attribute' => 'phone',
            'label' => 'Телефон',
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Driver $model) {
                return $model->mainDriverPhone ?
                    ($model->mainDriverPhone->phoneType->name . ': ' . $model->mainDriverPhone->phone) :
                    '';
            },
        ],
        [
            'filter' => $searchModel->getContractorFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'contractor_id',
            'label' => 'Где работает',
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (Driver $model) {
                return $model->contractor ?
                    Html::a($model->contractor->nameWithType, Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                    '';
            },
        ],
    ],
]); ?>
