<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\driver\IdentificationType;
use common\models\driver\PhoneType;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $company \common\models\Company */
/* @var $model \common\models\driver\Driver */
/* @var $newDriverPhone \common\models\driver\DriverPhone */

$cancelUrl = Url::previous('lastPage');
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$closeIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg>';
?>
<?php $form = ActiveForm::begin([
    'id' => 'vehicle-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
]); ?>

<?= $form->errorSummary($model); ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-12" style="display: flex;">
            <div class="form-group col-3 pad0" style="display: inline-block;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Фамилия:</label>
                </div>
                <?= $form->field($model, 'last_name', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Имя:</label>
                </div>
                <?= $form->field($model, 'first_name', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Отчество:</label>
                </div>
                <?= $form->field($model, 'patronymic', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="col-3 pad0" style="padding-top: 4px;">
                <?= $form->field($model, 'has_no_patronymic', ['labelOptions' => ['class' => 'pt-4 mt-2 ml-4']])
                    ->label('Нет отчества')
                    ->checkbox(['template' => '{beginLabel}{input}{labelTitle}{endLabel}{error}{hint}'], true); ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap body-fields">
    <div class="row d-block">
        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Где работает:</label>
                </div>
                <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить покупателя']
                        + $company->sortedContractorList(Contractor::TYPE_SELLER),
                    'options' => [
                        'class' => 'form-control',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Транспортное средство:</label>
                </div>
                <?= $form->field($model, 'vehicle_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => $company->getVehicleList(),
                    'options' => [
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-3">
                <div class="form-filter">
                    <label class="label" for="input3">Дата рождения:</label>
                </div>
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'birthday_date', [
                                'class' => 'form-control date-picker',
                                'value' => DateHelper::format($model->birthday_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Документ:</label>
                </div>
                <?= $form->field($model, 'identification_type_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(IdentificationType::find()->orderBy('sort')->all(), 'id', 'name'),
                    'options' => [
                        'id' => 'driver-identification_type_id',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 is_own_block">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Серия:</label>
                </div>
                <?= $form->field($model, 'identification_series', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Номер</label>
                </div>
                <?= $form->field($model, 'identification_number', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter">
                    <label class="label" for="input3">Дата выдачи:</label>
                </div>
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap field-driver-identification_date required">
                            <?= $form->field($model, 'identification_date', [
                                'template' => '{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>',
                                'options' => [
                                    'class' => '',
                                ]
                            ])->textInput([
                                'class' => 'form-control date-picker',
                                'aria-required' => 'true',
                                'aria-invalid' => 'true',
                                'value' => DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Кем выдан:</label>
                </div>
                <?= $form->field($model, 'identification_issued_by', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="form-group col-6">
            <div class="form-filter" style="display: inline-block;">
                <label class="label" for="cause">Водительское удостоверение:</label>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Серия:</label>
                </div>
                <?= $form->field($model, 'driver_license_series', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Номер</label>
                </div>
                <?= $form->field($model, 'driver_license_number', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter">
                    <label class="label" for="input3">Дата выдачи:</label>
                </div>
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap field-driver-driver_license_date required">
                            <?= $form->field($model, 'driver_license_date', [
                                'template' => '{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>',
                                'options' => [
                                    'class' => '',
                                ]
                            ])->textInput([
                                'class' => 'form-control date-picker',
                                'aria-required' => 'true',
                                'aria-invalid' => 'true',
                                'value' => DateHelper::format($model->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Кем выдан:</label>
                </div>
                <?= $form->field($model, 'driver_license_issued_by', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="form-group col-6">
            <div class="form-filter" style="display: inline-block;">
                <label class="label" for="cause">Телефоны:</label>
            </div>
        </div>
        <div class="driver_phone-list">
            <?php foreach ($model->driverPhones as $driverPhone): ?>
                <div class="row drive_phone-block col-12">
                    <div class="form-group col-3">
                        <?= $form->field($driverPhone, 'phone_type_id', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->dropDownList(ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                            'class' => 'form-control',
                            'id' => "driverphone-phone_type_id-{$driverPhone->id}",
                            'name' => "DriverPhone[{$driverPhone->id}][phone_type_id]",
                        ]); ?>
                    </div>
                    <div class="form-group col-3">
                        <?= MaskedInput::widget([
                            'model' => $driverPhone,
                            'attribute' => 'phone',
                            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                            'options' => [
                                'id' => "driverphone-phone-{$driverPhone->id}",
                                'class' => 'form-control',
                                'placeholder' => '+7(___) ___-__-__',
                                'name' => "DriverPhone[{$driverPhone->id}][phone]",
                            ],
                        ]); ?>
                    </div>
                    <div class="form-group col-3" style="padding-top: 11px;">
                        <?= Html::activeCheckbox($driverPhone, 'is_main', [
                            'class' => '',
                            'label' => 'Основной',
                            'id' => "driverphone-is_main-{$driverPhone->id}",
                            'name' => "DriverPhone[{$driverPhone->id}][is_main]",
                        ]); ?>
                    </div>
                    <div class="col-1" style="display: inline-block;position: relative;bottom: 3px;">
                        <span class="icon-close delete-driver_phone" style="font-size: 20px;cursor: pointer;">
                            <?= $closeIcon ?>
                        </span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?= $form->field($model, 'phone', [
            'options' => [
                'class' => 'form-group',
                'style' => 'position: relative;top: -15px;margin-bottom: 0;',
            ],
        ])->hiddenInput()->label(false); ?>

        <div class="form-group col-3">
            <span class="add-driver_phone" style="margin-bottom: 15px;">
                <?= "{$addIcon} ДОБАВИТЬ ТЕЛЕФОН" ?>
            </span>
        </div>

        <div class="template-driver_phone_block row col-12 hidden">
            <div class="form-group col-3">
                <?= Html::activeDropDownList($newDriverPhone, 'phone_type_id',
                    ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                        'class' => 'form-control',
                        'id' => 'driverphone-phone_type_id-new_0',
                        'name' => '',
                    ]); ?>
            </div>
            <div class="form-group col-3">
                <?= MaskedInput::widget([
                    'model' => $newDriverPhone,
                    'attribute' => 'phone',
                    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                    'options' => [
                        'id' => 'driverphone-phone-new_0',
                        'class' => 'form-control',
                        'placeholder' => '+7(___) ___-__-__',
                        'name' => '',
                    ],
                ]); ?>
            </div>
            <div class="form-group col-3" style="padding-top: 11px;">
                <?= Html::activeCheckbox($newDriverPhone, 'is_main', [
                    'class' => 'no-uniform',
                    'label' => 'Основной',
                    'id' => 'driverphone-is_main-new_0',
                    'name' => '',
                ]); ?>
            </div>
            <div class="col-1" style="display: inline-block;position: relative;bottom: 3px;">
                <span class="icon-close delete-driver_phone" style="font-size: 20px;cursor: pointer;">
                    <?= $closeIcon ?>
                </span>
            </div>
        </div>

        <div class="form-group col-6">
            <div class="form-filter" style="display: inline-block;">
                <label class="label" for="cause">Адреса:</label>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Город базирования:</label>
                </div>
                <?= $form->field($model, 'home_town', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Адрес проживания:</label>
                </div>
                <?= $form->field($model, 'residential_address', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Адрес регистрации:</label>
                </div>
                <?= $form->field($model, 'registration_address', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Комментарий:</label>
                </div>
                <?= $form->field($model, 'comment', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->textarea([
                    'rows' => 6,
                ]); ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php $this->registerJs('  
    var $phoneTmpID = 1;
    $(document).on("click", ".add-driver_phone", function (e) {
        var $phoneList = $(".driver_phone-list");
        var $phoneListLength = $phoneList.find(".drive_phone-block").length;
        
        if ($phoneListLength < 5) {
            var $driverPhoneBlock = $(".template-driver_phone_block").clone();
            
            $driverPhoneBlock.removeClass("hidden")
                .removeClass("template-driver_phone_block")
                .addClass("drive_phone-block");
            
            $driverPhoneBlock.find("#driverphone-phone_type_id-new_0")
                .attr("id", "driverphone-phone_type_id-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][phone_type_id]");
            
            $driverPhoneBlock.find("#driverphone-phone-new_0")
                .attr("id", "driverphone-phone-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][phone]")
                .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
                
            $driverPhoneBlock.find("input[type=\'checkbox\']")
                .attr("id", "driverphone-is_main-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][is_main]")
                .uniform("refresh");
            if ($phoneListLength == 0) {
                $driverPhoneBlock.find("input[type=\'checkbox\']").click();
            }
            $(".driver_phone-list").append($driverPhoneBlock);
            $phoneTmpID++;
        }
    });
    
    $(document).on("click", ".driver_phone-list .drive_phone-block .delete-driver_phone", function (e) {        
        if ($(".driver_phone-list .drive_phone-block").length > 1) {
            $(this).closest(".drive_phone-block").remove();
            if ($(".drive_phone-block input[type=\'checkbox\']:checked").length == 0) {
                $(".drive_phone-block input[type=\'checkbox\']:first").click();
            }
            checkPhone();
        }
    });
    
    $(document).on("change", ".drive_phone-block input[type=\'checkbox\']", function (e) {
       var $clickedID = $(this).attr("id");
       if ($(this).is(":checked")) {
           $(".drive_phone-block input[type=\'checkbox\']:checked:not(\'#" + $clickedID + "\')").prop("checked", false).uniform("refresh");
       } else if ($(".drive_phone-block input[type=\'checkbox\']:checked").length == 0) { 
           $(".drive_phone-block input[type=\'checkbox\']:first").prop("checked", true).uniform("refresh");
        }
    });
    
    $(document).on("change", ".driver_phone-list .drive_phone-block input:text", function (e) {
        checkPhone();
    });
    
    function checkPhone() {
        var $isSetPhone = false;
        $(".driver_phone-list .drive_phone-block input:text").each(function () {
            if ($(this).val() !== "") {
                $("#driver-phone").val($(this).val());
                $isSetPhone = true;
            }
        });
        if ($isSetPhone == false) {
            $("#driver-phone").val("");
        }
    }
');
