<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use frontend\themes\kub\modules\documents\widgets\CreatedByWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use frontend\themes\kub\widgets\file\FileUpload;

/* @var $this yii\web\View */
/* @var $model \common\models\driver\Driver */
/* @var $driverPhone \common\models\driver\DriverPhone */

$this->title = $model->getFio();
?>
<?= Html::a('Назад к списку', Url::to(['index']), ['class' => 'link mb-2']); ?>
<div class="wrap wrap_padding_small">
    <div class="page-in row">
       <div class="page-in-content-logistics column">
            <div class="page-border">
                <?= CreatedByWidget::widget([
                    'createdAt' => date("d.m.Y", $model->created_at),
                    'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                ]); ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), Url::to(['update', 'id' => $model->id]), [
                    'title' => 'Редактировать',
                    'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                ]) ?>
                <div class="doc-container doc-preview" style="margin-top: -50px;">
                    <div class="document-template">
                        <table style="margin-top: 60px;">
                            <tr>
                                <td style="font-weight: 600;font-size: 22px;">
                                    <?= $model->getFio(); ?>
                                </td>
                            </tr>
                        </table>
                        <table style="margin-top: 25px;">
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Где работает:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->contractor ?
                                        Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                        null; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Транспортное средство:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->vehicle ?
                                        Html::a($model->vehicle->getVehicleTypeText(), Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id])) :
                                        ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Дата рождения:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= DateHelper::format($model->birthday_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Документ:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->identificationType ? $model->identificationType->name : null; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Серия:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span><?= $model->identification_series; ?></span>
                                    <span style="margin-left: 30px;">Номер: <?= $model->identification_number; ?></span>
                                    <span style="padding-left: 10px;">
                                        Дата выдачи: <?= DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Кем выдан:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->identification_issued_by; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Водительское <br> удостоверение:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;"></td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Серия:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span><?= $model->driver_license_series; ?></span>
                                    <span style="margin-left: 30px;">Номер: <?= $model->driver_license_number; ?></span>
                                    <span style="padding-left: 10px;">
                                        Дата выдачи: <?= DateHelper::format($model->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Кем выдан:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->driver_license_issued_by; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Телефоны:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;"></td>
                            </tr>
                            <?php foreach ($model->getDriverPhones()->orderBy(['is_main' => SORT_DESC])->all() as $driverPhone): ?>
                                <tr>
                                    <td style="padding-bottom: 5px;">
                                        <?= $driverPhone->phoneType ? $driverPhone->phoneType->name : null; ?>:
                                    </td>
                                    <td style="padding-left: 20px;padding-bottom: 5px;">
                                        <?= $driverPhone->phone . ($driverPhone->is_main ? ' Основной' : null); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Адреса:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;"></td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Город базирования:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->home_town; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Адрес проживания:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->residential_address; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px;">
                                    Адрес регистрации:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->registration_address; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Комментарий:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->comment; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar-logistics column">
            <div class="about-card mb-3">
                <div class="about-card-item">
                    <div class="text-grey" style="margin-bottom: 15px;">Документы:</div>
                    <?= FileUpload::widget([
                        'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
                        'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
                        'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
                        'listUnderButton' => false,
                    ]); ?>
                </div>
            </div>
            <div class="about-card mb-3" style="border: none;padding-left: 0;padding-top: 0;">
                <div class="about-card-item">
                    <span style="font-weight: bold;margin-right: 10px;">Добавить комментарий</span>
                    <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'id' => 'comment_update',
                        'style' => 'cursor: pointer;',
                    ]); ?>
                    <div id="comment_view" style="margin-top:5px" class="">
                        <?= Html::encode($model->comment) ?>
                    </div>
                    <?= Html::beginTag('div', [
                        'id' => 'comment_form',
                        'class' => 'hidden',
                        'style' => 'position: relative;',
                        'data-url' => Url::to(['comment', 'id' => $model->id]),
                    ]) ?>
                    <?= Html::tag('i', '', [
                        'id' => 'comment_save',
                        'class' => 'fa fa-floppy-o',
                        'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
                    ]); ?>
                    <?= Html::textarea('comment', $model->comment, [
                        'id' => 'comment_input',
                        'rows' => 3,
                        'maxlength' => true,
                        'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
                    ]); ?>
                    <?= Html::endTag('div') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                'class' => 'button-clr button-regular button-hover-transparent w-full',
                'data-toggle' => 'toggleVisible',
                'data-target' => 'invoice',
            ]) ?>
        </div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить водителя?",
            ]); ?>
        </div>
    </div>
</div>
<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>
<?php $this->registerJs('
    $(document).on("click", "#comment_update", function () {
        $("#comment_view").toggleClass("hidden");
        $("#comment_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_save", function () {
        $.post($("#comment_form").data("url"), $("#comment_input").serialize(), function (data) {
            $("#comment_view").text(data.value);
            $("#comment_form").addClass("hidden");
            $("#comment_view").removeClass("hidden");
        })
    });
');
