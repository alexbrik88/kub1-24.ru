<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 1:47
 */

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $company \common\models\Company */

$this->title = $model->vehicleType->name;
?>
<div class="vehicle-update">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]); ?>
</div>
