<?php

use common\components\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $company \common\models\Company */

$cancelUrl = $model->id !== null ? ['view', 'id' => $model->id] : ['index'];
?>
<?php $form = ActiveForm::begin([
    'id' => 'vehicle-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
]); ?>

<?= $form->errorSummary($model); ?>

<?= $this->render('form/header', [
    'model' => $model,
]); ?>

<?= $this->render('form/body', [
    'model' => $model,
    'form' => $form,
    'company' => $company,
]); ?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#add-new", function() {
        refreshUniform();
    });

    $(document).ready(function (e) {
        if ($("#vehicle-is_state_number_russian").is(":checked")) {
            $("#vehicle-state_number").inputmask({
                "mask": "q{1} 9{3} q{2} 9{2,3}",
                "definitions": {
                    "q": {
                        validator: "[А-яа-я]",
                    }
                }
            });
        }
    });
       
    $(document).on("change", "#vehicle-is_own", function (e) {
        var $isOwnBlock = $(".is_own_block");
        var $isNotOwnBlock = $(".is_not_own_block");
        
        if ($(this).is(":checked")) {
            $isOwnBlock.show();
            $isNotOwnBlock.hide();       
        } else {
            $isOwnBlock.hide();
            $isNotOwnBlock.show();   
        }
    });
    
    $(document).on("change", "#vehicle-vehicle_type_id", function (e) {
        var $type = $(this).val();
        
        $("#vehicle-form").find(".body-fields .field-line-block").each(function () {
        console.log($(this).data("types"));
            if ($(this).data("types") !== undefined) {
                if ($.inArray(+$type, $(this).data("types")) == -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            }
        });
        $weightName = null;
        switch (+$(this).val()) {
            case 2:
                $weightName = "полуприцепа";
                break;
            case 3:
                $weightName = "фургона";
                break;
            case 4:
                $weightName = "прицепа";
                break;        
        }
        $(".weight-name").text($weightName);
    });
    
    $(document).on("change", ".volume-part", function (e) {
        var $calculate = true;
        var $volume = 1;
        
        $(".volume-part").each(function (e) {
            $volume = $volume * $(this).val().replace(",", ".");
            if ($(this).val() == "") {
                $calculate = false;
            }
        });
        if ($calculate) {
            $("#vehicle-volume").val($volume.toFixed(2));
        }
    });
    
    $(document).on("change", "#vehicle-is_state_number_russian", function (e) {
        if ($(this).is(":checked")) {
            $("#vehicle-state_number").inputmask({
                "mask": "q{1} 9{3} q{2} 9{2,3}",
                "definitions": {
                    "q": {
                        validator: "[А-яа-я]",
                    }
                }
            });
        } else {
            $("#vehicle-state_number").inputmask("remove").val("");
        }
    });
'); ?>
