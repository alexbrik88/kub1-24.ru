<?php

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */

use common\components\date\DateHelper;
use common\models\vehicle\VehicleType;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\themes\kub\modules\documents\widgets\CreatedByWidget;
use frontend\themes\kub\widgets\file\FileUpload;

$this->title = $model->getVehicleTitle(false);
?>
<?= Html::a('Назад к списку', Url::to(['index']), ['class' => 'link mb-2']); ?>
<div class="wrap wrap_padding_small">
    <div class="page-in row">
        <div class="page-in-content-logistics column">
            <div class="page-border">
                <?= CreatedByWidget::widget([
                    'createdAt' => date("d.m.Y", $model->created_at),
                    'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                ]); ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), Url::to(['update', 'id' => $model->id]), [
                    'title' => 'Редактировать',
                    'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                ]) ?>
                <div class="doc-container doc-preview" style="margin-top: -50px;">
                    <div class="document-template">
                        <table style="margin-top: 60px;">
                            <tr>
                                <td class="bold" style="font-size: 28px;padding-bottom: 15px;">
                                    <?= $model->getVehicleTypeText(); ?>
                                </td>
                                <td class="bold" style="font-size: 28px;padding-left: 20px;padding-bottom: 15px;">
                                    <?= $model->getMarkAndNumber(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    № ТС:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->number; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Собственное ТС:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->is_own ? 'Да' : 'Нет'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Тип ТС:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->getVehicleTypeText(); ?>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Вид топлива:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span style="width: 206px;display: inline-block;"><?= $model->fuelType ? $model->fuelType->name : null; ?></span>
                                    <span class="bold" style="margin-left: 5px;">Норма расхода:</span>
                                    <span style="padding-left: 10px;"><?= "{$model->fuel_consumption} л/100км"; ?></span>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Дата начального пробега:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= DateHelper::format($model->initial_mileage_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Начальный пробег:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span style="width: 206px;display: inline-block;"><?= "{$model->initial_mileage} км"; ?></span>
                                    <span class="bold" style="margin-left: 5px;">Текущий пробег:</span>
                                    <span style="padding-left: 10px;"><?= "{$model->current_mileage} км"; ?></span>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'none' : 'table-row'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Перевозчик:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->contractor ?
                                        Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                        null; ?>
                                </td>
                            </tr>
                            <tr style="display: <?= in_array($model->vehicle_type_id, [VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR]) ? 'table-row' : 'none'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Водитель:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->driver ?
                                        Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id])) : null; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Марка:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->model; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Номер ТС:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span style="width: 206px;display: inline-block;"><?= $model->state_number; ?></span>
                                    <span class="bold" style="margin-left: 5px;">ГосНомер РФ:</span>
                                    <span style="padding-left: 10px;"><?= $model->is_state_number_russian ? 'Да' : 'Нет'; ?></span>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->body_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row' ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Тип кузова:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->bodyType ? $model->bodyType->name : ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold" style="padding-bottom: 5px;">
                                    Цвет ТС:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->color; ?>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Тоннаж:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span style="width: 206px;display: inline-block;"><?= "{$model->tonnage} тонн"; ?></span>
                                    <span class="bold" style="margin-left: 5px;">Вес ПП:</span>
                                    <span style="padding-left: 10px;"><?= "{$model->weight} тонн"; ?></span>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row'; ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Внутренние габариты (м)
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <span class="bold" style="width: 14px;display: inline-block;">Д:</span>
                                    <span style="padding-left: 5px;width:42px;display: inline-block;"><?= $model->length; ?></span>
                                    <span class="bold" style="padding-left: 5px;width:27px;display: inline-block;">Ш:</span>
                                    <span style="padding-left: 5px;width:42px;display: inline-block;"><?= $model->width; ?></span>
                                    <span class="bold" style="padding-left: 5px;width:22px;display: inline-block;">В:</span>
                                    <span style="padding-left: 5px;width:42px;display: inline-block;"><?= $model->height; ?></span>
                                    <span class="bold" style="padding-left: 5px;display: inline-block;">Объём:</span>
                                    <span style="padding-left: 10px;display: inline-block;"><?= "{$model->volume} м3"; ?></span>
                                </td>
                            </tr>
                            <tr style="display:<?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'table-row' : 'none' ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Полуприцеп:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->semitrailerType ?
                                        Html::a(($model->semitrailerType->model ? ($model->semitrailerType->model . ' ') : null) . ' ' .
                                            $model->semitrailerType->getStateNumberFull(),
                                            Url::to(['view', 'id' => $model->semitrailer_type_id])) :
                                        null; ?>
                                </td>
                            </tr>
                            <tr style="display:<?= $model->vehicle_type_id == VehicleType::TYPE_WAGON ? 'table-row' : 'none' ?>;">
                                <td class="bold" style="padding-bottom: 5px;">
                                    Прицеп:
                                </td>
                                <td style="padding-left: 20px;padding-bottom: 5px;">
                                    <?= $model->trailerType ?
                                        Html::a(($model->trailerType->model ? ($model->trailerType->model . ' ') : null) . ' ' .
                                            $model->trailerType->getStateNumberFull(),
                                            Url::to(['view', 'id' => $model->trailer_type_id])) :
                                        null; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar-logistics column">
            <div class="about-card mb-3">
                <?= $model->getVehicleImg(); ?>
            </div>
            <div class="about-card mb-3">
                <div class="about-card-item">
                    <div class="text-grey" style="margin-bottom: 15px;">Документы:</div>
                    <?= FileUpload::widget([
                        'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
                        'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
                        'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
                        'listUnderButton' => false,
                    ]); ?>
                </div>
            </div>
            <div class="about-card mb-3" style="border: none;padding-left: 0;padding-top: 0;">
                <div class="about-card-item">
                    <span style="font-weight: bold;margin-right: 10px;">Добавить комментарий</span>
                    <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'id' => 'comment_update',
                        'style' => 'cursor: pointer;',
                    ]); ?>
                    <div id="comment_view" style="margin-top:5px" class="">
                        <?= Html::encode($model->comment) ?>
                    </div>
                    <?= Html::beginTag('div', [
                        'id' => 'comment_form',
                        'class' => 'hidden',
                        'style' => 'position: relative;',
                        'data-url' => Url::to(['comment', 'id' => $model->id]),
                    ]) ?>
                    <?= Html::tag('i', '', [
                        'id' => 'comment_save',
                        'class' => 'fa fa-floppy-o',
                        'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
                    ]); ?>
                    <?= Html::textarea('comment', $model->comment, [
                        'id' => 'comment_input',
                        'rows' => 3,
                        'maxlength' => true,
                        'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
                    ]); ?>
                    <?= Html::endTag('div') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= \yii\helpers\Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                'class' => 'button-clr button-regular button-hover-transparent w-full',
                'data-toggle' => 'toggleVisible',
                'data-target' => 'invoice',
            ]) ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                'message' => 'Вы уверены, что хотите скопировать это траспортное средство?',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1"></div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить транспортное средство?",
            ]); ?>
        </div>
    </div>
</div>
<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>
<?php $this->registerJs('
    $(document).on("click", "#comment_update", function () {
        $("#comment_view").toggleClass("hidden");
        $("#comment_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_save", function () {
        $.post($("#comment_form").data("url"), $("#comment_input").serialize(), function (data) {
            $("#comment_view").text(data.value);
            $("#comment_form").addClass("hidden");
            $("#comment_view").removeClass("hidden");
        })
    });
');
