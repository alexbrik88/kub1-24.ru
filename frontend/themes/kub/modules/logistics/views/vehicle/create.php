<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 13:45
 */

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $company \common\models\Company */

$this->title = 'Добавить транспортное средство';
?>
<div class="vehicle-create">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]); ?>
</div>