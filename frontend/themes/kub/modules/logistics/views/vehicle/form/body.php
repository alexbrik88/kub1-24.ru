<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\driver\Driver;
use common\models\vehicle\BodyType;
use common\models\vehicle\FuelType;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $company \common\models\Company */

?>
<div class="wrap body-fields">
    <div class="row d-block">
        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Тип ТС:</label>
                </div>
                <?= $form->field($model, 'vehicle_type_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(VehicleType::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
            <div class="form-group col-2" style="padding-top: 35px;">
                <?= Html::activeCheckbox($model, 'is_own', [
                    'class' => '',
                    'label' => false,
                ]); ?>
                <div class="form-filter" style="display: inline-block;margin-right: 10px;">
                    <label class="label" for="vehicle-is_own">Собственное ТС</label>
                </div>
            </div>
        </div>

        <div class="row col-12 is_own_block field-line-block" style="display: <?= $model->is_own ? null : 'none'; ?>;">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Вид топлива:</label>
                </div>
                <?= $form->field($model, 'fuel_type_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(FuelType::find()->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Норма расхода (л/100км):</label>
                </div>
                <?= $form->field($model, 'fuel_consumption', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 is_own_block field-line-block" style="display: <?= $model->is_own ? null : 'none'; ?>;">
            <div class="form-group col-3">
                <div class="form-filter">
                    <label class="label" for="input3">Дата начального пробега:</label>
                </div>
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'initial_mileage_date', [
                                'class' => 'form-control date-picker',
                                'value' => DateHelper::format($model->initial_mileage_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Начальный пробег (км):</label>
                </div>
                <?= $form->field($model, 'initial_mileage', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Текущий пробег (км):</label>
                </div>
                <?= $form->field($model, 'current_mileage', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 is_not_own_block field-line-block" style="display: <?= $model->is_own ? 'none' : 'block'; ?>;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Перевозчик:</label>
                </div>
                <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                    'class' => 'show-contractor-type-in-fields',
                ]])->widget(Select2::class, [
                    'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить перевозчика']
                        + $company->sortedContractorList(Contractor::TYPE_SELLER),
                    'options' => [
                        'class' => 'form-control contractor-select',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block"
                 data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR,]) ?>"
                 style="display: <?= in_array($model->vehicle_type_id, [VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR]) ? 'block' : 'none'; ?>;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Водитель:</label>
                </div>
                <?= $form->field($model, 'driver_id', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]])->widget(Select2::class, [
                    'data' => ArrayHelper::map(Driver::find()->andWhere(['company_id' => $company->id])->all(), 'id', 'fio'),
                    'options' => [
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Марка:</label>
                </div>
                <?= $form->field($model, 'model', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3"
                 data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]) ?>"
                 style="display: <?= $model->body_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'block'; ?>;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Тип кузова:</label>
                </div>
                <?= $form->field($model, 'body_type_id', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->widget(Select2::class, [
                    'data' => ArrayHelper::map(
                        BodyType::find()->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC,])->all(),
                        'id',
                        'name'
                    ),
                    'options' => [
                        'id' => 'body_type_id',
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Цвет ТС:</label>
                </div>
                <?= $form->field($model, 'color', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Номер ТС:</label>
                </div>
                <?= $form->field($model, 'state_number', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3" style="padding-top: 35px;">
                <?= Html::activeCheckbox($model, 'is_state_number_russian', [
                    'class' => '',
                    'label' => 'ГосНомер РФ',
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block"
             data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]); ?>"
             style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : null; ?>;">
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Тоннаж (тонн):</label>
                </div>
                <?= $form->field($model, 'tonnage', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
            <div class="form-group col-3">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Вес <span class="weight-name"></span> (тонн):</label>
                </div>
                <?= $form->field($model, 'weight', ['template' => "{input}", 'options' => [
                    'class' => '',
                ]]); ?>
            </div>
        </div>
        <div class="col-12 field-line-block"
             data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]); ?>"
             style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'block'; ?>;">
            <div class="form-group col-2 pad0" style="display: inline-block;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Внутренние габариты</label>
                </div>
            </div>
            <div class="form-group col-1 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Длина (м):</label>
                </div>
                <?= $form->field($model, 'length', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
            <div class="form-group col-1 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Ширина (м):</label>
                </div>
                <?= $form->field($model, 'width', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
            <div class="form-group col-1 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Высота (м):</label>
                </div>
                <?= $form->field($model, 'height', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('number', [
                    'class' => 'form-control volume-part',
                    'min' => 0,
                    'max' => 10000000,
                    'step' => 'any',
                ]); ?>
            </div>
            <div class="form-group col-1 pad0" style="display: inline-block;margin-left: 30px;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Объём (м3):</label>
                </div>
                <?= $form->field($model, 'volume', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->input('text', [
                    'class' => 'form-control',
                    'readOnly' => true,
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block"
                 data-types="<?= json_encode([VehicleType::TYPE_TRACTOR]); ?>"
                 style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'block' : 'none'; ?>;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Полуприцеп:</label>
                </div>
                <?= $form->field($model, 'semitrailer_type_id', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->widget(Select2::class, [
                    'data' => ArrayHelper::map(Vehicle::find()
                        ->andWhere(['company_id' => $company->id])
                        ->andWhere(['vehicle_type_id' => VehicleType::TYPE_SEMITRAILER])
                        ->all(), 'id', function (Vehicle $model) {
                            $name = $model->model ? ($model->model . ' ') : null;
                            $name .= $model->state_number;

                            return $name;
                        }
                    ),
                    'options' => [
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row col-12 field-line-block">
            <div class="form-group col-6 field-line-block"
                 data-types="<?= json_encode([VehicleType::TYPE_WAGON]); ?>"
                 style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_WAGON ? null : 'none'; ?>;">
                <div class="form-filter" style="display: inline-block;">
                    <label class="label" for="cause">Прицеп:</label>
                </div>
                <?= $form->field($model, 'trailer_type_id', [
                    'template' => "{input}",
                    'options' => [
                        'class' => '',
                    ],
                ])->widget(Select2::class, [
                    'data' => ArrayHelper::map(Vehicle::find()
                        ->andWhere(['company_id' => $company->id])
                        ->andWhere(['vehicle_type_id' => VehicleType::TYPE_TRAILER])
                        ->all(), 'id', function (Vehicle $model) {
                            $name = $model->model ? ($model->model . ' ') : null;
                            $name .= $model->state_number;

                            return $name;
                        }
                    ),
                    'options' => [
                        'prompt' => '',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
