<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 12:11
 */

use common\components\helpers\Html;
use frontend\themes\kub\widgets\SummarySelectWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\vehicle\Vehicle;
use common\components\grid\GridView;
use common\models\vehicle\VehicleType;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\VehicleSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $user \common\models\employee\Employee
 */

$this->title = 'Транспортные средства';
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$tableViewClass = $user->config->getTableViewClass('table_view_logistics');
?>
<div class="wrap wrap_padding_small pl-4 pr-2 pt-2 pb-0 mb-4">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <h4 class="mt-1 mb-2"><?= $this->title ?></h4>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-1 text-right">
                    <?= Html::a($addIcon . '<span>Добавить</span>', Url::to(['create']), [
                        'class' => 'button-regular button-regular_red button-width',
                        'style' => 'width: 150px;',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableViewWidget::widget(['attribute' => 'table_view_logistics']) ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => "table table-style table-count-list table-logistics table-logistics-joint-operations {$tableViewClass}",
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'emptyText' => 'Вы еще не добавили ни одного транспортного средства. ' . Html::a('Добавить', Url::to(['create'])),
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return Html::checkbox('Vehicle[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'attribute' => 'number',
            'label' => '№',
            'headerOptions' => [
                'width' => '6%',
            ],
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return Html::a($model->number, ['view', 'id' => $model->id]);
            },
        ],
        [
            'filter' => $searchModel->getVehicleTypeFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'vehicle_type_id',
            'label' => 'Тип ТС',
            'headerOptions' => [
                'width' => '11%',
            ],
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return $model->getVehicleImg([
                    'style' => 'width: 30px;',
                ]) . $model->getVehicleTypeText(true);
            },
        ],
        [
            'filter' => $searchModel->getBodyTypeFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'body_type_id',
            'label' => 'Тип кузова',
            'headerOptions' => [
                'width' => '11%',
            ],
            'contentOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return $model->bodyType ? $model->bodyType->name : '';
            },
        ],
        [
            'attribute' => 'model',
            'label' => 'Марка ТС',
            'headerOptions' => [
                'width' => '11%',
            ],
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return $model->model ? $model->model : '';
            },
        ],
        [
            'attribute' => 'state_number',
            'label' => 'Гос. номер',
            'headerOptions' => [
                'width' => '11%',
            ],
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return $model->getStateNumberFull(true);
            },
        ],
        [
            'attribute' => 'totalTonnage',
            'label' => 'Тонн',
            'headerOptions' => [
                'width' => '7%',
            ],
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                $tonnage = $model->tonnage;
                if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType) {
                    $tonnage += $model->semitrailerType->tonnage;
                } elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType) {
                    $tonnage += $model->trailerType->tonnage;
                }
                return $tonnage;
            },
        ],
        [
            'attribute' => 'totalVolume',
            'label' => 'Объем',
            'headerOptions' => [
                'width' => '7%',
            ],
            'contentOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                $volume = $model->volume;
                if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType) {
                    $volume += $model->semitrailerType->volume;
                } elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType) {
                    $volume += $model->trailerType->volume;
                }
                return $volume;
            },
        ],
        [
            'attribute' => 'files',
            'label' => 'Документы',
            'headerOptions' => [
                'width' => '10%',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                    'model' => $model,
                ]);
            },
        ],
        [
            'filter' => $searchModel->getOwnerFilter(),
            'hideSearch' => false,
            's2width' => '200px',
            'attribute' => 'owner',
            'label' => 'Кому принадлежит',
            'headerOptions' => [
                'width' => '20%',
            ],
            'format' => 'raw',
            'value' => function (Vehicle $model) {
                return $model->is_own ? 'Собственное ТС' :
                    ($model->contractor ?
                        Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                        '');
            },
        ],
    ],
]); ?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
    ],
]); ?>

<div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные транспортные средства?</h4>
            <div class="text-center">
                <?= Html::a('Да', null, [
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['many-delete']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
