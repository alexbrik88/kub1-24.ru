<?php

use common\components\grid\GridView;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\ProductCategory;
use common\models\product\ProductField;
use frontend\modules\out\models\OutInvoiceForm;
use frontend\modules\out\models\OutOrderDocumentForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */

$isProduct = $model->priceList->production_type == Product::PRODUCTION_TYPE_GOODS || $model->priceList->production_type == Product::PRODUCTION_TYPE_ALL;

// changing columns
$isName = $model->priceList->include_name_column;
$isArticle = $model->priceList->include_article_column;
$isGroup = $model->priceList->include_product_group_column;
$isReminder = $model->priceList->include_reminder_column;
$isUnit = $model->priceList->include_product_unit_column;
$isPrice = $model->priceList->include_price_column;
$isWeight = $model->priceList->include_weight_column;
$isNetto = $model->priceList->include_netto_column;
$isBrutto = $model->priceList->include_brutto_column;
$isCustomField = $model->priceList->include_custom_field_column;
$isCountInPackage = $model->priceList->include_count_in_package_column;
$isBoxType = $model->priceList->include_box_type_column;

/** @var ProductField $productField */
$productField = ProductField::findOne(['company_id' => $model->priceList->company_id]);
$productCategoryNames = Product::getCategoryNames();
$productCategoryFields = Product::getCategoryFieldNames();
$productCategoryColumns = [];
foreach ($model->priceList->productCategoryColumns as $fieldId) {
    $productCategoryColumns[] =
        [
            'attribute' => '_category_product_' . $fieldId,
            'label' => \common\components\helpers\ArrayHelper::getValue($productCategoryFields[ProductCategory::TYPE_BOOK_PUBLISHER], $fieldId),
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
            ],
            'format' => 'raw',
            'value' => function ($model) use ($fieldId) {
                $productCategoryItems = $model->product->getCategoryFieldValues(ProductCategory::TYPE_BOOK_PUBLISHER);
                return ArrayHelper::getValue($productCategoryItems, $fieldId) ?: '---';
            },
        ];
}

// for modal
$isDescription = $model->priceList->include_description_column;
$isImage = $model->priceList->include_image_column;

// $productArray = $model->priceList->priceListOrders;

$emptyMessage = 'Ничего не найдено';

$downloadPdfUrl = Url::to(['/price-list/download', 'type' =>'pdf', 'uid' => $model->priceList->uid]);
$downloadXlsUrl = Url::to(['/price-list/download', 'type' =>'xls', 'uid' => $model->priceList->uid]);

if ($isImage || $isDescription) {
    $this->registerJs(<<<JS
    if (!sessionStorage.getItem("tooltip-image-modal-content")) {
        $(".show-pricelist-modal").tooltipster({
            "theme": ["tooltipster-dark"],
            "trigger": "hover",
            "side": "left",
            "contentAsHTML": true,
            "contentCloning": true
        });
        $("#pricelist-image-modal").one("shown.bs.modal", function() {
            $(".show-pricelist-modal").tooltipster("destroy");
        });
    } else {
        sessionStorage.removeItem("tooltip-image-modal-content");
    }
JS
); } ?>


<?php \yii\widgets\Pjax::begin(['id' => 'price-list-pjax']); ?>
<?php $num = 0; ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'exel']), $downloadXlsUrl, [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
                'data' => ['pjax' => 0]
            ]
        ); ?>
        <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'pdf']), $downloadPdfUrl, [
                'class' => 'button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в PDF',
                'data' => ['pjax' => 0],
                'target' => '_blank'
            ]
        ); ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
                'data-pjax' => true
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= \common\components\helpers\Html::activeTextInput($searchModel, 'find_by', [
                'type' => 'search',
                'placeholder' => 'Поиск по названию' . ($isArticle ? ' и артикулу' : ''),
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-compact table-price-list-clickable',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
    'rowOptions' => function ($order, $key, $index, $grid) {
        return [
            'class' => 'show-pricelist-modal',
            'data' => [
                'key' => 'order_' . $order->id,
                'tooltip-content' => '#tooltip-image-modal-content'
            ]
        ];
    },
    'columns' => array_merge([
        [
            'headerOptions' => [
                'width' => '1%',
            ],
            'format' => 'raw',
            'value' => function ($order) use (&$num, $dataProvider) {
                return ++$num + ($dataProvider->pagination->page * $dataProvider->pagination->pageSize);
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
            'headerOptions' => [
                'width' => '20%',
                'class' => ($isName ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isName ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                return $order->name;
            },
        ]],
        $productCategoryColumns, [
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isArticle ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isArticle ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                return $order->article;
            },
        ],
        [
            'attribute' => 'product_group_id',
            'label' => 'Группа',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isGroup ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isGroup ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                return $order->productGroup ? $order->productGroup->title : '---';
            },
            'filter' => $searchModel->getFilterGroups(),
            's2width' => '200px'
        ],
        [
            'attribute' => 'product.weight',
            'label' => 'Вес (кг)',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isWeight ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isWeight ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->weight ?: '---';
            },
        ],
        [
            'attribute' => 'product.mass_net',
            'label' => 'Масса нетто',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isNetto ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isNetto ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->mass_net ?: '---';
            },
        ],
        [
            'attribute' => 'product.mass_gross',
            'label' => 'Масса брутто',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isBrutto ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isBrutto ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->mass_gross ?: '---';
            },
        ],
        [
            'attribute' => 'product.custom_field_value',
            'label' => $productField->title ?? 'Ваше название',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isCustomField ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isCustomField ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->custom_field_value ?: '---';
            },
        ],
        [
            'attribute' => 'quantity',
            'label' => 'Остаток',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isReminder ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isReminder ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                $isDecimal = ($order->quantity != (int)$order->quantity);
                return ($order->quantity > 0) ? TextHelper::numberFormat($order->quantity, $isDecimal ? 2 : 0) : 0;
            },
        ],
        [
            'attribute' => 'product.box_type',
            'label' => 'Вид упаковки',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isBoxType ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isBoxType ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->box_type ?: '---';
            },
        ],
        [
            'attribute' => 'product.count_in_package',
            'label' => 'Количество в упаковке',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isCountInPackage ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isCountInPackage ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return $model->product->count_in_package ?: '---';
            },
        ],
        [
            'attribute' => 'product_unit_id',
            'label' => 'Ед. измер-я',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isUnit ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => ($isUnit ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                return $order->productUnit ? $order->productUnit->name : '---';
            },
        ],
        [
            'attribute' => 'price_for_sell',
            'label' => 'Цена',
            'headerOptions' => [
                'width' => '10%',
                'class' => ($isPrice ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'text-right ' . ($isPrice ? '' : ' hidden')
            ],
            'format' => 'raw',
            'value' => function ($order) {
                return '<div style="min-width:90px">' . TextHelper::invoiceMoneyFormat($order->price_for_sell, 2) . '</div>';
            },
        ],
        [
            'attribute' => 'count',
            'label' => 'Кол-во',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'td-no-clickable'
            ],
            'format' => 'raw',
            'value' => function ($order) use ($model) {

                return '<div style="min-width:105px">'.$this->render('_product_table_count_row', ['prd' => $order, 'model' => $model]).'</div>';
            }
        ],
        [
            'attribute' => 'amount',
            'label' => 'Стоимость',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => function ($order, $key, $index, $grid) {
                return [
                    'class' => 'text-right ' . 'prod-sum-'.$order->product_id
                ];
            },
            'format' => 'raw',
            'value' => function ($order) use ($model) {
                return '<div style="min-width:90px">' . 0 . '</div>';
            }
        ]
    ]),
]); ?>
<?php \yii\widgets\Pjax::end(); ?>


<?php
$sortAttr = $model->priceList->getSortAttributeName();
$priceListOrders = $model->priceList->getPriceListOrders()
    ->joinWith('productGroup')
    ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
    ->indexBy('id')
    ->all();

echo $this->render('@frontend/views/price-list/_clickable_product_modal', [
    'priceList' => $model->priceList,
    'priceListOrders' => $priceListOrders,
    'showModalBtn' => '.show-pricelist-modal'
]) ?>
<div style="display: none">
    <div id="tooltip-image-modal-content">Кликните<br/> на строку,<br/>чтобы получить<br/>информацию<br/>о товаре</div>
</div>

<script>
    var priceValueFormat = function (value) {
        return new Intl.NumberFormat('ru-RU', {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(value);
    }
    var checkTotalCount = function() {
        var totalPosition = 0;
        var totalCount = 0;
        var totalSum = 0;
        var hasDiscountFromSum = <?= $model->priceList->has_discount_from_sum ? 1 : 0 ?>;
        var discountFromSum = "<?= $model->priceList->discount_from_sum ?>";
        var discountFromSumPercent = "<?= $model->priceList->discount_from_sum_percent ?>";
        var totalDiscount = 0;
        var hasMarkupFromSum = <?= $model->priceList->has_markup_from_sum ? 1 : 0 ?>;
        var markupFromSum = "<?= $model->priceList->markup_from_sum ?>";
        var markupFromSumPercent = "<?= $model->priceList->markup_from_sum_percent ?>";
        var totalMarkup = 0;

        $('.product-quantity-input').each(function(){
            var prodCount = Math.max(0, $(this).val() * 1);
            var prodSum = Math.round($(this).data('price') * prodCount);
            var prodId = $(this).data('product');
            //console.log(this);
            totalPosition += (prodCount ? 1:0);
            totalCount += prodCount;
            totalSum += prodSum;
            $('.out-order-document-form .prod-sum-' + prodId)
                .find('div').text(priceValueFormat(prodSum / 100));

            $('#data-product-' + prodId).val(prodCount);
        });

        $('#totals-block .total-position').text(totalPosition);
        $('#totals-block .total-count').text(totalCount);
        $('#totals-block .total-sum').text(priceValueFormat(totalSum / 100));
        if (totalCount > 0) {
            $('#outinvoice-submit')
                .removeClass('button-hover-transparent')
                .addClass('button-regular_red')
                .text('Отправить заказ')
                .prop('disabled', false);
        } else {
            $('#outinvoice-submit')
                .addClass('button-hover-transparent')
                .removeClass('button-regular_red')
                .text('Выберите количество')
                .prop('disabled', true);
        }

        $('.table-price-list-clickable > tbody > tr').each(function() {
            var input = $(this).find('.quantity-input');
            if (input.val() > 0) {
                $(this).addClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });

        // discount
        if (hasDiscountFromSum) {
            if (totalSum >= discountFromSum) {
                totalDiscount = discountFromSumPercent / 100 * totalSum;
                $('#totals-block .total-discount').text(priceValueFormat(totalDiscount / 100)).parent().show();
            } else {
                totalDiscount = 0;
                $('#totals-block .total-discount').text(priceValueFormat(totalDiscount / 100)).parent().hide();
            }
        }
        // markup
        if (hasMarkupFromSum) {
            if (totalSum >= markupFromSum) {
                totalMarkup = markupFromSumPercent / 100 * totalSum;
                $('#totals-block .total-markup').text(priceValueFormat(totalMarkup / 100)).parent().show();
            } else {
                totalMarkup = 0;
                $('#totals-block .total-markup').text(priceValueFormat(totalMarkup / 100)).parent().hide();
            }
        }
    };

    $(document).on('click', '.quantity-btn', function(e){
        e.preventDefault();
        var type      = $(this).attr('data-type');
        var input     = $(this).closest('.quantity-box').find('.quantity-input');
        var val = parseInt(input.val());
        if(type == 'minus') {
            val--;
        } else if(type == 'plus') {
            val++;
        }

        input.val(Math.min(input.attr('max'), Math.max(input.attr('min'), val))).change();
    });

    $(document).on('change', '.quantity-input', function(){
        var box = $(this).closest('.quantity-box');
        var minus = box.find(".quantity-btn.type-minus");
        var plus = box.find(".quantity-btn.type-plus");
        var minVal =  parseInt($(this).attr('min'));
        var maxVal =  parseInt($(this).attr('max'));
        var val = Math.min(maxVal, Math.max(minVal, parseFloat($(this).val().replace(',', '.'))));

        minus.prop('disabled', val == minVal);
        plus.prop('disabled', val == maxVal);

        if ($(this).val() != String(val)) {
            $(this).val(val);
        }
        $($(this).data('target')).val(val);

        checkTotalCount();
    });

    $(document).on('keydown', '.quantity-input', function(e){
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 90)) {
            e.preventDefault();
        }
    });

    $(document).on("pjax:complete", "#price-list-pjax", function(e) {
        $('#outinvoice-form').find('.product-quantity-input').each(function() {
            var product_id = $(this).data('product');
            var count = $(this).val();
            console.log(product_id, count);
            $('#price-list-pjax').find('.quantity-input').filter('[name="prod[' + product_id + ']"]').val(count);
        });
        checkTotalCount();
    });

    checkTotalCount();

</script>