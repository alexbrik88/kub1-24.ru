<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php foreach ($model->product as $key => $value) : ?>
    <?= Html::activeHiddenInput($model, "product[{$key}]", ['value' => $value]); ?>
<?php endforeach ?>

<?php foreach ($model->price as $key => $value) : ?>
    <?= Html::activeHiddenInput($model, "price[{$key}]", ['value' => $value]); ?>
<?php endforeach ?>
