<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\bootstrap\ActiveForm */

//$this->title = 'Реквизиты покупателя';
$this->title = 'Оформление заказа у ' . Yii::$app->params['outCompany']->getTitle(true);
$this->params['showHeader'] = true;

// Fields params
$is_autocomplete = true;
$is_bik_autocomplete = true;

$serviceSiteUrl = trim(Yii::$app->params['serviceSite'], '/');

$textInputConfig = [
    'options' => [
        'class' => 'form-group col-3',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$wideTextInputConfig = [
    'options' => [
        'class' => 'form-group col-12',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

$this->registerJs(<<<JS
$(document).on('keyup paste', '#data-legal_inn', function() {
    if (this.value.length >= 10) {
        $('.legal_data_collapse').collapse('show');
    }
})
JS
);

$autocompleteJs = <<<JS
$('#data-legal_inn').suggestions({
    serviceUrl: 'https://dadata.ru/api/v2',
    token: '78497656dfc90c2b00308d616feb9df60c503f51',
    type: 'PARTY',
    count: 10,
    onSelect: function(suggestion) {
        var data = suggestion.data;
        var opf = data.opf.short;
        var type = $("#data-legal_type option:contains('"+opf+"')").val();
        $('#data-legal_inn').val(data.inn);
        $('#data-legal_type').val(type);
        $('#data-legal_kpp').val(data.kpp);
        var address = '';
        if (data.address.data !== null) {
            if (data.address.data.postal_code) {
                address += data.address.data.postal_code;
                address += ', ';
            }
        }
        address += data.address.value;
        $('#data-legal_address').val(address);
        if (opf == 'ИП') {
            $('#data-legal_name').val(data.name ? data.name.full : '');
            $('#data-chief_name').val(data.name ? data.name.full : '');
        } else {
            $('#data-legal_name').val(data.name ? data.name.short : '');
            $('#data-chief_name').val(data.management ? data.management.name : '');
        }
        $('#data-chief_position').val(data.management ? data.management.post : '');
        $('.legal_data_collapse').collapse('show');
    }
});
JS;

$this->registerJs($autocompleteJs);

?>

<div class="out-invoice-form out-order-document-form">

    <?= $this->render('_header') ?>

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'uid' => $model->priceList->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],

    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $this->render('_orders_form', [
        'form' => $form,
        'model' => $model,
    ]) ?>

    <?= Html::activeHiddenInput($model, 'email'); ?>
    <?= Html::activeHiddenInput($model, 'phone'); ?>
    <?= Html::activeHiddenInput($model, 'comment'); ?>
    <?= Html::activeHiddenInput($model, 'face_type'); ?>

    <div class="wrap" style="margin-top: 20px">
        <div class="row">
            <div class="col-12">
                <?php if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON): ?>
                    <div class="physical-prop-box">
                        <div class="row">
                            <?= $form->field($model, 'physical_lastname', $textInputConfig)->label('Фамилия')->textInput([
                                'maxlength' => true,
                                'data' => [
                                    'toggle' => 'popover',
                                    'trigger' => 'focus',
                                    'placement' => 'bottom',
                                ],
                            ]); ?>
                            <?= $form->field($model, 'physical_firstname', $textInputConfig)->label('Имя')->textInput([
                                'maxlength' => true,
                                'data' => [
                                    'toggle' => 'popover',
                                    'trigger' => 'focus',
                                    'placement' => 'bottom',
                                ],
                            ]); ?>
                            <?= $form->field($model, 'physical_patronymic', $textInputConfig)->label('Отчество')->textInput([
                                'maxlength' => true,
                                'readonly' => (boolean)$model->physical_no_patronymic,
                                'data' => [
                                    'toggle' => 'popover',
                                    'trigger' => 'focus',
                                    'placement' => 'bottom',
                                ],
                            ]); ?>
                            <div class="form-group col-3">
                                <div class="checkbox checkbox_left" style="padding-top:30px">
                                    <label class="checkbox-label" for="contractor-physical_no_patronymic">
                                        <?= \yii\bootstrap4\Html::activeCheckbox($model, 'physical_no_patronymic', [
                                            'id' => 'contractor-physical_no_patronymic',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                        <span class="checkbox-txt">Нет отчества</span>
                                    </label>
                                </div>
                            </div>

                            <?= $form->field($model, 'physical_address', $wideTextInputConfig)->label('Адрес доставки')->textInput([
                                'maxlength' => true,
                                'data' => [
                                    'toggle' => 'popover',
                                    'trigger' => 'focus',
                                    'placement' => 'bottom',
                                ],
                            ]);
                            ?>
                        </div>

                    </div>
                <?php else: ?>
                    <div class="legal-prop-box">
                        <?= $form->field($model, 'legal_inn')->textInput([
                            'maxlength' => true,
                            'placeholder' => ($is_autocomplete) ? 'Автоматическое заполнение по ИНН' : null,
                        ])->label('ИНН вашей компании'); ?>
                        <div class="legal_data_collapse collapse <?= $model->legal_inn ? 'in' : ''; ?>">
                            <?= $form->field($model, 'legal_type')->dropDownList($model->getTypeArray()); ?>
                            <?= $form->field($model, 'legal_name')->textInput([
                                'maxlength' => true,
                                'placeholder' => 'Без ООО / ИП',
                            ]); ?>
                            <?= $form->field($model, 'legal_kpp')->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'legal_address')->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'chief_position')->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'chief_name')->textInput(['maxlength' => true]); ?>
                        </div>
                        <?= $form->field($model, 'legal_rs')->textInput([
                            'maxlength' => true,
                            'placeholder' => 'Для Актов и Товарных накладных',
                        ]); ?>
                        <?php if ($is_bik_autocomplete) : ?>
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'legal_bik')->widget(\common\components\widgets\BikTypeahead::classname(), [
                                        'remoteUrl' => Url::to(['/dictionary/bik']),
                                        'related' => [
                                            '#bank_name' => 'name',
                                        ],
                                    ])->textInput(['placeholder' => 'Для Актов и Товарных накладных', 'autocomplete' => 'off']); ?>
                                </div>
                                <div class="col-6">
                                    <label class="label">Название банка</label>
                                    <input id="bank_name" class="form-control" disabled=""/>
                                </div>
                            </div>
                        <?php else : ?>
                            <?= $form->field($model, 'legal_bik')->textInput([
                                'maxlength' => true,
                                'placeholder' => 'Для Актов и Товарных накладных',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="wrap wrap_btns fixed mb-0" id="totals-block" style="max-width: unset">
        <div class="row justify-content-end align-items-center">
            <div class="column mr-auto">
                <?php if ($model->scenario != OutInvoiceForm::SCENARIO_FIXPRODUCT) : ?>
                    <?= Html::submitButton('Назад', [
                        'class' => 'go-back-btn button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                        'form' => 'outinvoice-form',
                        'name' => 'back',
                        'value' => 'back',
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="col-6" style="text-align: right">
                <i>
                    Нажимая на кнопку “Оформить заказ”, вы даете согласие на обработку
                    <a target="_blank" href="<?=$serviceSiteUrl;?>/SecurityPolicy/Security_Policy.pdf">
                        персональных данных
                    </a>.
                </i>
            </div>
            <div class="column">
                <?= Html::submitButton('Оформить заказ', [
                    'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
                    'form' => 'outinvoice-form',
                    'style' => 'min-width: 160px'
                ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
