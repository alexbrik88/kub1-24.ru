<?php
use frontend\modules\out\models\OutOrderDocumentForm;
use common\models\product\PriceListOrder;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;

/** @var PriceListOrder $prd */
/** @var OutOrderDocumentForm $model */

$inputName = "product[{$prd->product_id}]";
$inputValue = max(0, ArrayHelper::getValue($model->product, $prd->product_id, 0));
?>

<?php $minusIco = '<svg version="1.1" viewBox="0 0 512 512" style="width:18px; padding:4px; fill:#0097fd !important;"> <g> <g> <path d="M492,236H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h472c11.046,0,20-8.954,20-20S503.046,236,492,236z"/> </g> </g> </svg>'; ?>
<?php $plusIco = '<svg version="1.1" viewBox="0 0 512 512" style="width:18px; padding:4px; fill:#0097fd !important;"> <g> <g> <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216 v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z"/> </g> </g> </svg>' ?>

<div style="margin: 5px;">
    <table class="input-group quantity-box" style="width: 100%; margin: 0;">
        <tr>
            <td style="padding: 0; border: none;">
                <?= Html::tag('span', $minusIco, [
                    'class' => 'button-clr button-regular quantity-btn type-minus',
                    'disabled' => ($inputValue == 0) ? true : false,
                    'data-type' => 'minus',
                    'type' => 'button',
                ]) ?>
            </td>
            <td style="padding: 0 3px; border: none;">
                <?= Html::textInput("prod[{$prd->product_id}]", $inputValue, [
                    'class' => 'form-control input-number quantity-input',
                    'style' => 'text-align: center; border-radius: 4px !important; padding: 6px;',
                    'min' => 0,
                    'max' => ($model->priceList->include_reminder_column) ? $prd->quantity : OutInvoiceForm::MAX_VALUE,
                    'data-target' => "#data-product-{$prd->product_id}",
                ]) ?>
            </td>
            <td style="padding: 0; border: none;">
                <?= Html::tag('span', $plusIco, [
                    'class' => 'button-clr button-regular quantity-btn type-plus',
                    'disabled' => false,
                    'data-type' => 'plus',
                    'type' => 'button',
                ]) ?>
            </td>
        </tr>
    </table>
</div>