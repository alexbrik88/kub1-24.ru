<?php

use common\components\image\EasyThumbnailImage;
use common\models\Company;
use frontend\modules\out\models\OutInvoiceForm;
use frontend\modules\out\models\OutOrderDocumentForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\widgets\ActiveForm */

//$this->title = 'Выберите товар/услугу';
$this->title = 'Оформление заказа у ' . Yii::$app->params['outCompany']->getTitle(true);
$this->params['showHeader'] = true;

$totalCount = array_sum($model->product);

$company = Yii::$app->params['outCompany'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$showHeader = empty($this->params['showHeader']) ? false : true;
$hasError = Yii::$app->params['hasError'];
$isDemo = Yii::$app->params['isDemo'];
if ($company && is_file($path = $company->getImage('logoImage'))) {
    $logoSrc = EasyThumbnailImage::thumbnailSrc(
        $path,
        $logoWidth,
        $logoHeight,
        EasyThumbnailImage::THUMBNAIL_INSET
    );
}

$searchModel = new \frontend\models\PriceListOrderSearch();
$searchModel->ownerPriceList = $model->priceList;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

?>

<div class="header" style="display: none; height: 0;"></div>

<div class="out-invoice-form out-order-document-form">

    <?= $this->render('_header') ?>

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'uid' => $model->priceList->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>
    <?= Html::activeHiddenInput($model, 'email') ?>
    <?= Html::activeHiddenInput($model, 'phone') ?>
    <?= Html::activeHiddenInput($model, 'comment') ?>
    <?= Html::activeHiddenInput($model, 'legal_type') ?>
    <?= Html::activeHiddenInput($model, 'legal_name') ?>
    <?= Html::activeHiddenInput($model, 'legal_inn') ?>
    <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
    <?= Html::activeHiddenInput($model, 'legal_address') ?>
    <?= Html::activeHiddenInput($model, 'legal_rs') ?>
    <?= Html::activeHiddenInput($model, 'legal_bik') ?>
    <?= Html::activeHiddenInput($model, 'chief_position') ?>
    <?= Html::activeHiddenInput($model, 'chief_name') ?>

    <?php
    foreach ($model->priceList->priceListOrders as $prd) {
        $inputValue = max(0, ArrayHelper::getValue($model->product, $prd->product_id, 0));
        $price = $prd->price_for_sell;
        echo Html::activeHiddenInput($model, "product[{$prd->product_id}]", [
            'value' => $inputValue,
            'class' => 'product-quantity-input',
            'data-product' => $prd->product_id,
            'data-price' => $price,
        ]);
    }
    ?>

    <?php ActiveForm::end(); ?>

    <div class="document">
        <?php if ($model->priceList->is_deleted || $model->priceList->is_archive): ?>
            <div class="text-center price-list_deleted">
                Данный прайс-лист не действителен. <br>
                Запросите новый.
            </div>
        <?php else: ?>

            <?= $this->render('_product_table', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]) ?>

        <?php endif; ?>
    </div>

</div>

<?php if (!$model->priceList->is_deleted && !$model->priceList->is_archive): ?>
<div class="wrap wrap_btns fixed mb-0" id="totals-block" style="max-width: unset">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['/price-list/out-view', 'uid' => $model->priceList->uid], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                'name' => 'back',
                'value' => 'back',
            ]) ?>
        </div>
        <div class="column text-bold">
            Выбрано позиций:
            <b class="total-position">
                0
            </b>
        </div>
        <div class="column text-bold">
            Кол-во единиц:
            <b class="total-count">
                0
            </b>
        </div>
        <div class="column text-bold">
            Сумма:
            <b class="total-sum">
                0
            </b>
            <br/>
            <span style="color:#e30611; font-size:14px; font-weight:300; display: none">
                Скидка:
                - <b class="total-discount">
                    0
                </b>
            </span>
            <span style="color:#e30611; font-size:14px; font-weight:300; display: none">
                Наценка:
                + <b class="total-markup">
                    0
                </b>
            </span>
        </div>
        <div class="column">
            <?= Html::submitButton($totalCount > 0 ? 'Отправить заказ' : 'Выберите количество', [
                'id' => 'outinvoice-submit',
                'class' => 'button-clr button-regular pr-3 pl-3 ' . ($totalCount > 0 ? 'button-regular_red' : 'button-hover-transparent'),
                'form' => 'outinvoice-form',
                'disabled' => $totalCount > 0 ? false : true,
                'style' => 'min-width: 160px'
            ]) ?>
        </div>
    </div>
</div>
<?php endif; ?>
