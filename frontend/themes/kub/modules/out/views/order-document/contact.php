<?php

use common\components\image\EasyThumbnailImage;
use common\models\Company;
use frontend\modules\out\models\OutInvoiceForm;
use frontend\modules\out\models\OutOrderDocumentForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap\ActiveForm */

//$this->title = 'Контакты покупателя';
$this->title = 'Оформление заказа у ' . Yii::$app->params['outCompany']->getTitle(true);
$this->params['showHeader'] = true;

$company = Yii::$app->params['outCompany'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$showHeader = empty($this->params['showHeader']) ? false : true;
$hasError = Yii::$app->params['hasError'];
$isDemo = Yii::$app->params['isDemo'];
if ($company && is_file($path = $company->getImage('logoImage'))) {
    $logoSrc = EasyThumbnailImage::thumbnailSrc(
        $path,
        $logoWidth,
        $logoHeight,
        EasyThumbnailImage::THUMBNAIL_INSET
    );
}

$searchModel = new \frontend\models\PriceListOrderSearch();
$searchModel->ownerPriceList = $model->priceList;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();
?>

<div class="header" style="display: none; height: 0;"></div>

<div class="out-invoice-form out-order-document-form">

    <?= $this->render('_header') ?>

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'uid' => $model->priceList->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $this->render('_orders', [
        'form' => $form,
        'model' => $model,
    ]) ?>

    <div class="wrap">
        <div class="row">
            <div class="col-6">
            <?= $form->field($model, 'email')->textInput(); ?>
            </div>
            <div class="col-6">
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                'options' => [
                    'class' => 'form-control',
                    'id' => 'legal-director_phone',
                    'placeholder' => '+7(XXX) XXX-XX-XX',
                ],
            ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <?= $form->field($model, 'comment')->textarea([
                'maxlength' => true,
                'class' => 'form-control placeholder-italic',
                'placeholder' => 'Если вы делаете заказ первый раз, напишите ваше ФИО'
            ]); ?>
            </div>
        </div>

        <?= Html::activeHiddenInput($model, 'legal_type') ?>
        <?= Html::activeHiddenInput($model, 'legal_name') ?>
        <?= Html::activeHiddenInput($model, 'legal_inn') ?>
        <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
        <?= Html::activeHiddenInput($model, 'legal_address') ?>
        <?= Html::activeHiddenInput($model, 'legal_rs') ?>
        <?= Html::activeHiddenInput($model, 'legal_bik') ?>
        <?= Html::activeHiddenInput($model, 'chief_position') ?>
        <?= Html::activeHiddenInput($model, 'chief_name') ?>
    </div>

    <div class="wrap wrap_btns fixed mb-0" id="totals-block" style="max-width: unset">
        <div class="row justify-content-end align-items-center">
            <div class="column mr-auto">
                <?php if ($model->scenario != OutInvoiceForm::SCENARIO_FIXPRODUCT) : ?>
                    <?= Html::submitButton('Назад', [
                        'class' => 'go-back-btn button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                        'form' => 'outinvoice-form',
                        'name' => 'back',
                        'value' => 'back',
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="column">
                <?= Html::submitButton('Продолжить', [
                    'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
                    'form' => 'outinvoice-form',
                    'style' => 'min-width: 160px'
                ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

