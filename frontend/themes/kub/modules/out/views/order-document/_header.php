<?php

use common\components\ImageHelper;
use frontend\modules\out\models\OutOrderDocumentForm;
use common\models\Company;
use common\components\image\EasyThumbnailImage;

$company = Yii::$app->params['outCompany'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$showHeader = empty($this->params['showHeader']) ? false : true;
$hasError = Yii::$app->params['hasError'];
$isDemo = Yii::$app->params['isDemo'];
if ($company && is_file($path = $company->getImage('logoImage'))) {
    $logoSrc = EasyThumbnailImage::thumbnailSrc(
        $path,
        $logoWidth,
        $logoHeight,
        EasyThumbnailImage::THUMBNAIL_INSET
    );
}
?>

<div class="header" style="margin-top:10px;">
    <table style="width: 100%">
        <tr>
            <?php if (is_file($path)) : ?>
            <td class="company-logo" style="width: 9%;">
                    <?= ImageHelper::getThumb($path, [200, 54.4], [
                        'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                    ]); ?>
            </td>
            <?php endif; ?>
            <td class="main-text text-left" style="<?php if (is_file($path)): ?>border-left: 1px solid #ddd; padding-left: 20px;<?php endif ?>">
                <?php if (Yii::$app->params['view'] == OutOrderDocumentForm::VIEW_PRODUCT): ?>
                    Оформление заказа
                    <br>
                    у <?= Yii::$app->params['outCompany']->getTitle(true) ?>
                <?php else: ?>
                    Оформление заказа
                    <br>
                    у <?= Yii::$app->params['outCompany']->getTitle(true) ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
</div>
