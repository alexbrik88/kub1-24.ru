<?php
use common\components\image\EasyThumbnailImage;
use common\models\Company;
use frontend\assets\AppAsset;
use frontend\modules\out\models\OutOrderDocumentForm;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

\frontend\themes\kub\assets\KubAsset::register($this);
$company = Yii::$app->params['outCompany'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$showHeader = empty($this->params['showHeader']) ? false : true;
$hasError = Yii::$app->params['hasError'];
$isDemo = Yii::$app->params['isDemo'];

if ($company && is_file($path = $company->getImage('logoImage'))) {
    $logoSrc = EasyThumbnailImage::thumbnailSrc(
        $path,
        $logoWidth,
        $logoHeight,
        EasyThumbnailImage::THUMBNAIL_INSET
    );
    $this->registerMetaTag([
        'property' => 'og:image',
        'content' => Url::to($logoSrc, true),
    ], 'og_image');
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=1" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css"/>
</head>
<body class="out-view-price-list theme-kub">
    <div class="wrapper__in">
        <?php $this->beginBody() ?>
        <div class="wrapper__content">
            <div class="container">
                <div class="price-list-view out-price-list-view">

                    <?= Alert::widget(); ?>

                    <div>
                        <?php if ($isDemo): ?>
                            <div class="demo-out-invoice rotatable">Образец</div>
                        <?php endif; ?>
                    </div>

                    <?php echo $content ?>

                    <?php if (!$company->hide_widget_footer) : ?>
                        <div style="margin-top: 20px;">
                            2015-<?= date('Y') ?> <?= Yii::$app->kubCompany->getTitle(true) ?>
                            <span class="pull-right">
                                <?= Html::a(
                                    'О сервисе КУБ',
                                    'https://kub-24.ru/price-list-1/?utm_source=lk_order_doc_price',
                                    ['target' => '_blank']
                                ) ?>
                            </span>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <?php $this->endBody(); ?>
    </div>

    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
        });
    </script>

</body>
</html>
<?php $this->endPage(); ?>
