<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<?php foreach ($model->product as $key => $value) : ?>
    <?= Html::activeHiddenInput($model, "product[{$key}]", ['value' => $value]); ?>
<?php endforeach ?>

<?php foreach ($model->price as $key => $value) : ?>
    <?= Html::activeHiddenInput($model, "price[{$key}]", ['value' => $value]); ?>
<?php endforeach ?>
