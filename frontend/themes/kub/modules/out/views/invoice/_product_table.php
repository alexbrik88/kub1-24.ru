<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */


$isArticle = (boolean) $model->outInvoice->show_article;
$paging = count($model->outInvoice->selectedProducts) > 10 ? 'true' : 'false';

$productArray = $model->outInvoice->selectedProducts;
$priceArray = [];
foreach ($productArray as $product) {
    $priceArray[$product->id] = isset($model->price[$product->id]) ?
        round($model->price[$product->id] * 100) :
        $product->price_for_sell_with_nds;
}

$jsColumn = $isArticle ? 'null,' : '';
$js = <<<JS
var priceValueFormat = function (value) {
    return new Intl.NumberFormat('ru-RU', {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(value);
}
var checkTotalCount = function() {
    var totalCount = 0;
    var totalSum = 0;
    $('#outinvoice-form .product-quantity-input').each(function(){
        var prodCount = Math.max(0, $(this).val() * 1);
        var prodSum = Math.round($(this).data('price') * prodCount);
        console.log(this);
        totalCount += prodCount;
        totalSum += prodSum;
        $('#outinvoice-form .prod-sum-'+$(this).data('product'))
            .text(priceValueFormat(prodSum/100));
    });
    $('#outinvoice-form .total-count').text(totalCount);
    $('#outinvoice-form .total-sum').text(priceValueFormat(totalSum/100));
    if (totalCount > 0) {
        $('#outinvoice-submit').text('Продолжить');
        $('#outinvoice-submit').prop('disabled', false);
    } else {
        $('#outinvoice-submit').text('Выберите количество');
        $('#outinvoice-submit').prop('disabled', true);
    }
}

$(document).on('click', '.quantity-btn', function(e){
    e.preventDefault();
    var type      = $(this).attr('data-type');
    var input     = $(this).closest('.quantity-box').find('.quantity-input');
    var val = parseInt(input.val());
    if(type == 'minus') {
        val--;
    } else if(type == 'plus') {
        val++;
    }
    input.val(Math.min(input.attr('max'), Math.max(input.attr('min'), val))).change();
});

$(document).on('change', '.quantity-input', function(){
    var box = $(this).closest('.quantity-box');
    var minus = box.find(".quantity-btn.type-minus");
    var plus = box.find(".quantity-btn.type-plus");
    var minVal =  parseInt($(this).attr('min'));
    var maxVal =  parseInt($(this).attr('max'));
    var val = Math.min(maxVal, Math.max(minVal, parseFloat($(this).val().replace(',', '.'))));

    minus.prop('disabled', val == minVal);
    plus.prop('disabled', val == maxVal);

    if ($(this).val() != String(val)) {
        $(this).val(val);
    }
    $($(this).data('target')).val(val);

    checkTotalCount();
});

$(document).on('keydown', '.quantity-input', function(){
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

checkTotalCount();

$.extend( $.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d,]/g, "" );
        return parseFloat( a );
    },
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );
var prodTable = $("#produc-table").DataTable({
    "paging": $paging,
    "info": false,
    "dom": "t<'row'<'col-sm-12'pl>>",
    "order": [[ 1, "asc" ]],
    "columns": [
        { "orderable": false, "searchable": false },
        {$jsColumn}
        null,
        { "type": "formatted-num", "searchable": false },
        { "orderable": false, "searchable": false },
    ],
    "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Все"]],
    "language": {
        "lengthMenu": "Выводить по _MENU_",
        "decimal": ",",
        "thousands": "&nbsp;",
    },
});
prodTable.on('order.dt', function () {
    prodTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
}).draw();
$(document).on('keyup paste', '#product-title-search', function() {
    prodTable.search(this.value).draw();
})
JS;

$this->registerJs($js);
?>
<style type="text/css">
table#produc-table {
    width: 100%;
    position: relative;
    margin-bottom: 5px;
    table-layout: fixed;
}
table#produc-table th {
    padding: 10px;
}
table#produc-table td {
    vertical-align: middle;
    box-sizing: border-box;
}
table.quantity-box td {
    vertical-align: middle;
    text-align: center;
}
table.quantity-box button.quantity-btn,
table.quantity-box button.quantity-btn:hover {
    height: 44px;
    width: 34px;
    margin: 0;
    padding: 5px;
    font-weight: bold;
    font-size: 20px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button,
.dataTables_wrapper .dataTables_paginate .paginate_button:hover,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
    padding: 0;
    border-width: 0;
}
</style>

<?php if (count($productArray) > 10) : ?>
    <div style="margin-bottom: 10px;">
        <?= Html::textInput('search', '', [
            'id' => 'product-title-search',
            'class' => 'form-control width-100',
            'placeholder' => 'Поиск по названию' . ($isArticle ? ' и артикулу' : ''),
            'autocomplete' => 'off'
        ]) ?>
    </div>
<?php endif; ?>

<table id="produc-table" class="table table-style table-count-list" style="table-layout: fixed;">
    <col width="40">
    <?php if ($isArticle ) : ?>
        <col width="100">
        <col width="208">
    <?php else : ?>
        <col width="308">
    <?php endif ?>
    <col width="100">
    <col width="150">
    <col width="100">
    <thead>
        <tr class="produc-table-row">
            <th style="">##</th>
            <?php if ($isArticle) : ?>
                <th style="">Артикул</th>
            <?php endif ?>
            <th style="">Наименование</th>
            <th style="text-align: center;">Цена (руб)</th>
            <th style="text-align: center;">Количество</th>
            <th style="text-align: center;">Стоимость</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($productArray as $product) : ?>
            <tr class="produc-table-row">
                <td style="text-align: right;">
                    <?= $i++; ?>
                </td>
                <?php if ($isArticle) : ?>
                    <td class="column-product-search" style="overflow: hidden;"><?= $product->article ?></td>
                <?php endif ?>
                <td class="column-product-search">
                    <?= $product->title ?>
                </td>
                <td style="text-align: right;">
                    <?php
                    $price = ArrayHelper::getValue($priceArray, $product->id, 0);
                    echo number_format(($price / 100), 2, ',', '&nbsp;');
                    ?>
                </td>
                <td style="padding: 0;">
                    <?php $inputName = "product[{$product->id}]"; ?>
                    <?php $inputValue = max(0, ArrayHelper::getValue($model->product, $product->id, 0)); ?>
                    <div style="margin: 5px 0;">
                        <table class="input-group quantity-box" style="width: 100%; margin: 0;">
                            <tr>
                                <td style="padding: 0;">
                                    <?= Html::button('<strong>-</strong>', [
                                        'class' => 'button-regular button-regular_red quantity-btn type-minus',
                                        'disabled' => ($inputValue == 0) ? true : false,
                                        'data-type' => 'minus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                                <td style="padding: 0 3px;">
                                    <?= Html::textInput("prod[{$product->id}]", $inputValue, [
                                        'class' => 'form-control input-number quantity-input',
                                        'style' => 'text-align: center; border-radius: 4px !important; padding: 6px;',
                                        'min' => 0,
                                        'max' => OutInvoiceForm::MAX_VALUE,
                                        'data-target' => "#data-product-{$product->id}",
                                    ]) ?>
                                </td>
                                <td style="padding: 0;">
                                    <?= Html::button('<strong>+</strong>', [
                                        'class' => 'button-regular button-regular_red quantity-btn type-plus',
                                        'disabled' => false,
                                        'data-type' => 'plus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class="prod-sum-<?=$product->id?>" style="text-align: right;">
                    0
                </td>
            </tr>
        <?php endforeach; ?>
        <tr class="font-weight-bold">
            <td colspan="<?= $isArticle ? 4 : 3; ?>">Итого</td>
            <td class="total-count text-center">0</td>
            <td class="total-sum text-right">0</td>
        </tr>
    </tbody>
</table>

<?php
foreach ($productArray as $product) {
    $inputValue = max(0, ArrayHelper::getValue($model->product, $product->id, 0));
    $price = ArrayHelper::getValue($priceArray, $product->id, 0);
    echo Html::activeHiddenInput($model, "product[{$product->id}]", [
        'value' => $inputValue,
        'class' => 'product-quantity-input',
        'data-product' => $product->id,
        'data-price' => $price,
    ]);
}
?>
