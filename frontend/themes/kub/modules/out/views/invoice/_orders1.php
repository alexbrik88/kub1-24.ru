<?php

use common\models\currency\Currency;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap4\ActiveForm */

$totalPrice = 0;
$totalQuantity = 0;

$isArticle = (boolean) $model->outInvoice->show_article;
$currency_name = ArrayHelper::getValue(Currency::findOne(['name' => $model->currency]), 'name', Currency::DEFAULT_NAME);
?>

<table class="table table-style table-count-list mb-4">
    <thead>
        <tr>
            <?php if ($isArticle) : ?>
                <th>Артикул</th>
            <?php endif ?>
            <th>Наименование</th>
            <th style="width: 110px; text-align: center;">Цена (<?=$currency_name?>)</th>
            <th style="width: 100px; text-align: center;">Количество</th>
            <th style="width: 110px; text-align: center;">Стоимость</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->getOrderList() as $order) : ?>
            <?php
            $rowPrice = bcmul($order->price, $order->quantity, 2);
            $totalPrice = bcadd($totalPrice, $rowPrice, 2);
            $totalQuantity += $order->quantity;
            ?>
            <tr>
                <?php if ($isArticle) : ?>
                    <td><?= $order->getProduct()->article ?></td>
                <?php endif ?>
                <td>
                    <?= $order->title ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($order->price, 2, ',', '&nbsp;') ?>
                </td>
                <td style="text-align: center;">
                    <?= $order->quantity ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($rowPrice, 2, ',', '&nbsp;') ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr style="font-weight: bold;">
            <td colspan="<?= $isArticle ? 3 : 2; ?>" style="text-align: right !important;">Итого</td>
            <td style="text-align: center;">
                <?= $totalQuantity ?>
            </td>
            <td style="text-align: right;">
                <?= number_format($totalPrice, 2, ',', '&nbsp;') ?>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('_orders_form1', [
    'form' => $form,
    'model' => $model,
]) ?>
