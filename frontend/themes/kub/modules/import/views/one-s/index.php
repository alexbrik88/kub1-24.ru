<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */
/* @var $emailModel \frontend\modules\import\models\autoload\OneSAutoloadEmail */

use \frontend\modules\import\models\autoload\OneSAutoloadEmail;
use yii\widgets\Pjax;

$this->title = 'Загрузка из 1С';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!empty($emailModel) && $emailModel->enabled && $emailModel->is_exit_by_error): ?>
        <div style="color:red; position: absolute; right:15px; top:0;">
            <?= OneSAutoloadEmail::MESSAGE_AUTOLOAD_DISABLED ?>
        </div>
    <?php endif; ?>
    <div class="wrap pt-2 pb-1 pl-4 pr-3">
        <div class="pt-2 pb-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="col-9 mr-auto">
                    <div class="row">
                        <div class="column" style="margin-top:-2px;">
                            <h4 class="mb-2">
                                <?= $this->title ?>
                            </h4>
                        </div>
                        <div class="column" style="margin-top:-7px;">
                            <button class="button-regular button-hover-transparent button-width" data-toggle="toggleVisible" data-target="invoice">
                                Инструкция
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-3 pl-1 pr-0" style="margin-top:-9px">
                    <?= \yii\helpers\Html::button('Загрузить данные из 1C', [
                        'class' => 'button-regular w-100 button-regular_red mb-2',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#import-xml',
                        ],
                    ]); ?>
                    <?= \yii\helpers\Html::button('Настройка АвтоЗагрузки', [
                        'class' => 'show-autoload-modal button-regular w-100 button-regular_red mb-0'
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap wrap_count mt-2">
        <div class="col-12">
            <div class="mt-1 mb-2"><strong>История загрузок</strong></div>

            <?php Pjax::begin([
                'id' => 'import-list',
            ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'emailModel' => $emailModel
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>


<?php
echo $this->render('_viewPartials/_help');
echo $this->render('_viewPartials/_modal');
echo $this->render('_viewAuto/_modal');
?>

<script>

    $(document).on('click', '.show-autoload-modal', function(e) {
        e.preventDefault();
        $('#email-upload-modal').modal();
        $.pjax({
            url: '/import/one-s/get-autoload-modal',
            container: '#email-upload-pjax',
            data: {},
            push: false,
            timeout: 5000
        });
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    let $xmlError = $('.xml-error');
    let $xmlButton = $('.xml-buttons');
    let $xmlCreatedModels = $('.created-models');

    function validateXml(fileExtension, fileSize) {
        let maxUploadedFileSize = 100 * 1024 * 1024;
        let $extensions = ['xml'];
        let $validation = true;

        if (fileSize > maxUploadedFileSize) {
            $xmlError.html('Превышен максимальный размер файла. Допустимый размер файла: 100 Мб');
            $xmlError.show();
            $validation = false;
        }

        if ($.inArray(fileExtension, $extensions) == -1) {
            $xmlError.html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + $extensions.join(', '));
            $xmlError.show();
            $validation = false;
        }

        if ($validation) {
            $xmlError.hide();
        }
        return $validation;
    }

    function initXmlUpload() {
        vidimusUploader = new ss.SimpleUpload({
            button: $('.add-xml-file'),
            url: '/import/one-s-ajax/upload', // server side handler
            progressUrl: '/import/one-s-ajax/progress', // enables cross-browser progress support (more info below)
            responseType: 'json',
            name: 'uploadfile',
            allowedExtensions: ['xml'], // for example, if we were uploading pics
            hoverClass: 'ui-state-hover',
            focusClass: 'ui-state-focus',
            disabledClass: 'ui-state-disabled',
            form: $('#xml-ajax-form'),
            multiple: false,
            multipleSelect: false,
            onSubmit: function (filename, extension) {
                let progress = document.createElement('div'),
                    bar = document.createElement('div'),
                    fileSize = document.createElement('div'),
                    wrapper = document.createElement('div'),
                    progressBox = document.getElementById('progressBox');
                $xmlError.hide();
                progress.className = 'progress progress-striped';
                bar.className = 'progress-bar progress-bar-success';
                fileSize.className = 'size';
                wrapper.className = 'wrapper';
                progress.appendChild(bar);
                wrapper.innerHTML = '<div class="name">' + filename + '</div>';
                wrapper.appendChild(progress);
                progressBox.appendChild(wrapper);
                this.setProgressBar(bar);
                this.setFileSizeBox(fileSize);
                this.setProgressContainer(wrapper);
            },
            onChange: function (filename, extension, uploadBtn, fileSize, file) {
                let $maxFilesCount = 1;
                let $filesQueueKey = vidimusUploader.getQueueSize();
                let $filesCount = $filesQueueKey + 1;
                if (!validateXml(extension, fileSize)) {
                    this.removeCurrent();
                    return false;
                }
                $xmlButton.hide();
                $xmlError.hide();
                $('.upload-xml-button a').removeClass('disabled');
                $('.upload-xml-button a.tooltipstered').tooltipster('disable');
                if ($filesCount == $maxFilesCount) {
                    $('.add-xml-file').addClass('disabled');
                }
                let $fileItemHtml = $('<div class="item">'
                    + '<div class="file-name">' + filename + '</div>'
                    + '<a href="#" data-queue="0" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                    + '<div></div>'
                    + '</div>');
                $('.file-list').append($fileItemHtml);
            },
            onComplete: function (filename, data) {

                $('.add-xml-file').addClass('disabled');
                $('input[name="uploadfile"]').prop('disabled', true);

                ///////////////////
                /// START PARSE ///
                ///////////////////
                let inProcess = false;
                let uploadTotals = '';
                let progressTimerId;
                let showTotals = function(title, data) {
                    // totals
                    uploadTotals = title + '<br/>';
                    uploadTotals += '<div class="info">';
                    uploadTotals += '<b>Загружено:</b>' + '<br/>' + data.uploadedItems;
                    uploadTotals += '<b>Дубли загрузки:</b>' + '<br/>' + data.doubleItems;
                    uploadTotals += '</div>';
                    $xmlError.html(uploadTotals);
                    $xmlError.show();
                };
                let refreshTotals = function() {
                    $.post('/import/progress/one-s', [], function (data) {
                        if (inProcess) {
                            showTotals('Дождитесь окончания загрузки ' + '<img width="24" src="/img/import-preloader.gif">', data);

                            if (data.completed) {
                                uploadsCompleted(data);
                            }
                        }
                    });
                };
                let uploadsCompleted = function(data) {
                    inProcess = false;
                    clearInterval(progressTimerId);
                    showTotals('Данные успешно загружены!', data);
                    $.pjax.reload('#import-list');
                    window.toastr.success("Данные успешно загружены", "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                    $('.add-xml-file').removeClass('disabled');
                    $('input[name="uploadfile"]').prop('disabled', false);
                    $('#import-xml .modal-close, #import-xml .modal-close-2').prop('disabled', false);
                };

                if (data.result) {

                    inProcess = true;

                    showTotals('Дождитесь окончания загрузки ' + '<img width="24" src="/img/import-preloader.gif">', data);

                    $.post('/import/one-s-ajax/parse', [], function (data) {});

                    progressTimerId = setInterval(refreshTotals, 2000);

                } else {

                    window.toastr.success(data.message, "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                }
            },
        });
    }

    $(document).ready(function () {

        $(document).on('click', '.upload-xml-button a', function (e) {
            e.preventDefault();
            $('.file-list .item').remove();
            $('.upload-xml-button a').addClass('disabled');
            $('.upload-xml-button a.tooltipstered').tooltipster('enable');
            $('#import-xml .modal-close, #import-xml .modal-close-2').prop('disabled', true);
            $('#xml-ajax-form').submit();
        });

        $('.file-list').on('click', '.delete-file', function (e) {
            e.preventDefault();
            $(this).parent().remove();
            vidimusUploader['_queue'].splice(0, 1);
            $('.upload-xml-button a').addClass('disabled');
            $('.upload-xml-button a.tooltipstered').tooltipster('enable');
            $('.add-xml-file').removeClass('disabled');
        });
        
        ////////////////
        initXmlUpload();
        ///////////////
    });
</script>

<style>
    .modal-close:disabled {
        opacity: .25!important;
        cursor: not-allowed!important;
    }
</style>