<?php
use common\components\ImageHelper;
use yii\helpers\Html;

$emailArr = explode('.', $model->email);
$customEmailPart = array_shift($emailArr);
$basisEmailPart = implode('.', $emailArr);

/** @var $model \frontend\modules\import\models\autoload\OneSAutoloadEmail */
?>

<h4 class="modal-title">Загрузка документов по e-mail</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row">
        <div class="col-12">
            <div class="mb-1">У вас есть способ избежать ручной загрузки файлов из 1С.</div>
            <div class="mb-1">Для этого настройте отправку выгрузок из 1С.</div>
            <div class="mb-1">
                <ul class="instructions">
                    <li>Скачайте файл обработку для АвтоЗагрузки из 1С <a href="/xls/download-epf?type=import1C">тут</a></li>
                    <li>
                        Откройте скаченный файл из вашей 1С и введите в запросе e-mail, который КУБ24 создал для вас:<br/>
                        <span style="color:#007bff"><?= ($oldEmail ?? '--') ?></span>
                    </li>
                    <li>Каждое утро в 8.30 ваша 1C будет формировать выгрузку, и отправлять на данный e-mail</li>
                    <li>КУБ24 будет обрабатывать полученный файл, и загружать новые документы в КУБ24.</li>
                </ul>
            </div>
            <div class="email-upload-text-wrap mt-2" style="<?= !$model->enabled ? '':'display:none' ?>">
                Не нравится e-mail, который мы вам создали?
                <a href="javascript:" class="btn-enable-email-upload">Выберите сами!</a>
            </div>
        </div>
        <div class="email-upload-text-wrap w-100"  style="<?= !$model->enabled ? '':'display:none' ?>">
            <div class="mt-3 text-center">
                <button class="btn-enable-email-upload button-regular button-width button-regular_red button-clr">Включить</button>
            </div>
        </div>
    </div>

    <div class="email-upload-form-wrap" style="<?= $model->enabled ? '':'display:none' ?>">
        <?php
        $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
            'action' => \yii\helpers\Url::to("/import/one-s/get-autoload-modal"),
            'method' => 'POST',

            'options' => [
                'id' => 'email-upload-form',
                'enctype' => 'multipart/form-data',
                'data-pjax' => true
            ],

            'fieldConfig' => [
                'labelOptions' => [
                    'class' => 'col-md-4 control-label',
                ],
            ],
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'validateOnSubmit' => false,
            'validateOnBlur' => false,
        ]));
        ?>

        <?= Html::activeHiddenInput($model, 'enabled', ['value' => '1']); ?>

        <div class="row">
            <div class="col-12">
                <label class="control-label" style="font-weight: bold" for="onesautoloademail-customemailpart">
                    Подтвердите ваш e-mail, на которые вы будете пересылать документы:</label>
            </div>
            <div class="col-12 mt-2">
                <div class="row">
                    <div class="col-4 pr-1">
                        <input type="text" id="onesautoloademail-customemailpart" class="form-control" name="OneSAutoloadEmail[customEmailPart]" value="<?=$customEmailPart?>" aria-required="true" <?= $model->enabled ? 'readonly' : '' ?>>
                    </div>
                    <div class="col-8 pl-1">
                        <input type="text" id="onesautoloademail-basisemailpart" class="form-control" name="OneSAutoloadEmail[basisEmailPart]" value="<?=$basisEmailPart?>" aria-required="true" readonly>
                    </div>
                </div>
                <p class="help-block help-block-error ">
                    <?= $model->getFirstError('customEmailPart') ?>
                    <?= $model->getFirstError('email') ?>
                </p>
            </div>
        </div>

        <?php if (!$model->enabled): ?>
            <div class="mt-3 d-flex justify-content-between">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-width button-regular_red button-clr',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <button type="button" class="button-clr button-regular button-widthbutton-hover-transparent" data-dismiss="modal" style="width: 130px!important;">Отменить</button>
            </div>
        <?php else: ?>
            <div class="mt-3 d-flex justify-content-between">
                <button type="button" class="button-clr button-regular button-widthbutton-hover-transparent ml-auto" data-dismiss="modal" style="width: 130px!important;">Закрыть</button>
            </div>
        <?php endif; ?>

        <?php
        $form->end();
        ?>
    </div>

</div>

<?php
$this->registerJs("
    $(document).on('click', '.btn-enable-email-upload', function(e) {
        $('.email-upload-text-wrap').hide();
        $('.email-upload-form-wrap').fadeIn(200);
    });
"); ?>

