<?php
use common\components\grid\DropDownDataColumn;
use common\models\employee\Employee;
use frontend\modules\import\models\Import1c;
use common\components\date\DateHelper;
use common\components\TextHelper;

?>
<div class="table-container" style="">
    <div class="dataTables_wrapper dataTables_extended_wrapper">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],
            //'options' => [
            //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            //],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата загрузки',
                    'headerOptions' => ['width' => '10%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return date('d.m.Y', $model->created_at);
                    }
                ],
                [
                    'attribute' => 'employee_id',
                    'label' => 'Ответственный',
                    'headerOptions' => ['width' => '15%'],
                    'filter' => $searchModel->getEmployeeByCompanyArray(\Yii::$app->user->identity->company->id, $emailModel),
                    'format' => 'raw',
                    'value' => function (Import1c $model) use ($emailModel) {
                        if ($model->is_autoload) {
                            if ($emailModel)
                                return $emailModel->email;
                            else
                                return 'Автозагрузка';
                        }

                        return $model->employee instanceof Employee ? $model->employee->currentEmployeeCompany->getFio(true) : '-';
                    },
                    's2width' => '200px',
                ],
                [
                    'attribute' => 'period',
                    'label' => 'Период загрузки',
                    'headerOptions' => ['width' => '15%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return $model->period;
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'Контрагенты',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_contractors, 0);
                    },
                ],
                [
                    'attribute' => 'total_products',
                    'label' => 'Товары Услуги',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_products, 0);
                    },
                ],
                [
                    'attribute' => 'total_in_invoices',
                    'label' => 'Счета Вх.',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_invoices, 0);
                    },
                ],
                [
                    'attribute' => 'total_in_upds',
                    'label' => 'УПД Вх.',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_upds, 0);
                    },
                ],
                [
                    'attribute' => 'total_in_invoice_factures',
                    'label' => 'СФ Вх.',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_invoice_factures, 0);
                    },
                ],
                [
                    'attribute' => 'total_out_invoices',
                    'label' => 'Счета Исх.',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_out_invoices, 0);
                    },
                ],
                [
                    'attribute' => 'total_out_upds',
                    'label' => 'УПД Исх',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_out_upds, 0);
                    },
                ],
                [
                    'attribute' => 'total_out_invoice_factures',
                    'label' => 'СФ Исх.',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_out_invoice_factures, 0);
                    },
                ],
                [
                    'attribute' => 'errors_messages',
                    'label' => 'Ошибки загрузки',
                    'headerOptions' => ['width' => '25%'],
                    'contentOptions' => ['style' => 'font-size: 12px'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return '<div style="max-height:99px; overflow-y:auto">'.nl2br($model->errors_messages).'</div>';
                    },
                ],
            ],
        ]); ?>
    </div>
</div>