<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.06.2018
 * Time: 7:40
 */

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="invoice-wrap" data-id="invoice">
    <div class="invoice-wrap-in invoice-wrap-scroll">
        <button class="invoice-wrap-close button-clr" type="button" data-toggle="toggleVisible" data-target="invoice">
            <?= $this->render('//svg-sprite', ['ico' => 'close']) ?>
        </button>

        <div class="main-block">
            <p class="bold" style="font-size:19px;">
                Загрузка в КУБ из 1С
            </p>

            <p>
                <strong>1</strong>. <a href="/xls/download-epf?type=import1C">Скачать обработку для 1С Бухгалтерия.</a>
            </p>

            <p>
                <strong>2</strong>. Откройте базу 1С на своем ПК.
            </p>

            <p>
                <strong>3</strong>. Откройте папку с обработкой, которую скачали из КУБ24:
            </p>
            <p>
                <img style="width:426px" src="/img/documents/kub/import_1c_1.jpg"/>
            </p>

            <p>
                <strong>4</strong>. Перенесите обработку в 1С, зажав файл левой кнопкой мыши.
            </p>

            <p>
                <strong>5</strong>. В базе 1С откроется обработка:
            </p>
            <p>
                <img style="width:426px" src="/img/documents/kub/import_1c_2.jpg"/>
            </p>

            <p>
                <strong>5.1</strong>. Выберите период выгрузки;
            </p>
            <p>
                <strong>5.2</strong>. Укажите папку на своем ПК, куда будете сохранен файл выгрузки из 1С;
            </p>
            <p>
                <strong>5.3</strong>. Выберите данные которые хотите выгрузить;
            </p>
            <p>
                <strong>5.4</strong>. Нажмите на кнопку сохранить в правом нижнем углу:
            </p>
            <p>
                <img style="width:426px" src="/img/documents/kub/import_1c_3.jpg"/>
            </p>

            <p>
                <strong>6</strong>. Перейдите в папку, которую указывали при выгрузке. В ней будет находиться файл выгрузки, который нужно будет загрузить в КУБ24:
            </p>
            <p>
                <img style="width:426px" src="/img/documents/kub/import_1c_4.jpg"/>
            </p>

            <br/>
            <br/>
            <br/>

        </div>

    </div>
</div>