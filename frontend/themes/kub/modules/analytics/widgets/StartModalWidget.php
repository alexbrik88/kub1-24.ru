<?php

namespace frontend\themes\kub\modules\analytics\widgets;

use common\models\Company;
use frontend\modules\analytics\assets\AnalyticsStartAsset;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 *
 */
class StartModalWidget extends \yii\base\Widget
{
    /**
     * @var commom\models\Company
     */
    public $company;

    /**
     * @var boolean
     */
    public $isFirstVisit;

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (!$this->company->analytics_module_activated) {
            AnalyticsStartAsset::register($this->view);

            echo $this->render('startModalWidget', [
                'company' => $this->company,
                'isFirstVisit' => $this->isFirstVisit,
            ]);
        }
    }
}
