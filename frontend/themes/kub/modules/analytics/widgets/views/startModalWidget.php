<?php

use frontend\components\Icon;
use frontend\modules\analytics\assets\AnalyticsStartAsset;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $company common\models\Company */
/** @var $isFirstVisit boolean */

AnalyticsStartAsset::register($this);

?>

<?php Modal::begin([
    'id' => 'analytics-start-modal',
    'closeButton' => false,
    'options' => [
        'data-url' => Url::to(['/analytics/start/code']),
    ],
    'clientOptions' => [
        'show' => $isFirstVisit,
        'backdrop' => 'static',
        'keyboard' => false,
    ],
]); ?>

<?php Modal::end(); ?>
