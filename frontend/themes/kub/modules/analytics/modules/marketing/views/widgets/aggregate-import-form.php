<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\components\helpers\Html;
use frontend\modules\analytics\modules\marketing\models\AggregateImportForm;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

/**
 * @var View $this
 * @var AggregateImportForm $form
 */

$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';

?>

<div class="statement-service-content" style="position: relative; min-height: 110px;">
<h4 class="mb-4">Запрос данных</h4>

<?php $widget = ActiveForm::begin([
    'id' => 'import-dialog-form',
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-pjax' => true,
    ],
]) ?>

<div class="row">
    <?= $widget->field($form, 'dateFrom', [
        'options' => [
            'class' => 'form-group col-6',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter date-picker-wrap',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
    ])->textInput([
        'class' => 'form-control date-picker',
    ]); ?>

    <?= $widget->field($form, 'dateTo', [
        'options' => [
            'class' => 'form-group col-6',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter date-picker-wrap',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
    ])->textInput([
        'class' => 'form-control date-picker',
    ]); ?>
</div>
<div class="d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Отправить запрос</span><span class="ladda-spinner"></span>', [
        'class' => 'button-clr button-regular button-regular_red',
        'data-style' => 'expand-right',
    ]) ?>

    <?= Html::button('Отменить', [
        'class' => 'button-clr button-regular button-hover-transparent',
        'data-dismiss' => 'modal',
        'title' => 'Отменить',
        'style' => 'width: 150px;',
    ]) ?>
</div>
<?php ActiveForm::end() ?>
</div>
