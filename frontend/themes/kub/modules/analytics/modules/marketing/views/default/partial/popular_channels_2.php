<?php

use yii\web\JsExpression;
use yii\bootstrap\Nav;
use miloschuman\highcharts\Highcharts;
use php_rutils\RUtils;

/* @var array $tableData
 */

$chartLabelsX= [];
foreach ($tableData['x'] as $date) {
    $dateArr = explode('-', $date);
    $chartLabelsX[] = $dateArr[2] . ' ' . RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $date,
    ]);
}
?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header">
                Ключевые показатели
            </div>
        </div>
        <div class="nav-tabs-row">
            <?= Nav::widget([
                'id' => 'graph_statistics_channels_nav',
                'encodeLabels' => false,
                'options' => [
                    'class' =>
                        'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 key-indicators-nav',
                    ],
                'items' => [
                    [
                        'label' => 'Все',
                        'url' => '#',
                        'active' => true,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link active',
                        ]
                    ],
                    [
                        'label' => 'Яндекс.Директ',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                    [
                        'label' => 'ВК',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                    [
                        'label' => 'Google.Ads',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                    [
                        'label' => 'FB',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                ],
            ]) ?>
        </div>
        <table class="table table-style table-count-list form-group popular_table marketing-key-indicators-table">
            <thead>
            <tr>
                <th width="35">Показатель</th>
                <th class="text-right" width="15">Сегодня</th>
                <th class="text-right" width="15">Вчера</th>
                <th class="text-right" width="15">Среднее</th>
                <th width="25"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($tableData['indicator'] as $data): ?>
                <tr>
                    <td>
                        <span class="pt-name"><?= $data['name'] ?></span><br/>
                        <span class="pt-subname"><?= $data['subname'] ?></span>
                    </td>
                    <td>
                        <span class="pt-name"><?= $data['today']['value'] . $data['unit_name'] ?></span><br/>
                        <span class="pt-percent <?= $data['today']['percent'] < 0 ? 'red' : 'green' ?>">
                            <?= $data['today']['percent'] > 0 ? "+{$data['today']['percent']}" : $data['today']['percent']?> %
                        </span>
                    </td>
                    <td>
                        <span class="pt-name one-line"><?= $data['yesterday']['value'] . $data['unit_name'] ?></span><br/>
                    </td>
                    <td>
                        <span class="pt-name one-line"><?= $data['average'] ?></span>
                    </td>
                    <td style="padding-left: 10px;">
                        <?= Highcharts::widget([
                                'scripts' => [
                                    'modules/exporting',
                                    'modules/pattern-fill',
                                    'themes/grid-light',
                                ],
                                'options' => [
                                    'title' => false,
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'legend' => [
                                        'enabled' => false
                                    ],
                                    'exporting' => [
                                        'enabled' => false
                                    ],
                                    'chart' => [
                                        'type' => 'column',
                                        'height' => 30,
                                        'margin' => [0, 0, 0, 0],
                                        'spacing' => [0, 0, 0, 0],
                                        'showAxes' => false,
                                        'reflow' => true
                                    ],
                                    'tooltip' => [
                                        'useHTML' => true,
                                        'outside' => true,
                                        'shared' => false,
                                        'backgroundColor' => "rgba(255,255,255,1)",
                                        'borderColor' => '#ddd',
                                        'borderWidth' => '1',
                                        'borderRadius' => 8,
                                        'formatter' => new JsExpression("
                                            function(args) {
                                                return '<table class=\"ht-in-table\">' +
                                                    '<tr><th class=\"text-left\" colspan=\"3\">' + window.chartLabelsX[this.point.x] + '</th></tr>' +
                                                    '<tr><td>' + '{$data['name']}' + '</td><td></td><td>' + this.y + '</td></tr>' +
                                                    '</table>';
                                            }
                                        ")
                                    ],
                                    'yAxis' => [
                                        [
                                            'title' => '',
                                            'labels' => false,
                                            'gridLineWidth' => 0,
                                            'minorGridLineWidth' => 0,
                                            'lineWidth' => 0,
                                        ],
                                    ],
                                    'xAxis' => [
                                        [
                                            'categories' => $tableData['x'],
                                            'labels' => false,
                                            'gridLineWidth' => 0,
                                            'minorGridLineWidth' => 0,
                                            'lineWidth' => 0,
                                        ],
                                    ],
                                    'series' => [
                                        [
                                            'name' => $data['name'],
                                            'data' => $data['y'],
                                            'color' => '#5F9EA0',
                                            'borderRadius' => '4.5',
                                            'marker' => [
                                                'enabled' => false
                                            ],
                                            'groupPadding' => 0,
                                        ],
                                    ],
                                ],
                            ]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(window).on('load', function () {
        $('.key-indicators-nav').tabdrop();
    });

    var chartLabelsX = <?= json_encode($chartLabelsX) ?>;
</script>
