<?php

namespace frontend\modules\analytics\modules\marketing\views;

use frontend\modules\analytics\modules\marketing\models\GoogleAnalyticsSettingsForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\Pjax;
use Yii;
use common\components\helpers\Html;
use kartik\select2\Select2;
use frontend\themes\kub\helpers\Icon;


/* @var GoogleAnalyticsSettingsForm $model
 * @var View $this
 */

$jsAnalyticsProperties = json_encode($model->getAnalyticsProperties());
$jsAnalyticsViews = json_encode($model->getAnalyticsViews());
$jsAnalyticsGoals = json_encode($model->getAnalyticsGoals());
$profileAvatar = "<img src=\"{$model->avatar}\" class=\"google-account-avatar\" width=\"30px\">";
$disconnectUrl = Url::to('/analytics/marketing/default/disconnect-google-analytics');
$disconnectIcon = "<span class='glyphicon glyphicon-remove-circle remove-google-account' data-url='{$disconnectUrl}'></span>";
$analyticsAccountQuestionTooltip = '<span class="tooltip2" data-tooltip-content="#tooltip_analytics_account">' . Icon::QUESTION . '</span>';
$analyticsPropertyQuestionTooltip = '<span class="tooltip2" data-tooltip-content="#tooltip_analytics_property">' . Icon::QUESTION . '</span>';
$analyticsViewQuestionTooltip = '<span class="tooltip2" data-tooltip-content="#tooltip_analytics_view">' . Icon::QUESTION . '</span>';
$analyticsGoalQuestionTooltip = '<span class="tooltip2" data-tooltip-content="#tooltip_analytics_goal">' . Icon::QUESTION . '</span>';
$goals = $model->getAnalyticsGoals()[$model->analyticsView] ?? [];

Pjax::begin([
    'id' => 'google-analytics-connect-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="google-analytics-connect-form">
    <?php $form = ActiveForm::begin([
        'id' => 'google-analytics-connect-form',
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'action' => ['google-analytics-settings'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]); ?>

    <?= Html::hiddenInput(null, $jsAnalyticsProperties, [
        'id' => 'jsAnalyticsProperties',
    ]) ?>

    <?= Html::hiddenInput(null, $jsAnalyticsViews, [
        'id' => 'jsAnalyticsViews',
    ]) ?>

    <?= Html::hiddenInput(null, $jsAnalyticsGoals, [
        'id' => 'jsAnalyticsGoals',
    ]) ?>

    <?= $form->field($model, 'name', [
        'template' => "{label}\n{beginWrapper}\n{$profileAvatar}{input}{$disconnectIcon}\n{error}\n{hint}\n{endWrapper}"
    ])->textInput(['readonly' => true])->label(false) ?>

    <?= $form->field($model, 'analyticsAccount', [
        'template' => "{label}{$analyticsAccountQuestionTooltip}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}"
    ])->widget(Select2::class, [
        'hideSearch' => true,
        'data' => $model->getAnalyticsAccounts(),
        'options' => [
            'placeholder' => 'Выберите аккаунт Google Analytics',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $form->field($model, 'analyticsProperty', [
        'template' => "{label}{$analyticsPropertyQuestionTooltip}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}"
    ])->widget(Select2::class, [
        'hideSearch' => true,
        'data' => $model->getAnalyticsProperties()[$model->analyticsAccount] ?? null,
        'options' => [
            'placeholder' => 'Выберите ресурс в этом аккаунте',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $form->field($model, 'analyticsView', [
        'template' => "{label}{$analyticsViewQuestionTooltip}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}"
    ])->widget(Select2::class, [
        'hideSearch' => true,
        'data' => $model->getAnalyticsViews()[$model->analyticsProperty] ?? null,
        'options' => [
            'placeholder' => 'Выберите представление',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $form->field($model, 'mainAnalyticsGoal' , [
        'template' => "{label}{$analyticsGoalQuestionTooltip}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ])->widget(Select2::class, [
        'hideSearch' => true,
        'data' => $goals,
        'options' => [
            'placeholder' => 'Выберите цель',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?php
    $maxGoals = range(1, 20); // (count($goals) > 1) ? range(1, count($goals) - 1) : [];

    $goalRemoveButton = '
        <button class="remove-product-from-invoice button-clr pl-3 js-goal-remove" type="button">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>-->
            </svg>
        </button>
        ';
    ?>

    <?php foreach ($maxGoals as $index): ?>

        <?php
        $attributeName = 'additionalAnalyticsGoals[' . $index . ']';
        $fieldGoalHideClass = '';

        if (!isset($model->additionalAnalyticsGoals[$index])) {
            $fieldGoalHideClass = 'hide';
        }

        $fieldGoalTemplate = "<div class=\"js-field-goal $fieldGoalHideClass\">{label}{$analyticsGoalQuestionTooltip}\n{beginWrapper}\n<div style=\"display: flex;\">{input}{$goalRemoveButton}</div>\n{error}\n{hint}\n{endWrapper}</div>";
        ?>

        <?= $form->field($model, $attributeName , [
            'template' => $fieldGoalTemplate,
        ])->widget(Select2::class, [
            'hideSearch' => true,
            'data' => $goals,
            'options' => [
                'placeholder' => 'Выберите цель',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
    <?php endforeach; ?>

    <?php
    $goalAddNewButtonDisabledClass = '';
    $goalAddNewButtonDisabled = '';

    if ((count($model->additionalAnalyticsGoals) + 1) === count($goals)) {
        $goalAddNewButtonDisabledClass = 'disabled';
        $goalAddNewButtonDisabled = 'disabled';
    }
    ?>
    <div class="pb-3">
        <button class="btn-add-line-table button-regular button-hover-content-red width-160 js-goal-add-new <?= $goalAddNewButtonDisabledClass ?>" type="button" <?= $goalAddNewButtonDisabled ?>>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
            </svg>
            <span>Добавить цель</span>
        </button>
    </div>

    <div class="mt-3 d-flex justify-content-between <?= $model->isIntegrationConnected() ? 'mb-3' : null ?>">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button width-160',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                data-dismiss="modal">
            Отменить
        </button>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if ($model->isIntegrationConnected()): ?>
        <?= Html::a('Отключить интеграцию с "Google Analytics"', $disconnectUrl) ?>
    <?php endif; ?>
</div>
<?php Pjax::end() ?>
