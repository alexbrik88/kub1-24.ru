<?php

use common\modules\marketing\models\MarketingUserConfig;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;

/** @var MarketingUserConfig $config */

?>

<div class="dropdown dropdown-settings d-inline-block" title="Настройка графика" style="z-index: 1001">
    <?= Html::button(Icon::get('cog'), [
        'class' => 'marketing-dropdown-btn-icon',
        'data-toggle' => 'dropdown',
    ]) ?>
    <ul id="marketing_dynamics_chart_config" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
        <li>
            <label class="bold nowrap">По умолчанию открывать</label>
        </li>
        <?php foreach (MarketingUserConfig::$chartPeriodName as $periodId => $periodName): ?>
            <li class="_chart_user_config_option" <?php if (!isset($_bt1)) { echo 'style="border-top: 1px solid #ddd"'; $_bt1 = true; } ?>>
                <?= Html::radio('MarketingUserConfig[dynamics_chart_period]', $config->dynamics_chart_period == $periodId, [
                    'class' => 'chart_user_config_radio',
                    'label' => $periodName,
                    'value' => $periodId
                ]); ?>
            </li>
        <?php endforeach; ?>
        <?php foreach (MarketingUserConfig::$chartTypeName as $typeId => $typeName): ?>
            <li class="_chart_user_config_option" <?php if (!isset($_bt2)) { echo 'style="border-top: 1px solid #ddd"'; $_bt2 = true; } ?>>
                <?= Html::radio('MarketingUserConfig[dynamics_chart_type]', $config->dynamics_chart_type == $typeId, [
                    'class' => 'chart_user_config_radio',
                    'label' => $typeName,
                    'value' => $typeId
                ]); ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>