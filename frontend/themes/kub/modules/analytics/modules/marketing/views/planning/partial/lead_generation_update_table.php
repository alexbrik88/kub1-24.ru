<?php

/* @var $this yii\web\View
 * @var $model \frontend\modules\analytics\modules\marketing\models\UpdatePlanningMarketingForm
 * @var $user \common\models\employee\Employee
 */

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\marketing\MarketingCalculatingPlanningSetting;
use frontend\modules\analytics\models\AbstractFinance;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;
use frontend\themes\kub\helpers\Icon;

$itemsCount = count($model->items) ?: 1;
$totalBudgetAmount = 0;
$totalMarketingServiceAmount = 0;
$totalAdditionalExpensesAmount = 0;
$totalTotalAdditionalExpensesAmount = 0;
$totalAdditionalExpensesAmountForOne = 0;
$totalTotalExpensesAmount = 0;
$totalClicksCount = 0;
$totalClickAmount = 0;
$totalFinalCostsClickAmount = 0;
$totalRegistrationConversion = 0;
$totalRegistrationCount = 0;
$totalRegistrationAmount = 0;
$totalFinalCostsRegistrationAmount = 0;
$totalActiveUserConversion = 0;
$totalActiveUsersCount = 0;
$totalActiveUserAmount = 0;
$totalFinalCostsUserAmount = 0;
$totalPurchaseConversion = 0;
$totalPurchasesCount = 0;
$totalSaleAmount = 0;
$totalAverageCheck = 0;
$totalProceedsFromNewAmount = 0;
$totalChurnRate = 0;
$totalRepeatedPurchasesAmount = 0;
$totalTotalProceedsAmount = 0;
$totalFinancialResult = 0;
$totalRepeatedPurchasesCount = 0;
$totalTotalPurchasesCount = 0;
$totalSalespersonSalaryAmount = 0;
$budgetAmountCalculatingSettings = [];
foreach ([
             'budget_amount',
             'click_amount',
             'registration_conversion',
             'active_user_conversion',
             'purchase_conversion',
             'marketing_service_amount',
             'salesperson_salary_amount',
             'additional_expenses_amount',
         ] as $calculatingSettingAttribute) {
    $budgetAmountCalculatingSettings[$calculatingSettingAttribute] = MarketingCalculatingPlanningSetting::find()
        ->andWhere(['marketing_plan_id' => $model->getMarketingPlan()->id])
        ->andWhere(['channel' => $model->getChannel()])
        ->andWhere(['attribute' => $calculatingSettingAttribute])
        ->asArray()
        ->one();
}

$currencySymbol = $model->getMarketingPlan()->getCurrencySymbol();
?>
<?php Pjax::begin([
    'id' => "channel-planning-table-pjax-{$model->getChannel()}",
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<style>
    .table-wrap ::-webkit-scrollbar {
        height: 11px;
    }

    .table-wrap ::-webkit-scrollbar-track {
        border-radius: 0;
        box-shadow: none;
        border: 0;
        background-color: #fff;
    }

    .table-wrap ::-webkit-scrollbar-thumb {
        border-radius: 5px;
        box-shadow: none;
        border: 0;
        background-color: #bbc1c7;
    }

    .table-wrap ::-webkit-scrollbar-thumb:hover {
        background-color: #bbc1c7;
    }
</style>

<div class="wrap planning-table planning-table-<?= $model->getChannel() ?>" data-channel="<?= $model->getChannel() ?>" style="padding-left: 0;padding-top: 0;">
    <div class="table-wrap">
        <table class="scrollable-table double-scrollbar-top table table-style table-count-list table-compact finance-model-shop-month marketing-planning-table">
            <thead>
            <tr>
                <th class="fixed-column" style="min-width: 280px;">
                    Маркетинговый план
                </th>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <th class="nowrap">
                        <?= ArrayHelper::getValue(AbstractFinance::$month, str_pad($marketingPlanItem['month'], 2, "0", STR_PAD_LEFT)) . ' ' . $marketingPlanItem['year'] ?>
                    </th>
                <?php endforeach; ?>
                <th class="nowrap">
                    Итог
                </th>
            </tr>
            </thead>
            <tbody>
            <tr data-attr="budget_amount">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Рекламный бюджет, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['budget_amount']) ? json_encode($budgetAmountCalculatingSettings['budget_amount']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalBudgetAmount += (int)$marketingPlanItem['budget_amount'] ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][budget_amount]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['budget_amount'], 2),
                            [
                                'class' => 'custom-value form-control ' . "budget_amount-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalBudgetAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="click_amount">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Стоимость клика, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['click_amount']) ? json_encode($budgetAmountCalculatingSettings['click_amount']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php $nonEmptyItemsCount = 0 ?>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php
                    $totalClickAmount += (int)$marketingPlanItem['click_amount'];
                    if ((int)$marketingPlanItem['click_amount'] > 0) {
                        $nonEmptyItemsCount++;
                    }
                    ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][click_amount]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['click_amount'], 2),
                            [
                                'class' => 'custom-value form-control ' . "click_amount-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat(round($totalClickAmount / ($nonEmptyItemsCount ?: 1)), 2) ?>
                </td>
            </tr>
            <tr data-attr="clicks_count_per_day">
                <td class="fixed-column text-grey text-left text_size_14">Клики в день, шт</td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php
                    $clicksCount = round((int)$marketingPlanItem['clicks_count'] / 30);
                    $totalClicksCount += $clicksCount;
                    ?>
                    <td class="month-block text-right">
                        <?= TextHelper::integerFormat($clicksCount) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::integerFormat(round($totalClicksCount / 30)) ?>
                </td>
            </tr>
            <tr data-attr="clicks_count">
                <td class="fixed-column bold text-left text_size_14">Клики в месяц, шт</td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalClicksCount += (int)$marketingPlanItem['clicks_count'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::integerFormat((int)$marketingPlanItem['clicks_count']) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?= TextHelper::integerFormat($totalClicksCount) ?>
                </td>
            </tr>
            <tr data-attr="registration_conversion">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Конверсия лида, %</div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['registration_conversion']) ? json_encode($budgetAmountCalculatingSettings['registration_conversion']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php $nonEmptyItemsCount = 0 ?>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php
                    $totalRegistrationConversion += (float)$marketingPlanItem['registration_conversion'];
                    if ((float)$marketingPlanItem['registration_conversion'] > 0) {
                        $nonEmptyItemsCount++;
                    }
                    ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][registration_conversion]",
                            TextHelper::invoiceMoneyFormat((float)$marketingPlanItem['registration_conversion'] * 100, 2),
                            [
                                'class' => 'custom-value form-control ' . "registration_conversion-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat(round($totalRegistrationConversion / ($nonEmptyItemsCount ?: 1)) * 100, 2) ?>
                </td>
            </tr>
            <tr data-attr="registration_count_per_day">
                <td class="fixed-column text-grey text-left text_size_14">Лидов в день, шт</td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php
                    $registrationsCount = round((int)$marketingPlanItem['registration_count'] / 30);
                    $totalRegistrationCount += $registrationsCount;
                    ?>
                    <td class="month-block text-right">
                        <?= TextHelper::integerFormat($registrationsCount) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::integerFormat(round((int)$totalRegistrationCount / 30)) ?>
                </td>
            </tr>
            <tr data-attr="registration_count">
                <td class="fixed-column bold text-left text_size_14">Лидов в месяц, шт</td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalRegistrationCount += (int)$marketingPlanItem['registration_count'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::integerFormat((int)$marketingPlanItem['registration_count']) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?= TextHelper::integerFormat((int)$totalRegistrationCount) ?>
                </td>
            </tr>
            <tr data-attr="purchases_count">
                <td class="fixed-column bold text-left text_size_14">Новые лиды, шт</td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalPurchasesCount += (int)$marketingPlanItem['purchases_count'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::integerFormat((int)$marketingPlanItem['purchases_count']) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?= TextHelper::integerFormat($totalPurchasesCount) ?>
                </td>
            </tr>
            <tr data-attr="sale_amount">
                <td class="fixed-column bold text-left text_size_14">Стоимость лида, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalSaleAmount += (int)$marketingPlanItem['sale_amount'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['sale_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?php if (isset($totalBudgetAmount)): ?>
                        <?= $totalPurchasesCount ? TextHelper::invoiceMoneyFormat(round($totalBudgetAmount / $totalPurchasesCount), 2) : '0,00' ?>
                    <?php else: ?>
                        0,00
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td class="fixed-column bold text-left text_size_14">Дополнительные затраты, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalTotalAdditionalExpensesAmount += (int)$marketingPlanItem['total_additional_expenses_amount'] ?>
                    <td class="month-block bold text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['total_additional_expenses_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block bold text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalTotalAdditionalExpensesAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="marketing_service_amount">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Услуги маркетолога, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['marketing_service_amount']) ? json_encode($budgetAmountCalculatingSettings['marketing_service_amount']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalMarketingServiceAmount += (int)$marketingPlanItem['marketing_service_amount'] ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][marketing_service_amount]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['marketing_service_amount'], 2),
                            [
                                'class' => 'custom-value form-control ' . "marketing_service_amount-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalMarketingServiceAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="salesperson_salary_amount">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">ЗП продавцов, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['salesperson_salary_amount']) ? json_encode($budgetAmountCalculatingSettings['salesperson_salary_amount']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalSalespersonSalaryAmount += (int)$marketingPlanItem['salesperson_salary_amount'] ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][salesperson_salary_amount]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['salesperson_salary_amount'], 2),
                            [
                                'class' => 'custom-value form-control ' . "salesperson_salary_amount-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalSalespersonSalaryAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="additional_expenses_amount">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Прочие затраты, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['additional_expenses_amount']) ? json_encode($budgetAmountCalculatingSettings['additional_expenses_amount']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php $totalAdditionalExpensesAmount += (int)$marketingPlanItem['additional_expenses_amount'] ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[items][{$model->getChannel()}][{$marketingPlanItemId}][additional_expenses_amount]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['additional_expenses_amount'], 2),
                            [
                                'class' => 'custom-value form-control ' . "additional_expenses_amount-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                            ]
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalAdditionalExpensesAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="additional_expenses_amount">
                <td class="fixed-column text-grey text-left text_size_14">Доп затраты на 1 продажу, <?= $currencySymbol ?></td>
                <?php $nonEmptyItemsCount = 0 ?>
                <?php foreach ($model->items as $marketingPlanItemId => $marketingPlanItem): ?>
                    <?php
                    $totalAdditionalExpensesAmountForOne += (int)$marketingPlanItem['additional_expenses_amount_for_one_sale'];
                    if ((int)$marketingPlanItem['additional_expenses_amount_for_one_sale'] > 0) {
                        $nonEmptyItemsCount++;
                    }
                    ?>
                    <td class="month-block text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['additional_expenses_amount_for_one_sale'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?php if (isset($totalTotalAdditionalExpensesAmount)): ?>
                        <?= $totalPurchasesCount ? TextHelper::invoiceMoneyFormat(round($totalTotalAdditionalExpensesAmount / $totalPurchasesCount), 2) : '0,00' ?>
                    <?php else: ?>
                        0,00
                    <?php endif; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php Pjax::end() ?>
