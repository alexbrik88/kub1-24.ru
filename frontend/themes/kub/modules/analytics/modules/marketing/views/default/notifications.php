<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\modules\marketing\models\MarketingNotification;
use common\modules\marketing\models\MarketingNotificationGroup;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $channels
 * @var array $groups
 * @var array $items
 * @var bool $isEditMode
 */

$this->title = 'Уведомления';

$floorMap = ['first-floor-1' => 1];

?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/tabs', [
    'controller' => Yii::$app->controller,
]) ?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/settings-tabs', [
    'type' => 0,
]) ?>

<div class="wrap wrap_count">
    <div class="scroll-table-other">

        <div class="row">
            <div class="column">
                <div class="mb-3">
                    <strong class="pb-2">Настройка отправки уведомлений при достижении критических событий</strong><br/>
                    Укажите пороговые значения, при нарушении которых вам придет уведомление.<br/>
                    Выберите куда вам должно прийти уведомление
                </div>
            </div>
            <div class="column ml-auto">
                <?php if (!$isEditMode): ?>
                    <a href="<?= Url::current(['edit' => 1]) ?>"
                       class="button-regular button-regular_red button-clr w-44 mb-2"
                       title="Изменить">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                        </svg>
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
            <div class="table-wrap">
                <?= Html::beginForm('', 'post', ['id' => 'table-marketing-notifications']) ?>
                <table class="flow-of-funds table table-style table-count-list table-compact mb-0">
                    <thead>
                    <tr class="quarters-flow-of-funds">
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 250px">
                            <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1">Критические события</span>
                            </button>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 200px">
                            <div class="pl-1 pr-1">Пороговое значение</div>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 50px">
                            <div class="pl-1 pr-1" style="writing-mode:vertical-lr; transform:rotate(180deg); min-height:70px;">Информер</div>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 50px">
                            <div class="pl-1 pr-1" style="writing-mode:vertical-lr; transform:rotate(180deg); min-height:70px;">E-mail</div>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 50px">
                            <div class="pl-1 pr-1" style="writing-mode:vertical-lr; transform:rotate(180deg); min-height:70px;">WhatsApp</div>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom" style="width: 50px">
                            <div class="pl-1 pr-1" style="writing-mode:vertical-lr; transform:rotate(180deg); min-height:70px;">Telegram</div>
                        </th>
                        <th class="pl-2 pr-2 pt-3 pb-3 align-bottom">
                            <div class="pl-1 pr-1">Описание</div>
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php foreach ($channels as $channel): ?>

                    <?php $floorKey = "first-floor-".$channel['id']; ?>
                    <?php $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, false); ?>

                        <tr class="main-block">
                            <td class="pl-2 pr-2 pt-3 pb-3">
                                <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                    <div class="text_size_14 weight-700 mr-2 nowrap">
                                        <button class="table-collapse-btn button-clr <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                                            <span class="table-collapse-icon">&nbsp;</span>
                                            <span class="weight-700 text_size_14 ml-1"><?= $channel['name'] ?></span>
                                        </button>
                                    </div>
                                </div>
                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap" colspan="6">
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                </div>
                            </td>
                        </tr>

                        <?php foreach ($groups as $group): ?>

                                <?php $attrBasis = 'rule['.$channel['id'].']['.$group['id'].']'; ?>
                                <?php $isZeroBalance = ($group['id'] == MarketingNotificationGroup::BALANCE_BY_ZERO); ?>

                                <tr class="item-block <?= (!$isOpenedFloor && $channel['id'] > 1) ? 'd-none':'' ?>" data-id="<?= $floorKey ?>">
                                    <td class="pl-2 pr-1 pt-3 pb-3">
                                        <span class="text-dark-alternative text_size_14 m-l-purse"><strong><?= $group['name']; ?></strong><br/><?= $group['rule'] ?></span>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap text-left">
                                        <?php if ($isEditMode): ?>
                                            <div class="pl-1 pr-1 text-dark-alternative text-left">
                                                <?= Html::input((!$isZeroBalance) ? "text" : "hidden", "{$attrBasis}[threshold]",
                                                    MarketingNotification::getValue('threshold', $channel['id'], $group['id'], $items), [
                                                    'class' => 'form-control js_input_to_money_format',
                                                    'style' => 'width:88%; display:inline-block;'
                                                ]) ?>
                                                <?php if (!$isZeroBalance): ?>
                                                    <span class="text-grey" style="display: inline-block"><?= MarketingNotificationGroup::getThresholdUnitName($group['threshold_unit']) ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <p class="help-block help-block-error text-left pl-1" style="display: none">Необходимо заполнить</p>
                                        <?php else: ?>
                                            <div class="pl-1 pr-1 text-dark-alternative text-right">
                                                <?= (!$isZeroBalance) ? MarketingNotification::getValue('threshold', $channel['id'], $group['id'], $items) : '' ?>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                        <div class="pl-1 pr-1 text-dark-alternative">
                                            <?= Html::checkbox("{$attrBasis}[notify_by_informer]", MarketingNotification::getValue('notify_by_informer', $channel['id'], $group['id'], $items), [
                                                'disabled' => !$isEditMode || $isZeroBalance
                                            ]) ?>
                                        </div>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                        <div class="pl-1 pr-1 text-dark-alternative">
                                            <?= Html::checkbox("{$attrBasis}[notify_by_email]", MarketingNotification::getValue('notify_by_email', $channel['id'], $group['id'], $items), ['disabled' => !$isEditMode]) ?>
                                        </div>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                        <div class="pl-1 pr-1 text-dark-alternative">
                                            <?= Html::checkbox("{$attrBasis}[notify_by_whatsapp]", MarketingNotification::getValue('notify_by_whatsapp', $channel['id'], $group['id'], $items), ['disabled' => !$isEditMode]) ?>
                                        </div>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                        <div class="pl-1 pr-1 text-dark-alternative">
                                            <?= Html::checkbox("{$attrBasis}[notify_by_telegram]", MarketingNotification::getValue('notify_by_telegram', $channel['id'], $group['id'], $items), ['disabled' => !$isEditMode]) ?>
                                        </div>
                                    </td>
                                    <td class="pl-2 pr-2 pt-3 pb-3">
                                        <div class="pl-1 pr-1 text-dark-alternative text-left">
                                            <div style="max-width: 300px; font-size: 14px;"><?= $group['description'] ?></div>
                                        </div>
                                    </td>

                                </tr>

                        <?php endforeach; ?>

                        <?php endforeach; ?>

                    </tbody>
                </table>
                <?= Html::endForm() ?>
            </div>
        </div>

    </div>
</div>

<?php if ($isEditMode): ?>
    <div class="wrap wrap_btns check-condition visible mb-0 mt-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <button type="submit" class="button-regular button-width button-regular_red button-clr mt-ladda-btn ladda-button" data-style="expand-right" form="table-marketing-notifications">
                    <span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>
                </button>
            </div>
            <div class="column">
                <span style="padding: 0 6px;">
                    <a class="button-clr button-width button-regular button-hover-transparent undo-contractor" href="<?= Url::current(['edit' => null]) ?>">Отменить</a>
                </span>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    $('#table-marketing-notifications').on('submit', function() {
        let isValid = true;
        $(this).find('tr').each(function() {
            if ($(this).find('[type="checkbox"]:checked').length)
                if ($(this).find('[type="text"]').length && !$(this).find('[type="text"]').val().length) {
                    $(this).find('.help-block').show();
                    isValid = false;
                }

        });
        if (!isValid)
            Ladda.stopAll();

        return isValid;
    });
    $('#table-marketing-notifications input').on('click', function() {
        $(this).closest('tr').find('.help-block').hide();
    })
</script>
