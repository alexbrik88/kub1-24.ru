<?php

use common\components\helpers\Month;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap\Nav;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\web\JsExpression;
use Carbon\Carbon;
use php_rutils\RUtils;
use common\modules\marketing\models\MarketingUserConfig;

/** @var MarketingUserConfig $marketingUserConfig */

$jsModel = $jsModel ?? 'Chart123.chart_21';
$id = $id ?? 'chart_21';

$leftOffset = 6;
$rightOffset = 0;
$channelType = intval($channelType ?? -1);
$customPeriod = $customPeriod ?? ($marketingUserConfig->dynamics_chart_period === 1 ? 'days' : 'months');
$customChartType = $customChartType ?? $marketingUserConfig->dynamics_chart_type;
$customOffset = $customOffset ?? null;
$customCampaign = $customCampaign ?? null;
$customFilters = [
    'offset' => $customOffset,
    'campaign' => $customCampaign
];

$byMonth = ($customPeriod === 'months');

if ($byMonth) {
    // by months
    $xPeriod = $xMonth = 9;
    $ctr = MarketingReportSearch::getKeyIndicatorDataMonth($company->id, $channelType, 'marketing_report.ctr', $xMonth, $customFilters); // CTR
    $clicks = MarketingReportSearch::getKeyIndicatorDataMonth($company->id, $channelType, 'marketing_report.clicks', $xMonth, $customFilters); // Клики
    $cpc = MarketingReportSearch::getKeyIndicatorDataMonth($company->id, $channelType, 'marketing_report.avg_cpc', $xMonth, $customFilters); // CPC
    $cost = MarketingReportSearch::getKeyIndicatorDataMonth($company->id, $channelType, 'marketing_report.cost', $xMonth, $customFilters); // Расходы
    $leads = MarketingReportSearch::getKeyIndicatorDataMonth($company->id, $channelType, 'marketing_report_google_analytics_goal.total', $xMonth, $customFilters); // Лиды
    $leadConversion = MarketingReportSearch::calcKeyIndicatorData($leads, $clicks, 'divide', true); // Конверсия в лид
    $leadCost = MarketingReportSearch::calcKeyIndicatorData($cost, $leads, 'divide'); // Стоимость лида

} else {
    // by days
    $xPeriod = $xDays = 12;
    $clicks = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.clicks', $xDays, $customFilters); // Клики
    $cost = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.cost', $xDays, $customFilters); // Расходы
    $ctr = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.ctr', $xDays, $customFilters); // CTR
    $cpc = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.avg_cpc', $xDays, $customFilters); // CPC
    $leads = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report_google_analytics_goal.total', $xDays, $customFilters); // Лиды
    $leadConversion = MarketingReportSearch::calcKeyIndicatorData($leads, $clicks, 'divide', true); // Конверсия в лид
    $leadCost = MarketingReportSearch::calcKeyIndicatorData($cost, $leads, 'divide'); // Стоимость лида
}

$_chartStructure = [
    'ctr' => ['customChartType' => 1, 'type' => 'line', 'name' => 'CTR', 'color' => '#ff5301', 'y' => []],
    'clicks' => ['customChartType' => 2, 'type' => 'line', 'name' => 'Клики', 'color' => '#36c3b0', 'y' => []],
    'cpc' => ['customChartType' => 3, 'type' => 'line', 'name' => 'CPC', 'color' => '#c526cd', 'y' => []],
    'conversion' => ['customChartType' => 4, 'type' => 'line', 'name' => 'Конверсия', 'color' => '#cf134b', 'y' => []],
    'leads' => ['customChartType' => 5, 'type' => 'line', 'name' => 'Лиды', 'color' => '#2e9fbf', 'y' => []],
    'cpl' => ['customChartType' => 6, 'type' => 'line', 'name' => 'CPL', 'color' => '#26cd58', 'y' => []],
    'cost' => ['customChartType' => 7, 'type' => 'column', 'name' => 'Расходы', 'color' => '#f3b72e', 'y' => []],
];

$graphData = [
    'x' => [],
    'xMonth' => [],
    'indicator' => $_chartStructure,
    'indicatorMonth' => $_chartStructure
];

for ($i = 0; $i < $xPeriod; $i++) {
    $graphData['x'][] = $clicks[$i]['x'];
    $graphData['indicator']['ctr']['y'][] = $ctr[$i]['y'] ?? null;
    $graphData['indicator']['clicks']['y'][] = $clicks[$i]['y'] ?? null;
    $graphData['indicator']['cpc']['y'][] = $cpc[$i]['y'] ?? null;
    $graphData['indicator']['conversion']['y'][] = $leadConversion[$i]['y'] ?? null;
    $graphData['indicator']['leads']['y'][] = $leads[$i]['y'] ?? null;
    $graphData['indicator']['cpl']['y'][] = $leadCost[$i]['y'] ?? null;
    $graphData['indicator']['cost']['y'][] = $cost[$i]['y'] ?? null;
}

/**
 * @var array $graphData
 * @var bool $byMonth
 */

$daysPeriods = [];
$labelsX = [];
$freeDays = [];

foreach ($graphData['x'] as $date) {
    $carbonDate = Carbon::createFromFormat('d.m.Y', $date);
    if ($byMonth) {
        $month = $carbonDate->format('n');
        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $labelsX[] = Month::$monthFullRU[$month] . ' ' . $carbonDate->format('Y');
    } else {
        $day = $carbonDate->format('d');
        $daysPeriods[] = $day;
        $freeDays[] = (in_array(date('w', strtotime($date)), [0, 6]));
        $labelsX[] = $day . ' ' . RUtils::dt()->ruStrFTime([
            'format' => 'F',
            'monthInflected' => true,
            'date' => $date,
        ]);
    }

    $currDayPos = $xPeriod - $customOffset - 1;
}

$series = [];
foreach ($graphData['indicator'] as $key => $data) {
    $series[] = [
        'type' => $data['type'],
        'name' => $data['name'],
        'data' => $data['y'],
        'color' => $data['color'],
        'marker' => [
            'symbol' => 'circle',
            'enabled' => false
        ],
        'showInLegend' => false,
        'visible' => ($data['customChartType'] == $customChartType)
    ];
}

?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $currDayPos ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($labelsX) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($freeDays) ?>;
    <?=($jsModel)?>.data = <?= json_encode([
        'series' => [
            ['data' => $graphData['indicator']['ctr']['y']],
            ['data' => $graphData['indicator']['clicks']['y']],
            ['data' => $graphData['indicator']['cpc']['y']],
            ['data' => $graphData['indicator']['conversion']['y']],
            ['data' => $graphData['indicator']['leads']['y']],
            ['data' => $graphData['indicator']['cpl']['y']],
            ['data' => $graphData['indicator']['cost']['y']],
        ],
        'xAxis' => [
            'categories' => $daysPeriods
        ]
    ]) ?>;
</script>

<?php if (isset($isAjax)) { return; } ?>

<?php
$htmlTooltip = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th class="text-left" colspan="3">{title}</th>
        </tr>
        <tr>
            <td>{itemName}</td>
            <td></td>
            <td><b>{itemData}</b></td>
        </tr>
    </table>
HTML;

$htmlTooltip = str_replace(["\r", "\n", "'"], "", $htmlTooltip);
?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header" style="text-transform: unset">
                ДИНАМИКА ПОКАЗАТЕЛЕЙ
                <?= $this->render('config/chart_21_config', [
                    'config' => $marketingUserConfig
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <?= Nav::widget([
                    'id' => $id.'_nav',
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100'],
                    'items' => [
                        [
                            'label' => 'CTR',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_CTR,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '0']
                        ],
                        [
                            'label' => 'Клики',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_CLICKS,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '1']
                        ],
                        [
                            'label' => 'CPC',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_CPC,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '2']
                        ],
                        [
                            'label' => 'Конверсия',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_CONVERSION,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '3']
                        ],
                        [
                            'label' => 'Лиды',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_LEADS,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '4']
                        ],
                        [
                            'label' => 'CPL',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_CPL,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '5']
                        ],
                        [
                            'label' => 'Расходы',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == $marketingUserConfig::CHART_TYPE_COST,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link', 'data-serie' => '6']
                        ],
                    ],
                ]) ?>
            </div>
            <div class="column ml-auto">
                <?= Nav::widget([
                    'id' => $id . '_period',
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_none w-100'],
                    'items' => [
                        [
                            'label' => 'День',
                            'url' => 'javascript:void(0)',
                            'active' => !$byMonth,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding-left: 0; padding-right: 9px;',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link' . (!$byMonth ? ' active' : ''),
                                'data-period' => 'days'
                            ]
                        ],
                        [
                            'label' => 'Месяц',
                            'url' => 'javascript:void(0)',
                            'active' => $byMonth,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding: 0',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link' . ($byMonth ? ' active' : ''),
                                'data-period' => 'months'
                            ]
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <?= $this->render('_arrows', ['chartId' => $id, 'offsetStep' => 1, 'class' => 'marketing-chart-offset-2', 'zIndex' => 2]) ?>
        <?= Highcharts::widget([
            'id' => $id,
            'scripts' => [
                'modules/exporting',
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'title' => false,
                'credits' => [
                    'enabled' => false
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'line',
                    'marginLeft' => '55',
                    'marginTop' => '25',
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'reflow' => true
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0'
                    ],
                    'itemDistance' => 10
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                        
                            let index = this.series.data.indexOf( this.point );
                            let data = Highcharts.numberFormat(this.y, 1, ',', ' ');

                            return '{$htmlTooltip}'
                                .replace(\"{title}\", window.Chart123.chart_21.labelsX[index])
                                .replace(\"{itemName}\", this.series.name)
                                .replace(\"{itemData}\", data);
                        }
                    ")
                ],
                'yAxis' => [
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
                'xAxis' => [
                    'min' => 0,
                    'categories' => $daysPeriods,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.Chart123.chart_21.currDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                            (window.Chart123.chart_21.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                        'useHTML' => true,
                    ],
                ],
                'series' => $series,
                'plotOptions' => [
                    'scatter' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ]
                    ],
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        'groupPadding' => 0.05,
                        'pointPadding' => 0.1,
                        'borderRadius' => 3,
                    ]
                ],
            ],
        ]) ?>
    </div>
</div>