<?php

use common\modules\marketing\models\MarketingUserConfig;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;
use kartik\select2\Select2;

/** @var MarketingUserConfig $config */

$showAsButton = $showAsButton ?? false;
$usePjax = $usePjax ?? true;
?>

<div class="dropdown dropdown-settings d-inline-block" title="Настройка графиков" style="<?=($showAsButton) ? '' : 'z-index: 1001; position: absolute; top:5px; left:120px' ?>">
    <?= Html::button(Icon::get('cog'), [
        'class' => ($showAsButton) ? 'button-list button-hover-transparent button-clr collapsed' : 'marketing-dropdown-btn-icon',
        'data-toggle' => 'dropdown',
    ]) ?>
    <ul id="marketing_statistics_chart_config" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px; width: 185px">
        <?php for ($blockNum = 1; $blockNum <= 3; $blockNum++): ?>
            <?php $param = "statistics_chart_type_{$blockNum}"; ?>
            <li class="_chart_user_config_option d-flex">
                    <div class="col-6 pl-0 pr-1 mb-0">
                        График <?= $blockNum ?>
                    </div>
                    <div class="col-6 pl-1 pr-0 mb-0">
                        <?= Select2::widget([
                            'name' => "MarketingUserConfig[$param]",
                            'data' => MarketingUserConfig::$chartTypeName,
                            'value' => $config->{$param} ?? null,
                            'hideSearch' => true,
                            'options' => [
                                'class' => 'chart_user_config_select' . ($usePjax ? '' : ' no_pjax')
                            ],
                            'pluginOptions' => [
                                'width' => '125px',
                                'containerCssClass' => 'marketing_chart_config_select2'
                            ]
                        ]) ?>
                    </div>
            </li>
        <?php endfor ?>
    </ul>
</div>