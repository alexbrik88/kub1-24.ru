<?php

/* @var $this yii\web\View
 * @var $marketingPlan MarketingPlan
 * @var $user \common\models\employee\Employee
 * @var $channel int
 */

use common\components\helpers\ArrayHelper;
use common\components\helpers\Url;
use common\models\marketing\MarketingPlan;
use common\models\marketing\MarketingPlanItem;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\modules\marketing\models\UpdatePlanningMarketingForm;
use kartik\widgets\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Tabs;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;
use frontend\themes\kub\helpers\Icon;

$this->title = "Редактирование: {$marketingPlan->name}";
$channels = MarketingPlanItem::find()->select('channel')
    ->andWhere(['marketing_plan_id' => $marketingPlan->id])
    ->groupBy('channel')
    ->column();

$updateTableView = null;
switch ($marketingPlan->template) {
    case MarketingPlan::TEMPLATE_LEAD_GENERATION:
        $updateTableView = 'partial/lead_generation_update_table';
        break;
    case MarketingPlan::TEMPLATE_ONLINE_SERVICE:
        $updateTableView = 'partial/online_service_update_table';
        break;
    case MarketingPlan::TEMPLATE_ONLINE_STORE:
        $updateTableView = 'partial/online_store_update_table';
        break;
}

$startAmountTooltip = '<span>Начальное значение - значение в ячейке месяца, который выбран в поле "С месяца".
В следующих полях вы задаете формулу, как в ячейках от месяца указанного в поле слева должны меняться значения.</span>';
$actonTooltip = '<span>"= Нач. значение" - значение у всех ячеек правее будут равно цифре в поле "Нач. значение"</span>';
$amountTooltip = '<span>Укажите число, которое нужно прибавить, или вычесть или на которое умножить. Если вам нужно
увеличить последующие цифры например на 10%, то вам нужно выбрать действие умножение "*" и в поле "число" поставить "1,1"</span>';
$limitAmountTooltip = '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>';

$updateModel = new UpdatePlanningMarketingForm($marketingPlan, $channel);
$monthData = [];
if ($channel === MarketingPlan::REPEATED_PURCHASES) {
    $updateModel->getRepeatedPurchasesItems($user);
}
foreach ($updateModel->items as $marketingPlanItem) {
    $monthData["{$marketingPlanItem['year']}{$marketingPlanItem['month']}"] = ArrayHelper::getValue(AbstractFinance::$month, str_pad($marketingPlanItem['month'], 2, "0", STR_PAD_LEFT)) . ' ' . $marketingPlanItem['year'];
}
?>
<style>
    .marketing-planning-table tbody td {
        height: 33px;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'marketing-planning-form',
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= Html::hiddenInput('activeChannel', $channel, ['id' => 'active-channel-input']) ?>
<?= Html::hiddenInput(null, $marketingPlan->id, ['id' => 'marketing-plan-id-input']) ?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-3">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-12 column pr-0">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between">
                        <h4 class="column mb-2">
                            <?= $this->title ?>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'nav nav-tabs w-100 mb-3 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane',
                    'style' => 'width:100%'
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Яндекс.Директ',
                        'content' => $this->render($updateTableView, [
                            'model' => new UpdatePlanningMarketingForm($marketingPlan, MarketingPlan::CHANNEL_YANDEX_DIRECT),
                            'user' => $user,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($channel === MarketingPlan::CHANNEL_YANDEX_DIRECT ? ' active' : null),
                            'data-tab' => MarketingPlan::CHANNEL_YANDEX_DIRECT,
                        ],
                        'active' => $channel === MarketingPlan::CHANNEL_YANDEX_DIRECT,
                        'visible' => in_array(MarketingPlan::CHANNEL_YANDEX_DIRECT, $channels),
                    ],
                    [
                        'label' => 'Google.Ads',
                        'content' => $this->render($updateTableView, [
                            'model' => new UpdatePlanningMarketingForm($marketingPlan, MarketingPlan::CHANNEL_GOOGLE_ADS),
                            'user' => $user,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($channel === MarketingPlan::CHANNEL_GOOGLE_ADS ? ' active' : null),
                            'data-tab' => MarketingPlan::CHANNEL_GOOGLE_ADS,
                        ],
                        'active' => $channel === MarketingPlan::CHANNEL_GOOGLE_ADS,
                        'visible' => in_array(MarketingPlan::CHANNEL_GOOGLE_ADS, $channels),
                    ],
                    [
                        'label' => 'Вконтакте',
                        'content' => $this->render($updateTableView, [
                            'model' => new UpdatePlanningMarketingForm($marketingPlan, MarketingPlan::CHANNEL_VK),
                            'user' => $user,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($channel === MarketingPlan::CHANNEL_VK ? ' active' : null),
                            'data-tab' => MarketingPlan::CHANNEL_VK,
                        ],
                        'active' => $channel === MarketingPlan::CHANNEL_VK,
                        'visible' => in_array(MarketingPlan::CHANNEL_VK, $channels),
                    ],
                    [
                        'label' => 'Facebook',
                        'content' => $this->render($updateTableView, [
                            'model' => new UpdatePlanningMarketingForm($marketingPlan, MarketingPlan::CHANNEL_FACEBOOK),
                            'user' => $user,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($channel === MarketingPlan::CHANNEL_FACEBOOK ? ' active' : null),
                            'data-tab' => MarketingPlan::CHANNEL_FACEBOOK,
                        ],
                        'active' => $channel === MarketingPlan::CHANNEL_FACEBOOK,
                        'visible' => in_array(MarketingPlan::CHANNEL_FACEBOOK, $channels),
                    ],
                    [
                        'label' => 'Повторные продажи',
                        'content' => $this->render('partial/repeated_purchases_update_table', [
                            'model' => new UpdatePlanningMarketingForm($marketingPlan, MarketingPlan::REPEATED_PURCHASES),
                            'user' => $user,
                            'reloadItems' => true,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($channel === MarketingPlan::REPEATED_PURCHASES ? ' active' : null),
                            'data-tab' => MarketingPlan::REPEATED_PURCHASES,
                        ],
                        'active' => $channel === MarketingPlan::REPEATED_PURCHASES,
                        'visible' => in_array($marketingPlan->template, MarketingPlan::$repeatedPurchasesTemplates),
                    ],
                ],
            ]); ?>
            <!-- update item-->
            <div id="update-planning-auto-fill-item-panel" class="wrap wrap_padding_none" style="display: none">
                <div class="ml-3 mr-3 p-2">
                    <?= Html::hiddenInput('autofill[attr]', null, ['class' => 'attr']); ?>
                    <div style="display: flex">
                        <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                            <span class="label w-700" style="text-align: center;">
                                С месяца
                            </span>
                            <?= Select2::widget([
                                'id' => 'auto-fill-month-start-planning',
                                'name' => 'autofill[month_start]',
                                'data' => $monthData,
                                'hideSearch' => true,
                                'options' => [
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                    'data-month' => $monthData,
                                    'autocomplete' => 'off',
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                ],
                            ]); ?>
                        </div>
                        <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                            <span class="label w-700" style="text-align: center;">
                                Нач. значение
                                <?= Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => $startAmountTooltip,
                                ]) ?>
                            </span>
                            <?= Html::textInput("autofill[start_amount]", null, [
                                'class' => 'form-control amount-input',
                                'id' => 'auto-fill-start_amount-planning',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]); ?>
                        </div>
                        <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                            <span class="label w-700" style="text-align: center;">
                                Действие
                                <?= Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => $actonTooltip,
                                ]) ?>
                            </span>
                            <?= Select2::widget([
                                'id' => 'auto-fill-action-planning',
                                'name' => 'autofill[action]',
                                'data' => UpdatePlanningMarketingForm::$autoPlanningActionsMap,
                                'value' => min(array_keys(UpdatePlanningMarketingForm::$autoPlanningActionsMap)),
                                'hideSearch' => true,
                                'options' => [
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                ],
                            ]); ?>
                        </div>
                        <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                            <span class="label w-700" style="text-align: center;">
                                Число
                                <?= Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => $amountTooltip,
                                ]) ?>
                            </span>
                            <?= Html::textInput("autofill[amount]", null, [
                                'class' => 'form-control amount-input',
                                'id' => 'auto-fill-amount-planning',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                                'disabled' => true,
                            ]); ?>
                        </div>
                        <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                            <span class="label w-700" style="text-align: center;">
                                <span class="dynamic-label">Макс. значение</span>
                                <?= Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => $limitAmountTooltip,
                                ]) ?>
                            </span>
                            <?= Html::textInput("autofill[limit_amount]", null, [
                                'class' => 'form-control amount-input',
                                'id' => 'auto-fill-limit_amount-planning',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                                'disabled' => true,
                            ]); ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-6 p-1 text-right">
                            <?= Html::a('Сохранить', 'javascript:;', [
                                'class' => 'complete-auto-planning-item button-regular button-width button-regular_red button-clr ladda-button mr-1',
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-6 p-1 ml-auto text-left">
                            <?= Html::a('Отменить', 'javascript:;', [
                                'class' => 'cancel-edit-costs-js button-regular button-width button-hover-transparent button-clr',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-start">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'form' => 'marketing-planning-form',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Очистить', 'javascript:;', [
                'class' => 'button-clr button-width button-regular button-hover-transparent clear-planning-table',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', Url::to(['view', 'id' => $marketingPlan->id]), [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
            ]); ?>
        </div>
    </div>
</div>

<?php $this->registerJs('PlanningAutoFillModelEditOnFly.init();', View::POS_READY); ?>

<?php $this->registerJs("
    $('.custom-value')
        .on('paste change', function (e) {
            const decimals = $(this).hasClass('int') ? 0 : 2;
            e.preventDefault();
            const value = e.type === 'paste'
                ? (e.originalEvent || e).clipboardData.getData('text/plain')
                : this.value;
            this.value = number_format(sanitize(value), decimals, ',', ' ');
        })
        .on('keydown', function(e){
            if(e.keyCode === 13) $(this).blur();
        })
        .on('blur', function(e){
            const decimals = $(this).hasClass('int') ? 0 : 2;
            this.value = number_format(sanitize(this.value), decimals, ',', ' ');
        });

    $('#marketing-planning-form').on('keydown', function(e) {
        if(e.keyCode === 13) return false;
    });

    $(document).on('click', '.clear-planning-table', function(e) {
        let tab = $('.nav-tabs-row .nav-link.active').attr('data-tab');

        $('.tab-pane.active .custom-value').val(0);
        $.pjax.reload('#channel-planning-table-pjax-' + tab, {
            'url': 'reload-channel-planning-table?id={$marketingPlan->id}&channel=' + tab,
            'type': 'post',
            'data': $('.tab-pane .custom-value').serialize(),
            'push': false,
            'replace': false,
            'timeout': 10000,
            'scrollTo': false,
            'container': '#channel-planning-table-pjax-' + tab
        });
    });

    function sanitize(val) {
        return String(val).replace(/\s/g, '').replace(',', '.').replace('-', '');
    }

    $(document).on('click', '.nav-tabs .nav-link', function(e) {
        $('#active-channel-input').val(+$(this).attr('data-tab'));
        let content = $($(this).attr('href'));
        let tableWidth = content.find('.scrollable-table.double-scrollbar-top').width();
        content.find('.scrollable-table-container .scrollable-table-bar > div').css('width', tableWidth);
        reloadTable(content.find('.planning-table'));
        
    });

    $(document).on('change', '.global-field', function(e) {
        if (e.originalEvent !== undefined) {
            let className = $(this).attr('data-class');
            $('.' + className).val($(this).val()).trigger('change');
        }
    });

    $(document).on('change', '.tab-pane.active .planning-table .custom-value', function() {
        reloadTable($(this).closest('.planning-table'));
    });

    function reloadTable(planningTable) {
        let channel = planningTable.attr('data-channel');
        let url = 'reload-channel-planning-table?id={$marketingPlan->id}&channel=' + channel;
        $('.scrollable-table-clone.table .custom-value').attr('disabled', true);
        $.pjax.reload('#channel-planning-table-pjax-' + channel, {
            'url': url,
            'type': 'post',
            'data': $('.tab-pane .custom-value').serialize(),
            'push': false,
            'replace': false,
            'timeout': 10000,
            'scrollTo': false,
            'container': '#channel-planning-table-pjax-' + channel,
        });
    }
");

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'maxWidth' => 500,
        'contentAsHTML' => true,
    ],
]);
