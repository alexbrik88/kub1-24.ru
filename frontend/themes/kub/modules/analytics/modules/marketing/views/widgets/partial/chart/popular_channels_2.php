<?php

use yii\web\JsExpression;
use yii\bootstrap\Nav;
use miloschuman\highcharts\Highcharts;
use php_rutils\RUtils;

/* @var array $tableData
 */

$chartLabelsX = [];
foreach ($tableData['x'] as $date) {
    $dateArr = explode('.', $date);
    $chartLabelsX[] = $dateArr[0] . ' ' . RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $date,
    ]);
}

?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header">
                Ключевые показатели
            </div>
        </div>
        <div class="nav-tabs-row">

        </div>
        <table class="table table-style table-count-list form-group popular_table marketing-key-indicators-table">
            <thead>
            <tr>
                <th width="35%">Показатель</th>
                <th class="text-right" width="20%">Сегодня</th>
                <th class="text-right" width="20%">Вчера</th>
                <th class="text-right" width="20%" style="white-space: nowrap;">Среднее <span style="text-transform: none">за 7 дн.</span></th>
                <th width="5%"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($tableData['indicator'] as $data): ?>
                <tr>
                    <td>
                        <span class="pt-name"><?= $data['name'] ?></span><br/>
                        <span class="pt-subname"><?= $data['subname'] ?></span>
                    </td>
                    <td>
                        <span class="pt-name"><?= $data['today']['value'] . $data['unit_name'] ?></span><br/>
                        <span class="pt-percent <?= $data['today']['percent'] < 0 ? 'red' : 'green' ?>">
                            <?php if ($data['today']['percent'] != 0): ?>
                                <?= $data['today']['percent'] > 0 ? "+{$data['today']['percent']}" : $data['today']['percent']?> %
                            <?php else: ?>
                                &nbsp;
                            <?php endif; ?>
                        </span>
                    </td>
                    <td>
                        <span class="pt-name one-line"><?= $data['yesterday']['value'] . $data['unit_name'] ?></span><br/>
                    </td>
                    <td>
                        <span class="pt-name one-line"><?= $data['average'] . $data['unit_name'] ?></span>
                    </td>
                    <td>
                        <?php if ($data['yesterday']['value'] != $data['average']): ?>
                            <img class="abc-arrow" style="width:14px;" src="/img/abc/arrow_<?= ($data['yesterday']['value'] > $data['average'] ? 'green':'red')?>.svg"/>
                            <br/><br/>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(window).on('load', function () {
        $('.key-indicators-nav').tabdrop();
    });

    var chartLabelsX2 = <?= json_encode($chartLabelsX) ?>;
</script>
