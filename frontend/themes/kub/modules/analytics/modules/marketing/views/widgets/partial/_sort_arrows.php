<?php

use yii\helpers\Url;

/**
 * @var string $attr
 * @var string $title
 * @var string $defaultSorting
 */

$currentSort = Yii::$app->request->get('sort', $defaultSorting);

if ($currentSort == $attr) {
    $sort = '-' . $attr;
    $active = 'sort_asc';
} elseif ($currentSort == '-'.$attr) {
    $sort = $attr;
    $active = 'sort_desc';
} else {
    $sort = '-'.$attr;
    $active = '';
}

?>
<div class="th-title <?= $active ?>">
    <a class="th-title-name asc" href="<?= Url::current(['sort' => $sort]) ?>" data-sort="<?= $sort ?>">
        <?= $title ?>
    </a>
    <span class="th-title-btns">
        <a class="th-title-btn icon_asc button-clr" href="<?= Url::current(['sort' => $sort]) ?>">
            <svg class="th-title-icon th-title-icon_reverse svg-icon" viewBox="0 0 8 4"><path d="M4 4L0 0h8z" fill-rule="evenodd"></path></svg>
        </a>
        <a class="th-title-btn icon_desc button-clr" href="<?= Url::current(['sort' => $sort]) ?>">
            <svg class="th-title-icon svg-icon" viewBox="0 0 8 4"><path d="M4 4L0 0h8z" fill-rule="evenodd"></path></svg>
        </a>
    </span>
</div>