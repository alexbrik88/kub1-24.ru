<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\components\TextHelper;
use common\components\helpers\Html;
use common\models\employee\Employee;
use common\modules\import\models\ImportJobData;
use common\modules\marketing\models\MarketingUserConfig;
use frontend\modules\analytics\modules\marketing\assets\JobStatusAsset;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\RangeButtonWidget;
use php_rutils\RUtils;
use Yii;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ImportJobData|null $lastJobData
 * @var string $logoSrc
 * @var string $integrationButton
 * @var string $balanceId
 * @var float $balanceAmount
 * @var int $balanceLeftDays
 * @var bool $hasAccessToken
 * @var int $channelType
 */

/** @var Employee $employee */
$employee = Yii::$app->user->identity;
$company = $employee->company;
$userConfig = $employee->config;
$showHelpPanel = $userConfig->marketing_help ?? false;
$showChartPanel = $userConfig->marketing_chart ?? false;
$marketingUserConfig = MarketingUserConfig::findOne(['employee_id' => Yii::$app->user->identity->id]);
if (!$marketingUserConfig) {
    $marketingUserConfig = new MarketingUserConfig(['employee_id' => Yii::$app->user->identity->id]);
    $marketingUserConfig->setDefaults();
}

// JobStatusAsset::register($this);
?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/tabs', [
    'controller' => Yii::$app->controller,
]) ?>

<div class="wrap pt-1 pb-1 pl-4 pr-3" style="margin-bottom:12px">
    <div class="row align-items-center">
        <div class="col-3 pt-1">
            <h3 class="page-title yd-title">
                <img src="<?= $logoSrc ?>" style="max-width: 143px; max-height: 45px;">
            </h3>
        </div>
        <div class="column">
            <?php if ($lastJobData): ?>
                <?php if ($lastJobData->getJobStatus() === ImportJobData::STATUS_JOB_PROCESSING): ?>
                    <span id="jobStatus" data-url="<?= Url::to(['job-status'])?>" data-color="red">
                        <span style="color: red;"><?= $lastJobData->getJobStatus() ?>
                    </span>
                <?php else: ?>
                    <?= $lastJobData->getJobStatus() ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-3 pr-2 pl-1 ml-auto">
            <?= $integrationButton ?>
        </div>
    </div>
</div>

<div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom:12px">
    <div class="pt-2 pb-2">
        <div class="row align-items-center pb-1">
            <div class="column">
                <div>
                    <h4 class="mb-1">
                        Баланс на счете <span><?= number_format($balanceAmount, 2, ',', ' ') ?> ₽</span> <span style="font-weight: 300">без НДС</span>
                    </h4>
                    <?php if ($balanceLeftDays < 9E9): ?>
                    <table style="width:100%; margin-top:-5px; margin-bottom:5px;">
                        <tr>
                            <td style="font-size: 14px">
                                Прогноз окончания баланса:
                            </td>
                            <td style="font-size: 14px; text-align: right">
                                через <?= RUtils::numeral()->getPlural($balanceLeftDays, ['день', 'дня', 'дней']); ?>
                            </td>
                        </tr>
                    </table>
                    <?php endif; ?>
                </div>
            </div>
            <div class="column pr-0 pl-2 mr-auto">
                <?php if ($hasAccessToken): ?>

                <?php else: ?>
                    <?= Html::a('<span>Пополнить баланс</span>', 'javascript:;', [
                        'class' => 'button-regular button-hover-dark button-clr button-width w-100 balance-yandex-direct',
                        'data' => [
                            'url' => Url::to(['/integration/yandex-direct/balance']),
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="column pr-2 ml-auto">
                <?= Html::button(Icon::get('diagram'), [
                    'class' => 'button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : null),
                    'data-toggle' => 'collapse',
                    'href' => '#chartCollapse',
                ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'), [
                    'class' => 'button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : null),
                    'data-toggle' => 'collapse',
                    'href' => '#helpCollapse',
                ]) ?>
            </div>
            <div class="col-6 col-xl-3 pl-1 pr-2 d-flex flex-column justify-content-top">
                <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row']) ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse collapse <?= $showChartPanel ? 'show' : null ?>" id="chartCollapse" data-attribute="marketing_chart">
        <?= $this->render('_header_charts', [
            'company' => $company,
            'channelType' => $channelType,
            'marketingUserConfig' => $marketingUserConfig
        ]) ?>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 collapse <?= $showHelpPanel ? 'show' : null ?>" id="helpCollapse" data-attribute="marketing_help" style="margin-bottom: 12px">
    <div class="pt-4 pb-3">
        Описание
    </div>
</div>
