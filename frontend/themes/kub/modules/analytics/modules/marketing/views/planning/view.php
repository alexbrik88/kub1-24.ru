<?php

/* @var $this yii\web\View
 * @var $model \common\models\marketing\MarketingPlan
 * @var $updateForm \frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm
 * @var $user \common\models\employee\Employee
 * @var $activeTab int
 * @var $data array|null
 */

use common\components\helpers\Html;
use common\components\helpers\Url;
use common\models\marketing\MarketingPlan;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap\Nav;

$this->title = $model->name;
$channels = $updateForm->channels;

echo Html::a('Назад к списку', ['index'], ['class' => 'link mb-2']);

echo $this->render('partial/view_header', [
    'model' => $model,
    'updateForm' => $updateForm,
]);
?>
<style>
    .marketing-planning-table ::-webkit-scrollbar {
        height: 11px;
    }

    .marketing-planning-table ::-webkit-scrollbar-track {
        border-radius: 0;
        box-shadow: none;
        border: 0;
        background-color: #fff;
    }

    .marketing-planning-table ::-webkit-scrollbar-thumb {
        border-radius: 5px;
        box-shadow: none;
        border: 0;
        background-color: #bbc1c7;
    }

    .marketing-planning-table ::-webkit-scrollbar-thumb:hover {
        background-color: #bbc1c7;
    }

    .marketing-planning-table td {
        white-space: nowrap;
    }

    .table-compact th {
        height: 30px;
        vertical-align: middle!important;
        padding: 0 10px !important;
    }
</style>

<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'report_marketing_planning_round_numbers', 'refresh-page' => true],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_marketing_planning']) ?>
    </div>
    <div class="col-6">
        <div class="row align-items-center justify-content-end">
            <div class="column" style="padding-right: 24px;">
                <?= Html::a(
                    SpriteIconWidget::widget(['icon' => 'pencil']),
                    Url::to(['update-channel-planning', 'id' => $model->id, 'activeChannel' => $activeTab !== null ? $activeTab : min($channels)]),
                    [
                        'title' => 'Редактировать',
                        'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                    ]
                ) ?>
            </div>
        </div>
    </div>
</div>

<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2 pb-1">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Итого',
                    'url' => ['view', 'id' => $model->id,],
                    'active' => $activeTab === null,
                    'visible' => count($channels) > 1,
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Яндекс.Директ',
                    'url' => ['view', 'id' => $model->id, 'activeTab' => MarketingPlan::CHANNEL_YANDEX_DIRECT],
                    'active' => $activeTab !== null && (int)$activeTab === MarketingPlan::CHANNEL_YANDEX_DIRECT,
                    'visible' => in_array(MarketingPlan::CHANNEL_YANDEX_DIRECT, $channels),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Google.Ads',
                    'url' => ['view', 'id' => $model->id, 'activeTab' => MarketingPlan::CHANNEL_GOOGLE_ADS],
                    'active' => (int)$activeTab === MarketingPlan::CHANNEL_GOOGLE_ADS,
                    'visible' => in_array(MarketingPlan::CHANNEL_GOOGLE_ADS, $channels),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Вконтакте',
                    'url' => ['view', 'id' => $model->id, 'activeTab' => MarketingPlan::CHANNEL_VK],
                    'active' => (int)$activeTab === MarketingPlan::CHANNEL_VK,
                    'visible' => in_array(MarketingPlan::CHANNEL_VK, $channels),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Facebook',
                    'url' => ['view', 'id' => $model->id, 'activeTab' => MarketingPlan::CHANNEL_FACEBOOK],
                    'active' => (int)$activeTab === MarketingPlan::CHANNEL_FACEBOOK,
                    'visible' => in_array(MarketingPlan::CHANNEL_FACEBOOK, $channels),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Повторные продажи',
                    'url' => ['view', 'id' => $model->id, 'activeTab' => MarketingPlan::REPEATED_PURCHASES],
                    'active' => (int)$activeTab === MarketingPlan::REPEATED_PURCHASES,
                    'visible' => in_array($model->template, MarketingPlan::$repeatedPurchasesTemplates),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]) ?>
    </div>
</div>

<div class="wrap wrap_padding_none marketing-planning-table">
    <div class="table-wrap">
        <?php if ($activeTab !== null): ?>
            <?php if ($activeTab === MarketingPlan::REPEATED_PURCHASES): ?>
                <?= $this->render('partial/repeated_purchases_view_table', [
                    'model' => $model,
                    'user' => $user,
                    'marketingPlanItemsData' => $data,
                ]) ?>
            <?php else: ?>
                <?php switch ($model->template) {
                    case MarketingPlan::TEMPLATE_LEAD_GENERATION:
                        echo $this->render('partial/lead_generation_view_table', [
                            'model' => $model,
                            'user' => $user,
                            'activeTab' => $activeTab,
                        ]);
                        break;
                    case MarketingPlan::TEMPLATE_ONLINE_SERVICE:
                        echo $this->render('partial/online_service_view_table', [
                            'model' => $model,
                            'user' => $user,
                            'activeTab' => $activeTab,
                        ]);
                        break;
                    case MarketingPlan::TEMPLATE_ONLINE_STORE:
                        echo $this->render('partial/online_store_view_table', [
                            'model' => $model,
                            'user' => $user,
                            'activeTab' => $activeTab,
                        ]);
                        break;
                } ?>
            <?php endif; ?>
        <?php else: ?>
            <?php switch ($model->template) {
                case MarketingPlan::TEMPLATE_LEAD_GENERATION:
                    echo $this->render('partial/lead_generation_view_total_table', [
                        'model' => $model,
                        'user' => $user,
                        'marketingPlanItemsData' => $data,
                    ]);
                    break;
                case MarketingPlan::TEMPLATE_ONLINE_SERVICE:
                    echo $this->render('partial/online_service_view_total_table', [
                        'model' => $model,
                        'user' => $user,
                        'marketingPlanItemsData' => $data,
                    ]);
                    break;
                case MarketingPlan::TEMPLATE_ONLINE_STORE:
                    echo $this->render('partial/online_store_view_total_table', [
                        'model' => $model,
                        'user' => $user,
                        'marketingPlanItemsData' => $data,
                    ]);
                    break;
            } ?>
        <?php endif; ?>
    </div>
</div>

<?php $this->registerJs('
    $(document).on("click", ".double-scrollbar-top .table-collapse-btn", function(e) {
        let tableWidth = $(this).closest(".scrollable-table.double-scrollbar-top").width();
        $(this).closest(".scrollable-table-container").find(".scrollable-table-bar > div").css("width", tableWidth);
    });
    
    $(document).on("click", ".scrollable-table-content tbody .table-collapse-btn", function(e) {
        $(".scrollable-table-clone tbody .table-collapse-btn[data-target=\"" + $(this).attr("data-target") + "\"]").click();
    });
'); ?>
