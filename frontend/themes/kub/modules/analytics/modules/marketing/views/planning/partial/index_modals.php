<?php

/* @var $this yii\web\View
 * @var $model \frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm
 */

use common\components\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

?>
<div id="add-marketing-plan-modal" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">
                Добавить маркетинговый план
            </h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?= $this->render('model_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<!-- Confirm delete -->
<?php Modal::begin([
    'id' => 'many-delete',
    'title' => 'Вы уверены, что хотите удалить выбранные модели?',
    'closeButton' => false,
    'titleOptions' => [
        'class' => 'text-center',
    ],
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
        'data-url' => Url::to(['many-delete']),
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'copy-model',
    'title' => 'Вы уверены, что хотите скопировать прогноз по маркетингу?',
    'closeButton' => false,
    'titleOptions' => [
        'class' => 'text-center',
    ],
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::a('Да', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
            'data-url' => Url::to(['many-copy']),
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>
