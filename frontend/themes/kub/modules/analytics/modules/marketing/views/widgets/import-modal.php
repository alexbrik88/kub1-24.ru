<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\ImportJobData;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\modules\marketing\models\MarketingSettings;
use frontend\modules\analytics\modules\marketing\models\ImportForm;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\View;

/**
 * @var View $this
 * @var ImportForm $form
 * @var MarketingSettings $marketingSettings
 * @var ImportJobData[] $lastAutoJobs
 * @var string $disconnectUrl
 */

$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';

?>
<?php Pjax::begin([
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>
<div class="statement-service-content" style="position: relative; min-height: 110px;">
        <?php $widget = ActiveForm::begin([
            'enableClientValidation' => false,
            'action' => ['import'],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            'options' => [
                'data-pjax' => true,
                'data-max-files' => 5,
            ],
        ]) ?>

        <div class="row">
            <?= $widget->field($form, 'dateFrom', [
                'options' => [
                    'class' => 'form-group col-6',
                ],
                'wrapperOptions' => [
                    'class' => 'form-filter date-picker-wrap',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'class' => 'form-control date-picker',
                'value' => DateHelper::format($form->dateFrom, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>

            <?= $widget->field($form, 'dateTo', [
                'options' => [
                    'class' => 'form-group col-6',
                ],
                'wrapperOptions' => [
                    'class' => 'form-filter date-picker-wrap',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'class' => 'form-control date-picker',
                'value' => DateHelper::format($form->dateTo, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>
        <div class="d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Отправить запрос</span><span class="ladda-spinner"></span>', [
                'class' => 'button-clr button-regular button-regular_red',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-regular button-hover-transparent',
                'data-dismiss' => 'modal',
                'title' => 'Отменить',
                'style' => 'width: 150px;',
            ]); ?>
        </div>
    <?php ActiveForm::end() ?>
</div>

<div class="banking-autoload-wrapper pt-2">
    <div class="banking-autoload-wrapper">
        <button class="link link_collapse link_bold button-clr mt-3 collapsed" type="button" data-toggle="collapse" data-target="#moreBankDetails" aria-expanded="false" aria-controls="moreBankDetails">
            <span class="link-txt">Автоматически загружать выписку</span>
            <svg class="link-shevron svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </button>
    </div>
</div>
<div class="collapse" id="moreBankDetails">
    <div class="row">
        <div class="col-6">
            <?= Html::radioList(
                'AutoImportForm[auto_import_type]',
                $marketingSettings->auto_import_type,
                MarketingSettings::AUTO_IMPORT_TYPE_MAP,
                ['id' => 'autoload-mode-selector']
            ) ?>

            <?= Html::button('Сохранить', [
                'class' => 'btn yellow save-autoload',
                'style' => 'margin-right: 10px; margin-bottom: 15px;',
                'data-url' => Url::to(['set-auto-import']),
            ]);
            ?>
            <span id="autoload_save_report">Настройки сохранены</span>
        </div>
        <div class="col-6">
            <div class="bank-form-wrap">
                <strong class="mb-2 d-block">Последние автозагрузки</strong>
                <table class="table table-style table-style_2 mb-0">
                    <thead>
                    <tr>
                        <th class="align-middle">Период</th>
                        <th class="align-middle">Дата <br> загрузки</th>
                        <th class="align-middle">Загружено <br> операций</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($lastAutoJobs) > 0) : ?>
                    <?php foreach ($lastAutoJobs as $jobData): $helper = new ImportParamsHelper($jobData) ?>
                        <tr>
                            <td class="nowrap">
                                <?php if ($helper->hasDateFrom() && $helper->hasDateTo()): ?>
                                    <?= sprintf(
                                        '%s-%s',
                                        $helper->getDateFrom()->format(DateHelper::FORMAT_USER_DATE),
                                        $helper->getDateTo()->format(DateHelper::FORMAT_USER_DATE)
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $jobData->getCreatedAt()->format(DateHelper::FORMAT_USER_DATE) ?>
                            </td>
                            <td>
                                <?= $jobData->count ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="3">Автозагрузки не найдены</td>
                        </tr>
                    <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="clearfix banking-delete-ask-wrapper" style="margin-top: 20px;">
        <?= ConfirmModalWidget::widget([
            'options' => [
                'id' => 'delete-confirm',
            ],
            'toggleButton' => [
                'tag' => 'a',
                'label' => 'Отключить интеграцию',
                'class' => 'link'
            ],
            'confirmUrl' => $disconnectUrl,
            'confirmParams' => [],
            'message' => 'Вы уверены, что хотите отключить интеграцию?',
        ]) ?>
    </div>
</div>
<?php Pjax::end() ?>
