<?php

namespace frontend\modules\analytics\modules\marketing\views;

use frontend\modules\analytics\modules\marketing\models\UploadType;
use yii\bootstrap\Nav;
use yii\web\View;

/**
 * @var int|null $type
 * @var View $this
 */

?>

<div class="nav-tabs-row mb-2 pb-1">
    <?= Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'Вариант загрузки 1',
                'url' => ['/analytics/marketing/default/upload-data', 'type' => UploadType::TYPE_ADS],
                'active' => ($type == UploadType::TYPE_ADS),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Вариант загрузки 2',
                'url' => ['/analytics/marketing/default/upload-data', 'type' => UploadType::TYPE_ADS_LEADS],
                'active' => ($type == UploadType::TYPE_ADS_LEADS),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Вариант загрузки 3',
                'url' => ['/analytics/marketing/default/upload-data', 'type' => UploadType::TYPE_ADS_LEADS_SELLS],
                'active' => ($type == UploadType::TYPE_ADS_LEADS_SELLS),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Уведомления',
                'url' => ['/analytics/marketing/default/notifications'],
                'active' => (!in_array($type, UploadType::TYPE_LIST)),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
        ],
    ]) ?>
</div>
