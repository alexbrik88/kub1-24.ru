<?php

namespace frontend\modules\analytics\modules\marketing\views;

use frontend\modules\analytics\modules\marketing\models\Integration;
use frontend\rbac\permissions\BusinessAnalytics;
use Yii;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Integration $integration
 */

$id = $integration->getChannel()->id;
$url = Url::to([sprintf('/marketing/%s/auth', $integration->getChannel()->getControllerId())]);

?>

<?php if ($integration->isActive()): ?>
<span>Активен</span>
<div class="semaphore tooltip2 success" data-tooltip-content="#semaphore-<?= $id ?>"></div>
<div id="tooltipster_templates" style="display: none;">
    <div id="semaphore-<?= $id ?>">Все работает</div>
</div>
<?php elseif (Yii::$app->user->can(BusinessAnalytics::ADMIN)): ?>
    <a href="<?= $url ?>" class="button-regular button-regular_padding_bigger button-hover-content-red">Подключить</a>
<?php endif; ?>
