<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\modules\marketing\models\MarketingChannel;
use frontend\modules\analytics\modules\marketing\models\UploadType;
use frontend\rbac\permissions\BusinessAnalytics;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;
use yii\widgets\Pjax;
use Yii;

/**
 * @var int $type
 * @var View $this
 * @var array $integrations
 * @var bool $showGoogleAnalyticsModal
 */

$this->title = 'Загрузка данных';

?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/tabs', [
    'controller' => Yii::$app->controller,
]) ?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/settings-tabs', [
    'type' => $type,
]) ?>

<div class="wrap" style="padding: 16px 12px!important;">
    <h4 class="mb-2">
        <?php if ($type == UploadType::TYPE_ADS_LEADS_SELLS): ?>
            Загрузка данных из Рекламных каналов + Лиды из Гугл Аналитикс + Продажи из CRM
        <?php elseif ($type == UploadType::TYPE_ADS_LEADS): ?>
            Загрузка данных из Рекламных каналов + Лиды из Гугл Аналитика
        <?php else: ?>
            Загрузка данных только из Рекламных каналов
        <?php endif; ?>
    </h4>
    <?php if ($type == UploadType::TYPE_ADS_LEADS_SELLS): ?>
        <span class="text-grey">Отличие от Варианта 2 - в данном варианте сквозная аналитика до продажи.</span>
    <?php elseif ($type == UploadType::TYPE_ADS_LEADS): ?>
        <span class="text-grey">Отличие от Варианта 1 - более точные данные по Лидам за счет использования Гугл Аналитика.</span>
    <?php endif; ?>
    <div class="mt-2">
        <?php if ($type == UploadType::TYPE_ADS_LEADS_SELLS): ?>
            Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
            У вас настроена Гугл Аналитика и там собираются данные по всем каналам.<br/>
            ВАЖНО - интеграции с рекламными каналами отличаются от интеграций в Варианте 1.<br/>
            Для этого варианта у вас должна быть СРМ, с которой можно интегрироваться.
        <?php elseif ($type == UploadType::TYPE_ADS_LEADS): ?>
            Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
            Если у вас настроена Гугл Аналитика и там собираются данные по всем каналам, то этот вариант для вас. В Гугл Аналитике точные данные по Лидам.<br/>
            ВАЖНО - интеграции с рекламными каналами отличаются от интеграций в Варианте 1.
        <?php else: ?>
            Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
            Если у вас НЕ настроена Гугл Аналитика и там НЕ собираются данные по всем каналам, то вам нужен Вариант 1.
        <?php endif; ?>
    </div>
</div>
<div class="wrap wrap_count">
    <div class="scroll-table-other">
        <div class="table-wrap">
            <table class="table table-style mb-0 text-dark">
                <tbody>
                <tr>
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-30" width="30%">
                        <span class="text_size_16 weight-700">Рекламные каналы</span>
                    </td>
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-20" width="20%">
                        <span class="text_size_16 weight-700">Статус</span>
                    </td>
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-50">
                        <span class="text_size_16 weight-700">Описание</span>
                    </td>
                </tr>
                <?php foreach ($integrations as $integration): ?>

                    <?php // skip ga
                    if (!empty($integration['channel']) &&
                        $integration['channel'] == MarketingChannel::GOOGLE_ANALYTICS &&
                        $type == UploadType::TYPE_ADS)
                        continue;
                    ?>

                    <tr>
                        <td class="pl-0 pr-0 pt-2 pb-2 no-border border-bottom">
                            <img src="<?= $integration['logo'] ?>" width="<?= $integration['logoWidth'] ?>"/>
                            <span style="vertical-align: middle;"><?= $integration['name'] ?></span>
                        </td>
                        <td class="pl-0 pr-0 pt-2 pb-2 no-border border-bottom">
                            <?php if (Yii::$app->user->can(BusinessAnalytics::ADMIN)) : ?>
                                <?php if (!$integration['isImplemented']): ?>
                                    <?= Html::a('Подключить', 'javascript:;', [
                                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red disabled',
                                        'disabled' => true,
                                        'style' => 'width: 120px;',
                                    ]) ?>
                                <?php elseif ($integration['status'] == true): ?>
                                    <?= Html::a($integration['disconnectText'] ?? 'Отключить', $integration['disconnectUrl'], [
                                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red '
                                            . ($integration['disconnectLinkClass'] ?? null),
                                        'data-url' => $integration['disconnectUrl'],
                                        'style' => 'width: 120px;',
                                    ]) ?>
                                <?php else: ?>
                                    <?= Html::a('Подключить', $integration['url'], [
                                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red '
                                            . ($integration['linkClass'] ?? null),
                                        'data-url' => $integration['url'],
                                        'style' => 'width: 120px;',
                                    ]) ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td class="pl-0 pr-0 pt-2 pb-2 no-border border-bottom">
                            <?= $integration['description'] ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none">
    <span id="tooltip_analytics_account" style="display: inline-block;">
        В Google Analytics есть 3 уровня иерархии: Аккаунт, Ресурс и Представление.<br>
        Аккаунт – это каталог для похожих сайтов. Всего у вас может быть до 100 аккаунтов. В одном Аккаунте может быть до 50 Ресурсов.
    </span>
    <span id="tooltip_analytics_property" style="display: inline-block;">
        Ресурс – это сайт или приложение, которое вы отслеживаете. В одном Аккаунте может быть до 50 Ресурсов.<br>
        На сайте или приложении размещается код для отслеживания, у него есть идентификатор отслеживания,<br>
        который указан на сайте Google Analytics в поле Google Analytics ID. Это и есть идентификатор Ресурса,<br>
        он имеет вид:  UA-ХХХХХХХХХХ.
    </span>
    <span id="tooltip_analytics_view" style="display: inline-block;">
        Представление – это набор настроек и отчетов. В них можно создавать разные цели,<br>
        настраивать оповещения и предоставлять доступ разным людям. Например, в одном представлении у вас будут показаны все данные,<br>
        а во втором – только по конкретному поддомену. Для каждого ресурса  можно создать до 25 представлений.
    </span>
    <span id="tooltip_analytics_goal" style="display: inline-block;">
        Цели позволяют узнать, как часто пользователи совершают нужное вам действие.<br>
        Цели являются отличным индикатором эффективности работы вашего сайта.<br>
        Целью может быть любое действие, в котором вы заинтересованы, называемое конверсией.<br>
        Вот некоторые примеры целей: покупка (для сайта электронной торговли),<br>
        отправка контактной информации (для сайта по привлечению клиентов).
    </span>
</div>
<?php Modal::begin([
    'id' => 'google-analytics-connect-modal',
    'options' => ['style' => 'z-index: 999'],
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>
<h4 class="modal-title">Настройка интеграции с Google Analytics</h4>
<?php Pjax::begin([
    'id' => 'google-analytics-connect-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<?php Pjax::end() ?>
<?php Modal::end() ?>
<script>
    $(document).ready(function(e) {
        if (<?= $showGoogleAnalyticsModal ?>) {
            $('.connect-google-analytics').click();
        }

        $(document).on('click', '.js-goal-add-new', function (e) {
            var $fieldGoalFormGroup = $('.js-field-goal.hide').parents('.form-group').eq(0);
            var $fieldGoalFormGroupLastVisible = $('.js-field-goal:not(.hide):last').parents('.form-group').eq(0);

            if ($fieldGoalFormGroupLastVisible.length) {
                $fieldGoalFormGroup.detach().insertAfter($fieldGoalFormGroupLastVisible);
            }

            $('.js-field-goal').find('select').each(function (index, el) {
                var realIndex = index + 1;
                var $name = $(el).attr('name');
                $(el).attr('name', $name.replace(/\[\d+\]/, '[' + realIndex + ']'));
            });

            $fieldGoalFormGroup.find('select').prop('disabled', false);
            $fieldGoalFormGroup.find('.js-field-goal').removeClass('hide');

            // var newOption = new Option('hello', '1', false, false);
            // $fieldGoalFormGroup.find('select').append(newOption).trigger('change');

            refreshGoalAddNewButton();
        });

        $(document).on('click', '.js-goal-remove', function (e) {
            $(this).parents('.js-field-goal').addClass('hide');
            $(this).parents('.js-field-goal').find('select').val(null).trigger('change');
            $(this).parents('.js-field-goal').find('select').prop('disabled', true);

            refreshGoalAddNewButton();
        });

        refreshGoalAddNewButton();
    });

    function refreshGoalAddNewButton() {
        var hiddenGoals = $('.js-field-goal.hide').length;

        if (hiddenGoals === 0) {
            $('.js-goal-add-new')
                .addClass('disabled')
                .prop('disabled', true);
        } else {
            $('.js-goal-add-new')
                .removeClass('disabled')
                .prop('disabled', false);
        }
    }

    $(document).on('change', '#settingsform-analyticsaccount', function (e) {
        var propertyInput = $('#settingsform-analyticsproperty');

        propertyInput.find('option').remove();
        $.each($.parseJSON($('#jsAnalyticsProperties').val())[$(this).val()], function (i, val) {
            var option = new Option(val, i);
            propertyInput.append($(option));
        });
        propertyInput.trigger('change');
    });

    $(document).on('change', '#settingsform-analyticsproperty', function (e) {
        var viewInput = $('#settingsform-analyticsview');

        viewInput.find('option').remove();
        $.each($.parseJSON($('#jsAnalyticsViews').val())[$(this).val()], function (i, val) {
            var option = new Option(val, i);
            viewInput.append($(option));
        });
        viewInput.trigger('change');
    });

    $(document).on('change', '#settingsform-analyticsview', function (e) {
        var goalInput = $('#settingsform-mainanalyticsgoal, .js-field-goal select');
        var goalError = goalInput.siblings('.invalid-feedback');
        var goals = $.parseJSON($('#jsAnalyticsGoals').val())[$(this).val()];

        goalInput.find('option').remove();
        goalInput.removeClass('is-invalid');
        goalError.text('');

        if (goals === undefined) {
            goalInput.addClass('is-invalid');
            goalError.text('Не найдены цели в представлении "' + $(this).val() + '". Пожалуйста создайте цели.');
        } else {
            $.each(goals, function (i, val) {
                var option = new Option(val, i);
                goalInput.append($(option));
            });

            goalInput.val(null);
        }

        goalInput.trigger('change');
        refreshGoalAddNewButton();
    });

    $(document).on('submit', '#google-analytics-connect-form', function() {
        $(this).find('.invalid-feedback').text('');
        $(this).find('.is-invalid').removeClass('is-invalid');
    });

    $(document).on('click', '.remove-google-account', function(e) {
        $.post($(this).data('url'), null);
    });
</script>
