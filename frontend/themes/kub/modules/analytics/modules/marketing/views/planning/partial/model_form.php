<?php

/* @var $this yii\web\View
 * @var $model \frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm
 */

use Carbon\Carbon;
use common\models\marketing\MarketingPlan;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

$marketingPlan = $model->getMarketingPlan();
$from = $model->startDate ? Carbon::createFromFormat('m.Y', $model->startDate) : Carbon::now();
$till = $model->endDate ? Carbon::createFromFormat('m.Y', $model->endDate) : Carbon::now()->addMonths(11);

$form = ActiveForm::begin([
    'id' => 'form-finance-model',
    'action' => $marketingPlan->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $marketingPlan->id]),
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'name'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'template')->widget(Select2::class, [
                        'data' => MarketingPlan::$templateMap,
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => '',
                            'disabled' => !$model->getMarketingPlan()->isNewRecord,
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'startDateText', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                        'value' => Yii::$app->formatter->asDate($from->format('Y-m-01'), 'LLLL Y'),
                        'class' => 'form-control field-datepicker field-from-datepicker ico',
                        'style' => 'text-transform: capitalize;',
                        'autocomplete' => 'off',
                        'readonly' => true
                    ])->label('Месяц начала'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'startDate')
                            ->hiddenInput(['value' => $from->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'endDateText', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                        'value' => Yii::$app->formatter->asDate($till->format('Y-m-01'), 'LLLL Y'),
                        'class' => 'form-control field-datepicker field-to-datepicker ico',
                        'style' => 'text-transform: capitalize;',
                        'autocomplete' => 'off',
                        'readonly' => true
                    ])->label('Месяц окончания'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'endDate')
                            ->hiddenInput(['value' => $till->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'currency')->widget(Select2::class, [
                        'data' => array_map(function(array $currency): string {
                            return "{$currency['name']}, {$currency['code']}";
                        }, MarketingPlan::$currencyMap),
                        'options' => [
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model,'channels', [
                        'options' => [
                            'class' => '',
                        ],
                    ])->checkboxList(MarketingPlan::$channelMap) ?>

                    <?= $form->field($model, 'channels', ['template' => '{error}'])
                        ->checkboxList(MarketingPlan::$channelMap) ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'comment')->textarea([
                'placeholder' => '',
                'rows' => '3',
            ]); ?>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>
<?php

$jsFrom = $from->format('Y').','.($from->format('m')-1).',1';
$jsTill = $till->format('Y').','.($till->format('m')-1).',1';

$this->registerJs(<<<JS

    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear();
      var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1);
      var dd = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
      return "".concat(yyyy).concat('-').concat(mm).concat('-').concat(dd);
    };

    const DatePickerFrom = $('.field-from-datepicker');
    const DatePickerTo = $('.field-to-datepicker');
    let datepickerConfig = kubDatepickerConfig;
    let datepickerConfigFrom;
    let datepickerConfigTo;
    
    datepickerConfig.view = 'months';
    datepickerConfig.minView = 'months';
    datepickerConfig.dateFormat = 'MM yyyy';
    datepickerConfig.language = 'ru';
    datepickerConfigFrom = Object.assign({}, datepickerConfig);    
    datepickerConfigFrom.onSelect = function(formattedDate, date) {
        $('#marketingplanningform-startdate').val(date.yyyymmdd());
    };
    datepickerConfigTo = Object.assign({}, datepickerConfig);
    datepickerConfigTo.onSelect = function(formattedDate, date) {
        $('#marketingplanningform-enddate').val(date.yyyymmdd());
    };
    
    
    DatePickerFrom.datepicker(datepickerConfigFrom);
    DatePickerTo.datepicker(datepickerConfigTo);

    DatePickerFrom.data('datepicker').selectDate(new Date({$jsFrom}));
    DatePickerTo.data('datepicker').selectDate(new Date({$jsTill}));
    DatePickerFrom.data('datepicker').date = (new Date({$jsFrom}));
    DatePickerTo.data('datepicker').date = (new Date({$jsTill}));
    
JS, View::POS_READY);
?>