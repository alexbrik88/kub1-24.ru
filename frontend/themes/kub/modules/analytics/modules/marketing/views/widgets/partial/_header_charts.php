<?php

use common\modules\marketing\models\MarketingReportSearch;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use common\modules\marketing\models\MarketingChannel;
use frontend\components\Icon;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/**
 * @var View $this
 * @var $marketingUserConfig
 * @var int $channelType
 */

$this->registerJsFile('@web/scripts/marketing.charts.js', [
    'position' => View::POS_HEAD
]);

$customCampaign = Yii::$app->request->get('campaign');
$customOffset = Yii::$app->request->get('offset');
$blockTitle = $blockTitle ?? 'Статистика';
$showConfig = $showConfig ?? true;
?>

<div class="wrap wrap_count" style="margin-bottom: 12px">
    <div class="row align-items-center">
        <div class="column col-8 mb-0">
            <div class="row">
                <div class="column col-xxx mr-auto position-relative">
                    <h4 class="mb-0"><?= $blockTitle ?></h4>
                    <?= ($showConfig) ? $this->render('chart/config/chart_11_config', [
                        'config' => $marketingUserConfig
                    ]) : '' ?>
                </div>
                <?php if (!empty($additionalButtons)): ?>
                    <div class="column col-xxx">
                        <?= $this->render('chart/config/chart_11_config', [
                            'config' => $marketingUserConfig,
                            'showAsButton' => true,
                            'usePjax' => false
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (!empty($additionalButtons)): ?>
        <div class="column col-4 mb-0">
            <div class="row">
                <div class="column col-xxx">
                    <?= Html::button(Icon::get('book'), [
                        'title' => 'Описание',
                        'class' => 'button-list button-hover-transparent button-clr collapsed',
                        'tab' => FinanceDashboard::TAB_MARKETING
                    ]) ?>
                </div>
                <div class="column col-xxx">
                    <?= Html::button(Icon::get('clip-2'), [
                        'title' => 'Скопировать ссылку',
                        'class' => 'button-list button-hover-transparent button-clr collapsed',
                        'tab' => FinanceDashboard::TAB_MARKETING
                    ]) ?>
                </div>
                <div class="column col-xxx" style="flex:1">
                    <?= Select2::widget([
                        'id' => 'marketing-chart-select-channel',
                        'name' => 'marketing-chart-select-channel',
                        'data' => MarketingReportSearch::getChannelList(),
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
        <?php else: ?>
        <div class="column col-4 mb-0 select2-wrapper">
            <?php if ($channelType == MarketingChannel::DASHBOARD_TOTALS): ?>
                <?= Select2::widget([
                    'id' => 'marketing-chart-select-channel',
                    'name' => 'marketing-chart-select-channel',
                    'data' => MarketingReportSearch::getChannelList(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            <?php else: ?>
                <?= Select2::widget([
                    'id' => 'marketing-chart-select-campaign',
                    'name' => 'marketing-chart-select-campaign',
                    'data' => ['' => 'Все'] + MarketingReportSearch::getCampaignList($company, $channelType),
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>

    <?php Pjax::begin([
        'id' => 'pjax_marketing_statistics_charts',
        'enableReplaceState' => false,
        'enablePushState' => false,
        'linkSelector' => false,
        'formSelector' => false,
        'scrollTo' => false,
        'timeout' => 10E3
    ]) ?>

    <?= $this->render('chart/chart_1', [
        'company' => $company,
        'channelType' => $channelType,
        'marketingUserConfig' => $marketingUserConfig,
        'customCampaign' => $customCampaign,
        'customOffset' => $customOffset
    ]) ?>

    <?php Pjax::end() ?>

    <?php if (Yii::$app->request->isPjax) return; ?>

    <?= $this->render('chart/chart_2', [
        'company' => $company,
        'channelType' => $channelType,
        'marketingUserConfig' => $marketingUserConfig
    ]) ?>

</div>

<input type="hidden" id="marketing-channel-type" value="<?= $channelType ?>">

<script>
    $(document).ready(function () {
        Chart123.init();
    });
</script>