<?php
/* @var $this yii\web\View
 * @var $model \frontend\modules\analytics\modules\marketing\models\UpdatePlanningMarketingForm
 * @var $user \common\models\employee\Employee
 * @var $reloadItems bool
 */

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\marketing\MarketingCalculatingPlanningSetting;
use frontend\modules\analytics\models\AbstractFinance;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;
use frontend\themes\kub\helpers\Icon;

if ($reloadItems) {
    $model->getRepeatedPurchasesItems($user);
}

$itemsCount = count($model->items) ?: 1;
$totalBudgetAmount = 0;
$totalMarketingServiceAmount = 0;
$totalAdditionalExpensesAmount = 0;
$totalTotalAdditionalExpensesAmount = 0;
$totalAdditionalExpensesAmountForOne = 0;
$totalTotalExpensesAmount = 0;
$totalClicksCount = 0;
$totalClickAmount = 0;
$totalFinalCostsClickAmount = 0;
$totalRegistrationConversion = 0;
$totalRegistrationCount = 0;
$totalRegistrationAmount = 0;
$totalFinalCostsRegistrationAmount = 0;
$totalActiveUserConversion = 0;
$totalActiveUsersCount = 0;
$totalActiveUserAmount = 0;
$totalFinalCostsUserAmount = 0;
$totalPurchaseConversion = 0;
$totalPurchasesCount = 0;
$totalSaleAmount = 0;
$totalAverageCheck = 0;
$totalProceedsFromNewAmount = 0;
$totalChurnRate = 0;
$totalRepeatedPurchasesAmount = 0;
$totalTotalProceedsAmount = 0;
$totalFinancialResult = 0;
$totalRepeatedPurchasesCount = 0;
$totalTotalPurchasesCount = 0;
$totalSalespersonSalaryAmount = 0;
$budgetAmountCalculatingSettings = [];
foreach (['average_check', 'churn_rate'] as $calculatingSettingAttribute) {
    $budgetAmountCalculatingSettings[$calculatingSettingAttribute] = MarketingCalculatingPlanningSetting::find()
        ->andWhere(['marketing_plan_id' => $model->getMarketingPlan()->id])
        ->andWhere(['channel' => $model->getChannel()])
        ->andWhere(['attribute' => $calculatingSettingAttribute])
        ->asArray()
        ->one();
}

$currencySymbol = $model->getMarketingPlan()->getCurrencySymbol();
?>
<?php Pjax::begin([
    'id' => "channel-planning-table-pjax-{$model->getChannel()}",
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<style>
    .table-wrap ::-webkit-scrollbar {
        height: 11px;
    }

    .table-wrap ::-webkit-scrollbar-track {
        border-radius: 0;
        box-shadow: none;
        border: 0;
        background-color: #fff;
    }

    .table-wrap ::-webkit-scrollbar-thumb {
        border-radius: 5px;
        box-shadow: none;
        border: 0;
        background-color: #bbc1c7;
    }

    .table-wrap ::-webkit-scrollbar-thumb:hover {
        background-color: #bbc1c7;
    }
</style>

<div class="wrap planning-table planning-table-<?= $model->getChannel() ?>" data-channel="<?= $model->getChannel() ?>" style="padding-left: 0;padding-top: 0;">
    <div class="table-wrap">
        <table class="scrollable-table double-scrollbar-top table table-style table-count-list table-compact finance-model-shop-month marketing-planning-table">
            <thead>
            <tr>
                <th class="fixed-column" style="min-width: 280px;">
                    Маркетинговый план
                </th>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <th class="nowrap">
                        <?= ArrayHelper::getValue(AbstractFinance::$month, str_pad($marketingPlanItem['month'], 2, "0", STR_PAD_LEFT)) . ' ' . $marketingPlanItem['year'] ?>
                    </th>
                <?php endforeach; ?>
                <th class="nowrap">
                    Итог
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="fixed-column bold text-left text_size_14">Продажи</td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <td></td>
                <?php endforeach; ?>
                <td></td>
            </tr>
            <tr>
                <td class="fixed-column bold text-left text_size_14">Новые продажи, шт</td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalPurchasesCount += (int)$marketingPlanItem['purchases_count'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::integerFormat((int)$marketingPlanItem['purchases_count']) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?= TextHelper::integerFormat($totalPurchasesCount) ?>
                </td>
            </tr>
            <tr data-attr="average_check">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Средний чек, <?= $currencySymbol ?></div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['average_check']) ? json_encode($budgetAmountCalculatingSettings['average_check']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php $nonEmptyItemsCount = 0 ?>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php
                    $totalAverageCheck += (int)$marketingPlanItem['average_check'];
                    if ((int)$marketingPlanItem['average_check'] > 0) {
                        $nonEmptyItemsCount++;
                    }
                    ?>
                    <td class="td-custom-value month-block">
                        <?= Html::textInput(
                            "UpdatePlanningMarketingForm[average_check][{$marketingPlanItem['month']}{$marketingPlanItem['year']}]",
                            TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['average_check'], 2),
                            [
                                'class' => 'custom-value form-control global-field average-check-' . $marketingPlanItem['month'] . $marketingPlanItem['year'] . " average_check-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                                'data-class' => "average-check-{$marketingPlanItem['month']}{$marketingPlanItem['year']}",
                            ],
                        ) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat(round($totalAverageCheck / ($nonEmptyItemsCount ?: 1)), 2) ?>
                </td>
            </tr>
            <tr data-attr="proceeds_from_new_amount">
                <td class="fixed-column text-grey text-left text_size_14">Выручка с новых, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalProceedsFromNewAmount += (int)$marketingPlanItem['proceeds_from_new_amount'] ?>
                    <td class="month-block text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['proceeds_from_new_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalProceedsFromNewAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="churn_rate">
                <td class="fixed-column text-grey text-left text_size_14">
                    <div class="d-flex">
                        <div class="mr-2 nowrap">Отток клиентов (Churn rate), %</div>
                        <div class="edit-costs-js link d-block ml-auto"
                             data-calculating-setting='<?= isset($budgetAmountCalculatingSettings['churn_rate']) ? json_encode($budgetAmountCalculatingSettings['churn_rate']) : null ?>'
                             title="Настройки">
                            <?= Icon::get('cog') ?>
                        </div>
                    </div>
                </td>
                <?php
                $churnRateNumber = 0;
                $nonEmptyItemsCount = 0;
                ?>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php
                    $totalChurnRate += (float)$marketingPlanItem['churn_rate'];
                    if ((float)$marketingPlanItem['churn_rate'] > 0) {
                        $nonEmptyItemsCount++;
                    }
                    ?>
                    <?php if ($churnRateNumber === 0): ?>
                        <td class="month-block text-right">
                            0
                        </td>
                    <?php else: ?>
                        <td class="td-custom-value month-block">
                            <?= Html::textInput(
                                "UpdatePlanningMarketingForm[churn_rate][{$marketingPlanItem['month']}{$marketingPlanItem['year']}]",
                                TextHelper::invoiceMoneyFormat((float)$marketingPlanItem['churn_rate'] * 100, 2),
                                [
                                    'class' => 'custom-value int form-control global-field churn-rate-' . $marketingPlanItem['month'] . $marketingPlanItem['year'] . " churn_rate-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                                    'data-class' => "churn-rate-{$marketingPlanItem['month']}{$marketingPlanItem['year']}",
                                ]
                            ) ?>
                        </td>
                    <?php endif; ?>
                    <?php $churnRateNumber++ ?>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat(round($totalChurnRate / ($nonEmptyItemsCount ?: 1)) * 100, 2) ?>
                </td>
            </tr>
            <tr data-attr="repeated_purchases_count">
                <td class="fixed-column text-grey text-left text_size_14">Повторные продажи, шт</td>
                <?php $repeatedPurchasesCountNumber = 0; ?>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalRepeatedPurchasesCount += (int)$marketingPlanItem['repeated_purchases_count'] ?>
                    <?php if ($repeatedPurchasesCountNumber === 0): ?>
                        <td class="td-custom-value month-block">
                            <?= Html::textInput(
                                "UpdatePlanningMarketingForm[repeated_purchases_count][{$marketingPlanItem['month']}{$marketingPlanItem['year']}]",
                                TextHelper::integerFormat((int)$marketingPlanItem['repeated_purchases_count']/4, 2),
                                [
                                    'class' => 'custom-value int form-control global-field repeated-purchases-count-' . $marketingPlanItem['month'] . $marketingPlanItem['year'] . " repeated_purchases_count-{$model->getChannel()}-{$marketingPlanItem['year']}{$marketingPlanItem['month']}",
                                    'data-class' => "repeated-purchases-count-{$marketingPlanItem['month']}{$marketingPlanItem['year']}",
                                ]
                            ) ?>
                        </td>
                    <?php else: ?>
                        <td class="month-block text-right">
                            <?= TextHelper::integerFormat((int)$marketingPlanItem['repeated_purchases_count'], 2) ?>
                        </td>
                    <?php endif; ?>
                    <?php $repeatedPurchasesCountNumber++; ?>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::integerFormat($totalRepeatedPurchasesCount, 2) ?>
                </td>
            </tr>
            <tr data-attr="repeated_purchases_amount">
                <td class="fixed-column text-grey text-left text_size_14">Выручка с повторных, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalRepeatedPurchasesAmount += (int)$marketingPlanItem['repeated_purchases_amount'] ?>
                    <td class="month-block text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['repeated_purchases_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalRepeatedPurchasesAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="total_purchases_count">
                <td class="fixed-column bold text-left text_size_14">Итого продажи, шт</td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalTotalPurchasesCount += (int)$marketingPlanItem['total_purchases_count'] ?>
                    <td class="month-block bold text-right">
                        <?= TextHelper::integerFormat((int)$marketingPlanItem['total_purchases_count'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="month-block bold text-right">
                    <?= TextHelper::integerFormat($totalTotalPurchasesCount, 2) ?>
                </td>
            </tr>
            <tr data-attr="total_proceeds_amount">
                <td class="fixed-column bold text-left text_size_14">Выручка итого, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalTotalProceedsAmount += (int)$marketingPlanItem['total_proceeds_amount'] ?>
                    <td class="bold month-block text-right">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['total_proceeds_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right">
                    <?= TextHelper::invoiceMoneyFormat($totalTotalProceedsAmount, 2) ?>
                </td>
            </tr>
            <tr data-attr="financial_result">
                <td class="fixed-column bold text-left text_size_14">Результат, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <?php $totalFinancialResult += (int)$marketingPlanItem['financial_result'] ?>
                    <td class="bold month-block text-right <?= (int)$marketingPlanItem['financial_result'] < 0 ? 'red-column' : null; ?>">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['financial_result'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td class="bold month-block text-right <?= (int)$totalFinancialResult < 0 ? 'red-column' : null; ?>">
                    <?= TextHelper::invoiceMoneyFormat($totalFinancialResult, 2) ?>
                </td>
            </tr>
            <tr data-attr="cumulative_financial_amount">
                <td class="fixed-column bold text-left text_size_14">Результат нарастающим, <?= $currencySymbol ?></td>
                <?php foreach ($model->items as $marketingPlanItem): ?>
                    <td class="bold month-block text-right <?= (int)$marketingPlanItem['cumulative_financial_amount'] < 0 ? 'red-column' : null; ?>">
                        <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['cumulative_financial_amount'], 2) ?>
                    </td>
                <?php endforeach; ?>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php Pjax::end() ?>
