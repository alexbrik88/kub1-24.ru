<?php

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\analytics\modules\marketing\models\PlanningMarketingSearch
 * @var $dataProvider \yii\data\DataProviderInterface
 * @var $user \common\models\employee\Employee
 * @var $model \frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm
 */

use Carbon\Carbon;
use common\components\grid\GridView;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\marketing\MarketingPlan;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\documents\widgets\SummarySelectWidget;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableViewWidget;
use yii\widgets\ActiveForm;

$this->title = 'Планирование по маркетингу';
$tabViewClass = $user->config->getTableViewClass('table_view_marketing_planning');
?>
    <div class="stop-zone-for-fixed-elems">
        <div class="page-head d-flex flex-wrap align-items-center mb-2">
            <h4><?= Html::encode($this->title) ?></h4>
            <?= Html::button('<svg class="svg-icon"> <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use> </svg><span>Добавить</span>', [
                'class' => 'button-regular button-regular_red button-width ml-auto add-planning-button',
                'data-toggle' => "modal",
                'data-target' => "#add-marketing-plan-modal"
            ]); ?>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_marketing_planning']) ?>
        </div>
        <div class="col-6">
            <?php $form = ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'name', [
                    'type' => 'search',
                    'placeholder' => 'Поиск по названию...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => 'У вас еще нет ни одного плана',
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-finance-models' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', [
        'totalCount' => $dataProvider->totalCount,
        'scroll' => true,
    ]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (MarketingPlan $model) {
                return Html::checkbox('FinanceModel[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data-id' => $model->id
                ]);
            },
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Дата',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (MarketingPlan $model) {
                return Carbon::createFromTimestamp($model->created_at)->format('d.m.Y');
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => 'link-view',
            ],
            'format' => 'raw',
            'value' => function (MarketingPlan $model) {
                return Html::a($model->name, ['view', 'id' => $model->id]);
            },
        ],
        [
            'attribute' => 'start_date',
            'label' => 'Начало',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (MarketingPlan $model) {
                $startDate = Carbon::createFromFormat('m.Y', $model->start_date);

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($startDate->format('m'), 2, "0", STR_PAD_LEFT)) . ' ' . $startDate->format('Y');
            },
        ],
        [
            'attribute' => 'end_date',
            'label' => 'Окончание',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (MarketingPlan $model) {
                $endDate = Carbon::createFromFormat('m.Y', $model->end_date);

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($endDate->format('m'), 2, "0", STR_PAD_LEFT)) . ' ' . $endDate->format('Y');
            },
        ],
        [
            'attribute' => 'total_revenue',
            'label' => 'Финансовый итог',
            'headerOptions' => [
                'width' => '10%',
            ],
            'value' => function (MarketingPlan $model) {
                return TextHelper::invoiceMoneyFormat($model->financial_result, 2);
            },
        ],
        [
            'attribute' => 'responsible_employee_id',
            'label' => 'Ответственный',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '15%',
            ],
            'contentOptions' => [
                'style' => 'white-space: initial;',
            ],
            'format' => 'raw',
            'filter' => $searchModel->getResponsibleEmployeeFilter(),
            's2width' => '200px',
            'value' => function (MarketingPlan $model) {
                return $model->responsibleEmployee->getFio(true);
            },
        ],
    ],
]); ?>

<?= SummarySelectWidget::widget([
    'hideCalculatedFields' => true,
    'buttons' => [
        Html::button(Icon::get('copied').' <span>Копировать</span>', [
            'id' => 'btn-copy-model',
            'class' => 'button-clr button-regular button-width button-hover-transparent tooltip3',
            'data-toggle' => 'modal',
            'data-target' => '#copy-model',
            'data-tooltip-content' => '#tooltip-copy-model',
            'title' => 'Копировать'
        ]),
        Html::button(Icon::get('garbage').' <span>Удалить</span>', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-target' => '#many-delete',
            'title' => 'Удалить'
        ]),
    ],
]); ?>

<?= $this->render('partial/index_modals', [
    'model' => $model,
]) ?>

<?php $this->registerJs("
    $(document).on('change', '#config-marketing_plan_by_budget', function(e) {
        let byTotalExpenses = $('#config-marketing_plan_by_total_expenses');
        if ($(this).is(':checked') && byTotalExpenses.is(':checked')) {
            byTotalExpenses.prop('checked', false).trigger('change').uniform();
        }
    });

    $(document).on('change', '#config-marketing_plan_by_total_expenses', function(e) {
        let byBudget = $('#config-marketing_plan_by_budget');
        if ($(this).is(':checked') && byBudget.is(':checked')) {
            byBudget.prop('checked', false).trigger('change').uniform();
        }
    });
    
    $(document).on('hidden.bs.modal', '#add-marketing-plan-modal', function(e) {
        $('#marketingplanningform-name, #marketingplanningform-comment').val('');
        $('#marketingplanningform-channels input:checked').click()
    });
");