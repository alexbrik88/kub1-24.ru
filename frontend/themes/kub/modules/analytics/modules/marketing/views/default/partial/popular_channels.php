<?php

use yii\bootstrap\Nav;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use Carbon\Carbon;
use php_rutils\RUtils;

/* @var array $graphData
 */

$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$datePeriods = [];
foreach ($graphData['x'] as $date) {
    $datePeriods[] = [
        'from' => $date,
        'to' => $date,
    ];
}

$daysPeriods = [];
$chartLabelsX= [];
foreach ($datePeriods as $i => $date) {
    $carbonDate = Carbon::createFromFormat('Y-m-d', $date['from']);
    $day = $carbonDate->format('d');
    $daysPeriods[] = $carbonDate->format('j');
    $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
    $chartLabelsX[] = $day . ' ' . RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $date['from'],
    ]);
}

$series = [];
foreach ($graphData['indicator'] as $data) {
    $series[] = [
        'name' => $data['name'],
        'data' => $data['y'],
        'color' => $data['color'],
        'marker' => [
            'symbol' => 'circle',
            'enabled' => false
        ]
    ];
}

$htmlHeader = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th class="text-left" colspan="4">{title}</th>
        </tr>
HTML;

$htmlData = <<<HTML
    <tr>
        <td style="color: {itemColor};">●</td>
        <td>{itemName}</td>
        <td></td>
        <td><b>{itemData}</b></td>
    </tr>
HTML;

$htmlFooter = <<<HTML
    <tr>
        <td colspan="2">Итого</td>
        <td></td>
        <td><b>{totalData}</b></td>
    </tr>
    </table>
HTML;

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);
?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header">
                Рекламные каналы
            </div>
            <div class="nav-tabs-row">
                <?= Nav::widget([
                    'id' => 'graph_period_nav',
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_none w-100'],
                    'items' => [
                        [
                            'label' => 'День',
                            'url' => '#',
                            'active' => true,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding-left: 0; padding-right: 9px;',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link active',
                            ]
                        ],
                        [
                            'label' => 'Месяц',
                            'url' => '#',
                            'active' => false,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding: 0',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ]
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="nav-tabs-row">
            <?= Nav::widget([
                'id' => 'graph_statistics_channels_nav',
                'encodeLabels' => false,
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100'],
                'items' => [
                    [
                        'label' => 'Клики',
                        'url' => '#',
                        'active' => true,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link active',
                        ]
                    ],
                    [
                        'label' => 'Лиды',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                    [
                        'label' => 'Расходы',
                        'url' => '#',
                        'active' => false,
                        'options' => [
                            'class' => 'nav-item',
                            'style' => 'padding-left: 0',
                        ],
                        'linkOptions' => [
                            'class' => 'nav-link',
                        ]
                    ],
                ],
            ]) ?>
        </div>
        <?= Highcharts::widget([
            'id' => 'chart-channels-statistics',
            'scripts' => [
                'modules/exporting',
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'title' => false,
                'credits' => [
                    'enabled' => false
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'line',
                    'events' => [
                        'load' => new JsExpression('redrawPlanMonths()')
                    ],
                    'marginLeft' => '55',
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'height' => 280,
                    'reflow' => true
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0'
                    ],
                    'itemDistance' => 10
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                            let x = +this.x - 1;
                            let tooltip = '{$htmlHeader}'.replace(\"{title}\", window.chartLabelsX[x]);
                            let total = 0;

                            $.each(args.chart.series, function(key, item) {
                                let data = Highcharts.numberFormat(item.data[x].y, 0, ',', ' ');
                                total += +data;
                                tooltip += '{$htmlData}'
                                    .replace(\"{itemColor}\", item.color)
                                    .replace(\"{itemName}\", item.name)
                                    .replace(\"{itemData}\", data);
                            });

                            tooltip += '{$htmlFooter}'.replace(\"{totalData}\", total);

                            return tooltip;
                        }
                    ")
                ],
                'yAxis' => [
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
                'xAxis' => [
                    'min' => 0,
                    'max' => 11,
                    'categories' => $daysPeriods,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chartCurrDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                            (window.chartFreeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                        'useHTML' => true,
                    ],
                ],
                'series' => $series,
            ],
        ]) ?>
    </div>
</div>
<script>
    var chartCurrDayPos = 1;
    var chartLabelsX = <?= json_encode($chartLabelsX) ?>;
    var chartFreeDays = <?= json_encode($chartFreeDays) ?>;

    function redrawPlanMonths() {
        var custom_pattern = function (color) {
            return {
                pattern: {
                    path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                    width: 10,
                    height: 10,
                    color: color
                }
            }
        }

        var chartToLoad = window.setInterval(function () {
            var chart = $('#chart-channels-statistics').highcharts();
            if (typeof(chart) !== 'undefined') {
                for (var i = (1 + chartCurrDayPos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color1 ?>");
                    chart.series[1].points[i].color = custom_pattern("<?= $color2 ?>");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();

                window.clearInterval(chartToLoad);
            }

        }, 100);
    }

    $(document).ready(function () {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });
</script>
