<?php

use common\components\grid\GridView;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$filterByCampaign = $filterByCampaign ?? "";

$form = ActiveForm::begin([
    'method' => 'get',
    'options' => [
        'data' => [
            'pjax' => true
        ],
    ],
]) ?>

<div class="table-settings row row_indents_s mb-3">
    <div class="col-12">
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::textInput('filter_by_campaign_name', $filterByCampaign, [
                    'id' => 'filter_by_campaign_name',
                    'type' => 'search',
                    'placeholder' => 'Поиск по названию...',
                    'class' => 'form-control',
                    'style' => 'margin-bottom: 0;'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    'style' => 'margin-bottom: 0;'
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php $form->end(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-compact',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-add-item-main',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return Html::checkbox('_', false, [
                    'value' => $data['utm_campaign'],
                    'class' => 'joint-operation-add-item',
                    'data-name' => Html::encode($data['campaign_name'])
                ]);
            },
        ],
        [
            'attribute' => 'campaign_name',
            'label' => 'Название',
            'value' => function($data) {
                return Html::encode($data['campaign_name']) . Html::tag('span', "({$data['utm_campaign']})", ['class' => 'text-grey font-12 pl-2']);
            },
            'format' => 'raw'
        ]
    ]
]) ?>


<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('Добавить отмеченные', [
        'class' => 'btn-add-items button-regular button-regular_red button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>
