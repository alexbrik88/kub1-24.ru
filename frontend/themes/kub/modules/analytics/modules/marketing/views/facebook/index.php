<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\components\helpers\Html;
use common\models\employee\Employee;
use common\modules\marketing\models\MarketingChannel;
use common\modules\import\models\ImportJobData;
use common\modules\marketing\models\MarketingReportSearch;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use Yii;

/**
 * @var View $this
 * @var ImportJobData|null $lastJobData
 * @var MarketingReportSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var ActiveDataProvider $totalDataProvider
 * @var string $defaultSorting
 * @var Employee $employee
 */

\common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 137, true);

$this->title = 'Facebook';
$employee = Yii::$app->user->identity;
$integrationData = $employee->company->getIntegrationData(Employee::INTEGRATION_FACEBOOK);
$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';

if (!$integrationData->hasAccessToken()) {
    $integrationButton = Html::a('Настроить интеграцию', ['auth'], [
        'class' => 'button-regular button-regular_red button-width ml-auto w-100',
    ]);
} else {
    $integrationButton = Html::a("{$addIcon} <span>Загрузить данные</span>", 'javascript:;', [
        'class' => 'button-regular button-regular_red button-width ml-auto w-100 upload-vk-ads',
        'data' => [
            'url' => $integrationData->getAccountId() ? Url::to(['import']) : Url::to(['connect']),
        ],
    ]);
}

?>

<?= $this->render('../widgets/partial/_header', [
    'searchModel' => $searchModel,
    'balanceId' => Employee::INTEGRATION_FACEBOOK,
    'balanceAmount' => $integrationData->getBalance(),
    'balanceLeftDays' => 9E9, // TODO:
    'hasAccessToken' => $integrationData->hasAccessToken(),
    'channelType' => MarketingChannel::FACEBOOK,
    'logoSrc' => '/images/fb-insta-logo.png',
    'integrationButton' => $integrationButton,
    'lastJobData' => $lastJobData,
]) ?>

<?= $this->render('../widgets/reports-grid', [
    'totalDataProvider' => $totalDataProvider,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'defaultSorting' => $defaultSorting,
    'channelType' => MarketingChannel::FACEBOOK
]) ?>

<?php Modal::begin([
    'id' => 'upload-vk-ads-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>
    <h4 class="modal-title">Запрос данных из Facebook</h4>
    <?= $this->render('partial/modal_header') ?>
    <?php Pjax::begin([
        'id' => 'upload-vk-ads-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
    ]) ?>
    <?php Pjax::end() ?>
<?php Modal::end() ?>
