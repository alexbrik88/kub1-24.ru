<?php

use common\components\TextHelper;
use miloschuman\highcharts\Highcharts;

/* @var string $title
 * @var array $data
 * @var array $graphData
 * @var string $chartId
 * @var array $color
 */
?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header">
            <?= $title ?>
        </div>
        <div class="graph_statistics">
            <div class="graph_day">
                <div class="statistic_block" style="margin-right: 20px;">
                    <div class="count" style="color: <?= $color['main'] ?>">
                        <?= $data['today'] ?>
                    </div>
                    <div class="description">
                        Сегодня
                    </div>
                </div>
                <div class="statistic_block">
                    <div class="count" style="color: <?= $color['main'] ?>">
                        <?= $data['yesterday'] ?>
                    </div>
                    <div class="description">
                        Вчера
                    </div>
                </div>
            </div>
            <div class="statistic_block" style="align-items: flex-end;">
                <div class="count" style="color: <?= $color['main'] ?>">
                    <?= $data['week'] ?>
                </div>
                <div class="description">
                    Неделя
                </div>
            </div>
        </div>
    </div>
    <?= Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'modules/pattern-fill',
            'themes/grid-light',
        ],
        'options' => [
            'title' => false,
            'credits' => [
                'enabled' => false
            ],
            'legend' => [
                'enabled' => false
            ],
            'exporting' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'areaspline',
                'height' => 100,
                'margin' => [-22, -12, 0, -12],
                'spacing' => [0, 0, 0, 0],
                'showAxes' => false,
                'reflow' => true
            ],
            'tooltip' => [
                'shared' => true
            ],
            'yAxis' => [
                [
                    'title' => '',
                    'labels' => false,
                    'gridLineWidth' => 0,
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
            ],
            'xAxis' => [
                [
                    'categories' => $graphData['x'],
                    'labels' => false,
                    'gridLineWidth' => 0,
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],

            ],
            'series' => [
                [
                    'name' => $title,
                    'data' => $graphData['y'],
                    'color' => $color['main'],
                    'fillColor' => [
                        'linearGradient' => [
                            'x1' => 0,
                            'y1' => 0,
                            'x2' => 0,
                            'y2' => 1,
                        ],
                        'stops' => [
                            [0, $color['gradient'][0]],
                            [1, $color['gradient'][1]]
                        ],
                    ],
                    'marker' => [
                        'enabled' => false
                    ],
                ],
            ],
        ],
    ]); ?>
</div>