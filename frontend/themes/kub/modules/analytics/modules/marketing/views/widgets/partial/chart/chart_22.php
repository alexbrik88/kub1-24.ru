<?php

use common\modules\marketing\models\GoogleAnalyticsProfileGoal;
use common\modules\marketing\models\MarketingReportSearch;
use php_rutils\RUtils;

/**
 * @var $company
 */

$jsModel = $jsModel ?? 'Chart123.chart_22';
$id = $id ?? 'chart_22';

$channelType = $channelType ?? -1;
$customOffset = $customOffset ?? 0;
$customCampaign = $customCampaign ?? null;
$customFilters = [
    'offset' => $customOffset,
    'campaign' => $customCampaign
];

/** @var GoogleAnalyticsProfileGoal $goal */
if ($company->googleAnalyticsProfile) {
    $goal = $company->googleAnalyticsProfile->getGoals()->one();
    $goalID = ($goal) ? $goal->external_id : null;
} else {
    $goalID = null;
}

// by days
$xDays = 12;
$clicks = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.clicks', $xDays, $customFilters); // Клики
$cost = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.cost', $xDays, $customFilters); // Расходы
$ctr = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.ctr', $xDays, $customFilters); // CTR
$cpc = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report.avg_cpc', $xDays, $customFilters); // CPC
$goalTotal = MarketingReportSearch::getKeyIndicatorData($company->id, $channelType, 'marketing_report_google_analytics_goal.total', $xDays, $customFilters, $goalID); // Лиды одной точки
$goalConversion = MarketingReportSearch::calcKeyIndicatorData($goalTotal, $clicks, 'divide', true); // Конверсия в лид (одной точки)
$goalCost = MarketingReportSearch::calcKeyIndicatorData($cost, $goalTotal, 'divide'); // Стоимость лида (одной точки)
$tableData = [
    'x' => array_column(array_slice($ctr, -8), 'x'),
    'indicator' => [
        'ctr' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'CTR',
            'Конверсия в клик',
            ' %',
            $ctr,
            $precision = 1
        ),
        'clicks' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'Клики',
            'Количество',
            ' ',
            $clicks
        ),
        'cpc' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'CPC',
            'Стоимость клика',
            ' ₽',
            $cpc
        ),
        'conversion' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'Конверсия',
            'в лид',
            ' %',
            $goalConversion,
            1
        ),
        'leads' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'Лиды',
            'Количество',
            ' ',
            $goalTotal
        ),
        'cpl' => MarketingReportSearch::_getIndicatorPopularChannel2(
            'CPL',
            'Стоимость лида',
            ' ₽',
            $goalCost,
            2
        ),
    ],
];

$chartLabelsX = [];
foreach ($tableData['x'] as $date) {
    $dateArr = explode('.', $date);
    $chartLabelsX[] = $dateArr[0] . ' ' . RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $date,
    ]);
}

?>
<div id="<?= $id ?>" class="graph_block" style="overflow: auto">
    <div class="graph_info" style="padding-bottom:0">
        <div class="graph_header_block">
            <div class="graph_header" style="margin-bottom:11px">
                Ключевые показатели
            </div>
        </div>
        <div style="min-width: 275px;">
            <table class="table table-style table-count-list form-group popular_table marketing-key-indicators-table" style="table-layout: unset">
                <thead>
                <tr>
                    <th width="35%" class="align-bottom">Показатель</th>
                    <th width="5%" class="align-bottom" style="min-width: 14px!important;"></th>
                    <th width="25%" class="align-bottom text-right">Вчера</th>
                    <th width="35%" class="align-bottom text-right">
                        <div class="nowrap" style="overflow: hidden;text-overflow: ellipsis;">
                            Среднее <span style="text-transform: none; ">за 7 дн.</span>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($tableData['indicator'] as $key => $data): ?>
                    <tr>
                        <td>
                            <div class="pt-name"><?= $data['name'] ?></div>
                            <div class="pt-subname nowrap"><?= $data['subname'] ?></div>
                        </td>
                        <td style="padding:4px 0!important; text-align: left!important; vertical-align: top!important;">
                            <?php if ($data['yesterday']['value'] != $data['average']): ?>
                                <?php
                                $arrowDirection = ($data['yesterday']['value'] > $data['average']) ? 'up' : 'down';
                                $arrowColor = ($key === 'cpc' || 'cpl' === $key)
                                    ? ($arrowDirection === 'up' ? 'red' : 'green')
                                    : ($arrowDirection === 'up' ? 'green' : 'red');
                                ?>
                                <img class="abc-arrow" style="width:14px;" src="/img/marketing/arrow_<?= $arrowDirection ?>_<?= $arrowColor ?>.svg"/>
                                
                            <?php endif; ?>
                        </td>
                        <td>
                            <span class="pt-name one-line"><?= $data['yesterday']['value'] . $data['unit_name'] ?></span>
                        </td>
                        <td>
                            <span class="pt-name one-line"><?= $data['average'] . $data['unit_name'] ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php if (isset($isAjax)) { return; } ?>

<script>
    $(window).on('load', function () {
        $('.key-indicators-nav').tabdrop();
    });

    var chartLabelsX2 = <?= json_encode($chartLabelsX) ?>;
</script>
