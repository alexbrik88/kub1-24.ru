<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\models\employee\Employee;
use common\modules\marketing\models\MarketingReportGroup;
use frontend\components\Icon;
use frontend\components\PageSize;
use common\modules\marketing\models\MarketingReport;
use common\components\TextHelper;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\modules\analytics\modules\marketing\widgets\SummarySelectWidget;
use Yii;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\TableViewWidget;
use frontend\widgets\TableConfigWidget;
use yii\web\View;

/**
 * @var View $this
 * @var MarketingReportSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var ActiveDataProvider $totalDataProvider
 * @var string $defaultSorting
 */

/** @var Employee $user */
$user = Yii::$app->user->identity;
$userConfig = $user->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_yandex_direct_by_campaign');
$googleAnalyticsProfile = $user->company->googleAnalyticsProfile;
$googleAnalyticsGoals = $googleAnalyticsProfile ? $googleAnalyticsProfile->goals : [];
$goalsColumns = MarketingReportSearch::getGoalColumns();

$allReports = $totalDataProvider->getModels();
$models = $dataProvider->getModels();
?>

<div class="wrap wrap_padding_none mb-0">
    <div class="custom-scroll-table">
        <table class="table table-style table-count-list invoice-table compact-disallow mb-2 up-table marketing-analysis-table">
            <thead>
            <tr role="row">
                <th rowspan="2" class="text-left" style="min-width: 200px; font-weight: 600;">Период</th>
                <th rowspan="2">Расход, ₽</th>
                <th rowspan="2">Показы</th>
                <th rowspan="2">Клики</th>
                <th rowspan="2">CTR, %</th>
                <th rowspan="2">Ср. цена <br/> клика, ₽</th>
                <?php foreach ($googleAnalyticsGoals as $goal): ?>
                    <th style="font-weight: 600;" colspan="3">
                        <button id="collapse-cell-order"
                                class="table-collapse-btn button-clr ml-1 active"
                                type="button"
                                data-collapse-trigger-top
                                data-target="cell-up-<?=($goal->id)?>"
                                data-colspan-close="2"
                                data-colspan-open="3">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap"><?= $goal->name ?></span>
                        </button>
                    </th>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach ($googleAnalyticsGoals as $goal): ?>
                    <?php foreach ($goalsColumns as $column): ?>
                        <th <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-up-<?=($goal->id)?>" style="font-weight: 600;">
                            <?= $column['name'] ?>
                        </th>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $searchModel->getStatisticPeriodName() ?></td>
                <td class="text-right"><?= TextHelper::numberFormat(MarketingReport::getTotal($allReports, 'cost') ?? 0) ?></td>
                <td class="text-right"><?= TextHelper::numberFormat( MarketingReport::getTotal($allReports, 'impressions') ?? 0) ?></td>
                <td class="text-right"><?= TextHelper::numberFormat(MarketingReport::getTotal($allReports, 'clicks') ?? 0) ?></td>
                <td class="text-right"><?= TextHelper::numberFormat(MarketingReport::getTotalCtr($allReports, 'clicks', 'impressions')) ?></td>
                <td class="text-right"><?= TextHelper::numberFormat(MarketingReport::getTotalCpc($allReports, 'cost', 'clicks')) ?></td>
                <?php foreach ($googleAnalyticsGoals as $goal): ?>
                    <?php foreach ($goalsColumns as $column): ?>
                        <td class="text-right" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-up-<?=($goal->id)?>">
                            <?= TextHelper::numberFormat($column['calculateValue']($allReports, "goal_{$goal->external_id}")) ?>
                        </td>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?= \yii\helpers\Html::beginForm(Url::current(), 'GET', [
    'id' => 'form_product_filters',
]);
?>
<div class="table-settings row row_indents_s" style="margin-top: 10px;">
    <div class="column">
        <?= \yii\helpers\Html::a(Icon::get('exel'), Url::current(['xls']), [
            'class' => 'get-xls-link button-regular button-regular_red button-clr w-44 mr-2',
            'title' => 'Скачать в Excel',
        ]); ?>

        <?php
        $sortingItems = [
            ['label' => 'Расход, ₽', 'attribute' => 'cost', 'checked' => $defaultSorting === 'cost'],
            ['label' => 'Клики', 'attribute' => 'clicks', 'checked' => $defaultSorting === 'clicks'],
            ['label' => 'CTR, %', 'attribute' => 'ctr', 'checked' => $defaultSorting === 'ctr'],
        ];
        if ($googleAnalyticsGoals) {
            foreach ($googleAnalyticsGoals as $goal) {
                foreach ($goalsColumns as $attribute => $column) {
                    if ($column['sort']) {
                        $sortingItems[] = ['label' => "$column[name] ({$goal->name})", 'attribute' => "goal_{$goal->external_id}_{$attribute}", 'checked' => $defaultSorting === "goal_{$goal->external_id}_{$attribute}"];
                    }
                }
            }
        }
        ?>

        <?= TableConfigWidget::widget([
            'sortingItemsTitle' => 'Сортировка по умолчанию',
            'sortingItems' => $sortingItems,
            'buttonClass' => 'button-list button-hover-transparent button-clr mr-2',
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_yandex_direct_by_campaign']) ?>
    </div>
    <div class="column ml-auto">
        <!-- Filters -->
        <div class="dropdown popup-dropdown popup-dropdown_filter products-filter">
            <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                <span class="button-txt">Фильтр</span>
                <svg class="svg-icon svg-icon-shevron">
                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                </svg>
            </button>
        </div>
    </div>
    <div class="col-4">
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group flex-grow-1 mr-2">
                <?= \yii\helpers\Html::activeTextInput($searchModel, 'campaignName', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control',
                    'value' => $searchModel->campaignName,
                ]) ?>
            </div>
            <div class="form-group">
                <?= \yii\helpers\Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
    </div>
</div>
<?= \yii\helpers\Html::endForm(); ?>

<div id="marketing-grid">

    <div class="wrap wrap_padding_none_all" style="position:relative!important;">
        <!-- TOP SCROLL -->
        <div id="cs-table-2" class="custom-scroll-table-double cs-top">
            <table style="width: 1010px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
        </div>
        <!-- FIXED COLUMN -->
        <div id="cs-table-first-column">
            <table class="table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>
        <!-- TABLE -->
        <div id="cs-table-1" class="custom-scroll-table-double">
            <div class="table-wrap">
                <table class="table table-style table-count-list mb-2 analysis-table marketing-analysis-table <?= $tabViewClass ?>">
                    <thead>
                    <tr role="row">
                        <th width="1%" rowspan="2">
                            <?= Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]) ?>
                        </th>
                        <th width="10%" rowspan="2" class="height-80">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Кампания', 'attr' => 'campaign_name', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Расход, ₽', 'attr' => 'cost', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Показы', 'attr' => 'impressions', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Клики', 'attr' => 'clicks', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'CTR, %', 'attr' => 'ctr', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Ср. цена <br/> клика, ₽', 'attr' => 'avg_cpc', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <?php foreach ($googleAnalyticsGoals as $goal): ?>
                            <th colspan="3" class="height-40">
                                <button id="collapse-cell-order"
                                        class="table-collapse-btn button-clr ml-1 active"
                                        type="button"
                                        data-collapse-trigger-bottom
                                        data-target="cell-down-<?=($goal->id)?>"
                                        data-colspan-close="2"
                                        data-colspan-open="3">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey weight-700 ml-1 nowrap"><?= $goal->name ?></span>
                                </button>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <?php foreach ($googleAnalyticsGoals as $goal): ?>
                            <?php foreach ($goalsColumns as $attribute => $column): ?>
                                <?php $attributeName = "goal_{$goal->external_id}_{$attribute}"; ?>
                                <th class="height-40" width="10%" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-down-<?=($goal->id)?>" style="font-weight: 600;">
                                    <?= $this->render('partial/_sort_arrows', ['title' => $column['name'], 'attr' => $attributeName, 'defaultSorting' => $defaultSorting]) ?>
                                </th>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <?php if (empty($models)): ?>
                        <tbody>
                            <tr>
                                <td colspan="100">
                                    <?php if ($searchModel->campaignName): ?>
                                        По вашему запросу ничего не найдено. Измените запрос и попробуйте еще раз.
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                        <?php foreach ($models as $model): ?>
                            <?php $isGroup = $model['group_id'] ?? false ?>
                            <?php $floorKey = "first-floor-{$model['group_id']}"; ?>
                            <tr class="<?= ($isGroup) ? 'bold' : '' ?>">
                                <td class="pr-2">
                                    <?php if ($isGroup): ?>
                                        <button class="table-collapse-btn table-collapse-btn-ajax button-clr text-left mb-3" type="button" style="display: block"
                                                data-id="<?= $model['group_id'] ?>"
                                                data-collapse-row-trigger data-target="<?= $floorKey ?>">
                                            <span class="table-collapse-icon">&nbsp;</span>
                                        </button>
                                    <?php else: ?>
                                        <?= Html::checkbox("MarketingReport[]", false, [
                                            'class' => 'joint-operation-checkbox',
                                            'value' => $model['utm_campaign'],
                                            'data' => [
                                                'name' => htmlspecialchars($model['campaign_name'] ?: $model['utm_campaign']),
                                                'cost' => $model['cost'] ?: 0,
                                                'clicks' => $model['clicks'] ?: 0,
                                                'ctr' => $model['ctr'] ?: 0,
                                                'impressions' => $model['impressions'] ?: 0
                                            ]
                                        ]) ?>
                                    <?php endif; ?>
                                </td>
                                <td class="col-name">
                                    <?php if ($isGroup): ?>
                                        <?= $model['campaign_name'] ?: $model['utm_campaign']; ?>
                                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'cog']), [
                                            'class' => 'marketing-modal-btn text-grey mr-1 pointer',
                                            'data-url' => Url::to(['group-update', 'id' => $model['group_id']]),
                                            'data-title' => 'Изменить группу рекламных кампаний',
                                            'title' => 'Изменить группу',
                                            'style' => 'float:right'
                                        ]) ?>
                                    <?php else: ?>
                                        <?= $model['campaign_name'] ?: '---' ?> <br/>
                                        <span style='font-size: 12px; color: #9198a0;'><?= $model['utm_campaign'] ?></span>
                                    <?php endif; ?>
                                </td>
                                <td class="text-right">
                                    <?= TextHelper::numberFormat($model['cost'] ?: 0) ?>
                                </td>
                                <td class="text-right">
                                    <?= TextHelper::numberFormat($model['impressions'] ?: 0) ?>
                                </td>
                                <td class="text-right">
                                    <?= TextHelper::numberFormat($model['clicks'] ?: 0) ?>
                                </td>
                                <td class="text-right">
                                    <?= TextHelper::numberFormat($model['ctr'] ?: 0) ?>
                                </td>
                                <td class="text-right">
                                    <?= TextHelper::numberFormat($model['avg_cpc'] ?: 0) ?>
                                </td>
                                <?php foreach ($googleAnalyticsGoals as $goal): ?>
                                    <?php foreach ($goalsColumns as $attribute => $column): ?>
                                        <?php $attributeName = "goal_{$goal->external_id}_{$attribute}"; ?>
                                        <td class="text-right" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-down-<?=($goal->id)?>">
                                            <?= TextHelper::numberFormat($model[$attributeName] ?: 0) ?>
                                        </td>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>

    <div class="table-settings-view row align-items-center">
        <div class="col-8">
            <nav>
                <?= \common\components\grid\KubLinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                ]) ?>
            </nav>
        </div>
        <div class="col-4">
            <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
                'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
                'pageSizeParam' => 'per-page',
            ]) ?>
        </div>
    </div>

</div>

<?= SummarySelectWidget::widget([
    'buttons' => [
        Html::button($this->render('//svg-sprite', ['ico' => 'archiev']).' <span>Сгруппировать</span>', [
            'class' => 'marketing-modal-btn button-clr button-regular button-hover-transparent',
            'data-url' => Url::to(['group-create']),
            'data-title' => 'Добавить группу рекламных кампаний',
            'data-new-group' => '1',
        ]),
    ],
]); ?>

<!------------------------------- MODAL -------------------------------------------------------------->

<?= $this->render('group-modal', [
    'dataProvider' => $dataProvider
]) ?>

<?php
$this->registerJs('

    // TOGGLE CELLS IN TOP TABLE
    $("[data-collapse-trigger-top]").click(function() {

        var collapseBtn = this;

        var _collapseToggle = function(collapseBtn) {
            var target = $(collapseBtn).data("target");
            var collapseCount = $(collapseBtn).hasClass("active") ? $(collapseBtn).data("colspan-close") : $(collapseBtn).data("colspan-open");

            $(collapseBtn).toggleClass("active").closest("th").attr("colspan", collapseCount);
            $(".up-table").find(\'[data-id="\'+target+\'"][data-collapse-cell]\').toggleClass("d-none");
        };

        _collapseToggle(collapseBtn);

        $(".custom-scroll-table").mCustomScrollbar("update");
    });
    
    // SEARCH
    $("input#yandexdirectcampaignreportsearch-campaignname").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $(this).closest("form").first().submit();
      }
    });

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }


    $("#form_product_filters").find(".filter-item").on("click", function(e) {
        e.preventDefault();
        var pop =  $(this).parents(".popup-dropdown_filter");
        var drop = $(this).parents(".dropdown-drop");
        var value = $(this).data("id");
        $(drop).find("input").val(value);
        $(drop).find(".drop-title").html($(this).text());
        $(drop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
        
        if ($(this).hasClass("filter-by-date")) {
            changePeriod($("#productsearch-filterdate").val(), "filter-date?actionType=set"); 
        }
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");
            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

');

$this->registerJs(<<<JS

    _activeMCS = 0;
    _offsetMCSLeft = 0;
    _firstCSTableColumn = $('#cs-table-first-column');

    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS === 1)
                    window._activeMCS = 0;

                if (this.mcs.left === 0 && $(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS === 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $("[data-collapse-trigger-bottom]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 240);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).hasClass('active') ? $(collapseBtn).data('colspan-close') : $(collapseBtn).data('colspan-open');

            $(collapseBtn).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $('.analysis-table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
        };

        _collapseToggle(collapseBtn);

        $("#cs-table-1").mCustomScrollbar("update");
    });

    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width());
        $("#cs-table-2").mCustomScrollbar("update");
    });
JS
);

if (!empty($models)) {

    $this->registerJs(<<<JS

    var ProductAnalysisTable = {
      fillFixedColumn: function() {

          var tableBlock = $('#cs-table-1');
          var columnBlock = $('#cs-table-first-column');
          // CLEAR
          $(columnBlock).find('thead').html('');
          $(columnBlock).find('tbody').html('');
          $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());

          // CLONE FIRST COLUMN
          $('.analysis-table thead tr > th:first-child').each(function(i,v) {
              var col = $(v).clone();
              $('.table-fixed-first-column thead').append($('<tr/>').append(col));

              return false;
          });
          $('.analysis-table tbody tr > td:first-child').each(function(i,v) {
              var col = $(v).clone();
              var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
              var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
              $(col).find('.sortable-row-icon').html('');
              $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
          });

          // ADD "+" EVENTS
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-1').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });
          $('#cs-table-1 [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-first-column').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });

          // main.js
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $(this).toggleClass('active');
              if ( $(this).hasClass('active') ) {
                  $('[data-id="'+target+'"]').removeClass('d-none');
              } else {
                  // level 1
                  $('[data-id="'+target+'"]').addClass('d-none');
                  $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
              }
              if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                  $('[data-collapse-all-trigger]').removeClass('active');
              } else {
                  $('[data-collapse-all-trigger]').addClass('active');
              }
		  });
          $('#cs-table-first-column [data-collapse-all-trigger]').click(function() {
              var _this = $(this);
              var table = $(this).closest('.table');
              var row = table.find('tr[data-id]');
              _this.toggleClass('active');
              if ( _this.hasClass('active') ) {
                  row.removeClass('d-none');
                  $(table).find('tbody .table-collapse-btn').addClass('active');
                  // row.find('.table-collapse-btn').addClass('active');
              } else {
                  row.addClass('d-none');
                  $(table).find('tbody .table-collapse-btn').removeClass('active');
              }
          });
          // end main.js

        // ADD "+" EVENT TOGGLE ALL
        function tableFixedColumnToggleAll(syncTable, trigger) {
            var row = $(syncTable).find('tr[data-id]');
            console.log(syncTable, trigger)
            if ( $(trigger).hasClass('active') ) {
                row.removeClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').removeClass('active');
            }
        }
        $('#cs-table-first-column [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-1');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });
        $('#cs-table-1 [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-first-column');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });

      }
    };

    // FIXED FIRST COLUMN
    $(document).ready(function() {
        ProductAnalysisTable.fillFixedColumn();
    });
    
    // SEARCH
    $("input#marketingreportsearch-campaignname").on("keydown", function(e) {
        if(e.keyCode === 13) {
            e.preventDefault();
            $(this).closest("form").first().submit();
        }
    });    
JS
    );
}