<?php
$channelType = $channelType ?? -1;
?>
<div class="row">
    <div class="col-8">
        <?= $this->render('chart_21', [
            'company' => $company,
            'channelType' => $channelType,
            'marketingUserConfig' => $marketingUserConfig
        ]) ?>
    </div>
    <div class="col-4">
        <?= $this->render('chart_22', [
            'company' => $company,
            'channelType' => $channelType,
            'marketingUserConfig' => $marketingUserConfig
        ]) ?>
    </div>
</div>

<style>
    #chart_21 { height: 275px; }
    #chart_21 .highcharts-axis-labels { z-index: -1!important; }
    #chart_21, #chart_13.highcharts-container, #chart_21 svg {overflow: visible!important; z-index: 1!important; }
</style>