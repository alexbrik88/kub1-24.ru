<?php
use common\modules\marketing\models\MarketingReport;
use frontend\themes\kub\components\Icon;
use common\components\grid\GridView;
use yii\data\SqlDataProvider;
use common\models\Company;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\db\Query;
use yii\widgets\Pjax;

/** @var Company $company */
/** @var int $channelType */

if ($sortParams = $dataProvider->sort->getAttributeOrders()) {
    $_sortKey = array_key_first($sortParams);
    $_sortVal = $sortParams[$_sortKey];
    $jsSortParam = ($_sortVal == SORT_DESC) ? "-{$_sortKey}" : "{$_sortKey}";
} else {
    $jsSortParam = null;
}
?>

<!-- main modal -->
<?php Modal::begin([
    'id' => 'marketing-modal-box',
    'closeButton' => ['label' => Icon::get('close'), 'class' => 'modal-close close'],
]); ?>
    <h4 id="marketing-modal-title" class="modal-title"></h4>
    <div id="marketing-modal-content"></div>
<?php Modal::end(); ?>

<!-- remove group modal -->
<?php Modal::begin([
    'id' => 'marketing-modal-remove-group',
    'title' => 'Вы уверены, что хотите удалить группу?',
    'options' => ['class' => 'confirm-modal fade'],
    'closeButton' => ['label' => Icon::get('close'), 'class' => 'modal-close close'],
    'headerOptions' => ['class' => 'text-center'],
]); ?>
<div class="text-center">
    <?= Html::button('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', [
        'class' => 'ladda-button yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
        'data-style' => 'expand-right',
    ]); ?>
    <button class="no button-clr button-regular button-hover-transparent button-width-medium ml-1">Нет</button>
</div>
<?php Modal::end(); ?>

<!-- remove item modal -->
<?php Modal::begin([
    'id' => 'marketing-modal-remove-item',
    'title' => 'Вы уверены, что хотите удалить кампанию из группы?',
    'options' => ['class' => 'confirm-modal fade', 'data-id' => null],
    'closeButton' => ['label' => Icon::get('close'), 'class' => 'modal-close close'],
    'headerOptions' => ['class' => 'text-center'],
]); ?>
    <div class="text-center">
        <?= Html::a('Да', null, [
            'class' => 'yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
        ]); ?>
        <button class="no button-clr button-regular button-hover-transparent button-width-medium ml-1">Нет</button>
    </div>
<?php Modal::end(); ?>

<!-- add item modal -->
<?php Modal::begin([
    'id' => 'marketing-modal-add-item',
    'closeButton' => ['label' => Icon::get('close'), 'class' => 'modal-close close'],
]); ?>
<h4 class="modal-title">Добавить рекламные кампании в группу</h4>
<div>
    <?php Pjax::begin([
        'id' => 'pjax_marketing_modal_add_item',
        'enableReplaceState' => false,
        'enablePushState' => false,
        'scrollTo' => false,
        'timeout' => 10E3,
    ]) ?>

    <?php Pjax::end() ?>
</div>
<?php Modal::end(); ?>


<script>
    MarketingGroup = {
        // page
        formSelector: '#marketing-group-form',
        tableSelector: '.marketing-analysis-table',
        linkSelector: '.marketing-modal-btn',
        // modal
        modalSelector: '#marketing-modal-box',
        modalItemsSelector: '#marketing-group-items',
        // submodals
        modalAddItem: '#marketing-modal-add-item',
        modalRemoveItem: '#marketing-modal-remove-item',
        modalRemoveGroup: '#marketing-modal-remove-group',
        // channel url part
        channelUrlPart: '<?= Yii::$app->controller->id ?>',
        init: function() {
            if (this.checkSelectors()) {
                this.bindEvents();
                this.bindTableEvents();
            }
        },
        bindEvents: function() {
            const that = this;

            // show modal
            $(document).on('click', that.linkSelector, function(e){
                e.preventDefault();
                const ajaxModal = $(that.modalSelector);
                const title = $(this).data('title') || $(this).attr('title');
                const url = $(this).data('url') || $(this).attr('href');
                const isNew = $(this).data('new-group') || false;

                $('#marketing-modal-title', ajaxModal).html(title);
                $('#marketing-modal-content', ajaxModal).load(url, function() {
                    that._appendTableItems(isNew);
                });

                ajaxModal.modal('show');
            });

            // hide modal
            $(document).on('hidden.bs.modal', that.modalSelector, function() {
                $('#marketing-modal-title', this).html('');
                $('#marketing-modal-content', this).html('');
                $('.modal-dialog', this).attr('class', 'modal-dialog');
            });

            // show "item add" modal
            $(document).on('click', '.marketing-group-add-item', function(e) {
                const pjaxOptions = {
                    'url': '/analytics/marketing/' + that.channelUrlPart + '/group-get-campaigns-list',
                    'container': '#pjax_marketing_modal_add_item',
                    'push': false,
                    'replace': false,
                    'timeout': 10E3
                };
                $.pjax.reload(pjaxOptions);
            });
            
            // pjax "item add" modal real query
            $(document).on('pjax:beforeSend', '#pjax_marketing_modal_add_item', function (event, xhr, settings) {

                // prevent infinite recursion
                if (settings.data)
                    return true;

                let exclude = [];
                $(that.modalItemsSelector).find('.item-input').each(function(i,input) {
                    exclude.push(input.value);
                });

                $.pjax({
                    type: 'POST',
                    url: settings.url,
                    data: {'exclude': exclude},
                    container: '#pjax_marketing_modal_add_item',
                    timeout: 10E3,
                    push: false,
                    scrollTo: false
                }).done(function() {
                    $(that.modalAddItem).modal('show');
                });

                return false;
            });

            // show "item remove" modal
            $(document).on('click', '.marketing-group-remove-item', function(e) {
                const id = $(this).closest('.marketing-group-item').data('id');
                if (id) {
                    $(that.modalRemoveItem)
                        .data('id', id)
                        .modal('show');
                }
            });

            // btn add items
            $(document).on('click', that.modalAddItem + ' .btn-add-items', function(e) {
                $(that.modalAddItem).find('.joint-operation-add-item:checked').each(function(i,ch) {
                    let id = $(ch).val();
                    let name = $(ch).data('name');
                    that._appendItem(id, name);
                });
                $(that.modalAddItem).modal('hide');
            });

            // btn remove item (no)
            $(document).on('click', that.modalRemoveItem + ' .no', function (e) {
                $(this).closest('.modal').modal('hide');
            });

            // btn remove item (yes)
            $(document).on('click', that.modalRemoveItem + ' .yes', function (e) {
                const id = $(that.modalRemoveItem).data('id');
                if (id) {
                    $(that.modalItemsSelector)
                        .find('.marketing-group-item')
                        .filter(function() { return $(this).data('id') == id })
                        .remove();
                }
                $(this).closest('.modal').modal('hide');
            });

            // btn remove group (no)
            $(document).on('click', that.modalRemoveGroup + ' .no', function (e) {
                $(this).closest('.modal').modal('hide');
            });

            // btn remove item (yes)
            $(document).on('click', that.modalRemoveGroup + ' .yes', function (e) {
                const id = $(that.formSelector).data('group_id');
                location.href = '/analytics/marketing/' + that.channelUrlPart + '/group-delete' + '?id=' + id;
            });

            // campaign-list items checkboxes
            $(document).on('change', that.modalAddItem + ' .joint-operation-add-item', function() {
                const checkboxes = $(that.modalAddItem + ' .joint-operation-add-item');
                const mainCheckbox = $(that.modalAddItem + ' .joint-operation-add-item-main');
                if (checkboxes.length === checkboxes.filter(':checked').length) {
                    mainCheckbox.prop('checked', true).uniform('refresh');
                } else {
                    mainCheckbox.prop('checked', false).uniform('refresh');
                }
            });
            // campaign-list items main checkbox
            $(document).on('change', that.modalAddItem + ' .joint-operation-add-item-main', function() {
                if ($(this).is(':checked')) {
                    $(that.modalAddItem + ' .joint-operation-add-item:not(:checked)').prop('checked', true).uniform('refresh');
                } else {
                    $(that.modalAddItem + ' .joint-operation-add-item:checked').prop('checked', false).uniform('refresh');
                }
                $('.joint-operation-checkbox').first().trigger('change');
            });
        },
        bindTableEvents: function() {
            const that = this;

            // preload group items
            $(document).on('click', '.table-collapse-btn-ajax', function() {

                const btn = this;
                const tr = $(btn).closest('tr');

                if ($(btn).hasClass('loaded'))
                    return true;

                const groupItem = $(btn).data('id');
                const sortParam = '<?= $jsSortParam ?>';

                $(btn).prop('disabled', true);
                $.get('/analytics/marketing/' + that.channelUrlPart + '/index-items/?group_id=' + $(btn).data('id') + '&sort=' + sortParam, function(html) {
                    $(btn).addClass('loaded').prop('disabled', false);
                    if (html) {
                        $(tr).after(html);
                    }
                });
            });
        },
        _appendTableItems: function(isNew) {

            if (!isNew)
                return;

            const that = this;
            $(that.tableSelector).find('.joint-operation-checkbox:checked').filter(':not(.has-group)').each(function (i, ch) {
                let id = $(ch).val();
                let name = $(ch).data('name');
                that._appendItem(id, name);
            });
        },
        _appendItem: function(id, name) {
            const that = this;
            const abstractTemplate = $(that.modalItemsSelector).find('.template');
            const template = abstractTemplate.clone();

            $(template)
                .removeClass('template')
                .find('.item-input').val(id).end()
                .find('.item-name').html(name).end()
                .data('id', id)
                .show();

            $(template).appendTo(that.modalItemsSelector);
        },
        checkSelectors: function() {
            let notFound = 0;
            if (!$(this.linkSelector).length)
                console.log('Selector "linkSelector" not found', notFound++);
            if (!$(this.modalSelector).length)
                console.log('Selector "modalSelector" not found ', notFound++);
            if (!$(this.tableSelector).length)
                console.log('Selector "tableSelector" not found ', notFound++);

            return !notFound;
        },
    };

    MarketingGroup.init();

</script>
