<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\models\employee\Employee;
use common\modules\marketing\models\MarketingReportGroup;
use frontend\components\Icon;
use frontend\components\PageSize;
use common\modules\marketing\models\MarketingReport;
use common\components\TextHelper;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\modules\analytics\modules\marketing\widgets\SummarySelectWidget;
use Yii;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\TableViewWidget;
use frontend\widgets\TableConfigWidget;
use yii\web\View;

/**
 * @var View $this
 * @var MarketingReportSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var ActiveDataProvider $totalDataProvider
 * @var string $defaultSorting
 */

/** @var Employee $user */
$user = Yii::$app->user->identity;
$userConfig = $user->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_yandex_direct_by_campaign');
$googleAnalyticsProfile = $user->company->googleAnalyticsProfile;
$googleAnalyticsGoals = $googleAnalyticsProfile ? $googleAnalyticsProfile->goals : [];
$goalsColumns = MarketingReportSearch::getGoalColumns();

$models = $dataProvider->getModels();

?>

<?php foreach ($models as $model): ?>
    <?php $isGroup = $model['group_id'] ?? false ?>
    <?php $floorKey = "first-floor-{$model['group_id']}"; ?>
    <tr class="<?= ($isGroup) ? 'bold' : '' ?>" data-id="<?= $floorKey ?>">
        <td class="pr-2">
            <?php if ($isGroup): ?>
            <?php else: ?>
                <?= Html::checkbox("MarketingReport[]", false, [
                    'class' => 'joint-operation-checkbox has-group',
                    'value' => $model['utm_campaign'],
                    'data' => [
                        'name' => htmlspecialchars($model['campaign_name'] ?: $model['utm_campaign']),
                        'cost' => $model['cost'] ?: 0,
                        'clicks' => $model['clicks'] ?: 0,
                        'ctr' => $model['ctr'] ?: 0,
                        'impressions' => $model['impressions'] ?: 0
                    ]
                ]) ?>
            <?php endif; ?>
        </td>
        <td class="col-name">
            <?php if ($isGroup): ?>
                <button class="table-collapse-btn table-collapse-btn-ajax button-clr ml-1 text-left" type="button"
                    data-id="<?= $model['group_id'] ?>"
                    data-collapse-row-trigger data-target="<?= $floorKey ?>">
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="d-block weight-700 text_size_14 ml-1"><?= $model['campaign_name'] ?: $model['utm_campaign']; ?></span>
                </button>
                <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'cog']), [
                    'class' => 'marketing-modal-btn text-grey ml-1 pointer',
                    'data-url' => Url::to(['group-update', 'id' => $model['group_id']]),
                    'data-title' => 'Изменить группу рекламных кампаний',
                    'title' => 'Изменить группу'
                ]) ?>
            <?php else: ?>
                <?= $model['campaign_name'] ?: '---' ?> <br/>
                <span style='font-size: 12px; color: #9198a0;'><?= $model['utm_campaign'] ?></span>
            <?php endif; ?>
        </td>
        <td class="text-right">
            <?= TextHelper::numberFormat($model['cost'] ?: 0) ?>
        </td>
        <td class="text-right">
            <?= TextHelper::numberFormat($model['impressions'] ?: 0) ?>
        </td>
        <td class="text-right">
            <?= TextHelper::numberFormat($model['clicks'] ?: 0) ?>
        </td>
        <td class="text-right">
            <?= TextHelper::numberFormat($model['ctr'] ?: 0) ?>
        </td>
        <td class="text-right">
            <?= TextHelper::numberFormat($model['avg_cpc'] ?: 0) ?>
        </td>
        <?php foreach ($googleAnalyticsGoals as $goal): ?>
            <?php foreach ($goalsColumns as $attribute => $column): ?>
                <?php $attributeName = "goal_{$goal->external_id}_{$attribute}"; ?>
                <td class="text-right" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-down-<?=($goal->id)?>">
                    <?= TextHelper::numberFormat($model[$attributeName] ?: 0) ?>
                </td>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tr>
<?php endforeach; ?>