<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

/** @var $id */
/** @var $jsModel */
/** @var $xAxis */
/** @var $yAxis */

$jsModel = $jsModel ?? 'UnknownChart';
$id = $id ?? strtolower($jsModel);

$color = '#cf134b';
$color2 = 'rgba(51,90,130,1)';
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
    <?=($jsModel)?>.data = <?= json_encode([
        'series' => [
            ['data' => $yAxis['curr']],
            ['data' => $yAxis['prev']],
        ],
        'xAxis' => [
            'categories' => $xAxis['x']
        ]
    ]) ?>;
</script>

<?php if (isset($isAjax)) { return; } ?>

<?= $this->render('../_arrows', ['chartId' => $id, 'offsetStep' => 1]) ?>

<?= Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'line'
            ],
            'title' => [
                'text' => $title ?? $id
            ],
            'series' => [
                [
                    'name' => 'Конверсия',
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'data' => $yAxis['curr'],
                ],
                [
                    'type' => 'scatter',
                    'name' => 'Неделя до',
                    'color' => $color2,
                    'data' => $yAxis['prev'],
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel, ['units' => '%', 'precision' => 1])
            ],
            'plotOptions' => [
                'scatter' => [
                    'marker' => [
                        'symbol' => 'c-rect',
                        'lineWidth' => 3,
                        'lineColor' => $color2,
                        'radius' => 8.5
                    ],
                ]
            ],
        ]
    ], DC::CHART_COLUMN)
);
?>