<?php

/**
 * NOT USED since 18.10.2021!
 */

namespace frontend\modules\analytics\modules\marketing\views;

use common\components\grid\GridView;
use common\models\employee\Employee;
use frontend\modules\analytics\modules\marketing\models\Integration;
use frontend\modules\analytics\modules\marketing\models\IntegrationRepository;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;

/**
 * @var View $this
 * @var IntegrationRepository $repository
 * @var Employee $employee
 */

$dataProvider = $repository->getDataProvider();
$models = $dataProvider->getModels();
$tabViewClass = $employee->config->getTableViewClass('table_view_marketing');
$format = function ($value): string {
    return number_format($value, is_float($value) ? 2 : 0, ',', ' ');
};

$summary = function (string $column) use ($format, $models) : string {
    $values = ArrayHelper::getColumn($models, $column);
    $sum = array_sum($values);

    return $format($sum);
};

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'showFooter' => true,
    'tableOptions' => [
        'class' => "table table-style table-count-list invoice-table {$tabViewClass}",
    ],
    'rowOptions' => [
        'class' => 'nowrap',
    ],
    'footerRowOptions' => [
        'class' => 'font-weight-bold',
    ],
    'layout' => <<<HTML
        <div class="wrap wrap_padding_none">
            <div class="table-wrap">
                {items}
            </div>
        </div>
HTML,
    'formatter' => [
        'class' => Formatter::class,
        'nullDisplay' => '',
    ],
    'columns' => [
        [
            'attribute' => 'name',
            'label' => 'Рекламный канал',
            'footer' => 'Итого',
            'format' => 'raw',
            'value' => function(Integration $integration): string {
                $url = Url::to(['/analytics/marketing/' . $integration->getChannel()->getControllerId()]);

                return Html::a($integration->getName(), $url);
            }
        ],
        [
            'attribute' => 'isActive',
            'label' => 'Статус',
            'format' => 'raw',
            'enableSorting' => false,
            'value' => function(Integration $integration): string {
                return $this->render('integration-status-column', ['integration' => $integration]);
            }
        ],
        [
            'attribute' => 'balance',
            'label' => 'Баланс (руб.)',
            'format' => $format,
            'footer' => $summary('balance'),
        ],
        [
            'attribute' => 'totalImpressions',
            'label' => 'Показы',
            'format' => $format,
            'footer' => $summary('totalImpressions'),
        ],
        [
            'attribute' => 'totalClicks',
            'label' => 'Клики',
            'format' => $format,
            'footer' => $summary('totalClicks'),
        ],

        [
            'attribute' => 'totalCtr',
            'label' => 'CTR (%)',
            'format' => $format,
            'footer' => $summary('totalCtr'),
        ],
        [
            'attribute' => 'totalCost',
            'label' => 'Расход (руб.)',
            'format' => $format,
            'footer' => $summary('totalCost'),
        ],
    ],
]) ?>
