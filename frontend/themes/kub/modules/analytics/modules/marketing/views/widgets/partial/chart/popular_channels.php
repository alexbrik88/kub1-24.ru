<?php

use common\components\helpers\Month;
use yii\bootstrap\Nav;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use Carbon\Carbon;
use php_rutils\RUtils;
use yii\helpers\Url;

/**
 * @var array $graphData
 * @var bool $byMonth
 */

$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';

$daysPeriods = [];
$chartLabelsX = [];
$chartFreeDays = [];

foreach ($graphData['x'] as $date) {

    $carbonDate = Carbon::createFromFormat('d.m.Y', $date);

    if ($byMonth) {
        $month = $carbonDate->format('n');
        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $carbonDate->format('Y');
    } else {

        $day = $carbonDate->format('d');
        $daysPeriods[] = $carbonDate->format('j');
        $chartFreeDays[] = (in_array(date('w', strtotime($date)), [0, 6]));
        $chartLabelsX[] = $day . ' ' . RUtils::dt()->ruStrFTime([
            'format' => 'F',
            'monthInflected' => true,
            'date' => $date,
        ]);

    }
}

$series = [];
foreach ($graphData['indicator'] as $key => $data) {
    $series[] = [
        'name' => $data['name'],
        'data' => $data['y'],
        'color' => $data['color'],
        'marker' => [
            'symbol' => 'circle',
            'enabled' => false
        ],
        'showInLegend' => false,
        'visible' => ($key == 'ctr')
    ];
}

$htmlTooltip = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th class="text-left" colspan="3">{title}</th>
        </tr>
        <tr>
            <td>{itemName}</td>
            <td></td>
            <td><b>{itemData}</b></td>
        </tr>
    </table>
HTML;

$htmlTooltip = str_replace(["\r", "\n", "'"], "", $htmlTooltip);
?>
<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header">
                Динамика показателей
            </div>
            <div class="nav-tabs-row">
                <?= Nav::widget([
                    'id' => 'graph_period_nav',
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_none w-100'],
                    'items' => [
                        [
                            'label' => 'День',
                            'url' => Url::current(['by_month' => null]),
                            'active' => !$byMonth,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding-left: 0; padding-right: 9px;',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link' . (!$byMonth ? ' active' : ''),
                            ]
                        ],
                        [
                            'label' => 'Месяц',
                            'url' => Url::current(['by_month' => 1]),
                            'active' => $byMonth,
                            'options' => [
                                'class' => 'nav-item',
                                'style' => 'padding: 0',
                            ],
                            'linkOptions' => [
                                'class' => 'nav-link' . ($byMonth ? ' active' : ''),
                            ]
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="nav-tabs-row">
            <?= Nav::widget([
                'id' => 'graph_statistics_channels_nav',
                'encodeLabels' => false,
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100'],
                'items' => [
                    [
                        'label' => 'CTR',
                        'url' => 'javascript:void(0)',
                        'active' => true,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link active', 'data-serie' => '0']
                    ],
                    [
                        'label' => 'Клики',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '1']
                    ],
                    [
                        'label' => 'CPC',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '2']
                    ],
                    [
                        'label' => 'Конверсия',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '3']
                    ],
                    [
                        'label' => 'Лиды',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '4']
                    ],
                    [
                        'label' => 'CPL',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '5']
                    ],
                    [
                        'label' => 'Расходы',
                        'url' => 'javascript:void(0)',
                        'active' => false,
                        'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                        'linkOptions' => ['class' => 'nav-link', 'data-serie' => '6']
                    ],
                ],
            ]) ?>
        </div>
        <?= Highcharts::widget([
            'id' => 'chart-channels-statistics',
            'scripts' => [
                'modules/exporting',
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'title' => false,
                'credits' => [
                    'enabled' => false
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'line',
                    'marginLeft' => '55',
                    'marginTop' => '25',
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'height' => 405,
                    'reflow' => true
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0'
                    ],
                    'itemDistance' => 10
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                        
                            let index = this.series.data.indexOf( this.point );
                            let data = Highcharts.numberFormat(this.y, 1, ',', ' ');

                            return '{$htmlTooltip}'
                                .replace(\"{title}\", window.chartLabelsX[index])
                                .replace(\"{itemName}\", this.series.name)
                                .replace(\"{itemData}\", data);
                        }
                    ")
                ],
                'yAxis' => [
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
                'xAxis' => [
                    'min' => 0,
                    'categories' => $daysPeriods,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chartCurrDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                            (window.chartFreeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                        'useHTML' => true,
                    ],
                ],
                'series' => $series,
            ],
        ]) ?>
    </div>
</div>
<script>
    var chartCurrDayPos = 1;
    var chartLabelsX = <?= json_encode($chartLabelsX) ?>;
    var chartFreeDays = <?= json_encode($chartFreeDays) ?>;

    $('#graph_statistics_channels_nav a').on('click', function(e) {

        e.preventDefault();

        const $nav = $('#graph_statistics_channels_nav');
        const $chart = $('#chart-channels-statistics').highcharts();
        const serieId = $(this).data('serie');

        $nav.find('li.active, a.active').removeClass('active');
        $(this).addClass('active').parent().addClass('active');

        $.each($chart.series, function(i, serie) {
            if (i == serieId)
                serie.update({visible: true});
            else
                serie.update({visible: false});
        });

    });
</script>
