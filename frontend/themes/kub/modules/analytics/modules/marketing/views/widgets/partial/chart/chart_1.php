<?php

use common\modules\marketing\models\MarketingReportSearch;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\helpers\ArrayHelper;

$leftOffset = 6;
$rightOffset = 0;
$channelType = $channelType ?? -1;
$customOffset = $customOffset ?? 0;
$customCampaign = $customCampaign ?? null;

$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$currWeekPeriods = ArrayHelper::getValue($xAxis, 'datePeriods');
$prevWeekPeriods = ArrayHelper::getValue(DC::getXAxisDays($leftOffset, $rightOffset, $customOffset - 7), 'datePeriods');
$yAxis = MarketingReportSearch::getChartDataByDays($company->id, $channelType, $currWeekPeriods, $prevWeekPeriods, $customCampaign);

$chartsViews = $marketingUserConfig->getTopChartViews();
$chartsOptions = $marketingUserConfig->getTopChartOptions($xAxis, $yAxis);
?>

<div class="row">
    <div class="col-4">
        <div class="kub-chart border">
            <?= $this->render($chartsViews[1], $chartsOptions[1] + ['isAjax' => $isAjax ?? null]) ?>
        </div>
    </div>
    <div class="col-4">
        <div class="kub-chart border">
            <?= $this->render($chartsViews[2], $chartsOptions[2] + ['isAjax' => $isAjax ?? null]) ?>
        </div>
    </div>
    <div class="col-4">
        <div class="kub-chart border">
            <?= $this->render($chartsViews[3], $chartsOptions[3] + ['isAjax' => $isAjax ?? null]) ?>
        </div>
    </div>
</div>

<?php if (isset($isAjax)) { return; } ?>

<div id="chart-transmitter">
    <!-- js updated data -->
</div>

<style>
    #chart_11,
    #chart_12,
    #chart_13 { height: 210px; }
    #chart_11 .highcharts-axis-labels,
    #chart_12 .highcharts-axis-labels,
    #chart_13 .highcharts-axis-labels { z-index: -1!important; }
    #chart_11, #chart_11.highcharts-container, #chart_11 svg,
    #chart_12, #chart_12.highcharts-container, #chart_12 svg,
    #chart_13, #chart_13.highcharts-container, #chart_13 svg {overflow: visible!important; z-index: 1!important; }
</style>