<?php

namespace frontend\modules\analytics\modules\marketing\views;

use Yii;
use yii\helpers\Url;
use common\components\helpers\Html;
use common\models\employee\Employee;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use frontend\widgets\RangeButtonWidget;
use common\modules\import\models\ImportJobData;
use frontend\themes\kub\widgets\ImportDialogWidget;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingUserConfig;
use frontend\modules\integration\models\bitrix24\Company;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\modules\analytics\modules\marketing\models\IntegrationRepository;

/**
 * @var array $balanceIntegrationsData
 * @var Employee $employee
 * @var IntegrationRepository $repository
 * @var ImportJobData $jobData
 * @var $integrations
 * @var Company $company
 * @var MarketingReportSearch $searchModel
 * @var string $defaultSorting
 */

$this->title = 'Дашборд';

$employee = Yii::$app->user->identity;
$userConfig = $employee->config;
$marketingUserConfig = MarketingUserConfig::findOne(['employee_id' => $employee->id]);
if (!$marketingUserConfig) {
    $marketingUserConfig = new MarketingUserConfig(['employee_id' => $employee->id]);
    $marketingUserConfig->setDefaults();
}

$showHelpPanel = $employee->config->marketing_help ?? false;
$showChartPanel = $employee->config->marketing_chart ?? false;
$showChannels = [
    MarketingChannel::YANDEX_AD => 'Яндекс.Директ',
    MarketingChannel::GOOGLE_AD => 'Google.Ads',
    MarketingChannel::FACEBOOK => 'Facebook',
    MarketingChannel::VKONTAKTE => 'Вконтакте'
];
?>

<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/tabs', [
    'controller' => Yii::$app->controller,
]) ?>

    <div class="wrap pt-1 pb-1 pl-4 pr-3" style="margin-bottom: 12px">
        <div class="pt-1 pb-1">
            <div class="row align-items-center">
                <?php $showed = []; ?>
                <?php foreach ($integrations as $i): ?>
                    <?php if (in_array($i['channel'] ?? null, array_keys($showChannels)) && !in_array($i['channel'], $showed)): ?>
                        <div class="column">
                            <img src="<?= $i['logo'] ?>" width="<?= $i['logoWidth'] ?>" class="<?= $i['status'] ? '' : 'channel-not-active' ?>"/>
                        </div>
                        <?php $showed[] = $i['channel'] ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="ml-auto column text-right align-self-center pt-1" style="margin-bottom: 7px;">
                    <?php if ($jobData): ?>
                        <span>Данные загружены <?= $jobData->getCreatedAt()->format('d.m.Y в H:i') ?></span>
                    <?php endif; ?>
                </div>
                <div class="ml-auto col-3 pl-1 pr-2">
                    <?= Html::button(Icon::get('add-icon') . '<span>Загрузить данные</span>', [
                        'class' => 'button-regular button-regular_red button-width ml-auto w-100 import-dialog',
                        'data-url' => Url::to(['aggregate-import']),
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom:12px">
        <div class="pt-1 pb-1">
            <div class="row align-items-center pb-1">
                <div class="column pr-2 ml-auto">
                    <?= Html::button(Icon::get('diagram'), [
                        'class' => 'button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : null),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
                </div>
                <div class="column pl-1 pr-2">
                    <?= Html::button(Icon::get('book'), [
                        'class' => 'button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : null),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
                </div>
                <div class="col-3 pl-1 pr-2 d-flex flex-column justify-content-top">
                    <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="jsSaveStateCollapse collapse <?= $showChartPanel ? 'show' : null ?>" id="chartCollapse" data-attribute="marketing_chart">
        <?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/partial/_header_charts', [
            'company' => $company,
            'channelType' => MarketingChannel::DASHBOARD_TOTALS,
            'marketingUserConfig' => $marketingUserConfig
        ]) ?>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 collapse <?= $showHelpPanel ? 'show' : null ?>"
         id="helpCollapse" data-attribute="marketing_help" style="margin-bottom: 12px">
        <div class="pt-4 pb-3">
            Описание
        </div>
    </div>

    <div class="table-settings row row_indents_s" style="margin-top: 10px;">
        <div class="col-12">
            <?= TableViewWidget::widget(['attribute' => 'table_view_marketing']) ?>
        </div>
    </div>

<?= $this->render('../widgets/dashboard-reports-grid', [
    'totalDataProvider' => null,
    'searchModel' => $searchModel,
    'defaultSorting' => $defaultSorting,
    'channelsList' => $showChannels,
]) ?>

<?php /* $this->render('partial/integration-grid', [
    'repository' => $repository,
    'employee' => $employee,
]) */ ?>

<?= ImportDialogWidget::widget() ?>

<?php $this->registerJs('
    $(document).ready(function(){
        $("#chartCollapse").on("show.bs.collapse", function() {
            $("#helpCollapse").collapse("hide");
        });
        $("#helpCollapse").on("show.bs.collapse", function() {
            $("#chartCollapse").collapse("hide");
        });
    });
');
