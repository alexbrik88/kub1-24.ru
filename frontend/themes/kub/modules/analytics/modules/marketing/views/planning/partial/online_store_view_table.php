<?php
/* @var $this yii\web\View
 * @var $model \common\models\marketing\MarketingPlan
 * @var $user \common\models\employee\Employee
 * @var $activeTab int
 */

use Carbon\Carbon;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\marketing\MarketingPlanItem;
use frontend\modules\analytics\models\AbstractFinance;

$quarters = $model->getQuarters();
$floorMap = Yii::$app->request->post('floorMap', []);
$_cellIds = $_dMonthes = [];
$q = 0;
$now = date('Ym');
$startDate = Carbon::createFromFormat('m.Y', $model->start_date)->format('Ym');
$endDate = Carbon::createFromFormat('m.Y', $model->end_date)->format('Ym');
if ($now < $startDate) {
    $_dMonthes[min(array_keys($quarters))] = 1;
}

if ($now > $endDate) {
    $_dMonthes[max(array_keys($quarters))] = 1;
}

foreach ($quarters as $quarter => $monthes) {
    $q++;
    $_cellIds[$quarter] = 'cell-' . $q;
    if (!isset($_dMonthes[$quarter])) {
        $_dMonthes[$quarter] = 0;
    }

    if (in_array($now, $monthes)) {
        $_dMonthes[$quarter] = 1;
    }
}

/** @var MarketingPlanItem[] $marketingPlanItems */
$marketingPlanItems = $model->getMarketingPlanItems()
    ->andWhere(['channel' => $activeTab])
    ->indexBy(fn(MarketingPlanItem $marketingPlanItem): string => "{$marketingPlanItem->year}{$marketingPlanItem->month}")
    ->all();
$itemsCount = count($marketingPlanItems) ?: 1;

$totalBudgetAmount = (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('budget_amount');
$totalRegistrationCount = (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('registration_count');
$totalActiveUsersCount = (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('active_users_count');
$totalPurchasesCount = (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('purchases_count');
$totalTotalAdditionalExpensesAmount = (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('total_additional_expenses_amount');

$roundDecimals = $user->config->report_marketing_planning_round_numbers;
$amountDecimals = $roundDecimals ? 0 : 2;

$currencySymbol = $model->getCurrencySymbol();
?>
<table class="scrollable-table double-scrollbar-top table table-style table-count-list table-compact analysis-table border-top my-1">
    <thead>
    <tr>
        <th class="fixed-column" rowspan="2" style="min-width: 280px;">
            Воронка продаж
        </th>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <th class="align-top" <?= $_dMonthes[$quarter] ? 'colspan="'.count($monthes).'"' : '' ?> data-collapse-cell-title data-id="<?= $_cellIds[$quarter] ?>" data-quarter="<?=($quarter)?>">
                <button class="table-collapse-btn button-clr ml-1 <?= $_dMonthes[$quarter] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="<?= $_cellIds[$quarter] ?>" data-columns-count="<?=(count($monthes))?>">
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap"><?= substr($quarter, 4, 2) ?> кв <?= substr($quarter, 0, 4); ?></span>
                </button>
            </th>
        <?php endforeach; ?>
        <th rowspan="2" class="nowrap">
            Итог
        </th>
    </tr>
    <tr>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php foreach ($monthes as $month): ?>
                <th class="<?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <div class="pl-1 pr-1">
                        <?= ArrayHelper::getValue(AbstractFinance::$month, substr($month, 4, 2), $month); ?>
                    </div>
                </th>
            <?php endforeach; ?>
            <th class="<?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1">Итого</div>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <tr data-attr="budget_amount">
        <td class="fixed-column text_size_14">
            <button class="table-collapse-btn button-clr" type="button" data-collapse-row-trigger data-target="floor-1">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="text_size_14 bold ml-1" style="color: #212529;">Рекламный бюджет, <?= $currencySymbol ?></span>
            </button>
        </td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->budget_amount;
                ?>
                <td class="text-right month-block bold <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->budget_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right bold">
            <?= TextHelper::invoiceMoneyFormat($totalBudgetAmount, $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="click_amount" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Стоимость клика, <?= $currencySymbol ?></td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->click_amount;
                if ((int)$marketingPlanItem->click_amount > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->click_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)), $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(
                round((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('click_amount') / ($nonEmptyItemsCount ?: 1)),
                $roundDecimals
            ) ?>
        </td>
    </tr>
    <tr data-attr="clicks_count_per_day" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Клики в день, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $clicksCount = round((int)$marketingPlanItem->clicks_count / 30);
                $quarterSum += $clicksCount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat($clicksCount) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat(round((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('clicks_count') / 30)) ?>
        </td>
    </tr>
    <tr data-attr="clicks_count" class="d-none" data-id="floor-1">
        <td class="fixed-column bold text_size_14" style="padding-left: 40px;">Клики в месяц, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->clicks_count;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem->clicks_count) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('clicks_count')) ?>
        </td>
    </tr>
    <tr data-attr="registration_conversion" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Конверсия в корзину, %</td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (float)$marketingPlanItem->registration_conversion;
                if ((float)$marketingPlanItem->registration_conversion > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((float)$marketingPlanItem->registration_conversion * 100, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)) * 100, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(round((float)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('registration_conversion') / ($nonEmptyItemsCount ?: 1)) * 100, $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="registration_count_per_day" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Товар в корзине в день, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $registrationsCount = round((int)$marketingPlanItem->registration_count / 30);
                $quarterSum += $registrationsCount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat($registrationsCount) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::integerFormat(round($totalRegistrationCount / 30)) ?>
        </td>
    </tr>
    <tr data-attr="registration_count" class="d-none" data-id="floor-1">
        <td class="fixed-column bold text_size_14" style="padding-left: 40px;">Товар в корзине в месяц, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->registration_count;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem->registration_count) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat($totalRegistrationCount) ?>
        </td>
    </tr>
    <tr data-attr="registration_amount" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Стоимость товара в корзине, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSumBudgetAmount = 0;
            $quarterSumRegistrationCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSumBudgetAmount += (int)$marketingPlanItem->budget_amount;
                $quarterSumRegistrationCount += (int)$marketingPlanItem->registration_count;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->registration_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1">
                    <?= TextHelper::invoiceMoneyFormat(
                        $quarterSumRegistrationCount ? round($quarterSumBudgetAmount / $quarterSumRegistrationCount) : 0,
                        $roundDecimals
                    ) ?>
                </div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?php if (isset($totalBudgetAmount)): ?>
                <?= $totalRegistrationCount ? TextHelper::invoiceMoneyFormat(round($totalBudgetAmount / $totalRegistrationCount), $roundDecimals) : ($roundDecimals ? 0 : '0,00') ?>
            <?php else: ?>
                <?= $roundDecimals ? 0 : '0,00' ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr data-attr="purchase_conversion" class="d-none" data-id="floor-1">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Конверсия в покупку, %</td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (float)$marketingPlanItem->purchase_conversion;
                if ((float)$marketingPlanItem->purchase_conversion > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((float)$marketingPlanItem->purchase_conversion * 100, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)) * 100, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(round((float)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('purchase_conversion') / ($nonEmptyItemsCount ?: 1)) * 100, $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="purchases_count">
        <td class="fixed-column bold text_size_14">Новые продажи, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->purchases_count;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem->purchases_count) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat($totalPurchasesCount) ?>
        </td>
    </tr>
    <tr data-attr="sale_amount">
        <td class="fixed-column bold text_size_14">Стоимость продажи, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSumBudgetAmount = 0;
            $quarterSumPurchasesCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSumBudgetAmount += (int)$marketingPlanItem->budget_amount;
                $quarterSumPurchasesCount += (int)$marketingPlanItem->purchases_count;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->sale_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1">
                    <?= TextHelper::invoiceMoneyFormat(
                        $quarterSumPurchasesCount ? round($quarterSumBudgetAmount / $quarterSumPurchasesCount) : 0,
                        $roundDecimals
                    ) ?>
                </div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?php if (isset($totalBudgetAmount)): ?>
                <?= $totalPurchasesCount ? TextHelper::invoiceMoneyFormat(round($totalBudgetAmount / $totalPurchasesCount), $roundDecimals) : ($roundDecimals ? 0 : '0,00') ?>
            <?php else: ?>
                <?= $roundDecimals ? 0 : '0,00' ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="fixed-column bold text_size_14">
            <button class="table-collapse-btn button-clr" type="button" data-collapse-row-trigger data-target="floor-2">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="text_size_14 bold ml-1" style="color: #212529;">Дополнительные затраты, <?= $currencySymbol ?></span>
            </button>
        </td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->total_additional_expenses_amount;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->total_additional_expenses_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block bold text-right">
            <?= TextHelper::invoiceMoneyFormat($totalTotalAdditionalExpensesAmount, $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="marketing_service_amount" class="d-none" data-id="floor-2">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Услуги маркетолога, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->marketing_service_amount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->marketing_service_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('marketing_service_amount'), $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="salesperson_salary_amount" class="d-none" data-id="floor-2">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">ЗП продавцов, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->salesperson_salary_amount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->salesperson_salary_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('salesperson_salary_amount'), $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="additional_expenses_amount" class="d-none" data-id="floor-2">
        <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">Прочие затраты, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->additional_expenses_amount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->additional_expenses_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('additional_expenses_amount'), $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="additional_expenses_amount_for_one_sale">
        <td class="fixed-column text_size_14 bold">Доп. затраты на 1 продажу, <?= $currencySymbol ?></td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSumTotalAdditionalExpensesAmount = 0;
            $quarterSumPurchasesCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSumTotalAdditionalExpensesAmount += (int)$marketingPlanItem->total_additional_expenses_amount;
                $quarterSumPurchasesCount += (int)$marketingPlanItem->purchases_count;
                ?>
                <td class="text-right month-block bold <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->additional_expenses_amount_for_one_sale, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1">
                    <?= TextHelper::invoiceMoneyFormat(
                        $quarterSumPurchasesCount ? round($quarterSumTotalAdditionalExpensesAmount / $quarterSumPurchasesCount) : 0,
                        $roundDecimals
                    ) ?>
                </div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right bold">
            <?php if (isset($totalTotalAdditionalExpensesAmount)): ?>
                <?= $totalPurchasesCount ? TextHelper::invoiceMoneyFormat(round($totalTotalAdditionalExpensesAmount / $totalPurchasesCount), $roundDecimals) : ($roundDecimals ? 0 : '0,00') ?>
            <?php else: ?>
                <?= $roundDecimals ? 0 : '0,00' ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="fixed-column bold text_size_14">Продажи</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php foreach ($monthes as $month): ?>
                <td class="month-block bold <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>"></td>
            <?php endforeach; ?>
            <td class="bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>"></td>
        <?php endforeach; ?>
        <td class="month-block text-right"></td>
    </tr>
    <tr>
        <td class="fixed-column bold text_size_14">Новые продажи, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->purchases_count;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem->purchases_count) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat($totalPurchasesCount) ?>
        </td>
    </tr>
    <tr data-attr="average_check">
        <td class="fixed-column text-grey text_size_14">Средний чек, <?= $currencySymbol ?></td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->average_check;
                if ((int)$marketingPlanItem->average_check > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->average_check, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)), $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(
                round((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('average_check') / ($nonEmptyItemsCount ?: 1)),
                $roundDecimals
            ) ?>
        </td>
    </tr>
    <tr data-attr="proceeds_from_new_amount">
        <td class="fixed-column text-grey text_size_14">Выручка с новых, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->proceeds_from_new_amount;
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->proceeds_from_new_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('proceeds_from_new_amount'), $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="financial_result">
        <td class="fixed-column bold text_size_14">Результат, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $quarterSum += (int)$marketingPlanItem->financial_result;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?> <?= (int)$marketingPlanItem->financial_result < 0 ? 'red-column' : null; ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->financial_result, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?= $quarterSum < 0 ? 'red-column' : null; ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right <?= (int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('financial_result') < 0 ? 'red-column' : null; ?>">
            <?= TextHelper::invoiceMoneyFormat((int)$model->getMarketingPlanItems()->andWhere(['channel' => $activeTab])->sum('financial_result'), $roundDecimals) ?>
        </td>
    </tr>
    <tr data-attr="cumulative_financial_amount">
        <td class="fixed-column bold text_size_14">Результат нарастающим, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $lastSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItems[$month];
                $lastSum = (int)$marketingPlanItem->cumulative_financial_amount;
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?> <?= (int)$marketingPlanItem->cumulative_financial_amount < 0 ? 'red-column' : null; ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem->cumulative_financial_amount, $roundDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?= $lastSum < 0 ? 'red-column' : null; ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($lastSum, $roundDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td></td>
    </tr>
    </tbody>
</table>
