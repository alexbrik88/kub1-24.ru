<?php
/**
 * @var $content string
 */

use frontend\modules\analytics\assets\FinanceModelAsset;

FinanceModelAsset::register($this);

$this->beginContent('@frontend/views/layouts/main.php');
echo $content;
$this->endContent();
