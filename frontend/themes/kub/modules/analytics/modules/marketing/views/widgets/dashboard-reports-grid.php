<?php

namespace frontend\modules\analytics\modules\marketing\views;

use common\models\employee\Employee;
use common\components\TextHelper;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingReportSearch;
use Yii;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\web\View;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\modules\marketing\widgets\SummarySelectWidget;

/**
 * @var View $this
 * @var MarketingReportSearch $searchModel
 * @var string $defaultSorting
 */

/** @var Employee $user */
$user = Yii::$app->user->identity;
$userConfig = $user->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_marketing');
$googleAnalyticsProfile = $user->company->googleAnalyticsProfile;
$googleAnalyticsGoals = $googleAnalyticsProfile ? $googleAnalyticsProfile->goals : [];
$goalsColumns = MarketingReportSearch::getGoalColumns();
$channelsList = $channelsList ?? [];
$channelsDetails = $searchModel->getQuery()->indexBy('channel')->all();

$balanceData = [
    MarketingChannel::YANDEX_AD => $user->company->getIntegrationData(Employee::INTEGRATION_YANDEX_DIRECT)->getBalance(),
    MarketingChannel::GOOGLE_AD => $user->company->getIntegrationData(Employee::INTEGRATION_GOOGLE_ADS)->getBalance(),
    MarketingChannel::FACEBOOK => $user->company->getIntegrationData(Employee::INTEGRATION_FACEBOOK)->getBalance(),
    MarketingChannel::VKONTAKTE => $user->company->getIntegrationData(Employee::INTEGRATION_VK)->getBalance()
];

// make array
$channelsArray = [];
foreach ($channelsList as $channelId => $channelName) {
    if (isset($channelsDetails[$channelId]) && isset($balanceData[$channelId])) {
        $channelsArray[] = $channelsDetails[$channelId] + ['name' => $channelName] + ['balance' => $balanceData[$channelId]];
    } else {
        $_emptyChannel = [
            'channel' => $channelId,
            'name' => $channelName,
            'balance' => 0,
            'cost' => 0,
            'impressions' => 0,
            'clicks' => 0,
            'ctr' => 0,
            'avg_cpc' => 0,
        ];

        foreach ($googleAnalyticsGoals as $goal) {
            foreach ($goalsColumns as $attribute => $column) {
                $attributeName = "goal_{$goal->external_id}_{$attribute}";
                $_emptyChannel[$attributeName] = 0;
            }
        }

        $channelsArray[] = $_emptyChannel;
    }
}

$dataProvider = new ArrayDataProvider([
    'allModels' => $channelsArray,
    'pagination' => ['pageSize' => 0],
    'sort' => [
        'attributes' => array_merge(['name', 'balance'], $searchModel->getSortAttributes()),
    ],
]);

?>

<div id="marketing-grid">

    <div class="wrap wrap_padding_none_all" style="position:relative!important;">
        <!-- TOP SCROLL -->
        <div id="cs-table-2" class="custom-scroll-table-double cs-top">
            <table style="width: 1010px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
        </div>
        <!-- FIXED COLUMN -->
        <div id="cs-table-first-column">
            <table class="table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>
        <!-- TABLE -->
        <div id="cs-table-1" class="custom-scroll-table-double">
            <div class="table-wrap">
                <table class="table table-style table-count-list mb-2 analysis-table marketing-analysis-table <?= $tabViewClass ?>">
                    <thead>
                    <tr role="row">
                        <th width="1%" rowspan="2">
                            <?= Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]) ?>
                        </th>
                        <th width="10%" rowspan="2" class="height-80">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Рекламный канал', 'attr' => 'name', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Баланс на сегодня', 'attr' => 'balance', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Расход, ₽', 'attr' => 'cost', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Показы', 'attr' => 'impressions', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Клики', 'attr' => 'clicks', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'CTR, %', 'attr' => 'ctr', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/_sort_arrows', ['title' => 'Ср. цена <br/> клика, ₽', 'attr' => 'avg_cpc', 'defaultSorting' => $defaultSorting]) ?>
                        </th>
                        <?php foreach ($googleAnalyticsGoals as $goal): ?>
                            <th colspan="3" class="height-40">
                                <button id="collapse-cell-order"
                                        class="table-collapse-btn button-clr ml-1 active"
                                        type="button"
                                        data-collapse-trigger-bottom
                                        data-target="cell-down-<?=($goal->id)?>"
                                        data-colspan-close="2"
                                        data-colspan-open="3">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey weight-700 ml-1 nowrap"><?= $goal->name ?></span>
                                </button>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <?php foreach ($googleAnalyticsGoals as $goal): ?>
                            <?php foreach ($goalsColumns as $attribute => $column): ?>
                                <?php $attributeName = "goal_{$goal->external_id}_{$attribute}"; ?>
                                <th class="height-40" width="10%" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-down-<?=($goal->id)?>" style="font-weight: 600;">
                                    <?= $this->render('partial/_sort_arrows', ['title' => $column['name'], 'attr' => $attributeName, 'defaultSorting' => $defaultSorting]) ?>
                                </th>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($dataProvider->getModels() as $model): ?>
                        <?php $channelId = $model['channel'] ?>
                        <?php $channelName = $model['name'] ?>
                        <tr>
                            <td class="pr-2">
                                <?= Html::checkbox("MarketingReport[]", false, [
                                    'class' => 'joint-operation-checkbox',
                                    'value' => $model['channel'],
                                    'data' => [
                                        'name' => htmlspecialchars($model['name']),
                                        'cost' => $model['cost'] ?: 0,
                                        'clicks' => $model['clicks'] ?: 0,
                                        'ctr' => $model['ctr'] ?: 0,
                                        'impressions' => $model['impressions'] ?: 0
                                    ]
                                ]) ?>
                            </td>
                            <td class="col-name">
                                <?= Html::a($channelName, Url::to(['/analytics/marketing/' . MarketingChannel::CONTROLLER_LIST[$channelId] ?? '#'])) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::moneyFormat(ArrayHelper::getValue($model, 'balance', 0)) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::numberFormat(ArrayHelper::getValue($model, 'cost', 0)) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::numberFormat(ArrayHelper::getValue($model, 'impressions', 0)) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::numberFormat(ArrayHelper::getValue($model, 'clicks', 0)) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::numberFormat(ArrayHelper::getValue($model, 'ctr', 0)) ?>
                            </td>
                            <td class="text-right">
                                <?= TextHelper::numberFormat(ArrayHelper::getValue($model, 'avg_cpc', 0)) ?>
                            </td>
                            <?php foreach ($googleAnalyticsGoals as $goal): ?>
                                <?php foreach ($goalsColumns as $attribute => $column): ?>
                                    <?php $attributeName = "goal_{$goal->external_id}_{$attribute}"; ?>
                                    <td class="text-right" <?= $column['toggle'] ? 'data-collapse-cell' : '' ?> data-id="cell-down-<?=($goal->id)?>">
                                        <?= TextHelper::numberFormat(ArrayHelper::getValue($model, $attributeName, 0)) ?>
                                    </td>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<?= SummarySelectWidget::widget(['buttons' => []]); ?>

<?php
$this->registerJs('

    // TOGGLE CELLS IN TOP TABLE
    $("[data-collapse-trigger-top]").click(function() {

        var collapseBtn = this;

        var _collapseToggle = function(collapseBtn) {
            var target = $(collapseBtn).data("target");
            var collapseCount = $(collapseBtn).hasClass("active") ? $(collapseBtn).data("colspan-close") : $(collapseBtn).data("colspan-open");

            $(collapseBtn).toggleClass("active").closest("th").attr("colspan", collapseCount);
            $(".up-table").find(\'[data-id="\'+target+\'"][data-collapse-cell]\').toggleClass("d-none");
        };

        _collapseToggle(collapseBtn);

        $(".custom-scroll-table").mCustomScrollbar("update");
    });
    
    // SEARCH
    $("input#yandexdirectcampaignreportsearch-campaignname").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $(this).closest("form").first().submit();
      }
    });

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }


    $("#form_product_filters").find(".filter-item").on("click", function(e) {
        e.preventDefault();
        var pop =  $(this).parents(".popup-dropdown_filter");
        var drop = $(this).parents(".dropdown-drop");
        var value = $(this).data("id");
        $(drop).find("input").val(value);
        $(drop).find(".drop-title").html($(this).text());
        $(drop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
        
        if ($(this).hasClass("filter-by-date")) {
            changePeriod($("#productsearch-filterdate").val(), "filter-date?actionType=set"); 
        }
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");
            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

');

$this->registerJs(<<<JS

    _activeMCS = 0;
    _offsetMCSLeft = 0;
    _firstCSTableColumn = $('#cs-table-first-column');

    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS === 1)
                    window._activeMCS = 0;

                if (this.mcs.left === 0 && $(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS === 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $("[data-collapse-trigger-bottom]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 240);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).hasClass('active') ? $(collapseBtn).data('colspan-close') : $(collapseBtn).data('colspan-open');

            $(collapseBtn).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $('.analysis-table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
        };

        _collapseToggle(collapseBtn);

        $("#cs-table-1").mCustomScrollbar("update");
    });

    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width());
        $("#cs-table-2").mCustomScrollbar("update");
    });
JS
);

if (!empty($models)) {

    $this->registerJs(<<<JS

    var ProductAnalysisTable = {
      fillFixedColumn: function() {

          var tableBlock = $('#cs-table-1');
          var columnBlock = $('#cs-table-first-column');
          // CLEAR
          $(columnBlock).find('thead').html('');
          $(columnBlock).find('tbody').html('');
          $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());

          // CLONE FIRST COLUMN
          $('.analysis-table thead tr > th:first-child').each(function(i,v) {
              var col = $(v).clone();
              $('.table-fixed-first-column thead').append($('<tr/>').append(col));

              return false;
          });
          $('.analysis-table tbody tr > td:first-child').each(function(i,v) {
              var col = $(v).clone();
              var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
              var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
              $(col).find('.sortable-row-icon').html('');
              $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
          });

          // ADD "+" EVENTS
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-1').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });
          $('#cs-table-1 [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-first-column').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });

          // main.js
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $(this).toggleClass('active');
              if ( $(this).hasClass('active') ) {
                  $('[data-id="'+target+'"]').removeClass('d-none');
              } else {
                  // level 1
                  $('[data-id="'+target+'"]').addClass('d-none');
                  $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
              }
              if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                  $('[data-collapse-all-trigger]').removeClass('active');
              } else {
                  $('[data-collapse-all-trigger]').addClass('active');
              }
		  });
          $('#cs-table-first-column [data-collapse-all-trigger]').click(function() {
              var _this = $(this);
              var table = $(this).closest('.table');
              var row = table.find('tr[data-id]');
              _this.toggleClass('active');
              if ( _this.hasClass('active') ) {
                  row.removeClass('d-none');
                  $(table).find('tbody .table-collapse-btn').addClass('active');
                  // row.find('.table-collapse-btn').addClass('active');
              } else {
                  row.addClass('d-none');
                  $(table).find('tbody .table-collapse-btn').removeClass('active');
              }
          });
          // end main.js

        // ADD "+" EVENT TOGGLE ALL
        function tableFixedColumnToggleAll(syncTable, trigger) {
            var row = $(syncTable).find('tr[data-id]');
            console.log(syncTable, trigger)
            if ( $(trigger).hasClass('active') ) {
                row.removeClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').removeClass('active');
            }
        }
        $('#cs-table-first-column [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-1');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });
        $('#cs-table-1 [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-first-column');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });

      }
    };

    // FIXED FIRST COLUMN
    $(document).ready(function() {
        ProductAnalysisTable.fillFixedColumn();
    });
    
    // SEARCH
    $("input#marketingreportsearch-campaignname").on("keydown", function(e) {
        if(e.keyCode === 13) {
            e.preventDefault();
            $(this).closest("form").first().submit();
        }
    });    
JS
    );
}