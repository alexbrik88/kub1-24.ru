<?php
/* @var $this yii\web\View
 * @var $marketingPlanItemsData array
 * @var $model \common\models\marketing\MarketingPlan
 * @var $user \common\models\employee\Employee
 */

use Carbon\Carbon;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\marketing\MarketingPlan;
use frontend\modules\analytics\models\AbstractFinance;

$quarters = $model->getQuarters();
$floorMap = Yii::$app->request->post('floorMap', []);
$_cellIds = $_dMonthes = [];
$q = 0;
$itemsCount = 0;
$now = date('Ym');
$startDate = Carbon::createFromFormat('m.Y', $model->start_date)->format('Ym');
$endDate = Carbon::createFromFormat('m.Y', $model->end_date)->format('Ym');
if ($now < $startDate) {
    $_dMonthes[min(array_keys($quarters))] = 1;
}

if ($now > $endDate) {
    $_dMonthes[max(array_keys($quarters))] = 1;
}

foreach ($quarters as $quarter => $monthes) {
    $q++;
    $_cellIds[$quarter] = 'cell-' . $q;
    $itemsCount += count($monthes);
    if (!isset($_dMonthes[$quarter])) {
        $_dMonthes[$quarter] = 0;
    }

    if (in_array($now, $monthes)) {
        $_dMonthes[$quarter] = 1;
    }
}

$roundDecimals = $user->config->report_marketing_planning_round_numbers;
$amountDecimals = $roundDecimals ? 0 : 2;

$currencySymbol = $model->getCurrencySymbol();

$itemsCount = $itemsCount > 0 ?: 1;
$totalBudgetAmount = 0;
$totalMarketingServiceAmount = 0;
$totalAdditionalExpensesAmount = 0;
$totalTotalAdditionalExpensesAmount = 0;
$totalAdditionalExpensesAmountForOne = 0;
$totalTotalExpensesAmount = 0;
$totalClicksCount = 0;
$totalClickAmount = 0;
$totalFinalCostsClickAmount = 0;
$totalRegistrationConversion = 0;
$totalRegistrationCount = 0;
$totalRegistrationAmount = 0;
$totalFinalCostsRegistrationAmount = 0;
$totalActiveUserConversion = 0;
$totalActiveUsersCount = 0;
$totalActiveUserAmount = 0;
$totalFinalCostsUserAmount = 0;
$totalPurchaseConversion = 0;
$totalPurchasesCount = 0;
$totalSaleAmount = 0;
$totalAverageCheck = 0;
$totalProceedsFromNewAmount = 0;
$totalChurnRate = 0;
$totalRepeatedPurchasesAmount = 0;
$totalTotalProceedsAmount = 0;
$totalFinancialResult = 0;
$totalRepeatedPurchasesCount = 0;
$totalTotalPurchasesCount = 0;
$totalSalespersonSalaryAmount = 0;
?>
<table class="scrollable-table double-scrollbar-top table table-style table-count-list table-compact analysis-table border-top my-1">
    <thead>
    <tr>
        <th class="fixed-column" rowspan="2" style="min-width: 280px;">
            Повторные продажи
        </th>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <th class="align-top" <?= $_dMonthes[$quarter] ? 'colspan="'.count($monthes).'"' : '' ?> data-collapse-cell-title data-id="<?= $_cellIds[$quarter] ?>" data-quarter="<?=($quarter)?>">
                <button class="table-collapse-btn button-clr ml-1 <?= $_dMonthes[$quarter] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="<?= $_cellIds[$quarter] ?>" data-columns-count="<?=(count($monthes))?>">
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap"><?= substr($quarter, 4, 2) ?> кв <?= substr($quarter, 0, 4); ?></span>
                </button>
            </th>
        <?php endforeach; ?>
        <th rowspan="2" class="nowrap">
            Итог
        </th>
    </tr>
    <tr>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php foreach ($monthes as $month): ?>
                <th class="<?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <div class="pl-1 pr-1">
                        <?= ArrayHelper::getValue(AbstractFinance::$month, substr($month, 4, 2), $month); ?>
                    </div>
                </th>
            <?php endforeach; ?>
            <th class="<?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1">Итого</div>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="fixed-column bold text_size_14">Продажи</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php foreach ($monthes as $month): ?>
                <td class="month-block bold <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>"></td>
            <?php endforeach; ?>
            <td class="<?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>"></td>
        <?php endforeach; ?>
        <td class="month-block bold text-right"></td>
    </tr>
    <tr>
        <td class="fixed-column text-grey text_size_14">
            <button class="table-collapse-btn button-clr" type="button" data-collapse-row-trigger data-target="floor-3">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="text_size_14 bold ml-1" style="color: #212529;">Новые продажи, шт</span>
            </button>
        </td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['purchases_count'];
                $totalPurchasesCount += (int)$marketingPlanItem['purchases_count'];
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem['purchases_count']) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::integerFormat($totalPurchasesCount) ?>
        </td>
    </tr>
    <?php foreach ($marketingPlanItemsData['purchases_count_by_channels'] as $channel => $purchasesCountByChannel): ?>
        <tr data-attr="purchases_count_channel_<?= $channel ?>" class="d-none" data-id="floor-3">
            <td class="fixed-column text-grey text_size_14" style="padding-left: 40px;">
                <?= MarketingPlan::$channelMap[$channel] ?>
            </td>
            <?php foreach ($quarters as $quarter => $monthes): ?>
                <?php $quarterSum = 0 ?>
                <?php foreach ($monthes as $month): ?>
                    <?php $quarterSum += (int)$purchasesCountByChannel[$month]; ?>
                    <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                        <?= TextHelper::integerFormat((int)$purchasesCountByChannel[$month]) ?>
                    </td>
                <?php endforeach; ?>
                <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                    <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum) ?></div>
                </td>
            <?php endforeach; ?>
            <td class="month-block text-right">
                <?= TextHelper::integerFormat($purchasesCountByChannel['total']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr data-attr="average_check">
        <td class="fixed-column text-grey text_size_14">Средний чек, <?= $currencySymbol ?></td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['average_check'];
                $totalAverageCheck += (int)$marketingPlanItem['average_check'];
                if ((int)$marketingPlanItem['average_check'] > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['average_check'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)), $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(round($totalAverageCheck / ($nonEmptyItemsCount ?: 1)), $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="proceeds_from_new_amount">
        <td class="fixed-column text-grey text_size_14">Выручка с новых, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['proceeds_from_new_amount'];
                $totalProceedsFromNewAmount += (int)$marketingPlanItem['proceeds_from_new_amount'];
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['proceeds_from_new_amount'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat($totalProceedsFromNewAmount, $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="churn_rate">
        <td class="fixed-column text-grey text_size_14">Отток клиентов (Churn rate), %</td>
        <?php $nonEmptyItemsCount = 0 ?>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php
            $quarterSum = 0;
            $monthsCount = 0;
            ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (float)$marketingPlanItem['churn_rate'];
                $totalChurnRate += (float)$marketingPlanItem['churn_rate'];
                if ((float)$marketingPlanItem['churn_rate'] > 0) {
                    $nonEmptyItemsCount++;
                    $monthsCount++;
                }
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((float)$marketingPlanItem['churn_rate'] * 100, $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(round($quarterSum / ($monthsCount ?: 1)) * 100, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat(round($totalChurnRate / ($nonEmptyItemsCount ?: 1)) * 100, $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="repeated_purchases_count">
        <td class="fixed-column text-grey text_size_14">Повторные продажи, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['repeated_purchases_count'];
                $totalRepeatedPurchasesCount += (int)$marketingPlanItem['repeated_purchases_count'];
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem['repeated_purchases_count'], 2) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum, 2) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::integerFormat($totalRepeatedPurchasesCount, 2) ?>
        </td>
    </tr>
    <tr data-attr="repeated_purchases_amount">
        <td class="fixed-column text-grey text_size_14">Выручка с повторных, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['repeated_purchases_amount'];
                $totalRepeatedPurchasesAmount += (int)$marketingPlanItem['repeated_purchases_amount'];
                ?>
                <td class="text-right month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['repeated_purchases_amount'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block text-right">
            <?= TextHelper::invoiceMoneyFormat($totalRepeatedPurchasesAmount, $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="total_purchases_count">
        <td class="fixed-column bold text-left text_size_14">Итого продажи, шт</td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['total_purchases_count'];
                $totalTotalPurchasesCount += (int)$marketingPlanItem['total_purchases_count'];
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::integerFormat((int)$marketingPlanItem['total_purchases_count'], 2) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::integerFormat($quarterSum, 2) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="month-block bold text-right">
            <?= TextHelper::integerFormat($totalTotalPurchasesCount, 2) ?>
        </td>
    </tr>
    <tr data-attr="total_proceeds_amount">
        <td class="fixed-column bold text_size_14">Выручка итого, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['total_proceeds_amount'];
                $totalTotalProceedsAmount += (int)$marketingPlanItem['total_proceeds_amount'];
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['total_proceeds_amount'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right">
            <?= TextHelper::invoiceMoneyFormat($totalTotalProceedsAmount, $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="financial_result">
        <td class="fixed-column bold text_size_14">Результат, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $quarterSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $quarterSum += (int)$marketingPlanItem['financial_result'];
                $totalFinancialResult += (int)$marketingPlanItem['financial_result'];
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?> <?= (int)$marketingPlanItem['financial_result'] < 0 ? 'red-column' : null; ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['financial_result'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?= $quarterSum < 0 ? 'red-column' : null; ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($quarterSum, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td class="bold month-block text-right <?= $totalFinancialResult < 0 ? 'red-column' : null; ?>">
            <?= TextHelper::invoiceMoneyFormat($totalFinancialResult, $amountDecimals) ?>
        </td>
    </tr>
    <tr data-attr="cumulative_financial_amount">
        <td class="fixed-column bold text_size_14">Результат нарастающим, <?= $currencySymbol ?></td>
        <?php foreach ($quarters as $quarter => $monthes): ?>
            <?php $lastSum = 0 ?>
            <?php foreach ($monthes as $month): ?>
                <?php
                $marketingPlanItem = $marketingPlanItemsData[$month];
                $lastSum = (int)$marketingPlanItem['cumulative_financial_amount'];
                ?>
                <td class="text-right bold month-block <?= $_dMonthes[$quarter] ? '' : 'd-none' ?> <?= (int)$marketingPlanItem['cumulative_financial_amount'] < 0 ? 'red-column' : null; ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                    <?= TextHelper::invoiceMoneyFormat((int)$marketingPlanItem['cumulative_financial_amount'], $amountDecimals) ?>
                </td>
            <?php endforeach; ?>
            <td class="text-right bold <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?= $lastSum < 0 ? 'red-column' : null; ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat($lastSum, $amountDecimals) ?></div>
            </td>
        <?php endforeach; ?>
        <td></td>
    </tr>
    </tbody>
</table>

