<?php

use frontend\modules\analytics\modules\marketing\models\VkAdsSettingsForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use common\components\helpers\Html;

/**
 * @var VkAdsSettingsForm $form
 */

?>
<?php Pjax::begin(['id' => 'upload-vk-ads-pjax', 'enablePushState' => false, 'linkSelector' => false]) ?>
    <div class="statement-service-content" style="position: relative; min-height: 110px;">
        <?php $widget = ActiveForm::begin([
            'id' => 'upload-vk-ads-form',
            'enableClientValidation' => false,
            'action' => ['connect'],
            'options' => [
                'data-pjax' => true,
            ],
        ]) ?>

        <div class="row">
            <?= $widget->field($form, 'account_id', [
                'options' => ['class' => 'form-group col-6'],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}"
            ])->widget(Select2::class, [
                'hideSearch' => true,
                'data' => $form->getAccountList(),
                'options' => ['placeholder' => 'Выберите рекламный аккаунт'],
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>

        <div class="d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Подключить интеграцию</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width:220px!important;',
            ]); ?>
            <?= Html::button('Отмена', [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'data-dismiss' => 'modal',
                'title' => 'Отмена',
                'style' => 'width: 150px;',
            ]); ?>
        </div>

        <?php ActiveForm::end() ?>
    </div>
<?php Pjax::end() ?>
