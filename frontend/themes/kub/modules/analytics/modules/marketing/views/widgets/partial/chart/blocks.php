<?php

/* @var array $data
 * @var array $graphData
 * @var array $balanceIntegrationsData
 */

$totalBalanceAmount = 0;
$blockColor = [
    [
        'main' => '#66C3A0',
        'gradient' => ['#67C3A0', 'rgba(103, 195, 160, 0.15)'],
    ],
    [
        'main' => '#4E9FB0',
        'gradient' => ['#4E9FB0', 'rgba(78, 159, 176, 0.15)'],
    ],
    [
        'main' => '#F3B73B',
        'gradient' => ['#F3B73B', 'rgba(243, 183, 59, 0.15)'],
    ],
    [
        'main' => '#416D93',
        'gradient' => ['#416D93', 'rgba(65, 109, 147, 0.15)'],
    ],
];

?>
<div class="marketing-graph-block col-6 col-xl-3">
    <?= $this->render('graph_block', [
        'title' => 'Клики',
        'color' => $blockColor[0],
        'data' => $blockData1,
        'graphData' => $blockGraphData1
    ]) ?>
</div>
<div class="marketing-graph-block col-6 col-xl-3">
    <?= $this->render('graph_block', [
        'title' => 'Лиды',
        'color' => $blockColor[1],
        'data' => $blockData2,
        'graphData' => $blockGraphData2
    ]) ?>
</div>
<div class="graph_block graph_info marketing-balance-amount-block col-6 col-xl-6">
    <table>
        <tr>
            <th width="50%">
                БАЛАНС на СЕГОДНЯ
            </th>
            <th width="25%">

            </th>
            <th class="text-right" width="25%">
                ПРОГНОЗ
            </th>
        </tr>
        <?php if (!empty($balanceHistories)): ?>
            <?php foreach ($balanceIntegrationsData as $balanceHistory): ?>
                <?php $totalBalanceAmount += $balanceHistory['amount']; ?>
                <tr>
                    <td>
                        <?= $balanceHistory['name'] ?>
                    </td>
                    <td class="text-center">
                        <?= $balanceHistory['amount'] ?>
                    </td>
                    <td class="text-right">
                        <?= "{$balanceHistory['balanceLeftDays']} дн." ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr class="total-row">
                <td>Итого</td>
                <td class="text-center"><?= $totalBalanceAmount ?></td>
                <td></td>
            </tr>
        <?php else: ?>
            <tr class="text-center">
                <td style="font-size: 12px;padding-top: 60px;" colspan="3">Нет подключенных интеграций с возможностью получения
                    баланса
                </td>
            </tr>
        <?php endif; ?>
    </table>
</div>
