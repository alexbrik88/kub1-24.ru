<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.03.2020
 * Time: 15:17
 */

use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use yii\bootstrap\Collapse;
use Carbon\Carbon;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \frontend\models\googleAds\GetReportForm $model
 * @var \common\models\jobs\Jobs[] $lastAutoJobs
 */

$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';
$arrowIco = '<svg class="arrow-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#arrow"></use></svg>';
?>
<?php Pjax::begin([
    'id' => 'upload-vk-ads-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<div class="statement-service-content" style="position: relative; min-height: 110px;">
    <?= $this->render('partial/modal_header') ?>

    <?php $form = ActiveForm::begin([
        'id' => 'upload-vk-ads-form',
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'action' => ['import'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]) ?>
        <div class="row">
            <?= $form->field($model, 'dateFrom', [
                'options' => [
                    'class' => 'form-group col-6',
                ],
                'wrapperOptions' => [
                    'class' => 'form-filter date-picker-wrap',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'class' => 'form-control date-picker',
                'value' => DateHelper::format($model->dateFrom, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>

            <?= $form->field($model, 'dateTo', [
                'options' => [
                    'class' => 'form-group col-6',
                ],
                'wrapperOptions' => [
                    'class' => 'form-filter date-picker-wrap',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'class' => 'form-control date-picker',
                'value' => DateHelper::format($model->dateTo, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>
        <div class="d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Отправить запрос</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width: 170px;',
            ]); ?>
            <?= Html::button('Отмена', [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'data-dismiss' => 'modal',
                'title' => 'Отмена',
                'style' => 'width: 150px;',
            ]); ?>
        </div>
    <?php ActiveForm::end() ?>

    <br />

    <?= Html::a('Отключить интеграцию с "VK"', Url::to('/analytics/marketing/vk-ads/disconnect')) ?>
</div>
<?php Pjax::end() ?>

