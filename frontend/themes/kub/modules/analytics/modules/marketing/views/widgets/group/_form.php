<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use frontend\themes\kub\components\Icon;

/** @var \common\modules\marketing\models\MarketingReportGroup $model */
?>

<?php $form = ActiveForm::begin([
    'id' => 'marketing-group-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' =>  Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-isNewRecord' => (int) $model->isNewRecord,
        'data-group_id' => (int) $model->id
    ],
]); ?>

<div class="row">
    <div class="col-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="mb-2">
            <strong>Рекламные кампании в группе:</strong>
        </div>
        <div id="marketing-group-items">
            <?php if (!$model->isNewRecord): ?>
                <?php foreach ($model->items as $item): ?>
                    <div class="marketing-group-item mb-2" data-id="<?= $item['utm_campaign'] ?>">
                        <div class="d-flex">
                            <input type="hidden" class="item-input" name="marketingGroupItems[]" value="<?= $item['utm_campaign'] ?>" />
                            <div class="item-name"><?= $item['campaign_name'] ?></div>
                            <div class="item-actions">
                                <?= Html::tag('span', Icon::get('circle-close'), ['class' => 'marketing-group-remove-item grey-link ml-2', 'title' => 'Удалить из группы']) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="marketing-group-item mb-2 template" data-id="<?php // js ?>" style="display: none">
                <div class="d-flex">
                    <input type="hidden" class="item-input" name="marketingGroupItems[]" value="<?php // js ?>" />
                    <div class="item-name"><?php // js ?></div>
                    <div class="item-actions">
                        <?= Html::tag('span', Icon::get('circle-close'), ['class' => 'marketing-group-remove-item grey-link ml-2', 'title' => 'Удалить из группы']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-2 mb-3">
            <?= $form->field($model, 'itemsErrors', ['options' => ['class' => ''], 'template' => '{input}{error}'])->hiddenInput() ?>
        </div>
        <div>
            <?= Html::button(Icon::get('add-icon') . '<span class="ml-2">Добавить</span>', ['class' => 'marketing-group-add-item button-regular button-hover-content-red', 'title' => 'Добавить кампании']) ?>
        </div>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'ladda-button button-regular button-regular_red button-width button-clr',
        'data-style' => 'expand-right',
    ]); ?>
    <?php if (!$model->isNewRecord): ?>
        <?= Html::button('Разгруппировать', [
            'class' => 'marketing-group-remove button-regular button-hover-transparent button-width button-clr',
            'data-toggle' => 'modal',
            'data-target' => '#marketing-modal-remove-group'
        ]); ?>
    <?php endif; ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>
