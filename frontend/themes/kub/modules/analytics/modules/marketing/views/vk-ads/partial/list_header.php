<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.04.2020
 * Time: 0:41
 */

use common\components\TextHelper;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\RangeButtonWidget;

/* @var $budgetAmount int */
?>
<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h3 class="page-title yd-title">
                    <img src="/images/vk_logo.png" width="40"/>
                </h3>
            </div>
<!--            <div class="column pr-2">-->
<!--                <h4>-->
<!--                    Оставшийся бюджет: <span>--><?//= TextHelper::numberFormat($budgetAmount) ?><!-- ₽</span>-->
<!--                </h4>-->
<!--            </div>-->
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('diagram'), [
                    'class' => 'button-list button-hover-transparent button-clr mb-2',
                    'data-toggle' => 'collapse',
                    'href' => '#chartCollapse',
                ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'), [
                    'class' => 'button-list button-hover-transparent button-clr mb-2',
                    'data-toggle' => 'collapse',
                    'href' => '#helpCollapse',
                ]) ?>
            </div>
            <div class="col-6 col-xl-3 pl-1 pr-2 d-flex flex-column justify-content-top" style="margin-top:-9px">
                <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row']) ?>
            </div>
        </div>
    </div>
</div>
