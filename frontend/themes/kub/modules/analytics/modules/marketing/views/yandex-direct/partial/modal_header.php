<?php

use yii\web\View;

/**
 * @var View $this
 */

?>
<div style="margin: 0 -5px 10px;">
    <div class="cont-img_bank-logo" style="padding-top: 22px;width: 150px; float: left;">
        <img src="/images/yandex_direct_logo.png"/>
    </div>
    <div id="statement-bank-info" style="margin-left: 150px; padding: 5px;">
        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee;">
            Данные из Яндекс.Директ загружаются в автоматическом режиме за предыдущий день, ежедневно в 1:00 МСК. Затраты загружаются в КУБ24 без НДС, по одному аккаунту, указанному при подключении, по всем рекламным кампаниям.<br />
            Для загрузки данных за прошедшие периоды, обратитесь в техническую поддержку.
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<br>
