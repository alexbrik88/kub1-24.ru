<?php

/* @var $this yii\web\View
 * @var $model \common\models\marketing\MarketingPlan
 * @var $updateForm \frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm
 */

use common\models\marketing\MarketingPlan;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;

?>
<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-12 column pr-0">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;">
                            <?= htmlspecialchars($this->title); ?>
                        </h4>
                        <div class="column" style="margin-bottom: auto;">
                            <?= Html::button(SpriteIconWidget::widget(['icon' => 'pencil']), [
                                    'title' => 'Редактировать',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#update-marketing-plan-modal',
                                ]
                            ) ?>
                        </div>
                    </div>
                    <div class="row" style="width: 100%;">
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Начало
                            </div>
                            <div style="text-transform: capitalize">
                                <?= Yii::$app->formatter->asDate("01.{$model->start_date}", 'LLLL Y') ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Окончание
                            </div>
                            <div style="text-transform: capitalize">
                                <?= Yii::$app->formatter->asDate("01.{$model->end_date}", 'LLLL Y') ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Период
                            </div>
                            <div>
                                <?= $model->getPeriod() ?> мес.
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Валюта
                            </div>
                            <div>
                                <?= MarketingPlan::$currencyMap[$model->getCurrency()]['code'] ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Комментарий
                            </div>
                            <div>
                                <?= $model->comment ?>
                            </div>
                        </div>
                        <div class="column mb-4 ml-2 pb-2" style="margin-left: auto!important;">
                            <div class="label weight-700 mb-3">
                                Тип воронки продаж
                            </div>
                            <div>
                                <?= MarketingPlan::$templateMap[$model->template] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="update-marketing-plan-modal" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">
                Редактирование маркетингово плана
            </h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?= $this->render('model_form', [
                    'model' => $updateForm,
                ]) ?>
            </div>
        </div>
    </div>
</div>