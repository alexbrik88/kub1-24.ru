<?php

namespace frontend\modules\analytics\modules\marketing\views;

use yii\bootstrap\Nav;
use yii\web\Controller;

/**
 * @var Controller $controller
 */

?>

<div class="nav-tabs-row mb-2 pb-1">
    <?= Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'Дашборд',
                'url' => ['/analytics/marketing/default'],
                'active' => $controller->id === 'default' && $controller->action->id === 'index',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Яндекс.Директ',
                'url' => ['/analytics/marketing/yandex-direct'],
                'active' => $controller->id === 'yandex-direct',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Google.Ads',
                'url' => ['/analytics/marketing/google-words'],
                'active' => $controller->id === 'google-words',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Вконтакте',
                'url' => ['/analytics/marketing/vk-ads'],
                'active' => $controller->id === 'vk-ads',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Facebook',
                'url' => ['/analytics/marketing/facebook'],
                'active' => $controller->id === 'facebook',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Настройки',
                'url' => ['/analytics/marketing/default/upload-data'],
                'active' => $controller->id === 'default' && in_array($controller->action->id, ['upload-data', 'notifications']),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
        ],
    ]) ?>
</div>
