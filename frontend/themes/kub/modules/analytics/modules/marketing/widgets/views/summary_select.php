<?php

use yii\helpers\Html;

/* @var $widget frontend\modules\documents\widgets\SummarySelectWidget */

$js = <<<SCRIPT
;(function($){
    var checkSum = function() {
        let count = 0;
        let cost = 0;
        let clicks = 0;
        let impressions = 0;
        let ctr = 0;
        $('.joint-operation-checkbox:checked').each(function(){
            count++;
            cost += parseFloat($(this).data('cost'));
            clicks += parseFloat($(this).data('clicks'));
            impressions += parseFloat($(this).data('impressions'));
            ctr = (impressions > 0) ? (100 * clicks / impressions) : 0;
        });
        $('.selected-count').text(count);
        $('.selected-cost').text(number_format(cost, 2, ',',' '));
        $('.selected-clicks').text(number_format(clicks, 0, ',',' '));
        $('.selected-ctr').text(number_format(ctr, 2, ',',' '));
    }
    $(document).on('change', '.joint-operation-main-checkbox', function () {
        $('.joint-operation-main-checkbox').not(this).prop('checked', $(this).is(':checked')).uniform('refresh');
        $('.joint-operation-checkbox').prop('checked', $(this).is(':checked')).uniform('refresh');
        checkSum();
    });
    $(document).on('change', '.joint-operation-checkbox', function(){
        checkSum();
        $('.joint-operation-main-checkbox')
            .prop('checked', $('.joint-operation-checkbox:not(:checked)').length == 0)
            .uniform('refresh');
    });
    if ($('.page-proxy').length) {
        $('.wrap_btns.check-condition').find('.total-cost').parent().hide();
        $('.wrap_btns.check-condition').find('.total-cnt').parent().addClass('mr-auto');
    }
}(jQuery));
SCRIPT;

$this->registerJs($js);
?>
<div class="wrap wrap_btns check-condition" style="padding-right: 10px; padding-left: 10px;">
    <div class="row align-items-center justify-content-end actions-many-items">
        <div class="column flex-shrink-0">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table">
            <span class="total-cnt total-txt-foot ml-3">
                Выбрано:
                <strong class="selected-count ml-1" >0</strong>
            </span>
        </div>
        <div class="column flex-shrink-0 mr-auto ml-5">
            <?php if (!$widget->hideCalculatedFields): ?>
            <span class="total-cost total-txt-foot mr-3">
                Расходы:
                <strong class="selected-cost ml-1">0,00</strong>
            </span>
            <span class="total-clicks total-txt-foot mr-3">
                Клики:
                <strong class="selected-clicks ml-1">0</strong>
            </span>
            <span class="total-ctr total-txt-foot mr-3">
                CTR:
                <strong class="selected-ctr ml-1">0,00</strong>
            </span>
            <?php endif; ?>
        </div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
