<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
/* @var integer $step */
/* @var array $inInvoices */
/* @var array $outInvoices */

$inInvoices = $inInvoices ?: [];
$outInvoices = $outInvoices ?: [];

$outInvoicesModels = ($step > 1 && $outInvoices) ? Invoice::find()
    ->where(['company_id' => $searchModel->company_id, 'id' => $outInvoices])
    ->orderBy(['document_number' => SORT_ASC])
    ->all() : [];
?>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php if ($step > 1 && $outInvoices): ?>
    <div class="out-invoices-list">
        <?php foreach ($outInvoicesModels as $i): ?>
            <div class="pt-1 nowrap" style="overflow: hidden; text-overflow: ellipsis">
                <?= '№ ' . $i->fullNumber . ' от '
                . date_format(date_create($i->document_date), 'd.m.Y') . ', '
                . TextHelper::invoiceMoneyFormat($i->total_amount_with_nds, 2) . ' '
                . $i->contractor->getShortName()  ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="invoice-list mt-3 pt-2">

    <?= Html::beginForm(['/analytics/invoice-report/linked-invoices-list'], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <div class="table-settings row row_indents_s mb-3">
        <div class="col-12">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер счета, название или ИНН контрагента',
                        'class' => 'form-control',
                        'style' => 'margin-bottom: 0;',
                        'autocomplete' => 'off',
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                        'style' => 'margin-bottom: 0;'
                    ]) ?>
                </div>
            </div>
        </div>
        <?php /*
        <div id="range-ajax-button">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'ajax m-r-10 m-t-10',]); ?>
        </div>*/ ?>
    </div>

    <div class="accounts-list">
        <div id="in_invoice_table" class="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list table-compact',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'invoice_id',
                        'label' => '',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'padding: 7px 5px 5px 5px;',
                        ],
                        'value' => function ($data) use ($step, $inInvoices, $outInvoices) {

                            $checked =
                                ($step == 1 && in_array($data->id, $outInvoices)) ||
                                ($step == 2 && in_array($data->id, $inInvoices));

                            /** @var Invoice $data */
                            return Html::checkbox(Html::getInputName($data, 'id') . '[]', $checked, [
                                'value' => $data->id,
                                'class' => 'checkbox-invoice-id',
                                'data-step' => $step
                            ]);
                        },
                        'headerOptions' => [
                            'width' => '4%',
                        ],
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            /** @var Invoice $data */
                            return Html::a($data->fullNumber, ['invoice/view', 'type' => $data->type, 'id' => $data->id]);
                        },
                    ],
                    [
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell text-right',
                        ],
                        'attribute' => 'total_amount_with_nds',
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'contractor_id',
                        's2width' => '250px',
                        'hideSearch' => false,
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '48%',
                        ],
                        'contentOptions' => [
                            'class' => 'contractor-cell',
                        ],
                        'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return Html::tag('span', htmlspecialchars($model->contractor_name_short), ['title' => $model->contractor_name_short]);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?php if ($step > 1): ?>
        <?php $disabled = (empty($inInvoices)) ? 'disabled=""' : '' ?>
        <button data-step="<?= $step ?>" id="linked_modal_prev_button" type="button" class="button-clr button-regular button-width button-hover-transparent">Назад</button>
        <button data-step="<?= $step ?>" id="linked_modal_next_button" class="btn-add button-regular button-width button-regular_red button-clr" <?= $disabled ?>>Связать</button>
    <?php else: ?>
        <?php $disabled = (empty($outInvoices)) ? 'disabled=""' : '' ?>
        <button data-step="<?= $step ?>" type="button" class="button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
        <button data-step="<?= $step ?>" id="linked_modal_next_button" class="btn-add button-regular button-width button-regular_red button-clr" <?= $disabled ?>>Далее</button>
    <?php endif; ?>
</div>