<?php
use common\components\TextHelper;
use frontend\themes\kub\components\Icon;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use frontend\modules\analytics\models\InvoiceReportSearch;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use common\models\document\InvoiceInToInvoiceOut as In2Out;
use yii\widgets\Pjax;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $dataProvider ActiveDataProvider */
/* @var $searchModel InvoiceReportSearch */

$this->title = 'Отчет по связанным счетам';

$companyId = $searchModel->company_id;
$statisticItemsArray = $searchModel->linkedStatistic();
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_invoice');

$inBtn = Yii::$app->session->get('report.invoice.linked.button.in', false);
$outBtn = Yii::$app->session->get('report.invoice.linked.button.out', false);
?>

<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/_by_invoices_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <?= RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-9">
            <table class="table table-style table-count-list compact-disallow">
                <thead>
                <tr class="heading">
                    <th>Сотрудник</th>
                    <th>Сумма исх. счетов</th>
                    <th>Сумма вх. счетов</th>
                    <th>Маржа</th>
                    <th>%% маржи</th>
                </tr>
                </thead>
                <tbody>
                <?php $sumOut = $sumIn = 0; ?>
                <?php if ($statisticItemsArray) :
                    foreach ($statisticItemsArray as $row) :
                        $sumOut += $row['out_total_amount_with_nds'];
                        $sumIn += $row['in_total_amount_with_nds'];
                        ?>
                        <tr>
                            <td><?= $row['lastname'] ?> <?= mb_substr($row['firstname'], 0, 1) ?>
                                .<?= mb_substr($row['patronymic'], 0, 1) ?>.
                            </td>
                            <td><?= TextHelper::invoiceMoneyFormat($row['out_total_amount_with_nds'], 2) ?></td>
                            <td><?= TextHelper::invoiceMoneyFormat($row['in_total_amount_with_nds'], 2) ?></td>
                            <td><?= ($row['margin_amount'] > 0) ? TextHelper::invoiceMoneyFormat($row['margin_amount'], 2) : '' ?></td>
                            <td><?= ($row['margin_percent'] > 0) ? TextHelper::numberFormat($row['margin_percent'], 1) : '' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td>--/--</td>
                        <td>0,00</td>
                        <td>0,00</td>
                        <td>0,00</td>
                        <td>0,0%</td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><b>Итого</b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($sumOut, 2) ?></b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($sumIn, 2) ?></b></td>
                    <td><b><?= ($sumOut - $sumIn > 0) ? TextHelper::invoiceMoneyFormat($sumOut - $sumIn, 2) : '' ?></b></td>
                    <td><b><?= ($sumOut > 0) ? TextHelper::numberFormat(100 * ($sumOut - $sumIn) / $sumOut, 1) : '' ?></b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-3">
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-12">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Список счетов: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_invoice']) ?>
            </div>
            <div class="column ml-auto">
                <?= Html::button(Icon::get('add-icon') . '<span class="ml-2">Связать</span>', ['class' => 'linked_modal_add_item_button button-regular button-regular_red button-width', 'title' => 'Связать счета']) ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            <table class="table table-style table-count-list <?= $tabViewClass ?>">
                <thead>
                    <tr>
                        <th colspan="<?=($outBtn) ? '6':'3' ?>" data-id="out" data-collapse-cell-title>
                            <button class="table-collapse-btn button-clr <?=($outBtn) ? 'active':'' ?>" type="button" data-collapse-trigger data-target="out" data-columns-count="6" data-columns-count-collapsed="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Исходящие счета</span>
                            </button>
                        </th>
                        <th colspan="<?=($inBtn) ? '6':'3' ?>" data-id="in" data-collapse-cell-title>
                            <button class="table-collapse-btn button-clr <?=($inBtn) ? 'active':'' ?>" type="button" data-collapse-trigger data-target="in" data-columns-count="6" data-columns-count-collapsed="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Входящие счета</span>
                            </button>
                        </th>
                        <th colspan="2">Маржа</th>
                        <th colspan="1" rowspan="2">
                            <?= $this->render('linked/_filter', ['searchModel' => $searchModel, 'attr' => 'responsible_employee_id', 'title' => 'От&shy;вет&shy;ствен&shy;ный']) ?>
                        </th>
                        <th colspan="1" rowspan="2"></th>
                    </tr>
                    <tr>
                        <!--out-->
                        <th data-id="out" class="<?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'out_document_date', 'title' => 'Дата счета']) ?></th>
                        <th><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'out_document_number', 'title' => '№ счета']) ?></th>
                        <th><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'out_total_amount_with_nds', 'title' => 'Сумма']) ?></th>
                        <th><?= $this->render('linked/_filter', ['searchModel' => $searchModel, 'attr' => 'out_contractor_id', 'title' => 'Покупатель']) ?></th>
                        <th data-id="out" class="<?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_filter', ['searchModel' => $searchModel, 'attr' => 'out_invoice_status_id', 'title' => 'Статус']) ?></th>
                        <th data-id="out" class="<?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'out_payment_date', 'title' => 'Дата оплаты']) ?></th>
                        <!--in-->
                        <th data-id="in" class="<?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'in_document_date', 'title' => 'Дата счета']) ?></th>
                        <th data-id="in" class="<?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'in_document_number', 'title' => '№ счета']) ?></th>
                        <th><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'in_total_amount_with_nds', 'title' => 'Сумма']) ?></th>
                        <th><?= $this->render('linked/_filter', ['searchModel' => $searchModel, 'attr' => 'in_contractor_id', 'title' => 'Поставщик']) ?></th>
                        <th><?= $this->render('linked/_filter', ['searchModel' => $searchModel, 'attr' => 'in_invoice_status_id', 'title' => 'Статус']) ?></th>
                        <th data-id="in" class="<?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'in_payment_date', 'title' => 'Дата оплаты']) ?></th>
                        <!--margin-->
                        <th><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'margin_amount', 'title' => 'Сумма']) ?></th>
                        <th><?= $this->render('linked/_sort', ['searchModel' => $searchModel, 'attr' => 'margin_percent', 'title' => '%%']) ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataProvider->getModels() as $model): ?>
                        <?php $linkNumber = $model['number'] ?>
                        <tr>
                            <!--out-->
                            <td data-id="out" class="nowrap <?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getOutInfo($companyId, $linkNumber, 'document_date') ?></td>
                            <td class="nowrap"><?= In2Out::getOutInfo($companyId, $linkNumber, 'document_number') ?></td>
                            <td class="nowrap text-right"><?= In2Out::getOutInfo($companyId, $linkNumber, 'total_amount_with_nds') ?></td>
                            <td class="contractor-cell"><?= In2Out::getOutInfo($companyId, $linkNumber, 'contractor.name') ?></td>
                            <td data-id="out" class="nowrap <?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getOutInfo($companyId, $linkNumber, 'invoiceStatus.name') ?></td>
                            <td data-id="out" class="nowrap <?=($outBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getOutInfo($companyId, $linkNumber, 'payment_date') ?></td>
                            <!--in-->
                            <td data-id="in" class="nowrap <?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getInInfo($companyId, $linkNumber, 'document_date') ?></td>
                            <td data-id="in" class="nowrap <?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getInInfo($companyId, $linkNumber, 'document_number') ?></td>
                            <td class="nowrap text-right"><?= In2Out::getInInfo($companyId, $linkNumber, 'total_amount_with_nds') ?></td>
                            <td class="contractor-cell"><?= In2Out::getInInfo($companyId, $linkNumber, 'contractor.name') ?></td>
                            <td class="nowrap"><?= In2Out::getInInfo($companyId, $linkNumber, 'invoiceStatus.name') ?></td>
                            <td data-id="in" class="nowrap <?=($inBtn) ? '':'d-none' ?>" data-collapse-cell><?= In2Out::getInInfo($companyId, $linkNumber, 'payment_date') ?></td>
                            <!--margin-->
                            <td class="nowrap text-right"><?= TextHelper::invoiceMoneyFormat($model['margin_amount']) ?></td>
                            <td class="nowrap text-right"><?= TextHelper::numberFormat($model['margin_percent'], 1) ?></td>
                            <!--responsible-->
                            <td class="nowrap"><?= In2Out::getResponsibleEmployee($companyId, $linkNumber) ?></td>
                            <!--actions-->
                            <td class="nowrap">
                                <?= Html::tag('span', Icon::get('pencil'), [
                                    'class' => 'linked_modal_edit_item_button link mr-1',
                                    'title' => 'Редактировать',
                                    'data-link-number' => $linkNumber,
                                    'data-out-invoices' =>  In2Out::getOutInfo($companyId, $linkNumber, 'id', ','),
                                    'data-in-invoices' =>  In2Out::getInInfo($companyId, $linkNumber, 'id', ',')
                                ]) ?>
                                <?= ConfirmModalWidget::widget([
                                'theme' => 'gray',
                                'toggleButton' => ['label' => Icon::get('garbage'), 'class' => 'button-clr link', 'tag' => 'button', 'title' => 'Удалить'],
                                'confirmUrl' => Url::to(['/analytics/invoice-report/delete-link-in-out-invoices', 'linkNumber' => $linkNumber]),
                                'confirmParams' => [],
                                'message' => 'Вы уверены, что хотите удалить связь?',
                                ]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- add item modal -->
<?php Modal::begin([
    'id' => 'linked_modal_add_item',
    'closeButton' => ['label' => Icon::get('close'), 'class' => 'modal-close close'],
]); ?>
    <h4 class="modal-title mb-0">
        <span data-step="1" class="hidden">Шаг 1. Выберите исходящий счет</span>
        <span data-step="2" class="hidden">Шаг 2. Выберите входящий счет</span>
    </h4>
    <div class="modal-subtitle mb-0">
        <span data-step="1" class="hidden">К выбранному счету далее привяжете входящий счет</span>
        <span data-step="2" class="hidden">Исходящие счета, выбранные на шаге 1:</span>
    </div>
    <?php Pjax::begin([
        'id' => 'pjax_linked_modal_add_item',
        'enableReplaceState' => false,
        'enablePushState' => false,
        'scrollTo' => false,
        'timeout' => 10E3,
    ]) ?>
    <?php Pjax::end() ?>
<?php Modal::end(); ?>


<script>
    
    // MODAL
    LinkedInvoices = {
        init: function() {
            this.table.bindEvents();
            this.modal.bindEvents();
        },
        modal: {
            step: 1,
            linkNumber: null,
            invoices: {
                out: [],
                in: []
            },
            ajaxOptions: {
                url: {
                    create: '/analytics/invoice-report/create-link-in-out-invoices',
                    delete: '/analytics/invoice-report/delete-link-in-out-invoices',
                    update: '/analytics/invoice-report/update-link-in-out-invoices',
                }
            },
            pjaxOptions: {
                'url': '/analytics/invoice-report/linked-invoices-list', // get list
                'container': '#pjax_linked_modal_add_item',
                'push': false,
                'replace': false,
                'timeout': 10E3
            },
            bindEvents: function() {
                const that = this;

                // show modal (create)
                $(document).on('click', '.linked_modal_add_item_button', function(e) {
                    that.step = 1;
                    that.linkNumber = null;
                    that.invoices.in = [];
                    that.invoices.out = [];
                    $.pjax.reload(that.pjaxOptions);
                });

                // show modal (update)
                $(document).on('click', '.linked_modal_edit_item_button', function(e) {
                    const linkNumber = Number($(this).data('link-number'));
                    const inInvoices = String($(this).data('in-invoices'));
                    const outInvoices = String($(this).data('out-invoices'));

                    that.step = 1;
                    that.linkNumber = linkNumber || -1;
                    that.invoices.in = inInvoices.split(',').map(function(value) { return Number(value); });
                    that.invoices.out = outInvoices.split(',').map(function(value) { return Number(value); });
                    $.pjax.reload(that.pjaxOptions);
                });

                // pjax modal real query
                $(document).on('pjax:beforeSend', '#pjax_linked_modal_add_item', function (event, xhr, settings) {
                    // prevent infinite recursion
                    if (settings.data)
                        return true;

                    $.pjax({
                        type: 'POST',
                        url: settings.url,
                        data: {
                            'step': that.step,
                            'linkNumber': that.linkNumber,
                            'inInvoices': that.invoices.in,
                            'outInvoices': that.invoices.out
                        },
                        container: '#pjax_linked_modal_add_item',
                        timeout: 10E3,
                        push: false,
                        scrollTo: false
                    }).done(function() {
                        that.showModal();
                    }).fail(function() {
                        window.toastr.error('При формировании списка произошла ошибка. Пожалуйста, попробуйте повторить запрос позднее.');
                    });

                    return false;
                });

                // checkbox
                $(document).on('change', '.checkbox-invoice-id', function() {
                    const nextButton = $('#linked_modal_next_button');
                    const checkboxes = $('.checkbox-invoice-id');
                    const selectedCheckboxes = $(checkboxes).filter(':checked');
                    const step = $(this).attr('data-step') || null;
                    const value = Number($(this).val());

                    $(nextButton).prop('disabled', selectedCheckboxes.length === 0);

                    if ($(this).prop('checked')) {
                        if (step > 1) {
                            that.invoices.in.push(value);
                        } else {
                            that.invoices.out.push(value);
                        }
                    } else{
                        if (step > 1) {
                            for (let i in that.invoices.in)
                                if (that.invoices.in[i] === value)
                                    that.invoices.in.splice(i, 1);
                        } else {
                            for (let i in that.invoices.out)
                                if (that.invoices.out[i] === value)
                                    that.invoices.out.splice(i, 1);
                        }
                    }
                });

                // next/create link button
                $(document).on('click', '#linked_modal_next_button', function() {
                    if ($(this).attr('data-step') > 1) {

                        let l = Ladda.create(this);
                        l.start();

                        $.ajax({
                            type: 'POST',
                            url: that.ajaxOptions.url.create,
                            data: {
                                'linkNumber': that.linkNumber,
                                'inInvoices': that.invoices.in,
                                'outInvoices': that.invoices.out
                            },
                            'success': function(data) {
                                if (data && !data.success) {
                                    window.toastr.error('При связывании счетов произошла ошибка. Пожалуйста, попробуйте повторить запрос позднее.');
                                    Ladda.stopAll();
                                    return;
                                }
                                // not used because server redirect
                                // location.href = location.href;
                            },
                        });

                    } else{
                        that.step++;
                        $.pjax.reload(that.pjaxOptions);
                    }
                });

                // back button
                $(document).on('click', '#linked_modal_prev_button', function() {
                    if (that.step > 1) {
                        that.step--;
                        $.pjax.reload(that.pjaxOptions);
                    }
                });
            },
            showModal: function()
            {
                const modal = $('#linked_modal_add_item');
                const spans = modal.find('.modal-title > span, .modal-subtitle > span');
                spans.addClass('hidden').filter('[data-step=' + this.step + ']').removeClass('hidden');
                modal.modal('show');
            }
        },
        table: {
            saveStateOptions: {
                'url': '/analytics/invoice-report/save-linked-table-state', // save plus-buttons state
            },
            filtersAttributes: [
                'responsible_employee_id',
                'in_contractor_id',
                'out_contractor_id',
                'in_invoice_status_id',
                'out_invoice_status_id'
            ],
            bindEvents: function() {
                const that = this;

                // filters
                for (let i in this.filtersAttributes) {
                    let attr = this.filtersAttributes[i];
                    // click
                    $(document).on('click', '#filter_' + attr, function (e) {
                        e.preventDefault();
                        const select2 = $('#select2_' + attr);
                        if (!select2.data('select2').isOpen()) {
                            select2.select2('open');
                        }
                        return false;
                    });
                    // change
                    $(document).on('change', '#select2_' + attr, function() {
                        location.href = that._addOrReplaceParam(document.location.search, 'InvoiceReportSearch['+attr+']', $(this).val());
                    });
                }

                // plus-buttons
                $('[data-collapse-trigger]').click(function() {
                    $.ajax({
                        type: 'POST',
                        url: that.saveStateOptions.url,
                        data: {
                            'target': $(this).data('target'),
                            'active': $(this).hasClass('active') ? 0 : 1,
                        }
                    });
                });
            },
            /**
             * Add a URL parameter (or modify if already exists)
             * @param {string} url
             * @param {string} param the key to set
             * @param {string} value
             */
            _addOrReplaceParam: function (url, param, value) {
                param = encodeURIComponent(param);
                var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
                var a = document.createElement('a');
                var regex = new RegExp(r);
                var str = param + (value ? "=" + encodeURIComponent(value) : "");
                a.href = url;
                var q = a.search.replace(regex, "$1" + str);
                if (q === a.search) {
                    a.search += (a.search ? "&" : "") + str;
                } else {
                    a.search = q;
                }
                return a.href;
            }
        }
    };

    //////////////////////
    LinkedInvoices.init();
    //////////////////////

</script>