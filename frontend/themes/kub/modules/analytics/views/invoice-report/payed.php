<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use frontend\modules\documents\components\FilterHelper;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\analytics\models\InvoiceReportSearch */

$this->title = 'Отчет по оплаченным счетам';

$statisticItemsArray = $searchModel->payedStatistic();

$userArray = [];
foreach ($statisticItemsArray as $row) {
    $user = EmployeeCompany::findOne([
        'employee_id' => $row['document_author_id'],
        'company_id' => $searchModel->company_id,
    ]) ? : Employee::findOne($row['document_author_id']);

    $userArray[$row['document_author_id']] = $user ? $user->getFio(true) : '';
}
asort($userArray);
$sumDebtStart = 0;
$sumCreated = 0;
$sumPayed = 0;

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_invoice');
?>

<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/_by_invoices_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <?= RangeButtonWidget::widget(['pjaxSelector' => '#abc-analysis-pjax']); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-9">
            <table class="table table-style table-count-list compact-disallow">
                <thead>
                <tr class="heading">
                    <th style="width: 20%;">Сотрудник</th>
                    <th style="width: 18%;">Долг на начало периода</th>
                    <th style="width: 18%;">Выставлено за период</th>
                    <th style="width: 18%;">Оплачено за период</th>
                    <th style="width: 8%;">%%</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($statisticItemsArray) :
                    foreach ($statisticItemsArray as $row) {
                        $sumDebtStart += $row['all_debtStart_amount'];
                        $sumCreated += $row['all_created_amount'];
                        $sumPayed += $row['all_payed_amount'];
                    }
                    foreach ($statisticItemsArray as $row) :
                        $percent = $sumPayed ? $row['all_payed_amount'] / $sumPayed * 100 : 0;
                        ?>
                        <tr>
                            <td><?= $userArray[$row['document_author_id']] ?>.</td>
                            <td><?= TextHelper::invoiceMoneyFormat($row['all_debtStart_amount'], 2) ?></td>
                            <td><?= TextHelper::invoiceMoneyFormat($row['all_created_amount'], 2) ?></td>
                            <td><?= TextHelper::invoiceMoneyFormat($row['all_payed_amount'], 2) ?></td>
                            <td><?= TextHelper::invoiceMoneyFormat($percent * 100, 2) ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td><b>Итого</b></td>
                        <td><b><?= TextHelper::invoiceMoneyFormat($sumDebtStart, 2) ?></b></td>
                        <td><b><?= TextHelper::invoiceMoneyFormat($sumCreated, 2) ?></b></td>
                        <td><b><?= TextHelper::invoiceMoneyFormat($sumPayed, 2) ?></b></td>
                        <td><b>100</b></td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td>--/--</td>
                        <td>0,00</td>
                        <td>0,00</td>
                        <td>0,00</td>
                        <td>0,00</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="col-3">
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-12">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Список счетов: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_invoice']) ?>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'enableSorting' => false,
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
            'format' => 'raw',
            'value' => 'contractor_name_short',
            's2width' => '250px'
        ],

        [
            'attribute' => 'document_date',
            'label' => 'Дата счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '12%',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '12%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $data) {
                return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $data,
                ])
                    ? Html::a($data->fullNumber, ['/documents/invoice/view',
                        'type' => $data->type,
                        'id' => $data->id,
                        'contractorId' => $data->contractor_id,
                    ])
                    : $data->fullNumber;
            },
        ],
        [
            'label' => 'Сумма счета',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '12%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'attribute' => 'total_amount_with_nds',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
            },
        ],
        [
            'label' => 'Оплачено',
            'attribute' => 'flow_sum',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '12%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return TextHelper::invoiceMoneyFormat($model->flow_sum, 2);
            },
        ],
        [
            'attribute' => 'flow_date',
            'label' => 'Дата оплаты',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '12%',
            ],
            'value' => 'flow_date',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'label' => 'Тип оплаты',
            'attribute' => 'flow_way',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '12%',
            ],
            'filter' => ['' => 'Все'] + $searchModel->flowWays,
            'value' => 'flowWay',
            's2width' => '150px'
        ],
        [
            'label' => 'Выставил счет',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '20%',
            ],
            'attribute' => 'document_author_id',
            'filter' => ['' => 'Все'] + $userArray,
            'value' => function (Invoice $model) use ($userArray) {
                return isset($userArray[$model->document_author_id]) ? $userArray[$model->document_author_id] : '';
            },
            's2width' => '250px'
        ],
    ],
]); ?>