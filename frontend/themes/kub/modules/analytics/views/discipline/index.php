<?php

use common\assets\MorrisChartAsset;
use common\components\grid\GridView;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\modules\analytics\models\DisciplineSearch;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $searchModel DisciplineSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

MorrisChartAsset::register($this);

$this->title = 'Платежная дисциплина';

$monthColumns = [];
foreach ($searchModel->monthArray as $month) {
    $monthColumns[] = [
        'attribute' => "m{$month}",
        'headerOptions' => [
            'rowspan' => 1,
            'colspan' => 1,
            'style' => 'border-right-width: 1px;'
        ],
        'contentOptions' => [
            'class' => 'text-right',
        ],
        'value' => function ($data) use ($month) {
            $attr = "m{$month}";

            return round(max($data->$attr, 0));
        },
    ];
}

$this->registerJs('
    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#disciplinesearch-id--filter, disciplinesearch-abc--filter").val("");
        $("#disciplinesearch-group--filter").val($(this).data("group"));
        $("#discipline-grid").yiiGridView("applyFilter");
    });
    $(document).on("pjax:success", "#discipline-pjax", function() {
		$(".custom-scroll-table").mCustomScrollbar({
			horizontalScroll: true,
			axis:"x",
			scrollInertia: 300,
			advanced:{
				autoExpandHorizontalScroll: true,
				updateOnContentResize: true,
				updateOnImageLoad: false
			},
			mouseWheel:{ enable: false },
		});
    });
');

$seriesData = [];
foreach ($searchModel->overallResult as $key => $row) {
    if ($row['contractor_part'] > 0) {
        $item = [
            'label' => $searchModel->getGroupLabel($row['group']),
            'color' => $searchModel->getGroupColor($row['group']),
            'value' => round($row['contractor_part'], 2),
        ];
        array_unshift($seriesData, $item);
    }
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');
?>

<style type="text/css">
.overal-result-table .result-row { cursor: pointer; }
.overal-result-table .result-row:hover td { background-color: #f1f1f1; }
</style>

<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/_by_clients_submenu') ?>

<?php $pjax = Pjax::begin([
    'id' => 'discipline-pjax',
    'timeout' => 10000,
]); ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="pt-1 mt-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <span class="dropdown">
                    <?= Html::a($searchModel->periodName, '#', [
                        'class' => 'button-regular button-hover-transparent w-100',
                        'data-toggle' => 'dropdown',
                    ]); ?>
                    <?= \yii\bootstrap4\Dropdown::widget([
                        'items' => $searchModel->periodItems,
                        'options' => [
                            'class' => 'form-filter-list list-clr '
                        ],
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-9">
            <table class="table table-style table-count-list overal-result-table" style="width: auto;">
                <thead>
                <tr class="heading">
                    <th>Группа</th>
                    <th style="text-align: center;">Кол-во покупателей</th>
                    <th style="text-align: center;">% покупателей</th>
                    <th style="text-align: center;">Среднее кол-во дней на оплату</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($searchModel->overallResult as $row) : ?>
                    <tr class="result-row" data-group="<?= $row['group'] ?>">
                        <td class="text-bold" style="color: <?= $searchModel->getGroupColor($row['group']); ?>;">
                            <?= $searchModel->getGroupLabel($row['group']); ?>
                        </td>
                        <td class="text-right"><?= $row['contractor_count'] ?></td>
                        <td class="text-right text-bold" style="color: <?= $searchModel->getGroupColor($row['group']); ?>;">
                            <?= round($row['contractor_part'], 2); ?>
                        </td>
                        <td class="text-right"><?= round($row['days_count']) ?></td>
                    </tr>
                <?php endforeach ?>
                <tr class="result-row text-bold" data-group="">
                    <td>Итого</td>
                    <td class="text-right"><?= $searchModel->tmpRowCount ?></td>
                    <td class="text-right">100</td>
                    <td class="text-right"><?= $searchModel->averageDays ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-12">
                </div>
            </div>
            <?php if ($seriesData) : ?>
                <div class="row">
                    <div class="col-12">
                        <div id="morris_chart_donut" style="width:100%; margin: 18px auto 0; max-width: 180px; height: 180px;"></div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Покупатели: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'id' => 'discipline-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' .$tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => array_merge(
        [
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                    //'width' => '12%',
                    'class' => 'nowrap-normal max10list',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'html',
                'filter' => $searchModel->getContractorFilterItems(),
                'value' => function ($data) {
                    return Html::a($data->nameWithType, [
                        '/contractor/view',
                        'type' => $data->type,
                        'id' => $data->id,
                    ], [
                        'title' => html_entity_decode($data->nameWithType)
                    ]);
                },
                's2width' => '250px'
            ],
            [
                'attribute' => 'delay',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
            ],
            [
                'label' => 'Просрочка (дн.)',
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'text-center',
                    'rowspan' => 1,
                    'colspan' => count($searchModel->monthArray),
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
            ],
        ],
        $monthColumns,
        [
            [
                'attribute' => 'days',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($data) {
                    return round(max($data->days, 0));
                },
            ],
            [
                'attribute' => 'overdue',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($data) {
                    return round(max($data->overdue, 0));
                },
            ],
            [
                'attribute' => 'group',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'filter' => ['' => 'Все'] + DisciplineSearch::$groups,
                'value' => 'groupLabel',
            ],
            [
                'attribute' => 'abc',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'filter' => ['' => 'Все'] + AnalysisSearch::$groups,
                'value' => 'abcValue',
            ],
        ]
    ),
]); ?>

<script type="text/javascript">
$(document).ready(function() {
    //$(".modal#modal-loader-items").modal();
    var morrisDonut = Morris.Donut({
        element: "morris_chart_donut",
        formatter: function (y, data) { return y + "%" },
        data: <?= Json::encode($seriesData) ?>
    });
    morrisDonut.select(0);
});
</script>

<?php $pjax->end(); ?>
