<?php

use frontend\themes\kub\modules\analytics\widgets\StartModalWidget;
use kartik\widgets\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

$this->title = 'Ключевые возможности';
?>
<style>
    .ba-title {
        font-size: 20px;
        font-weight: 700;
    }
    .ba-blue-title {
        font-size: 20px;
        font-weight: 700;
        color: #4679AE;
    }
    .ba-blue-title.short {
         max-width: 275px;
    }
    .ba-small-text {
        font-size: 14px;
        font-weight: 300;
    }
    .ba-bold-text {
        font-size: 15px;
        font-weight: 700;
    }
    .ba-bg1 {
        background-image: url(/img/ba/bg1.png);
        background-repeat: no-repeat;
        background-position:right;
    }
    .ba-ico {
        display: flex;
        align-items: center;
        background-repeat: no-repeat;
        background-position: left 5px;
        padding-left: 90px;
        min-height: 75px;
        margin-top: -5px;
        margin-bottom: 10px;
    }
    .ba-ico-sm {
        display: flex;
        align-items: center;
        background-repeat: no-repeat;
        background-position: left 5px;
        padding-left: 55px;
        min-height: 45px;
        margin-top: -5px;
        margin-bottom: 10px;
    }
    .ba-ico-1 { background-image: url(/img/ba/ico1.svg); }
    .ba-ico-2 { background-image: url(/img/ba/ico2.svg); }
    .ba-ico-3 { background-image: url(/img/ba/ico3.svg); }
    .ba-ico-4 { background-image: url(/img/ba/ico4.svg); }
    .ba-ico-5 { background-image: url(/img/ba/ico5.svg); }
    .ba-ico-6 { background-image: url(/img/ba/ico6.svg); }
    .ba-ico-7 { background-image: url(/img/ba/ico7.svg); }
    .ba-ico-8 { background-image: url(/img/ba/ico8.svg); }
    .ba-ico-9 { background-image: url(/img/ba/ico9.svg); background-size: 40px; /* temp */ }
    .ba-ico-10 { background-image: url(/img/ba/ico10.svg); }
    .ba-ico-11 { background-image: url(/img/ba/ico11.svg); }
    .ba-tile {
        border-radius: 4px !important;
        border: 1px solid #ddd;
        padding: 20px 15px 20px 20px;
    }
    #ba-video-wrap {
        width: 100%;
    }
</style>
<div class="wrap ba-bg1">
    <div style="width: 65%">
        <div class="ba-title mb-3 pb-1">Добро пожаловать в КУБ24.ФинДиректор!</div>
        <div class="ba-small-text mb-3 pb-1">
            Каждый бизнес генерирует много данных, и именно эти данные могут помочь вам управлять бизнесом
            лучше и эффективнее. Данные покажут то, что не увидишь с помощью интуиции. Данные помогут проще
            и быстрее принимать правильные бизнес решения. Чтобы данные вам помогали, их нужно собрать
            и проанализировать.
        </div>
        <div class="ba-bold-text">
            КУБ24.ФинДиректор - поможет вам собрать данные в отчеты и проанализирует их за вас.<br/>
            Наша цель: помочь увеличить вашу ПРИБЫЛЬ и МАСШТАБИРОВАТЬ ваш бизнес.
        </div>
    </div>
</div>

<div class="wrap">
    <div class="ba-title mb-3 pb-1">Ключевые возможности</div>
    <div class="row">
        <div class="col-6 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico ba-ico-1">
                    <div class="ba-blue-title short">
                        Определим 3-5 ключевых параметров, которые увеличат ПРИБЫЛЬ
                    </div>
                </div>
                <div class="ba-small-text">
                    На основе ваших данных КУБ24 определит параметры, влияющие на
                    результат бизнеса. Ваши бизнес задачи станут прозрачнее и очевиднее —
                    за счет небольших изменений этих параметров вы сможете
                    существенно увеличить прибыль. Графики и отчеты помогут
                    отслеживать результат ежедневно.
                </div>
            </div>
        </div>
        <div class="col-6 pl-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico ba-ico-2">
                    <div class="ba-blue-title short">
                        Автоматически соберем данные из различных источников
                    </div>
                </div>
                <div class="ba-small-text">
                    Подключите загрузку выписки из банка, выписки из ОФД, данных
                    из базы 1С, СРМ и Excel. Данные будут автоматически
                    подгружаться в КУБ в установленное время. КУБ24 даже
                    распознает сканы товарных накладных и УПД, раньше, чем
                    бухгалтер внесет их в 1С.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="ba-title mb-3 pb-1">Основные возможности</div>
    <div class="row mb-3">
        <div class="col-4 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-3">
                    <div class="ba-blue-title">
                        МНОГО ИНТЕГРАЦИЙ
                    </div>
                </div>
                <div class="ba-small-text">
                    Интеграции будут собирать ваши данные
                    автоматически. Вам не нужно тратить свое
                    время и время ваших сотрудников на
                    ручной ввод данных.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-4">
                    <div class="ba-blue-title">
                        ГОТОВЫЕ ОТЧЕТЫ
                    </div>
                </div>
                <div class="ba-small-text">
                    Не нужно настраивать формулы. При клике
                    на любую цифру в отчете, вы получите
                    подробную информацию, как она
                    посчиталась, и какие данные входят в её
                    расчет.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-5">
                    <div class="ba-blue-title">
                        МНОГО ГРАФИКОВ
                    </div>
                </div>
                <div class="ba-small-text">
                    Графики наглядно покажут структуру
                    любого отчета. При клике на столбцы, вы
                    получите больше подробностей.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-4 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-6">
                    <div class="ba-blue-title">
                        ОПИСАТЕЛЬ&shy;НАЯ аналитика
                    </div>
                </div>
                <div class="ba-small-text">
                    Подскажет, если данные внесены неверно
                    или их не хватает. Даст понимание, что
                    происходит в бизнесе, определит и измерит
                    основные KPI бизнеса.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-7">
                    <div class="ba-blue-title">
                        ДИАГНОСТИ&shy;ЧЕСКАЯ аналитика
                    </div>
                </div>
                <div class="ba-small-text">
                    Сопоставит разные отчеты и покажет
                    то, что не увидишь с помощью интуиции.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-8">
                    <div class="ba-blue-title">
                        ПРЕДИКТИВ&shy;НАЯ аналитика
                    </div>
                </div>
                <div class="ba-small-text">
                    Ответит на вопрос, что ожидать
                    через месяц.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="ba-title mb-3 pb-1">Вспомогательные возможности</div>
    <div class="row">
        <div class="col-4 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-9">
                    <div class="ba-blue-title">
                        РАСПОЗНАВА&shy;НИЕ<br/>
                        актов, накладных и УПД
                    </div>
                </div>
                <div class="ba-small-text">
                    Не нужно вводить документы от
                    поставщиков! Отправляйте сканы в КУБ и
                    операции добавятся самостоятельно во все
                    отчеты.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2 pr-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-10">
                    <div class="ba-blue-title">
                        ПРОВЕРКА<br/>
                        клиентов и поставщиков
                    </div>
                </div>
                <div class="ba-small-text">
                    Полезно, а часто необходимо знать
                    финансовую ситуацию у ваших клиентов
                    должников. А перед оплатой новому
                    поставщику проверьте его — возможно, он
                    вам никогда не отгрузит товар.
                </div>
            </div>
        </div>
        <div class="col-4 pl-2" style="display: flex">
            <div class="ba-tile">
                <div class="ba-ico-sm ba-ico-11">
                    <div class="ba-blue-title">
                        УВЕДОМЛЕ&shy;НИЯ<br/>
                        на почту и смс
                    </div>
                </div>
                <div class="ba-small-text">
                    Установите оповещения о событиях,
                    которые проинформируют вас, когда
                    происходят провалы, аномалии или
                    перевыполнение планов.
                    <span>СКОРО</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="ba-title mb-3 pb-1">Рекомендуем</div>
    <div class="row">
        <div class="col-6 pr-2" style="display: flex">
            <div class="ba-tile">
                <div>
                    <div class="ba-blue-title mb-1">
                        ВЕБИНАР
                    </div>
                    <div class="ba-bold-text mb-1">
                        Начало работы с КУБ24.ФинДиректор
                    </div>
                </div>
                <div class="ba-small-text mb-1">
                    Расскажем и покажем, как внести данные и все настроить -
                    подробно по шагам с 1-го по 4-й, которые на следующем шаге. Также расскажем о целях и
                    задачах каждого отчета.
                </div>
                <div class="ba-form mt-3" style="width: 95%">
                    <?php $form = ActiveForm::begin([
                        'id' => 'ba-phone-form',
                        'action' => 'send-signup-to-webinar',
                        'enableClientValidation' => true,
                        'enableAjaxValidation' => true,
                        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
                    ]); ?>
                    <div class="row">
                        <div class="col-6 pr-1">
                            <label class="label">Телефон</label>
                            <?= MaskedInput::widget([
                                'name' => 'phone',
                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => '+7(___) ___-__-__',
                                    'style' => 'width: 100%',
                                ],
                            ]); ?>
                            <div class="invalid-feedback">Необходимо заполнить</div>
                        </div>
                        <div class="col-6 pl-1">
                            <label class="label">&nbsp;</label>
                            <?= Html::submitButton('Записаться на вебинар', [
                                'class' => 'button-regular button-regular_red button-clr',
                                'style' => 'width: 100%',
                                'disabled' => Yii::$app->request->cookies->getValue('signup-to-webinar') ? true : false,
                                'title' => Yii::$app->request->cookies->getValue('signup-to-webinar') ? "Заявка уже отправлена" : false
                            ]); ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="col-6 pl-2" style="display: flex;">
            <div id="ba-video-wrap"></div>
        </div>
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'company',
    'skip_button' => false,
    'next_button' => true,
]); ?>

<?= StartModalWidget::widget([
    'company' => $company,
    'isFirstVisit' => true,
]) ?>

<script>
    $('#ba-phone-form').on('submit', function(e) {
        e.preventDefault();

        let form = $('#ba-phone-form');
        let input = $(form).find('input[name="phone"]');
        if ($(input).val().replace(/\D/g, '').length !== 11) {
            $(input).addClass('is-invalid');
            return false;
        } else {
            $(input).removeClass('is-invalid');
        }

        $.post('send-signup-to-webinar', $(form).serialize(), function(data) {

            let msg = (data.result) ?
                "Ваше заявка отправлена" :
                "Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.";

            window.toastr.success(msg, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false,
            });

            if (data.result) {
                form[0].reset();
                $(form).find('[type="submit"]').prop('disabled', true);
            }
        });

        return false;
    });

    $('#ba-phone-form').find('input[name="phone"]').on('focus', function() {
        $(this).removeClass('is-invalid');
    });

    $(document).ready(function() {
        // $('#ba-video-wrap').append('<iframe width="100%" height="230" src="https://www.youtube.com/embed/Gm-HbTvKw_0" frameborder="0" allowfullscreen></iframe>');
    })
</script>
