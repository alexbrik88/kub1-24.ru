<?php

use frontend\modules\telegram\widgets\CodeButtonWidget;

?>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/img/telegram/logo.svg" width="37" alt="">
            </div>
            <div class="ml-1">
                <div>Telegram</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <?= CodeButtonWidget::widget(); ?>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="d-flex flex-nowrap align-items-center justify-content-between">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="mr-2 nowrap"></div>
                <div class="text_size_12"></div>
            </div>
            <div class="pr-2"></div>
        </div>
    </td>
</tr>