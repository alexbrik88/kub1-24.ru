<?php
use yii\bootstrap4\Html;
use common\models\ofd\Ofd;

$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();

$devRows = [
    0 => [
        'title' => 'ПлатформаОФД',
        'logo' => 'platforma.jpg',
    ],
    1 => [
        'title' => 'TaxCOM',
        'logo' => 'taxcom.jpg',
    ],
    2 => [
        'title' => 'Первый ОФД',
        'logo' => 'perviy.jpg',
    ],
    3 => [
        'title' => 'ОФД Я',
        'logo' => 'yarus.jpg',
    ],
    4 => [
        'title' => 'Яндекс ОФД',
        'logo' => 'yandex_ofd.jpg',
    ],
    5 => [
        'title' => 'Сбис',
        'logo' => 'sbis.jpg',
    ],
    6 => [
        'title' => 'Калуга.Астрал',
        'logo' => 'astral.jpg',
    ],
    7 => [
        'title' => 'ИнитПРО',
        'logo' => 'initpro.jpg',
    ],
    8 => [
        'title' => 'ОФД.ru',
        'logo' => 'ofdru.jpg',
    ],
];
$employee = Yii::$app->user->identity;
$isConnected = [
    Ofd::PLATFORMA => $employee->getOfdUser(Ofd::PLATFORMA)->exists(),
    Ofd::TAXCOM => $employee->getOfdUser(Ofd::TAXCOM)->exists(),
];
?>

<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/img/icons/<?= $devRows[0]['logo'] ?>" width="37" alt="">
            </div>
            <div class="ml-1">
                <div><?= $devRows[0]['title'] ?></div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <?= Html::a($isConnected[Ofd::PLATFORMA] ? 'Подключено' : 'Подключить', [
            "/ofd/platforma/default/integration",
        ], [
            'class' => 'button-clr button-regular pr-3 pl-3 ofd_module_open_link ' . (
                $isConnected[Ofd::PLATFORMA] ? 'integration-success' : 'button-hover-transparent'
            ),
            'style' => [
                'pointer-events' => 'auto',
            ]
        ]); ?>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/img/icons/<?= $devRows[1]['logo'] ?>" width="37" alt="">
            </div>
            <div class="ml-1">
                <div><?= $devRows[1]['title'] ?></div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <?= Html::a($isConnected[Ofd::TAXCOM] ? 'Подключено' : 'Подключить', [
            "/ofd/taxcom/default/integration",
        ], [
            'class' => 'button-clr button-regular pr-3 pl-3 ofd_module_open_link ' . (
                $isConnected[Ofd::TAXCOM] ? 'integration-success' : 'button-hover-transparent'
                ),
            'style' => [
                'pointer-events' => 'auto',
            ]
        ]); ?>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>

<!-- IN DEV -->
<?php for ($i=2; $i<=8; $i++): ?>
    <tr>
        <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
            <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                <div class="mr-2">
                    <img src="/img/icons/<?= $devRows[$i]['logo'] ?>" width="37" alt="">
                </div>
                <div class="ml-1">
                    <div><?= $devRows[$i]['title'] ?></div>
                </div>
            </div>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
            <span style="color:#4379AE">Скоро</span>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
            <div class="pl-5">Бесплатно</div>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

        </td>
    </tr>
<?php endfor; ?>
