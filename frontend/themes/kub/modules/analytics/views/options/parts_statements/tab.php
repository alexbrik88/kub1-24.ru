<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\modules\telegram\widgets\CodeButtonWidget;
use frontend\modules\telegram\widgets\CodeModalWidget;

/** @var $title string */
/** @var $tpl string */
/** @var $params array */
?>

<div class="mt-4">
    <div class="table-wrap">
            <table class="table table-style mb-0 text-dark w-100">
                <thead>
                    <tr role="button" data-toggle="collapse" data-target="#tableMore" aria-expanded="true" aria-controls="tableMore">
                        <td width="250px" class="pb-3 pl-0 pr-3 pt-3 no-border text-left">
                            <div class="weight-700"><?= $columnTitle ?? $title ?></div>
                        </td>
                        <td width="140px"  class="pb-3 pl-3 pr-3 pt-3 no-border text-left">
                            <div class="weight-700">Статус</div>
                        </td>
                        <td width="200px" class="pb-3 pl-3 pr-4 pt-3 no-border text-left">
                            <div class="weight-700 pl-5">Стоимость интеграции</div>
                        </td>
                        <td class="pb-3 pl-3 pr-4 pt-3 no-border text-left">
                            <div class="weight-700">Описание</div>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?= $this->render($tpl, $params) ?>
                </tbody>
            </table>
    </div>
</div>