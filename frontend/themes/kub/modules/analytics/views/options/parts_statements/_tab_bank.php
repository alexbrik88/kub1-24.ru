<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\modules\telegram\widgets\CodeButtonWidget;
use frontend\modules\telegram\widgets\CodeModalWidget;

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'bik' => Banking::bikList(),
])->indexBy('bik')->all();

$accountByBikArray = [];
foreach ($company->bankingAccountants as $account) {
    $accountByBikArray[$account->bik] = $account->id;
}
?>

<?php if ($bankArray): ?>
    <?php foreach (Banking::$modelClassArray as $banking): ?>
        <?php $bank = ArrayHelper::getValue($bankArray, $banking::BIK); ?>
        <?php $userHasBik = array_intersect(array_keys($accountByBikArray), $banking::$bikList); ?>
        <?php if (!$bank) continue; ?>
        <tr>
            <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
                <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                    <div class="mr-2">
                        <img src="<?= \common\components\ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link) ?>" width="37" alt="">
                    </div>
                    <div class="ml-1">
                        <div><?= $bank->bank_name ?></div>
                    </div>
                </div>
            </td>
            <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

                <?php if ($userHasBik): ?>
                    <?php if ($userHasIntegration = BankingParams::getValue($company, $banking::ALIAS, 'access_token')): ?>
                        <span class="button-clr button-regular integration-success">Подключен</span>
                    <?php else: ?>
                        <?= \yii\bootstrap4\Html::a('<span>Подключить</span>', [
                            '/cash/banking/'.$banking::ALIAS.'/default/index',
                            'account_id' => $accountByBikArray[reset($userHasBik)],
                            'bankingAfterLoginRedirectUrl' => '/analytics/options/statements',
                            'p' => Banking::routeEncode($currentPageRoute),
                        ], [
                            'class' => 'banking-module-open-link button-clr button-regular button-hover-transparent pr-3 pl-3',
                            'data' => [
                                'pjax' => '0',
                            ]
                        ]); ?>
                    <?php endif; ?>
                <?php else: ?>
                    <button class="button-clr button-regular button-disabled pr-3 pl-3" type="button">Подключить</button>
                <?php endif; ?>
            </td>
            <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
                <div class="pl-5">Бесплатно</div>
            </td>
            <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

            </td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>