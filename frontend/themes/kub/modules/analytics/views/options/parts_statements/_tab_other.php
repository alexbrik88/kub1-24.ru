<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/images/openCartLogo.png" width="37" alt="">
            </div>
            <div class="ml-1">
                <div>OpenCart</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <span style="color:#4679AE">Скоро</span>
        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/images/inSaleLogo.png" width="41" alt="">
            </div>
            <div class="ml-1">
                <div>InSale</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <span style="color:#4679AE">Скоро</span>
        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/images/bitrix1.png" width="41" alt="">
            </div>
            <div class="ml-1">
                <div>Битрикс 1С</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <span style="color:#4679AE">Скоро</span>
        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/images/excelLogo.png" width="37" alt="">
            </div>
            <div class="ml-1">
                <div>Excel</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <span style="color:#4679AE">Скоро</span>
        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>
<tr>
    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
            <div class="mr-2">
                <img src="/images/uniLogo.png" width="41" alt="">
            </div>
            <div class="ml-1">
                <div>UniSender</div>
            </div>
        </div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <span style="color:#4679AE">Скоро</span>
        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
        <div class="pl-5">Бесплатно</div>
    </td>
    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

    </td>
</tr>