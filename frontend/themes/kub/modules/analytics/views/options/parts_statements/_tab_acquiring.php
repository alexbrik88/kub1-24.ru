<?php

namespace frontend\modules\analytics\views;

use frontend\modules\acquiring\widgets\MonetaButtonWidget;
use frontend\modules\acquiring\widgets\YookassaButtonWidget;
use yii\helpers\Url;

$rows = [
    [
        'title' => 'ЮKassa',
        'logo' => '/img/yookassa/logo.png',
        'button' => YookassaButtonWidget::widget([
            'hasImport' => false,
            'hasBlock' => false,
            'redirectUrl' => Url::to(['/acquiring']),
        ]),
    ],
    [
        'title' => 'Монета',
        'logo' => '/img/icons/moneta_ru.png',
        'button' => MonetaButtonWidget::widget([
            'hasImport' => false,
            'hasBlock' => false,
            'redirectUrl' => Url::to(['/acquiring']),
        ]),
    ],
    [
        'title' => 'Робокасса',
        'logo' => '/img/icons/robokassa.png',
        'button' => 'Скоро',
    ],
    [
        'title' => 'Тинькофф',
        'logo' => '/img/icons/tinkoff_acquiring.jpg',
        'button' => 'Скоро',
    ]
];

?>

<!-- IN DEV -->
<?php foreach($rows as $row): ?>
    <tr>
        <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
            <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                <div class="mr-2">
                    <img src="<?= $row['logo'] ?>" width="37" alt="">
                </div>
                <div class="ml-1">
                    <div><?= $row['title'] ?></div>
                </div>
            </div>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
            <span style="color:#4679AE"><?= $row['button'] ?></span>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">
            <div class="pl-5">Бесплатно</div>
        </td>
        <td class="pt-2 pb-2 pl-3 pr-3 no-border-right">

        </td>
    </tr>
<?php endforeach; ?>
