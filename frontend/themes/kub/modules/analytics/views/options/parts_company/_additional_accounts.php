<?php

use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\BtnConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php $i = 1000; ?>
<?php foreach ($additionalAccounts as $additionalAccount) : ?>
    <?php $i++; ?>
    <div class="company-account" data-id="<?= $additionalAccount->id ?>">
        <strong class="small-title d-block mb-3 pt-3">
            Расчетный счет № <span><?=$i?></span>

            <?= BtnConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                    'class' => 'ml-1 link',
                    'style' => 'color: #bbc1c7;',
                    'tag' => 'a'
                ],
                'modelId' => $additionalAccount->id,
                'message' => 'Вы уверены, что хотите удалить расчетный счет?',
            ]); ?>

        </strong>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($additionalAccount, 'bik')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control input-sm dictionary-bik',
                            'autocomplete' => 'off',
                            'data' => [
                                'url' => Url::to(['/dictionary/bik']),
                                'target-name' => '#' . Html::getInputId($additionalAccount, 'bank_name'),
                                'target-city' => '#' . Html::getInputId($additionalAccount, 'bank_city'),
                                'target-ks' => '#' . Html::getInputId($additionalAccount, 'ks'),
                                'target-rs' => '#' . Html::getInputId($additionalAccount, 'rs'),
                                'target-collapse' => '#company-bank-block',
                            ]
                        ])->label('БИК вашего банка') ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($additionalAccount, 'rs')->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($additionalAccount, 'bank_name')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($additionalAccount, 'ks')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($additionalAccount, 'bank_city')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php foreach ($newAccounts as $newAccount) : ?>
    <?php $i++; ?>
    <div class="company-account new-account" style="display: none" data-id="<?= "new-{$i}" ?>">
        <strong class="small-title d-block mb-3 pt-3">
            Расчетный счет № <span><?=$i?></span>

            <?= BtnConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'circle-close']),
                    'class' => 'ml-1 link',
                    'tag' => 'a'
                ],
                'modelId' => "new-{$i}",
                'message' => 'Вы уверены, что хотите удалить расчетный счет?',
            ]); ?>

        </strong>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($newAccount, 'bik')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control input-sm dictionary-bik',
                            'autocomplete' => 'off',
                            'data' => [
                                'url' => Url::to(['/dictionary/bik']),
                                'target-name' => '#' . Html::getInputId($newAccount, 'bank_name'),
                                'target-city' => '#' . Html::getInputId($newAccount, 'bank_city'),
                                'target-ks' => '#' . Html::getInputId($newAccount, 'ks'),
                                'target-rs' => '#' . Html::getInputId($newAccount, 'rs'),
                                'target-collapse' => '#company-bank-block',
                            ]
                        ])->label('БИК вашего банка') ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($newAccount, 'rs')->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($newAccount, 'bank_name')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($newAccount, 'ks')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="form-group col-3 mb-3">
                        <?= $form->field($newAccount, 'bank_city')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?= Html::button(Icon::get('add-icon', [
    'class' => 'svg-icon mr-1',
]).' <span class="ml-2">Добавить расчётный счёт</span>', [
    'class' => 'button-clr button-regular button-regular_red pl-3 pr-3',
    'id' => 'add-new-account',
]); ?>

<?php
$this->registerJs(<<<JS

    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('.show-first-account-number').show();
        $('.new-account:hidden').first().show(250);
    });

JS
);

?>

<style>
    .show-first-account-number{<?=(empty($additionalAccounts)) ? 'display:none':''?>}
</style>
