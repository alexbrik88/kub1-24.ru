<?php

use common\components\helpers\Html;

$tmpLogo = [
    '/images/amo.png',
    '/images/bitrix.png'
];
$_height = '26px';
?>
<?= Html::a(Html::img($tmpLogo[0], ['style' => "max-width: 120px; max-height:{$_height};"]), 'javascript:;', [
    'class' => 'button-regular button-hover-content-red button-clr la-btn-title disabled',
    'disabled' => true,
    'title' => 'Скоро'
]) ?>
<?= Html::a(Html::img($tmpLogo[1], ['style' => "max-width: 120px; max-height:{$_height};"]), 'javascript:;', [
    'class' => 'button-regular button-hover-content-red button-clr la-btn-title disabled',
    'disabled' => true,
    'title' => 'Скоро'
]) ?>
<?= Html::a('Другая CRM', 'javascript:;', [
    'class' => 'send-proposal-crm button-regular button-hover-content-red button-clr la-btn-title',
]) ?>
