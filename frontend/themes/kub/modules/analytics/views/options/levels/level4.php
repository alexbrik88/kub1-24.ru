<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 4;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'УПРАВЛЕНИЕ ТОВАРАМИ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            ABC анализ по нескольким параметрам, XYZ анализ, Управление скидками,<br/> Скорость продаж, Товары в пути, Анализ цен Поставщиков.<br/>
                            Рекомендации по товарной матрице
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            Увеличение прибыли без увеличения затрат за счет:<br/>
                            <b>1</b>. Использования рекомендаций по каждому товару.<br/>
                            <b>2</b>. Оптимизации товарной матрицы.<br/>
                            <b>3</b>. Управления ценами на товар<br/>
                            <b>4</b>. Определения оптимального кол-ва товарных остатков.<br/>
                            <b>5</b>. Автоматического формирования заказа у поставщиков.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>