<?php

use common\models\company\CheckingAccountant;
use yii\bootstrap4\Modal;
use yii\widgets\Pjax;

echo $this->render('@frontend/themes/kub/views/company/form/modal_rs/_partial/_modal_form', [
    'checkingAccountant' => new CheckingAccountant([
        'company_id' => $company->id,
        'type' => ($company->mainCheckingAccountant) ? CheckingAccountant::TYPE_ADDITIONAL : CheckingAccountant::TYPE_MAIN
    ]),
    'title' => 'Добавить рублевый счет',
    'id' => 'add-company-rs',
    'company' => $company,
]);

echo $this->render('@frontend/modules/analytics/views/options/parts_company_structure/_modal');

?>

<?php
////// GA //////////////////////////////////////////////////////////////////////
Modal::begin([
    'id' => 'google-analytics-connect-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>
<h4 class="modal-title">Настройка интеграции с Google Analytics</h4>
<?php Pjax::begin([
    'id' => 'google-analytics-connect-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
<?php Pjax::end() ?>
<?php Modal::end();
////// GA //////////////////////////////////////////////////////////////////////
?>

<script>
    // banking
    $(document).on("click", ".add-checking-accountant", function(e) {
        e.preventDefault();
        $("#add-company-rs").find("form")[0].reset();
        if ($(this).data('bik')) {
            $('#checkingaccountant-bik').val($(this).data('bik'));
            $('#checkingaccountant-bank_name').val($(this).data('name'));
            $('#checkingaccountant-bank_city').val($(this).data('city'));
            $('#checkingaccountant-ks').val($(this).data('ks'));
        }

        $("#add-company-rs").modal("show");
    });
    $(document).on("beforeSubmit", "form.form-checking-accountant", function () {
        let $this = $(this);
        let $modal = $(this).closest(".modal");
        let $btn;
        let serializedObj = {};
        $this.find("input:checkbox").each(function(){
            serializedObj[this.name] = this.checked ? 1 : 0;
        });
        $this.find("input:text").each(function(){
            serializedObj[this.name] = $(this).val();
        });

        $.post($(this).attr("action"), serializedObj, function (data) {
            if (data.result) {

                $modal.modal("hide");
                Ladda.stopAll();
                $("#add-company-rs").find("form")[0].reset();

                if (data.can_integration) {
                    $btn = $('#banking').find('[data-banking="' + data.banking_alias + '"]');
                    if ($btn) {
                        $btn.attr('href', $btn.attr('href').replace('UNSET_ACCOUNT_ID', data.account_id));
                        $btn.removeClass('add-checking-accountant');
                        $btn.addClass('banking-module-open-link');
                        $btn.click();
                    } else {
                        console.log('btn not found')
                    }
                } else {
                    $btn = $('#banking_other').find('.banking-module-open-link');
                    $btn.attr('href', data.upload_statement_href);
                    $btn.click();
                }

            } else {

                $this.html($(data.html).find("form").html());
            }
        });
        return false;
    });

    // cashbox
    $(document).on("submit", "#cashbox-form", function () {
        $this = $(this);
        $modal = $this.closest(".modal");
        $(this).append("<input type=\"hidden\" name=\"cashbox\" value=\"' . $this->cashbox . '\">");
        $action = $this.data("isnewrecord") == 1 ? "/cash/order/create-cashbox" : "/cash/order/update-cashbox?id=" + $this.data("modelid");
        $.post($action, $(this).serialize(), function (data) {
            if (data.result == true) {
                $modal.modal("hide");
                //$("ul#user-bank-dropdown").replaceWith(data.html);
                if (data.label) {
                    $("span.cashbox-label").text(data.label + " ");
                }
                Ladda.stopAll();

                var block = $("#user-bank-dropdown");
                var update_block = $(block).find(".form-edit").filter("[data-id="+data.id+"]");

                if ($(update_block).length) {
                    $(update_block).find("a.goto-cashbox").html(data.name);
                } else {
                    var template = $(block).find(".cash-template").clone();
                    var href_main = $(template).find("a.goto-cashbox").attr("href");
                    var href_update = $(template).find("a.update-cashbox").attr("href");
                    $(template).addClass("form-edit form-edit_alternative").removeClass("cash-template").attr("data-id", data.id);
                    $(template).find("a.goto-cashbox").attr("href", href_main.replace("cashbox=0", "cashbox=" + data.id)).html(data.name);
                    $(template).find("a.update-cashbox").attr("href", href_update.replace("id=0", "id=" + data.id));
                    $(".form-edit").last().before(template);
                    $(template).show();
                }

            } else {
                $this.html($(data.html).find("form").html());
                refreshUniform();
                refreshDatepicker();
                createSimpleSelect2("cashbox-accessible");

                $("#cashbox-createstartbalance").on("change", function (e) {
                    $(".start-balance-block").toggleClass("hidden");
                });
            }
        });

        return false;
    });

    // salepoint

    // ad

    // crm
    $(document).on("click", ".send-proposal-crm", function(e) {
        e.preventDefault();

        $.pjax({
            url: "send-proposal-crm",
            container: "#proposal-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
        });

        $(document).on("pjax:success", "#proposal-pjax", function() {
            $("#proposal-modal").modal("show");
        });

        return false;
    });

</script>
