<div class="row link_collapse <?= (!empty($collapsed)) ? 'collapsed':'' ?>" type="button" data-toggle="collapse" data-target="#<?=($id)?>" aria-expanded="false" aria-controls="<?=($id)?>">
    <div class="column">
        <div class="la-num"><?=($num)?></div>
    </div>
    <div class="column pl-0 pr-0">
        <div class="la-text-small text-grey">УРОВЕНЬ</div>
        <div class="la-text-big"><?=($title)?></div>
    </div>
    <div class="column ml-auto">
        <div class="la-shevron">
            <svg class="link-shevron svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </div>
    </div>
</div>