<?php

use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\modules;
use yii\helpers\Url;

/** @var $collapsed bool */

$num = 1;
$levelID = "level_{$num}";

?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'КОНТРОЛЬ ДЕНЕГ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">Отчет о Движении Денежных Средств, Платежный календарь, ПланФакт, Аналитика по Приходам и Расходам, По Поставщикам и Покупателям.</div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            <b>1</b>. Контролировать денежные потоки<br/>
                            <b>2</b>. Планировать поступления и расходы<br/>
                            <b>3</b>. Не допустить кассового разрыва
                        </div>
                    </div>
                </div>
            </div>

            <!-- STEP 1 ------------------------------------------------------------------------------>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="la-step-title">Шаг 1</div>
                    <div class="la-step-title-action">Добавить Кошельки</div>
                    <div class="la-text">Кошелек – это места нахождения денег бизнеса. Обычно это банковские Счета, Кассы с наличными<br/> деньгами, Интернет-эквайринг, Электронные кошельки.</div>
                </div>
            </div>

            <!-- BANK -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Банк</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка операций из банков</div>
                                <div id="banking" class="la-sub-text">
                                    <?= $this->render('_banking', [
                                        'company' => $company
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузим операции за прошлые периоды. Будем ежедневно подгружать операции за день.</div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Онлайн остатки по счетам.<br/>
                                    Все операции внесены в отчеты.<br/>
                                    При первой загрузке - загрузятся все контрагенты.
                                </div>
                            </div>
                        </div>
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pt-2">
                                <div class="la-sub-title-action pb-2">Полуавтоматическая загрузка выписок из банка</div>
                                <div id="banking_other" class="la-sub-text">
                                    <?= $this->render('_banking_other', [
                                        'company' => $company
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузка выписок из банка в формате txt или вручную.</div>
                            </div>
                            <div class="col-4 pl-20">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- CASHBOX -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Касса</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка продаж из ОФД</div>
                                <div class="la-sub-text">
                                    <?php
                                    // custom bank icons
                                    $icons = [
                                        'evotor' => 'ofd_evotor.png',
                                        'taxcom' => 'ofd_taxcom.png',
                                    ];
                                    foreach ($icons as $ofd => $ico) {
                                        echo Html::button(Html::img("/images/analytics/options/{$ico}"), [
                                            'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                                            'style' => ''
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузим операции за прошлые периоды. Будем ежедневно подгружать операции за день.</div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Сумма продаж по каждой кассе онлайн.<br/>
                                    Остатки по кассам за каждый день.<br/>
                                    Все операции внесены в отчеты.<br/>
                                    Загрузка предыдущих периодов для анализа.<br/>
                                </div>
                            </div>
                        </div>
                        <div class="pl-3 row">
                            <div class="pl-0 col-8 la-br pt-2">
                                <div class="la-sub-title-action pb-2">Наличные</div>
                                <div class="la-sub-text">
                                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span class="ml-2">Добавить кассу</span>', ['/cashbox/create'], [
                                        'class' => 'add-cashbox ajax-modal-btn button-regular button-hover-content-red button-clr la-btn-title',
                                        'data-url' => Url::to(['/cashbox/create']),
                                        'data-title' => 'Добавить кассу',
                                        'style' => ''
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузите операции за прошлые периоды из Excel. Внесение приходов и расходов.</div>
                            </div>
                            <div class="col-4 pl-20">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ACQUIRING -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title pb-2">Интернет - Эквайринг</div>
                                <div class="la-sub-title-action pb-2">Подключите Интернет-эквайринг</div>
                                <div class="la-sub-text">
                                    <?php
                                    // custom bank icons
                                    $icons = [
                                        'yandex' => 'acquiring_yandex.png',
                                        'moneta' => 'acquiring_moneta.png',
                                        'robokassa' => 'acquiring_robokassa.png',
                                        'tinkoff' => 'acquiring_tinkoff.png',
                                    ];
                                    foreach ($icons as $ofd => $ico) {
                                        echo Html::button(Html::img("/images/analytics/options/{$ico}"), [
                                            'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                                            'style' => ''
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузим операции за прошлые периоды.<br/>Будем ежедневно подгружать операции за день: оплаты клиентов, комиссии за эквайринг, переводы на банковский счет.</div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Реальные остатки на счете эквайринга.<br/>
                                    Каждая операция по продажам на сайте сразу попадает в отчет.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- EMONEY -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title pb-2">E-money</div>
                                <div class="la-sub-title-action pb-2">Подключите электронные счета</div>
                                <div class="la-sub-text">
                                    <?php
                                    // custom bank icons
                                    $icons = [
                                        'yandex' => 'emoney_yandex.png',
                                        'webmoney' => 'emoney_webmoney.png',
                                    ];
                                    foreach ($icons as $ofd => $ico) {
                                        echo Html::button(Html::img("/images/analytics/options/{$ico}"), [
                                            'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                                            'style' => ''
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузим операции за прошлые периоды.<br/>Будем ежедневно подгружать операции за день.</div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Реальные остатки на счете электронной платежной системы.<br/>
                                    Каждая операция сразу попадает в отчет.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BANK CARDS -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title pb-2">Банковские карты</div>
                                <div class="la-sub-title-action pb-2">Подключите бизнес карты</div>
                                <div class="la-sub-text">
                                    <span style="opacity: 0.5; color: #001424; font-size: 24px; font-weight: 300; letter-spacing: 5px;">
                                        СКОРО
                                    </span>
                                </div>
                                <div class="la-sub-text pt-1"></div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Реальные остатки на картах.<br/>
                                    Каждая операция сразу попадает в отчет.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="mb-3 la-bb">&nbsp;</div>

            <!-- STEP 2 ------------------------------------------------------------------------------>
            <div class="row mb-3 pt-1">
                <div class="col-12">
                    <div class="la-step-title">Шаг 2</div>
                    <div class="la-step-title-action">Статьи операций</div>
                </div>
            </div>

            <!-- ARTICLES -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title pb-2">Статьи прихода и расхода</div>
                                <div class="la-sub-title-action pb-2">Настроить Статьи</div>
                                <div class="la-sub-text">
                                    После загрузки выписки из банка, ваши контрагенты добавлены в КУБ24.
                                    Теперь нужно привязать статьи прихода и расхода к контрагентам. Статьи можно менять в любое время. В КУБ24 уже есть предустановленные статьи. Используйте
                                    их или добавьте свои. Не делайте отдельные статьи под разовые и мелкие операции, лучше их сгруппировать. Используйте Настройку правил, что бы автоматически присваивать статью для новых покупателей и поставщиков.
                                </div>
                                <div class="la-sub-text pt-2"  style="float:left">
                                    <?php
                                    echo Html::a('Настроить статьи', ['/analytics/articles/index', 'type' => 2], [
                                        'class' => 'button-regular button-hover-content-red button-clr la-btn-title la-btn-title-text',
                                        'style' => ''
                                    ]);
                                    ?>
                                </div>
                                <div class="la-sub-text pt-2" style="font-size: 12px; line-height:1.2; margin-top: 6px;">Необходимы для группирования<br/> операций в отчетах.</div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    Операции будут сгруппированы по статьям Прихода и Расхода, это позволит анализировать движение денег.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3 la-bb">&nbsp;</div>

            <!-- STEP 3 ------------------------------------------------------------------------------>
            <div class="row mb-3 pt-1">
                <div class="col-12">
                    <div class="la-step-title">Шаг 3</div>
                    <div class="la-step-title-action">Отсрочка платежей</div>
                </div>
            </div>

            <!-- PAYMENT_DELAY -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title-action pb-2">Указать отсрочку по оплатам:</div>
                                <div class="la-sub-text pt-2"  style="float:left">
                                    <div class="pb-1"><b>1</b>. Для Покупателей ООО и ИП (для розничных продаж это не нужно) срок отсрочки <br/><span style="padding-left: 17px">оплаты ваших счетов.</span></div>
                                    <div class="pb-1"><b>2</b>. Для ваших Поставщиков срок отсрочки по оплате их счетов.</div>
                                    <div class="pb-1"><b>3</b>. По умолчанию отсрочка - 10 дней.</div>
                                    <div class="pt-1">
                                        <?php
                                        echo Html::a('Указать сроки по отсрочке', [
                                            '/analytics/finance/payment-calendar', 'subTab' => 'autoplan', 'section' => '2'
                                        ], [
                                            'class' => 'button-regular button-hover-content-red button-clr la-btn-title la-btn-title-text',
                                            'style' => ''
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <div class="pb-1"><b>1</b>. <b>АвтоПланирование</b> платежей по выставленным счетам Покупателям и полученным счетам от Поставщиков.</div>
                                    <div class="pb-1"><b>2</b>. Автоматическое <b>прогнозирование Кассового разрыва.</b></div>
                                    <div class="pb-1"><b>3</b>. Построение <b>Отчета по должникам.</b></div>
                                    <div class="pb-1"><b>4</b>. Расчет <b>оптимального срока отсрочки</b> для клиентов.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3 la-bb">&nbsp;</div>

            <!-- STEP 4 ------------------------------------------------------------------------------>
            <div class="row mb-3 pt-1">
                <div class="col-12">
                    <div class="la-step-title">Шаг 4</div>
                    <div class="la-step-title-action">Точки продаж</div>
                </div>
            </div>

            <!-- SALEPOINTS -->
            <div class="row">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title-action pb-2">Добавить Точки продаж</div>
                                <div class="la-sub-text pt-2"  style="float:left">
                                    <div class="pb-1">
                                        Если у вас больше одной точки продаж, то укажите все точки продаж.<br/>
                                        Например: Отдел продаж, Филиал, Магазин, Интернет Магазин, Кафе или Ресторан.<br/>
                                        Дайте каждой точке продаж название. Например: Магазин Молоко.
                                    </div>
                                    <div class="pt-1">
                                        <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span class="ml-2">Добавить точку продаж</span>', [
                                            'class' => 'toggle-add-element-modal button-regular button-hover-content-red button-clr la-btn-title la-btn-title-text',
                                            'data' => [
                                                'toggle' => 'modal',
                                                'target' => '#add-element-modal',
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <div class="pb-1"><b>1</b>. Точки продаж помогут структурировать загружаемые данные и структурировать отчеты.</div>
                                    <div class="pb-1"><b>2</b>. Сравнение Точек продаж по продажам, расходам, маржинальности.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>