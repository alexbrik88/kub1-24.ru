<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 2;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ЭФФЕКТИВНОСТЬ БИЗНЕСА'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            Отчет о Прибыли и Убытках (P&L),<br/>
                            Точка Без Убыточности,<br/>
                            Рентабельность клиентов,<br/>
                            Рентабельность проектов.
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            <b>1</b>. Определить эффективность бизнеса в целом.<br/>
                            <b>2</b>. Определить эффективность каждой Точки продаж.<br/>
                            <b>3</b>. Определить эффективность каждого проекта.<br/>
                            <b>4</b>. Сравнить доходность с другими инструментами.<br/>
                        </div>
                    </div>
                </div>
            </div>

            <!-- DOCUMENTS + PRODUCTS -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Документы + Товар</div>
                                <div class="la-sub-title-action pb-2">Загрузить документы</div>
                                <div class="la-sub-text">
                                    Нужны счета, Акты, Накладные, УПД, Счета-фактуры как по продажам Клиентам, так и от ваших Поставщиков
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <div>
                                        Что и когда продали, отгрузили или оказали услугу.<br/>
                                        Маржинальность товаров.<br/>
                                        Рентабельность бизнеса.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pt-2">
                                <div class="la-sub-title-action pb-2">Загрузить товары и услуги</div>
                                <div class="la-sub-text">
                                    Нужны товары с ценой Продажи и Покупки, а также начальными остатками.
                                    Нужны услуги с ценой Продажи и ценой Покупки, если применимо.
                                </div>
                            </div>
                            <div class="col-4 pl-20">

                            </div>
                        </div>
                    </div>

                    <div class="wrap">
                        <div class="row pl-3">
                            <div class="pl-0 col-4 la-br pb-3">
                                <div class="la-sub-title pb-2">Способы загрузки</div>
                                <div class="la-sub-title-action pb-2">Загрузить из 1С</div>
                                <div class="la-sub-text">
                                    <a download href="/xls/download-epf">Скачать обработку для 1С Бухгалтерия</a><br/>
                                    <span class="text-grey">Скачать обработку для 1С Управление Торговлей</span>
                                </div>
                            </div>
                            <div class="pl-20 col-4 la-br pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Загрузить из Excel</div>
                                <div class="la-sub-text">
                                    <a download href="/xls/download-template?type=2">Скачать шаблон для товаров</a><br/>
                                    <a download href="/xls/download-template?type=1">Скачать шаблон для услуг</a><br/>
                                    <span class="text-grey">Скачать шаблон для документов</span>
                                </div>
                            </div>
                            <div class="pl-20 col-4 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Вручную</div>
                                <div class="la-sub-text">
                                    Подойдет, если документов и товара/услуг не много
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>