<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 6;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ЭФФЕКТИВНЫЕ ПРОДАЖИ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            Воронка продаж, План-факт, Эффективность менеджеров
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            Определить ROI
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Добавить данные из СРМ</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка данных из СРМ </div>
                                <div id="banking" class="la-sub-text">
                                    <?= $this->render('_crm', [
                                        'company' => $company
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">
                                    Загрузка продаж из СРМ.<br/>
                                    Будут ежедневно подгружаться операции за прошедший день.<br/>
                                    Загрузим операции за прошедшие периоды.
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <b>1</b>. Все результаты указанные в Варианте 2.<br/>
                                    <b>2</b>. Сквозная аналитика до Продаж.<br/>
                                    <b>3</b>. Расчет ROI.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>