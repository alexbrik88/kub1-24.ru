<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 8;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ПЛАНИРОВАНИЕ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            Планы в разрезе каждого отдела, филиала, точки продаж и сотрудника<br/>
                            Бюджет Движения Денежных Средств
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            Сделать декомпозицию планов для достижению целей, поставленных на из уровне № 7<br/>
                            <b>1</b>. Планы продаж на отделы, точки продаж и каждого продавца.<br/>
                            <b>2</b>. Маркетинговый план.<br/>
                            <b>3</b>. Планы по товарным остаткам.<br/>
                            <b>4</b>. Планы по товарной матрице.<br/>
                            <b>5</b>. План по необходимым финансовым средствам.<br/>
                            <b>6</b>. Планы для сотрудников, не связанных с продажами.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>