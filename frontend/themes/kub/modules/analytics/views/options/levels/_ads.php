<?php

use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\models\Company;
use frontend\modules\analytics\modules\marketing\models\UploadDataSearch;

/**
 * @var Company $company
 */

$searchModel = new UploadDataSearch($company);
$integrations = $searchModel->search();
$exceptChannels = $exceptChannels ?? [];
$brAfterChannels = $brAfterChannels ?? [];

?>
<div class="table-wrap">

        <?php foreach ($integrations as $integration): ?>
            <?php
            $channel = ArrayHelper::getValue($integration, 'channel', -1);
            if (in_array($channel, $exceptChannels))
                continue;

            switch ($integration['logo']) {
                case '/images/yandex_direct_logo.png':
                    $_height = '20px';
                    break;
                case '/images/google-analytics-logo.png':
                case '/images/mytarget-logo.png':
                    $_height = '34px';
                    break;
                case '/images/yandex-market-logo.svg':
                    $_height = '35px';
                    break;
                case '/images/google_words_logo.png':
                    $_height = '28px';
                    break;
                default:
                    $_height = '26px';
                    break;
            }
            $logo = Html::img($integration['logo'], ['style' => "max-width: 120px; max-height:{$_height};"]);
            ?>

            <?php if (!$integration['isImplemented']): ?>

                <?= Html::a($logo, 'javascript:;', [
                    'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                    'disabled' => true,
                    'title' => 'Скоро'
                ]) ?>
            <?php elseif ($integration['status'] == true): ?>

                <?= Html::a($logo, 'javascript:;', [
                    'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                    'disabled' => true,
                    //'style' => 'border:2px solid green;',
                    'title' => 'Подключен',
                ]) ?>
            <?php else: ?>

                <?= Html::a($logo, $integration['url'], [
                    'class' => 'button-regular button-hover-content-red button-clr la-btn-title' . ($integration['linkClass'] ?? null),
                    'data-url' => $integration['url'],
                    'title' => 'Подключить ' . $integration['name']
                ]) ?>
            <?php endif; ?>

            <?php if (in_array($channel, $brAfterChannels)): ?>
                <br/>
            <?php endif; ?>

        <?php endforeach; ?>

</div>
