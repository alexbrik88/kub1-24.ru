<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 3;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ГДЕ ДЕНЬГИ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">Управленческий баланс, Нам должны,<br/> Мы должны, Чем владею, Кредиты и Займы </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            <b>1</b>. Определить чем владею и какие источники капитала.<br/>
                            <b>2</b>. Увидеть где мои деньги.<br/>
                            <b>3</b>. Кто должен мне. Кому должен Я
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>