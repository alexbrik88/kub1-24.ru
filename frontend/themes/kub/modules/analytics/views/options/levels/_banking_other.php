<?php
// custom bank icons
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$currentPageRoute = ['/analytics/options/levels'];

$icons = [
    '044525593' => 'bank_alpha.png',
    '044525745' => 'bank_vtb.png',
    '044525555' => 'bank_psb.png',
    '046577795' => 'bank_ural.png',
    '044525700' => 'bank_raiffaizen.png',
    '044525201' => 'bank_avangard.png',
];
$banksData = BikDictionary::find()
    ->select(['bik', 'name', 'city', 'ks'])
    ->andWhere(['bik' => array_keys($icons)])
    ->indexBy('bik')
    ->asArray()
    ->all();

foreach ($icons as $bik => $ico) {
    echo Html::a(Html::img("/images/analytics/options/{$ico}"), [
            '/cash/banking/default/index',
            'p' => Banking::routeEncode($currentPageRoute),
        ], [
        'class' => 'add-checking-accountant button-regular button-hover-content-red button-clr la-btn-title',
        'style' => '',
        'data' => [
            'pjax' => '0',
            'banking' => null,
            'bik' => $bik,
            'name' => ArrayHelper::getValue($banksData, "{$bik}.name"),
            'city' => ArrayHelper::getValue($banksData, "{$bik}.city"),
            'ks' => ArrayHelper::getValue($banksData, "{$bik}.ks"),
        ]
    ]);
}

echo Html::button('Другой банк', [
    'class' => 'add-checking-accountant button-regular button-hover-content-red button-clr la-btn-title',
    'style' => '',
    'data' => [
        'pjax' => '0',
        'banking' => null,
        'bik' => null
    ]
]);

echo Html::a("Загрузить выписку", [
    '/cash/banking/default/index',
    'p' => Banking::routeEncode($currentPageRoute),
], [
    'class' => 'banking-module-open-link',
    'style' => 'display:none!important',
    'data' => [
        'pjax' => '0',
        'banking' => null,
        'bik' => $bik
    ]
]);

?>
