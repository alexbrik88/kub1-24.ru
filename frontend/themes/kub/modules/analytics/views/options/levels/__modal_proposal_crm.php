<?php

use common\models\employee\Employee;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $isSended boolean */

if (Yii::$app->request->isAjax && $isSended) {

    echo Html::script('

        $("#proposal-modal").modal("hide");
        
        window.toastr.success("Ваш запрос отправлен", "", {
            "closeButton": true,
            "showDuration": 1000,
            "hideDuration": 1000,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            "escapeHtml": false,
        });  
        
        ', ['type' => 'text/javascript']);

} ?>

<h4 class="modal-title">
    Запрос в техническую поддержку на подключение CRM
</h4>

<?php
$form = ActiveForm::begin([
    'id' => 'proposal-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'description')->textarea([
                'placeholder' => 'Напишите, какая у вас CRM и ваши контактные данные.',
                'rows' => '12']) ?>
        </div>
    </div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Отправить', [
        'class' => 'button-regular button-regular_red button-width button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<?= Html::script('$("#store-form input:checkbox").uniform();', ['type' => 'text/javascript']) ?>