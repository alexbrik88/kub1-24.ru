<?php

use backend\models\Bank;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\modules;
use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'bik' => Banking::bikList(),
])->indexBy('bik')->all();

$accountByBikArray = [];
foreach ($company->bankingAccountants as $account) {
    $accountByBikArray[$account->bik] = $account->id;
}

$currentPageRoute = ['/analytics/options/levels'];

$icons = [
    modules\sberbank\models\BankModel::class => 'bank_sber.png',
    modules\otkrytiye\models\BankModel::class => 'bank_otkritie.png',
    modules\tinkoff\models\BankModel::class => 'bank_tinkoff.png',
    modules\tochka\models\BankModel::class => 'bank_tochka.png',
    modules\modulbank\models\BankModel::class => 'bank_module.png',
    //modules\alfabank\models\BankModel::class => 'bank_alpha.png'
];

$banksData = BikDictionary::find()
    ->select(['bik', 'name', 'city', 'ks'])
    ->andWhere(['bik' => [
        modules\sberbank\models\BankModel::BIK,
        modules\otkrytiye\models\BankModel::BIK,
        modules\tinkoff\models\BankModel::BIK,
        modules\tochka\models\BankModel::BIK,
        modules\modulbank\models\BankModel::BIK,
        //modules\alfabank\models\BankModel::BIK
    ]])
    ->indexBy('bik')
    ->asArray()
    ->all();

?>

<?php foreach (Banking::$modelClassArray as $banking): ?>
    <?php $bank = ArrayHelper::getValue($bankArray, $banking::BIK); ?>
    <?php $userHasBik = array_intersect(array_keys($accountByBikArray), $banking::$bikList); ?>
    <?php $ico = ArrayHelper::getValue($icons, $banking) ?>
    <?php if (!$bank) continue; ?>

    <?php if ($userHasBik): ?>
        <?php if ($userHasIntegration = BankingParams::getValue($company, $banking::ALIAS, 'access_token')): ?>

            <?php echo Html::tag('span', ($ico) ? Html::img("/images/analytics/options/{$ico}") : $bank->bank_name, [
                'class' => 'button-regular button-hover-content-red button-clr la-btn-title',
                //'style' => 'border:2px solid green;',
                'title' => 'Подключен'
            ]); ?>

        <?php else: ?>

            <?= Html::a(($ico) ? Html::img("/images/analytics/options/{$ico}") : $bank->bank_name, [
                '/cash/banking/'.$banking::ALIAS.'/default/index',
                'account_id' => $accountByBikArray[reset($userHasBik)],
                'bankingAfterLoginRedirectUrl' => $currentPageRoute,
                'p' => Banking::routeEncode($currentPageRoute),
            ], [
                'class' => 'banking-module-open-link button-regular button-hover-content-red button-clr la-btn-title',
                'style' => '',
                'data' => [
                    'pjax' => '0',
                    'banking' => $banking::ALIAS
                ]
            ]); ?>

        <?php endif; ?>
    <?php else: ?>

        <?= Html::a(($ico) ? Html::img("/images/analytics/options/{$ico}") : $bank->bank_name, [
            '/cash/banking/'.$banking::ALIAS.'/default/index',
            'account_id' => "UNSET_ACCOUNT_ID",
            'bankingAfterLoginRedirectUrl' => $currentPageRoute,
            'p' => Banking::routeEncode($currentPageRoute),
        ], [
            'class' => 'add-checking-accountant button-regular button-hover-content-red button-clr la-btn-title',
            'style' => '',
            'data' => [
                'pjax' => '0',
                'banking' => $banking::ALIAS,
                'bik' => $banking::BIK,
                'name' => \yii\helpers\ArrayHelper::getValue($banksData, $banking::BIK.".name"),
                'city' => ArrayHelper::getValue($banksData, $banking::BIK.".city"),
                'ks' => ArrayHelper::getValue($banksData, $banking::BIK.".ks"),
            ]
        ]); ?>

    <?php endif; ?>
<?php endforeach; ?>
