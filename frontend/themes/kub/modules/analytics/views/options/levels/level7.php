<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 7;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ТОЧКИ РОСТА'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            Анализ «Что, если…»<br/>
                            Сценарии<br/>
                            Финансовая модель
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            <b>1</b>. Определить финансовые цели на 3 месяца или на 1-3 года.<br/>
                            <b>2</b>. Найти 3-5 ключевых параметра, минимальное изменение которых позволит достичь вашу цель. Т.е. найти показатели сильнее всего влияющих, например, на увеличение прибыли.<br/>
                            <b>3</b>. Построить реальную Финансовую модель вашего бизнеса на основе ваших данных и ваших целей.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>