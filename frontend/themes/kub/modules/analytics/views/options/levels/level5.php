<?php
use yii\helpers\Html;
/** @var $collapsed bool */

$num = 5;
$levelID = "level_{$num}";
?>
<div class="wrap">
    <?= $this->render('_head', [
        'id' => $levelID,
        'num' => $num,
        'collapsed' => $collapsed,
        'title' => 'ЭФФЕКТИВНЫЙ МАРКЕТИНГ'
    ]) ?>
    <div id="<?=($levelID)?>" class="collapse <?= (!$collapsed) ? 'show':'' ?> pt-1">
        <div class="la-bt pt-3">

            <div class="row mb-3">
                <div class="col-8 d-flex pr-3">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Отчеты данного уровня</div>
                        <div class="la-sub-text">
                            Анализ эффективности рекламных каналов, рекламных компаний.<br/>
                            Анализ эффективности сайта и/или лендингов.
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex pl-0">
                    <div class="wrap wrap-grey mb-0" style="width:100%">
                        <div class="la-sub-title">Цель</div>
                        <div class="la-sub-text">
                            <b>1</b>. Определить эффективность расходования бюджета на рекламу.<br/>
                            <b>2</b>. Определить эффективные / не эффективные каналы.<br/>
                            <b>3</b>. Эффективность маркетолога.<br/>
                            <b>4</b>. Видеть остатки денег в рекламных каналах.<br/>
                            <b>5</b>. Видеть прогноз расходование остатка денег.
                        </div>
                    </div>
                </div>
            </div>

            <!-- STEP 1 ------------------------------------------------------------------------------>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="la-step-title">Вариант 1</div>
                    <div class="la-step-title-action">Добавить только Рекламные каналы</div>
                    <div class="la-text">
                        Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
                        Если у вас НЕ настроена Гугл Аналитика и там НЕ собираются данные по всем каналам, то вам нужен Вариант 1.
                    </div>
                </div>
            </div>
            <!-- BUTTONS 1 -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pb-3">
                                <div class="la-sub-title pb-2">Рекламные каналы</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка данных из рекламных каналов</div>
                                <div id="banking" class="la-sub-text">
                                    <?= $this->render('_ads', [
                                        'company' => $company,
                                        'exceptChannels' => [\common\modules\marketing\models\MarketingChannel::GOOGLE_ANALYTICS],
                                        'brAfterChannels' => []
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">
                                    Данные по рекламному трафику будут загружаться онлайн.<br/>
                                    Будут ежедневно подгружаться операции за прошедший день.<br/>
                                    Загрузим операции за прошедшие периоды.<br/>
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <b>1</b>. Отчеты с итоговой информацией по всем каналам.<br/>
                                    <b>2</b>. Отчеты сравнения результатов рекламных каналов<br/>
                                    <b>3</b>. Вся информация по рекламному каналу, как если бы вы зашли в ваш аккаунт на этой площадке.<br/>
                                    <b>4</b>. Остатки денег по каналам и прогноз их расходования.<br/>
                                    <b>5</b>. Определение эффективности рекламных  каналов.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STEP 2 ------------------------------------------------------------------------------>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="la-step-title">Вариант 2</div>
                    <div class="la-step-title-action">Добавить Рекламные каналы + Лиды</div>
                    <div class="la-text">
                        Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
                        Если у вас настроена Гугл Аналитика и там собираются данные по всем каналам, то этот вариант для вас. В Гугл Аналитике точные данные по Лидам.<br/>
                        ВАЖНО - интеграции с рекламными каналами отличаются от интеграций в Варианте 1.
                    </div>
                </div>
            </div>
            <!-- BUTTONS 2 -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Рекламные каналы + Лиды</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка данных из рекламных каналов</div>
                                <div id="banking" class="la-sub-text">
                                    <?= $this->render('_ads', [
                                        'company' => $company,
                                        'exceptChannels' => [],
                                        'brAfterChannels' => [\common\modules\marketing\models\MarketingChannel::GOOGLE_ANALYTICS]
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">
                                    Данные по рекламному трафику будут загружаться онлайн.<br/>
                                    Будут ежедневно подгружаться операции за прошедший день.<br/>
                                    Загрузим операции за прошедшие периоды.
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title pb-2">&nbsp;</div>
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <b>1</b>. Все результаты указанные в Варианте 1.<br/>
                                    <b>2</b>. Подтвержденные, за счет Гугл Аналитикс, лиды/заявки.<br/>
                                    <b>3</b>. Аналитика до Лида..<br/>
                                    <b>4</b>. Более точная эффективности каналов по сравнению с Вариантом 1.<br/>
                                    <b>5</b>. Определение «Лидогенерирующих каналов».
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STEP 3 ------------------------------------------------------------------------------>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="la-step-title">Вариант 3</div>
                    <div class="la-step-title-action">Добавить Рекламные каналы + Лиды + Продажи</div>
                    <div class="la-text">
                        Рекламные каналы – это источники рекламного трафика в интернет. Это площадки, где вы настраиваете рекламные компании.<br/>
                        У вас настроена Гугл Аналитика и там собираются данные по всем каналам.<br/>
                        ВАЖНО - интеграции с рекламными каналами отличаются от интеграций в Варианте 1.<br/>
                        У вас есть СРМ<br/>
                    </div>
                </div>
            </div>
            <!-- BUTTONS 3 -->
            <div class="row mb-3">
                <div class="col-12">
                    <div class="wrap wrap-grey">
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br la-bb pb-3">
                                <div class="la-sub-title pb-2">Рекламные каналы + Лиды + Продажи</div>
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка данных из рекламных каналов</div>
                                <div id="banking" class="la-sub-text">
                                    <?= $this->render('_ads', [
                                        'company' => $company,
                                        'exceptChannels' => [],
                                        'brAfterChannels' => [\common\modules\marketing\models\MarketingChannel::GOOGLE_ANALYTICS]
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">
                                    Данные по рекламному трафику будут загружаться онлайн.<br/>
                                    Будут ежедневно подгружаться операции за прошедший день.<br/>
                                    Загрузим операции за прошедшие периоды.
                                </div>
                            </div>
                            <div class="col-4 pl-20 pb-3">
                                <div class="la-sub-title-action pb-2">Результат</div>
                                <div class="la-sub-text">
                                    <b>1</b>. Все результаты указанные в Варианте 2.<br/>
                                    <b>2</b>. Сквозная аналитика до Продаж.<br/>
                                    <b>3</b>. Определение «Продающих каналов».<br/>
                                    <b>4</b>. Расчет ROI.
                                </div>
                            </div>
                        </div>
                        <div class="row pl-3">
                            <div class="pl-0 col-8 la-br pt-2">
                                <div class="la-sub-title-action pb-2">Автоматическая загрузка данных из СРМ</div>
                                <div id="banking_other" class="la-sub-text">
                                    <?= $this->render('_crm', [
                                        'company' => $company
                                    ]) ?>
                                </div>
                                <div class="la-sub-text pt-1">Загрузка продаж из СРМ.</div>
                            </div>
                            <div class="col-4 pl-20 pt-2">
                                <div class="la-sub-title-action pb-2"></div>
                                <div class="la-sub-text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>