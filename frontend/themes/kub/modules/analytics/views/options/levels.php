<?php

$this->title = 'Уровни бизнес аналитики';

?>
<style>
    /* todo: fix right align bug */
    #banking-module-pjax {text-align:left!important;}

    .la .link_collapse { user-select: none; }
    .la > .wrap { margin-bottom:10px; }
    .la .wrap .wrap { padding:1rem; margin-bottom:0; }
    .la .link-shevron { font-size: 16px; }
    .la-shevron { font-size: 16px; margin-top:12px; color:#4679AE; }

    .la-num { margin-top:-10px; width: 27px; opacity: 0.5; color: #4679ae; font-size: 47px; font-weight: 700; }
    .la-text-small { color: #9e9e9e; font-size: 14px; font-weight: 300; text-transform: uppercase; }
    .la-text-big { color: #001424; font-size: 20px; font-weight: 700; }
    .la-bt { border-top: 1px solid #e2e5eb; }
    .la-br { border-right: 1px solid #e2e5eb; }
    .la-bb { border-bottom: 1px solid #e2e5eb; }
    .wrap.wrap-grey { background-color: rgba(242, 243, 247, .5); }
    .la-sub-title { font-size:16px; font-weight:700; }
    .la-sub-title-action { font-size:16px; font-weight:700; }
    .la-sub-text { font-size:14px; font-weight:300; }
    .la-sub-text > b { font-weight: 700; }
    .la-text { font-size:14px; font-weight:300; }
    .la-text > b { font-weight: 700; }
    .la-step-title { color: #a2bcd6; font-size: 18px; padding-bottom: 5px; }
    .la-step-title-action { color: #001424; font-size: 22px; font-weight: 700; padding-bottom: 10px; }
    .la .pl-20 {padding-left: 20px !important;}
    .la-btn-title {
        color: #001424;font-size: 14px;font-weight: 700;line-height: 11px;height: 44px;
        padding:0 20px; margin:0 5px 8px 0; min-width:139px; vertical-align:top;
    }
    .la-btn-title-text {
        padding-bottom: 3px;
    }

    .la-sub-title, .la-sub-title-action, .la-sub-text, .la-text {
        line-height: 20px;
    }
    .la .button-hover-content-red:hover > img {
        opacity: .75;
    }
</style>

<div class="la">
    <?= $this->render('levels/level1', [
         'company' => $company,
         'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level2', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level3', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level4', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level5', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level6', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level7', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
    <?= $this->render('levels/level8', [
        'company' => $company,
        'collapsed' => true
    ]) ?>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'company-structure',
    'skip_button' => true,
    'skip_button_title' => 'Пропустить'
]); ?>

<?= $this->render('levels/_modals', [
    'company' => $company
]) ?>