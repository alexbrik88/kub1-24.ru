<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\modules\telegram\widgets\CodeButtonWidget;
use frontend\modules\telegram\widgets\CodeModalWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;

/** @var $this \yii\base\View */

$this->title = 'Загрузка данных';

/** @var \common\models\Company $company */
$company = Yii::$app->user->identity->company;
$currentPageRoute = ['/analytics/options/statements'];
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom: 10px">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto pt-1 pb-1">
                <h4 class="mb-2">Подключение источников, для автоматической загрузки данных</h4>
            </div>
            <div class="column"  style="height: 44px;"></div>
        </div>
    </div>
</div>

<div class="wrap pl-4 pr-4 pb-4 pt-2">
    <div class="nav-tabs-row" id="nav-tabs-payment-calendar">
    <?= \yii\bootstrap4\Tabs::widget([
        'options' => [
            'class' => 'nav nav-tabs nav-tabs-tight nav-tabs_border_bottom_grey w-100',
        ],
        'linkOptions' => [
            'class' => 'nav-link',
        ],
        'tabContentOptions' => [
            'class' => 'tab-pane mt-1',
            'style' => 'width:100%'
        ],
        'headerOptions' => [
            'class' => 'nav-item',
        ],
        'items' => [
            [
                'label' => 'Банки',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_bank',
                    'title' => 'Банки',
                    'params' => [
                        'company' => $company,
                        'currentPageRoute' => $currentPageRoute
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link active',
                ],
                'active' => true
            ],
            [
                'label' => 'ОФД',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_ofd',
                    'title' => 'ОФД',
                    'params' => [
                        'company' => $company,
                        'currentPageRoute' => $currentPageRoute
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Эквайринг',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_acquiring',
                    'title' => 'Эквайринг',
                    'columnTitle' => 'Интернет эквайринг',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Личные карты',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_cards',
                    'title' => 'Личные карты',
                    'columnTitle' => 'Личные карты',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'E-money',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_emoney',
                    'title' => 'E-money',
                    'columnTitle' => 'Электронные кошельки',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Рекламные каналы',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_ads',
                    'title' => 'Рекламные каналы',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'CRM',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_crm',
                    'title' => 'CRM',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Бухгалтерии',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_accounting',
                    'title' => 'Бухгалтерии',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Разное',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_other',
                    'title' => 'Разное',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
            [
                'label' => 'Уведомления',
                'content' => $this->render('parts_statements/tab', [
                    'tpl' => '_tab_notification',
                    'title' => 'Уведомления',
                    'params' => [
                        'company' => $company,
                    ],
                ]),
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
            ],
        ],
    ]); ?>
</div>
</div>

<?= BankingModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to($currentPageRoute),
]) ?>
<?= OfdModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to($currentPageRoute),
]) ?>

<?= CodeModalWidget::widget() ?>
<?= ImportDialogWidget::widget() ?>
