<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>
<style>
    .ba-title {
        font-size: 20px;
        font-weight: 700;
    }
    .ba-blue-title {
        font-size: 20px;
        font-weight: 700;
        color: #4679AE;
    }
    .ba-blue-title.short {
        max-width: 275px;
    }
    .ba-small-text {
        font-size: 14px;
        font-weight: 300;
    }
    .ba-small-text strong {
        font-weight: 700;
    }
    .ba-medium-text {
        font-size: 16px;
    }
    .bold {
        font-weight: 700;
    }
    .ba-tile {
        border-radius: 4px !important;
        border: 1px solid #ddd;
        padding: 20px 15px 20px 20px;
    }
    .ba-tile-blue {
        border-radius: 4px !important;
        border: 2px dashed #4679AE;
        padding: 20px 15px 20px 20px;
    }
    .ba-tile-blue ul {
        margin: 0;
        padding: 0;
    }
    .ba-tile-blue ul li {
        display: block;
        list-style: none;
        padding: 4px 0;
    }
    .ba-tile-blue .lh15 {
        padding: 4px 0;
    }
    .ba-tile-blue ul li:not(:last-child)::before {
        content: "\2022";
        color: #9198a0;
        font-size: 16px;
        padding-right: 10px;
    }
    .ba-bg-bottom {
        min-height: 295px;
        background-repeat: no-repeat;
        background-position: center bottom;
    }
    .ba-bg-1 { background-image: url(/img/ba/bg21.png); }
    .ba-bg-2 { background-image: url(/img/ba/bg22.png); }
    .ba-bg-3 { background-image: url(/img/ba/bg23.png); }
    .ba-bg-4 { background-image: url(/img/ba/bg24.png); }
    .ba-bg-bottom-stub { height: 160px; }

    .ba-arrow {
        position: absolute;
        z-index: 2;
    }
    @media (max-width:1250px) {
        .ba-arrow {
            display: none;
        }
    }
    .ba-arrow.a1 {
        top: 85px;
        right: -25px;
    }
    .ba-arrow.a2 {
        top: 105px;
        right: -35px;
    }
    .ba-arrow.a3 {
        top: 125px;
        right: -45px;
    }
    .ba-arrow.a4 {
        bottom: -70px;
        right: 0px;
    }
    .ba-arrow.a5 {
        bottom: -335px;
        right: 35px;
    }
    .ba-arrow.a6 {
        bottom: -100px;
        left: 20px;
    }
    .ba-arrow.a7 {
        bottom: -70px;
        right: 20px;
    }
    .ba-table {
        width: 100%;
    }
    .ba-table td, .ba-table th {
        border: 1px solid #ddd;
        font-size: 14px;
        font-weight: 300;
        padding: 10px 20px;
    }
    .ba-table th {
        font-weight: 700;
    }

    #ba-video-wrap {
        width: 100%;
    }
    .wrap-video .ba-tile {
        border-radius: 4px !important;
        border: 1px solid #ddd;
        padding: 20px 15px 20px 20px;
    }
</style>

<div class="wrap">
    <div class="ba-title mb-3 pb-1">Первые 4 шага</div>
    <div class="ba-small-text mb-3 pb-1">Большую часть делаете вы, а мы помогаем</div>
    <div class="row mb-3 pb-1">
        <div class="col-3 pr-2" style="display: flex">
            <div class="ba-tile ba-bg-bottom ba-bg-1">
                <div class="">
                    <div class="ba-blue-title mb-2">
                        Шаг 1
                    </div>
                </div>
                <div class="ba-medium-text">
                    Задать структуру вашей компании.
                </div>
                <div class="ba-bg-bottom-stub"></div>
            </div>
            <img class="ba-arrow a1" src="/img/ba/a1.svg"/>
            <img class="ba-arrow a4" src="/img/ba/a4.svg"/>
        </div>
        <div class="col-3 pl-2 pr-2" style="display: flex">
            <div class="ba-tile ba-bg-bottom ba-bg-2">
                <div class="">
                    <div class="ba-blue-title mb-2">
                        Шаг 2
                    </div>
                </div>
                <div class="ba-medium-text">
                    Ввести максимальное <span style="white-space: nowrap">кол-во</span> данных по вашей компании.
                </div>
                <div class="ba-bg-bottom-stub"></div>
            </div>
            <img class="ba-arrow a2" src="/img/ba/a2.svg"/>
            <img class="ba-arrow a5" src="/img/ba/a5.svg"/>
        </div>
        <div class="col-3 pl-2 pr-2" style="display: flex">
            <div class="ba-tile ba-bg-bottom ba-bg-3">
                <div class="">
                    <div class="ba-blue-title mb-2">
                        Шаг 3
                    </div>
                </div>
                <div class="ba-medium-text">
                    Привязать к статьям прихода и расхода операции и контрагентов.
                </div>
                <div class="ba-bg-bottom-stub"></div>
            </div>
            <img class="ba-arrow a3" src="/img/ba/a3.svg"/>
            <img class="ba-arrow a6" src="/img/ba/a6.svg"/>
        </div>
        <div class="col-3 pl-2" style="display: flex">
            <div class="ba-tile ba-bg-bottom ba-bg-4">
                <div class="">
                    <div class="ba-blue-title mb-2">
                        Шаг 4
                    </div>
                </div>
                <div class="ba-medium-text">
                    Задать вопросы нашей <span style="white-space: nowrap">тех поддержке</span> и консультантам.
                </div>
                <div class="ba-bg-bottom-stub"></div>
            </div>
            <img class="ba-arrow a7" src="/img/ba/a7.svg"/>
        </div>
    </div>

    <div class="ba-title mb-3 pb-2 pt-2">Подробнее</div>
    <div class="row mb-3 pb-1">
        <div class="col-4" style="display: flex">
            <div class="ba-tile-blue">
                <div class="ba-small-text bold mb-2">
                    Для создания структуры компании нужно:
                </div>
                <div class="ba-small-text">
                    <ul>
                        <li>Добавить информацию о компании</li>
                        <li>Добавить банковские счета</li>
                        <li>Добавить кассы</li>
                        <li>Добавить склады</li>
                        <li>Добавить точки продаж</li>
                        <li style="padding:0 15px;margin-top:-5px;">(магазины, сайты и т.д.)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-4" style="display: flex">
            <div class="ba-tile-blue">
                <div class="ba-small-text bold mb-2">
                    Это делается в Справочнике, который в левом меню.
                </div>
                <div class="ba-small-text">
                    Можно настроить сразу, можно настроить позже. Один раз настроив статьи расходов и приходов они будут автоматически присваиваться и для новых контрагентов.
                </div>
            </div>
        </div>
        <div class="col-4" style="display: flex">
            <div class="ba-tile-blue">
                <div class="ba-small-text bold mb-2">
                    Если вам потребуется помощь, то наша <span style="white-space: nowrap">тех поддержка</span> и финансовые консультанты подскажут и помогут.
                </div>
                <div class="ba-small-text">
                    <div class="lh15"><strong>Чат:</strong> в правом нижнем углу.</div>
                    <div class="lh15"><strong>Телефон:</strong> 8 800 500 54 36</div>
                    <div class="lh15"><strong>E-mail:</strong> <a href="mailto:support@kub-24.ru">support@kub-24.ru</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="ba-small-text bold mb-2 pb-1">Начальный ввод данных</div>
    <div class="row">
        <div class="col-12">
            <table class="ba-table">
                <tr>
                    <th width="36%">Данные, которые нужно загрузить:</th>
                    <th width="36%">Как это сделать:</th>
                    <th>Где это сделать (меню слева):</th>
                </tr>
                <tr>
                    <td>Выписки из банков</td>
                    <td>Используйте интеграцию с банк-клиентом или загрузите файл с выпиской из банк-клиента вручную</td>
                    <td>Раздел Деньги -- > Банк</td>
                </tr>
                <tr>
                    <td>Номенклатуру по товарам и услугам с ценами и остатками</td>
                    <td>Используйте интеграцию с 1С или загрузите из Эксель файла</td>
                    <td>Раздел Товары -- > Склад<br/>или Раздел Услуги</td>
                </tr>
                <tr>
                    <td>Счета, товарные накладные, акты, УПД от Поставщиков</td>
                    <td>Используйте интеграцию с 1С</td>
                    <td>Раздел Загрузка / Выгрузка</td>
                </tr>
                <tr>
                    <td>Счета, товарные накладные, акты, УПД выставленные Клиентам</td>
                    <td>Используйте интеграцию с 1С</td>
                    <td>Раздел Загрузка / Выгрузка</td>
                </tr>
                <tr>
                    <td>Данные из ОФД</td>
                    <td>Используйте интеграцию с ОФД или загрузите файл, выгруженный из ОФД вручную</td>
                    <td>Раздел Деньги -- > Касса</td>
                </tr>
                <tr>
                    <td>Данные по кассе (не кассовые аппараты)</td>
                    <td>Загрузите из Эксель файла</td>
                    <td>Раздел Деньги -- > Касса</td>
                </tr>
                <tr>
                    <td>Загрузить данные из рекламных кабинетов</td>
                    <td>Используйте интеграции с Яндекс.Директ, Google.Adwords, FB, VK</td>
                    <td>Раздел Маркетинг</td>
                </tr>
                <tr>
                    <td>Загрузить данные из вашей СРМ</td>
                    <td>Используйте интеграцию с AMOcrm или Bitrix24</td>
                    <td>Раздел Продажи -- > Воронка продаж</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="wrap wrap-video">
    <div class="ba-title mb-3 pb-1">Рекомендуем</div>
    <div class="row">
        <div class="col-6 pr-2" style="display: flex">
            <div class="ba-tile">
                <div>
                    <div class="ba-blue-title mb-1">
                        ВЕБИНАР
                    </div>
                    <div class="ba-bold-text mb-1">
                        Начало работы с КУБ24.ФинДиректор
                    </div>
                </div>
                <div class="ba-small-text mb-1">
                    Расскажем и покажем, как внести данные и все настроить -
                    подробно по шагам с 1-го по 4-й, которые на следующем шаге. Также расскажем о целях и
                    задачах каждого отчета.
                </div>
                <div class="ba-form mt-3" style="width: 95%">
                    <?php $form = ActiveForm::begin([
                        'id' => 'ba-phone-form',
                        'action' => 'send-signup-to-webinar',
                        'enableClientValidation' => true,
                        'enableAjaxValidation' => true,
                        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
                    ]); ?>
                    <div class="row">
                        <div class="col-6 pr-1">
                            <label class="label">Телефон</label>
                            <?= MaskedInput::widget([
                                'name' => 'phone',
                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => '+7(___) ___-__-__',
                                    'style' => 'width: 100%',
                                ],
                            ]); ?>
                            <div class="invalid-feedback">Необходимо заполнить</div>
                        </div>
                        <div class="col-6 pl-1">
                            <label class="label">&nbsp;</label>
                            <?= Html::submitButton('Записаться на вебинар', [
                                'class' => 'button-regular button-regular_red button-clr',
                                'style' => 'width: 100%',
                                'disabled' => Yii::$app->request->cookies->getValue('signup-to-webinar') ? true : false,
                                'title' => Yii::$app->request->cookies->getValue('signup-to-webinar') ? "Заявка уже отправлена" : false
                            ]); ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="col-6 pl-2" style="display: flex;">
            <div id="ba-video-wrap"></div>
        </div>
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'company',
    'skip_button' => false,
    'next_button' => true,
]); ?>

<script>
    $('#ba-phone-form').on('submit', function(e) {
        e.preventDefault();

        let form = $('#ba-phone-form');
        let input = $(form).find('input[name="phone"]');
        if ($(input).val().replace(/\D/g, '').length !== 11) {
            $(input).addClass('is-invalid');
            return false;
        } else {
            $(input).removeClass('is-invalid');
        }

        $.post('send-signup-to-webinar', $(form).serialize(), function(data) {

            let msg = (data.result) ?
                "Ваше заявка отправлена" :
                "Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.";

            window.toastr.success(msg, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false,
            });

            if (data.result) {
                form[0].reset();
                $(form).find('[type="submit"]').prop('disabled', true);
            }
        });

        return false;
    });

    $('#ba-phone-form').find('input[name="phone"]').on('focus', function() {
        $(this).removeClass('is-invalid');
    });

    $(document).ready(function() {
        $('#ba-video-wrap').append('<iframe width="100%" height="230" src="https://www.youtube.com/embed/Gm-HbTvKw_0" frameborder="0" allowfullscreen></iframe>');
    })
</script>
