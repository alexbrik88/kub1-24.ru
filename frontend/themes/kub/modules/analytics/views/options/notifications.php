<div class="wrap">
    <div class="weight-700 mb-4">
        <h4>СКОРО ЗАПУСТИМ</h4>
    </div>
    <div class="text-center">
        <img src="/images/notif-img.png" class="w-75" alt="">
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'referrals',
    'skip_button' => true,
    'submit_button' => true,
]); ?>