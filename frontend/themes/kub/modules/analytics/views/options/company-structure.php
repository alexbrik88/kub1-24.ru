<?php

use backend\models\Bank;
use common\components\ImageHelper;
use common\models\company\CheckingAccountant;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use common\models\companyStructure\SalePointType;
use common\models\companyStructure\SalePoint;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Структура компании';

/** @var SalePointType[] $salePointTypes */
/** @var SalePoint[] $companySalePoints */
/** @var \common\models\Company $company */

$salePointTypes = SalePointType::find()->all();
$companySalePoints = SalePoint::find()->where(['company_id' => $company->id])->all();
$accounts = $company->getCheckingAccountants()->andWhere(['<>', 'type', CheckingAccountant::TYPE_CLOSED])->orderBy([
    'type' => SORT_ASC,
    'id' => SORT_ASC,
])->all();

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

?>

<div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom: 10px">
    <div class="p-2">
        <div class="row pb-1">
            <div class="column">
                <div class="pt-1"><h4><?= $this->title ?></h4></div>
            </div>
            <div class="column ml-auto">
                <?= Html::button(\frontend\themes\kub\components\Icon::get('add-icon', ['class' => 'mr-2']).'Добавить точку продаж', [
                    'class' => 'button-regular button-regular_red toggle-add-element-modal',
                    'data' => [
                        'pulsate' => 1,
                        'toggle' => 'modal',
                        'target' => '#add-element-modal',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap p-4 company-structure">
    <div class="p-2">
        <div class="row">
        <?php foreach ($accounts as $account): ?>
            <?php $bank = Bank::findOne(['bik' => $account->bik, 'is_blocked' => false]); ?>
            <div class="col-4">
                <div class="sale-point">
                    <div class="point-title-single">
                        <span class="ico">
                            <?= ($bank && $bank->little_logo_link) ?
                                ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29]) :
                                $this->render('//svg-sprite', ['ico' => 'bank-4', 'class' => 'svg-icon']) ?>
                        </span>
                        <span class="name"><?= Html::encode($account->bank_name) ?></span>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php foreach ($company->ofdTypes as $ofd): ?>
                <div class="col-4">
                    <div class="sale-point">
                        <div class="point-title-single">
                            <span class="ico">
                                <?= $ofd->logo ?
                                    Html::img($ofd->logo, ['width' => '27px']) :
                                    $this->render('//svg-sprite', ['ico' => 'pay-calendar', 'class' => 'svg-icon']) ?>
                            </span>
                            <span class="name"><?= Html::encode($ofd->name) ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
        <?php foreach ($companySalePoints as $salePoint): ?>
            <div class="col-4">
                <div class="sale-point" data-id="<?= $salePoint->id ?>">
                    <div class="point-title">
                        <span class="ico">
                            <img src="/images/<?= $salePoint->getIcon() ?>"/>
                        </span>
                        <span class="name"><?= Html::encode($salePoint->name) ?></span>
                    </div>
                    <div class="employee-list pad-ico">
                        <?php foreach ($salePoint->employers as $employee): ?>
                            <div class="employee-item">
                                <span class="ico-small"><img width="20" src="/images/sale-point-employee.png"/></span>
                                <span class="small-name"><?= Html::encode($employee->getShortFio()) ?></span>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="cashbox-list">
                        <?php foreach ($salePoint->cashboxes as $cashbox): ?>
                            <div class="cashbox-item">
                                <span class="ico"><img src="/images/sale-point-cashbox.png"/></span>
                                <span class="name"><?= Html::encode($cashbox->name) ?></span>
                            </div>
                        <?php endforeach; ?>
                        <?php if ($salePoint->type_id == SalePoint::TYPE_INTERNET_SHOP && $salePoint->onlinePaymentType): ?>
                            <div class="cashbox-item">
                                <span class="ico">
                                    <?= $salePoint->onlinePaymentType->logo ?
                                        Html::img($salePoint->onlinePaymentType->logo, ['width' => '27px']) :
                                        $this->render('//svg-sprite', ['ico' => 'bill', 'class' => 'svg-icon']) ?>
                                </span>
                                <span class="name"><?= Html::encode($salePoint->onlinePaymentType->name) ?></span>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="store-list">
                        <div class="store-item">
                            <span class="ico"><img src="/images/sale-point-store.png"/></span>
                            <span class="name"><?= Html::encode($salePoint->store->name) ?></span>
                        </div>
                    </div>

                    <div class="sale-point-actions">
                        <?= \yii\helpers\Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'class' => 'edit-sale-point button-regular button-hover-transparent button-clr',
                            'title' => 'Редактировать',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                            'class' => 'delete-sale-point button-regular button-hover-transparent button-clr',
                            'title' => 'Удалить',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
        </div>
        <?php /*
        <div class="row mt-3">
            <div class="col-9">
                <div class="row">
                    <?php foreach ($salePointTypes as $key => $salePointType): ?>
                        <div class="col-4">
                            <div class="form-group">
                                <span class="label">
                                    <?= $salePointType->name ?>
                                    <?= Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip2_type_' . $salePointType->id,
                                    ]) ?>
                                </span>
                                <br/>
                                <button class="button-clr button-regular button-regular_red pl-3 pr-3 add-sale-point" data-type="<?= $salePointType->id ?>">
                                    <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
                                    <span class="ml-1">Добавить</span>
                                </button>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <div class="col-4">
                        <div class="form-group">
                                <span class="label">
                                    Другое
                                </span>
                            <br/>
                            <button class="button-clr button-regular button-regular_red pl-3 pr-3 send-proposal">
                                <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
                                <span class="ml-1">Добавить</span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        */ ?>
    </div>
</div>

<div style="display: none">
    <?php foreach ($salePointTypes as $salePointType) {
        echo Html::tag('div', SalePointType::$helpText[$salePointType->id], [
            'id' => 'tooltip2_type_' . $salePointType->id
        ]);
    } ?>
</div>

<?php

echo $this->render('actions-buttons', [
    'next_step' => 'statements',
    'skip_button' => true,
    'submit_button' => true,
    'text' => 'Структура позволяет визуализировать ваш бизнес. В дальнейшем каждая точка продаж будет иметь свою аналитику. Для начала работы Структура не обязательна.'
]);

echo $this->render('parts_company_structure/_modal');

/* preload styles */
echo Html::tag('div', Select2::widget(['name' => 'empty', 'data' => []]), ['style' => 'display:none']);