<?php

use common\components\grid\GridView;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\analytics\models\ProductAnalysisABC;
use common\components\TextHelper;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisABC */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'ABC анализ';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');
$products = $dataProvider->getModels();
?>

<?= $this->render('@frontend/themes/kub/modules/analytics/views/layouts/product_analysis_menu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-0 pl-1">
                <?= \yii\bootstrap4\Html::beginForm(['abc'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= \common\components\helpers\Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<?php /* $pjax = Pjax::begin([
    'id' => 'abc-analysis-pjax',
    'timeout' => 10000,
]); */ ?>


<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск по наименованию или артикулу',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'id' => 'abc-analysis-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'id',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '10%'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return $data['title'];
            },
        ],
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'headerOptions' => [
                'width' => '10%'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return $data['article'];
            },
        ],
        [
            'attribute' => 'turnover',
            'label' => 'Оборот',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                $value = TextHelper::invoiceMoneyFormat($data['turnover'], 2);
                $prevValue = TextHelper::invoiceMoneyFormat($data['turnover'], 2);
                return '<div class="grid-cell-3"><div>'.$value.'</div><div><div>20,00%</div><div>12 197,54</div></div></div>';
            },
        ],
        [
            'attribute' => 'selling_price',
            'label' => 'Цена продажи',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::invoiceMoneyFormat($data['selling_price'], 2);
            },
        ],
        [
            'attribute' => 'cost_price',
            'label' => 'Себестоимость',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::invoiceMoneyFormat($data['cost_price'], 2);
            },
        ],
        [
            'attribute' => 'purchase_price',
            'label' => 'Цена покупки',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::invoiceMoneyFormat($data['purchase_price'], 2);
            },
        ],
        [
            'attribute' => 'margin',
            'label' => 'Маржа Р',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::invoiceMoneyFormat($data['margin'], 2);
            },
        ],
        [
            'attribute' => 'margin_percent',
            'label' => '% Маржи',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::numberFormat(100 * $data['margin_percent']);
            },
        ],
        [
            'attribute' => 'selling_quantity',
            'label' => 'Кол-во продано',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::numberFormat($data['selling_quantity']);
            },
        ],
        [
            'attribute' => 'group_margin_percent',
            'label' => 'Доля маржи в группе',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::numberFormat(100 * $data['group_margin_percent']);
            },
        ],
        [
            'attribute' => 'number_of_sales',
            'label' => 'Кол-во продаж',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::numberFormat($data['number_of_sales']);
            },
        ],
        [
            'attribute' => 'average_check',
            'label' => 'Средний чек',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return TextHelper::invoiceMoneyFormat($data['average_check'], 2);
            },
        ],
        [
            'attribute' => 'total_ratio',
            'label' => 'Итоговый коэффициент',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return "";
            },
        ],
        [
            'attribute' => 'abc',
            'label' => 'АВС комбинация',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return "";
            },
        ],
        [
            'attribute' => 'xyz',
            'label' => 'XYZ значение',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return "";
            },
        ],
    ],
]); ?>

<?php /* $pjax->end(); */ ?>

<script>
    $('#productanalysisabc-year').on('change', function() {
        location.href = $(this).closest('form').attr('action') + '?' + $(this).closest('form').serialize();
    });
</script>