<?php

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\analytics\models\ProductAnalysisSellingSpeedSearch;
use frontend\themes\kub\widgets\SummarySelectWidget;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use frontend\widgets\TableConfigWidget;
use yii\widgets\ActiveForm;

/** @var $this View */
/** @var $searchModel ProductAnalysisSellingSpeedSearch */
/** @var $dataProvider ActiveDataProvider */
/** @var Employee $user */
/** @var Config $userConfig */
/** @var Company $company */

$this->title = 'Количество дней товарного запаса';


$user = Yii::$app->user->identity;
$userConfig = $user->config;

$showHelpPanel = $userConfig->selling_speed_help ?? false;
$showChartPanel = $userConfig->selling_speed_chart ?? false;

$tabViewClass = $userConfig->getTableViewClass('table_selling_speed');

$emptyText = 'В указаном периоде нет данных для отображения';

// for charts

$products = []; //$dataProvider->getModels();

$model = new Product();

$company = $user->company;
$model->company_id = $company->id;

$stockQuantity = 0;//$model->balanceAtDate(false, array_column($products, 'product_id'), true, Yii::$app->db2);
$prevStockQuantity = 0;//$model->balanceAtDate(false, array_column($products, 'product_id'), true, Yii::$app->db2);

//end

?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]); ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]); ?>

    <div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
        <div class="pl-2 pr-2 pb-1">
            <div class="row align-items-center">
                <div class="column mr-auto ml-0 pl-0">
                    <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
                </div>
                <div class="column pr-2">
                    <?= \yii\bootstrap4\Html::button(Icon::get('diagram'),
                        [
                            'id' => 'btnChartCollapse',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : ''),
                            'data-toggle' => 'collapse',
                            'href' => '#chartCollapse',
                            'data-tooltip-content' => '#tooltip_chart_collapse',
                            'onclick' => new JsExpression('$(this).tooltipster("hide")')
                        ]) ?>
                </div>
                <div class="column pl-1 pr-2">
                    <?= Html::button(Icon::get('book'),
                        [
                            'id' => 'btnHelpCollapse',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                            'data-toggle' => 'collapse',
                            'href' => '#helpCollapse',
                            'data-tooltip-content' => '#tooltip_help_collapse',
                            'onclick' => new JsExpression('$(this).tooltipster("hide")')
                        ]) ?>
                </div>
                <div class="column col-xl-3 pl-1 pr-2">
                    <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                </div>
            </div>
        </div>

        <div style="display: none">
            <div id="tooltip_help_collapse" data-open="Открыть описание отчёта"
                 data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
            <div id="tooltip_chart_collapse" data-open="Открыть график"
                 data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
        </div>

    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>"
         id="chartCollapse" data-attribute="selling_speed_chart">
        <div class="pt-4 pb-3">
            <div class="row">
                <div class="col-4 pr-0 pl-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => 'ВЫРУЧКА',
                        'ico' => 'ico_turnover.svg',
                        'value' => 1 / 100 * array_sum(array_column($products, 'turnover')),
                        'prevValue' => 0,//1 / 100 * array_sum(array_column($prevProducts, 'turnover')),
                        'units' => '₽',
                        'borders' => ['bottom', 'right']
                    ]) ?>
                </div>
                <div class="col-4 pr-0 pl-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => 'МАРЖА',
                        'ico' => 'ico_margin.svg',
                        'value' => 1 / 100 * array_sum(array_column($products, 'margin')),
                        'prevValue' => 0,//1 / 100 * array_sum(array_column($prevProducts, 'margin')),
                        'units' => '₽',
                        'borders' => ['bottom', 'right']
                    ]) ?>
                </div>
                <div class="col-4 pl-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => 'ОСТАТОК НА СКЛАДЕ',
                        'ico' => 'ico_store.svg',
                        'value' => $stockQuantity,
                        'prevValue' => $prevStockQuantity,
                        'units' => 'шт.',
                        'borders' => ['bottom']
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-4 pr-0 pl-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => 'ОБЪЕМ ПРОДАЖ',
                        'ico' => 'ico_sells_quantity.svg',
                        'value' => array_sum(array_column($products, 'selling_quantity')),
                        'prevValue' => 0,//array_sum(array_column($prevProducts, 'selling_quantity')),
                        'units' => 'шт.',
                        'borders' => ['right']
                    ]) ?>
                </div>
                <div class="col-4 pl-0 pr-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => '% МАРЖИ',
                        'ico' => 'ico_margin_percent.svg',
                        'value' => (count($products) > 0) ? 100 * array_sum(array_column($products, 'margin_percent')) / count($products) : 0,
                        'prevValue' => 0,//(count($prevProducts) > 0) ? 100 * array_sum(array_column($prevProducts, 'margin_percent')) / count($prevProducts) : 0,
                        'units' => '%',
                        'borders' => ['right']
                    ]) ?>
                </div>
                <div class="col-4 pl-0">
                    <?= $this->render('partial/chart_totals_cell', [
                        'title' => 'СРЕДНИЙ ЧЕК',
                        'ico' => 'ico_average_check.svg',
                        'value' => (count($products) > 0) ? 1 / 100 * array_sum(array_column($products, 'average_check')) / count($products) : 0,
                        'prevValue' => 0,//(count($prevProducts) > 0) ? 1 / 100 * array_sum(array_column($prevProducts, 'average_check')) / count($prevProducts) : 0,
                        'units' => '₽',
                        'borders' => []
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>"
         id="helpCollapse" data-attribute="selling_speed_help">
        <div class="pt-4 pb-3">
            <p>Данный отчет применим для равномерных продаж, например E-Commerce или розница. Не подходит для опта, так
                как в этом канале продаж сделки проходят всплесками и расчет скорости просто размазывает количество
                одной-двух сделок по всему периоду, что искажает реальную картину. Для опта более применим метод
                неснижаемого остатка.
            </p>
            <p>Период отчета следует брать в зависимости от задачи, которую предстоит решать.
            </p>
            <ul>
                <li>Например, если цель заключается в подсортировке ТТ или склада, то период необходимо выставить 1-2
                    месяца (важно понимать, что данный отчет не учитывает колебания сезонного спроса);
                </li>
                <li>Если задача заключается в анализе сезона продаж, например для подготовки заказа на следующий сезон,
                    то необходимо брать период равный сезону – примерно 1 месяц (зависит от особенностей рынка), в
                    который идут распродажи;
                </li>
                <li>Если задача состоит в том, чтобы проанализировать стоки на предмет неликвидов и сверхнормативов, то
                    период следует выставлять нормам оборачиваемости товаров исходя из особенностей конкретного рынка.
                </li>
            </ul>
        </div>
    </div>


    <div class="table-settings row row_indents_s mt-2">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => array_filter([
                    [
                        'attribute' => 'report_selling_speed_unit',
                    ],
                    [
                        'attribute' => 'report_selling_speed_article',
                    ],
                    [
                        'attribute' => 'report_selling_speed_group',
                    ],
                    [
                        'attribute' => 'report_selling_speed_storage',
                    ],
                ]),
                'sortingItemsTitle' => 'Выводить',
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_selling_speed']) ?>
        </div>
        <div class="col-6">
            <?php $form = ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'search', [
                    'type' => 'search',
                    'placeholder' => 'Поиск по наименованию или артикулу',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyText,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table' . $tabViewClass,
    ],
    'rowOptions' => function ($data) {
        return [];
    },
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('@frontend/themes/kub/views/layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (Product $model) {
                /** @var Invoice $invoice */
                $invoice = $model->getInvoices()
                    ->andWhere(['type' => Documents::IO_TYPE_IN])
                    ->orderBy(Invoice::tableName() . '.id DESC')
                    ->one();
                return Html::checkbox('ids[]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data' => [
                        'store_id' => $model->storeId,
                        'product-id' => $model->id,
                        'product-name' => $model->title,
                        'product-price' => $model->priceForBuyWithoutNds,
                        'product-price-with-vat' => $model->price_for_buy_with_nds,
                        'unit' => $model->productUnit->name,
                        'contractor-id' => $invoice ? $invoice->contractor->id : 0,
                        'contractor-name' => $invoice ? $invoice->contractor->nameWithType : "",
                        'quantity' => floor($model->lastOrderQuantity),
                    ],
                ]);
            },
        ],
        [
            'attribute' => 'daysOfInventory',
            'label' => 'Количество дней товарного запаса',
            'headerOptions' => [
                'class' => 'sorting',
                'style' => 'max-width:120px'
            ],
            'format' => 'raw',
            'value' => function(Product $model) {
                return round($model->daysOfInventory, 1);
            }
        ],
        [
            'attribute' => 'sellingSpeed',
            'label' => 'Скорость продаж, ед./день',
            'headerOptions' => [
                'class' => 'sorting',
                'style' => 'min-width:100px;'
            ],
            'contentOptions' => [
                'class' => 'nowrap'
            ],
            'format' => 'raw',
            'value' => function (Product $model) {
                return floor($model->sellingSpeed);
            },
        ],
        [
            'attribute' => 'remainder',
            'label' => 'Остаток',
            'headerOptions' => [
                'class' => 'sorting',
                'style' => 'min-width:80px;'
            ],
            'contentOptions' => [
                'class' => 'nowrap'
            ],
            'format' => 'raw',
            'value' => function (Product $model) {
                return floor($model->remainder);
            },
        ],
        [
            'attribute' => 'soldDuringPeriod',
            'label' => 'Продано за период',
            'headerOptions' => [
                'class' => 'sorting',
                'style' => 'min-width:120px;'
            ],
            'format' => 'raw',
            'value' => function (Product $model) {
                return TextHelper::invoiceMoneyFormat($model->soldDuringPeriod, 2);
            },
        ],
        [
            'attribute' => 'article',
            'label' => 'Артикул',
            'headerOptions' => [
                'style' => 'min-width:70px',
                'class' => 'col_report_selling_speed_article ' . ($userConfig->report_selling_speed_article ? '' : 'hidden')
            ],
            'contentOptions' => [
                'class' => 'contractor-cell col_report_selling_speed_article ' . ($userConfig->report_selling_speed_article ? '' : 'hidden')
            ],
            'format' => 'raw',
            'value' => function (Product $model) {
                return $model->article;
            },
        ],
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'headerOptions' => [
                'style' => 'min-width:70px',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell',
            ],
            's2width' => '200px',
            'hideSearch' => false,
            'filter' => $searchModel->filterProductName(),
            'format' => 'raw',
            'value' => function (Product $model) {
                return $model->title;
            },
        ],
        [
            'attribute' => 'unit',
            'label' => 'Ед. измерения',
            'headerOptions' => [
                'style' => 'min-width:70px',
                'class' => 'col_report_selling_speed_unit ' . ($userConfig->report_selling_speed_unit ? '' : 'hidden')
            ],
            'contentOptions' => [
                'class' => 'col_report_selling_speed_unit ' . ($userConfig->report_selling_speed_unit ? '' : 'hidden')
            ],
            's2width' => '200px',
            'hideSearch' => false,
            'filter' => $searchModel->filterUnitProduct(),
            'format' => 'raw',
            'value' => function (Product $model) {
                return $model->productUnit->name;
            }
        ],
        [
            'attribute' => 'group',
            'label' => 'Группа товара',
            'headerOptions' => [
                'style' => 'min-width:70px',
                'class' => 'col_report_selling_speed_group ' . ($userConfig->report_selling_speed_group ? '' : 'hidden')
            ],
            'contentOptions' => [
                'class' => 'clause-cell col_report_selling_speed_group ' . ($userConfig->report_selling_speed_group ? '' : 'hidden')
            ],
            's2width' => '200px',
            'hideSearch' => false,
            'filter' => $searchModel->filterGroup(),
            'format' => 'raw',
            'value' => function (Product $model) {
                return $model->group->title;
            },
        ],
        [
            'attribute' => 'provider',
            'label' => 'Поставщик',
            'headerOptions' => [
                'style' => 'min-width:120px',
            ],
            'contentOptions' => [
                'class' => 'clause-cell',
            ],
            's2width' => '200px',
            'filter' => $searchModel->filterSupplier(),
            'format' => 'raw',
            'value' => function (Product $model) {
                /** @var Invoice $invoice */
                $invoice = $model->getInvoices()
                    ->andWhere(['type' => Documents::IO_TYPE_IN])
                    ->orderBy(Invoice::tableName() . '.id DESC')
                    ->one();
                return !$invoice ? "-" : Html::a($invoice->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $invoice->contractor->id, 'type' => Documents::IO_TYPE_IN]), ['target' => '_blank']);
            },
        ],
        [
            'attribute' => 'storeName',
            'label' => 'Склад',
            'headerOptions' => [
                'style' => 'min-width:80px',
                'class' => 'col_report_selling_speed_storage ' . ($userConfig->report_selling_speed_storage ? '' : 'hidden')

            ],
            'contentOptions' => [
                'class' => 'clause-cell col_report_selling_speed_storage ' . ($userConfig->report_selling_speed_storage ? '' : 'hidden')
            ],
            's2width' => '200px',
            'hideSearch' => false,
            'filter' => $searchModel->filterStorage(),
            'format' => 'raw',
            'value' => function (Product $model) {
                return $model->storeName;
            },
        ],
    ],
]); ?>

<?= SummarySelectWidget::widget([
        'buttons' => [
            Html::a('<span>Заказать</span>', '#create-order-document', [
                'class' => 'button-clr button-regular button-width button-hover-transparent create-product-order-document',
                'data-toggle' => 'modal',
            ]),
        ],
]); ?>

<div class="modal fade" id="create-order-document" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Вы уверены, что хотите подготовить заказы по поставщикам?</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <div class="order-list text-left mt-3 mb-3">
                </div>
                <div class="text-center">
                    <?= \yii\bootstrap\Html::a('Да', null, [
                        'class' => 'btn-confirm-yes btn-save button-clr button-regular button-hover-transparent button-width-medium mr-2',
                        'data-dismiss' => 'modal',
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>

<?php

$this->registerJs(<<<JS
    $('.order-list').append($('<ol></ol>'));

    $(document).on('change', '.joint-operation-checkbox', function(e){
        $('.order-list ol').html("");
        
        $('.joint-operation-checkbox:checked').each(function(index){
            el = $('<li></li>').append('<div>' + $(this).data('product-name') + ", " + $(this).data('quantity') + " " + $(this).data('unit') + '. поставщик ' + $(this).data('contractor-name') + '</div');
            $('.order-list ol').append(el);
        })        
                
    })
    
    $(document).on('click', '.btn-save', function(){
        var data = [];
        
        $('.joint-operation-checkbox:checked').each(function(index){
            data.push({
                OrderDocument: {
                    'contractor_id': $(this).data('contractor-id'),
                    'store_id': $(this).data('store_id')
                },
                orderArray: [{
                    'id': $(this).data('product-id'),
                    'title': $(this).data('product-name'),
                    'model': 'product',
                    'count': $(this).data('quantity'),
                    'price': $(this).data('product-price') / 100,
                    'priceOneWithVat': $(this).data('product-price-with-vat') / 100
                }]
            });
        })
        
        $.post('/documents/order-document/many-create?type=1', {data: JSON.stringify(data)}, function(data){
            if ('true' === data.success) {
                window.toastr.success("Заказ успешно создан.", "", {
                    "closeButton": true,
                    "showDuration": 1000,
                    "hideDuration": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 1000,
                    "escapeHtml": false
                });
            }
        }, 'json');
    })
JS
);