<?php

use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\assets\AnalyticsAsset;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\analytics\models\ProductAnalysisABC;
use frontend\widgets\TableConfigWidget;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisABC */
/* @var $dataProvider \yii\data\ActiveDataProvider */

AnalyticsAsset::register($this);

$this->title = 'ABC анализ';
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->product_abc_help ?? false;
$showChartPanel = $userConfig->product_abc_chart ?? false;

$SHOW_BY_GROUPS = $userConfig->report_abc_show_groups;
$tabConfig = [
    'article' => 'col_report_abc_article' . ($userConfig->report_abc_article ? '' : ' hidden'),
    'cost_price' => 'col_report_abc_cost_price' . ($userConfig->report_abc_cost_price ? '' : ' hidden'),
    'purchase_price' => 'col_report_abc_purchase_price' . ($userConfig->report_abc_purchase_price ? '' : ' hidden'),
    'selling_price' => 'col_report_abc_selling_price' . ($userConfig->report_abc_selling_price ? '' : ' hidden'),
    'group_margin' => 'col_report_abc_group_margin' . ($userConfig->report_abc_group_margin ? '' : ' hidden'),
    'number_of_sales' => 'col_report_abc_number_of_sales' . ($userConfig->report_abc_number_of_sales ? '' : ' hidden'),
    'average_check' => 'col_report_abc_average_check' . ($userConfig->report_abc_average_check ? '' : ' hidden'),
    'comment' => 'col_report_abc_comment' . ($userConfig->report_abc_comment ? '' : ' hidden')
];
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_abc');

$products = $dataProvider->getModels();
$prevProducts = &$searchModel->prevPeriodData;

$model = new \common\models\product\Product();
$model->company_id = Yii::$app->user->identity->company_id;
foreach ($products as &$product) {

    $model->setDateStart(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year)));
    $model->setDateEnd(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)));
    $product['balance_at_start'] = $model->balanceAtDate(true, [$product['product_id']]);
    $product['balance_at_end'] = $model->balanceAtDate(false,  [$product['product_id']]);
    $product['irreducible_quantity'] = array_sum($model->getIrreducibleQuantity(null, \Yii::$app->db2));
}

$model->setDateEnd(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)));
$stockQuantity = $model->balanceAtDate(false, array_column($products, 'product_id'), true, Yii::$app->db2);
$model->setDateEnd((new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)))->modify("-1 month"));
$prevStockQuantity = $model->balanceAtDate(false, array_column($products, 'product_id'), true, Yii::$app->db2);

if ($SHOW_BY_GROUPS) {
    $groups = $searchModel->getGroupsByProducts($products, true);
    $prevGroups = $searchModel->getGroupsByProducts($prevProducts, false);
} else {
    $groups = &$products;
    $prevGroups = &$prevProducts;
}

$colors = [
    ProductAnalysisABC::GROUP_A => 'rgba(38, 205, 88, 0.25)',
    ProductAnalysisABC::GROUP_B => 'rgba(200, 205, 38, 0.25)',
    ProductAnalysisABC::GROUP_C => 'rgba(227, 6, 17, 0.25)',
]
?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap4\Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pr-0 pl-1">
                <?= \yii\bootstrap\Html::beginForm(['abc'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '155px',
                    ],
                ]); ?>
                <?= \common\components\helpers\Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="product_abc_chart">
    <div class="pt-4 pb-3">
        <div class="row">
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ВЫРУЧКА',
                    'ico' => 'ico_turnover.svg',
                    'value' => 1/100 * array_sum(array_column($products, 'turnover')),
                    'prevValue' => 1/100 * array_sum(array_column($prevProducts, 'turnover')),
                    'units' => '₽',
                    'borders' => ['bottom', 'right']
                ]) ?>
            </div>
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'МАРЖА',
                    'ico' => 'ico_margin.svg',
                    'value' => 1/100 * array_sum(array_column($products, 'margin')),
                    'prevValue' => 1/100 * array_sum(array_column($prevProducts, 'margin')),
                    'units' => '₽',
                    'borders' => ['bottom', 'right']
                ]) ?>
            </div>
            <div class="col-4 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ОСТАТОК НА СКЛАДЕ',
                    'ico' => 'ico_store.svg',
                    'value' => $stockQuantity,
                    'prevValue' => $prevStockQuantity,
                    'units' => 'шт.',
                    'borders' => ['bottom']
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ОБЪЕМ ПРОДАЖ',
                    'ico' => 'ico_sells_quantity.svg',
                    'value' => array_sum(array_column($products, 'selling_quantity')),
                    'prevValue' => array_sum(array_column($prevProducts, 'selling_quantity')),
                    'units' => 'шт.',
                    'borders' => ['right']
                ]) ?>
            </div>
            <div class="col-4 pl-0 pr-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => '% МАРЖИ',
                    'ico' => 'ico_margin_percent.svg',
                    'value' => (count($products) > 0) ? 100 * array_sum(array_column($products, 'margin_percent')) / count($products) : 0,
                    'prevValue' => (count($prevProducts) > 0) ? 100 * array_sum(array_column($prevProducts, 'margin_percent')) / count($prevProducts) : 0,
                    'units' => '%',
                    'borders' => ['right']
                ]) ?>
            </div>
            <div class="col-4 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'СРЕДНИЙ ЧЕК',
                    'ico' => 'ico_average_check.svg',
                    'value' => (count($products) > 0) ? 1/100 * array_sum(array_column($products, 'average_check')) / count($products) : 0,
                    'prevValue' => (count($prevProducts) > 0) ? 1/100 * array_sum(array_column($prevProducts, 'average_check')) / count($prevProducts) : 0,
                    'units' => '₽',
                    'borders' => []
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="product_abc_help">
    <div class="pt-4 pb-3">
        ...
        <?php
        //echo $this->render('_partial/odds-panel')
        ?>
    </div>
</div>


<?php
//$pjax = Pjax::begin([
//    'id' => 'abc-analysis-pjax',
//    'timeout' => 10000,
//]);
?>

<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => array_filter([
                [
                    'attribute' => 'report_abc_article',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_cost_price',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_selling_price',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_purchase_price',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_group_margin',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_number_of_sales',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_average_check',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
                [
                    'attribute' => 'report_abc_comment',
                    'callback-object' => 'CSTable',
                    'callback-method' => 'toggleHiddenColumn'
                ],
            ]),
            'sortingItemsTitle' => 'Выводить',
            'sortingItems' => [
                [
                    'attribute' => 'show_groups',
                    'label' => 'По группам товара',
                    'checked' => $SHOW_BY_GROUPS,
                ],
                [
                    'attribute' => 'dont_show_groups',
                    'label' => 'Без групп товара',
                    'checked' => !$SHOW_BY_GROUPS,
                ],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_report_abc']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск по наименованию или артикулу',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div id="cs-table-11x" class="custom-scroll-table-double cs-top">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="cs-table-2x">
    <div class="table-wrap">
        <table class="table table-style table-count-list">
            <!--CSTable.setStickyCell-->
        </table>
    </div>
    <div class="fixed-first-cell">
        <!--CSTable.setStickyHeader-->
    </div>
</div>

<div id="abc-analysis-grid">
    <div class="wrap wrap_padding_none_all" style="position:relative!important;">
        <div id="cs-table-1x" class="custom-scroll-table-double">
            <div class="table-wrap">
                <table class="table table-style table-count-list analysis-table <?= $tabViewClass ?>" aria-describedby="datatable_ajax_info" role="grid">
                    <thead>
                    <tr>
                        <th>
                            <?php if ($SHOW_BY_GROUPS): ?>
                                <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger-cs>
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey weight-700 ml-1">Группа товара</span>
                                </button>
                            <?php else: ?>
                                Название товара
                            <?php endif; ?>
                        </th>
                        <th rowspan="2" class="<?= $tabConfig['article'] ?>">Артикул</th>
                        <th colspan="<?= ProductAnalysisABC::getFluidColspan($userConfig) ?>" class="height-30">
                            <button class="collapse-cell-data table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-cs
                                    data-target="cell-data"
                                    data-colspan-close="4"
                                    data-colspan-open="<?= ProductAnalysisABC::getFluidColspan($userConfig) ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Данные</span>
                            </button>
                        </th>
                        <th colspan="2">
                            <button class="collapse-cell-result table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-cs
                                    data-target="cell-result"
                                    data-colspan-close="1"
                                    data-colspan-open="2">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Результат</span>
                            </button>
                        </th>
                        <th colspan="7">
                            <button class="collapse-cell-guidance table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-cs
                                    data-target="cell-guidance"
                                    data-colspan-close="4"
                                    data-colspan-open="7">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Рекомендации</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button class="collapse-cell-store table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-cs
                                    data-target="cell-store"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Склад</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button class="collapse-cell-order table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-cs
                                    data-target="cell-order"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Заказ</span>
                            </button>
                        </th>
                        <th rowspan="2" class="<?= $tabConfig['comment'] ?>">Комментарий</th>
                    </tr>
                    <tr id="abc-analysis-grid-filters" class="filters">
                        <th><!-- need for css-positioning --></th>
                        <!--<th width="10%">Название</th>
                        <th width="10%">Артикул</th>-->

                        <!-- DATA -->
                        <th width="10%" class="height-30">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Выручка', 'attr' => 'turnover']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['cost_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Себестоимость', 'attr' => 'cost_price']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['selling_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Цена продажи', 'attr' => 'selling_price']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['purchase_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Цена покупки', 'attr' => 'purchase_price']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Маржа ₽', 'attr' => 'margin']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => '% Маржи', 'attr' => 'margin_percent']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Объем продаж', 'attr' => 'selling_quantity']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['group_margin'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Доля маржи в группе', 'attr' => 'group_margin_percent']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['number_of_sales'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Кол-во сделок', 'attr' => 'number_of_sales']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['average_check'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Средний чек', 'attr' => 'average_check']) ?>
                        </th>

                        <!-- RESULTS -->
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'АВС', 'attr' => 'abc']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-result">
                            <?= $this->render('partial/sort_arrows', ['title' => 'XYZ', 'attr' => 'variation_coefficient']) ?>
                        </th>

                        <!-- GUIDANCE -->
                        <th width="10%">
                            Наличие
                        </th>
                        <th width="10%">
                            Реклама
                        </th>
                        <th width="10%">
                            Скидки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Акции
                        </th>
                        <th width="10%">
                            Цена продажи
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Цена закупки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Вывод из ассортимента
                        </th>

                        <!-- STORE -->
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Остаток на начало
                        </th>
                        <th width="10%">
                            Остаток на конец
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Неснижаемый остаток
                        </th>

                        <!-- ORDER -->
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Мин. рекомендуе&shy;мый заказ (ед.)
                        </th>
                        <th width="10%">
                            Заказ (ед.)
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Сумма заказа
                        </th>

                    </tr>
                    </thead>
                    <?php if (!empty($products)): ?>
                    <tbody>
                    <?php foreach ($groups as $key => $group): ?>
                        <?php $isGroup = !empty($group['items']); ?>
                        <?php $prevGroup = $prevGroups[$key] ?? []; ?>
                        <?php $recommendations = $searchModel::getRecommendations($group['abc'], $group['variation_coefficient']); ?>

                        <?= $this->render('partial/table_row', [
                            'searchModel' => $searchModel,
                            'key' => $key,
                            'tabConfig' => $tabConfig,
                            'product' => $group,
                            'prevProduct' => $prevGroup,
                            'isGroup' => $isGroup,
                            'colors' => $colors,
                            'recommendations' => $recommendations
                        ]); ?>

                        <?php if (!empty($group['items'])) foreach ($group['items'] as &$product): ?>
                            <?php $prevProduct = $prevProducts[$product['product_id']] ?? []; ?>
                            <?php $recommendations = $searchModel::getRecommendations($product['abc'], $product['variation_coefficient']); ?>

                            <?= $this->render('partial/table_row', [
                                'searchModel' => $searchModel,
                                'key' => $key,
                                'tabConfig' => $tabConfig,
                                'product' => $product,
                                'prevProduct' => $prevProduct,
                                'isGroupRow' => true,
                                'colors' => $colors,
                                'recommendations' => $recommendations
                            ]); ?>

                        <?php endforeach; ?>
                    <?php endforeach; ?>

                    <?= $this->render('partial/table_totals', [
                        'searchModel' => $searchModel,
                        'key' => $key,
                        'tabConfig' => $tabConfig,
                        'products' => $products,
                        'prevProducts' => $prevProducts
                    ]); ?>

                    </tbody>
                    <?php else: ?>
                        <tr>
                            <td colspan="11">Нет данных за выбранный период</td>
                            <td colspan="11"></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
//$pjax->end();
?>

<?php

$this->registerJs(<<<JS

    ///////////////
    CSTable.init();
    ///////////////

    $('.table-settings [type="checkbox"]').on('change', function () {
        const DATA_CELLS_CNT = 10;
        const collapseColBtn = $('.collapse-cell-data');
        const calcColspan = DATA_CELLS_CNT - (
            ($('#config-report_abc_cost_price').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_group_margin').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_number_of_sales').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_average_check').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_purchase_price').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_selling_price').prop('checked') ? 0 : 1)
        );

        $(collapseColBtn).data('colspan-open', calcColspan);
        if ($(collapseColBtn).hasClass('active')) {
            $(collapseColBtn).closest('th').attr('colspan', calcColspan);
        }
    });

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
JS
, $this::POS_READY);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);