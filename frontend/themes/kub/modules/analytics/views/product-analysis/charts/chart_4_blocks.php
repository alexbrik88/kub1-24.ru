<?php
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductSearch;
use frontend\modules\analytics\components\ProductTurnoverHelper;
use frontend\modules\analytics\models\AbstractFinance;

$turnoverData = ProductTurnoverHelper::analyticsProductDashboard4blocks($company->id);
$sells = $margin = $buys = $balance = [];

$dateStart = date_create('first day of -1 month');
$sells['today'] = $turnoverData['sellsToday'] / 100;
$buys['today'] = $turnoverData['buysToday'] / 100;
$margin['today'] = $turnoverData['marginToday'] / 100;
$balance['today'] = $turnoverData['balanceToday'] / 100;
$sells['yesterday'] = $turnoverData['sellsYesterday'] / 100;
$buys['yesterday'] = $turnoverData['buysYesterday'] / 100;
$margin['yesterday'] = $turnoverData['marginYesterday'] / 100;
$balance['yesterday'] = $turnoverData['balanceYesterday'] / 100;
$sells['month'] = $turnoverData['sellsThisMonth'] / 100;
$buys['month'] = $turnoverData['buysThisMonth'] / 100;
$margin['month'] = $turnoverData['marginThisMonth'] / 100;
$balance['month'] = $turnoverData['balanceThisMonth'] / 100;
$sells['prev_month'] = $turnoverData['sellsLastMonth'] / 100;
$buys['prev_month'] = $turnoverData['buysLastMonth'] / 100;
$margin['prev_month'] = $turnoverData['marginLastMonth'] / 100;
$balance['prev_month'] = $turnoverData['balanceLastMonth'] / 100;

?>

<div class="row">

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Продано
                <?= $this->render('_arrow', [
                    'today' => $sells['today'],
                    'yesterday' => $sells['yesterday']
                ]) ?>
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($sells['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($sells['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($sells['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($sells['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Маржа
                <?= $this->render('_arrow', [
                    'today' => $margin['today'],
                    'yesterday' => $margin['yesterday']
                ]) ?>
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($margin['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($margin['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($margin['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($margin['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Закуплено
                <?= $this->render('_arrow', [
                    'today' => $buys['today'],
                    'yesterday' => $buys['yesterday']
                ]) ?>
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($buys['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($buys['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($buys['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($buys['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Остаток
                <?= $this->render('_arrow', [
                    'today' => $balance['today'],
                    'yesterday' => $balance['yesterday']
                ]) ?>
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($balance['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($balance['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($balance['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($balance['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
