<?php
/** @var int $today */
/** @var int $yesterday */
?>
<?php if ($today != $yesterday): ?>
    <img class="abc-arrow" width="14" src="/img/abc/arrow_<?= ($today > $yesterday ? 'green':'red')?>.svg" style="float:right"/>
<?php endif; ?>