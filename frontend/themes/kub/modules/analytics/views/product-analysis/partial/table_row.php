<?php
use yii\helpers\ArrayHelper;
/**
  * @var $searchModel \frontend\modules\analytics\models\ProductAnalysisABC
  * @var $key int
  * @var $tabConfig array
  * @var $product array
  * @var $prevProduct array
  * @var $recommendations array
  * @var $isGroup bool
  * @var $isGroupRow bool
 */

$isGroup = $isGroup ?? false;
$isGroupRow = $isGroupRow ?? false;
?>


<tr data-key="<?= $key ?>" <?= ($isGroupRow) ? "data-id=\"$key\"" : null ?> class="<?= ($isGroupRow) ? 'd-none' : '' ?>">
    <!--Название-->
    <td class="col-name <?= $isGroup ? 'col-name-group' : '' ?> <?= $isGroupRow ? 'col-name-group-row' : '' ?>">
        <?php if ($isGroup): ?>
            <button class="table-collapse-btn button-clr ml-1 text-left" type="button" data-collapse-row-trigger-cs data-target="<?= $key ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="d-block text_size_14 ml-1" title="<?= htmlspecialchars($product['title']) ?>">
                    <?= $product['title'] ?>
                </span>
            </button>
        <?php else: ?>
            <span title="<?= htmlspecialchars($product['title']) ?>">
                <?= $product['title'] ?>
            </span>
        <?php endif; ?>
    </td>
    <!--Артикул-->
    <td class="<?= $tabConfig['article'] ?>">
        <?= $product['article'] ?>
    </td>

    <!--Оборот-->
    <td class="text-right">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['turnover'],
            'prevValue' => 1/100 * ($prevProduct['turnover'] ?? 0),
        ]); ?>
    </td>
    <!--Себестоимость-->
    <td class="text-right <?= $tabConfig['cost_price'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['cost_price'],
            'prevValue' => 1/100 * ($prevProduct['cost_price'] ?? 0),
        ]); ?>
    </td>
    <!--Цена продажи-->
    <td class="text-right <?= $tabConfig['selling_price'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['selling_price'],
            'prevValue' => 1/100 * ($prevProduct['selling_price'] ?? 0),
        ]); ?>
    </td>
    <!--Цена покупки-->
    <td class="text-right <?= $tabConfig['purchase_price'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['purchase_price'],
            'prevValue' => 1/100 * ($prevProduct['purchase_price'] ?? 0),
        ]); ?>
    </td>
    <!--Маржа Р-->
    <td class="text-right"  style="background-color: <?= $colors[substr($product['abc'], 0, 1)] ?? null ?>">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['margin'],
            'prevValue' => 1/100 * ($prevProduct['margin'] ?? 0),
            'isNew' => (date('Ym', $product['first_date']) >= $searchModel->year),
        ]); ?>
    </td>
    <!--% Маржи-->
    <td class="text-right"  style="background-color: <?= $colors[substr($product['abc'], 1, 1)] ?? null ?>">
        <?= $this->render('triple_cell', [
            'value' => 100 * $product['margin_percent'],
            'prevValue' => 100 * ($prevProduct['margin_percent'] ?? 0),
            'isNew' => (date('Ym', $product['first_date']) >= $searchModel->year),
        ]); ?>
    </td>
    <!--Кол-во продано-->
    <td class="text-right"  style="background-color: <?= $colors[substr($product['abc'], 2, 1)] ?? null ?>">
        <?= $this->render('triple_cell', [
            'value' => $product['selling_quantity'],
            'prevValue' => $prevProduct['selling_quantity'] ?? 0,
            'isNew' => (date('Ym', $product['first_date']) >= $searchModel->year),
        ]); ?>
    </td>
    <!--Доля маржи в группе-->
    <td class="text-right <?= $tabConfig['group_margin'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 100 * $product['group_margin_percent'],
            'prevValue' => 100 * ($prevProduct['group_margin_percent'] ?? 0),
        ]); ?>
    </td>
    <!--Кол-во продаж-->
    <td class="text-right <?= $tabConfig['number_of_sales'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => $product['number_of_sales'],
            'prevValue' => $prevProduct['number_of_sales'] ?? 0,
            'prevFirstDate' => ($prevProduct['first_date'] ?? null),
            'currDate' => $searchModel->year,
            'int' => true,
        ]); ?>
    </td>
    <!--Средний чек-->
    <td class="text-right <?= $tabConfig['average_check'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * $product['average_check'],
            'prevValue' => 1/100 * ($prevProduct['average_check'] ?? 0),
        ]); ?>
    </td>

    <!--АВС комбинация-->
    <td class="text-right">
        <?= str_replace([1,2,3], ['A', 'B', 'C'], $product['abc']) ?>
    </td>
    <!--XYZ значение-->
    <td class="text-right" data-collapse-cell data-id="cell-result" data-variation-coefficient="<?= $product['variation_coefficient'] ?>">
        <?php
        if ($product['variation_coefficient'] <= 10)
            echo 'X';
        elseif ($product['variation_coefficient'] > 10 && $product['variation_coefficient'] <= 25)
            echo 'Y';
        else
            echo 'Z';
        ?>
    </td>

    <!-- GUIDANCE -->
    <td>
        <?= ArrayHelper::getValue($recommendations, 'irreducible_quantity') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'advertising') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'discount') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'action') ?>
    </td>
    <td>
        <?= ArrayHelper::getValue($recommendations, 'selling_price') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'purchase_price') ?>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
        <?= ArrayHelper::getValue($recommendations, 'out_from_range') ?>
    </td>

    <!-- STORE -->
    <td class="text-right" data-collapse-cell data-id="cell-store">
        <?= Yii::$app->formatter->asNumber(ArrayHelper::getValue($product, 'balance_at_start')) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber(ArrayHelper::getValue($product, 'balance_at_end'))?>
    </td>
    <td class="text-right" data-collapse-cell data-id="cell-store">
        <?= ArrayHelper::getValue($product, 'irreducible_quantity') ?>
    </td>

    <!-- ORDER -->
    <td class="text-right" data-collapse-cell data-id="cell-order">
    </td>
    <td class="text-right">
    </td>
    <td class="text-right" data-collapse-cell data-id="cell-order">
    </td>

    <!--Комментарий-->
    <td class="<?= $tabConfig['comment'] ?>">
    </td>

</tr>
