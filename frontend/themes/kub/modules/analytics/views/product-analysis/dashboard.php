<?php

use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $data []
 * @var $expenditureItems []
 * @var $types []
 * @var $balance []
 * @var $growingBalance []
 * @var $blocks []
 * @var $warnings []
 * @var $checkMonth boolean
 * @var $activeTab integer
 * @var $periodSize string
 */

$this->title = 'Дашборд по товарам';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

$userConfig = Yii::$app->user->identity->config;

?>

<div class="wrap" style="padding: 10px 15px 5px 20px">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pl-1 pr-0">
            </div>
        </div>
    </div>
</div>

<div class="in-row-wrap-p-20">
    <?php // 4 blocks
    echo $this->render('charts/chart_4_blocks', [
        'company' => $company,
    ]); ?>
</div>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-2">
    <div class="pt-4 pb-3">
        <?php
        // Chart
        echo $this->render('charts/chart_plan_fact_days', [
            'company' => $company,
            'searchModel' => $searchModel,
        ]); ?>
    </div>
</div>