<?php

use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-multi-company-btn',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

$multiCompanyManager = \Yii::$app->multiCompanyManager;
$disabled = !$multiCompanyManager->getIsModeEnabled() || !empty($temporaryDisabled);
?>

<div class="column <?= $wrapperClass ?? 'pr-2' ?>">
    <div style="display: inline-block; position: relative;">
        <?= Html::button(Icon::get('three-squares-plus'), [
            'class' => 'tooltip-multi-company-btn button-regular button-list button-hover-transparent button-clr mb-2' .
                ($disabled ? ' disabled' : '') .
                (!$multiCompanyManager->getIsModeOn() ? ' collapsed' : ''),
            'data-toggle' => 'modal',
            'href' => '#modal-multi-company',
            'data-tooltip-content' => '#tooltip_multi_company_collapse',
            'disabled' => $disabled,
            'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
        ]) ?>

        <div style="display: none">
            <div id="tooltip_multi_company_collapse">
                <?= ($multiCompanyManager->getIsModeEnabled()) ?
                    ($multiCompanyManager->getIsModeOn() ?
                        'Отключение режима консолидированной отчетности' :
                        'Включение режима консолидированной отчетности') :
                    'Кнопка  режима консолидированной отчетности не доступна, когда в аккаунте только одна компания'
                ?>
            </div>
        </div>

        <?php if ($disabled) : ?>
            <?= Html::tag('div', '', [
                'title' => 'Доступно, когда в аккаунте 2 и более компании',
                'style' => 'position: absolute; top: 0; bottom: 0; left: 0; right: 0; cursor: not-allowed;'
            ]) ?>
        <?php endif ?>
    </div>
</div>
