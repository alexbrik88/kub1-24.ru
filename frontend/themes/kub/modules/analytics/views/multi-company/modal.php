<?php

use common\components\helpers\Html;
use frontend\modules\analytics\models\AnalyticsMultiCompanyForm;
use yii\bootstrap4\ActiveForm;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;

$multiCompanyManager = \Yii::$app->multiCompanyManager;
$multiCompanyForm = new AnalyticsMultiCompanyForm([
    'employee_id' => Yii::$app->user->identity->id,
    'primary_company_id' => Yii::$app->user->identity->company_id
]);

$companiesList = $multiCompanyManager->getAllowedCompaniesList();
$theFirstWord = ($multiCompanyManager->getIsModeOn()) ? 'Отключить' : 'Включить';
?>
<!-- Modal Many Delete -->
<div id="modal-multi-company" class="modal-multi-company confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-left mb-4">
                <?=($theFirstWord)?> режим консолидированной отчетности
            </h4>

            <?php $form = ActiveForm::begin([
                'action' => $multiCompanyForm::CHANGE_MODE_URL,
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            ]); ?>

            <?= $form->field($multiCompanyForm, 'isModeEnabled')->hiddenInput()->label(false) ?>

            <div class="row">
                <?= $form->field($multiCompanyForm, 'selectedCompaniesIds', [
                    'template' => "{input}\n{error}",
                    'options' => [
                        'class' => 'col-12 form-group'
                    ],
                    'labelOptions' => [
                        'class' => 'label mb-4',
                    ],
                ])->checkboxList($companiesList, [
                    'class' => 'row',
                    'item' => function ($index, $label, $name, $checked, $value) use ($multiCompanyForm) {
                        return Html::tag('label', Html::checkbox($name, $checked, [
                                'value' => $value,
                                'class' => 'company-id-checker',
                                'disabled' => $multiCompanyForm->isModeEnabled,
                            ]) . '<div style="display:inline-block;">'.$label.'</div>', [
                            'class' => 'col-4',
                        ]);
                    },
                ])->label(false); ?>
            </div>

            <div class="mt-3 d-flex justify-content-between">
                <?= \yii\helpers\Html::submitButton($theFirstWord, [
                    'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::button('Отменить', [
                    'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                    'style' => 'width: 130px!important;',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>

            <?php $form->end(); ?>

        </div>
    </div>
</div>