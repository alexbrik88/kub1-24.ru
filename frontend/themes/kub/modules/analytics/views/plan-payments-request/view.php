<?php

use yii\helpers\Html;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChat as Chat;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChatUser as ChatUser;
use common\models\Company;
use common\models\employee\Employee;

/**
 * @var Employee $user
 * @var Company $company
 * @var PaymentsRequest $model
 * @var ChatUser $chatUser
 * @var $tab
 * @var $tabData
 */

$this->title = $model->title;

$canDelete = $canSend = true; // todo
$canApprove = $model->canApprovePlanFlows($user);

$_renderParams = [
    'user' => $user,
    'company' => $company,
    'model' => $model,
    'chatUser' => $chatUser,
    'tab' => $tab,
    'tabData' => $tabData,
    'canApprove' => $canApprove,
    'canDelete' => $canDelete,
    'canSend' => $canSend,
];
?>

<?= Html::a('Назад к списку', ['index'], [
    'class' => 'link mb-2',
]) ?>

<div class="stop-zone-for-fixed-elems">
    <?= $this->render('view/_card', $_renderParams) ?>
    <?= $this->render('view/_tabs', $_renderParams) ?>
</div>