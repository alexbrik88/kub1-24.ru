<?php
/** @var $tabData */

use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<div class="table-settings row row_indents_s mt-1">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'payments_request_order_priority'],
                ['attribute' => 'payments_request_order_industry'],
                ['attribute' => 'payments_request_order_sale_point'],
                ['attribute' => 'payments_request_order_project'],
                ['attribute' => 'payments_request_order_invoice'],
                ['attribute' => 'payments_request_order_responsible'],
            ],
        ]); ?>
    </div>
</div>


<?php Pjax::begin([
    'id' => 'pjax-flow-grid',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
]); ?>

<?= $this->render('tabs_flows/_list', [
    'user' => $user,
    'company' => $company,
    'model' => $model,
    'searchModel' => $tabData['searchModel'],
    'dataProvider' => $tabData['dataProvider'],
    'canApprove' => $canApprove,
    'canDelete' => $canDelete,
    'canSend' => $canSend
]) ?>

<?php Pjax::end(); ?>

<?= $this->render('tabs_flows/_buttons', [
    'model' => $model,
    'canSend' => $canSend,
    'canDelete' => $canDelete,
]); ?>

<script>
    $(document).on('click', '#many-approve-plan-flows, #many-dismiss-approve-plan-flows', function() {
        const that = this;
        const checkboxes = $('.joint-operation-checkbox:checked');
        if (!$(that).hasClass('clicked')) {
            if ($(checkboxes).length > 0) {
                $(that).addClass('clicked');
                $.post($(that).data('url'), $(checkboxes).serialize(), function (data) {
                    $(that).removeClass('clicked');
                    $.pjax.reload("#pjax-flow-grid", {push: false, replace: false, scrollTo: false});
                    if (data.message !== undefined) {
                        window.toastr.success(data.message);
                    }
                });
            }
        }
    })
</script>
