<?php

use frontend\components\Icon;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="wrap wrap_btns visible mb-0 actions-buttons" id="payments-request-actions-buttons" style="position: sticky!important; bottom:0;">
    <?php $form = ActiveForm::begin([
        'id' => 'form-payments-request-chat',
        'method' => 'POST',
        'action' => Url::to(['add-chat-message', 'payments_request_id' => $model->id]),
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>
    <div class="d-flex" style="margin-bottom: -30px">
        <div class="form-group w-100 mr-2">
            <?= $form->field($chat, 'message')->textInput([
                'type' => 'text',
                'placeholder' => 'Введите сообщение',
                'class' => 'form-control'
            ])->label(false); ?>
        </div>
        <div class="form-group flex-grow-1">
            <?= Html::submitButton(Icon::get('check-2'), [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
    </div>

    <?php $form->end(); ?>

</div>