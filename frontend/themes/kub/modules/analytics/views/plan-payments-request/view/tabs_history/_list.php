<?php
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestHistory as History;

/** @var $list History[] */

foreach ($list as $l): ?>

    <div class="mt-2 p-2 border rounded">
        <?= date('d.m.Y H:i', $l->created_at) . ' ' . $l->message . ' ' . $l->author->getShortFio() ?>
    </div>

<?php endforeach; ?>
