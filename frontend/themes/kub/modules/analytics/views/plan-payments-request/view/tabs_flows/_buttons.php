<?php

use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url; ?>

<div class="wrap wrap_btns visible mb-0 actions-buttons" id="payments-request-actions-buttons" style="position: sticky!important; bottom:0;">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canSend) : ?>
                <?=Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    //'data-toggle' => 'toggleVisible',
                    //'data-target' => 'invoice',
                    'disabled' => true
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::button($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>',
                //[
                //    'document-print',
                //    'actionType' => 'print',
                //    'id' => $model->id,
                //    'filename' => $model->getPrintTitle(),
                //],
                [
                    'target' => '_blank',
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'disabled' => true
                ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full no-after',
                    'data-toggle' => 'dropdown',
                    'disabled' => true
                ]); ?>
                <?php /* yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">XML</span> для ЭДО',
                            'encode' => false,
                            'url' => ['/edm/diadoc/packing-list', 'id' => $model->id],
                            'linkOptions' => ['target' => '_blank'],
                        ],
                    ],
                ]); */ ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canDelete): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить заявку</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'id' => $model->id,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить эту заявку?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные операции?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete-plan-flows', 'payments_request_id' => $model->id]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>


<script>
    $(document).on('change', '.joint-operation-checkbox', function(){
        const buttonsPanel = $('#payments-request-actions-buttons');
        const approveButton = $('#many-approve-plan-flows');
        const dismissApproveButton = $('#many-dismiss-approve-plan-flows');
        let canDismissApprove = true;
        if ($('.joint-operation-checkbox:checked').length) {
            buttonsPanel.hide();
            $('.joint-operation-checkbox:checked').each(function(i,ch) {
                if (!$(ch).data('can-dismiss-approve'))
                    canDismissApprove = false;
            });
            if (canDismissApprove) {
                dismissApproveButton.show();
                approveButton.hide();
            } else {
                dismissApproveButton.hide();
                approveButton.show();
            }
        } else {
            buttonsPanel.show();
        }
    });
    $(document).on('pjax:success', '#pjax-flow-grid', function() {
        const buttonsPanel = $('#payments-request-actions-buttons');
        buttonsPanel.show();
    });
</script>