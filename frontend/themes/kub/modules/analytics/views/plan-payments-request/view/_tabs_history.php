<?php
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestHistory as History;

/** @var PaymentsRequest $model */

$list = $tabData['list'] ?? History::find()
    ->where(['request_id' => $model->id])
    ->orderBy(['created_at' => SORT_ASC])
    ->all();

?>

<div class="row">
    <div class="col-12">
        <div class="wrap">
            <?= $this->render('tabs_history/_list', [
                'user' => $user,
                'company' => $company,
                'model' => $model,
                'list' => $list
            ]) ?>
        </div>
    </div>
</div>