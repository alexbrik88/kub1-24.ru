<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\product\Product;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestStatus as Status;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestStatisticsHelper as Statistics;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignStep as SignStep;
/**
 * @var Employee $user
 * @var Company $company
 * @var PaymentsRequest $model
 */

$canUpdate = true; // todo

$status = $model->status;
$statusName = $model->getStatusName();

$statusIcon = [
    Status::STATUS_APPROVAL => 'new-doc',
    Status::STATUS_SIGNING => 'envelope',
    Status::STATUS_SIGNED => 'check-2',
    Status::STATUS_REJECTED => 'stop',
];
$statusBackground = [
    Status::STATUS_APPROVAL => '#4679AE',
    Status::STATUS_SIGNING => '#FAC031',
    Status::STATUS_SIGNED => '#26cd58',
    Status::STATUS_REJECTED => '#f2f3f7',
];

$icon = ArrayHelper::getValue($statusIcon, $status->id);
$background = ArrayHelper::getValue($statusBackground, $status->id);
$color = $status->id == Status::STATUS_REJECTED ? '#001424' : '#ffffff';
$border = $status->id == Status::STATUS_REJECTED ? '#e2e5eb' : $background;

$cashStatisticInfo = Statistics::getPeriodStatisticInfo($model);
$balanceDate = min(date('Y-m-d'), $model->payment_date);

/** @var SignStep[] $signSteps */
$signSteps = $model->getSignSteps()->orderBy('step')->all();
?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-3 single-product-page">

    <div class="page-in row">

        <!-- CARD -->

        <div class="col-9 column pr-4">
            <div class="pr-2">
                <div class="row align-items-start justify-content-between">
                    <h4 class="column mb-2 mt-1 break-word-title">
                        <?= Html::encode($model->title); ?>
                    </h4>
                    <div class="column">
                        <?= DocumentLogWidget::widget([
                            'model' => $model,
                            'toggleButton' => [
                                'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                                'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                                'title' => 'Последние действия',
                            ]
                        ]); ?>
                        <?php if ($canUpdate): ?>
                            <a href="<?= Url::toRoute(['update', 'id' => $model->id]); ?>"
                               title="Редактировать"
                               class="button-regular button-regular_red button-clr w-44 mb-2 ml-1">
                                <svg class="svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                </svg>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>

                <div style="max-width: 840px">
                    <div class="ht-caption">

                    </div>
                    <table class="ht-table dashboard-cash" style="max-width: 640px">
                        <thead>
                            <tr class="ht-caption">
                                <td class="text-left" width="25%">ДЕНЬГИ</td>
                                <td class="text-right" width="25%"><span style="text-transform: none"> на </span> <?= DateHelper::format($balanceDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></td>
                                <td class="text-right" width="25%">Сумма заявки</td>
                                <td class="text-right" width="25%">Остаток</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($cashStatisticInfo as $key => $item) : ?>
                            <?php $isSubstring = ('substring' === substr($item['cssClass'], -9)); ?>
                            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                                <td class="text-left">
                                    <div>
                                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                                            <?= $item['typeName']; ?>
                                            <?php if ('toggle-string' != $item['cssClass']): ?>
                                                <i class="ht-empty left"></i>
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                </td>
                                <td class="nowrap text-right">
                                    <span class="ht-empty-wrap">
                                        <?= TextHelper::invoiceMoneyFormat($item['factEndBalance'], 2); ?>
                                        <i class="ht-empty right"></i>
                                    </span>
                                </td>
                                <td class="nowrap text-right">
                                    <span class="ht-empty-wrap">
                                        <?= TextHelper::invoiceMoneyFormat($item['planEndBalance'], 2); ?>
                                        <i class="ht-empty right"></i>
                                    </span>
                                </td>
                                <td class="text-right">
                                    <span class="ht-empty-wrap <?= $item['diffBalance'] < 0 ? 'red' : '' ?>">
                                        <?= TextHelper::invoiceMoneyFormat($item['diffBalance'], 2); ?>
                                        <i class="ht-empty right"></i>
                                    </span>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- SIDEBAR -->

        <div class="col-3 column pl-0">
            <div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
                <div class="column flex-grow-1 mt-xl-0" style="padding: 0 5px;">
                    <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
                            background-color: <?=$background?>;
                            border-color: <?=$border?>;
                            color: <?=$color?>;
                            ">
                        <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
                        <span id="request-status-name"><?= $statusName; ?></span>
                    </div>

                    <div id="sign-steps" class="about-card no-border pl-0 pb-0">

                        <button class="sign-steps-collapse link link_collapse link_bold button-clr mb-3 collapsed" type="button">
                            <span class="link-txt">Шаги подписания</span>
                            <svg class="link-shevron svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>

                        <?php $noCurrentEmployee = !in_array($user->id, ArrayHelper::getColumn($signSteps, 'employee_id')); ?>
                        <?php foreach ($signSteps as $signStep): ?>
                            <?php $isCurrentEmployee = $signStep->employee_id === $user->id; ?>
                            <?php $activeStep = $isCurrentEmployee || $noCurrentEmployee && $signStep->can_sign ?>
                            <div class="about-card-item mb-3 <?=($activeStep) ? 'active':'' ?> <?=($signStep->signed_at) ? 'b-l-green' : 'b-l-grey' ?>" style="display: <?= ($activeStep) ? 'block' : 'none' ?>">
                                <div class="pl-2">
                                    <span class="bold">Шаг №<?= $signStep->step ?>:</span>
                                    <span class="bold"><?= Html::encode($signStep->name) ?></span>
                                    <br/>
                                    <span class="text-grey">Подписант:</span>
                                    <span><?= $signStep->employee->getShortFio() ?></span>
                                    <br/>
                                    <?php if ($signStep->signed_at): ?>
                                        <span class="text-grey">Подписано:</span>
                                        <span><?= date('d.m.Y H:i', $signStep->signed_at) ?></span>
                                    <?php else: ?>
                                        <span class="about-card-signed_name text-grey">Ожидает подписания</span>
                                        <span class="about-card-signed_at"></span>
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <?php foreach ($signSteps as $signStep): ?>
                            <?php if ($signStep->employee_id === $user->id): ?>
                                <button class="button-clr button-regular button-sign <?= ($signStep->can_sign) ? '' : 'hidden' ?>" data-url="<?= Url::to(['sign-payments-request', 'id' => $model->id, 'step' => $signStep->step]) ?>">
                                    Подписать
                                </button>
                                <button class="button-clr button-regular button-unsign <?= ($signStep->can_unsign) ? '' : 'hidden' ?>" data-url="<?= Url::to(['unsign-payments-request', 'id' => $model->id, 'step' => $signStep->step]) ?>">
                                    Снять подпись
                                </button>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).on("click", "table.dashboard-cash .toggle-string .flow-label", function() {
        const container = $(this).closest('table');
        $($(this).data("target"), container).slideToggle(150);
    });
    $(document).on("click", "#sign-steps .sign-steps-collapse", function() {
        const container = $(this).closest('#sign-steps');
        const collapseBtn = this;
        $(collapseBtn).toggleClass('collapsed');
        if ($(collapseBtn).hasClass('collapsed')) {
            $(".about-card-item:not(.active)", container).slideUp();
        } else {
            $(".about-card-item", container).slideDown();
        }
    });
    $(document).on("click", "#sign-steps .button-sign", function() {
        const container = $(this).closest('#sign-steps');
        const url = $(this).data('url');
        const el = $('.about-card-item.active');
        const nextEl = $(el).next();
        const collapseBtn = $('.sign-steps-collapse');

        $.post(url, {}, function(data) {
            if (data.success) {
                $('.button-sign', container).addClass('hidden');
                $('.button-unsign', container).removeClass('hidden');
                $('#request-status-name').html(data.status_name);
                $(el).find('.about-card-signed_name').html(data.signed_name);
                $(el).find('.about-card-signed_at').html(data.signed_at);
                $(el).toggleClass('b-l-green b-l-grey');
                if (nextEl.is(':hidden') && collapseBtn.hasClass('collapsed')) {
                    $(nextEl).slideDown(150);
                }
            } else {
                toastr.error('Ошибка подписания');
            }
        });
    });
    $(document).on("click", "#sign-steps .button-unsign", function() {
        const container = $(this).closest('#sign-steps');
        const url = $(this).data('url');
        const el = $('.about-card-item.active');
        const nextEl = $(el).next();
        const collapseBtn = $('.sign-steps-collapse');

        $.post(url, {}, function(data) {
            if (data.success) {
                $('.button-unsign', container).addClass('hidden');
                $('.button-sign', container).removeClass('hidden');
                $('#request-status-name').html(data.status_name);
                $(el).find('.about-card-signed_name').html(data.signed_name);
                $(el).find('.about-card-signed_at').html(data.signed_at);
                $(el).toggleClass('b-l-green b-l-grey');
                if (nextEl.is(':visible') && collapseBtn.hasClass('collapsed')) {
                    $(nextEl).slideUp(150);
                }
            } else {
                toastr.error('Ошибка снятия подписи');
            }
        });
    });    
</script>