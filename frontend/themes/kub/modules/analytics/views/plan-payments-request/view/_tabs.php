<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChat as Chat;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChatUser as ChatUser;

/** @var PaymentsRequest $model */
/** @var ChatUser $chatUser */
/** @var $tab */
/** @var $tabData */

$unreadMessagesCount = $chatUser->getUnreadMessagesCount();
$unreadBadge = Html::tag('span', $unreadMessagesCount, ['class' => 'badge badge-blue']);
$unreadBadgeFloat = ($unreadMessagesCount)
    ? Html::tag('div', $unreadBadge, ['style' => 'position:absolute;right:17px; top:8px'])
    : null;
?>

<div class="pl-1">
    <div class="nav-tabs-row row">
        <?php // Pjax::begin([ 'id' => 'request-view-tabs-pjax', 'linkSelector' => '#request-view-tabs > .nav-item > .nav-link', 'timeout' => 10000, 'options' => [ 'style' => 'width:100%' ] ]); ?>

        <div class="col-12">
            <?= Tabs::widget([
            'id' => 'request-view-tabs',
            'encodeLabels' => false,
            'options' => [
                'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-2',
            ],
            'linkOptions' => [
                'class' => 'nav-link',
            ],
            'tabContentOptions' => [
                'class' => 'tab-pane pl-3 pr-3',
                'style' => 'width:100%'
            ],
            'headerOptions' => [
                'class' => 'nav-item',
            ],
            'items' => [
                [
                    'label' => 'Платежи',
                    'url' => Url::current(['tab' => null]),
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == '_flows' ? ' active' : ''),
                        'data-tab' => '_flows'
                    ],
                    'active' => $tab == '_flows'
                ],
                [
                    'label' => 'Чат' . $unreadBadgeFloat,
                    'url' => Url::current(['tab' => '_chat']),
                    'headerOptions' => [
                        'class' => 'nav-item ml-auto',
                        'style' => 'position:relative'
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == '_chat' ? ' active' : ''),
                        'data-tab' => '_chat'
                    ],
                    'active' => $tab == '_chat'
                ],
                [
                    'label' => 'История',
                    'url' => Url::current(['tab' => '_history']),
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == '_history' ? ' active' : ''),
                        'data-tab' => '_history'
                    ],
                    'active' => ($tab == '_history')
                ],
            ],
        ]); ?>
        </div>

        <div class="pl-3 pr-3 tab-content w-100">
            <?= $this->render('_tabs' . $tab, [
                'model' => $model,
                'user' => $user,
                'company' => $company,
                'tabData' => $tabData,
                'canApprove' => $canApprove,
                'canDelete' => $canDelete,
                'canSend' => $canSend,
            ]) ?>
        </div>

        <?php // Pjax::end(); ?>
    </div>
</div>
