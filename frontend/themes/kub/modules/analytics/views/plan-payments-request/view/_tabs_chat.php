<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use frontend\components\Icon;
use common\models\employee\Employee;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChat as Chat;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChatUser as ChatUser;

/** @var PaymentsRequest $model */
/** @var Employee $user */

$list = $tabData['list'] ?? Chat::find()
    ->where(['request_id' => $model->id, 'company_id' => $model->company_id])
    ->orderBy(['created_at' => SORT_ASC])
    ->all();



$chat = $tabData['chat'] ?? new Chat([
    'request_id' => $model->id,
    'company_id' => $model->company_id,
    'author_id' => $user->id
]);

?>

<?php Pjax::begin([
    'id' => 'pjax-payments-request-chat',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'formSelector' => '#form-payments-request-chat'
]); ?>

<div class="row">
    <div class="col-12">
        <div class="wrap">
            <?= $this->render('tabs_chat/_list', [
                'user' => $user,
                'company' => $company,
                'model' => $model,
                'list' => $list
            ]) ?>
        </div>
    </div>
</div>

<?= $this->render('tabs_chat/_form', [
    'user' => $user,
    'company' => $company,
    'model' => $model,
    'chat' => $chat
]) ?>

<?php Pjax::end(); ?>