<?php
use yii\helpers\Html;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChat as Chat;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChatUser as ChatUser;

/** @var $list Chat[] */

foreach ($list as $l): ?>

    <div class="mt-2 p-2 border rounded">
        <div class="text-muted">
            <?= date('d.m.Y H:i', $l->created_at) . ' ' . $l->author->getShortFio() ?>
        </div>
        <div>
            <?= Html::encode($l->message) ?>
        </div>
    </div>

<?php endforeach; ?>
