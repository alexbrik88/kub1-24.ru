<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSearch;
use yii\helpers\Url;

// AnalyticsAsset::register($this);

/** @var $searchModel PaymentsRequestSearch */

$this->title = 'Согласование платежей';

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');
$canDelete = $canAddSignTemplate = $canUpdateSignTemplate = true; // todo
$stat = $searchModel->getStat();

$exists = PaymentsRequest::find()->andWhere(['company_id' => $company->id])->exists();
$isFilter = (boolean)($searchModel->byNumber || $searchModel->status_id || $searchModel->author_id);
if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет заявок. Измените период, чтобы увидеть имеющиеся заявки.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одной заявки.';
}

?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto" style="padding-top: 6px; padding-bottom: 6px;">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
        </div>
    </div>
</div>

<div class="stop-zone-for-fixed-elems">
    <div class="wrap wrap_count">
        <div class="row">
            <?php foreach ($stat as $statusId => $statusData): ?>
            <div class="col-6 col-xl-5ths">
                <div class="sorting_block count-card count-card-smaller <?=($statusData['class'])?> wrap" data-url=<?= Url::to(['plan-payments-request', 'PaymentsRequestSearch[status_id]' => $statusId]); ?>>
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($statusData['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $statusData['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        Кол-во заявок: <?= $statusData['count'] ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="col-6 col-xl-5ths d-flex flex-column justify-content-top">
                <?= Html::a(Icon::get('add-icon') . '<span class="ml-2">Добавить заявку</span>', ['create'], [
                    'class' => 'button-regular w-100 button-hover-content-red',
                ]); ?>
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <?= $this->render('index/_sign_template_dropdown', [
                    'buttonName' => 'Шаблоны подписания',
                    'company' => $company,
                    'canAdd' => $canAddSignTemplate,
                    'canUpdate' => $canUpdateSignTemplate
                ]) ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'payments_request_date',
                    ],
                    [
                        'attribute' => 'payments_request_payments_count',
                    ],
                    [
                        'attribute' => 'payments_request_payments_status',
                    ],
                    [
                        'attribute' => 'payments_request_author',
                    ],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_payments_request']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'byNumber', [
                    'type' => 'search',
                    'placeholder' => 'Поиск по номеру заявки',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return Html::checkbox('PaymentsRequest[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->payments_sum
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата заявки',
                'headerOptions' => [
                    'class' => 'sorting col_payments_request_date' . ($userConfig->payments_request_date ? '' : ' hidden'),
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'col_payments_request_date' . ($userConfig->payments_request_date ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ заявки',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return Html::a($model->fullNumber, ['view', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'payment_date',
                'label' => 'Дата платежа',
                'headerOptions' => [
                    'class' => '',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return DateHelper::format($model->payment_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'payments_count',
                'label' => 'Кол-во платежей',
                'headerOptions' => [
                    'width' => '5%',
                    'class' => 'col_payments_request_payments_count' . ($userConfig->payments_request_payments_count ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'link-view col_payments_request_payments_count' . ($userConfig->payments_request_payments_count ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return TextHelper::numberFormat($model->payments_count, 0);
                },
            ],
            [
                'attribute' => 'payments_sum',
                'label' => 'Сумма',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'link-view text-right',
                ],
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    $price = TextHelper::invoiceMoneyFormat($model->payments_sum, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'sign_template_id',
                'label' => 'Шаблон',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'filter' => $searchModel->filterSignTemplate(),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return ($model->signTemplate) ? Html::encode($model->signTemplate->name) : '---';
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус заявки',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'filter' => $searchModel->filterStatus(),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return $model->getStatusName();
                },
            ],
            [
                'attribute' => 'payments_status',
                'label' => 'Статус платежа',
                'headerOptions' => [
                    'width' => '5%',
                    'class' => 'col_payments_request_payments_status' . ($userConfig->payments_request_payments_status ? '' : ' hidden'),
                ],
                'filter' => $searchModel->filterPaymentsStatus(),
                'contentOptions' => [
                    'class' => 'col_payments_request_payments_status' . ($userConfig->payments_request_payments_status ? '' : ' hidden'),
                ],                
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return null; // todo
                },
            ],
            [
                'attribute' => 'author_id',
                'label' => 'Инициатор',
                'headerOptions' => [
                    'class' => 'col_payments_request_author' . ($userConfig->payments_request_author ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_payments_request_author' . ($userConfig->payments_request_author ? '' : ' hidden'),
                ],
                's2width' => '200px',
                'filter' => $searchModel->filterAuthor(),
                'format' => 'raw',
                'value' => function (PaymentsRequestSearch $model) {
                    return $model->author ? $model->author->getFio(true) : '';
                },
            ],
        ],
    ]); ?>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        //$canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
        //    'many-document-print',
        //    'actionType' => 'pdf',
        //    'type' => $ioType,
        //    'multiple' => ''
        //], [
        //    'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
        //    'target' => '_blank',
        //]) : null,
        //$canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
        //    'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
        //    'data-url' => Url::to(['many-send', 'type' => $ioType]),
        //]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        //$dropItems ? Html::tag('div', Html::button('<span>Еще</span>'. Icon::get('shevron'), [
        //        'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
        //        'data-toggle' => 'dropdown',
        //        'style' => 'width: 90px; padding: 12px 15px;',
        //    ]) . Dropdown::widget([
        //        'items' => $dropItems,
        //        'options' => [
        //            'class' => 'dropdown-menu-right form-filter-list list-clr'
        //        ],
        //    ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные заявки?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete']),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>

<?php Modal::begin([
    'id' => 'modal-delete-sign-template',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите удалить шаблон подписания?
</h4>
<div class="text-center">
    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
        'class' => 'yes button-clr button-regular button-hover-transparent button-width-medium ladda-button',
    ]); ?>
    <button class="no button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
        Нет
    </button>
</div>
<?php Modal::end(); ?>
