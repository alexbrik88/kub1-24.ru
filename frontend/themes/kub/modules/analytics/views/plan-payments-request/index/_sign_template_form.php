<?php

use common\models\employee\Employee;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\Company;
use yii\helpers\ArrayHelper;
use frontend\components\Icon;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplate as SignTemplate;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplateStep as SignTemplateStep;

/** @var SignTemplate $template */
/** @var SignTemplateStep[] $templateSteps */
/** @var Company $company */

$employeeList = ArrayHelper::map($company->getEmployeeCompanies()
    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');

$form = ActiveForm::begin([
    'id' => 'form-payments-request-template',
    'action' => $template->isNewRecord ? Url::to(['add-sign-template']) : Url::to(['update-sign-template', 'id' => $template->id]),
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-12">
                <?= $form->field($template, 'name'); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= $form->field($template, 'comment')->textarea([
            'placeholder' => '',
            'rows' => '3'
        ]); ?>
    </div>
</div>

<div id="payments-request-sign-template-steps" class="mb-3">
    <div class="row">
        <div class="col-6"><label class="label">Название шага</label></div>
        <div class="col-6"><label class="label">Подписант</label></div>
    </div>
    <?php foreach ($templateSteps as $key => $step): ?>
    <div class="row row-step <?= $step->isVisible ? '' : 'hidden' ?>">
        <div class="col-6">
            <?= $form->field($step, 'name', [
                'template' => '{input}{error}',
                'options' => [
                    'class' => 'mb-2',
                ]
            ])->textInput([
                'placeholder' => 'Отдел закупок'
            ])->label(false) ?>
        </div>
        <div class="col-6">
            <?= $form->field($step, 'employee_id', [
                'template' => '{input}{error}',
                'options' => [
                    'class' => 'mb-2'
                ]
            ])->widget(Select2::class, [
                'data' => $employeeList,
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => ''
                ],
                'options' => [
                    'id' => '_sign_template_step_employee_' . ($key+1),
                ]
            ])->label(false); ?>
        </div>
    </div>
    <?php endforeach; ?>

    <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']) . 'Добавить', [
        'class' => 'add-step button-regular button-hover-transparent button-clr button-width',
    ]); ?>

    <?= $form->field($template, 'hasSteps', [
        'template' => '{input}{error}',
        'options' => [
            'class' => 'mb-2'
        ]
    ])->hiddenInput()->label(false); ?>

</div>


<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <?php if (!$template->isNewRecord): ?>
        <?= Html::button(($template->isPresentInRequests ? 'В архив' : 'Удалить'), [
        'class' => 'button-clr button-width button-regular button-hover-transparent ml-auto mr-3',
        'style' => 'width: 130px!important;',
        'data-toggle' => 'modal',
        'data-target' => '#modal-delete-sign-template',
        'onclick' => '$("#modal-delete-sign-template").find(".yes").attr("href", "'.Url::to(['delete-sign-template', 'id' => $template->id]).'");',
    ]); ?>
    <?php endif; ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'style' => 'width: 130px!important;',
        'data-dismiss' => 'modal',
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<script>
    $('.add-step').on('click', function() {
        $('#payments-request-sign-template-steps').find('.row-step.hidden').first().removeClass('hidden');
    });
</script>
