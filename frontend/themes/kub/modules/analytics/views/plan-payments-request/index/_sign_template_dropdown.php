<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplate as Template;
/**
 * @var $buttonName string
 * @var $company Company
 * @var $canAdd bool
 * @var $canUpdate bool
 */

$templateItems = Template::find()
    ->where([
        'company_id' => $company->id,
        'is_active' => true
    ])->all();
?>

<div class="dropdown">
    <button class="button-regular w-100 button-hover-content-red mb-0" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <?=($buttonName)?>
    </button>
    <div class="dropdown-menu w-100 p-0 m-0">
        <div class="popup-dropdown-in" id="payments-request-templates-dropdown">
            <?php foreach ($templateItems as $templateItem): ?>
                <div class="form-edit form-edit_alternative" data-id="<?= $templateItem['id'] ?>">
                    <div class="form-edit-result hide show" data-id="form-edit">
                        <div class="weight-400 popup-content pt-3 pb-3 block-nowrap" title="<?=  $templateItem['name'] ?>">
                            <span class="black-link"><?= Html::encode($templateItem['name']) ?></span>
                        </div>
                        <?php if ($canUpdate): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <button class="ajax-modal-btn button-clr link" title="Обновить" data-url="<?= Url::to(['update-sign-template', 'id' => $templateItem['id']]) ?>" data-title="Изменить шаблон подписания">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if ($canAdd): ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3 text-left">
                    <button class="ajax-modal-btn black-link link_bold link_font-14 link_decoration_none nowrap button-clr" data-url="<?= Url::to(['add-sign-template']) ?>" data-title="Добавить шаблон подписания">
                        <svg class="add-button-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span class="ml-2">Добавить шаблон</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    SignTempates = {
        modalSelector: '#ajax-modal-box',
        buttonSelector: {
            add: '#add-sign-template',
            update: '.update-sign-template'
        },
        init: function() {
            this.bind();
        },
        bind: function() {
            const that = this;
            $(document).on('click', that.buttonSelector.add, function() {
                $(that.modalSelector).modal();
            });
        }
    };

    ////////////////////
    SignTempates.init();
    ////////////////////

</script>