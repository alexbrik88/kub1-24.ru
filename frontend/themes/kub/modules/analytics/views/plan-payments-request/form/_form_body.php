<?php

use common\models\company\CheckingAccountant;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplate as SignTemplate;

/** @var \common\models\Company $company */

$companyRs = $company->getRubleAccounts()
    ->select(['name', 'rs', 'id', 'bik'])
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->asArray()
    ->all();

$companyCashboxes = $company->getRubleCashboxes()
    ->select(['id', 'name'])
    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
    ->asArray()
    ->all();

$rsData = [];
$rsData[PaymentsRequest::PAYMENT_SOURCE_BANK] = 'Все банки';
foreach ($companyRs as $data) {
    $rsData[PaymentsRequest::PAYMENT_SOURCE_BANK.'_'.$data['id']] = str_repeat('&nbsp;', 4) . $data['name'];
}
$rsData[PaymentsRequest::PAYMENT_SOURCE_CASHBOX] = 'Все кассы';
foreach ($companyCashboxes as $id => $data) {
    $rsData[PaymentsRequest::PAYMENT_SOURCE_CASHBOX.'_'.$data['id']] = str_repeat('&nbsp;', 4) . $data['name'];
}
$signTemplateData = SignTemplate::find()
    ->where(['company_id' => $company->id])
    ->orderBy(['name' => SORT_ASC])
    ->select(['name', 'id'])
    ->indexBy('id')
    ->column();
?>

<div class="wrap">
    <div class="form-group col-12">
        <div class="form-filter">
            <label class="label">
                Расч/счета и кассы<span class="important" aria-required="true">*</span>
            </label>
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'payment_source', [
                        'template' => "{input}",
                        'options' => [
                            'class' => '',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $rsData,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            // 'templateResult' => new JsExpression('formatCompanyRsTemplateResult'),
                            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                            'width' => '100%'
                        ],
                        'options' => [
                            'class' => 'form-control',
                        ]
                    ])->label(false); ?>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <!-- todo -->
                    <span class="hidden contractor-invoice-debt small-txt col-3">
                        Остаток на <span class="date"></span>: <span class="balance"></span> Р
                    </span>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group col-12">
        <div class="form-filter">
            <label class="label">
                Шаблон подписания<span class="important" aria-required="true">*</span>
            </label>
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'sign_template_id', [
                        //'template' => "{input}{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $signTemplateData,

                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                        'options' => [
                            'prompt' => '',
                            'class' => 'form-control',
                        ]
                    ])->label(false); ?>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>