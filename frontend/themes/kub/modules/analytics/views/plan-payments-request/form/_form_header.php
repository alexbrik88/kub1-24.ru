<?php

use common\components\date\DateHelper;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $model PaymentsRequest */
/* @var $form ActiveForm */

if (!$model->document_date)
    $model->document_date = date('Y-m-d');
?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        Заявка на платежи
                    </span>
                    <span>№</span>
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'data-required' => 1,
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                    ]); ?>
                </div>

                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'data-required' => 1,
                        'class' => 'form-control',
                        'placeholder' => 'доп. номер',
                    ]); ?>
                </div>

                <div class="form-txt d-inline-block mb-0 column">на дату</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'payment_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'data-is-inited' => 0,
                            'value' => DateHelper::format($model->payment_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>