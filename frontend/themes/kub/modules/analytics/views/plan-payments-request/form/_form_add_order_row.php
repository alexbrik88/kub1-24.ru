<?php

use kartik\select2\Select2;
use yii\web\JsExpression;
use \frontend\themes\kub\helpers\Icon;
use common\models\cash\CashFlowsBase;

/** @var $hasOrders bool */

$company = Yii::$app->user->identity->company;
?>

<tr id="from-new-add-row" class="from-new-add disabled-row" role="row" <?= $hasOrders ? 'style="display: none;"': ''; ?> >
    <td class="delete-column-left">
        <span class="icon-close remove-flow-from-invoice from-new"></span>
    </td>
    <td data-attr="date">

    </td>
    <td data-attr="amount">
        <div style="height: 33px"></div>
    </td>
    <td data-attr="contractor">

    </td>
    <td data-attr="description">

    </td>
    <td data-attr="invoice" class="<?= $tabConfigClass['invoice'] ?>">

    </td>
    <td data-attr="industry" class="<?= $tabConfigClass['industry'] ?>">

    </td>
    <td data-attr="sale_point" class="<?= $tabConfigClass['sale_point'] ?>">

    </td>
    <td data-attr="project" class="<?= $tabConfigClass['project'] ?>">

    </td>
    <td data-attr="expenditure_item">

    </td>
    <td data-attr="payment_type">

    </td>
    <td data-attr="payment_priority" class="<?= $tabConfigClass['priority'] ?>">

    </td>
</tr>

<?php

$this->registerJS(<<<JS

    ORDER_PLAN_FLOW = {
        table: '#table-for-invoice',
        flowSelected: [],
        flowRemoveButton: null,
        showAddModal: function(url) {
            const ajaxModal = $('#add-new');
            $('#add-new-modal-content', ajaxModal).load(url, function() {
                refreshDatepicker(ajaxModal);
                refreshUniform(ajaxModal);
                ajaxModal.modal('show');
            });
        },
        showSearchModal: function() {
            const ajaxModal = $('#add-from-exists');
            const exists = $('input.plan_operation_id').map(function (idx, elem) {
                return $(elem).val();
            }).get();
            
            $.pjax({
                url: '/analytics/plan-payments-request/get-plan-flows-modal',
                container: '#pjax-flow-grid',
                data: {exists: exists},
                push: false,
                timeout: 10000,
            });
            ajaxModal.modal('show');
        },
        addFlowsToTable: function(items) {
            if (items) {
                const table = $(this.table);
                const template = table.find('.template').first().clone();
                let rowsArray = [];
                
                template.removeClass('template').addClass('flow-row');
                template.find('input').attr('disabled', false);

                for (let i = 0; i < items.length; i++) {
                    let item = items[i];
                    let row = template.clone();
                    
                    row.attr('id', item.id);
                    row.find('.remove-plan-flow').data('id', item.id);                    
                    row.find('.plan_operation_id').val(item.id);
                    row.find('td[data-attr=date]').html(item.date);
                    row.find('td[data-attr=amount]').html(item.amount);
                    row.find('td[data-attr=contractor]').html(item.contractor);
                    row.find('td[data-attr=description]').html(item.description);
                    row.find('td[data-attr=invoice]').html(item.invoice);
                    row.find('td[data-attr=industry]').html(item.industry);
                    row.find('td[data-attr=sale_point]').html(item.sale_point);
                    row.find('td[data-attr=project]').html(item.project);
                    row.find('td[data-attr=expenditure_item]').html(item.expenditure_item);
                    row.find('td[data-attr=payment_type]').html(item.payment_type);
                    row.find('td[data-attr=payment_priority]').html(item.payment_priority);
                    
                    rowsArray.push(row);
                }
                
                for (let i in rowsArray) {
                    rowsArray[i].show().insertBefore('#from-new-add-row');
                }
            }
            
            $('#from-new-add-row').hide();
        },
        removeFlowFromRequest: function()
        {
            const btn = ORDER_PLAN_FLOW.flowRemoveButton;

            if ($(btn).hasClass('from-new')) {
                $('#from-new-add-row').hide();
            } else {
                $(btn).parents('tr:first').remove();
            }
        }
    };

    // add new
    $(document).on('submit', '#js-plan_cash_flow_form', function() {
        const url = $(this).attr('action');
        const form = $(this).serialize();
        $.post(url, form, function(data) {
            ORDER_PLAN_FLOW.addFlowsToTable([data]);
            $('#add-new').modal('hide');
            $('#add-new-modal-content').html('');
        });
        return false;
    });

    // add from list
    $('#add-from-exists').on('shown.bs.modal', function () {
        ORDER_PLAN_FLOW.flowSelected = [];
    });
    $(document).on('click', '#pjax-flow-grid a:not(.store-opt)', function (e) {
        e.preventDefault();
        $.pjax.reload('#pjax-flow-grid', {
            url: $(this).attr('href'),
            type: 'post',
            data: {flow: ORDER_PLAN_FLOW.flowSelected},
            push: false,
            replace: false,
            timeout: 5000
        });
    });
    $(document).on('change', '#flows_in_order .flow_selected', function () {
        const prodId = $(this).val();
        if (this.checked) {
            ORDER_PLAN_FLOW.flowSelected.push(prodId);
        } else {
            ORDER_PLAN_FLOW.flowSelected = jQuery.grep(ORDER_PLAN_FLOW.flowSelected, function (value) {
                return value != prodId;
            });
        }
    });
    $(document).on('change', '#flows_in_order .flow_selected_one', function () {
        var prodId = $(this).val();
        if (this.checked) {
            ORDER_PLAN_FLOW.flowSelected = [prodId];
            $('#flows_in_order .flow_selected_one').each(function(i, checkbox) {
                if ($(checkbox).val() != prodId) {
                    $(checkbox).prop('checked', false).uniform('refresh');
                }
            });
        } else {
            if (ORDER_PLAN_FLOW.flowSelected[0] == prodId) {
                ORDER_PLAN_FLOW.flowSelected = [];
            }
        }
    });
    $(document).on('click', '#add-to-request-button', function () {
        const url = '/analytics/plan-payments-request/get-plan-flows-modal';
        const form = {in_order: ORDER_PLAN_FLOW.flowSelected};
        $.post(url, form, function(data) {
            ORDER_PLAN_FLOW.addFlowsToTable(data);
            $('#add-from-exists').modal('hide');
        });
    });
    $(document).on("change", "#flow_selected-all", function() {
        const form = $(this).closest('form');
        $(form).find("input.flow_selected").prop("checked", $(this).prop("checked")).trigger("change");
        $(form).find("input.flow_selected:not(.md-check, .md-radiobtn)").uniform("refresh");
    });
    
    // remove
    $(document).on('click', '.remove-plan-flow', function () {
        $('#modal-remove-one-flow').modal("show");
        ORDER_PLAN_FLOW.flowRemoveButton = this;
    });
    $(document).on('click', '#modal-remove-one-flow .yes', function (e) {
        ORDER_PLAN_FLOW.removeFlowFromRequest();
    });
    
    // modals triggers   
    $(document).on('click', '.add-modal-plan-flow-new', function () {
        $('#order-add-select').select2('close');
        ORDER_PLAN_FLOW.showAddModal('/analytics/plan-payments-request/add-plan-flow-modal');
    });
    $(document).on('click', '.add-modal-plan-flows', function () {
        $('#order-add-select').select2('close');
        ORDER_PLAN_FLOW.showSearchModal('/analytics/plan-payments-request/get-plan-flows-modal');
    });
    
JS);