<?php

use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestOrder;

/** @var PaymentsRequestOrder $order */
/** @var PaymentsRequest $model */

$flow = ($order->planCashFlow instanceof \frontend\modules\analytics\models\PlanCashFlows)
    ? PaymentsRequestOrder::flow2json($order->planCashFlow)
    : null;
?>

<?php if ($flow): ?>
<tr role="row">
    <td class="plan-flow-delete delete-column-left" style="white-space: nowrap;">

        <input type="hidden" class="plan_operation_id" name="orderArray[][plan_operation_id]" value="<?= $flow['id'] ?>">

        <div style="max-width: 45px">
            <button class="remove-plan-flow button-clr" type="button" data-id="<?= $flow['id'] ?>">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
            <button class="sortable-row-icon button-clr" type="button">
                <svg class="table-count-icon table-count-icon_small svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                </svg>
            </button>
        </div>
    </td>
    <td data-attr="date">
        <?= $flow['date'] ?>
    </td>
    <td data-attr="amount">
        <?= $flow['amount'] ?>
    </td>
    <td data-attr="contractor">
        <?= $flow['contractor'] ?>
    </td>
    <td data-attr="description">
        <?= $flow['description'] ?>
    </td>
    <td data-attr="invoice" class="<?= $tabConfigClass['invoice'] ?>">
        <?= $flow['invoice'] ?>
    </td>
    <td data-attr="industry" class="<?= $tabConfigClass['industry'] ?>">
        <?= $flow['industry'] ?>
    </td>
    <td data-attr="sale_point" class="<?= $tabConfigClass['sale_point'] ?>">
        <?= $flow['sale_point'] ?>
    </td>
    <td data-attr="project" class="<?= $tabConfigClass['project'] ?>">
        <?= $flow['project'] ?>
    </td>
    <td data-attr="expenditure_item">
        <?= $flow['expenditure_item'] ?>
    </td>
    <td data-attr="payment_type">
        <?= $flow['payment_type'] ?>
    </td>
    <td data-attr="payment_priority" class="<?= $tabConfigClass['priority'] ?>">
        <?= $flow['payment_priority'] ?>
    </td>
</tr>
<?php endif; ?>