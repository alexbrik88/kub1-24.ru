<?php

use common\assets\SortableAsset;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\document\Invoice;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\web\View;
use yii\widgets\Pjax;
use frontend\modules\analytics\models\PlanCashFlows;

/* @var $this View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $company Company */
/* @var $projectEstimateId integer */

SortableAsset::register($this);

/** @var Company $company */
$company = Yii::$app->user->identity->company;
$userConfig = Yii::$app->user->identity->config;

$tabConfigClass = [
    'invoice' => 'col_payments_request_order_invoice' . ($userConfig->payments_request_order_invoice ? '' : ' hidden'),
    'industry' => 'col_payments_request_order_industry' . ($userConfig->payments_request_order_industry ? '' : ' hidden'),
    'sale_point' => 'col_payments_request_order_sale_point' . ($userConfig->payments_request_order_sale_point ? '' : ' hidden'),
    'project' => 'col_payments_request_order_project' . ($userConfig->payments_request_order_project ? '' : ' hidden'),
    'priority' => 'col_payments_request_order_priority' . ($userConfig->payments_request_order_priority ? '' : ' hidden'),
];

$orders = $model->orders;
usort($orders, function($a, $b) { return $a->number <=> $b->number; });

$canAdd = true;
$planFlowsCount = PlanCashFlows::find()
    ->where(['company_id' => $company->id])
    ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
    ->andWhere(['>=', 'date', date('Y-m-d')])
    ->andWhere(['>=', 'first_date', date('Y-m-d')])
    ->count()
?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'payments_request_order_invoice'],
                ['attribute' => 'payments_request_order_industry'],
                ['attribute' => 'payments_request_order_sale_point'],
                ['attribute' => 'payments_request_order_project'],
                ['attribute' => 'payments_request_order_priority'],
            ],
        ]); ?>
    </div>
</div>

<div id="flow-table-invoice" class="wrap" style="position: relative">
    <div class="table-wrap table-responsive">
        <table id="table-for-invoice" class="table table-style table-count" style="position: relative;">
            <thead>
            <tr class="heading" role="row">
                <th class="delete-column-left" style="min-width:45px; width:1%"></th>
                <th style="min-width:85px; width: 5%;" class="">
                    Плановая дата
                </th>
                <th style="min-width:215px; width: 15%;" class="">
                    Сумма
                </th>
                <th style="min-width:135px; width: 10%;" class="">
                    Поставщик
                </th>
                <th style="min-width:215px; width: 15%;" class="">
                    Назначение
                </th>
                <th style="min-width:75px;  width: 5%" class="<?= $tabConfigClass['invoice'] ?>">
                    Счет
                </th>
                <th style="min-width:135px; width: 10%;" class="<?= $tabConfigClass['industry'] ?>">
                    Направление
                </th>
                <th style="min-width:135px; width: 10%;" class="<?= $tabConfigClass['sale_point'] ?>">
                    Точка продаж
                </th>
                <th style="min-width:135px; width: 10%;" class="<?= $tabConfigClass['project'] ?>">
                    Проект
                </th>
                <th style="min-width:135px; width: 5%;" class="">
                    Статья
                </th>
                <th style="min-width:75px; width: 5%;" class="">
                    Тип оплаты
                </th>
                <th style="min-width:75px; width: 5%;" class="<?= $tabConfigClass['priority'] ?>">
                    Приоритет в оплате
                </th>
            </tr>
            </thead>
            <tbody id="table-flow-list-body" data-number="<?= count($orders) ?>">
            <?php if (count($orders) > 0) : ?>
                <?php
                foreach ($orders as $key => $order) {
                    echo $this->render('_form_order_row', [
                        'userConfig' => $userConfig,
                        'tabConfigClass' => $tabConfigClass,
                        'model' => $model,
                        'order' => $order,
                        'number' => $key,
                    ]);
                }
                ?>
            <?php endif; ?>
            <?= $this->render('_form_add_order_row', [
                'userConfig' => $userConfig,
                'tabConfigClass' => $tabConfigClass,
                'hasOrders' => count($orders) > 0,
            ]) ?>
            </tbody>
            <tfoot>
            <tr class="template disabled-row" role="row" style="display:none">
                <td class="plan-flow-delete delete-column-left" style="white-space: nowrap;">

                    <input disabled="disabled" type="hidden" class="plan_operation_id" name="orderArray[][plan_operation_id]">

                    <div style="max-width: 45px">
                        <button class="remove-plan-flow button-clr" type="button">
                            <svg class="table-count-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                            </svg>
                        </button>
                        <button class="sortable-row-icon button-clr" type="button">
                            <svg class="table-count-icon table-count-icon_small svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                            </svg>
                        </button>
                    </div>
                </td>
                <td data-attr="date">

                </td>
                <td data-attr="amount">

                </td>
                <td data-attr="contractor">

                </td>
                <td data-attr="description">

                </td>
                <td data-attr="invoice" class="<?= $tabConfigClass['invoice'] ?>">

                </td>
                <td data-attr="industry" class="<?= $tabConfigClass['industry'] ?>">

                </td>
                <td data-attr="sale_point" class="<?= $tabConfigClass['sale_point'] ?>">

                </td>
                <td data-attr="project" class="<?= $tabConfigClass['project'] ?>">

                </td>
                <td data-attr="expenditure_item">

                </td>
                <td data-attr="payment_type">

                </td>
                <td data-attr="payment_priority" class="<?= $tabConfigClass['priority'] ?>">

                </td>
            </tr>
            </tfoot>
        </table>
    </div>

    <div class="row align-flex-start justify-content-start mt-3">
        <!--
        <div class="column button-add-line">
            <span class="btn-add-line-table button-regular button-hover-content-red">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </span>
        </div>
        -->

        <div class="col-12">
            <div class="dropdown form-filter-list">
                <button class="button-regular button-hover-content-red mb-0 w-130" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
                <div class="dropdown-menu p-0 m-0">
                    <?php if ($canAdd) : ?>
                        <a class="add-modal-plan-flow-new dropdown-item block-nowrap" href="javascript:void(0)">
                            Добавить новый платеж
                        </a>
                    <?php endif ?>
                    <a class="add-modal-plan-flows dropdown-item block-nowrap" href="javascript:void(0)">
                        Выбрать из реестра плановых операций  (<span class="flow-count-value"><?= $planFlowsCount ?></span>)
                    </a>
                </div>
            </div>
        </div>

    </div>



</div>

<?php

$this->registerJs(<<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: ".sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS);

$this->registerCss(<<<CSS
    .flow-search-dropdown #select2-order-add-select-results {
        display: none!important;
    }
CSS
);