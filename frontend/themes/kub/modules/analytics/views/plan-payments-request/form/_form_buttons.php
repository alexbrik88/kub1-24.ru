<?php

use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model PaymentsRequest */

$cancelUrl = ($model->isNewRecord) ? ['index'] : ['view', 'id' => $model->id];
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'id' => 'invoice-form-submit',
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>
