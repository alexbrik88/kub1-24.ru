<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\helpers\Html;
use yii\widgets\Pjax;

$flowIdArray = Yii::$app->request->post('flow', []);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_payments_request');
$tabConfig = [
    'priority' => $userConfig->payments_request_order_priority ?? false,
    'project' => $userConfig->payments_request_order_project ?? false,
    'sale_point' => $userConfig->payments_request_order_sale_point ?? false,
    'industry' => $userConfig->payments_request_order_industry ?? false,
];

?>

<?php Pjax::begin([
    'id' => 'pjax-flow-grid',
    'linkSelector' => false,
    'formSelector' => '#flows_in_order',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]);

echo Html::beginForm(['/analytics/plan-payments-request/get-plan-flows-modal'], 'get', [
    'id' => 'flows_in_order',
    'class' => 'add-to-request',
    'data' => [
        'pjax' => true,
    ]
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => \common\components\helpers\Html::checkbox('in_order_all', false, [
                'id' => 'flow_selected-all',
                'class' => 'joint-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-left',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center joint-checkbox-td',
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($flowIdArray) {

                return Html::checkbox('in_order[]', in_array($flows['id'], $flowIdArray), [
                    'id' => 'product-' . $flows['id'],
                    'value' => $flows['id'],
                    'class' =>'flow_selected',
                ]);
            },
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'value' => function ($flows) use ($searchModel) {
                return !in_array($searchModel->group, [PaymentCalendarSearch::GROUP_CONTRACTOR, PaymentCalendarSearch::GROUP_PAYMENT_TYPE]) ?
                    DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
            },
        ],
        [
            'attribute' => 'amountIncome',
            'label' => 'Приход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: none',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right',
                'style' => 'display: none',
            ],
            'value' => function ($flows) {
                return $flows['amountIncome'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2) : '-';
            },
        ],
        [
            'attribute' => 'amountExpense',
            'label' => 'Расход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: table-cell',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right',
                'style' => 'display: table-cell',
            ],
            'value' => function ($flows) {
                return $flows['amountExpense'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2) : '-';
            },
        ],
        [
            'attribute' => 'payment_type',
            'label' => 'Тип оплаты',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                PaymentCalendarSearch::CASH_BANK_BLOCK => 'Банк',
                PaymentCalendarSearch::CASH_ORDER_BLOCK => 'Касса',
                PaymentCalendarSearch::CASH_EMONEY_BLOCK => 'E-money'
            ],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [PaymentCalendarSearch::GROUP_CONTRACTOR, PaymentCalendarSearch::GROUP_DATE])) {
                    switch ($flows['payment_type']) {
                        case PlanCashFlows::PAYMENT_TYPE_BANK:
                            $checkingAccountant = CheckingAccountant::find()->andWhere([
                                'id' => $flows['checking_accountant_id']
                            ])->orderBy(['type' => SORT_ASC])->one();
                            if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                        'class' => 'little_logo_bank',
                                        'style' => 'display: inline-block;',
                                    ]);
                            }
                            return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_ORDER:
                            return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_EMONEY:
                            return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                        default:
                            return '';
                    }
                }
                return '';
            },
        ],
        [
            'attribute' => 'priority_type',
            'label' => 'Приоритет в оплате',
            'headerOptions' => [
                'width' => '13%',
                'class' => 'col_payments_request_order_priority' . ($tabConfig['priority'] ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'text-left purpose-cell col_payments_request_order_priority' . ($tabConfig['priority'] ? '' : ' hidden')
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                Contractor::PAYMENT_PRIORITY_HIGH => '1 - cамые приоритетные',
                Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - менее приоритетные',
                Contractor::PAYMENT_PRIORITY_LOW => '3 - наименьший приоритет',
            ],
            'value' => function ($flows) use ($searchModel) {
                $contractor = Contractor::findOne($flows['contractor_id']);
                if ($contractor && $contractor->type == Contractor::TYPE_SELLER) {
                    switch ($contractor->payment_priority) {
                        case Contractor::PAYMENT_PRIORITY_HIGH:
                            return '1 - cамые приоритетные';
                            break;
                        case Contractor::PAYMENT_PRIORITY_MEDIUM:
                            return '2 - менее приоритетные';
                            break;
                        case Contractor::PAYMENT_PRIORITY_LOW:
                            return '3 - наименьший приоритет';
                            break;
                    }
                }

                return '-';
            },
            's2width' => '250px'
        ],
        //[
        //    'attribute' => 'status_type',
        //    'label' => 'Статус',
        //    'headerOptions' => [
        //        'width' => '13%',
        //    ],
        //    'contentOptions' => [
        //        'class' => 'text-left',
        //    ],
        //    'format' => 'raw',
        //    'filter' => [
        //        '' => 'Все',
        //        PaymentCalendarSearch::STATUS_PLAN => 'План',
        //        PaymentCalendarSearch::STATUS_OVERDUE => 'Просрочен',
        //        PaymentCalendarSearch::STATUS_MOVED => 'Перенос'
        //    ],
        //    'value' => function ($flows) use ($searchModel) {
        //
        //        $currDate = date('Y-m-d');
        //        if ($flows['first_date'] < $currDate && $flows['date'] != $flows['first_date'])
        //            return 'Перенос';
        //        if ($flows['date'] >= $currDate)
        //            return 'План';
        //        if ($flows['date'] < $currDate)
        //            return 'Просрочен';
        //
        //        return '';
        //    },
        //    's2width' => '125px'
        //],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '30%',
                'class' => 'nowrap-normal max10list',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell'
            ],
            'format' => 'raw',
            'filter' => !in_array($searchModel->group, [PaymentCalendarSearch::GROUP_PAYMENT_TYPE, PaymentCalendarSearch::GROUP_DATE]) ?
                $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [PaymentCalendarSearch::GROUP_PAYMENT_TYPE, PaymentCalendarSearch::GROUP_DATE])) {
                    /* @var $contractor Contractor */
                    $contractor = Contractor::findOne($flows['contractor_id']);
                    $model = PlanCashFlows::findOne($flows['id']);

                    return $contractor !== null ?
                        ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                        ($model->cashContractor ?
                            ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                        );
                }

                return '';
            },
            's2width' => '250px',
        ],
        [
            's2width' => '250px',
            'attribute' => 'project_id',
            'label' => 'Проект',
            'headerOptions' => [
                'class' => 'col_payments_request_order_project' . ($tabConfig['project'] ? '' : ' hidden'),
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_payments_request_order_project ' . ($tabConfig['project'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                    $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                }

                return $ret;
            },
        ],
        [
            's2width' => '250px',
            'attribute' => 'sale_point_id',
            'label' => 'Точка продаж',
            'headerOptions' => [
                'class' => 'col_payments_request_order_sale_point' . ($tabConfig['sale_point'] ? '' : ' hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_payments_request_order_sale_point ' . ($tabConfig['sale_point'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                    $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                }

                return $ret;
            },
        ],
        [
            's2width' => '250px',
            'attribute' => 'industry_id',
            'label' => 'Направление',
            'headerOptions' => [
                'class' => 'col_payments_request_order_industry' . ($tabConfig['industry'] ? '' : ' hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_payments_request_order_industry ' . ($tabConfig['industry'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                    $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                }

                return $ret;
            },
        ],
        [
            'attribute' => 'description',
            'label' => 'Назначение',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '30%',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell'
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group) && $flows['description']) {
                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                    return Html::tag('div',strlen($flows['description']) > 100 ? $description . '...' : $description, ['class' => 'purpose-cell', 'title' => $flows['description']]);
                }

                return '';
            },
        ],
        [
            'attribute' => 'reason_id',
            'label' => 'Статья',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'reason-cell text-left',
            ],
            'filter' => empty($searchModel->group) ?
                array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                ['' => 'Все статьи'],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group)) {
                    if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $reason = ($reasonModel = InvoiceIncomeItem::findOne($flows['income_item_id'])) ? $reasonModel->fullName : null;
                    } elseif ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                        $reason = ($reasonModel = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ? $reasonModel->fullName : null;
                    } else {
                        $reason = null;
                    }

                    return $reason ? ('<span title="' . htmlspecialchars($reason) . '">' . $reason . '</span>') : '-';
                }

                return '';
            },
            's2width' => '250px',
        ],
    ],
]);

echo Html::endForm();

?>

<br>
<div class="d-flex justify-content-between mt-2">
    <?= Html::button('Добавить отмеченные', [
        'class' => 'button-regular button-regular_red button-clr',
        'data-dismiss' => 'modal',
        'id' => 'add-to-request-button',
        'style' => 'margin-left: 15px;',
    ]); ?>
</div>

<?php Pjax::end(); ?>
