<?php


use common\models\company\CheckingAccountant;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/** @var View $this */
/** @var ActiveForm $form */
/** @var PaymentsRequest $model */

$this->title = ($model->isNewRecord ? 'Создать ' : 'Изменить ') . 'заявку';
$errorModelArray = array_merge([$model], $model->orders);
$formDefaultConfig = [
    'layout' => 'default',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => '',
        ],
        'hintOptions' => [
            'tag' => 'small',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => '',
            'hint' => '',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
];
?>

<div class="form">
    <?php $form = ActiveForm::begin(array_merge($formDefaultConfig, [
        'id' => 'payments-request-form',
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
    ])); ?>

    <?= $form->errorSummary($errorModelArray); ?>

    <?= Html::hiddenInput('returnUrl', $returnUrl) ?>

    <?= $this->render('form/_form_header', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('form/_form_body', [
        'model' => $model,
        'company' => $company,
        'form' => $form,
    ]); ?>

    <?= $this->render('form/_form_table', [
        'model' => $model,
        'company' => $company,
    ]); ?>

    <?= $this->render('form/_form_buttons', [
        'model' => $model,
    ]); ?>

    <?php ActiveForm::end(); ?>
</div>

<!-- MODALS -->

<!-- remove row -->
<div id="modal-remove-one-flow" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить эту позицию?
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add row -->
<div id="add-new" class="modal fade t-p-f modal_scroll_center mobile-modal" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить плановую операцию</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div id="add-new-modal-content" class="modal-body">

            </div>
        </div>
    </div>
</div>

<div id="add-from-exists" class="modal mobile-modal">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content">
            <h4 class="modal-title">Добавить платежи в заявку</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <?php Pjax::begin([
                'id' => 'pjax-flow-grid',
                'linkSelector' => false,
                'formSelector' => '#flows_in_order',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
