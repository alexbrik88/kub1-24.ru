<?php

return $this->render('@frontend/modules/reference/views/articles/_partial/_form', [
    'model' => $model,
    'parentModel' => $parentModel ?? null,
    'type' => $type,
    'activeTab' => (int)$activeTab,
    'updateList' => $updateList ?? false,
])

?>