<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.04.2020
 * Time: 1:32
 */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\components\helpers\ArrayHelper;

/**
 * @var $type int
 * @var $model \frontend\modules\reference\models\SetDefaultContractorItem
 */

$incomeItemsMap = ArrayHelper::map(InvoiceIncomeItem::find()
    ->andWhere(['IN', 'id', [
        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
        InvoiceIncomeItem::ITEM_AGENT_FEE,
        InvoiceIncomeItem::ITEM_COMMISSION,
        InvoiceIncomeItem::ITEM_TRADE_RECEIPTS,
    ]])
    ->all(), 'id', 'name');
$expenditureItemsMap = ArrayHelper::map(InvoiceExpenditureItem::find()
    ->andWhere(['IN', 'id', [
        InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
        InvoiceExpenditureItem::ITEM_PRODUCT,
        InvoiceExpenditureItem::ITEM_SERVICE,
        InvoiceExpenditureItem::ITEM_OTHER,
    ]])
    ->all(), 'id', 'name');
?>
<div id="articles-rules" class="fade modal" role="modal" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <h4 class="modal-title">Правила назначения статей новым контрагентам</h4>
            <span class="modal-header-description">
                Правила необходимы для того, что бы новый контрагент сразу попал в отчеты.
                В любое время вы можете вручную поменять статью на любую другую.
            </span>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'autoact-form',
                    'action' => ['set-default-item', 'type' => $type],
                    'options' => [
                        'class' => 'autoact-form',
                        'data-pjax' => '',
                    ],
                ]); ?>

                <div class="itemHeader">Покупатели</div>
                <?= $form->field($model, 'incomeItemId', ['options' => ['class' => '']])->label(false)
                    ->radioList([
                        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER => $incomeItemsMap[InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER],
                        InvoiceIncomeItem::ITEM_AGENT_FEE => $incomeItemsMap[InvoiceIncomeItem::ITEM_AGENT_FEE],
                        InvoiceIncomeItem::ITEM_COMMISSION => $incomeItemsMap[InvoiceIncomeItem::ITEM_COMMISSION],
                        InvoiceIncomeItem::ITEM_TRADE_RECEIPTS => $incomeItemsMap[InvoiceIncomeItem::ITEM_TRADE_RECEIPTS],
                    ], [
                        'encode' => false,
                        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
                            $input = Html::radio($name, $checked, [
                                'value' => $value,
                                'class' => 'autoact-rule-radio',
                                'label' => Html::tag('span', $label),
                                'labelOptions' => [
                                    'class' => 'autoact-rule-label'
                                ],
                            ]);

                            return Html::tag('div', $input);
                        }
                    ]); ?>

                <div class="itemHeader">Поставщики</div>
                <?= $form->field($model, 'expenditureItemId', ['options' => ['class' => '']])->label(false)
                    ->radioList([
                        InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT => $expenditureItemsMap[InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT],
                        InvoiceExpenditureItem::ITEM_PRODUCT => $expenditureItemsMap[InvoiceExpenditureItem::ITEM_PRODUCT],
                        InvoiceExpenditureItem::ITEM_SERVICE => $expenditureItemsMap[InvoiceExpenditureItem::ITEM_SERVICE],
                        InvoiceExpenditureItem::ITEM_OTHER => $expenditureItemsMap[InvoiceExpenditureItem::ITEM_OTHER],
                    ], [
                        'encode' => false,
                        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
                            $input = Html::radio($name, $checked, [
                                'value' => $value,
                                'class' => 'autoact-rule-radio',
                                'label' => Html::tag('span', $label),
                                'labelOptions' => [
                                    'class' => 'autoact-rule-label'
                                ],
                            ]);

                            return Html::tag('div', $input);
                        }
                    ]); ?>

                <div class="mt-3 d-flex justify-content-between">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::button('Отменить', [
                        'class' => 'button-clr button-width button-regular button-hover-transparent',
                        'style' => 'width: 130px!important;',
                        'data-dismiss' => 'modal',
                    ]); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
