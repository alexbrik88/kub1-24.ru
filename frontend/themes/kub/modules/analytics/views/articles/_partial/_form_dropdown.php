<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.04.2020
 * Time: 23:46
 */

use common\components\helpers\Html;
use frontend\modules\reference\models\ArticleDropDownForm;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use frontend\modules\reference\models\ArticlesSearch;
use common\components\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $model ArticleDropDownForm
 * @var $type int
 * @var $inputSelector string
 */

Pjax::begin([
    'id' => 'article-dropdown-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>
    <div class="article-form">
        <?php $form = ActiveForm::begin([
            'id' => 'article-form',
            'enableClientValidation' => false,
            'action' => ['/analytics/articles/create-from-dropdown', 'type' => $type, 'inputSelector' => $inputSelector,],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            'options' => [
                'data-pjax' => true,
                'data-max-files' => 5,
            ],
        ]) ?>

        <?= $form->field($model, 'isNew', [
            'options' => [
                'class' => 'form-group',
            ]])
            ->radioList([
                ArticleDropDownForm::TYPE_EXISTS_ITEM => 'Добавить статьи '
                    . ($type == ArticlesSearch::TYPE_INCOME ? 'приходов' : 'расходов')
                    . ', которые еще у вас не подключены',
                ArticleDropDownForm::TYPE_NEW_ITEM => 'Добавить свою статью '
                    . ($type == ArticlesSearch::TYPE_INCOME ? 'прихода' : 'расхода'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
                    $result = Html::radio($name, $checked, [
                        'class' => 'flow-type-toggle-input',
                        'value' => $value,
                        'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                        'labelOptions' => [
                            'class' => 'label mb-3 mr-3 mt-2',
                            'style' => $value === ArticleDropDownForm::TYPE_EXISTS_ITEM ? 'margin-bottom: ' : null,
                        ],
                    ]);

                    if ($value === ArticleDropDownForm::TYPE_EXISTS_ITEM) {
                        $result .= $form->field($model, 'items', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_EXISTS_ITEM ? 'block;' : 'none;'),
                            ],
                        ])->checkboxList(ArrayHelper::map($model->getExistsItems(), 'id', 'name'))->label(false);
                    } else {
                        $result .= $form->field($model, 'name', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' &&  $model->isNew == ArticleDropDownForm::TYPE_NEW_ITEM ? 'block;' : 'none;'),
                            ],
                            'inputOptions' => [
                                'placeholder' => 'Введите название статьи',
                            ],
                        ])->label(false);
                    }

                    return $result;
                },
            ])->label(false); ?>

        <span>
            Всю информацию об используемых и неиспользуемых статьях смотрите в
            <?= Html::a('справочнике статей', Url::to(['/analytics/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY])); ?>
        </span>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                    data-dismiss="modal">Отменить
            </button>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php Pjax::end() ?>