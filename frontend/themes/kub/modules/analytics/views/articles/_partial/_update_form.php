<?php

return $this->render('@frontend/modules/reference/views/articles/_partial/_update_form', [
    'model' => $model,
    'onlyContractor' => $onlyContractor ?? false,
    'activeTab' => (int)$activeTab,
    'updateList' => $updateList ?? [],
]);