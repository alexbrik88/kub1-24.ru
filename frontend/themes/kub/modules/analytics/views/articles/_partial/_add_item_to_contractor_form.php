<?php

return $this->render('@frontend/modules/reference/views/articles/_partial/_add_item_to_contractor_form', [
    'model' => $model,
    'type' => $type,
    'activeTab' => (int)$activeTab,
    'updateList' => $updateList ?? false,
])

?>