<?= $this->render('@frontend/modules/reference/views/articles/articles-list', [
    'searchModel' => $searchModel,
    'data' => $data,
    'type' => $type,
    'contractorsOnPage' => $contractorsOnPage ?? 9E9
]) ?>