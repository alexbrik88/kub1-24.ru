<?php

use common\models\product\ProductGroup;
use frontend\components\Icon;
use frontend\modules\analytics\assets\WhatIfAssets;
use frontend\widgets\TableViewWidget;
use frontend\widgets\TableConfigWidget;
use kartik\range\RangeInput;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $searchModel \frontend\modules\analytics\models\WhatIfSearch */

WhatIfAssets::register($this);

$this->title = 'Анализ "Что, если..."';
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->what_if_help ?? false;
$showChartPanel = $userConfig->what_if_chart ?? false;

$SHOW_BY_GROUPS = $userConfig->report_abc_show_groups;
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_abc');

$rows = $dataProvider->getModels();

function valueColor($value) {
    return $value < 0 ? 'red' : ($value > 0 ? 'green' : '');
}
?>

<div class="what-if">
    <div class="wrap py-2 pl-4 pr-3 mt-12px mb-12px">
        <div class="row align-items-center">
            <h4 class="column mr-auto my-0"><?= $this->title ?></h4>
            <div class="column px-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap mt-12px mb-12px collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="what_if_help">
        <?= $this->render('what-if/_description') ?>
    </div>

    <?php $pjax = Pjax::begin([
       'id' => 'what-if-pjax',
       'timeout' => 10000,
    ]) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'what-if-modeling-form',
        'data-pjax' => '',
    ]) ?>

    <div class="wrap pt-3 pb-4 px-4 mt-12px mb-12px hidden">
        <div class="pl-2 pr-2 pb-1">
            <div class="row">
                <div class="col-3">
                    <label class="label">
                        Тип продукта
                    </label>
                    <?= Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'product_type',
                        'data' => [null => null],
                        'pluginOptions' => ['width' => '100%'],
                        'hideSearch' => true,
                    ]) ?>
                </div>
                <div class="col-3">
                    <label class="label">
                        Вид продукта
                    </label>
                    <?= Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'sales_type',
                        'data' => [null => null],
                        'pluginOptions' => ['width' => '100%'],
                        'hideSearch' => true,
                    ]) ?>
                </div>
                <div class="col-3">
                    <label class="label">
                        Точка продаж
                    </label>
                    <?= Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'sales_point',
                        'data' => [null => null],
                        'pluginOptions' => ['width' => '100%'],
                        'hideSearch' => true,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-12px mb-12px">
        <div class="col-8">
            <?= $this->render('what-if/_result', [
                'data' => $searchModel->resultItems,
            ]) ?>
        </div>
        <div class="col-4 pl-0">
            <?= $this->render('what-if/_modeling', [
                'model' => $searchModel,
            ]) ?>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap mt-12px mb-12px collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="what_if_chart">
        <div class="row">
            <div class="col-8 pr-0">
                <div class="row">
                    <div class="col-12 mb-2">
                        <?= $this->render('what-if/_chart_profit_for_year', [
                            'model' => $searchModel,
                        ]) ?>
                    </div>
                    <div class="col-6 mt-2" style="max-width: 700px;">
                        <?= $this->render('what-if/_chart_changies', [
                            'model' => $searchModel,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <?= $this->render('what-if/_chart_product_profit', [
                    'model' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row mt-12px mb-12px">
        <div class="col-6">
            <?= frontend\widgets\TableConfigWidget::widget([
                'sortingItemsTitle' => 'Выводить',
                'sortingItems' => [
                    [
                        'attribute' => 'show_groups',
                        'label' => 'По группам товара',
                        'checked' => $SHOW_BY_GROUPS,
                    ],
                    [
                        'attribute' => 'dont_show_groups',
                        'label' => 'Без групп товара',
                        'checked' => !$SHOW_BY_GROUPS,
                    ],
                ]
            ]); ?>
        </div>
        <div class="col-6">
            <div class="d-flex flex-nowrap align-items-center">
                <div class="flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'filter', [
                        'type' => 'search',
                        'placeholder' => 'Поиск по наименованию или артикулу',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div id="abc-analysis-grid" style="width: 100%; overflow: hidden;">
        <div class="wrap py-1 pl-0 pr-2" style="width: 100%; overflow: hidden;">
            <div class="table-wrap" style="width: 100%; overflow: hidden;">
                <table class="scrollable-table double-scrollbar-top table table-style table-count-list analysis-table border-top my-1 <?= $tabViewClass ?>" aria-describedby="datatable_ajax_info" role="grid">
                    <thead>
                    <tr>
                        <th rowspan="2" class="fixed-column">
                            <?php if ($SHOW_BY_GROUPS): ?>
                                <span class="table-collapse-btn button-clr ml-1 collapse-all-trigger">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey weight-700 ml-1">Группа товара</span>
                                </span>
                            <?php else: ?>
                                Название товара
                            <?php endif; ?>
                        </th>
                        <th colspan="2" class="height-30">
                            Выручка, ₽
                        </th>
                        <th colspan="2" class="height-30">
                            Изменение выручки
                        </th>
                        <th colspan="2" class="height-30">
                            Маржа, ₽
                        </th>
                        <th colspan="2" class="height-30">
                            Изменение маржи
                        </th>
                        <th colspan="3" class="height-30">
                            Прибыль, ₽
                        </th>
                        <th colspan="2" class="height-30">
                            Изменение прибыли
                        </th>
                        <th colspan="2" class="height-30">
                            Рентабельность, %
                        </th>
                    </tr>
                    <tr id="abc-analysis-grid-filters" class="filters">
                        <!-- Выручка -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Факт', 'attr' => 'proceeds_1']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Ожидаем', 'attr' => 'proceeds_2']) ?>
                        </th>
                        <!-- Изменение выручки -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '₽', 'attr' => 'change_proceeds_abs']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '%', 'attr' => 'change_proceeds_rel']) ?>
                        </th>
                        <!-- Маржа -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Факт', 'attr' => 'margin_1']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Ожидаем', 'attr' => 'margin_2']) ?>
                        </th>
                        <!-- Изменение маржи -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '₽', 'attr' => 'change_margin_abs']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '%', 'attr' => 'change_margin_rel']) ?>
                        </th>
                        <!-- Прибыль -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Факт', 'attr' => 'profit_1']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Доля , %', 'attr' => 'overal_profit_percent_1']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Ожидаем', 'attr' => 'profit_2']) ?>
                        </th>
                        <!-- Изменение прибыли -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '₽', 'attr' => 'change_profit_abs']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => '%', 'attr' => 'change_profit_rel']) ?>
                        </th>
                        <!-- Рентабельность -->
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Факт', 'attr' => 'rentability_1']) ?>
                        </th>
                        <th width="5%" class="height-30">
                            <?= $this->render('what-if/_sort_header', ['title' => 'Ожидаем', 'attr' => 'rentability_2']) ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($rows)): ?>
                            <?php foreach ($rows as $key => $row): ?>
                                <?php $isGroup = array_key_exists('items', $row); ?>

                                <?= $this->render('what-if/_table_row', [
                                    'key' => $key,
                                    'data' => $row,
                                    'isGroup' => $isGroup,
                                ]); ?>

                                <?php if ($isGroup) : ?>
                                    <?php foreach ($row['items'] as &$item) : ?>
                                        <?= $this->render('what-if/_table_row', [
                                            'key' => $key,
                                            'data' => $item,
                                            'isGroup' => false,
                                        ]); ?>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            <?php endforeach; ?>

                            <?= $this->render('what-if/_table_totals', [
                                'total' => $searchModel->total,
                            ]); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="16">Нет данных за выбранный период</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?= Html::endForm() ?>

    <?php $pjax->end() ?>
</div>

<?php
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);
