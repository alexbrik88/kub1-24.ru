<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $data array */

?>

<div class="wrap my-0 what-if-modeling-result">
    <h5>
        <label class="label font-weight-bold">
            РЕЗУЛЬТАТ "ЧТО, ЕСЛИ..."
        </label>
    </h5>
    <div class="d-flex">
        <div class="col p-3 border-right">
            <?= $this->render('_result_item', [
                'data' => $data[0],
            ]) ?>
        </div>
        <div class="col p-3">
            <?= $this->render('_result_item', [
                'data' => $data[1],
            ]) ?>
        </div>
    </div>
    <div class="d-flex border-top">
        <div class="col p-3 border-right">
            <?= $this->render('_result_item', [
                'data' => $data[2],
            ]) ?>
        </div>
        <div class="col p-3">
            <?= $this->render('_result_item', [
                'data' => $data[3],
            ]) ?>
        </div>
    </div>
    <div class="d-flex border-top">
        <div class="col p-3 border-right">
            <?= $this->render('_result_item', [
                'data' => $data[4],
            ]) ?>
        </div>
        <div class="col p-3">
            <?= $this->render('_result_item', [
                'data' => $data[5],
            ]) ?>
        </div>
    </div>
</div>
