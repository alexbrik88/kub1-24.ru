<?php

use frontend\components\Icon;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $data array */

$icon = $data['icon'];
$append = $data['append'];
$label = $data['label'];
$val1 = $data['value1'];
$val2 = $data['value2'];
$formatting = $data['formatting'];
$percent = $val1 ? (($val2 / $val1 - 1) * 100) : null;
$ico = isset($percent) ? ($percent > 0 ? 'changes_up' : ($percent < 0 ? 'changes_down' : '')) : null;
?>

<div class="d-flex">
    <?= Icon::get($icon, ['class' => 'item-icon']) ?>
    <div class="ml-4">
        <div style="margin-bottom: 3px;">
            <?= $label ?>
        </div>
        <div class="d-flex" style="font-size: 18px;">
            <div class="font-weight-bold">
                <?= Yii::$app->formatter->$formatting($val2) ?><?= $append ?>
            </div>
            <div class="d-flex" data-color="<?= valueColor($percent) ?>">
                <div class="ml-2 mr-1">
                    <?= isset($ico) ? Icon::get($ico, ['class' => 'changes-icon']) : '' ?>
                </div>
                <?= isset($percent) ? Yii::$app->formatter->asNumber(round($percent, 2)).'%' : ''?>
            </div>
        </div>
        <label class="label">
            <span style="font-size: 12px;">
                Факт
            </span>
            <span style="font-size: 14px; font-weight: bold;">
                <?= Yii::$app->formatter->$formatting($val1) ?><?= $append ?>
            </span>
        </label>
    </div>
</div>