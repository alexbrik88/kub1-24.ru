<?php

use kartik\range\RangeInput;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\analytics\models\WhatIfSearch */

$isChanged = $model->quantity || $model->price || $model->variable_expenses || $model->fixed_expenses;
$btnCss = $isChanged ? 'button-regular_red' : 'button-hover-transparent';
?>

<div class="wrap my-0 what-if-modeling">
    <div class="d-flex justify-content-between">
        <h5>
            <label class="label font-weight-bold">
                МОДЕЛИРОВАНИЕ
            </label>
        </h5>
        <?= Html::button('Сброс', [
            'class' => "button-regular px-4 what-if-modeling-reset {$btnCss}",
        ]) ?>
    </div>
    <div class="">
        <label class="label">Спрос</label>
        <?= RangeInput::widget([
            'model' => $model,
            'attribute' => 'quantity',
            'options' => [
                'data-color' => valueColor($model->quantity),
            ],
            'html5Options' => ['min' => -100, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
        ]) ?>
    </div>
    <div class="mt-2">
        <label class="label">Цена</label>
        <?= RangeInput::widget([
            'model' => $model,
            'attribute' => 'price',
            'options' => [
                'data-color' => valueColor($model->price),
            ],
            'html5Options' => ['min' => -100, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
        ]) ?>
    </div>
    <div class="mt-2">
        <label class="label">Переменные расходы</label>
        <?= RangeInput::widget([
            'model' => $model,
            'attribute' => 'variable_expenses',
            'options' => [
                'data-color' => valueColor($model->variable_expenses),
            ],
            'html5Options' => ['min' => -100, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
        ]) ?>
    </div>
    <div class="mt-2">
        <label class="label">Постоянные расходы</label>
        <?= RangeInput::widget([
            'model' => $model,
            'attribute' => 'fixed_expenses',
            'options' => [
                'data-color' => valueColor($model->fixed_expenses),
            ],
            'html5Options' => ['min' => -100, 'max' => 100],
            'addon' => ['append' => ['content' => '%']]
        ]) ?>
    </div>
</div>
