<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $key int */
/* @var $data array */
/* @var $isGroup bool */

?>

<?= Html::beginTag('tr', [
    'data-key' => $key,
    'data-id' => $isGroup ? null : $key,
    'class' => ($isGroup ? 'group-row' : 'group-item-row hidden'),
]) ?>
    <!--Название-->
    <td class="fixed-column col-name">
        <?php if ($isGroup): ?>
            <span class="table-collapse-btn button-clr ml-1 text-left collapse-row-trigger" data-target="<?= $key ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <strong class="d-block text_size_14 ml-1" title="<?= Html::encode($data['title']) ?>">
                    <?= Html::encode($data['title']) ?>
                </strong>
            </span>
        <?php else: ?>
            <span title="<?= Html::encode($data['title']) ?>" style="padding-left: 30px;">
                <?= Html::encode($data['title']) ?>
            </span>
        <?php endif; ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['proceeds_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['proceeds_2']) ?>
    </td>
    <!-- Изменение выручки -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['change_proceeds_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['change_proceeds_rel'], 2) ?>%
    </td>
    <!-- Маржа -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['margin_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['margin_2']) ?>
    </td>
    <!-- Изменение маржи -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['change_margin_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['change_margin_rel'], 2) ?>%
    </td>
    <!-- Прибыль -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['profit_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['overal_profit_percent_1'], 2) ?>%
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['profit_2']) ?>
    </td>
    <!-- Изменение прибыли -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($data['change_profit_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['change_profit_rel'], 2) ?>%
    </td>
    <!-- Рентабельность -->
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['rentability_1'], 2) ?>%
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($data['rentability_2'], 2) ?>%
    </td>
</tr>
