<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\analytics\models\WhatIfSearch */

$data = $model->getProfitForYear();
?>

<script type="text/javascript">
    var YearProfitChartData = <?= yii\helpers\Json::encode($data['otherData']) ?>
</script>

<div class="chart-item-wrap">
    <h5 class="m-0">
        <label class="label font-weight-bold">
            ПРИБЫЛЬ на ГОД ВПЕРЕД
        </label>
    </h5>
    <?= Highcharts::widget([
        'scripts' => [
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'title' => ['text' => ''],
            'chart' => [
                'type' => 'spline',
            ],
            'title' => false,
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'xAxis' => [
                'categories' => $data['categories'],
                'labels' => [
                    'formatter' => new \yii\web\JsExpression(<<<EOL
                        function() {
                            let data = YearProfitChartData[this.pos];
                            let w = this.axis.width / this.axis.categories.length / 2;
                            console.log(this);
                            let val = this.value.substring(0, 3).toUpperCase();
                            let cssClass = 'axis-label' + (data.current ? ' current' : '');
                            let content = '<div class="axis-label-wrap">';
                            content += '<div class="' + cssClass + '">' + val + '</div>';
                            if (data.month == 12 || data.month == 1) {
                                content += '<div class="axis-label-footer">' + data.year + '</div>';
                                if (data.month == 12 && !this.isLast) {
                                    content += '<div class="axis-label-separator" style="width: ' + w + 'px;"></div>';
                                }
                            }
                            content += '</div>';

                            return content;
                        }
EOL
                        ),
                    'useHTML' => true,
                    'autoRotation' => false,
                ],
            ],
            'yAxis' => [
                'title' => '',
                'minorGridLineWidth' => 0,
                'labels' => [
                    'useHTML' => true,
                    'style' => [
                        'fontWeight' => '300',
                        'fontSize' => '13px',
                        'whiteSpace' => 'nowrap'
                    ],
                    'formatter' => new yii\web\JsExpression("function() {
                        const formatCash = function(n) {
                          if (Math.abs(n) < 1e3) return n;
                          if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                          if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                        };

                        return formatCash(this.value);
                    } "),
                ],
            ],
            'series' => [
                [
                    'name' => 'Изменение прибыли',
                    'color' => 'rgba(57,194,176,1)',
                    'negativeColor' => 'red',
                    'data' => $data['seriesData'],
                ],
            ],
            'plotOptions' => [
                'series' => [
                    'pointWidth' => 20,
                    'tooltip' => [
                        'crosshairs' => true,
                        'headerFormat' => '{point.x}',
                        'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                    ],
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                    //'groupPadding' => 0.05,
                    //'pointPadding' => 0.1,
                    'borderRadius' => 3,
                    'borderWidth' => 1
                ]
            ],
            'tooltip' => [
                'useHTML' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new yii\web\jsExpression(<<<EOL
                    function(args) {
                        let content = '';
                        let data = YearProfitChartData[this.point.index];
                        content += '<strong class="title">' + data.title + '</strong>';
                        content += '<table class="indicators">';
                        content += '<tr><td class="gray-text">Прибыль факт:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(data.profitFact, 2, ',', ' ') + '&nbsp;₽</td></tr>';
                        content += '<tr><td class="gray-text">Прибыль план:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(data.profitPlan, 2, ',', ' ') + '&nbsp;₽</td></tr>';
                        content += '<tr><td class="gray-text">Изменение прибыли:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(this.y, 2, ',', ' ') + '&nbsp;₽</td></tr>';

                        return content + '</table>';
                    }
EOL
                ),
            ],
        ],
    ]) ?>
</div>
