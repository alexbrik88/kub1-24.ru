<?php

/* @var $this \yii\web\View */
/* @var $total array */

?>

<tr data-key="totals" style="font-weight: bold;">
    <!--Название-->
    <td class="fixed-column">
        <b>ИТОГО</b>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['proceeds_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['proceeds_2']) ?>
    </td>
    <!-- Изменение выручки -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['change_proceeds_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['change_proceeds_rel'], 2) ?>%
    </td>
    <!-- Маржа -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['margin_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['margin_2']) ?>
    </td>
    <!-- Изменение маржи -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['change_margin_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['change_margin_rel'], 2) ?>%
    </td>
    <!-- Прибыль -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['profit_1']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['overal_profit_percent_1'], 2) ?>%
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['profit_2']) ?>
    </td>
    <!-- Изменение прибыли -->
    <td class="text-right">
        <?= Yii::$app->formatter->asMoney($total['change_profit_abs']) ?>
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['change_profit_rel'], 2) ?>%
    </td>
    <!-- Рентабельность -->
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['rentability_1'], 2) ?>%
    </td>
    <td class="text-right">
        <?= Yii::$app->formatter->asNumber($total['rentability_2'], 2) ?>%
    </td>
</tr>
