<?php

use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\analytics\models\WhatIfSearch */

$data = $model->getProfitFactors();
$categories = $data['categories'];
$seriesData = $data['seriesData'];
$color1 = 'rgba(57,194,176,1)';
$color1_opacity = 'rgba(57,194,176,.95)';
$min = min($seriesData);
$max = max($seriesData);
$scale = ceil(max(1000, abs($min), $max));
?>

<div class="chart-item-wrap">
    <h5>
        <label class="label font-weight-bold">
            ИЗМЕНЕНИЯ "ЧТО, ЕСЛИ..."
        </label>
    </h5>
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-changies',
        'class' => 'finance-charts',
        'scripts' => [
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'bar',
                'spacing' => [0,0,0,0],
                'height' => 216,
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                ],
                'marginTop' => '32',
                'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                //'outside' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression(<<<EOL
                    function(args) {
                        let content = '<div class ="tooltip-container">';
                        content += '<strong class="title">' + this.point.category + '</strong>';
                        content += '<table class="indicators">';
                        content += '<tr><td class="gray-text">Изменение:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>';

                        return content + '</table></div>';
                    }
EOL
                ),
                'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) {
                    return {x: point.plotX / 3 + 75, y: point.plotY + 50};
                }'),
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                'min' => -$scale,
                'max' => $scale,
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'endOnTick' => false,
                'tickPixelInterval' => 1,
                'visible' => false,
                'stackLabels' => [
                    'enabled' => true,
                ],
            ],
            'xAxis' => [
                'categories' => $categories,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'gridLineWidth' => 0,
                'offset' => 0,
            ],
            'series' => [
                [
                    'showInLegend' => false,
                    'name' => 'Изменение',
                    'pointPadding' => 0,
                    'color' => 'rgba(57,194,176,1)',
                    'negativeColor' => 'red',
                    'borderColor' => $color1_opacity,
                    'data' => $seriesData,
                ],
            ],
            'plotOptions' => [
                'bar' => [
                    'pointWidth' => 18,
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'borderRadius' => 3,
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                ],
                'series' => [
                    'minPointLength' => 2,
                ],
            ],
        ],
    ]); ?>
</div>
