<?php

use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\analytics\models\WhatIfSearch */

$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';

$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 21;
$cropNamesLength = 12;
$croppedNameLength = 10;
$data = $model->getProductProfit();
$categories = $data['categories'];
$seriesData = $data['seriesData'];

if (empty($data)) {
    $categories = [['Нет данных']];
    $data = [[null]];
}

$series = [
    [
        'name' => 'Факт',
        'maxPointWidth' => 18,
        'data' => $seriesData[1],
        'color' => $color1,
        'borderColor' => $color1_opacity,
        'states' => [
            'hover' => [
                'color' => [
                    'pattern' => [
                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color' => $color1,
                        'width' => 5,
                        'height' => 5
                    ]
                ]
            ],
        ]
    ],
];
if ($model->getHasChangies()) {
    $series[] = [
        'name' => 'План',
        'maxPointWidth' => 6,
        'data' => $seriesData[2],
        'color' => 'rgba(0,0,0,1)',
        'borderColor' => 'rgba(0,0,0,1)',
    ];
}
$calculatedChartHeight = count($categories) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
?>

<script type="text/javascript">
    var ProductProfitChartData = <?= yii\helpers\Json::encode($data['items']) ?>
</script>

<div class="chart-item-wrap">
    <h5 class="m-0">
        <label class="label font-weight-bold">
            ПРИБЫЛЬ ПО ТОВАРАМ
        </label>
    </h5>
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-product-profit',
        'class' => 'finance-charts',
        'scripts' => [
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'bar',
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                ],
                'height' => $calculatedChartHeight,
                'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                //'outside' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression(<<<EOL
                    function(args) {
                        let content = '<div class ="tooltip-container">';
                        let data = ProductProfitChartData[this.point.index];
                        content += '<strong class="title">' + data.title + '</strong>';
                        content += '<table class="indicators">';
                        content += '<tr><td class="gray-text">Прибыль факт:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(data.profit_1, 2, ',', ' ') + ' ₽</td></tr>';
                        content += '<tr><td class="gray-text">Прибыль план:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(data.profit_2, 2, ',', ' ') + ' ₽</td></tr>';
                        content += '<tr><td class="gray-text">Изменение:</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(data.change_profit_rel, 2, ',', ' ') + ' %</td></tr>';

                        return content + '</table></div>';
                    }
EOL
                ),
                'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) {
                    return {x: point.plotX / 3 + 75, y: point.plotY + 50};
                }'),
            ],
            'title' => [
                'text' => '',
            ],
            'yAxis' => [
                'title' => [
                    'text' => null,
                ],
                'visible' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'gridLineWidth' => 0,
                //'offset' => 0,
                'title' => [
                    'text' => null,
                ],
                'labels' => [
                    'align' => 'left',
                    'reserveSpace' => true,
                    'formatter' => new yii\web\JsExpression("
                        function() {
                            return (this.value.length > {$cropNamesLength}) ?
                                    (this.value.substring(0,{$croppedNameLength}) + '...') :
                                    this.value
                        }
                    "),
                ],
            ],
            'series' => $series,
            'plotOptions' => [
                'bar' => [
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                ],
                'series' => [
                    'minPointLength' => 2,
                ]
            ],
        ],
    ]); ?>
</div>
