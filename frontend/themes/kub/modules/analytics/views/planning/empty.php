<?php

use common\models\service\ServiceModule;
use frontend\components\Icon;
use yii\bootstrap4\Html;

$this->title = $title;
$showChartPanel = false;
$showHelpPanel = false;
$subtitle = $subtitle ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'disabled tooltip2 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")'),
                        'disabled' => true,
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'disabled tooltip2 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")'),
                        'disabled' => true,
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">

            </div>
        </div>
    </div>

</div>

<div class="wrap mt-3 text-center">
    <?php if ($subtitle): ?>
        <div>
            <?= $subtitle ?>
        </div>
    <?php else: ?>
        <div>
            <?= Icon::get('block', [
                'style' => 'font-size: 100px;',
            ]) ?>
        </div>
        <div class="mt-5">
            Данный отчет доступен только
            в оплаченном тарифе "ФинДиректор Pro"
        </div>
        <div class="mt-5">
            <?= Html::a('Оплатить КУБ24', '#', [
                'class' => 'disabled button-regular button-hover-transparent px-3',
                'disabled' => true,
            ]) ?>
        </div>
    <?php endif; ?>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_odds_chart">
    <div class="pt-4 pb-3">
        <span class="text-grey">Блок в разработке</span>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_odds_help">
    <div class="pt-4 pb-3">
        <span class="text-grey">Блок в разработке</span>
    </div>
</div>
