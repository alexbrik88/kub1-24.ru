<?php

use common\models\product\ProductGroup;
use frontend\components\Icon;
use frontend\modules\analytics\assets\ScenarioAnalysisAssets;
use frontend\widgets\TableViewWidget;
use frontend\widgets\TableConfigWidget;
use kartik\range\RangeInput;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $searchModel \frontend\modules\analytics\models\ScenarioAnalysisSearch */
/* @var $data array */

ScenarioAnalysisAssets::register($this);

$this->title = 'Сценарный анализ';

$model = $searchModel;
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->scenario_analysis_help ?? false;
$showChartPanel = $userConfig->scenario_analysis_chart ?? false;
?>

<div class="scenario-analysis">
    <div class="wrap py-2 pl-4 pr-3 mt-12px mb-12px">
        <div class="row align-items-center">
            <h4 class="column mr-auto my-0"><?= $this->title ?></h4>
            <div class="column px-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap mt-12px mb-12px collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="scenario_analysis_help">
        <?= $this->render('scenario/_description') ?>
    </div>

    <?php $pjax = Pjax::begin([
       'id' => 'scenario-analysis-pjax',
       'timeout' => 10000,
    ]) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'scenario-analysis-form',
        'data-pjax' => '',
    ]) ?>

    <div class="jsSaveStateCollapse wrap mt-12px mb-12px chart-box-wrap collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="scenario_analysis_chart">
        <div class="">
            <?= $this->render('scenario/_chart_1', [
                'model' => $searchModel,
            ]) ?>
        </div>
        <div class="">
            <?= $this->render('scenario/_chart_2', [
                'model' => $searchModel,
            ]) ?>
        </div>
    </div>

    <div class="wrap py-2 pl-0 pr-2">
        <div>
            <div class="px-2"><strong>УПРАВЛЕНИЕ ДОХОДАМИ</strong></div>
            <table class="table table-style table-count-list analysis-table table-compact">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 28%;">
                            <span class="table-collapse-btn button-clr ml-1 collapse-all-trigger">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1">Группы покупателей</span>
                            </span>
                        </th>
                        <th rowspan="2" style="width: 8%;">Доля группы, %</th>
                        <th rowspan="2" style="width: 8%;">Маржа, %</th>
                        <th colspan="4">Отсрочка покупателям, дней</th>
                        <th colspan="2">Коэффициент оборач-ти дебиторки</th>
                        <th rowspan="2" style="width: 8%;">Скидка / наценка, %</th>
                    </tr>
                    <tr>
                        <th style="width: 8%; text-align: center;">По договору</th>
                        <th style="width: 8%;">Факт</th>
                        <th style="width: 8%;">План</th>
                        <th style="width: 8%;">Изменение</th>
                        <th style="width: 8%;">Факт</th>
                        <th style="width: 8%;">План</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['type2']['g'] as $key => $group) : ?>
                        <?php
                        $k = "row_type2_{$key}";
                        $t = $group['total'];
                        ?>
                        <tr data-key="<?= $k ?>" class="group-row">
                            <td>
                                <span class="table-collapse-btn button-clr ml-1 text-left collapse-row-trigger" data-target="<?= $k ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="d-block text_size_14 ml-1">
                                        Группа "<?= strtoupper($key) ?>"
                                    </span>
                                </span>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['proceeds_part'], 2) ?>%
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['margin_part'], 2) ?>%
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_set'], 1) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_fact'], 1) ?>
                            </td>
                            <td class="p-0 custom-value">
                                <?= Html::activeTextInput($model, "delay_plan[type2][{$key}]", [
                                    'class' => 'form-control',
                                    'type' => 'number',
                                ]) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_change'], 1) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['turnover_fact'], 2) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['turnover_plan'], 2) ?>
                            </td>
                            <td class="p-0 custom-value">
                                <?= Html::activeTextInput($model, "price_plan[type2][{$key}]", [
                                    'class' => 'form-control',
                                    'type' => 'number',
                                ]) ?>
                            </td>
                        </tr>
                        <?php foreach ($group['items'] as $item) : ?>
                            <tr class="hidden group-item-row" data-key="<?= $k ?>" data-id="<?= $k ?>">
                                <td class="pl-40px">
                                    <span title="<?= Html::encode($item['title']) ?>">
                                        <?= Html::encode($item['title']) ?>
                                    </span>
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['proceeds_part'], 2) ?>%
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['margin_part'], 2) ?>%
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['delay_set'], 1) ?>
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['delay_fact'], 1) ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endforeach ?>
                    <tr>
                        <?php
                        $t = $data['type2']['total'];
                        ?>
                        <td>Итого</td>
                        <td>100,0%</td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['margin_part'], 1) ?>%
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_set'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_fact'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_plan'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_change'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['turnover_fact'], 2) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['turnover_plan'], 2) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['price_plan'], 2) ?>%
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="mt-4">
            <div class="px-2"><strong>УПРАВЛЕНИЕ РАСХОДАМИ</strong></div>
            <table class="table table-style table-count-list analysis-table table-compact">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 28%;">
                            <span class="table-collapse-btn button-clr ml-1 collapse-all-trigger">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1">Группы поставщиков</span>
                            </span>
                        </th>
                        <th rowspan="2" style="width: 16%;">Доля группы, %</th>
                        <th colspan="4">Отсрочка от поставщиков, дней</th>
                        <th colspan="2">Коэффициент оборач-ти кредиторки</th>
                        <th rowspan="2" style="width: 8%;">Скидка / наценка, %</th>
                    </tr>
                    <tr>
                        <th style="width: 8%; text-align: center;">По договору</th>
                        <th style="width: 8%;">Факт</th>
                        <th style="width: 8%;">План</th>
                        <th style="width: 8%;">Изменение</th>
                        <th style="width: 8%;">Факт</th>
                        <th style="width: 8%;">План</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['type1']['g'] as $key => $group) : ?>
                        <?php
                        $k = "row_type1_{$key}";
                        $t = $group['total'];
                        ?>
                        <tr data-key="<?= $k ?>" class="group-row">
                            <td>
                                <span class="table-collapse-btn button-clr ml-1 text-left collapse-row-trigger" data-target="<?= $k ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="d-block text_size_14 ml-1">
                                        Группа "<?= strtoupper($key) ?>"
                                    </span>
                                </span>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['proceeds_part'], 1) ?>%
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_set'], 1) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_fact'], 1) ?>
                            </td>
                            <td class="p-0 custom-value">
                                <?= Html::activeTextInput($model, "delay_plan[type1][{$key}]", [
                                    'class' => 'form-control',
                                    'type' => 'number',
                                ]) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['delay_change'], 1) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['turnover_fact'], 2) ?>
                            </td>
                            <td>
                                <?= Yii::$app->formatter->asNumber($t['turnover_plan'], 2) ?>
                            </td>
                            <td class="p-0 custom-value">
                                <?= Html::activeTextInput($model, "price_plan[type1][{$key}]", [
                                    'class' => 'form-control',
                                    'type' => 'number',
                                ]) ?>
                            </td>
                        </tr>
                        <?php foreach ($group['items'] as $item) : ?>
                            <tr class="hidden group-item-row" data-key="<?= $k ?>" data-id="<?= $k ?>">
                                <td class="pl-40px">
                                    <span title="<?= Html::encode($item['title']) ?>">
                                        <?= Html::encode($item['title']) ?>
                                    </span>
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['proceeds_part'], 2) ?>%
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['delay_set'], 1) ?>
                                </td>
                                <td>
                                    <?= Yii::$app->formatter->asNumber($item['delay_fact'], 1) ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endforeach ?>
                    <tr>
                        <?php
                        $t = $data['type1']['total'];
                        ?>
                        <td>Итого</td>
                        <td>100,0%</td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_set'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_fact'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_plan'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['delay_change'], 1) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['turnover_fact'], 2) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['turnover_plan'], 2) ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asNumber($t['price_plan'], 2) ?>%
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?= Html::endForm() ?>

    <?php $pjax->end() ?>
</div>
