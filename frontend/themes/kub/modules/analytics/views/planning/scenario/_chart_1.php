<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\analytics\models\WhatIfSearch */

$data = $model->getChart1Data();
$series = [
    [
        'name' => 'Выручка',
        'color' => '#7cb5ec',
        'negativeColor' => 'red',
        'data' => $data['series']['proceeds'],
    ],
    [
        'name' => 'Линия тренда',
        'color' => 'gray',
        'negativeColor' => 'red',
        'data' => $data['series']['trend'],
    ],
    [
        'name' => 'Операционная прибыль',
        'color' => '#90ee7e',
        'negativeColor' => 'red',
        'data' => $data['series']['profit'],
    ],
];
if ($data['hasPlan']) {
    $series[] = [
        'name' => 'Выручка план',
        'color' => '#7cb5ec',
        'negativeColor' => 'red',
        'dashStyle' => 'Dash',
        'data' => $data['series']['proceeds_plan'],
        'marker' => [
            'symbol' => 'square'
        ],
    ];
    $series[] = [
        'name' => 'Операционная прибыль план',
        'color' => '#90ee7e',
        'negativeColor' => 'red',
        'dashStyle' => 'Dash',
        'data' => $data['series']['profit_plan'],
        'marker' => [
            'symbol' => 'square'
        ],
    ];
}
?>

<script type="text/javascript">
    var Chart1Data = <?= yii\helpers\Json::encode($data['data']) ?>
</script>

<div class="">
    <h5 class="m-0">
        <label class="label font-weight-bold">
            ДИНАМИКА ВЫРУЧКИ и ОПЕРАЦИОННОЙ ПРИБЫЛИ (прогноз)
        </label>
    </h5>
    <?= Highcharts::widget([
        'scripts' => [
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'title' => ['text' => ''],
            'chart' => [
                'type' => 'spline',
            ],
            'title' => false,
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'xAxis' => [
                'categories' => $data['categories'],
                'labels' => [
                    'formatter' => new \yii\web\JsExpression(<<<EOL
                        function() {
                            let data = Chart1Data[this.pos];
                            let w = this.axis.width / this.axis.categories.length / 2;
                            let val = this.value.substring(0, 3).toUpperCase();
                            let cssClass = 'axis-label' + (data.current ? ' current' : '');
                            let content = '<div class="axis-label-wrap">';
                            content += '<div class="' + cssClass + '">' + val + '</div>';
                            if (data.month == 12 || data.month == 1) {
                                content += '<div class="axis-label-footer">' + data.year + '</div>';
                                if (data.month == 12 && !this.isLast) {
                                    content += '<div class="axis-label-separator" style="width: ' + w + 'px;"></div>';
                                }
                            }
                            content += '</div>';

                            return content;
                        }
EOL
                        ),
                    'useHTML' => true,
                    'autoRotation' => false,
                ],
            ],
            'yAxis' => [
                'title' => ['text' => false]
            ],
            'series' => $series,
            'tooltip' => [
                'useHTML' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new yii\web\jsExpression(<<<EOL
                    function(args) {
                        console.log(this);
                        let content = '';
                        let data = Chart1Data[this.point.index];
                        content += '<strong class="title">' + data.title + '</strong>';
                        content += '<table class="indicators">';
                        content += '<tr><td class="gray-text">' + this.series.name + '</td><td class="gray-text-b">' +
                                    Highcharts.numberFormat(this.point.y, 2, ',', ' ') + '&nbsp;₽</td></tr>';

                        return content + '</table>';
                    }
EOL
                ),
            ],
            'plotOptions' => [
                'spline' => [
                    'marker' => [
                        'enabled' => false,
                        'symbol' => 'circle',
                        'radius' => 3,
                    ],
                ],
            ],
        ]
    ]) ?>
</div>
