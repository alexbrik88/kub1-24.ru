<?php
use yii\helpers\Html;
use common\models\Company;
use frontend\components\Icon;
use frontend\modules\analytics\models\financeDashboard\tabFinances;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;

/** @var string $tab */
/** @var Company $company */
/** @var FinanceDashboard $dashboard */
/** @var tabFinances\TabSearch $searchModel */

$title = FinanceDashboard::TAB_NAME[$searchModel::CURRENT_TAB];
$currentYear = substr($searchModel->dateTo, 0, 4);
?>

<!-- head -->
<div class="wrap wrap_count">
    <div class="row mb-12px align-items-center">
        <div class="column pl-2 mr-auto">
            <div class="h4 mb-0">
                <?= Html::tag('div', $title, ['style' => 'display:inline-flex']) ?>
            </div>
        </div>
        <div class="column pl-1 pr-2">
            <?= Html::button(Icon::get('cog'), ['title' => 'Настройки',
                'class' => 'button-list button-hover-transparent button-clr collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('book'), ['title' => 'Описание',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('clip-2'), ['title' => 'Скопировать ссылку',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'href' => '#copy-link-dashboard-modal',
                'data-toggle' => 'modal',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
        </div>
    </div>
    <?= $this->render('_statistics', $this->params, $dashboard->context) ?>
</div>

<!-- body -->

<?= $dashboard->renderBlock($this,'@frontend/modules/analytics/views/detailing/industry/view/tab_dashboard', [
    'palModel' => $searchModel->chart->getPalModel(),
    'oddsModel' => $searchModel->chart->getOddsModel(),
    'customOffsetDays' => $searchModel->chart->getChartCurrentOffset("days", 6, 6), // for chart_plan_fact
    'customOffsetMonths' => $searchModel->chart->getChartCurrentOffset("months", 6, 6), // for chart_plan_fact
    'customOffset' => $searchModel->chart->getChartCurrentOffset("months", 6, 6) + 1, // for chart_pal_revenue
    'dateFrom' => $searchModel->chart->dateFrom,
    'dateTo' => $searchModel->chart->dateTo,
]) ?>

<div class="wrap p-0">
    <?= $dashboard->renderBlock($this, '@frontend/modules/analytics/views/finance/_charts/_chart_industry_compare', [
        'company' => $company,
        'analyticsModel' => $searchModel->chart->getAnalyticsSimpleSearchModel(),
        'customChartType' => DetailingUserConfig::CHART_TYPE_REVENUE,
        'customOffset' => $searchModel->chart->getChartCurrentOffset("months", 1, 1),
        'currentYear' => $currentYear,
        'pieChartHeight' => 287,
        'onPage' => 'industry',
        'palModel' => $searchModel->chart->getPalModel(),
    ]) ?>
</div>

<div class="wrap p-2">
    <div class="row">
        <div class="col-6 pr-1">
            <div class="dashboard-card dashboard-card-currency wrap">
                <?= $dashboard->renderBlock($this, '@frontend/modules/analytics/views/finance/_charts/_chart_balance_stacked', [
                    'id' => 'chart-balance-stacked-assets',
                    'title' => 'Активы',
                    'customArticle' => 'assets',
                    //'dateFrom' => $searchModel->chart->dateFrom,
                    'dateTo' => $searchModel->chart->dateTo,
                    'customOffset' => $searchModel->chart->getChartCurrentOffset("months", 0, 0)
                ]) ?>
            </div>
        </div>
        <div class="col-6 pl-1">
            <div class="dashboard-card dashboard-card-currency wrap">
                <?= $dashboard->renderBlock($this, '@frontend/modules/analytics/views/finance/_charts/_chart_balance_stacked', [
                    'id' => 'chart-balance-stacked-liabilities',
                    'title' => 'Пассивы',
                    'customArticle' => 'liabilities',
                    //'dateFrom' => $searchModel->chart->dateFrom,
                    'dateTo' => $searchModel->chart->dateTo,
                    'customOffset' => $searchModel->chart->getChartCurrentOffset("months", 0, 0)
                ]) ?>
            </div>
        </div>
    </div>
</div>
