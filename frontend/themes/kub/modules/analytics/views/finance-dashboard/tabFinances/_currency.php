<?php
use common\components\helpers\ArrayHelper;
use common\components\date\DateHelper;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use common\models\currency\CurrencyRate;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$currencyModels = $searchModel->currency->getModels();
$currentRates = CurrencyRate::getRateOnDate(date_create_from_format('Y-m-d', $searchModel->dateTo));
$oldRates = CurrencyRate::getRateOnDate(date_create_from_format('Y-m-d', $searchModel->dateFrom));
?>

<div class="dashboard-card-column col-6">

    <?= frontend\widgets\RangeButtonWidget::widget(['byMonths' => true]); ?>

    <?php if (!empty($currencyModels)): ?>
        <div class="dashboard-card dashboard-card-currency wrap">
            <div class="ht-caption">
                КУРСЫ ВАЛЮТ
                <span style="text-transform: none!important;"> на </span>
                <?= $searchModel->subtitleLastDate ?>
            </div>
            <table class="ht-table">
                <?php

                foreach ($currencyModels as $item) :
                    $currentValue = ArrayHelper::getValue($currentRates, "{$item->name}.value", null);
                    $oldValue = ArrayHelper::getValue($oldRates, "{$item->name}.value", null);
                    
                    if ($currentValue > $oldValue) {
                        $icon = 'arrow_green.svg';
                        $cssClass = 'green';
                        $iconClass = null;
                    } elseif ($currentValue < $oldValue) {
                        $icon = 'arrow_red.svg';
                        $cssClass = 'red';
                        $iconClass = null;
                    } else {
                        $icon = null;
                        $cssClass = '';
                        $iconClass = 'fa-minus';
                    }
                    ?>

                    <?php if ($currentValue === null || $oldValue === null): ?>
                        <tr>
                            <td><i class="fa <?= $iconClass ?>"></i></td>
                            <td><?= $item->name; ?> ЦБ</td>
                            <td></td>
                            <td></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td>
                                <?php if (isset($icon)): ?>
                                    <img src="/img/abc/<?= $icon ?>" width="14">
                                <?php else: ?>
                                    <i class="fa <?= $iconClass . ' ' . $cssClass; ?>"></i>
                                <?php endif ?>
                            </td>
                            <td>
                                <?= $item->name; ?> ЦБ
                            </td>
                            <td class="<?= $cssClass; ?>">
                                <?= round($currentValue - $oldValue, 4); ?>
                            </td>
                            <td class="bold">
                                <?= $currentValue; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </table>
        </div>
    <?php endif; ?>
</div>
