<?php
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabFinances;

/** @var FinanceDashboard $dashboard */
/** @var tabFinances\TabSearch $searchModel */
/** @var tabFinances\StatisticsSearch $statistics */

$statistics = $searchModel->statistics;

$columns = [
    [
        'pos' => 1,
        'id' => tabFinances\StatisticsSearch::STATISTICS_REVENUE,
        'title' => 'ВЫРУЧКА' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabFinances\StatisticsSearch::STATISTICS_REVENUE),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabFinances\StatisticsSearch::STATISTICS_REVENUE),
        'amountPlan' => null, // todo
        'color' => 'rgba(46,159,191,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabFinances\StatisticsSearch::STATISTICS_REVENUE)
    ],
    [
        'pos' => 2,
        'id' => tabFinances\StatisticsSearch::STATISTICS_PROFIT,
        'title' => 'ПРИБЫЛЬ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabFinances\StatisticsSearch::STATISTICS_PROFIT),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabFinances\StatisticsSearch::STATISTICS_PROFIT),
        'amountPlan' => null,
        'color' => 'rgba(57,194,176,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabFinances\StatisticsSearch::STATISTICS_PROFIT)
    ],    
    [
        'pos' => 3,
        'id' => tabFinances\StatisticsSearch::STATISTICS_ROIC,
        'title' => 'ROIC' . $statistics->subtitlePeriod,
        'titleFull' => 'ROIC (Рентабельность собственного капитала)' . $statistics->subtitlePeriod,
        'amount' => 100 * $statistics->getPeriodSum(tabFinances\StatisticsSearch::STATISTICS_ROIC),
        'amountPrevPeriod' => 100 * $statistics->getPrevPeriodSum(tabFinances\StatisticsSearch::STATISTICS_ROIC),
        'amountPlan' => null,
        'color' => 'rgba(65,109,147,1)',
        'customTooltip' => [
            'target' => '#roic_mini_chart',
            'title' => 'ROIC по месяцам',
            'chart' => DC::getOptions([
                'options' => [
                    'chart' => [
                        'type' => 'line',
                        'spacing' => [0,0,0,0],
                        'marginTop' => '15',
                        'height' => 170,
                        'animation' => false
                    ],
                    'title' => false,
                    'series' => [
                        [
                            'name' => 'ROIC',
                            'color' => 'rgba(65,109,147,1)',
                            'data' => array_column($statistics->getCustomTooltipStatistics('roic'), 'y'),
                        ],
                    ],
                    'xAxis' => [
                        'categories' => array_column($statistics->getCustomTooltipStatistics('roic'), 'x'),
                        'labels' => [
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap',
                                'color' => '#9198a0'
                            ]
                        ]
                    ],
                    'yAxis' => [
                        'min' => null,
                        'index' => null,
                        'labels' => [
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap',
                                'color' => '#9198a0'
                            ]
                        ]
                    ],
                    'tooltip' => ['enabled' => false],
                    'legend' => ['enabled' => false],
                    'plotOptions' => [
                        'series' => [
                            'animation' => false
                        ]
                    ]
                ]
            ], DC::CHART_LINE),
        ],
        'icon' => '%'
    ],
];
?>

<div class="row wide-statistics">
    <?php foreach ($columns as $column): ?>
        <?= $dashboard->renderBlock($this, '../tab/_partial/_statistics_column', $column) ?>
    <?php endforeach; ?>

    <?= $dashboard->renderBlock($this, '_currency', $this->params); ?>

</div>
