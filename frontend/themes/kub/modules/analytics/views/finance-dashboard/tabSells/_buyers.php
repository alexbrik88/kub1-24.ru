<?php

use frontend\models\Documents;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabSells;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */

$widgetDate = $searchModel->getDateTo();
$statisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');
$model = $searchModel->chart->getIncomeSearchModel();
$rawSellsData = $model->getRawPeriodSellsGroupedByContractor($searchModel->dateFrom, $searchModel->dateTo);
?>

<div class="row">
    <div class="col-4 pr-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_sells_top_1', [
            'model' => $model,
            'type' => Documents::IO_TYPE_OUT,
            'rawSellsData' => $rawSellsData,
        ]);
        ?>
    </div>
    <div class="col-4 pl-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_sells_top_2', [
            'model' => $model,
            'type' => Documents::IO_TYPE_OUT,
            'rawSellsData' => $rawSellsData,
        ]);
        ?>
    </div>
    <div class="col-4 pl-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_sells_top_3', [
            'model' => $model,
            'type' => Documents::IO_TYPE_OUT,
            'rawSellsData' => $rawSellsData,
        ]);
        ?>
    </div>
</div>
