<?php

use frontend\models\Documents;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabSells;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */

$widgetDate = $searchModel->getDateTo();
$statisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');

?>

<div class="row">
    <div class="col-4 pr-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_funnel_top_1', [
            'onDate' => $statisticsDate,
        ]);
        ?>
    </div>
    <div class="col-4 pl-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_funnel_top_2', [
            'onDate' => $statisticsDate,
        ]);
        ?>
    </div>
    <div class="col-4 pl-2 finance-charts-group">
        <?php
        // Charts
        echo $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_funnel_top_3', [
            'onDate' => $statisticsDate,
        ]);
        ?>
    </div>
</div>
