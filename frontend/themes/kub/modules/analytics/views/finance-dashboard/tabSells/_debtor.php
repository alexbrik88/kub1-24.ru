<?php
use frontend\models\Documents;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabSells;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */

$widgetDate = $searchModel->getDateTo();
$statisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');
$model = $searchModel->debtor->prepareAndGetModel(Documents::IO_TYPE_OUT, $statisticsDate);
?>

<div class="row">
    <div class="col-4 pr-2">
        <?= $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_debtor_top_1', [
            'reportType' => $model->report_type,
            'searchBy' => $model->search_by,
            'type' => $model->type,
            'model' => $model,
            'model2' => null,
        ]) ?>
    </div>
    <div class="col-4 pl-2">
        <?= $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_debtor_top_2', [
            'reportType' => $model->report_type,
            'searchBy' => $model->search_by,
            'type' => $model->type,
            'model' => $model,
            'model2' => null,
            'color' => 'rgba(227,6,17,1)'
        ]) ?>
    </div>
    <div class="col-4 pl-2">
        <?= $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_debtor_top_3', [
            'reportType' => $model->report_type,
            'searchBy' => $model->search_by,
            'type' => $model->type,
            'model' => $model,
            'model2' => null,
            'color' => 'rgba(233,131,117,1)'
        ]) ?>
    </div>
</div>
