<?php
use frontend\modules\analytics\models\financeDashboard\tabSells\TabSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabSells;
use frontend\widgets\RangeButtonWidget;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */
/** @var tabSells\StatisticsSearch $statistics */

$statistics = $searchModel->statistics;

$columns = [
    [
        'pos' => 1,
        'id' => tabSells\StatisticsSearch::STATISTICS_REVENUE,
        'title' => 'ПРОДАЖИ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabSells\StatisticsSearch::STATISTICS_REVENUE),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabSells\StatisticsSearch::STATISTICS_REVENUE),
        'amountPlan' => null, // todo
        'color' => 'rgba(46,159,191,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabSells\StatisticsSearch::STATISTICS_REVENUE)
    ],
    [
        'pos' => 2,
        'id' => tabSells\StatisticsSearch::STATISTICS_MONEY,
        'title' => 'Поступление ДС' . $statistics->subtitlePeriod,
        'titleFull' => 'Поступление Денежных Средств' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabSells\StatisticsSearch::STATISTICS_MONEY),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabSells\StatisticsSearch::STATISTICS_MONEY),
        'amountPlan' => null,
        'color' => 'rgba(51,109,154,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabSells\StatisticsSearch::STATISTICS_MONEY),
    ],
    [
        'pos' => 3,
        'id' => tabSells\StatisticsSearch::STATISTICS_MARGIN,
        'title' => 'Маржин. ДОХОД' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabSells\StatisticsSearch::STATISTICS_MARGIN),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabSells\StatisticsSearch::STATISTICS_MARGIN),
        'amountPlan' => null,
        'color' => 'rgba(57,194,176,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabSells\StatisticsSearch::STATISTICS_MARGIN)
    ],
];

?>

<div class="row wide-statistics">
    <?php foreach ($columns as $column): ?>
        <?= $dashboard->renderBlock($this, '../tab/_partial/_statistics_column', $column) ?>
    <?php endforeach; ?>
    <?= $dashboard->renderBlock($this, '_currency', $this->params); ?>
</div>
