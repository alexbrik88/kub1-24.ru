<?php

use frontend\modules\analytics\models\financeDashboard\tabSells;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */
/** @var tabSells\ChartSearch $chart */

$palModel = $searchModel->chart->getPalModel();

////// consts ////////////
$LEFT_DATE_OFFSET = 8;
$RIGHT_DATE_OFFSET = 7;
$periodType = "months";
$customOffsetMonths = $searchModel->getChartCurrentOffset("months", $LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET);
$customOffsetDays = $searchModel->getChartCurrentOffset("days", $LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET);

echo $dashboard->renderBlock($this, '@frontend/modules/analytics/views/finance/_charts/_chart_profit_loss_compare', [
    'model' => $palModel,
    'height' => '147px',
    'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
    'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
    'customOffsetDays' => $customOffsetDays,
    'customOffsetMonths' => $customOffsetMonths,
    'customOffset' => ($periodType === "months") ? $customOffsetMonths : $customOffsetDays,
]);
