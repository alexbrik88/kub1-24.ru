<?php
use yii\helpers\Html;
use common\models\Company;
use frontend\components\Icon;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var yii\web\View $this */
/** @var string $tab */
/** @var Company $company */
/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$title = FinanceDashboard::TAB_NAME[$searchModel::CURRENT_TAB];
?>

<!-- head -->
<div class="wrap wrap_count">
    <div class="row mb-12px align-items-center">
        <div class="column pl-2 mr-auto">
            <div class="h4 mb-0">
                <?= Html::tag('div', $title, ['style' => 'display:inline-flex']) ?>
            </div>
        </div>
        <div class="column pl-1 pr-2">
            <?= Html::button(Icon::get('cog'), ['title' => 'Настройки',
                'class' => 'button-list button-hover-transparent button-clr collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('book'), ['title' => 'Описание',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('clip-2'), ['title' => 'Скопировать ссылку',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'href' => '#copy-link-dashboard-modal',
                'data-toggle' => 'modal',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
        </div>
    </div>
    <?= $dashboard->renderBlock($this, '_statistics', $this->params) ?>
</div>

<!-- body -->
<div class="wrap wrap_count">
    <div class="row">
        <div class="col-9">
            <div class="dashboard-card wrap">
                <?= $dashboard->renderBlock($this, '_chart', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="dashboard-card wrap" style="min-height:210px">
                <?= $this->render('_indicators', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
        </div>
        <div class="col-12">
            <div class="dashboard-card wrap sibling z-index-3">
                <?= $dashboard->renderBlock($this, '_debtor', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
            <div class="dashboard-card wrap sibling z-index-2">
                <?= $dashboard->renderBlock($this, '_buyers', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
            <div class="dashboard-card wrap sibling z-index-1">
                <?= $dashboard->renderBlock($this, '_employers', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
            <div class="dashboard-card wrap">
                <?= $dashboard->renderBlock($this, '_funnel', $this->params, FinanceDashboard::RENDER_BLOCK_BODY) ?>
            </div>
        </div>
    </div>
</div>