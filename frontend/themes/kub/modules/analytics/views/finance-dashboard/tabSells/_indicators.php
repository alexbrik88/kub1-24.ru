<?php

use frontend\models\Documents;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use yii\db\Query;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabSells;

/** @var FinanceDashboard $dashboard */
/** @var tabSells\TabSearch $searchModel */

$widgetDate = $searchModel->getDateTo();
$statisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');
$id = "chart_34x"; // todo: temp
$pjaxId = "{$id}_pjax";

$totalBuyersCount = $searchModel->indicators->totalBuyersCount;
$paidBuyersCount = $searchModel->indicators->paidBuyersCount;
$paidBuyersAmount = $searchModel->indicators->paidBuyersAmount;
$avgPaymentDelay = $searchModel->indicators->avgPaymentDelay;
$conversion = ($totalBuyersCount > 0) ? (100 * $paidBuyersCount / $totalBuyersCount) : 0;
$avgCheck = ($paidBuyersCount > 0) ? (1/100 * $paidBuyersAmount / $paidBuyersCount) : 0;

$statisticInfo = [
    [
        'typeName' => 'Кол-во клиентов',
        'endBalance' => $totalBuyersCount,
        'cssClass' => '',
        'target' => ''
    ],
    [
        'typeName' => 'Кол-во купивших',
        'endBalance' => $paidBuyersCount,
        'cssClass' => '',
        'target' => ''
    ],
    [
        'typeName' => 'Конверсия',
        'endBalance' => $conversion,
        'cssClass' => '',
        'target' => '',
        'symbol' => '%'
    ],
    [
        'typeName' => 'Средний чек',
        'endBalance' => $avgCheck,
        'cssClass' => '',
        'target' => ''
    ],
    [
        'typeName' => 'Средн. срок «Счет -> Оплата»',
        'endBalance' => $avgPaymentDelay,
        'cssClass' => '',
        'target' => ''
    ],
];

?>
<div id="<?=($id)?>" class="wrap wrap-block" style="overflow: visible">
    <div class="d-flex align-items-center pb-2">
        <div class="ht-caption pb-0">
            <b>ПОКАЗАТЕЛИ</b> <span style="text-transform: none"> на </span> <?= DateHelper::format($statisticsDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
        </div>
        <?php /* $this->render('_cash_config', ['pjaxId' => $pjaxId]) */ ?>
    </div>
    <?php Pjax::begin([
        'id' => $pjaxId,
        'linkSelector' => false,
        'options' => [
            'style' => 'overflow:hidden'
        ]
    ]) ?>
    <table class="ht-table dashboard-cash">
        <?php foreach ($statisticInfo as $key => $item) : ?>
            <?php $isSubstring = in_array($item['cssClass'], ['substring']); ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <span class="bold"><?= $item['typeName']; ?></span>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <span class="bold">
                            <?= TextHelper::numberFormat($item['endBalance'] ?? 0, 2); ?><?= $item['symbol'] ?? '' ?>
                        </span>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php Pjax::end() ?>
</div>