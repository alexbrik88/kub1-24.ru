<?php
use common\models\Company;
use frontend\modules\analytics\assets\FinanceDashboardAsset;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi\TabSearch as KpiSearch;
use frontend\modules\analytics\models\financeDashboard\tabFinances\TabSearch as FinancesSearch;

/** @var yii\web\View $this */
/** @var Company $company */
/** @var string $tab */
/** @var FinanceDashboard $dashboard */
/** @var KpiSearch|FinancesSearch $searchModel */

$this->title = 'Дашборд';
$this->context->layoutWrapperCssClass = 'home-page home-page-analytics';

///////////////////////////////////////
FinanceDashboardAsset::register($this);
///////////////////////////////////////
?>

<?= $this->render('tab/modal', $this->params) ?>
<?= $this->render('tab/header', $this->params) ?>
<?= $this->render('tab/menu', $this->params) ?>

<div class="stop-zone-for-fixed-elems site-index analytics-index finance-index">
    <?php if (in_array($tab, ['kpi', 'marketing', 'sells', 'finances', 'employers', 'projects'])) {
        echo $this->render('tab'.ucfirst($tab).'/tab', $this->params);
    } else {
        echo $this->render('tab/dev', $this->params);
    } ?>
</div>