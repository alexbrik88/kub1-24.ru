<?php
use yii\helpers\Html;
use common\models\Company;
use frontend\components\Icon;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var yii\web\View $this */
/** @var string $tab */
/** @var Company $company */
/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$title = FinanceDashboard::TAB_NAME[$searchModel::CURRENT_TAB];
?>

<!-- head -->
<div class="wrap wrap_count">
    <div class="row pb-12px align-items-center">
        <div class="column pl-2 mr-auto">
            <div class="h4 mb-0">
                <?= Html::tag('div', $title, ['style' => 'display:inline-flex']) ?>
            </div>
        </div>
        <div class="column pl-1 pr-2">
            <?= Html::button(Icon::get('cog'), ['title' => 'Настройки',
                'class' => 'button-list button-hover-transparent button-clr collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('book'), ['title' => 'Описание',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
            <?= Html::button(Icon::get('clip-2'), ['title' => 'Скопировать ссылку',
                'class' => 'button-list button-hover-transparent button-clr ml-2 collapsed',
                'href' => '#copy-link-dashboard-modal',
                'data-toggle' => 'modal',
                'tab' => $searchModel::CURRENT_TAB
            ]) ?>
        </div>
    </div>
</div>

<!-- body -->
<div class="wrap wrap_count">
    <div class="row wide-statistics">
        <div class="dashboard-card-wide-column col-9">
            <div class="dashboard-card wrap sibling"></div>
            <div class="dashboard-card wrap"></div>
        </div>
        <div class="dashboard-card-column col-3">
            <div class="dashboard-card wrap sibling"></div>
            <div class="dashboard-card wrap"></div>
        </div>
    </div>
</div>