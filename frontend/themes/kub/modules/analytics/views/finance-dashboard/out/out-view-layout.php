<?php

use frontend\themes\kub\assets\KubAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<div class="wrapper__in">
    <div class="mt-4 ml-1 mr-1">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
