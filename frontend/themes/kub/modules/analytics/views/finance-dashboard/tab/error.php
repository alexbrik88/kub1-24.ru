<?php
/* @var $this yii\web\View */

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\image\EasyThumbnailImage;
use frontend\themes\kub\helpers\Icon;

/** @var Company $company */
/** @var string $tab */
/** @var ?string $dashboardErrorMessage */

$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
?>

<?php if ($dashboardErrorMessage): ?>
    <?= \yii\bootstrap\Alert::widget([
        'body' => $dashboardErrorMessage,
        'closeButton' => ['tag' => 'span'],
        'options' => [
            'id' => 'dashboard-error-' . md5($dashboardErrorMessage),
            'class' => 'alert-danger',
        ]
    ]) ?>
<?php endif;
