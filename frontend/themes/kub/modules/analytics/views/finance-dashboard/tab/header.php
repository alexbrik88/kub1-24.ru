<?php
/* @var $this yii\web\View */

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\image\EasyThumbnailImage;
use frontend\themes\kub\helpers\Icon;

/** @var Company $company */
/** @var string $tab */

$dashboardOptionSelected = Yii::$app->request->cookies->getValue('financeDashboardOption', 1); // todo

$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$dashboardOptionsList = [
    1 => [
        'title' => 'Оказание услуг B2B',
        'disabled' => false,
    ],
    2 => [
        'title' => 'Оптовая торговля',
        'disabled' => false,
    ],
    3 => [
        'title' => 'Розничная торговля',
        'disabled' => true,
        'help' => 'По запросу. Напишите нам на почту ' . $supportEmail = \Yii::$app->params['emailList']['support'] . ' В теме письма напишите Дашборд'
    ],
    4 => [
        'title' => 'Онлайн школа',
        'disabled' => true,
        'help' => 'По запросу. Напишите нам на почту ' . $supportEmail = \Yii::$app->params['emailList']['support'] . ' В теме письма напишите Дашборд'
    ],
    5 => [
        'title' => 'Оказание услуг B2C',
        'disabled' => true,
        'help' => 'По запросу. Напишите нам на почту ' . $supportEmail = \Yii::$app->params['emailList']['support'] . ' В теме письма напишите Дашборд'
    ]
];
?>

<div class="wrap d-flex flex-wrap align-items-center mb-0" style="padding: 12px 24px">
    <div class="row">
        <div class="column ml-0">
            <div class="h4 mb-0">
                <?= $company->getTitle(true) ?>
            </div>
        </div>
        <div class="column pl-0 pr-0">
            <div class="dropdown dropdown-settings pt-1">
                <button class="button-clr dropdown-btn" type="button" id="dropdownDashboardHelp" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-left: 2px;">
                    <?= Icon::get('cog-finance-dashboard', ['class' => 'dropdown-btn-icon cog-finance-dashboard-btn']) ?>
                </button>
                <ul id="cog-finance-dashboard-items" class="dropdown-popup dropdown-menu">
                    <li class="bold">Дашборды</li>
                    <li class="bold" style="border-top: 1px solid #ddd;">Выбор типа бизнеса</li>
                    <?php foreach ($dashboardOptionsList as $id => $d) : ?>
                        <?= Html::beginTag('li') ?>
                        <span <?= ($d['disabled']) ? 'title="'.$d['help'].'"' : '' ?>>
                            <?= Html::radio('dashboard-option-item,', $id == $dashboardOptionSelected, [
                                'label' => $d['title'],
                                'value' => $id,
                                'disabled' => $d['disabled'],
                                'onchange' => new \yii\web\JsExpression('location.href = "' . Url::current(['action' => 'updateFinanceDashboardOption', 'value' => $id]) . '"')
                            ]) ?>
                        </span>
                        <?= Html::endTag('li') ?>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>

    <?php if (Yii::$app->user->can(frontend\rbac\permissions\Service::HOME_LOGO)): ?>
        <div class="ml-auto">
            <?php if (is_file($path = $company->getImage('logoImage'))) : ?>
                <img src="<?= EasyThumbnailImage::thumbnailSrc($path, $logoWidth, $logoHeight, EasyThumbnailImage::THUMBNAIL_INSET, []) ?>"
                     style="max-height: 90px"/>
            <?php endif ?>
        </div>
    <?php endif; ?>
</div>
