<?php
use common\components\TextHelper;
use yii\web\JsExpression;

$chartId = 'dashboard_statistics_chart_' . ($pos ?? md5($title ?? ''));
// amount
$amount = $amount ?? null;
$amountPrevPeriod = $amountPrevPeriod ?? null;
$amountPlan = $amountPlan ?? null;
// percent
$percentPlan = ($amountPlan > 0) ? round($amount / $amountPlan * 100) : null;
$percentPrevPeriod = ($amountPrevPeriod != 0) ? round($amount / $amountPrevPeriod * 100) : ($amount ? 0 : null);
$percentPrevPeriodSymbol = ($amount > $amountPrevPeriod) ? '+' : '-';
// isNegative
$isChartNegative = $amount < 0;
// colors
$colorRed = $colorRed ?? 'rgba(227,6,17,1)';
$color = $color ?? 'rgba(103,131,228,1)';
$bgColor = $bgColor ?? 'rgba(226,229,234,1)';
// mix
$pos = $pos ?? null;
$title = $title ?? '';
$titleFull = $titleFull ?? '';
$icon = $icon ?? '₽';
$tooltipIcon = $tooltipIcon ?? '₽';
$tooltipSubtitle = $tooltipSubtitle ?? '';
// chart series
if ($percentPrevPeriod > 0) {
    $factData = [min(150.0, $percentPrevPeriod)];
    $planData = [150.0];
} elseif ($percentPrevPeriod < 0) {
    $factData = [min(150.0, abs($percentPrevPeriod))];
    $planData = [150.0];
} else {
    $factData = [$amount > 0 ? 100 : 0];
    $planData = [150.0];
}
// decimals
if (abs($amount) < 1E6) {
    $decimals = 2;
    $decimalsPostfix = '';
    $formattedAmount = $amount;
} elseif (abs($amount) < 1E8) {
    $decimals = 0;
    $decimalsPostfix = '';
    $formattedAmount = $amount;
} else {
    $decimals = 1;
    $decimalsPostfix = 'k';
    $formattedAmount = round($amount / 1E3, 1);
}

// percent
$percentFormatted = ($percentPrevPeriod !== null)
    ? ($percentPrevPeriodSymbol . TextHelper::numberFormat(abs(100 - $percentPrevPeriod), 1) . '%')
    : (null);
$percentFormattedTitle = ($percentFormatted !== null)
    ? ($percentFormatted . ' к предыдущему периоду')
    : (null);
// tooltip
$tooltip = $tooltip ?? [];
$customTooltip = $customTooltip ?? [];
if ($tooltip) {
    $tooltipOption = [
        'useHTML' => true,
        'shared' => false,
        'backgroundColor' => null,
        'borderWidth' => 0,
        'shadow' =>  false,
        'formatter' => new jsExpression("function(args) { return {$chartId}.tooltipFormatter.call(this, args); }"),
        'positioner' => new jsExpression("function(boxWidth, boxHeight, point) { return {$chartId}.tooltipPositioner.call(this, boxWidth, boxHeight, point); }"),
        //'hideDelay' => 99999,
    ];
    $tooltipData = [];
    $maxAmount = max(array_column($tooltip, 'sum')) ?: 9E99;
    $row = 0;
    foreach ($tooltip as $t) {
        if (++$row > 20) continue;
        $tooltipData[] = [
            'name' => $t['name'],
            'percent' => round($t['sum'] / $maxAmount * 100),
            'sum' => $t['sum'] / 100
        ];
    }
} elseif ($customTooltip) {
    $tooltip = $tooltipData = [true];
    $tooltipChart = json_encode($customTooltip['chart']['options'] ?? []);
    $tooltipChartTarget = ltrim($customTooltip['target'] ?? '??', '#');
    $tooltipChartTitle = $customTooltip['title'] ?? '';

    $tooltipOption = [
        'useHTML' => true,
        'shared' => false,
        'backgroundColor' => null,
        'borderWidth' => 0,
        'shadow' =>  false,
        'formatter' => new jsExpression("function(args) { 
            setTimeout(function() {
                $('#{$tooltipChartTarget}').highcharts({$tooltipChart});
            }, 100);

            return '<div class=\'ht-in-table-chart\'><div class=\'ht-in-title\'>{$tooltipChartTitle}</div><div id=\'{$tooltipChartTarget}\'></div></div>'; 
        }"),
        'positioner' => new jsExpression("function(boxWidth, boxHeight, point) { return {$chartId}.tooltipPositioner.call(this, boxWidth, boxHeight, point); }"),
        //'hideDelay' => 99999,
    ];
} else {
    $tooltipOption = ['enabled' => false];
    $tooltipData = [];
}
// arrows
$invertRedGreenArrow = $invertRedGreenArrow ?? false;
?>
<div class="dashboard-card-column col-6" style="z-index: <?=(3-($pos??0)+5)?>">
    <div class="dashboard-card wrap" style="z-index: <?=(3-($pos??0)+5)?>">
        <div class="dashboard-card-title text-truncate" title="<?= $titleFull ?: $title; ?>">
            <div class="d-inline-block"><?= $title; ?></div>
            <div class="d-inline-block" style="float:right"><?= '' ?></div>
        </div>
        <div class="dashboard-card-main <?= ($amount < 0) ? 'text-red' : '' ?>">
            <?= TextHelper::numberFormat($formattedAmount / 100, $decimals) ?><?= $decimalsPostfix ?>
            <?= $icon ?>
            <?php if ((abs($amountPrevPeriod) > 0 || abs($amount) > 0)): ?>
                <span class="nowrap">
                    <span class="dashboard-card-main-arrow" <?= ($percentFormatted) ? "title='{$percentFormattedTitle}'" : "" ?>>
                        <img class="<?= ($invertRedGreenArrow) ? 'inverted':'' ?>"
                             width="14"
                             src="/img/abc/<?=($amount >= $amountPrevPeriod)
                                 ? ($invertRedGreenArrow ? 'arrow_red.svg' : 'arrow_green.svg')
                                 : ($invertRedGreenArrow ? 'arrow_green.svg' : 'arrow_red.svg') ?>"/>
                    </span>
                    <span class="dashboard-card-main-description">
                        <?= $percentFormatted ?>
                    </span>
                </span>
            <?php endif; ?>
        </div>
        <div class="dashboard-card-main">
            <?= ($percentPlan === null) ? null
                : TextHelper::numberFormat($percentPlan, 0) . '%' ?>
            <span class="dashboard-card-main-description">
                <?= ($percentPlan === null) ? 'нет плана' : ' от плана' ?>
            </span>
        </div>
        <div class="dashboard-card-chart">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => $chartId,
                'class' => 'finance-charts',
                'scripts' => [
                    'themes/grid-light',
                    'modules/pattern-fill',
                    'highcharts-more'
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'exporting' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0, 0, 25, 0],
                        'margin' => [0,0,25,0],
                        'height' => 50,
                        'inverted' => true,
                        'animation' => false
                    ],
                    'legend' => false,
                    'tooltip' => $tooltipOption,
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'max' => 150,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => true,
                        'tickInterval' => 50,
                        'tickWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ],
                            'formatter' => new JsExpression(" function() { return this.value + '%'; }"),
                        ]
                    ],
                    'xAxis' => [
                        'categories' => ['_not_used_'],
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => false
                    ],
                    'series' => [
                        [
                            'name' => 'План',
                            'pointPadding' => 0,
                            'data' => $planData,
                            'color' => $bgColor,
                            'borderColor' => $bgColor,
                            'enableMouseTracking' => false
                        ],
                        [
                            'name' => 'Факт',
                            'data' => $factData,
                            'color' => ($isChartNegative) ? $colorRed : $color,
                            'borderColor' => ($isChartNegative) ? $colorRed : $color,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => ($isChartNegative) ? $colorRed : $color,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'План на плане',
                            'data' => [100],
                            'type' => 'scatter',
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 20,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => ['opacity' => 1]
                            ],
                            'pointPadding' => 0,
                            'cursor' => 'pointer',
                        ],
                        'scatter' => [
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(0, 20, 36, .75)',
                                'radius' => 13.5
                            ],
                            'states' => [
                                'inactive' => ['opacity' => 1]
                            ],
                            'pointStart' => 0,
                            'pointPlacement' => 0,
                            'stickyTracking' => false,
                            'showInLegend' => false,
                            'enableMouseTracking' => false
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>



<?php if ($tooltip): // TOOLTIP // ?>
    <?php
    $htmlHeader = '
    <div class="ht-in-table-wrap">
    <table class="table-bleak ht-in-table">
        <tr class="title">
            '.($tooltipSubtitle
                ? '<th colspan="2" class="text-left">{title}</th><th>'.$tooltipSubtitle.'</th>'
                : '<th colspan="3" class="text-left">{title}</th>').'
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>';
    $htmlData = '
        <tr>
            <td><div class="ht-title">{name}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart" style="width: {percent}%; background-color: {color_rgb};"></div></div></td>
            <td><div class="ht-total nowrap">{sum} '.$tooltipIcon.'</div></td>
        </tr>';
    $htmlFooter = '
    </table>
    </div>';

    $htmlHeader = trim(str_replace(["\r", "\n", "'"], "", $htmlHeader));
    $htmlData = trim(str_replace(["\r", "\n", "'"], "", $htmlData));
    $htmlFooter = trim(str_replace(["\r", "\n", "'"], "", $htmlFooter));
    ?>
    <script>
        <?=($chartId)?> = {
            title: '<?= htmlspecialchars($title) ?>',
            color: '<?= $color ?>',
            colorRed: '<?= $colorRed ?>',
            totalAmount: '<?= $amount ?>',
            tooltipTemplate: {
                htmlHeader: '<?= $htmlHeader ?>',
                htmlData: '<?= $htmlData ?>',
                htmlFooter: '<?= $htmlFooter ?>',
            },
            tooltipData: <?= json_encode($tooltipData) ?>,
            tooltipFormatter: function(args) {
                const that = <?=($chartId)?>; 
                let title = that.title;
                let totalAmount = that.amount;
                let htmlHeader = that.tooltipTemplate.htmlHeader;
                let htmlData = that.tooltipTemplate.htmlData;
                let htmlFooter = that.tooltipTemplate.htmlFooter;
                let arr = that.tooltipData;
                let tooltipHtml = htmlHeader;
                let color = that.color;
                let colorRed = that.colorRed;

                tooltipHtml = tooltipHtml
                    .replace("{title}", title)
                    .replace("{point.x}", '')
                    .replace("{fact.y}", Highcharts.numberFormat(totalAmount, 0, ',', ' '));

                if (arr) {
                    arr.forEach(function(data) {
                        if (data.sum >= 0) {

                            // todo: temp
                            if (data.name==="Другие доходы")
                                color = 'rgba(46,159,191,1)';
                            else
                                color = that.color;

                            tooltipHtml += htmlData
                                .replace("{name}", data.name)
                                .replace("{percent}", data.percent)
                                .replace("{sum}", Highcharts.numberFormat(data.sum, 0, ',', ' '))
                                .replace("{color_rgb}", color)
                        } else {
                            tooltipHtml += htmlData
                                .replace("{name}", data.name)
                                .replace("{percent}", String(Math.abs(Number(data.percent))))
                                .replace("{sum}", Highcharts.numberFormat(data.sum, 0, ',', ' '))
                                .replace("{color_rgb}", colorRed)
                        }
                    });
                } else {
                    tooltipHtml += '<tr><td colspan="3"></td></tr>';
                }

                tooltipHtml += htmlFooter;

                return tooltipHtml;
            },
            tooltipPositioner: function(boxWidth, boxHeight, point) {
                return {x: 0, y: point.plotY + 5};
            }            
        };
    </script>
<?php endif; ?>
