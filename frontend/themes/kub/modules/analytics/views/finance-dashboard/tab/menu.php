<?php
use yii\bootstrap\Nav;
use common\models\Company;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard as Dashboard;

/** @var Company $company */
/** @var string $tab */

$uid = Yii::$app->request->get('uid');
?>

<div class="nav-finance">
    <div class="nav-tabs-row mb-2">
        <?php
            echo Nav::widget([
                'id' => 'dashboard-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-3'],
                'items' => [
                    [
                        'label' => 'Основные KPI',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => ''],
                        'active' => !$tab || $tab == Dashboard::TAB_KPI,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Платежи на сегодня',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_TODAY_PAYMENTS],
                        'active' => $tab == Dashboard::TAB_TODAY_PAYMENTS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Продажи',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_SELLS],
                        'active' => $tab == Dashboard::TAB_SELLS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Маркетинг',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_MARKETING],
                        'active' => $tab == Dashboard::TAB_MARKETING,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Финансы',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_FINANCES],
                        'active' => $tab == Dashboard::TAB_FINANCES,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Проекты',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_PROJECTS],
                        'active' => $tab == Dashboard::TAB_PROJECTS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => Yii::$app->request->cookies->getValue('financeDashboardOption') != 2 // todo
                    ],
                    [
                        'label' => 'Сбыт и запасы',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_STOCKS],
                        'active' => $tab == Dashboard::TAB_STOCKS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => Yii::$app->request->cookies->getValue('financeDashboardOption') > 1 // todo
                    ],
                    [
                        'label' => 'Сотрудники',
                        'url' => [null, 'uid' => $uid ?? null, 'tab' => Dashboard::TAB_EMPLOYERS],
                        'active' => $tab == Dashboard::TAB_EMPLOYERS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);
        ?>
    </div>
</div>
