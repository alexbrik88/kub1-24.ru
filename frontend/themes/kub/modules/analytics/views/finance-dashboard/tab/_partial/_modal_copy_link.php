<?php
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use frontend\components\Icon;
use frontend\modules\analytics\models\financeDashboard\OutFinanceDashboard;

$outViewModel = OutFinanceDashboard::findOrCreate($company);
?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'copy-link-dashboard-modal',
]); ?>
    <h4 class="modal-title mb-4">Поделиться ссылкой на дашборд</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <span class="text-grey">Просматривать смогут все, у кого есть ссылка.</span>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Ссылка на дашборд
                </label>
                <?= \yii\helpers\Html::input('search', 'link', $outViewModel->getLink(), [
                    'id' => 'copy-link-dashboard-input',
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-start">
        <?= \yii\helpers\Html::button(Icon::get('clip-2') . '<span class="ladda-label">Скопировать ссылку</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-regular_red button-clr --ladda-button',
            'data-url' => Url::to(['#']),
            'data-style' => 'expand-right',
            'onclick' => 'copyTextToClipboard(document.getElementById("copy-link-dashboard-input").value)',
            'disabled' => true,
            'title' => 'В разработке'
        ]); ?>
        <button type="button" class="ml-3 button-clr button-width button-regular button-hover-transparent" onclick="location.href='/site/index/?action=refreshFinanceDashboardLink'">Удалить ссылку</button>
        <button type="button" class="ml-auto button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>