<?php

use common\models\Company;
use frontend\components\Icon;
use common\models\employee\Employee;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingUserConfig;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var yii\web\View $this */
/** @var string $tab */
/** @var Company $company */
/** @var Employee $identity */
/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$title = FinanceDashboard::TAB_NAME[$searchModel::CURRENT_TAB];
$marketingUserConfig = MarketingUserConfig::findOne($identity->id);
if (!$marketingUserConfig) {
    $marketingUserConfig = new MarketingUserConfig(['employee_id' => $identity->id]);
    $marketingUserConfig->setDefaults();
}
?>

<!-- body -->
<?= $this->render('@frontend/modules/analytics/modules/marketing/views/widgets/partial/_header_charts', [
    'company' => $company,
    'channelType' => MarketingChannel::DASHBOARD_TOTALS,
    'marketingUserConfig' => $marketingUserConfig,
    'blockTitle' => 'Маркетинг',
    'showConfig' => false,
    'additionalButtons' => true
]) ?>