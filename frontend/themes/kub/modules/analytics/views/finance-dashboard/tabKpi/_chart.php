<?php
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$userConfig = Yii::$app->user->identity->config;

$LEFT_DATE_OFFSET = 6;
$RIGHT_DATE_OFFSET = 6;
$periodType = ($userConfig->report_odds_chart_period ?? 0) ? "months" : "days";
$customOffsetMonths = $searchModel->getChartCurrentOffset("months", $LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET);
$customOffsetDays = $searchModel->getChartCurrentOffset("days", $LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET);

/**
 * ActiveChart
 */
echo $dashboard->renderBlock($this, '@frontend/modules/analytics/views/finance/_charts/_chart_plan_fact_days', [
    'LEFT_DATE_OFFSET' => 6,
    'RIGHT_DATE_OFFSET' => 6,
    'MOVE_OFFSET' => 1,
    'ON_PAGE' => 'ODDS',
    'customOffsetDays' => $customOffsetDays,
    'customOffsetMonths' => $customOffsetMonths,
    'customOffset' => ($periodType === "months") ? $customOffsetMonths : $customOffsetDays,
    'walletFilterSelect2' => '',
], false);