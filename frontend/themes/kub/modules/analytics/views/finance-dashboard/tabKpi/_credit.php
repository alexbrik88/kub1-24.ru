<?php
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\DebtCalculatorFactory;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\ArrayHelper;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$chartId = "chart_44"; // todo: rename me

//////////////////////////////////////////////////
$widgetDate = $searchModel->getDateTo();
$creditStatisticsDate =  ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-t');
$creditStatisticsMonth = ArrayHelper::getValue(
    AbstractFinance::$monthIn, substr($creditStatisticsDate, 5,2));
$credits = $searchModel->credit->getModels();
//////////////////////////////////////////////////

$debt = [];
$repay = [
    'body' => 0,
    'percent' => 0
];
$totalDebt = 0;

foreach ($credits as $credit) {
    /** @var Credit $credit */
    $wallet = $credit->wallet_type.'_'.$credit->wallet_id;
    $calculator = (new DebtCalculatorFactory)->createCalculator($credit);
    $_debt = $credit->credit_amount;
    $_repayBody = $calculator->schedule->getTotalDebtOnDate($creditStatisticsDate, true);
    $_repayPercent = $calculator->schedule->getTotalDebtOnDate($creditStatisticsDate, false, true);
    // debt
    if (!isset($debt[$wallet])) {
        $debt[$wallet] = [
            'name' => $credit->wallet->name,
            'amount' => 0,
            'isGroup' => false,
            'target' => null,
            'class' => "credit-substring-debt",
        ];
    }
    // total sum
    $debt[$wallet]['amount'] += 100 * $_debt;
    $totalDebt += 100 * $_debt;
    // repay
    $repay['body'] += 100 * $_repayBody;
    $repay['percent'] += 100 * $_repayPercent;
}

$stat = array_merge([
    // Debt amount
    [
        'name' => 'Сумма кредитов',
        'amount' => $totalDebt,
        'isGroup' => true,
        'target' => '.credit-substring-debt',
        'class' => 'toggle-string',
    ]
], array_values($debt), [
    [
        'name' => 'Погасить в ' . $creditStatisticsMonth,
        'amount' => $repay['body'] + $repay['percent'],
        'isGroup' => true,
        'target' => '.credit-substring-repay',
        'class' => 'toggle-string',
    ],
    [
        'name' => 'Кредит',
        'amount' => $repay['body'],
        'isGroup' => false,
        'target' => null,
        'class' => 'credit-substring-repay',
    ],
    [
        'name' => 'Проценты',
        'amount' => $repay['percent'],
        'isGroup' => false,
        'target' => null,
        'class' => 'credit-substring-repay',
    ],
]);
?>
<div id="<?=($chartId)?>" class="wrap wrap-block w-100" style="overflow: hidden">
    <div class="ht-caption">
        <b>КРЕДИТЫ</b> <span style="text-transform: none"> на </span> <?= $searchModel->subtitleLastDate ?>
    </div>
    <table class="ht-table dashboard-cash w-100">
        <?php foreach ($stat as $key => $item) : ?>
            <?php $isSubstring = in_array($item['class'], ['credit-substring-debt', 'credit-substring-repay']); ?>
            <tr class="<?= $item['class'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['class'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <?= $item['name']; ?>
                            <?php if ('toggle-string' != $item['class']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <?= TextHelper::invoiceMoneyFormat($item['amount'], 2); ?>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<script>
    $(document).on("click", "#<?=($chartId)?> table.dashboard-cash .toggle-string .flow-label", function() {
        $("#<?=($chartId)?> table.dashboard-cash " + $(this).data("target") + "").slideToggle(150);
    });
</script>


