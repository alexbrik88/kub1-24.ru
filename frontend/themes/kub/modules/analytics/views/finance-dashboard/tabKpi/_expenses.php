<?php
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\web\JsExpression;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$expensesSearchModel = $searchModel->chart->getExpensesSearchModel();

////////// chart params /////////////////////////
$id = 'chart-top-2';
$chartMaxRows = 5;
$customPurse = $customPurse ?? null;
$customMonth = $customMonth ?? date('m');
$cropNamesLength = 12;
$croppedNameLength = 10;
////////////////////////////////////////////////

$model = new ExpensesSearch();
$dateFrom = $searchModel->getDateFrom();
$dateTo = $searchModel->getDateTo();
$mainData = $model->getChartStructureByArticles($dateFrom, $dateTo, $customPurse);

$yAxis['categories'] = array_slice(array_column($mainData, 'name'), 0, $chartMaxRows);
$yAxis['factData'] = array_slice(array_column($mainData, 'amountFact'), 0, $chartMaxRows);
$yAxis['planData'] = array_slice(array_column($mainData, 'amountPlan'), 0, $chartMaxRows);
$yAxis['articlesIds'] = array_slice(array_column($mainData, 'id'), 0, $chartMaxRows);
$yAxis['expenseTotalAmount'] = array_sum($yAxis['factData']);
$totalAmount = (int)$yAxis['expenseTotalAmount'];
?>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="ht-caption noselect">
            СПИСАНИЯ  <span style="text-transform: none"><?= $searchModel->getSubtitlePeriod() ?></span>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= Highcharts::widget([
                    'id' => $id,
                    'options' => [
                        'chart' => [
                            'type' => 'column',
                            'inverted' => true,
                            'height' => DC::calcChartColumnHeight(count($yAxis['categories']), 40),
                            'spacing' => [0,0,0,0]
                        ],
                        'credits' => [
                            'enabled' => false
                        ],
                        'legend' => [
                            'layout' => 'horizontal',
                            'align' => 'right',
                            'verticalAlign' => 'top',
                            'backgroundColor' => '#fff',
                            'itemStyle' => [
                                'fontSize' => '11px',
                                'color' => '#9198a0'
                            ],
                            'symbolRadius' => 2
                        ],
                        'tooltip' => [
                            'useHTML' => true,
                            'shape' => 'rect',
                            'backgroundColor' => "rgba(255,255,255,1)",
                            'borderColor' => '#ddd',
                            'borderWidth' => '1',
                            'borderRadius' => 8,
                            'formatter' => new jsExpression("
                                function(args) {
                    
                                    var index = this.series.data.indexOf( this.point );
                                    var series_index = this.series.index;
                                    var totalAmount = {$totalAmount} || 9E99;
                    
                                    return '<span class=\"title\">' + this.point.category + '</span>' +
                                        '<table class=\"indicators\">' +
                                            ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                            ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                            ('<tr>' + '<td class=\"gray-text\">Доля в расходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[0].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                                        '</table>';
                                }
                            "),
                            'positioner' => new \yii\web\JsExpression('
                                function (boxWidth, boxHeight, point) { 
                                    var x = this.chart.containerWidth - boxWidth;
                                    var y = point.plotY + 50;
                                    //console.log(x)
                                    return {x: x, y: y}; 
                                }
                            '),
                        ],
                        'title' => ['text' => ''],
                        'series' => [
                            [
                                'name' => 'Факт',
                                'color' => 'rgba(243,183,46,1)',
                                'states' => DC::getSerieState('rgba(243,183,46,1)'),
                                'data' => $yAxis['factData'],
                            ],
                            [
                                'name' => 'План',
                                'color' => 'rgba(0,0,0,1)',
                                'data' => $yAxis['planData'],
                                'pointWidth' => 4,
                                'borderWidth' => 0,
                            ],
                        ],
                        'yAxis' => [
                            'min' => 0,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'lineWidth' => 0,
                            'endOnTick' => false,
                            'tickPixelInterval' => 1,
                            'visible' => false
                        ],
                        'xAxis' => [
                            'categories' => $yAxis['categories'],
                            'minorGridLineWidth' => 0,
                            'lineWidth' => 0,
                            'gridLineWidth' => 0,
                            'offset' => 0,
                            'labels' => [
                                'align' => 'left',
                                'reserveSpace' => true,
                                'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                            ],
                        ],
                        'plotOptions' => [
                            'column' => [
                                'pointWidth' => 18,
                                'dataLabels' => [
                                    'enabled' => false,
                                ],
                                'grouping' => false,
                                'shadow' => false,
                                'borderWidth' => 0,
                                'borderRadius' => 3,
                                'states' => [
                                    'inactive' => [
                                        'opacity' => 1
                                    ],
                                ],
                            ],
                        ],
                    ]
                ]
            ); ?>
        </div>
    </div>
</div>