<?php
use frontend\rbac\permissions;
use frontend\modules\analytics\models\financeDashboard\tabKpi\CashSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var FinanceDashboard $dashboard */

$showBanks = Yii::$app->user->can(permissions\Service::HOME_CASH);
?>

<?= $dashboard->renderBlock($this, '@frontend/views/site/_indexPartials/_cash', [
    'id' => 'chart_34',
    'cashStatisticInfo' => CashSearch::getCashStatisticInfo($showBanks),
    'foraignCashStatisticInfo' => CashSearch::getForaignCashStatisticInfo($showBanks),
], false) ?>