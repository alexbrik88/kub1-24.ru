<?php
use yii\web\JsExpression;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$incomeSearchModel = $searchModel->chart->getIncomeSearchModel();

// consts top charts
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 5;
$cropNamesLength = 12;
$croppedNameLength = 10;

///////////////// dynamic vars ///////////
$customPeriod = $searchModel->getDateRange();
$customMonth = null;
//
$customPurse = $customPurse ?? null;
$customArticle = $customArticle ?? null;
$customClient = $customClient ?? null;
$customProduct = $customProduct ?? null;
$customEmployee = $customEmployee ?? null;
/////////////////////////////////////////

$dataBuyers = $incomeSearchModel->getChartStructureByBuyers(null, $customPurse, $customArticle, $customProduct, $customEmployee, $customPeriod);
// arrays top charts
$categories = array_slice(array_column($dataBuyers, 'name'), 0, $chartMaxRows);
$factData = array_slice(array_column($dataBuyers, 'amountFact'), 0, $chartMaxRows);
$planData = array_slice(array_column($dataBuyers, 'amountPlan'), 0, $chartMaxRows);
$totalAmount = (int)array_sum(array_column($dataBuyers, 'amountFact'));
$clientsIds = array_slice(array_column($dataBuyers, 'id'), 0, $chartMaxRows);
$calculatedChartHeight = count($categories) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;

///////////////// colors ///////
$color1 = 'rgba(57,194,176,1)';
$color2 = 'rgba(226,229,234,1)';
////////////////////////////////
?>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect">
            ПОКУПАТЕЛИ <span style="text-transform: none"><?= $searchModel->getSubtitlePeriod() ?></span>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-top-1',
                'class' => 'finance-charts',
                'scripts' => [
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {
                        
                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var totalAmount = {$totalAmount} || 9E99;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' + 
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +                                         
                                        ('<tr>' + '<td class=\"gray-text\">Доля в приходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[1].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                                    '</table>';
                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('
                            function (boxWidth, boxHeight, point) { 
                                var x = this.chart.containerWidth - boxWidth;
                                var y = point.plotY + 50;
                                //console.log(x)
                                return {x: x, y: y}; 
                            }
                        '),
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'План',
                            'pointPadding' => 0,
                            'data' => $planData,
                            'color' => $color2,
                            'borderColor' => $color2,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ],
                            'legendIndex' => 1
                        ],
                        [
                            'name' => 'Факт',
                            'pointPadding' => 0,
                            'data' => $factData,
                            'color' => $color1,
                            'borderColor' => $color1,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
