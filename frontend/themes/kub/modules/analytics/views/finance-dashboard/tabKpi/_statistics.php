<?php
use frontend\modules\analytics\models\financeDashboard\tabKpi\TabSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */
/** @var tabKpi\StatisticsSearch $statistics */

$statistics = $searchModel->statistics;

$columns = [
    [
        'pos' => 1,
        'id' => tabKpi\StatisticsSearch::STATISTICS_REVENUE,
        'title' => 'ПРОДАЖИ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabKpi\StatisticsSearch::STATISTICS_REVENUE),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabKpi\StatisticsSearch::STATISTICS_REVENUE),
        'amountPlan' => null, // todo
        'color' => 'rgba(46,159,191,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabKpi\StatisticsSearch::STATISTICS_REVENUE)
    ],
    [
        'pos' => 2,
        'id' => tabKpi\StatisticsSearch::STATISTICS_EXPENSES,
        'title' => 'РАСХОДЫ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabKpi\StatisticsSearch::STATISTICS_EXPENSES),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabKpi\StatisticsSearch::STATISTICS_EXPENSES),
        'amountPlan' => null,
        'color' => 'rgba(243,183,46,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabKpi\StatisticsSearch::STATISTICS_EXPENSES),
        'invertRedGreenArrow' => true,
    ],
    [
        'pos' => 3,
        'id' => tabKpi\StatisticsSearch::STATISTICS_PROFIT,
        'title' => 'ПРИБЫЛЬ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabKpi\StatisticsSearch::STATISTICS_PROFIT),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabKpi\StatisticsSearch::STATISTICS_PROFIT),
        'amountPlan' => null,
        'color' => 'rgba(57,194,176,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabKpi\StatisticsSearch::STATISTICS_PROFIT)
    ],
];

?>

<div class="row wide-statistics">
    <?php foreach ($columns as $column): ?>
        <?= $dashboard->renderBlock($this, '../tab/_partial/_statistics_column', $column) ?>
    <?php endforeach; ?>

    <?= $dashboard->renderBlock($this, '_currency', $this->params); ?>

</div>
