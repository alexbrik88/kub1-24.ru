<?php

use frontend\models\Documents;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\rbac\permissions;
use common\components\TextHelper;
use common\components\debts\DebtsHelper;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabKpi;
use common\components\date\DateHelper;

/** @var FinanceDashboard $dashboard */
/** @var tabKpi\TabSearch $searchModel */

$widgetDate = $searchModel->getDateTo();
$statisticsDate = ($widgetDate <= date('Y-m-d')) ? $widgetDate : date('Y-m-d');
$modelOut = $searchModel->debtor->prepareAndGetModel(Documents::IO_TYPE_OUT, $statisticsDate);
$modelIn = $searchModel->debtor->prepareAndGetModel(Documents::IO_TYPE_IN, $statisticsDate);
$totalDebtsData = [
    'out' => $modelOut->getTotalDebts(),
    'in' => $modelIn->getTotalDebts()
];
$topChartData = [
    'out' => $modelOut->getTopByContractors(5),
    'in' => $modelIn->getTopByContractors(5),
];
?>

<?php if (Yii::$app->user->can(permissions\Service::HOME_CASH)): ?>
    <div class="row" style="margin-bottom: 12px">
        <div class="col-6 mb-0">
            <div class="dashboard-card wrap">
                <div class="ht-caption">
                    НАМ ДОЛЖНЫ <span style="text-transform:none">на <?= DateHelper::format($statisticsDate, 'd.m.Y', 'Y-m-d') ?></span>
                    <div class="site-index-more-link"><?= \yii\helpers\Html::a('Подробнее', ['/analytics/finance/debtor', 'type' => 2], ['class' => 'link']) ?></div>
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_table', ['totalDebts' => $totalDebtsData['out']]) ?>
                <div class="devider mt-3 mb-3"></div>
                <div class="ht-caption">
                    ТОП 5 Должников
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_chart', ['topData' => $topChartData['out'], 'type' => 2]) ?>
                <div class="devider mt-3 mb-3"></div>
                <div class="ht-caption">
                    ДИНАМИКА ПО ДЕБИТОРКЕ
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_week_debit', ['CENTER_DATE' => $statisticsDate]) ?>
            </div>
        </div>
        <div class="col-6 mb-0">
            <div class="dashboard-card wrap">
                <div class="ht-caption">
                    МЫ ДОЛЖНЫ <span style="text-transform:none">на <?= DateHelper::format($statisticsDate, 'd.m.Y', 'Y-m-d') ?></span>
                    <div class="site-index-more-link"><?= \yii\helpers\Html::a('Подробнее', ['/analytics/finance/debtor', 'type' => 1], ['class' => 'link']) ?></div>
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_table', ['totalDebts' => $totalDebtsData['in']]) ?>
                <div class="devider mt-3 mb-3"></div>
                <div class="ht-caption">
                    ТОП 5 Кредиторов
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_chart', ['topData' => $topChartData['in'], 'type' => 1]) ?>
                <div class="devider mt-3 mb-3"></div>
                <div class="ht-caption">
                    ДИНАМИКА ПО КРЕДИТОРКЕ
                </div>
                <?= $dashboard->renderBlock($this, '__debtor_week_credit', ['CENTER_DATE' => $statisticsDate]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>
