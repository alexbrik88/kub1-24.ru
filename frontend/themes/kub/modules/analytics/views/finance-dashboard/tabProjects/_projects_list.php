<?php
use yii\helpers\Html;
use common\models\project\Project;
use frontend\modules\analytics\models\financeDashboard\tabProjects;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;

/** @var FinanceDashboard $dashboard */
/** @var tabProjects\TabSearch $searchModel */
/** @var tabProjects\StatisticsSearch $statistics */

$dropdownItems = $searchModel->getFullProjectsList();
$checkedDropdownItems = Yii::$app->request->cookies->getValue('project_chart_series_user_filter', []);
$checkedAllDropdownItems = empty($checkedDropdownItems);

if ($dropdownItems): ?>
    <div class="w-100">
        <div class="form-group mb-3">
            <div class="dropdown">
                <a class="a-filter button-regular button-hover-content-red text-left pl-3 pr-5 w-100" href="#" style="width: 175px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="nowrap">Проекты: <span class="filter-title"><?= ($checkedDropdownItems) ? (count($checkedDropdownItems) . ' шт.') : 'все' ?></span></span>
                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                    </svg>
                </a>
                <div class="dropdown-menu pt-2 pb-0" data-id="dropdown3" style="width: 175px;">
                    <div style=" max-height: 195px; overflow-y: auto ">
                        <div class="pl-2 pr-0">
                            <?= Html::checkbox('user_only_project_all', $checkedAllDropdownItems, [
                                'class' => 'form-group user_only_project_all',
                                'label' => '<span class="label">Все проекты</span>',
                                'value' => 'all',
                                'autocomplete' => 'off',
                            ]) ?>
                        </div>
                        <?php foreach ($dropdownItems as $itemId => $itemName): ?>
                            <div class="pl-2 pr-0">
                                <?php $checked = in_array($itemId, $checkedDropdownItems) ?>
                                <?php $name = (mb_strlen($itemName) > 16) ? (mb_substr($itemName, 0, 14).'...') : $itemName ?>
                                <?= Html::checkbox('user_only_project', $checked, [
                                    'class' => 'form-group user_only_project',
                                    'label' => '<span class="label">'.htmlspecialchars($name).'</span>',
                                    'value' => $itemId,
                                    'autocomplete' => 'off',
                                ]) ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="mt-2 p-2 text-center">
                        <button class="user_only_project_apply button-width button-regular button-hover-grey" type="button">Применить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
