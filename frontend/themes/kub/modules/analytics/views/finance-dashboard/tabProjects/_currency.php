<?php
use common\components\helpers\ArrayHelper;
use common\components\date\DateHelper;
use frontend\modules\analytics\models\financeDashboard\tabProjects;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use common\models\currency\CurrencyRate;

/** @var FinanceDashboard $dashboard */
/** @var tabProjects\TabSearch $searchModel */

?>

<div class="dashboard-card-column col-6">

    <?= frontend\widgets\RangeButtonWidget::widget(['byMonths' => true]); ?>
    <?= $dashboard->renderBlock($this, '_projects_list', $this->params) ?>

</div>
