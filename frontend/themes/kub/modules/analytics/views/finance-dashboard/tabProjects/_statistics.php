<?php
use frontend\modules\analytics\models\financeDashboard\tabProjects\TabSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\tabProjects;

/** @var FinanceDashboard $dashboard */
/** @var tabProjects\TabSearch $searchModel */
/** @var tabProjects\StatisticsSearch $statistics */

$statistics = $searchModel->statistics;

$columns = [
    [
        'pos' => 1,
        'id' => tabProjects\StatisticsSearch::STATISTICS_PROJECTS_COUNT,
        'title' => 'ПРОЕКТЫ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabProjects\StatisticsSearch::STATISTICS_PROJECTS_COUNT),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabProjects\StatisticsSearch::STATISTICS_PROJECTS_COUNT),
        'amountPlan' => null,
        'color' => 'rgb(1,88,157)',
        'tooltip' => $statistics->getTooltipStatistics(tabProjects\StatisticsSearch::STATISTICS_PROJECTS_COUNT),
        'icon' => 'шт.',
        'tooltipIcon' => 'дн.',
        'tooltipSubtitle' => 'До окончания',
    ],
    [
        'pos' => 2,
        'id' => tabProjects\StatisticsSearch::STATISTICS_REVENUE,
        'title' => 'ВЫРУЧКА' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabProjects\StatisticsSearch::STATISTICS_REVENUE),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabProjects\StatisticsSearch::STATISTICS_REVENUE),
        'amountPlan' => null,
        'color' => 'rgba(46,159,191,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabProjects\StatisticsSearch::STATISTICS_REVENUE),
    ],
    [
        'pos' => 3,
        'id' => tabProjects\StatisticsSearch::STATISTICS_PROFIT,
        'title' => 'ПРИБЫЛЬ' . $statistics->subtitlePeriod,
        'amount' => $statistics->getPeriodSum(tabProjects\StatisticsSearch::STATISTICS_PROFIT),
        'amountPrevPeriod' => $statistics->getPrevPeriodSum(tabProjects\StatisticsSearch::STATISTICS_PROFIT),
        'amountPlan' => null,
        'color' => 'rgba(57,194,176,1)',
        'tooltip' => $statistics->getTooltipStatistics(tabProjects\StatisticsSearch::STATISTICS_PROFIT)
    ],
];

?>

<div class="row wide-statistics">
    <?php foreach ($columns as $column): ?>
        <?= $dashboard->renderBlock($this, '../tab/_partial/_statistics_column', $column) ?>
    <?php endforeach; ?>

    <?= $dashboard->renderBlock($this, '_currency', $this->params) ?>

</div>


