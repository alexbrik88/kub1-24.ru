<?php

use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\assets\AnalyticsAsset;
use frontend\themes\kub\widgets\VideoInstructionWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Url;
use frontend\modules\analytics\models\PlanFactSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View
 * @var $searchModel PlanFactSearch
 *
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 *
 * @var $plan []
 * @var $growingPlan []
 * @var $totalPlan []
 *
 * @var $activeTab integer
 * @var $periodSize string
 */

AnalyticsAsset::register($this);

$this->title = 'План-Факт' . $searchModel->multiCompanyManager->getSubTitle();

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_plan_fact_help ?? false;
$showChartPanel = $userConfig->report_plan_fact_chart ?? false;
$isExpanded = $userConfig->report_plan_fact_expanded;
if ($activeTab == PlanFactSearch::TAB_BY_PURSE_DETAILED) {
    $viewTable = '_partial/plan_fact_table_detailed';
} else {
    $viewTable = '_partial/plan_fact_table';
}
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <?= $this->render('@frontend/modules/analytics/views/multi-company/button') ?>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= VideoInstructionWidget::widget([
                    'title' => 'Видео инструкция по отчету',
                    'src' => 'https://www.youtube.com/embed/goLHYb8yKaY'
                ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= Html::beginForm(['plan-fact', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_plan_fact_chart">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_charts/_chart_plan_fact_days_2', [
            'model' => $searchModel,
        ]); ?>
    </div>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_plan_fact_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/plan_fact_panel')
        ?>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="ml-0 pl-0">
        <div class="mb-2">
            <?= TableConfigWidget::widget([
                'sortingItemsTitle' => 'Столбцы',
                'sortingItems' => [
                    [
                        'attribute' => 'not_expanded',
                        'label' => 'Свернуто',
                        'checked' => !$isExpanded,
                        'data-url' => 'plan-fact'
                    ],
                    [
                        'attribute' => 'expanded',
                        'label' => 'Развернуто',
                        'checked' => $isExpanded,
                        'data-url' => 'plan-fact'
                    ],
                ]
            ]); ?>
        </div>
    </div>
    <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
        <div class="mb-2">
            <?= \common\components\helpers\Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr ml-1',
                    'title' => 'Скачать в Excel',
                ]); ?>
        </div>
    <?php endif; ?>

    <div class="ml-2 mb-2">
        <div class="pl-1">
            <?= TableViewWidget::widget(['attribute' => 'table_view_finance_plan_fact']) ?>
        </div>
    </div>

    <div class="column ml-auto mb-2 pr-0 tooltip2-deviation" data-tooltip-content="#tooltip_deviation_block">
        <?= $this->render('_partial/plan_fact_slider') ?>
    </div>

    <div class="column mb-2 ml-3 pr-0 pl-0" style="width: 240px">
        <div class="ml-1">
            <?= Select2::widget([
                'id' => 'planfactearch-tab',
                'name' => 'planfactearch-tab',
                'value' => $activeTab,
                'data' => [
                    PlanFactSearch::TAB_BY_ACTIVITY => 'По видам деятельности',
                    PlanFactSearch::TAB_BY_PURSE => 'По типам кошельков',
                    PlanFactSearch::TAB_BY_PURSE_DETAILED => 'По кошелькам',
                    PlanFactSearch::TAB_BY_INDUSTRY => 'По направлениям',
                    PlanFactSearch::TAB_BY_SALE_POINT => 'По точкам продаж',
                ],
                'options' => [
                    'options' => [
                        PlanFactSearch::TAB_BY_ACTIVITY => [
                            'data-url' => Url::current([
                                'activeTab' => PlanFactSearch::TAB_BY_ACTIVITY,
                                'PlanFactSearch' => null
                            ])
                        ],
                        PlanFactSearch::TAB_BY_PURSE => [
                            'data-url' => Url::current([
                                'activeTab' => PlanFactSearch::TAB_BY_PURSE,
                                'PlanFactSearch' => null
                            ])
                        ],
                        PlanFactSearch::TAB_BY_PURSE_DETAILED => [
                            'data-url' => Url::current([
                                'activeTab' => PlanFactSearch::TAB_BY_PURSE_DETAILED,
                                'PlanFactSearch' => null
                            ])
                        ],
                        PlanFactSearch::TAB_BY_INDUSTRY => [
                            'data-url' => Url::current([
                                'activeTab' => PlanFactSearch::TAB_BY_INDUSTRY,
                                'PlanFactSearch' => null
                            ])
                        ],
                        PlanFactSearch::TAB_BY_SALE_POINT => [
                            'data-url' => Url::current([
                                'activeTab' => PlanFactSearch::TAB_BY_SALE_POINT,
                                'PlanFactSearch' => null
                            ])
                        ],
                    ],
                    'onchange' => new \yii\web\JsExpression('location.href = this.options[this.selectedIndex].dataset.url')
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </div>    
    
</div>

<div class="wrap wrap_padding_none mb-2" style="position:relative">

    <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
    <?= $this->render($viewTable, [
        'searchModel' => $searchModel,
        'activeTab' => $activeTab,
        'isExpanded' => $isExpanded,
        'userConfig' => $userConfig,

        'data' => $data,
        'growingData' => $growingData,
        'totalData' => $totalData,

        'plan' => $plan,
        'growingPlan' => $growingPlan,
        'totalPlan' => $totalPlan,

        'floorMap' => []
    ]); ?>
</div>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial_operations_block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы) в компанию и их возврат. <br/>
        Выплата дивидендов. Предоставление займов и депозитов.<br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_operation_activities_block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br/>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)<br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_investment_activities_block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br/>
        Затраты на новые проекты и поступление выручки от них. <br/>
        В данной строке отображается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом по данному<br/>
        виду деятельности
    </span>
    <span id="tooltip_own_funds_block" style="display: inline-block; text-align: center;">
        Перемещение денег между своими счетами. <br/>
        Тут отражаются операции со статьей <br/>
        "Перевод собственных средств". <br/>
        Итого по этому разделу всегда <br/>
        должно быть равно нулю
    </span>
    <span id="tooltip_net_cash_flow" style="display: inline-block; text-align: center;">
        "Чистый денежный поток" – это разница между всеми приходами<br/>
        и всеми расходами, за период указанный в столбце<br/>
        (день, месяц, квартал, год)
    </span>
    <span id="tooltip_block_bank" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Банк
    </span>
    <span id="tooltip_block_order" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Касса
    </span>
    <span id="tooltip_block_emoney" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу E-money
    </span>
    <span id="tooltip_block_acquiring" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Интернет-эквайринг
    </span>
    <span id="tooltip_block_card" style="display: inline-block; text-align: center;">
        В данной строке отражается Чистый Денежный Поток (ЧДП),<br/>
        т.е. разница между Приходом и Расходом, по разделу Карты
    </span>

    <span id="item-plan-tooltip_template" style="display: inline-block;">
        <span class="pull-left" style="min-width: 100px;">Факт</span>
        <span class="pull-right fact-amount"></span><br>

        <span class="pull-left" style="min-width: 100px;">План</span>
        <span class="pull-right plan-amount"></span><br>

        <span class="pull-left text-bold" style="min-width: 100px;">Разница</span>
        <span class="pull-right diff-amount text-bold"></span><br>

        <span class="pull-left text-bold deviation-text" style="min-width: 100px;">Отклонение</span>
        <span class="pull-right deviation text-bold"></span><br>
    </span>
    <span id="tooltip_deviation_block" class="text-center" style="display: inline-block;">
        Допустимое отклонение <br>
        Плана от Факта
    </span>
</div>
<?php
echo $this->render('@frontend/modules/analytics/views/multi-company/modal');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-deviation',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'side' => 'bottom'
    ],
]);
$this->registerJs(<<<JS
    $('.tooltip2-deviation-td').tooltipster({
        multiple: true,
        theme: ['tooltipster-kub'],
        contentCloning: true,
        contentAsHTML: true,
        trigger: 'hover',
        functionBefore: function (instance) {
            const template = $('#item-plan-tooltip_template');
            const deviationLine = template.find('.deviation, .deviation-text');
            const item = instance['_\$origin'];
            const factAmount = String(item.data('fact-amount'));
            const planAmount = String(item.data('plan-amount'));
            const diffAmount = String(item.data('diff-amount'));
            const deviation  = String(item.data('deviation'));

            template.find('.fact-amount').html(factAmount === '-' ? '-' : (number_format(factAmount, 2, ',', ' ') + ' ₽'));
            template.find('.plan-amount').html(planAmount === '-' ? '-' : (number_format(planAmount, 2, ',', ' ') + ' ₽'));
            template.find('.diff-amount').html(diffAmount === '-' ? '-' : (number_format(diffAmount, 2, ',', ' ') + ' ₽'));
            template.find('.deviation').html(deviation    === '-' ? '-' : (number_format(deviation, 2, ',', ' ')  + ' %'));
            deviationLine.css('color', '#212529');
            if (item.css('background-color') !== 'transparent') {
                if ('+' === deviation.substr(0,1))
                    deviationLine.css('color', '#27ae60');
                else if ('-' === deviation.substr(0,1) && deviation.length > 1)
                    deviationLine.css('color', '#e30611');
            }

            instance.content(template);
        }
    });

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });

JS
);

if ($isExpanded) {
    $this->registerJs("
    SimpleDoubleScroll.init({
        selectorTop: '#sds-top',
        selectorBottom: '#sds-bottom',
        refreshByClick: ['.table-collapse-btn']
    });
    ");
}
?>
