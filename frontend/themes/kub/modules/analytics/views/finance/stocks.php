<?php

/* @var $this yii\web\View
 * @var $searchModel frontend\modules\analytics\models\StocksSearch
 * @var $dataProvider yii\data\SqlDataProvider
 * @var $dataTotals array
 */

$this->title = 'Запасы';

$renderParams = [
    'title' => $this->title,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'dataTotals' => $dataTotals,
    'userConfig' => Yii::$app->user->identity->config,
    'floorMap' => Yii::$app->request->post('floorMap', []),
];
$pjaxParams = [
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'timeout' => 10000,
];

// First run
if (Yii::$app->request->isGet) {
    echo $this->render('_partial/stocks_header', $renderParams);
}

yii\widgets\Pjax::begin(array_merge($pjaxParams, ['id' => 'refresh_stocks_table']));
echo $this->render('_partial/stocks_control_panel', $renderParams);
echo $this->render('_partial/stocks_table', $renderParams);
yii\widgets\Pjax::end();
?>