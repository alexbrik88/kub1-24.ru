<?php

use frontend\modules\analytics\models\ExpensesSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $model ExpensesSearch */

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? date('m');
$customPurse = $customPurse ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(70,189,170,1)';
$color1_opacity = 'rgba(70,189,170,.5)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.5)';
//////////////////////////////////////////

///////////////// consts /////////////////
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 6;
$cropNamesLength = 12;
$croppedNameLength = 10;
//////////////////////////////////////////

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$mainData = $model->getChartStructureBySuppliers($dateFrom, $dateTo, $customPurse);
$categories = array_slice(array_column($mainData, 'name'), 0, $chartMaxRows);
$factData = array_slice(array_column($mainData, 'amountFact'), 0, $chartMaxRows);
$planData = array_slice(array_column($mainData, 'amountPlan'), 0, $chartMaxRows);
$clientsIds = array_slice(array_column($mainData, 'id'), 0, $chartMaxRows);
$expenseTotalAmount = array_sum($factData);

///////////////// calc /////////////////
$calculatedChartHeight = max(count($factData), count($planData)) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
//////////////////////////////////////////

if (Yii::$app->request->post('chart-expense-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'chartHeight' => $calculatedChartHeight,
        'expenseTotalAmount' => $expenseTotalAmount,
        'clientsIds' => $clientsIds,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $categories,
            ],
            'series' => [
                [
                    'data' => $planData,
                ],
                [
                    'data' => $factData,
                ]
            ],
        ],
    ]);

    exit;
}



?>
<style>
    #chart-expense-structure-2 { height: auto; }
</style>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect mb-2">
            ТОП ПОСТАВЩИКОВ
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-expense-structure-2',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var totalAmount = (ChartExpensesStructure2.expenseTotalAmount > 0) ? ChartExpensesStructure2.expenseTotalAmount : 9E9;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">Доля в расходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[1].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                                    '</table>';


                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('
                            function (boxWidth, boxHeight, point) {
                                var x = this.chart.containerWidth - boxWidth;
                                var y = point.plotY + 50;
                                console.log(x)
                                return {x: x, y: y};
                            }
                        '),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineColor' => 'transparent',
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'План',
                            'pointPadding' => 0,
                            'data' => $planData,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Факт',
                            'pointPadding' => 0,
                            'data' => $factData,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
                                    'click' => new JsExpression("
                                        function() {
                                            var clientId = ChartExpensesStructure2.clientsIds[this.index];
                                            console.log(clientId)
                                            if (!ChartExpensesMain.byClient || ChartExpensesMain.byClient != clientId) {
                                                ChartExpensesMain.byClient = clientId;
                                                ChartExpensesIncome.byClient = clientId;
                                                ChartExpensesStructure2.setPoint(this);
                                                // old selection
                                                ChartExpensesMain.byArticle = null;
                                                ChartExpensesIncome.byArticle = null;
                                                ChartExpensesStructure1.resetPoints();
                                            } else {
                                                ChartExpensesMain.byClient = null;
                                                ChartExpensesIncome.byClient = null;
                                                ChartExpensesStructure2.resetPoints();
                                            }

                                            ChartExpensesMain.redrawByClick();
                                            ChartExpensesIncome.redrawByClick();
                                        }
                                    ")
                                ]
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    window.ChartExpensesStructure2 = {
        chart: 'expense-structure-2',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        chartPoints: {},
        chartHeight: "<?= $calculatedChartHeight ?>",
        byPurse: null,
        expenseTotalAmount: "<?= $expenseTotalAmount ?>",
        clientsIds: <?= json_encode($clientsIds) ?>,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

        },
        redrawByClick: function() {

            return window.ChartExpensesStructure2._getData().done(function() {
                $('#chart-expense-structure-2').highcharts().update(ChartExpensesStructure2.chartPoints);
                $('#chart-expense-structure-2').highcharts().update({"chart": {"height": ChartExpensesStructure2.chartHeight + 'px'}});
                window.ChartExpensesStructure2._inProcess = false;
            });
        },
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': '<?= $color1 ?>',
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        resetPoints: function() {
            var chart = $('#chart-expense-structure-2').highcharts();
            chart.series[1].data.forEach(function(p) {
                p.update({
                    color: '<?= $color1 ?>'
                });
            });
            chart.series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color2 ?>'
                });
            });
        },
        _getData: function() {
            window.ChartExpensesStructure2._inProcess = true;
            return $.post('/analytics/finance-ajax/get-expense-charts-data', {
                    "chart-expense-ajax": true,
                    "purse": ChartExpensesStructure2.byPurse,
                    "chart": ChartExpensesStructure2.chart,
                    "year": ChartExpensesStructure2.year,
                    "month": ChartExpensesStructure2.month
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartExpensesStructure2.chartPoints = data.optionsChart;
                    ChartExpensesStructure2.chartHeight = data.chartHeight;
                    ChartExpensesStructure2.expenseTotalAmount = data.expenseTotalAmount;
                    ChartExpensesStructure2.clientsIds = data.clientsIds;
                }
            );
        },
    };

    window.ChartExpensesStructure2.init();
</script>