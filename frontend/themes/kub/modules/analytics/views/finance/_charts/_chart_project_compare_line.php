<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;

$id = $id ?? 'chart_21';
$height = $height ?? 250;

$htmlTooltip = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th class="text-left" colspan="3">{title}</th>
        </tr>
        <tr>
            <td class="text-grey">{itemName}: </td>
            <td></td>
            <td class="text-grey"><b>{itemData}</b></td>
        </tr>
    </table>
HTML;

$htmlTooltip = str_replace(["\r", "\n", "'"], "", $htmlTooltip);

$showLegend = $showLegend ?? false;
$dropdownItems = $dropdownItems ?? [];
$checkedDropdownItems = $checkedDropdownItems ?? [];
$checkedAllDropdownItems = $checkedAllDropdownItems ?? true;

$showLegend=1;

?>

<div class="graph_block">
    <div class="graph_info">


        <?= $this->render('_chart_industry_compare_arrow', ['chartId' => $id, 'offsetStep' => 1, 'class' => 'chart-offset-2', 'zIndex' => 2]) ?>

        <div style="min-height: <?= $height ?>px">
        <?= Highcharts::widget([
            'id' => $id,
            'scripts' => [
                'modules/exporting',
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' => $title,
                    'align' => 'left',
                    'floating' => false,
                    'style' => [
                        'font-size' => '15px',
                        'color' => '#9198a0',
                    ],
                    'x' => 0,
                    'y' => 5
                ],
                'credits' => [
                    'enabled' => false
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'spline',
                    //'marginLeft' => '55',
                    //'marginTop' => '50',
                    'marginBottom' => null,
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'reflow' => true,
                    'height' => $height,
                ],
                'legend' => $showLegend ? [
                    'useHTML' => true,
                    //'floating' => true,
                    'x' => mb_strlen($title) * 8 + 20,
                    'y' => -45,
                    'margin' => 15,
                    'layout' => 'horizontal',
                    'align' => 'left',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0',
                        'fontWeight' => '400',
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'labelFormatter' => new JsExpression('function () {
                        const name = (this.name.length > 20) ? (this.name.substr(0, 17) + "...") : this.name;
                        return \'<span class="name-name">\' + name + \'</span>\';
                    }'),
                    'itemDistance' => 15,
                    'symbolWidth' => 10,
                ] : ['enabled' => false],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                        
                            const index = this.series.data.indexOf( this.point );
                            const data = Highcharts.numberFormat(this.y, 2, ',', ' ');

                            return '{$htmlTooltip}'
                                .replace(\"{title}\", window.chart_21.labelsX[index])
                                .replace(\"{itemName}\", this.series.name)
                                .replace(\"{itemData}\", data);
                        }
                    ")
                ],
                'yAxis' => [
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
                'xAxis' => [
                    'min' => 0,
                    'categories' => $categories,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chart_21.currDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                            (window.chart_21.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                        'useHTML' => true,
                    ],
                ],
                'series' => $series,
                'plotOptions' => [
                    'scatter' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ]
                    ],
                    'series' => [
                        'marker' => [
                            'enabled' => true,
                            'symbol' => 'square',
                            'radius' => 5
                        ],
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        'events' => [
                            'legendItemClick' => new JsExpression("function () {
                                $('#{$id} .highcharts-legend-item').eq(this.index).trigger('click');
                                return true;
                            }")
                        ]
                    ]
                ],
            ],
        ]) ?>
        </div>

    </div>
</div>