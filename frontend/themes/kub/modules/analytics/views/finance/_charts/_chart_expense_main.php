<?php

use frontend\modules\analytics\models\ExpensesSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $model ExpensesSearch */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = "months";
$customPurse = $customPurse ?? null;
$customArticle = $customArticle ?? null;
$customClient = $customClient ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$mainData = $model->getPlanFactSeriesData($datePeriods, $customPurse, $customPeriod, $customArticle, $customClient);
$incomeFlowsFactPlan = &$mainData['incomeFlowsFact'];
$outcomeFlowsFactPlan = &$mainData['outcomeFlowsFact'];
$incomeFlowsPlan = &$mainData['incomeFlowsPlan'];
$outcomeFlowsPlan = &$mainData['outcomeFlowsPlan'];

if ($currDayPos >= 0 && $currDayPos < 9999) {
    // chart1
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $incomeFlowsFactPlan[$i] = $incomeFlowsPlan[$i];
        $outcomeFlowsFactPlan[$i] = $outcomeFlowsPlan[$i];
        $incomeFlowsPlan[$i] = null;
        $outcomeFlowsPlan[$i] = null;
    }

} elseif ($currDayPos == -1) {

}
elseif ($currDayPos === 9999) {
    for ($i = 0; $i < count($datePeriods); $i++) {
        $incomeFlowsFactPlan[$i] = $incomeFlowsPlan[$i];
        $outcomeFlowsFactPlan[$i] = $outcomeFlowsPlan[$i];
    }
    $incomeFlowsPlan = $outcomeFlowsPlan = [];
}

if (Yii::$app->request->post('chart-expense-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $outcomeFlowsFactPlan,
                ],
                [
                    'data' => $outcomeFlowsPlan,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-expense { height: 235px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="chart-expense-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-expense-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            ДИНАМИКА РАСХОДОВ
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'id' => 'chart-expense-purse-type',
                    'name' => 'purseType',
                    'data' => [
                        '' => 'Итого',
                        AbstractFinance::CASH_BANK_BLOCK => 'Банк',
                        AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
                        AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
                        AbstractFinance::CASH_ACQUIRING_BLOCK => 'Интернет-эквайринг',
                        AbstractFinance::CASH_CARD_BLOCK => 'Карты',
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '190px'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-expense',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartExpensesMain.redrawByLoad()')
                        ],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                if ((index > window.ChartExpensesMain.currDayPos && window.ChartExpensesMain.currDayPos != -1) || window.ChartExpensesMain.currDayPos == 9999) {
                                    return '<span class=\"title\">' + window.ChartExpensesMain.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' +
                                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '</table>';
                                }

                                return '<span class=\"title\">' + window.ChartExpensesMain.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                    '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartExpensesMain.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartExpensesMain.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartExpensesMain.wrapPointPos) {
                                            result += window.ChartExpensesMain.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Расход Факт',
                            'data' => $outcomeFlowsFactPlan,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Расход План',
                            'data' => $outcomeFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(50,50,50,1)',
                                'radius' => 10
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0,
                            'stickyTracking' => false,
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });

    // MOVE CHART
    window.ChartExpensesMain = {
        chart: 'main',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        byPurse: null,
        byArticle: null,
        byClient: null,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-expense-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartExpensesMain._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartExpensesMain.offset[ChartExpensesMain.period] -= <?= $MOVE_OFFSET ?>;
                    window.ChartExpensesIncome.offset[ChartExpensesIncome.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartExpensesMain.offset[ChartExpensesMain.period] += <?= $MOVE_OFFSET ?>;
                    window.ChartExpensesIncome.offset[ChartExpensesIncome.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartExpensesMain.redrawByClick();
                window.ChartExpensesIncome.redrawByClick();
            });

            $('#chart-expense-purse-type').on('change', function() {

                window.ChartExpensesMain.byPurse = $(this).val();
                window.ChartExpensesIncome.byPurse = $(this).val();
                window.ChartExpensesStructure1.byPurse = $(this).val();
                window.ChartExpensesStructure2.byPurse = $(this).val();

                window.ChartExpensesMain.redrawByClick();
                window.ChartExpensesIncome.redrawByClick();
                window.ChartExpensesStructure1.redrawByClick();
                window.ChartExpensesStructure2.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartExpensesMain._getData().done(function() {
                $('#chart-expense').highcharts().update(ChartExpensesMain.chartPoints);
                window.ChartExpensesMain._redrawPlanMonths();
                window.ChartExpensesMain._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartExpensesMain.isLoaded)
                return;

            var chartToLoad = window.setInterval(function () {
                var chart = $('#chart-expense').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.ChartExpensesMain._redrawPlanMonths();
                    window.clearInterval(chartToLoad);
                    window.ChartExpensesMain.isLoaded = true;
                }
            }, 100);
        },
        _redrawPlanMonths: function() {
            var custom_pattern = function (color) {
                return {
                    pattern: {
                        path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                        width: 10,
                        height: 10,
                        color: color
                    }
                }
            };
            var chart = $('#chart-expense').highcharts();
            if (typeof(chart) !== 'undefined') {
                //console.log(chart.series);
                var pos = ChartExpensesMain.currDayPos;
                if (pos == 9999)
                    pos = 0; // all plan
                if (pos == -1)
                    return; // all fact

                for (var i = (1+pos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color2 ?>");
                }
                chart.series[0].redraw();
            }
        },
        _getData: function() {
            window.ChartExpensesMain._inProcess = true;
            return $.post('/analytics/finance-ajax/get-expense-charts-data', {
                    "chart-expense-ajax": true,
                    "chart": ChartExpensesMain.chart,
                    "period": ChartExpensesMain.period,
                    "offset": window.ChartExpensesMain.offset[ChartExpensesMain.period],
                    "purse": ChartExpensesMain.byPurse,
                    "year": ChartExpensesMain.year,
                    "article": ChartExpensesMain.byArticle,
                    "client": ChartExpensesMain.byClient
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartExpensesMain.freeDays = data.freeDays;
                    ChartExpensesMain.currDayPos = data.currDayPos;
                    ChartExpensesMain.labelsX = data.labelsX;
                    ChartExpensesMain.wrapPointPos = data.wrapPointPos;
                    ChartExpensesMain.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-expense').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartExpensesMain.period == 'months') ? 0.705 : 0.625;

            if (window.ChartExpensesMain.wrapPointPos[x + 1]) {
                name = window.ChartExpensesMain.wrapPointPos[x + 1].prev;
                left = window.ChartExpensesMain.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartExpensesMain.wrapPointPos[x]) {
                name = window.ChartExpensesMain.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    window.ChartExpensesMain.init();
</script>