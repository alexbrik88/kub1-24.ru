<?php

use common\components\date\DateHelper;
use common\components\TextHelper;

$chartId = "chart_in_funnel";
$onDate = $onDate ?? date('Y-m-d');

// todo
$totalAmount = 0;
$totalCount = 0;
$conversion = 0;
$transactionTerm  = 0;

$statisticInfo = [
    [
        'typeName' => 'Сумма, ₽',
        'value' => $totalAmount,
        'cssClass' => '',
        'target' => ''
    ],
    [
        'typeName' => 'Кол-во, шт.',
        'value' => $totalCount,
        'cssClass' => '',
        'target' => ''
    ],
    [
        'typeName' => 'Конверсия, %',
        'value' => $conversion,
        'cssClass' => '',
        'target' => '',
        'symbol' => '%'
    ],
    [
        'typeName' => 'Срок сделки, дн.',
        'value' => $transactionTerm,
        'cssClass' => '',
        'target' => ''
    ],
];

?>
<div id="<?=($chartId)?>" class="wrap wrap-block" style="overflow: visible">
    <div class="d-flex align-items-center pb-2">
        <div class="ht-caption pb-0">
            <b>В ВОРОНКЕ</b> <span style="text-transform: none"> на </span> <?= DateHelper::format($onDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
        </div>
    </div>
    <table class="ht-table dashboard-cash">
        <?php foreach ($statisticInfo as $key => $item) : ?>
            <?php $isSubstring = in_array($item['cssClass'], ['substring']); ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <span class="bold"><?= $item['typeName']; ?></span>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <span class="bold">
                            <?php /* TextHelper::numberFormat($item['value'] ?? 0, 2); ?><?= $item['symbol'] ?? '' */ ?>
                        </span>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>