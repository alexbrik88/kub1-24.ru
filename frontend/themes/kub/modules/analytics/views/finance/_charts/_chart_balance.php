<?php

use frontend\modules\analytics\models\BalanceSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\AbstractFinance;

/* @var $model \frontend\modules\analytics\models\BalanceSearch */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? -4;
$customArticle = $customArticle ?? null;
$customPeriod = "months";
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(70,189,170,1)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 6;
$RIGHT_DATE_OFFSET = 5;
$MOVE_OFFSET = 1;
////////////////////////////////////////

if ($customOffset + $LEFT_DATE_OFFSET <= 0)
    $currDayPos = -1;
elseif ($customOffset - $RIGHT_DATE_OFFSET >= 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsDates($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $monthPeriods = [];
    $chartLabelsX = [];
    $chartYearMonthX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0]->format('Y');
    // x-axis texts
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date->format('Y-m-d'));
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $monthPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        $chartYearMonthX[] = $year.'-'.$month;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

// first run
if (!$customArticle) {
    if ((float)bcdiv($model->getAssets($date), 100, 2) > (float)bcdiv($model->getLiabilities($date), 100, 2))
        $customArticle = "assets";
    else
        $customArticle = "liabilities";
}

$balanceChartData = [];
foreach ($datePeriods as $i => $date) {

    if ($date->format('Ym') > date('Ym')) {
        $balanceChartData[] = null;
        continue;
    }

    $balanceChartData[] = ($customArticle == "assets") ?
        (float)bcdiv($model->getAssets($date), 100, 2) :
        (float)bcdiv($model->getLiabilities($date), 100, 2);

}

if (Yii::$app->request->post('chart-balance-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartLabelsX,
        'yearMonthX' => $chartYearMonthX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $monthPeriods,
            ],
            'series' => [
                [
                    'data' => $balanceChartData,
                ],
            ],
        ],
        'datePeriods' => $datePeriods
    ]);

    exit;
}

?>
<style>
    #chart-balance { height: 265px; }
</style>

    <div class="ht-caption noselect mb-2 mr-2">
        ДИНАМИКА БАЛАНСА
        <div class="wrap-select2-no-padding ml-1" style="float:right; margin-top:-13px;">
            <?= \kartik\select2\Select2::widget([
                'hideSearch' => true,
                'id' => 'chart-balance-article',
                'name' => 'chart-balance-article',
                'data' => [
                    'assets' => 'Активы',
                    'liabilities' => 'Пассивы'
                ],
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'value' => $customArticle,
                'pluginOptions' => [
                    'width' => '106px',
                    'containerCssClass' => 'select2-balance'
                ]
            ]); ?>
        </div>
    </div>
    <div class="clearfix"></div>

    <div style="width: 100%; position: relative">

        <div class="chart-balance-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:30px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-balance-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div style="min-height:265px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-balance',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'line',
                        'marginBottom' => '50',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        'animation' => false
                    ],
                    'legend' => false,
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + window.ChartBalance.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        '<tr>' + '<td class=\"gray-text\">' + window.ChartBalance.getArticleName() + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                    '</table>';
                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ],
                            'formatter' => new JsExpression("function() {

                                const formatCash = function(n) {
                                  if (Math.abs(n) < 1e3) return n;
                                  if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                                  if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                                };

                                return formatCash(this.value);
                            } "),
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $monthPeriods,
                            'labels' => [
                                'formatter' => new JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartBalance.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartBalance.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartBalance.wrapPointPos) {
                                            result += window.ChartBalance.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Баланс',
                            'data' => $balanceChartData,
                            'color' => $color1,
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
                                    'click' => new JsExpression("
                                        function() {
                                            var year = ChartBalance.yearMonthX[this.x].substring(0, 4);
                                            var month = ChartBalance.yearMonthX[this.x].substring(5, 7);
                                            //console.log(ChartBalance.yearMonthX[this.x], year, month)
                                            ChartBalanceStructure.year = year;
                                            ChartBalanceStructure.month = month;
                                            ChartBalanceStructure.redrawByClick();
                                            ChartBalance.setPoint(this);
                                            $('#chart-balance-structure-month').val('').trigger('change');
                                        }
                                    ")
                                ]
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ]
                    ],
                ],
            ]); ?>
        </div>

    </div>

<script>

    // MOVE CHART
    window.ChartBalance = {
        _inProcess: false,
        year: '<?= $model->year ?>',
        period: '<?= $customPeriod ?>',
        article: '<?= $customArticle ?>',
        offset: {
            days: 0,
            months: <?= (int)$customOffset ?>
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartLabelsX) ?>,
        yearMonthX: <?= json_encode($chartYearMonthX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('.chart-balance-days-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartBalance._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left')
                    window.ChartBalance.offset[ChartBalance.period] -= <?= $MOVE_OFFSET ?>;
                if ($(this).data('move') === 'right')
                    window.ChartBalance.offset[ChartBalance.period] += <?= $MOVE_OFFSET ?>;

                window.ChartBalance.redrawByClick();
            });

            $('#chart-balance-article').on('change', function() {

                // prevent double-click
                if (window.ChartBalance._inProcess) {
                    return false;
                }

                window.ChartBalance.article = $(this).val();
                window.ChartBalance.redrawByClick();
            });
        },
        redrawByClick: function() {

            return window.ChartBalance._getData().done(function() {
                $('#chart-balance').highcharts().update(ChartBalance.chartPoints);
                window.ChartBalance._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartBalance._inProcess = true;
            return $.post('/analytics/finance-ajax/get-balance-data', {
                    "chart-balance-ajax": true,
                    "article": ChartBalance.article,
                    "year": ChartBalance.year,
                    "period": ChartBalance.period,
                    "offset": window.ChartBalance.offset[ChartBalance.period],
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartBalance.freeDays = data.freeDays;
                    ChartBalance.currDayPos = data.currDayPos;
                    ChartBalance.labelsX = data.labelsX;
                    ChartBalance.yearMonthX = data.yearMonthX;
                    ChartBalance.wrapPointPos = data.wrapPointPos;
                    ChartBalance.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-balance').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartBalance.period == 'months') ? 0.725 : 0.625;

            if (window.ChartBalance.wrapPointPos[x + 1]) {
                name = window.ChartBalance.wrapPointPos[x + 1].prev;
                left = window.ChartBalance.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartBalance.wrapPointPos[x]) {
                name = window.ChartBalance.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
        getArticleName: function() {

            return this.article == 'assets' ? 'Активы' : 'Пассивы';
        },
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: 'red'
            });
        },
        resetPoints: function() {
            $('#chart-balance').highcharts().series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color1 ?>'
                });
            });
        }
    };

    window.ChartBalance.init();

    // fix bug
    $(document).ready(function() {
        setTimeout('$(".select2-selection__rendered").removeAttr("title")', 500);
        setTimeout('$(".select2-selection__rendered").removeAttr("title","")', 2500);
    });

</script>