<?php

use \frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use common\components\TextHelper;

/** @var $model DebtReportSearch2 */
/** @var $model2 DebtReportSearchAsBalance */

if ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY) {
    $totalDebts = $model->getTotalDebts();
} else {
    $totalDebts = $model2->getTotalPrepayments($type);
}
?>
<div class="ht-caption">
    <?= ($type == 2) ? 'НАМ ДОЛЖНЫ' : 'МЫ ДОЛЖНЫ' ?>
</div>
<table class="table-bleak ht-table">
    <tr>
        <td class="gray bold size15">Текущая задолженность</td>
        <td class="bold nowrap size15"><?= TextHelper::invoiceMoneyFormat($totalDebts['current_debt_sum'] ?? 0, 2); ?></td>
    </tr>
    <tr>
        <td class="red bold">Просрочка дней</td>
        <td></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">0-10 дней<i class="ht-empty left"></i></span></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::invoiceMoneyFormat($totalDebts['debt_0_10_sum'] ?? 0, 2); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">11-30 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::invoiceMoneyFormat($totalDebts['debt_11_30_sum'] ?? 0, 2); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">31-60 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::invoiceMoneyFormat($totalDebts['debt_31_60_sum'] ?? 0, 2); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">61-90 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::invoiceMoneyFormat($totalDebts['debt_61_90_sum'] ?? 0, 2); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">Больше 90 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::invoiceMoneyFormat($totalDebts['debt_more_90_sum'] ?? 0, 2); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr>
        <td class="red bold">Просроченая задолженность</td>
        <td class="nowrap red bold"><?= TextHelper::invoiceMoneyFormat($totalDebts['overdue_debt_sum'] ?? 0, 2); ?></td>
    </tr>
    <tr>
        <td class="gray bold size15">ИТОГО задолженность</td>
        <td class="bold nowrap size15">
            <?= TextHelper::invoiceMoneyFormat(($totalDebts['debt_all_sum'] ?? 0), 2) ?>
        </td>
    </tr>
</table>