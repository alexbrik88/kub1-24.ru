<?php

use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use frontend\themes\kub\components\Icon;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;

/** @var $type */
/** @var $reportType */
/** @var $searchBy */
/** @var $model DebtReportSearch2 */
/** @var $allDebtSum int */

$searchBy = $searchBy ?? DebtorHelper2::SEARCH_BY_INVOICES;

///////////////// dynamic vars ///////////
$customPeriod = "months";
$customOffset = $customOffset ?? 0;
$customDebtPeriod = $customDebtPeriod ?? DebtorHelper2::PERIOD_OVERDUE;
$customContractor = $customContractor ?? null;
$customEmployee = $customEmployee ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(255, 126, 87, 1)';
$color_opacity1 = 'rgba(255, 126, 87, .5)';
$color2 = 'rgba(227, 6, 17, 1)';
$color_opacity2 = 'rgba(227, 6, 17, .5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 12;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$fullPeriodFrom = $datePeriods[0]['from'];
$fullPeriodMonthsCount = count($datePeriods);
DebtorHelper2::$SEARCH_BY = $searchBy;
DebtorHelper2::$REPORT_TYPE = $reportType;
$rawData = DebtorHelper2::getTotalNet($fullPeriodFrom, $fullPeriodMonthsCount);
$dataDebtsOut = &$rawData['out'];
$dataDebtsIn = &$rawData['in'];
$dataNetTotal = &$rawData['diff'];

$fullPeriodFromPrev = date_create_from_format('Y-m-d', $fullPeriodFrom)->modify("-1 YEAR")->format('Y-m-d');
$rawDataPrev = DebtorHelper2::getTotalNet($fullPeriodFromPrev, $fullPeriodMonthsCount);
$dataNetTotalPrev = &$rawDataPrev['diff'];

if (Yii::$app->request->post('chart-debtor-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataDebtsOut,
                ],
                [
                    'data' => $dataDebtsIn,
                ]
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataNetTotalPrev,
                ],
                [
                    'data' => $dataNetTotal,
                ],
            ],
        ]
    ]);

    exit;
}

?>
<style>
    #chart-debtor { height: 235px; }
    #chart-debtor-2 { height: 235px; }
</style>

<div style="margin-right:20px;">
    <!-- CHART #1 -->
    <div style="width: 100%;position: relative; ">
        <div class="chart-debtor-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-debtor-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            НАМ ДОЛЖНЫ ИТОГО и МЫ ДОЛЖНЫ ИТОГО
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-debtor',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );

                                return '<span class=\"title\">' + window.ChartDebtorNet.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                    '</table>';
                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        [
                            'min' => 0,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ]
                        ],
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartDebtorNet.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartDebtorNet.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartDebtorNet.wrapPointPos) {
                                            result += window.ChartDebtorNet.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'yAxis' => 0,
                            'name' => 'Нам должны',
                            'data' => $dataDebtsOut,
                            'color' => $color2,
                            'borderColor' => $color_opacity2,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'yAxis' => 0,
                            'name' => 'Мы должны',
                            'data' => $dataDebtsIn,
                            'color' => $color1,
                            'borderColor' => $color_opacity1,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <!-- CHART #2 -->
    <div style="width: 100%;position: relative; ">
        <div class="chart-debtor-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-debtor-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            САЛЬДО (Нам должны минус Мы должны)
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-debtor-2',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + ChartDebtorNet.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' +
                                            ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[series_index].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[series_index].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        '</table>';

                            }
                        ")
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == ChartDebtorNet.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (ChartDebtorNet.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (ChartDebtorNet.wrapPointPos) {
                                            result += ChartDebtorNet.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Сальдо (предыдущий год)',
                            'data' => $dataNetTotalPrev,
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 1
                        ],
                        [
                            'name' => 'Сальдо',
                            'data' => $dataNetTotal,
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    // MOVE CHART
    window.ChartDebtorNet = {
        chart: 'net-total',
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        byContractor: "<?= $customContractor ?>",
        byEmployee: "<?= $customEmployee ?>",
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $(document).on('click', '.chart-debtor-arrow', function() {

                // prevent double-click
                if (window.ChartDebtorNet._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartDebtorNet.offset[ChartDebtorNet.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartDebtorNet.offset[ChartDebtorNet.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartDebtorNet.redrawByClick();
            });
        },
        redrawByClick: function() {

            return window.ChartDebtorNet._getData().done(function() {
                $('#chart-debtor').highcharts().update(ChartDebtorNet.chartPoints);
                $('#chart-debtor-2').highcharts().update(ChartDebtorNet.chartPoints2);
                window.ChartDebtorNet._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartDebtorNet._inProcess = true;
            return $.post('/analytics/finance-ajax/get-debtor-charts-data', {
                    "chart-debtor-ajax": true,
                    "type": <?= (int)$type ?>,
                    "report-type": <?= (int)$reportType ?>,
                    "search-by": <?= (int)$searchBy ?>,
                    "chart": ChartDebtorNet.chart,
                    "period": ChartDebtorNet.period,
                    "offset": window.ChartDebtorNet.offset[ChartDebtorNet.period],
                    "year": ChartDebtorNet.year,
                    "contractor": ChartDebtorNet.byContractor,
                    "employee": ChartDebtorNet.byEmployee
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartDebtorNet.freeDays = data.freeDays;
                    ChartDebtorNet.currDayPos = data.currDayPos;
                    ChartDebtorNet.labelsX = data.labelsX;
                    ChartDebtorNet.wrapPointPos = data.wrapPointPos;
                    ChartDebtorNet.chartPoints = data.optionsChart;
                    ChartDebtorNet.chartPoints2 = data.optionsChart2;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-debtor').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartDebtorNet.period == 'months') ? 0.705 : 0.625;

            if (window.ChartDebtorNet.wrapPointPos[x + 1]) {
                name = window.ChartDebtorNet.wrapPointPos[x + 1].prev;
                left = window.ChartDebtorNet.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartDebtorNet.wrapPointPos[x]) {
                name = window.ChartDebtorNet.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        getSerieName: function() {
            return $('#chart-debtor-period').find('option:selected').data('chart-title');
        }
    };

    //////////////////////////////
    window.ChartDebtorNet.init();
    //////////////////////////////

</script>