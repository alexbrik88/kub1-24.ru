<?php

use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\reference\models\ArticlesSearch;
use yii\helpers\Html;
use frontend\modules\analytics\models\PlanFactSearch;
use common\models\cash\CashFlowsBase;

/** @var $searchModel PlanFactSearch */
/** @var $articlesSearchModel ArticlesSearch */

$activityItems = [
    '' => 'Все',
    AbstractFinance::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
    AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
    AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность'
];
$purseItems = [
    '' => 'Все',
    AbstractFinance::CASH_BANK_BLOCK => 'Банк',
    AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
    AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
    AbstractFinance::CASH_ACQUIRING_BLOCK => 'Интернет-эквайринг',
    AbstractFinance::CASH_CARD_BLOCK => 'Карты',
];

$articlesSearchModel = new ArticlesSearch();
$articlesItems = $articlesSearchModel->search();
unset($articlesItems['block-'.$articlesSearchModel::BLOCK_FREE_CONTRACTORS]);


?>

    <!-- filter -->
    <div class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter_right cash-filter" data-check-items="dropdown" style="z-index: 1001">
        <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="button-txt">Фильтр</span>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </button>
        <div class="dropdown-menu" aria-labelledby="filter">
            <div class="popup-dropdown-in p-3">
                <div class="p-1">
                    <div class="row">
                        <div class="col-12" data-filter="activity">
                            <div class="form-group mb-3">
                                <div class="dropdown-drop" data-id="dropdown1">
                                    <div class="label">По видам деятельности</div>
                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown1">
                                        <span><?= reset($activityItems) ?></span>
                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                        </svg>
                                    </a>
                                    <div class="dropdown-drop-menu" data-id="dropdown1">
                                        <div class="dropdown-drop-in">
                                            <?= Html::radioList('activity', '', $activityItems, [
                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                    return Html::radio($name, $checked, [
                                                        'value' => $value,
                                                        'label' => $label,
                                                        'labelOptions' => [
                                                            'class' => 'radio-label p-2 no-border',
                                                        ],
                                                        'data-label' => htmlspecialchars($label),
                                                        'data-block_type' => $value
                                                    ]);
                                                },
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" data-filter="purse">
                            <div class="form-group mb-3">
                                <div class="dropdown-drop" data-id="dropdown2">
                                    <div class="label">По кошелькам</div>
                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown2">
                                        <span><?= reset($purseItems) ?></span>
                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                        </svg>
                                    </a>
                                    <div class="dropdown-drop-menu" data-id="dropdown2">
                                        <div class="dropdown-drop-in">
                                            <?= Html::radioList('purse', '', $purseItems, [
                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                    return Html::radio($name, $checked, [
                                                        'value' => $value,
                                                        'label' => $label,
                                                        'labelOptions' => [
                                                            'class' => 'radio-label p-2 no-border',
                                                        ],
                                                        'data-label' => htmlspecialchars($label)
                                                    ]);
                                                },
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" data-filter="article">
                            <div class="form-group mb-3">
                                <div class="dropdown-drop" data-id="dropdown3">
                                    <div class="label">По статьям</div>
                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown3">
                                        <span>Все</span>
                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                        </svg>
                                    </a>
                                    <div class="dropdown-drop-menu" data-id="dropdown3" style="max-height: 320px; overflow-y: auto;">
                                        <div class="dropdown-drop-in">
                                            <label class="checkbox-label mt-2 mb-1">
                                                <input class="checkbox-main" type="checkbox" name="article[]" value="" checked />
                                                Все
                                            </label>
                                            <ul class="articles-filter-list">
                                                <?php foreach ($articlesItems as $blockItem): ?>
                                                    <li class="main-block" data-block_type="<?= $blockItem['id'] ?>">
                                                        <?= $blockItem['name'] ?>
                                                    </li>
                                                    <?php foreach ($blockItem['items'] as $key => $flowItem): ?>
                                                        <?php $cashFlowType = ($flowItem['flowType'] == ArticlesSearch::TYPE_INCOME) ?
                                                                CashFlowsBase::FLOW_TYPE_INCOME : CashFlowsBase::FLOW_TYPE_EXPENSE ?>

                                                        <!-- MAIN INCOME/EXPENSE ROW -->
                                                        <li class="category flow-item-category" data-block_type="<?= $blockItem['id'] ?>">
                                                            <?= Html::button('<span class="table-collapse-icon" style="pointer-events: none">&nbsp;</span>', [
                                                                'class' => 'table-collapse-btn button-clr ',
                                                            ]) ?>
                                                            <?= $flowItem['name'] ?>
                                                        </li>
                                                        <?php if (isset($flowItem['items'])): ?>
                                                            <ul class="subCategory pl-0 hidden" data-block_type="<?= $blockItem['id'] ?>">
                                                                <?php foreach ($flowItem['items'] as $basicItem): ?>

                                                                    <?php if (!$basicItem['isVisibleFlowOfFundsItem'])
                                                                        continue; ?>

                                                                    <li class="item-<?= $basicItem['id'] ?>" style="padding-left: 25px;">
                                                                        <label class="checkbox-label">
                                                                            <?= Html::checkbox('article[]', false, [
                                                                                'class' => 'articles-filter-checkbox',
                                                                                'data-id' => $basicItem['id'],
                                                                                'data-flow_type' => $cashFlowType,
                                                                                'data-label' => htmlspecialchars($basicItem['name'])
                                                                            ]) ?>
                                                                            <?= $basicItem['name'] ?>
                                                                        </label>
                                                                    </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-6 pr-0">
                            <button class="cash_filters_apply button-regular button-hover-content-red button-width-medium button-clr">
                                Применить
                            </button>
                        </div>
                        <div class="col-6 pl-0 text-right">
                            <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                Сбросить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>

ChartPlanFactFilter = {
    Chart: null,
    selector: '.cash-filter',
    selectorButtons: '.cash-filter-buttons',
    init: function() {
        this.bindEvents();
        this.bindEventsArticles();
    },
    bindEvents: function() {
        const that = this;
        // sub-dropdown
        $(that.selector + ' .popup-dropdown-in').on('click', function(e) {
            e.stopPropagation();

            const el = that.normalizeElement(e.target);

            if (that.detectElement.isFilter(el)) {
                that.toggleDropdown(el);
                return false;
            }
            else if (that.detectElement.isCheckboxMain(el)) {
                that.toggleCheckboxes(el, true);
                that.setDropdownValue(el);
                return true;
            }
            else if (that.detectElement.isCheckbox(el)) {
                that.toggleCheckboxes(el, false);
                that.setDropdownValue(el);
                return true;
            }
            else if (that.detectElement.isRadio(el)) {
                that.setDropdownValue(el);
                that.closeDropdown(el);
                that.toggleRelatedInputs();
                return true;
            }
            else if (that.detectElement.isButton(el)) {
                return true;
            }
            else if (that.detectElement.isLi(el)) {
                return true;
            }
            else if (that.detectElement.isSelfBody(el)) {
                return false;
            }
            else {
                that.closeAllDropdowns();
                return false;
            }
        });
        // apply filter
        $(that.selector + ' .cash_filters_apply').on('click', function() {
            that.applyFilter();
        });
        // reset filter
        $(that.selector + ' .cash_filters_reset').on('click', function() {
            that.resetFilter();
            that.setSubmitColorActive();
        });
        // change submit color
        $(that.selector + ' input').on('change', function(e) {
            e.preventDefault();
            that.setSubmitColorActive();
        });
    },
    bindEventsArticles: function() {
        const that = this;
        $(that.selector + ' ul.articles-filter-list .category').on('click', function(e) {
            if ($(e.target).hasClass("category") || $(e.target).hasClass("table-collapse-icon") || $(e.target).hasClass("actions")
                || $(e.target).hasClass("itemName")) {
                let subCategory = $(this).next(".subCategory");

                $(this).toggleClass("open");
                $(this).find(".table-collapse-btn").toggleClass("active");
                subCategory.toggleClass("hidden");
                subCategory.find(".category.open").trigger('click');
            }
        });
        $(that.selector + ' ul.articles-filter-list .subCategory li').on('click', function(e) {
            if ($(e.target).is('li'))
                $(e.target).find('input').trigger('click');
        });
        $(document).on('click', that.selectorButtons + ' .close', function () {
            const block = $(this).closest('.top-filter-block').parent();

            if (block.attr('data-filter') === 'activity') {
                const inputs = $(that.selector).find('[data-filter=activity] input');
                inputs.filter(':checked').prop('checked', false).uniform();
                inputs.first().prop('checked', true).uniform();
                that.setDropdownValue(inputs.first());
                if (that.hasChart()) {
                    that.Chart.byActivity = null;
                    that.Chart.redrawByClick().done(function() {
                        that.refreshFiltersBlocks();
                        that.toggleRelatedInputs();
                    });
                }
            }

            if (block.attr('data-filter') === 'purse') {
                const inputs = $(that.selector).find('[data-filter=purse] input');
                inputs.filter(':checked').prop('checked', false).uniform();
                inputs.first().prop('checked', true).uniform();
                that.setDropdownValue(inputs.first());
                if (that.hasChart()) {
                    that.Chart.byPurse = null;
                    that.Chart.redrawByClick().done(function() {
                        that.refreshFiltersBlocks();
                    });
                }
            }

            if (block.attr('data-filter') === 'article') {
                const id = block.data('id');
                const flow_type = block.data('flow_type');
                const input = $(that.selector).find('[data-filter=article] input').filter('[data-flow_type='+flow_type+']').filter('[data-id='+id+']');
                input.trigger('click');

                if (that.hasChart()) {
                    let _idx = that.Chart.byArticle[flow_type].indexOf(id);
                    if (_idx !== -1) {
                        that.Chart.byArticle[flow_type].splice(_idx, 1);
                    }
                    that.Chart.redrawByClick().done(function() {
                        that.refreshFiltersBlocks();
                    });
                }
            }

        });
    },
    toggleCheckboxes: function(el, isMain) {
        const wrapper = $(el).closest('.dropdown-drop-in');
        const main = wrapper.find('input.checkbox-main');
        const plain = $(el).find('input:checkbox');
        const others = wrapper.find('input:checkbox').not('.checkbox-main');
        if (isMain) {
            if (main.prop('checked'))
                others.prop('checked', false).uniform('refresh');
            else
                others.first().prop('checked', true).uniform('refresh');
        } else {
            if (plain.prop('checked'))
                main.prop('checked', false).uniform('refresh');
            else if (!others.filter(':checked').length)
                main.prop('checked', true).uniform('refresh');
        }
    },
    normalizeElement: function(el) {
        if ($(el).attr('type') === 'checkbox')
            return $(el).closest('.checkbox-label')[0];
        if ($(el).attr('type') === 'radio')
            return $(el).closest('.radio-label')[0];

        return el;
    },
    detectElement: {
        isFilter: function(el) {
            return $(el).hasClass('a-filter');
        },
        isCheckboxMain: function(el) {
            return $(el).hasClass('checkbox-label') && $(el).find('input:checkbox').hasClass('checkbox-main');
        },
        isCheckbox: function(el) {
            return $(el).hasClass('checkbox-label');
        },
        isRadio: function(el) {
            return $(el).hasClass('radio-label');
        },
        isButton: function(el) {
            return $(el).is('button');
        },
        isLi: function(el) {
            return $(el).is('li');
        },
        isSelfBody: function(el) {
            return $(el).hasClass('dropdown-drop-in');
        }
    },
    toggleDropdown: function(el) {
        const that = this;
        const target = $(el).data('target');
        $(that.selector).find(' [data-id="'+target+'"]').toggleClass('visible show');
        $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').not('[data-id="'+target+'"]').removeClass('visible show');
    },
    closeDropdown: function(el) {
        $(el).parents('.dropdown-drop-menu, .dropdown-drop').removeClass('visible show');
    },
    closeAllDropdowns: function() {
        const that = this;
        $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').removeClass('visible show');
    },
    setDropdownValue: function(el) {
        const wrapper = $(el).closest('.dropdown-drop-in');
        const checkedInputs = $(wrapper).find('input:checked');
        const value = (checkedInputs.length > 1) ? (checkedInputs.length + ' шт.') : $(checkedInputs).closest('label').text();
        $(el).parents('.dropdown-drop').find('a > span').html(value);
    },
    applyFilter: function()
    {
        const that = this;
        let articlesIds = [[],[]];

        const activityInput = $(that.selector).find('[data-filter=activity]').find(':checked');
        const purseInput = $(that.selector).find('[data-filter=purse]').find(':checked');

        that.closeAllDropdowns();

        if (that.hasChart()) {
            $(that.selector).find('[data-filter=article]').find(':checked').each(function(i,input) {
                let id = $(input).data('id');
                let flow_type = $(input).data('flow_type');
                if (id) {
                    articlesIds[flow_type].push(id);
                }
            });

            if (!articlesIds[0].length && !articlesIds[1].length)
                articlesIds = null;

            that.Chart.byActivity = $(activityInput).val();
            that.Chart.byPurse = $(purseInput).val();
            that.Chart.byArticle = articlesIds;
            that.Chart.redrawByClick().done(function() {
                that.refreshFiltersBlocks();
                $(that.selector).dropdown('hide');
                that.setSubmitColorPassive();
            });
        }
    },
    resetFilter: function() {
        const that = this;
        that.closeAllDropdowns();

        $(that.selector).find('.dropdown-drop-in').each(function(i, el) {
            const radio = $(this).find('input:radio');
            const checkbox = $(this).find('input:checkbox');
            if (radio.length) {
                radio.filter(':checked').prop('checked', false).uniform();
                radio.first().prop('checked', true).uniform();
            }
            if (checkbox.length) {
                checkbox.prop('checked', false).uniform().first().prop('checked', true).uniform();
            }
            that.setDropdownValue(el);
        });

        that.toggleRelatedInputs();
    },
    setSubmitColorActive: function() {
        $(this.selector).find('.cash_filters_apply')
            .removeClass('button-hover-content-red')
            .addClass('button-regular_red');
    },
    setSubmitColorPassive: function() {
        $(this.selector).find('.cash_filters_apply')
            .addClass('button-hover-content-red')
            .removeClass('button-regular_red');
    },
    refreshFiltersBlocks: function() {
        const that = this;
        const activityInput = $(that.selector).find('[data-filter=activity]').find(':checked');
        const purseInput = $(that.selector).find('[data-filter=purse]').find(':checked');
        const articleButtonTemplate = $(that.selectorButtons).find('.template');

        $(that.selectorButtons).find('[data-filter=activity]').find('.name').html($(activityInput).attr('data-label'));
        $(that.selectorButtons).find('[data-filter=purse]').find('.name').html($(purseInput).attr('data-label'));
        if (that.Chart.byActivity) {
            $(that.selectorButtons).find('[data-filter=activity]').removeClass('hidden');
        } else {
            $(that.selectorButtons).find('[data-filter=activity]').addClass('hidden');
        }
        if (that.Chart.byPurse) {
            $(that.selectorButtons).find('[data-filter=purse]').removeClass('hidden');
        } else {
            $(that.selectorButtons).find('[data-filter=purse]').addClass('hidden');
        }
        if (that.Chart.byArticle) {
            $(that.selectorButtons).find('[data-filter=article]').filter(':not(.template)').remove();
            $(that.selector).find('[data-filter=article]').find(':checked').each(function(i,input) {
                let id = $(input).data('id');
                let flow_type = $(input).data('flow_type');
                let name = $(input).data('label');
                if (id) {
                    let block = $(articleButtonTemplate).clone();
                    $(block).data('id', id);
                    $(block).data('flow_type', flow_type);
                    $(block).find('.name').html(name);
                    $(block).appendTo(that.selectorButtons).removeClass('template hidden');
                }
            });

        } else {
            $(that.selectorButtons).find('[data-filter=article]').filter(':not(.template)').remove();
        }
    },
    hasChart: function() {
        if (this.Chart)
            return true;
        if (window.ChartPlanFact) {
            this.Chart = window.ChartPlanFact;
            return true;
        }

        alert('Chart not detected!');
        return false;
    },
    toggleRelatedInputs: function()
    {
        const that = this;
        const activityInput = $(that.selector).find('[data-filter=activity]').find(':checked');
        const block_type = $(activityInput).attr('data-block_type');
        if (block_type !== undefined) {
            if (block_type) {
                $('.articles-filter-list').children().each(function(i,v) {
                    if ($(v).attr('data-block_type') === block_type)
                        $(v).show();
                    else
                        $(v).hide();
                });
            } else {
                $('.articles-filter-list').children().show();
            }
        }
    },
};

//////////////////
ChartPlanFactFilter.init();
//////////////////

</script>