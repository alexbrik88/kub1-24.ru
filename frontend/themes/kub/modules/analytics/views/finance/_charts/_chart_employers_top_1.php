<?php
use common\models\EmployeeCompany;
use yii\web\JsExpression;

/** @var $type int */
/** @var array $rawSellsData */
/** @var $model \frontend\modules\analytics\models\IncomeSearch */

/////////////// consts //////////////////
$color = 'rgba(46,159,191,1)';
$maxRowsCount = 8;
/////////////////////////////////////////

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-top-employers').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = 54;
        var left = chart.plotWidth - data.dataLabel.width + offset;
        
        data.dataLabel.attr({
            x: left
        });
    });
}
JS;
$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX * 0.1 - 10,y:point.plotY + 15}; }
JS;

$companyIds = $model->multiCompanyIds;
usort($rawSellsData, function($a, $b) { return ($b['turnover'] ?? 0) <=> ($a['turnover'] ?? 0); });
$rawSellsData = array_slice($rawSellsData, 0, $maxRowsCount);
$categories = array_map(function($v) use ($companyIds) {
    $ec = EmployeeCompany::findOne(['employee_id' => $v, 'company_id' => $companyIds]);
    return mb_strtoupper($ec ? $ec->getShortFio() : $v);
}, array_column($rawSellsData, 'responsible_employee_id'));
$data = array_map(function($v) { return (float)$v / 100; }, array_column($rawSellsData, 'turnover'));
?>

<div class="ht-caption">
    ПРОДАЖИ по менеджерам
</div>
<div style="min-height: 125px">
    <?php
    echo \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-top-employers',
        'class' => 'finance-charts',
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill'
        ],
        'options' => [
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => false,
                'style' => [
                    'font-size' => '15px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'credits' => [
                'enabled' => false
            ],
            'legend' => [
                'enabled' => false
            ],
            'exporting' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => max(30 * count($categories), 30),
                'spacing' => [0,0,0,0],
                'marginRight' => 50,
                'events' => [
                    'load' => new \yii\web\JsExpression($jsLoadFunc),
                    'redraw' => new \yii\web\JsExpression($jsLoadFunc),
                ]
            ],
            'tooltip' => [
                'useHTML' => true,
                'shared' => false,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {

                        var index = this.series.data.indexOf( this.point );
                        var series_index = this.series.index;

                        return '<span class=\"title\">' + 'Сумма выручки' + '</span>' +
                            '<table class=\"indicators\">' +
                                '<tr>' + '<td class=\"gray-text\">' + this.x + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>' +
                            '</table>';
                    }
                ")
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'title' => '',
                'labels' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'labels' => [
                    //'useHTML' => true,
                    'align' => 'left',
                    'padding' => 100,
                    'style' => [
                        'width' => '100px'
                    ],
                    'reserveSpace' => true,
                    'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
                ],
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0
            ],
            'series' => [
                [
                    'name' => 'Сумма выручки',
                    'data' => $data,
                    'color' => $color,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ]
                    ],
                    'pointWidth' => 18,
                    'borderRadius' => 3
                ],
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' '); }")
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'cursor' => 'pointer',
                    'point' => [
                        'events' => []
                    ],
                ]
            ],
        ],
    ]);
    ?>
</div>
