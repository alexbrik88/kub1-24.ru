<?php
use common\models\contractor\ContractorHelper;
use yii\web\JsExpression;

/** @var $type int */
/** @var array $rawSellsData */
/** @var $model \frontend\modules\analytics\models\IncomeSearch */

/////////////// consts //////////////////
$color = '#336d9a';
$maxRowsCount = 8;
/////////////////////////////////////////

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-top-sells-3').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = 54;
        var left = chart.plotWidth - data.dataLabel.width + offset;
        
        data.dataLabel.attr({
            x: left
        });
    });
}
JS;
$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX * 0.1 - 10,y:point.plotY + 15}; }
JS;

usort($rawSellsData, function($a, $b) { return ($b['number_of_sales'] ?? 0) <=> ($a['number_of_sales'] ?? 0); });
$rawSellsData = array_slice($rawSellsData, 0, $maxRowsCount);
$categories = array_map(function($v) { return ContractorHelper::getShortNameById($v); }, array_column($rawSellsData, 'contractor_id'));
$data = array_map(function($v) { return (float)$v; }, array_column($rawSellsData, 'number_of_sales'));
?>

<div class="ht-caption">
    ТОП по заказам
</div>
<div style="min-height: 125px">
    <?php
    echo \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-top-sells-3',
        //'class' => 'finance-charts',
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill'
        ],
        'options' => [
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => false,
                'style' => [
                    'font-size' => '15px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'credits' => [
                'enabled' => false
            ],
            'legend' => [
                'enabled' => false
            ],
            'exporting' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => max(30 * count($categories), 30),
                'spacing' => [0,0,0,0],
                'marginRight' => 50,
                'events' => [
                    'load' => new \yii\web\JsExpression($jsLoadFunc),
                    'redraw' => new \yii\web\JsExpression($jsLoadFunc),
                ]
            ],
            'tooltip' => [
                'useHTML' => true,
                'shared' => false,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {

                        var index = this.series.data.indexOf( this.point );
                        var series_index = this.series.index;

                        return '<span class=\"title\">' + 'Кол-во документов' + '</span>' +
                            '<table class=\"indicators\">' +
                                '<tr>' + '<td class=\"gray-text\">' + this.x + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 0, ',', ' ') + ' шт.</td></tr>' +
                            '</table>';
                    }
                ")
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'title' => '',
                'labels' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'labels' => [
                    //'useHTML' => true,
                    'style' => [
                        'width' => '100px'
                    ],
                    'align' => 'left',
                    'padding' => 100,
                    'reserveSpace' => true,
                    'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
                ],
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0
            ],
            'series' => [
                [
                    'name' => 'Сумма маржи',
                    'data' => $data,
                    'color' => $color,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ]
                    ],
                    'pointWidth' => 18,
                    'borderRadius' => 3
                ],
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 0, ',', ' '); }")
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'cursor' => 'pointer',
                    'point' => [
                        'events' => []
                    ],
                ]
            ],
        ],
    ]);
    ?>
</div>
