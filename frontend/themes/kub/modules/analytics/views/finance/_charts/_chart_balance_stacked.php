<?php

use yii\web\JsExpression;
use common\components\helpers\Month;
use common\modules\analytics\AnalyticsManager;

/* @var $dateTo */
/* @var $model \frontend\modules\analytics\models\BalanceSearch */

$model = AnalyticsManager::getBalanceModel(substr($dateTo, 0, 4));

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customArticle = $customArticle ?? null;
$customPeriod = "months";
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(70,189,170,1)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 5;
$RIGHT_DATE_OFFSET = 4;
$MOVE_OFFSET = 1;
$currDayPos = null;
////////////////////////////////////////

if ($customOffset + $LEFT_DATE_OFFSET <= 0)
    $currDayPos = -1;
elseif ($customOffset - $RIGHT_DATE_OFFSET >= 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsDates($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset - 3);

    $monthPeriods = [];
    $chartLabelsX = [];
    $chartYearMonthX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0]->format('Y');
    // x-axis texts
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date->format('Y-m-d'));
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $monthPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        $chartYearMonthX[] = $year.'-'.$month;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}
// Assets
$series = [
    [
        'name' => 'Внеоборотные активы',
        'data' => [],
        'color' => 'rgb(77,142,178)',
    ],
    [
        'name' => 'Складские запасы',
        'data' => [],
        'color' => 'rgb(77,198,194)',
        'getStocks'
    ],
    [
        'name' => 'Дебиторская задолженность',
        'data' => [],
        'color' => 'rgb(212,127,129)',
    ],
    [
        'name' => 'Денежные средства',
        'data' => [],
        'color' => 'rgb(101,223,237)',
    ],
];
// Liabilities
$series2 = [
    [
        'name' => 'Кредиторская задолженность',
        'data' => [],
        'color' => 'rgb(246,204,115)',
    ],
    [
        'name' => 'Кредиты и займы',
        'data' => [],
        'color' => 'rgb(118,178,185)',
    ],
    [
        'name' => 'Собственный капитал',
        'data' => [],
        'color' => 'rgb(81,203,165)',
        'negativeColor' => 'rgba(227,6,17,1)',
        'negativeFillColor' => 'rgba(227,6,17,.75)',
    ],
];

foreach ($datePeriods as $i => $date) {

    if ($date->format('Ym') > date('Ym')) {
        $series[0]['data'][] = null;
        $series[1]['data'][] = null;
        $series[2]['data'][] = null;
        $series[3]['data'][] = null;
        $series2[0]['data'][] = null;
        $series2[1]['data'][] = null;
        $series2[2]['data'][] = null;
        continue;
    }

    // Get by year!
    $balanceYear = (int)$date->format('Y');
    $model = AnalyticsManager::getBalanceModel($balanceYear);
    // Assets
    $series[0]['data'][] = (float)bcdiv($model->getFixedAssets($date), 100, 2);
    $series[1]['data'][] = (float)bcdiv($model->getStocks($date), 100, 2);
    $series[2]['data'][] = (float)bcdiv($model->getReceivables($date), 100, 2);
    $series[3]['data'][] = (float)bcdiv($model->getCash($date), 100, 2);
    // Liabilities
    $series2[0]['data'][] = (float)bcdiv($model->getShorttermAccountsPayable($date), 100, 2);
    $series2[1]['data'][] = (float)bcdiv($model->getLongtermLoans($date) + $model->getShorttermBorrowings($date), 100, 2);
    $series2[2]['data'][] = (float)bcdiv($model->getCapital($date), 100, 2);
}

if (Yii::$app->request->post('chart-balance-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartLabelsX,
        'yearMonthX' => $chartYearMonthX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $monthPeriods,
            ],
            'series' => $series,
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $monthPeriods,
            ],
            'series' => $series2,
        ],
        'datePeriods' => $datePeriods
    ]);

    exit;
}

?>
<style>
    #chart-balance-stacked-assets,
    #chart-balance-stacked-liabilities { height: 250px; }
    .chart-balance-ico {
        width: 56px;
        height: 56px;
        border-radius: 28px;
        padding-top: 10px;
        padding-left: 10px;
        margin-left: auto;
        margin-right: auto;
    }
</style>

    <div class="ht-caption noselect mb-2 mr-2">
        <?= $title ?>
    </div>

    <?php if ($customArticle === "assets"): ?>
        <div class="row mb-3">
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series[3]['color'])?>">
                    <img src="/images/analytics/icon/balance_cash.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Денежные<br>средства</div>
            </div>
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series[2]['color'])?>">
                    <img src="/images/analytics/icon/balance_debtor.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Дебиторская<br>задолженность</div>
            </div>
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series[1]['color'])?>">
                    <img src="/images/analytics/icon/balance_stocks.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Складские<br>запасы</div>
            </div>
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series[0]['color'])?>">
                    <img src="/images/analytics/icon/balance_assets.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Внеоборотные<br>активы</div>
            </div>
        </div>
    <?php else: ?>
        <div class="row mb-3">
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series2[2]['color'])?>">
                    <img src="/images/analytics/icon/balance_capital.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Собственный<br>капитал</div>
            </div>
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series2[1]['color'])?>">
                    <img src="/images/analytics/icon/balance_credit.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Кредиты<br> и займы</div>
            </div>
            <div class="col-3 p-0">
                <div class="chart-balance-ico" style="background-color: <?=($series2[0]['color'])?>">
                    <img src="/images/analytics/icon/balance_loan.svg"/>
                </div>
                <div class="w-100 text-center font-14 mt-1">Кредиторская<br>задолженность</div>
            </div>
            <div class="col-3 p-0"></div>
        </div>
    <?php endif; ?>

    <div class="clearfix"></div>

    <div style="width: 100%; position: relative">

        <div class="chart-balance-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:30px; bottom:25px; z-index: 1">
            <?= \frontend\components\Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-balance-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= \frontend\components\Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div style="min-height:265px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => $id,
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'exporting' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'marginBottom' => '50',
                        'spacingBottom' => '0',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        'animation' => false
                    ],
                    'legend' => false,
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + window.ChartBalance.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        '<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>' +
                                    '</table>';
                            }
                        ")
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ],
                            'formatter' => new JsExpression("function() {

                                const formatCash = function(n) {
                                  if (Math.abs(n) < 1e3) return n;
                                  if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                                  if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                                };

                                return formatCash(this.value);
                            } "),
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $monthPeriods,
                            'labels' => [
                                'formatter' => new JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartBalance.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartBalance.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartBalance.wrapPointPos) {
                                            result += window.ChartBalance.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => ($customArticle === "assets") ? $series : $series2,
                    'plotOptions' => [
                        'series' => [
                            'cursor' => 'pointer',
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ],
                        'areaspline' => [
                            'stacking' => 'normal',
                            'lineWidth' => 1,
                            'marker' => [
                                'enabled' => false,
                            ]
                        ]
                    ],
                ],
            ]); ?>
        </div>

    </div>

<script>

    // MOVE CHART
    if (!window.ChartBalance)
    window.ChartBalance = {
        _inProcess: false,
        year: '<?= $model->year ?>',
        period: '<?= $customPeriod ?>',
        article: '<?= $customArticle ?>',
        offset: {
            days: 0,
            months: <?= (int)$customOffset ?>
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartLabelsX) ?>,
        yearMonthX: <?= json_encode($chartYearMonthX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {
            assets: {},
            liabilities: {},
        },
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('.chart-balance-days-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartBalance._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left')
                    window.ChartBalance.offset[ChartBalance.period] -= <?= $MOVE_OFFSET ?>;
                if ($(this).data('move') === 'right')
                    window.ChartBalance.offset[ChartBalance.period] += <?= $MOVE_OFFSET ?>;

                window.ChartBalance.redrawByClick();
            });

            $('#chart-balance-article').on('change', function() {

                // prevent double-click
                if (window.ChartBalance._inProcess) {
                    return false;
                }

                window.ChartBalance.article = $(this).val();
                window.ChartBalance.redrawByClick();
            });
        },
        redrawByClick: function() {
            const that = this;
            return window.ChartBalance._getData().done(function() {
                $('#chart-balance-stacked-assets').highcharts().update(ChartBalance.chartPoints.assets);
                $('#chart-balance-stacked-liabilities').highcharts().update(ChartBalance.chartPoints.liabilities);
                window.ChartBalance._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartBalance._inProcess = true;
            return $.post('/analytics/finance-ajax/get-balance-stacked-data', {
                    "chart-balance-ajax": true,
                    "year": ChartBalance.year,
                    "period": ChartBalance.period,
                    "offset": window.ChartBalance.offset[ChartBalance.period],
                    "dateTo": '<?=($dateTo)?>'
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartBalance.freeDays = data.freeDays;
                    ChartBalance.currDayPos = data.currDayPos;
                    ChartBalance.labelsX = data.labelsX;
                    ChartBalance.yearMonthX = data.yearMonthX;
                    ChartBalance.wrapPointPos = data.wrapPointPos;
                    ChartBalance.chartPoints.assets = data.optionsChart;
                    ChartBalance.chartPoints.liabilities = data.optionsChart2;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-balance-stacked-assets').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartBalance.period == 'months') ? 0.725 : 0.625;

            if (window.ChartBalance.wrapPointPos[x + 1]) {
                name = window.ChartBalance.wrapPointPos[x + 1].prev;
                left = window.ChartBalance.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartBalance.wrapPointPos[x]) {
                name = window.ChartBalance.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
        getArticleName: function() {
            return this.article === 'assets' ? 'Активы' : 'Пассивы';
        },
    };

    window.ChartBalance.init();

    // fix bug
    //$(document).ready(function() {
    //    setTimeout('$(".select2-selection__rendered").removeAttr("title")', 500);
    //    setTimeout('$(".select2-selection__rendered").removeAttr("title","")', 2500);
    //});

</script>