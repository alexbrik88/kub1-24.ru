<?php

use frontend\modules\analytics\models\StocksSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;

/** @var $model StocksSearch */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = "months";
$customGroup = $customGroup ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $datePeriodsPrevYear = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$fullPeriodFrom = (new \DateTime())->modify("-1 years")->modify("-{$LEFT_DATE_OFFSET} month")->modify("{$customOffset} month")->format('Y-m-01');
$fullPeriodMonthsCount = ((1 + $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET) + 12);
$model->prepareMainChartData($fullPeriodFrom, $fullPeriodMonthsCount, $customGroup);

$mainData = [
  'current' => $model->getMainChartData($datePeriods),
  'prev' => $model->getMainChartData($datePeriodsPrevYear),
];

if (Yii::$app->request->post('chart-stocks-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $mainData['prev']
                ],
                [
                    'data' => $mainData['current']
                ]
            ],
        ],
    ]);

    exit;
}

$companyGroups = ($model->byGroups) ? ([1 => 'Без группы'] + $model->getAllGroups()) : [];
?>
<style>
    #chart-stocks-main { height: 235px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="chart-stocks-main-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-stocks-main-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect mb-2">
            Динамика товарных остатков в закупочных ценах
            <?php if ($model::$HAS_GROUPS): ?>
            <div class="wrap-select2-no-padding ml-1" style="float:right; margin-top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'hideSearch' => (!$model->byGroups),
                    'id' => 'chart-stocks-group-filter',
                    'name' => 'group',
                    'data' => ['' => 'Итого'] + $companyGroups,
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'pluginOptions' => [
                        'width' => '200px'
                    ]
                ]); ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-stocks-main',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + ChartStocksMain.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' +
                                            ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[series_index].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[series_index].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        '</table>';

                            }
                        ")
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == ChartStocksMain.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (ChartStocksMain.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (ChartStocksMain.wrapPointPos) {
                                            result += ChartStocksMain.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток (предыдущий год)',
                            'data' => $mainData['prev'],
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 1
                        ],
                        [
                            'name' => 'Остаток',
                            'data' => $mainData['current'],
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    ChartStocksMain = {
        chart: 'main',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        byGroup: null,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-stocks-main-arrow').on('click', function() {

                // prevent double-click
                if (ChartStocksMain._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    ChartStocksMain.offset[ChartStocksMain.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    ChartStocksMain.offset[ChartStocksMain.period] += <?= $MOVE_OFFSET ?>;
                }

                ChartStocksMain.redrawByClick();
            });

            $('#chart-stocks-group-filter').on('change', function(e) {
                ChartStocksMain.byGroup = $(this).val();
                ChartStocksMain.redrawByClick();

                // same element
                if (ChartStocksMain.byGroup != ChartStocksStructure.byGroup) {
                    ChartStocksStructure.byGroup = null;
                    ChartStocksStructure.resetPoints();
                }
            });
        },
        redrawByClick: function() {

            <?php if ($model->byGroups): ?>
                // same element (groups only)
                if (ChartStocksMain.byGroup != $('#chart-stocks-group-filter').val()) {
                    $('#chart-stocks-group-filter').val(ChartStocksMain.byGroup).trigger('change');
                    return;
                }
            <?php endif; ?>

            return ChartStocksMain._getData().done(function() {
                $('#chart-stocks-main').highcharts().update(ChartStocksMain.chartPoints);
                ChartStocksMain._inProcess = false;
            });
        },
        _getData: function() {
            ChartStocksMain._inProcess = true;
            return $.post('/analytics/finance-ajax/get-stocks-chart-data', {
                    "chart-stocks-ajax": true,
                    "chart": ChartStocksMain.chart,
                    "year": ChartStocksMain.year,
                    "period": ChartStocksMain.period,
                    "offset": ChartStocksMain.offset[ChartStocksMain.period],
                    "group": ChartStocksMain.byGroup,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartStocksMain.freeDays = data.freeDays;
                    ChartStocksMain.currDayPos = data.currDayPos;
                    ChartStocksMain.labelsX = data.labelsX;
                    ChartStocksMain.wrapPointPos = data.wrapPointPos;
                    ChartStocksMain.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-stocks-main').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (ChartStocksMain.period === 'months') ? 0.705 : 0.625;

            if (ChartStocksMain.wrapPointPos[x + 1]) {
                name = ChartStocksMain.wrapPointPos[x + 1].prev;
                left = ChartStocksMain.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (ChartStocksMain.wrapPointPos[x]) {
                name = ChartStocksMain.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ChartStocksMain.init();
</script>