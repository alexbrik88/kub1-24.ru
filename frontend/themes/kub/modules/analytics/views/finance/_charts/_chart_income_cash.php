<?php

use frontend\modules\analytics\models\IncomeSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $model IncomeSearch */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = "months";
$customPurse = $customPurse ?? null;
$customArticle = $customArticle ?? null;
$customClient = $customClient ?? null;
$customProduct = $customProduct ?? null;
$customEmployee = $customEmployee ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2 = 'rgba(70,189,170,1)';
$color2_opacity = 'rgba(70,189,170,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$cashData = $model->getPlanFactSeriesData($datePeriods, "cash", $customPeriod, $customArticle, $customClient, $customProduct, $customEmployee);
$cashlessData = $model->getPlanFactSeriesData($datePeriods, "cashless", $customPeriod, $customArticle, $customClient, $customProduct, $customEmployee);
$cashFlowsFact = &$cashData['incomeFlowsFact'];
$cashlessFlowsFact = &$cashlessData['incomeFlowsFact'];
$cashFlowsPlan = &$cashData['incomeFlowsPlan'];
$cashlessFlowsPlan = &$cashlessData['incomeFlowsPlan'];

if ($currDayPos >= 0 && $currDayPos < 9999) {
    // chart1
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $cashFlowsFact[$i] = $cashFlowsPlan[$i];
        $cashlessFlowsFact[$i] = $cashlessFlowsPlan[$i];
        $cashFlowsPlan[$i] = null;
        $cashlessFlowsPlan[$i] = null;
    }

} elseif ($currDayPos == -1) {

}
elseif ($currDayPos === 9999) {
    for ($i = 0; $i < count($datePeriods); $i++) {
        $cashFlowsFact[$i] = $cashFlowsPlan[$i];
        $cashlessFlowsFact[$i] = $cashlessFlowsPlan[$i];
    }
    $cashFlowsPlan = $cashlessFlowsPlan = [];
}

if (Yii::$app->request->post('chart-income-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $cashFlowsFact,
                ],
                [
                    'data' => $cashlessFlowsFact,
                ],
                [
                    'data' => $cashFlowsPlan,
                ],
                [
                    'data' => $cashlessFlowsPlan,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-income-cash { height: 235px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="chart-income-cash-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-income-cash-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            БЕЗНАЛИЧНЫЕ И НАЛИЧНЫЕ
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-income-cash',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartIncomeCash.redrawByLoad()')
                        ],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                if ((index > window.ChartIncomeCash.currDayPos && window.ChartIncomeCash.currDayPos != -1) || window.ChartIncomeCash.currDayPos == 9999) {
                                    return '<span class=\"title\">' + window.ChartIncomeCash.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' +
                                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[3].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '</table>';
                                }

                                return '<span class=\"title\">' + window.ChartIncomeCash.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[2].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[3].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[3].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                    '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartIncomeCash.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartIncomeCash.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartIncomeCash.wrapPointPos) {
                                            result += window.ChartIncomeCash.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Безнал Факт',
                            'data' => $cashFlowsFact,
                            'color' => $color1,
                            'borderColor' => 'rgba(46,159,191,.3)',
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Нал Факт',
                            'data' => $cashlessFlowsFact,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Безнал План',
                            'data' => $cashFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(21,67,96,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => -0.225,
                            'stickyTracking' => false,
                            'visible' => true,
                            'showInLegend' => true
                        ],
                        [
                            'name' => 'Нал План',
                            'data' => $cashlessFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(50,50,50,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0.22,
                            'stickyTracking' => false,
                            'visible' => true,
                            'showInLegend' => true
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        //Highcharts.seriesTypes.Areaspline.prototype.drawLegendSymbol = function (legend) {
        //    this.options.marker.enabled = true;
        //    Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
        //    this.options.marker.enabled = false;
        //}
    });

    // MOVE CHART
    window.ChartIncomeCash = {
        chart: 'cash',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-income-cash-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartIncomeCash._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartIncomeCash.offset[ChartIncomeCash.period] -= <?= $MOVE_OFFSET ?>;
                    window.ChartIncomeMain.offset[ChartIncomeMain.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartIncomeCash.offset[ChartIncomeCash.period] += <?= $MOVE_OFFSET ?>;
                    window.ChartIncomeMain.offset[ChartIncomeMain.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartIncomeCash.redrawByClick();
                window.ChartIncomeMain.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartIncomeCash._getData().done(function() {
                $('#chart-income-cash').highcharts().update(ChartIncomeCash.chartPoints);
                window.ChartIncomeCash._redrawPlanMonths();
                window.ChartIncomeCash._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartIncomeCash.isLoaded)
                return;

            var chartToLoad = window.setInterval(function () {
                var chart = $('#chart-income-cash').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.ChartIncomeCash._redrawPlanMonths();
                    window.clearInterval(chartToLoad);
                    window.ChartIncomeCash.isLoaded = true;
                }
            }, 100);
        },
        _redrawPlanMonths: function() {
            var custom_pattern = function (color) {
                return {
                    pattern: {
                        path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                        width: 10,
                        height: 10,
                        color: color
                    }
                }
            };
            var chart = $('#chart-income-cash').highcharts();
            if (typeof(chart) !== 'undefined') {
                //console.log(chart.series);
                var pos = ChartIncomeCash.currDayPos;
                if (pos == 9999)
                    pos = 0; // all plan
                if (pos == -1)
                    return; // all fact

                for (var i = (1+pos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color1 ?>");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();
            }
        },
        _getData: function() {
            window.ChartIncomeCash._inProcess = true;
            return $.post('/analytics/finance-ajax/get-income-charts-data', {
                    "chart-income-ajax": true,
                    "chart": ChartIncomeCash.chart,
                    "period": ChartIncomeCash.period,
                    "offset": window.ChartIncomeCash.offset[ChartIncomeCash.period],
                    "year": ChartIncomeCash.year,
                    "purse":    ChartFilter.byPurse,
                    "article":  ChartFilter.byArticle,
                    "client":   ChartFilter.byClient,
                    "product":  ChartFilter.byProduct,
                    "employee": ChartFilter.byEmployee,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartIncomeCash.freeDays = data.freeDays;
                    ChartIncomeCash.currDayPos = data.currDayPos;
                    ChartIncomeCash.labelsX = data.labelsX;
                    ChartIncomeCash.wrapPointPos = data.wrapPointPos;
                    ChartIncomeCash.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-income-cash').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartIncomeCash.period == 'months') ? 0.705 : 0.625;

            if (window.ChartIncomeCash.wrapPointPos[x + 1]) {
                name = window.ChartIncomeCash.wrapPointPos[x + 1].prev;
                left = window.ChartIncomeCash.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartIncomeCash.wrapPointPos[x]) {
                name = window.ChartIncomeCash.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    window.ChartIncomeCash.init();
</script>