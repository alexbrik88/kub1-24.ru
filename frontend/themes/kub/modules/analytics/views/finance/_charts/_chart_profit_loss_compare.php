<?php

use common\components\helpers\Month;
use frontend\modules\analytics\models\ChartHelper;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

/** @var ProfitAndLossSearchModel $model */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = $customPeriod ?? "months";
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customAmountKey = $customAmountKey ?? 'amount'; // amount||amountTax
$customChartsGroup = $customChartsGroup ?? null;
/////////////////////////////////////////

////// consts dynamics chart ////////////
$LEFT_DATE_OFFSET = $LEFT_DATE_OFFSET ?? 8;
$RIGHT_DATE_OFFSET = $RIGHT_DATE_OFFSET ?? 7;
$MOVE_OFFSET = $MOVE_OFFSET ?? 1;
$CENTER_DATE = date('Y-m-d');
////// consts structures charts ////////
$chartMaxRows = 10;
///////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

$chart1 = [
    'totalRevenue' => [],
    'prevTotalRevenue' => []
];

if ($customPeriod == "months") {

    $datePeriods = ChartHelper::getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevDatePeriods = ChartHelper::getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);
    $chartPeriodsX = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $str_month =  $dateArr[1];
        $day   = (int)$dateArr[2];

        $prevDatePeriods[] = [
            'from' => date_create_from_format('Y-m-d', $date['from'])->modify("-1 year")->format('Y-m-d'),
            'to' => date_create_from_format('Y-m-d', $date['to'])->modify("-1 year")->format('Y-m-d'),
        ];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $chartPeriodsX[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    $datePeriods = ChartHelper::getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevDatePeriods = ChartHelper::getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $chartPeriodsX = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $monthPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        $prevDatePeriods[] = [
            'from' => date_create_from_format('Y-m-d', $date['from'])->modify("-14 day")->format('Y-m-d'),
            'to' => date_create_from_format('Y-m-d', $date['to'])->modify("-14 day")->format('Y-m-d'),
        ];

        if ($monthPrev != $month && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                'next' => Month::$monthFullRU[$month]
            ];
            // emulate align right
            switch ($month) {
                case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
            }
        }

        $monthPrev = $month;

        $chartPeriodsX[] = $day;
        $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
        $chartLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);

        if ($CENTER_DATE == $date['from'])
            $currDayPos = $i;
    }

}

// 1. REVENUE
foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $year  = (int)$dateArr[0];
    $str_month =  $dateArr[1];

    if ($year.$str_month > date('Ym')) {
        $chart1['totalRevenue'][] = null;
    } else {
        $chart1['totalRevenue'][] = round($model->getValue('totalRevenue', $year, $str_month, $customAmountKey) / 100, 2);
    }
}
foreach ($prevDatePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $year  = (int)$dateArr[0];
    $str_month =  $dateArr[1];

    if ($year.$str_month > date('Ym')) {
        $chart1['prevTotalRevenue'][] = null;
    } else {
        $chart1['prevTotalRevenue'][] = round($model->getValue('totalRevenue', $year, $str_month, $customAmountKey) / 100, 2);
    }
}

$pageFilter = $pageFilter ?? [
        'industry_id' => null,
        'sale_point_id' => null,
        'project_id' => null,
    ];
///////////////////////////////////////////////////////////////

if (Yii::$app->request->post('chart-profit-loss-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'amountKey' => $customAmountKey,
        'revenue' => [
            'currDayPos' => $currDayPos,
            'wrapPointPos' => $wrapPointPos,
            'freeDays' => $chartFreeDays,
            'labelsX' => $chartLabelsX,
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $chartPeriodsX,
                ],
                'series' => [
                    [
                        'data' => $chart1['totalRevenue'],
                    ],
                    [
                        'data' => $chart1['prevTotalRevenue'],
                    ]
                ],
            ],
        ],
    ]);

    exit;
}
?>

<?= $this->render('@frontend/modules/analytics/views/finance/_charts/_chart_profit_loss_compare_revenue', [
    'model' => $model,
    // consts
    'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
    'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
    // vars
    'customPeriod' => $customPeriod,
    'chartPeriodsX' => $chartPeriodsX,
    'totalRevenue' => $chart1['totalRevenue'],
    'prevTotalRevenue' => $chart1['prevTotalRevenue'],
]); ?>

<script>
    // MOVE CHART
    ChartsProfitAndLoss = {
        isLoaded: false,
        pageFilter: <?= json_encode($pageFilter) ?>,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        month: "<?= $customMonth ?>",
        byAmountKey: "<?= $customAmountKey ?>",
        Revenue: {
            offset: {
                days: <?= $customOffset ?>,
                months: <?= $customOffset ?>
            },
            currDayPos: <?= (int)$currDayPos; ?>,
            wrapPointPos: <?= json_encode($wrapPointPos) ?>,
            labelsX: <?= json_encode($chartLabelsX) ?>,
            freeDays: <?= json_encode($chartFreeDays) ?>,
            chartPoints: {},
            getWrapPointXLabel: function(x) { return ChartsProfitAndLoss._getWrapPointXLabel(x); }
        },
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('.chart-profit-loss-revenue-arrow').on('click', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                if ($(this).data('move') === 'left') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] += <?= $MOVE_OFFSET ?>;
                }
                ChartsProfitAndLoss.redrawByClick('revenue');
            });

            $('#chart-profit-loss-revenue-type').on('change', function() {
                ChartsProfitAndLoss.byAmountKey = $(this).val();
                ChartsProfitAndLoss.redrawByClick();
            });

            $('#chart-profit-loss-structure-month').on('change', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                ChartsProfitAndLoss.month = $(this).val();
                ChartsProfitAndLoss.redrawByClick('top');
            });

            $('.chart-profit-loss-days-tab').on('show.bs.tab', function (e) {
                ChartsProfitAndLoss.period = $(this).data('period');
                ChartsProfitAndLoss.redrawByClick();
            });
        },
        redrawByClick: function(chartsGroup) {

            return ChartsProfitAndLoss._getData(chartsGroup).done(function() {
                var revenue = $('#chart-profit-loss-revenue').highcharts();
                if (!chartsGroup || chartsGroup == 'revenue') {
                    revenue.update(ChartsProfitAndLoss.Revenue.chartPoints);
                }

                ChartsProfitAndLoss._inProcess = false;
            });
        },
        _getData: function(chartsGroup) {
            ChartsProfitAndLoss._inProcess = true;
            return $.post('/analytics/finance-ajax/get-profit-loss-charts-data', {
                    "chart-profit-loss-ajax": true,
                    "chartViewType": "profit_loss_compare",
                    //"chartsGroup": chartsGroup,
                    "year": ChartsProfitAndLoss.year,
                    "month": ChartsProfitAndLoss.month,
                    "offset": ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period],
                    "amountKey": ChartsProfitAndLoss.byAmountKey,
                    "pageFilter": this.pageFilter,
                    "period": ChartsProfitAndLoss.period,
                },
                function(data) {
                    data = JSON.parse(data);
                    const revenue = data.revenue;
                    if (!chartsGroup || chartsGroup == 'revenue') {
                        ChartsProfitAndLoss.Revenue.freeDays = revenue.freeDays;
                        ChartsProfitAndLoss.Revenue.currDayPos = revenue.currDayPos;
                        ChartsProfitAndLoss.Revenue.labelsX = revenue.labelsX;
                        ChartsProfitAndLoss.Revenue.wrapPointPos = revenue.wrapPointPos;
                        ChartsProfitAndLoss.Revenue.chartPoints = revenue.optionsChart;
                    }
                }
            );
        },
        _getWrapPointXLabel: function(x, chartId) {

            var chart = $('#chart-profit-loss-revenue').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (ChartsProfitAndLoss.period == 'months') ? 0.705 : 0.625;

            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].prev;
                left = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ///////////////////////////
    ChartsProfitAndLoss.init();
    ///////////////////////////

    $(document).ready(function() {

        window._chartProfitAndLossArrowZ = 1;
        $('#chart-profit-loss-revenue').mousemove(function(event) {
            let setZ = function() { $('.chart-profit-loss-revenue-arrow').css({"z-index": window._chartProfitAndLossArrowZ - 1}); };
            let y = event.pageY - $(this).offset().top;
            let x = event.pageX - $(this).offset().left;

            if (y > 186) {
                if (window._chartProfitAndLossArrowZ === 1) {
                    window._chartProfitAndLossArrowZ = 2;
                    setZ();
                }
            }
            else if (window._chartProfitAndLossArrowZ === 2) {
                window._chartProfitAndLossArrowZ = 1;
                setZ();
            }

        });

    });
</script>
