<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

// todo
$data = [
    ['name' => 'Дорого', 'y' => 23, 'color' => 'rgba(244,183,32,1)'],
    ['name' => 'Не то', 'y' => 12, 'color' => 'rgba(142,240,21,1)'],
    ['name' => 'Не знаю', 'y' => 12, 'color' => 'rgba(152,141,199,1)'],
    ['name' => 'Выбрали конкурентов', 'y' => 12, 'color' => 'rgba(67,196,227,1)'],
    ['name' => 'Не устраивает качество', 'y' => 36, 'color' => 'rgba(255,133,96,1)'],
    ['name' => 'Долго ждать', 'y' => 15, 'color' => 'rgba(142,152,161,1)'],
    ['name' => 'Нет потребности', 'y' => 36, 'color' => 'rgba(13,216,255,1)'],
];


$chartId = "chart_funnel_top_chart";
$onDate = $onDate ?? date('Y-m-d');
$series = [
    'name' => 'not_used',
    'colorByPoint' => true,
    'data' => $data
];
?>
<div class="wrap wrap-block" style="overflow: visible">
    <div class="d-flex align-items-center pb-2">
        <div class="ht-caption pb-0">
            <b>Причины отказов</b>
        </div>
    </div>
</div>

<?php echo Highcharts::widget(
    DC::getOptions([
        'id' => $chartId,
        'options' => [
            'chart' => [
                //'margin' => 0,
                //'spacingTop' => 0,
                'height' => $height ?? 250,
            ],
            'title' => [
                'text' => ''
            ],
            'series' => [$series],
            'legend' => [
                'floating' => true,
                'layout' => 'vertical',
                'verticalAlign' => 'top',
                'align' => 'left',
                'y' => 0,
                'x' => -20,
                'itemMarginBottom' => 5
            ],
            'tooltip' => [
                'formatter' => new JsExpression("
                    function(args) {
                    
                        const serieName = this.key;
                        
                        return ('<table class=\"indicators\">' +
                                '<tr>' +
                                    '<td class=\"title text-left\" colspan=\"2\">' + serieName + '</td>' +
                                '</tr>' +                                 
                                '<tr>' +
                                    '<td class=\"gray-text\">Кол-во:</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' </td>' +
                                '</tr>' +
                            '</table>');
                        }
        ")
            ],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => false,
                    'borderWidth' => 0,
                    'size' => '75%',
                    'innerSize' => '55%',
                    'dataLabels' => [
                        'enabled' => true,
                        'distance' => -15,
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 0, ',', ' '); }"),
                        'style' => [
                            'fontSize' => '12px',
                            'fontWeight' => '400',
                            'color' => '#FFF',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                            'textOutline' => false,
                        ]
                    ],
                    //'cursor' => 'pointer',
                    //'dataLabels' => false,
                    'center' => ['200', '20']
                ]
            ],
        ],

    ], DC::CHART_PIE)
);