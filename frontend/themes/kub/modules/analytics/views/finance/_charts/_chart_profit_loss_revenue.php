<?php

use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $chartPeriodsX array */
/** @var $totalRevenue array */
/** @var $netIncomeLoss array */

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2 = 'rgba(57,194,176,1)';
$color2_opacity = 'rgba(57,194,176,.95)';
$color2_negative = 'rgba(227,6,17,1)';
//////////////////////////////////////////

?>

<style>
    #chart-profit-loss-revenue { height: 235px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="chart-profit-loss-revenue-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 0">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-profit-loss-revenue-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 0">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            ДИНАМИКА ВЫРУЧКИ <span style="text-transform: none">и</span> ЧИСТОЙ ПРИБЫЛИ
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'id' => 'chart-profit-loss-revenue-type',
                    'name' => 'purseType',
                    'data' => [
                        'amount' => 'Все операции',
                        'amountTax' => 'Для бухгалтерии'
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '176px'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-profit-loss-revenue',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var profitability = (args.chart.series[0].data[index].y != 0) ? (args.chart.series[1].data[index].y / args.chart.series[0].data[index].y * 100) : null;

                                return '<span class=\"title\">' + window.ChartsProfitAndLoss.Revenue.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        (profitability ? ('<tr>' + '<td class=\"gray-text\">Рентабельность: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(profitability, 2, ',', ' ') + ' %</td></tr>') : '') +
                                    '</table>';
                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        //'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ],
                            'formatter' => new JsExpression("function() {

                                const formatCash = function(n) {
                                  if (Math.abs(n) < 1e3) return n;
                                  if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                                  if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                                };

                                return formatCash(this.value);
                            } "),
                        ],
                    ],
                    'xAxis' => [
                        [
                            'lineColor' => '#9198a0',
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $chartPeriodsX,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartsProfitAndLoss.Revenue.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartsProfitAndLoss.Revenue.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartsProfitAndLoss.Revenue.wrapPointPos) {
                                            result += window.ChartsProfitAndLoss.Revenue.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false,
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Прибыль',
                            'data' => $netIncomeLoss,
                            'color' => $color2,
                            'negativeColor' => $color2_negative,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ],
                            'legendIndex' => 1
                        ],
                        [
                            'name' => 'Выручка',
                            'type' => 'line',
                            'data' => $totalRevenue,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            'pointWidth' => 25,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            //'groupPadding' => 0.05,
                            //'pointPadding' => 0.1,
                            'borderRadius' => 3,
                            'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>