<?php
use common\models\Contractor;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use common\components\TextHelper;
use yii\web\JsExpression;

/** @var  $type int */
/** @var $model DebtReportSearch2 */
/** @var $model2 DebtReportSearchAsBalance */

/////////////// consts //////////////////
$color = $color ?? '#36c3b0';
$maxRowsCount = 8;
/////////////////////////////////////////

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-debt-2').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = 54;
        var left = chart.plotWidth - data.dataLabel.width + offset;

        data.dataLabel.attr({
            x: left
        });
    });
}
JS;
$jsPositionerFunc = 'function (boxWidth, boxHeight, point) { return {x:point.plotX * 0.1 - 10,y:point.plotY + 15}; }';
$htmlHeader = '
    <table class="table-bleak ht-in-table">
        <tr>
            <th colspan="3" class="ht-title text-left black" style="max-width:250px; padding-bottom:2px;">{serie_name}</th>
        </tr>
        <tr>
            <th class="red">'.(($type == 2 && $reportType == 1 || $type == 1 && $reportType == 2) ? 'ТОП 5 должников' : 'Просрочка на').'</th>
            <th colspan="2"></th>
        </tr>';
$htmlDataBlue = '
        <tr>
            <td><div class="ht-title">{period}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart" style="background-color:'.$color.'; width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>';
$htmlFooter = '
    </table>';

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlDataBlue = str_replace(["\r", "\n", "'"], "", $htmlDataBlue);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

$categories = $debt = $jsTopData = $employersIds = [];
if ($type == 2 && $reportType == 1 || $type == 1 && $reportType == 2) {

    $chartTitle = 'ДОЛЖНИКИ ПО СОТРУДНИКАМ';

    $topDataArr = ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY) ?
        $model->getTopByEmployersArr($maxRowsCount) :
        $model2->getTopPrepaymentByEmployersArr($type, $maxRowsCount);

    if ($topDataArr) {

        $topData = &$topDataArr['chart'];
        $jsTopData = &$topDataArr['tooltip'];

        foreach ($topData as $data) {
            $employeeCompany = \common\models\EmployeeCompany::findOne([
                'company_id' => Yii::$app->user->identity->currentEmployeeCompany->company->id,
                'employee_id' => $data['responsible_employee_id']
            ]);

            $categories[] = mb_strtoupper(($employeeCompany) ? $employeeCompany->getShortFio() : '---');
            $debt[] = round($data['sum'] / 100, 2);
            $employersIds[] = $data['responsible_employee_id'];
        }

    } else {
        $categories[] = 'Нет данных';
        $debt[] = 0;
    }

} else {

    $chartTitle = 'СТРУКТУРА ПО СТАТЬЯМ';

    $topData = ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY) ?
        $model->getTopByExpenditureItem($maxRowsCount) :
        $model2->getTopPrepaymentByExpenditureItem($type, $maxRowsCount);

    if ($topData) {

        foreach ($topData as $data) {
            $expenditureItem = \common\models\document\InvoiceExpenditureItem::findOne($data['invoice_expenditure_item_id']);

            $categories[] = mb_strtoupper(($expenditureItem) ? $expenditureItem->name : '---');
            $debt[] = round($data['debt_all_sum'] / 100, 2);
        }

        // tooltip credit
        $jsTopData = [];
        foreach ($topData as $data) {

            $max_sum = max(1, $data['debt_0_10_sum'], $data['debt_11_30_sum'], $data['debt_31_60_sum'], $data['debt_61_90_sum'], $data['debt_more_90_sum']);

            $jsTopData[] = [
                ['period' => '0-10 дней', 'percent' => $max_sum ? round(100 * $data['debt_0_10_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_0_10_sum'], 2)],
                ['period' => '11-30 дней', 'percent' => $max_sum ? round(100 * $data['debt_11_30_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_11_30_sum'], 2)],
                ['period' => '31-60 дней', 'percent' => $max_sum ? round(100 * $data['debt_31_60_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_31_60_sum'], 2)],
                ['period' => '61-90 дней', 'percent' => $max_sum ? round(100 * $data['debt_61_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_61_90_sum'], 2)],
                ['period' => 'Больше 90 дней', 'percent' => $max_sum ? round(100 * $data['debt_more_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_more_90_sum'], 2)],
            ];
        }

    } else {
        $categories[] = 'Нет данных';
        $debt[] = 0;
    }
}

$jsTopData = json_encode($jsTopData);

$jsFormatterFunc = <<<JS
function (a) {

      if(1 == this.point.series.index)
          return false;

      var idx = this.point.index;
      var tooltipHtml;

      var arr = $jsTopData;

      tooltipHtml = '$htmlHeader'.replace("{serie_name}", this.point.category);
      if (arr[idx] != undefined) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '$htmlDataBlue'
               .replace("{period}", data.period)
               .replace("{percent}", data.percent)
               .replace("{sum}", data.sum);
          });
      } else {
          tooltipHtml += '<tr><td colspan="3">Нет данных</td></tr>';
      }

      tooltipHtml += '$htmlFooter';

      return tooltipHtml.replace();
    }
JS;
?>

<?php
 ?>
<div class="ht-caption">
    <?= $chartTitle ?>
</div>
<div style="height: 150px!important;">
    <?php
    echo \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-debt-2',
        'class' => 'finance-charts',
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill'
        ],
        'options' => [
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => false,
                'style' => [
                    'font-size' => '15px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'credits' => [
                'enabled' => false
            ],
            'legend' => [
                'enabled' => false
            ],
            'exporting' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => max(30 * count($categories), 30),
                'spacing' => [0,0,0,0],
                'marginRight' => 50,
                'events' => [
                    'load' => new \yii\web\JsExpression($jsLoadFunc),
                    'redraw' => new \yii\web\JsExpression($jsLoadFunc),
                ]
            ],
            'tooltip' => [
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'positioner' => new \yii\web\JsExpression($jsPositionerFunc),
                'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
                'useHTML' => true,
                'shape' => 'rect',
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'title' => '',
                'labels' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'labels' => [
                    //'useHTML' => true,
                    'align' => 'left',
                    'padding' => 100,
                    'style' => [
                        'width' => '100px'
                    ],
                    'reserveSpace' => true,
                    'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
                ],
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0
            ],
            'series' => [
                [
                    'name' => ($type == 2) ? 'Сумма долга' : 'Сумма кредита',
                    'data' => $debt,
                    'color' => $color,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ]
                    ],
                    'pointWidth' => 18,
                    'borderRadius' => 3
                ],
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' '); }")
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'cursor' => 'pointer',
                    'point' => [
                        'events' => [
                            'click' => new JsExpression("
                                function() {
                                    ".(($type == 2 && $reportType == 1 || $type == 1 && $reportType == 2) ? "
                                    var employeeId = ChartDebtTop2.employersIds[this.index];
                                    if (!ChartDebtorMain.byEmployee || ChartDebtorMain.byEmployee != employeeId) {
                                        ChartDebtorMain.byEmployee = employeeId;
                                        ChartDebtorMain2.byEmployee = employeeId;
                                        ChartDebtTop2.setPoint(this);
                                        // old selection
                                        if ('ChartDebtTop1' in window) {
                                            ChartDebtorMain.byContractor = null;
                                            ChartDebtTop1.resetPoints();
                                        }
                                    } else {
                                        ChartDebtorMain.byEmployee = null;
                                        ChartDebtorMain2.byEmployee = null;
                                        ChartDebtTop2.resetPoints();
                                    }

                                    ChartDebtorMain.redrawByClick();
                                    ChartDebtorMain2.redrawByClick();
                                    " : "")."
                                }
                            ")
                        ]
                    ],
                ]
            ],
        ],
    ]);
    ?>
</div>

<?php if ($type == 2 && $reportType == 1 || $type == 1 && $reportType == 2): ?>
<script>
    window.ChartDebtTop2 = {
        chart: 'chart-debt-2',
        employersIds: <?= json_encode($employersIds) ?>,
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': '<?= $color ?>',
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        resetPoints: function() {
            var chart = $('#chart-debt-2').highcharts();
            chart.series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color ?>'
                });
            });
        },
    };
</script>
<?php endif; ?>