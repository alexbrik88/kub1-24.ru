<?php

use frontend\modules\analytics\models\StocksSearch;
use yii\web\JsExpression;
use frontend\modules\analytics\models\AbstractFinance;
use yii\helpers\ArrayHelper;

/** @var $model StocksSearch */

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customGroup = $customGroup ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////

///////////////// consts /////////////////
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 6;
$cropNamesLength = 12;
$croppedNameLength = 10;
//////////////////////////////////////////

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$mainData = $model->getStructureChartData($dateFrom, $dateTo);
$categories = array_slice(array_column($mainData, 'title'), 0, $chartMaxRows);
$data = array_slice(array_column($mainData, 'amount'), 0, $chartMaxRows);
$groupIds = array_slice(array_column($mainData, 'id'), 0, $chartMaxRows);
//$totalAmount = array_sum(array_column($mainData, 'amount'));
$totalAmount = 0;
foreach ($mainData as $d)
    if ($d['amount'] > 0)
        $totalAmount += $d['amount'];

if (empty($data)) {
    $categories = [['Нет данных']];
    $data = [[null]];
}

///////////////// calc /////////////////
$calculatedChartHeight = count($data) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
//////////////////////////////////////////

if (Yii::$app->request->post('chart-stocks-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'chartHeight' => $calculatedChartHeight,
        'totalAmount' => $totalAmount,
        'groupIds' => $groupIds,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $categories,
            ],
            'series' => [
                [
                    'data' => $data,
                ]
            ],
        ],
    ]);

    exit;
}

$chartTitle = $model->byGroups ? 'СТРУКТУРА ПО ГРУППАМ' : 'СТРУКТУРА ПО ТОВАРАМ';
$tooltipSubtitle = $model->byGroups ? 'Доля группы' : 'Доля товара';

if (Yii::$app->request->post('chart-stocks-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'chartHeight' => $calculatedChartHeight,
        'totalAmount' => $totalAmount,
        'groupIds' => $groupIds,
        'chartTitle' => $chartTitle,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $categories,
            ],
            'series' => [
                [
                    'data' => $data,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-stocks-structure { height: auto; }
    #chart-stocks-structure .highcharts-container { z-index: 2!important; }
</style>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            <?= $chartTitle ?>
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'hideSearch' => true,
                    'id' => 'chart-stocks-structure-month',
                    'name' => 'chart-stocks-structure-month',
                    'data' => array_filter(array_reverse(AbstractFinance::$month, true),
                        function($k) use ($model) {
                            return ($model->year < date('Y') || (int)$k <= date('n'));
                        }, ARRAY_FILTER_USE_KEY),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'value' => $customMonth,
                    'pluginOptions' => [
                        'width' => '106px',
                        'containerCssClass' => 'select2-balance-structure'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-stocks-structure',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        'marginTop' => '32',
                        'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var totalAmount = (ChartStocksStructure.totalAmount > 0) ? ChartStocksStructure.totalAmount : 9E9;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">{$tooltipSubtitle}: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[0].data[index].y / totalAmount, 2, ',', ' ') + ' %</td></tr>') +
                                    '</table>';


                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('
                            function (boxWidth, boxHeight, point) {
                                var x = this.chart.containerWidth - boxWidth;
                                var y = point.plotY + 50;
                                //console.log(x)
                                return {x: x, y: y};
                            }
                        '),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'showInLegend' => false,
                            'name' => 'Остаток',
                            'pointPadding' => 0,
                            'data' => $data,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
                                    'click' => new JsExpression("
                                        function() {
                                            var groupId = ChartStocksStructure.groupIds[this.index];
                                            if (ChartStocksStructure.byGroup != groupId) {
                                                ChartStocksMain.byGroup = ChartStocksStructure.byGroup = groupId;
                                                ChartStocksStructure.setPoint(this);
                                            } else {
                                                ChartStocksMain.byGroup = ChartStocksStructure.byGroup = null;
                                                ChartStocksStructure.resetPoints(); 
                                            }
                                            
                                            ChartStocksMain.redrawByClick();
                                        }
                                    ")
                                ]
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    ChartStocksStructure = {
        chart: 'structure',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        chartPoints: {},
        chartHeight: "<?= $calculatedChartHeight ?>",
        totalAmount: "<?= $totalAmount ?>",
        byGroup: null,
        groupIds: <?= json_encode($groupIds) ?>,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('#chart-stocks-structure-month').on('change', function() {

                // prevent double-click
                if (ChartStocksStructure._inProcess) {
                    return false;
                }

                if (window.ChartStocksMain.byGroup) {
                    window.ChartStocksMain.byGroup = null;
                    ChartStocksStructure.resetPoints();
                    window.ChartStocksMain.redrawByClick();
                }

                ChartStocksStructure.year = $('#stockssearch-year').val();
                ChartStocksStructure.month = $(this).val();
                ChartStocksStructure.redrawByClick();

                if (window.ChartStocksSquares) {
                    ChartStocksSquares.month = $(this).val();
                    ChartStocksSquares.redrawByClick();
                }
            });

        },
        redrawByClick: function() {
            return ChartStocksStructure._getData().done(function() {
                const $chart = $('#chart-stocks-structure').highcharts();
                $chart.update(ChartStocksStructure.chartPoints);
                $chart.update({"chart": {"height": ChartStocksStructure.chartHeight + 'px'}});
                ChartStocksStructure._inProcess = false;
            });
        },
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': '<?= $color1 ?>',
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        resetPoints: function() {
            var chart = $('#chart-stocks-structure').highcharts();
            chart.series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color1 ?>'
                });
            });
        },
        _getData: function() {
            ChartStocksStructure._inProcess = true;
            return $.post('/analytics/finance-ajax/get-stocks-chart-data', {
                    "chart-stocks-ajax": true,
                    "chart": ChartStocksStructure.chart,
                    "year": ChartStocksStructure.year,
                    "month": ChartStocksStructure.month,
                    "group": ChartStocksStructure.byGroup,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartStocksStructure.chartPoints = data.optionsChart;
                    ChartStocksStructure.chartHeight = data.chartHeight;
                    ChartStocksStructure.totalAmount = data.totalAmount;
                    //ChartStocksStructure.groupIds = data.groupIds;
                }
            );
        },
    };

    ChartStocksStructure.init();
</script>