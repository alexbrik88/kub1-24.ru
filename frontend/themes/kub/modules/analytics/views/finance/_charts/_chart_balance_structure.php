<?php

use frontend\modules\analytics\models\AbstractFinance;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;

/* @var $model \frontend\modules\analytics\models\BalanceSearch */

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
/////////////////////////////////////////

///////////////// consts /////////////////
$minHideLabelPercent = 7;
$color1 = 'rgba(245,183,46,1)';
$color2 = 'rgba(52,109,154,1)';
$color3 = 'rgba(56,195,176,1)';
$color4 = 'rgba(255,81,60,1)';
$colorGray = 'rgba(81,81,81,1)';
//////////////////////////////////////////

$currDate = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$currDate = $currDate->modify('last day of this month');

$ACTIVE = [
    1 => (float)bcdiv($model->getReceivables($currDate), 100, 2) ?: null,
    2 => (float)bcdiv($model->getStocks($currDate), 100, 2) ?: null,
    3 => (float)bcdiv($model->getCash($currDate), 100, 2) ?: null,
    4 => (float)bcdiv($model->getIncompleteProjects($currDate), 100, 2) ?: null,
    5 => (float)bcdiv($model->getFixedAssets($currDate), 100, 2) ?: null
];

$PASSIVE = [
    1 => (float)bcdiv($model->getShorttermAccountsPayable($currDate), 100, 2) ?: null,
    2 => (float)bcdiv($model->getShorttermBorrowings($currDate), 100, 2) ?: null,
    3 => (float)bcdiv($model->getLongtermDuties($currDate), 100, 2) ?: null,
];

$SELF = (float)bcdiv($model->getCapital($currDate), 100, 2) ?: null;

$sumActive = array_sum($ACTIVE);
$sumPassive = array_sum($PASSIVE) + ($SELF > 0 ? $SELF : 0);
$sumSelf = $SELF ?: 0;

$series = [
    // Активы
    ['name' => 'Дебиторская задолженность',  'color' => $color1, 'data' => [$ACTIVE[1], null]],
    ['name' => 'Складские запасы',           'color' => $color1, 'data' => [$ACTIVE[2], null]],
    ['name' => 'Денежные средства',          'color' => $color1, 'data' => [$ACTIVE[3], null]],
    ['name' => 'Незавершенные проекты',      'color' => $color1, 'data' => [$ACTIVE[4], null]],
    ['name' => 'Основные средства',          'color' => $color1, 'data' => [$ACTIVE[5], null]],
    // Пассивы
    ['name' => 'Кредиторская задолженность', 'color' => $color2, 'data' => [null, $PASSIVE[1]]],
    ['name' => 'Кредиты до 12 месяцев',      'color' => $color2, 'data' => [null, $PASSIVE[2]]],
    ['name' => 'Долгосрочные обязательства', 'color' => $color2, 'data' => [null, $PASSIVE[3]]],
    ['name' => 'Собственный капитал',        'color' => ($SELF > 0) ? $color3 : $color4, 'data' => [null, $SELF]],
];

if ($sumActive > $sumPassive) {
    $series[] = ['name' => 'Не сходится', 'color' => $colorGray, 'data' => [null, $sumActive - $sumPassive]];
} elseif ($sumActive < $sumPassive) {
    $series[] = ['name' => 'Не сходится', 'color' => $colorGray, 'data' => [$sumPassive - $sumActive, null]];
}

$yMin = round(($sumSelf < 0) ? $sumSelf : 0);
$yMax = round(max($sumActive, $sumPassive));

if (Yii::$app->request->post('chart-balance-structure-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDate' => $currDate,
        'self' => $SELF,
        'optionsChart' => [
            'sumActive' => (float)$sumActive,
            'sumPassive' => (float)$sumPassive,
            'sumSelf' => (float)$sumSelf,
            'yAxis' => [
                'min' => $yMin,
                'max' => $yMax
            ],
            'series' => $series,
        ],
    ]);

    exit;
}

?>
<style>
    #chart-balance-structure { height: 265px; }
</style>

<div class="ht-caption noselect mb-2">
    СТРУКТУРА БАЛАНСА
    <div class="wrap-select2-no-padding ml-1" style="float:right; margin-top:-13px;">
        <?= \kartik\select2\Select2::widget([
            'hideSearch' => true,
            'id' => 'chart-balance-structure-month',
            'name' => 'chart-balance-structure-month',
            'data' => array_filter(array_reverse(AbstractFinance::$month, true),
                function($k) use ($model) {
                    return ($model->year < date('Y') || (int)$k <= date('n'));
                }, ARRAY_FILTER_USE_KEY),
            'options' => [
                'class' => 'form-control',
                'style' => 'display: inline-block;',
            ],
            'value' => $customMonth,
            'pluginOptions' => [
                'width' => '106px',
                'containerCssClass' => 'select2-balance-structure'
            ]
        ]); ?>
    </div>
</div>
<div class="clearfix"></div>

<?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-balance-structure',
            'scripts' => [
                //'modules/exporting',
                'themes/grid-light',
                'modules/pattern-fill',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'events' => [],
                    'marginRight' => 1,
                    'marginBottom' => '50',
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'animation' => false
                ],
                'legend' => false,
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {

                            var index = this.series.data.indexOf( this.point );
                            var series_index = this.series.index;
                            var amount = args.chart.series[series_index].data[index].y;
                            var percent;
                            
                            if (9 == series_index) // serie 'does not converge'
                            {
                                amount = ChartBalanceStructure.sumActive - ChartBalanceStructure.sumPassive;
                                if (ChartBalanceStructure.sumSelf < 0)
                                    amount += Math.abs(ChartBalanceStructure.sumSelf);
                            }
                            
                            percent = (ChartBalanceStructure.sumActive != 0) ? (100 * amount / ChartBalanceStructure.sumActive) : 100;

                            return '<span class=\"title\">' + args.chart.series[series_index].name + '</span><br/>' +
                                '<table class=\"indicators\">' +
                                    '<tr>' + '<td class=\"gray-text\">Сумма: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(amount, 2, ',', ' ') + ' ₽</td></tr>' +
                                    '<tr>' + '<td class=\"gray-text\">Доля в балансе: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(percent, 0, ',', ' ') + ' %</td></tr>' +
                                '</table>';
                        }
                    "),
                    'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX,y:point.plotY - 60}; }'),
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => ['text' => ''],
                'xAxis' => [
                    [
                        'categories' => [
                            '<b>АКТИВЫ</b><br/>Что имеем',
                            '<b>ПАССИВЫ</b><br/>Источники капитала'
                        ],
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '400',
                                'fontSize' => '14px',
                                'whiteSpace' => 'nowrap',
                                'color' => '#9198a0'
                            ],
                        ]
                    ],
                ],
                'yAxis' => [
                    'max' => $yMax,
                    'min' => $yMin,
                    'index' => 0,
                    'title' => '',
                    'tickPixelInterval' => 1,
                    'endOnTick' => false,
                    'startOnTick' => false,
                    'minorGridLineWidth' => 0,
                    'gridLineWidth' => 0,
                    'labels' => [
                        'useHTML' => true,
                        'style' => [
                            'fontWeight' => '300',
                            'fontSize' => '13px',
                            'whiteSpace' => 'nowrap'
                        ],
                        'formatter' => new JsExpression("function() {

                            const formatCash = function(n) {
                              if (Math.abs(n) < 1e3) return n;
                              if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                              if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                            };

                            if (this.isLast || this.isFirst || this.value == 0) {
                                return formatCash(this.value);
                            }
                        } "),
                    ]
                ],
                'series' => $series,
                'plotOptions' => [
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        //'borderRadius' => 3
                    ],
                    'column' => [
                        'dataLabels' => [
                            'enabled' => true,
                            'inside' => true,
                            'useHtml' => true,
                            'formatter' => new jsExpression("
                                function() {

                                    var index = this.series.data.indexOf( this.point );
                                    var series_index = this.series.index;
                                    if (Math.abs(Math.round(100 * (this.series.yData[0] + this.series.yData[1]) / ChartBalanceStructure.sumActive)) >= {$minHideLabelPercent})
                                        return '<span class=\"stack-title\">' + this.series.name + '</span>';
                                }
                            "),
                        ],
                        'pointPadding' => 0,
                        'groupPadding' => 0,
                        'ordinal' => true,
                        'stacking' => true
                    ]
                ],
            ],
        ]); ?>

<script>

    // CHANGE MONTH
    window.ChartBalanceStructure = {
        _inProcess: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        chartPoints: {},
        yAxis: {},
        sumActive: <?= (float)$sumActive ?>,
        sumPassive: <?= (float)$sumPassive ?>,
        sumSelf: <?= (float)$sumSelf ?>,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('#chart-balance-structure-month').on('change', function() {

                // prevent double-click
                if (window.ChartBalanceStructure._inProcess) {
                    return false;
                }

                window.ChartBalanceStructure.year = $('#balancesearch-year').val();
                window.ChartBalanceStructure.month = $(this).val();
                window.ChartBalanceStructure.redrawByClick();

                window.ChartBalance.resetPoints();
            });

        },
        redrawByClick: function() {
            return window.ChartBalanceStructure._getData().done(function() {
                $('#chart-balance-structure').highcharts().update({"series": ChartBalanceStructure.chartPoints});
                $('#chart-balance-structure').highcharts().update({"yAxis": ChartBalanceStructure.yAxis});
                window.ChartBalanceStructure._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartBalanceStructure._inProcess = true;
            return $.post('/analytics/finance-ajax/get-balance-structure-data', {
                    "chart-balance-structure-ajax": true,
                    "year": ChartBalanceStructure.year,
                    "month": ChartBalanceStructure.month,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartBalanceStructure.chartPoints = data.optionsChart.series;
                    ChartBalanceStructure.yAxis = data.optionsChart.yAxis;
                    ChartBalanceStructure.sumActive = data.optionsChart.sumActive;
                    ChartBalanceStructure.sumPassive = data.optionsChart.sumPassive;
                    ChartBalanceStructure.sumSelf = data.optionsChart.sumSelf;
                }
            );
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    ////////////////////////////////////
    window.ChartBalanceStructure.init();
    ////////////////////////////////////

    // fix bug
    $(document).ready(function() {
        setTimeout('$(".select2-selection__rendered").removeAttr("title")', 500);
        setTimeout('$(".select2-selection__rendered").removeAttr("title","")', 2500);
    });

</script>