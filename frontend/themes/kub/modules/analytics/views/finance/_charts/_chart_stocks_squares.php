<?php

use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\StocksSearch;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $model StocksSearch */

///////////////// consts ////////
$color = 'rgba(46,159,191,1)';
$colorGray = 'rgba(81,81,81,1)';
$LIMIT = 100; // max products count
$nMax = 20; // max blocks count
/////////////////////////////////

///////////////// dynamic vars ////////////////////////////////////////////////////////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
///////////////////////////////////////////////////////////////////////////////////////////

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$mainData = $model->getStructureChartData($dateFrom, $dateTo);
//$totalAmount = array_sum(array_column($mainData, 'amount'));
$totalAmount = 0;
foreach ($mainData as $d)
    if ($d['amount'] > 0)
        $totalAmount += $d['amount'];

$n = 0;
$otherBlockAmount = 0;
foreach ($mainData as $d) {

    $n++;

    if ($n < $nMax || $d['amount'] <= 0)
        continue;

    $otherBlockAmount += $d['amount'];
}

$n = 0;
$lim = 0;
$data = [];
foreach ($mainData as $d) {

    if ($d['amount'] <= 0)
        continue;

    $n++;

    if ($n < $nMax) {

        $id = "id_{$n}";

        $data[] = [
            'id' => $id,
            'name' => $d['title'],
            'color' => $color,
            'value' => (float)$d['amount'],
        ];

    }  else {

        $parentId = "id_{$nMax}";
        $id = "{$parentId}_{$n}";

        if ($n === $nMax) {
            $data[] = [
                'id' => $parentId,
                'name' => ($model->byGroups) ? 'Остальные группы товара' : 'Остальные товары',
                'color' => $color,
                'value' => (float)$otherBlockAmount
            ];
            $data[] = [
                'id' => $id,
                'parent' => $parentId,
                'name' => $d['title'],
                'color' => $color,
                'value' => (float)$d['amount'],
            ];
        } else {
            $data[] = [
                'id' => $id,
                'parent' => $parentId,
                'name' => $d['title'],
                'color' => $color,
                'value' => (float)$d['amount'],
            ];
        }
    }

    if (++$lim > $LIMIT)
        break;
}

if (empty($data)) {
    $data[] = [
        'id' => 'id_empty',
        'name' => 'Нет данных',
        'color' => $colorGray,
        'value' => 0.001,
    ];
}

$chartTitle = ($model->byGroups) ? 'СТРУКТУРА ПО ТОВАРНЫМ ГРУППАМ' : 'СТРУКТУРА ПО ТОВАРНЫМ ПОЗИЦИЯМ'
    . ' ' . '<span style="text-transform:none">за</span>'
    . ' ' . ArrayHelper::getValue(AbstractFinance::$month, $customMonth, '?')
    . ' ' . $model->year . '<span style="text-transform:none"> г</span>';
$tooltipSubtitle = ($model->byGroups) ? 'Доля группы' : 'Доля товара';

if (Yii::$app->request->post('chart-stocks-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'totalAmount' => $totalAmount,
        'chartTitle' => $chartTitle,
        'optionsChart' => [
            'series' => [
                [
                    'data' => $data,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-stocks-squares {
        height: 215px;
    }
</style>

<div id="chart-stocks-squares-title" class="ht-caption noselect mb-2">
    <?= $chartTitle ?>
</div>

<div class="clearfix"></div>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-stocks-squares',
    'scripts' => [
        'themes/grid-light',
        'modules/data',
        'modules/heatmap',
        'modules/treemap',
    ],
    'options' => [
        'credits' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'treemap',
            'alternateStartingDirection' => true,
            'events' => [],
            'margin' => 0,
            'style' => [
                'fontFamily' => '"Corpid E3 SCd", sans-serif',
            ],
            'animationLimit' => 1000,
            //'animation' => false,
        ],
        'legend' => false,
        'title' => ['text' => ''],
        'series' => [
            [
                'layoutAlgorithm' => 'strip',
                'allowDrillToNode' => true,
                'levelIsConstant' => false,
                'levels' => [
                    [
                        'level' => 1,
                        'dataLabels' => [
                            'enabled' => true
                        ],
                        'borderWidth' => 1,
                        'borderColor' => 'rgba(255,255,255,1)'
                    ],
                    [
                        'level' => 2,
                        'dataLabels' => [
                            'enabled' => false
                        ],
                        'borderWidth' => 1,
                        'borderColor' => 'rgba(255,255,255,.1)'
                    ]
                ],
                'drillUpButton' => [
                    'text' => '< Назад',
                ],
                'data' => $data
            ]
        ],
        'plotOptions' => [
            'treemap' => [
                'layoutAlgorithm' => 'squarified',
                'dataLabels' => [
                    'enabled' => true,
                    'inside' => true,
                    'useHTML' => false,
                    'formatter' => new jsExpression("
                        function() {
                            if (this.point.shapeArgs && this.point.shapeArgs.height > 15)
                                return '<span class=\"stack-title\">' + this.point.name + '</span>';
                        }
                    "),
                    'style' => [
                        'fontSize' => '12px',
                        'fontWeight' => 'normal'
                    ]
                ],
            ]
        ],
        'tooltip' => [
            'useHTML' => true,
            'shared' => false,
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'borderRadius' => 8,
            'formatter' => new jsExpression("
                function(args) {

                    const amount = this.point.value;
                    const totalAmount = (ChartStocksSquares.totalAmount > 0) ? ChartStocksSquares.totalAmount : 9E9;

                    return '<span class=\"title\">' + this.point.name + '</span><br/>' +
                        '<table class=\"indicators\">' +
                            '<tr>' + '<td class=\"gray-text\">Остаток: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(amount, 2, ',', ' ') + ' ₽</td></tr>' +
                                        ('<tr>' + '<td class=\"gray-text\">{$tooltipSubtitle}: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * amount / totalAmount, 2, ',', ' ') + ' %</td></tr>') +                            
                        '</table>';
                }
            "),
            //'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX,y:point.plotY - 60}; }'),
            //'hideDelay' => 9000
        ],

    ],
]); ?>

<script>
    ChartStocksSquares = {
        chart: 'squares',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        totalAmount: "<?= $totalAmount ?>",
        chartPoints: {},
        chartTitle: null,
        _inProcess: false,
        redrawByClick: function() {
            return ChartStocksSquares._getData().done(function() {
                const $chart = $('#chart-stocks-squares').highcharts();
                const $title = $('#chart-stocks-squares-title');
                $chart.update(ChartStocksSquares.chartPoints);
                $title.html(ChartStocksSquares.chartTitle);
                ChartStocksSquares._inProcess = false;
            });
        },
        _getData: function() {
            ChartStocksSquares._inProcess = true;
            return $.post('/analytics/finance-ajax/get-stocks-chart-data', {
                    "chart-stocks-ajax": true,
                    "chart": ChartStocksSquares.chart,
                    "year": ChartStocksSquares.year,
                    "month": ChartStocksSquares.month,
                    "group": ChartStocksSquares.byGroup,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartStocksSquares.chartPoints = data.optionsChart;
                    ChartStocksSquares.totalAmount = data.totalAmount;
                    ChartStocksSquares.chartTitle = data.chartTitle;
                }
            );
        },        
    };
</script>
