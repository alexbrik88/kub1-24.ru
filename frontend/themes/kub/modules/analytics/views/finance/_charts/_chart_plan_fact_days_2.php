<?php
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\themes\kub\components\Icon;
use frontend\modules\analytics\models\PlanFactSearch;
use frontend\modules\analytics\models\AbstractFinance;

/* @var $model PlanFactSearch */
$model = $model ?? (new PlanFactSearch());

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = $customPeriod ?? "months";
$customFilters = [
    'purse' => $customPurse ?? null,
    'activity' => $customActivity ?? null,
    'article' => $customArticle ?? []
];
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color3 = 'rgba(38, 205, 88, .75)';
$color4 = 'rgba(227, 6, 17, .75)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 10;
$RIGHT_DATE_OFFSET = 10;
$MOVE_OFFSET = 6;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $LEFT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $RIGHT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

elseif ($customPeriod == "days") {

    $datePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $monthPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($monthPrev != $month && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                'next' => Month::$monthFullRU[$month]
            ];
            // emulate align right
            switch ($month) {
                case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
            }
        }

        $monthPrev = $month;

        $daysPeriods[] = $day;
        $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
        $chartPlanFactLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);
        if ($CENTER_DATE == $date['from'])
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');
}

$mainData = $model->getPlanFactSeriesData($datePeriods, $customPeriod, $customFilters);
$balance = &$mainData['balance'];
$incomeDiff = &$mainData['incomeDiff'];
$outcomeDiff = &$mainData['outcomeDiff'];
$planIncome = &$mainData['planIncome'];
$planOutcome = &$mainData['planOutcome'];
$factIncome = &$mainData['factIncome'];
$factOutcome = &$mainData['factOutcome'];
////////////////////////////////////////////////

if (Yii::$app->request->post('chart-plan-fact-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $incomeDiff,
                ],
                [
                    'data' => $outcomeDiff,
                ],
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $balance,
                ],
            ],
        ],
        'rawData' => [
            'planIncome'  => $planIncome,
            'planOutcome' => $planOutcome,
            'factIncome'  => $factIncome,
            'factOutcome' => $factOutcome
        ]
    ]);

    exit;
}

?>
<style>
    #chart-plan-fact { height: 213px; }
    #chart-plan-fact-2 { height: 213px; }
    #chart-plan-fact  .highcharts-axis-labels,
    #chart-plan-fact-2 .highcharts-axis-labels { z-index: -1!important; }
    /* tooltip */
    #chart-plan-fact, #chart-plan-fact .highcharts-container , #chart-plan-fact svg {
        overflow: visible!important;
        z-index: 1!important;
    }
</style>

<div style="position: relative">
    <div style="width: 100%">

        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="left" style="position: absolute; left:40px; bottom:264px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="right" style="position: absolute; right:0px; bottom:264px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:0px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="noselect mb-2">
            <div class="w-100" style="display: flex">
                <div class="ht-caption">Отклонение прихода <span style="text-transform: lowercase">и</span> расхода</div>
                <div class="ml-auto mr-2">
                    <ul class="nav nav-tabs" role="tablist" style="border-bottom: none; margin-top:-1px;">
                        <li class="nav-item pr-2">
                            <a class="chart-plan-fact-days-tab nav-link <?= $customPeriod == 'days' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="days" data-toggle="tab"><b>ДЕНЬ</b></a>
                        </li>
                        <li class="nav-item pr-2">
                            <a class="chart-plan-fact-days-tab nav-link <?= $customPeriod == 'months' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="months" data-toggle="tab"><b>МЕСЯЦ</b></a>
                        </li>
                    </ul>
                </div>
                <div class="plan-fact-filter">
                    <?= $this->render('_chart_plan_fact_days_2_filter', [
                        'searchModel' => $model
                    ]) ?>
                </div>
            </div>
            <!-- Filters Blocks -->
            <div class="cash-filter-buttons mb-0 row row_indents_s mt-2">
                <div class="col-2 pb-2 hidden" data-filter="activity">
                    <div class="top-filter-block">
                        <span class="close"><?= \frontend\components\Icon::get('close') ?></span>
                        <span class="name">Все</span>
                    </div>
                </div>
                <div class="col-2 pb-2 hidden" data-filter="purse">
                    <div class="top-filter-block">
                        <span class="close"><?= Icon::get('close') ?></span>
                        <span class="name">Все</span>
                    </div>
                </div>
                <div class="template col-2 pb-2 hidden" data-filter="article" data-flow_type="" data-id="">
                    <div class="top-filter-block">
                        <span class="close"><?= Icon::get('close') ?></span>
                        <span class="name">---</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div style="min-height:213px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartPlanFact.redrawByLoad()')
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {
                            
                                let idx = this.point.index;

                                const title = '<span class=\"title\">' + window.ChartPlanFact.labelsX[this.point.index] + '</span>';
                                const subtitle = (this.series.index === 0) ? 'Приход' : 'Расход';
                                let   fact = (this.series.index === 0) ? ChartPlanFact.rawData.factIncome[idx] : ChartPlanFact.rawData.factOutcome[idx];
                                let   plan = (this.series.index === 0) ? ChartPlanFact.rawData.planIncome[idx] : ChartPlanFact.rawData.planOutcome[idx];
                                let   deviation, deviationPercent;
                                let   result = '';
                                
                                if (fact !== undefined && plan !== undefined) {
                                
                                    // calc deviation
                                    deviation = (fact - plan);
                                    deviationPercent = (Math.round(plan) !== 0) ? (deviation / plan * 100) : 100;
                                    
                                    if (this.series.index === 1) {
                                        deviation = -1 * deviation;
                                        deviationPercent = -1 * deviationPercent;
                                    }
                                
                                    result += '<tr>' + '<td class=\"gray-text\">' + subtitle + ' факт: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(fact, 2, ',', ' ') + ' ₽</td></tr>';
                                    result += '<tr>' + '<td class=\"gray-text\">' + subtitle + ' план: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(plan, 2, ',', ' ') + ' ₽</td></tr>';
                                    result += '<tr>' + '<td class=\"gray-text\">Отклонение: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(deviation, 2, ',', ' ') + ' ₽</td></tr>';
                                    result += '<tr>' + '<td class=\"gray-text\">Отклонение: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(deviationPercent, 2, ',', ' ') + ' %</td></tr>';                                        
                                }

                                return title + '<table class=\"indicators\">' + result + '</table>';
                            }
                        ")
                        //'hideDelay' => 99999,
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        //'min' => 0,
                        //'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        let result = (this.pos == window.ChartPlanFact.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartPlanFact.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartPlanFact.wrapPointPos) {
                                            result += window.ChartPlanFact.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Отклонения по приходу',
                            'data' => $incomeDiff,
                            'color' => $color1,
                            'negativeColor' => $color4,
                            'negativeFillColor' => $color4,
                            'point' => [
                                'events' => [
                                    'mouseOver' => new JsExpression('function() {
                                        this.graphic.attr({
                                            fill: {
                                                pattern: {
                                                    path: "M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6",
                                                    color: (this.y < 0) ? "'.$color4.'" : "'.$color1.'",
                                                    width: 5,
                                                    height: 5
                                                }
                                            }
                                        });
                                    }'),
                                ]
                            ]
                        ],
                        [
                            'name' => 'Отклонения по расходу',
                            'data' => $outcomeDiff,
                            'color' => $color2,
                            'negativeColor' => $color4,
                            'negativeFillColor' => $color4,
                            'point' => [
                                'events' => [
                                    'mouseOver' => new JsExpression('function() {
                                        this.graphic.attr({
                                            fill: {
                                                pattern: {
                                                    path: "M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6",
                                                    color: (this.y < 0) ? "'.$color4.'" : "'.$color2.'",
                                                    width: 5,
                                                    height: 5
                                                }
                                            }
                                        });
                                    }'),
                                ]
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'groupPadding' => 0.05,
                            'pointPadding' => 0.1,
                            'borderRadius' => 3,



                        ]
                    ],
                ],
            ]); ?>
        </div>

        <div class="ht-caption noselect">
            Отклонение результата
        </div>
        <div style="min-height:213px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-2',
                'scripts' => [
                   // 'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => null
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' =>
                            new jsExpression("
                                function(args) {
                                    let idx = this.point.index;

                                    const title = '<span class=\"title\">' + window.ChartPlanFact.labelsX[this.point.index] + '</span>';
                                    const fact = ChartPlanFact.rawData.factIncome[idx] - ChartPlanFact.rawData.factOutcome[idx];
                                    const plan = ChartPlanFact.rawData.planIncome[idx] - ChartPlanFact.rawData.planOutcome[idx];
                                    let   deviation, deviationPercent;
                                    let   result = '';
                                    
                                    if (fact !== undefined && plan !== undefined) {
                                    
                                        // calc deviation
                                        deviation = (fact - plan);
                                        deviationPercent = (Math.round(plan) !== 0) ? (deviation / plan * 100) : 100;
                                    
                                        result += '<tr>' + '<td class=\"gray-text\">Результат факт: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(fact, 2, ',', ' ') + ' ₽</td></tr>';
                                        result += '<tr>' + '<td class=\"gray-text\">Результат план: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(plan, 2, ',', ' ') + ' ₽</td></tr>';
                                        result += '<tr>' + '<td class=\"gray-text\">Отклонение: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(deviation, 2, ',', ' ') + ' ₽</td></tr>';
                                        result += '<tr>' + '<td class=\"gray-text\">Отклонение: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(deviationPercent, 2, ',', ' ') + ' %</td></tr>';                                        
                                    }

                                    return title + '<table class=\"indicators\">' + result + '</table>';
                                }
                            ")

                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        'min' => 1,
                        'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                        'categories' => $daysPeriods,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("
                                function() {
                                    result = (this.pos == window.ChartPlanFact.currDayPos) ?
                                        ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                        (window.ChartPlanFact.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                    if (window.ChartPlanFact.wrapPointPos) {
                                        result += window.ChartPlanFact.getWrapPointXLabel(this.pos);
                                    }

                                    return result;
                                }"),
                            'useHTML' => true,
                            'autoRotation' => false
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Отклонение результата',
                            'data' => $balance,
                            'color' => $color3,
                            'fillColor' => $color3,
                            'negativeColor' => $color4,
                            'negativeFillColor' => $color4,
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => null,
                            'point' => [
                                'events' => [
                                    'mouseOver' => new JsExpression('function() {
                                        this.graphic.attr({
                                            fill: {
                                                pattern: {
                                                    path: "M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6",
                                                    color: (this.y < 0) ? "'.$color4.'" : "'.$color3.'",
                                                    width: 5,
                                                    height: 5
                                                }
                                            }
                                        });
                                    }'),
                                ]
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'groupPadding' => 0.05,
                            'pointPadding' => 0.3,
                            'borderRadius' => 3,
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    window.ChartPlanFact = {
        isLoaded: false,
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        byPurse: null,
        byActivity: null,
        byArticle: null,
        _inProcess: false,
        rawData: {
            factIncome: <?= json_encode($factIncome) ?>,
            factOutcome: <?= json_encode($factOutcome) ?>,
            planIncome: <?= json_encode($planIncome) ?>,
            planOutcome: <?= json_encode($planOutcome) ?>
        },
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-plan-fact-days-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartPlanFact._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left')
                    window.ChartPlanFact.offset[ChartPlanFact.period] -= <?= $MOVE_OFFSET ?>;
                if ($(this).data('move') === 'right')
                    window.ChartPlanFact.offset[ChartPlanFact.period] += <?= $MOVE_OFFSET ?>;

                window.ChartPlanFact.redrawByClick();
            });

            $('.chart-plan-fact-days-tab').on('show.bs.tab', function (e) {
                ChartPlanFact.period = $(this).data('period');
                ChartPlanFact.redrawByClick();
            });
        },
        redrawByClick: function() {

            return window.ChartPlanFact._getData().done(function() {
                $('#chart-plan-fact-2').highcharts().update(ChartPlanFact.chartPoints2);
                $('#chart-plan-fact').highcharts().update(ChartPlanFact.chartPoints);
                window.ChartPlanFact._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartPlanFact.isLoaded)
                return;

            let chartToLoad = window.setInterval(function () {
                let chart = $('#chart-plan-fact').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.clearInterval(chartToLoad);
                    window.ChartPlanFact.isLoaded = true;
                }
            }, 100);
        },
        _getData: function() {
            window.ChartPlanFact._inProcess = true;
            return $.post('/analytics/finance-ajax/get-plan-fact-data', {
                    "chart-plan-fact-ajax": true,
                    "period": ChartPlanFact.period,
                    "offset": window.ChartPlanFact.offset[ChartPlanFact.period],
                    "purse": ChartPlanFact.byPurse,
                    "activity": ChartPlanFact.byActivity,
                    "article": ChartPlanFact.byArticle,
                    "onPage": "PLAN_FACT"
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartPlanFact.freeDays = data.freeDays;
                    ChartPlanFact.currDayPos = data.currDayPos;
                    ChartPlanFact.labelsX = data.labelsX;
                    ChartPlanFact.wrapPointPos = data.wrapPointPos;
                    ChartPlanFact.chartPoints = data.optionsChart;
                    ChartPlanFact.chartPoints2 = data.optionsChart2;
                    ChartPlanFact.tooltipData = data.tooltipData;
                    ChartPlanFact.rawData = data.rawData;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            let chart = $('#chart-plan-fact').highcharts();
            let colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            let name, left, txtLine, txtLabel;

            let k = (window.ChartPlanFact.period == 'months') ? 0.725 : 0.625;

            if (window.ChartPlanFact.wrapPointPos[x + 1]) {
                name = window.ChartPlanFact.wrapPointPos[x + 1].prev;
                left = window.ChartPlanFact.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartPlanFact.wrapPointPos[x]) {
                name = window.ChartPlanFact.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ////////////////////////////////
    window.ChartPlanFact.init();
    ////////////////////////////////

    $(document).ready(function() {
        window._ChartPlanFactArrowZ = 1;
        $('#chart-plan-fact').mousemove(function(event) {
            let setZ = function() { $('.chart-plan-fact-days-arrow').css({"z-index": window._ChartPlanFactArrowZ}); };
            let y = event.pageY - $(this).offset().top;

            if (y > 167 && window._ChartPlanFactArrowZ === 1) {
                window._ChartPlanFactArrowZ = 2;
                setZ();
            }
            if (y <= 167 && window._ChartPlanFactArrowZ === 2) {
                window._ChartPlanFactArrowZ = 1;
                setZ();
            }
        });

    });

</script>