<?php

use frontend\components\StatisticPeriod;
use yii\web\JsExpression;
use common\components\TextHelper;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\OddsSearch;

/** @var $colors array */
/** @var $customChartType */

$id = $id ?? 'chart_22';
//$periodSessionName = StatisticPeriod::getSessionNameMonths();
$period = StatisticPeriod::getSessionPeriod();
$symbol = ($customChartType == DetailingUserConfig::CHART_TYPE_PROFITABILITY) ? '%' : '₽';
$chartType = 'pie';
?>

<div class="kub-chart border" style="min-width:335px; min-height:250px;">
    <div class="p-2">
    <div class="ht-caption" style="position: absolute; top:1.5rem; left:16px">
        <b><?= $title ?></b>
        <span style="text-transform: none; font-weight:400;">
            <span class="text-pie-chart-period">
                <?= OddsSearch::getSubtitlePeriod($period['from'], $period['to']) ?>
            </span>
        </span>
    </div>
    <?php

    echo Highcharts::widget(
        DC::getOptions([
            'id' => $id,
            'options' => [
                'chart' => [
                    'type' => $chartType,
                    'marginTop' => 30,
                    'spacingTop' => 20,
                    'height' => $height ?? 250,
                    'events' => new JsExpression('
                        setTimeout(function() {
                            const legend = $("#'.$id.'").closest(".kub-chart").parent().find(".detailing-pie-chart-hint");
                            legend.appendTo("#'.$id.' > .highcharts-container");
                            if (Number(chart_22.chartType) !== 4 && 5 !== Number(chart_22.chartType)) {
                                legend.fadeIn(250);
                            }
                        }, 500)')
                ],
                'title' => false,
                'series' => [$series],
                'tooltip' => [
                    'formatter' => new JsExpression("
                        function(args) {
                        
                            const serieName = this.key;
                            const periodName = (chart_22.periodText || ['-']);
                            const total = {$total};
                            const y = (this.color === 'rgba(227,6,17,1)') ? -this.y : this.y;
                            
                            return ('<table class=\"indicators\">' +
                                    '<span class=\"title\">' + periodName[0].toUpperCase() + periodName.slice(1) + '</span>' + 
                                    '<tr>' +
                                        '<td class=\"gray-text text-left\" colspan=\"2\">' + serieName + '</td>' +
                                    '</tr>' +                                 
                                    '<tr>' +
                                        '<td class=\"gray-text\">Сумма:</td>' +
                                        '<td class=\"gray-text-b\">' + Highcharts.numberFormat(y, 2, ',', ' ') + ' ₽</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td class=\"gray-text\">Доля: ' + '</td>' +
                                        '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * y / (total || 9E9), 2, ',', ' ') + ' %</td>' +
                                    '</tr>' +                              
                                '</table>');
                            }
            ")
                ],
                'legend' => [
                    'floating' => true,
                    'layout' => 'vertical',
                    'verticalAlign' => 'middle',
                    'align' => 'left',
                    'y' => 5,
                    'x' => 210,
                    'itemMarginBottom' => 5,
                    'labelFormatter' => new JsExpression('function () {
                        const name = (this.name.length > 20) ? (this.name.substr(0, 17) + "...") : this.name;
                        return \'<span class="name-name">\' + name + \'</span>\';
                    }'),
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => false,
                        'borderWidth' => 0,
                        'size' => '90%',
                        'innerSize' => '50%',
                        'cursor' => 'pointer',
                        'dataLabels' => false,
                        'center' => ['80', '100']
                    ],
                    'column' => [
                        'pointWidth' => 20,
                        'grouping' => false,
                        'shadow' => false,
                        'groupPadding' => 0.1,
                        'pointPadding' => 0.14,
                        'borderRadius' => 3,
                    ]
                ],
                'xAxis' => [
                    'type' => 'category',
                    'gridLineWidth' => 1,
                    'tickmarkPlacement' => 'between'
                ],
                'yAxis' => [
                    'min' => null
                ]
            ],

        ], DC::CHART_PIE)
    );
    ?>
    </div>
</div>

<?php
if (abs($total) < 1E4) {
    $decimals = 2;
    $decimalsTitle = '';
    $decimalsPostfix = '';
    $formattedAmount = $total;
} elseif (abs($total) < 1E6) {
    $decimals = 0;
    $decimalsTitle = '';
    $decimalsPostfix = '';
    $formattedAmount = $total;
} else {
    $decimals = 1;
    $decimalsTitle = '';
    $decimalsPostfix = 'k';
    $formattedAmount = round($total / 1E3, 1);
}
?>

<div class="detailing-pie-chart-hint" style="display: none">
    <div class="pie-label">
        <span class="pie-title"><?= $title ?></div>
    <div class="pie-label mb-2">
        <span class="pie-total bold">
            <?= TextHelper::numberFormat($formattedAmount, $decimals) ?><?= $decimalsPostfix ?>
            <?= $symbol ?>
        </span>
    </div>
</div>
