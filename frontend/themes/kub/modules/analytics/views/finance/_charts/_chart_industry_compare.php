<?php

use common\models\Company;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\AnalyticsSimpleSearch as AnalyticsSS;
use frontend\components\StatisticPeriod;
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\OddsSearch;

/** @var Company $company */
/** @var null|ProfitAndLossSearchModel $palModel */
/** @var null|AnalyticsSS $analyticsModel */

$id = 'chart_21';
$id2 = 'chart_22';

////////////////////////////////////////////////////////////
$MAX_PIE_PIECES = 9;
$xPeriod = 9;
$customPeriod = 'months';
$customOffset = $customOffset ?? 0;
$customChartType = $customChartType ?? null;
$currDayPos = $xPeriod - $customOffset - 1;
$byMonth = ($customPeriod === 'months');
$onPage = 'industry';
$showCog = $showCog ?? true;
$showMenu = $showMenu ?? true;
$lineChartHeight = $lineChartHeight ?? 250;
$pieChartHeight = $pieChartHeight ?? 299;
$periodText = trim(str_replace('за', '', OddsSearch::getSubtitlePeriod()));
$colorsTop = $colorsBottom = [
    'rgb(132,141,201)',
    'rgb(243,183,59)',
    'rgb(132,197,201)',
    'rgb(201,132,132)',
    'rgb(145,152,160)',
    'rgb(0,20,36)',
];
$colorByItem = [];
////////////////////////////////////////////////////////////

$templateSerie = [
    'type' => 'spline',
    'name' => '---',
    'color' => '#000',
    'data' => [],
];

$items = CompanyIndustry::getSelect2Data();
$paramLinesGroupBy = AnalyticsSS::GROUP_BY_INDUSTRY;
$userPeriodFrom = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'from');
$userPeriodTo = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'to');

// CHART PIE ////////////////////////////////////////////////////////////////////////////////

$div100 = function($v) { return round($v / 100, 2); };
$rawItemsData2Top = array_map($div100, $palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo));
$rawItemsData2Bottom = array_map($div100, $palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo));

$itemsData2Top = $itemsData2Bottom = [];
foreach ($items as $itemId => $itemName) {
    $itemsData2Top[] = [
        'name' => $itemName,
        'amount' => $rawItemsData2Top[$itemId] ?? 0,
        'id' => $itemId
    ];
}
usort($itemsData2Top, function ($a, $b) {
    if ($b['amount'] == $a['amount']) {
        return $a['id'] <=> $b['id'];
    }
    return ($b['amount'] < $a['amount']) ? -1 : 1;
});
$resortByPie = array_column($itemsData2Top, 'id');
uksort($items, function($key1, $key2) use ($resortByPie) {
    return (array_search($key1, $resortByPie) > array_search($key2, $resortByPie));
});
foreach ($items as $itemId => $itemName) {
    $itemsData2Bottom[] = [
        'name' => $itemName,
        'amount' => $rawItemsData2Bottom[$itemId] ?? 0,
        'id' => $itemId
    ];
}

$OTHER_SERIES_IDS = [];

$title2Top = 'Выручка';
$data2Top = [];
$total2Top = 0;
$piece = 0;
foreach ($itemsData2Top as $k => $v) {

    $amount = $v['amount'];
    $total2Top += $amount;

    if (++$piece > $MAX_PIE_PIECES) {
        $data2Top[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
        $data2Top[$MAX_PIE_PIECES - 1]['industryID'] = 'other';
        $data2Top[$MAX_PIE_PIECES - 1]['y'] += $amount;
        $OTHER_SERIES_IDS[] = $v['id'];
        continue;
    }

    if (!isset($colorByItem[$v['id']]))
        $colorByItem[$v['id']] = $colorsTop[$piece - 1] ?? $colorsTop[count($colorsTop) - 1];

    $data2Top[$piece - 1] = [
        'name' => $v['name'],
        'industryID' => $v['id'],
        'y' => abs($amount),
        'color' => ($amount >= 0) ? $colorByItem[$v['id']] : 'rgba(227,6,17,1)'
    ];
}
$series2Top = [
    'name' => 'Pie Highchart Top',
    'colorByPoint' => true,
    'data' => $data2Top
];

$title2Bottom = 'Прибыль';
$data2Bottom = [];
$total2Bottom = 0;
$piece = 0;

foreach ($itemsData2Bottom as $k => $v) {

    $amount = $v['amount'];
    $total2Bottom += $amount;

    if (++$piece && in_array($v['id'], $OTHER_SERIES_IDS)) {
        $data2Bottom[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
        $data2Top[$MAX_PIE_PIECES - 1]['industryID'] = 'other';
        $data2Bottom[$MAX_PIE_PIECES - 1]['y'] += $amount;
        continue;
    }

    if (!isset($colorByItem[$v['id']]))
        $colorByItem[$v['id']] = $colorsBottom[$piece - 1] ?? $colorsBottom[count($colorsBottom) - 1];

    $data2Bottom[$piece - 1] = [
        'name' => $v['name'],
        'industryID' => $v['id'],
        'y' => abs($amount),
        'color' => ($amount >= 0) ? $colorByItem[$v['id']] : 'rgba(227,6,17,1)'
    ];
}
$series2Bottom = [
    'name' => 'Pie Highchart Bottom',
    'colorByPoint' => true,
    'data' => $data2Bottom
];

// CHART LINE ////////////////////////////////////////////////////////////////////////////////

// prepare X
$categories = [];
$labelsX = [];
for ($x = $xPeriod - 1; $x >= 0; $x--) {
    $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
    $month = $date->format('n');
    $categories[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
    $labelsX[] = Month::$monthFullRU[$month] . ' ' . $date->format('Y');
}

// prepare Y
$dateFrom = (new DateTime())->modify("-{$xPeriod} months")->modify("+{$customOffset} months")->format('Y-m-01');
$dateTo = (new DateTime())->modify("+{$customOffset} months")->format('Y-m-t');
$itemsDataTop = $itemsDataBottom = [];
for ($x = $xPeriod - 1; $x >= 0; $x--) {
    $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
    $y = $date->format('Y');
    $m = $date->format('m');
    $revenueByYear = $palModel->getMonthDataGroupedByMonth($y, 'totalRevenue');
    $profitByYear = $palModel->getMonthDataGroupedByMonth($y, 'netIncomeLoss');
    foreach ($items as $itemId => $itemName) {
        $itemsDataTop[$itemId][] = round(($revenueByYear[$itemId][$m] ?? 0) / 100, 2);
        $itemsDataBottom[$itemId][] = round(($profitByYear[$itemId][$m] ?? 0) / 100, 2);
    }
}

// LINE
$seriesTop = $seriesBottom = [];
$piece = 0;
foreach ($itemsDataTop as $itemId => $data) {

    $itemName = $items[$itemId] ?? "id={$itemId}";

    if (++$piece && in_array($v['id'], $OTHER_SERIES_IDS)) {
        $seriesTop[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
        $seriesTop[$MAX_PIE_PIECES - 1]['industryID'] = 'other';
        foreach ($seriesTop[$MAX_PIE_PIECES - 1]['data'] as $k => $d) {
            $seriesTop[$MAX_PIE_PIECES - 1]['data'][$k] += $data[$k];
        }

        continue;
    }

    if (!isset($colorByItem[$itemId]))
        $colorByItem[$itemId] = $colorsTop[$piece - 1] ?? $colorsTop[count($colorsTop) - 1];

    $seriesTop[$piece - 1] = array_replace($templateSerie, [
        'name' => $itemName,
        'industryID' => $itemId,
        'color' => $colorByItem[$itemId],
        'data' => $data
    ]);
}
$piece = 0;
foreach ($itemsDataBottom as $itemId => $data) {

    $itemName = $items[$itemId] ?? "id={$itemId}";

    if (++$piece && in_array($v['id'], $OTHER_SERIES_IDS)) {
        $seriesBottom[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
        $seriesTop[$MAX_PIE_PIECES - 1]['industryID'] = 'other';
        foreach ($seriesBottom[$MAX_PIE_PIECES - 1]['data'] as $k => $d) {
            $seriesBottom[$MAX_PIE_PIECES - 1]['data'][$k] += $data[$k];
        }

        continue;
    }

    if (!isset($colorByItem[$itemId]))
        $colorByItem[$itemId] = $colorsBottom[$piece - 1] ?? $colorsBottom[count($colorsBottom) - 1];

    $seriesBottom[$piece - 1] = array_replace($templateSerie, [
        'name' => $itemName,
        'industryID' => $itemId,
        'color' => $colorByItem[$itemId],
        'data' => $data
    ]);
}

?>

<script>
    if (window.chart_21) {
        chart_21.currDayPos = <?= $currDayPos ?>;
        chart_21.labelsX = <?= json_encode($labelsX) ?>;
        chart_21.dataTop = <?= json_encode(['series' => $seriesTop, 'xAxis' => ['categories' => $categories]]) ?>;
        chart_21.dataBottom = <?= json_encode(['series' => $seriesBottom, 'xAxis' => ['categories' => $categories]]) ?>;
    }
    //if (window.chart_22) {
    //    chart_22.title = '<?//= $title2Top ?>//';
    //    chart_22.total = <?//= $total2Top ?>//;
    //    chart_22.data = <?//= json_encode([
    //        'series' => $series2Top
    //    ]) ?>//;
    //}
</script>

<?php if (isset($isAjax)) { return; } ?>

<div class="row m-0">
    <div class="col-sm-12 col-md-8 pr-2 pt-2 pl-2 pb-0">
        <?= $this->render('_chart_industry_compare_line', [
            'id' => 'chart_21_top',
            'title' => 'ВЫРУЧКА по НАПРАВЛЕНИЯМ',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesTop,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-0" style="overflow: hidden">
        <?= $this->render('_chart_industry_compare_pie', [
            'id' => 'chart_22_top',
            'title' => 'Выручка',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $series2Top,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'total' => $total2Top,
            'height' => $pieChartHeight,
        ]) ?>
    </div>

    <div class="col-sm-12 col-md-8 p-2">
        <?= $this->render('_chart_industry_compare_line', [
            'id' => 'chart_21_bottom',
            'title' => 'ПРИБЫЛЬ по НАПРАВЛЕНИЯМ',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesBottom,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
        ]) ?>
    </div>

    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-2" style="overflow: hidden">
        <?= $this->render('_chart_industry_compare_pie', [
            'id' => 'chart_22_bottom',
            'title' => 'Прибыль',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $series2Bottom,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'total' => $total2Bottom,
            'height' => $pieChartHeight,
        ]) ?>
    </div>    

    <div id="chart-transmitter"><!-- js update charts data --></div>

</div>

<script>
    window.chart_21 = {
        dataTop: {},
        dataBottom: {},
        labelsX: <?= json_encode($labelsX) ?>,
        freeDays: [],
        currDayPos: <?= $currDayPos ?>,
        offset: <?= $customOffset ?>,
        period: 'months',
        chartType: '<?= $customChartType ?>',
        onPage: '<?= $onPage ?>',
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            const that = this;

            // move chart
            $(document).on("click", ".chart-offset-2", function () {
                const offset = $(this).data('offset');

                if (that._inProcess) {
                    return false;
                }

                that.offset += offset;
                that.refresh();
            });
        },
        refresh: function() {
            const that = this;
            that._inProcess = true;
            that._getData().done(function() {
                that._repaint();
                that._inProcess = false;
                // pie
                //if (window.chart_22) {
                //    window.chart_22.chartType = that.chartType;
                //    window.chart_22._repaint();
                //}
            });
        },
        _getData: function() {
            const that = this;
            return $.post('/analytics/finance-ajax/get-industry-compare-data', {
                    "chart-industry-compare-ajax": 1,
                    "period": that.period || 'months',
                    "offset": that.offset || 0,
                    "chart-type": that.chartType || 1,
                    "on-page": that.onPage || '',
                },
                function(data) {
                    $('#chart-transmitter').append(data).html(null);
                }
            );
        },
        _repaint: function() {
            const that = this;
            $('#chart_21_top').highcharts().update(that.dataTop);
            $('#chart_21_bottom').highcharts().update(that.dataBottom);
        },
    };

    window.chart_22 = {
        chartType: '<?= $customChartType ?>',
        total: <?= $total2Top ?>,
        title: '<?= $title2Top ?>',
        periodText: '<?= $periodText ?>',
        data: {},
        onPage: '<?= $onPage ?>',
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            const that = this;
            $(document).on('click', '#chart_22_top .highcharts-legend-item', function(e) {
                const index = $(this).index();
                if (!e.isTrigger) {
                    $('#chart_22_bottom .highcharts-legend-item').eq(index).trigger('click', {eee: 'eee'});
                    setTimeout(that.toggleSeries(this), 100);
                }
            });
            $(document).on('click', '#chart_22_bottom .highcharts-legend-item', function(e) {
                const index = $(this).index();
                if (!e.isTrigger) {
                    $('#chart_22_top .highcharts-legend-item').eq(index).trigger('click', {eee: 'eee'});
                    setTimeout(that.toggleSeries(this), 100);
                }
            });
        },
        toggleSeries: function(serie) {
            const chartTop = $('#chart_21_top').highcharts();
            const chartBottom = $('#chart_21_bottom').highcharts();
            const index = $(serie).index();
            const visible = !$(serie).hasClass('highcharts-legend-item-hidden');

            chartTop.series[index].update({visible: visible});
            chartBottom.series[index].update({visible: visible});
        },
        _repaint: function() {
            const that = this;
            const chartTop = $('#chart_22_top');
            const symbol = String(that.chartType) === "5" ? ' %' : ' ₽';
            chartTop.highcharts().update(that.dataTop);
            chartTop.find('.pie-title').html(that.title);
            chartTop.find('.pie-total').html(number_format(that.total, 0, ',', ' ') + symbol);
            chartTop.highcharts().series[0].update({
                type: 'pie'
            });
        },
    };

    ///////////////////////////////
    $(document).ready(function() {
        window.chart_21.init();
        window.chart_22.init();
    });
    ///////////////////////////////

</script>