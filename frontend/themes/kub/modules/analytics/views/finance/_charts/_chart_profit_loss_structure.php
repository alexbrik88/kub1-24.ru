<?php

use frontend\modules\analytics\models\ExpensesSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $yMin float */
/** @var $yMax float */
/** @var $categories array */
/** @var $series array */
?>
<style>
    #chart-profit-loss-structure { height: 235px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            СТРУКТУРА ПРИБЫЛИ
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'hideSearch' => true,
                    'id' => 'chart-profit-loss-structure-month',
                    'name' => 'chart-profit-loss-structure-month',
                    'data' => array_filter(array_reverse(AbstractFinance::$month, true),
                        function($k) use ($model) {
                            return ($model->year < date('Y') || (int)$k <= date('n'));
                        }, ARRAY_FILTER_USE_KEY),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'value' => $customMonth,
                    'pluginOptions' => [
                        'width' => '106px',
                        'containerCssClass' => 'select2-balance-structure'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-profit-loss-structure',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                    'highcharts-more'
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'columnrange',
                        'spacing' => [0,0,0,0],
                        'marginBottom' => 50,
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        //'animation' => false
                    ],
                    'legend' => false,
                    'tooltip' => [
                        'useHTML' => true,
                        //'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var year_name = $('select#profitandlosssearchmodel-year').find('option:selected').text();
                                var month_name = $('select#chart-profit-loss-structure-month').find('option:selected').text();
                                var totalExpenseAmount = (ChartsProfitAndLoss.Structure.totalExpenseAmount > 0) ? ChartsProfitAndLoss.Structure.totalExpenseAmount : 9E9;
                                var isExpense = ChartsProfitAndLoss.Structure.isExpense[index] || false;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' +
                                       ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.point.high - this.point.low, 2, ',', ' ') + ' ₽</td></tr>') +
                                       ((isExpense) ?
                                           ('<tr>' + '<td class=\"gray-text\">Доля в расходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * (this.point.high - this.point.low) / totalExpenseAmount, 0, ',', ' ') + ' %</td></tr>') : '') +
                                    '</table>';
                            }
                        "),
                        //'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x: point.plotX / 3 + 75, y: point.plotY + 50}; }'),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => $yMin,
                        'max' => $yMax,
                        'title' => '',
                        'lineWidth' => 1,
                        'gridLineWidth' => 0,
                        'minorGridLineWidth' => 0,
                        'endOnTick' => false,
                        'startOnTick' => false,
                        'tickPixelInterval' => 1,
                        'index' => 0,
                        'lineColor' => '#9198a0',
                        'plotLines' => [
                                [
                                    'color' => 'rgb(100,100,100,.175)',
                                    'width' => 1,
                                    'value' => 0,
                                    'zIndex' => 5
                                ]
                        ],
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ],
                            'formatter' => new JsExpression("function() {

                                const formatCash = function(n) {
                                  if (Math.abs(n) < 1e3) return n;
                                  if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                                  if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                                };

                                if (this.isLast || this.isFirst || this.value == 0) {
                                    return formatCash(this.value);
                                }
                            } "),
                        ]
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'lineWidth' => 1,
                        'gridLineWidth' => 0,
                        'minorGridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => false,
                        'lineColor' => '#9198a0',
                    ],
                    'series' => $series,
                    'plotOptions' => [
                        'columnrange' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
        <div style="width: 90%; font-size: 13px; color: #001400; margin-top: -42px; user-select: none; margin-left:10%;">
            <div style="width:33%; float:left; text-align: left;">ВЫРУЧКА</div>
            <div style="width:33%; float:left; text-align: center;">РАСХОДЫ</div>
            <div style="width:33%; float:left; text-align: right;">ПРИБЫЛЬ</div>
        </div>
    </div>
</div>