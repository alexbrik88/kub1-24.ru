<?php

use common\models\Company;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\AnalyticsSimpleSearch as AnalyticsSS;
use frontend\components\StatisticPeriod;
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;

/** @var Company $company */
/** @var null|ProfitAndLossSearchModel $palModel */
/** @var null|AnalyticsSS $analyticsModel */

$id = 'chart_21';
$id2 = 'chart_22';

////////////////////////////////////////////////////////////
$items = $items ?? [];
$paramLinesGroupBy = AnalyticsSS::GROUP_BY_PROJECT;
$MAX_PIE_PIECES = 9;
$xPeriod = 9;
$customPeriod = 'months';
$customOffset = $customOffset ?? 0;
$customChartType = $customChartType ?? null;
$currDayPos = $xPeriod - $customOffset - 1;
$byMonth = ($customPeriod === 'months');
$onPage = 'project';
$showCog = $showCog ?? true;
$showMenu = $showMenu ?? true;
$lineChartHeight = $lineChartHeight ?? 250;
$pieChartHeight = $pieChartHeight ?? 255;
$periodText = trim(str_replace('за', '', OddsSearch::getSubtitlePeriod()));
$colors = [
    'rgb(1,88,157)',
    'rgb(61,135,194)',
    'rgb(182,222,255)',
    'rgb(0,81,147)',
    'rgb(23,126,208)',
    'rgb(133,189,234)',
    'rgb(34,157,255)',
    'rgb(91,155,206)',
    'rgb(84,124,155)',
];
$colorByItem = [];
////////////////////////////////////////////////////////////

// CHART LINE ////////////////////////////////////////////////////////////////////////////////

// prepare X
$categories = [];
$labelsX = [];
for ($x = $xPeriod - 1; $x >= 0; $x--) {
    $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
    $month = $date->format('n');
    $categories[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
    $labelsX[] = Month::$monthFullRU[$month] . ' ' . $date->format('Y');
}

// prepare Y
$dateFrom = (new DateTime())->modify("-{$xPeriod} months")->modify("+{$customOffset} months")->format('Y-m-01');
$dateTo = (new DateTime())->modify("+{$customOffset} months")->format('Y-m-t');

$lineDataRevenue = $lineDataProfit = $lineDataIncome = $lineDataExpense = $lineDataRentability = [];
for ($x = $xPeriod - 1; $x >= 0; $x--) {
    $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
    $y = $date->format('Y');
    $m = $date->format('m');
    $revenueByYear = $palModel->getMonthDataGroupedByMonth($y, 'totalRevenue');
    $profitByYear = $palModel->getMonthDataGroupedByMonth($y, 'netIncomeLoss');
    $incomeByYear = $analyticsModel->getAllLinesByFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_INCOME, $paramLinesGroupBy);
    $expenseByYear = $analyticsModel->getAllLinesByFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $paramLinesGroupBy);

    foreach ($items as $itemId => $itemName) {
        $lineDataRevenue[$itemId][] = round(($revenueByYear[$itemId][$m] ?? 0) / 100, 2);
        $lineDataProfit[$itemId][] = round(($profitByYear[$itemId][$m] ?? 0) / 100, 2);
        $lineDataIncome[$itemId][] = $incomeByYear[$itemId][$y.$m] ?? 0;
        $lineDataExpense[$itemId][] = $expenseByYear[$itemId][$y.$m] ?? 0;
        $lineDataRentability[$itemId][] = round(100 * ($profitByYear[$itemId][$m] ?? 0) / (($revenueByYear[$itemId][$m] ?? 0) ?: 9E99), 2);
    }
}

// todo: create helper
class HelpLine {

    static $templateSerie = [
        'type' => 'spline',
        'name' => '---',
        'color' => '#000',
        'data' => [],
    ];

    static function getSeries($rawData, $items, $colors, &$colorByItem, $isAvgCalc = false)
    {
        // Line Revenue
        $MAX_PIE_PIECES = 9;
        $series = [];
        $piece = 0;
        foreach ($items as $itemId => $itemName) {
            $data = $rawData[$itemId] ?? [0];

            if (++$piece > $MAX_PIE_PIECES) {
                $series[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
                $series[$MAX_PIE_PIECES - 1]['color'] = $colors[count($colors) - 1];
                foreach ($series[$MAX_PIE_PIECES - 1]['data'] as $k => $d) {
                    $series[$MAX_PIE_PIECES - 1]['data'][$k] += ($data[$k] ?? 0);
                }
                continue;
            }

            if (!isset($colorByItem[$itemId]))
                $colorByItem[$itemId] = $colors[$piece - 1] ?? $colors[count($colors) - 1];

            $series[$piece - 1] = array_replace(self::$templateSerie, [
                'name' => $itemName,
                'color' => $colorByItem[$itemId],
                'data' => $data,
            ]);
        }

        if ($isAvgCalc && count($items) >= $MAX_PIE_PIECES) {
            if ($avgKoef = 1 + count($items) - $MAX_PIE_PIECES) {
                foreach ($series[$MAX_PIE_PIECES - 1]['data'] as $k => $d) {
                    $series[$MAX_PIE_PIECES - 1]['data'][$k] = $series[$MAX_PIE_PIECES - 1]['data'][$k] / $avgKoef;
                }
            }
        }

        return $series;
    }
}

// Line
$seriesLineRevenue = HelpLine::getSeries($lineDataRevenue, $items, $colors, $colorByItem);
$seriesLineProfit = HelpLine::getSeries($lineDataProfit, $items, $colors, $colorByItem);
$seriesLineIncome = HelpLine::getSeries($lineDataIncome, $items, $colors, $colorByItem);
$seriesLineExpense = HelpLine::getSeries($lineDataExpense, $items, $colors, $colorByItem);
$seriesLineRentability = HelpLine::getSeries($lineDataRentability, $items, $colors, $colorByItem, true);


// CHART PIE ////////////////////////////////////////////////////////////////////////////////

$userPeriodFrom = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'from');
$userPeriodTo = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'to');
$div100 = function($v) { return round($v / 100, 2); };

$pieRawRevenue = array_map($div100, $palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo));
$pieRawProfit = array_map($div100, $palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo));
$pieRawIncome = $analyticsModel->getAllPiesByFlowsByPeriod($userPeriodFrom, $userPeriodTo, CashFlowsBase::FLOW_TYPE_INCOME, $paramLinesGroupBy);
$pieRawExpense = $analyticsModel->getAllPiesByFlowsByPeriod($userPeriodFrom, $userPeriodTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $paramLinesGroupBy);
$pieRawRentability = [];
foreach (array_keys($items) as $pid) {
    $r = ArrayHelper::getValue($palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo), $pid, 0);
    $n = ArrayHelper::getValue($palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo), $pid, 0);
    $pieRawRentability[$pid] = 100 * $n / ($r ?: 9E99);
}

$pieDataRevenue = $pieDataProfit = $pieDataIncome = $pieDataExpense = $pieDataRentability = [];
foreach ($items as $itemId => $itemName) {
    $pieDataRevenue[] = ['name' => $itemName, 'amount' => $pieRawRevenue[$itemId] ?? 0, 'id' => $itemId];
    $pieDataProfit[] = ['name' => $itemName, 'amount' => $pieRawProfit[$itemId] ?? 0, 'id' => $itemId];
    $pieDataIncome[] = ['name' => $itemName, 'amount' => $pieRawIncome[$itemId] ?? 0, 'id' => $itemId];
    $pieDataExpense[] = ['name' => $itemName, 'amount' => $pieRawExpense[$itemId] ?? 0, 'id' => $itemId];
    $pieDataRentability[] = ['name' => $itemName, 'amount' => $pieRawRentability[$itemId] ?? 0, 'id' => $itemId];
}

// Rentability = calc avg total percents
$otherAvgTotal2 = $allAvgTotal2 = [
    'revenue' => 0,
    'profit' => 0
];
$piece = 0;
$netIncomeLoss = $palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo);
$revenue = $palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo);
foreach ($pieDataRentability as $p) {
    if (++$piece >= $MAX_PIE_PIECES) {
        $r = ArrayHelper::getValue($revenue, $p['id'], 0);
        $n = ArrayHelper::getValue($netIncomeLoss, $p['id'], 0);
        $otherAvgTotal2['revenue'] += $r;
        $otherAvgTotal2['profit'] += $n;
    }
    $allAvgTotal2['revenue'] += $r;
    $allAvgTotal2['profit'] += $n;
}

// todo: create helper
class HelpPie {

    static function getSeries($pieData, $colors, &$colorByItem, &$total, $calcAvg = false, $allAvgTotal2 = [], $otherAvgTotal2 = [])
    {
        $MAX_PIE_PIECES = 9;
        $data2 = [];
        $total2 = 0;
        $piece = 0;
        foreach ($pieData as $k => $v) {

            $amount = $v['amount'];

            if ($calcAvg) {
                // avg
                $total2 = 100 * $allAvgTotal2['profit'] / ($allAvgTotal2['revenue'] ?: 9E99);
            } else {
                // sum
                $total2 += $amount;
            }

            if (++$piece > $MAX_PIE_PIECES) {
                $data2[$MAX_PIE_PIECES - 1]['name'] = 'Другие';
                $data2[$MAX_PIE_PIECES - 1]['color'] = $colors[count($colors) - 1];
                if ($calcAvg) {
                    // precalculated percents avg
                    $data2[$MAX_PIE_PIECES - 1]['y'] = 100 * $otherAvgTotal2['profit'] / ($otherAvgTotal2['revenue'] ?: 9E99);
                } else {
                    // sum
                    $data2[$MAX_PIE_PIECES - 1]['y'] += $amount;
                }
                continue;
            }

            if (!isset($colorByItem[$v['id']]))
                $colorByItem[$v['id']] = $colors[$piece - 1] ?? $colors[count($colors) - 1];

            $data2[$piece - 1] = [
                'name' => $v['name'],
                'y' => $amount,
                'color' => $colorByItem[$v['id']]
            ];
        }

        $total = $total2;

        return [
            'name' => '_not_used',
            'colorByPoint' => true,
            'data' => $data2
        ];
    }
}

$seriesPieRevenue = HelpPie::getSeries($pieDataRevenue, $colors, $colorByItem, $total2Revenue);
$seriesPieProfit = HelpPie::getSeries($pieDataProfit, $colors, $colorByItem, $total2Profit);
$seriesPieIncome = HelpPie::getSeries($pieDataIncome, $colors, $colorByItem, $total2Income);
$seriesPieExpense = HelpPie::getSeries($pieDataExpense, $colors, $colorByItem, $total2Expense);
$seriesPieRentability = HelpPie::getSeries($pieDataRentability, $colors, $colorByItem, $total2Rentability, true, $allAvgTotal2, $otherAvgTotal2);

$jsItems = [];
foreach ($items as $itemId => $itemName)
    $jsItems["project_{$itemId}"] = $itemName; // prevent js auto-sort array
?>

<script>
    if (window.chart_21) {
        chart_21.currDayPos = <?= $currDayPos ?>;
        chart_21.labelsX = <?= json_encode($labelsX) ?>;
        chart_21.dataRevenue = <?= json_encode(['series' => $seriesLineRevenue, 'xAxis' => ['categories' => $categories]]) ?>;
        chart_21.dataProfit = <?= json_encode(['series' => $seriesLineProfit, 'xAxis' => ['categories' => $categories]]) ?>;
        chart_21.dataIncome = <?= json_encode(['series' => $seriesLineIncome, 'xAxis' => ['categories' => $categories]]) ?>;
        chart_21.dataExpense = <?= json_encode(['series' => $seriesLineExpense, 'xAxis' => ['categories' => $categories]]) ?>;
        chart_21.dataRentability = <?= json_encode(['series' => $seriesLineRentability, 'xAxis' => ['categories' => $categories]]) ?>;
    }
</script>

<?php if (isset($isAjax)) { return; } ?>

<!--INCOME-->
<div class="row m-0">
    <div class="col-sm-12 col-md-8 pr-2 pt-2 pl-2 pb-0">
        <?= $this->render('_chart_project_compare_line', [
            'id' => 'chart_21_income',
            'title' => 'ПРИХОД',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesLineIncome,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_INCOME,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-0" style="overflow: hidden">
        <?= $this->render('_chart_project_compare_pie', [
            'id' => 'chart_22_income',
            'title' => 'Приход',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $seriesPieIncome,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_INCOME,
            'total' => $total2Income,
            'height' => $pieChartHeight,
        ]) ?>
    </div>
</div>
<!-- EXPENSE -->
<div class="row m-0">
    <div class="col-sm-12 col-md-8 pr-2 pt-2 pl-2 pb-0">
        <?= $this->render('_chart_project_compare_line', [
            'id' => 'chart_21_expense',
            'title' => 'РАСХОД',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesLineExpense,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_EXPENSE,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-0" style="overflow: hidden">
        <?= $this->render('_chart_project_compare_pie', [
            'id' => 'chart_22_expense',
            'title' => 'Расход',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $seriesPieExpense,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_EXPENSE,
            'total' => $total2Expense,
            'height' => $pieChartHeight,
        ]) ?>
    </div>
</div>
<!-- REVENUE -->
<div class="row m-0">
    <div class="col-sm-12 col-md-8 pr-2 pt-2 pl-2 pb-0">
        <?= $this->render('_chart_project_compare_line', [
            'id' => 'chart_21_revenue',
            'title' => 'ВЫРУЧКА',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesLineRevenue,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_REVENUE,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
            'showLegend' => true
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-0" style="overflow: hidden">
        <?= $this->render('_chart_project_compare_pie', [
            'id' => 'chart_22_revenue',
            'title' => 'Выручка',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $seriesPieRevenue,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_REVENUE,
            'total' => $total2Revenue,
            'height' => $pieChartHeight,
        ]) ?>
    </div>
</div>
<!-- PROFIT -->
<div class="row m-0">
    <div class="col-sm-12 col-md-8 p-2">
        <?= $this->render('_chart_project_compare_line', [
            'id' => 'chart_21_profit',
            'title' => 'ПРИБЫЛЬ',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesLineProfit,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_PROFIT,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-2" style="overflow: hidden">
        <?= $this->render('_chart_project_compare_pie', [
            'id' => 'chart_22_profit',
            'title' => 'Прибыль',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $seriesPieProfit,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_PROFIT,
            'total' => $total2Profit,
            'height' => $pieChartHeight,
        ]) ?>
    </div>
</div>
<!-- RENTABILITY -->
<div class="row m-0 pb-2">
    <div class="col-sm-12 col-md-8 pr-2 pt-2 pl-2 pb-0">
        <?= $this->render('_chart_project_compare_line', [
            'id' => 'chart_21_rentability',
            'title' => 'РЕНТАБЕЛЬНОСТЬ',
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $seriesLineRentability,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_PROFITABILITY,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
            'showLegend' => true
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-0" style="overflow: hidden">
        <?= $this->render('_chart_project_compare_pie', [
            'id' => 'chart_22_rentability',
            'title' => 'Рентабельность',
            'company' => $company,
            'palModel' => $palModel,
            'series' => $seriesPieRentability,
            'onPage' => $onPage,
            'customChartType' => DetailingUserConfig::CHART_TYPE_PROFITABILITY,
            'total' => $total2Rentability,
            'height' => $pieChartHeight,
            'hidePartPercent' => true,
        ]) ?>
    </div>
</div>

<div id="chart-transmitter"><!-- js update charts data --></div>

<script>
    window.chart_21 = {
        dataRevenue: {},
        dataProfit: {},
        dataIncome: {},
        dataExpense: {},
        dataRentability: {},
        _pointStoreRevenue: [],
        _pointStoreRentability: [],
        labelsX: <?= json_encode($labelsX) ?>,
        freeDays: [],
        currDayPos: <?= $currDayPos ?>,
        offset: 0,
        period: 'months',
        chartType: '<?= $customChartType ?>',
        onPage: '<?= $onPage ?>',
        _inProcess: false,
        init: function() {
            this.bindEvents();
            Highcharts.Series.prototype.drawPoints = function () { };
        },
        bindEvents: function() {
            const that = this;

            // move chart
            $(document).on("click", ".chart-offset-2", function () {
                const offset = $(this).data('offset');

                if (that._inProcess) {
                    return false;
                }

                that.offset += offset;
                that.refresh();
            });

            // show series checkboxes
            $('.user_only_project_all').on('change', function() {
                let title;
                if ($(this).prop('checked')) {
                    $('.user_only_project:checked').prop('checked', false).uniform('refresh');
                    title = 'все';
                } else {
                    $('.user_only_project').prop('checked', true).uniform('refresh');
                    title = $('.user_only_project:checked').length + ' шт.';
                }

                $(this).closest('.dropdown').find('.filter-title').html(title);
                $('.user_only_project_apply').removeClass('button-hover-grey').addClass('button-regular_red');
                //that.updateSeriesFilter();
            });

            $('.user_only_project').on('change', function() {
                let title;
                if ($('.user_only_project:checked').length === 0) {
                    $('.user_only_project_all').prop('checked', true).uniform('refresh');
                    title = 'все';
                } else {
                    $('.user_only_project_all').prop('checked', false).uniform('refresh');
                    title = $('.user_only_project:checked').length + ' шт.';
                }

                $(this).closest('.dropdown').find('.filter-title').html(title);
                $('.user_only_project_apply').removeClass('button-hover-grey').addClass('button-regular_red');
                //that.updateSeriesFilter();
            });

            $('.user_only_project_apply').on('click', function() {
                that.updateSeriesFilter();
            });

            // multi click series
            const charts = [
                'income',
                'expense',
                'revenue',
                'profit',
                'rentability'
            ];

            for (let i in charts) {
                let chart21 = '#chart_21_' + charts[i];
                let chart22 = '#chart_22_' + charts[i];
                let isClickableChart21 = (charts[i] === 'revenue' || charts[i] === 'rentability');
                let clickableElements = (isClickableChart21 ? chart21 : chart22) + ' .highcharts-legend-item';
                if ($(chart21).length && $(chart22).length) {
                    // bind legend click
                    $(document).on('click', clickableElements, function(e) {
                        const index = $(this).index();
                        const visible = !$(this).hasClass('highcharts-legend-item-hidden');

                        if (isClickableChart21) {
                            let point = $(chart22).highcharts().series[0].data[index];
                            if (charts[i] === 'revenue') {
                                if (!visible) {
                                    $(chart22).highcharts().series[0].data[index].update({'y': that._pointStoreRevenue[index] ?? 9E9});
                                } else {
                                    that._pointStoreRevenue[index] = point.y;
                                    $(chart22).highcharts().series[0].data[index].update({'y': 0});
                                }
                            }
                            if (charts[i] === 'rentability') {
                                if (!visible) {
                                    $(chart22).highcharts().series[0].data[index].update({'y': that._pointStoreRentability[index] ?? 9E9});
                                } else {
                                    that._pointStoreRentability[index] = point.y;
                                    $(chart22).highcharts().series[0].data[index].update({'y': 0});
                                }
                            }
                        } else {
                            $(chart21).highcharts().series[index].update({visible: visible});
                        }
                    });
                }
            }
        },
        updateSeriesFilter: function() {
            const that = this;
            let checked = [];
            $('.user_only_project_all, .user_only_project').each(function() {
                if ($(this).prop('checked'))
                    checked.push($(this).val());
            });

            $.post('/analytics/detailing-ajax/set-user-chart-series-filter', {
                    "on-page": that.onPage || '',
                    "checked": checked || ['all']
                }, function(data) {
                    if (data.success)
                        location.reload();
                }
            );
        },
        refresh: function() {
            const that = this;
            that._inProcess = true;
            that._getData().done(function() {
                that._repaint();
                that._inProcess = false;
            });
        },
        _getData: function() {
            const that = this;
            return $.post('/analytics/finance-ajax/get-project-compare-data', {
                    "chart-project-compare-ajax": 1,
                    "period": that.period || 'months',
                    "offset": that.offset || 0,
                    "chart-type": that.chartType || 1,
                    "on-page": that.onPage || '',
                    "projects": <?= json_encode($jsItems) ?>
                },
                function(data) {
                    $('#chart-transmitter').append(data).html(null);
                }
            );
        },
        _repaint: function() {
            const that = this;
            $('#chart_21_revenue').highcharts().update(that.dataRevenue);
            $('#chart_21_profit').highcharts().update(that.dataProfit);
            $('#chart_21_income').highcharts().update(that.dataIncome);
            $('#chart_21_expense').highcharts().update(that.dataExpense);
            $('#chart_21_rentability').highcharts().update(that.dataRentability);
        },
    };

    window.chart_22 = {
        chartType: '<?= $customChartType ?>',
        periodText: '<?= $periodText ?>',
        data: {},
        onPage: '<?= $onPage ?>',
        //_repaint: function() {
        //    const that = this;
        //    const chartTop = $('#chart_22_top');
        //    const symbol = String(that.chartType) === "5" ? ' %' : ' ₽';
        //    chartTop.highcharts().update(that.dataRevenue);
        //    chartTop.find('.pie-title').html(that.title);
        //    chartTop.find('.pie-total').html(number_format(that.total, 0, ',', ' ') + symbol);
        //    chartTop.highcharts().series[0].update({
        //        type: 'pie'
        //    });
        //},
    };

    ///////////////////////////////
    $(document).ready(function() {
        window.chart_21.init();
    });
    ///////////////////////////////

</script>