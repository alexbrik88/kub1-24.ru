<?php

/* @var $this yii\web\View
 * @var $model \frontend\modules\analytics\models\operationalEfficiency\OperationalEfficiencySearchModel
 * @var $customMonth string|null
 * @var $customOffset int|null
 */

use common\components\helpers\Month;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customOffset = $customOffset ?? 0;
$MOVE_OFFSET = 1;
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$color1 = 'rgba(57,194,176,1)';
$color2 = 'rgba(46,159,191,1)';
$color3 = 'rgba(183,183,0,1)';
$color4 = 'rgba(0,255,255,1)';
$color5 = 'rgba(148,0,211,1)';
$colorNegative = 'red';
$datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
$chartPeriodsX = [];
$chartFreeDays = [];
$yearPrev = $datePeriods[0];
$chart = [];

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $year  = (int)$dateArr[0];
    $month = (int)$dateArr[1];
    $str_month =  $dateArr[1];
    $day   = (int)$dateArr[2];
    if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
        $wrapPointPos[$i] = [
            'prev' => $year - 1,
            'next' => $year
        ];
        // emulate align right
        switch ($year - 1) {
            case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
            default:
                $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
        }
    }

    $yearPrev = $year;
    $chartPeriodsX[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
    $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
    if ($month == (int)date('m') && $year == (int)date('Y')) {
        $currDayPos = $i;
    }

    if ("{$year}{$str_month}" > date('Ym')) {
        $chart['rom'][] = null;
        $chart['npn'][] = null;
        $chart['roa'][] = null;
        $chart['roe'][] = null;
        $chart['rota'][] = null;
    } else {
        $chart['rom'][] = round($model->getValue($year, $str_month, 2, 0) / 100, 2);
        $chart['npn'][] = round($model->getValue($year, $str_month, 2, 4) / 100, 2);
        $chart['roa'][] = round($model->getValue($year, $str_month, 3, 1) / 100, 2);
        $chart['roe'][] = round($model->getValue($year, $str_month, 3, 2) / 100, 2);
        $chart['rota'][] = round($model->getValue($year, $str_month, 3, 3) / 100, 2);
    }
}
?>
<?php if (Yii::$app->request->post('chart-operational-efficiency-ajax')) {
    echo json_encode([
        'revenue' => [
            'currDayPos' => $currDayPos,
            'wrapPointPos' => $wrapPointPos,
            'freeDays' => $chartFreeDays,
            'labelsX' => $chartLabelsX,
            'optionsChart' => [
                'xAxis' => ['categories' => $chartPeriodsX],
                'series' => [
                    ['data' => $chart['rom']],
                    ['data' => $chart['npn']],
                    ['data' => $chart['roa']],
                    ['data' => $chart['roe']],
                    ['data' => $chart['rota']],
                ],
            ],
        ],
    ]);

    exit;
} ?>
<style>
    #chart-operational-efficiency { height: 235px; }
</style>
<div class="row mb-4 pb-3">
    <div class="col-12">
        <div style="position: relative">
            <div style="width: 100%;">
                <div class="chart-operational-efficiency-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
                    <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
                </div>
                <div class="chart-operational-efficiency-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
                    <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
                </div>
                <div class="ht-caption noselect" style="margin-bottom: 16px">
                    ДИНАМИКА ПОКАЗАТЕЛЕЙ ЭФФЕКТИВНОСТИ
                    <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                        <?= Select2::widget([
                            'id' => 'chart-profit-loss-revenue-type',
                            'name' => 'purseType',
                            'data' => [
                                'amount' => 'Все показатели',
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'style' => 'display: inline-block;',
                            ],
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '176px'
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="finance-charts-group" style="min-height:125px">
                    <?= Highcharts::widget([
                        'id' => 'chart-operational-efficiency',
                        'class' => 'finance-charts',
                        'scripts' => [
                            'themes/grid-light',
                            'modules/pattern-fill',
                        ],
                        'options' => [
                            'credits' => [
                                'enabled' => false,
                            ],
                            'chart' => [
                                'type' => 'column',
                                'spacing' => [0,0,0,0],
                                'marginBottom' => '50',
                                'marginLeft' => '55',
                                'style' => [
                                    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                                ],
                            ],
                            'legend' => [
                                'layout' => 'horizontal',
                                'align' => 'right',
                                'verticalAlign' => 'top',
                                'backgroundColor' => '#fff',
                                'itemStyle' => [
                                    'fontSize' => '11px',
                                    'color' => '#9198a0'
                                ],
                                'symbolRadius' => 2,
                            ],
                            'tooltip' => [
                                'useHTML' => true,
                                'shared' => false,
                                'backgroundColor' => "rgba(255,255,255,1)",
                                'borderColor' => '#ddd',
                                'borderWidth' => '1',
                                'borderRadius' => 8,
                                'formatter' => new jsExpression("
                                    function(args) {
                                        var index = this.series.data.indexOf( this.point );
                                        var series_index = this.series.index;
        
                                        return '<span class=\"title\">' + window.ChartsOperationalEfficiency.Revenue.labelsX[this.point.index] + '</span>' +
                                            '<table class=\"indicators\">' +
                                                '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' %</td></tr>' +
                                                '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' %</td></tr>' +
                                                '<tr>' + '<td class=\"gray-text\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[2].data[index].y, 2, ',', ' ') + ' %</td></tr>' +
                                                '<tr>' + '<td class=\"gray-text\">' + args.chart.series[3].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[3].data[index].y, 2, ',', ' ') + ' %</td></tr>' +
                                                '<tr>' + '<td class=\"gray-text\">' + args.chart.series[4].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[4].data[index].y, 2, ',', ' ') + ' %</td></tr>' +
                                            '</table>';
                                    }
                                "),
                            ],
                            'lang' => [
                                'printChart' => 'На печать',
                                'downloadPNG' => 'Скачать PNG',
                                'downloadJPEG' => 'Скачать JPEG',
                                'downloadPDF' => 'Скачать PDF',
                                'downloadSVG' => 'Скачать SVG',
                                'contextButtonTitle' => 'Меню',
                            ],
                            'title' => ['text' => ''],
                            'yAxis' => [
                                'title' => '',
                                'minorGridLineWidth' => 0,
                                'labels' => [
                                    'useHTML' => true,
                                    'style' => [
                                        'fontWeight' => '300',
                                        'fontSize' => '13px',
                                        'whiteSpace' => 'nowrap'
                                    ],
                                    'formatter' => new JsExpression("function() {
                                        const formatCash = function(n) {
                                            if (Math.abs(n) < 1e3) return n;
                                            if (Math.abs(n) >= 1e3 && Math.abs(n) < 1e6) return +(n / 1e3).toFixed(1) + \"k\";
                                            if (Math.abs(n) >= 1e6) return +(n / 1e6).toFixed(1) + \"M\";
                                        };

                                        return formatCash(this.value);
                                    }"),
                                ],
                            ],
                            'xAxis' => [
                                [
                                    'lineColor' => '#9198a0',
                                    'min' => 1,
                                    'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                                    'categories' => $chartPeriodsX,
                                    'labels' => [
                                        'formatter' => new JsExpression("
                                            function() {
                                                var result = (this.pos == window.ChartsOperationalEfficiency.Revenue.currDayPos) ?
                                                    ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                                    (window.ChartsOperationalEfficiency.Revenue.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
        
                                                if (window.ChartsOperationalEfficiency.Revenue.wrapPointPos) {
                                                    result += window.ChartsOperationalEfficiency.Revenue.getWrapPointXLabel(this.pos);
                                                }
        
                                                return result;
                                            }"),
                                        'useHTML' => true,
                                        'autoRotation' => false,
                                    ],
                                ],
                            ],
                            'series' => [
                                [
                                    'name' => 'ROM',
                                    'type' => 'line',
                                    'data' => $chart['rom'],
                                    'color' => $color1,
                                    'negativeColor' => $colorNegative,
                                    'states' => [
                                        'hover' => [
                                            'color' => [
                                                'pattern' => [
                                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                                    'color' => $color1,
                                                    'width' => 5,
                                                    'height' => 5
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'name' => 'NPN',
                                    'type' => 'line',
                                    'data' => $chart['npn'],
                                    'color' => $color2,
                                    'negativeColor' => $colorNegative,
                                    'states' => [
                                        'hover' => [
                                            'color' => [
                                                'pattern' => [
                                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                                    'color' => $color2,
                                                    'width' => 5,
                                                    'height' => 5
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'name' => 'ROA',
                                    'type' => 'line',
                                    'data' => $chart['roa'],
                                    'color' => $color3,
                                    'negativeColor' => $colorNegative,
                                    'states' => [
                                        'hover' => [
                                            'color' => [
                                                'pattern' => [
                                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                                    'color' => $color3,
                                                    'width' => 5,
                                                    'height' => 5
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'name' => 'ROE',
                                    'type' => 'line',
                                    'data' => $chart['roe'],
                                    'color' => $color4,
                                    'negativeColor' => $colorNegative,
                                    'states' => [
                                        'hover' => [
                                            'color' => [
                                                'pattern' => [
                                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                                    'color' => $color4,
                                                    'width' => 5,
                                                    'height' => 5
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'name' => 'ROTA',
                                    'type' => 'line',
                                    'data' => $chart['rota'],
                                    'color' => $color5,
                                    'negativeColor' => $colorNegative,
                                    'states' => [
                                        'hover' => [
                                            'color' => [
                                                'pattern' => [
                                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                                    'color' => $color5,
                                                    'width' => 5,
                                                    'height' => 5
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                            'plotOptions' => [
                                'series' => [
                                    'pointWidth' => 20,
                                    'tooltip' => [
                                        'crosshairs' => true,
                                        'headerFormat' => '{point.x}',
                                        'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                                    ],
                                    'states' => [
                                        'inactive' => [
                                            'opacity' => 1
                                        ],
                                    ],
                                    'borderRadius' => 3,
                                    'borderWidth' => 1
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    ChartsOperationalEfficiency = {
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: 'months',
        month: "<?= $customMonth ?>",
        byAmountKey: "amount",
        Revenue: {
            offset: {
                days: 0,
                months: 0
            },
            currDayPos: <?= (int)$currDayPos; ?>,
            wrapPointPos: <?= json_encode($wrapPointPos) ?>,
            labelsX: <?= json_encode($chartLabelsX) ?>,
            freeDays: <?= json_encode($chartFreeDays) ?>,
            chartPoints: {},
            getWrapPointXLabel: function(x) { return ChartsOperationalEfficiency._getWrapPointXLabel(x); }
        },
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-operational-efficiency-arrow').on('click', function() {
                if (ChartsOperationalEfficiency._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    ChartsOperationalEfficiency.Revenue.offset[ChartsOperationalEfficiency.period] -= <?= $MOVE_OFFSET ?>;
                }

                if ($(this).data('move') === 'right') {
                    ChartsOperationalEfficiency.Revenue.offset[ChartsOperationalEfficiency.period] += <?= $MOVE_OFFSET ?>;
                }

                ChartsOperationalEfficiency.redrawByClick();
            });
        },
        redrawByClick: function() {
            return ChartsOperationalEfficiency._getData().done(function() {
                var revenue = $('#chart-operational-efficiency').highcharts();

                revenue.update(ChartsOperationalEfficiency.Revenue.chartPoints);
                ChartsOperationalEfficiency._inProcess = false;
            });
        },
        _getData: function() {
            ChartsOperationalEfficiency._inProcess = true;
            return $.post('/analytics/finance-ajax/get-operational-efficiency-charts-data', {
                    "chart-operational-efficiency-ajax": true,
                    "year": ChartsOperationalEfficiency.year,
                    "month": ChartsOperationalEfficiency.month,
                    "offset": ChartsOperationalEfficiency.Revenue.offset[ChartsOperationalEfficiency.period],
                },
                function(data) {
                    data = JSON.parse(data);
                    const revenue = data.revenue;

                    ChartsOperationalEfficiency.Revenue.freeDays = revenue.freeDays;
                    ChartsOperationalEfficiency.Revenue.currDayPos = revenue.currDayPos;
                    ChartsOperationalEfficiency.Revenue.labelsX = revenue.labelsX;
                    ChartsOperationalEfficiency.Revenue.wrapPointPos = revenue.wrapPointPos;
                    ChartsOperationalEfficiency.Revenue.chartPoints = revenue.optionsChart;
                }
            );
        },
        _getWrapPointXLabel: function(x, chartId) {
            var chart = $('#chart-operational-efficiency').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;
            var k = (ChartsOperationalEfficiency.period === 'months') ? 0.705 : 0.625;
            if (ChartsOperationalEfficiency.Revenue.wrapPointPos[x + 1]) {
                name = ChartsOperationalEfficiency.Revenue.wrapPointPos[x + 1].prev;
                left = ChartsOperationalEfficiency.Revenue.wrapPointPos[x + 1].leftMargin;
                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }

            if (ChartsOperationalEfficiency.Revenue.wrapPointPos[x]) {
                name = ChartsOperationalEfficiency.Revenue.wrapPointPos[x].next;
                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ChartsOperationalEfficiency.init();
</script>
