<?php
/** @var $model \frontend\modules\analytics\models\IncomeSearch */
//////////////////////////////////// TOP CHARTS /////////////////////

// consts top charts
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 20;
$cropNamesLength = 12;
$croppedNameLength = 10;

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customPurse = $customPurse ?? null;
$customArticle = $customArticle ?? null;
$customClient = $customClient ?? null;
$customProduct = $customProduct ?? null;
$customEmployee = $customEmployee ?? null;
/////////////////////////////////////////

if (!isset($dataBuyers) || !isset($dataProducts)) {
    $dataBuyers = $model->getChartStructureByBuyers($customMonth, $customPurse, $customArticle, $customProduct, $customEmployee);
    $dataProducts = $model->getTopProductSeriesData($customMonth, $customPurse, $customArticle, $customClient, $customProduct, $customEmployee);
}

// arrays top charts
$topChart1 = [
    'categories' => array_slice(array_column($dataBuyers, 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataBuyers, 'amountFact'), 0, $chartMaxRows),
    'planData' => array_slice(array_column($dataBuyers, 'amountPlan'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataBuyers, 'amountFact')),
    'clientsIds' => array_slice(array_column($dataBuyers, 'id'), 0, $chartMaxRows)
];

$topChart2 = [
    'categories' => array_slice(array_column($dataProducts['topProducts'], 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataProducts['topProducts'], 'amount'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataProducts['topProducts'], 'amount')),
    'productsIds' => array_keys($dataProducts['topProducts']),
];

$topChart3 = [
    'categories' => array_slice(array_column($dataProducts['topEmployers'], 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataProducts['topEmployers'], 'amount'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataProducts['topEmployers'], 'amount')),
    'employersIds' => array_keys($dataProducts['topEmployers']),
];

if (empty($topChart1['categories'])) {
    $topChart1 = [
        'categories' => ["Нет данных"],
        'factData' => [0],
        'planData' => [0],
        'totalAmount' => 0,
        'clientsIds' => []
    ];
}
if (empty($topChart2['categories'])) {
    $topChart2 = [
        'categories' => ["Нет данных"],
        'factData' => [0],
        'totalAmount' => 0,
        'productsIds' => []
    ];
}
if (empty($topChart3['categories'])) {
    $topChart3 = [
        'categories' => ["Нет данных"],
        'factData' => [0],
        'totalAmount' => 0,
        'employersIds' => []
    ];
}
$topChart1['calculatedChartHeight'] = count($topChart1['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$topChart2['calculatedChartHeight'] = count($topChart2['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$topChart3['calculatedChartHeight'] = count($topChart3['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
///////////////////////////////////////////////////////////////////////////

if (Yii::$app->request->post('chart-income-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'top1' => [
            'chartHeight' => $topChart1['calculatedChartHeight'],
            'totalAmount' => $topChart1['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart1['categories']],
                'series' => [
                    ['data' => $topChart1['planData']],
                    ['data' => $topChart1['factData']],
                ],
            ],
            'clientsIds' => $topChart1['clientsIds']
        ],
        'top2' => [
            'chartHeight' => $topChart2['calculatedChartHeight'],
            'totalAmount' => $topChart2['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart2['categories']],
                'series' => [['data' => $topChart2['factData']]],
            ],
            'productsIds' => $topChart2['productsIds']
        ],
        'top3' => [
            'chartHeight' => $topChart3['calculatedChartHeight'],
            'totalAmount' => $topChart3['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart3['categories']],
                'series' => [['data' => $topChart3['factData']]],
            ],
            'employersIds' => $topChart3['employersIds']
        ],
    ]);

    exit;
}

$topChartConsts = [
    'chartWrapperHeight' => $chartWrapperHeight,
    'chartHeightHeader' => $chartHeightHeader,
    'chartHeightFooter' => $chartHeightFooter,
    'chartHeightColumn' => $chartHeightColumn,
    'chartMaxRows' => $chartMaxRows,
    'cropNamesLength' => $cropNamesLength,
    'croppedNameLength' => $croppedNameLength
];
?>

<?php if (isset($row) && $row == 1): ?>

    <?=
    // Row 1: show only employers chart
    $this->render('_chart_income_top_3', array_merge($topChartConsts, [
        'searchModel' => $model,
        'categories' => $topChart3['categories'],
        'factData' => $topChart3['factData'],
        'calculatedChartHeight' => $topChart3['calculatedChartHeight'],
    ])); ?>

<?php else: ?>

    <?php // Row 2: show full charts line ?>
    <div class="pt-4 pb-3" style="margin-top: 0px;">
        <div class="row">
            <div class="col-4 pr-2">
                <?= $this->render('_chart_income_top_1', array_merge($topChartConsts, [
                    'searchModel' => $model,
                    'categories' => $topChart1['categories'],
                    'planData' => $topChart1['planData'],
                    'factData' => $topChart1['factData'],
                    'calculatedChartHeight' => $topChart1['calculatedChartHeight'],
                ])); ?>
            </div>
            <div class="col-4 pl-2 pr-2">
                <?= $this->render('_chart_income_top_2', array_merge($topChartConsts, [
                    'searchModel' => $model,
                    'categories' => $topChart2['categories'],
                    'factData' => $topChart2['factData'],
                    'calculatedChartHeight' => $topChart2['calculatedChartHeight'],
                ])); ?>
            </div>
            <div class="col-4 pl-2">
                <?= $this->render('_chart_income_projects', [
                    'model' => $model,
                ]);
                ?>
            </div>
        </div>
    </div>

<?php endif; ?>

<script>
    // MOVE CHART
    window.ChartIncomeTop = {
        chart: 'top',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        clientsIds: <?= json_encode($topChart1['clientsIds']) ?>,
        productsIds: <?= json_encode($topChart2['productsIds']) ?>,
        employersIds: <?= json_encode($topChart3['employersIds']) ?>,
        _inProcess: false,
        Top1: {
            chartHeight: "<?= $topChart1['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart1['totalAmount'] ?>",
            chartPoints: {},
            setPoint:    function(point) { return ChartIncomeTop._setPoint(point, 'rgba(57,194,176,1)'); },
            resetPoints: function(point) { return ChartIncomeTop._resetPoints(point, 'rgba(57,194,176,1)'); }
        },
        Top2: {
            chartHeight: "<?= $topChart2['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart2['totalAmount'] ?>",
            chartPoints: {},
            setPoint:    function(point) { return ChartIncomeTop._setPoint(point, 'rgba(103,131,228,1)'); },
            resetPoints: function(point) { return ChartIncomeTop._resetPoints(point, 'rgba(103,131,228,1)'); }
        },
        Top3: {
            chartHeight: "<?= $topChart3['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart3['totalAmount'] ?>",
            chartPoints: {},
            setPoint:    function(point) { return ChartIncomeTop._setPoint(point, 'rgba(243,183,46,1)'); },
            resetPoints: function(point) { return ChartIncomeTop._resetPoints(point, 'rgba(243,183,46,1)'); }
        },
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

        },
        redrawByClick: function(exceptChartNum) {

            return window.ChartIncomeTop._getData(exceptChartNum).done(function() {
                $('#chart-top-1').highcharts().update(ChartIncomeTop.Top1.chartPoints);
                $('#chart-top-1').highcharts().update({"chart": {"height": ChartIncomeTop.Top1.chartHeight + 'px'}});
                $('#chart-top-2').highcharts().update(ChartIncomeTop.Top2.chartPoints);
                $('#chart-top-2').highcharts().update({"chart": {"height": ChartIncomeTop.Top2.chartHeight + 'px'}});
                $('#chart-top-3').highcharts().update(ChartIncomeTop.Top3.chartPoints);
                $('#chart-top-3').highcharts().update({"chart": {"height": ChartIncomeTop.Top3.chartHeight + 'px'}});
                window.ChartIncomeTop._inProcess = false;
            });
        },
        _getData: function(exceptChartNum) {
            window.ChartIncomeTop._inProcess = true;
            return $.post('/analytics/finance-ajax/get-income-charts-data', {
                    "chart-income-ajax": true,
                    "chart": ChartIncomeTop.chart,
                    "year": ChartIncomeTop.year,
                    "month": ChartIncomeTop.month,
                    "purse":    ChartFilter.byPurse,
                    "article":  ChartFilter.byArticle,
                    "client":   ChartFilter.byClient,
                    "product":  ChartFilter.byProduct,
                    "employee": ChartFilter.byEmployee,
                },
                function(data) {
                    data = JSON.parse(data);
                    if (exceptChartNum !== 1) {
                        ChartIncomeTop.Top1.chartPoints = data.top1.optionsChart;
                        ChartIncomeTop.Top1.chartHeight = data.top1.chartHeight;
                        ChartIncomeTop.Top1.totalAmount = data.top1.totalAmount;
                        ChartIncomeTop.clientsIds  = data.top1.clientsIds;
                    }
                    if (exceptChartNum !== 2) {
                        ChartIncomeTop.Top2.chartPoints = data.top2.optionsChart;
                        ChartIncomeTop.Top2.chartHeight = data.top2.chartHeight;
                        ChartIncomeTop.Top2.totalAmount = data.top2.totalAmount;
                        ChartIncomeTop.productsIds  = data.top2.productsIds;
                    }
                    if (exceptChartNum !== 3) {
                        ChartIncomeTop.Top3.chartPoints = data.top3.optionsChart;
                        ChartIncomeTop.Top3.chartHeight = data.top3.chartHeight;
                        ChartIncomeTop.Top3.totalAmount = data.top3.totalAmount;
                        ChartIncomeTop.employersIds  = data.top3.employersIds;
                    }
                }
            );
        },
        _setPoint: function(point, color) {
            this._resetPoints(point, color);
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': color,
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        _resetPoints: function(point, color) {

            const planColor = 'rgba(226,229,234,1)';
            let series = point.series.chart.series;

            if (series.length === 2) {
                series[1].data.forEach(function (p) {
                    p.update({
                        color: color
                    });
                });
                series[0].data.forEach(function (p) {
                    p.update({
                        color: planColor
                    });
                });
            } else {
                series[0].data.forEach(function (p) {
                    p.update({
                        color: color
                    });
                });
            }
        },
    };

    /////////////////////////////
    window.ChartIncomeTop.init();
    /////////////////////////////

</script>