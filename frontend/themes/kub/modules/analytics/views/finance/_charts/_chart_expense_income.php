<?php

use frontend\modules\analytics\models\ExpensesSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $model ExpensesSearch */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = "months";
$customPurse = $customPurse ?? null;
$customArticle = $customArticle ?? null;
$customClient = $customClient ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(70,189,170,1)';
$color1_opacity = 'rgba(70,189,170,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$mainData = $model->getFactSeriesData($datePeriods, $customPurse, $customPeriod, $customArticle, $customClient);
$incomeFlowsFactPlan = &$mainData['incomeFlowsFact'];
$outcomeFlowsFactPlan = &$mainData['outcomeFlowsFact'];

$resultFlowsRatio = [];
for ($i = 0; $i < count($datePeriods); $i++) {

    $dateArr = explode('-', $datePeriods[$i]['from']);
    $year  = $dateArr[0];
    $month = $dateArr[1];

    if ($year.$month <= date('Ym')) {
        $resultFlowsRatio[$i] = ($incomeFlowsFactPlan[$i] > 0) ?
            round($outcomeFlowsFactPlan[$i] / $incomeFlowsFactPlan[$i] * 100) : 0;
    }
}

if (Yii::$app->request->post('chart-expense-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $resultFlowsRatio,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-expense-income { height: 233px; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="chart-expense-income-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-expense-income-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect mb-2">
            Доля расходов в приходе
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-expense-income',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'line',
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + window.ChartExpensesIncome.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' +
                                            ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 0, ',', ' ') + ' %</td></tr>') +
                                        '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartExpensesIncome.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartExpensesIncome.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartExpensesIncome.wrapPointPos) {
                                            result += window.ChartExpensesIncome.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => '% расходов в приходе',
                            'data' => $resultFlowsRatio,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    window.ChartExpensesIncome = {
        chart: 'expense-income',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        byPurse: null,
        byArticle: null,
        byClient: null,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-expense-income-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartExpensesIncome._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartExpensesMain.offset[ChartExpensesMain.period] -= <?= $MOVE_OFFSET ?>;
                    window.ChartExpensesIncome.offset[ChartExpensesIncome.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartExpensesMain.offset[ChartExpensesMain.period] += <?= $MOVE_OFFSET ?>;
                    window.ChartExpensesIncome.offset[ChartExpensesIncome.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartExpensesMain.redrawByClick();
                window.ChartExpensesIncome.redrawByClick();
            });
        },
        redrawByClick: function() {

            return window.ChartExpensesIncome._getData().done(function() {
                $('#chart-expense-income').highcharts().update(ChartExpensesIncome.chartPoints);
                window.ChartExpensesIncome._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartExpensesIncome._inProcess = true;
            return $.post('/analytics/finance-ajax/get-expense-charts-data', {
                    "chart-expense-ajax": true,
                    "chart": ChartExpensesIncome.chart,
                    "period": ChartExpensesIncome.period,
                    "offset": window.ChartExpensesIncome.offset[ChartExpensesIncome.period],
                    "purse": ChartExpensesIncome.byPurse,
                    "year": ChartExpensesIncome.year,
                    "article": ChartExpensesIncome.byArticle,
                    "client": ChartExpensesIncome.byClient
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartExpensesIncome.freeDays = data.freeDays;
                    ChartExpensesIncome.currDayPos = data.currDayPos;
                    ChartExpensesIncome.labelsX = data.labelsX;
                    ChartExpensesIncome.wrapPointPos = data.wrapPointPos;
                    ChartExpensesIncome.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-expense-income').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartExpensesIncome.period == 'months') ? 0.705 : 0.625;

            if (window.ChartExpensesIncome.wrapPointPos[x + 1]) {
                name = window.ChartExpensesIncome.wrapPointPos[x + 1].prev;
                left = window.ChartExpensesIncome.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartExpensesIncome.wrapPointPos[x]) {
                name = window.ChartExpensesIncome.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    window.ChartExpensesIncome.init();
</script>