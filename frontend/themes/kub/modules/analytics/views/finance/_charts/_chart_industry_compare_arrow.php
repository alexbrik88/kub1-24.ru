<?php
use frontend\components\Icon;
use yii\helpers\Html;

/** @var string $chartId */
/** @var string $offsetStep */
/** @var string|null $syncChart */

$class = $class ?? 'marketing-chart-offset';

echo Html::a(Icon::get('shevron', ['style' => 'transform: rotate(90deg);']), 'javascript:void(0)', [
    'data-chart' => $chartId,
    'data-offset' => -1 * $offsetStep,
    'data-sync-chart' => $syncChart ?? null,
    'class' => 'link ' . $class,
    'style' => 'position: absolute; left:13px; bottom:15px; z-index: ' . ($zIndex ?? 1)
]);

echo Html::a(Icon::get('shevron', ['style' => 'transform: rotate(270deg);']), 'javascript:void(0)', [
    'data-chart' => $chartId,
    'data-offset' => 1 * $offsetStep,
    'data-sync-chart' => $syncChart ?? null,
    'class' => 'link ' . $class,
    'style' => 'position: absolute; right:11px; bottom:15px; z-index: ' . ($zIndex ?? 1)
]);