<?php

use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use frontend\themes\kub\components\Icon;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;

/** @var $model DebtReportSearch2 */
/** @var $model2 DebtReportSearchAsBalance */
/** @var $allDebtSum int */

$searchBy = $searchBy ?? DebtorHelper2::SEARCH_BY_INVOICES;

///////////////// dynamic vars ///////////
$customPeriod = "months";
$customOffset = $customOffset ?? 0;
$customDebtPeriod = $customDebtPeriod ?? DebtorHelper2::PERIOD_OVERDUE;
$customContractor = $customContractor ?? null;
$customEmployee = $customEmployee ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color = ($type == 2) ? 'rgba(51, 109, 154, 1)' : 'rgba(51, 109, 154, 1)';
$color_opacity = ($type == 2) ? 'rgba(51, 109, 154, .5)' : 'rgba(51, 109, 154, .5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 12;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$fullPeriodFrom = $datePeriods[0]['from'];
$fullPeriodMonthsCount = count($datePeriods);
DebtorHelper2::$SEARCH_BY = $searchBy;
if ($reportType == DebtorHelper2::REPORT_TYPE_MONEY) {
    DebtorHelper2::$REPORT_TYPE = $reportType;
    $data = DebtorHelper2::getDebts($customDebtPeriod, $customContractor, $customEmployee, $fullPeriodFrom, $fullPeriodMonthsCount, $type); // cached in
} else {
    $data = DebtorHelper2::getPrepayments($customDebtPeriod, $customContractor, $customEmployee, $fullPeriodFrom, $fullPeriodMonthsCount, $type); // cached in
}
$dataContractorsCount = &$data['contractors_count'];

if (Yii::$app->request->post('chart-debtor-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataContractorsCount,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-debtor-2 { height: 235px; }
</style>

<div style="position: relative; margin-right:20px;">
    <div style="width: 100%;">

        <div class="chart-debtor-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-debtor-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            КОЛИЧЕСТВО ДОЛЖНИКОВ
        </div>

        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-debtor-2',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;

                                return '<span class=\"title\">' + window.ChartDebtorMain2.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + window.ChartDebtorMain2.getSerieName() + '</td>' + '<td class=\"gray-text-b\"></td></tr>' +
                                         '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 0, ',', ' ') + '</td></tr>') +
                                    '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        [
                            'min' => 0,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ]
                        ],
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartDebtorMain2.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartDebtorMain2.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartDebtorMain2.wrapPointPos) {
                                            result += window.ChartDebtorMain2.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'yAxis' => 0,
                            'name' => 'Количество должников',
                            'data' => $dataContractorsCount,
                            'color' => $color,
                            'borderColor' => $color_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    // MOVE CHART
    window.ChartDebtorMain2 = {
        chart: 'main2',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: <?= $customOffset ?>
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        byDebtorPeriod: <?= $customDebtPeriod ?>,
        byContractor: "<?= $customContractor ?>",
        byEmployee: "<?= $customEmployee ?>",
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

        },
        redrawByClick: function() {

        },
        _getData: function() {

        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-debtor-2').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartDebtorMain2.period == 'months') ? 0.705 : 0.625;

            if (window.ChartDebtorMain2.wrapPointPos[x + 1]) {
                name = window.ChartDebtorMain2.wrapPointPos[x + 1].prev;
                left = window.ChartDebtorMain2.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartDebtorMain2.wrapPointPos[x]) {
                name = window.ChartDebtorMain2.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        getSerieName: function() {
            return $('#chart-debtor-period').find('option:selected').data('chart-title');
        }
    };

    //////////////////////////////
    window.ChartDebtorMain2.init();
    //////////////////////////////

</script>