<?php

use common\components\helpers\Month;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

/** @var $model ProfitAndLossSearchModel */

$PAGE_FILTER = $PAGE_FILTER ?? [
        'industry_id' => null,
        'sale_point_id' => null,
        'project_id' => null,
    ];

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = "months";
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customAmountKey = $customAmountKey ?? 'amount'; // amount||amountTax
$customChartsGroup = $customChartsGroup ?? null;
/////////////////////////////////////////

////// consts dynamics chart ////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
////// consts structures charts ////////
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 6;
$cropNamesLength = 12;
$croppedNameLength = 10;
///////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

$chart1 = [
    'totalRevenue' => [],
    'netIncomeLoss' => []
];
$chart2 = [
    'categories' => [],
    'factData' => [],
    'planData' => []
];
$chart3 = [
    'categories' => [],
    'factData' => [],
];
$chart4 = [
    'categories' => [],
    'series' => [],
    'isExpense' => [],
    'colors' => [
        'income' => 'rgba(46,159,191,1)',
        'expense' => 'rgba(245,183,46,1)',
        'profit' => 'rgba(57,194,176,1)',
        'red' => 'red',
    ]
];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $chartPeriodsX = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $str_month =  $dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $chartPeriodsX[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

// 1. REVENUE

foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $year  = (int)$dateArr[0];
    $str_month =  $dateArr[1];

    if ($year.$str_month > date('Ym')) {
        $chart1['totalRevenue'][] = null;
        $chart1['netIncomeLoss'][] = null;
    } else {
        $chart1['totalRevenue'][] = round($model->getValue('totalRevenue', $year, $str_month, $customAmountKey) / 100, 2);
        $chart1['netIncomeLoss'][] = round($model->getValue('netIncomeLoss', $year, $str_month, $customAmountKey) / 100, 2);
    }
}
// by month charts
$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$revenueVal = round($model->getValue('totalRevenue', $model->year, $customMonth, $customAmountKey) / 100, 2);
$anotherIncomeVal = round($model->getValue('totalAnotherIncome', $model->year, $customMonth, $customAmountKey) / 100, 2);
$receivedPercentVal = round($model->getValue('receivedPercent', $model->year, $customMonth, $customAmountKey) / 100, 2);
$variableCostsVal = round($model->getValue('totalVariableCosts', $model->year, $customMonth, $customAmountKey) / 100, 2);
$fixedCostsVal = round($model->getValue('totalFixedCosts', $model->year, $customMonth, $customAmountKey) / 100, 2);
$operatingCostsVal = round($model->getValue('totalOperatingCosts', $model->year, $customMonth, $customAmountKey) / 100, 2);
$anotherCostsVal = round($model->getValue('totalAnotherCosts', $model->year, $customMonth, $customAmountKey) / 100, 2);
$paymentPercentVal = round($model->getValue('paymentPercent', $model->year, $customMonth, $customAmountKey) / 100, 2);
$taxVal = round($model->getValue('tax', $model->year, $customMonth, $customAmountKey) / 100, 2);
$netIncomeLossVal = round($model->getValue('netIncomeLoss', $model->year, $customMonth, $customAmountKey) / 100, 2);

// 2. CLIENTS
$chart2Data = $model->getChartRevenueByClients($dateFrom, $dateTo, $customAmountKey);
$chart2['categories']  = array_slice(array_column($chart2Data, 'name'), 0, $chartMaxRows);
$chart2['factData']    = array_slice(array_column($chart2Data, 'amountFact'), 0, $chartMaxRows);
$chart2['planData']    = array_slice(array_column($chart2Data, 'amountPlan'), 0, $chartMaxRows);
$chart2['totalAmount'] = $revenueVal;
$chart2['calculatedChartHeight'] = count($chart2['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;

// 3. PRODUCTS
$chart3Data = $model->getChartMarginByProducts($dateFrom, $dateTo, $customAmountKey);
$chart3['categories']  = array_slice(array_column($chart3Data, 'name'), 0, $chartMaxRows);
$chart3['margin'] = array_slice(array_column($chart3Data, 'margin'), 0, $chartMaxRows);
$chart3['quantity'] = array_slice(array_column($chart3Data, 'quantity'), 0, $chartMaxRows);
$chart3['marginPercent'] = array_slice(array_column($chart3Data, 'marginPercent'), 0, $chartMaxRows);
$chart3['calculatedChartHeight'] = count($chart3['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;

// 4. STRUCTURE
function statesPattern($color) {
    return ['hover' => ['color' => ['pattern' => ['path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6', 'color' => $color, 'width' => 5, 'height' => 5]]]];
}

$chart4['categories'][] = 'Чистая прибыль';
$chart4['isExpense'][] = 0;
$chart4['series'][] = [
    'name' => 'Сумма',
    'data' => [[0, ($lastVal = $netIncomeLossVal)]],
    'color' => ($netIncomeLossVal > 0) ? $chart4['colors']['profit'] : $chart4['colors']['red'],
    'states' => statesPattern(($netIncomeLossVal > 0) ? $chart4['colors']['profit'] : $chart4['colors']['red'])
];
if ($taxVal) {
    $chart4['categories'][] = 'Налоги';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $taxVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($paymentPercentVal) {
    $chart4['categories'][] = 'Проценты уплаченные';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $paymentPercentVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($anotherCostsVal) {
    $chart4['categories'][] = 'Другие расходы';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $anotherCostsVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($operatingCostsVal) {
    $chart4['categories'][] = 'Операционные расходы';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $operatingCostsVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($fixedCostsVal) {
    $chart4['categories'][] = 'Постоянные расходы';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $fixedCostsVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($variableCostsVal) {
    $chart4['categories'][] = 'Переменные расходы';
    $chart4['isExpense'][] = 1;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$lastVal, ($lastVal = $lastVal + $variableCostsVal)]],
        'color' => $chart4['colors']['expense'],
        'states' => statesPattern($chart4['colors']['expense'])
    ];
}
if ($receivedPercentVal) {
    $chart4['categories'][] = 'Проценты полученные';
    $chart4['isExpense'][] = 0;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$revenueVal + $anotherIncomeVal, $revenueVal + $anotherIncomeVal + $receivedPercentVal]],
        'color' => $chart4['colors']['income'],
        'states' => statesPattern($chart4['colors']['income'])
    ];
}
if ($anotherIncomeVal) {
    $chart4['categories'][] = 'Другие доходы';
    $chart4['isExpense'][] = 0;
    $chart4['series'][] = [
        'name' => 'Сумма',
        'data' => [[$revenueVal, $revenueVal + $anotherIncomeVal]],
        'color' => $chart4['colors']['income'],
        'states' => statesPattern($chart4['colors']['income'])
    ];
}
$chart4['categories'][] = 'Выручка';
$chart4['isExpense'][] = 0;
$chart4['series'][] = [
    'name' => 'Сумма',
    'data' => [[0, $revenueVal]],
    'color' => $chart4['colors']['income'],
    'states' => statesPattern($chart4['colors']['income'])
];

$chart4['categories'] = array_reverse($chart4['categories']);
$chart4['series'] = array_reverse($chart4['series']);
$chart4['isExpense'] = array_reverse($chart4['isExpense']);
// arrange by categories positions
foreach ($chart4['series'] as $key => &$serie) {
    $serieValue = $serie['data'][0];
    $serie['data'] = [];
    for ($i=0; $i<$key; $i++)
        $serie['data'][] = [null, null];
    $serie['data'][] = $serieValue;
}

$chart4['yMin'] = round(($netIncomeLossVal < 0) ? $netIncomeLossVal : 0);
$chart4['yMax'] = round($revenueVal + $anotherIncomeVal + $receivedPercentVal);

$chart4['totalExpenseAmount'] = ($variableCostsVal + $fixedCostsVal + $operatingCostsVal + $anotherCostsVal + $paymentPercentVal + $taxVal);

if (Yii::$app->request->post('chart-profit-loss-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'amountKey' => $customAmountKey,
        'revenue' => [
            'currDayPos' => $currDayPos,
            'wrapPointPos' => $wrapPointPos,
            'freeDays' => $chartFreeDays,
            'labelsX' => $chartLabelsX,
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $chartPeriodsX,
                ],
                'series' => [
                    [
                        'data' => $chart1['totalRevenue'],
                    ],
                    [
                        'data' => $chart1['netIncomeLoss'],
                    ]
                ],
            ],
        ],
        'clients' => [
            'chartHeight' => $chart2['calculatedChartHeight'],
            'totalAmount' => $chart2['totalAmount'],
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $chart2['categories'],
                ],
                'series' => [
                    [
                        'data' => $chart2['planData'],
                    ],
                    [
                        'data' => $chart2['factData'],
                    ]
                ],
            ],
        ],
        'products' => [
            'chartHeight' => $chart3['calculatedChartHeight'],
            'quantity' => $chart3['quantity'],
            'marginPercent' => $chart3['marginPercent'],
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $chart3['categories'],
                ],
                'series' => [
                    [
                        'data' => $chart3['margin'],
                    ]
                ],
            ],
        ],
        'structure' => [
            'totalExpenseAmount' => $chart4['totalExpenseAmount'],
            'isExpense' => $chart4['isExpense'],
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $chart4['categories'],
                ],
                'yAxis' => [
                    'min' => $chart4['yMin'],
                    'max' => $chart4['yMax']
                ],
                'series' => $chart4['series']
            ],
        ]
    ]);

    exit;
}

?>

<div class="row mb-4 pb-3">
    <div class="col-8 pr-2">
        <?= $this->render('_chart_profit_loss_revenue', [
            'model' => $model,
            // consts
            'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
            'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
            // vars
            'chartPeriodsX' => $chartPeriodsX,
            'totalRevenue' => $chart1['totalRevenue'],
            'netIncomeLoss' => $chart1['netIncomeLoss'],
        ]); ?>
    </div>
    <div class="col-4 pl-2">
        <div class="row">
            <div class="col-12">
                <?php
                // Expense Charts
                echo $this->render('_chart_profit_loss_structure', [
                    'model' => $model,
                    // vars
                    'customMonth' => $customMonth,
                    'categories' => $chart4['categories'],
                    'series' => $chart4['series'],
                    'yMin' => $chart4['yMin'],
                    'yMax' => $chart4['yMax']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<?php if (!$hideSecondLine): ?>
    <div class="row">
        <div class="col-4 pr-2">
            <?= $this->render('_chart_profit_loss_clients', [
                'model' => $model,
                // consts
                'chartWrapperHeight' => $chartWrapperHeight,
                'chartHeightHeader' => $chartHeightHeader,
                'chartHeightFooter' => $chartHeightFooter,
                'chartHeightColumn' => $chartHeightColumn,
                'chartMaxRows' => $chartMaxRows,
                'cropNamesLength' => $cropNamesLength,
                'croppedNameLength' => $croppedNameLength,
                // vars
                'customMonth' => $customMonth,
                'categories' => $chart2['categories'],
                'factData' => $chart2['factData'],
                'planData' => $chart2['planData'],
                'calculatedChartHeight' => $chart2['calculatedChartHeight'],
            ]); ?>
        </div>
        <div class="col-4 pl-2 pr-2">
            <?= $this->render('_chart_profit_loss_products', [
                'model' => $model,
                // consts
                'chartWrapperHeight' => $chartWrapperHeight,
                'chartHeightHeader' => $chartHeightHeader,
                'chartHeightFooter' => $chartHeightFooter,
                'chartHeightColumn' => $chartHeightColumn,
                'chartMaxRows' => $chartMaxRows,
                'cropNamesLength' => $cropNamesLength,
                'croppedNameLength' => $croppedNameLength,
                // vars
                'customMonth' => $customMonth,
                'categories' => $chart3['categories'],
                'marginData' => $chart3['margin'],
                'calculatedChartHeight' => $chart3['calculatedChartHeight'],
            ]); ?>
        </div>
        <div class="col-4 pl-2">
            <?php
            // Expense Charts
            echo $this->render('_chart_profit_loss_projects', [
                'model' => $model
            ]);
            ?>
        </div>
    </div>
<?php endif; ?>

<script>

    // MOVE CHART
    ChartsProfitAndLoss = {
        isLoaded: false,
        pageFilter: <?= json_encode($PAGE_FILTER) ?>,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        month: "<?= $customMonth ?>",
        byAmountKey: "<?= $customAmountKey ?>",
        Revenue: {
            offset: {
                days: 0,
                months: 0
            },
            currDayPos: <?= (int)$currDayPos; ?>,
            wrapPointPos: <?= json_encode($wrapPointPos) ?>,
            labelsX: <?= json_encode($chartLabelsX) ?>,
            freeDays: <?= json_encode($chartFreeDays) ?>,
            chartPoints: {},
            getWrapPointXLabel: function(x) { return ChartsProfitAndLoss._getWrapPointXLabel(x); }
        },
        Clients: {
            chartHeight: "<?= $chart2['calculatedChartHeight'] ?>",
            totalAmount: "<?= $chart2['totalAmount'] ?>",
            chartPoints: {}
        },
        Products: {
            chartHeight: "<?= $chart3['calculatedChartHeight'] ?>",
            quantity: <?= json_encode($chart3['quantity']) ?>,
            marginPercent: <?= json_encode($chart3['marginPercent']) ?>,
            chartPoints: {}
        },
        Structure: {
            totalExpenseAmount: "<?= $chart4['totalExpenseAmount'] ?>",
            isExpense: <?= json_encode($chart4['isExpense']) ?>,
            chartPoints: {}
        },
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('.chart-profit-loss-revenue-arrow').on('click', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                if ($(this).data('move') === 'left') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] += <?= $MOVE_OFFSET ?>;
                }
                ChartsProfitAndLoss.redrawByClick('revenue');
            });

            $('#chart-profit-loss-revenue-type').on('change', function() {
                ChartsProfitAndLoss.byAmountKey = $(this).val();
                ChartsProfitAndLoss.redrawByClick();
            });

            $('#chart-profit-loss-structure-month').on('change', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                ChartsProfitAndLoss.month = $(this).val();
                ChartsProfitAndLoss.redrawByClick('top');
            });
        },
        redrawByClick: function(chartsGroup) {

            return ChartsProfitAndLoss._getData(chartsGroup).done(function() {
                var revenue = $('#chart-profit-loss-revenue').highcharts();
                var clients = $('#chart-profit-loss-clients').highcharts();
                var products = $('#chart-profit-loss-products').highcharts();
                var structure = $('#chart-profit-loss-structure').highcharts();
                if (!chartsGroup || chartsGroup == 'revenue') {
                    revenue.update(ChartsProfitAndLoss.Revenue.chartPoints);
                }
                if (!chartsGroup || chartsGroup == 'top') {
                    if (clients) {
                        clients.update(ChartsProfitAndLoss.Clients.chartPoints);
                        clients.update({"chart": {"height": ChartsProfitAndLoss.Clients.chartHeight + 'px'}});
                    }
                    if (products) {
                        products.update(ChartsProfitAndLoss.Products.chartPoints);
                        products.update({"chart": {"height": ChartsProfitAndLoss.Products.chartHeight + 'px'}});
                    }
                    if (structure) {
                        var series4 = structure.series;
                        var newSeries4 = ChartsProfitAndLoss.Structure.chartPoints.series;
                        if (series4.length > newSeries4.length) {
                            while (series4.length > newSeries4.length)
                                structure.series[newSeries4.length].remove();
                        } else if (newSeries4.length > series4.length) {
                            for (var j = series4.length; j < newSeries4.length; j++)
                                structure.addSeries({'name': 'Сумма', 'data': [[null, null]]});
                        }

                        structure.update(ChartsProfitAndLoss.Structure.chartPoints);
                    }
                }
                ChartsProfitAndLoss._inProcess = false;
            });
        },
        _getData: function(chartsGroup) {
            ChartsProfitAndLoss._inProcess = true;
            return $.post('/analytics/finance-ajax/get-profit-loss-charts-data', {
                    "chart-profit-loss-ajax": true,
                    "chartsGroup": chartsGroup,
                    "year": ChartsProfitAndLoss.year,
                    "month": ChartsProfitAndLoss.month,
                    "offset": ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period],
                    "amountKey": ChartsProfitAndLoss.byAmountKey,
                    "pageFilter": this.pageFilter
                    //"period": ChartsProfitAndLoss.period,
                },
                function(data) {
                    data = JSON.parse(data);
                    const revenue = data.revenue;
                    const clients = data.clients;
                    const products = data.products;
                    const structure = data.structure;
                    if (!chartsGroup || chartsGroup == 'revenue') {
                        ChartsProfitAndLoss.Revenue.freeDays = revenue.freeDays;
                        ChartsProfitAndLoss.Revenue.currDayPos = revenue.currDayPos;
                        ChartsProfitAndLoss.Revenue.labelsX = revenue.labelsX;
                        ChartsProfitAndLoss.Revenue.wrapPointPos = revenue.wrapPointPos;
                        ChartsProfitAndLoss.Revenue.chartPoints = revenue.optionsChart;
                    }
                    if (!chartsGroup || chartsGroup == 'top') {
                        ChartsProfitAndLoss.Clients.totalAmount = clients.totalAmount;
                        ChartsProfitAndLoss.Clients.chartHeight = clients.chartHeight;
                        ChartsProfitAndLoss.Clients.chartPoints = clients.optionsChart;
                        ChartsProfitAndLoss.Products.totalAmount = products.totalAmount;
                        ChartsProfitAndLoss.Products.quantity    = products.quantity;
                        ChartsProfitAndLoss.Products.chartHeight = products.chartHeight;
                        ChartsProfitAndLoss.Products.chartPoints = products.optionsChart;
                        ChartsProfitAndLoss.Structure.totalExpenseAmount = structure.totalExpenseAmount;
                        ChartsProfitAndLoss.Structure.chartPoints = structure.optionsChart;
                        ChartsProfitAndLoss.Structure.isExpense = structure.isExpense;
                    }
                }
            );
        },
        _getWrapPointXLabel: function(x, chartId) {

            var chart = $('#chart-profit-loss-revenue').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (ChartsProfitAndLoss.period == 'months') ? 0.705 : 0.625;

            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].prev;
                left = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ///////////////////////////
    ChartsProfitAndLoss.init();
    ///////////////////////////

    $(document).ready(function() {
        window._chartProfitAndLossArrowZ = 1;
        $('#chart-profit-loss-revenue').mousemove(function(event) {
            let setZ = function() { $('.chart-profit-loss-revenue-arrow').css({"z-index": window._chartProfitAndLossArrowZ - 1}); };
            let y = event.pageY - $(this).offset().top;
            let x = event.pageX - $(this).offset().left;

            if (y > 186) {
                if (window._chartProfitAndLossArrowZ === 1) {
                    window._chartProfitAndLossArrowZ = 2;
                    setZ();
                }
            }
            else if (window._chartProfitAndLossArrowZ === 2) {
                window._chartProfitAndLossArrowZ = 1;
                setZ();
            }

        });

    });

</script>