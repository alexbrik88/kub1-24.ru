<?php
/* @var $this yii\web\View
 * @var $user \common\models\employee\Employee
 * @var $searchModel \frontend\modules\analytics\models\operationalEfficiency\OperationalEfficiencySearchModel
 * @var $activeTab int
 */

use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap\Html;
use kartik\select2\Select2;
use yii\helpers\Url;

$this->title = 'Операционная эффективность';

$userConfig = $user->config;
$showHelpPanel = $userConfig->report_operational_efficiency_help ?? false;
$showChartPanel = $userConfig->report_operational_efficiency_chart ?? false;
?>
<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2">
                    Показатели операционной эффективности
                </h4>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('diagram'), [
                    'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                    'data-toggle' => 'collapse',
                    'href' => '#chartCollapse',
                ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'), [
                    'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                    'data-toggle' => 'collapse',
                    'href' => '#helpCollapse',
                ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px; max-height: 44px">
                <?= Html::beginForm(['operational-efficiency'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'options' => [
                        'id' => 'operationalefficiencysearchmodel-year',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_operational_efficiency_chart">
    <div class="pt-3 pb-3">
        <?= $this->render('_charts/_chart_operational_efficiency', ['model' => $searchModel]) ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_operational_efficiency_help">
    <div class="pt-4 pb-3">
        <?= $this->render('_partial/operational_efficiency_help_panel') ?>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1">
    <?= TableConfigWidget::widget([
        'mainTitle' => 'Выводить разделы',
        'items' => [
            ['attribute' => 'report_operational_efficiency_row_profit_and_loss', 'refresh-page' => true],
            ['attribute' => 'report_operational_efficiency_row_msfo_profit', 'refresh-page' => true],
            ['attribute' => 'report_operational_efficiency_row_revenue_profitability', 'refresh-page' => true],
            ['attribute' => 'report_operational_efficiency_row_return_on_investment', 'refresh-page' => true],
        ]
    ]); ?>
    <?php if (YII_ENV_DEV || !$user->company->isFreeTariff): ?>
        <div class="ml-1 mr-2">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                    'title' => 'Скачать в Excel',
                ]); ?>
        </div>
    <?php endif; ?>
    <div class="ml-1 mb-2">
        <?= TableViewWidget::widget(['attribute' => 'table_view_operational_efficiency']) ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?= $this->render('_partial/operational_efficiency_table', [
        'searchModel' => $searchModel,
        'activeTab' => $activeTab,
        'user' => $user,
    ]); ?>
</div>

<script>
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
    });
</script>
