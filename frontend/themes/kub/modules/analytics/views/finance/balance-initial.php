<?php

use yii\helpers\Html;
use common\components\helpers\Url;
use frontend\themes\kub\helpers\Icon;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\assets\BalanceAsset;
use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\analytics\models\BalanceSearch;
use frontend\modules\analytics\models\balance\BalanceViewHelper;
use common\components\TextHelper;
use common\models\product\ProductInitialBalance;
use \frontend\modules\analytics\models\balance\actives\current\cash\BalanceCashRows;

/* @var $this yii\web\View */
/* @var $model BalanceSearch */
/* @var $initialModel BalanceInitial */
/* @var $balance array */

BalanceAsset::register($this);

$this->title = 'Баланс начальный';
$activeCheckbox = Yii::$app->session->get('balance.activeCheckboxes');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-balance',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true,
    ],
]);

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_balance_help ?? false;
$showChartPanel = $userConfig->report_balance_chart ?? false;
$tabViewClass = $userConfig->getTableViewClass('table_view_finance_balance');
// to compare dates
$etalonInitialDate = $initialModel->date;
$storeInitialDates = [
    'min' => ProductInitialBalance::find()->joinWith('product')->where(['product.company_id' => Yii::$app->user->identity->company->id])->min('date'),
    'max' => ProductInitialBalance::find()->joinWith('product')->where(['product.company_id' => Yii::$app->user->identity->company->id])->max('date'),
];
//
$balanceFlat = BalanceViewHelper::flattenTable($balance);
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center" style="min-height: 52px">
            <div class="column">
                <div class="h4 mb-2"><?= $this->title ?> на
                    <span class="js-change-balance-initial-date title-balance-initial-date">
                        <?= DateHelper::format($initialModel->date, 'd.m.Y', 'Y-m-d') ?>
                    </span>
                </div>
            </div>
            <div class="column p-0 mb-2 mr-auto">
                <?= Html::button(Icon::get('pencil'), [
                    'title' => 'Изменить',
                    'class' => 'js-change-balance-initial-date button-clr link',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="d-flex pt-1 pb-1">
    <div class="ml-auto">
        <?= Html::a(Icon::get('close') . '<span>Закрыть</span>', 'balance', [
            'class' => 'button-regular button-width button-hover-transparent button-clr mb-2',
        ]) ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            <table class="table-balance table-bleak flow-of-funds table table-style table-count-list <?= $tabViewClass ?> table-count-list_collapse_none mb-0" style="max-width: 700px">
                <thead>
                <tr class="quarters-flow-of-funds">
                    <th width="50%" class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1">Статьи</span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2" colspan="2">
                        <div class="pl-1 pr-1">Начальные остатки</div>
                    </th>
                </tr>
                <tr>
                    <th width="20%" class="pl-2 pr-2">
                        <div class="pl-1 pr-1">На дату</div>
                    </th>
                    <th width="20%" class="pl-2 pr-2">
                        <div class="pl-1 pr-1">Сумма</div>
                    </th>
                </tr>
                </thead>

                <tbody>

                <?php /** @var array[] $balanceFlat */ ?>
                <?php foreach ($balanceFlat as $row): ?>
                    <?php
                    $currentQuarter = (int)ceil(date('m') / 3);
                    $suffix = ArrayHelper::remove($row, 'suffix');
                    $options = ArrayHelper::remove($row, 'options', []);
                    $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
                    $skipEmpty = ArrayHelper::remove($row, 'skipEmpty', false);
                    $canUpdate = ArrayHelper::remove($row, 'canUpdate', false);
                    $classBold = !$includedRow ? 'weight-700' : '';
                    if ($skipEmpty && array_sum($row['data']) === 0) {
                        continue;
                    }

                    $formattedDate = ($row['dataInitial']['date'] ?? null) ? DateHelper::format($row['dataInitial']['date'], 'd.m.Y', 'Y-m-d') : '';
                    $formattedAmount = ($row['dataInitial']['amount'] ?? null) ? TextHelper::invoiceMoneyFormat($row['dataInitial']['amount'], 2) : '';
                    $classRed = ($formattedDate && $row['dataInitial']['date'] !== $etalonInitialDate) ? 'text-red' : '';

                    // store products
                    $isStoreRow = ($row['jsActions']['editInitialAmount'] ?? null) === 'js-change-balance-initial-store';
                    if ($isStoreRow) {
                        $classRed = ($storeInitialDates['min'] && $row['dataInitial']['date'] !== $storeInitialDates['min']
                            || $storeInitialDates['max'] && $row['dataInitial']['date'] !== $storeInitialDates['max'])
                                ? 'text-red'
                                : '';
                        $storeDiffDatesText = ($classRed) ? 'Разные даты' : '';
                        $formattedDate = ($classRed) ? '' : $formattedDate;
                    }

                    $cellClickClass = $row['jsActions']['cellClickClass'] ?? '';
                    $cellClickDataUrl = ($row['jsActions']['cellClickUrl'] ?? '') ? "data-url=\"{$row['jsActions']['cellClickUrl']}\"" : "";

                    // cash bank row
                    $isCashBankRow = (isset($options['class']) && strpos($options['class'], 'row-cash-' . BalanceCashRows::T_BANK) !== false);
                    ?>

                    <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']): ?>

                        <tr class="not-drag expenditure_type sub-block <?= ($includedRow) ? 'd-none' : '' ?>" <?php if ($row['floorId']): ?> data-id="<?= $row['floorId'] ?>" <?php endif; ?>>
                            <td class="pl-2 pr-2 pt-3 pb-3">
                                <div class="d-flex">
                                    <button class="<?= $row['includedClass'] ?> table-collapse-btn button-clr ml-1 text-left d-block" type="button" data-collapse-row-trigger data-target="<?= $row['floorTarget'] ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="d-block text_size_14 ml-1 <?= $classBold ?>"><?= $row['label']; ?></span>
                                    </button>
                                    <?php if ($row['jsActions']['addInitialAmountItem'] ?? false): ?>
                                        <?= Html::button(Icon::get('add-icon'), [
                                            'title' => 'Добавить',
                                            'class' => $row['jsActions']['addInitialAmountItem'] . ' button-clr link d-block ml-auto'
                                        ]) ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 <?= $classRed ?>">
                                <span class="<?= $classBold ?>"><?= $formattedDate ?></span>
                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 <?= $cellClickClass ?>" <?= $cellClickDataUrl ?>>
                                <span class="<?= $classBold ?>"><?= $formattedAmount ?></span>
                            </td>
                        </tr>

                    <?php else: ?>

                        <tr class="item-block <?= ($includedRow) ? 'd-none' : '' ?>" <?php if ($row['floorId']): ?> data-id="<?= $row['floorId'] ?>" <?php endif; ?>>
                            <td class="pl-3 pr-2 pt-3 pb-3">
                                <div class="<?= $classBold ?: 'text-grey' ?> text_size_14 <?= $row['includedClass'] ?>" style="padding-top:3px">
                                    <div class="d-flex">
                                        <div class="d-block"><?= $row['label']; ?></div>
                                        <div class="d-block ml-auto">
                                            <?php if ($row['jsActions']['editInitialAmount'] ?? false): ?>
                                                <?= Html::button(Icon::get('pencil'), [
                                                    'title' => 'Изменить',
                                                    'class' => $row['jsActions']['editInitialAmount'] . ' button-clr link',
                                                    'data-url' => $row['jsActions']['urlEditInitialAmount'] ?? '#',
                                                    'data-title' => 'Добавить начальный баланс',
                                                ]) ?>
                                            <?php endif; ?>
                                            <?php if ($row['jsActions']['addInitialAmountItem'] ?? false): ?>
                                                <?= Html::button(Icon::get('add-icon'), [
                                                    'title' => 'Добавить',
                                                    'class' => $row['jsActions']['addInitialAmountItem'] . ' button-clr link',
                                                    'data-url' => $row['jsActions']['urlAddInitialAmountItem'] ?? '#',
                                                ]) ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 <?= $classRed ?>">
                                <?php if ($isCashBankRow && $classRed): ?>
                                    <span class="tooltip-balance" data-tooltip-content="#cash-bank-date-error-tooltip">
                                        <?= $formattedDate ?>
                                    </span>
                                <?php elseif ($isStoreRow): ?>
                                    <?= $formattedDate . $storeDiffDatesText ?>
                                <?php else: ?>
                                    <?= $formattedDate ?>
                                <?php endif; ?>

                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 <?= $cellClickClass ?>" <?= $cellClickDataUrl ?>>
                                <span class="<?= $classBold ?>"><?= $formattedAmount ?></span>
                            </td>
                        </tr>

                    <?php endif; ?>

                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
// modals
echo $this->render('_partial/_balance_initial/modal_new_wallet_bank');
echo $this->render('_partial/_balance_initial/modal_new_wallet_cashbox');
echo $this->render('_partial/_balance_initial/modal_new_wallet_emoney');
echo $this->render('_partial/_balance_initial/modal_update_wallet_balance');
echo $this->render('_partial/_balance_initial/modal_update_balance_initial', ['initialModel' => $initialModel]);
echo $this->render('_partial/_balance_initial/modal_add_balance_article');
echo $this->render('_partial/_balance_initial/modal_delete_balance_initital');
echo $this->render('_partial/_balance_initial/grid_balance_article_pjax');
echo $this->render('_partial/_balance_initial/grid_store_pjax');
?>

<?php $this->registerJs('
    $(document).ready(function() {
            
        // table  
        BalanceTable.init();        
        BalanceTable.clickActiveCheckboxes('.json_encode((array)$activeCheckbox).');        
        
        // modals
        BalanceWalletModal.init();
        BalanceInitial.init();
        BalanceArticleInitial.init();
        
        // grids
        BalanceArticleGrid.init();
        BalanceStoreGrid.init();
    });
'); ?>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<div style="display: none">
    <div id="cash-bank-date-error-tooltip">
        Дата не соответствует дате в шапке.<br/>
        Рекомендуем не менять тут дату, а<br/>
        загрузить выписку в разделе Деньги-Банк<br/>
        с даты указанной в шапке баланса.
    </div>
</div>