<?php

use yii\helpers\Url;

/**
 * @var string $attr
 * @var string $title
 */

$defaultSort = $defaultSort ?? 'name';
$currentSort = Yii::$app->request->get('sort', $defaultSort);

if ($currentSort == $attr) {
    $sort = '-' . $attr;
    $active = 'sort_asc';
} elseif ($currentSort == '-'.$attr) {
    $sort = $attr;
    $active = 'sort_desc';
} else {
    $sort = $attr;
    $active = '';
}

if ($attr == 'month_05') {
    //var_dump($currentSort, $attr);
    //var_dump($sort, $active);
    //exit;
}

?>
<div class="th-title <?= $active ?>">
    <span class="th-sort th-title-name" data-sort="<?= $sort ?>" style="cursor: pointer">
        <?= $title ?>
    </span>
    <span class="th-title-btns">
        <span class="th-sort th-title-btn icon_asc button-clr" data-sort="<?= $sort ?>" style="cursor: pointer">
            <svg class="th-title-icon th-title-icon_reverse svg-icon" viewBox="0 0 8 4"><path d="M4 4L0 0h8z" fill-rule="evenodd"></path></svg>
        </span>
        <a class="th-sort th-title-btn icon_desc button-clr" data-sort="<?= $sort ?>" style="cursor: pointer">
            <svg class="th-title-icon svg-icon" viewBox="0 0 8 4"><path d="M4 4L0 0h8z" fill-rule="evenodd"></path></svg>
        </a>
    </span>
</div>