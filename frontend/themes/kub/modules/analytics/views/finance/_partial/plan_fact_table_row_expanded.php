<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\models\PlanFactSearch;

/** @var $cellIds array */
/** @var $dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $isSumQuarters */
/** @var $trClass string */
/** @var $level int */
/** @var $searchModel PlanFactSearch */

$trData  = '';
$trData .= (isset($dataPaymentType)) ? ('data-payment_type="'.$dataPaymentType.'"') : '';
$trData .= (isset($dataFlowType)) ? ('data-flow_type="'.$dataFlowType.'"') : '';
$trData .= (isset($dataItemId)) ? ('data-item_id="'.$dataItemId.'"') : '';
$trData .= (isset($dataId)) ? ('data-id="'.$dataId.'"') : '';
$trData .= (isset($dataType)) ? ('data-type_id="'.$dataType.'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

//$canHover = $canHover ?? true;
$isSumQuarters = $isSumQuarters ?? true;
$isSortable = $isSortable ?? false;
$isBold = $level <= 1;
$isActiveRow = $level == 2;
$plan = $plan ?? null;
$isIncome = $isIncome ?? true;
?>
<tr class="<?=($trClass)?> <?=($level >= 3 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="pl-2 pr-2 pt-3 pb-3 <?=($isActiveRow || $isSortable ? 'checkbox-td':'')?> <?=($isBold ? 'tooltip-td':'')?>">
        <?php if ($level == 4): ?>
            <span class="text-grey text_size_12 mr-1 m-l-purse-big">
                <?= $title ?>
            </span>
        <?php elseif ($level == 3 && $isSortable): ?>
            <svg class="sortable-row-icon svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
            </svg>
            <span class="text-grey text_size_14"><?= $title; ?></span>
        <?php elseif ($level == 3 && !$isOpenable): ?>
            <span class="text-grey text_size_14 mr-1 m-l-purse">
                <?= $title ?>
            </span>
        <?php elseif ($level == 3 || $level == 2): ?>
            <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor ? 'active':'' ?> <?= ($level == 3) ? 'icon-grey':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="<?=($isBold ? 'weight-700':'text-grey')?> text_size_14 ml-1"><?= $title; ?></span>
            </button>
        <?php else: ?>
            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                <div class="text_size_14 mr-2 nowrap <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </td>
    <?php $key = $quarterSum = $totalSum = 0; ?>
    <?php $key = $planQuarterSum = $planTotalSum = 0; ?>
    <?php foreach (AbstractFinance::$month as $month => $monthText): ?>
        <?php
        $key++;
        $quarter = (int)ceil($key / 3);
        $amount = isset($data[$month]) ? $data[$month] : 0;
        $planAmount = ArrayHelper::getValue($plan ?? [], $month, 0);

        if ($isSumQuarters) {
            $quarterSum += $amount;
            $totalSum += $amount;
            $planQuarterSum += $planAmount;
            $planTotalSum += $planAmount;
        }
        else {
            $quarterSum = $amount;
            $totalSum = $amount;
            $planQuarterSum = $planAmount;
            $planTotalSum = $planAmount;
        }

        // tooltip
        $showDeviation = !($isCurrentYear && $month > date('m') || $level == 1);
        $diff = ($amount - $planAmount) * ($isIncome ? 1 : -1);
        $deviation = ($planAmount > 0) ? round($diff / $planAmount * 100, 2) : ($amount > 0 ? ($isIncome ? 1 : -1) * 100 : 0);
        $tdColor = ($showDeviation) ? PlanFactSearch::getItemColor($deviation, true) : 'transparent';
        $tdClass = ($showDeviation) ? ' tooltip2-deviation-td ' : null;

        ?>

        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? '' : 'd-none' ?> <?=($isBold ? 'weight-700':'')?>" data-collapse-cell data-id="<?= $cellIds[$quarter] ?>">
            <?= ($plan === null) ? ' ' : TextHelper::invoiceMoneyFormat($planAmount, 2); ?>
        </td>
        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? '' : 'd-none' ?> <?=($isBold ? 'weight-700':'')?>" data-collapse-cell data-id="<?= $cellIds[$quarter] ?>">
            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
        </td>
        <td class="<?=($tdClass)?> pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? '' : 'd-none' ?> <?=($isBold ? 'weight-700':'')?>" data-collapse-cell data-id="<?= $cellIds[$quarter] ?>" style="background-color: <?=($tdColor)?>"
            data-fact-amount="<?= $amount/100 ?>"
            data-plan-amount="<?= $planAmount/100 ?>"
            data-diff-amount="<?= $diff/100 ?>"
            data-deviation="<?= $deviation > 0 ? "+{$deviation}" : $deviation; ?>"
        >
            <?= ($plan === null) ? ' ' : TextHelper::invoiceMoneyFormat($diff, 2); ?>
        </td>
        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? '' : 'd-none' ?> <?=($isBold ? 'weight-700':'')?>" data-collapse-cell data-id="<?= $cellIds[$quarter] ?>">
            <?= ($plan === null) ? ' ' : TextHelper::numberFormat($deviation, 2) . '%'; ?>
        </td>
        <?php if ($key % 3 == 0): ?>

            <?php
            // tooltip
            $showDeviation = !($isCurrentYear && $quarter > $currentQuarter || $level == 1);
            $diff = ($quarterSum - $planQuarterSum) * ($isIncome ? 1 : -1);
            $deviation = ($planQuarterSum > 0) ? round($diff / $planQuarterSum * 100, 2) : ($quarterSum > 0 ? ($isIncome ? 1 : -1) * 100 : 0);
            $tdColor = ($showDeviation) ? PlanFactSearch::getItemColor($deviation, true) : 'transparent';
            $tdClass = ($showDeviation) ? ' tooltip2-deviation-td ' : null;        
            ?>

            <td class="<?=($tdClass)?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cellIds[$quarter] ?>" style="background-color: <?=($tdColor)?>"
                data-fact-amount="<?= $quarterSum/100 ?>"
                data-plan-amount="<?= $planQuarterSum/100 ?>"
                data-diff-amount="<?= $diff/100 ?>"
                data-deviation="<?= $deviation > 0 ? "+{$deviation}" : $deviation; ?>"
            >
                <div class="text-dark-alternative <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?php echo TextHelper::invoiceMoneyFormat($planQuarterSum, 2);
                    $quarterSum = $planQuarterSum = 0; ?>
                </div>
            </td>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php
    // tooltip
    $showDeviation = !($level == 1);
    $diff = ($totalSum - $planTotalSum) * ($isIncome ? 1 : -1);
    $deviation = ($planTotalSum > 0) ? round($diff / $planTotalSum * 100, 2) : ($totalSum > 0 ? ($isIncome ? 1 : -1) * 100 : 0);

    $tdColor = ($showDeviation) ? PlanFactSearch::getItemColor($deviation, true) : 'transparent';
    $tdClass = ($showDeviation) ? ' tooltip2-deviation-td ' : null;
    ?>

    <td class="<?=($tdClass)?> pl-2 pr-2 pt-3 pb-3 nowrap total-block <?=($isBold ? 'weight-700':'')?>" style="background-color: <?=($tdColor)?>"
        data-fact-amount="<?= $totalSum/100 ?>"
        data-plan-amount="<?= $planTotalSum/100 ?>"
        data-diff-amount="<?= $diff/100 ?>"
        data-deviation="<?= $deviation > 0 ? "+{$deviation}" : $deviation; ?>"
    >
        <div class="text-dark-alternative">
            <?= TextHelper::invoiceMoneyFormat($planTotalSum, 2); ?>
        </div>
    </td>
</tr>