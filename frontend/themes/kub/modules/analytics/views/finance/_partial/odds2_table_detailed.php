<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\employee\Config;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = $userConfig->getTableViewClass('table_view_finance_odds');

// Remove empty Wallets by user config
if ($userConfig->report_odds_hide_zeroes) {
    foreach ($growingData as $k0 => $d0) {

        if (!array_key_exists('children', $d0)) {
            unset($data[$k0]);
            unset($growingData[$k0]);
            continue;
        }

        foreach ($d0['children'] as $k1 => $d1) {
            if (0 === array_sum($d1['data'])) {
                unset($data[$k0]['children'][$k1]);
                unset($growingData[$k0]['children'][$k1]);
            }
        }
    }
}
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table by_purse table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
            <?= $this->render('odds2_table_head', [
                'searchModel' => $searchModel,
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
            ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 0
        foreach ($data as $paymentType => $level0) {

            if ($searchModel::hideBlockByUser($activeTab, $paymentType, $userConfig)) {
                continue;
            }

            if (count($level0['children']) > 1) {
                echo $this->render('odds2_table_row', [
                    'level' => 0,
                    'title' => $level0['title'],
                    'data' => $level0['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'canHover' => false,
                    'question' => $level0['question'] ?? null,
                    'negativeRed' => true,
                ]);
            }

            // LEVEL 1
            foreach ($level0['children'] as $walletID => $level1) {

                echo $this->render('odds2_table_row', [
                    'level' => 1,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'dataWalletId' => $walletID,
                    'negativeRed' => true,
                ]);

                // LEVEL 2
                foreach ($level1['levels'] as $paymentFlowType => $level2) {

                    $floorKey = "floor-{$walletID}-{$paymentFlowType}";
                    $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                    $isIncome = $paymentFlowType;

                    echo $this->render('odds2_table_row', [
                        'level' => 2,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                        'trId' => $paymentFlowType,
                        'dMonthes' => $d_monthes,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => $paymentType,
                        'dataFlowType' => $paymentFlowType,
                        'dataWalletId' => $walletID,
                        'floorKey' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isOpenable' => true,
                    ]);

                    // LEVEL 3
                    foreach ($level2['levels'] as $itemId => $level3) {

                        $floorKey2 = "floor-item-{$itemId}";
                        $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                        echo $this->render('odds2_table_row', [
                            'level' => 3,
                            'title' => $level3['title'],
                            'data' => $level3['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dMonthes' => $d_monthes,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => $paymentType,
                            'dataFlowType' => $paymentFlowType,
                            'dataWalletId' => $walletID,
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $itemId,
                            'dataId' => $floorKey,
                            'isOpenedFloor' => $isOpenedFloor,
                            'isSortable' => ($activeTab == OddsSearch::TAB_ODDS && empty($level3['levels'])),
                            'isOpenable' => (!empty($level3['levels'])),
                            'floorKey' => $floorKey2,
                            'dataHoverText' => $level3['dataHoverText'] ?? null
                        ]);

                        if (empty($level3['levels']))
                            continue 1;

                        // LEVEL 4
                        foreach ($level3['levels'] as $subItemId => $level4) {

                            echo $this->render('odds2_table_row', [
                                'level' => 4,
                                'title' => $level4['title'],
                                'data' => $level4['data'],
                                'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                                'dMonthes' => $d_monthes,
                                'cellIds' => $cell_id,
                                'dataPaymentType' => $paymentType,
                                'dataFlowType' => $paymentFlowType,
                                'dataWalletId' => $walletID,
                                'dataType' => $paymentFlowType,
                                'dataItemId' => $subItemId,
                                'dataId' => $floorKey2,
                                'isOpenedFloor' => $isOpenedFloor2,
                                'isSortable' => false
                            ]);
                        }
                    }
                }

                if (isset($growingData[$paymentType]['hide']))
                    continue;

                // GROWING BY WALLET
                echo $this->render('odds2_table_row', [
                    'level' => 1,
                    'title' => $growingData[$paymentType]['children'][$walletID]['title'],
                    'data' => $growingData[$paymentType]['children'][$walletID]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'negativeRed' => true,
                ]);
            }

            // GROWING
            if (count($level0['children']) > 1) {
                echo $this->render('odds2_table_row', [
                    'level' => 0,
                    'title' => $growingData[$paymentType]['title'],
                    'data' => $growingData[$paymentType]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'negativeRed' => true,
                ]);
            }
        }

        // TOTAL BY MONTH
        echo $this->render('odds2_table_row', [
            'level' => 1,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag net-cash-flow',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'canHover' => false,
            'question' => $totalData['month']['question'] ?? null,
            'negativeRed' => true,
        ]);

        // TOTAL BY YEAR
        echo $this->render('odds2_table_row', [
            'level' => 1,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            'negativeRed' => true,
        ]);

        ?>
        </tbody>
    </table>
    </div>
</div>