<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.01.2019
 * Time: 16:59
 */

use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use kartik\checkbox\CheckboxX;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use common\components\ImageHelper;
use yii\helpers\Url;
use frontend\modules\analytics\models\AbstractFinance;

/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $data []
 * @var $expenditureItems []
 * @var $types []
 * @var $balance []
 * @var $growingBalance []
 * @var $blocks []
 * @var $checkMonth boolean
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $floorMap []
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

if (!isset($dataDAY))
    $dataDAY = [];

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');
?>

<script>
    // ODDS FIXED COLUMN
    $(document).ready(function() {
        OddsTable.fillFixedColumn();
    });
</script>

<div id="cs-table-2" class="custom-scroll-table-double cs-top">
    <table style="width: 1832px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
</div>

<!-- ODDS FIXED COLUMN -->
<div id="cs-table-first-column">
    <table class="table-bleak table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
        <thead></thead>
        <tbody></tbody>
    </table>
</div>

<div id="cs-table-1" class="custom-scroll-table-double">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <tr class="quarters-flow-of-funds">
          <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
              <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                  <span class="table-collapse-icon">&nbsp;</span>
                  <span class="text-grey weight-700 ml-1">Статьи</span>
              </button>
          </th>
          <?php foreach (FlowOfFundsReportSearch::$month as $key => $month): ?>
              <?php $key = (int)$key; ?>
              <?php $colspan = cal_days_in_month(CAL_GREGORIAN, $key, $searchModel->year) ?>
              <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_days[$key] ? "colspan='{$colspan}'" : '' ?> data-collapse-cell-title data-id="<?= $cell_id[$key] ?>">
                  <button class="table-collapse-btn button-clr ml-1 <?= $d_days[$key] ? 'active' : '' ?>" type="button" data-columns-count="<?=($colspan)?>" data-collapse-trigger-days data-target="<?= $cell_id[$key] ?>" data-month="<?= $key ?>">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap"><?= $month . ' ' . $searchModel->year; ?></span>
                  </button>
              </th>
          <?php endforeach; ?>
          <th class="pl-2 pr-2 pt-3 pb-3 align-top">
              <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
          </th>
        </tr>
        <tr>
          <?php foreach (FlowOfFundsReportSearch::$month as $month => $monthName): ?>
              <?php $month = (int)$month; ?>
              <?php foreach ($searchModel->getDaysInMonth($month) as $date => $dayName): ?>
                  <?php $dayClass = (date('N', strtotime($date)) >= 6) ? 'font-color-red' : '' ?>
                  <th class="pl-2 pr-2 <?= $d_days[$month] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$month] ?>" data-month="<?= $month; ?>" data-day="<?= (int)$dayName ?>">
                      <div class="pl-1 pr-1 <?= $dayClass ?>"><span class="<?= ($date == date('Y-m-d')) ? 'current-day' : '' ?>"><?= (int)$dayName ?></span></div>
                  </th>
              <?php endforeach; ?>
              <th class="pl-2 pr-2 <?= $d_days[$month] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$month] ?>" data-month="<?= $month; ?>" data-month-name="<?= $monthName ?>">
                  <div class="pl-1 pr-1">Итого</div>
              </th>
          <?php endforeach; ?>
          <th class="pl-2 pr-2 pt-3 pb-3 align-top">
              <div class="pl-1 pr-1">Итого</div>
          </th>
        </tr>
        </thead>

        <tbody>
        <?php foreach (FlowOfFundsReportSearch::$types as $typeID => $typeName): ?>

            <?php $floorKey1 = "first-floor-{$typeID}"; ?>
            <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1); ?>

            <?php $class = in_array($typeID, [
              FlowOfFundsReportSearch::RECEIPT_FINANCING_TYPE_FIRST,
              FlowOfFundsReportSearch::INCOME_OPERATING_ACTIVITIES,
              FlowOfFundsReportSearch::INCOME_INVESTMENT_ACTIVITIES]) ?
              'income' : 'expense'; ?>

            <?php if ($class == 'income'): ?>
                <tr class="main-block">
                <td class="pl-2 pr-2 pt-3 pb-3 tooltip-td">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            <?= FlowOfFundsReportSearch::$blocks[FlowOfFundsReportSearch::$blockByType[$typeID]]; ?>
                            <?php if ($typeID == FlowOfFundsReportSearch::RECEIPT_FINANCING_TYPE_FIRST): ?>
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_financial-operations-block',
                                ]) ?>
                            <?php elseif ($typeID == FlowOfFundsReportSearch::INCOME_OPERATING_ACTIVITIES): ?>
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_operation-activities-block',
                                ]) ?>
                            <?php else: ?>
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_investment-activities-block',
                                ]) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                    <?php $month = (int)$monthNumber; ?>
                    <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap day-block <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php /* days */ ?>
                                <?php $dayAmount = isset($dataDAY['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]][$date]) ?
                                    $dataDAY['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]][$date]['flowSum'] : 0; ?>
                                <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                            </div>
                        </td>
                    <?php endforeach; ?>

                    <?php $amount = isset($blocks[FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]) ?
                        $blocks[FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>

                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell-total>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                        </div>
                    </td>
                <?php endforeach; ?>
                <td class="total-block can-hover operation-actitivities-block-total-flow-sum pl-2 pr-2 pt-3 pb-3 nowrap weight-700">
                    <div class="pl-1 pr-1"><?= isset($blocks[FlowOfFundsReportSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum']) ? TextHelper::invoiceMoneyFormat($blocks[FlowOfFundsReportSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum'], 2) : 0; ?></div>
                </td>
                </tr>
            <?php endif; ?>

            <tr class="not-drag expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>">
                <td class="pl-2 pr-2 pt-3 pb-3 checkbox-td">
                    <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey text_size_14 ml-1"><?= $typeName; ?></span>
                    </button>
                </td>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                    <?php $month = (int)$monthNumber; ?>
                    <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap day-block <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1">
                                <?php /* days */ ?>
                                <?php $dayAmount = isset($dataDAY['types'][$typeID][$date]) ? $dataDAY['types'][$typeID][$date]['flowSum'] : 0; ?>
                                <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                            </div>
                        </td>
                    <?php endforeach; ?>

                    <?php $amount = isset($types[$typeID][$monthNumber]) ? $types[$typeID][$monthNumber]['flowSum'] : 0; ?>
                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$month] ?>">
                        <div class="pl-1 pr-1">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                <?php endforeach; ?>
                <td class="total-block can-hover total-flow-sum-<?= $typeID; ?> pl-2 pr-2 pt-3 pb-3 nowrap">
                    <div class="pl-1 pr-1"><?= isset($types[$typeID]['totalFlowSum']) ? TextHelper::invoiceMoneyFormat($types[$typeID]['totalFlowSum'], 2) : 0; ?></div>
                </td>
            </tr>

            <?php if (isset($data[$typeID], $expenditureItems[$typeID])): ?>
                <?php foreach ($expenditureItems[$typeID] as $expenditureItemID => $expenditureItemName): ?>
                    <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                        <tr class="item-block <?= !$isOpenedFloor1 ? 'd-none':'' ?> <?=  $class; ?>" data-id="<?= $floorKey1 ?>" data-type_id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>">
                            <td class="pl-2 pr-2 pt-3 pb-3 checkbox-td">
                                <svg class="sortable-row-icon svg-icon text_size-14 text-grey mr-2 ml-1 flex-shrink-0">
                                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                </svg>
                                <span class="text-grey text_size_14"><?= $expenditureItemName; ?></span>
                            </td>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                                <?php $month = (int)$monthNumber; ?>
                                <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap day-block <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                                        <div class="pl-1 pr-1">
                                            <?php /* days */ ?>
                                            <?php $dayAmount = isset($dataDAY[$typeID][$expenditureItemID][$date]) ? $dataDAY[$typeID][$expenditureItemID][$date]['flowSum'] : 0; ?>
                                            <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                                        </div>
                                    </td>
                                <?php endforeach; ?>

                                <?php $key++;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($data[$typeID][$expenditureItemID][$monthNumber]) ? $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                ?>

                                <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$month] ?>">
                                    <div class="pl-1 pr-1">
                                        <?php echo TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                    </div>
                                </td>
                            <?php endforeach ?>
                            <td class="total-block pl-2 pr-2 pt-3 pb-3 nowrap total-month-total-flow-sum can-hover">
                                <div class="pl-1 pr-1"><?= isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ? TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID]['totalFlowSum'], 2) : 0; ?></div>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($class == 'expense'): ?>
                <tr>
                <td class="pl-2 pr-2 pt-3 pb-3 tooltip-td">
                  <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                      <div class="text_size_14 weight-700 mr-2 nowrap">
                          <?= FlowOfFundsReportSearch::$growingBalanceLabelByType[$typeID]; ?>
                      </div>
                  </div>
                </td>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                    <?php $month = (int)$monthNumber; ?>
                    <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php /* days */ ?>
                                <?php $dayAmount = isset($dataDAY['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$date]) ?
                                    $dataDAY['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$date]['flowSum'] : 0; ?>
                                <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                            </div>
                        </td>
                    <?php endforeach; ?>

                    <?php $amount = isset($data['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]) ?
                        $data['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell-total>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap total-month-total-flow-sum"></td>
                </tr>
            <?php endif; ?>

        <?php endforeach; ?>

        <!-- TOTALS -->
        <tr class="not-drag cancel-drag">
            <td class="pl-2 pr-2 pt-3 pb-3">
                <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                    <div class="text_size_14 weight-700 mr-2 nowrap">
                        Результат по месяцу
                    </div>
                </div>
            </td>
            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                <?php $month = (int)$monthNumber; ?>
                <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?php /* days */ ?>
                            <?php $dayAmount = isset($dataDAY['balance'][$date]) ? $dataDAY['balance'][$date] : 0; ?>
                            <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                        </div>
                    </td>
                <?php endforeach; ?>

                <?php $amount = isset($balance[$monthNumber]) ? $balance[$monthNumber] : 0; ?>

                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell-total>
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                    </div>
                </td>
            <?php endforeach; ?>
            <td class="pl-2 pr-2 pt-3 pb-3 nowrap total-month-total-flow-sum weight-700">
                <div class="pl-1 pr-1"><?= TextHelper::invoiceMoneyFormat(isset($balance['totalFlowSum']) ? $balance['totalFlowSum'] : 0, 2); ?></div>
            </td>
        </tr>
        <tr class="not-drag cancel-drag">
            <td class="pl-2 pr-2 pt-3 pb-3">
                <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                    <div class="text_size_14 weight-700 mr-2 nowrap">
                        Остаток на конец месяца
                    </div>
                </div>
            </td>
            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>

                <?php $month = (int)$monthNumber; ?>
                <?php foreach ($searchModel->getDaysInMonth($monthNumber) as $date => $dayName): ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? '' : 'd-none' ?>" data-month="<?= $month ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?php /* days */ ?>
                            <?php $dayAmount = isset($dataDAY['growingBalance'][$date]) ? $dataDAY['growingBalance'][$date] : 0; ?>
                            <?= TextHelper::invoiceMoneyFormat($dayAmount, 2) ?>
                        </div>
                    </td>
                <?php endforeach; ?>

                <?php $amount = isset($growingBalance[$monthNumber]) ? $growingBalance[$monthNumber] : 0; ?>

                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_days[$month] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$month] ?>" data-collapse-cell-total>
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                    </div>
                </td>
            <?php endforeach; ?>
            <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty weight-700" role="row"></td>
        </tr>

        </tbody>

    </table>
    </div>
</div>