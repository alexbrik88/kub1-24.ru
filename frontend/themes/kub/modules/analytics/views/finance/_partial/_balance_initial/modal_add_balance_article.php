<?php
use yii\web\View;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;
use frontend\modules\reference\models\BalanceArticlesSubcategories;

Modal::begin([
    'id' => 'balance-article-modal-container',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>
<h4 class="modal-title" id="balance-article-modal-header"></h4>
<?php Pjax::begin([
    'id' => 'balance-article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false
]) ?>
<?php Pjax::end() ?>
<?php Modal::end() ?>

<?php

$jsCategories = json_encode(BalanceArticlesSubcategories::MAP_BY_CATEGORIES) ?: "[]";
$jsUsefulLifeInMonth = json_encode(BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP) ?: "[]";

$this->registerJs("
    window.balanceArticlesSubcategoriesMapByCategories = {$jsCategories};
    window.usefulLifeInMonthSubcategoriesMap = {$jsUsefulLifeInMonth};
", View::POS_BEGIN);