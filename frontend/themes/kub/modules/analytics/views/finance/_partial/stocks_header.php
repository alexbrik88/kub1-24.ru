<?php

use common\components\helpers\Html;
use frontend\modules\analytics\models\StocksSearch;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;

/**
 * @var $title
 * @var $this yii\web\View
 * @var $searchModel StocksSearch
 */
$showByQuantity = (int)Yii::$app->request->get('quantity');
$showHelpPanel = $userConfig->report_stocks_help ?? false;
$showChartPanel = $userConfig->report_stocks_chart ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['stocks'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_stocks_chart">
    <div class="pt-3 pb-3">
        <div class="row mb-4">
            <div class="col-8 pr-2" style="z-index: 2">
                <?php
                // Main Chart
                echo $this->render('../_charts/_chart_stocks_main', [
                    'model' => $searchModel,
                    'userConfig' => $userConfig
                ]);
                ?>
            </div>
            <div class="col-4 pl-2" style="z-index: 1">
                <?php
                // Structure Chart
                echo $this->render('../_charts/_chart_stocks_structure', [
                    'model' => $searchModel,
                    'userConfig' => $userConfig
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php
                // Main Chart
                echo $this->render('../_charts/_chart_stocks_squares', [
                    'model' => $searchModel,
                    'userConfig' => $userConfig
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_stocks_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('stocks_panel')
        ?>
    </div>
</div>

<?php $this->registerJs(<<<JS
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });

    _offsetMCSLeft = 0;
    _firstCSTableColumn = $('#cs-table-first-column');
    StocksTable = {
        year: $('#stockssearch-year').val(), 
        getProductsList: function(target) 
        {
            const PRODUCTS_LIMIT = 50;
            const tmpRow = $('[data-id="'+target+'"]').filter('.wait-for-ajax-load');
            let   productsOffset = parseInt($(tmpRow).attr('data-offset') || 0);
            let   fixedProductsOffset = productsOffset;

            if (tmpRow.length !== 1) {
                console.log('find more than one tmpRow');
                return false;
            }
            
            $.post('/analytics/finance-ajax/get-stocks-products-by-group', {
                    "table-stocks-ajax": true,
                    "year": StocksTable.year,
                    "target": target,
                    "floorMap": StocksTable.getFloorMap(),
                    "title": $('#stockssearch-title').val(),
                    "quantity": {$showByQuantity},
                },
                function(data) 
                {
                    const floorMap = StocksTable.getFloorMap();
                    const tmpRowOffset = $(tmpRow).index();
                    const tableRef = document.getElementById('stocks-table').getElementsByTagName('tbody')[0];
                    let newRow;
                    let newCell;
                    let rowData;
                    let dataId;
                    let quarterFloorMap;
                    let isQuarterCell;
                    
                    data = JSON.parse(data);
                    for (let i=0; i < data.length; i++) {
                        
                        if (i < productsOffset)
                            continue;
                        
                        rowData = data[i];
                        newRow = tableRef.insertRow(tmpRowOffset + (productsOffset - fixedProductsOffset));
                        newRow.dataset.id = 'group_' + rowData.id;
                        
                        newCell = newRow.insertCell(0);
                        newCell.className = 'stocks-product-cell text_size_14';
                        newCell.innerHTML = '<div style="max-width: 255px;overflow: hidden;text-overflow: ellipsis;padding-left: 26px;">' + rowData.title + '</div>';
                          //'<div style="max-width: 255px;overflow: hidden;text-overflow: ellipsis;padding-left: 26px;">' +
                          //'<a data-pjax="0" target="_blank" href="/product/view/?productionType=1&id='+ rowData.id + '">' + rowData.title +'</a>' +
                          //'</div>';
                        
                        for (let j=0; j<16; j++) {
                            if (j <= 3) {
                                dataId = 'first-cell';
                                quarterFloorMap = floorMap['first-cell'];
                                isQuarterCell = (j === 3);
                            } else if (j <= 7) {
                                dataId = 'second-cell';                                
                                quarterFloorMap = floorMap['second-cell'];
                                isQuarterCell = (j === 7);
                            }  else if (j <= 11) {
                                dataId = 'third-cell';                                
                                quarterFloorMap = floorMap['third-cell'];
                                isQuarterCell = (j === 11);
                            } else if (j <= 15) {
                                dataId = 'fourth-cell';                                
                                quarterFloorMap = floorMap['fourth-cell'];
                                isQuarterCell = (j === 15);
                            }
                            newCell = newRow.insertCell(j+1);
                            newCell.dataset.id = dataId;
                            newCell.className = 'text_size_14 sum-cell nowrap';
                            newCell.setAttribute((isQuarterCell) ? 'data-collapse-cell-total' : 'data-collapse-cell', '');                            
                            
                            if (quarterFloorMap && isQuarterCell || !quarterFloorMap && !isQuarterCell) {
                                newCell.className += ' d-none';
                            } 
                            newCell.innerHTML = rowData.data[j];
                        }

                        productsOffset++;

                        if (i >= fixedProductsOffset + PRODUCTS_LIMIT - 1)
                            break;
                    }

                    if (data.length > productsOffset) {
                        $(tmpRow)
                            .attr('data-offset', productsOffset)
                            .html('<td style="padding-left:36px"><a href="javascript:void(0)" class="link show-more-products">Еще ' + ' ' + 
                            (data.length - productsOffset) + 
                            StocksTable._declOfNum(data.length - productsOffset, [' товар', ' товара', ' товаров']) + '</a></td>');
                    } else {
                        $(tmpRow).remove();
                    }

                    StocksTable.fillFixedColumn();
                }
            );
        },
        getFloorMap: function() 
        {
            let floorMap = {};
            $('table.stocks-table').find('[data-stocks-collapse-trigger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            
            return floorMap;
        },
        bindEvents: function() 
        {
            // COL
            $(document).on('click', '[data-stocks-collapse-trigger]', function() {
                var target = $(this).data('target');
                var collapseCount = $(this).data('columns-count') || 3;
                $(this).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
                }
                $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
            });
            // ROW
            $(document).on('click', '[data-stocks-collapse-row-trigger]', function() {
                var target = $(this).data('target');
                var this1 = $('#cs-table-1').find('[data-stocks-collapse-row-trigger]').filter('[data-target="'+target+'"]');
                var this2 = $('#cs-table-first-column').find('[data-stocks-collapse-row-trigger]').filter('[data-target="'+target+'"]');
                
                $(this1).toggleClass('active');
                $(this2).toggleClass('active');

                if ( $(this1).hasClass('active') ) {
                  $('[data-id="'+target+'"]').removeClass('d-none');
                    
                  //
                  // AJAX REQUEST
                  //
                  StocksTable.getProductsList(target);
                    
                } else {
                    $('[data-id="'+target+'"]').addClass('d-none');
                }
                //if ( $('[data-stocks-collapse-row-trigger].active').length <= 0 ) {
                //    $('[data-collapse-all-trigger]').removeClass('active');
                //} else {
                //    $('[data-collapse-all-trigger]').addClass('active');
                //}
            });
            // Products More
            $(document).on('click', '.show-more-products', function() {
                const target = $(this).closest('tr').attr('data-id');
                StocksTable.getProductsList(target);
            })
        },
        initCustomScroll: function() {
            // CUSTOM SCROLL
            $('#cs-table-1').mCustomScrollbar({
                horizontalScroll: true,
                axis:"x",
                scrollInertia: 0,
                setLeft: _offsetMCSLeft + "px",
                callbacks: {
                    onScrollStart: function() {},
                    onScroll: function() {
                        if (this.mcs.left === 0 && $(_firstCSTableColumn).is(':visible'))
                            $(_firstCSTableColumn).hide();
                    },
                    whileScrolling:function(){
                        window._offsetMCSLeft = this.mcs.left;
                        //if (window._activeMCS === 1)
                        //    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
        
                        if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                            $(_firstCSTableColumn).show();
                    }
                },
                advanced:{
                    autoExpandHorizontalScroll: 3,
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnImageLoad: false
                },
                mouseWheel:{ enable: false },
            });
        },
        fillFixedColumn: function() {

          var tableBlock = $('#cs-table-1');
          var columnBlock = $('#cs-table-first-column');
          // CLEAR
          $(columnBlock).find('thead').html('');
          $(columnBlock).find('tbody').html('');
          $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());

          // CLONE FIRST COLUMN
          $('.stocks-table thead tr > th:first-child').each(function(i,v) {
              var col = $(v).clone();
              $('.table-fixed-first-column thead').append($('<tr/>').append(col));

              return false;
          });
          $('.stocks-table tbody tr > td:first-child').each(function(i,v) {
              var col = $(v).clone();
              var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
              var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
              $(col).find('.sortable-row-icon').html('');
              trClass = trClass.replace('wait-for-ajax-load', '');
              $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
          });
      },
        showFixedColumn: function() {
            $(_firstCSTableColumn).show();
        },
        _declOfNum: function(number, titles) {
            let cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        }
    };

    // TABLE FLOORS
    $('#refresh_stocks_table').on('pjax:beforeSend', function (event, xhr, settings) {
        if (settings.data == undefined) {

            let title = $('#stockssearch-title').val();
            let floorMap = {};
            $('table.stocks-table').find('[data-stocks-collapse-trigger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });

            jQuery.pjax({
                url: settings.url,
                type: 'POST',
                data: {'floorMap': floorMap, 'title': title},
                container: '#refresh_stocks_table',
                timeout: 10000,
                push: false,
                scrollTo: false
            });

            return false;
        }
    });
    
    // INIT
    $(document).ready(function() {
        StocksTable.bindEvents();
        StocksTable.fillFixedColumn();
        StocksTable.initCustomScroll();
    });
    // REINIT ON TABLE RELOAD
    $(document).on("pjax:success", "#refresh_stocks_table", function(event, data) {
        window._firstCSTableColumn = $('#cs-table-first-column');
        StocksTable.initCustomScroll();
        StocksTable.fillFixedColumn();
        if (_offsetMCSLeft !== 0)
            StocksTable.showFixedColumn();
        $('.dropdown-settings').click();
    });
JS
);
?>

<style>
    #cs-table-first-column { top:-1px; }
</style>