<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\Upd;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\ProfitAndLossSearchItemsModel;
use frontend\rbac\permissions\document\Document;
use yii\widgets\Pjax;
use frontend\models\Documents;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\Invoice;
use common\models\document\GoodsCancellation;

/** @var ProfitAndLossSearchItemsModel $searchModel */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');

$multiCompanyManager = &$searchModel->multiCompanyManager;
?>

<?php $pjax = Pjax::begin([
    'id' => 'doc-items-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>

<?php if (!empty($searchBy)) echo Html::hiddenInput('searchBy', $searchBy, ['id' => 'searchBy']) ?>
<?php if (!empty($isSearchByChanged)) echo Html::hiddenInput('isSearchByChanged', 1, ['id' => 'isSearchByChanged']) ?>

<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none mb-0">
        <div class="custom-scroll-table">
            <div class="table-wrap">
                <?= GridView::widget([
                    'id' => 'doc-items-pjax-grid',
                    'dataProvider' => $itemsDataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => \yii\helpers\Html::checkbox('', false, [
                                'class' => 'joint-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                            ],
                            'format' => 'raw',
                            'value' => function ($doc) use ($searchModel) {

                                //switch ($doc['tb']) {
                                //    case 'act':
                                //        $docModel = Act::findOne($doc['id']);
                                //        break;
                                //    case 'packing_list':
                                //        $docModel = PackingList::findOne($doc['id']);
                                //        break;
                                //    case 'upd':
                                //        $docModel = Upd::findOne($doc['id']);
                                //        break;
                                //}
                                //
                                //if (!empty($docModel)) {
                                //    $doc['amount'] = ($searchModel->isPrimeCosts)
                                //        ? $docModel->getTotalPurchaseAmountOnDate($searchModel->primeCostsArticle == InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT)
                                //        : $docModel->getTotalAmountWithNds();
                                //} else {
                                //    $doc['amount'] = '---';
                                //}

                                if ($doc['type'] == Documents::IO_TYPE_OUT) {
                                    $typeCss = 'income-item';
                                    $income = round($doc['amount'] / 100, 2);
                                    $expense = 0;
                                } else {
                                    $typeCss = 'expense-item';
                                    $income = 0;
                                    $expense = round($doc['amount'] / 100, 2);
                                }

                                return Html::checkbox("docId[{$doc['tb']}][]", false, [
                                    'class' => 'joint-checkbox ' . $typeCss,
                                    'value' => $doc['id'],
                                    'data' => [
                                        'income' => $income,
                                        'expense' => $expense,
                                    ],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'document_date',
                            'label' => 'Дата документа',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => '',
                            ],
                            'format' => 'raw',
                            'value' => function ($doc) {
                                return DateHelper::format($doc['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'document_number',
                            'label' => '№ документа',
                            'headerOptions' => [
                                'class' => 'sorting nowrap',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'document_number',
                            ],
                            'format' => 'raw',
                            'value' => function ($doc) use ($multiCompanyManager) {
                                $fullNumber = $doc['type'] == Documents::IO_TYPE_OUT
                                    ? $doc['document_number'] . $doc['document_additional_number']
                                    : $doc['document_number'];

                                switch ($doc['tb']) {
                                    case Act::tableName():
                                        $url = ['/documents/act/view', 'type' => $doc['type'], 'id' => $doc['id']];
                                        break;
                                    case PackingList::tableName():
                                        $url = ['/documents/packing-list/view', 'type' => $doc['type'], 'id' => $doc['id']];
                                        break;
                                    case Upd::tableName():
                                        $url = ['/documents/upd/view', 'type' => $doc['type'], 'id' => $doc['id']];
                                        break;
                                    case GoodsCancellation::tableName():
                                        $url = ['/documents/goods-cancellation/view', 'id' => $doc['id']];
                                        break;
                                    default:
                                        $url = '#';
                                }

                                return ($multiCompanyManager->canEdit($doc)) ?
                                    Html::a($fullNumber, $url, ['data-pjax' => 0, 'target' => '_blank']) :
                                    Html::tag('span', $fullNumber);
                            },
                        ],
                        [
                            'attribute' => 'doc_type',
                            'label' => 'Тип документа',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'filter' => [
                                '' => 'Все',
                                ProfitAndLossSearchModel::ACT_BLOCK => 'Акт',
                                ProfitAndLossSearchModel::PACKING_LIST_BLOCK => 'ТН',
                                ProfitAndLossSearchModel::UPD_BLOCK => 'УПД',
                                //ProfitAndLossSearchModel::GOODS_CANCELLATION_BLOCK => 'Списание товара',
                            ],
                            'value' => function ($doc) use ($searchModel) {
                                switch ($doc['tb']) {
                                    case Act::tableName():
                                        return 'Акт';
                                    case PackingList::tableName():
                                        return 'ТН';
                                    case Upd::tableName():
                                        return 'УПД';
                                    case GoodsCancellation::tableName():
                                        return 'Списание товара';
                                    default:
                                        return '';
                                }
                            },
                        ],
                        [
                            'attribute' => 'amount',
                            'label' => 'Сумма',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell',
                            ],
                            'value' => function ($doc) use ($searchModel) {
                                //switch ($doc['tb']) {
                                //    case 'act':
                                //        $docModel = Act::findOne($doc['id']);
                                //        break;
                                //    case 'packing_list':
                                //        $docModel = PackingList::findOne($doc['id']);
                                //        break;
                                //    case 'upd':
                                //        $docModel = Upd::findOne($doc['id']);
                                //        break;
                                //}
                                //
                                //if (!empty($docModel)) {
                                //    $doc['amount'] = ($searchModel->isPrimeCosts)
                                //        ? $docModel->getTotalPurchaseAmountOnDate($searchModel->primeCostsArticle == InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT)
                                //        : $docModel->getTotalAmountWithNds();
                                //} else {
                                //    $doc['amount'] = '---';
                                //}

                                return \common\components\TextHelper::invoiceMoneyFormat($doc['amount'], 2);
                            },
                        ],
                        [
                            's2width' => '250px',
                            'filter' => ['' => 'Все'] + $multiCompanyManager->getAllowedCompaniesList(),
                            'attribute' => 'company_id',
                            'label' => 'Компания',
                            'headerOptions' => [
                                'class' => $multiCompanyManager->getIsModeOn() ? '' : ' hidden',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'company-cell text-left' . ($multiCompanyManager->getIsModeOn() ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($multiCompanyManager) {
                                $companyName = $multiCompanyManager->getCompanyName($flows['company_id']);
                                return '<span title="' . htmlspecialchars($companyName) . '">' . $companyName . '</span>';
                            },
                        ],
                        [
                            's2width' => '250px',
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                            ],
                            'contentOptions' => [
                                'class' => 'contractor-cell'
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getContractorFilterItems(),
                            'value' => function ($doc) {
                                if ($contractor = Contractor::findOne($doc['contractor_id'])) {
                                    return '<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>';
                                }
                            },
                        ],
                        [
                            'attribute' => 'invoice_document_number',
                            'label' => 'Счёт №',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'format' => 'raw',
                            'value' => function ($doc) use ($multiCompanyManager) {
                                if ($invoice = Invoice::findOne($doc['invoice_document_number'])) {
                                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice])
                                    && $multiCompanyManager->canEdit($doc)) {
                                        return \yii\helpers\Html::a($invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ], ['data-pjax' => 0, 'target' => '_blank']);
                                    } else {
                                        return $invoice->fullNumber;
                                    }
                                }

                                return '';
                            }
                        ],
                        [
                            's2width' => '200px',
                            'attribute' => 'contractor_reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'reason-cell text-left',
                            ],
                            //'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()),
                            'format' => 'raw',
                            'value' => function ($doc) use ($searchModel) {

                                if ($doc['invoice_income_item_id']) {
                                    $article = InvoiceIncomeItem::findOne($doc['invoice_income_item_id']) ?: "id={$doc['invoice_income_item_id']}";
                                } elseif ($doc['invoice_expenditure_item_id']) {
                                    $article = InvoiceExpenditureItem::findOne($doc['invoice_expenditure_item_id']) ?: "id={$doc['invoice_expenditure_item_id']}";
                                } else {
                                    return '-';
                                }

                                return $article ? ('<span title="' . htmlspecialchars($article->fullName) . '">' . $article->fullName . '</span>') : '-';
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>