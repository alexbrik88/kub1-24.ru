<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\analytics\models\AbstractFinance;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\currency\Currency;

/** @var \frontend\modules\analytics\models\ProfitAndLossSearchModel $searchModel */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');

$showIncomeColumn = !empty($searchModel->income_item_id) || ($searchModel->isRevenue ?? false);
$showExpenseColumn = !empty($searchModel->expenditure_item_id);

$multiCompanyManager = &$searchModel->multiCompanyManager;
?>
<style>#flow-items-pjax .cash-bank-need-clarify { display:none; }</style>

<?php $pjax = Pjax::begin([
    'id' => 'flow-items-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>

<?php if (!empty($searchBy)) echo Html::hiddenInput('searchBy', $searchBy, ['id' => 'searchBy']) ?>
<?php if (!empty($isSearchByChanged)) echo Html::hiddenInput('isSearchByChanged', 1, ['id' => 'isSearchByChanged']) ?>

<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none mb-0">
        <div class="custom-scroll-table">
            <div class="table-wrap">
                <?= GridView::widget([
                    'id' => 'flow-items-pjax-grid',
                    'dataProvider' => $itemsDataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center joint-checkbox-td',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) {

                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $typeCss = 'income-item';
                                    $income = bcdiv($flows['tin_child_amount'] ?: $flows['amount_rub'], 100, 2);
                                    $expense = 0;
                                } else {
                                    $typeCss = 'expense-item';
                                    $income = 0;
                                    $expense = bcdiv($flows['tin_child_amount'] ?: $flows['amount_rub'], 100, 2);
                                }

                                return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                    'class' => 'joint-checkbox ' . $typeCss,
                                    'value' => $flows['id'],
                                    'data' => [
                                        'income' => $income,
                                        'expense' => $expense,
                                        'is_foreign' => (($flows['currency_name'] ?? "RUB") !== "RUB") ? 1 : 0,
                                    ],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'recognition_date',
                            'label' => 'Дата признания',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) {
                                return DateHelper::format($flows['recognition_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Дата оплаты',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) {
                                return DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'amountIncome',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . ($showIncomeColumn ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell',
                                'style' => 'display: ' . ($showIncomeColumn ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                    return TextHelper::invoiceMoneyFormat($flows['tin_child_amount'] ?: $flows['amount'], 2);
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'amountExpense',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . ($showExpenseColumn ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell',
                                'style' => 'display: ' . ($showExpenseColumn ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_EXPENSE) {
                                    return TextHelper::invoiceMoneyFormat($flows['tin_child_amount'] ?: $flows['amount'], 2);
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'payment_type',
                            'label' => 'Тип оплаты',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'filter' => ['' => 'Все'] + $searchModel->getPaymentTypeFilterItems(),
                            'value' => function ($flows) use ($searchModel) {

                                $currencySymbol = ($flows['currency_name'] === "RUB") ? '' : ArrayHelper::getValue(Currency::$currencySymbols, $flows['currency_name'], '');

                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        /* @var $checkingAccountant CheckingAccountant */
                                        $checkingAccountant = CheckingAccountant::find()->andWhere([
                                            'rs' => $model->rs,
                                            'company_id' => $model->company->id,
                                        ])->orderBy(['type' => SORT_ASC])->one();
                                        if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                            return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                    'class' => 'little_logo_bank',
                                                    'style' => 'display: inline-block;',
                                                ]);
                                        }
                                        return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                                    case CashOrderFlows::tableName():
                                        return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                                    case CashEmoneyFlows::tableName():
                                        return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                                    case AcquiringOperation::tableName():
                                        return 'Эквайринг';
                                    case CardOperation::tableName():
                                        return 'Карты';
                                    case CashBankForeignCurrencyFlows::tableName():
                                        return 'Банк  ' . $currencySymbol;
                                    case CashOrderForeignCurrencyFlows::tableName():
                                        return 'Касса ' . $currencySymbol;
                                    case CashEmoneyForeignCurrencyFlows::tableName():
                                        return 'E-money ' . $currencySymbol;
                                    default:
                                        return '';
                                }
                                return '';
                            },
                            's2width' => '150px',
                        ],
                        [
                            's2width' => '250px',
                            'filter' => ['' => 'Все'] + $multiCompanyManager->getAllowedCompaniesList(),
                            'attribute' => 'company_id',
                            'label' => 'Компания',
                            'headerOptions' => [
                                'class' => $multiCompanyManager->getIsModeOn() ? '' : ' hidden',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'company-cell text-left' . ($multiCompanyManager->getIsModeOn() ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($multiCompanyManager) {
                                $companyName = $multiCompanyManager->getCompanyName($flows['company_id']);
                                return '<span title="' . htmlspecialchars($companyName) . '">' . $companyName . '</span>';
                            },
                        ],
                        [
                            's2width' => '250px',
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                            ],
                            'contentOptions' => [
                                'class' => 'contractor-cell'
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getContractorFilterItems(),
                            'value' => function ($flows) use ($searchModel) {
                                /* @var $contractor Contractor */
                                $contractor = Contractor::findOne($flows['contractor_id']);
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $model = CashEmoneyFlows::findOne($flows['id']);
                                        break;
                                    case AcquiringOperation::tableName():
                                        $model = AcquiringOperation::findOne($flows['id']);
                                        break;
                                    case CardOperation::tableName():
                                        $model = CardOperation::findOne($flows['id']);
                                        break;
                                    default:
                                        return '';
                                }
                                return $contractor !== null ?
                                    ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                    (($model->cashContractor ?? null) ?
                                        ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                    );
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Назначение',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '30%',
                            ],
                            'contentOptions' => [
                                'class' => 'purpose-cell'
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if ($flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                } else {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        case AcquiringOperation::tableName():
                                        case CardOperation::tableName():
                                        default:
                                            $model = null;
                                    }
                                    if ($model && ($invoiceArray = $model->getInvoices()->all())) {
                                        $linkArray = [];
                                        foreach ($invoiceArray as $invoice) {
                                            $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ]);
                                        }

                                        return join(', ', $linkArray);
                                    }
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'billPaying',
                            'label' => 'Опл. счета',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($multiCompanyManager) {
                                if ($multiCompanyManager->canEdit($flows))
                                {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            $url = Url::to(['/cash/bank/update', 'id' => $flows['id']]);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            $url = Url::to(['/cash/order/update', 'id' => $flows['id']]);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            $url = Url::to(['/cash/e-money/update', 'id' => $flows['id']]);
                                            break;
                                        case AcquiringOperation::tableName():
                                            $model = null;
                                            $url = Url::to(['/acquiring/acquiring/update', 'id' => $flows['id']]);
                                            break;
                                        case CardOperation::tableName():
                                            $model = null;
                                            $url = Url::to(['/cards/operation/update', 'id' => $flows['id'], 'redirectUrl' => Url::current()]);
                                            break;
                                        default:
                                            $model = null;
                                            $url = null;
                                            break;
                                    }

                                    if (!$model)
                                        return '';

                                    $invoicesList = $model->getBillPaying(false);

                                    if ($model->getAvailableAmount() > 0) {
                                        $flows['is_internal_transfer'] = 0;
                                        return ($invoicesList ? ($invoicesList . '<br />') : '') .
                                            Html::a('<span class="red-link">Уточнить</span>', $url,
                                                [
                                                    'class' => 'update-flow-item red-link',
                                                    'title' => 'Прикрепите счета, которые были оплачены',
                                                    'data' => [
                                                        'toggle' => 'modal',
                                                        'target' => '#update-movement',
                                                        'pjax' => 0
                                                    ],
                                                ]);
                                    } else {
                                        return $invoicesList;
                                    }
                                }

                                return '';
                            },
                        ],
                        [
                            's2width' => '200px',
                            'attribute' => 'reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'reason-cell text-left',
                            ],
                            'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()),
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $reason = ($reasonModel = InvoiceIncomeItem::findOne($flows['income_item_id'])) ? $reasonModel->fullName : null;
                                } elseif ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                                    $reason = ($reasonModel = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ? $reasonModel->fullName : null;
                                } else {
                                    $reason = null;
                                }

                                return $reason ? ('<span title="' . htmlspecialchars($reason) . '">' . $reason . '</span>') : '-';
                            },
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}{delete}',
                            'headerOptions' => [
                                'width' => '3%',
                            ],
                            'contentOptions' => [
                                'class' => 'action-line nowrap',
                            ],
                            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                            'buttons' => [
                                'update' => function ($url, $flows, $key) use ($multiCompanyManager) {
                                    $url = 'javascript:;';
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $url = Url::to(['/cash/bank/update', 'id' => $flows['id']]);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $url = Url::to(['/cash/order/update', 'id' => $flows['id']]);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $url = Url::to(['/cash/e-money/update', 'id' => $flows['id']]);
                                            break;
                                        case AcquiringOperation::tableName():
                                            $url = Url::to(['/acquiring/acquiring/update', 'id' => $flows['id']]);
                                            break;
                                        case CardOperation::tableName():
                                            $url = Url::to(['/cards/operation/update', 'id' => $flows['id'], 'redirectUrl' => Url::current()]);
                                            break;
                                        case CashBankForeignCurrencyFlows::tableName():
                                            $url = Url::to(['/cash/bank/update', 'id' => $flows['id'], 'foreign' => 1]);
                                            break;
                                        case CashOrderForeignCurrencyFlows::tableName():
                                            $url = Url::to(['/cash/order/update', 'id' => $flows['id'], 'foreign' => 1]);
                                            break;
                                        case CashEmoneyForeignCurrencyFlows::tableName():
                                            $url = Url::to(['/cash/e-money/update', 'id' => $flows['id'], 'foreign' => 1]);
                                            break;
                                    }
                                    return ($multiCompanyManager->canEdit($flows)) ?
                                        Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', $url, [
                                            'title' => 'Изменить',
                                            'class' => 'update-flow-item button-clr link mr-1',
                                            'data' => [
                                                'toggle' => 'modal',
                                                'target' => '#update-movement',
                                                'pjax' => 0
                                            ],
                                        ]) : '';
                                },
                                'delete' => function ($url, $flows) use ($multiCompanyManager) {
                                    return ($multiCompanyManager->canEdit($flows)) ?
                                        \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                                                'class' => 'delete-flow-item button-clr link',
                                                'tag' => 'a',
                                            ],
                                            'options' => [
                                                'id' => 'delete-flow-item-' . $flows['id'],
                                                'class' => 'modal-delete-flow-item',
                                            ],
                                            'confirmUrl' => Url::to([
                                                '/analytics/finance-ajax/delete-flow-item',
                                                'id' => $flows['id'],
                                                'tb' => $flows['tb']
                                            ]),
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите удалить операцию?',
                                        ]) : '';
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>

<?php $this->registerJs('
    $(document).on("show.bs.modal", "#update-movement", function(event) {
        $(".alert-success").remove();
    
        var button = $(event.relatedTarget);
        var modal = $(this);
    
        modal.find(".modal-body").load(button.attr("href"));
    
    });
    
    $(document).on("hide.bs.modal", "#update-movement", function(event) {
        if (event.target.id === "update-movement") {
            $("#update-movement .modal-body").empty();
        }
    });
');