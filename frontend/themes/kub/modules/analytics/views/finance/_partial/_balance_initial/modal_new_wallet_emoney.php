<?php

use common\models\cash\Emoney;

$company = Yii::$app->user->identity->currentEmployeeCompany->company;
$newEmoney = new Emoney([
    'company_id' => $company->id
]);

echo $this->render('@frontend/views/emoney/_modal_form', [
    'emoney' => $newEmoney,
    'title' => 'Добавить e-money',
    'id' => 'add-company-emoney',
    'company' => $company,
    'enableAjaxValidation' => true,
    'actionUrl' => '/emoney/create-with-ajax-validation',
]);