<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use yii\web\View;
use common\models\employee\Config;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');
?>

<div id="sds-top" class="table-scroll-double top" style="display: none">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="sds-bottom" class="table-scroll-double bottom sticky-column">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <?= $this->render('payment_calendar_table_head_days', [
            'searchModel' => $searchModel,
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'year' => $searchModel->year
        ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            //if (!$userConfig->report_odds_own_funds && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK)
            //    continue;

            echo $this->render('payment_calendar_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'negativeRed' => true,
                'year' => $searchModel->year
            ]);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? [1,3,4,6] : [1,3,5]);

                echo $this->render('payment_calendar_table_row_days', [
                    'level' => 2,
                    'searchModel' => $searchModel,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'year' => $searchModel->year
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $floorKey2 = "floor-item-{$itemId}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                    echo $this->render('payment_calendar_table_row_days', [
                        'level' => 3,
                        'searchModel' => $searchModel,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dDays' => $d_days,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => false, // ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY && empty($level3['levels'])),
                        'isOpenable' => (!empty($level3['levels'])),
                        'floorKey' => $floorKey2,
                        'year' => $searchModel->year
                    ]);

                    if (empty($level3['levels']))
                        continue 1;

                    // LEVEL 4
                    foreach ($level3['levels'] as $subItemId => $level4) {

                        echo $this->render('payment_calendar_table_row_days', [
                            'level' => 4,
                            'searchModel' => $searchModel,
                            'title' => $level4['title'],
                            'data' => $level4['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dDays' => $d_days,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $subItemId,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'isSortable' => false,
                            'year' => $searchModel->year
                        ]);
                    }
                }
            }

            // GROWING
            echo $this->render('payment_calendar_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $growingData[$paymentType]['title'],
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false,
                'hidePlanDays' => true,
                'year' => $searchModel->year,
                'negativeRed' => true
            ]);
        }

        // TOTAL BY MONTH
        echo $this->render('payment_calendar_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag net-cash-flow',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'canHover' => false,
            'year' => $searchModel->year,
            'negativeRed' => true
        ]);

        // TOTAL BY YEAR
        echo $this->render('payment_calendar_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            'year' => $searchModel->year,
            'hidePlanDays' => true,
            'negativeRed' => true
        ]);

        ?>
        </tbody>

    </table>
    </div>
</div>