<?php

use common\components\helpers\ArrayHelper;use common\components\helpers\Html;
use common\components\TextHelper;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\themes\kub\helpers\Icon;
use kartik\checkbox\CheckboxX;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel ProfitAndLossSearchModel
 * @var $data array
 * @var $activeTab int
 * @var $floorMap array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = $searchModel->isCurrentYear;
$floorMap = $floorMap ?? [];

if (!empty($floorMap)) {
    $d_all_rows = ArrayHelper::getValue($floorMap, 'all-rows');
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_all_rows = false;
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

// for taxable view
$AMOUNT_KEY = ($activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'amountTax' : 'amount';
$GROUPED_AMOUNT_KEY = ($activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'groupedAmountTax' : 'groupedAmount';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');

// MONTH GROUPS
$monthGroupBy = $searchModel->getMonthGroupBy();
$monthGroupNames = $searchModel->getGroupByNames();
$countMonthGroups = count($monthGroupNames) ?: 0;
$colspanQuarters = 'colspan="' . (3 * ($countMonthGroups + 1)) . '"';
$colspanMonths = 'colspan="' . (1 * ($countMonthGroups + 1)) . '"';
$buttonColspan = 'data-columns-count="'.(3 * ($countMonthGroups + 1)).'" data-columns-count-collapsed="'.(1 * ($countMonthGroups + 1)).'"';
$firstCellRowspan = ($countMonthGroups > 1) ? 3 : 2;
$tdGroupDataAttribute = "data-{$monthGroupBy}=\"%s\"";
$isFirstColumnFixed = true;
?>

<div class="table-wrap">
    <table class="scrollable-table double-scrollbar-top table-bleak flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <tr class="quarters-flow-of-funds">
            <th class="fixed-column pl-2 pr-2 pt-3 pb-3 align-top" rowspan="<?= $firstCellRowspan ?>">
                <button class="table-collapse-btn button-clr ml-1 <?= ($d_all_rows) ? 'active' : '' ?>" type="button" data-collapse-all-tiger data-target="all-rows">
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1">Статьи</span>
                </button>
            </th>
            <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? $colspanQuarters : $colspanMonths ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-tiger data-target="first-cell" <?= $buttonColspan ?>>
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                </button>
            </th>
            <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? $colspanQuarters : $colspanMonths ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-tiger data-target="second-cell" <?= $buttonColspan ?>>
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                </button>
            </th>
            <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? $colspanQuarters : $colspanMonths ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-tiger data-target="third-cell" <?= $buttonColspan ?>>
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                </button>
            </th>
            <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? $colspanQuarters : $colspanMonths ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-tiger data-target="fourth-cell" <?= $buttonColspan ?>>
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                </button>
            </th>
            <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $colspanMonths ?>>
                <span class="text-grey weight-700 ml-1 nowrap">
                    <?= $searchModel->year ?>
                </span>
            </th>
        </tr>
        <tr>
            <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $month): ?>
                <?php $quarter = (int)ceil($monthNumber / 3); ?>
                <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" <?= $colspanMonths ?>data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $monthNumber; ?>">
                    <div class="pl-1 pr-1"><?= $month; ?></div>
                </th>
                <?php if ($monthNumber % 3 == 0): ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" <?= $colspanMonths ?>data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                        <div class="pl-1 pr-1">Итого</div>
                    </th>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php if ($countMonthGroups > 1): ?>
                <th class="pl-2 pr-2" <?= $colspanMonths ?>>
                    <div class="pl-1 pr-1">Итого</div>
                </th>
            <?php else: ?>
                <th class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Итого</div>
                </th>
            <?php endif; ?>

        </tr>
        <?php if ($countMonthGroups > 1): ?>
            <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $month): ?>
                <?php $quarter = (int)ceil($monthNumber / 3); ?>
                <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $monthNumber; ?>" style="border-top:1px solid #f2f3f7">
                        <div class="pl-1 pr-1"><?= $monthGroupName; ?></div>
                    </th>
                <?php endforeach; ?>
                <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $monthNumber; ?>" style="border-top:1px solid #f2f3f7">
                    <div class="pl-1 pr-1">Итого</div>
                </th>
                <?php if ($monthNumber % 3 == 0): ?>
                    <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" style="border-top:1px solid #f2f3f7">
                        <div class="pl-1 pr-1"><?= $monthGroupName ?></div>
                    </th>
                    <?php endforeach; ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" style="border-top:1px solid #f2f3f7">
                        <div class="pl-1 pr-1">Итого</div>
                    </th>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                <th class="pl-2 pr-2 td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> style="border-top:1px solid #f2f3f7">
                    <div class="pl-1 pr-1"><?= $monthGroupName ?></div>
                </th>
            <?php endforeach; ?>
            <th class="pl-2 pr-2" style="border-top:1px solid #f2f3f7">
                <div class="pl-1 pr-1">Итого</div>
            </th>

        <?php endif; ?>
        </thead>

        <tbody>

        <?php $floorID = 0; ?>
        <?php foreach ($data as $rowID => $row): ?>
            <?php
            $currentQuarter = (int)ceil(date('m') / 3);
            $suffix = ArrayHelper::remove($row, 'suffix');
            $options = ArrayHelper::remove($row, 'options', []);
            $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
            $classTextGrey = (isset($options['class']) && strpos($options['class'], 'text-grey') !== false);
            $classBold = (!$includedRow && !$classTextGrey) ? 'weight-700' : '';

            if (!empty($row['data-movement-type'])) {
                $canClick = 'can-click';
                $dataClick = implode(' ', [
                    'data-article="' . $row['data-article'] . '"',
                    'data-movement-type="' . $row['data-movement-type'] . '"'
                ]);
            } elseif (!empty($row['data-hover_text'])) {
                $canClick = 'can-click';
                $dataClick = 'data-hover_text="'.$row['data-hover_text'].'"';
            } else {
                $canClick = $dataClick = '';
            }
            $hasSubitems = !empty($row['items']);
            ?>

            <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']): ?>

                <?php $floorID++; ?>
                <?php $floorKey1 = "first-floor-{$floorID}"; ?>
                <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1); ?>

                <tr class="not-drag expenditure_type sub-block">
                    <td class="fixed-column pl-2 pr-2 pt-3 pb-3">
                        <button class="table-collapse-btn button-clr ml-1 text-left nowrap <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-tiger data-target="<?= $floorKey1 ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="d-block weight-700 text_size_14 ml-1"><?= $row['label']; ?></span>
                        </button>
                    </td>

                    <?php IF ($monthGroupNames): /* MONTH GROUPS */ ?>

                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $quarter = (int)ceil($monthNumber / 3); ?>
                            <?php $monthAmountTotal = $row[$AMOUNT_KEY][$monthNumber] ?? 0; ?>
                            <?php $quarterAmountTotal = $row[$AMOUNT_KEY]['quarter-' . $quarter] ?? 0; ?>

                            <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                <?php $amount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId][$monthNumber] ?? 0; ?>
                                <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                    <div class="pl-1 pr-1 weight-700">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endforeach; ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 weight-700">
                                    <?= TextHelper::invoiceMoneyFormat($monthAmountTotal, 2) . $suffix; ?>
                                </div>
                            </td>

                            <?php if ($monthNumber % 3 == 0): ?>
                                <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                    <?php $quarterAmount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId]['quarter-' . $quarter] ?? 0; ?>
                                    <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                        <div class="pl-1 pr-1 weight-700">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterAmount, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endforeach; ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 weight-700">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterAmountTotal, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>

                        <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                            <?php $yearAmount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId]['total'] ?? 0; ?>
                            <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                                <div class="pl-1 pr-1 weight-700">
                                    <?php echo TextHelper::invoiceMoneyFormat($yearAmount, 2) . $suffix; ?>
                                </div>
                            </td>
                        <?php endforeach; ?>
                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 weight-700">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>

                    <?php ELSE: /* MONTH SINGLE */ ?>
                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php
                            $quarter = (int)ceil($monthNumber / 3);
                            $amount = $row[$AMOUNT_KEY][$monthNumber] ?? 0;
                            ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 weight-700">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                </div>
                            </td>
                            <?php $quarterSum = isset($row[$AMOUNT_KEY]['quarter-' . $quarter]) ? $row[$AMOUNT_KEY]['quarter-' . $quarter] : 0; ?>
                            <?php if ($monthNumber % 3 == 0): ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 weight-700">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 weight-700">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>

                    <?php ENDIF; ?>

                </tr>

            <?php else: ?>

                <?php $floorKey2 = "second-floor-{$rowID}"; ?>
                <?php $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2); ?>

                <tr class="item-block <?= ($includedRow && !$isOpenedFloor1) ? 'd-none' : '' ?>" <?= ($includedRow) ? 'data-id="'.$floorKey1.'"' : '' ?> >
                    <td class="fixed-column pl-2 pr-1 pt-3 pb-3">
                        <?php if ($hasSubitems): ?>
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 mr-2 nowrap">
                                    <button class="table-collapse-btn button-clr icon-grey nowrap <?= $isOpenedFloor2 ? 'active':'' ?>" type="button" data-collapse-row-tiger data-target="<?= $floorKey2 ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="text-grey text_size_14 ml-1"><?= $row['label']; ?></span>
                                    </button>
                                </div>
                            </div>
                        <?php else: ?>
                            <span class="d-block <?= $classBold ?: 'text-grey' ?> text_size_14 nowrap <?= ($classBold || $classTextGrey) ? 'ml-1 mr-1' : 'm-l-purse' ?>">
                            <?= $row['label']; ?>
                            <?php if ('Налог на прибыль' === $row['label']): ?>
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_profit_loss_tax_item',
                                ]) ?>
                            <?php endif; ?>
                        </span>
                        <?php endif; ?>
                    </td>

                    <?php IF (!empty($row[$GROUPED_AMOUNT_KEY])): /* MONTH GROUPS */ ?>

                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $quarter = (int)ceil($monthNumber / 3); ?>
                            <?php $monthAmountTotal = $row[$AMOUNT_KEY][$monthNumber] ?? 0; ?>
                            <?php $quarterAmountTotal = $row[$AMOUNT_KEY]['quarter-' . $quarter] ?? 0; ?>

                            <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                <?php $amount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId][$monthNumber] ?? 0; ?>
                                <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?> td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endforeach; ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?= TextHelper::invoiceMoneyFormat($monthAmountTotal, 2) . $suffix; ?>
                                </div>
                            </td>

                            <?php if ($monthNumber % 3 == 0): ?>
                                <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                    <?php $quarterAmount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId]['quarter-' . $quarter] ?? 0; ?>
                                    <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?> td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterAmount, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endforeach; ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterAmountTotal, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>

                        <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                            <?php $yearAmount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId]['total'] ?? 0; ?>
                            <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> <?= $dataClick ?>>
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?php echo TextHelper::invoiceMoneyFormat($yearAmount, 2) . $suffix; ?>
                                </div>
                            </td>
                        <?php endforeach; ?>
                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 <?= $classBold ?>">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>

                    <?php ELSE: /* MONTH SINGLE */ ?>

                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php
                            $quarter = (int)ceil($monthNumber / 3);
                            $amount = isset($row[$AMOUNT_KEY][$monthNumber]) ? $row[$AMOUNT_KEY][$monthNumber] : 0;
                            ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                </div>
                            </td>
                            <?php $quarterSum = isset($row[$AMOUNT_KEY]['quarter-' . $quarter]) ? $row[$AMOUNT_KEY]['quarter-' . $quarter] : 0; ?>
                            <?php if ($monthNumber % 3 == 0): ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 <?= $classBold ?>">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>

                    <?php ENDIF; ?>
                    <!---->

                </tr>

                <!-- SUBITEMS -->
                <?php if ($hasSubitems): ?>
                    <?php foreach ($row['items'] as $r):  ?>
                    <?php
                    $dataClick = implode(' ', [
                        'data-article="' . $r['data-article'] . '"',
                        'data-movement-type="' . $r['data-movement-type'] . '"'
                    ]); ?>
                    <tr class="item-block <?= ($includedRow && !$isOpenedFloor2) ? 'd-none' : '' ?>" <?= ($includedRow) ? 'data-id="'.$floorKey2.'"' : '' ?> >
                        <td class="fixed-column pl-2 pr-1 pt-3 pb-3">
                            <span class="d-block <?= $classBold ?: 'text-grey' ?> text_size_12 nowrap <?= $classBold ? 'ml-1 mr-1' : ($includedRow ? 'm-l-purse-big':'m-l-purse') ?>">
                                <?= $r['label']; ?>
                            </span>
                        </td>

                        <?php IF (!empty($r[$GROUPED_AMOUNT_KEY])): /* MONTH GROUPS ////////////////////////////////////////////////////////////////////////////////////////// */ ?>

                            <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                                <?php $quarter = (int)ceil($monthNumber / 3); ?>
                                <?php $monthAmountTotal = $row[$AMOUNT_KEY][$monthNumber] ?? 0; ?>
                                <?php $quarterAmountTotal = $row[$AMOUNT_KEY]['quarter-' . $quarter] ?? 0; ?>

                                <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                    <?php $amount = $r[$GROUPED_AMOUNT_KEY][$monthGroupId][$monthNumber] ?? 0; ?>
                                    <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?> td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endforeach; ?>
                                <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?= TextHelper::invoiceMoneyFormat($monthAmountTotal, 2) . $suffix; ?>
                                    </div>
                                </td>

                                <?php if ($monthNumber % 3 == 0): ?>
                                    <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                        <?php $quarterAmount = $r[$GROUPED_AMOUNT_KEY][$monthGroupId]['quarter-' . $quarter] ?? 0; ?>
                                        <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?> td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                            <div class="pl-1 pr-1 <?= $classBold ?>">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterAmount, 2) . $suffix; ?>
                                            </div>
                                        </td>
                                    <?php endforeach; ?>
                                    <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterAmountTotal, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>

                            <?php endforeach; ?>

                            <?php foreach ($monthGroupNames as $monthGroupId => $monthGroupName): ?>
                                <?php $yearAmount = $row[$GROUPED_AMOUNT_KEY][$monthGroupId]['total'] ?? 0; ?>
                                <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap td-filter" <?= sprintf($tdGroupDataAttribute, $monthGroupId) ?> <?= $dataClick ?>>
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?php echo TextHelper::invoiceMoneyFormat($yearAmount, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endforeach; ?>
                            <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                    <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                                </div>
                            </td>

                        <?php ELSE: /* MONTH SINGLE ////////////////////////////////////////////////////////////////////////////////////////// */ ?>

                            <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                                <?php
                                $quarter = (int)ceil($monthNumber / 3);
                                $amount = isset($r[$AMOUNT_KEY][$monthNumber]) ? $r[$AMOUNT_KEY][$monthNumber] : 0;
                                ?>
                                <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                                <?php $quarterSum = isset($r[$AMOUNT_KEY]['quarter-' . $quarter]) ? $r[$AMOUNT_KEY]['quarter-' . $quarter] : 0; ?>
                                <?php if ($monthNumber % 3 == 0): ?>
                                    <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?php $totalSum = isset($r[$AMOUNT_KEY]['total']) ? $r[$AMOUNT_KEY]['total'] : 0; ?>
                                    <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                                </div>
                            </td>

                        <?php ENDIF; /* ////////////////////////////////////////////////////////////////////////////////////////// */ ?>

                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!-- end of SUBITEMS -->
            <?php endif; ?>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>

<div style="display: none">
    <div id="tooltip_profit_loss_tax_item">
        Налог на прибыль считается по операциям<br/>
        с пометкой "Для бухгалтерии". Сумма налога<br/>
        может отличаться от налога, рассчитанного вашей<br/>
        бухгалтерией, если вы не все данные внесли в КУБ24.
    </div>
</div>

<style>
    .scrollable-table-clone { margin-top:2px;}
</style>