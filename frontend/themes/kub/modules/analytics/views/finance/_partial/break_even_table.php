<?php

use \frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\TextHelper;
use frontend\modules\analytics\models\BreakEvenSearchModel;

/* @var $this yii\web\View
 * @var $searchModel BreakEvenSearchModel
 * @var $data array
 * @var $floorMap array
 * @var $total array
 * @var $tabVievClass string
 */

if ($total['revenue'] - $total['variableCosts'] > 0 && $total['quantity'] > 0) {
    $breakEvenQ = $total['fixedCosts'] / ($total['revenue'] - $total['variableCosts']) * $total['quantity'];
    $breakEvenA = $breakEvenQ * $total['revenue'] / $total['quantity'];
} else {
    $breakEvenQ = $breakEvenA = 0;
}
if ($total['revenuePrev'] - $total['variableCostsPrev'] > 0 && $total['quantityPrev'] > 0) {
    $breakEvenQPrev = $total['fixedCostsPrev'] / ($total['revenuePrev'] - $total['variableCostsPrev']) * $total['quantityPrev'];
    $breakEvenAPrev = $breakEvenQPrev * $total['revenuePrev'] / $total['quantityPrev'];
} else {
    $breakEvenQPrev = $breakEvenAPrev = 0;
}

$tableRows = [
    [
        'key' => 'revenue',
        'label' => 'Выручка, ₽',
        'prevValue' => $total['revenue'],
        'value' => null
    ],
    [
        'key' => 'break-even-amount',
        'label' => 'Точка безубыточности, ₽',
        'prevValue' => $breakEvenAPrev,
        'value' => null
    ],
    [
        'key' => 'break-even-quantity',
        'label' => 'Точка безубыточности, ед./месяц',
        'prevValue' => $breakEvenQPrev,
        'value' => null
    ],
    [
        'key' => 'critical-average-check',
        'label' => 'Средний чек критический, ₽',
        'prevValue' => ($total['quantityPrev'] > 0) ? ($breakEvenAPrev / $total['quantityPrev']) : 0,
        'value' => null,
        'iconQuestion' => Html::tag('span', Icon::QUESTION, [
            'title' => 'Минимальная цена продажи<br/> для достижения безубыточного уровня',
            'title-as-html' => 1
        ])
    ],
    [
        'key' => 'financial-safety-amount',
        'label' => 'Запас финансовой прочности, ₽',
        'prevValue' => $total['revenuePrev'] - $breakEvenAPrev,
        'value' => null
    ],
    [
        'key' => 'financial-safety-quantity',
        'label' => 'Запас финансовой прочности, ед.',
        'prevValue' => $total['quantityPrev'] - $breakEvenQPrev,
        'value' => null,
    ],
    [
        'key' => 'financial-safety-percent',
        'label' => 'Запас финансовой прочности, %',
        'prevValue' => ($total['revenuePrev'] > 0) ? (($total['revenuePrev'] - $breakEvenAPrev) / $total['revenuePrev'] * 100) : 0,
        'value' =>  null,
        'suffix' => '%',
        'iconQuestion' => Html::tag('span', Icon::QUESTION, [
            'title' => 'Максимальный размер скидки от первоначальной цены',
            'title-as-html' => 1
        ])
    ],
]

?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table id="break-even-table" class="flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th width="30%" class="pl-2 pr-2 align-middle" rowspan="2">
                    <span class="d-block weight-700 text_size_14 ml-1">
                        Показатели финансовой прочности
                    </span>
                </th>
                <th class="pl-2 pr-2 align-top">
                    <div class="pl-1 pr-1 weight-700 nowrap">
                        <?= AbstractFinance::$month[$searchModel->month].' '.$searchModel->year; ?>
                    </div>
                </th>
                <th class="pl-2 pr-2 align-top" colspan="2">
                    <div class="pl-1 pr-1 weight-700 nowrap">
                        Прогноз месячный
                    </div>
                </th>
                <th class="pl-2 pr-2 align-top" colspan="2">
                    <div class="pl-1 pr-1 weight-700 nowrap">
                        Изменение
                    </div>
                </th>
            </tr>
            <tr>
                <th with="15%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1 nowrap">Факт</div>
                </th>
                <th with="15%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1 nowrap">Расчетный</div>
                </th>
                <th with="15%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1 nowrap">По графику</div>
                </th>
                <th with="15%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1 nowrap">Абсолютное</div>
                </th>
                <th with="15%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1 nowrap">%%</div>
                </th>
            </tr>
            </thead>

            <tbody>

                <?php foreach ($tableRows as $row): ?>
                <tr class="<?=($row['key'])?>">
                    <td class="pl-2 pr-1 pt-3 pb-3">
                        <div class="d-block text_size_14 ml-1 weight-700">
                            <?= $row['label']; ?>
                            <?= $row['iconQuestion'] ?? null ?>
                        </div>
                    </td>
                    <td class="prev-amount pl-2 pr-2 nowrap">
                        <div class="pl-1 pr-1">
                            <?= TextHelper::moneyFormat($row['prevValue'], 2) ?><?=($row['suffix'] ?? null)?>
                        </div>
                    </td>
                    <td class="curr-amount pl-2 pr-2 nowrap">
                        <div class="pl-1 pr-1">
                            <?= TextHelper::moneyFormat($row['value'], 2) ?><?=($row['suffix'] ?? null)?>
                            <!--js calc (td > div)-->
                        </div>
                    </td>
                    <td class="calc-amount pl-2 pr-2 nowrap">
                        <div class="pl-1 pr-1">
                            <!--js calc (td > div)-->
                        </div>
                    </td>
                    <td class="variation-amount pl-2 pr-2 nowrap">
                        <div class="pl-1 pr-1">
                            <!--js calc (td > div)-->
                        </div>
                    </td>
                    <td class="variation-percent pl-2 pr-2 nowrap">
                        <div class="pl-1 pr-1">
                            <span><!--js calc (td > div > span)--></span>%
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>