<?php

use common\components\helpers\Html;
use frontend\modules\reference\models\ArticleDropDownForm;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use frontend\modules\reference\models\ArticlesSearch;
use common\components\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $model ArticleDropDownForm
 * @var $type int
 * @var $block int
 */

Pjax::begin([
    'id' => 'pjax-break-even-article-modal',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false,
]) ?>
    <div class="article-form">
        <?php $form = ActiveForm::begin([
            'id' => 'article-form',
            'enableClientValidation' => false,
            'action' => ['/analytics/finance-ajax/add-article-to-break-even', 'block' => $block],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            'options' => [
                'data-pjax' => true,
            ],
        ]) ?>

        <?= $form->field($model, 'isNew', [
            'options' => [
                'class' => 'form-group',
            ]])
            ->radioList([
                ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN => 'Добавить статью ' . ($type == ArticlesSearch::TYPE_INCOME ? 'приходов' : 'расходов') . ' из имеющегося списка',
                ArticleDropDownForm::TYPE_EXISTS_ITEM => 'Добавить статьи ' . ($type == ArticlesSearch::TYPE_INCOME ? 'приходов' : 'расходов') . ', которые еще у вас не подключены',
                ArticleDropDownForm::TYPE_NEW_ITEM => 'Добавить свою статью ' . ($type == ArticlesSearch::TYPE_INCOME ? 'прихода' : 'расхода'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
                    $result = Html::radio($name, $checked, [
                        'class' => 'flow-type-toggle-input',
                        'value' => $value,
                        'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                        'labelOptions' => [
                            'class' => 'label mb-3 mr-3 mt-2',
                        ],
                    ]);

                    if ($value === ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN) {
                        $result .= $form->field($model, 'items2', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN ? 'block;' : 'none;'),
                            ],
                        ])->checkboxList(ArrayHelper::map($model->getExistsBreakEvenItems(), 'id', 'name'))->label(false);
                    }
                    elseif ($value === ArticleDropDownForm::TYPE_EXISTS_ITEM) {
                        $result .= $form->field($model, 'items', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_EXISTS_ITEM ? 'block;' : 'none;'),
                            ],
                        ])->checkboxList(ArrayHelper::map($model->getExistsItems(), 'id', 'name'))->label(false);
                    }
                    else {
                        $result .= $form->field($model, 'name', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' &&  $model->isNew == ArticleDropDownForm::TYPE_NEW_ITEM ? 'block;' : 'none;'),
                            ],
                            'inputOptions' => [
                                'placeholder' => 'Введите название статьи',
                            ],
                        ])->label(false);
                    }

                    return $result;
                },
            ])->label(false); ?>

        <span>
            Всю информацию об используемых и неиспользуемых статьях смотрите в
            <?= Html::a('справочнике статей', Url::to(['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY])); ?>
        </span>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                    data-dismiss="modal">Отменить
            </button>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php Pjax::end() ?>