<?php

use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_break_even_help ?? false;
$showChartPanel = $userConfig->report_break_even_chart ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px; width:175px;">
                <?= \yii\bootstrap\Html::beginForm(['break-even-point'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'yearMonth',
                    'data' => array_reverse($searchModel->getYearMonthFilter(), true),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_break_even_chart">
    <div class="pt-3 pb-3">
        <?= $this->render('break_even_chart', [
            'searchModel' => $searchModel,
            'total' => $total
        ]); ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_break_even_help">
    <div class="pt-4 pb-3">
        <?= $this->render('break_even_help_panel'); ?>
    </div>
</div>

<?php $this->registerJs(<<<JS
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
JS
);
?>