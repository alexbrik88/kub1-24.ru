<?php
/* @var $this yii\web\View
 * @var $searchModel OperationalEfficiencySearchModel
 * @var $activeTab int
 * @var $user \common\models\employee\Employee
 */

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\operationalEfficiency\OperationalEfficiencySearchModel;

$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = $searchModel->isCurrentYear;
$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];
$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];
$tabViewClass = $user->config->getTableViewClass('table_view_operational_efficiency');
?>
<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="table-bleak flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Показатель</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <span class="text-grey weight-700 ml-1 nowrap">
                        <?= $searchModel->year ?>
                    </span>
                </th>
            </tr>
            <tr>
                <?php foreach (OperationalEfficiencySearchModel::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Итого</div>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php $floorID = 0; ?>
            <?php foreach ($searchModel->handleItems() as $rowId => $row): ?>
                <?php
                $currentQuarter = (int)ceil(date('m') / 3);
                $options = ArrayHelper::remove($row, 'options', []);
                $hasSubitems = !empty($row['items']);
                $flor = "first-floor-{$rowId}";
                ?>
                <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']): ?>
                    <tr class="not-drag expenditure_type sub-block" style="border-bottom: 1px solid #E5E5E5;">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <button class="table-collapse-btn button-clr ml-1 text-left" type="button" data-collapse-row-trigger data-target="<?= $flor ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="d-block weight-700 text_size_14 ml-1"><?= $row['label']; ?></span>
                            </button>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (OperationalEfficiencySearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 weight-700"></div>
                            </td>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 weight-700"></div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td class="total-value pl-2 pr-2 pt-3 pb-3 nowrap">
                            <div class="pl-1 pr-1 weight-700"></div>
                        </td>
                    </tr>
                    <?php if (isset($row['items'])): ?>
                        <?php foreach ($row['items'] as $subItem): ?>
                            <tr class="item-block d-none" data-id="<?= $flor ?>">
                                <td class="pl-2 pr-1 pt-3 pb-3">
                                    <span class="d-block text-grey text_size_14 m-l-purse">
                                        <?= $subItem['label']; ?>
                                    </span>
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (OperationalEfficiencySearchModel::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = (int)ceil($key / 3);
                                    $amount = isset($subItem['amount'][$monthNumber]) ? $subItem['amount'][$monthNumber] : 0;
                                    ?>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?> <?= $amount < 0 ? 'red-column' : null; ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= (int)$monthNumber ?>">
                                        <div class="pl-1 pr-1 <?= $subItem['class'] ?? null ?>">
                                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                        </div>
                                    </td>
                                    <?php $quarterSum = isset($subItem['amount']['quarter-' . $quarter]) ? $subItem['amount']['quarter-' . $quarter] : 0; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?> <?= $quarterSum < 0 ? 'red-column' : null; ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                            <div class="pl-1 pr-1 <?= $subItem['class'] ?? null ?>">
                                                <?= TextHelper::invoiceMoneyFormat($quarterSum, 2); ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="total-value pl-2 pr-2 pt-3 pb-3 nowrap">
                                    <?php $totalSum = isset($subItem['amount']['total']) ? $subItem['amount']['total'] : 0; ?>
                                    <div class="pl-1 pr-1 <?= $subItem['class'] ?? null ?> <?= $totalSum < 0 ? 'red-column' : null; ?>">
                                        <?= TextHelper::invoiceMoneyFormat($totalSum, 2); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
