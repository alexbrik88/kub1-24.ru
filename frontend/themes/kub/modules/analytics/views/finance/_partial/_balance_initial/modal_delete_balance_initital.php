<?php
use common\models\balance\BalanceArticle;
use yii\helpers\Url;
use common\components\helpers\Html;
use yii\bootstrap4\Modal;
?>

<?php Modal::begin([
    'id' => 'many-delete-balance-articles-fixed',
    'closeButton' => false,
    'options' => ['class' => 'fade confirm-modal'],
]); ?>
    <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные основные средства?</h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete ladda-button',
            'data-url' => Url::to(['/analytics/balance-articles/many-delete', 'type' => BalanceArticle::TYPE_FIXED_ASSERTS]),
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'many-delete-balance-articles-non-material',
    'closeButton' => false,
    'options' => ['class' => 'fade confirm-modal'],
]); ?>
    <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные нематериальные активы?</h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete ladda-button',
            'data-url' => Url::to(['/analytics/balance-articles/many-delete', 'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS]),
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end(); ?>