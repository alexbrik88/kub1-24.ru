<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\employee\Config;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = $userConfig->getTableViewClass('table_view_finance_pc');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table by_purse table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
            <?= $this->render('odds2_table_head', [
                'searchModel' => $searchModel,
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
            ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            if (!$userConfig->report_odds_own_funds && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK)
                continue;

            echo $this->render('payment_calendar_table_row', [
                'level' => 1,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'negativeRed' => true,
                'positiveRed' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK),
                'year' => $searchModel->year,
            ]);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? [1,3,4,6] : [1,3,5]);

                echo $this->render('payment_calendar_table_row', [
                    'level' => 2,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'year' => $searchModel->year
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $floorKey2 = "floor-item-{$itemId}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                    echo $this->render('payment_calendar_table_row', [
                        'level' => 3,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dMonthes' => $d_monthes,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY && empty($level3['levels'])),
                        'isOpenable' => (!empty($level3['levels'])),
                        'floorKey' => $floorKey2,
                        'year' => $searchModel->year
                    ]);

                    if (empty($level3['levels']))
                        continue 1;

                    // LEVEL 4
                    foreach ($level3['levels'] as $subItemId => $level4) {

                        echo $this->render('payment_calendar_table_row', [
                            'level' => 4,
                            'title' => $level4['title'],
                            'data' => $level4['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dMonthes' => $d_monthes,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $subItemId,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                        ]);
                    }
                }
            }

            if (isset($growingData[$paymentType]['hide']))
                continue;

            // GROWING
            echo $this->render('payment_calendar_table_row', [
                'level' => 1,
                'title' => $growingData[$paymentType]['title'] . ' (План)',
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false,
                'negativeRed' => true,
                'year' => $searchModel->year,
                'hidePlanMonths' => true
            ]);
        }

        // TOTAL BY MONTH
        echo $this->render('payment_calendar_table_row', [
            'level' => 1,
            'title' => $totalData['month']['title'] . ' (План)',
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag net-cash-flow',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'canHover' => false,
            'year' => $searchModel->year,
            'negativeRed' => true
        ]);

        // TOTAL BY YEAR
        echo $this->render('payment_calendar_table_row', [
            'level' => 1,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            'negativeRed' => true,
            'year' => $searchModel->year,
            'hidePlanMonths' => true
        ]);

        ?>
        </tbody>
    </table>
    </div>
</div>