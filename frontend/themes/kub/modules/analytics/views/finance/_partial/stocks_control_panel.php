<?php
use common\components\helpers\Html;
use frontend\modules\analytics\models\StocksSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$SHOW_BY_GROUPS = $userConfig->report_abc_show_groups;
?>

<div class="row pt-1" style="margin-bottom: -5px">
    <div class="col-6">
        <div class="d-flex flex-nowrap">
            <div>
                <?= TableConfigWidget::widget([
                    'sortingItemsTitle' => 'Выводить',
                    'sortingItems' => [
                        [
                            'attribute' => 'show_groups',
                            'label' => 'По группам товара',
                            'checked' => $userConfig->report_abc_show_groups,
                            'data-url' => 'stocks'
                        ],
                        [
                            'attribute' => 'dont_show_groups',
                            'label' => 'По товарам',
                            'checked' => !$userConfig->report_abc_show_groups,
                            'data-url' => 'stocks'
                        ],
                    ],
                    'showItems' => [
                        [
                            'attribute' => 'product_zeroes_turnover',
                            'label' => 'Товары с нулями',
                        ]
                    ]
                ]); ?>
            </div>
            <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
                <div>
                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                        Url::to(['/analytics/finance/get-xls',
                            'type' => StocksSearch::REPORT_ID,
                            'io_type' => null,
                            'year' => $searchModel->year]), [
                            'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2 disabled',
                            'title' => 'Скачать в Excel',
                            'data-pjax' => 0
                        ]); ?>
                </div>
            <?php endif; ?>
            <div class="ml-2">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_stocks']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'id' => 'stocks-title-filter',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
                'data-pjax' => true,
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск по названию товара...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>