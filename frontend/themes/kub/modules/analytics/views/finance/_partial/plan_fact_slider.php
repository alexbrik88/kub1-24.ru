<div class="row align-items-center flex-wrap">
    <div class="column text_size_14 weight-700 pr-0">0%</div>
    <div class="column flex-grow-1">
        <div class="nstSlider-wrap">
            <div class="nstSlider plan-fact" data-range_min="0" data-range_max="100" data-cur_min="0">
                <div class="leftGrip d-flex flex-column justify-content-center align-items-center ">
                    <div class="leftGrip-icons d-flex flex-nowrap">
                        <svg class="svg-icon svg-icon_rotate_90 text_size_8">
                            <use xlink:href="/img/svg/svgSprite.svg#arrow"></use>
                        </svg>
                        <svg class="svg-icon svg-icon_rotate_90_negative text_size_8">
                            <use xlink:href="/img/svg/svgSprite.svg#arrow"></use>
                        </svg>
                    </div>
                    <div class="leftGrip-in text_size_14 weight-700 nowrap">
                        <span class="leftGrip-value"></span>
                        <span>%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column text_size_14 weight-700 pl-0">100%</div>
</div>