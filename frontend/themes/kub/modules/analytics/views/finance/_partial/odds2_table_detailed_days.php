<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\OddsSearch;
use yii\web\View;
use common\models\employee\Config;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');

// Remove empty Wallets by user config
if ($userConfig->report_odds_hide_zeroes) {
    foreach ($growingData as $k0 => $d0) {

        if (!array_key_exists('children', $d0)) {
            unset($data[$k0]);
            unset($growingData[$k0]);
            continue;
        }

        foreach ($d0['children'] as $k1 => $d1) {
            if (0 === array_sum($d1['data'])) {
                unset($data[$k0]['children'][$k1]);
                unset($growingData[$k0]['children'][$k1]);
            }
        }
    }
}
?>

<script>
    // ODDS FIXED COLUMN
    $(document).ready(function() {
        OddsTable.fillFixedColumn();
    });
</script>

<div id="cs-table-2" class="custom-scroll-table-double cs-top">
    <table style="width: 1832px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
</div>

<!-- ODDS FIXED COLUMN -->
<div id="cs-table-first-column">
    <table class="table-bleak table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
        <thead></thead>
        <tbody></tbody>
    </table>
</div>

<div id="cs-table-1" class="custom-scroll-table-double">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <?= $this->render('odds2_table_head_days', [
            'searchModel' => $searchModel,
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'year' => $searchModel->year
        ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 0
        foreach ($data as $paymentType => $level0) {

            if (count($level0['children']) > 1) {
                echo $this->render('odds2_table_row_days', [
                    'level' => 0,
                    'searchModel' => $searchModel,
                    'title' => $level0['title'],
                    'data' => $level0['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'canHover' => false,
                    'question' => $level0['question'] ?? null,
                    'negativeRed' => true,
                ]);
            }

            // LEVEL 1
            foreach ($level0['children'] as $walletID => $level1) {

                echo $this->render('odds2_table_row_days', [
                    'level' => 1,
                    'searchModel' => $searchModel,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'dataWalletId' => $walletID,
                    'negativeRed' => true,
                ]);

                // LEVEL 2
                foreach ($level1['levels'] as $paymentFlowType => $level2) {

                    $floorKey = "floor-{$paymentFlowType}";
                    $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                    $isIncome = $paymentFlowType;

                    echo $this->render('odds2_table_row_days', [
                        'level' => 2,
                        'searchModel' => $searchModel,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                        'trId' => $paymentFlowType,
                        'dDays' => $d_days,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                        'dataWalletId' => $walletID,
                        'floorKey' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isOpenable' => true,
                    ]);

                    // LEVEL 3
                    foreach ($level2['levels'] as $itemId => $level3) {

                        $floorKey2 = "floor-item-{$itemId}";
                        $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                        echo $this->render('odds2_table_row_days', [
                            'level' => 3,
                            'searchModel' => $searchModel,
                            'title' => $level3['title'],
                            'data' => $level3['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dDays' => $d_days,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                            'dataWalletId' => $walletID,
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $itemId,
                            'dataId' => $floorKey,
                            'isOpenedFloor' => $isOpenedFloor,
                            'isSortable' => ($activeTab == OddsSearch::TAB_ODDS && empty($level3['levels'])),
                            'isOpenable' => (!empty($level3['levels'])),
                            'floorKey' => $floorKey2,
                            'dataHoverText' => $level3['dataHoverText'] ?? null
                        ]);

                        if (empty($level3['levels']))
                            continue 1;

                        // LEVEL 4
                        foreach ($level3['levels'] as $subItemId => $level4) {

                            echo $this->render('odds2_table_row_days', [
                                'level' => 4,
                                'searchModel' => $searchModel,
                                'title' => $level4['title'],
                                'data' => $level4['data'],
                                'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                                'dDays' => $d_days,
                                'cellIds' => $cell_id,
                                'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                                'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                                'dataWalletId' => $walletID,
                                'dataType' => $paymentFlowType,
                                'dataItemId' => $subItemId,
                                'dataId' => $floorKey2,
                                'isOpenedFloor' => $isOpenedFloor2,
                                'isSortable' => false
                            ]);
                        }

                    }
                }

                // GROWING BY WALLET
                echo $this->render('odds2_table_row_days', [
                    'level' => 1,
                    'searchModel' => $searchModel,
                    'title' => $growingData[$paymentType]['children'][$walletID]['title'],
                    'data' => $growingData[$paymentType]['children'][$walletID]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'negativeRed' => true
                ]);
            }

            // GROWING
            if (count($level0['children']) > 1) {
                echo $this->render('odds2_table_row_days', [
                    'level' => 0,
                    'searchModel' => $searchModel,
                    'title' => $growingData[$paymentType]['title'],
                    'data' => $growingData[$paymentType]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'negativeRed' => true
                ]);
            }
        }

        // TOTAL BY MONTH
        echo $this->render('odds2_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag net-cash-flow',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'canHover' => false,
            'negativeRed' => true,
        ]);

        // TOTAL BY YEAR
        echo $this->render('odds2_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            'negativeRed' => true
        ]);

        ?>
        </tbody>

    </table>
    </div>
</div>