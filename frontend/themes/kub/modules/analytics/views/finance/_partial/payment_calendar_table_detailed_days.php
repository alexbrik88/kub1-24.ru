<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\models\employee\Config;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');
?>

<div id="sds-top" class="table-scroll-double top" style="display: none">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="sds-bottom" class="table-scroll-double bottom sticky-column">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <?= $this->render('payment_calendar_table_head_days', [
            'searchModel' => $searchModel,
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'year' => $searchModel->year
        ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 0
        foreach ($data as $paymentType => $level0) {

            if (count($level0['children']) > 1) {
                echo $this->render('payment_calendar_table_row_days', [
                    'level' => 0,
                    'searchModel' => $searchModel,
                    'title' => $level0['title'],
                    'data' => $level0['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'canHover' => false,
                    'year' => $searchModel->year,
                    'negativeRed' => true
                ]);
            }

            // LEVEL 1
            foreach ($level0['children'] as $walletID => $level1) {

                echo $this->render('payment_calendar_table_row_days', [
                    'level' => 1,
                    'searchModel' => $searchModel,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'main-block not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'dataWalletId' => $walletID,
                    'year' => $searchModel->year,
                    'negativeRed' => true
                ]);

                // LEVEL 2
                foreach ($level1['levels'] as $paymentFlowType => $level2) {

                    $floorKey = "floor-{$paymentFlowType}";
                    $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                    $isIncome = $paymentFlowType;

                    echo $this->render('payment_calendar_table_row_days', [
                        'level' => 2,
                        'searchModel' => $searchModel,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                        'trId' => $paymentFlowType,
                        'dDays' => $d_days,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => $paymentType,
                        'dataFlowType' => $paymentFlowType,
                        'dataWalletId' => $walletID,
                        'floorKey' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'year' => $searchModel->year
                    ]);

                    // LEVEL 3
                    foreach ($level2['levels'] as $itemId => $level3) {

                        $floorKey2 = "floor-item-{$itemId}";
                        $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                        echo $this->render('payment_calendar_table_row_days', [
                            'level' => 3,
                            'searchModel' => $searchModel,
                            'title' => $level3['title'],
                            'data' => $level3['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dDays' => $d_days,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => $paymentType,
                            'dataFlowType' => $paymentFlowType,
                            'dataWalletId' => $walletID,
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $itemId,
                            'dataId' => $floorKey,
                            'isOpenedFloor' => $isOpenedFloor,
                            'isSortable' => false,
                            'isOpenable' => (!empty($level3['levels'])),
                            'floorKey' => $floorKey2,
                            'year' => $searchModel->year
                        ]);

                        if (empty($level3['levels']))
                            continue 1;

                        // LEVEL 4
                        foreach ($level3['levels'] as $subItemId => $level4) {

                            echo $this->render('payment_calendar_table_row_days', [
                                'level' => 4,
                                'searchModel' => $searchModel,
                                'title' => $level4['title'],
                                'data' => $level4['data'],
                                'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                                'dDays' => $d_days,
                                'cellIds' => $cell_id,
                                'dataPaymentType' => $paymentType,
                                'dataFlowType' => $paymentFlowType,
                                'dataWalletId' => $walletID,
                                'dataType' => $paymentFlowType,
                                'dataItemId' => $subItemId,
                                'dataId' => $floorKey2,
                                'isOpenedFloor' => $isOpenedFloor2,
                                'isSortable' => false,
                                'year' => $searchModel->year
                            ]);
                        }
                    }
                }

                // GROWING BY WALLET
                echo $this->render('payment_calendar_table_row_days', [
                    'level' => 1,
                    'searchModel' => $searchModel,
                    'title' => $growingData[$paymentType]['children'][$walletID]['title'],
                    'data' => $growingData[$paymentType]['children'][$walletID]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'hidePlanDays' => true,
                    'year' => $searchModel->year,
                    'negativeRed' => true
                ]);
            }

            // GROWING
            if (count($level0['children']) > 1) {
                echo $this->render('payment_calendar_table_row_days', [
                    'level' => 1,
                    'searchModel' => $searchModel,
                    'title' => $growingData[$paymentType]['title'],
                    'data' => $growingData[$paymentType]['data'],
                    'trClass' => 'not-drag cancel-drag',
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => $paymentType,
                    'isSumQuarters' => false,
                    'canHover' => false,
                    'hidePlanDays' => true,
                    'year' => $searchModel->year,
                    'negativeRed' => true
                ]);
            }
        }

        // TOTAL BY MONTH
        echo $this->render('payment_calendar_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag net-cash-flow',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'canHover' => false,
            'year' => $searchModel->year,
            'negativeRed' => true
        ]);

        // TOTAL BY YEAR
        echo $this->render('payment_calendar_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            'year' => $searchModel->year,
            'hidePlanDays' => true,
            'negativeRed' => true
        ]);

        ?>
        </tbody>

    </table>
    </div>
</div>