<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\PlanFactSearch;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\employee\Config;

/**
 * @var $isExpanded bool
 * @var $this yii\web\View
 * @var $searchModel PlanFactSearch
 *
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 *
 * @var $plan []
 * @var $growingPlan []
 * @var $totalPlan []
 *
 * @var $activeTab integer
 * @var $currentMonth string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$currentMonth = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonth < 4 && $isCurrentYear,
        2 => $currentMonth > 3 && $currentMonth < 7,
        3 => $currentMonth > 6 && $currentMonth < 10,
        4 => $currentMonth > 9 && $isCurrentYear
    ];
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_plan_fact');
$tabViewHead = ($isExpanded) ? 'plan_fact_table_head_expanded' : 'plan_fact_table_head';
$tabViewBody = ($isExpanded) ? 'plan_fact_table_row_expanded' : 'plan_fact_table_row';
?>

<div id="sds-top" class="table-scroll-double top" style="display: none">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="sds-bottom" class="table-scroll-double bottom">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
            <?= $this->render($tabViewHead, [
                'searchModel' => $searchModel,
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
            ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($plan as $paymentType => $planLevel1) {

            if ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK)
                continue;

            $level1 = ArrayHelper::getValue($data, $paymentType);

            echo $this->render($tabViewBody, [
                'level' => 1,
                'title' => $planLevel1['title'],
                'plan' => $planLevel1['data'] ?? [],
                'data' => $level1['data'] ?? [],
                'trClass' => 'main-block not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'canHover' => false,
                //
                'currentMonth' => $currentMonth,
                'currentQuarter' => $currentQuarter,
                'isCurrentYear' => $isCurrentYear
            ]);

            // LEVEL 2
            foreach ($planLevel1['levels'] as $paymentFlowType => $planLevel2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY) ? [1,3,4,6] : [1,3,5]);

                $level2 = ArrayHelper::getValue($level1['levels'] ?? [], $paymentFlowType);

                echo $this->render($tabViewBody, [
                    'level' => 2,
                    'title' => $planLevel2['title'],
                    'plan' => $planLevel2['data'] ?? [],
                    'data' => $level2['data'] ?? [],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'canHover' => false,
                    //
                    'isIncome' => $isIncome,
                    'currentMonth' => $currentMonth,
                    'currentQuarter' => $currentQuarter,
                    'isCurrentYear' => $isCurrentYear
                ]);

                // LEVEL 3
                foreach ($planLevel2['levels'] as $itemId => $planLevel3) {

                    $floorKey2 = "floor-item-{$itemId}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                    $level3 = ArrayHelper::getValue($level2['levels'] ?? [], $itemId);

                    echo $this->render($tabViewBody, [
                        'level' => 3,
                        'title' => $planLevel3['title'],
                        'plan' => $planLevel3['data'] ?? [],
                        'data' => $level3['data'] ?? [],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dMonthes' => $d_monthes,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => false,
                        'isOpenable' => (!empty($level3['levels'])),
                        'floorKey' => $floorKey2,
                        'canHover' => false,
                        //
                        'isIncome' => $isIncome,
                        'currentMonth' => $currentMonth,
                        'currentQuarter' => $currentQuarter,
                        'isCurrentYear' => $isCurrentYear
                    ]);

                    if (empty($planLevel3['levels']))
                        continue 1;

                    // LEVEL 4
                    foreach ($planLevel3['levels'] as $subItemId => $planLevel4) {

                        $level4 = ArrayHelper::getValue($level3['levels'] ?? [], $subItemId);

                        echo $this->render($tabViewBody, [
                            'level' => 4,
                            'title' => $planLevel4['title'],
                            'plan' => $planLevel4['data'] ?? [],
                            'data' => $level4['data'] ?? [],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dMonthes' => $d_monthes,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY ? null : ($isIncome ? '1' : '0')),
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $subItemId,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'isSortable' => false,
                            'canHover' => false,
                            //
                            'isIncome' => $isIncome,
                            'currentMonth' => $currentMonth,
                            'currentQuarter' => $currentQuarter,
                            'isCurrentYear' => $isCurrentYear
                        ]);
                    }
                }
            }

            if (isset($growingPlan[$paymentType]['hide']))
                continue;

            // GROWING
            echo $this->render($tabViewBody, [
                'level' => 1,
                'title' => $growingPlan[$paymentType]['title'],
                'data' => $growingPlan[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false,
                //
                'hideTooltip' => true,
                'currentMonth' => $currentMonth,
                'currentQuarter' => $currentQuarter,
                'isCurrentYear' => $isCurrentYear
            ]);
        }

        // TOTAL BY MONTH
        echo $this->render($tabViewBody, [
            'level' => 1,
            'title' => $totalPlan['month']['title'],
            'plan' => $totalPlan['month']['data'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'canHover' => false,
            //
            'hideTooltip' => true,
            'currentMonth' => $currentMonth,
            'currentQuarter' => $currentQuarter,
            'isCurrentYear' => $isCurrentYear,
            'question' => $totalData['month']['question'] ?? null,
        ]);

        // TOTAL BY YEAR
        echo $this->render($tabViewBody, [
            'level' => 1,
            'title' => $totalPlan['growing']['title'],
            'plan' => $totalPlan['growing']['data'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'isSumQuarters' => false,
            'canHover' => false,
            //
            'hideTooltip' => true,
            'currentMonth' => $currentMonth,
            'currentQuarter' => $currentQuarter,
            'isCurrentYear' => $isCurrentYear
        ]);

        ?>
        </tbody>
    </table>
    </div>
</div>