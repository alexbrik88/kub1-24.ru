<?php

use common\components\helpers\Html;
use frontend\components\PageSize;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\StocksSearch;
use common\components\TextHelper;

/**
 * @var $this yii\web\View
 * @var $searchModel StocksSearch
 * @var $dataTotals arraty
 */

$products = $dataProvider->getModels();

// js options
$floorMap = $floorMap ?? [];
$isCurrentYear = $searchModel->isCurrentYear;
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$d_monthes = [
    1 => ArrayHelper::getValue($floorMap, 'first-cell', $currentMonthNumber < 4 && $isCurrentYear),
    2 => ArrayHelper::getValue($floorMap, 'second-cell', $currentMonthNumber > 3 && $currentMonthNumber < 7),
    3 => ArrayHelper::getValue($floorMap, 'third-cell', $currentMonthNumber > 6 && $currentMonthNumber < 10),
    4 => ArrayHelper::getValue($floorMap, 'fourth-cell', $currentMonthNumber > 9 && $isCurrentYear),
];
$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];
$tabViewClass = $userConfig->getTableViewClass('table_view_finance_stocks');
?>

<div class="wrap wrap_padding_none_all" style="position:relative!important;">

    <!-- ODDS FIXED COLUMN -->
    <div id="cs-table-first-column">
        <table class="table-bleak table-fixed-first-column stocks-table-fixed-column table table-style table-count-list <?= $tabViewClass ?>">
            <thead></thead>
            <tbody></tbody>
        </table>
    </div>

    <div id="cs-table-1" class="custom-scroll-table-double">
        <div class="table-wrap">
        <table id="stocks-table" class="flow-of-funds stocks-table table table-style table-count-list <?= $tabViewClass ?> mb-0" >
            <thead>
            <tr class="quarters-flow-of-funds" style="position: relative">
              <th class="sum-cell align-top" rowspan="2" style="vertical-align: middle!important;">
                  <?= $this->render('_sort_arrows', [
                      'title' => $searchModel->byGroups ? 'Группа товара' : 'Название товара',
                      'attr' => 'title',
                      'defaultSort' =>$searchModel->sort
                  ]) ?>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                  <button class="table-collapse-btn button-clr <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-stocks-collapse-trigger data-target="first-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                  <button class="table-collapse-btn button-clr <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-stocks-collapse-trigger data-target="second-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                  <button class="table-collapse-btn button-clr <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-stocks-collapse-trigger data-target="third-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                  <button class="table-collapse-btn button-clr <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-stocks-collapse-trigger data-target="fourth-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
            </tr>
            <tr>
              <?php foreach (AbstractFinance::$month as $key => $month): ?>
                  <?php $quarter = (int)ceil($key / 3); ?>
                  <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                      data-collapse-cell
                      data-id="<?= $cell_id[$quarter] ?>"
                      data-month="<?= $key; ?>">
                      <div class="pl-1 pr-1">
                          <?= $this->render('_sort_arrows', [
                              'title' => $month,
                              'attr' => $key,
                              'defaultSort' =>$searchModel->sort
                          ]) ?>
                      </div>
                  </th>
                  <?php if ($key % 3 == 0): ?>
                      <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                          data-collapse-cell-total
                          data-id="<?= $cell_id[$quarter] ?>">
                          <div class="pl-1 pr-1">
                              <?= $this->render('_sort_arrows', [
                                  'title' => 'Итого',
                                  'attr' => $key,
                                  'defaultSort' =>$searchModel->sort
                              ]) ?>
                          </div>
                      </th>
                  <?php endif; ?>
              <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <!-- TOTALS -->
            <tr>
                <td class="contractor-cell text_size_14 weight-700" style="max-width: 255px; min-width: 255px; width: 255px;">
                    <div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">
                        Итого
                    </div>
                </td>
                <?php $key = 0; ?>
                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = ArrayHelper::getValue($dataTotals, $monthNumber, 0) ?>

                    <td class="sum-cell nowrap weight-700 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                    </td>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block sum-cell nowrap weight-700 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            <?php if ($searchModel->byGroups): ?>
                <!-- BY GROUPS -->
                <?php foreach ($products as $row): ?>
                    <?php $floorKey = "group_{$row['id']}"; ?>
                    <?php $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey); ?>
                    <!-- Group -->
                    <tr>
                        <td class="contractor-cell text_size_14" style="max-width: 255px; min-width: 255px; width: 255px;">
                            <div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">
                                <button class="table-collapse-btn button-clr <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-stocks-collapse-row-trigger data-target="<?= $floorKey ?>" style="text-align: left">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="bold ml-1" style="display: inline-block">
                                        <?= $row['title'] ?>
                                    </span>
                                </button>
                            </div>
                        </td>
                        <?php $key = 0; ?>
                        <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = ArrayHelper::getValue($row, $monthNumber, 0) ?>

                            <td class="sum-cell nowrap bold <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </td>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block sum-cell nowrap bold <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>
                    <!-- Product preloader -->
                    <tr class="wait-for-ajax-load <?= !$isOpenedFloor ? 'd-none':'' ?>" data-id="<?= $floorKey ?>">
                        <td colspan="17">
                            <img width="24" src="/img/import-preloader.gif">
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <!-- BY PRODUCTS -->
                <?php foreach ($products as $row): ?>
                    <tr>
                        <td class="contractor-cell text_size_14" style="max-width: 255px; min-width: 255px; width: 255px;">
                            <div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">
                                <?= Html::a($row['title'], ['/product/view', 'id' => $row['id'], 'productionType' => 1], [
                                    'title' => html_entity_decode($row['title']),
                                    'data-pjax' => 0
                                ]) ?>
                            </div>
                        </td>
                        <?php $key = 0; ?>
                        <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = ArrayHelper::getValue($row, $monthNumber, 0) ?>

                            <td class="sum-cell nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </td>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block sum-cell nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
        </div>
    </div>
</div>

<div class="table-settings-view row align-items-center">
    <div class="col-8">
        <nav>
            <?= \common\components\grid\KubLinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]) ?>
        </nav>
    </div>
    <div class="col-4">
        <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
            'maxTitle' => $dataProvider->totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => 'per-page',
        ]) ?>
    </div>
</div>