
<!-- Modals -->
<div id="industry-creation-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">У вас еще нет направлений, хотите добавить?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', '/company/update/?tab=industry', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="sale-point-creation-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">У вас еще нет точек продаж, хотите добавить?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', '/company/update/?tab=sale_point', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="project-creation-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">У вас еще нет проектов, хотите добавить?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', '/project/index', [
                    'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>