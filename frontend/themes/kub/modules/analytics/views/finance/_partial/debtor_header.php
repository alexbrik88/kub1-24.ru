<?php
use frontend\modules\analytics\models\DebtReportSearch2;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\Url;

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_debtor_help ?? false;
$showChartPanel = $userConfig->report_debtor_chart ?? false;

switch ($reportType) {
    case DebtReportSearch2::REPORT_TYPE_MONEY:
        $title = $title . ' Деньги';
        break;
    case DebtReportSearch2::REPORT_TYPE_GUARANTY:
        $title = $title . ' Обязательства';
        break;
    case DebtReportSearch2::REPORT_TYPE_NET_TOTAL:
        $title = 'Сальдо между "Нам должны" и "Мы должны"';
        break;
}

if ($searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS) {
    $searchBy = $userConfig->report_debtor_period_type;
}
?>
<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap4\Html::beginForm(Url::current(), 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_debtor_chart">
    <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL): ?>
        <div class="pt-3 pb-3 mb-2">
            <div class="row mb-4">
                <div class="col-12 pr-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_net_total', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel
                    ]);
                    ?>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="pt-3 pb-3 mb-2">
            <div class="row mb-4">
                <div class="col-12 pr-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_main', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel,
                        'customOffset' => (($searchModel->year - date('Y')) * 12)
                    ]);
                    ?>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 pr-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_main_2', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel,
                        'customOffset' => (($searchModel->year - date('Y')) * 12)
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-4 pr-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_top_1', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel,
                        'model2' => $searchModel2,
                    ]);
                    ?>
                </div>
                <div class="col-4 pl-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_top_2', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel,
                        'model2' => $searchModel2,
                    ]);
                    ?>
                </div>
                <div class="col-4 pl-2">
                    <?php
                    // Charts
                    echo $this->render('../_charts/_chart_debtor_top_3', [
                        'reportType' => $reportType,
                        'searchBy' => $searchBy,
                        'type' => $type,
                        'model' => $searchModel,
                        'model2' => $searchModel2,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_debtor_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('debtor_help_panel', [
            'reportType' => $reportType,
            'searchBy' => $searchBy,
            'type' => $type,
        ])
        ?>
    </div>
</div>