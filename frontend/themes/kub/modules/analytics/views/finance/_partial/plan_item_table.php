<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.02.2019
 * Time: 19:36
 */

use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\components\helpers\Html;
use yii\widgets\Pjax;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\rbac\permissions;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $activeTab integer
 * @var $subTab integer
 */

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
?>

<div class="items-table-block pt-1 pb-1 hidden">
    <div class="row align-items-center flex-nowrap">
        <div class="column">
            <h4 class="caption mb-2">Реестр плановых платежей</h4>
        </div>
        <div class="column ml-auto">
            <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
                <a class="button-regular button-regular_red ml-2 mb-2 --close-item-table close-odds" href="javascript:;">Закрыть</a>
            </div>
        </div>
    </div>

    <?php $pjax = Pjax::begin([
        'id' => 'odds-items_pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'scrollTo' => false,
        'timeout' => 10000,
        'options' => [
            'data-send-simple' => 1
        ]
    ]); ?>

    <?php if (isset($itemsDataProvider)): ?>
        <?= $this->render('_plan_table', [
            'searchModel' => $searchModel,
            'itemsDataProvider' => $itemsDataProvider,
            'activeTab' => $activeTab,
            'subTab' => $subTab,
            'canDelete' => $canDelete,
        ]) ?>
    <?php endif; ?>

    <?php Pjax::end(); ?>

    <?= $this->render('_summary_select/_summary_select', [
        'buttons' => [
            $canUpdate ? \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'calendar']).' <span>Дата</span>', '#many-date', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
            $canUpdate ? \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
            $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
        ],
    ]); ?>

</div>

<?php // $this->render('_item_table/modals', ['searchModel' => $searchModel]) ?>