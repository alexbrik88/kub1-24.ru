<?php

use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PlanCashRule;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;

$incomeItemsList = InvoiceIncomeItem::find()
    ->alias('i')
    ->leftJoin(['ii' => IncomeItemFlowOfFunds::tableName()], 'i.id = ii.income_item_id')
    ->select('i.name, i.id')
    ->where(['i.id' => PlanCashRule::INCOME_ITEMS_IDS])
    ->andWhere(['not', ['ii.is_visible' => 0]])
    ->orderBy('i.name')
    ->indexBy('i.id')
    ->column();
$expenditureItemsList = InvoiceExpenditureItem::find()
    ->alias('i')
    ->leftJoin(['ee' => ExpenseItemFlowOfFunds::tableName()], 'i.id = ee.expense_item_id')
    ->select('i.name, i.id')
    ->where(['i.id' => PlanCashRule::EXPENDITURE_ITEMS_IDS])
    ->andWhere(['not', ['ee.is_visible' => 0]])
    ->orderBy('i.name')
    ->indexBy('i.id')
    ->column();

$incomeSelection = PlanCashRule::find()
    ->select('income_item_id')
    ->where([
        'company_id' => Yii::$app->user->identity->company->id,
        'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME
    ])
    ->column();
$expenditureSelection = PlanCashRule::find()
    ->select('expenditure_item_id')
    ->where([
        'company_id' => Yii::$app->user->identity->company->id,
        'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE
    ])
    ->column();

$allIncomeItems = in_array(null, $incomeSelection);
$allExpenseItems = in_array(null, $expenditureSelection);
?>

<!-- MODAL UPDATE AUTOPLAN RULES -->
<div class="modal fade" id="update-autoplan-rules" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Правила создания плановых платежей</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'form_auto_plan_rules',
                    'action' => '/analytics/auto-plan-ajax/update-rules',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => false,
                    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
                ]); ?>

                    <div class="js_autoplan_rules_flow_type income mb-4">
                        <div style="margin-bottom: 10px;">
                            <strong>По Исходящим счетам</strong> (Создание плановых платежей по Приходу)
                        </div>
                        <div class="mb-2">
                            <div style="font-style: italic; margin-bottom: 10px;">Правила распространяется как на ТЕКУЩИХ, так и на НОВЫХ покупателей.</div>
                            <div class="row" style="margin-left: 20px;">
                                <div class="col-12 pl-0 pr-0">
                                    <?= \yii\helpers\Html::checkbox('income_items_ids[]', $allIncomeItems, [
                                        'class' => 'by-flow-type-main-checkbox',
                                        'label' => 'Все имеющиеся Статьи Прихода',
                                        'value' => 'all',
                                        'labelOptions' => [
                                            'class' => 'label bold',
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-6 pl-0">
                                    <?= Html::checkboxList('income_items_ids', $incomeSelection, array_slice($incomeItemsList, 0, ceil(count($incomeItemsList) / 2), true), [
                                        'item' => function ($index, $label, $name, $checked, $value) use ($allIncomeItems) {
                                            return Html::checkbox($name, $checked || $allIncomeItems, [
                                                'value' => $value,
                                                'label' => $label,
                                                'class' => 'by-flow-type-checkbox',
                                                'labelOptions' => [
                                                    'class' => 'mt-1 mb-1 label',
                                                ],
                                                'data-checked' => (int)$checked,
                                                'disabled' => $allIncomeItems
                                            ]).'<br/>';
                                        },
                                    ]); ?>
                                </div>
                                <div class="col-6 pr-0">
                                    <?= Html::checkboxList('income_items_ids', $incomeSelection, array_slice($incomeItemsList, ceil(count($incomeItemsList) / 2), null, true), [
                                        'item' => function ($index, $label, $name, $checked, $value) use ($allIncomeItems) {
                                            return Html::checkbox($name, $checked || $allIncomeItems, [
                                                'value' => $value,
                                                'label' => $label,
                                                'class' => 'by-flow-type-checkbox',
                                                'labelOptions' => [
                                                    'class' => 'mt-1 mb-1 label',
                                                ],
                                                'data-checked' => (int)$checked,
                                                'disabled' => $allIncomeItems
                                            ]).'<br/>';
                                        },
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="js_autoplan_rules_flow_type expense">
                        <div style="margin-bottom: 10px;">
                            <strong>По Входящим счетам</strong> (Создание плановых платежей по Расходу)
                        </div>
                        <div class="mb-2">
                            <div style="font-style: italic; margin-bottom: 10px;">Правила распространяется как на ТЕКУЩИХ, так и на НОВЫХ поставщиков.</div>
                            <div class="row" style="margin-left: 20px;">
                                <div class="col-12 pl-0 pr-0">
                                    <?= \yii\helpers\Html::checkbox('expenditure_items_ids[]', $allExpenseItems, [
                                        'class' => 'by-flow-type-main-checkbox',
                                        'label' => 'Все имеющиеся Статьи Расхода',
                                        'value' => 'all',
                                        'labelOptions' => [
                                            'class' => 'label bold',
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-6 pl-0">
                                    <?= Html::checkboxList('expenditure_items_ids', $expenditureSelection, array_slice($expenditureItemsList, 0, ceil(count($expenditureItemsList) / 2), true), [
                                        'item' => function ($index, $label, $name, $checked, $value) use ($allExpenseItems) {
                                            return Html::checkbox($name, $checked || $allExpenseItems, [
                                                'value' => $value,
                                                'label' => $label,
                                                'class' => 'by-flow-type-checkbox',
                                                'labelOptions' => [
                                                    'class' => 'mt-1 mb-1 label',
                                                ],
                                                'data-checked' => (int)$checked,
                                                'disabled' => $allExpenseItems
                                            ]).'<br/>';
                                        },
                                    ]) ?>
                                </div>
                                <div class="col-6 pr-0">
                                    <?= Html::checkboxList('expenditure_items_ids', $expenditureSelection, array_slice($expenditureItemsList, ceil(count($expenditureItemsList) / 2), null, true), [
                                        'item' => function ($index, $label, $name, $checked, $value) use ($allExpenseItems) {
                                            return Html::checkbox($name, $checked || $allExpenseItems, [
                                                'value' => $value,
                                                'label' => $label,
                                                'class' => 'by-flow-type-checkbox',
                                                'labelOptions' => [
                                                    'class' => 'mt-1 mb-1 label',
                                                ],
                                                'data-checked' => (int)$checked,
                                                'disabled' => $allExpenseItems
                                            ]).'<br/>';
                                        },
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                        ]); ?>
                        <div class="mr-auto ml-2 mt-1 hidden msg-saved-rules italic" style="color:#9198a0">Настройка правил.<br>Может занять несколько минут.</div>
                        <button type="button" class="button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                <?php $form->end() ?>
            </div>
        </div>
    </div>
</div>