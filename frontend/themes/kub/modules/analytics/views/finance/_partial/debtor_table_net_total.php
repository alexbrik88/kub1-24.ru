<?php
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use kartik\select2\Select2;
use frontend\components\Icon;
use common\components\TextHelper;
use yii\helpers\Html;
use frontend\components\PageSize;
use \common\models\EmployeeCompany;

/** @var $searchModel \frontend\modules\analytics\models\DebtReportSearch2 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell'];
$floorMap = $floorMap ?? [];
$d_column = [
    1 => ArrayHelper::getValue($floorMap, 'first-cell', 0),
    2 => ArrayHelper::getValue($floorMap, 'second-cell', 0)
];

$rows = $dataProvider->getModels();
$totals = $searchModel->netTotals;

foreach ($rows as $key => $row) {
    $contractor = Contractor::find()->where(['id' => $row['cid']])->andWhere(['or', ['company_id' => $company->id], ['company_id' => null]])->one();
    $employee = EmployeeCompany::find()->where(['employee_id' => $row['responsible_employee_id']])->andWhere(['company_id' => $company->id])->one();
    $rows[$key]['name'] = ($contractor) ?
        Html::a($contractor->nameWithType, ['/contractor/view', 'id' => $contractor->id, 'type' => $contractor->type], [
            'title' => html_entity_decode($contractor->nameWithType),
            'data-pjax' => 0
        ]) : $row['cid'];

    $rows[$key]['employee_name'] = ($employee) ? $employee->getShortFio() : $row['responsible_employee_id'];
    $rows[$key]['type'] = ($contractor) ? $contractor->type : null;
    $rows[$key]['opposite_id'] = ($contractor) ? $contractor->opposite_id : null;
}
?>

<div class="wrap wrap_padding_none_all" style="position: relative!important;">

    <!-- ODDS FIXED COLUMN -->
    <div id="cs-table-first-column">
        <table class="table-bleak table-fixed-first-column debtor-table-fixed-column table table-style table-count-list <?= $tabViewClass ?>">
            <thead></thead>
            <tbody></tbody>
        </table>
    </div>

    <div id="cs-table-2" class="custom-scroll-table-double cs-top">
        <table style="width: 1255px; font-size:0;"><tr><td></td></tr></table>
    </div>

    <div id="cs-table-1" class="custom-scroll-table-double">
        <div class="table-wrap">
        <table class="table-bleak flow-of-funds debtor-table-3 odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0" >
            <thead>
            <tr class="quarters-flow-of-funds" style="position: relative;">
                <th class="sum-cell align-middle" rowspan="2">
                    <div class="pl-1 pr-1">
                        Контрагент
                    </div>
                </th>
                <th class="sum-cell align-middle" rowspan="2">
                    <div class="pl-1 pr-1">
                        <div id="debtor-net_contractor_type-toggle" class="th-title filter <?= $searchModel->net_contractor_type ? 'active' : '' ?>" >
                            <span class="th-title-name">Тип</span>
                            <span class="th-title-btns">
                                <?= \yii\helpers\Html::button(Icon::get('filter', ['class' => 'th-title-icon-filter']), ['class' => 'th-title-btn button-clr']) ?>
                            </span>
                            <div class="filter-select2-select-container">
                                <?= Select2::widget([
                                    'id' => 'debtor-net_contractor_type',
                                    'name' => 'responsible_employee_id',
                                    'data' => ['' => 'Все', '21' => 'Покупатель & Поставщик', '2' => 'Покупатель', '1' => 'Поставщик'],
                                    'value' => $searchModel->net_contractor_type,
                                    'pluginOptions' => [
                                        'width' => '300px',
                                        'containerCssClass' => 'select2-filter-by-product'
                                    ],
                                    'hideSearch' => true
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </th>
                <th class="sum-cell align-middle" <?= $d_column[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="<?= $cell_id[1] ?>">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_column[1] ? 'active' : '' ?>" type="button" data-collapse-trigger-debtor data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="weight-700 ml-1 nowrap">Нам должны <span class="text-grey">(Дебиторка)</span></span>
                    </button>
                </th>
                <th class="sum-cell align-middle" <?= $d_column[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="<?= $cell_id[2] ?>">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_column[2] ? 'active' : '' ?>" type="button" data-collapse-trigger-debtor data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="weight-700 ml-1 nowrap">Мы должны <span class="text-grey">(Кредиторка)</span></span>
                    </button>
                </th>
                <th class="sum-cell align-middle" rowspan="2">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Сальдо', 'attr' => 'total_diff', 'defaultSort' => '-total_diff']) ?>
                    </div>
                </th>
                <th class="sum-cell align-middle" rowspan="2">
                    <div class="pl-1 pr-1">
                        <div id="debtor-responsible_employee_id-toggle" class="th-title filter <?= $searchModel->responsible_employee_id ? 'active' : '' ?>" >
                            <span class="th-title-name">От&shy;вет&shy;ствен&shy;ный</span>
                            <span class="th-title-btns">
                                <?= \yii\helpers\Html::button(Icon::get('filter', ['class' => 'th-title-icon-filter']), ['class' => 'th-title-btn button-clr']) ?>
                            </span>
                            <div class="filter-select2-select-container">
                                <?= Select2::widget([
                                    'id' => 'debtor-responsible_employee_id',
                                    'name' => 'responsible_employee_id',
                                    'data' => $searchModel2->getEmployersFilter(),
                                    'value' => $searchModel->responsible_employee_id,
                                    'pluginOptions' => [
                                        'width' => '300px',
                                        'containerCssClass' => 'select2-filter-by-product'
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
            <tr class="quarters-flow-of-funds" style="position: relative;">
                <th class="pl-2 pr-2 <?= $d_column[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Деньги по обязательствам', 'attr' => 'money_2']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2 <?= $d_column[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Обязательства', 'attr' => 'guarancy_1']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Итого', 'attr' => 'total_2']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2 <?= $d_column[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Деньги по обязательствам', 'attr' => 'money_1']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2 <?= $d_column[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Обязательства', 'attr' => 'guarancy_2']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows', ['title' => 'Итого', 'attr' => 'total_1']) ?>
                    </div>
                </th>
            </tr>
            </thead>

            <?php
            $floorKey = "first-floor-contractors";
            $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
            ?>

            <tbody>
                <!-- TOTALS -->
                <tr>
                    <td class="contractor-cell text_size_14 weight-700" style="max-width: 200px; min-width: 200px; width: 200px;">
                        <div style="max-width: 200px; overflow: hidden; text-overflow: ellipsis;">
                            <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey text_size_14 ml-1">
                                    ИТОГО задолженность
                                </span>
                            </button>
                        </div>
                    </td>
                    <td class="weight-700">

                    </td>
                    <td class="sum-cell nowrap weight-700 <?= $d_column[1] ? '' : 'd-none' ?>" data-id="<?= $cell_id[1] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($totals['money_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700 <?= $d_column[1] ? '' : 'd-none' ?>" data-id="<?= $cell_id[1] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($totals['guarancy_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700">
                        <?= TextHelper::invoiceMoneyFormat($totals['total_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700 <?= $d_column[2] ? '' : 'd-none' ?>" data-id="<?= $cell_id[2] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($totals['money_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700 <?= $d_column[2] ? '' : 'd-none' ?>" data-id="<?= $cell_id[2] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($totals['guarancy_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700">
                        <?= TextHelper::invoiceMoneyFormat($totals['total_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap weight-700">
                        <?= TextHelper::invoiceMoneyFormat($totals['total_diff'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap text-left weight-700">

                    </td>
                </tr>
                <!-- CONTRACTORS -->
                <?php foreach ($rows as $row): ?>
                <tr class="<?= !$isOpenedFloor ? 'd-none':'' ?>" data-id="<?= $floorKey ?>">
                    <td class="contractor-cell text_size_14" style="max-width: 200px; min-width: 200px; width: 200px;">
                        <div style="max-width: 200px; overflow: hidden; text-overflow: ellipsis; padding-left:5px;">
                            <?= $row['name'] ?>
                        </div>
                    </td>
                    <td class="nowrap">
                        <?php if (!$row['opposite_id'])
                            echo ($row['type'] == 2) ? 'Покупатель' : 'Поставщик';
                        else
                            echo 'Покуп. & Пост.';
                        ?>
                    </td>
                    <td class="sum-cell nowrap <?= $d_column[1] ? '' : 'd-none' ?>" data-id="<?= $cell_id[1] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($row['money_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap <?= $d_column[1] ? '' : 'd-none' ?>" data-id="<?= $cell_id[1] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($row['guarancy_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap">
                        <?= TextHelper::invoiceMoneyFormat($row['total_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap <?= $d_column[2] ? '' : 'd-none' ?>" data-id="<?= $cell_id[2] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($row['money_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap <?= $d_column[2] ? '' : 'd-none' ?>" data-id="<?= $cell_id[2] ?>" data-collapse-cell>
                        <?= TextHelper::invoiceMoneyFormat($row['guarancy_2'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap">
                        <?= TextHelper::invoiceMoneyFormat($row['total_1'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap">
                        <?= TextHelper::invoiceMoneyFormat($row['total_diff'], 2) ?>
                    </td>
                    <td class="sum-cell nowrap text-left">
                        <?= $row['employee_name'] ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="table-settings-view row align-items-center">
    <div class="col-8">
        <nav>
            <?= \common\components\grid\KubLinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]) ?>
        </nav>
    </div>
    <div class="col-4">
        <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
            'maxTitle' => $dataProvider->totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => 'per-page',
        ]) ?>
    </div>
</div>