<?php
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $searchModel OddsSearch */
/** @var $dDays array */
/** @var $cellIds array */
?>
<tr class="quarters-flow-of-funds">
    <th class="pl-2 pr-2 pt-3 pb-3 align-top" style="border-bottom: none">
        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1">Статьи</span>
        </button>
    </th>
    <?php foreach (AbstractFinance::$month as $key => $month): ?>
        <?php $key = (int)$key; ?>
        <?php $colspan = cal_days_in_month(CAL_GREGORIAN, $key, $searchModel->year) ?>
        <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $dDays[$key] ? "colspan='{$colspan}'" : '' ?> data-collapse-cell-title data-id="<?= $cellIds[$key] ?>">
            <button class="table-collapse-btn button-clr ml-1 <?= $dDays[$key] ? 'active' : '' ?>" type="button" data-columns-count="<?=($colspan)?>" data-collapse-trigger-days data-target="<?= $cellIds[$key] ?>" data-month="<?= $key ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="text-grey weight-700 ml-1 nowrap"><?= $month . ' ' . $searchModel->year; ?></span>
            </button>
        </th>
    <?php endforeach; ?>
    <th class="pl-2 pr-2 pt-3 pb-3 align-top">
        <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
    </th>
</tr>
<tr>
    <th style="border-top: none">&nbsp;</th>
    <?php foreach (AbstractFinance::$month as $month => $monthName): ?>
        <?php $month = (int)$month; ?>
        <?php foreach (AbstractFinance::getDaysInMonth($year, $month) as $date => $dayName): ?>
            <?php $dayClass = (date('N', strtotime($searchModel->year.$date)) >= 6) ? 'font-color-red' : '' ?>
            <th class="pl-2 pr-2 <?= $dDays[$month] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cellIds[$month] ?>" data-month="<?= $month; ?>" data-day="<?= (int)$dayName ?>">
                <div class="pl-1 pr-1 <?= $dayClass ?>"><span class="<?= ($date == date('Y-m-d')) ? 'current-day' : '' ?>"><?= (int)$dayName ?></span></div>
            </th>
        <?php endforeach; ?>
        <th class="pl-2 pr-2 <?= $dDays[$month] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cellIds[$month] ?>" data-month="<?= $month; ?>" data-month-name="<?= $monthName ?>">
            <div class="pl-1 pr-1">Итого</div>
        </th>
    <?php endforeach; ?>
    <th class="pl-2 pr-2 pt-3 pb-3 align-top">
        <div class="pl-1 pr-1">Итого</div>
    </th>
</tr>
