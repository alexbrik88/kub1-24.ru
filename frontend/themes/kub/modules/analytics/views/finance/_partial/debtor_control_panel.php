<?php
use common\components\helpers\Html;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/** @var $searchBy */
/** @var $searchModel DebtReportSearch2 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
$__hasOptionsBtn =
    $reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL ||
    $reportType == DebtReportSearch2::REPORT_TYPE_MONEY && $searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS;
?>

<div class="row pt-1 row_indents_s" style="margin-bottom: -5px">
    <div class="col-6">
        <div class="d-flex flex-nowrap">
            <div class="" style="font-size: 24px; font-weight: bold; padding-top: 6px;">
                <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY): ?>
                    <?php if ($searchBy == DebtReportSearch2::SEARCH_BY_INVOICES): ?>
                        Долги по счетам
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2 mr-1',
                            'data-tooltip-content' => '#tooltip_debtor_search_by_1',
                        ]) ?>
                    <?php elseif ($searchBy == DebtReportSearch2::SEARCH_BY_DOCUMENTS): ?>
                        Долги по обязательствам
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2 mr-1',
                            'data-tooltip-content' => '#tooltip_debtor_search_by_2',
                        ]) ?>
                    <?php elseif ($searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS): ?>
                        Долги по срокам <?= ($searchModel->isSearchByPeriodsByInvoices) ? ' по счетам' : ' по обязательствам' ?>
                    <?php endif; ?>
                <?php elseif ($reportType == DebtReportSearch2::REPORT_TYPE_GUARANTY): ?>
                    Долги по обязательствам
                    <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2 mr-1',
                        'data-tooltip-content' => '#tooltip_debtor_search_by_2',
                    ]) ?>
                <?php endif; ?>
                </div>
            <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY && $searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS): ?>
                <div class="ml-2">
                    <div class="dropdown dropdown-settings d-inline-block" title="Настройка таблицы" style="z-index: 1001">
                        <?= Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>', [
                            'class' => 'button-list button-hover-transparent button-clr mr-2',
                            'data-toggle' => 'dropdown',
                        ]) ?>
                        <ul id="config_items_box" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
                            <li>
                                <label style="font-weight: bold;">Настройка таблицы</label>
                            </li>
                            <li class="bold" style="border-top: 1px solid #ddd;">
                                <label style="font-weight: bold;">Выводить долги</label>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::radio('report_debtor_period_type', !$searchModel->isSearchByPeriodsByInvoices, [
                                    'class' => 'debt-report-by-period-type',
                                    'label' => 'По обязательствам',
                                    'data-url' => '/analytics/finance/debtor-change-view',
                                    'value' => $searchModel::SEARCH_BY_DEBT_PERIODS_BY_DOCUMENTS
                                ]); ?>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::radio('report_debtor_period_type', $searchModel->isSearchByPeriodsByInvoices, [
                                    'class' => 'debt-report-by-period-type',
                                    'label' => 'По счетам',
                                    'data-url' => '/analytics/finance/debtor-change-view',
                                    'value' => $searchModel::SEARCH_BY_DEBT_PERIODS_BY_INVOICES
                                ]); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_GUARANTY): ?>
                <div class="ml-2">
                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>',
                    '#debtor-prepayment-options', [
                        'class' => 'button-list button-hover-transparent button-clr ml-2 mb-2 disabled',
                        'data-toggle' => 'modal',
                        'title' => 'Настройки',
                    ]); ?>
                </div>
            <?php endif; ?>
            <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
                <div class="<?=($__hasOptionsBtn) ? '' : 'ml-2' ?>">
                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                        Url::to(['/analytics/finance/get-xls',
                            'type' => DebtReportSearch2::REPORT_ID,
                            'io_type' => $type,
                            'search_by' => $searchBy,
                            'report_type' => $reportType,
                            'year' => $searchModel->year]), [
                            'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2 disabled',
                            'title' => 'Скачать в Excel',
                        ]); ?>
                </div>
            <?php endif; ?>
            <div class="ml-2">
                <?= TableViewWidget::widget(['attribute' => ($type == 2) ? 'table_view_report_client' : 'table_view_report_supplier']) ?>
            </div>
            <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL): ?>
            <div class="ml-auto">
                <!-- filter -->
                <div id="debtor-filter" class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter_small <?= $searchModel->net_debt_type ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
                        <button class="button-regular button-regular-more button-hover-transparent button-clr button-debtor-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="button-txt">Фильтр</span>
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div class="dropdown-menu keep-open" aria-labelledby="filter">
                            <div class="popup-dropdown-in p-3">
                                <div class="p-1">
                                    <div class="form-group mb-3">
                                        <div class="dropdown-drop" data-id="dropdown1">
                                            <div class="label">Кто кому должен</div>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown1" onclick="toggleVisible(this); return false;">
                                                <span>
                                                    <?php switch ($searchModel->net_debt_type) {
                                                        case '1':
                                                            echo 'Мы должны';
                                                            break;
                                                        case '2':
                                                            echo 'Нам должны';
                                                            break;
                                                        case '21':
                                                            echo 'Встречные долги';
                                                            break;
                                                        default:
                                                            echo 'Все';
                                                            break;
                                                    } ?>
                                                </span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown1">
                                                <div class="dropdown-drop-in">
                                                    <?php foreach (['' => 'Все', '21' => 'Встречные долги', '1' => 'Мы должны', '2' => 'Нам должны'] as $value => $label) {
                                                        $checked = ($value == $searchModel->net_debt_type);
                                                        echo Html::radio('DebtReportSearch2[net_debt_type]', $checked, [
                                                            'value' => $value,
                                                            'label' => $label,
                                                            'labelOptions' => [
                                                                'class' => 'p-2 no-border',
                                                            ],
                                                            'data-default' => ($checked) ? 1:0
                                                        ]);
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-between">
                                        <div class="col-6 pr-0">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                <span>Применить</span>
                                            </button>
                                        </div>
                                        <div class="col-6 pl-0 text-right">
                                            <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                                <span>Сбросить</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'id' => 'debtor-title-filter',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="tooltip-template" style="display: none;">
    <?php if ($type == 2): ?>
        <span id="tooltip_debtor_search_by_2" style="display: inline-block; text-align: center;">
            <?php if ($reportType == 1): ?>
            Вы выполнили свои обязательства (отгрузили товар или оказали услугу), но деньги не получили.<br>
            Т.е. по счетам, к которым уже есть Акты, Товарные накладные или УПД, деньги не поступили.<br>
            Данные тут - это данные в Балансе в строке «Задолженность покупателей» в «Дебиторской задолженности».<br>
            Разница сумм в отчетах «По обязательствам» и «По счетам» – в отчете «По счетам» сумма может быть больше <br>
            за счет выставленных счетов на получение авансов от покупателей.
            <?php endif; ?>
            <?php if ($reportType == 2): ?>
            Вы оплатили поставщику, но вам не оказали услугу (нет Акта)<br>
            или не отгрузили товар (нет Товарной накладной или УПД)<br>
            полностью или частично.
            <?php endif; ?>
        </span>
        <span id="tooltip_debtor_search_by_1" style="display: inline-block; text-align: center;">
            Вы выставили счета, но деньги не получили.<br>
            Разница сумм в отчетах «По обязательствам» и «По счетам» – <br>
            в отчете «По счетам» сумма может быть больше за счет выставленных счетов <br>
            на получение авансов от покупателей.
        </span>
    <?php else: ?>
        <span id="tooltip_debtor_search_by_2" style="display: inline-block; text-align: center;">
            <?php if ($reportType == 1): ?>
            Поставщик выполнил свои обязательства перед вами (отгрузил товар или оказал услугу),<br/>
            но деньги вы ему еще не оплатили. Т.е. это счета от поставщиков, к которым <br/>
            уже есть Акты, Товарные Накладные или УПД, которые вы не оплатили.
            <?php endif; ?>
            <?php if ($reportType == 2): ?>
            Вы получили деньги от покупателя, но не выполнили свои обязательства,<br/>
            т.е. вы не отгрузили товар (нет товарной накладной или УПД)<br/> или не оказали услугу (нет Акта) полностью или частично.
            <?php endif; ?>
        </span>
        <span id="tooltip_debtor_search_by_1" style="display: inline-block; text-align: center;">
            Вам выставил счета поставщик, но вы их не оплатили.<br/>
            В отличие от «Долгов по Обязательствам» тут еще учитываются<br/>
            счета от поставщиков на предоплату (на получение аванса).
        </span>
    <?php endif; ?>
</div>

<script>
    // FILTER
    $("#debtor-filter").find("input:radio").on("change", function() {
        var submit = $("#debtor-filter").find("[type=submit]");
        var filter_on = !!$(this).val();
        $(this).parents(".dropdown-drop").find("a > span").html($(this).parents("label").text());
        $(this).parents(".dropdown-drop-menu, .dropdown-drop").removeClass("visible show");

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    });
    $("#debtor-filter .cash_filters_reset").on("click", function() {
        var submit = $("#debtor-filter").find("[type=submit]");
        $(this).parents(".dropdown").find(".dropdown-drop-in").each(function() {
            $(this).find("input:radio").first().prop("checked", true);
            $(this).find("input:radio").uniform();
            $(this).parents(".dropdown-drop").find("a > span").html($(this).find("input:radio").first().parents("label").text());
        });
        $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    });

    // OPTIONS
    $('.debt-report-by-period-type').on('change', function() {
        $.ajax({
            "type": "post",
            "url": $(this).attr("data-url") + '?report_debtor_period_type=' + $(this).val(),
            "data": $(this).serialize(),
            "success": function(data) {
                location.href = location.href;
            }
        });
    });
</script>