<?php

use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\EmployeeCompany;

/* @var $searchModel \frontend\modules\analytics\models\DebtReportSearch2 */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
    'tableOptions' => [
        'class' => 'table table-debtor table-style table-count-list flow-of-funds' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'name',
            'label' => ($type == 2) ? 'Покупатели' : 'Поставщики',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '15%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell text_size_14',
                'style' => 'max-width: 255px; min-width: 255px; width: 255px;'
            ],
            'filter' => $searchModel->getContractorFilter($type),
            'value' => function (Contractor $model) {
                return '<div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">' . Html::a($model->nameWithType, [
                        '/contractor/view',
                        'id' => $model->id,
                        'type' => $model->type
                    ], [
                        'title' => html_entity_decode($model->nameWithType),
                        'data-pjax' => 0
                    ]) . '</div>';
            },
            's2width' => '300px',
            'footer' => 'Итого',
        ],
        [
            'attribute' => 'current_debt_sum',
            'label' => 'Текущие неоплаченные счета',
            'format' => 'raw',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'value' => function (Contractor $data) {
                return TextHelper::invoiceMoneyFormat($data->current_debt_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('current_debt_sum'), 2)
        ],
        [
            'attribute' => 'debt_0_10_sum',
            'label' => '1-10 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_0_10_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_0_10_sum'), 2)
        ],
        [
            'attribute' => 'debt_11_30_sum',
            'label' => '11-30 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_11_30_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_11_30_sum'), 2)
        ],
        [
            'attribute' => 'debt_31_60_sum',
            'label' => '31-60 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_31_60_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_31_60_sum'), 2)
        ],
        [
            'attribute' => 'debt_61_90_sum',
            'label' => '61-90 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_61_90_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_61_90_sum'), 2)
        ],
        [
            'attribute' => 'debt_more_90_sum',
            'label' => 'Больше 90 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_more_90_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_more_90_sum'), 2)
        ],
        [
            'attribute' => 'debt_all_sum',
            'label' => 'Итого',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_all_sum, 2);
            },
            'footer' => TextHelper::invoiceMoneyFormat($searchModel->getFilteredTotalDebts('debt_all_sum'), 2)
        ],
        [
            'attribute' => 'responsible_employee_id',
            'label' => 'От&shy;вет&shy;ствен&shy;ный',
            'headerOptions' => [
                'width' => '15%',
            ],
            'contentOptions' => [
                'class' => 'text-left nowrap',
            ],
            'encodeLabel' => false,
            'filter' => $searchModel->getResponsibleItemsByQuery(),
            'value' => function (Contractor $model) use ($company) {
                $responsible = \common\models\EmployeeCompany::item($model->responsible_employee_id, $company->id);

                return $responsible ? $responsible->getFio(true) : '';
            },
            's2width' => '250px',
            'footer' => null
        ],
    ],
]); ?>