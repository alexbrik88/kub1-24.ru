<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel IncomeSearch|ExpensesSearch
 * @var $data array
 * @var $dataIncomeByMonth array
 */

$userConfig = Yii::$app->user->identity->config;

$isIncome = $isIncome ?? true;
$tdIncome = ($isIncome) ? 'income' : '';
$BLOCK = ($isIncome) ? IncomeSearch::BLOCK_INCOME : ExpensesSearch::BLOCK_EXPENSES;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

if (!empty($floorMap)) {
    $d_all = ArrayHelper::getValue($floorMap, 'all');
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_all = 1;
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = ($isIncome)
    ? $userConfig->getTableViewClass('table_view_finance_income')
    : $userConfig->getTableViewClass('table_view_finance_expenses');

$showPercent = ($isIncome)
    ? $userConfig->report_odds_income_percent
    : $userConfig->report_odds_expense_percent;
$showPercent2 = ($isIncome)
    ? 0
    : $userConfig->report_odds_expense_percent_2;

$columnsCount = 3 * ($showPercent + $showPercent2 + 1);
$columnsCountCollapsed = 1 * ($showPercent + $showPercent2 + 1);

$colspan = [
    'single' => 'colspan="' . $columnsCountCollapsed . '"',
    'multi' => 'colspan="' . $columnsCount . '"',
];
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-income flow-of-expenses table-bleak flow-of-funds-double table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds-double">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="3">
                    <button class="table-collapse-btn button-clr ml-1 <?=($d_all ? 'active':'')?>" type="button" data-collapse-all-trigger data-target="all">
                        <span class="table-collapse-icon">&nbsp;</span>
                    </button>
                    <div style="margin-left: 30px">
                        <?= $this->render('_sort_arrows_ajax', ['title' => 'Статьи', 'attr' => 'name', 'defaultSort' => 'name']) ?>
                    </div>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th <?= $colspan['single'] ?> class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
                </th>
            </tr>
            <tr>
                <?php foreach (AbstractFinance::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th <?= $colspan['single'] ?> class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" style="border-bottom: 1px solid;"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th <?= $colspan['single'] ?> class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" style="border-bottom: 1px solid;"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th <?= $colspan['single'] ?> class="pl-2 pr-2" style="border-top-color: transparent; border-bottom: 1px solid;"><div class="pl-1 pr-1">Итого</div></th>
            </tr>
            <tr class="th-flow-of-funds-double">
                <?php foreach (AbstractFinance::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                        <div class="pl-1 pr-1">
                            <?= $this->render('_sort_arrows_ajax', ['title' => 'Факт', 'attr' => "month_{$key}"]) ?>
                        </div>
                    </th>
                    <?php if ($showPercent): ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                        <div class="pl-1 pr-1">
                            <?= $this->render('_sort_arrows_ajax', ['title' => '%', 'attr' => "month_{$key}"]) ?>
                        </div>
                    </th>
                    <?php endif; ?>
                    <?php if ($showPercent2): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                            <div class="pl-1 pr-1">
                                <?= $this->render('_sort_arrows_ajax', ['title' => '%%', 'attr' => "month_{$key}"]) ?>
                            </div>
                        </th>
                    <?php endif; ?>

                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                            <div class="pl-1 pr-1">
                                <?= $this->render('_sort_arrows_ajax', ['title' => 'Факт', 'attr' => "quarter_{$quarter}"]) ?>
                            </div>
                        </th>
                        <?php if ($showPercent): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                            <div class="pl-1 pr-1">
                                <?= $this->render('_sort_arrows_ajax', ['title' => '%', 'attr' => "quarter_{$quarter}"]) ?>
                            </div>
                        </th>
                        <?php endif; ?>
                        <?php if ($showPercent2): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                <div class="pl-1 pr-1">
                                    <?= $this->render('_sort_arrows_ajax', ['title' => '%%', 'attr' => "quarter_{$quarter}"]) ?>
                                </div>
                            </th>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows_ajax', ['title' => 'Факт', 'attr' => 'year']) ?>
                    </div>
                </th>
                <?php if ($showPercent): ?>
                <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                    <div class="pl-1 pr-1">
                        <?= $this->render('_sort_arrows_ajax', ['title' => '%', 'attr' => 'year']) ?>
                    </div>
                </th>
                <?php endif; ?>
                <?php if ($showPercent2): ?>
                    <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                        <div class="pl-1 pr-1">
                            <?= $this->render('_sort_arrows_ajax', ['title' => '%%', 'attr' => 'year']) ?>
                        </div>
                    </th>
                <?php endif; ?>
            </tr>
            </thead>

            <tbody>

            <!-- INCOME ROW IN EXPENSES -->
            <?php if (!$isIncome && $showPercent2): ?>

            <tr class="main-block not-drag cancel-drag expenditure_type">
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            <span class="weight-700 text_size_14 m-l-purse"><?= 'Приходы' ?></span>
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $monthIncomeAmount = ArrayHelper::getValue($dataIncomeByMonth, $monthNumber, 0);
                    $quarterIncomeAmount = ArrayHelper::getValue($dataIncomeByMonth, "q{$quarter}", 0);
                    $yearIncomeAmount = ArrayHelper::getValue($dataIncomeByMonth, "y", 0);
                    ?>

                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($monthIncomeAmount, 2) ?>
                        </div>
                    </td>

                    <?php if ($showPercent): ?>
                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">

                        </div>
                    </td>
                    <?php endif; ?>
                    <?php if ($showPercent2): ?>
                        <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat(100, 2) ?>
                            </div>
                        </td>
                    <?php endif; ?>

                    <?php if ($key % 3 == 0): ?>
                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterIncomeAmount, 2); ?>
                            </div>
                        </td>

                        <?php if ($showPercent): ?>
                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">

                            </div>
                        </td>
                        <?php endif; ?>
                        <?php if ($showPercent2): ?>
                            <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::numberFormat(100, 2) ?>
                                </div>
                            </td>
                        <?php endif; ?>

                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat($yearIncomeAmount, 2) ?>
                    </div>
                </td>

                <?php if ($showPercent): ?>
                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">

                    </div>
                </td>
                <?php endif; ?>
                <?php if ($showPercent2): ?>
                    <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::numberFormat(100, 2) ?>
                        </div>
                    </td>
                <?php endif; ?>
            </tr>

            <?php endif; ?>

            <?php $floorKey1 = "first-floor-".$BLOCK; ?>
            <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1, true); ?>

            <tr class="main-block not-drag cancel-drag expenditure_type" id="<?= $BLOCK; ?>">
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            <button class="table-collapse-btn button-clr <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="weight-700 text_size_14 ml-1"><?= ($isIncome) ? 'Приходы' : 'Расходы' ?></span>
                            </button>
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = $data['types'][$BLOCK][$monthNumber]['flowSum'] ?? 0;
                ?>

                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                        </div>
                    </td>

                    <?php if ($showPercent): ?>
                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::numberFormat(100, 2) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <?php if ($showPercent2): ?>
                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat(100 * $amount / (ArrayHelper::getValue($dataIncomeByMonth, $monthNumber) ?: 9E9), 2) ?>
                            </div>
                        </td>
                    <?php endif; ?>

                    <?php $quarterSum += $amount; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2); ?>
                            </div>
                        </td>

                        <?php if ($showPercent): ?>
                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat(100, 2) ?>
                            </div>
                        </td>
                        <?php endif; ?>
                        <?php if ($showPercent2): ?>
                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::numberFormat(100 * $quarterSum / (ArrayHelper::getValue($dataIncomeByMonth, "q{$quarter}") ?: 9E9), 2) ?>
                                </div>
                            </td>
                        <?php endif; ?>

                        <?php $quarterSum = 0; ?>

                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= isset($data['types'][$BLOCK]['totalFlowSum']) ?
                            TextHelper::invoiceMoneyFormat($data['types'][$BLOCK]['totalFlowSum'], 2) : 0; ?>
                    </div>
                </td>

                <?php if ($showPercent): ?>
                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::numberFormat(100, 2) ?>
                    </div>
                </td>
                <?php endif; ?>
                <?php if ($showPercent2): ?>
                    <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $BLOCK; ?>">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= isset($data['types'][$BLOCK]['totalFlowSum']) ?
                                TextHelper::numberFormat(100 * $data['types'][$BLOCK]['totalFlowSum'] / (ArrayHelper::getValue($dataIncomeByMonth, "y") ?: 9E9), 2) : 0; ?>
                        </div>
                    </td>
                <?php endif; ?>
            </tr>
            <?php if (isset($data[$BLOCK])): ?>
                <?php foreach ($data['itemName'][$BLOCK] as $expenditureItemID => $expenditureItemName): ?>
                    <?php if (isset($data[$BLOCK][$expenditureItemID])): ?>

                        <?php $hasSubitems = isset($data[$BLOCK][$expenditureItemID]['subItems']); ?>
                        <?php $floorKey2 = "second-floor-".$expenditureItemID; ?>
                        <?php $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2, false); ?>

                        <tr class="item-block <?=($tdIncome)?> <?= !$isOpenedFloor1 ? 'd-none':'' ?>" data-id="<?= $floorKey1 ?>" data-type_id="<?= $BLOCK; ?>" data-item_id="<?= $expenditureItemID; ?>">
                            <td class="pl-2 pr-1 pt-3 pb-3">
                                <?php if ($hasSubitems): ?>
                                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                        <div class="text_size_14 mr-2 nowrap">
                                            <button class="table-collapse-btn button-clr icon-grey <?= $isOpenedFloor2 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey2 ?>">
                                                <span class="table-collapse-icon">&nbsp;</span>
                                                <span class="text-grey text_size_14 ml-1"><?= $expenditureItemName; ?></span>
                                            </button>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <span class="text-grey text_size_14 m-l-purse"><?= $expenditureItemName; ?></span>
                                <?php endif; ?>
                            </td>
                            <?php $key = $quarterSum = $totalSum = 0; ?>
                            <?php $totalMonthAmount = $totalQuarterAmount = $totalYearAmount = 0; ?>
                            <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = (int)ceil($key / 3);
                                $amount = $data[$BLOCK][$expenditureItemID][$monthNumber]['flowSum'] ?? 0;
                                $totalMonthAmount = $data['types'][$BLOCK][$monthNumber]['flowSum'] ?? 0;
                                $totalQuarterAmount += $totalMonthAmount;
                                $totalYearAmount += $totalMonthAmount;
                                ?>

                                <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                    </div>
                                </td>

                                <?php if ($showPercent): ?>
                                <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::numberFormat($amount / ($totalMonthAmount ?: 9E9) * 100, 2) // percent Month ?>
                                    </div>
                                </td>
                                <?php endif; ?>
                                <?php if ($showPercent2): ?>
                                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::numberFormat($amount / (ArrayHelper::getValue($dataIncomeByMonth, $monthNumber) ?: 9E9) * 100, 2) // todo: percent2 Month ?>
                                        </div>
                                    </td>
                                <?php endif; ?>

                                <?php $quarterSum += $amount; $totalSum += $amount; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($quarterSum, 2); ?>
                                        </div>
                                    </td>

                                    <?php if ($showPercent): ?>
                                    <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::numberFormat($quarterSum / ($totalQuarterAmount ?: 9E9) * 100, 2) // percent Quarter ?>
                                        </div>
                                    </td>
                                    <?php endif; ?>
                                    <?php if ($showPercent2): ?>
                                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?= TextHelper::numberFormat($quarterSum / (ArrayHelper::getValue($dataIncomeByMonth, "q{$quarter}") ?: 9E9) * 100, 2) // todo: percent2 Quarter ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>

                                    <?php $quarterSum = $totalQuarterAmount = 0; ?>
                                <?php endif; ?>
                            <?php endforeach ?>
                            <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                <div class="pl-1 pr-1">
                                    <?= isset($data[$BLOCK][$expenditureItemID]['totalFlowSum']) ?
                                        TextHelper::invoiceMoneyFormat($data[$BLOCK][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                </div>
                            </td>

                            <?php if ($showPercent): ?>
                            <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                <div class="pl-1 pr-1">
                                    <?= TextHelper::numberFormat($totalSum / ($totalYearAmount ?: 9E9) * 100, 2) // percent Year ?>
                                </div>
                            </td>
                            <?php endif; ?>
                            <?php if ($showPercent2): ?>
                                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::numberFormat($totalSum / (ArrayHelper::getValue($dataIncomeByMonth, 'y') ?: 9E9) * 100, 2) // todo: percent2 Year ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        </tr>

                        <!-- SUBITEMS -->
                        <?php if ($hasSubitems): ?>
                            <?php foreach ($data[$BLOCK][$expenditureItemID]['subItems'] as $subItemID => $subItemAmount): ?>
                                <?php $subItemName = ArrayHelper::getValue($data['itemName'][$BLOCK], $subItemID); ?>
                                <tr class="item-block <?=($tdIncome)?> <?= !$isOpenedFloor2 ? 'd-none':'' ?>" data-id="<?= $floorKey2 ?>" data-type_id="<?= $BLOCK; ?>" data-item_id="<?= $subItemID; ?>">
                                    <td class="pl-2 pr-1 pt-3 pb-3">
                                        <span class="text-grey text_size_12 m-l-purse-big"><?= $subItemName; ?></span>
                                    </td>
                                    <?php $key = $quarterSum = $totalSum = 0; ?>
                                    <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                                        <?php $key++;
                                        $quarter = (int)ceil($key / 3);
                                        $amount = ArrayHelper::getValue($subItemAmount, "{$monthNumber}.flowSum", 0);
                                        $totalMonthAmount = $data['types'][$BLOCK][$monthNumber]['flowSum'] ?? 0;
                                        ?>
                                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                            </div>
                                        </td>

                                        <?php if ($showPercent): ?>
                                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?php // percent Month ?>
                                            </div>
                                        </td>
                                        <?php endif; ?>
                                        <?php if ($showPercent2): ?>
                                            <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                                <div class="pl-1 pr-1">
                                                    <?php // percent2 Month ?>
                                                </div>
                                            </td>
                                        <?php endif; ?>

                                        <?php $quarterSum += $amount; $totalSum += $amount; ?>
                                        <?php if ($key % 3 == 0): ?>
                                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                                <div class="pl-1 pr-1">
                                                    <?= TextHelper::invoiceMoneyFormat($quarterSum, 2) ?>
                                                </div>
                                            </td>

                                            <?php if ($showPercent): ?>
                                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                                <div class="pl-1 pr-1">
                                                    <?php // percent Quarter ?>
                                                </div>
                                            </td>
                                            <?php endif; ?>
                                            <?php if ($showPercent2): ?>
                                                <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                                    <div class="pl-1 pr-1">
                                                        <?php // percent2 Quarter ?>
                                                    </div>
                                                </td>
                                            <?php endif; ?>

                                            <?php $quarterSum = 0; ?>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                    <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $subItemID; ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($totalSum, 2); ?>
                                        </div>
                                    </td>

                                    <?php if ($showPercent): ?>
                                    <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $subItemID; ?>">
                                        <div class="pl-1 pr-1">
                                            <?php // percent Year ?>
                                        </div>
                                    </td>
                                    <?php endif; ?>
                                    <?php if ($showPercent2): ?>
                                        <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $subItemID; ?>">
                                            <div class="pl-1 pr-1">
                                                <?php // percent2 Year ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>

                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <!-- end SUBITEMS -->

                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <!-- TOTALS -->
            <tr class="not-drag cancel-drag">
                <td class="pl-2 pr-1 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Итого на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = $totalSum = 0; ?>
                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = $data['growingBalance'][$monthNumber] ?? 0;
                    ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>

                    <?php if ($showPercent): ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::numberFormat(100, 2); ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <?php if ($showPercent2): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat($amount / (ArrayHelper::getValue($dataIncomeByMonth, $monthNumber) ?: 9E9) * 100, 2); ?>
                            </div>
                        </td>
                    <?php endif; ?>

                    <?php $quarterSum += $amount; $totalSum += $amount; ?>

                    <?php if ($key % 3 == 0): ?>

                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) ?>

                            </div>
                        </td>

                        <?php if ($showPercent): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat(100, 2) ?>
                            </div>
                        </td>
                        <?php endif; ?>
                        <?php if ($showPercent2): ?>
                            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::numberFormat($quarterSum / (ArrayHelper::getValue($dataIncomeByMonth, "q{$quarter}") ?: 9E9) * 100, 2); ?>
                                </div>
                            </td>
                        <?php endif; ?>

                        <?php $quarterSum = 0; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance']['totalFlowSum']) ? $data['growingBalance']['totalFlowSum'] : 0, 2); ?>
                    </div>
                </td>

                <?php if ($showPercent): ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::numberFormat(100, 2); ?>
                    </div>
                </td>
                <?php endif; ?>
                <?php if ($showPercent2): ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= (isset($data['growingBalance']['totalFlowSum']))
                                ? TextHelper::numberFormat($data['growingBalance']['totalFlowSum'] / (ArrayHelper::getValue($dataIncomeByMonth, "y") ?: 9E9) * 100, 2)
                                : 0
                            ?>
                        </div>
                    </td>
                <?php endif; ?>

            </tr>
            </tbody>

        </table>
    </div>
</div>