<?php

use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\models\cash\CashFlowsBase;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Contractor;

/* @var $model PaymentCalendarSearch */

$contractorFilterItems = $model->getContractorFilterItems();
$reasonFilterItems = $model->getReasonFilterItems();
$priorityFilterItems = [
    '' => 'Все',
    Contractor::PAYMENT_PRIORITY_HIGH => '1 - cамые приоритетные',
    Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - менее приоритетные',
    Contractor::PAYMENT_PRIORITY_LOW => '3 - наименьший приоритет'
];
$flowFilterItems = [
    '' => 'Все',
    CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
    CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
];

$hasFilters = strlen($model->flow_type) || strlen($model->contractor_id) || strlen($model->reason_id) || strlen($model->priority_type);
?>
<!-- filter -->
<div class="dropdown popup-dropdown popup-dropdown_filter cash-filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
    <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="button-txt">Фильтр</span>
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </button>
    <div class="dropdown-menu keep-open" aria-labelledby="filter">
        <div class="popup-dropdown-in p-3">
            <div class="row">
                <div class="form-group col-6 mb-3">
                    <div class="dropdown-drop" data-id="dropdown1">
                        <div class="label">Приход/Расход</div>
                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                           data-target="dropdown1" onclick="toggleVisible(this); return false;">
                            <span>
                                <?= ArrayHelper::getValue($flowFilterItems, $model->flow_type, 'Все') ?>
                            </span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-drop-menu" data-id="dropdown1">
                            <div class="dropdown-drop-in">
                                <?= $form->field($model, 'flow_type', [
                                    'options' => [
                                        'class' => 'form-filter-list list-clr pt-3',
                                    ],
                                ])->label(false)->radioList($flowFilterItems, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return Html::radio($name, $checked, [
                                            'value' => $value,
                                            'label' => $label,
                                            'labelOptions' => [
                                                'class' => 'p-2 no-border w-100',
                                            ],
                                            'data-default' => ($checked) ? 1:0
                                        ]);
                                    },
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-6 mb-3">
                    <div class="dropdown-drop" data-id="dropdown2">
                        <div class="label">Контрагент</div>
                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                           data-target="dropdown2" onclick="toggleVisible(this); return false;">
                            <span>
                                <?= ArrayHelper::getValue($contractorFilterItems, $model->contractor_id, 'Все контрагенты') ?>
                            </span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-drop-menu" data-id="dropdown2">
                            <div class="dropdown-drop-in">
                                <?= $form->field($model, 'contractor_id', [
                                    'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                ])->label(false)->radioList($contractorFilterItems, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return Html::radio($name, $checked, [
                                            'value' => $value,
                                            'label' => $label,
                                            'labelOptions' => [
                                                'class' => 'p-2 no-border w-100',
                                            ],
                                            'data-default' => ($checked) ? 1:0
                                        ]);
                                    },
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-6 mb-3">
                    <div class="dropdown-drop" data-id="dropdown3">
                        <div class="label">Статья</div>
                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                           data-target="dropdown3" onclick="toggleVisible(this); return false;">
                            <span>
                                <?= ArrayHelper::getValue($reasonFilterItems, $model->reason_id, 'Все статьи') ?>
                            </span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-drop-menu" data-id="dropdown3">
                            <div class="dropdown-drop-in">
                                <?= $form->field($model, 'reason_id', [
                                    'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                ])->label(false)->radioList(array_merge(['' => 'Все статьи'], $model->reasonFilterItems), [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return Html::radio($name, $checked, [
                                            'value' => $value,
                                            'label' => $label,
                                            'labelOptions' => [
                                                'class' => 'p-2 no-border w-100',
                                            ],
                                            'data-default' => ($checked) ? 1:0
                                        ]);
                                    },
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-6 mb-3">
                    <div class="dropdown-drop" data-id="dropdown1">
                        <div class="label">Приоритет</div>
                        <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                           data-target="dropdown1" onclick="toggleVisible(this); return false;">
                            <span>
                                <?= ArrayHelper::getValue($priorityFilterItems, $model->priority_type, 'Все') ?>
                            </span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-drop-menu" data-id="dropdown1">
                            <div class="dropdown-drop-in">
                                <?= $form->field($model, 'priority_type', [
                                    'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                ])->label(false)->radioList($priorityFilterItems, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return Html::radio($name, $checked, [
                                            'value' => $value,
                                            'label' => $label,
                                            'labelOptions' => [
                                                'class' => 'p-2 no-border w-100',
                                            ],
                                            'data-default' => ($checked) ? 1:0
                                        ]);
                                    },
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between mt-3">
                <div class="col-6 pr-0">
                    <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                        <span>Применить</span>
                    </button>
                </div>
                <div class="col-6 pl-0 text-right">
                    <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                        <span>Сбросить</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>