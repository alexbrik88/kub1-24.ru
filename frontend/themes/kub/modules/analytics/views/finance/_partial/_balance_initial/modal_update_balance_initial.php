<?php
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\components\date\DateHelper;
use frontend\modules\analytics\models\balance\BalanceInitial;

/** @var BalanceInitial $initialModel */

$formattedInitialDate = DateHelper::format($initialModel->date, 'd.m.Y', 'Y-m-d')
?>

<!-- DATE -->
<div class="modal fade" id="modal-balance-initial-date" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить дату начального баланса</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>

                <div class="form-body">
                    <div class="date-picker-wrap">
                        <?= Html::textInput(BalanceInitial::MAIN_DATE, $formattedInitialDate, [
                            'class' => 'form-control date-picker ico',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'style' => 'max-width: 135px'
                        ]); ?>
                    </div>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'style' => 'width: 130px!important;',
                            'data-url' => Url::to(['/analytics/finance/update-balance-initial', 'param' => BalanceInitial::MAIN_DATE])
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CAPITAL -->
<div class="modal fade" id="modal-balance-initial-capital" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить уставный капитал</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>

                <div class="form-body">
                    <?= Html::textInput(BalanceInitial::CAPITAL, round($initialModel->company->capital / 100, 2), [
                        'class' => 'form-control',
                        'size' => 16,
                        'style' => 'max-width: 135px'
                    ]); ?>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'style' => 'width: 130px!important;',
                            'data-url' => Url::to(['/analytics/finance/update-balance-initial', 'param' => BalanceInitial::CAPITAL])
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- UNDISTRIBUTED PROFIT -->
<div class="modal fade" id="modal-balance-initial-undistributed-profit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить нераспределенную прибыль</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>

                <div class="form-body">
                    <?= Html::textInput(BalanceInitial::UNDISTRIBUTED_PROFIT, round($initialModel->undistributed_profit / 100, 2), [
                        'class' => 'form-control',
                        'size' => 16,
                        'style' => 'max-width: 135px'
                    ]); ?>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'style' => 'width: 130px!important;',
                            'data-url' => Url::to(['/analytics/finance/update-balance-initial', 'param' => BalanceInitial::UNDISTRIBUTED_PROFIT])
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- STORE -->
<div class="modal fade" id="modal-balance-initial-store" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4 text-center">
                    Товары лучше и удобнее добавлять в разделе <br/> Дополнительно -> Товары ->
                        <a target="_blank" href="/product/index?productionType=1&ProductSearch[filterStatus]=2">Склад</a>
                </h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="form-body">
                    <div class="mt-3 d-flex justify-content-center">
                        <button type="button"
                                style="width: 130px!important"
                                class="button-clr button-width button-regular button-hover-transparent"
                                data-dismiss="modal">Ок</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- STORE INITIAL DATE -->
<div class="modal fade" id="modal-balance-store-initial-date" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить дату начального баланса</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>

                <div class="form-body">
                    <div class="date-picker-wrap">
                        <?= Html::textInput('newProductInitialBalanceDate', $formattedInitialDate, [
                            'class' => 'form-control date-picker ico',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'style' => 'max-width: 135px'
                        ]); ?>
                    </div>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'style' => 'width: 130px!important;',
                            'data-url' => Url::to(['/analytics/finance/update-balance-initial', 'param' => 'newProductInitialBalanceDate'])
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CREDIT -->
<div class="modal fade" id="modal-balance-initial-credit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4 text-center">
                    Кредиты лучше и удобнее добавлять в разделе <br/> Контроль долгов ->
                    <a target="_blank" href="/analytics/finance/loans">Кредиты</a>
                </h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="form-body">
                    <div class="mt-3 d-flex justify-content-center">
                        <button type="button"
                                style="width: 130px!important"
                                class="button-clr button-width button-regular button-hover-transparent"
                                data-dismiss="modal">Ок</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- UNDISTRIBUTED PROFIT -->
<div class="modal fade" id="modal-balance-initial-fixed-assets-other" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить прочие внеаборотные активы</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>

                <div class="form-body">
                    <?= Html::textInput(BalanceInitial::FIXED_ASSETS_OTHER, round($initialModel->fixed_assets_other / 100, 2), [
                        'class' => 'form-control',
                        'size' => 16,
                        'style' => 'max-width: 135px'
                    ]); ?>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::button('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'style' => 'width: 130px!important;',
                            'data-url' => Url::to(['/analytics/finance/update-balance-initial', 'param' => BalanceInitial::FIXED_ASSETS_OTHER])
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>