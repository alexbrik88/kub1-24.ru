<?php

use Cake\Utility\Text;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\widgets\TableViewWidget;
use php_rutils\RUtils;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\components\date\DateHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $floorMap array
 */

/*
$planContractorFlow structure

array(3) {
  ["common\models\cash\CashBankFlows"]=>
  array(1) {
    [54]=>
    array(1) {
      [0]=>
      int(779)
    }
  }
  ["common\models\cash\CashOrderFlows"]=>
  array(1) {
    [26]=>
    array(1) {
      [0]=>
      int(429)
    }
  }
  ["common\models\cash\CashEmoneyFlows"]=>
  array(0) {
  }
}

 */

if ($searchByName = Yii::$app->request->get('byContractorName')) {
    $filterContractorIds = Contractor::find()->where(['company_id' => Yii::$app->user->identity->company->id])
        ->andWhere(['or', ['like', 'name', $searchByName], ['like', 'ITN', $searchByName]])
        ->select('id')
        ->column() ?: [-1];
} else {
    $filterContractorIds = [];
}

$models = PlanCashContractor::find()
    ->where(['company_id' => Yii::$app->user->identity->company->id])
    ->andWhere(['plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS])
    ->andFilterWhere(['contractor_id' => $filterContractorIds])
    ->asArray()
    ->all();

$planContractorFlow = [
    PlanCashContractor::$PAYMENT_TO_CLASS[1] => [],
    PlanCashContractor::$PAYMENT_TO_CLASS[2] => [],
    PlanCashContractor::$PAYMENT_TO_CLASS[3] => [],
];
foreach ($models as $m) {

    $isIncome = ($m['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME);

    $flow = $m['flow_type'];
    $payment = $m['payment_type'];
    $item = $m['item_id'] ?: 'all';
    $contractor = $m['contractor_id'];

    $paymentClass = PlanCashContractor::$PAYMENT_TO_CLASS[$payment] or die('unknown payment');

    if (!isset($planContractorFlow[$paymentClass]))
        $planContractorFlow[$paymentClass] = [];
    if (!isset($planContractorFlow[$paymentClass][$item]))
        $planContractorFlow[$paymentClass][$item] = [];
    if (false === array_search($contractor, $planContractorFlow[$paymentClass][$item]))
        $planContractorFlow[$paymentClass][$item][] = $contractor;
}

$emptyData = [
    1 => [
        'name' => 'Плановые ПРИХОДЫ',
        'flowSum' => 0,
        'cash_bank_flows' => [
            'name' => 'Банк',
            'flowSum' => 0
        ],
        'cash_order_flows' => [
            'name' => 'Касса',
            'flowSum' => 0
        ],
        'cash_emoney_flows' => [
            'name' => 'E-money',
            'flowSum' => 0
        ],
    ],
    0 => [
        'name' => 'Плановые РАСХОДЫ',
        'flowSum' => 0,
        'cash_bank_flows' => [
            'name' => 'Банк',
            'flowSum' => 0
        ],
        'cash_order_flows' => [
            'name' => 'Касса',
            'flowSum' => 0
        ],
        'cash_emoney_flows' => [
            'name' => 'E-money',
            'flowSum' => 0
        ],
    ]
];
//var_dump($searchModel->searchAutoPlanItems($planContractorFlow, false));die;
$data = array_replace_recursive($emptyData, $searchModel->searchAutoPlanItems($planContractorFlow, false));

$years = $data['years'];
ksort($years);
unset($data['years']);
$key = 0;

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');
//var_dump($data);die;
?>

<?php
Pjax::begin([
    'id' => 'plan-by-prev-periods-pjax',
    'enablePushState' => false,
    'formSelector' => '#search_auto_plan_by_prev_periods',
    'timeout' => 10000,
    'scrollTo' => false,
]);
?>

<div class="wrap wrap_padding_none mb-2 pt-1">
    <div class="col-12 pb-2 mt-2" style="display: flex">
        <div class="col-9 pl-0 mt-1">
            <span class="label mb-2">
                На этой закладке плановые платежи формируются по предыдущим приходам от Покупателей и вашим платежам
                Поставщикам. Плановые платежи формируются только по тем контрагентам, по которым были платежи каждый
                месяц последние 3 месяца, т.е. ежемесячным платежам. Например, платежи за аренду, интернет,
                телефонию и т.д.
            </span>
            <br/>
            <span class="label">
                <strong style="color:#001424">Кнопка «Добавить контрагента»</strong> - добавляйте вручную Покупателей и Поставщиков, по которым будут формироваться Плановые платежи.
                По добавленным контрагентам будет формироваться плановый платеж на среднюю сумму за последние 3 месяца (сумму можно поменять) и среднюю дату платежа.
            </span>
        </div>
        <div class="col-3 pr-0 pl-2 mb-1">
            <button type="button" class="button-regular button-hover-transparent ml-auto d-block w-100"
                    data-toggle="modal" data-target="#add-autoplan-contractor-prev">
                <?= \frontend\components\Icon::get('add-icon', ['style' => 'margin-top:-2px; margin-right:-4px;']) ?>
                <span class="ml-2">Добавить контрагента</span>
            </button>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_pc']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'id' => 'search_auto_plan_by_prev_periods',
            'enableClientValidation' => false,
            'method' => 'GET',
        ]); ?>
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group flex-grow-1 mr-2">
                <?= \yii\bootstrap4\Html::input('search', 'byContractorName', $searchByName, ['class' => 'form-control search-by-contractor', 'placeholder' => 'Поиск по контрагенту...']); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2 pt-1">
<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="table-bleak table table-style table-count-list mb-0 plan-contractor <?= $tabViewClass ?>">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" colspan="<?= $years ? count($years) : 1; ?>">
                    <div class="text-grey weight-700 ml-1 nowrap">Средняя сумма ежемесячных платежей</div>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <div class="text-grey weight-700 ml-1 nowrap">Сумма <br> планового <br> платежа</div>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <div class="text-grey weight-700 ml-1 nowrap">Дата <br> планового <br> платежа</div>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2" colspan="2">
                    <div class="weight-700 ml-1 nowrap">Дата <br> окончания <br> планирования</div>
                </th>
            </tr>
            <tr>
                <?php if ($years): ?>
                    <?php foreach ($years as $year): ?>
                        <th class="pl-2 pr-2"><div class="pl-1 pr-1 nowrap">за <?= $year ?> г.</div></th>
                    <?php endforeach; ?>
                <?php else: ?>
                    <th class="pl-2 pr-2"></th>
                <?php endif; ?>
            </tr>
            </thead>

            <tbody>

            <!-- 1. FLOW_TYPE -->
            <?php foreach ($data as $flowType => $flowTypeBlock): ?>
                <tr class="main-checkbox-side-sub-item">
                    <td class="expenditure_type pl-2 pr-2 pt-3 pb-3">
                        <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                            <div class="text_size_14 weight-700 mr-2 nowrap"><?= $flowTypeBlock['name']; ?></div>
                        </div>
                    </td>
                    <?php foreach ($years as $year): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::invoiceMoneyFormat(isset($flowTypeBlock['flowSum'][$year]) ? ($flowTypeBlock['flowSum'][$year]) : 0, 2); ?>
                            </div>
                        </td>
                    <?php endforeach; ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap text-right">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?php
                            // OLD: $amount = $searchModel->calculatePlanMonthAmount($flowType);
                            $amount = PlanCashContractor::find()->where([
                                'company_id' => Yii::$app->user->identity->company->id,
                                'plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                                'flow_type' => $flowType,
                            ])->andFilterWhere(['contractor_id' => $filterContractorIds])->sum('amount');
                            ?>
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                </tr>
                <?php unset($flowTypeBlock['name']); ?>

                <!-- 2. PAYMENT_TYPE -->
                <?php foreach ($flowTypeBlock as $paymentType => $paymentTypeBlock): ?>

                    <?php $firstFloor = "first-floor-{$flowType}-{$paymentType}"; ?>
                    <?php $isFirstOpened = ArrayHelper::getValue($floorMap, $firstFloor, 1) ?>

                    <?php if ($paymentType == 'flowSum') { continue; }
                    $cloneBlock = $paymentTypeBlock;
                    unset($cloneBlock['name']);
                    unset($cloneBlock['flowSum']);
                    $items = array_keys($cloneBlock);
                    $contractors = [];
                    foreach ($paymentTypeBlock as $item => $itemBlock) {
                        if ($item == 'flowSum' || $item == 'name') { continue; }
                        $cloneBlock = $itemBlock;
                        unset($cloneBlock['name']);
                        unset($cloneBlock['flowSum']);
                        $contractors = array_merge($contractors, array_keys($cloneBlock));
                    }
                    $contractors = array_map("unserialize", array_unique(array_map("serialize", $contractors)));
                    // OLD: $amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $items, $contractors);
                    //if ($amount == 0) {
                    //    continue;
                    //}

                    $amount = PlanCashContractor::find()->where([
                        'company_id' => Yii::$app->user->identity->company->id,
                        'plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                        'flow_type' => $flowType,
                        'payment_type' => PlanCashContractor::normalizePaymentType($paymentType),
                    ])->andFilterWhere(['contractor_id' => $filterContractorIds])->sum('amount');
                    ?>

                    <tr class="main-checkbox-side-sub-item">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <button class="table-collapse-btn button-clr ml-1 <?= $isFirstOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $firstFloor ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-dark weight-700 text_size_14 ml-1">
                                    <?= $paymentTypeBlock['name']; ?>
                                </span>
                            </button>
                            <?php if (!empty($paymentTypeBlock) && count($paymentTypeBlock) > 2): ?>
                                <div class="badge-contractors-count">
                                    <?php $countContractors = 0; foreach ($paymentTypeBlock as $key => $itemBlock) {
                                        if (in_array($key, ['name', 'flowSum']))
                                            continue;

                                        $countContractors += (count($itemBlock) - 2);
                                    } ?>
                                    <?= ($countContractors + 2) . ' ' .
                                    RUtils::numeral()->choosePlural($countContractors + 2, !$flowType ?
                                        ['Поставщик', 'Поставщика', 'Поставщиков'] :
                                        ['Покупатель', 'Покупателя', 'Покупателей']) ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <?php foreach ($years as $year): ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat(isset($paymentTypeBlock['flowSum'][$year]) ? ($paymentTypeBlock['flowSum'][$year]) : 0, 2); ?>
                                </div>
                            </td>
                        <?php endforeach; ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap text-right">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </div>
                        </td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                    </tr>
                    <?php unset($paymentTypeBlock['name']); ?>

                    <!-- 3. EXPENDITURE ITEM (e.g. "Айти") -->
                    <?php foreach ($paymentTypeBlock as $item => $itemBlock): ?>

                        <?php $secondFloor = "second-floor-{$flowType}-{$paymentType}-{$item}"; ?>
                        <?php $isSecondOpened = ArrayHelper::getValue($floorMap, $secondFloor, 1) ?>

                        <?php if ($item == 'flowSum') { continue; }
                        $cloneBlock = $itemBlock;
                        unset($cloneBlock['flowSum']);
                        $contractors = array_keys($cloneBlock);
                        //$amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $item, $contractors);
                        //if ($amount == 0) {
                        //    continue;
                        //}

                        $amount = PlanCashContractor::find()->where([
                                'company_id' => Yii::$app->user->identity->company->id,
                                'plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                                'flow_type' => $flowType,
                                'payment_type' => PlanCashContractor::normalizePaymentType($paymentType),
                                'item_id' => $item
                            ])->andFilterWhere(['contractor_id' => $filterContractorIds])->sum('amount');
                        ?>
                        <tr class="expenditure-item-line <?= $paymentType; ?> <?= $isFirstOpened ? '' : 'd-none' ?>" data-id="<?= $firstFloor ?>">
                            <td class="expenditure_type pl-3 pr-3 pt-3 pb-3">
                                <button class="table-collapse-btn button-clr ml-1 <?= $isSecondOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $secondFloor ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey text_size_14 mr-5">
                                        <?= $itemBlock['name']; ?>
                                    </span>
                                </button>
                            </td>
                            <?php foreach ($years as $year): ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat(isset($itemBlock['flowSum'][$year]) ? ($itemBlock['flowSum'][$year]) : 0, 2); ?>
                                    </div>
                                </td>
                            <?php endforeach; ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap text-right">
                                <div class="pl-1 pr-1">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                </div>
                            </td>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                        </tr>
                        <?php unset($itemBlock['name']); ?>

                        <!-- 4. CONTRACTOR (e.g "ООО "Фирма"") -->
                        <?php foreach ($itemBlock as $contractor => $contractorBlock): ?>
                            <?php if ($contractor == 'flowSum') {
                                continue;
                            }
                            //$cashFlowsAutoPlanItem = $searchModel->findCashFlowAutoPlan(
                            //    $flowType,
                            //    $searchModel->getPaymentTypeByTableName($paymentType),
                            //    $item,
                            //    $contractor
                            //);
                            //$amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $item, $contractor);
                            //if ($amount == 0) {
                            //    continue;
                            //}

                            //$planMonthDate = $searchModel->calculatePlanMonthDate($flowType, $paymentType, $item, $contractor);
                            //$endPlanDate = '31.12.' . date('Y');

                            /** @var  $planCashContractor PlanCashContractor */
                            $planCashContractor = PlanCashContractor::find()->where([
                                'company_id' => Yii::$app->user->identity->company->id,
                                'plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                                'contractor_id' => $contractor,
                                'flow_type' => $flowType,
                                'payment_type' => PlanCashContractor::normalizePaymentType($paymentType),
                                'item_id' => $item
                            ])->one();

                            if (!$planCashContractor)
                                continue;

                            $amount = $planCashContractor->amount;
                            $planMonthDate = DateHelper::format($planCashContractor->plan_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            $endPlanDate = DateHelper::format($planCashContractor->end_plan_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

                            ?>
                            <tr class="contractor-item-line <?= $isSecondOpened ? '' : 'd-none' ?>" data-id="<?= $secondFloor ?>" id="<?= "item-{$flowType}-{$paymentType}-{$item}-{$contractor}" ?>"
                                data-plan-type="<?= PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS ?>">
                                <td class="pl-3 pr-3 pt-3 pb-3">
                                    <div class="deleted-auto-plan-item hidden"></div>
                                    <div class="pl-4">
                                        <span class="text-grey text_size_14 mr-5">
                                            <?= Text::truncate($contractorBlock['name'], 35, [
                                                'ellipsis' => '...',
                                                'exact' => true,
                                            ]); ?>
                                        </span>
                                    </div>
                                </td>
                                <?php foreach ($years as $year): ?>
                                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat(isset($contractorBlock['flowSum'][$year]) ? ($contractorBlock['flowSum'][$year]) : 0, 2); ?>
                                        </div>
                                    </td>
                                <?php endforeach; ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap text-right">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                    </div>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                    <div class="pl-1 pr-1">
                                        <?= $planMonthDate; ?>
                                    </div>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                    <div class="pl-1 pr-1">
                                        <?= $endPlanDate; ?>
                                    </div>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap td-action">
                                    <div class="d-flex flex-nowrap">
                                        <?php $dropdownKey =  "dropdown-{$flowType}-{$paymentType}-{$item}-{$contractor}"; ?>
                                        <div class="dropdown dropdown_autoplan_clipped dropdown_tr-settings d-inline-block" data-highlight="tr" data-id="<?= $dropdownKey ?>">

                                            <button class="button-clr link" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg class="svg-icon">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                                </svg>
                                            </button>

                                            <div class="dropdown-menu dropdown_menu_autoplan_clipped p-0 keep-open" aria-labelledby="filter">
                                                <div class="popup-dropdown-in p-2">
                                                    <form class="form-control-wrap p-1" action="<?= Url::to([
                                                        '/analytics/auto-plan-ajax/update-contractor',
                                                        'planType' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                                                        'contractorId' => $contractor,
                                                        'flowType' => $flowType,
                                                        'paymentType' => $paymentType,
                                                        'itemId' => $item
                                                    ]) ?>">
                                                        <div class="row row_indents_sm flex-nowrap">
                                                            <?php foreach ($years as $year): ?>
                                                                <div class="column form-group mb-3">
                                                                    <input class="form-control" type="text" disabled value="<?= TextHelper::invoiceMoneyFormat(isset($contractorBlock['flowSum'][$year]) ? ($contractorBlock['flowSum'][$year]) : 0, 2); ?>">
                                                                </div>
                                                            <?php endforeach; ?>
                                                            <div class="column form-group mb-3">
                                                                <input name="PlanCashContractor[amount]" class="form-control js_input_to_money_format" type="text" value="<?=  TextHelper::invoiceMoneyFormat($amount, 2, '.', '');  ?>">
                                                            </div>
                                                            <div class="column form-group mb-3">
                                                                <div class="date-picker-wrap">
                                                                    <input name="PlanCashContractor[plan_date]" class="form-control date-picker" type="text" value="<?= $planMonthDate ?>">
                                                                    <svg class="date-picker-icon svg-icon input-toggle">
                                                                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                            <div class="column form-group custom-end-plan-date mb-3">
                                                                <div class="date-picker-wrap">
                                                                    <input name="PlanCashContractor[end_plan_date]" class="form-control date-picker" type="text" value="<?= $endPlanDate ?>">
                                                                    <svg class="date-picker-icon svg-icon input-toggle">
                                                                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row row_indents_sm pt-1">
                                                            <div class="column">
                                                                <button class="update-plan-contractor ladda button-clr button-regular-no-compact button-regular_red button-width pr-4 pl-4" type="button" onclick="AUTOPLAN.updatePlanContractorByKey(this)" data-dropdown-id="<?= $dropdownKey ?>">
                                                                    <span class="ladda-label">Сохранить</span>
                                                                </button>
                                                            </div>
                                                            <div class="column ml-auto">
                                                                <button class="button-clr button-regular-no-compact button-hover-transparent width-120 pl-4 pr-4" data-dropdown-disable type="button">Отменить</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="delete-contractor-btn button-clr link ml-2" type="button" data-pjax="0" data-url="<?= Url::to([
                                            '/analytics/auto-plan-ajax/delete-contractor',
                                            'planType' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                                            'contractorId' => $contractor,
                                            'flowType' => $flowType,
                                            'paymentType' => $paymentType,
                                            'itemId' => $item
                                        ]) ?>">
                                            <svg class="svg-icon">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php unset($contractorBlock['name']); ?>

                            <?php $key++; ?>

                        <?php endforeach; ?>

                    <?php endforeach; ?>

                <?php endforeach; ?>

            <?php endforeach; ?>


            </tbody>
        </table>
    </div>
</div>
</div>
<?php Pjax::end() ?>