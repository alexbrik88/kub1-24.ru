<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;

/** @var $cellIds array */
/** @var $dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $isSumQuarters */
/** @var $trClass string */
/** @var $level int */
/** @var $year int */

$trData  = '';
$trData .= (isset($dataPaymentType)) ? ('data-payment_type="'.$dataPaymentType.'"') : '';
$trData .= (isset($dataFlowType)) ? ('data-flow_type="'.$dataFlowType.'"') : '';
$trData .= (isset($dataItemId)) ? ('data-item_id="'.$dataItemId.'"') : '';
$trData .= (isset($dataId)) ? ('data-id="'.$dataId.'"') : '';
$trData .= (isset($dataType)) ? ('data-type_id="'.$dataType.'"') : '';
$trData .= (isset($dataWalletId)) ? ('data-wallet_id="'.$dataWalletId.'"') : '';
$trData .= (isset($dataHoverText)) ? ('data-hover_text="'.($dataHoverText).'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

$canHover = $canHover ?? true;
$isSumQuarters = $isSumQuarters ?? true;
$isSortable = $isSortable ?? false;
$isOpenable = $isOpenable ?? false;
$isBold = $level <= 1;
$isActiveRow = $level == 2 || $level == 3 && $isOpenable;
$negativeRed = $negativeRed ?? false;
$positiveRed = $positiveRed ?? false;
?>
<tr class="<?=($trClass)?> <?=($level >= 3 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="pl-2 pr-2 pt-3 pb-3 <?=($isActiveRow || $isSortable ? 'checkbox-td':'')?> <?=($isBold ? 'tooltip-td':'')?>">
        <?php if ($level == 4): ?>
            <span class="text-grey text_size_12 mr-1 m-l-purse-big">
                <?= $title ?>
            </span>
        <?php elseif ($level == 3 && $isSortable): ?>
            <svg class="sortable-row-icon svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
            </svg>
            <span class="text-grey text_size_14"><?= $title; ?></span>
        <?php elseif ($level == 3 && !$isOpenable): ?>
            <span class="text-grey text_size_14 mr-1 m-l-purse">
                <?= $title ?>
            </span>
        <?php elseif ($level == 2 && !$isOpenable): ?>
            <span class="text-grey text_size_14 mr-1 ml-1">
                <?= $title ?>
            </span>
        <?php elseif ($level == 3 || $level == 2): ?>
            <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor ? 'active':'' ?> <?= ($level == 3) ? 'icon-grey':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="<?=($isBold ? 'weight-700':'text-grey')?> text_size_14 ml-1"><?= $title; ?></span>
            </button>
        <?php elseif ($level == 0): ?>
            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                <div class="text_size_14 mr-2 nowrap weight-700">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php else: ?>
            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                <div class="text_size_14 mr-2 nowrap <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </td>
    <?php $key = $quarterSum = $totalSum = 0; ?>
    <?php foreach (AbstractFinance::$month as $month => $monthText): ?>
        <?php $key++;
        $quarter = (int)ceil($key / 3);
        $amount = isset($data[$month]) ? $data[$month] : 0;
        if ($isSumQuarters) {
            $quarterSum += $amount;
            $totalSum += $amount;
        } else {
            $quarterSum = $amount;
            $totalSum = $amount;
        }
        ?>

        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? '' : 'd-none' ?> <?= $canHover ? 'can-hover':'' ?>" data-id="<?= $cellIds[$quarter] ?>" data-collapse-cell>
            <div class="pl-1 pr-1 <?=(($negativeRed && $amount < 0 || $positiveRed && $amount > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
            </div>
        </td>
        <?php if ($key % 3 == 0): ?>
            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $dMonthes[$quarter] ? 'd-none' : '' ?> <?= $canHover ? 'can-hover':'' ?>" data-id="<?= $cellIds[$quarter] ?>" data-collapse-cell-total>
                <div class="pl-1 pr-1 <?=(($negativeRed && $quarterSum < 0 || $positiveRed && $quarterSum > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                    $quarterSum = 0; ?>
                </div>
            </td>
        <?php endif; ?>
    <?php endforeach; ?>
    <td class="pl-2 pr-2 pt-3 pb-3 nowrap total-block <?=($isBold ? 'weight-700':'')?> <?= $canHover ? 'can-hover':'' ?>">
        <div class="pl-1 pr-1 <?=(($negativeRed && $totalSum < 0 || $positiveRed && $totalSum > 0) ? 'red-link' : 'text-dark-alternative')?>">
            <?= TextHelper::invoiceMoneyFormat($totalSum, 2); ?>
        </div>
    </td>
</tr>