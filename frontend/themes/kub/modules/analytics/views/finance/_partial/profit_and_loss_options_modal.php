<?php

use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\AnalyticsOptions as Options;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use kartik\select2\Select2;

$company = Yii::$app->user->identity->company;

$show = $show ?? [];

$SHOW = [
    'part_articles' => $show['part_articles'] ?? 1,
    'part_additional' => $show['part_additional'] ?? 1,
    'part_revenue_analytics' => $show['part_revenue_analytics'] ?? 1,
    'part_bottom_text' => $show['part_bottom_text'] ?? 1,
];

Modal::begin([
    'id' => 'profit-and-loss-options',
    'closeButton' => false,
    'toggleButton' => false,
]);

$title = 'Настройка отчета о прибылях и убытках (ОПиУ)';

$model = new AnalyticsArticleForm($company);

$formData = [
    [
        'title' => 'Статьи прихода',
        'type' => Article::FLOW_TYPE_INCOME,
        'key' => 'incomeItems',
        'items' => $model->getItemsList(Article::FLOW_TYPE_INCOME),
        'pal_types' => [
            Article::PAL_UNSET => 'Не установлено',
            Article::PAL_INCOME_REVENUE => 'Выручка',
            Article::PAL_INCOME_OTHER => 'Другие доходы',
            Article::PAL_INCOME_PERCENT => 'Проценты полученные',
        ]
    ],
    [
        'title' => 'Статьи расхода',
        'type' => Article::FLOW_TYPE_EXPENSE,
        'key' => 'expenseItems',
        'items' => $model->getItemsList(Article::FLOW_TYPE_EXPENSE),
        'pal_types' => [
            Article::PAL_UNSET => 'Не установлено',
            Article::PAL_EXPENSES_PRIME_COST => 'Себестоимость',
            Article::PAL_EXPENSES_VARIABLE => 'Переменные расходы',
            Article::PAL_EXPENSES_FIXED => 'Постоянные расходы',
            Article::PAL_EXPENSES_OTHER => 'Другие расходы',
            Article::PAL_EXPENSES_PERCENT => 'Проценты уплаченные',
            Article::PAL_EXPENSES_DIVIDEND => 'Дивиденды',
        ]
    ],
];

if ($model->monthGroupBy) {
    $disabledRevenueRuleTitle = 'Включена детализация по ';
    switch ($model->monthGroupBy) {
        case $model::MONTH_GROUP_INDUSTRY:
            $disabledRevenueRuleTitle .= ' направлениям.';
            $disabledRevenueRuleId = $model::REVENUE_RULE_GROUP_INDUSTRY;
            break;
        case $model::MONTH_GROUP_SALE_POINT:
            $disabledRevenueRuleTitle .= ' точкам продаж.';
            $disabledRevenueRuleId = $model::REVENUE_RULE_GROUP_SALE_POINT;
            break;
        case $model::MONTH_GROUP_PROJECT:
            $disabledRevenueRuleTitle .= ' проектам.';
            $disabledRevenueRuleId = $model::REVENUE_RULE_GROUP_PROJECT;
            break;
    }
    $disabledRevenueRuleTitle .= '<br/>Чтобы выбрать тут, необходимо отключить детализацию.';
} else {
    $disabledRevenueRuleId = false;
    $disabledRevenueRuleTitle = null;
}

?>

<style>
    .pal-options-select2 .select2-container--krajee-bs4 .select2-selection,
    .pal-options-select2 .select2-container--krajee-bs4 .select2-selection:focus {
        border: none!important;
    }
    .pal-options-select2 .select2-container .select2-selection--single {
        padding: 0!important;
        height: unset!important;
        min-height: unset!important;
    }
    .pal-options-select2 .select2-container--krajee-bs4 .select2-selection--single .select2-selection__arrow {
        display: none;
    }
    .pal-options-select2 .select2-container .select2-selection--single .select2-selection__rendered {
        color: #0097fd;
    }
    .pal-options-select2 .select2-container .select2-selection--single .select2-selection__rendered:hover {
        color: #0048aa;
    }
    .pal-options-select2.black .select2-container .select2-selection--single .select2-selection__rendered {
        color: #001424;
    }
    .pal-options-select2.black .select2-container .select2-selection--single .select2-selection__rendered:hover {
        color: #001b31;
    }
    .button-reset-pal-options {
        border-color: #0097fd;
        color: #0097fd;
    }
    .button-reset-pal-options:hover,
    .button-reset-pal-options:active {
        border-color: #0048aa;
        color: #0048aa;
    }
    .button-reset-pal-options > .ladda-spinner > div > div > div {
        background-color: #0097fd!important;
    }
    .button-reset-pal-options:hover > .ladda-spinner > div > div > div,
    .button-reset-pal-options:active > .ladda-spinner > div > div > div {
        background-color: #0048aa!important;
    }
    .pal-revenue-rule-box {
        padding-left: 30px;
    }
</style>

<h4 class="modal-title mb-4"><?= $title ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'profit-and-loss-options-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
    'action' => [
        '/analytics/finance-ajax/update-profit-and-loss-articles'
    ],
    'options' => [
        'data-pjax' => 0
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?php if ($SHOW['part_articles']): ?>

    <div class="row">
        <div class="col-12 text_size_14 mb-2">
            <strong><span style="text-decoration: underline">Настройка закрепления статей</span> прихода и расхода по разделам ОПиУ</strong><br/>
            Вы можете расположить статьи так, как вы считаете правильнее для вас и вашего бизнеса.
        </div>
    </div>

    <?php FOREACH ($formData as $data): ?>
    <div class="row mb-1">
        <div class="col-12 mb-2 text-bold">
            <button class="table-collapse-btn button-clr" type="button" data-collapse-row-trigger data-target="<?= $data['key'] ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="weight-700 ml-1"><?= $data['title'] ?></span>
            </button>
        </div>
        <div class="col-12 d-none" data-id="<?= $data['key'] ?>">
            <div class="row">
                <?php $i=0; ?>
                <?php $partLength = ceil(count($data['items']) / 2); ?>
                <?php foreach ($data['items'] as $item): ?>
                    <?php if ($i==0): ?>
                    <div class="col-6">
                    <?php endif; ?>
                        <div class="row">
                            <div class="col-6 pr-1">
                                <div class="text-truncate w-100 text-grey font-14" style="line-height: 1.45">
                                    <?= $item['name'] ?>
                                </div>
                            </div>
                            <div class="col-6 pl-1">
                                <div class="text-truncate w-100 pal-options-select2 <?= 0 == $item['profit_and_loss_type'] ? 'black':'' ?>">
                                    <?php if (in_array($item['id'], Article::UNSET_ALWAYS_ARTICLES[$data['type']])): ?>
                                        <?= Html::tag('span', Icon::QUESTION, [
                                            'title' => Article::UNSET_ALWAYS_ARTICLES_HELP[$data['type']][$item['id']],
                                            'title-as-html' => 1
                                        ]) ?>
                                    <?php else: ?>
                                        <?= Select2::widget([
                                            'name' => "AnalyticsArticleForm[{$data['key']}][{$item['id']}]",
                                            'data' => $data['pal_types'],
                                            'value' => $item['profit_and_loss_type'],
                                            'hideSearch' => true,
                                            'options' => [
                                                'class' => 'pal-options-select'
                                            ],
                                            'pluginOptions' => [
                                                'width' => '100%',
                                                'containerCssClass' => 'select2-pal-container',
                                                'dropdownCssClass' => 'select2-pal-dropdown',
                                            ],
                                        ]) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php $i++; ?>
                    <?php if ($i >= $partLength): ?>
                    </div>
                    <?php $i=0; ?>
                    <?php endif; ?>
                <?php endforeach ?>
                <?= ($i > 0) ? '</div>' : '' ?>
            </div>
        </div>
    </div>
    <?php ENDFOREACH; ?>

    <div class="row">
        <div class="col-12">
            <?php if (Options::isPalResetButtonEnable($company->id)): ?>
                <?= Html::tag('div', '<span class="ladda-label">Сбросить до установок КУБ24</span><span class="ladda-spinner"></span>', [
                    'id' => 'button-reset-pal-options',
                    'class' => 'button-regular button-clr ladda-button button-reset-pal-options',
                    'data-style' => 'expand-right',
                    'data-url' => '/analytics/finance-ajax/reset-profit-and-loss-articles-options',
                ]); ?>
            <?php else: ?>
                <?= Html::tag('div', '<span>Сбросить до установок КУБ24</span>', [
                    'class' => 'button-regular button-clr disabled',
                    'title' => 'Установки применены'
                ]); ?>
            <?php endif; ?>
        </div>
    </div>

    <?php endif; ?>

<?php if ($SHOW['part_additional']): ?>

    <div class="row mt-3">
        <div class="col-12 text_size_14 mb-2">
            <strong style="text-decoration: underline">Дополнительные настройки</strong>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'skipPrepayments', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('Не учитывать в выручке приходы без закрывающих документов (авансы)', ['class' => 'text_size_14']) ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'skipTaxes', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('Налог на прибыль равен нулю', ['class' => 'text_size_14']) ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'skipZeroesRows', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('Не выводить строки с нулями', ['class' => 'text_size_14']) ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'skipCalculatePrimeCosts', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('Отключить автоматическое формирование себестоимости', ['class' => 'text_size_14']) ?>
        </div>
        <?php if ($company->companyTaxationType->osno): ?>
        <div class="col-12">
            <?= $form->field($model, 'calcWithoutNds', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('В отчете выручка и расходы без НДС', ['class' => 'text_size_14']) ?>
        </div>
        <?php endif; ?>
        <div class="col-12">
            <?= $form->field($model, 'showAmortization', ['options' => ['class' => '']])
                ->checkbox(['class' => ''])
                ->label('Включить раздел Амортизация', ['class' => 'text_size_14']) ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'showEbit', ['options' => ['class' => '']])
                ->checkbox(['class' => '', 'disabled' => !$model->showAmortization])
                ->label('Включить EBIT', ['class' => 'text_size_14']) ?>
        </div>
    </div>

<?php endif; ?>

<?php if ($SHOW['part_revenue_analytics']): ?>

    <div class="row mt-3">
        <div class="col-12 text_size_14 mb-2">
            <strong style="text-decoration: underline">Аналитика по выручке</strong>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'revenueRuleGroup')
                ->label(false)->radioList(AnalyticsArticleForm::$revenueRuleGroups, [
                'id' => 'pal-revenue-rule-groups',
                'encode' => false,
                'item' => function ($index, $label, $name, $checked, $value) use ($form, $model, $disabledRevenueRuleId, $disabledRevenueRuleTitle) {
                    $disabled = ($value === $disabledRevenueRuleId);
                    $input = Html::radio($name, $checked, [
                        'value' => $value,
                        'class' => 'pal-revenue-rule-group-radio',
                        'label' => Html::tag('span', $label, [
                            'class' => 'text_size_14' . ($disabled ? ' text-grey' : ''),
                            'style' => 'font-weight: bold;',
                        ]),
                        'labelOptions' => [
                            'class' => 'pal-revenue-rule-group-label'
                        ],
                        'disabled' => $disabled,
                        'title' => ($disabled) ? $disabledRevenueRuleTitle : null,
                        'title-as-html' => 1
                    ]);

                    $input .= $this->render("_profit_and_loss_options/_revenue_rule", [
                        'form' => $form,
                        'model' => $model,
                        'groupValue' => $value,
                        'groupChecked' => $checked,
                    ]);

                    return Html::tag('div', $input, [
                        'class' => '',
                    ]);
                }
            ]); ?>
        </div>
    </div>

<?php endif; ?>

<?php if ($SHOW['part_bottom_text']): ?>
    <div class="row mt-3">
        <div class="col-12 text_size_14">
            <strong>ВАЖНО:</strong> данные настройки будут применены для всех компаний в вашем аккаунте. Без этого нет возможности сделать консолидированный отчет.
        </div>
    </div>
<?php endif; ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<script>

    $('#button-reset-pal-options').on('click', function(e)
    {
        const form = $(this).closest('form');
        form.find('[type="submit"]').attr('disabled', true);

        let l = Ladda.create(this);
        l.start();

        $.ajax($(this).attr("data-url"), {
            method: "POST",
            data: $(this).serialize(),
            success: function (data)
            {
                Ladda.stopAll();
                if (data.success) {
                    location.href = location.href;
                } else {
                    window.showToastr("Ошибка обновления");
                }
            },
            error: function ()
            {
                Ladda.stopAll();
                window.showToastr("Ошибка обновления");
            }
        });

        return false;
    });

    $('#profit-and-loss-options-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax($(this).attr("action"), {
            method: "POST",
            data: $(this).serialize(),
            success: function (data)
            {
                Ladda.stopAll();
                if (data.success) {
                    location.href = location.href;
                } else {
                    window.showToastr("Ошибка обновления");
                }
            },
            error: function ()
            {
                Ladda.stopAll();
                window.showToastr("Ошибка обновления");
            }
        });

        return false;
    });

    $('#profit-and-loss-options-form .table-collapse-btn').on('click', function()
    {
        if (!$(this).hasClass('active')) {
            $(this).closest('form').find('.table-collapse-btn').not(this).each(function() {
                if ($(this).hasClass('active'))
                    $(this).click();
            });
        }
    });

    $('#profit-and-loss-options-form .pal-options-select').on('change', function() {
        const selectWrapper = $(this).closest('.pal-options-select2');
        if (Number(this.value) === 0) {
            selectWrapper.addClass('black');
        } else {
            selectWrapper.removeClass('black');
        }
    });

    $('#profit-and-loss-options-form .pal-revenue-rule-group-radio').on('change', function() {
        var rule = $('.pal-revenue-rule-group-radio:checked').val();
        $("#pal-revenue-rule-groups .collapse.show").collapse("hide");
        if (rule) {
            $("#profit-and-loss-options-form .revenue-rule-" + rule).collapse("show");
        }
    });

    // amortization + ebit checkboxes
    $('#analyticsarticleform-showamortization').on('change', function() {
        if ($(this).prop('checked')) {
            $('#analyticsarticleform-showebit').prop('disabled', false).uniform();
        } else {
            $('#analyticsarticleform-showebit').prop('checked', false).prop('disabled', true).uniform();
        }
    });

</script>