<?php

use common\components\helpers\Html;
use frontend\components\PageSize;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\DebtReportSearch2;
use common\components\TextHelper;
use common\models\Contractor;
use \frontend\modules\analytics\models\DebtReportSearchAsBalance;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;

/* @var $this yii\web\View
 * @var $searchModel DebtReportSearch2
 * @var $searchModel2 DebtReportSearchAsBalance
 * @var $rowTotal []
 * @var $growingBalance []
 * @var $blocks []
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $reportType integer
 * @var $searchBy integer
 */

///////////////////////////////////////////
$contractors = $dataProvider2->getModels();
///////////////////////////////////////////

$isCurrentYear = $searchModel2->isCurrentYear;
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];
$floorMap = $floorMap ?? [];
$d_monthes = [
    1 => ArrayHelper::getValue($floorMap, 'first-cell', $currentMonthNumber < 4 && $isCurrentYear),
    2 => ArrayHelper::getValue($floorMap, 'second-cell', $currentMonthNumber > 3 && $currentMonthNumber < 7),
    3 => ArrayHelper::getValue($floorMap, 'third-cell', $currentMonthNumber > 6 && $currentMonthNumber < 10),
    4 => ArrayHelper::getValue($floorMap, 'fourth-cell', $currentMonthNumber > 9 && $isCurrentYear),
];

$rowContractors = $rowTotal = [];
foreach ($contractors as $data) {
    $contractor = Contractor::find()->where(['id' => $data['contractor_id']])->andWhere(['or', ['company_id' => $company->id], ['company_id' => null]])->one();
    $responsible = $contractor ? \common\models\EmployeeCompany::item($contractor->responsible_employee_id, $contractor->company_id) : null;

    $rowContractors[$data['contractor_id']] = [
        'id' => $data['contractor_id'],
        'name' => ($contractor) ?
            Html::a($contractor->nameWithType, ['/contractor/view', 'id' => $contractor->id, 'type' => $contractor->type], [
                'title' => html_entity_decode($contractor->nameWithType),
                'data-pjax' => 0
            ]) : $data['contractor_id'],
        'employee_id' => ($contractor) ? $contractor->responsible_employee_id : null,
        'employee_name' => $responsible ? $responsible->getShortFio() : '---',
        'data' => [
            "01" => $data['month_1'],
            "02" => $data['month_2'],
            "03" => $data['month_3'],
            "04" => $data['month_4'],
            "05" => $data['month_5'],
            "06" => $data['month_6'],
            "07" => $data['month_7'],
            "08" => $data['month_8'],
            "09" => $data['month_9'],
            "10" => $data['month_10'],
            "11" => $data['month_11'],
            "12" => $data['month_12'],
        ]
    ];
}

$rowTotal = [
    "01" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_1'),
    "02" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_2'),
    "03" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_3'),
    "04" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_4'),
    "05" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_5'),
    "06" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_6'),
    "07" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_7'),
    "08" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_8'),
    "09" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_9'),
    "10" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_10'),
    "11" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_11'),
    "12" => ArrayHelper::getValue($searchModel2->totalsByYear, 'month_12'),
];

/* OLD
$rawContractorsData = DebtorHelper2::getContractorsPrepaidAmount($searchModel->year, Documents::IO_TYPE_IN, CashFlowsBase::FLOW_TYPE_EXPENSE);

$rowContractors = $rowTotal = [];
foreach ($rawContractorsData as $data) {

    $monthNumber = str_pad($data['m'], 2, "0", STR_PAD_LEFT);
    $contractor = Contractor::find()->where(['id' => $data['contractor_id'], 'company_id' => $company->id])->one();

    if (!isset($rowContractors[$data['contractor_id']]))
        $rowContractors[$data['contractor_id']] = [
            'id' => $data['contractor_id'],
            'name' => ($contractor) ?
                Html::a($contractor->nameWithType, ['/contractor/view', 'id' => $contractor->id, 'type' => $contractor->type], [
                    'title' => html_entity_decode($contractor->nameWithType),
                    'data-pjax' => 0
                ]) : $data['contractor_id'],
            'employee_id' => ($contractor) ? $contractor->responsible_employee_id : null,
            'employee_name' => ($contractor && $contractor->responsibleEmployeeCompany) ? $contractor->responsibleEmployeeCompany->getShortFio() : '---',
            'data' => []
        ];

    if (!isset($rowTotal[$monthNumber]))
        $rowTotal[$monthNumber] = 0;
    if (!isset($rowContractors[$data['contractor_id']]['data'][$monthNumber]))
        $rowContractors[$data['contractor_id']]['data'][$monthNumber] = 0;

    $rowContractors[$data['contractor_id']]['data'][$monthNumber] += $data['amount'];
    $rowTotal[$monthNumber] += $data['amount'];
}

foreach ($rowContractors as $key => $contractor) {
    if (array_sum($contractor['data']) == 0)
        unset($rowContractors[$key]);
}
*/
//var_dump($rowTotal);exit;

?>

<div class="wrap wrap_padding_none" style="position: relative">
    <div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
        <div class="table-wrap">
        <table class="table-bleak flow-of-funds debtor2 odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0" >
            <thead>
            <tr class="quarters-flow-of-funds" style="position: relative">
              <th class="sum-cell align-top" rowspan="2" style="vertical-align: middle!important;">
                  <div class="pl-1 pr-1">
                      <?= ($type == 2 && $reportType == DebtReportSearch2::REPORT_TYPE_MONEY
                          || $type == 1 && $reportType == DebtReportSearch2::REPORT_TYPE_GUARANTY) ?
                          'Покупатели' : 'Поставщики'
                      ?>
                  </div>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                  <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                  <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                  <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
              <th class="sum-cell align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                  <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                      <span class="table-collapse-icon">&nbsp;</span>
                      <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                  </button>
              </th>
                <th class="sum-cell align-top" rowspan="2" style="vertical-align: middle!important;">
                    <div class="pl-1 pr-1">
                        <div id="debtor-responsible_employee_id-toggle" class="th-title filter <?= $searchModel->responsible_employee_id ? 'active' : '' ?>" >
                            <span class="th-title-name">От&shy;вет&shy;ствен&shy;ный</span>
                            <span class="th-title-btns">
                                <?= \yii\helpers\Html::button(
                                    Icon::get('filter', ['class' => 'th-title-icon-filter']),
                                    ['class' => 'th-title-btn button-clr'])
                                ?>
                            </span>
                            <div class="filter-select2-select-container">
                                <?= Select2::widget([
                                    'id' => 'debtor-responsible_employee_id',
                                    'name' => 'responsible_employee_id',
                                    'data' => $searchModel2->getEmployersFilter(),
                                    'value' => $searchModel2->responsible_employee_id,
                                    'pluginOptions' => [
                                        'width' => '300px',
                                        'containerCssClass' => 'select2-filter-by-product'
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
            <tr>
              <?php foreach (AbstractFinance::$month as $key => $month): ?>
                  <?php $quarter = (int)ceil($key / 3); ?>
                  <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                      data-collapse-cell
                      data-id="<?= $cell_id[$quarter] ?>"
                      data-month="<?= $key; ?>"
                  >
                      <div class="pl-1 pr-1">
                          <?= $this->render('_sort_arrows', [
                              'title' => $month,
                              'attr' => "month_" . (int)$key
                          ]) ?>
                      </div>
                  </th>
                  <?php if ($key % 3 == 0): ?>
                      <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                          data-collapse-cell-total
                          data-id="<?= $cell_id[$quarter] ?>">
                          <div class="pl-1 pr-1">
                              <?= $this->render('_sort_arrows', [
                                  'title' => 'Итого',
                                  'attr' => "month_" . (int)$key
                              ]) ?>
                          </div>
                      </th>
                  <?php endif; ?>
              <?php endforeach; ?>
            </tr>
            </thead>

            <?php
            $floorKey = "first-floor-contractors";
            $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
            ?>

            <tbody>
            <!-- TOTALS -->
            <tr class="not-drag cancel-drag">
                <td class="sum-cell weight-700 checkbox-td" style="max-width: 255px; min-width: 255px; width: 255px;">
                    <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey text_size_14 ml-1">
                            <?= ($reportType == DebtReportSearch2::SEARCH_BY_INVOICES) ? 'ИТОГО задолженность' : 'Предоплата (авансы)' ?>
                        </span>
                    </button>
                </td>
                <?php $key = 0; ?>
                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    if (!$isCurrentYear || $monthNumber <= date('m'))
                        $amount = isset($rowTotal[$monthNumber]) ? $rowTotal[$monthNumber] : 0; ?>

                    <td class="sum-cell nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?> weight-700" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                    </td>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block sum-cell nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?> weight-700" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="sum-cell nowrap total-month-total-flow-sum weight-700">
                    &nbsp;
                </td>
            </tr>
            <!-- CONTRACTORS -->
            <?php foreach ($rowContractors as $row): ?>
                <tr class="<?= !$isOpenedFloor ? 'd-none':'' ?>" data-id="<?= $floorKey ?>">
                    <td class="contractor-cell text_size_14" style="max-width: 255px; min-width: 255px; width: 255px;">
                        <div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis; padding-left:5px;">
                            <?= $row['name'] ?>
                        </div>
                    </td>
                    <?php $key = 0; ?>
                    <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3);
                        if (!$isCurrentYear || $monthNumber <= date('m'))
                            $amount = ArrayHelper::getValue(ArrayHelper::getValue($row, 'data'), $monthNumber, 0) ?>

                        <td class="sum-cell nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </td>
                        <?php if ($key % 3 == 0): ?>
                            <td class="quarter-block sum-cell nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="sum-cell nowrap total-month-total-sum text-left">
                        <?= $row['employee_name']; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    </div>
</div>

<div class="table-settings-view row align-items-center">
    <div class="col-8">
        <nav>
            <?= \common\components\grid\KubLinkPager::widget([
                'pagination' => $dataProvider2->pagination,
            ]) ?>
        </nav>
    </div>
    <div class="col-4">
        <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
            'maxTitle' => $dataProvider2->totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => 'per-page',
        ]) ?>
    </div>
</div>