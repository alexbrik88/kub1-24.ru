<?php

use common\models\employee\Employee;
use frontend\modules\reference\models\ArticleDropDownForm;
use frontend\modules\reference\models\ArticlesSearch;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\cash\CashFlowsBase;

/** @var Employee $user */
/** @var int $type */

Modal::begin([
    'id' => 'debtor-prepayment-options',
    'closeButton' => false,
    'toggleButton' => false,
]);

$title = ($type == 2) ?
    'Настройка отображения Наших предоплат (авансов) Поставщикам' :
    'Настройка отображения предоплат (авансов) Покупателей Нам';

$typeFlow = ($type == 2) ? CashFlowsBase::FLOW_TYPE_EXPENSE : CashFlowsBase::FLOW_TYPE_INCOME;
$typeArticle = ($type == 2) ? ArticlesSearch::TYPE_EXPENSE : ArticlesSearch::TYPE_INCOME;

$user = Yii::$app->user->identity;
$model = new ArticleDropDownForm($user, $typeArticle);
$model->isNew = ArticleDropDownForm::TYPE_UPDATE_PREPAYMENT_COLUMN;
$items = $model->getDebtorPrepaymentItems();
$partLength = ceil(count($items) / 3);
$prepaymentWallets = \common\models\company\CompanyPrepaymentWallets::findOne(['company_id' => $user->currentEmployeeCompany->company_id]);
?>

<h4 class="modal-title mb-4"><?= $title ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'debtor-prepayment-options-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
    'action' => [
        '/analytics/finance-ajax/update-prepayment-articles',
        'type' => $typeArticle
    ],
    'options' => [
        'data-pjax' => 0
    ],
]); ?>

<div class="row">
    <div class="col-12 text_size_14">
        <?php if ($type == 2): ?>
            Есть ситуации, когда вы оплатили и не будет закрывающих документов, например по платежам по кассе, но это не значит, что обязательства перед вам не выполнены.<br/>
            Снимите галочки по тем пунктам, по которым у вас нет задолженности: как по типам платежей, так и по статьям расхода.
        <?php else: ?>
            Есть ситуации, когда нам оплатили и не будет закрывающих документов, например по платежам по кассе, но это не значит, что обязательства перед покупателем не выполнены.<br/>
            Снимите галочки по тем пунктам, по которым у вас нет задолженности: как по типам платежей, так и по статьям прихода.
        <?php endif; ?>
    </div>
</div>

<?php if ($prepaymentWallets): ?>
<div class="row mt-3 mb-3">
    <div class="col-12 mb-2 text-bold"><strong>Тип <?=($type == 2) ? 'расхода' : 'прихода' ?></strong></div>
    <div class="column">
        <div class="text-truncate w-100">
            <input id="wb1" type="checkbox" name="CompanyPrepaymentWallets[bank]" value="1"<?=($prepaymentWallets->bank) ? 'checked' : '' ?>>
            <label style="display: inline!important; line-height: 1.25" for="wb1">Банк</label>
        </div>
    </div>
    <div class="column">
        <div class="text-truncate w-100">
            <input id="wb2" type="checkbox" name="CompanyPrepaymentWallets[order]" value="1"<?=($prepaymentWallets->order) ? 'checked' : '' ?>>
            <label style="display: inline!important; line-height: 1.25" for="wb2">Касса</label>
        </div>
    </div>
    <div class="column">
        <div class="text-truncate w-100">
            <input id="wb3" type="checkbox" name="CompanyPrepaymentWallets[emoney]" value="1"<?=($prepaymentWallets->emoney) ? 'checked' : '' ?>>
            <label style="display: inline!important; line-height: 1.25" for="wb3">Emoney</label>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="row mt-3 mb-3">
    <div class="col-12 mb-2 text-bold"><strong>Статьи <?=($type == 2) ? 'расхода' : 'прихода' ?></strong></div>
    <?php $i=0; ?>
    <?php foreach ($items as $item): ?>
        <?php if ($i==0): ?>
        <div class="col-4">
        <?php endif; ?>
            <div class="text-truncate w-100">
                <input
                    id="i<?= $item['id'] ?>"
                    type="checkbox"
                    name="ArticleDropDownForm[items2][]"
                    value="<?= $item['id'] ?>"
                    <?=($item['is_prepayment']) ? 'checked' : '' ?>
                    <?=(!$item['company_id'] && !in_array($item['id'], AbstractFinance::$_DEFAULT_PREPAYMENT_ITEMS[$typeFlow])) ? 'disabled' : '' ?>
                >
                <label style="display: inline!important; line-height: 1.25" for="i<?= $item['id'] ?>">
                    <?= $item['name'] ?>
                </label>
            </div>
        <?php $i++; ?>
        <?php if ($i >= $partLength): ?>
        </div>
        <?php $i=0; ?>
        <?php endif; ?>
    <?php endforeach ?>
    <?= ($i > 0) ? '</div>' : '' ?>
</div>

<div class="row">
    <div class="col-12 text_size_14">
        <?php if ($type == 2): ?>
            <b>ВАЖНО:</b> Данные этого отчета попадают в баланс в<br/>
            АКТИВЫ --> Оборотные активы --> Дебиторская задолженность --> Предоплата поставщикам (авансы).
        <?php else: ?>
            <b>ВАЖНО</b>: Данные этого отчета попадают в баланс в<br/>
            ПАССИВЫ -->  Краткосрочные обязательства  --> Кредиторская задолженность --> Предоплата покупателей (авансы).
        <?php endif; ?>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= \common\components\helpers\Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<script>
    $('#debtor-prepayment-options-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax($(this).attr("action"), {
            method: "POST",
            data: $(this).serialize(),
            success: function (data) {
                Ladda.stopAll();
                if (data.success) {
                    location.href = location.href;
                } else {
                    window.toastr.success("Ошибка обновления", "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                }
            },
            error: function () {
                Ladda.stopAll();

                window.toastr.success("Ошибка обновления", "", {
                    "closeButton": true,
                    "showDuration": 1000,
                    "hideDuration": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 1000,
                    "escapeHtml": false
                });
            }
        });

        return false;
    });
</script>
