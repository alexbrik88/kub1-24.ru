<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\project\Project;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $activeTab integer
 * @var $subTab integer
 */

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_finance_pc');
$tabConfig = [
    'priority' => $userConfig->report_pc_priority ?? false,
    'project' => $userConfig->report_pc_project ?? false,
    'sale_point' => $userConfig->report_pc_sale_point ?? false,
    'industry' => $userConfig->report_pc_industry ?? false,
    'description' => $userConfig->report_pc_description ?? false,
];
$multiCompanyManager = $searchModel->multiCompanyManager;

echo GridView::widget([
    'dataProvider' => $itemsDataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-left',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center joint-checkbox-td',
            ],
            'format' => 'raw',
            'value' => function ($flows) {
                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $typeCss = 'income-item';
                    $income = bcdiv($flows['amount'], 100, 2);
                    $expense = 0;
                } else {
                    $typeCss = 'expense-item';
                    $income = 0;
                    $expense = bcdiv($flows['amount'], 100, 2);
                }

                return Html::checkbox("flowId[plan_cash_flows][]", false, [
                    'class' => 'joint-checkbox ' . $typeCss,
                    'value' => $flows['id'],
                    'data' => [
                        'income' => $income,
                        'expense' => $expense,
                    ],
                ]);
            },
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'value' => function ($flows) use ($searchModel) {
                return !in_array($searchModel->group, [PaymentCalendarSearch::GROUP_CONTRACTOR, PaymentCalendarSearch::GROUP_PAYMENT_TYPE]) ?
                    DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
            },
        ],
        [
            'attribute' => 'amountIncome',
            'label' => 'Приход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: ' . (!empty($searchModel->income_item_id) || $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
                'style' => 'display: ' . (!empty($searchModel->income_item_id) || $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'value' => function ($flows) {
                return $flows['amountIncome'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2) : '-';
            },
        ],
        [
            'attribute' => 'amountExpense',
            'label' => 'Расход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) || $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) || $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'value' => function ($flows) {
                return $flows['amountExpense'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2) : '-';
            },
        ],
        [
            's2width' => '200px',
            'filter' => ['' => 'Все'] + $multiCompanyManager->getAllowedCompaniesList(),
            'attribute' => 'company_id',
            'label' => 'Компания',
            'headerOptions' => [
                'class' => $multiCompanyManager->getIsModeOn() ? '' : ' hidden',
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'company-cell text-left' . ($multiCompanyManager->getIsModeOn() ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($multiCompanyManager) {
                $companyName = $multiCompanyManager->getCompanyName($flows['company_id']);
                return '<span title="' . htmlspecialchars($companyName) . '">' . $companyName . '</span>';
            },
        ],
        [
            'attribute' => 'payment_type',
            'label' => 'Тип оплаты',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                PaymentCalendarSearch::CASH_BANK_BLOCK => 'Банк',
                PaymentCalendarSearch::CASH_ORDER_BLOCK => 'Касса',
                PaymentCalendarSearch::CASH_EMONEY_BLOCK => 'E-money'
            ],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [PaymentCalendarSearch::GROUP_CONTRACTOR, PaymentCalendarSearch::GROUP_DATE])) {
                    switch ($flows['payment_type']) {
                        case PlanCashFlows::PAYMENT_TYPE_BANK:
                            $checkingAccountant = CheckingAccountant::find()->andWhere([
                                'id' => $flows['checking_accountant_id']
                            ])->orderBy(['type' => SORT_ASC])->one();
                            if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                        'class' => 'little_logo_bank',
                                        'style' => 'display: inline-block;',
                                    ]);
                            }
                            return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_ORDER:
                            return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_EMONEY:
                            return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                        default:
                            return '';
                    }
                }
                return '';
            },
        ],
        [
            'attribute' => 'priority_type',
            'label' => 'Приоритет в оплате',
            'headerOptions' => [
                'width' => '13%',
                'class' => 'col_report_pc_priority' . ($tabConfig['priority'] ? '' : ' hidden')
            ],
            'contentOptions' => [
                'class' => 'text-left purpose-cell col_report_pc_priority' . ($tabConfig['priority'] ? '' : ' hidden')
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                Contractor::PAYMENT_PRIORITY_HIGH => '1 - cамые приоритетные',
                Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - менее приоритетные',
                Contractor::PAYMENT_PRIORITY_LOW => '3 - наименьший приоритет',
            ],
            'value' => function ($flows) use ($searchModel) {
                $contractor = Contractor::findOne($flows['contractor_id']);
                if ($contractor && $contractor->type == Contractor::TYPE_SELLER) {
                    switch ($contractor->payment_priority) {
                        case Contractor::PAYMENT_PRIORITY_HIGH:
                            return '1 - cамые приоритетные';
                            break;
                        case Contractor::PAYMENT_PRIORITY_MEDIUM:
                            return '2 - менее приоритетные';
                            break;
                        case Contractor::PAYMENT_PRIORITY_LOW:
                            return '3 - наименьший приоритет';
                            break;
                    }
                }

                return '-';
            },
            's2width' => '250px'
        ],
        [
            'attribute' => 'status_type',
            'label' => 'Статус',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-left',
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                PaymentCalendarSearch::STATUS_PLAN => 'План',
                PaymentCalendarSearch::STATUS_OVERDUE => 'Просрочен',
                PaymentCalendarSearch::STATUS_MOVED => 'Перенос'
            ],
            'value' => function ($flows) use ($searchModel) {

                $currDate = date('Y-m-d');
                if ($flows['first_date'] < $currDate && $flows['date'] != $flows['first_date'])
                    return 'Перенос';
                if ($flows['date'] >= $currDate)
                    return 'План';
                if ($flows['date'] < $currDate)
                    return 'Просрочен';

                return '';
            },
            's2width' => '125px'
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '30%',
                'class' => 'nowrap-normal max10list',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell'
            ],
            'format' => 'raw',
            'filter' => !in_array($searchModel->group, [PaymentCalendarSearch::GROUP_PAYMENT_TYPE, PaymentCalendarSearch::GROUP_DATE]) ?
                $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [PaymentCalendarSearch::GROUP_PAYMENT_TYPE, PaymentCalendarSearch::GROUP_DATE])) {
                    /* @var $contractor Contractor */
                    $contractor = Contractor::findOne($flows['contractor_id']);
                    $model = PlanCashFlows::findOne($flows['id']);

                    return $contractor !== null ?
                        ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                        ($model->cashContractor ?
                            ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                        );
                }

                return '';
            },
            's2width' => '250px',
        ],
        [
            's2width' => '250px',
            'attribute' => 'industry_id',
            'label' => 'Направление',
            'headerOptions' => [
                'class' => 'col_report_pc_industry' . ($tabConfig['industry'] ? '' : ' hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_report_pc_industry ' . ($tabConfig['industry'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                    $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                }

                return $ret;
            },
        ],
        [
            's2width' => '250px',
            'attribute' => 'sale_point_id',
            'label' => 'Точка продаж',
            'headerOptions' => [
                'class' => 'col_report_pc_sale_point' . ($tabConfig['sale_point'] ? '' : ' hidden'),
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_report_pc_sale_point ' . ($tabConfig['sale_point'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                    $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                }

                return $ret;
            },
        ],
        [
            's2width' => '250px',
            'attribute' => 'project_id',
            'label' => 'Проект',
            'headerOptions' => [
                'class' => 'col_report_pc_project' . ($tabConfig['project'] ? '' : ' hidden'),
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell col_report_pc_project ' . ($tabConfig['project'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                    $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                }

                return $ret;
            },
        ],

        [
            'attribute' => 'description',
            'label' => 'Назначение',
            'headerOptions' => [
                'class' => 'sorting col_report_pc_description' . ($tabConfig['description'] ? '' : ' hidden'),
                'width' => '30%',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell col_report_pc_description' . ($tabConfig['description'] ? '' : ' hidden'),
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group) && $flows['description']) {
                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                    return Html::tag('div',strlen($flows['description']) > 100 ? $description . '...' : $description, ['class' => 'purpose-cell', 'title' => $flows['description']]);
                }

                return '';
            },
        ],
        [
            'attribute' => 'reason_id',
            'label' => 'Статья',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'reason-cell text-left',
            ],
            'filter' => empty($searchModel->group) ?
                array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                ['' => 'Все статьи'],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group)) {
                    if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $reason = ($reasonModel = InvoiceIncomeItem::findOne($flows['income_item_id'])) ? $reasonModel->fullName : null;
                    } elseif ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                        $reason = ($reasonModel = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ? $reasonModel->fullName : null;
                    } else {
                        $reason = null;
                    }

                    return $reason ? ('<span title="' . htmlspecialchars($reason) . '">' . $reason . '</span>') : '-';
                }

                return '';
            },
            's2width' => '250px',
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{update}{delete}',
            'headerOptions' => [
                'width' => '3%',
            ],
            'contentOptions' => [
                'class' => 'action-line nowrap',
            ],
            'visible' => $canDelete,
            'buttons' => [
                'update' => function ($url, $flows, $key) use ($multiCompanyManager) {
                    return ($multiCompanyManager->canEdit($flows)) ?
                        Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', [
                            'title' => 'Изменить',
                            'class' => 'button-clr link mr-1',
                            'data-toggle' => 'modal',
                            'data-target' => '#update-movement',
                            'data-url' => Url::to([
                                '/analytics/finance-ajax/update-plan-item',
                                'id' => $flows['id'],
                                'tb' => 'plan_cash_flows'
                            ]),
                        ]) : '';
                },
                'delete' => function ($url, $flows) use ($multiCompanyManager) {
                    return ($multiCompanyManager->canEdit($flows)) ?
                        \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                                'class' => 'delete-item-payment-calendar button-clr link',
                                'tag' => 'a',
                            ],
                            'options' => [
                                'id' => 'delete-plan-item-' . $flows['id'],
                                'class' => 'modal-delete-plan-item',
                            ],
                            'confirmUrl' => Url::to([
                                '/analytics/finance-ajax/delete-flow-item',
                                'id' => $flows['id'],
                                'tb' => 'plan_cash_flows'
                            ]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить запланированный платеж?',
                        ]) : '';
                },
            ],
        ],
    ],
]);