<?php
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model Form */
/* @var $groupValue integer */
/* @var $groupChecked boolean */

$cssClass = "pal-revenue-rule-box revenue-rule-{$groupValue} collapse" . ($groupChecked ? ' show' : '');

switch ($groupValue) {
    case Form::REVENUE_RULE_GROUP_CUSTOMER:
        $text = ['покупателей', 'покупателей'];
        break;
    case Form::REVENUE_RULE_GROUP_PRODUCT:
        $text = ['товаров и услуг', 'товаров и услуг'];
        break;
    case Form::REVENUE_RULE_GROUP_PRODUCT_GROUP:
        $text = ['групп', 'группы'];
        break;
    case Form::REVENUE_RULE_GROUP_PROJECT:
        $text = ['проектов', 'проекты'];
        break;
    case Form::REVENUE_RULE_GROUP_INDUSTRY:
        $text = ['направлений', 'направления'];
        break;
    case Form::REVENUE_RULE_GROUP_UNSET:
    case Form::REVENUE_RULE_GROUP_SALE_POINT:
    default:
        $text = [];
        break;
}
?>

<div class="<?= $cssClass ?>">
    <div class="pal-revenue-rule-content">

        <?php if ($text): ?>

            <?= $form->field($model, 'revenueRule', ['options' => ['class' => '']])->label(false)->radioList(Form::$revenueRules, [
                'unselect' => null,
                'id' => 'pal-revenue-rules',
                'encode' => false,
                'item' => function ($index, $label, $name, $checked, $value) use ($form, $model, $groupValue, $groupChecked, $text) {
                    $name = $name . "[{$groupValue}]";
                    $checked =
                        $model->revenueRuleGroup == $groupValue && $model->revenueRule == $value ||
                        $model->revenueRuleGroup != $groupValue && $value == Form::REVENUE_RULE_TOP;
                    $input = Html::radio($name, $checked, [
                        'value' => $value,
                        'class' => 'pal-revenue-rule-radio',
                        'label' => Html::tag('span', str_replace(['записей', 'записи'], $text, $label), [
                            'class' => 'text_size_14',
                        ]),
                        'labelOptions' => [
                            'class' => 'pal-revenue-rule-label'
                        ],
                    ]);

                    return Html::tag('div', $input, [
                        'class' => '',
                    ]);
                }
            ]); ?>

        <?php endif; ?>

    </div>
</div>