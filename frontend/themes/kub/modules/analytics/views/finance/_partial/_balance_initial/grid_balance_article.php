<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\balance\BalanceArticle;
use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\reference\models\BalanceArticlesCategories;
use frontend\modules\reference\models\BalanceArticlesSubcategories;
use frontend\modules\reference\models\BalanceArticlesSearch;
use frontend\modules\reference\widgets\SummarySelectWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var BalanceArticlesSearch $searchModel */

$columnConfig = [
    'purchased_at' => true,
    'category' => true,
    'amount' => true,
    'useful_life_in_month' => true,
    'month_in_use' => true,
    'month_left' => true,
    'write_off_date' => true,
    'status' => true,
];

$initialDate = BalanceInitial::getInitialDate();
$formattedInitialDate = DateHelper::format($initialDate, 'd.m.Y', 'Y-m-d');

Pjax::begin([
    'id' => 'balance-article-grid-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false
]);

if ($type === BalanceArticle::TYPE_FIXED_ASSERTS) {
    $title = BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP[$category] ?? 'Основные средства';
} else {
    $title = 'Нематериальные активы';
}
?>

<div class="row align-items-center pb-1 flex-nowrap items-table-block">
    <div class="column">
        <h4 class="caption mb-2">Детализация "<?= $title ?>"</h4>
    </div>
    <div class="column ml-auto">
        <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
            <span class="button-regular button-regular_red ml-2 mb-2 close-balance-item-detalization">Закрыть</span>
        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-compact table-style table-count-list invoice-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-left',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return Html::checkbox('BalanceArticle[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'attribute' => 'cost_for_today',
            'label' => "Остаток на {$formattedInitialDate}",
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#cost_for_today-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) use ($initialDate) {
                $costForDate = max(0, $model->getRemainingAmountOnDate($initialDate));
                return '<span class="price nowrap" data-price="' . TextHelper::moneyFormatFromIntToFloat($costForDate) . '">'
                    . TextHelper::invoiceMoneyFormat($costForDate, 2)
                    . '</span>';
            },
        ],
        [
            'attribute' => 'purchased_at',
            'label' => 'Дата приобретения',
            'headerOptions' => ['class' => $columnConfig['purchased_at']],
            'contentOptions' => ['class' => $columnConfig['purchased_at']],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return DateHelper::format($model->purchased_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'contentOptions' => [
                'class' => 'contact-cell'
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return Html::tag('span', htmlspecialchars($model->name), ['title' => $model->name]);
            },
        ],
        [
            'attribute' => 'subcategory',
            'label' => 'Подвид',
            'filter' => $searchModel->getSubCategoryFilter(),
            'headerOptions' => [
                'class' => 'tooltip2' . $columnConfig['category'],
                'data-tooltip-content' => '#category-tooltip',
            ],
            'contentOptions' => [
                'class' => $columnConfig['category']
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return BalanceArticlesSubcategories::MAP[$model->subcategory];
            },
            's2width' => '250px'
        ],
        [
            'attribute' => 'count',
            'label' => 'Кол-во',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
        ],
        [
            'attribute' => 'amount',
            'label' => 'Стоимость приобретения',
            'headerOptions' => [
                'class' => 'tooltip2' . $columnConfig['amount'],
                'data-tooltip-content' => '#amount-tooltip',
            ],
            'contentOptions' => [
                'class' => $columnConfig['amount']
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return Html::tag('span', TextHelper::invoiceMoneyFormat($model->amount, 2), ['class' => 'nowrap']);
            },
        ],
        [
            'attribute' => 'cost_for_today',
            'label' => "Остаточная стоимость",
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#cost_for_today-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                $costForToday = max(0, $model->amount - $model->calcAmortization());
                return '<span class="price nowrap" data-price="' . TextHelper::moneyFormatFromIntToFloat($costForToday) . '">'
                    . TextHelper::invoiceMoneyFormat($costForToday, 2)
                    . '</span>';
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}',
            'contentOptions' => [
                'class' => 'actions-grid',
            ],
            'buttons' => [
                'update' => function ($url, BalanceArticle $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                        'class' => 'balance-article-modal-link',
                        'style' => 'cursor: pointer;color: #0097fd;',
                        'data-url' => Url::to(['/analytics/balance-articles/update', 'id' => $model->id, 'type' => $model->type]),
                        'title' => Yii::t('yii', 'Обновить'),
                        'aria-label' => Yii::t('yii', 'Обновить'),
                    ]);
                },
                'delete' => function ($url, $model) use ($type) {
                    return ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                            'class' => '',
                            'tag' => 'a',
                            'style' => 'color: #0097fd;margin-left: 10px;',
                        ],
                        'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить '
                            . ($type === BalanceArticle::TYPE_FIXED_ASSERTS
                                ? 'основное средство'
                                : 'нематериальный актив') . '?',
                    ]);
                },
            ],
        ],
    ]
]) ?>

<?= SummarySelectWidget::widget(['buttons' => [
    Html::a('Удалить', ($type === BalanceArticle::TYPE_FIXED_ASSERTS)
        ? '#many-delete-balance-articles-fixed'
        : '#many-delete-balance-articles-non-material',
        [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
]]) ?>

<?php Pjax::end() ?>