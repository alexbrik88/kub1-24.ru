<?php

use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\employee\Config;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\OddsSearch;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = $userConfig->getTableViewClass('table_view_finance_odds');

// Remove empty Wallets by user config
if ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE) {
    if ($userConfig->report_odds_hide_zeroes) {
        foreach ($data as $k1 => $d1) {
            if (0 === array_sum($d1['data'])) {
                if (0 === array_sum($growingData[$k1]['data'])) {
                    unset($data[$k1]);
                    unset($growingData[$k1]);
                }
            }
        }
    }
}
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table by_purse table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
            <?= $this->render('odds2_table_head', [
                'searchModel' => $searchModel,
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
            ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            if ($searchModel::hideBlockByUser($activeTab, $paymentType, $userConfig))
                continue;

            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'negativeRed' => true,
                'positiveRed' => ($activeTab == OddsSearch::TAB_ODDS && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK),
            ]);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentType}-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);

                $isIncome = in_array($paymentFlowType, ($activeTab == OddsSearch::TAB_ODDS) ? [1,3,4,6] : [1,3,5,7]);
                if ($activeTab == OddsSearch::TAB_ODDS_BY_CURRENCY) {
                    @list($_0, $_1, $isIncome) = explode('_', $paymentFlowType);
                }

                echo $this->render('odds2_table_row', [
                    'level' => 2,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'isOpenable' => ($activeTab != OddsSearch::TAB_ODDS_BY_PURSE_DETAILED),
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $floorKey2 = "floor-item-{$paymentType}-{$paymentFlowType}-{$itemId}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                    echo $this->render('odds2_table_row', [
                        'level' => 3,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dMonthes' => $d_monthes,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => ($activeTab == OddsSearch::TAB_ODDS && empty($level3['levels'])),
                        'isOpenable' => (!empty($level3['levels'])),
                        'floorKey' => $floorKey2,
                        'dataHoverText' => $level3['dataHoverText'] ?? null
                    ]);

                    if (empty($level3['levels']))
                        continue 1;

                    // LEVEL 4
                    foreach ($level3['levels'] as $subItemId => $level4) {

                        $subItemIds = (isset($level4['consolidatedSubItemsIds']))
                            ? implode('-', $level4['consolidatedSubItemsIds'])
                            : $subItemId;

                        echo $this->render('odds2_table_row', [
                            'level' => 4,
                            'title' => $level4['title'],
                            'data' => $level4['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dMonthes' => $d_monthes,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $subItemIds,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'isSortable' => false
                        ]);
                    }
                }
            }

            if (isset($growingData[$paymentType]['hide']))
                continue;

            // GROWING
            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $growingData[$paymentType]['title'],
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false,
                'negativeRed' => true,
                'question' => $growingData[$paymentType]['question'] ?? null,
            ]);
        }

        // TOTAL BY MONTH
        if (isset($totalData['month'])) {
            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $totalData['month']['title'],
                'data' => $totalData['month']['data'],
                'trClass' => 'not-drag cancel-drag net-cash-flow',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'canHover' => false,
                'question' => $totalData['month']['question'] ?? null,
                'negativeRed' => true,
            ]);
        }

        // TOTAL BY YEAR
        if (isset($totalData['growing'])) {
            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $totalData['growing']['title'],
                'data' => $totalData['growing']['data'],
                'trClass' => 'not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'isSumQuarters' => false,
                'canHover' => false,
                'negativeRed' => true,
            ]);
        }

        ?>
        </tbody>
    </table>
    </div>
</div>