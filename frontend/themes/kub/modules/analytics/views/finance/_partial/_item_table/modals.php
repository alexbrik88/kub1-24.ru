<?php

use common\components\helpers\Html;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\currency\Currency;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap4\Modal;
use yii\db\Expression;
use yii\helpers\Url;
use kartik\select2\Select2;
?>

<div id="many-delete" class="modal-many-delete-plan-item confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['/analytics/finance-ajax/many-delete-flow-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-update-flow" id="update-movement" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 id="js-modal_update_title" class="modal-title">Изменить движение</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-many-plan-item" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['/analytics/finance-ajax/many-flow-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-income_item_id_wrapper',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => Html::getInputId($searchModel, 'incomeItemIdManyItem'),
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <?= Html::radio(null, true, [
                                        'label' => 'Расход изменить на:',
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-expenditure_item_id_wrapper required',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => Html::getInputId($searchModel, 'expenditureItemIdManyItem'),
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-recognition-date" id="many-recognition-date" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить дату признания</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['/analytics/finance-ajax/many-flow-recognition-date']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_date_form',
                ])); ?>
                <div class="form-body">
                    <div class="form-group row">
                        <label class="col-12 mb-0" for="cashbankflowsform-flow_type">
                            Применится только для операций по деньгам, к которым не прикреплены счета.
                        </label>
                    </div>
                    <?= $form->field($searchModel, 'recognitionDateManyItem', [
                        'labelOptions' => [
                            'class' => 'col-12 label',
                        ],
                        'wrapperOptions' => [
                            'class' => '',
                        ],
                        'options' => [
                            'class' => 'form-group js-recognition_date_wrapper',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker ico',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                        'style' => 'width:172px'
                    ])->label('Дата признания'); ?>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php if (isset($showManyChangeCashbox)): ?>

    <?php
    $cashboxes = Yii::$app->user->identity->getCashboxes()
        ->with('currency')
        ->orderBy(new Expression(
            'IF(currency_id = '.Currency::DEFAULT_ID.', 0, currency_id), is_main DESC, name'))
        ->indexBy('id')
        ->all();

    $cashboxesList = [];
    foreach ($cashboxes as $c) {
        $cashboxesList[$c->id] = ($c->currency_id == Currency::DEFAULT_ID) ?  $c->name : ($c->currency->name.' '.$c->name);
    }
    ?>

    <div class="modal fade" id="many-cashbox" role="modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title">Изменить кассу</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row form-horizontal">

                            <div class="col-12 income-block hidden">

                                <div class="row">
                                    <label class="col-12" for="cashorderflowsform-flow_type">
                                        Для типа
                                    </label>
                                    <div class="col-12" style="margin:0 0 8px">
                                        <div id="cashorderflowsform-flow_type" aria-required="true">
                                            <div class="">
                                                <?= Html::radio(null, true, [
                                                    'label' => 'Приход',
                                                    'labelOptions' => [
                                                        'class' => 'radio-txt-bold font-14',
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label for="contractor-responsible_employee">
                                    Изменить кассу на
                                </label>
                                <?= Select2::widget([
                                    'name' => 'cashboxIdManyItemIncome',
                                    'options' => [
                                        'class' => 'operation-many-cashbox-field'
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%',
                                        'placeholder' => '',
                                    ],
                                    'data' => $cashboxesList,
                                ]); ?>
                            </div>

                            <div class="col-12 expense-block hidden">

                                <div class="row">
                                    <label class="col-12" for="cashorderflowsform-flow_type">
                                        Для типа
                                    </label>
                                    <div class="col-12" style="margin:0 0 8px">
                                        <div id="cashorderflowsform-flow_type" aria-required="true">
                                            <div class="">
                                                <?= Html::radio(null, true, [
                                                    'label' => 'Расход',
                                                    'labelOptions' => [
                                                        'class' => 'radio-txt-bold font-14',
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label for="contractor-responsible_employee">
                                    Изменить кассу на
                                </label>
                                <?= Select2::widget([
                                    'name' => 'cashboxIdManyItemExpense',
                                    'options' => [
                                        'class' => 'operation-many-cashbox-field'
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%',
                                        'placeholder' => '',
                                    ],
                                    'data' => $cashboxesList,
                                ]); ?>
                            </div>

                        </div>
                    </div>
                    <br>
                    <div class="mt-3 d-flex justify-content-between">
                        <?= \yii\helpers\Html::button('<span>Сохранить</span>', [
                            'id' => 'many-change-cashbox-ajax',
                            'class' => 'button-regular button-width button-regular_red button-clr',
                            'data-url' => Url::to(['/cash/default/many-change-flow-cashbox', 'ajax' => 1]),
                            'data-style' => 'expand-right',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-company-industry',
]); ?>
    <h4 class="modal-title mb-4">Изменить направление</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных операций изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Направление
                </label>
                <?= \kartik\widgets\Select2::widget([
                    'id' => 'operation-many-company-industry',
                    'name' => 'industryIdManyItem',
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'data' => CompanyIndustry::getSelect2Data(),
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label pr-3">Сохранить</span>', [
            'class' => 'modal-many-change-company-industry button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/cash/default/many-change-flow-industry', 'ajax' => 1]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-sale-point',
]); ?>
    <h4 class="modal-title mb-4">Изменить точку продаж</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    Для выбранных операций изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="contractor-responsible_employee" class="label">
                    Точка продаж
                </label>
                <?= \kartik\widgets\Select2::widget([
                    'id' => 'operation-many-sale-point',
                    'name' => 'salePointIdManyItem',
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'data' => SalePoint::getSelect2Data(),
                ]); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label pr-3">Сохранить</span>', [
            'class' => 'modal-many-change-sale-point button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/cash/default/many-change-flow-sale-point', 'ajax' => 1]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-project',
]); ?>
<h4 class="modal-title mb-4">Изменить проект</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных операций изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Проект
            </label>
            <?= \kartik\widgets\Select2::widget([
                'id' => 'operation-many-project',
                'name' => 'projectIdManyItem',
                'pluginOptions' => [
                    'width' => '100%',
                ],
                'data' => \common\models\project\Project::getSelect2Data(),
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label pr-3">Сохранить</span>', [
        'class' => 'modal-many-change-project button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/cash/default/many-change-flow-project', 'ajax' => 1]),
        'style' => 'width: 130px!important;',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>
