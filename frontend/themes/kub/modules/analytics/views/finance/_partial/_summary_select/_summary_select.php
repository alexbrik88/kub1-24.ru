<?php
use yii\helpers\Html;
/** @var $buttons array */
$buttons = array_filter((array)($buttons ?? []));
$dropdownButtons = array_filter((array)($dropdownButtons ?? []));
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-panel-checkbox" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Приход: <strong class="total-income ml-1">0</strong>
            <span class="currency-symbol bold hidden">₽</span>
        </div>
        <div class="column column-expense total-txt-foot mr-3">
            Расход: <strong class="total-expense ml-1">0</strong>
            <span class="currency-symbol bold hidden">₽</span>
        </div>
        <div style="display:none!important;" class="column total-txt-foot mr-3">
            Разница: <strong class="total-difference ml-1">0</strong>
        </div>
        <div class="column ml-auto"></div>

        <?php if ($dropdownButtons): ?>
        <div class="column odds-dropdown-button">
            <div class="dropup dropup-right-align">
                <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                    'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle button-width',
                    'data-toggle' => 'dropdown',
                ]) ?>
                <?= \yii\bootstrap4\Dropdown::widget([
                    'items' => $dropdownButtons,
                    'options' => [
                        'class' => 'form-filter-list list-clr',
                        'style' => 'margin-left: -16px'
                    ],
                ]) ?>
            </div>
        </div>
        <?php endif; ?>

        <?php
        foreach ($buttons as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column odds-button',
            ]) ?>
        <?php endforeach; ?>

        <?php if (!$buttons && !$dropdownButtons): ?>
            <div class="column odds-dropdown-button">
                <div style="height: 44px"></div>
            </div>
        <?php endif; ?>

    </div>
</div>

<?php
$js = <<<SCRIPT

// fast joint operations checkboxes

$(document).on('change', '.joint-main-checkbox', function() {
    let table = $(this).closest('table');
    let checkboxes = table.find('.joint-checkbox');
    checkboxes.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.last().trigger('change');
});

$(document).on('change', '.joint-checkbox', function() {
    let table = $(this).closest('table');
    let checkboxes = table.find('.joint-checkbox');
    if (checkboxes.length === checkboxes.filter(':checked').length) {
        table.find('.joint-main-checkbox').prop('checked', true).uniform('refresh');
    } else {
        table.find('.joint-main-checkbox').prop('checked', false).uniform('refresh');
    }
});

$(document).on('change', '.joint-main-checkbox', function() {
    let table = $('.items-table-block').first().find('table');
    let checkboxes = table.find('.joint-checkbox');
    checkboxes.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.last().trigger('change');
});

$(document).on('change', '.joint-checkbox', function() {
    let block = $('.items-table-block').first();
    let checkboxes = block.find('.joint-checkbox');
    if (checkboxes.length === checkboxes.filter(':checked').length) {
        block.find('.joint-panel-checkbox').prop('checked', true).uniform('refresh');
    } else {
        block.find('.joint-panel-checkbox').prop('checked', false).uniform('refresh');
    }
});

$(document).on('change', '.joint-main-checkbox', function() {
    let block = $('.items-table-block').first();
    let panelCheckbox = block.find('.joint-panel-checkbox');
    panelCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh');
});

$(document).on('change', '.joint-panel-checkbox', function() {
    let block = $('.items-table-block').first();
    let mainCheckbox = block.find('.joint-main-checkbox');
    mainCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh').trigger('change');
});

$(document).on('change', '.joint-checkbox', function(e) {
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    var showCurrency = false;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
        if ($(this).data('is_foreign')) 
            showCurrency = true;
        
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }

    if (showCurrency) {
        $('#summary-container').find('.currency-symbol').removeClass('hidden');
    } else {
        $('#summary-container').find('.currency-symbol').addClass('hidden');
    }
    
});

SCRIPT;

$this->registerJs($js);
?>