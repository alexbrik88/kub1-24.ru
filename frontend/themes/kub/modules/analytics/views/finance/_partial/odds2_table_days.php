<?php

use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\employee\Config;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\OddsSearch;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $userConfig Config
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');

// Remove empty Wallets by user config
if ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE) {
    if ($userConfig->report_odds_hide_zeroes) {
        foreach ($data as $k1 => $d1) {
            if (0 === array_sum($d1['data'])) {
                if (0 === array_sum($growingData[$k1]['data'])) {
                    unset($data[$k1]);
                    unset($growingData[$k1]);
                }
            }
        }
    }
}
?>

<script>
    // ODDS FIXED COLUMN
    $(document).ready(function() {
        OddsTable.fillFixedColumn();
    });
</script>

<div id="cs-table-2" class="custom-scroll-table-double cs-top">
    <table style="width: 1832px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
</div>

<!-- ODDS FIXED COLUMN -->
<div id="cs-table-first-column">
    <table class="table-bleak table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
        <thead></thead>
        <tbody></tbody>
    </table>
</div>

<div id="cs-table-1" class="custom-scroll-table-double">
    <div class="table-wrap">
    <table class="table-bleak flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <?= $this->render('odds2_table_head_days', [
            'searchModel' => $searchModel,
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'year' => $searchModel->year
        ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            if ($searchModel::hideBlockByUser($activeTab, $paymentType, $userConfig))
                continue;

            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'negativeRed' => true,
                'positiveRed' => ($activeTab == OddsSearch::TAB_ODDS && $paymentType == AbstractFinance::OWN_FUNDS_BLOCK),
            ]);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == OddsSearch::TAB_ODDS) ? [1,3,4,6] : [1,3,5]);

                echo $this->render('odds2_table_row_days', [
                    'level' => 2,
                    'searchModel' => $searchModel,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $floorKey2 = "floor-item-{$itemId}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey);

                    echo $this->render('odds2_table_row_days', [
                        'level' => 3,
                        'searchModel' => $searchModel,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dDays' => $d_days,
                        'cellIds' => $cell_id,
                        'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => ($activeTab == OddsSearch::TAB_ODDS && empty($level3['levels'])),
                        'isOpenable' => (!empty($level3['levels'])),
                        'floorKey' => $floorKey2,
                        'dataHoverText' => $level3['dataHoverText'] ?? null
                    ]);

                    if (empty($level3['levels']))
                        continue 1;

                    // LEVEL 4
                    foreach ($level3['levels'] as $subItemId => $level4) {

                        $subItemIds = (isset($level4['consolidatedSubItemsIds']))
                            ? implode('-', $level4['consolidatedSubItemsIds'])
                            : $subItemId;

                        echo $this->render('odds2_table_row_days', [
                            'level' => 4,
                            'searchModel' => $searchModel,
                            'title' => $level4['title'],
                            'data' => $level4['data'],
                            'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                            'dDays' => $d_days,
                            'cellIds' => $cell_id,
                            'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                            'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                            'dataType' => $paymentFlowType,
                            'dataItemId' => $subItemIds,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'isSortable' => false
                        ]);
                    }

                }
            }

            // GROWING
            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $growingData[$paymentType]['title'],
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false,
                'negativeRed' => true
            ]);
        }

        // TOTAL BY MONTH
        if (isset($totalData['month'])) {
            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $totalData['month']['title'],
                'data' => $totalData['month']['data'],
                'trClass' => 'not-drag cancel-drag net-cash-flow',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'canHover' => false,
                'question' => $totalData['month']['question'] ?? null,
                'negativeRed' => true,
            ]);
        }

        // TOTAL BY YEAR
        if (isset($totalData['growing'])) {
            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $totalData['growing']['title'],
                'data' => $totalData['growing']['data'],
                'trClass' => 'not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'isSumQuarters' => false,
                'canHover' => false,
                'negativeRed' => true
            ]);
        }

        ?>
        </tbody>

    </table>
    </div>
</div>