<?php
use common\models\Company;
use common\models\company\CheckingAccountant;

$company = Yii::$app->user->identity->currentEmployeeCompany->company;
$newCheckingAccountant = new CheckingAccountant([
    'company_id' => $company->id,
    'type' => ($company->mainCheckingAccountant)
        ? CheckingAccountant::TYPE_ADDITIONAL
        : CheckingAccountant::TYPE_MAIN
]);

echo $this->render('@frontend/views/company/form/modal_rs/_create', [
    'checkingAccountant' => $newCheckingAccountant,
    'title' => 'Добавить расчетный счет',
    'id' => 'add-company-rs',
    'company' => $company,
    'actionUrl' => '/company/create-checking-accountant'
]);

?>


