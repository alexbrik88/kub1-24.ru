<?php

use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\BreakEvenSearchModel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $searchModel BreakEvenSearchModel
 * @var $data array
 * @var $floorMap array
 * @var $total array
 * @var $tabVievClass string
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;
$floorMap = $floorMap ?? [];
$customerInputsMap = $customerInputsMap ?? [];

$d_monthes = [
    1 => ArrayHelper::getValue($floorMap, 'first-cell', 1),
    2 => ArrayHelper::getValue($floorMap, 'second-cell', 1),
    3 => ArrayHelper::getValue($floorMap, 'third-cell', 1),
    4 => ArrayHelper::getValue($floorMap, 'fourth-cell', 1),
];
?>
<form id="break-even-customer-inputs">
<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table id="break-even-table-2" class="flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th width="20%" class="pl-2 pr-2 align-middle" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1 text-left active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1" style="display: inline-block">Финансовые показатели</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 align-top" <?= $d_monthes[1] ? 'colspan="2"' : '' ?> data-collapse-cell-title data-id="first-cell">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell" data-columns-count="2">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap"><?= AbstractFinance::$month[$searchModel->month].' '.$searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 align-top" <?= $d_monthes[2] ? 'colspan="4"' : '' ?> data-collapse-cell-title data-id="second-cell">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell" data-columns-count="4" data-columns-count-collapsed="2">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">Прогноз месячный</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 align-top" <?= $d_monthes[4] ? 'colspan="2"' : '' ?> data-collapse-cell-title data-id="fourth-cell">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell" data-columns-count="2">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">Изменение</span>
                    </button>
                </th>
            </tr>
            <tr>
                <th width="10%" class="pl-2 pr-2 <?= $d_monthes[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                    <div class="pl-1 pr-1">Доля в выручке, %</div>
                </th>
                <th width="10%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Факт, ₽</div>
                </th>
                <th width="10%" class="pl-2 pr-2 <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                    <div class="pl-1 pr-1">Доля в выручке, %</div>
                </th>
                <th width="10%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Расчетный, ₽</div>
                </th>
                <th width="10%" class="pl-2 pr-2 <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                    <div class="pl-1 pr-1">Доля в выручке, %</div>
                </th>
                <th width="10%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1">По графику, ₽</div>
                </th>
                <th width="10%" class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Абсолютное</div>
                </th>
                <th width="10%" class="pl-2 pr-2 <?= $d_monthes[4] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[4] ?>">
                    <div class="pl-1 pr-1">%%</div>
                </th>
            </tr>
            </thead>

            <tbody>

            <?php $floorId = 0; $floorKey1 = null; ?>
            <?php $floorId2 = 0; $floorKey2 = null; ?>
            <?php foreach ($data as $key => $row): ?>
                <?php
                $options = ArrayHelper::remove($row, 'options', []);
                $canClick = $dataClick = '';
                $amountPrev = ArrayHelper::getValue($row, $searchModel->year.$searchModel->month, 0);
                $amount = ArrayHelper::getValue($row, $searchModel->year.$searchModel->month, 0);
                $percentPrev = ($total['revenuePrev'] > 0) ? 100 * $amountPrev / $total['revenuePrev'] : '';
                $percent = ($total['revenue'] > 0) ? 100 * $amount / $total['revenue'] : '';
                $isRevenue = ($options['data-block'] == 'revenue');
                $hideInput = (($isRevenue && strpos($options['class'], 'quantity')) || $options['data-block'] == 'calculated');
                $isAddArticleBtn = $options['data-block'] == 'add-article-btn';
                $isParentRow = $row['hasSubItems'] ?? false;
                $isChildRow = $row['hasParent'] ?? false;
                if ($key == 'totalRevenue') {
                    $percentPrev = $percent = 100;
                    $customerAmount = ArrayHelper::getValue($customerInputsMap, 'revenue', $amount);
                }
                ?>

                <?php if ($isAddArticleBtn): ?>
                    <?php
                    if ($key == 'addToVariableCosts')
                        $addArticleBlockType = BreakEvenSearchModel::BLOCK_VARIABLE_COSTS;
                    elseif ($key == 'addToFixedCosts')
                        $addArticleBlockType = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                    else
                        $addArticleBlockType = null;
                    ?>
                    <tr class="<?=($options['class'])?> not-drag" data-id="<?=($floorKey1)?>">
                        <td class="text_size_14 pl-2 pr-2">
                            <i class="add-new-article link nowrap" href="javascript:void(0)" type="button" data-url="<?= Url::to(['/analytics/finance-ajax/add-article-to-break-even', 'block' => $addArticleBlockType]) ?>" style="padding-left: 6px">
                                <?= $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span style="padding-left:10px">'.$row['label'].'</span>' ?>
                            </i>
                        </td>
                        <td class="<?= $d_monthes[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>"></td>
                        <td></td>
                        <td class="<?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>"></td>
                        <td></td>
                        <td class="<?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>"></td>
                        <td></td>
                        <td></td>
                        <td class="<?= $d_monthes[4] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[4] ?>"></td>
                    </tr>
                <?php elseif (isset($row['addCheckboxX']) || isset($row['noCheckboxX'])): ?>
                    <?php $floorId++; ?>
                    <?php $floorKey1 = "first-floor-{$floorId}"; ?>
                    <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1, 1); ?>
                    <?php $showPercents = !($key == 'totalEfficiency'); ?>
                    <?php $suffix = ($key == 'totalEfficiency') ? '%' : ''; ?>

                    <tr class="<?=($options['class'])?> not-drag expenditure_type sub-block">
                        <td class="pl-2 pr-2">
                            <?php if (isset($row['addCheckboxX'])): ?>
                                <button class="table-collapse-btn button-clr ml-1 text-left <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="d-block weight-700 text_size_14 ml-1"><?= $row['label']; ?></span>
                                </button>
                            <?php else: ?>
                                <span class="d-block weight-700 text_size_14 m-l-purse">
                                <?= $row['label']; ?>
                            </span>
                            <?php endif; ?>
                        </td>
                        <td class="prev-percent pl-2 pr-2 nowrap <?= $d_monthes[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                            <div class="pl-1 pr-1 weight-700">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percentPrev, 2) ?></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="prev-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-700">
                                <span><?= TextHelper::moneyFormat($amountPrev, 2); ?></span><?=($suffix)?>
                            </div>
                        </td>
                        <td class="curr-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1 weight-700 w-100">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percent, 2) ?></span>%
                                    <input type="hidden" value="<?= (float)$percent ?>">
                                <?php endif; ?>
                            </div>
                        </td>
                        <?php if ($isRevenue): ?>
                            <td class="curr-amount pl-0 pr-0 nowrap td-custom-value">
                                <div class="weight-700 w-100">
                                    <input type="text" name="customerInputsMap[revenue]" class="custom-value form-control text-right" value="<?= TextHelper::moneyFormat((float)$customerAmount, 2) ?>"/>
                                </div>
                            </td>
                        <?php else: ?>
                            <td class="curr-amount pl-2 pr-2 nowrap">
                                <div class="pl-1 pr-1 weight-700 w-100">
                                    <span><?= TextHelper::moneyFormat((float)$amount, 2); ?></span><?=($suffix)?>
                                    <input type="hidden" value="<?= (float)$amount ?>"/>
                                </div>
                            </td>
                        <?php endif; ?>
                        <td class="calc-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1 weight-700">
                                <?php if ($showPercents): ?>
                                    <span><!--js calc (don't change structure: td > div > span)--></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="calc-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-700">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-700">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-percent pl-2 pr-2 nowrap <?= $d_monthes[4] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[4] ?>">
                            <div class="pl-1 pr-1 weight-700">
                                <span><!--js calc (don't change structure: td > div > span)--></span>%
                            </div>
                        </td>
                    </tr>

                <?php elseif ($isParentRow): ?>

                    <?php $floorId2++; ?>
                    <?php $floorKey2 = "second-floor-{$floorId2}"; ?>
                    <?php $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2, 1); ?>
                    <?php $showPercents = !($key == 'totalEfficiency'); ?>
                    <?php $suffix = ($key == 'totalEfficiency') ? '%' : ''; ?>

                    <tr class="<?=($options['class'])?> not-drag expenditure_type parent" data-id="<?= $floorKey1 ?>" <?= (!empty($row['data-id'])) ? "data-item_id=\"{$row['data-id']}\"" : '' ?>>
                        <td class="pl-2 pr-2">
                            <button class="table-collapse-btn button-clr ml-1 text-left icon-grey text-grey link-bleak m-l-purse <?= $isOpenedFloor2 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey2 ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="d-block weight-400 text_size_14 ml-1" style="padding-top: 1px;"><?= $row['label']; ?></span>
                            </button>
                        </td>
                        <td class="prev-percent pl-2 pr-2 nowrap <?= $d_monthes[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                            <div class="pl-1 pr-1 weight-400">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percentPrev, 2) ?></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="prev-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-400">
                                <span><?= TextHelper::moneyFormat($amountPrev, 2); ?></span><?=($suffix)?>
                            </div>
                        </td>
                        <td class="curr-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1 weight-400 w-100">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percent, 2) ?></span>%
                                    <input type="hidden" value="<?= (float)$percent ?>">
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="curr-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-400 w-100">
                                <span><?= TextHelper::moneyFormat((float)$amount, 2); ?></span><?=($suffix)?>
                                <input type="hidden" value="<?= (float)$amount ?>"/>
                            </div>
                        </td>
                        <td class="calc-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1 weight-400">
                                <?php if ($showPercents): ?>
                                    <span><!--js calc (don't change structure: td > div > span)--></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="calc-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-400">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1 weight-400">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-percent pl-2 pr-2 nowrap <?= $d_monthes[4] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[4] ?>">
                            <div class="pl-1 pr-1 weight-400">
                                <span><!--js calc (don't change structure: td > div > span)--></span>%
                            </div>
                        </td>
                    </tr>

                <?php else: ?>

                    <?php if (!$isOpenedFloor1) $options['class'] .= ' d-none'; ?>
                    <?php $showPercents = !($options['data-block'] == 'revenue' || $options['data-block'] == 'calculated'); ?>
                    <?php $customerAmount = ArrayHelper::getValue($customerInputsMap, $options['data-article'], $amount); ?>
                    <?php $suffix = ($key == 'marginality') ? '%' : ''; ?>

                    <tr class="<?=($options['class'])?> item-block <?=($isChildRow ? 'child' : '')?>" data-id="<?=($isChildRow ? $floorKey2 : $floorKey1)?>" <?= (!empty($row['data-parent_id'])) ? "data-parent_item_id=\"{$row['data-parent_id']}\"" : '' ?>>
                        <td class="pl-2 pr-1">
                            <span class="d-block text-grey <?= ($isChildRow) ? 'text_size_12' : 'text_size_14' ?> m-l-purse">
                                <?= $row['label']; ?>
                            </span>
                        </td>
                        <td class="prev-percent pl-2 pr-2 nowrap <?= $d_monthes[1] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[1] ?>">
                            <div class="pl-1 pr-1">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percentPrev, 2) ?></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="prev-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1">
                                <span><?= TextHelper::moneyFormat((float)$amountPrev, 2); ?></span><?=($suffix)?>
                            </div>
                        </td>
                        <td class="curr-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1">
                                <?php if ($showPercents): ?>
                                    <span><?= TextHelper::moneyFormat((float)$percent, 2) ?></span>%
                                    <input type="hidden" value="<?= (float)$percent ?>">
                                <?php endif; ?>
                            </div>
                        </td>
                        <?php if ($hideInput): ?>
                            <td class="curr-amount pl-2 pr-2 nowrap">
                                <div class="pl-1 pr-1">
                                    <span><?= TextHelper::moneyFormat((float)$amount, 2); ?></span><?=($suffix)?>
                                    <input type="hidden" value="<?= (float)$amount ?>"/>
                                </div>
                            </td>
                        <?php else: ?>
                            <td class="curr-amount pl-0 pr-0 nowrap td-custom-value">
                                <div class="w-100">
                                    <input type="text" name="customerInputsMap[<?=($options['data-article'])?>]" class="custom-value form-control text-right" value="<?= TextHelper::moneyFormat((float)$customerAmount, 2) ?>"/>
                                </div>
                            </td>
                        <?php endif; ?>
                        <td class="calc-percent pl-2 pr-2 nowrap <?= $d_monthes[2] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[2] ?>">
                            <div class="pl-1 pr-1">
                                <?php if ($showPercents): ?>
                                    <span><!--js calc (don't change structure: td > div > span)--></span>%
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="calc-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-amount pl-2 pr-2 nowrap">
                            <div class="pl-1 pr-1">
                                <!--js calc (don't change structure: td > div > span)-->
                            </div>
                        </td>
                        <td class="variation-percent pl-2 pr-2 nowrap <?= $d_monthes[4] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[4] ?>">
                            <div class="pl-1 pr-1">
                                <span><!--js calc (don't change structure: td > div > span)--></span>%
                            </div>
                        </td>
                    </tr>

                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</form>

<div class="modal fade" id="break-even-article-modal" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить статью расходов в расчет точки безубыточности</h4>
            <span class="modal-header-description">
                Перед тем как добавить свою статью, проверьте, есть ли такая статья в предустановленных,
                но еще не используемых вами
            </span>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php Pjax::begin([
                    'id' => 'pjax-break-even-article-modal',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'scrollTo' => false,
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click', '.add-article-btn i', function() {
        $.pjax({url: $(this).data('url'), container: '#pjax-break-even-article-modal', push: false, scrollTo: false});
        $(document).on('pjax:success', '#pjax-break-even-article-modal', function(e, data) {
            $('#break-even-article-modal').modal();
            $('#break-even-customer-inputs').find('input').each(function(i,v) {
                let input = $(this).clone();
                if ($(input).hasClass('custom-value')) {
                    $(input)
                        .attr('type', 'hidden')
                        .removeAttr('class')
                        .val($(input).val().sanitizeKub())
                        .appendTo('#article-form');
                }
            });
        });
    });
    $(document).off('change', '#articledropdownform-isnew');
    $(document).on('change', '#articledropdownform-isnew', function(e) {
        const isNew = +$(this).find(':checked').val();

        const blockItems = $('.field-articledropdownform-items');
        const blockItems2 = $('.field-articledropdownform-items2');
        const blockNewItem = $('.field-articledropdownform-name');

        if (isNew === 2) {
            blockItems2.slideDown();
            blockNewItem.slideUp();
            blockItems.slideUp();
        }
        else if (isNew === 1) {
            blockNewItem.slideDown();
            blockItems2.slideUp();
            blockItems.slideUp();
        } else if (isNew === 0) {
            blockItems.slideDown();
            blockItems2.slideUp();
            blockNewItem.slideUp();
        }
    });
</script>