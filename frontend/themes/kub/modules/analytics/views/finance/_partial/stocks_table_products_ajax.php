<?php

use common\components\helpers\Html;
use frontend\components\PageSize;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use \common\models\product\Product;

/**
 * @var $this yii\web\View
 * @var $products array
 * @var $isCurrentYear bool
 * @var $groupId
 */

// js options
$floorMap = $floorMap ?? [];
$d_monthes = [
    1 => ArrayHelper::getValue($floorMap, 'first-cell'),
    2 => ArrayHelper::getValue($floorMap, 'second-cell'),
    3 => ArrayHelper::getValue($floorMap, 'third-cell'),
    4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
];
$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];
$currentQuarter = (int)ceil(date('m') / 3);

$retJson = [];
foreach ($products as $row) {
    $key = 0;
    $data = [];
    foreach (AbstractFinance::$month as $monthNumber => $monthText) {
        $key++;
        $quarter = (int)ceil($key / 3);
        $amount = ArrayHelper::getValue($row, $monthNumber, 0);
        $data[] = ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2);
        if ($key % 3 == 0) {
            $data[] = ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2);
        }
    }

    $retJson[] = [
        'id' => $groupId,
        'title' => $row['title'],
        'data' => $data
    ];
}

echo json_encode($retJson);
exit;
/*
foreach ($products as $row): ?>
    <?php $floorKey = "group_{$groupId}"; ?>
    <?php $isOpenedFloor = true; ?>
    <tr class="<?= !$isOpenedFloor ? 'd-none':'' ?>" data-id="<?= $floorKey ?>">
        <td class="contractor-cell text_size_14" style="max-width: 255px; min-width: 255px; width: 255px; padding-left: 40px;">
            <div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">
                <?= Html::a($row['title'], ['/product/view', 'id' => $row['id'], 'productionType' => 1], [
                    'title' => html_entity_decode($row['title']),
                    'class' => 'tooltipstered', // test
                    'data-pjax' => 0
                ]) ?>
            </div>
        </td>
        <?php $key = 0; ?>
        <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): ?>
            <?php $key++;
            $quarter = (int)ceil($key / 3);
            $amount = ArrayHelper::getValue($row, $monthNumber, 0) ?>

            <td class="sum-cell nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                <?php echo ($isCurrentYear && $monthNumber > date('m')) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
            </td>
            <?php if ($key % 3 == 0): ?>
                <td class="quarter-block sum-cell nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                    <?php echo ($isCurrentYear && $quarter > $currentQuarter) ? '0,00' : TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </td>
            <?php endif; ?>
        <?php endforeach; ?>
    </tr>
<?php endforeach; ?>*/