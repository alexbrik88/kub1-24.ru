<?php
use common\components\TextHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\cash\models\CashContractorType;
/**
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 */

///////////////////////////////////////////////////////////////////////////
$customYM = Yii::$app->request->post('customYM', date('Ym'));
///////////////////////////////////////////////////////////////////////////

function _getBlankItem($customYM, DateTime $date)
{
    return [
        'dayNumber' => $date->format('d'),
        'isHoliday' => in_array($date->format('N'), [6,7]),
        'isCurrentMonth' =>  $date->format('Ym') == $customYM,
        'income' => 0,
        'expense' => 0,
        'total' => 0,
        'flowList' => []
    ];
}

$date = date_create_from_format('Ymd', $customYM.'01');
$prevDate = (clone $date)->modify("-1 month");
$nextDate = (clone $date)->modify("+1 month");
$currMonthData = [
    'title' => ArrayHelper::getValue(AbstractFinance::$month, $date->format('m')) .', '. $date->format('Y'),
    'value' => $date->format('Ym')
];
$prevMonthData = [
    'title' => ArrayHelper::getValue(AbstractFinance::$month, $prevDate->format('m')) .', '. $prevDate->format('Y'),
    'value' => $prevDate->format('Ym'),
    'icon'  => '<i class="mr-1" style="transform:rotate(90deg);"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#shevron"></use></svg></i>'
];
$nextMonthData = [
    'title' => ArrayHelper::getValue(AbstractFinance::$month, $nextDate->format('m')) .', '. $prevDate->format('Y'),
    'value' => $nextDate->format('Ym'),
    'icon'  => '<i class="ml-1" style="transform:rotate(270deg);"><svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#shevron"></use></svg></i>'
];

$ret = [];
$weekNumber = 0;
if ($date->format('N') > 1) {
    while ($date->format('N') > 1) {
        $date = $date->modify("-1 days");
    }
}
while ($date->format('Ym') <= $customYM) {

    if (!isset($ret[$weekNumber]))
        $ret[$weekNumber] = [];

    $ymd = $date->format('Ymd');
    $dayNumber = $date->format('d');
    $dayOfWeek = $date->format('N');
    $ret[$weekNumber][$ymd] = _getBlankItem($customYM, $date);

    if ($dayOfWeek == 7)
        $weekNumber++;

    $date = $date->modify("+1 days");
}
$lastWeekNumber = array_key_last($ret);
if (count($ret[$lastWeekNumber]) < 7) {
    while (count($ret[$lastWeekNumber]) < 7) {

        $ymd = $date->format('Ymd');
        $dayNumber = $date->format('d');
        $dayOfWeek = $date->format('N');
        $ret[$lastWeekNumber][$ymd] = _getBlankItem($customYM, $date);

        $date = $date->modify("+1 days");
    }
}

$monthData = &$ret;

// get db data
$periodFrom = array_key_first($ret[0]);
$periodTo = array_key_last($ret[$lastWeekNumber]);
if ($periodFrom && $periodTo) {
    $periodFrom = date_create_from_format('Ymd', $periodFrom);
    $periodTo = date_create_from_format('Ymd', $periodTo);
    if ($periodFrom && $periodTo) {

        $dbFlows = $searchModel->searchByPaymentCalendar($periodFrom->format('Y-m-d'), $periodTo->format('Y-m-d'));

        foreach ($monthData as $weekNum => $weeks) {
            foreach ($weeks as $ymd => $day) {
                if (isset($dbFlows[$ymd])) {
                    foreach ($dbFlows[$ymd] as $dbFlow) {
                        if ($dbFlow['type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                            $monthData[$weekNum][$ymd]['income'] += $dbFlow['amount'];
                            $monthData[$weekNum][$ymd]['total'] += $dbFlow['amount'];
                            $monthData[$weekNum][$ymd]['flowList'][] = $dbFlow;
                        } elseif ($dbFlow['type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                            $monthData[$weekNum][$ymd]['expense'] += $dbFlow['amount'];
                            $monthData[$weekNum][$ymd]['total'] -= $dbFlow['amount'];
                            $monthData[$weekNum][$ymd]['flowList'][] = $dbFlow;
                        }
                    }
                }
            }
        }
    }
}

$currentYmd = date('Ymd');
$floorMap = Yii::$app->request->post('floorMap', []);
?>

<style>
    .pcc_month {
        width: 100%;
    }
    .pcc_month .pcc_row {
        border-top: 1px solid #e2e5eb;
    }
    .pcc_month .pcc_row:first-child {
        border-top: none;
    }
    .pcc_month .pcc_day {
        border-right: 1px solid #e2e5eb;
        vertical-align: top;
    }
    .pcc_month .pcc_day:last-child {
        border-right: none;
    }
    .pcc_month .pcc_day.from_other_month {
        opacity: 0.5;
    }
    .pcc_month .pcc_day_header {
        text-align: center;
        font-size: 18px;
        padding: 8px;
        font-weight: bold;
        border-bottom: 1px solid #e2e5eb;
    }
    .pcc_month .pcc_day_header.red {
        color: #e7131e;
    }
    .pcc_month .pcc_day.current {
        background-color: #f9f8fb;
    }
    .pcc_month .pcc_day.current .pcc_day_header > span {
        color: #0097fd!important;
        padding: 0 6px;
        border: 2px solid #0097fd;
        border-radius: 4px;
    }
    .pcc_month .pcc_day_body {
        width: 100%;
    }
    .pcc_day_body .label {
        display: inline-block;
        font-size: 14px;
        color: #9198a0;
        padding: 0;
        margin: 0;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .pcc_day_body .label-contractor {
        display: inline-block;
        max-width: 150px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        font-size: 12px;
        font-weight: 300;
        color: #9198a0;
        padding: 0;
        margin: 0;
    }
    .pcc_day_body .details {
        padding: 3px 0 0 10px;
        white-space: nowrap;
        font-size: 14px;
        text-align: right;
    }
    .pcc_day_body .amount {
        padding: 3px 0 0 10px;
        white-space: nowrap;
        font-size: 14px;
        text-align: right;
        font-weight: bold;
    }
    .pcc_day_body .amount.red,
    .pcc_day_body .amount.red > a {
        color: #e7131e;
    }
    .pcc_day_body .amount.blue,
    .pcc_day_body .amount.blue > a {
        color: #0097fd;
    }
    .pcc_day_body a:hover,
    .pcc_day_body a:active {
        text-decoration: none!important;
    }
</style>

<?php Pjax::begin([
    'id' => 'pjax-payment-calendar',
    'enablePushState' => false,
    'timeout' => 5000
]) ?>

<?= Html::hiddenInput('customYM', $customYM, ['id' => 'customYM-tab_report']); ?>

<div class="row p-3">
    <div class="col-4 text-left">
        <?= Html::button($prevMonthData['icon'] . $prevMonthData['title'], [
            'class' => 'pcc-change-month button-regular button-hover-transparent',
            'data-ym' => $prevMonthData['value']
        ]) ?>
    </div>
    <div class="col-4 text-center nowrap">
        <h4>
            <?= $currMonthData['title'] ?>
        </h4>
    </div>
    <div class="col-4 text-right">
        <?= Html::button($nextMonthData['title'] . $nextMonthData['icon'], [
            'class' => 'pcc-change-month button-regular button-hover-transparent',
            'data-ym' => $nextMonthData['value']
        ]) ?>
    </div>
</div>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap p-3">
        <table class="table-bleak pcc_month">
        <?php foreach ($monthData as $weekNumber => $weekData): ?>
            <tr class="pcc_row">
            <?php foreach ($weekData as $ymd => $dayData): ?>
                <?php $detailsKey = ($dayData['flowList']) ? $ymd : null ?>
                <td width="14.285%" class="pcc_day <?=(!$dayData['isCurrentMonth']) ? 'from_other_month':'' ?> <?=($ymd == $currentYmd) ? 'current':'' ?>">
                    <div class="pcc_day_header <?= $dayData['isHoliday'] ? 'red':'' ?>">
                        <span>
                            <?= (int)$dayData['dayNumber'] ?>
                        </span>
                    </div>
                    <div class="p-2">
                        <table class="table-bleak pcc_day_body">
                            <tr>
                                <td class="label">Приход</td>
                                <td class="amount <?= ($dayData['income'] > 0) ? 'blue':'' ?>">
                                    <?= TextHelper::invoiceMoneyFormat($dayData['income'], 2) ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">Расход</td>
                                <td class="amount <?= ($dayData['expense'] > 0) ? 'red':'' ?>">
                                    <?= TextHelper::invoiceMoneyFormat($dayData['expense'], 2) ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="label bold">
                                    <div class="pt-2 pb-1">
                                        Итого
                                    </div>
                                </td>
                                <td class="amount <?= ($dayData['total'] < 0) ? 'red':'' ?>" style="padding-top:0">
                                    <div class="pt-2 pb-1">
                                        <?= TextHelper::invoiceMoneyFormat($dayData['total'], 2) ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <?php if ($detailsKey): ?>
                                        <?= php_rutils\RUtils::numeral()->getPlural(count($dayData['flowList']), ['платеж', 'платежа', 'платежей']); ?>
                                    <?php endif; ?>
                                </td>
                                <td class="details">
                                    <?php if ($detailsKey): ?>
                                        <a class="pcc_show_details" href="javascript:void(0)" data-key="<?=($detailsKey)?>">
                                            Показать
                                        </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr class="pcc_details <?=(ArrayHelper::getValue($floorMap, $detailsKey) ? '' : 'hidden') ?>" data-key="<?=($detailsKey)?>">
                                <td colspan="2" class="text-right">
                                    <div class="pt-2">
                                    <?php foreach ($dayData['flowList'] as $flow): ?>


                                            <div class="amount <?=($flow['type'] == CashFlowsBase::FLOW_TYPE_INCOME) ? 'blue':'red' ?>" style="font-weight: normal!important;">
                                                <?php if ($searchModel->multiCompanyManager->canEdit($flow)): ?>
                                                    <?= Html::a(TextHelper::invoiceMoneyFormat($flow['amount'], 2), '#update-movement', [
                                                        'title' => 'Изменить',
                                                        'class' => '',
                                                        'data-toggle' => 'modal',
                                                        'data-url' => Url::to([
                                                            '/analytics/finance-ajax/update-plan-item',
                                                            'id' => $flow['id'],
                                                            'tb' => 'plan_cash_flows'
                                                        ]),
                                                    ]) ?>
                                                <?php else: ?>
                                                    <?= TextHelper::invoiceMoneyFormat($flow['amount'], 2) ?>
                                                <?php endif ?>
                                            </div>
                                        <div class="label-contractor">
                                            <?php if ($flow['contractor_name'])
                                                echo htmlspecialchars($flow['contractor_name']);
                                            else
                                                switch ($flow['contractor_id']) {
                                                    case CashContractorType::CUSTOMERS_TEXT:
                                                        echo 'Физ. лица';
                                                        break;
                                                    case CashContractorType::COMPANY_TEXT:
                                                        echo 'Компания';
                                                        break;
                                                    case CashContractorType::BALANCE_TEXT:
                                                        echo 'Баланс начальный';
                                                        break;
                                                    case CashContractorType::BANK_TEXT:
                                                    case CashContractorType::EMONEY_TEXT:
                                                    case CashContractorType::ORDER_TEXT:
                                                        echo 'Перевод м.с.';
                                                        break;
                                                    default:
                                                        echo $flow['contractor_id'];
                                                        break;
                                                }
                                            ?>
                                        </div>
                                    <?php endforeach; ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </table>
    </div>
</div>

<?php Pjax::end(); ?>

<script>

    PCC = {
        pjaxContainer: '#pjax-payment-calendar',
        messageAfterUpdate: '',
        init: function() {
            PCC.bindEvents();
        },
        bindEvents: function () {

            const that = this;

            // change month
            $(document).on('click', '.pcc-change-month', function()
            {
                $.pjax.reload(that.pjaxContainer, {
                    type: "post",
                    data: {"customYM": $(this).data('ym')}
                });
            });

            // show day details
            $(document).on('click', '.pcc_show_details', function() {
                const nextTr = $(this).closest('tr').next();
                $(this).html(nextTr.hasClass('hidden') ? 'Скрыть' : 'Показать');
                nextTr.toggleClass('hidden');
            });

            //update plan flow
            $(document).on('submit', '#js-plan_cash_flow_form', function(e) {
                e.preventDefault();

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    PCC.messageAfterUpdate = data.msg;
                    PCC.reloadTable();
                    return false;
                });
            });

            // after update
            $(document).on("pjax:complete", this.pjaxContainer, function() {
                const modal = $('.modal:visible');
                if (modal.length) {
                    $(modal).modal('hide');
                    PCC.reloadChart();
                    PCC.showFlash(PCC.messageAfterUpdate);
                    PCC.messageAfterUpdate = '';
                }
            });
        },
        reloadTable: function()
        {
            const activeTab = $('#active-tab_report').val();
            const customYM = $('#customYM-tab_report').val();
            let floorMap = {};

            $('table.pcc_month').find('.pcc_details').each(function(i,v) {
                if (!$(v).hasClass('hidden'))
                    floorMap[$(v).data('key')] = 1;
            });

            $.pjax.reload(this.pjaxContainer, {
                type: "post",
                data: {
                    "customYM": customYM,
                    "activeTab": activeTab,
                    "floorMap": floorMap,
                }
            });
        },
        reloadChart: function() {
            return window.ChartPlanFactDays.redrawByClick();
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    /////////////////////
    PCC.init();
    /////////////////////    
    
</script>
