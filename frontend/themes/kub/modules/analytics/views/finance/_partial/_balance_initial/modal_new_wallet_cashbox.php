<?php

use common\models\cash\Cashbox;

$company = Yii::$app->user->identity->currentEmployeeCompany->company;
$newCashbox = new Cashbox([
    'company_id' => $company->id,
    'is_accounting' => false,
]);

echo $this->render('@frontend/views/cashbox/_modal_form', [
    'cashbox' => $newCashbox,
    'title' => 'Добавить кассу',
    'id' => 'add-company-cashbox',
    'company' => $company,
    'enableAjaxValidation' => true,
    'actionUrl' => '/cashbox/create-with-ajax-validation',
]);