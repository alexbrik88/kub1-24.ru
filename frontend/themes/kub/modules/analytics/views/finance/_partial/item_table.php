<?php

use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\components\helpers\Html;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\components\grid\GridView;
use frontend\widgets\TableConfigWidget;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use common\models\company\CheckingAccountant;
use common\components\ImageHelper;
use common\models\cash\CashFlowsBase;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use frontend\rbac\permissions;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use common\components\TextHelper;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\currency\Currency;

/* @var $this yii\web\View
 * @var $searchModel AbstractFinance
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => Yii::$app->user->identity->company_id])->exists();
$canIndustryUpdate = $canUpdate && CompanyIndustry::find()->where(['company_id' => Yii::$app->user->identity->company_id])->exists();
$canProjectUpdate = $canUpdate && Project::find()->where(['company_id' => Yii::$app->user->identity->company_id])->exists();
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');
$multiCompanyManager = $searchModel->multiCompanyManager;

$userConfig = Yii::$app->user->identity->config;
$tableConfigItems = [
    ['attribute' => 'report_pc_industry'],
    ['attribute' => 'report_pc_sale_point'],
    ['attribute' => 'report_pc_project'],
];
$tabConfig = [
    'industry' => $userConfig->report_pc_industry ?? false,
    'sale_point' => $userConfig->report_pc_sale_point ?? false,
    'project' => $userConfig->report_pc_project ?? false,
];

$dropdownButtons = [
    $canUpdate ? \yii\helpers\Html::a('<span>Дата признания</span>', '#many-recognition-date', [
        'class' => 'dropdown-item',
        'data-toggle' => 'modal',
        'data-pjax' => 0
    ]) : null,
    $canUpdate ? \yii\helpers\Html::a('<span>Статья</span>', '#many-item', [
        'class' => 'dropdown-item',
        'data-toggle' => 'modal',
        'data-pjax' => 0
    ]) : null,
    $canSalePointUpdate ? \yii\helpers\Html::a('<span>Точка продаж</span>', '#many-sale-point', [
        'class' => 'dropdown-item',
        'data-toggle' => 'modal',
        'data-pjax' => 0
    ]) : null,
    $canIndustryUpdate ? Html::a('<span>Направление</span>', '#many-company-industry', [
        'class' => 'dropdown-item',
        'data-toggle' => 'modal',
        'data-pjax' => 0
    ]) : null,
    $canProjectUpdate ? Html::a('<span>Проект</span>', '#many-project', [
        'class' => 'dropdown-item',
        'data-toggle' => 'modal',
        'data-pjax' => 0
    ]) : null,
];

if (isset($showManyChangeCashbox)) {
    $dropdownButtons = array_merge($dropdownButtons, [
        $canUpdate ? \yii\helpers\Html::a('<span>Касса</span>', '#many-cashbox', [
            'class' => 'dropdown-item dropdown-item-cashbox',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
    ]);
}
?>
<div class="items-table-block  pt-1 pb-1 hidden">
<div class="row align-items-center pb-1 flex-nowrap">
    <div class="column">
        <h4 class="caption mb-2">Детализация</h4>
    </div>
    <div class="column ml-auto">
        <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
            <a class="button-regular button-regular_red ml-2 mb-2 --close-item-table close-odds" href="javascript:;">Закрыть</a>
        </div>
    </div>
</div>

<div class="mr-1">
    <div class="mb-2">
        <?= TableConfigWidget::widget(['items' => $tableConfigItems]); ?>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'odds-items_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>
<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none mb-0">
        <div class="custom-scroll-table">
            <div class="table-wrap">
            <?= GridView::widget([
                'dataProvider' => $itemsDataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-left',
                            'width' => '1%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center joint-checkbox-td',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) {

                            if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                $typeCss = 'income-item';
                                $income = bcdiv($flows['tin_child_amount'] ?: $flows['amount_rub'], 100, 2);
                                $expense = 0;
                            } else {
                                $typeCss = 'expense-item';
                                $income = 0;
                                $expense = bcdiv($flows['tin_child_amount'] ?: $flows['amount_rub'], 100, 2);
                            }

                            return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                'class' => 'joint-checkbox ' . $typeCss,
                                'value' => $flows['id'],
                                'data' => [
                                    'income' => $income,
                                    'expense' => $expense,
                                    'is_foreign' => (($flows['currency_name'] ?? "RUB") !== "RUB") ? 1 : 0,
                                    'industry' => $flows['industry_id'],
                                    'sale_point' => $flows['sale_point_id'],
                                    'project' => $flows['project_id']
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'value' => function ($flows) use ($searchModel) {
                            return !in_array($searchModel->group, [AbstractFinance::GROUP_CONTRACTOR, AbstractFinance::GROUP_PAYMENT_TYPE]) ?
                                DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
                        },
                    ],
                    [
                        'attribute' => 'amountIncome',
                        'label' => 'Приход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell',
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {
                            if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                return TextHelper::invoiceMoneyFormat($flows['tin_child_amount'] ?: $flows['amount'], 2);
                            }
                            return null;
                        },
                    ],
                    [
                        'attribute' => 'amountExpense',
                        'label' => 'Расход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell',
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {
                            if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_EXPENSE) {
                                return TextHelper::invoiceMoneyFormat($flows['tin_child_amount'] ?: $flows['amount'], 2);
                            }
                            return null;
                        },
                    ],
                    [
                        'attribute' => 'payment_type',
                        'label' => 'Тип оплаты',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            AbstractFinance::CASH_BANK_BLOCK => 'Банк',
                            AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
                            AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
                            AbstractFinance::CASH_ACQUIRING_BLOCK => 'Эквайринг',
                            AbstractFinance::CASH_CARD_BLOCK => 'Карты',
                        ],
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [AbstractFinance::GROUP_CONTRACTOR, AbstractFinance::GROUP_DATE])) {

                                $currencySymbol = ($flows['currency_name'] === "RUB") ? '' : ArrayHelper::getValue(Currency::$currencySymbols, $flows['currency_name'], '');

                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        /* @var $checkingAccountant CheckingAccountant */
                                        $checkingAccountant = CheckingAccountant::find()->andWhere([
                                            'rs' => $model->rs,
                                            'company_id' => $model->company->id,
                                        ])->orderBy(['type' => SORT_ASC])->one();
                                        if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                            return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                    'class' => 'little_logo_bank',
                                                    'style' => 'display: inline-block;',
                                                ]);
                                        }
                                        return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                                    case CashOrderFlows::tableName():
                                        return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                                    case CashEmoneyFlows::tableName():
                                        return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                                    case AcquiringOperation::tableName():
                                        return 'Эквайринг';
                                    case CardOperation::tableName():
                                        return 'Карты';
                                    case CashBankForeignCurrencyFlows::tableName():
                                        return 'Банк  ' . $currencySymbol;
                                    case CashOrderForeignCurrencyFlows::tableName():
                                        return 'Касса ' . $currencySymbol;
                                    case CashEmoneyForeignCurrencyFlows::tableName():
                                        return 'E-money ' . $currencySymbol;
                                    default:
                                        return '';
                                }
                            }
                            return '';
                        },
                        's2width' => '150px',
                    ],
                    [
                        's2width' => '200px',
                        'filter' => ['' => 'Все'] + $multiCompanyManager->getAllowedCompaniesList(),
                        'attribute' => 'company_id',
                        'label' => 'Компания',
                        'headerOptions' => [
                            'class' => $multiCompanyManager->getIsModeOn() ? '' : ' hidden',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'company-cell text-left' . ($multiCompanyManager->getIsModeOn() ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($multiCompanyManager) {
                            $companyName = $multiCompanyManager->getCompanyName($flows['company_id']);
                            return '<span title="' . htmlspecialchars($companyName) . '">' . $companyName . '</span>';
                        },
                    ],
                    [
                        's2width' => '250px',
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'contractor-cell'
                        ],
                        'format' => 'raw',
                        'filter' => !in_array($searchModel->group, [AbstractFinance::GROUP_PAYMENT_TYPE, AbstractFinance::GROUP_DATE]) ?
                            $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [AbstractFinance::GROUP_PAYMENT_TYPE, AbstractFinance::GROUP_DATE])) {
                                /* @var $contractor Contractor */
                                $contractor = Contractor::findOne($flows['contractor_id']);
                                if ($contractor !== null) {
                                    $contractorName = $contractor->nameWithType;
                                } else {

                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        case AcquiringOperation::tableName():
                                            $model = AcquiringOperation::findOne($flows['id']);
                                            break;
                                        case CardOperation::tableName():
                                            $model = CardOperation::findOne($flows['id']);
                                            break;
                                        default:
                                            return '';
                                    }

                                    if (!empty($model->otherCashbox)) {
                                        $contractorName = $model->otherCashbox->name;
                                    } elseif (!empty($model->cashContractor)) {
                                        $contractorName = $model->cashContractor->text;
                                    } else {
                                        $contractorName = '---';
                                    }
                                }

                                return '<span title="' . htmlspecialchars($contractorName) . '">' . $contractorName . '</span>';
                            }

                            return '';
                        },
                    ],
                    [
                        's2width' => '250px',
                        'attribute' => 'industry_id',
                        'label' => 'Направление',
                        'headerOptions' => [
                            'class' => 'col_report_pc_industry' . ($tabConfig['industry'] ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left reason-cell col_report_pc_industry ' . ($tabConfig['industry'] ? '' : ' hidden'),
                        ],
                        'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
                        'format' => 'raw',
                        'value' => function ($flows) {

                            $ret = '';

                            if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                                $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                            }

                            return $ret;
                        },
                    ],
                    [
                        's2width' => '250px',
                        'attribute' => 'sale_point_id',
                        'label' => 'Точка продаж',
                        'headerOptions' => [
                            'class' => 'col_report_pc_sale_point' . ($tabConfig['sale_point'] ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left reason-cell col_report_pc_sale_point ' . ($tabConfig['sale_point'] ? '' : ' hidden'),
                        ],
                        'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
                        'format' => 'raw',
                        'value' => function ($flows) {

                            $ret = '';

                            if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                                $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                            }

                            return $ret;
                        },
                    ],
                    [
                        's2width' => '250px',
                        'attribute' => 'project_id',
                        'label' => 'Проект',
                        'headerOptions' => [
                            'class' => 'col_report_pc_project' . ($tabConfig['project'] ? '' : ' hidden'),
                            'width' => '13%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left reason-cell col_report_pc_project ' . ($tabConfig['project'] ? '' : ' hidden'),
                        ],
                        'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
                        'format' => 'raw',
                        'value' => function ($flows) {

                            $ret = '';

                            if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                                $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                            }

                            return $ret;
                        },
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'purpose-cell'
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                if ($flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                } else {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        case AcquiringOperation::tableName():
                                        case CardOperation::tableName():
                                        default:
                                            $model = null;
                                    }
                                    if ($model && ($invoiceArray = $model->getInvoices()->all())) {
                                        $linkArray = [];
                                        foreach ($invoiceArray as $invoice) {
                                            $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ]);
                                        }

                                        return join(', ', $linkArray);
                                    }
                                }
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'billPaying',
                        'label' => 'Опл. счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($multiCompanyManager) {

                            switch ($flows['tb']) {
                                case CashBankFlows::tableName():
                                    $model = CashBankFlows::findOne($flows['id']);
                                    $route = '/cash/bank/update';
                                    break;
                                case CashOrderFlows::tableName():
                                    $model = CashOrderFlows::findOne($flows['id']);
                                    $route = '/cash/order/update';
                                    break;
                                case CashEmoneyFlows::tableName():
                                    $model = CashEmoneyFlows::findOne($flows['id']);
                                    $route = '/cash/e-money/update';
                                    break;
                                case AcquiringOperation::tableName():
                                case CardOperation::tableName():
                                default:
                                    $model = null;
                                    $route = null;
                            }

                            if ($model) {

                                $invoices = [];
                                foreach ($model->invoices as $invoice) {
                                    $invoices[] = ($multiCompanyManager->canEdit($flows)) ?
                                        Html::a('№' . $invoice->fullNumber, [
                                            '/documents/invoice/view',
                                            'id' => $invoice->id,
                                            'type' => $invoice->type,
                                        ]) :
                                        Html::tag('span', '№' . $invoice->fullNumber);
                                }

                                $billInvoices = !empty($invoices) ? implode(', ', $invoices) : '';

                                if ($model->getAvailableAmount() > 0 && $multiCompanyManager->canEdit($flows)) {
                                    return $billInvoices . ($billInvoices ? '<br />' : '') .
                                        Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', [
                                            $route,
                                            'id' => $model->id,
                                        ], [
                                            'title' => 'Уточнить',
                                            'data' => [
                                                'toggle' => 'modal',
                                                'target' => '#update-movement',
                                            ],
                                        ]);
                                }

                                return $billInvoices;
                            }

                            return '';
                        },
                    ],
                    [
                        's2width' => '200px',
                        'attribute' => 'reason_id',
                        'label' => 'Статья',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'reason-cell text-left',
                        ],
                        'filter' => empty($searchModel->group) ?
                            array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                            ['' => 'Все статьи'],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $reason = ($reasonModel = InvoiceIncomeItem::findOne($flows['income_item_id'])) ? $reasonModel->fullName : null;
                                } elseif ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                                    $reason = ($reasonModel = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ? $reasonModel->fullName : null;
                                } else {
                                    $reason = null;
                                }

                                return $reason ? ('<span title="' . htmlspecialchars($reason) . '">' . $reason . '</span>') : '-';
                            }

                            return '';
                        },
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update}{delete}',
                        'headerOptions' => [
                            'width' => '3%',
                        ],
                        'contentOptions' => [
                            'class' => 'action-line nowrap',
                        ],
                        'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                        'buttons' => [
                            'update' => function ($url, $flows, $key) use ($multiCompanyManager) {
                                $url = 'javascript:;';
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $url = Url::to(['/cash/bank/update', 'id' => $flows['id']]);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $url = Url::to(['/cash/order/update', 'id' => $flows['id']]);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $url = Url::to(['/cash/e-money/update', 'id' => $flows['id']]);
                                        break;
                                    case AcquiringOperation::tableName():
                                        $url = Url::to(['/acquiring/acquiring/update', 'id' => $flows['id']]);
                                        break;
                                    case CardOperation::tableName():
                                        $url = Url::to(['/cards/operation/update', 'id' => $flows['id'], 'redirectUrl' => Url::current()]);
                                        break;
                                    case CashBankForeignCurrencyFlows::tableName():
                                        $url = Url::to(['/cash/bank/update', 'id' => $flows['id'], 'foreign' => 1]);
                                        break;
                                    case CashOrderForeignCurrencyFlows::tableName():
                                        $url = Url::to(['/cash/order/update', 'id' => $flows['id'], 'foreign' => 1]);
                                        break;
                                    case CashEmoneyForeignCurrencyFlows::tableName():
                                        $url = Url::to(['/cash/e-money/update', 'id' => $flows['id'], 'foreign' => 1]);
                                        break;
                                }
                                return ($multiCompanyManager->canEdit($flows)) ?
                                    Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', $url, [
                                    'title' => 'Изменить',
                                    'class' => 'update-flow-item button-clr link mr-1',
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#update-movement',
                                        'pjax' => 0
                                    ],
                                ]) : '';
                            },
                            'delete' => function ($url, $flows) use ($multiCompanyManager) {
                                return ($multiCompanyManager->canEdit($flows)) ?
                                    \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                                            'class' => 'delete-flow-item button-clr link',
                                            'tag' => 'a',
                                        ],
                                        'options' => [
                                            'id' => 'delete-flow-item-' . $flows['id'],
                                            'class' => 'modal-delete-flow-item',
                                        ],
                                        'confirmUrl' => Url::to([
                                            '/analytics/finance-ajax/delete-flow-item',
                                            'id' => $flows['id'],
                                            'tb' => $flows['tb']
                                        ]),
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить операцию?',
                                    ]) : '';
                            },
                        ],
                    ],
                ],
            ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>

<?= $this->render('_summary_select/_summary_select', [
    'buttons' => [
        $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
    ],
    'dropdownButtons' => $dropdownButtons
]); ?>

<?= $this->render('_item_table/modals', [
    'searchModel' => $searchModel,
    'showManyChangeCashbox' => true
]) ?>

<?php
$js = <<<SCRIPT

// SummarySelect

$(document).on('change', '.joint-checkbox', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

$(document).on("pjax:complete", "#odds-items_pjax", function(e) {
    $('#summary-container').removeClass('visible check-true');
});



SCRIPT;

$this->registerJs($js);
?>

</div>

<?php $this->registerJs('
$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();

    var button = $(event.relatedTarget);
    var modal = $(this);

    modal.find(".modal-body").load(button.attr("href"));

});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(".close-item-table").on("click", function() {
    var $block = $(".items-table-block");
    $block.find(".caption").html("Детализация");
    $block.find(".wrap_padding_none").html("");
    $block.addClass("hidden");
});
'); ?>