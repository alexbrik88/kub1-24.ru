<?php

use \frontend\modules\analytics\models\BreakEvenSearchModel;
use common\components\TextHelper;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use yii\web\JsExpression;

/** @let $total array */
/** @let $searchModel BreakEvenSearchModel */

///////////////// colors ////////////////
$color = [
    'revenue' => 'rgba(127,221,127,1)',
    'fixedCosts' => 'rgba(50,50,50,1)',
    'totalCosts' => 'rgba(253,45,1,1)',
    'breakEven' => 'rgba(50,50,50,1)',
    'zoneProfit' => 'rgba(127,221,127,.3)',
    'zoneLoss' => 'rgba(253,45,1,.3)',
    'plotLine' => 'rgba(153,153,153,1)',
];
/////////////////////////////////////////

/////////////// consts //////////////////
$SCALE_VIEW = 2;
/////////////////////////////////////////

// LINES
$revenue = [
    [0, 0],
    [$SCALE_VIEW * $total['quantity'], $SCALE_VIEW * $total['revenue']]
];
$fixedCosts = [
    [0, $total['fixedCosts']],
    [$SCALE_VIEW * $total['quantity'], $total['fixedCosts']]
];
$totalCosts = [
    [0, $total['fixedCosts']],
    [$SCALE_VIEW * $total['quantity'], $SCALE_VIEW * $total['variableCosts'] + $total['fixedCosts']]
];

// POINTS
$pointRevenue = [[
    $total['quantity'],
    $total['revenue']
]];
$pointTotalCosts = [[
    $total['quantity'],
    $total['fixedCosts'] + $total['variableCosts']
]];

$__pointBreakEvenX = ($total['revenue'] - $total['variableCosts'] > 0) ?
    ($total['fixedCosts'] / ($total['revenue'] - $total['variableCosts']) * $total['quantity']) : 0;
$__pointBreakEvenY = ($total['quantity'] > 0) ?
    ($__pointBreakEvenX * $total['revenue'] / $total['quantity']) : 0;

$pointBreakEven = [[
    round($__pointBreakEvenX, 2),
    round($__pointBreakEvenY, 2)
]];

?>

<script>

    const colorZoneProfit = '<?=($color['zoneProfit'])?>';
    const colorZoneLoss = '<?=($color['zoneLoss'])?>';

    String.prototype.sanitizeKub = function sanitizeKubFN() { return this.replace(/\s/g, '').replace(',', '.'); };

    BreakEven = {

        _loaded: false,

        init: function()
        {
            this.Table2.bindEvents();
            this.Table2.recalcCalculationColumns();
            this.Table2.recalcVariationColumns();

            this.Chart.updateCurrentMonth($('#break-even-table-2 .revenue.total'));
            this.Chart.repaintTotals();

            this.Table.updateCurrentMonthColumns();
            this.Table.recalcCalculationColumns();
            this.Table.recalcVariationColumns();
        },

        Chart: {

            SCALE_VIEW: <?= (float)$SCALE_VIEW ?>,
            xBreakEven: <?= (float)$__pointBreakEvenX ?>,
            yBreakEven: <?= (float)$__pointBreakEvenY ?>,
            // table2 data
            yRevenue: <?= (float)$total['revenue'] ?>,
            xQuantity: <?= (float)$total['quantity'] ?>,
            yVariableCosts: <?= (float)$total['variableCosts'] ?>,
            yFixedCosts: <?= (float)$total['fixedCosts'] ?>,
            // plot line data
            yCustomRevenue: <?= (float)$total['revenue'] ?>,
            xCustomQuantity: <?= (float)$total['quantity'] ?>,
            yCustomCosts: <?= (float)$total['fixedCosts'] + (float)$total['variableCosts'] ?>,

            draggablePlotLine: function(axis, plotLineId)
            {
                let clickX, clickY;

                let getPlotLine = function () {
                    for (let i = 0; i < axis.plotLinesAndBands.length; i++) {
                        if (axis.plotLinesAndBands[i].id === plotLineId) {
                            return axis.plotLinesAndBands[i];
                        }
                    }
                };

                let getValue = function() {
                    let plotLine = getPlotLine();
                    let translation = axis.horiz ? plotLine.svgElem.translateX : plotLine.svgElem.translateY;
                    let new_value = axis.toValue(translation) - axis.toValue(0) + plotLine.options.value;
                    new_value = Math.max(axis.min, Math.min(axis.max, new_value));
                    return new_value;
                };

                let drag_start = function (e) {
                    $(document).bind({
                        'mousemove.line': drag_step,
                        'mouseup.line': drag_stop
                    });

                    let plotLine = getPlotLine();
                    clickX = e.pageX - plotLine.svgElem.translateX;
                    clickY = e.pageY - plotLine.svgElem.translateY;
                    if (plotLine.options.onDragStart) {
                        plotLine.options.onDragStart(getValue());
                    }
                };

                let drag_step = function (e) {
                    let plotLine = getPlotLine();
                    let new_translation = axis.horiz ? e.pageX - clickX : e.pageY - clickY;
                    let new_value = axis.toValue(new_translation) - axis.toValue(0) + plotLine.options.value;
                    new_value = Math.max(axis.min, Math.min(axis.max, new_value));
                    new_translation = axis.toPixels(new_value + axis.toValue(0) - plotLine.options.value);
                    plotLine.svgElem.translate(
                        axis.horiz ? new_translation : 0,
                        axis.horiz ? 0 : new_translation);

                    if (plotLine.options.onDragChange) {
                        plotLine.options.onDragChange(new_value);
                    }
                };

                let drag_stop = function () {
                    $(document).unbind('.line');

                    let plotLine = getPlotLine();
                    let plotLineOptions = plotLine.options;
                    //Remove + Re-insert plot line
                    //Otherwise it gets messed up when chart is resized
                    if (plotLine.svgElem.hasOwnProperty('translateX')) {
                        plotLineOptions.value = getValue()
                        axis.removePlotLine(plotLineOptions.id);
                        axis.addPlotLine(plotLineOptions);

                        if (plotLineOptions.onDragFinish) {
                            plotLineOptions.onDragFinish(plotLineOptions.value);
                        }
                    }

                    getPlotLine().svgElem
                        .css({'cursor': 'pointer'})
                        .translate(0, 0)
                        .on('mousedown', drag_start);
                };

                drag_stop();

                // init
                if (!BreakEven._loaded) {
                    this.recalcChart(BreakEven.Chart.yCustomRevenue, true);
                    BreakEven._loaded = true;
                }
            },

            recalcChart: function(new_Y, updateZone)
            {
                const chart = $('#chart-break-even').highcharts();
                const revenue = chart.series[3];
                const costs   = chart.series[4];
                const polygon = chart.series[6];
                const break_even_X = this.xBreakEven;
                const break_even_Y = this.yBreakEven;
                const fixed_costs_Y = this.yFixedCosts;
                const costs_k = (costs.points[1].x - costs.points[0].x) / (costs.points[1].y - costs.points[0].y);
                const revenue_k = (revenue.points[1].x - revenue.points[0].x) / (revenue.points[1].y - revenue.points[0].y);
                const revenue_X = revenue_k * (new_Y);
                const costs_X = costs_k * (new_Y - fixed_costs_Y);
                const costs_Y = revenue_X / costs_k + fixed_costs_Y;
                const revenue_Y = new_Y;

                if (updateZone) {
                    polygon.setData([[break_even_X, break_even_Y], [revenue_X - 0.0001, new_Y], [revenue_X, costs_Y]]);
                    polygon.options.color = (new_Y >= break_even_Y) ? colorZoneProfit : colorZoneLoss;
                    polygon.update(polygon.options);
                }

                this.xCustomQuantity = revenue_X;
                this.yCustomRevenue = revenue_Y;
                this.yCustomCosts = costs_Y;
            },

            updateCurrentMonth: function(tr)
            {
                let newVal = $(tr).find('td.curr-amount input').val().sanitizeKub();

                if (isNaN(newVal)) {
                    console.log('newVal is NaN!');
                    console.log(tr);
                    newVal = 0;
                }

                if ($(tr).hasClass('revenue')) {
                    this._updateRevenue($(tr), newVal);
                    this._updateCalculatedRows();
                } else if ($(tr).hasClass('variable-costs')) {
                    this._updateVariableCosts($(tr), newVal);
                    this._updateCalculatedRows();
                } else if ($(tr).hasClass('fixed-costs')) {
                    this._updateFixedCosts($(tr), newVal);
                    this._updateCalculatedRows();
                }

                const chart = $('#chart-break-even').highcharts();
                const pointBreakEven = chart.series[0];
                const pointRevenue = chart.series[1];
                const pointCosts = chart.series[2];
                const revenue = chart.series[3];
                const costs = chart.series[4];
                const fixedCosts = chart.series[5];

                this.xBreakEven = this.yFixedCosts / (this.yRevenue - this.yVariableCosts) * this.xQuantity;
                this.yBreakEven = this.xBreakEven * this.yRevenue / this.xQuantity;

                pointBreakEven.setData([ [this.xBreakEven, this.yBreakEven] ]);
                pointRevenue.setData([ [this.xQuantity, this.yRevenue] ]);
                pointCosts.setData([ [this.xQuantity, this.yFixedCosts + this.yVariableCosts] ]);
                revenue.setData([ [0, 0], [this.SCALE_VIEW * this.xQuantity, this.SCALE_VIEW * this.yRevenue] ]);
                fixedCosts.setData([ [0, this.yFixedCosts], [this.SCALE_VIEW * this.xQuantity, this.yFixedCosts] ]);
                costs.setData([ [0, this.yFixedCosts], [this.SCALE_VIEW * this.xQuantity, this.SCALE_VIEW * this.yVariableCosts + this.yFixedCosts] ]);

                pointBreakEven.update(pointBreakEven.options);
                pointRevenue.update(pointRevenue.options);
                pointCosts.update(pointCosts.options);
                revenue.update(revenue.options);
                fixedCosts.update(fixedCosts.options);
                costs.update(costs.options);

                chart.yAxis[0].options.plotLines[0].value = this.yRevenue;
                chart.yAxis[0].plotLinesAndBands[0].options.value = this.yRevenue;
                chart.yAxis[0].plotLinesAndBands[0].render();
                BreakEven.Chart.recalcChart(this.yRevenue, true);
                BreakEven.Chart.repaintTotals();

                BreakEven.Table.updateCurrentMonthColumns();
                BreakEven.Table.recalcCalculationColumns();
                BreakEven.Table.recalcVariationColumns();

                BreakEven.Table2.recalcCalculationColumns();
                BreakEven.Table2.recalcVariationColumns();
            },

            _updateRevenue: function(tr, val)
            {
                const trs = tr.closest('tbody').find('tr.revenue');
                const trRevenue = trs.filter('.total');
                const trQuantity = trs.filter('.quantity');
                const trAverageCheck = trs.filter('.average-check');

                if (tr.hasClass('total')) {
                    BreakEven.Chart.yRevenue = parseFloat(val.sanitizeKub());
                    BreakEven.Chart.xQuantity = parseFloat(val) / parseFloat(trAverageCheck.find('td.curr-amount input').val().sanitizeKub());
                } else if (tr.hasClass('average-check')) {
                    BreakEven.Chart.xQuantity = parseFloat(trRevenue.find('td.curr-amount input').val().sanitizeKub()) / parseFloat(val);
                }
                trQuantity.find('td.curr-amount input').val(BreakEven.Chart.xQuantity);
                trQuantity.find('td.curr-amount span').html(number_format(BreakEven.Chart.xQuantity, 0, ',', ' '));

                ////// update costs percents ////////////////////
                const table = $('#break-even-table-2 > tbody');
                $(table).find('tr').each(function(i,tr) {
                    const revenue = BreakEven.Chart.yRevenue;
                    if ($(tr).hasClass('variable-costs') || $(tr).hasClass('fixed-costs')) {
                        const amount = $(tr).find('td.curr-amount input').val().sanitizeKub();
                        $(tr).find('td.curr-percent input').val((revenue !== 0) ? (amount / revenue * 100) : 0);
                        $(tr).find('td.curr-percent span').html(number_format((revenue !== 0) ? (amount / revenue * 100) : 0, 2, ',', ' '));
                    }
                });
                ////////////////////////////////////////////////
            },
            _updateVariableCosts: function(tr, val)
            {
                if (tr.hasClass('total')) {
                    // calc sum
                } else {
                    const table = tr.closest('table');
                    const trTotal = $(table).find('tr.total.variable-costs');
                    const revenue = BreakEven.Chart.yRevenue;

                    let totalAmount = 0;
                    let totalPercent;
                    let parentTr = {};
                    let parentTotalAmount = {};

                    $(table.find('tr').filter('.variable-costs')).each(function(i,v) {
                        let itemId = $(v).data('item_id');
                        let parentId = $(v).data('parent_item_id');
                        let rowAmount = $(v).find('td.curr-amount input').val().sanitizeKub();

                        if($(v).hasClass('total'))
                            return true;

                        if ($(v).hasClass('parent') && itemId) {
                            parentTr[itemId] = $(v);
                            parentTotalAmount[itemId] = 0;
                            return true;
                        }

                        if ($(v).hasClass('child') && parentId) {
                            parentTotalAmount[parentId] += parseFloat(rowAmount);
                        }

                        totalAmount += parseFloat(rowAmount);

                        $(v).find('td.curr-percent span').html(number_format((revenue !== 0) ? (100 * rowAmount / revenue) : 0, 2, ',', ' '));
                        $(v).find('td.curr-percent input').val((revenue !== 0) ? (100 * rowAmount / revenue) : 0);
                    });

                    $.each(parentTr, function (itemId, trParent) {
                        let parentAmount = parentTotalAmount[itemId];
                        let parentPercent = (revenue !== 0) ? (100 * parentAmount / revenue) : 0;
                        trParent.find('td.curr-percent span').html(number_format(parentPercent, 2, ',', ' '));
                        trParent.find('td.curr-percent input').val(parentPercent);
                        trParent.find('td.curr-amount span').html(number_format(parentAmount, 2, ',', ' '));
                        trParent.find('td.curr-amount input').html(parentAmount);
                    });

                    totalPercent = (revenue !== 0) ? (100 * totalAmount / revenue) : 0;
                    trTotal.find('td.curr-percent span').html(number_format(totalPercent, 2, ',', ' '));
                    trTotal.find('td.curr-percent input').val(totalPercent);
                    trTotal.find('td.curr-amount span').html(number_format(totalAmount, 2, ',', ' '));
                    trTotal.find('td.curr-amount input').html(totalAmount);
                    BreakEven.Chart.yVariableCosts = totalAmount;
                }
            },
            _updateFixedCosts: function(tr, val)
            {
                if (tr.hasClass('total')) {
                    // calc sum
                } else {
                    const table = tr.closest('table');
                    const trTotal = $(table).find('tr.total.fixed-costs');
                    const revenue = BreakEven.Chart.yRevenue;

                    let totalAmount = 0;
                    let totalPercent;
                    let parentTr = {};
                    let parentTotalAmount = {};

                    $(table.find('tr').filter('.fixed-costs')).each(function(i,v) {
                        let itemId = $(v).data('item_id');
                        let parentId = $(v).data('parent_item_id');
                        let rowAmount = $(v).find('td.curr-amount input').val().sanitizeKub();

                        if($(v).hasClass('total'))
                            return true;

                        if ($(v).hasClass('parent') && itemId) {
                            parentTr[itemId] = $(v);
                            parentTotalAmount[itemId] = 0;
                        }
                        else if ($(v).hasClass('child') && parentId) {
                            parentTotalAmount[parentId] += parseFloat(rowAmount);
                            totalAmount -= parseFloat(rowAmount);
                        }

                        totalAmount += parseFloat(rowAmount);

                        $(v).find('td.curr-percent span').html(number_format((revenue !== 0) ? (100 * rowAmount / revenue) : 0, 2, ',', ' '));
                        $(v).find('td.curr-percent input').val((revenue !== 0) ? (100 * rowAmount / revenue) : 0);
                    });

                    $.each(parentTr, function (itemId, trParent) {
                        let parentAmount = parentTotalAmount[itemId];
                        let parentPercent = (revenue !== 0) ? (100 * parentAmount / revenue) : 0;
                        trParent.find('td.curr-percent span').html(number_format(parentPercent, 2, ',', ' '));
                        trParent.find('td.curr-percent input').val(parentPercent);
                        trParent.find('td.curr-amount span').html(number_format(parentAmount, 2, ',', ' '));
                        trParent.find('td.curr-amount input').html(parentAmount);
                    });

                    totalPercent = (revenue !== 0) ? (100 * totalAmount / revenue) : 0;
                    trTotal.find('td.curr-percent span').html(number_format(totalPercent, 2, ',', ' '));
                    trTotal.find('td.curr-percent input').val(totalPercent);
                    trTotal.find('td.curr-amount span').html(number_format(totalAmount, 2, ',', ' '));
                    trTotal.find('td.curr-amount input').html(totalAmount);
                    BreakEven.Chart.yFixedCosts = totalAmount;
                }
            },
            _updateCalculatedRows: function()
            {
                const table = $('#break-even-table-2 > tbody');
                $(table).find('tr').filter('.calculated').each(function(i,tr) {

                    const revenue = BreakEven.Chart.yRevenue;
                    const variableCosts = BreakEven.Chart.yVariableCosts;
                    const fixedCosts = BreakEven.Chart.yFixedCosts;
                    const marginalIncome = revenue - variableCosts;
                    const marginality = (revenue !== 0) ? (100 * marginalIncome / revenue) : 0;
                    const relativeVariableCosts = (revenue !== 0) ? (variableCosts / revenue) : 0;
                    const relativeFixedCosts = (revenue !== 0) ? (fixedCosts / revenue) : 0;
                    const allCosts = fixedCosts + variableCosts;
                    const relativeAllCosts = (revenue !== 0) ? (allCosts / revenue) : 0;
                    const operatingProfit = marginalIncome - fixedCosts;
                    const relativeOperatingProfit = (revenue !== 0) ? (operatingProfit / revenue) : 0;
                    const efficiency = (revenue !== 0) ? (100 * operatingProfit / revenue) : 0;

                    if ($(tr).hasClass('variable-costs') && $(tr).hasClass('total')) {
                        $(tr).find('td.curr-percent > div > span').html(number_format(100 * relativeVariableCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('fixed-costs') && $(tr).hasClass('total')) {
                        $(tr).find('td.curr-percent > div > span').html(number_format(100 * relativeFixedCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('marginal-income')) {
                        $(tr).find('td.curr-amount > div').html(number_format(marginalIncome, 2, ',', ' '));
                        $(tr).find('td.curr-percent > div > span').html(number_format(100 * marginality, 2, ',', ' '));
                    } else if ($(tr).hasClass('marginality')) {
                        $(tr).find('td.curr-amount > div').html(number_format(marginality, 2, ',', ' ') + '%');
                    } else if ($(tr).hasClass('relative-variable-costs')) {
                        $(tr).find('td.curr-amount > div').html(number_format(relativeVariableCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('relative-fixed-costs')) {
                        $(tr).find('td.curr-amount > div').html(number_format(relativeFixedCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('all-costs')) {
                        $(tr).find('td.curr-amount > div').html(number_format(allCosts, 2, ',', ' '));
                        $(tr).find('td.curr-percent > div > span').html(number_format(100 * relativeAllCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('relative-all-costs')) {
                        $(tr).find('td.curr-amount > div').html(number_format(relativeAllCosts, 2, ',', ' '));
                    } else if ($(tr).hasClass('operating-profit')) {
                        $(tr).find('td.curr-amount > div').html(number_format(operatingProfit, 2, ',', ' '));
                        $(tr).find('td.curr-percent > div > span').html(number_format(100 * relativeOperatingProfit, 2, ',', ' '));
                    } else if ($(tr).hasClass('relative-operating-profit')) {
                        $(tr).find('td.curr-amount > div').html(number_format(relativeOperatingProfit, 2, ',', ' '));
                    } else if ($(tr).hasClass('efficiency')) {
                        $(tr).find('td.curr-amount > div').html(number_format(efficiency, 2, ',', ' ') + '%');
                    }
                });

            },

            repaintTotals: function()
            {
                $('#revenue_tip_1').text(nFormatter(Math.ceil(this.yCustomRevenue || 0), 1));
                $('#revenue_tip_2').text(nFormatter(Math.ceil(this.yCustomCosts || 0), 1));
                $('#revenue_tip_3').text(nFormatter(Math.ceil(this.yCustomRevenue - this.yCustomCosts) || 0, 1));
            },

            onDragPointLine(new_Y, updateZone)
            {
                BreakEven.Table2.recalcCalculationColumns();
                BreakEven.Table2.recalcVariationColumns();
                BreakEven.Table.recalcCalculationColumns();
                BreakEven.Table.recalcVariationColumns();
                BreakEven.Chart.recalcChart(new_Y, updateZone);
                BreakEven.Chart.repaintTotals();
            }
        },
        Table: {
            recalcCalculationColumns: function()
            {
                const table = $('#break-even-table > tbody');
                const revenue = BreakEven.Chart.yCustomRevenue;
                const quantity = BreakEven.Chart.xCustomQuantity;
                const customCosts = BreakEven.Chart.yCustomCosts;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const variableCosts = customCosts - fixedCosts;
                let breakEvenQ;
                let breakEvenA;
                let financialSafetyPercent;
                let criticalAverageCheck;

                if (revenue - variableCosts !== 0 && quantity !== 0 && revenue !== 0) {
                    breakEvenQ = fixedCosts / (revenue - variableCosts) * quantity;
                    breakEvenA = breakEvenQ * revenue / quantity;
                    financialSafetyPercent = (revenue - breakEvenA) / revenue * 100;
                    criticalAverageCheck = (breakEvenA / quantity);
                } else {
                    breakEvenQ = breakEvenA = financialSafetyPercent = criticalAverageCheck = 0;
                }

                $(table).find('tr').each(function(i,tr) {
                    tr = $(tr);
                    if (tr.hasClass('revenue')) {
                        tr.find('td.calc-amount > div').html(number_format(revenue, 2, ',', ' '));
                    } else if (tr.hasClass('break-even-amount')) {
                        tr.find('td.calc-amount > div').html(number_format(breakEvenA, 2, ',', ' '));
                    } else if (tr.hasClass('break-even-quantity')) {
                        tr.find('td.calc-amount > div').html(number_format(breakEvenQ, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-amount')) {
                        tr.find('td.calc-amount > div').html(number_format(revenue - breakEvenA, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-quantity')) {
                        tr.find('td.calc-amount > div').html(number_format(quantity - breakEvenQ, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-percent')) {
                        tr.find('td.calc-amount > div').html(number_format(financialSafetyPercent, 2, ',', ' ') + '%');
                    } else if (tr.hasClass('critical-average-check')) {
                        tr.find('td.calc-amount > div').html(number_format(criticalAverageCheck, 2, ',', ' '));
                    }
                });
            },
            recalcVariationColumns: function()
            {
                const table = $('#break-even-table > tbody');
                $(table).find('tr').each(function(i,tr) {
                    let factAmount = parseFloat($(tr).find('td.prev-amount div').text().trim().sanitizeKub()) || 0;
                    let planAmount =  parseFloat($(tr).find('td.calc-amount div').text().trim().sanitizeKub()) || 0;
                    let variationAmount = planAmount - factAmount;
                    let variationPercent = (factAmount > 0) ? ((planAmount - factAmount) / factAmount * 100) : 100;

                    $(tr).find('td.variation-amount > div').html(number_format(variationAmount, 2, ',', ' '));
                    $(tr).find('td.variation-percent > div > span').html(number_format(variationPercent, 2, ',', ' '));
                });
            },
            updateCurrentMonthColumns: function()
            {
                const table = $('#break-even-table > tbody');
                const revenue = BreakEven.Chart.yRevenue;
                const quantity = BreakEven.Chart.xQuantity;
                const variableCosts = BreakEven.Chart.yVariableCosts;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                let breakEvenQ;
                let breakEvenA;
                let financialSafetyPercent;
                let criticalAverageCheck;

                if (revenue - variableCosts !== 0 && quantity !== 0 && revenue !== 0) {
                    breakEvenQ = fixedCosts / (revenue - variableCosts) * quantity;
                    breakEvenA = breakEvenQ * revenue / quantity;
                    financialSafetyPercent = (revenue - breakEvenA) / revenue * 100;
                    criticalAverageCheck = (breakEvenA / quantity);
                } else {
                    breakEvenQ = breakEvenA = financialSafetyPercent = criticalAverageCheck = 0;
                }

                $(table).find('tr').each(function(i,tr) {
                    tr = $(tr);
                    if (tr.hasClass('revenue')) {
                        tr.find('td.curr-amount > div').html(number_format(revenue, 2, ',', ' '));
                    } else if (tr.hasClass('break-even-amount')) {
                        tr.find('td.curr-amount > div').html(number_format(breakEvenA, 2, ',', ' '));
                    } else if (tr.hasClass('break-even-quantity')) {
                        tr.find('td.curr-amount > div').html(number_format(breakEvenQ, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-amount')) {
                        tr.find('td.curr-amount > div').html(number_format(revenue - breakEvenA, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-quantity')) {
                        tr.find('td.curr-amount > div').html(number_format(quantity - breakEvenQ, 2, ',', ' '));
                    } else if (tr.hasClass('financial-safety-percent')) {
                        tr.find('td.curr-amount > div').html(number_format(financialSafetyPercent, 2, ',', ' ') + '%');
                    } else if (tr.hasClass('critical-average-check')) {
                        tr.find('td.curr-amount > div').html(number_format(criticalAverageCheck, 2, ',', ' '));
                    }
                });
            },
        },
        Table2: {
            bindEvents: function()
            {
                // Takes money from input and modifies it in right float format.
                $('#break-even-table-2').on('paste change', '.custom-value', function (e) { // delete all non-number symbols
                    e.preventDefault();
                    const tr = $(this).closest('tr');
                    const value = e.type === 'paste'
                        ? (e.originalEvent || e).clipboardData.getData('text/plain')
                        : this.value;
                    this.value = number_format(value.sanitizeKub(), 2, ',', ' ');

                    BreakEven.Chart.updateCurrentMonth(tr);
                });
            },
            recalcCalculationColumns: function()
            {
                const table = $('#break-even-table-2 > tbody');
                $(table).find('tr').each(function(i,tr) {
                    tr = $(tr);
                    if (tr.hasClass('revenue')) {
                        BreakEven.Table2._calcRevenue(tr);
                    } else if (tr.hasClass('variable-costs')) {
                        BreakEven.Table2._calcVariableCosts(tr);
                    } else if (tr.hasClass('fixed-costs')) {
                        BreakEven.Table2._calcFixedCosts(tr);
                    } else if (tr.hasClass('marginal-income')) {
                        BreakEven.Table2._calcMarginalIncome(tr);
                    } else if (tr.hasClass('marginality')) {
                        BreakEven.Table2._calcMarginality(tr);
                    } else if (tr.hasClass('relative-variable-costs')) {
                        BreakEven.Table2._calcRelativeVariableCosts(tr);
                    } else if (tr.hasClass('relative-fixed-costs')) {
                        BreakEven.Table2._calcRelativeFixedCosts(tr);
                    } else if (tr.hasClass('all-costs')) {
                        BreakEven.Table2._calcAllCosts(tr);
                    } else if (tr.hasClass('relative-all-costs')) {
                        BreakEven.Table2._calcRelativeAllCosts(tr);
                    } else if (tr.hasClass('operating-profit')) {
                        BreakEven.Table2._calcOperatingProfit(tr);
                    } else if (tr.hasClass('relative-operating-profit')) {
                        BreakEven.Table2._calcRelativeOperatingProfit(tr);
                    } else if (tr.hasClass('efficiency')) {
                        BreakEven.Table2._calcEfficiency(tr);
                    }
                });
            },
            _calcRevenue: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const quantity = BreakEven.Chart.xCustomQuantity;
                const averageCheck = (quantity !== 0) ? revenue / quantity : 0;
                const calcPercent = 100;

                if (tr.hasClass('total')) {
                    tr.find('td.calc-amount > div').html(number_format(revenue, 2, ',', ' '));
                    tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
                } else if (tr.hasClass('quantity')) {
                    tr.find('td.calc-amount > div').html(number_format(quantity, 0, ',', ' '));
                } else if (tr.hasClass('average-check')) {
                    tr.find('td.calc-amount > div').html(number_format(averageCheck, 2, ',', ' '));
                }
            },
            _calcVariableCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const calcPercent = parseFloat(tr.find('td.curr-percent input').val().sanitizeKub());
                const calcAmount = (tr.hasClass('total')) ? (totalCosts - fixedCosts) : (calcPercent / 100 * revenue);

                tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcFixedCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yRevenue;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const calcPercent = parseFloat(tr.find('td.curr-percent input').val().sanitizeKub());
                const calcAmount = (tr.hasClass('total')) ? fixedCosts : (calcPercent / 100 * revenue);

                tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcMarginalIncome: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const calcAmount = (revenue - totalCosts + fixedCosts);
                const calcPercent = (revenue !== 0) ? (100 * calcAmount / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
                tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
            },
            _calcMarginality: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const marginalIncome = (revenue - totalCosts + fixedCosts);
                const calcAmount = (revenue !== 0) ? (100 * marginalIncome / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' ') + '%');
            },
            _calcRelativeVariableCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yRevenue;
                const variableCosts = BreakEven.Chart.yVariableCosts;
                const calcAmount = (revenue !== 0) ? (variableCosts / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcRelativeFixedCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const fixedCosts = BreakEven.Chart.yFixedCosts;
                const calcAmount = (revenue !== 0) ? (fixedCosts / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcAllCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const calcAmount = totalCosts;
                const calcPercent = (revenue !== 0) ? (100 * totalCosts / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
                tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
            },
            _calcRelativeAllCosts: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const calcAmount = (revenue !== 0) ? (totalCosts / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcOperatingProfit: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const calcAmount = revenue - totalCosts;
                const calcPercent = (revenue !== 0) ? (100 * (revenue - totalCosts) / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
                tr.find('td.calc-percent > div > span').html(number_format(calcPercent, 2, ',', ' '));
            },
            _calcRelativeOperatingProfit: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const calcAmount = (revenue !== 0) ? ((revenue - totalCosts) / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' '));
            },
            _calcEfficiency: function(tr)
            {
                const revenue = BreakEven.Chart.yCustomRevenue;
                const totalCosts = BreakEven.Chart.yCustomCosts;
                const calcAmount = (revenue !== 0) ? (100 * (revenue - totalCosts) / revenue) : 0;

                tr.find('td.calc-amount > div').html(number_format(calcAmount, 2, ',', ' ') + '%');
            },

            recalcVariationColumns: function()
            {
                const table = $('#break-even-table-2 > tbody');
                $(table).find('tr').each(function(i,tr) {
                    let factAmount = parseFloat($(tr).find('td.prev-amount div').text().trim().sanitizeKub()) || 0;
                    let planAmount =  parseFloat($(tr).find('td.calc-amount div').text().trim().sanitizeKub()) || 0;
                    let variationAmount = planAmount - factAmount;
                    let variationPercent = (factAmount > 0) ? ((planAmount - factAmount) / factAmount * 100) : 100;

                    $(tr).find('td.variation-amount > div').html(number_format(variationAmount, 2, ',', ' '));
                    $(tr).find('td.variation-percent > div > span').html(number_format(variationPercent, 2, ',', ' '));
                });
            },
        }
    };

    function nFormatter(num, digits) {

        let negative = false;
        if (num < 0) {
            num = Math.abs(num);
            negative = true;
        }

        let si = [
            { value: 1, symbol: "" },
            { value: 1E3, symbol: "K" },
            { value: 1E6, symbol: "M" },
        ];
        let rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        let i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }

        return (((negative) ? -1 : 1 ) * num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

</script>

<div class="row">
    <div class="col-8">
        <div class="row">
            <div class="col-4" style="padding:5px">
                <table class="table-bleak graph-break-even-table">
                    <tr>
                        <td>Выручка</td>
                        <td><i id="revenue_tip_1"><!-- js calc --></i> ₽</td>
                    </tr>
                </table>
            </div>
            <div class="col-4" style="padding:5px">
                <table class="table-bleak graph-break-even-table">
                    <tr>
                        <td>Суммарные затраты</td>
                        <td><i id="revenue_tip_2"><!-- js calc --></i> ₽</td>
                    </tr>
                </table>
            </div>
            <div class="col-4" style="padding:5px">
                <table class="table-bleak graph-break-even-table">
                    <tr>
                        <td>Операционная прибыль</td>
                        <td><i id="revenue_tip_3"><!-- js calc --></i> ₽</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
    <div class="col-4">

    </div>
</div>
<div class="row">
    <div class="col-8">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-break-even',
            'scripts' => [
                'themes/grid-light',
                'highcharts-more'
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'spacing' => [0,0,0,25],
                    //'animation' => false,
                    'events' => [
                        'load' => new JsExpression('
                            function(event) {
                                BreakEven.init();                            
                                BreakEven.Chart.draggablePlotLine(this.yAxis[0], "plotLineID");
                            }'
                        )
                    ],
                ],
                'title' => ['text' => ''],
                'exporting' => [
                    'enabled' => false,
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => 'rgba(255,255,255,1)',
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                            const thisPercent = (BreakEven.Chart.yrevenue !== 0) ? (this.y / BreakEven.Chart.yRevenue * 100) : 0;  
                            return '<span class=\"title\">' + this.series.name + '</span><br/>' +
                                '<table class=\"indicators\">' +
                                    '<tr>' + '<td class=\"gray-text\">Сумма: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>' +
                                    '<tr>' + '<td class=\"gray-text\">Доля в выручке: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(thisPercent, 0, ',', ' ') + ' %</td></tr>' +
                                '</table>';
                        }
                    "),
                    'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX,y:point.plotY - 60}; }'),
                ],
                'xAxis' => [
                    [
                        'min' => 0,
                    ]
                ],
                'yAxis' => [
                    'title' => '₽',
                    'min' => 0,
                    'plotLines' => [
                        [
                            'zIndex' => 4,
                            'color' => $color['plotLine'],
                            'width' => 2,
                            'value' => $total['revenue'],
                            'dashStyle' => null,
                            'id' => 'plotLineID',
                            //'onDragStart' => new JsExpression('function (new_Y) { BreakEven.Chart.onDragPointLine(new_Y); }'),
                            'onDragChange' => new JsExpression('function (new_Y) { BreakEven.Chart.onDragPointLine(new_Y, false); }'),
                            'onDragFinish' => new JsExpression('function (new_Y) { BreakEven.Chart.onDragPointLine(new_Y, true); }'),
                        ]
                    ],
                ],
                'series' => [
                    // POINTS
                    [
                        'name' => 'Точка безубыточности',
                        'data' => $pointBreakEven,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => $color['breakEven']
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    [
                        'name' => 'Выручка',
                        'data' => $pointRevenue,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => $color['revenue']
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    [
                        'name' => 'Суммарные затраты',
                        'data' => $pointTotalCosts,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => $color['totalCosts']
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    // LINES
                    [
                        'type' => 'line',
                        'name' => 'Выручка',
                        'data' => $revenue,
                        'color' => $color['revenue'],
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'zIndex' => 1,
                        'enableMouseTracking' => false
                    ],
                    [
                        'type' => 'line',
                        'name' => 'Суммарные затраты',
                        'data' => $totalCosts,
                        'color' => $color['totalCosts'],
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'zIndex' => 1,
                        'enableMouseTracking' => false
                    ],
                    [
                        'type' => 'line',
                        'name' => 'Фиксированные затраты',
                        'data' => $fixedCosts,
                        'color' => $color['fixedCosts'],
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'enableMouseTracking' => false,
                        'showInLegend' => false,
                    ],
                    // POLYGONS
                    [
                        'name' => 'Зона прибыли/убытка',
                        'data' => [
                            [$__pointBreakEvenX, $__pointBreakEvenY],
                            [$total['quantity'], $total['revenue']],
                            [$total['quantity'], $total['fixedCosts'] + $total['variableCosts']]
                        ],
                        'type' => 'polygon',
                        'color' => ($total['revenue'] >= $__pointBreakEvenY) ? $color['zoneProfit'] : $color['zoneLoss'],
                        'lineWidth' => 0.25,
                        'lineColor' => ($total['revenue'] >= $__pointBreakEvenY) ? $color['zoneProfit'] : $color['zoneLoss'],
                        'zIndex' => 0,
                        'marker' => [
                            'enabled' => false
                        ],
                        'enableMouseTracking' => false,
                        'showInLegend' => false,
                    ],
                ],
                'plotOptions' => [
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ]
                    ]
                //    'polygon' => [
                //        'animation' => [
                //            'duration' => 0,
                //        ]
                //    ]
                ]
            ],

        ])
        ?>
    </div>
    <div class="col-4">
        <div class="graph-break-even">
            <div class="part">
                <div class="value">
                    <?= ArrayHelper::getValue(AbstractFinance::$month, $searchModel->month) .' '. $searchModel->year ?> (факт)
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" width="12" height="12" style="margin-top: -2px">
                        <circle cx="60" cy="60" r="50" fill="<?=($color['revenue'])?>"/>
                        <circle cx="60" cy="60" r="25" fill="#FFF"/>
                    </svg>
                    <span class="ml-1">Выручка</span>
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($total['revenue']) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" width="12" height="12" style="margin-top: -2px">
                        <circle cx="60" cy="60" r="50" fill="<?=($color['totalCosts'])?>"/>
                        <circle cx="60" cy="60" r="25" fill="#FFF"/>
                    </svg>
                    <span class="ml-1">Суммарные затраты</span>
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($total['fixedCosts'] + $total['variableCosts']) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" width="12" height="12" style="margin-top: -2px">
                        <circle cx="60" cy="60" r="50" fill="<?=($color['breakEven'])?>"/>
                        <circle cx="60" cy="60" r="25" fill="#FFF"/>
                    </svg>
                    <span class="ml-1">Точка безубыточности</span>
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($__pointBreakEvenY) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    Запас прочности
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($total['revenue'] - $__pointBreakEvenY) ?> ₽ <!-- =(Выручка - Точка безубыточности) -->
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    Переменные затраты
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat(($total['revenue'] > 0) ? ($total['variableCosts'] / $total['revenue']) : 0, 2) ?> ₽ на 1 ₽ выручки
                </div>
            </div>
            <div class="part">
                <div class="caption" style="position:relative;">
                    <svg viewBox="0 0 240 120" version="1.1" xmlns="http://www.w3.org/2000/svg" width="24" height="12" style="margin-top: -2px">
                        <circle cx="120" cy="60" r="40" fill="<?=($color['fixedCosts'])?>" opacity="1" stroke-width="0"/>
                        <rect x="0" y="48" width="240" height="25" fill="<?=($color['fixedCosts'])?>" opacity="1"  stroke-width="0"/>
                    </svg>
                    <span class="ml-1">Фиксированные затраты</span>
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($total['fixedCosts']) ?> ₽
                </div>
            </div>
        </div>
    </div>
</div>