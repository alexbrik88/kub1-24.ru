<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use frontend\modules\reference\widgets\SummarySelectWidget;
use yii\widgets\Pjax;

$title = 'Детализация «Складские запасы»';

Pjax::begin([
    'id' => 'balance-store-grid-container',
    'enablePushState' => false,
    'scrollTo' => false
]);
?>

<div class="row align-items-center pb-1 flex-nowrap items-table-block">
    <div class="column">
        <h4 class="caption mb-2"><?= $title ?></h4>
    </div>
    <div class="column ml-auto">
        <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
            <span class="button-regular button-regular_red ml-2 mb-2 close-balance-item-detalization">Закрыть</span>
        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-compact table-style table-count-list invoice-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-left',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return Html::checkbox('ProductInitialBalance[' . $data['product_id'] . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'attribute' => 'product_name',
            'label' => 'Наименование',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'contentOptions' => [
                'class' => 'contact-cell'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return Html::tag('span', htmlspecialchars($data['product_name']), ['title' => $data['product_name']]);
            },
        ],
        [
            'attribute' => 'initial_quantity',
            'label' => 'Начальное кол-во',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function ($data) {
                $price = $data['initial_quantity'];
                return TextHelper::numberFormat($price, 3);
            },
        ],
        [
            'attribute' => 'initial_date',
            'label' => 'На дату',
            'format' => 'raw',
            'value' => function ($data) {
                return DateHelper::format($data['initial_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'initial_price',
            'label' => "Цена покупки",
            'format' => 'raw',
            'value' => function ($data) {
                $price = $data['initial_price'];
                return TextHelper::invoiceMoneyFormat($price, 2);
            },
        ],
        [
            'attribute' => 'initial_amount',
            'label' => "Начальный остаток",
            'format' => 'raw',
            'value' => function ($data) {
                return '<span class="price nowrap" data-price="' . TextHelper::moneyFormatFromIntToFloat($data['initial_amount']) . '">'
                    . TextHelper::invoiceMoneyFormat($data['initial_amount'], 2)
                    . '</span>';
            },
        ],
    ]
]) ?>

<?= SummarySelectWidget::widget(['buttons' => [
        Html::button('<span>Изменить дату</span>', [
            'id' => 'btn-copy-model',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-target' => '#modal-balance-store-initial-date',
        ])
]]) ?>

<?php Pjax::end() ?>