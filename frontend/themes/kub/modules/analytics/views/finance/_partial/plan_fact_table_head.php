<?php
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $searchModel OddsSearch */
/** @var $dMonthes array */
/** @var $cellIds array */
?>
<tr class="quarters-flow-of-funds">
    <th class="align-top" rowspan="2">
        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1">Статьи</span>
        </button>
    </th>
    <th class="align-top" <?= $dMonthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
        <button class="table-collapse-btn button-clr ml-1 <?= $dMonthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
        </button>
    </th>
    <th class="align-top" <?= $dMonthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
        <button class="table-collapse-btn button-clr ml-1 <?= $dMonthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
        </button>
    </th>
    <th class="align-top" <?= $dMonthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
        <button class="table-collapse-btn button-clr ml-1 <?= $dMonthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
        </button>
    </th>
    <th class="align-top" <?= $dMonthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
        <button class="table-collapse-btn button-clr ml-1 <?= $dMonthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
        </button>
    </th>
    <th class="align-top">
        <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
    </th>
</tr>
<tr>
    <?php foreach (AbstractFinance::$month as $key => $month): ?>
        <?php $quarter = (int)ceil($key / 3); ?>
        <th class="pl-2 pr-2 <?= $dMonthes[$quarter] ? '' : 'd-none' ?>"
            data-collapse-cell
            data-id="<?= $cellIds[$quarter] ?>"
            data-month="<?= $key; ?>"
        >
            <div class="pl-1 pr-1"><?= $month; ?></div>
        </th>
        <?php if ($key % 3 == 0): ?>
            <th class="pl-2 pr-2 <?= $dMonthes[$quarter] ? 'd-none' : '' ?>"
                data-collapse-cell-total
                data-id="<?= $cellIds[$quarter] ?>"
            >
                <div class="pl-1 pr-1">Итого</div>
            </th>
        <?php endif; ?>
    <?php endforeach; ?>
    <th class="align-top">
        <div class="pl-1 pr-1">Итого</div>
    </th>
</tr>

