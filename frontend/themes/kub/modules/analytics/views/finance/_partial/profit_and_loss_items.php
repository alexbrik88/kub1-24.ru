<?php
use frontend\rbac\permissions;

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

?>
<div class="items-table-block  pt-1 pb-1 hidden">
    <div class="row align-items-center flex-nowrap">
        <div class="column">
            <h4 class="caption mb-2">Детализация</h4>
        </div>
        <div class="column ml-auto">
            <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
                <a class="button-regular button-regular_red ml-2 mb-2 close-profit-and-loss-items" href="javascript:;">Закрыть</a>
            </div>
        </div>
    </div>
    <div id="custom-movement-type" class="row align-items-center pb-1 flex-nowrap">
        <div class="column w-100">
            <div class="nav-tabs-row mb-2 pb-1 w-100">
                <ul class="nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3">
                    <li class="custom-movement-type docs nav-item active" data-custom-movement-type="docs">
                        <a class="nav-link" href="javascript:void(0);">
                            Документы
                        </a>
                    </li>
                    <li class="custom-movement-type flows nav-item" data-custom-movement-type="flows">
                        <a class="nav-link" href="javascript:void(0);">
                            Деньги
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?= $this->render('profit_and_loss_items_flows', [
        'searchModel' => $searchModel,
    ]); ?>
    <?= $this->render('profit_and_loss_items_docs', [
        'searchModel' => $searchModel,
    ]); ?>
    <?= $this->render('_summary_select/_summary_select', [
        'dropdownButtons' => [
            $canUpdate ? \yii\helpers\Html::a('<span>Дата признания</span>', '#many-recognition-date', [
                'class' => 'dropdown-item dropdown-item-recognition-date',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
            $canUpdate ? \yii\helpers\Html::a('<span>Статья</span>', '#many-item', [
                'class' => 'dropdown-item',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
        ],
        'buttons' => [
            $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-pjax' => 0
            ]) : null,
        ],
    ]); ?>

</div>

