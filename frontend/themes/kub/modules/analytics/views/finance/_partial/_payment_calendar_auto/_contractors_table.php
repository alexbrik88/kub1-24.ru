<?php

use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashRule;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\PlanCashContractor;
use common\components\helpers\ArrayHelper;

/**
 * @var $planType int
 * @var $calendarSearchModel PaymentCalendarSearch
 * @var $incomeItemsArr array
 * @var $expenseItemsArr array
 */

if (!isset($contractorsToItemsIds)) $contractorsToItemsIds = [];
if (!isset($showAmountColumn)) $showAmountColumn = false;

$emptyMessage = 'Ничего не найдено';
?>

<?= Html::beginForm(null, 'get', [
    'id' => 'form_add_auto_plan_contractor' . ($planType == PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS ? '_prev' : ''),
    'class' => 'add-to-invoice',
    'data' => [
        'pjax' => true,
        'save-url' => Url::to(['/analytics/auto-plan-ajax/add-contractor', 'planType' => $planType, 'flowType' => $flowType])
    ]
]); ?>

    <?= \yii\bootstrap4\Html::radioList('flowType', $flowType, [
        CashFlowsBase::FLOW_TYPE_INCOME => 'Покупатели',
        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Поставщики',
    ], [
        'uncheck' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'class' => 'js_autoplan_contractor_flow_type_input',
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'class' => 'mb-3 mr-3 mt-2',
                ],
            ]);
        },
    ]); ?>

    <div class="search-form-default">
        <div class="pull-right" style="width: 100%">
            <div class="input-group">
                <div class="input-cont inp_pad" style="width: calc(100% - 95px)">
                    <?= Html::input('search', 'ContractorSimpleSearch[name]', $searchModel->title, [
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <span class="input-group-btn">
                <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                    'class' => 'mb-3 ml-1 button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]); ?>
            </span>
            </div>
        </div>
    </div>

    <div class="portlet m-b add-to-invoice-table" id="add-to-invoice-tbody">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'emptyText' => $emptyMessage,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout_no_scroll_modal', ['totalCount' => $dataProvider->totalCount]),

                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-autoplan-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::checkbox('Contractor[' . $model->id . '][checked]', false, [
                                    'class' => 'joint-autoplan-checkbox',
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Название',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '40%',
                            ],
                            'contentOptions' => [
                                'class' => 'contractor-cell'
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTitle(true);
                            },
                        ],
                        [
                            'attribute' => 'payment_delay',
                            'label' => 'Дней отсрочки',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return (int)$model->payment_delay;
                            },
                        ],
                        [
                            'attribute' => 'amount',
                            'label' => 'Плановая сумма ' . ($flowType == 0 ? ' расхода ' : ' прихода ') . 'в месяц',
                            'headerOptions' => [
                                'class' => 'sorting ' . (!$showAmountColumn ? 'hidden' : ''),
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell ' . (!$showAmountColumn ? 'hidden' : '')
                            ],
                            'format' => 'raw',
                            'value' => function ($model) use ($calendarSearchModel, $flowType, $showAmountColumn) {

                                $textAmount = '';
                                $inputAmount = '';

                                if ($showAmountColumn) {

                                    $amount = $calendarSearchModel->calculatePlanMonthAmount($flowType, null, null, $model->id);
                                    $textAmount .= TextHelper::invoiceMoneyFormat($amount, 2) . '<br/>';
                                    $inputAmount .= Html::hiddenInput("amountByContractor[{$model->id}]",  bcdiv((int)$amount, 100)) . "\n";
                                }

                                return $textAmount . "\n" . $inputAmount;
                            },
                        ],
                        [
                            'attribute' => ($planType == PlanCashContractor::PLAN_BY_FUTURE_INVOICES) ? 'item_id' : 'item_id_prev',
                            'label' => 'Статья ' . ($flowType == 0 ? 'расхода' : 'прихода'),
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '59%',
                            ],
                            'contentOptions' => [
                                'class' => ''
                            ],
                            'filter' => $searchModel->getArticleFilterByQuery($dataProvider->query),
                            'format' => 'raw',
                            'value' => function ($model) use ($incomeItemsArr, $expenseItemsArr) {

                                if ($model->type == Contractor::TYPE_CUSTOMER) {

                                    //if (!$model->invoice_income_item_id)
                                    //    $model->invoice_income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;

                                    return ArrayHelper::getValue($incomeItemsArr, $model->invoice_income_item_id, '--');

                                } else {

                                    if (!$model->invoice_expenditure_item_id)
                                        $model->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_PRODUCT;

                                    return ArrayHelper::getValue($expenseItemsArr, $model->invoice_expenditure_item_id, '--');

                                }
                            },
                            's2width' => '300px'
                        ]
                    ],
                ]); ?>
            </div>
            <span class="empty-choose-error" style="color:red; display: none">Необходимо выбрать</span>
        </div>
    </div>
<?= Html::endForm(); ?>

<div class="mt-3 d-flex justify-content-between">
    <?= \yii\helpers\Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'id' => 'add-contractors-to-autoplan' . ($planType == PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS ? '-prev' : ''),
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php /*
    <script type="text/javascript">
        $(document).on("change", "#cf_selected-all", function() {
            $("input.cf_selected").prop("checked", $(this).prop("checked")).trigger("change").uniform("refresh");
        });
    </script>
*/ ?>
