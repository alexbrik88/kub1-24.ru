<?php

use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\widgets\VideoInstructionWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Url;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\widgets\ActiveForm;
use frontend\widgets\TableConfigWidget;
use frontend\modules\analytics\assets\AnalyticsAsset;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $subTab string
 * @var $periodSize string
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */

AnalyticsAsset::register($this);

$this->title = 'Платежный календарь' . $searchModel->multiCompanyManager->getSubTitle();
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_pc_help ?? false;
$showChartPanel = $userConfig->report_pc_chart ?? false;

$subTabAutoplan = $subTabTable = $subTabRegister = false;
if ($subTab == 'register') {
    $subTabRegister = true;
} else if ($subTab == 'autoplan') {
    $subTabAutoplan = true;
} else {
    $subTabTable = true;
}

if (Yii::$app->request->get('show_add_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            let url = $(".add-operation").data("url");
            $("#add-movement").find(".modal-body").load(url);
            $("#add-movement").modal("show");
            window.history.pushState("object", document.title, location.href.split("?")[0]);
        });
    ');
}

if ($activeTab == $searchModel::TAB_CALENDAR) {
    $viewTable = '_partial/payment_calendar_calendar';
} elseif ($activeTab == $searchModel::TAB_BY_PURSE_DETAILED) {
    $viewTable = '_partial/payment_calendar_table_detailed';
} else {
    $viewTable = '_partial/payment_calendar_table';
}
?>

<input type="hidden" id="isAnalyticsModule" value="1"/>
<?php /*
<div class="mb-2 d-flex flex-wrap align-items-center">
    <button class="add-operation button-regular button-regular_red button-width ml-auto" data-toggle="modal" href="#add-movement"
            data-url="<?= Url::to(['create-plan-item', 'activeTab' => $activeTab, 'subTab' => $subTab, 'year' => $searchModel->year]) ?>">
        <?= \frontend\components\Icon::get('add-icon') ?>
        <span class="ml-2">Добавить</span>
    </button>
</div>*/ ?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <?= $this->render('@frontend/modules/analytics/views/multi-company/button') ?>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip2 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip2 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= VideoInstructionWidget::widget([
                    'title' => 'Видео инструкция по отчету',
                    'src' => 'https://www.youtube.com/embed/m41JTqE-C9E'
                ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['payment-calendar', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => [date('Y') + 1 => date('Y') + 1] + $searchModel->getRealFlowsYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_pc_chart">
    <div class="pt-4 pb-3">
        <?php
        // Chart
        echo $this->render('_charts/_chart_plan_fact_days', [
            'currYear' => $searchModel->year
        ]); ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_pc_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/payment-calendar-panel')
        ?>
    </div>
</div>

<?= $this->render('@frontend/modules/analytics/views/layouts/_payment_calendar_submenu', [
    'subTab' => $subTab
]) ?>

<?php if ($subTabTable || $subTabRegister): ?>
<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <?php if ($subTabRegister): ?>
        <div class="mr-1">
            <div class="mb-2">
                <?= TableConfigWidget::widget(['items' => [
                    ['attribute' => 'report_pc_priority'],
                    ['attribute' => 'report_pc_industry'],
                    ['attribute' => 'report_pc_sale_point'],
                    ['attribute' => 'report_pc_project'],
                    ['attribute' => 'report_pc_description'],
                ]]); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($subTabTable || $subTabRegister): ?>
    <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
        <div class="mr-1">
            <div class="mr-2">
                <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                    Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year, 'sub_tab' => $subTab]), [
                        'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="mr-1 mb-2">
        <div class="mr-2">
            <?= TableViewWidget::widget(['attribute' => 'table_view_finance_pc']) ?>
        </div>
    </div>
    <?php if ($subTabRegister): ?>
        <div class="mr-1">
            <div class="mr-2">
                <?= Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>' .
                    '<span>Заявка на платеж</span>', [
                    'class' => 'add-to-payments-request button-clr button-regular button-regular_padding_bigger button-regular_red mb-2',
                    'title' => 'Выберите расходную плановую операцию<br/> для создания заявки на платеж',
                    'title-as-html' => 1,
                    'disabled' => true
                ]); ?>
                <form id="form-add-to-payments-request" action="/analytics/plan-payments-request/create" method="POST">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>"/>
                </form>
            </div>
        </div>
    <?php endif; ?>
    <?php endif; ?>

    <?php if ($subTabTable): ?>
        <div class="mr-1">
            <div class="mr-2">
                <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>',
                    Url::current(($activeTab == PaymentCalendarSearch::TAB_CALENDAR) ?
                        ['activeTab' => Yii::$app->session->get('payment_calendar.activeTab',PaymentCalendarSearch::TAB_BY_ACTIVITY), 'PaymentCalendarSearch' => null] :
                        ['activeTab' => PaymentCalendarSearch::TAB_CALENDAR, 'PaymentCalendarSearch' => null]
                    ), [
                        'class' => 'button-regular pl-3 pr-3 mb-2' . ($activeTab == PaymentCalendarSearch::TAB_CALENDAR ? ' button-regular_red' : null),
                    ]); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($subTabRegister): ?>
        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'searchByContractor',
                'class' => 'col-6 ml-auto pr-0 mb-2 d-block',
            ],
            'method' => 'GET',
            'action' => Url::to(['payment-calendar', 'subTab' => 'register']),
            'enableClientScript' => false,
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
        ]); ?>

            <div class="d-flex flex-nowrap align-items-end">
                <div class="mr-2 flex-grow-0">
                    <?= $this->render('_partial/payment_calendar_filter', [
                        'form' => $form,
                        'model' => $searchModel
                    ]) ?>
                </div>
                <div class="mr-2 flex-grow-1">
                    <?= \yii\helpers\Html::activeTextInput($searchModel, 'contractor_name', [
                        'type' => 'search',
                        'placeholder' => 'Поиск по контрагенту',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="flex-grow-0">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            </div>

        <?php $form->end() ?>
    <?php endif; ?>

    <?php if ($subTabTable && $activeTab != PaymentCalendarSearch::TAB_CALENDAR): ?>
        <div class="d-flex flex-nowrap ml-auto">
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'days') ? 'button-regular_red' : '' ?> pl-4 pr-4 mb-0" href="<?= Url::current(['periodSize' => 'days']) ?>">День</a>
            </div>
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'months') ? 'button-regular_red' : '' ?> pl-3 pr-3 mb-0" href="<?= Url::current(['periodSize' => 'months']) ?>">Месяц</a>
            </div>
            <div class="radio_join mb-2 ml-2" style="width: 240px">
                <div class="ml-1">
                    <?= Select2::widget([
                        'id' => 'paymentcalendarsearch-tab',
                        'name' => 'paymentcalendarsearch-tab',
                        'value' => $activeTab,
                        'data' => [
                            PaymentCalendarSearch::TAB_BY_ACTIVITY => 'По видам деятельности',
                            PaymentCalendarSearch::TAB_BY_PURSE => 'По типам кошельков',
                            PaymentCalendarSearch::TAB_BY_PURSE_DETAILED => 'По кошелькам',
                            PaymentCalendarSearch::TAB_BY_INDUSTRY => 'По направлениям',
                            PaymentCalendarSearch::TAB_BY_SALE_POINT => 'По точкам продаж',
                        ],
                        'options' => [
                            'options' => [
                                PaymentCalendarSearch::TAB_BY_ACTIVITY => ['data-url' => Url::current(['activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY, 'PaymentCalendarSearch' => null])],
                                PaymentCalendarSearch::TAB_BY_PURSE => ['data-url' => Url::current(['activeTab' => PaymentCalendarSearch::TAB_BY_PURSE, 'PaymentCalendarSearch' => null])],
                                PaymentCalendarSearch::TAB_BY_PURSE_DETAILED => ['data-url' => Url::current(['activeTab' => PaymentCalendarSearch::TAB_BY_PURSE_DETAILED, 'PaymentCalendarSearch' => null])],
                                PaymentCalendarSearch::TAB_BY_INDUSTRY => ['data-url' => Url::current(['activeTab' => PaymentCalendarSearch::TAB_BY_INDUSTRY, 'PaymentCalendarSearch' => null])],
                                PaymentCalendarSearch::TAB_BY_SALE_POINT => ['data-url' => Url::current(['activeTab' => PaymentCalendarSearch::TAB_BY_SALE_POINT, 'PaymentCalendarSearch' => null])],
                            ],
                            'onchange' => new \yii\web\JsExpression('location.href = this.options[this.selectedIndex].dataset.url')
                        ],
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>
<?php endif; ?>

<?= Html::hiddenInput('activeTab', $activeTab, [
    'id' => 'active-tab_report',
]); ?>

<?php if (!$subTab || $subTab == 'table'): ?>
    <div class="wrap wrap_padding_none_all" style="margin-bottom: 12px; position: relative">
        <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
        <?= Html::hiddenInput('periodSize', $periodSize, ['id' => 'active-period-size']); ?>
        <?= $this->render($viewTable  . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'userConfig' => $userConfig,
            'data' => &$tableData['data'],
            'totalData' => &$tableData['totalData'],
            'growingData' => &$tableData['growingData'],
            'floorMap' => []
        ]) ?>
    </div>
<?php elseif ($subTab == 'register'): ?>
    <div class="wrap wrap_padding_none mb-2 pt-1" id="subtab-pc-register">
        <?= $this->render('_partial/payment_calendar_register', [
            'searchModel' => $searchModel,
            'itemsDataProvider' => $itemsDataProvider,
            'activeTab' => $activeTab,
            'subTab' => $subTab,
        ]) ?>
    </div>
<?php elseif ($subTab == 'autoplan'): ?>
    <?= $this->render('_partial/payment_calendar_auto', [
        'searchModel' => $searchModel,
        'itemsDataProvider' => $itemsDataProvider,
        'activeTab' => $activeTab,
        'subTab' => $subTab,
        'year' => $searchModel->year,
    ]) ?>
<?php endif; ?>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<?php if (!$subTab || $subTab == 'table'): ?>
    <?= $this->render('_partial/plan_item_table', [
        'searchModel' => $searchModel,
        'itemsDataProvider' => $itemsDataProvider,
        'activeTab' => $activeTab,
        'subTab' => $subTab
    ]); ?>
<?php endif; ?>

<?= $this->render('_partial/_payment_calendar/modals', ['searchModel' => $searchModel]) ?>
<?= $this->render('@frontend/modules/analytics/views/multi-company/modal') ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);
?>

<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial_operations_block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы)<br>
        в компанию и их возврат. <br>
        Выплата дивидендов.<br>
        Предоставление займов и депозитов.
    </span>
    <span id="tooltip_operation_activities_block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
    </span>
    <span id="tooltip_investment_activities_block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br>
        Затраты на новые проекты и поступление выручки от них. <br>
    </span>
    <span id="tooltip_net_cash_flow" style="display: inline-block; text-align: center;">
        "Чистый денежный поток" – это разница между всеми приходами<br/>
        и всеми расходами, за период указанный в столбце<br/>
        (день, месяц, квартал, год).
    </span>
</div>

<?php if ($activeTab != PaymentCalendarSearch::TAB_CALENDAR): ?>
<script>
    PlanItemTable = {
        init: function() {
            PlanItemTable.bindEvents();
        },
        bindEvents: function () {

            // delete
            $(document).on("click", ".modal-delete-plan-item .btn-confirm-yes", function() {
                let l = Ladda.create(this);
                l.start();

                $.get($(this).data('url'), null, function(data) {
                    PlanItemTable.reloadAll(function() {
                        Ladda.stopAll();
                        l.remove();
                        $('.modal:visible').modal('hide');
                        PlanItemTable.showFlash(data.msg);
                    });
                });

                return false;
            });

            // many delete
            $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                let $this = $(this);
                let l = Ladda.create(this);
                l.start();

                if (!$this.hasClass('clicked')) {
                    if ($('.joint-checkbox:checked').length > 0) {
                        $this.addClass('clicked');
                        $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {
                            PlanItemTable.reloadAll(function() {
                                Ladda.stopAll();
                                l.remove();
                                $('.modal:visible').modal('hide');
                                PlanItemTable.showFlash(data.msg);
                                $this.removeClass('clicked');
                            });
                        });
                    }
                }
                return false;
            });

            //update
            $(document).on('submit', '#js-plan_cash_flow_form', function(e) {
                e.preventDefault();

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    PlanItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        PlanItemTable.showFlash(data.msg);
                    });
                });

                return false;
            });

            // many item (update articles)
            $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                e.preventDefault();

                var l = Ladda.create($(this).find(".btn-save")[0]);
                var $hasError = false;

                l.start();
                $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                    if ($(this).find("select").val() == "") {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    PlanItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        PlanItemTable.showFlash(data.msg);
                        Ladda.stopAll();
                        l.remove();
                    });
                });

                return false;
            });

            // many item (update dates)
            $(document).on('submit', '#js-cash_flow_update_item_form-2', function(e) {
                e.preventDefault();

                var l = Ladda.create($(this).find(".btn-save")[0]);
                var $hasError = false;

                l.start();
                $(".modal-many-plan-date").find("[type='text']:visible").each(function () {
                    var parent = $(this).closest(".form-group");
                    $(parent).removeClass("has-error");
                    $(parent).find(".help-block").text("");
                    if (!$(this).val().match(/^\d{2}[./-]\d{2}[./-]\d{4}$/)) {
                        $hasError = true;
                        $(parent).addClass("has-error");
                        $(parent).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    PlanItemTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        PlanItemTable.showFlash(data.msg);
                        Ladda.stopAll();
                        l.remove();
                    });
                });

                return false;
            });

            // pjax reload
            $(document).on("pjax:complete", "#odds-items_pjax", function(e) {
                $('#summary-container').removeClass('visible check-true');
                if ($('#subtab-pc-register').length) {
                    $('#subtab-pc-register .custom-scroll-table').mCustomScrollbar({
                        horizontalScroll: true,
                        axis:"x",
                        scrollInertia: 300,
                        advanced:{
                            autoExpandHorizontalScroll: 3,
                            updateOnContentResize: true,
                            updateOnImageLoad: false
                        },
                        mouseWheel:{ enable: false },
                    });
                }
            });

            // many item
            $(document).on("shown.bs.modal", "#many-item", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);
                var $header = $modal.find(".modal-header h1");
                var $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>");
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                });

                $(".js-expenditure_item_id_wrapper, .js-income_item_id_wrapper").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                });
            });
            $(document).on("hidden.bs.modal", "#many-item", function () {
                $(".expenditure-item-block", this).addClass("hidden");
                $(".income-item-block", this).addClass("hidden");
                $(".additional-header-text", this).remove();
                $("#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
            });
            // many date
            $(document).on("shown.bs.modal", "#many-date", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);
                var $header = $modal.find(".modal-header h1");
                var $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>");
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form-2").prepend($(this).clone().hide());
                });

                $(".js-expenditure_item_id_wrapper, .js-income_item_id_wrapper").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                });
            });
            $(document).on("hidden.bs.modal", "#many-date", function () {
                $(".expenditure-item-block", this).addClass("hidden");
                $(".income-item-block", this).addClass("hidden");
                $(".additional-header-text", this).remove();
                $("#many-date .joint-checkbox").remove();
                $("#many-date [type='text']").val('');
            });            
            $("#paymentcalendarsearch-expenditureitemidmanyitem, #paymentcalendarsearch-incomeitemidmanyitem").on('change', function() {
                var wrap = $(this).closest('.form-group');
                $(wrap).removeClass("has-error");
                $(wrap).find(".help-block").text("");
            })
        },
        reloadAll: function(callback)
        {
            let debugCurrTime = Date.now();
            return PlanItemTable._reloadChart().done(function() {
                console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                PlanItemTable._reloadSubTable().done(function() {
                    console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                    if ($('#subtab-pc-register').length) {
                        if (typeof callback === "function") {
                            return callback();
                        }
                    }

                    PlanItemTable._reloadMainTable().done(function() {
                        console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        if (typeof callback === "function") {
                            return callback();
                        }
                    }).catch(function() { PlanItemTable.showFlash('Ошибка сервера #1') });
                }).catch(function() { PlanItemTable.showFlash('Ошибка сервера #2') });
            }).catch(function() { PlanItemTable.showFlash('Ошибка сервера #3') });
        },
        _reloadSubTable: function()
        {
            const $year = "PaymentCalendarSearch[year]=" + ($('#paymentcalendarsearch-year').length ? $('#paymentcalendarsearch-year').val() : '');
            const $contractorId = "PaymentCalendarSearch[contractor_id]=" + ($('#paymentcalendarsearch-contractor_id--filter').length ? $('#paymentcalendarsearch-contractor_id--filter').val() : '');
            const $contractorName = "PaymentCalendarSearch[contractor_name]=" + ($('#paymentcalendarsearch-contractor_name').length ? $('#paymentcalendarsearch-contractor_name').val() : '');
            const $statusType = "PaymentCalendarSearch[status_type]=" + ($('#paymentcalendarsearch-status_type--filter').length ? $('#paymentcalendarsearch-status_type--filter').val() : '');
            const $paymentType = "PaymentCalendarSearch[payment_type]=" + ($('#paymentcalendarsearch-payment_type--filter').length ? $('#paymentcalendarsearch-payment_type--filter').val() : '');
            const $priorityType = "PaymentCalendarSearch[priority_type]=" + ($('#paymentcalendarsearch-priority_type--filter').length ? $('#paymentcalendarsearch-priority_type--filter').val() : '');

            $itemIDs['activeTab'] = $('#active-tab_report').val(); // todo: var from custom.js

            return jQuery.pjax({
                url: "/analytics/finance/item-list?" + $year + '&' + $contractorId + '&' + $contractorName + '&' + $statusType + '&' + $paymentType + '&' + $priorityType,
                type: 'POST',
                data: {'items': JSON.stringify($itemIDs)},
                container: '#odds-items_pjax',
                timeout: 10000,
                push: false,
                scrollTo: false
            });
        },
        _reloadMainTable: function ()
        {
            let floorMap = {};
            $('table.flow-of-funds').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-days]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            let data = {
                floorMap: floorMap
            };
            let activeTab = $('#active-tab_report').val();
            let periodSize = $('#active-period-size').val();

            return $.post('/analytics/finance-ajax/payment-calendar-part/?activeTab=' + activeTab + '&periodSize=' + periodSize, data, function ($data) {
                let html = new DOMParser().parseFromString($data, "text/html");
                let table = html.querySelector('table.flow-of-funds tbody');
                let origTable = document.querySelector('table.flow-of-funds tbody');
                origTable.innerHTML = table.innerHTML;

                if ($coloredItemsCoords) {
                    $.each($coloredItemsCoords, function(i,v) {
                        $('table.flow-of-funds tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                    });
                }

                PlanItemTable._bindMainTableEvents();
            });
        },
        _reloadChart: function() {
            return window.ChartPlanFactDays.redrawByClick();
        },
        _bindMainTableEvents: function() {

            // main.js
            $('[data-collapse-row-trigger]', 'table.flow-of-funds tbody').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    // level 1
                    $('[data-id="'+target+'"]').addClass('d-none');
                    $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        // level 2
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                    $('[data-collapse-all-trigger]').removeClass('active');
                } else {
                    $('[data-collapse-all-trigger]').addClass('active');
                }
            });
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    /////////////////////
    PlanItemTable.init();
    /////////////////////
</script>
<?php endif; ?>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>

<?php if ($periodSize == 'days') {
$this->registerJs(<<<JS

    $("[data-collapse-trigger-days]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 250);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).data('columns-count') || 3;

            $(collapseBtn).toggleClass('active');
            $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            if ( $(collapseBtn).hasClass('active') ) {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
            } else {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
            }
            $(collapseBtn).closest('.custom-scroll-table').mCustomScrollbar("update");
        };

        _collapseToggle(collapseBtn);

    });


    $(document).ready(function() {
        SimpleDoubleScroll.init({
            selectorTop: '#sds-top',
            selectorBottom: '#sds-bottom',
            fixedColumnWidth: 250,
            refreshByClick: ['.table-collapse-btn']
        });
    });
JS
);
} ?>

<?php
$this->registerJs(<<<JS

    CashFilter = 
    {
        pop: null,
        init: function() {
            this.pop = $(".cash-filter");
            this.bindEvents();
        },
        bindEvents: function() {
            const that = this;
            that.pop.find("input:radio").on("change", function(e) {
                e.preventDefault();
                $(this).parents(".dropdown-drop").find("a > span").html($(this).parents("label").text());
                $(this).parents(".dropdown-drop-menu, .dropdown-drop").removeClass("visible show");                
                that._checkSubmitColor();
            });
            that.pop.find(".popup-dropdown-in").on("click", function(e) {
                e.stopPropagation();
                $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop.visible.show").removeClass("visible show");
                $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop-menu.visible.show").removeClass("visible show");
            });
            that.pop.find(".cash_filters_reset").on("click", function() {
                $(this).parents(".dropdown").find(".dropdown-drop-in").each(function() {
                    $(this).find("input:radio").first().prop("checked", true);
                    $(this).find("input:radio").uniform();
                    $(this).parents(".dropdown-drop").find("a > span").html($(this).find("input:radio").first().parents("label").text());
                });
                that._checkSubmitColor();
            });
        },
        _checkSubmitColor: function() {
            const that = this;
            const submit = that.pop.find("[type=submit]");
            let filter_on = false;
            that.pop.find(".form-filter-list").each(function() {
                var a_val = $(this).find("input").filter("[data-default=1]").val();
                var i_val = $(this).find("input:checked").val();
                if (i_val === undefined) {
                    i_val = $(this).find("label:first-child").find("input").val();
                }
                if (a_val === undefined) {
                    a_val = $(this).find("label:first-child").find("input").val();
                }
                if (a_val !== i_val) {
                    filter_on = true;
                    return false;
                }
            });
        
            if (filter_on) {
                submit.addClass("button-regular_red").removeClass("button-hover-content-red");
            } else {
                submit.removeClass("button-regular_red").addClass("button-hover-content-red");
            }
        }
    };

    $(document).ready(function() {
        CashFilter.init();
    });
    
    // SEARCH
    $("input#paymentcalendarsearch-contractor_name").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $("#searchByContractor").submit();
      }
    });
    
    // PAYMENTS REQUEST
    $(document).on("change", "input.joint-checkbox", function() {
        const btn = $(".add-to-payments-request");
        const hasExpenses = $("input.joint-checkbox.expense-item:checked").length;
        $(btn).attr("disabled", !hasExpenses);
        $(btn).tooltipster(hasExpenses ? "disable" : "enable");
    });
    
    $(document).on("click", ".add-to-payments-request", function() {
        const form = $('#form-add-to-payments-request');
        const expenseFlows = $("input.joint-checkbox.expense-item:checked");
        $(expenseFlows).each(function(i, flow) {
            $(form).append($('<input>', {
                'name': 'fromFlows[]',
                'value': flow.value,
                'type': 'hidden'
            }));
        });

        form.submit();
    });
JS
);
?>
