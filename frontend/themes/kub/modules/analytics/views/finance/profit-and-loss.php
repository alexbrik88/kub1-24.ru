<?php
/**
 * Created by PhpStorm.
 * User: maxfouri
 * Date: 15.03.2021
 */

use common\components\helpers\Html;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\project\Project;
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\assets\AnalyticsAsset;

AnalyticsAsset::register($this);

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel ProfitAndLossSearchModel
 * @var $data array
 * @var $activeTab int
 */

$title = 'Отчет о Прибылях и Убытках' . ($searchModel->multiCompanyManager->getIsModeOn() ?
    $searchModel->multiCompanyManager->getSubTitle() : " для {$searchModel->company->companyTaxationType->getName()}");

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_profit_loss_help ?? false;
$showChartPanel = $userConfig->report_profit_loss_chart ?? false;
$hideSecondLine = $hideSecondLine ?? false;

$pageFilter = $pageFilter ?? [];
$show = $show ?? [];

$PAGE_FILTER = [
    'project_id' => $pageFilter['project_id'] ?? null,
    'industry_id' => $pageFilter['industry_id'] ?? null,
    'sale_point_id' => $pageFilter['sale_point_id'] ?? null,
];

$SHOW = [
    'text' => [
        'title' => $title
    ],
    'block' => [
        'header' => 1,
        'body' => 1,
        'footer' => 1
    ],
    'button' => [
        'multicompany' => 1,
        'chart' => 1,
        'help' => 1,
        'video' => 1,
        'options' => 1,
        'excel' => 1,
        'table_view' => 1,
        'month_group_by' => 1
    ],
    'modal_options' => [
        'part_articles' => 1,
        'part_additional' => 1,
        'part_revenue_analytics' => 1,
        'part_bottom_text' => 1
    ],
    'select' => [
        'year' => 1
    ]
];

foreach ($show as $elementType => $options)
    foreach ($options as $optKey => $optVal)
        $SHOW[$elementType][$optKey] = $optVal;

$this->title = $SHOW['text']['title'];

$monthGroupBy = $searchModel->getUserOption('monthGroupBy');

// todo: remove in class
$groupByItemsRadio = [
    AnalyticsArticleForm::MONTH_GROUP_UNSET => [
        'itemsExists' => true,
        'itemsCreationModal' => null,
    ],
    AnalyticsArticleForm::MONTH_GROUP_INDUSTRY => [
        'itemsExists' => ($SHOW['button']['month_group_by'])
            ? CompanyIndustry::find()->where(['company_id' => $searchModel->multiCompanyIds])->exists()
            : true,
        'itemsCreationModal' => '#industry-creation-modal'
    ],
    AnalyticsArticleForm::MONTH_GROUP_SALE_POINT => [
        'itemsExists' => ($SHOW['button']['month_group_by'])
            ? SalePoint::find()->where(['company_id' => $searchModel->multiCompanyIds])->exists()
            : true,
        'itemsCreationModal' => '#sale-point-creation-modal'
    ],
    AnalyticsArticleForm::MONTH_GROUP_PROJECT => [
        'itemsExists' => ($SHOW['button']['month_group_by'])
            ? Project::find()->where(['company_id' => $searchModel->multiCompanyIds])->exists()
            : true,
        'itemsCreationModal' => '#project-creation-modal'
    ],
];
?>

<?php if ($SHOW['block']['header']): ?>
    <div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="column mr-auto">
                    <h4 class="mb-2"><?= $title ?></h4>
                </div>
                <?php if ($SHOW['button']['multicompany']): ?>
                    <?= $this->render('@frontend/modules/analytics/views/multi-company/button', ['temporaryDisabled' => false]) ?>
                <?php endif; ?>
                <?php if ($SHOW['button']['chart']): ?>
                    <div class="column pl-1 pr-2">
                        <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                            [
                                'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#chartCollapse',
                            ]) ?>
                    </div>
                <?php endif; ?>
                <?php if ($SHOW['button']['help']): ?>
                    <div class="column pl-1 pr-2">
                        <?= Html::button(Icon::get('book'),
                            [
                                'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#helpCollapse',
                            ]) ?>
                    </div>
                <?php endif; ?>
                <?php if ($SHOW['select']['year']): ?>
                <div class="column pl-1 pr-0" style="margin-top:-9px; max-height: 44px">
                    <?= \yii\bootstrap\Html::beginForm('', 'GET', [
                        'validateOnChange' => true,
                    ]); ?>
                    <?= Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'year',
                        'data' => $searchModel->getYearFilter(),
                        'hideSearch' => true,
                        'options' => [
                            'id' => 'profitandlosssearchmodel-year',
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                    ]); ?>
                    <?= Html::endForm(); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_profit_loss_chart">
        <div class="pt-3 pb-3">
            <?php
            echo $this->render('_charts/_chart_profit_loss', [
                'model' => $searchModel,
                'PAGE_FILTER' => $PAGE_FILTER,
                'hideSecondLine' =>  $hideSecondLine
            ]);
            ?>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_profit_loss_help">
        <div class="pt-4 pb-3">
            <?php
            echo $this->render('_partial/profit_and_loss_panel')
            ?>
        </div>
    </div>
<?php endif; ?>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <?php if ($SHOW['button']['options']): ?>
        <div class="ml-0 mr-2 pr-1">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>',
                '#profit-and-loss-options', [
                    'class' => 'button-list button-hover-transparent button-clr mb-2 disabled',
                    'data-toggle' => 'modal',
                    'title' => 'Настройки',
                ]); ?>
        </div>
    <?php endif; ?>
    <?php if ($SHOW['button']['excel']): ?>
        <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
            <div class="mr-2 pr-1">
                <?= \yii\helpers\Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                    Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                        'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($SHOW['button']['table_view']): ?>
        <div class="mr-2 pr-1">
            <div class="mb-2">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_balance']) ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($SHOW['button']['month_group_by']): ?>
        <div class="mr-2 pr-1 dropdown dropdown-settings">
            <div class="mb-2">
                <?= Html::button(Icon::get('pie-pal'), [
                    'class' => 'button-list button-hover-transparent button-clr',
                    'data-toggle' => 'dropdown',
                    'aria-haspopup' => 'true',
                    'aria-expanded' => 'false',
                    'title' => 'Детализация'
                ]) ?>
                <ul class="dropdown-popup dropdown-popup-settings dropdown-menu" role="menu" style="padding: 10px 15px;">
                    <li class="mb-2">
                        <label>Детализация</label>
                    </li>
                    <?= Html::radioList('_monthGroupBy', $monthGroupBy, AnalyticsArticleForm::$monthGroups, [
                        'encode' => false,
                        'item' => function ($index, $label, $name, $checked, $value) use ($groupByItemsRadio) {
                            $input = Html::radio($name, $checked, [
                                'value' => $value,
                                'label' => Html::tag('span', $label, [
                                    'class' => 'nowrap',
                                    'style' => 'font-weight: bold;',
                                ]),
                                'data-has-items' => $groupByItemsRadio[$index]['itemsExists'] ? 1 : 0,
                                'data-creation-modal' => $groupByItemsRadio[$index]['itemsCreationModal'],
                                'data-change-url' => Url::current(['AnalyticsArticleForm[monthGroupBy]' => $index]),
                                'onchange' => new \yii\web\JsExpression('const el = $(this); if (el.data("has-items")) {location.href = el.data("change-url")} else { $(el.data("creation-modal")).modal()}')
                            ]);

                            return Html::tag('li', $input);
                        },
                    ]); ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
    <div class="column mb-2 mr-0 pr-0 ml-auto" style="width: 270px">
        <div class="ml-1" style="max-height: 44px">
            <?= Select2::widget([
                'id' => 'income-tab',
                'name' => 'income-tab',
                'value' => $activeTab,
                'data' => [
                    ProfitAndLossSearchModel::TAB_ALL_OPERATIONS => 'Все операции',
                    ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY => 'Для бухгалтерии',
                ],
                'options' => [
                    'options' => [
                        ProfitAndLossSearchModel::TAB_ALL_OPERATIONS => [
                            'data-url' => Url::current([
                                'activeTab' => ProfitAndLossSearchModel::TAB_ALL_OPERATIONS,
                                'ProfitAndLossSearchModel' => null
                            ])
                        ],
                        ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY => [
                            'data-url' => Url::current([
                                'activeTab' => ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY,
                                'ProfitAndLossSearchModel' => null
                            ])
                        ],
                    ],
                    'onchange' => new \yii\web\JsExpression('location.href = this.options[this.selectedIndex].dataset.url')
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </div>
    <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?= $this->render('_partial/profit_and_loss_table', [
        'searchModel' => $searchModel,
        'data' => $data,
        'activeTab' => $activeTab
    ]); ?>
</div>

<?= $this->render('_partial/profit_and_loss_items', [
    'searchModel' => $searchModel
]) ?>

<?= $this->render('_partial/_item_table/modals', ['searchModel' => $searchModel]) ?>
<?= $this->render('@frontend/modules/analytics/views/multi-company/modal') ?>
<?= $this->render('_partial/profit_and_loss_options_modal', ['show' => $SHOW['modal_options']]) ?>
<?= $this->render('_partial/profit_and_loss_additional_modal') ?>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<?php
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$this->registerJs(<<<JS
  
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
JS);
?>

<script>

    ProfitAndLoss = {
        pageFilter: <?= json_encode($PAGE_FILTER) ?>,
        tdFilter: {
            'industry_id': null,
            'sale_point_id': null,
            'project_id': null
        },
        year: <?= $searchModel->year ?>,
        month: null,
        quarter: null,
        article: null,
        hoverText: null,
        movementType: null,
        customMovementType: "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS ?>",
        accountingOperationsOnly: <?= $activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY ? 1:0 ?>,
        _alertEmptyTab: false,
        _enableTabsAutoToggle: false,
        init: function() {
            this.bindEvents();
            this.bindModalEvents();
            this.addListeners();
        },
        bindEvents: function() {

            // main table events
            ProfitAndLoss._bindMainTableEvents();

            // open items table
            $(document).on('click', '.scrollable-table-content > table.flow-of-funds td.can-click', function () {

                ProfitAndLoss.year = $('#profitandlosssearchmodel-year').val();
                ProfitAndLoss.quarter = $(this).data('quarter');
                ProfitAndLoss.month = $(this).data('month');
                ProfitAndLoss.article = $(this).data('article');
                ProfitAndLoss.hoverText = $(this).data('hover_text');
                ProfitAndLoss.movementType = $(this).data('movement-type');
                ProfitAndLoss._alertEmptyTab = false;
                ProfitAndLoss._enableTabsAutoToggle = true;

                if (ProfitAndLoss.hoverText) {
                    ProfitAndLoss._setCellColor(this);
                    ProfitAndLoss._setTableNameByCell(this);
                    ProfitAndLoss._showHoverText();
                    return false;
                }

                if (!ProfitAndLoss.article || '0,00' === $(this).text().trim()) {
                    ProfitAndLoss._showFlash('Нет данных');
                    return false;
                }

                if ($(this).hasClass('td-filter')) {
                    ProfitAndLoss._setTdFilter(this);
                } else {
                    ProfitAndLoss._resetTdFilter();
                }

                ProfitAndLoss._setCellColor(this);
                ProfitAndLoss._setTableNameByCell(this);
                ProfitAndLoss._showPreloader();
                ProfitAndLoss.refresh();
            });

            // change custom movement type
            $('li.custom-movement-type').on('click', function() {

                if ($(this).hasClass('disabled'))
                    return false;

                var $block = $(this).closest('ul');
                $block.find('li').removeClass('active');
                $(this).addClass('active');
                ProfitAndLoss.customMovementType = $(this).data('custom-movement-type');
                ProfitAndLoss._alertEmptyTab = true;
                ProfitAndLoss._enableTabsAutoToggle = false;
                ProfitAndLoss.refresh();
            });

            // close items table
            $(".close-profit-and-loss-items").on("click", function () {
                let $block = $(".items-table-block");
                $block.find(".caption").html("Детализация");
                $block.find(".wrap_padding_none").html("");
                $block.addClass("hidden");
                $('.scrollable-table-content > table.flow-of-funds td.hover-checked').removeClass('hover-checked');
            });
        },
        bindModalEvents: function() {

            //update flow
            $(document).on('submit', '#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form, #operationUpdateForm', function(e) {
                e.preventDefault();

                $(this).prepend('<input type="hidden" name="fromOdds" value="1">');

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    ProfitAndLoss.reloadAll(function() {
                        Ladda.stopAll();
                        $('.modal:visible').modal('hide');
                        ProfitAndLoss._showFlash('Операция обновлена.'); // todo: show errors
                    });
                });

                return false;
            });

            // delete flow
            $(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
                let l = Ladda.create(this);
                l.start();

                $.get($(this).data('url'), null, function(data) {
                    ProfitAndLoss.reloadAll(function() {
                        Ladda.stopAll();
                        // l.remove();
                        $('.modal:visible').modal('hide');
                        ProfitAndLoss._showFlash(data.msg);
                    });
                });

                return false;
            });

            // many delete
            $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                let $this = $(this);
                let l = Ladda.create(this);
                let checkedCheckboxes = $('.joint-checkbox:checked');
                let customMovementType = $('.custom-movement-type.active').data('custom-movement-type');
                let deleteUrl = (customMovementType === 'docs') ? '/analytics/finance-ajax/many-delete-doc-item' : '/analytics/finance-ajax/many-delete-flow-item';
                l.start();

                if (!$this.hasClass('clicked')) {
                    if ($(checkedCheckboxes).length > 0) {
                        $this.addClass('clicked');
                        $.post(deleteUrl, $(checkedCheckboxes).serialize(), function(data) {
                            ProfitAndLoss.reloadAll(function() {
                                Ladda.stopAll();
                                // l.remove();
                                $('.modal:visible').modal('hide');
                                ProfitAndLoss._showFlash(data.msg);
                                $this.removeClass('clicked');
                            });
                        });
                    }
                }
                return false;
            });

            // many item (update articles)
            $(document).on("shown.bs.modal", "#many-item", function () {

                let checkedCheckboxes = $('.joint-checkbox:checked');
                let $includeExpenditureItem = $(checkedCheckboxes).filter('.expense-item').length > 0;
                let $includeIncomeItem = $(checkedCheckboxes).filter('.income-item').length > 0;
                let $modal = $(this);
                let $header = $modal.find(".modal-header h1");
                let $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                });
            });

            // many item (update articles)
            $(document).on("hidden.bs.modal", "#many-item", function () {
                $(".expenditure-item-block").addClass("hidden");
                $(".income-item-block").addClass("hidden");
                $(".additional-header-text").remove();
                $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
            });

            // many item (update articles)
            $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                e.preventDefault();

                let l = Ladda.create($(this).find(".btn-save")[0]);
                let $hasError = false;

                l.start();
                $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                    if ($(this).find("select").val() == "") {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                let customMovementType = $('.custom-movement-type.active').data('custom-movement-type');
                let updateUrl = (customMovementType === 'docs') ? '/analytics/finance-ajax/many-doc-item' : '/analytics/finance-ajax/many-flow-item';

                $.post(updateUrl, $(this).serialize(), function (data) {
                    ProfitAndLoss.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ProfitAndLoss._showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            // many recognition date
            $(document).on("shown.bs.modal", "#many-recognition-date", function () {

                let checkedCheckboxes = $('.joint-checkbox:checked');
                let $includeExpenditureItem = $(checkedCheckboxes).filter('.expense-item').length > 0;
                let $includeIncomeItem = $(checkedCheckboxes).filter('.income-item').length > 0;
                let $modal = $(this);
                let $header = $modal.find("h4.modal-title");

                if ($includeExpenditureItem) {
                    $header.html('Изменить дату признания расхода');
                } else if ($includeIncomeItem) {
                    $header.html('Изменить дату признания дохода');
                }

                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form").prepend($(this).clone().hide());
                });
            });

            // many recognition date
            $(document).on("hidden.bs.modal", "#many-recognition-date", function () {
                $(".additional-header-text").remove();
                $(".modal#many-recognition-date form .joint-checkbox").remove();
            });

            // many recognition date (update dates)
            $(document).on('submit', '#js-cash_flow_update_date_form', function(e) {

                e.preventDefault();

                let l = Ladda.create($(this).find(".btn-save")[0]);
                let $hasError = false;

                l.start();
                $(".js-recognition_date_wrapper:visible").each(function () {

                    let input = $(this).find("input").val();

                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");

                    if (!input.match(/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1-9]|1[0-2])\.(20[0-9][0-9])$/)) {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    ProfitAndLoss.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ProfitAndLoss._showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            // many recognition date (title)
            $(document).on('change', '.joint-checkbox', function() {
                const customMovementType = $('.custom-movement-type.active').data('custom-movement-type');
                if (customMovementType == 'docs') {
                    $('#summary-container').find('.dropdown-item-recognition-date').hide();
                } else {
                    $('#summary-container').find('.dropdown-item-recognition-date').html($(this).hasClass('income-item') ? 'Дата признания дохода' : 'Дата признания расхода');
                    $('#summary-container').find('.dropdown-item-recognition-date').show();
                }
            });
        },
        addListeners: function() {
            const that = this;
            // pjax complete
            $('#flow-items-pjax').on("pjax:complete", function () {
                ProfitAndLoss._hidePreloader();
                ProfitAndLoss._setDocsFlowsMenu();
                $('#doc-items-pjax').html('');
                $('#summary-container').removeClass('visible check-true');
                $('.items-table-block').removeClass('hidden');
                $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");
            });
            // pjax complete
            $('#doc-items-pjax').on("pjax:complete", function () {
                ProfitAndLoss._hidePreloader();
                ProfitAndLoss._setDocsFlowsMenu();
                $('#flow-items-pjax').html('');
                $('#summary-container').removeClass('visible check-true');
                $('.items-table-block').removeClass('hidden');
                $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");
            });
        },
        refresh: function() {
            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_FLOWS ?>") {
                $('#custom-movement-type').hide();
                ProfitAndLoss._getItems("#flow-items-pjax", ProfitAndLoss.getItemsUrl(ProfitAndLoss.getFlowsUrlData()));
            }
            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS ?>") {
                $('#custom-movement-type').hide();
                ProfitAndLoss._getItems("#doc-items-pjax", ProfitAndLoss.getItemsUrl(ProfitAndLoss.getDocsUrlData()));
            }
            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS_AND_FLOWS ?>") {
                $('#custom-movement-type').show();
                if (ProfitAndLoss.customMovementType === 'docs') {
                    ProfitAndLoss._getItems("#doc-items-pjax", ProfitAndLoss.getItemsUrl(ProfitAndLoss.getDocsUrlData()));
                } else {
                    ProfitAndLoss._getItems("#flow-items-pjax", ProfitAndLoss.getItemsUrl(ProfitAndLoss.getFlowsUrlData()));
                }
            }
        },
        getFlowsUrlData: function() {
            return {
                'searchBy': 'flows',
                'pageFilter': ProfitAndLoss.pageFilter,
                'tdFilter': ProfitAndLoss.tdFilter,
                'year':    ProfitAndLoss.year,
                'quarter': ProfitAndLoss.quarter,
                'month':   ProfitAndLoss.month,
                'article': ProfitAndLoss.article,
                'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                'enableToggle': ProfitAndLoss._enableTabsAutoToggle ? 1 : 0
            };
        },
        getDocsUrlData: function() {
            return {
                'searchBy': 'docs',
                'pageFilter': ProfitAndLoss.pageFilter,
                'tdFilter': ProfitAndLoss.tdFilter,
                'year':    ProfitAndLoss.year,
                'quarter': ProfitAndLoss.quarter,
                'month':   ProfitAndLoss.month,
                'article': ProfitAndLoss.article,
                'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                'enableToggle': ProfitAndLoss._enableTabsAutoToggle ? 1 : 0
            };
        },
        getItemsUrl: function(data) {
            var a = document.createElement("a");
            a.href = '/analytics/finance-ajax/get-profit-and-loss-items';
            var queryParams = new URLSearchParams();
            for (let key in data) {
                if (data.hasOwnProperty(key)) {
                    let val = data[key];
                    if (typeof val === 'object') {
                        for (let subKey in val) {
                            if (val.hasOwnProperty(subKey)) {
                                queryParams.set(key+'['+subKey+']', val[subKey] || '');
                            }
                        }
                    } else {
                        if (val !== null) {
                            queryParams.set(key, val);
                        } else {
                            queryParams.delete(key);
                        }
                    }
                }
            }
            a.search = queryParams.toString();

            return a.href;
        },
        _getItems: function(pjaxContainer, url) {
            $.pjax({
                container: pjaxContainer,
                url: url,
                timeout: 5000,
                push: false
            }).fail(function () {
                ProfitAndLoss._hidePreloader();
            });
        },
        _showPreloader: function() {
            $('#hellopreloader').show();
            $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
        },
        _hidePreloader: function() {
            let hellopreloader = document.getElementById("hellopreloader_preload");
            fadeOutPreloader(hellopreloader);
        },
        _setCellColor: function(td) {
            $('.scrollable-table-content > table.flow-of-funds td.hover-checked').removeClass('hover-checked');
            let $i = 0;
            if ($(td).hasClass('quarter-block')) {
                let $prevTd = $(td).prev('td');
                do {
                    $prevTd.addClass('hover-checked');
                    $prevTd = $prevTd.prev('td');
                    $i++
                }
                while ($i < 3);
            }
            $(td).addClass('hover-checked');
        },
        _setTableNameByCell: function(td) {
            const monthName = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
            let year = $('#profitandlosssearchmodel-year').val();
            let quarter = $(td).data('quarter');
            let month = $(td).data('month');
            let articleName = $(td).closest('tr').find('td').first().text().trim();
            let result = 'Детализация' +
                (articleName ? (' "' + articleName + '" за ') : '') +
                (quarter ? (quarter + ' кв.') : '') +
                (month ? monthName[month - 1] : '') +
                (year ? (' ' + year + 'г.') : '');

            $('.items-table-block h4').html(result);
        },
        _setDocsFlowsMenu: function() {

            let searchBy = $('#searchBy');
            let changedSearchBy = $('#isSearchByChanged');
            let customMovementType = $('.custom-movement-type');

            if (searchBy.length) {

                let activeLiClass = ($(searchBy).val() === 'flows') ? '.flows' : '.docs';
                let disabledLiClass = ($(searchBy).val() === 'flows') ? '.docs' : '.flows';

                //$(customMovementType).removeClass('disabled');

                if ($(changedSearchBy).length) {
                    $(customMovementType).filter(activeLiClass).addClass('active');
                    $(customMovementType).filter(disabledLiClass).removeClass('active');
                    $(changedSearchBy).remove();
                    $(searchBy).remove();

                    //if (ProfitAndLoss._alertEmptyTab) {
                    //    ProfitAndLoss._showFlash('Нет данных в разделе "' + ($(searchBy).val() === 'flows' ? 'Документы' : 'Деньги') + '"');
                    //    ProfitAndLoss._alertEmptyTab = false;
                    //}

                } else {
                    $(customMovementType).filter(activeLiClass).addClass('active');
                    $(customMovementType).filter(disabledLiClass).removeClass('active');
                    $(searchBy).remove();
                }
            }
        },
        reloadAll: function(callback)
        {
            let debugCurrTime = Date.now();
            return ProfitAndLoss._reloadChart().done(function() {
                console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                ProfitAndLoss._reloadSubTable().done(function() {
                    console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                    ProfitAndLoss._reloadMainTable().done(function() {
                        console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        $('body').removeClass('modal-open');

                        if (typeof callback === "function") {
                            return callback();
                        }

                    }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #1') });
                }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #2') });
            }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #3') });
        },
        _reloadSubTable: function()
        {
            return jQuery.pjax({
                url: ProfitAndLoss.getItemsUrl({
                    'searchBy': ProfitAndLoss.customMovementType,
                    'pageFilter': this.pageFilter,
                    'year':    ProfitAndLoss.year,
                    'quarter': ProfitAndLoss.quarter,
                    'month':   ProfitAndLoss.month,
                    'article': ProfitAndLoss.article,
                    'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                    'enableToggle': 0,
                    'ProfitAndLossSearchModel': {'year': $('#profitandlosssearchmodel-year').val()}
                }),
                container: (ProfitAndLoss.customMovementType === 'flows') ? "#flow-items-pjax" : "#doc-items-pjax",
                timeout: 10000,
                push: false,
                scrollTo: false
            });
        },
        _reloadMainTable: function ()
        {
            let floorMap = {};
            $('.scrollable-table-content > table.flow-of-funds').removeClass('scrolling-done').find('[data-collapse-row-tiger], [data-collapse-all-tiger], [data-collapse-tiger], [data-collapse-tiger-days]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            let data = {
                floorMap: floorMap,
                pageFilter: this.pageFilter,
            };
            let activeTab = $('#active-tab_report').val();

            return $.post('/analytics/finance-ajax/profit-and-loss-part/?activeTab=' + activeTab, data, function ($data) {
                let html = new DOMParser().parseFromString($data, "text/html");
                let table = html.querySelector('table.flow-of-funds').parentElement;
                let origTable = document.querySelector('.scrollable-table-container').parentElement;
                origTable.innerHTML = table.innerHTML;

                if ($coloredItemsCoords) {
                    $.each($coloredItemsCoords, function(i,v) {
                        $('.scrollable-table-content > table.flow-of-funds tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                    });
                }

                setTimeout(function() {ProfitAndLoss._bindMainTableEvents()}, 0);
            });
        },
        _reloadChart: function() {
            return window.ChartsProfitAndLoss.redrawByClick();
        },
        _bindMainTableEvents: function()
        {
            const mainTable = 'table.flow-of-funds';

            // main.js
            $(mainTable + ' [data-collapse-row-tiger]').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    // level 1
                    $('[data-id="'+target+'"]').addClass('d-none');
                    $('[data-id="'+target+'"]').find('[data-collapse-row-tiger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        // level 2
                        $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]').addClass('d-none');
                        $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]').find('[data-collapse-row-tiger]').removeClass('active');
                        $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]').each(function(i, row) {
                            $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-tiger].active').length <= 0 ) {
                    $('[data-collapse-all-tiger]').removeClass('active');
                } else {
                    $('[data-collapse-all-tiger]').addClass('active');
                }
            });
            $(mainTable + ' [data-collapse-all-tiger]').click(function() {
                var table = $(mainTable);
                var _this = $(mainTable + ' thead').find('[data-collapse-all-tiger]');
                var row = table.find('tr[data-id]');
                _this.toggleClass('active');
                if ( _this.hasClass('active') ) {
                    row.removeClass('d-none');
                    $(table).find('tbody .table-collapse-btn').addClass('active');
                } else {
                    row.addClass('d-none');
                    $(table).find('tbody .table-collapse-btn').removeClass('active');
                }
            });
            // column
            $(mainTable + ' [data-collapse-tiger]').click(function() {
                var target = $(this).data('target');
                var collapseCount = $(this).data('columns-count') || 3;
                var uncollapseCount = $(this).data('columns-count-collapsed') || 1;
                $(this).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', uncollapseCount);
                }

                const origTable = $('table.flow-of-funds.double-scrollbar-top:not(.scrollable-table-clone)');
                if (origTable.length) {
                    $(origTable).closest(".scrollable-table-container").find(".scrollable-table-bar > div").css("width", origTable.width());
                }
            });
        },
        _setTdFilter: function(td) {

            if (typeof $(td).data('industry_id') !== "undefined") {
                ProfitAndLoss.tdFilter.industry_id = $(td).data('industry_id');
            }
            if (typeof $(td).data('sale_point_id') !== "undefined") {
                ProfitAndLoss.tdFilter.sale_point_id = $(td).data('sale_point_id');
            }
            if (typeof $(td).data('project_id') !== "undefined") {
                ProfitAndLoss.tdFilter.project_id = $(td).data('project_id');
            }
        },
        _resetTdFilter: function() {
            ProfitAndLoss.tdFilter.industry_id =
                ProfitAndLoss.tdFilter.sale_point_id =
                    ProfitAndLoss.tdFilter.project_id = null;
        },
        _showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
        _showHoverText: function() {
            $('#custom-movement-type').hide();
            $('#flow-items-pjax').html('<div class="wrap">' + ProfitAndLoss.hoverText + '</div>');
            $('.items-table-block').removeClass('hidden');
            $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");
        }
    };

    /////////////////////
    ProfitAndLoss.init();
    /////////////////////

</script>
