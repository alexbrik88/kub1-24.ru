<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use common\components\TextHelper;
use common\components\helpers\Url;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use common\components\helpers\ArrayHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\assets\BalanceAsset;
use frontend\modules\analytics\models\BalanceSearch;
use frontend\modules\analytics\models\balance\BalanceViewHelper;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;

/* @var $this yii\web\View */
/* @var $model frontend\modules\analytics\models\BalanceSearch */
/* @var $balance array */

BalanceAsset::register($this);

$this->title = 'Баланс';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = date('Y') == $model->year;
$activeCheckbox = Yii::$app->session->get('balance.activeCheckboxes');

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-balance',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true,
    ],
]);

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_balance_help ?? false;
$showChartPanel = $userConfig->report_balance_chart ?? false;
$tabViewClass = $userConfig->getTableViewClass('table_view_finance_balance');

//
$balanceFlat = BalanceViewHelper::flattenTable($balance);
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <?= $this->render('@frontend/modules/analytics/views/multi-company/button') ?>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0 select2-wrapper" style="margin-top:-9px">
                <?= Html::beginForm(['balance'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'year',
                    'data' => $model->getYearFilter(),
                    'hideSearch' => true,
                    'options' => [
                        'id' => 'balancesearch-year',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_balance_chart">
    <div class="pt-4 pb-3">
        <div class="row">
            <div class="col-6 pr-2" style="min-width: 500px!important;">
                <?php
                echo $this->render('_charts/_chart_balance', [
                    'model' => $model
                ]); ?>
            </div>
            <div class="col-6 pl-2" style="min-width: 500px!important;">
                <?php
                echo $this->render('_charts/_chart_balance_structure', [
                    'model' => $model
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_balance_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/balance_panel')
        ?>
    </div>
</div>

<div class="d-flex pt-1 pb-1">
    <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
        <div class="mr-1">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                Url::to(['/analytics/finance/get-xls', 'type' => BalanceSearch::TAB_BALANCE, 'year' => $model->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                    'title' => 'Скачать в Excel',
                ]); ?>
        </div>
    <?php endif; ?>
    <div class="ml-2">
        <?= TableViewWidget::widget(['attribute' => 'table_view_finance_balance']) ?>
    </div>
    <div class="ml-auto">
        <?= Html::button(Icon::get('cog') . '<span>Начальные остатки</span>', [
            'id' => 'button-change-balance-page',
            'class' => 'button-regular button-clr button-hover-transparent pl-3 pr-3',
            'data-page' => 'balance-initial',
            'data-first-run' => (Yii::$app->session->get('balance.activeCheckboxes') === null) ? '1' : '0',
        ]) ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            <table class="table-balance table-bleak flow-of-funds table table-style table-count-list <?= $tabViewClass ?> table-count-list_collapse_none mb-0">
                <thead>
                <tr class="quarters-flow-of-funds">
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1">Статьи</span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $model->year; ?></span>
                        </button>
                    </th>
                </tr>
                <tr>
                    <?php foreach (ProfitAndLossSearchModel::$month as $key => $month): ?>
                        <?php $quarter = (int)ceil($key / 3); ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                            data-collapse-cell
                            data-id="<?= $cell_id[$quarter] ?>"
                            data-month="<?= $key; ?>"
                        >
                            <div class="pl-1 pr-1"><?= $month; ?></div>
                        </th>
                        <?php if ($key % 3 == 0): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                data-collapse-cell-total
                                data-id="<?= $cell_id[$quarter] ?>"
                            >
                                <div class="pl-1 pr-1">Итого</div>
                            </th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                </thead>

                <tbody>

                <?php foreach ($balanceFlat as $row): ?>
                    <?php
                    $currentQuarter = (int)ceil(date('m') / 3);
                    $suffix = ArrayHelper::remove($row, 'suffix');
                    $options = ArrayHelper::remove($row, 'options', []);
                    $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
                    $skipEmpty = \yii\helpers\ArrayHelper::remove($row, 'skipEmpty', false);
                    $canUpdate = ArrayHelper::remove($row, 'canUpdate', false);
                    if ($skipEmpty && array_sum($row['data']) === 0) {
                       continue;
                    }
                    $classBold = !$includedRow ? 'weight-700' : '';
                    ?>

                    <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']): ?>

                        <tr class="not-drag expenditure_type sub-block <?= ($includedRow) ? 'd-none' : '' ?>" <?php if ($row['floorId']): ?> data-id="<?= $row['floorId'] ?>" <?php endif; ?>>
                            <td class="pl-2 pr-2 pt-3 pb-3">
                                <button class="<?= $row['includedClass'] ?> table-collapse-btn button-clr ml-1 text-left" type="button" data-collapse-row-trigger data-target="<?= $row['floorTarget'] ?>">
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="d-block text_size_14 ml-1 <?= $classBold ?>"><?= $row['label']; ?></span>
                                </button>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $monthNumber = (int)$monthNumber;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($row['data'][$monthNumber]) ? $row['data'][$monthNumber] : 0;

                                ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                                <?php $quarterSum = isset($row['data']['quarter-' . $quarter]) ? $row['data']['quarter-' . $quarter] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>

                    <?php else: ?>
                        <tr class="item-block <?= ($includedRow) ? 'd-none' : '' ?>" <?php if ($row['floorId']): ?> data-id="<?= $row['floorId'] ?>" <?php endif; ?>>
                            <td class="pl-3 pr-2 pt-3 pb-3">
                                <div class="<?= $classBold ?: 'text-grey' ?> text_size_14 <?= $row['includedClass'] ?>" style="padding-top:3px">
                                    <?php if ('АКТИВ - ПАССИВ' === $row['label']): ?>
                                        АКТИВ
                                        <img class="icon-balance-active-passive" src="/img/<?= (array_filter($row['data'])) ? 'icon-not-equal-2.png' : 'icon-equal-2.png' ?>" width="18">
                                        ПАССИВ
                                        <?= Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip-balance',
                                            'style' => 'cursor:pointer',
                                            'data-tooltip-content' => '#tooltip_active_minus_passive_item',
                                        ]) ?>
                                    <?php elseif ('Предоплата покупателей (авансы)' === $row['label']): ?>
                                        <?= $row['label']; ?>
                                        <?= Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip-balance',
                                            'style' => 'cursor:pointer',
                                            'data-tooltip-content' => '#tooltip_prepayment_customer',
                                        ]) ?>
                                    <?php elseif ('Предоплата поставщикам (авансы)' === $row['label']): ?>
                                        <?= $row['label']; ?>
                                        <?= Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip-balance',
                                            'style' => 'cursor:pointer',
                                            'data-tooltip-content' => '#tooltip_prepayment_seller',
                                        ]) ?>
                                    <?php else: ?>
                                        <?= $row['label']; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $monthNumber = (int)$monthNumber;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($row['data'][$monthNumber]) ? $row['data'][$monthNumber] : 0;
                                ?>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?: 'text-grey' ?>">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                    </div>
                                </td>
                                <?php $quarterSum = isset($row['data']['quarter-' . $quarter]) ? $row['data']['quarter-' . $quarter] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1 <?= $classBold ?: 'text-grey' ?>">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>

                    <?php endif; ?>



                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div style="display: none">
    <div id="tooltip_active_minus_passive_item">
        Когда Активы не равны Пассивам, нужно найти либо недостающие<br/>
        Активы, если в строке цифра с минусом, либо найти Пассивы,<br/>
        если цифра в строке с плюсом.<br/>
    </div>
    <div id="tooltip_prepayment_seller">
        Данные в эту строку попадают из отчета Нам должны Обязательства<br/>за минусом сумм, которые отражены в строке Баланса "Депозит по аренде".<br/>
        Посмотреть данные можно в данном отчете.<br/>
        Настроить их можно в том же отчете по кнопке Настройка таблицы.<br/>
        Перейти в отчет можно <a href="/analytics/finance/debtor?type=2&report=2" class="link" target="_blank">тут</a>.
    </div>
    <div id="tooltip_prepayment_customer">
        Данные в эту строку попадают из отчета Мы должны Обязательства.<br/>
        Посмотреть данные можно в данном отчете.<br/>
        Настроить их можно в том же отчете по кнопке Настройка таблицы.<br/>
        Перейти в отчет можно <a href="/analytics/finance/debtor?type=1&report=2" class="link" target="_blank">тут</a>.
    </div>
</div>

<?= $this->render('@frontend/modules/analytics/views/multi-company/modal') ?>

<?php $this->registerJs('
    $(document).ready(function() {
        BalanceTable.init();
        BalanceTable.clickActiveCheckboxes('.json_encode((array)$activeCheckbox).');        
    });
'); ?>
