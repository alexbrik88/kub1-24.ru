<?php

use common\components\helpers\Html;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 * @var $dataIncomeByMonth array
 */

$this->title = 'Расходы' . $searchModel->multiCompanyManager->getSubTitle();

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_expenses_help ?? false;
$showChartPanel = $userConfig->report_expenses_chart ?? false;
$showPercentColumns = ($userConfig->report_odds_expense_percent || $userConfig->report_odds_expense_percent_2);
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <?= $this->render('@frontend/modules/analytics/views/multi-company/button') ?>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['expenses', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_expenses_chart">
    <div class="pt-3 pb-3">
        <div class="row mb-4">
            <div class="col-8 pr-2">
                <?php
                // Expense Charts
                echo $this->render('_charts/_chart_expense_main', [
                    'model' => $searchModel,
                ]);
                ?>
            </div>
            <div class="col-4 pl-2">
                <div class="row">
                    <div class="col-12">
                        <?php
                        // Expense Charts
                        echo $this->render('_charts/_chart_expense_structure_1', [
                            'model' => $searchModel,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-8 pr-2">
                <?php
                // Expense Charts
                echo $this->render('_charts/_chart_expense_income', [
                    'model' => $searchModel,
                ]);
                ?>
            </div>
            <div class="col-4 pl-2">
                <?php
                // Expense Charts
                echo $this->render('_charts/_chart_expense_structure_2', [
                    'model' => $searchModel,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_expenses_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/expenses_panel')
        ?>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
    <div class="ml-0">
        <div class="mb-2">
            <?= TableConfigWidget::widget([
                'mainTitle' => 'Выводить',
                'items' => [
                    [
                        'attribute' => 'report_odds_expense_percent',
                        'refresh-page' => true,
                        'help' => 'Столбец, в котором указан % каждой <br/>статьи расхода от общего Расхода'
                    ],
                    [
                        'attribute' => 'report_odds_expense_percent_2',
                        'refresh-page' => true,
                        'help' => 'Столбец, в котором указан %% каждой <br/>статьи расхода от общего Прихода'
                    ],
                ]
            ]); ?>
        </div>
    </div>
    <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
        <div class="pl-1">
            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                    'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                    'title' => 'Скачать в Excel',
                ]); ?>
        </div>
    <?php endif; ?>
    <div class="ml-2 pl-1">
        <div class="mb-2">
            <?= TableViewWidget::widget(['attribute' => 'table_view_finance_expenses']) ?>
        </div>
    </div>
    <div class="column mb-2 mr-0 pr-0 ml-auto" style="width: 270px">
        <div class="ml-1">
            <?= Select2::widget([
                'id' => 'income-tab',
                'name' => 'income-tab',
                'value' => $activeTab,
                'data' => [
                    ExpensesSearch::TAB_BY_ACTIVITY => 'По видам деятельности',
                    ExpensesSearch::TAB_BY_PURSE => 'По типам кошельков',
                ],
                'options' => [
                    'options' => [
                        ExpensesSearch::TAB_BY_ACTIVITY => [
                            'data-url' => Url::current([
                                'activeTab' => ExpensesSearch::TAB_BY_ACTIVITY,
                                'PlanFactSearch' => null
                            ])
                        ],
                        ExpensesSearch::TAB_BY_PURSE => [
                            'data-url' => Url::current([
                                'activeTab' => ExpensesSearch::TAB_BY_PURSE,
                                'PlanFactSearch' => null
                            ])
                        ],
                    ],
                    'onchange' => new \yii\web\JsExpression('location.href = this.options[this.selectedIndex].dataset.url')
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?php if ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY): ?>
        <?= $this->render('_partial/income_expenses_by_activity' . ($showPercentColumns ? '_percent' : ''), [
            'isIncome' => false,
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'dataIncomeByMonth' => $dataIncomeByMonth,
            'floorMap' => [],
        ]); ?>
    <?php elseif ($activeTab == ExpensesSearch::TAB_BY_PURSE): ?>
        <?= $this->render('_partial/income_expenses_by_purse' . ($showPercentColumns ? '_percent' : ''), [
            'isIncome' => false,
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'dataIncomeByMonth' => $dataIncomeByMonth,
            'floorMap' => []
        ]); ?>
    <?php endif; ?>
    <?= Html::hiddenInput('activeTab', $activeTab, [
        'id' => 'active-tab_report',
    ]); ?>
</div>

<?= $this->render('_partial/item_table', [
    'searchModel' => $searchModel,
    'showManyChangeCashbox' => true
]); ?>

<?= $this->render('@frontend/modules/analytics/views/multi-company/modal') ?>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<script>

    ExpensesTable = {
        sort: null,
        init: function () {
            ExpensesTable.bindEvents();
        },
        bindEvents: function() {
            // delete
            $(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
                let l = Ladda.create(this);
                l.start();

                $.get($(this).data('url'), null, function(data) {
                    ExpensesTable.reloadAll(function() {
                        Ladda.stopAll();
                        // l.remove();
                        $('.modal:visible').modal('hide');
                        ExpensesTable.showFlash(data.msg);
                    });
                });

                return false;
            });

            //update
            $(document).on('submit', '#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form, #operationUpdateForm', function(e) {
                e.preventDefault();
                var submitBtn = this.querySelector('button[type="submit"]');

                $(this).prepend('<input type="hidden" name="fromOdds" value="1">');

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    let l = Ladda.create(submitBtn);
                    l.start();
                    ExpensesTable.reloadAll(function() {
                        Ladda.stopAll();
                        // l.remove();
                        $('.modal:visible').modal('hide');
                        ExpensesTable.showFlash('Операция обновлена.'); // todo: show errors
                    });
                });

                return false;
            });

            // many delete
            $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                let $this = $(this);
                let l = Ladda.create(this);
                l.start();

                if (!$this.hasClass('clicked')) {
                    if ($('.joint-checkbox:checked').length > 0) {
                        $this.addClass('clicked');
                        $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {
                            ExpensesTable.reloadAll(function() {
                                Ladda.stopAll();
                                // l.remove();
                                $('.modal:visible').modal('hide');
                                ExpensesTable.showFlash(data.msg);
                                $this.removeClass('clicked');
                            });
                        });
                    }
                }
                return false;
            });

            // many item (update articles)
            $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                e.preventDefault();

                var l = Ladda.create($(this).find(".btn-save")[0]);
                var $hasError = false;

                l.start();
                $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                    if ($(this).find("select").val() == "") {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    ExpensesTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ExpensesTable.showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            $(document).on("shown.bs.modal", "#many-item", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);
                var $header = $modal.find(".modal-header h1");
                var $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                });
            });

            $(document).on("hidden.bs.modal", "#many-item", function () {
                $(".expenditure-item-block").addClass("hidden");
                $(".income-item-block").addClass("hidden");
                $(".additional-header-text").remove();
                $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
            });

            // sort
            $(document).on("click", ".th-sort", function() {
                ExpensesTable.sort = $(this).data('sort');
                ExpensesTable._reloadMainTable();
                if ($('.close-odds').is(':visible')) {
                    $coloredItemsCoords = [];
                    $('.close-odds').trigger('click');
                }
            });

            // many cashbox (update cashbox)
            $(document).on('click', '#many-change-cashbox-ajax', function(e) {
                e.preventDefault();

                var modal = $(this).closest('.modal');
                var $hasError = false;
                var data = $('.joint-checkbox, .operation-many-cashbox-field').serialize();
                var l = Ladda.create(this);

                l.start();

                if ($hasError) {
                    Ladda.stopAll();
                    // l.remove();
                    return false;
                }

                $.post($(this).data('url'), data, function (data) {
                    ExpensesTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ExpensesTable.showFlash(data.msg);
                        Ladda.stopAll();
                        // l.remove();
                    });
                });

                return false;
            });

            $(document).on("shown.bs.modal", "#many-cashbox", function () {
                var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                var $modal = $(this);

                if ($includeExpenditureItem) {
                    $(".expense-block", $modal).removeClass("hidden");
                } else {
                    $(".expense-block", $modal).addClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-block", $modal).removeClass("hidden");
                } else {
                    $(".income-block", $modal).addClass("hidden");
                }
            });

            $(document).on("hide.bs.modal", "#many-cashbox", function () {
                var modal = $(this);
                $(".income-block, .expense-block", modal).addClass("hidden");
            });

            // show/hide "change cashbox" button
            $(document).on('change', '.joint-checkbox', function() {
                const table = $(this).closest('table');
                const checkboxes = table.find('.joint-checkbox:checked');
                const summaryContainer = $('#summary-container');
                let showCashboxBtn = true;
                if (checkboxes.length) {
                    $(checkboxes).each(function(i,v){
                        if ($(v).attr('name').indexOf('cash_order_flows') === -1)
                            showCashboxBtn = false;
                    });
                } else {
                    showCashboxBtn = false;
                }

                if (showCashboxBtn) {
                    $(summaryContainer).find('.dropdown-item-cashbox').removeClass('hidden');
                } else {
                    $(summaryContainer).find('.dropdown-item-cashbox').addClass('hidden');
                }
            });

            // many project, industry, salepoint
            $(document).on('click', '.modal-many-change-project, .modal-many-change-sale-point, .modal-many-change-company-industry', function (e) {

                e.preventDefault();

                let l = Ladda.create(this);
                l.start();
                $.post($(this).data('url'), $('.joint-checkbox, #operation-many-project, #operation-many-sale-point, #operation-many-company-industry').serialize(), function (data) {
                    ExpensesTable.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ExpensesTable.showFlash(data.message);
                        Ladda.stopAll();
                    });
                });

                return false;
            });

            $(document).on("show.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {

                let defaultVal = null;
                let defaults = {industry: [], sale_point: [], project: []};
                const unique = function(array){
                    return array.filter(function(el, index, arr) {
                        return index === arr.indexOf(el);
                    });
                };

                $('.joint-checkbox:checked').each(function(i,v) {
                    defaults.industry.push($(v).data('industry') || 0);
                    defaults.sale_point.push($(v).data('sale_point') || 0);
                    defaults.project.push($(v).data('project') || 0);
                });

                switch ($(this).attr('id')) {
                    case 'many-company-industry':
                        defaultVal = (unique(defaults.industry).length === 1) ? defaults.industry[0] : 0;
                        break;
                    case 'many-sale-point':
                        defaultVal = (unique(defaults.sale_point).length === 1) ? defaults.sale_point[0] : 0;
                        break;
                    case 'many-project':
                        defaultVal = (unique(defaults.project).length === 1) ? defaults.project[0] : 0;
                        break;
                }

                $(this).find('select').val(defaultVal).trigger('change');
            });

            $(document).on("hidden.bs.modal", "#many-company-industry, #many-sale-point, #many-project", function () {
                $(this).find('select').val(null).trigger('change');
            });
        },
        reloadAll: function(callback)
        {
            let debugCurrTime = Date.now();
            return ExpensesTable._reloadChart().done(function() {
                console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                ExpensesTable._reloadSubTable().done(function() {
                    console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                    ExpensesTable._reloadMainTable().done(function() {
                        console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        if (typeof callback === "function") {
                            return callback();
                        }

                    }).catch(function() { ExpensesTable.showFlash('Ошибка сервера #1') });
                }).catch(function() { ExpensesTable.showFlash('Ошибка сервера #2') });
            }).catch(function() { ExpensesTable.showFlash('Ошибка сервера #3') });
        },
        _reloadSubTable: function()
        {
            const $year = "ExpensesSearch%5Byear%5D=" + $('#expensessearch-year').val();
            $itemIDs['activeTab'] = $('#active-tab_report').val(); // todo: var from custom.js

            return jQuery.pjax({
                url: "/analytics/finance/item-list?" + $year,
                type: 'POST',
                data: {'items': JSON.stringify($itemIDs)},
                container: '#odds-items_pjax',
                timeout: 10000,
                push: false,
                scrollTo: false
            });
        },
        _reloadMainTable: function ()
        {
            let floorMap = {};
            $('table.flow-of-expenses').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-all-trigger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            let data = {floorMap: floorMap};
            let activeTab = $('#active-tab_report').val();

            return $.post('/analytics/finance-ajax/expenses-part/?activeTab=' + activeTab + '&sort=' + this.sort, data, function ($data) {
                let html = new DOMParser().parseFromString($data, "text/html");
                let table = html.querySelector('table.flow-of-expenses');
                let origTable = document.querySelector('table.flow-of-expenses');
                origTable.innerHTML = table.innerHTML;

                if ($coloredItemsCoords) {
                    $.each($coloredItemsCoords, function(i,v) {
                        $('table.flow-of-expenses tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                    });
                }

                ExpensesTable._bindMainTableEvents();
            });
        },
        _reloadChart: function() {
            window.ChartExpensesStructure2.redrawByClick();
            window.ChartExpensesStructure1.redrawByClick();
            window.ChartExpensesIncome.redrawByClick();
            return window.ChartExpensesMain.redrawByClick();
        },
        _bindMainTableEvents: function() {

            // main.js
            $('[data-collapse-row-trigger]', 'table.flow-of-expenses tbody').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    // level 1
                    $('[data-id="'+target+'"]').addClass('d-none');
                    $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        // level 2
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                    $('[data-collapse-all-trigger]').removeClass('active');
                } else {
                    $('[data-collapse-all-trigger]').addClass('active');
                }
            });

            // main.js
            $('[data-collapse-trigger]', 'table.flow-of-income thead').click(function() {
                var target = $(this).data('target');
                var collapseCount = $(this).data('columns-count') || 3;
                var uncollapseCount = $(this).data('columns-count-collapsed') || 1;
                $(this).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', uncollapseCount);
                }
                $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
                $(this).trigger('dataCollapseTriggerEnd');
            });

            // main.js
            $('[data-collapse-all-trigger]').click(function() {
                var _this = $(this);
                var table = $(this).closest('.table');
                var row = table.find('tr[data-id]');
                _this.toggleClass('active');
                if ( _this.hasClass('active') ) {
                    row.removeClass('d-none');
                    $(table).find('tbody .table-collapse-btn').addClass('active');
                } else {
                    row.addClass('d-none')
                    $(table).find('tbody .table-collapse-btn').removeClass('active');
                }
                $(this).trigger('dataCollapseTriggerEnd');
            });
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    /////////////////////
    ExpensesTable.init();
    /////////////////////

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>

<?php /* $this->registerJs('

    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
    });

     $(".expenses-panel-trigger").click(function (e) {
        $(".expenses-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });

    $(document).on("submit", "#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form", function (e) {
        $(this).prepend(\'<input type="hidden" name="fromOdds" value="1">\');
        $.ajax({
            "type": "post",
            "url": $(this).attr("action"),
            "data": $(this).serialize(),
            "success": function(data) {
                $(".odds-model-movement, #ajax-modal-box").modal("hide");
                location.href = location.href;
            }
        });

        return false;
    });
    $(document).on("shown.bs.modal", "#many-item", function () {
        var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
        var $header = $modal.find(".modal-header h1");
        var $additionalHeaderText = null;

        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        if ($includeExpenditureItem && $includeIncomeItem) {
            $additionalHeaderText = " прихода / расхода";
        } else if ($includeExpenditureItem) {
            $additionalHeaderText = " расхода";
        } else if ($includeIncomeItem) {
            $additionalHeaderText = " прихода";
        }
        $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
        $(".joint-checkbox:checked").each(function() {
            $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
        });
    });
    $(document).on("hidden.bs.modal", "#many-item", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
        $(".additional-header-text").remove();
        $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
    });
    $(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
        var l = Ladda.create($(this).find(".btn-save")[0]);
        var $hasError = false;

        l.start();
        $(".field-expensessearch-expenditureitemidmanyitem:visible").each(function () {
            $(this).removeClass("has-error");
            $(this).find(".help-block").text("");
            if ($(this).find("select").val() == "") {
                $hasError = true;
                $(this).addClass("has-error");
                $(this).find(".help-block").text("Необходимо заполнить.");
            }
        });
        if ($hasError) {
            return false;
        }
    });
'); ?>
*/ ?>