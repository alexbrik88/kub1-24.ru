<?php

use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use \frontend\modules\analytics\assets\AnalyticsAsset;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\models\BreakEvenSearchModel;

/** @var $searchModel BreakEvenSearchModel */

AnalyticsAsset::register($this);

$this->title = 'Точка безубыточности';

$total = [
    'revenue' => ArrayHelper::getValue($data['totalRevenue'], $searchModel->year.$searchModel->month, 0),
    'quantity' => ArrayHelper::getValue($data['quantity'], $searchModel->year.$searchModel->month, 0),
    'fixedCosts' => ArrayHelper::getValue($data['totalFixedCosts'], $searchModel->year.$searchModel->month, 0),
    'variableCosts' => ArrayHelper::getValue($data['totalVariableCosts'], $searchModel->year.$searchModel->month, 0),
    'revenuePrev' => ArrayHelper::getValue($data['totalRevenue'], $searchModel->year.$searchModel->month, 0),
    'quantityPrev' => ArrayHelper::getValue($data['quantity'], $searchModel->year.$searchModel->month, 0),
    'fixedCostsPrev' => ArrayHelper::getValue($data['totalFixedCosts'], $searchModel->year.$searchModel->month, 0),
    'variableCostsPrev' => ArrayHelper::getValue($data['totalVariableCosts'], $searchModel->year.$searchModel->month, 0),
];

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');
?>

<?= $this->render('_partial/break_even_header', [
    'searchModel' => $searchModel,
    'total' => $total
]); ?>

<div class="wrap wrap_padding_none mb-2" style="margin-top: 12px">
    <?= $this->render('_partial/break_even_table', [
        'searchModel' => $searchModel,
        'total' => $total,
        'tabViewClass' => $tabViewClass
    ]); ?>
</div>

<div class="wrap wrap_padding_none mb-2" style="margin-top: 12px">
    <?= $this->render('_partial/break_even_table_2', [
        'searchModel' => $searchModel,
        'data' => $data,
        'total' => $total,
        'tabViewClass' => $tabViewClass,
        'customerInputsMap' => $customerInputsMap ?? []
    ]); ?>
</div>

<?php
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>