<?php

use frontend\components\PageSize;
use frontend\modules\analytics\assets\AnalyticsAsset;
use yii\widgets\Pjax;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use frontend\modules\analytics\models\debtor\DebtorHelper2;

/* @var $searchModel DebtReportSearch2
 * @var $searchModel2 DebtReportSearchAsBalance
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $reportType integer
 * @var $searchBy integer
 * @var $this \yii\web\View
 */

// Used models:
// debtor_table_months.php -> $searchModel2
// debtor_table_debt_periods.php -> $searchModel
// debtor_table_net_total.php -> $searchModel (one time used $searchModel2 to get employers filter list)

AnalyticsAsset::register($this);

$this->title = ($type == 2 ? 'Нам должны' : 'Мы должны');
$reportType = $reportType ?? DebtReportSearch2::REPORT_TYPE_MONEY;
$searchBy = $searchBy ?? DebtReportSearch2::SEARCH_BY_DOCUMENTS;
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass(($type == 2) ? 'table_view_report_client' : 'table_view_report_supplier');
$filterByTitle = \common\components\helpers\ArrayHelper::getValue(Yii::$app->request->get('DebtReportSearch2'), 'title');

switch ($reportType) {
    case DebtReportSearch2::REPORT_TYPE_MONEY:
        $dataProvider = $searchModel->findDebtor(Yii::$app->request->get(), $type); // need for charts
        $dataProvider2 = $searchModel2->getTradeReceivables($type, $filterByTitle);
        break;
    case DebtReportSearch2::REPORT_TYPE_GUARANTY:
        $dataProvider = $searchModel->findDebtor(Yii::$app->request->get(), $type); // need for charts
        $dataProvider2 = $searchModel2->getPrepaymentAmount($type, $filterByTitle);
        break;
    case DebtReportSearch2::REPORT_TYPE_NET_TOTAL:
        $dataProvider = $searchModel->findNetTotalDebts(Yii::$app->request->get()); // need for charts
        $dataProvider2 = null;
        break;
}

$renderParams = [
    'type' => $type,
    'title' => $this->title,
    'company' => $searchModel->company,
    'searchBy' => $searchBy,
    'reportType' => $reportType,
    'searchModel' => $searchModel,
    'searchModel2' => $searchModel2,
    'tabViewClass' => $tabViewClass,
    'dataProvider' => $dataProvider,
    'dataProvider2' => $dataProvider2,
    'floorMap' => Yii::$app->request->post('floorMap', []),
];
$pjaxParams = [
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'timeout' => 10000,
    'formSelector' => '#debtor-title-filter',
];
?>

<?php

// 0. HEADERS (prevent calculate charts when sort table)
if (!Yii::$app->request->isPjax) {
    // main menu
    echo $this->render('@frontend/modules/analytics/views/layouts/_debtor_submenu', $renderParams);
    // charts
    echo $this->render('_partial/debtor_header', $renderParams);
    // sub menu
    echo $this->render('@frontend/modules/analytics/views/layouts/_debtor_sub_submenu', $renderParams);
    // options & search
    echo $this->render('_partial/debtor_control_panel', $renderParams);
}

// 1. MONEY
if ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY) {

    if ($searchBy == DebtReportSearch2::SEARCH_BY_DOCUMENTS || $searchBy == DebtReportSearch2::SEARCH_BY_INVOICES) {
        Pjax::begin(array_merge($pjaxParams, ['id' => 'refresh_debtor_table_months']));
        echo $this->render('_partial/debtor_table_months', $renderParams);
        Pjax::end();
    }

    if ($searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS) {
        Pjax::begin(array_merge($pjaxParams, ['id' => 'refresh_debtor_table_debt_periods']));
        echo $this->render('_partial/debtor_table_debt_periods', $renderParams);
        Pjax::end();
    }
}

// 2. GUARANTY
else if ($reportType == DebtReportSearch2::REPORT_TYPE_GUARANTY) {
    Pjax::begin(array_merge($pjaxParams, ['id' => 'refresh_debtor_table_months']));
    echo $this->render('_partial/debtor_table_months', $renderParams);
    Pjax::end();
    echo $this->render('_partial/debtor_options_modal', $renderParams);
}

// 3. NET TOTAL
else if ($reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL) {
    Pjax::begin(array_merge($pjaxParams, ['id' => 'refresh_debtor_table_net_total']));
    echo $this->render('_partial/debtor_table_net_total', $renderParams);
    Pjax::end();
}

?>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });

    // TABLE 2 RELOAD
    $(document).on("pjax:success", "#refresh_debtor_table_months", function(event, data) {

        <?php if ($reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL): ?>
            // FILTER
            const filter = $('#debtor-filter');
            $(filter).removeClass('show').find('.dropdown-menu.show').removeClass('show');
            if ($(filter).find(':radio:checked').val()) {
                $(filter).addClass('itemsSelected');
            } else {
                $(filter).removeClass('itemsSelected');
            }
            // FIXED COL
            window._firstCSTableColumn = $('#cs-table-first-column');
            initDebtorDoubleScroll();
            if (window._offsetMCSLeft != 0)
                $(_firstCSTableColumn).show();
        <?php else: ?>
            // COL
            $('[data-collapse-trigger]').click(function() {
                var target = $(this).data('target');
                var collapseCount = $(this).data('columns-count') || 3;
                $(this).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
                }
                $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
            });
            // ROW
            $('[data-collapse-row-trigger]').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                    });
                    $('[data-id="'+target+'"]').addClass('d-none');
                }
                if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                    $('[data-collapse-all-trigger]').removeClass('active');
                } else {
                    $('[data-collapse-all-trigger]').addClass('active');
                }
            });
            // SCROLL
            $('.custom-scroll-table').mCustomScrollbar({
                horizontalScroll: true,
                axis:"x",
                scrollInertia: 300,
                advanced:{
                    autoExpandHorizontalScroll: 3,
                    updateOnContentResize: true,
                    updateOnImageLoad: false
                },
                mouseWheel:{ enable: false },
            });
        <?php endif; ?>
    });

    // TABLE2 - MAIN PJAX REQUEST
    $('#refresh_debtor_table_months').on('pjax:beforeSend', function (event, xhr, settings) {
        if (settings.data == undefined) {

            let floorMap = {};
            $('table.debtor2, table.debtor-table-3').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-debtor]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });

            let titleFilter = $('#debtreportsearch2-title').val().trim();
            if (titleFilter) {
                settings.url += '&DebtReportSearch2[title]=' + titleFilter;
            }

            let responsibleEmployeeFilter = $('#debtor-responsible_employee_id').val();
            if (responsibleEmployeeFilter) {
                settings.url += '&DebtReportSearch2[responsible_employee_id]=' + responsibleEmployeeFilter;
                settings.url += '&DebtReportSearchAsBalance[responsible_employee_id]=' + responsibleEmployeeFilter;
            }

            let netContractorTypeFilter = $('#debtor-net_contractor_type').val();
            if (netContractorTypeFilter) {
                settings.url += '&DebtReportSearch2[net_contractor_type]=' + netContractorTypeFilter;
            }

            let netDebtTypeFilter = $('#debtor-filter :radio:checked').val();
            if (netDebtTypeFilter) {
                settings.url += '&DebtReportSearch2[net_debt_type]=' + netDebtTypeFilter;
            }

            jQuery.pjax({
                url: settings.url,
                type: 'POST',
                data: {'floorMap': floorMap},
                container: '#refresh_debtor_table_months',
                timeout: 15000,
                push: false,
                replace: false,
                scrollTo: false
            });

            return false;
        }
    });

    // TABLE2 FILTERS
    $(document).on("click", "#debtor-responsible_employee_id-toggle", function (e) {
        e.preventDefault();
        if (!$("#debtor-responsible_employee_id").data("select2").isOpen()) {
            $("#debtor-responsible_employee_id").select2("open");
        }
        return false;
    });

    $(document).on("click", "#debtor-net_contractor_type-toggle", function (e) {
        e.preventDefault();
        if (!$("#debtor-net_contractor_type").data("select2").isOpen()) {
            $("#debtor-net_contractor_type").select2("open");
        }
        return false;
    });

    $(document).on("change", "#debtor-responsible_employee_id", function() {
        $.pjax.reload("#refresh_debtor_table_months");
    });

    $(document).on("change", "#debtor-net_contractor_type", function() {
        $.pjax.reload("#refresh_debtor_table_months");
    });

    $(document).on("click", "#debtor-filter [type=submit]", function() {
        $.pjax.reload("#refresh_debtor_table_months");
    });

</script>

<?php if ($reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL) {
echo $this->registerJs(<<<JS

    // TABLE3
    window._activeMCS = 0;
    window._offsetMCSLeft = 0;
    window._firstCSTableColumn = $('#cs-table-first-column');
  
    $('[data-collapse-row-trigger]').unbind();
  
    initDebtorDoubleScroll = function() { 
        
        // ODDS FIXED COLUMN
        OddsTable.fillFixedColumn();
        
        $('#cs-table-1').mCustomScrollbar({
            horizontalScroll: true,
            axis:"x",
            scrollInertia: 0,
            setLeft: window._offsetMCSLeft + 'px',
            callbacks: {
                onScrollStart: function() {
                    if (!window._activeMCS)
                        window._activeMCS = 1;
                },
                onScroll: function() {
                    if (window._activeMCS == 1)
                        window._activeMCS = 0;
    
                    if (this.mcs.left == 0 && $(_firstCSTableColumn).is(':visible'))
                        $(_firstCSTableColumn).hide();
                },
                whileScrolling:function(){
                    window._offsetMCSLeft = this.mcs.left;
                    if (window._activeMCS == 1)
                        $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
    
                    if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                        $(_firstCSTableColumn).show();
                }
            },
            advanced:{
                autoExpandHorizontalScroll: true,
                updateOnContentResize: true,
                updateOnImageLoad: false,
            },
            mouseWheel:{ enable: false },
        });
    
        $('#cs-table-2').mCustomScrollbar({
            horizontalScroll: true,
            axis:"x",
            scrollInertia: 0,
            setLeft: window._offsetMCSLeft + 'px',
            callbacks: {
                onScrollStart: function() {
                    this.scrollInertia = 1000;
                    if (!window._activeMCS)
                        window._activeMCS = 2;
                },
                onScroll: function() {
                    if (window._activeMCS == 2)
                        window._activeMCS = 0;
                },
                whileScrolling:function(){
                    window._offsetMCSLeft = this.mcs.left;
                    if (window._activeMCS == 2)
                        $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
                }
            },
            advanced:{
                autoExpandHorizontalScroll: true,
                updateOnContentResize: true,
                updateOnImageLoad: false,
            },
            mouseWheel:{ enable: false },
        });
    
        $("[data-collapse-trigger-debtor]").click(function() {
    
            var collapseBtn = this;
    
            setTimeout(function() {
                $("#cs-table-2 table").width($("#cs-table-1 table").width() - 3);
                $("#cs-table-2").mCustomScrollbar("update");
            }, 250);
    
            var _collapseToggle = function(collapseBtn)
            {
                var target = $(collapseBtn).data('target');
                var collapseCount = $(collapseBtn).data('columns-count') || 3;
    
                $(collapseBtn).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(collapseBtn).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
                }
                $(collapseBtn).closest('.custom-scroll-table').mCustomScrollbar("update");
            };
    
            _collapseToggle(collapseBtn);
    
        });
        
        $("[data-collapse-row-trigger]").click(function() {
            var target = $(this).data('target');
            $(this).toggleClass('active');
            if ( $(this).hasClass('active') ) {
                $('[data-id="'+target+'"]').removeClass('d-none');
            } else {
                $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                $('[data-id="'+target+'"]').each(function(i, row) {
                    $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                });
                $('[data-id="'+target+'"]').addClass('d-none');
            }
        });

        $("#cs-table-2 table").width($("#cs-table-1 table").width() - 3);
        $("#cs-table-2").mCustomScrollbar("update");
        setTimeout(function() {
            $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
        }, 250);
    }
    
    $(document).ready(function() {
    /////////////////////////
    initDebtorDoubleScroll();
    /////////////////////////
    }); 
JS
);
}

