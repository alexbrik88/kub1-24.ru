<?php

use common\models\company\EmployeeCount;
use frontend\modules\analytics\models\ReportsStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\analytics\models\ReportsStartForm */

?>

<div class="reports-start-header">
    <h4>
        Создание аккаунта для вашей компании займет около минуты.
    </h4>
    <h4>
        Посмотрите краткое описание некоторых отчетов:
    </h4>
</div>
<div class="mt-3 mb-5">
    <div id="promo_item_1" class="promo_item collapse show">
        <h6>Отечет Движения Денежных Средств</h6>
        <div>
            Ответит на вопросы: Сколько? Когда? Кому? От кого?
            Поможет избежать кассовых разрывов и взять деньги под контроль.
            Контролируешь деньги – управляешь бизнесом.
        </div>
        <div class="mt-4">
            <?= Html::img('/images/analytics/odds.gif', [
                'class' => 'w-100',
            ]) ?>
        </div>
    </div>
    <div id="promo_item_2" class="promo_item collapse">
        <h6>Отчет о Прибылях и Убытках</h6>
        <div>
            Покажет, насколько эффективен ваш бизнес, или продукт, или руководитель.
            Отчет позволяет сравнить и выявить наиболее прибыльные / убыточные направления.
        </div>
        <div class="mt-4">
            <?= Html::img('/images/analytics/opiu.gif', [
                'class' => 'w-100',
            ]) ?>
        </div>
    </div>
    <div id="promo_item_3" class="promo_item collapse">
        <h6>Управленческий Баланс</h6>
        <div>
            Покажет, сколько стоит ваш бизнес на определенную дату – чем владеете и сколько должны.
        </div>
        <div class="mt-4">
            <?= Html::img('/images/analytics/balance.gif', [
                'class' => 'w-100',
            ]) ?>
        </div>
    </div>
    <div id="promo_item_4" class="promo_item collapse">
        <h6>Отчет План Факт</h6>
        <div>
            Платежный календарь сформирует плановые операции на основе предыдущих операций и текущих сделок.
            А в отчете План-Факт наглядно видно, что выполнено, а что нет
        </div>
        <div class="mt-4">
            <?= Html::img('/images/analytics/plan_fact.gif', [
                'class' => 'w-100',
            ]) ?>
        </div>
    </div>
    <div id="promo_item_5" class="promo_item collapse">
        <h6>АБС анализ товаров</h6>
        <div>
            Отчет покажет, на какой товар поднять цену и  увеличить маржинальность.
            Предупредит, какой товар заморозил ваши деньги в остатках.
        </div>
        <div class="mt-4">
            <?= Html::img('/images/analytics/abs.gif', [
                'class' => 'w-100',
            ]) ?>
        </div>
    </div>
</div>

<div class="counter-toggle-hide text-center">
    <div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="transition: width 0.1s"></div>
    </div>
    Осталось:
    <strong style="display: inline-block; font-size: 20px; width: 50px">
        <span class="counter-element link">15</span>
    </strong>
    секунд
</div>
<div class="counter-toggle-hide hidden text-center">
    <?= Html::button('OK', [
        'data-dismiss' => 'modal',
        'class' => 'button-regular button-hover-transparent min-w-130',
    ]) ?>
</div>


<script type="text/javascript">
$(document).ready(function(){
    $('.promo_item.collapse').collapse({
      toggle: false
    });
});
</script>