<?php

use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\components\Icon;
use frontend\modules\analytics\models\ReportsStartForm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\analytics\models\ReportsStartForm */

$company = $model->company;
$taxation = $company->companyTaxationType;
?>

<div class="reports-start-header">
    <h4>
        <?= 'Система налогообложения' . \yii\helpers\Html::tag('span', \frontend\themes\kub\helpers\Icon::QUESTION, [
            'title' => 'Информация о системе налогообложения<br/> позволит учитывать ваши налоги в отчетах',
            'title-as-html' => 1
        ]) ?>
    </h4>
</div>

<div class="mt-4">
    Для учета налогов при формировании отчетов
</div>

<?php $form = ActiveForm::begin([
    'id' => 'taxation-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'action' => ['taxation'],
    'options' => [
        'class' => 'mt-3',
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <div class="row">
        <div class="column">
            <label class="label mr-4">
                <?= Html::activeCheckbox($model, 'tax_osno', [
                    'class' => 'tax_osno',
                    'label' => false,
                ]) ?>
                <?= $model->getAttributeLabel('tax_osno') ?>
            </label>

            <label class="label mr-4">
                <?= Html::activeCheckbox($model, 'tax_usn', [
                    'class' => 'tax_not_osno',
                    'label' => false,
                ]) ?>
                <?= $model->getAttributeLabel('tax_usn') ?>
            </label>
            <label class="label mr-4">
                <?= Html::activeCheckbox($model, 'tax_psn', [
                    'class' => 'tax_not_osno',
                    'label' => false,
                    'disabled' => $company->company_type_id != CompanyType::TYPE_IP,
                ]) ?>
                <?= $model->getAttributeLabel('tax_psn') ?>
            </label>
            <?php if ($error = $model->getFirstError('osno')) : ?>
                <div class="text-danger">
                    <?= $error ?>
                </div>
            <?php endif ?>
            <?= Html::beginTag('div', [
                'class' => 'tax-usn-config collapse' . ($taxation->usn ? ' show' : ''),
            ]) ?>
            <?= $form->field($model, 'usn_type', [
                'options' => [
                    'class' => 'row',
                    'style' => 'max-width: 300px; margin-left: 76px;',
                ],
            ])->radioList(CompanyTaxationType::$usnType, [
                'class' => 'column mb-2',
                'style' => 'margin-top: 10px;',
                'item' => function ($index, $label, $name, $checked, $value) use ($model, $taxation) {
                    $radio = Html::radio($name, $checked, [
                        'id' => 'usn_radio_'.$index,
                        'class' => 'usn_type_radio',
                        'value' => $value,
                        'label' => $label,
                        'labelOptions' => [
                            'wrapInput' => true,
                            'class' => 'label',
                        ],
                    ]);
                    $itemRadio = Html::tag('div', $radio, [
                        'class' => 'column mb-2',
                    ]);
                    $percent = Html::activeTextInput($model, 'usn_percent', [
                            'class' => 'form-control d-inline-block text-right usn-percent usn-percent-' . $value,
                            'style' => 'width: 75px;',
                            'disabled' => !$checked,
                            'value' => $checked ? $model->usn_percent : $taxation->defaultUsnPercent($value),
                            'data-default' => $taxation->defaultUsnPercent($value),
                        ]) . ' <span class="ml-1">%</span>';
                    $itemPercent = Html::tag('div', $percent, [
                        'class' => 'column mb-2 pl-0',
                    ]);

                    return Html::tag('div', "$itemRadio\n$itemPercent", [
                        'class' => 'row align-items-center justify-content-between usn_type_row',
                    ]);
                },
            ])->label(false) ?>

            <?= Html::endTag('div') ?>
        </div>
    </div>

    <div class="mt-4">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-hover-transparent min-w-130',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<script type="text/javascript">
$(document).ready(function () {
    function resetDisabledCheckboxes(form) {
        $('.form-group', form).removeClass('has-success').removeClass('has-error');
    }

    $(document).on("change", "#reportsstartform-tax_osno", function() {
        var form = this.form;
        if (this.checked) {
            $(".nds-view-osno", form).collapse("show");
        } else {
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes(form);
    });
    $(document).on("change", "#reportsstartform-tax_usn", function() {
        var form = this.form;
        if (this.checked) {
            $(".tax-usn-config", form).collapse("show");
            $("input.usn-percent", form).each(function (i, el) {
                $(el).val($(el).data('default'));
            });
        } else {
            $(".tax-usn-config", form).collapse("hide");
            $("#reportsstartform-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#reportsstartform-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#reportsstartform-usn_percent", form).val("");
        }
        resetDisabledCheckboxes(form);
    });
    $(document).on("change", "#reportsstartform-tax_envd", function() {
        var form = this.form;
        if (this.checked) {
            $("#reportsstartform-tax_psn:checked", form).prop("checked", false).trigger('change');
        }
        resetDisabledCheckboxes(form);
    });
    $(document).on("change", "#reportsstartform-tax_psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#reportsstartform-tax_envd:checked", form).prop("checked", false).trigger('change');
            $(".tax-patent-config", form).collapse("show");
        } else {
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes(form);
    });
    $(document).on("change", "#taxation-form input.tax_osno", function() {
        console.log(this.checked);
        if (this.checked) {
            $("input.tax_not_osno:checked", this.form).prop("checked", false).trigger('change');
        }
    });
    $(document).on("change", "#taxation-form input.tax_not_osno", function() {
        console.log(this.checked);
        if (this.checked) {
            $("input.tax_osno:checked", this.form).prop("checked", false).trigger('change');
        }
    });
    $(document).on("change", "input.usn_type_radio", function() {
        $("input.usn_type_radio", this.form).each(function (i, el) {
            $(el).closest('.usn_type_row').find('input.usn-percent').prop('disabled', !$(this).is(':checked'));
        });
    });
});
</script>