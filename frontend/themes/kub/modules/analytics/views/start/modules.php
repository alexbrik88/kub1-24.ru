<?php

use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\components\Icon;
use frontend\modules\analytics\models\ReportsStartForm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\analytics\models\ReportsStartForm */

?>

<div class="reports-start-header">
    <h4>
        Выберите модули КУБа, которые могут быть полезны для вашего бизнеса
    </h4>
</div>

<div>
    Данные модули будут вам подключены для ознакомления на 14 дней. Никакой платы с вас не будет браться.
</div>

<?php $form = ActiveForm::begin([
    'id' => 'modules-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'action' => ['modules'],
    'options' => [
        'class' => 'mt-4',
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <div class="row">
        <div class="column">
            <?php foreach (ReportsStartForm::$modulesItems as $key => $value) : ?>
                <div class="mt-3 d-flex justify-content-start align-items-start">
                    <div>
                        <?= Html::activeCheckbox($model, "modules[{$key}]", [
                            'label' => false,
                        ]) ?>
                    </div>
                    <label class="label" for="<?= Html::getInputId($model, "modules[{$key}]") ?>">
                        <strong style="color: #001424"><?= $value['name'] ?></strong>
                        <div><?= $value['description'] ?></div>
                    </label>
                </div>
            <?php endforeach ?>

            <?php if ($error = $model->getFirstError('modules')) : ?>
                <div class="text-danger">
                    <?= $error ?>
                </div>
            <?php endif ?>

        </div>
    </div>

    <div class="mt-4">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-hover-transparent min-w-130',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<script type="text/javascript">
$(document).ready(function () {
});
</script>