<?php

use common\models\company\EmployeeCount;
use frontend\modules\analytics\models\ReportsStartForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\analytics\models\ReportsStartForm */

?>

<div class="reports-start-header">
    <h4>
        Завершение регистрации
    </h4>
</div>

<div>
    Просьба проверить ваш e-mail:
    <strong>
        <?= Html::encode($model->employee->email) ?>
    </strong>
</div>

<div class="mt-4">
    Мы отправили вам 4-х значный код, для подтверждения вашего e-mail.
    Для продолжения, нужно ввести данный код ниже
</div>

<?php $form = ActiveForm::begin([
    'id' => 'code-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'action' => ['code'],
    'options' => [
        'class' => 'mt-4',
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <?= Html::activeTextInput($model, 'code[0]', [
        'class' => 'confirm-code-input form-control d-inline text-center mr-2',
        //'minlength' => 1,
        //'maxlength' => 1,
        'style' => 'width: 50px;'
    ]).Html::activeTextInput($model, 'code[1]', [
        'class' => 'confirm-code-input form-control d-inline text-center mr-2',
        //'minlength' => 1,
        //'maxlength' => 1,
        'style' => 'width: 50px;'
    ]).Html::activeTextInput($model, 'code[2]', [
        'class' => 'confirm-code-input form-control d-inline text-center mr-2',
        //'minlength' => 1,
        //'maxlength' => 1,
        'style' => 'width: 50px;'
    ]).Html::activeTextInput($model, 'code[3]', [
        'class' => 'confirm-code-input form-control d-inline text-center',
        //'minlength' => 1,
        //'maxlength' => 1,
        'style' => 'width: 50px;'
    ]) ?>

    <?php if ($error = $model->getFirstError('code')) : ?>
        <div class="text-danger">
            <?= $error ?>
        </div>
    <?php endif ?>

    <div class="mt-4">
        <?= Html::submitButton('Подтвердить', [
            'class' => 'button-regular button-hover-transparent min-w-130',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<script type="text/javascript">
$(document).ready(function(){
    $("input.confirm-code-input").on('keypress input', function(e) {
        console.log(this.value);
        this.value = this.value.substr(-1);
        if(this.value.length) {
            $(this).next("input.confirm-code-input").focus();
        }
    });
});
</script>