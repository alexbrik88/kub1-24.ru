<?php

use common\models\company\CompanyInfoIndustry;
use common\models\company\CompanyInfoPlace;
use common\models\employee\EmployeeInfoRole;
use frontend\components\Icon;
use frontend\modules\analytics\models\ReportsStartForm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\analytics\models\ReportsStartForm */

$siteIds = CompanyInfoIndustry::find()->select('id')->where(['need_site' => 1])->column();
$shopIds = CompanyInfoIndustry::find()->select('id')->where(['need_shop' => 1])->column();
$storeIds = CompanyInfoIndustry::find()->select('id')->where(['need_storage' => 1])->column();
$showSite = in_array($model->info_industry_id, $siteIds);
$showShop = in_array($model->info_industry_id, $shopIds);
$showStore = in_array($model->info_industry_id, $storeIds);
?>

<div class="reports-start-header">
    <h4>
        Данные необходимые для создания аккаунта
    </h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'account-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'action' => ['account'],
    'options' => [
        'class' => 'mt-4',
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <?= $form->field($model, 'info_place_id')->widget(Select2::class, [
                //'hideSearch' => true,
                'data' => CompanyInfoPlace::find()->select('name')->indexBy('id')->column(),
                'options' => [
                    // 'class' => '',
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '100%',
                ],
            ]); ?>
        </div>
        <div class="col-md-6 col-sm-12"></div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <?= $form->field($model, 'info_role_id')->widget(Select2::class, [
                'hideSearch' => true,
                'data' => EmployeeInfoRole::find()->select('name')->indexBy('id')->orderBy('sort')->column(),
                'options' => [
                    // 'class' => '',
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '100%',
                ],
            ])->label($model->getAttributeLabel('info_role_id').Icon::get('question', [
                'class' => 'tooltip-question-icon ml-2',
                'style' => 'margin-top: -3px; margin-bottom: -3px;',
                'title' => 'Данная информация поможет нам предложить наиболее подходящие отчеты. '.
                           'При необходимости, вы сможете изменить вашу роль в настройках',
            ])); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <?= $form->field($model, 'info_industry_id')->widget(Select2::class, [
                'hideSearch' => true,
                'data' => CompanyInfoIndustry::find()->select('name')->orderBy('[[id]] = :other asc, [[name]] asc')->addParams([
                    ':other' => CompanyInfoIndustry::OTHER,
                ])->indexBy('id')->column(),
                'options' => [
                    // 'class' => '',
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '100%',
                ],
            ])->label($model->getAttributeLabel('info_industry_id').Icon::get('question', [
                'class' => 'tooltip-question-icon ml-2',
                'style' => 'margin-top: -3px; margin-bottom: -3px;',
                'title' => 'Для разных отраслей применяются различные отчеты и аналитические коэффициенты. '.
                           'При необходимости, вы сможете изменить вашу роль в настройках',
            ])); ?>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="collapse-info_sites_count collapse <?= $showSite ? 'show' : '' ?>">
                        <?= $form->field($model, 'info_sites_count')->textInput([
                            'disabled' => !$showSite,
                            'type' => 'number',
                            'min' => 0,
                            'step' => 1,
                        ]) ?>
                    </div>
                    <div class="collapse-info_shops_count collapse <?= $showShop ? 'show' : '' ?>">
                        <?= $form->field($model, 'info_shops_count')->textInput([
                            'disabled' => !$showShop,
                            'type' => 'number',
                            'min' => 0,
                            'step' => 1,
                        ]) ?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="collapse-info_storage_count collapse <?= $showStore ? 'show' : '' ?>">
                        <?= $form->field($model, 'info_storage_count')->textInput([
                            'disabled' => !$showStore,
                            'type' => 'number',
                            'min' => 0,
                            'step' => 1,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-4">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-hover-transparent min-w-130',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<script type="text/javascript">
$(document).ready(function () {
    var siteIds = [<?= implode(',', $siteIds) ?>];
    var shopIds = [<?= implode(',', $shopIds) ?>];
    var storeIds = [<?= implode(',', $storeIds) ?>];
    $("input.confirm-code-input").on('keypress input', function(e) {
        this.value = this.value.charAt(0);
        if(this.value.length) {
            $(this).next("input.confirm-code-input").focus();
        }
    });
    $('#account-form .collapse').collapse({
        toggle: false
    });
    $(document).on("change", "#reportsstartform-info_industry_id", function () {
        var form = this.form;
        var val = parseInt(this.value);
        console.log(val);
        console.log(siteIds.indexOf(val));
        console.log(shopIds.indexOf(val));
        console.log(storeIds.indexOf(val));
        if (siteIds.indexOf(val) === -1) {
            $(".collapse-info_sites_count", form).collapse("hide").find("input").prop("disabled", true);
        } else {
            $(".collapse-info_sites_count", form).collapse("show").find("input").prop("disabled", false);
        }
        if (shopIds.indexOf(val) === -1) {
            $(".collapse-info_shops_count", form).collapse("hide").find("input").prop("disabled", true);
        } else {
            $(".collapse-info_shops_count", form).collapse("show").find("input").prop("disabled", false);
        }
        if (storeIds.indexOf(val) === -1) {
            $(".collapse-info_storage_count", form).collapse("hide").find("input").prop("disabled", true);
        } else {
            $(".collapse-info_storage_count", form).collapse("show").find("input").prop("disabled", false);
        }
    });
});
</script>