<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.11.2017
 * Time: 8:24
 */

use yii\bootstrap\Dropdown;
use frontend\modules\analytics\models\DynamicReportSearch;
use yii\helpers\Json;
use miloschuman\highcharts\HighchartsAsset;
use common\components\grid\GridView;
use yii\data\ArrayDataProvider;
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\widgets\Pjax;

/* @var \yii\web\View $this
 * @var DynamicReportSearch $searchModel
 * @var $highChartsOptions []
 * @var ArrayDataProvider $arrayDataTable
 */

$this->title = 'По динамике задолженности';

$highChartsOptions = Json::encode($highChartsOptions);

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-sm-3 col-sm-offset-9">
            <div style="position: relative;">
                <div class="btn default p-t-7 p-b-7" data-toggle="dropdown" style="width: 100%">
                    <?= $searchModel->getPeriodName(); ?> <b class="fa fa-angle-down"></b>
                </div>
                <?= Dropdown::widget([
                    'items' => $searchModel->getPeriodItems(),
                ]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="high-charts-container" style="min-height: 400px;"></div>
    </div>

    <script type="text/javascript">
        window.highchartsOptions = <?= $highChartsOptions; ?>;
    </script>

<?php Pjax::begin([
    'timeout' => 10000,
]); ?>
    <div class="portlet box darkblue">
        <div class="portlet-title float-left">
            <div class="caption ">
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $arrayDataTable,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $arrayDataTable->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'period_date',
                            'label' => 'Дата',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return DateHelper::format($data['period_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'current_invoice_sum',
                            'label' => 'Текущие неоплаченные счета',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['current_invoice_sum'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_1_10',
                            'label' => '1-10 дней просрочено',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_1_10'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_11_30',
                            'label' => '11-30 дней просрочено',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_11_30'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_31_60',
                            'label' => '31-60 дней просрочено',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_31_60'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_61_90',
                            'label' => '61-90 дней просрочено',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_61_90'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_more_90',
                            'label' => 'Больше 90 дней просрочено',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_more_90'], 2);
                            },
                        ],
                        [
                            'attribute' => 'debt_all',
                            'label' => 'Вся задолженность',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data['debt_all'], 2);
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>
<?php
$js = <<<JS
function createHighcharts() {
    Highcharts.chart('high-charts-container', window.highchartsOptions);
}
createHighcharts();
JS;

$this->registerJs($js);

