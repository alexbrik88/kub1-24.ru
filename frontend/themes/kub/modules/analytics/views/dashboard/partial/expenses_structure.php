<?php
$categories = ['Себест-ть товара', 'Зарплата', 'Аренда', 'Транспорт', 'Прочее'];
$fact = [40000, 35000, 25000, 20000, 15000];
$plan = [35000, 35000, 20000, 20000, 10000];
?>
<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-expenses-structure',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'inverted' => true
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                ],
                'grouping' => false,
                'shadow' => false,
                'borderWidth' => 0
            ]
        ],
        'tooltip' => [
            'shared' => true
        ],
        'title' => [
            'text' => 'Структура расходов',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => $categories],

        ],
        'series' => [
            ['name' => 'Факт', 'pointPadding' => 0.4, 'data' => $fact, 'color' => 'rgba(247,163,92,1)'],
            ['name' => 'План', 'pointPadding' => 0.25, 'data' => $plan, 'color' => 'rgba(0,0,0,.25)']
        ]
    ],
]); ?>