<?php
$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX + 40,y:point.plotY + 40}; }
JS;

$htmlHeader = <<<HTML
    
    <table class="ht-in-table">
        <tr>
            <th>Просрочка на</th>
            <th colspan="2">{point.x}</th>
        </tr>
HTML;
$htmlData = <<<HTML
        <tr>
            <td><div class="ht-title">{period}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart green" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
HTML;

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

$jsFormatterFunc = <<<JS

function (a) {
    
      if(1 == this.point.series.index) 
          return false;
    
      var idx = this.point.index;
      var tooltipHtml;

      var arr = [
          [
              {period: '0-10 дней', percent: 100, sum: "100 000,00"},
              {period: '11-30 дней', percent: 75, sum: "75 000,00"},
              {period: '31-60 дней', percent: 50, sum: "50 000,00"},
              {period: '61-90 дней', percent: 40, sum: "40 000,00"},
              {period: 'Больше 90 дней', percent: 40, sum: "40 000,00"},            
          ],
          [
              {period: '0-10 дней', percent: 100, sum: "90 000,00"},
              {period: '11-30 дней', percent: 90, sum: "50 000,00"},              
              {period: '31-60 дней', percent: 60, sum: "20 000,00"},
              {period: '61-90 дней', percent: 25, sum: "15 000,00"},
              {period: 'Больше 90 дней', percent: 20, sum: "10 000,00"}
          ],
          [
              {period: '0-10 дней', percent: 100, sum: "100 000,00"},
              {period: '11-30 дней', percent: 75, sum: "25 000,00"},
              {period: '31-60 дней', percent: 50, sum: "10 000,00"},
              {period: '61-90 дней', percent: 40, sum: "10 000,00"},
              {period: 'Больше 90 дней', percent: 40, sum: "5 000,00"},   
          ],
          [
              {period: '0-10 дней', percent: 50, sum: "50 000,00"},
              {period: '11-30 дней', percent: 25, sum: "25 000,00"},
              {period: '31-60 дней', percent: 10, sum: "10 000,00"},
              {period: '61-90 дней', percent: 10, sum: "10 000,00"},
              {period: 'Больше 90 дней', percent: 5, sum: " 5 000,00"},   
          ],
          [
              {period: '0-10 дней', percent: 50, sum: "50 000,00"},
              {period: '11-30 дней', percent: 25, sum: "25 000,00"},
              {period: '31-60 дней', percent: 10, sum: "10 000,00"},
              {period: '61-90 дней', percent: 10, sum: "10 000,00"},
              {period: 'Больше 90 дней', percent: 5, sum: " 5 000,00"},   
          ], 
      ];
      
      //console.log(arr[idx]);
      
      tooltipHtml = '$htmlHeader'.replace("{point.x}", "02.09.2019");
      if (arr[idx] != undefined) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '$htmlData'
               .replace("{period}", data.period)
               .replace("{percent}", data.percent)
               .replace("{sum}", data.sum);              
          });
      } else {
          tooltipHtml += '<tr><td colspan="3">Нет данных</td></tr>';
      }
      
      tooltipHtml += '$htmlFooter';

      return tooltipHtml.replace();
    }

JS;

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-debt').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = (((i==0) ? 0:0) + ((i==1) ? 3:0) + ((i==2) ? 3:0) + ((i==3) ? 3:0) + ((i==4) ? 3:0)); // todo: align right
        var left = chart.plotWidth - 43;
        
        if (data.dataLabel.text.textStr == '0')
            left += 31;
        else 
            left += offset;
        
        console.log(data.dataLabel.text.textStr);
        data.dataLabel.attr({
            x: left
        });
    });
}
JS;


?>

<div class="ht-caption">
    НАМ ДОЛЖНЫ
</div>
<table class="ht-table">
    <tr>
        <td class="gray pad-10">Текущая задолженность</td>
        <td class="bold">1 500 900,00</td>
    </tr>
    <tr>
        <td class="red">Просрочка дней</td>
        <td></td>
    </tr>
    <tr class="bb">
        <td>0-10 дней</td>
        <td>0,00</td>
    </tr>
    <tr class="bb">
        <td>11-30 дней</td>
        <td>2 744,00</td>
    </tr>
    <tr class="bb">
        <td>31-60 дней</td>
        <td>0,00</td>
    </tr>
    <tr class="bb">
        <td>61-90 дней</td>
        <td>0,00</td>
    </tr>
    <tr class="bb">
        <td>Больше 90 дней</td>
        <td>48 556,50</td>
    </tr>
    <tr>
        <td class="red">Просроченая задолженность</td>
        <td>1 301 128,00</td>
    </tr>
    <tr>
        <td class="gray pad-10">ИТОГО задолженность</td>
        <td class="bold">2 300 456,00</td>
    </tr>
</table>

<div class="devider mt-2 mb-2"></div>

<?php
$categories = ['ООО "ОГИМА"', 'ООО "РОКФОЛ"', 'ООО "РЕМПРОМ"', 'ООО "АЛИКОВ"', 'Ип Сатыренко'];
$debt = [305000, 185000, 150000, 100000, 100000];
?>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-debt',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
        'modules/pattern-fill'
    ],
    'options' => [
        'title' => [
            'text' => 'ТОП 5 Должников',
            'align' => 'left',
            'floating' => false,
            'style' => [
                'font-size' => '12px',
                'color' => '#9198a0',
            ],
            'x' => 0,
        ],
        'credits' => [
            'enabled' => false
        ],
        'legend' => [
            'enabled' => false
        ],
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'inverted' => true,
            'height' => 180,
            'spacing' => [0,0,0,0],
            'events' => [
                'load' => new \yii\web\JsExpression($jsLoadFunc),
                'redraw' => new \yii\web\JsExpression($jsLoadFunc),
            ]
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                ],
                'grouping' => false,
                'shadow' => false,
                'borderWidth' => 0
            ]
        ],
        'tooltip' => [
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'positioner' => new \yii\web\JsExpression($jsPositionerFunc),
            'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
            'useHTML' => true,
            //'shape' => 'rect',
        ],
        'yAxis' => [
            'min' => 0,
            'index' => 0,
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'title' => '',
            'labels' => false,
            'max' => 570000, // TEMP FOR ANIMATION!
        ],
        'xAxis' => [
            'categories' => $categories,
            'labels' => [
                'useHTML' => true,
                'align' => 'left',
                'padding' => 100,
                'style' => [
                    'width' => '100px'
                ],
                'reserveSpace' => true,
                'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
            ],
            'gridLineWidth' => 0,
            'minorGridLineWidth' => 0,
            'lineWidth' => 0,
            'offset' => 0
        ],
        'series' => [
            [
                'name' => 'Сумма долга',
                'data' => $debt,
                'color' => 'rgba(105,195,160,1)',
                'states' => getPatternHover('rgba(105,195,160,1)'),
                'pointWidth' => 18,
                'borderRadius' => 3
            ],
        ]
    ],
]); ?>

<a href="#" class="link" style="font-size: 13px;padding:10px 0;">Посмотреть все</a>

<script>
    $(document).ready(function() {

    })
</script>