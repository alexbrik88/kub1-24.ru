<?php

$categories = ['3 кв. 2018', '4 кв. 2018', '1 кв. 2019', '2 кв. 2019'];
$actives = [110000000, 120000000, 130000000, 130000000];
$profit  = [90000000, 100000000, 110000000, 110000000];

?>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-actives-and-profit',
    'scripts' => [
        'modules/exporting',
        'modules/pattern-fill',
        'themes/grid-light',
    ],
    'options' => [
        'exporting' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
        ],
        'tooltip' => [
            'shared' => true
        ],
        'title' => [
            'text' => 'Активы и чистая прибыль',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => $categories],

        ],
        'series' => [
            [
                'name' => 'Активы',
                'data' => $actives,
                'color' => 'rgba(93,173,226,1)',
                'states' => [
                    'hover' => [
                        'color' => 'url(#patternActivesAndProfit)'
                    ]
                ]
            ],
            [
                'type' => 'line',
                'name' => 'Чистая прибыль',
                'data' => $profit,
                'color' => '#666'
            ],
        ],
    ],
]); ?>