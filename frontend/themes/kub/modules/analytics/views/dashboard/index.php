<?php

use frontend\modules\analytics\models\PaymentCalendarSearch;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Дашборд';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);

function getPatternHover($color) {
    return ['hover' => [
        'color' => [
            'pattern' => [
                'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                'color' => $color,
                'width' => 5,
                'height' => 5
            ]
        ]
    ]
    ];
}

?>

<style>
    .analytics-dashboard > .row > .col-3,
    .analytics-dashboard > .row > .col-6 {
        margin-bottom:10px;
    }
    .pad-5 {
        padding-left:5px;
        padding-right:5px;
    }
    #chart-plan-fact-income-outcome {
        height: 150px;
    }
    #chart-plan-fact-income-outcome-2 {
        height: 150px;
    }
    #chart-actives-and-profit,
    #chart-actives-structure,
    #chart-passives-structure {
        height: 300px;
    }

    #chart-revenue-structure,
    #chart-expenses-structure {
        height: 345px;
    }

    #chart-revenue-and-gross-profit,
    #chart-expenses {
        height: 290px;
        border: 1px dashed #999;
    }
    .chart-in-table {
        border: 1px dashed #999;
        margin-bottom:10px;
    }
    .chart-in-table > table {
        width:100%;
        margin: 5px 0 10px 0;
    }
    .chart-in-table > table td {
        padding:0 10px;
        font-weight:bold;
        font-size:14px;
    }

    #chart-plan-fact-income-outcome {
        border-top: 1px dashed #999;
        border-left: 1px dashed #999;
        border-right: 1px dashed #999;
    }
    #chart-plan-fact-income-outcome-2 {
        border-bottom: 1px dashed #999;
        border-left: 1px dashed #999;
        border-right: 1px dashed #999;
    }
    #chart-actives-and-profit,
    #chart-revenue-structure,
    #chart-expenses-structure,
    #chart-actives-structure,
    #chart-passives-structure {
        border: 1px dashed #999;
    }
    #table-debt {
        height: 300px;
        border: 1px dashed #999;
    }
    #table-debt table {
        width:100%;

    }
    #table-debt table td {
        padding:8px;
        vertical-align: middle;
        height: 60px;
    }
    #table-debt table td.caption {
        font-size: 13px;
        font-weight:bold;
        max-width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    #table-debt table td.value {
        font-size: 13px;
        font-weight: bold;
        vertical-align: middle;
        text-align: right;
    }
    #table-debt table tr:nth-child(1),
    #table-debt table tr:nth-child(2),
    #table-debt table tr:nth-child(3),
    #table-debt table tr:nth-child(4){
        border-bottom: 1px dashed #999;
    }

</style>

<div class="analytics-dashboard wrap">

    <h4 class="page-title"><?= $this->title; ?></h4>

    <div class="row mb-3">
        <div class="col-8 p-1">
            <div class="row row_indents_m">
                <div class="col-6">
                    <div class="page-border border-radius-4 pt-2 pl-3 pr-3 pb-2 flex-grow-1">
                        <?= $this->render('partial/sells', []) ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="page-border border-radius-4 pt-2 pl-3 pr-3 pb-2 flex-grow-1">
                        <?= $this->render('partial/debt', []) ?>
                    </div>
                </div>
                <div class="col-6">
                    <br>
                    <button id="chart-sells-refresh" class="button-regular  button-hover-content-red">Обновить</button>
                </div>
                <div class="col-6">
                    <br>
                    <button id="chart-debt-refresh" class="button-regular  button-hover-content-red">Обновить</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 p-1">
            <?= $this->render('partial/plan_fact_income_outcome', []) ?>
        </div>
        <div class="col-3 p-1">
            <?= $this->render('partial/actives_and_profit', []) ?>
        </div>
        <div class="col-3 p-1">
            <?= $this->render('partial/table_debt', []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3 p-1">
            <?= $this->render('partial/revenue_structure', []) ?>
        </div>
        <div class="col-3 p-1">
            <?= $this->render('partial/revenue_and_gross_profit', []) ?>
        </div>
        <div class="col-3 p-1">
            <?= $this->render('partial/expenses', []) ?>
        </div>
        <div class="col-3 p-1">
            <?= $this->render('partial/expenses_structure', []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6 p-1">
            <?= $this->render('partial/actives_structure', []) ?>
        </div>
        <div class="col-6 p-1">
            <?= $this->render('partial/passives_structure', []) ?>
        </div>
    </div>

    <div class="row">
        <div class="p-1" style="width:99.2%; border: 1px dashed #999; margin-left:4px;">
            <?php
            $chartSearchModel = new PaymentCalendarSearch();
            // Chart
            echo $this->render('partial/_payment_calendar_chart_plan_fact', [
                'model' => $chartSearchModel,
                'currYear' => date('Y')
            ]); ?>
        </div>
    </div>

    <svg xmlns='http://www.w3.org/2000/svg' width='0' height='0'>
        <defs>
            <pattern id="patternActivesAndProfit" width="10" height="10" patternUnits="userSpaceOnUse">
                <rect width='10' height='10' fill='rgba(93,173,226,1)'/>
                <path d='M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11' stroke='white' stroke-width='1.25'/>
            </pattern>
        </defs>
    </svg>

</div>

<script>
    $(document).ready(function() {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        $('#chart-debt-refresh').on('click', function() {
            var chart = $('#chart-debt').highcharts();
            var data = [305000, 185000, 150000, 100000, 100000];
            chart.series[0].setData([0,0,0,0,0]);
            function setChartData() {
                chart.series[0].setData(data);
            }
            setTimeout(setChartData, 1500);
        });

        $('#chart-sells-refresh').on('click', function() {

            $('#chart-sells-refresh').prop('disabled', true);

            var data0 = [150000, 200000, 180000, 150000, 160000, 150000, 190000];
            var data1 = [170000, 220000, 180000, 150000, 170000, 180000, 200000];
            var chart = $('#chart-sells').highcharts();

            var charts2 = $('.ht-table-2').find('.ht-chart');
            var value0 = 601270;
            var value1 = 700950;


            var serie0 = chart.series[0];
            var serie1 = chart.series[1];

            // reset
            serie0.setData();
            serie1.setData();
            charts2.width(0);

            var i=0;
            var step0 = 5000;
            var threshold = 50000;
            var newData0 = [0, 0, 0, 0, 0, 0, 0];
            var newData1 = [];

            var runAnimation = [1,0,0,0,0,0,0,0];

            var totalSetsI = 0;
            var totalSets = 727; // from experiment

            function showCharts() {
                $(data0).each(function(j,k) {
                    if (newData0[j] > threshold)
                        runAnimation[j+1] = 1;

                    if (runAnimation[j])
                        newData0[j] += step0;

                    if (newData0[j] >= data0[j]) {
                        newData0[j] = data0[j];
                        i = Math.max(i, j);
                        //console.log(i);
                    }

                    if (i === 6) {
                        clearInterval(window.chartTimerId);
                        window.chartTimerId = setInterval(showCharts2, 100);
                        console.log("totalSets: " + totalSetsI);
                        i = 0;

                        return;
                    }
                    serie0.setData(newData0);
                    totalSetsI++;

                    // chart2
                    $(charts2).first().width(Math.round(totalSetsI/totalSets*100) + "%");
                    $(charts2).last().width(Math.round(0.7 * totalSetsI/totalSets*100) + "%");
                });
            }

            function showCharts2() {
                newData1[i] = data1[i];

                serie1.setData(newData1);
                //console.log(i);

                if (i === 6) {
                    clearInterval(window.chartTimerId);

                    $('#chart-sells-refresh').prop('disabled', false);

                    return;
                }
                i++;
            }

            window.chartTimerId = setInterval(showCharts, 1);
        });

    });
</script>