<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\modules\analytics\controllers\DetailingController;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectEstimateSearch;

/* @var $this yii\web\View */
/* @var $model Project */
/* @var $tab string */
/* @var $type string */
/* @var $activeYear int */

$this->title = $model->name;

$tabLink = Url::toRoute(['/analytics/detailing/view-project', 'id' => $model->id]);

$tabFile = ArrayHelper::remove($tabData, 'tabFile');
$type = ArrayHelper::remove($tabData, 'type');

$user = Yii::$app->user->identity;
$company = $user->currentEmployeeCompany->company;

/** @var ProjectCustomer[] $customers */
if ($customers = $model->getCustomers()->all()) {
    $customer = array_shift($customers);
} else {
    $customer = null;
}

$estimate_income = (new ProjectEstimateSearch(['project_id' => $model->id]))->getQuery()->sum('income_estimate_sum');
$agreement_income = array_sum(ArrayHelper::getColumn($model->customers, 'amount'));

$logArray = Log::find()
    ->andWhere(['model_name' => $model->getClassName()])
    ->andWhere(['model_id' => $model->id])
    ->orderBy(['id' => SORT_ASC])
    ->all();
?>
    <a class="link mb-2" href="<?= Url::to(['/analytics/detailing/index', 'tab' => DetailingController::TAB_PROJECT]) ?>">
        Назад к списку
    </a>

    <!-- KUB -->
    <div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <div class="pr-2">
                        <div class="row align-items-center justify-content-between mb-3">
                            <h4 class="column mb-2" style="max-width: 550px;"><?= Html::encode($this->title); ?></h4>
                            <div class="column" style="margin-bottom: auto;">
                                <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                        type="button"
                                        data-toggle="modal"
                                        title="Последние действия"
                                        href="#basic">
                                    <svg class="svg-icon svg-icon_size_">
                                        <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                    </svg>
                                </button>
                                <a href="<?= Url::to(['/project/update', 'id' => $model->id]) ?>"
                                   class="button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                                   title="Изменить">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Дата начала</div>
                                    <?= DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Дата завершения</div>
                                    <?= DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Статус</div>
                                    <div id="projectStatuses">
                                        <?= kartik\select2\Select2::widget([
                                            'model' => $model,
                                            'attribute' => 'status',
                                            'options' => [
                                                'id' => 'project_status',
                                            ],
                                            'data' => Project::$statuses,
                                            'pluginOptions' => [
                                                'width' => '150px',
                                            ],
                                            'hideSearch' => true,
                                            'pluginLoading' => false,
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Номер проекта</div>
                                    <?= htmlspecialchars($model->number) . htmlspecialchars($model->addition_number) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Заказчик</div>
                                    <?php if ($customer): ?>
                                        <?= Html::a($customer->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $customer->customer->id, 'type' => Contractor::TYPE_CUSTOMER]), ['class' => 'link']); ?>
                                    <?php endif; ?>
                                    <?php if ($customers): ?>
                                        <div style="margin-bottom: -18px">
                                            <div class="tooltip-black font-14 link pointer" data-tooltip-content="#tooltip_project_other_customers">
                                                + еще <?= count($customers) ?>
                                            </div>
                                            <div style="display:none">
                                                <div id="tooltip_project_other_customers">
                                                    <?php foreach ($customers as $c): ?>
                                                        <div class="pl-2 pr-2 pb-1 pt-1">
                                                            <?= Html::a($c->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $c->customer->id, 'type' => Contractor::TYPE_CUSTOMER]), ['class' => 'link']); ?>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Направление</div>
                                    <?= ($model->industry) ? $model->industry->name : '—'; ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Отвественный</div>
                                    <?php
                                    $employee = \common\models\EmployeeCompany::find()
                                        ->where(['employee_id' => $model->responsible, 'company_id' => $company->id])
                                        ->one();
                                    ?>
                                    <?= ($employee) ? ($employee->lastname . ' '
                                        . $employee->firstname_initial . '.'
                                        . $employee->patronymic_initial . '.') : '--'; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Сумма по договору</div>
                                    <?php if ($agreement_income > 0) {
                                        echo '<br/>' . Html::tag('span', TextHelper::invoiceMoneyFormat($agreement_income, 2) . ' ₽', ['title' => 'Сумма по договорам']);
                                    } ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Сумма по смете</div>
                                    <?php if ($estimate_income > 0) {
                                        echo '<br/>' . Html::tag('span', TextHelper::invoiceMoneyFormat($estimate_income, 2) . ' ₽', ['title' => 'Сумма по сметам с НДС']);
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 column pl-0">
                    <div class="mb-1 text-right">
                        <?= $this->render('../_common/_select_year', [
                            'company' => $model->company,
                            'year' => $activeYear,
                            'tab' => $tab
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs-row">
        <?= Nav::widget([
            'id' => 'contractor-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-2 pb-1'],
            'items' => [
                [
                    'label' => 'Дашборд',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DASHBOARD,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_DASHBOARD ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'ОДДС',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_ODDS,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_ODDS ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'ОПиУ',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_PAL,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_PAL ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Нам должны',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DEBTOR . '&type=2',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . (($tab == DetailingController::TAB_VIEW_DEBTOR && $type == 2) ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Мы должны',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DEBTOR . '&type=1',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . (($tab == DetailingController::TAB_VIEW_DEBTOR && $type == 1) ? ' active' : '')
                    ],
                ],
            ],
        ]); ?>
    </div>

<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab active">
            <?= $this->render("{$tabFile}", $tabData); ?>
        </div>
    </div>
<?php endif ?>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>
    <table class="table table-style">
        <thead>
        <tr>
            <th>Дата</th>
            <th>Тип</th>
            <th>Сотрудник</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= DateHelper::format(date('Y-m-d', $model->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
            <td>Создан</td>
            <td><?= $model->getResponsibleEmployee()->one()->fio; ?></td>
        </tr>
        <?php foreach ($logArray as $log): ?>
            <tr>
                <td><?= DateHelper::format(date('Y-m-d', $log->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></td>
                <td><?= ArrayHelper::getValue(DocumentLogWidget::$eventsF, $log->log_event_id) ?></td>
                <td><?= $log->employeeCompany ? $log->employeeCompany->fio : ($log->employee ? $log->employee->fio : '') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php Modal::end(); ?>

<?= $this->render('partial/modals') ?>