<?php
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\employee\Config;
use frontend\components\StatisticPeriod;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use frontend\themes\kub\widgets\SummarySelectContractorWidget;
use common\models\project\Project;
use common\models\project\ProjectSearch;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel ProjectSearch */
/* @var $userConfig Config */
/* @var $palModel \frontend\modules\analytics\models\ProfitAndLossSearchModel */
/* @var $analyticsModel \frontend\modules\analytics\models\AnalyticsSimpleSearch */

$this->title = 'Проекты';

$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;
$company = $user->company;

$period = StatisticPeriod::getSessionName();

$exists = Project::find()
    ->andWhere(['company_id' => $company->id])
    ->exists();

if ($exists) {
    $emptyMessage = "В выбранном периоде «{$period}», у вас нет проектов. Измените период, чтобы увидеть имеющиеся проекты.";
} else {
    $emptyMessage = 'Вы еще не добавили ни одного проекта. '
        . Html::a('Создать проект', '/project/create', ['class' => 'link'])
        . '.';
}

$tabViewClass = $userConfig->getTableViewClass('table_view_detailing');
$tabConfig = [
    'date' => $user->config->report_detailing_industry_date,
    'income' => $user->config->report_detailing_industry_income,
    'expense' => $user->config->report_detailing_industry_expense,
    'revenue' => $user->config->report_detailing_industry_revenue,
    'margin' => $user->config->report_detailing_industry_margin,
    'net_income_loss' => $user->config->report_detailing_industry_net,
    'undistributed_profit' => $user->config->report_detailing_industry_profit,
];

$showHelpPanel = $userConfig->report_odds_help ?? false;
$showChartPanel = $userConfig->report_odds_chart ?? false;
?>

<div class="stop-zone-for-fixed-elems project-index">
    <!-- HEADER -->
    <div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom:12px;">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="column mr-auto">
                    <h4 class="mb-2"><?= $this->title ?></h4>
                </div>
                    <div class="column pl-1 pr-2">
                        <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                            [
                                'id' => 'btnChartCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#chartCollapse',
                                'data-tooltip-content' => '#tooltip_chart_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>
                    <div class="column pl-1 pr-2">
                        <?= Html::button(Icon::get('book'),
                            [
                                'id' => 'btnHelpCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#helpCollapse',
                                'data-tooltip-content' => '#tooltip_help_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>

                    <div class="column pl-1 pr-0 select2-wrapper" style="margin-top:-9px">
                        <?= frontend\widgets\RangeButtonWidget::widget([
                            'byMonths' => true
                        ]); ?>
                    </div>
            </div>
        </div>

        <div style="display: none">
            <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
            <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
        </div>

    </div>

    <div class="jsSaveStateCollapse wrap p-0 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_odds_chart">
        <?= $this->render('../_charts/_index_charts', [
            'company' => $company,
            'palModel' => $palModel,
            'analyticsModel' => $analyticsModel,
            'onPage' => $onPage,
        ]) ?>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_odds_help">
        <div class="pt-4 pb-3">
            <span class="text-justify">На этой странице вы можете увидеть детализацию вашего бизнеса по проектам,
                проанализировать движение денежных средств, прибыль и рентабельность, дебиторскую и кредиторскую задолженность,
                состояние запасов по проектам, а также кредиты и займы. Это позволит оценить какие проекты приносят вам прибыль,
                а какие съедают ваши деньги.
            </span>
        </div>
    </div>
    <!-- END OF HEADER -->

    <div class="table-settings row row_indents_s" style="margin-top:0!important;">

        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'project_number'],
                    ['attribute' => 'project_end_date'],
                    ['attribute' => 'project_direction'],
                    ['attribute' => 'project_before_date'],
                    ['attribute' => 'project_clients'],
                    ['attribute' => 'project_income'],
                    ['attribute' => 'project_expense'],
                    ['attribute' => 'project_result_sum'],
                    ['attribute' => 'project_profit'],
                    ['attribute' => 'project_profitability'],
                    ['attribute' => 'project_responsible'],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_detailing']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'searchProject', [
                        'type' => 'search',
                        'placeholder' => 'Название проекта',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'project-pjax-container',
        'enablePushState' => false,
        'scrollTo' => false
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($model) {

                    $income = round($model['income_sum'] / 100, 2);
                    $expense = round($model['expense_sum'] / 100, 2);
                    $profit = round($model['pal_net_income_loss'] / 100, 2);
                    $revenue = round($model['pal_revenue'] / 100, 2);

                    return Html::checkbox('id[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $model['id'],
                        'data' => [
                            'copy-url' => Url::to(['create', 'copy' => $model['id']]),
                            'income' => $income,
                            'expense' => $expense,
                            'profit' => $profit,
                            'revenue' => $revenue
                        ]
                    ]);
                },
            ],
            [
                'attribute' => 'number',
                'label' => '№ проекта',
                'headerOptions' => [
                    'class' => 'sorting col_project_number' . ($userConfig->project_number ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'document_number col_project_number' . ($userConfig->project_number ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $number =  $model['number'] . $model['addition_number'];
                    return Html::a($number, ['/project/view', 'id' => $model['id']], ['class' => 'link']);
                },
            ],
            [
                'attribute' => 'start_date',
                'label' => 'Дата начала',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'value' => function ($model) {
                    return DateHelper::format($model['start_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'end_date',
                'label' => 'Дата окончания',
                'headerOptions' => [
                    'class' => 'col_project_end_date sorting' . ($userConfig->project_end_date ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_end_date' . ($userConfig->project_end_date ? '' : ' hidden'),
                ],
                'value' => function ($model) {
                    return DateHelper::format($model['end_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'name',
                'label' => 'Название проекта',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '180px',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model['name']), ['view-project', 'id' => $model['id']], ['class' => 'link', 'data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => 'sorting col_project_direction' . ($userConfig->project_direction ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_direction' . ($userConfig->project_direction ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getIndustryFilter(),
                's2width' => '250px',
                'format' => 'html',
                'value' => function ($model) {

                    if ($industry = \common\models\company\CompanyIndustry::findOne($model['industry_id']))
                        return $industry->name;

                    return '';
                },
            ],
            [
                'attribute' => 'before_date',
                'label' => 'Дней до окончания',
                'headerOptions' => [
                    'class' => 'sorting col_project_before_date sorting' . ($userConfig->project_before_date ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_before_date' . ($userConfig->project_before_date ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model['status'] == Project::STATUS_INPROGRESS) ?
                        Html::tag('span', $model['before_date'], [
                            'style' => ($model['before_date'] < 0) ? 'color: red' : ''
                        ]) : 0;
                },
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => 'nowrap',
                ],
                'filter' => $searchModel->getStatusFilter(),
                's2width' => '150px',
                'value' => function ($model) {
                    return $model['status'] === Project::STATUS_INPROGRESS
                        ? Project::STRING_INPROGRESS
                        : Project::STRING_CLOSED;
                },
            ],
            [
                'attribute' => 'clients',
                'label' => 'Заказчик',
                'headerOptions' => [
                    'class' => 'col_project_clients' . ($userConfig->project_clients ? '' : ' hidden'),
                    'width' => '200px',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell col_project_clients' . ($userConfig->project_clients ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getClientsFilter(),
                's2width' => '250px',
                'format' => 'raw',
                'value' => function ($model) {

                    $result = '';
                    $clients = $model['clients'];

                    if (empty($clients)) {
                        return $result;
                    }

                    $client = array_shift($clients);

                    $result .= Html::a(htmlspecialchars($client['name']), Url::to(['/contractor/view', 'id' => $client['id'], 'type' => $client['type']]), ['class' => 'link']) . '<br>';

                    if ($clients) {

                        $clientsTooltip = '';
                        foreach ($clients as $c) {
                            $link = Html::a(htmlspecialchars($c['name']), Url::to(['/contractor/view', 'id' => $c['id'], 'type' => $c['type']]), ['class' => 'link']) . '<br>';
                            $clientsTooltip .= Html::tag('div', $link, ['class' => 'pl-2 pr-2 pb-1 pt-1']);

                        }

                        $result .= Html::tag('div', '+ еще ' . count($clients), [
                            'class' => 'tooltip-project-clients font-14 link pointer',
                            'data-tooltip-content' => ".tooltip_project_other_clients[data-project={$model['id']}]"
                        ]);

                        $result .= Html::tag('div', Html::tag('div', $clientsTooltip, [
                            'class' => 'tooltip_project_other_clients',
                            'data-project' => $model['id']
                        ]), ['style' => 'display:none']);
                    }

                    return $result;
                },
            ],
            [
                'attribute' => 'income_sum',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => 'sorting col_project_income' . ($userConfig->project_income ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'nowrap text-right col_project_income' . ($userConfig->project_income ? '' : ' hidden'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['income_sum'], 2);
                },
            ],
            [
                'attribute' => 'expense_sum',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => 'sorting col_project_expense' . ($userConfig->project_expense ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'nowrap text-right col_project_expense' . ($userConfig->project_expense ? '' : ' hidden'),
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['expense_sum'], 2);
                },
            ],
            [
                'attribute' => 'pal_revenue',
                'label' => 'Выручка',
                'headerOptions' => [
                    'class' => 'col_project_result_sum sorting' . ($userConfig->project_result_sum ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'nowrap text-right col_project_result_sum' . ($userConfig->project_result_sum ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_revenue'], 2);
                },
            ],
            [
                'attribute' => 'pal_net_income_loss',
                'label' => 'Прибыль',
                'headerOptions' => [
                    'class' => 'col_project_profit sorting' . ($userConfig->project_profit ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'nowrap text-right col_project_profit' . ($userConfig->project_profit ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['pal_net_income_loss'], 2);
                },
            ],
            [
                'attribute' => 'pal_profitability',
                'label' => 'Рентабельность',
                'headerOptions' => [
                    'class' => 'col_project_profitability sorting' . ($userConfig->project_profitability ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'nowrap text-right col_project_profitability' . ($userConfig->project_profitability ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return TextHelper::numberFormat($model['pal_profitability'], 2) . ' %';
                },
            ],
            [
                'attribute' => 'responsible',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'col_project_responsible' . ($userConfig->project_responsible ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => 'col_project_responsible' . ($userConfig->project_responsible ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getResponsibleEmployeeFilter(),
                'selectPluginOptions' => [
                    'width' => '150px',
                ],
                'value' => function ($model) use ($userConfig) {
                    /* @var $employee EmployeeCompany */
                    $employee = EmployeeCompany::find()
                        ->joinWith('employee')
                        ->andWhere([
                            EmployeeCompany::tableName() . '.company_id' => $userConfig->employee->company_id,
                            EmployeeCompany::tableName() . '.employee_id' => $model['responsible'],
                            EmployeeCompany::tableName() . '.is_working' => Employee::STATUS_IS_WORKING,
                            Employee::tableName() . '.is_active' => Employee::ACTIVE,
                            Employee::tableName() . '.is_deleted' => Employee::NOT_DELETED,
                        ])->one();
                    if ($employee) {
                        return $employee->getShortFio();
                    }

                    return '';
                },
            ],
        ],
    ]); ?>

    <?php Pjax::end() ?>

</div>

<?= SummarySelectContractorWidget::widget([
    'buttons' => [
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-width button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align-sm']),
        Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', '#many-delete-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ])
    ],
]); ?>

<?= $this->render('partial/modals') ?>
<?= $this->render('../_common/_modals') ?>

<?= $this->registerJs('
    $(document).on("pjax:success", "#project-pjax-container", function(event) {
        $("#summary-container").removeClass("visible");
    });

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("close"));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("open"));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("close"));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("open"));
    });    
') ?>