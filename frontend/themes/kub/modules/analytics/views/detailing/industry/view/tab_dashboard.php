<?php

use yii\helpers\ArrayHelper;
use common\components\helpers\Month;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

/** @var OddsSearch $oddsModel */
/** @var ProfitAndLossSearchModel $palModel */
/** @var array $pageFilter */

$chartView = [
    // first row
    '@frontend/modules/analytics/views/finance/_charts/_chart_plan_fact_days',
    '@frontend/modules/analytics/views/detailing/_charts/_dashboard_pie_chart_income',
    '@frontend/modules/analytics/views/detailing/_charts/_dashboard_pie_chart_expense',
    // second row
    '@frontend/modules/analytics/views/finance/_charts/_chart_profit_loss_revenue',
    '@frontend/modules/analytics/views/detailing/_charts/_dashboard_pie_profit_loss_revenue'
];

///////////////////////////////////////////////////////////////
/// todo: move into one file (double with _chart_profit_loss.php)

/** @var $palModel ProfitAndLossSearchModel */

$pageFilter = $pageFilter ?? [
        'industry_id' => null,
        'sale_point_id' => null,
        'project_id' => null,
    ];

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 1; // to sync with plan-fact
$customPeriod = "months";
$customMonth = $customMonth ?? ($palModel->year == date('Y') ? date('m') : 12);
$customAmountKey = $customAmountKey ?? 'amount'; // amount||amountTax
$customChartsGroup = $customChartsGroup ?? null;
$dateFrom = $dateFrom ?? "{$oddsModel->year}-01-01";
$dateTo = $dateTo ?? "{$oddsModel->year}-12-31";
/////////////////////////////////////////

////// consts dynamics chart ////////////
$LEFT_DATE_OFFSET = 7;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
////// consts structures charts ////////
$chartMaxRows = 10;
///////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

$chart1 = [
    'totalRevenue' => [],
    'netIncomeLoss' => []
];

if ($customPeriod == "months") {

    $datePeriods = $palModel->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $chartPeriodsX = [];
    $chartLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $str_month =  $dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $chartPeriodsX[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

// 1. REVENUE

foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $year  = (int)$dateArr[0];
    $str_month =  $dateArr[1];

    if ($year.$str_month > date('Ym')) {
        $chart1['totalRevenue'][] = null;
        $chart1['netIncomeLoss'][] = null;
    } else {
        $chart1['totalRevenue'][] = round($palModel->getValue('totalRevenue', $year, $str_month, $customAmountKey) / 100, 2);
        $chart1['netIncomeLoss'][] = round($palModel->getValue('netIncomeLoss', $year, $str_month, $customAmountKey) / 100, 2);
    }
}

$periodText = trim(str_replace('за', '', OddsSearch::getSubtitlePeriod()));
///////////////////////////////////////////////////////////////
?>

<div class="wrap p-0">
    <div class="row m-0 pl-2 pt-2">
        <div class="col-lg-8 col-md-12 mb-2 pl-0 pr-2">
            <div class="kub-chart border pt-4 pl-2 pr-2 pb-2">
                <?= $this->render($chartView[0], [
                    'currYear' => $oddsModel->year,
                    'SHOW_PLAN' => true,
                    'ON_PAGE' => 'ODDS',
                    'PAGE_FILTER' => [
                        'industry_id' => $pageFilter['industry_id'],
                        'sale_point_id' => null,
                        'project_id' => null
                    ],
                    'LEFT_DATE_OFFSET' => 6,
                    'RIGHT_DATE_OFFSET' => 6,
                    'MOVE_OFFSET' => 1,
                    'customOffsetDays' => $customOffsetDays ?? 0,
                    'customOffsetMonths' => $customOffsetMonths ?? 0,
                ]); ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 mb-2 pl-0 pr-0">
            <div class="row m-0">
                <div class="col-lg-12 col-md-6 pl-0 pr-2">
                    <div class="kub-chart border mb-2" style="min-height:250px; padding-bottom: 3px;">
                        <div class="ht-caption" style="position: absolute; top:1.5rem; left:0.5rem;">
                            <b>СТРУКТУРА</b>
                            <span style="text-transform: none; font-weight:400;">
                                приходов <span class="text-pie-chart-period">
                                    <?= OddsSearch::getSubtitlePeriod($dateFrom, $dateTo) ?>
                                </span>
                            </span>
                        </div>
                        <?= $this->render($chartView[1], [
                            'oddsModel' => $oddsModel,
                            'pageFilter' => $pageFilter,
                            'id' => 'dashboard_pie_chart_income',
                            'title' => null,
                            'height' => 250,
                            'dateFrom' => $dateFrom ?? null,
                            'dateTo' => $dateTo ?? null,
                        ]) ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-6 pl-0 pr-2">
                    <div class="kub-chart border" style="min-height:250px; padding-bottom: 3px;">
                        <div class="ht-caption" style="position: absolute; top:1.5rem; left:0.5rem;">
                            <b>СТРУКТУРА</b>
                            <span style="text-transform: none; font-weight:400;">
                                расходов <span class="text-pie-chart-period">
                                    <?= OddsSearch::getSubtitlePeriod($dateFrom, $dateTo) ?>
                                </span>
                            </span>
                        </div>
                        <?= $this->render($chartView[2], [
                            'oddsModel' => $oddsModel,
                            'pageFilter' => $pageFilter,
                            'id' => 'dashboard_pie_chart_expense',
                            'title' => null,
                            'height' => 250,
                            'dateFrom' => $dateFrom ?? null,
                            'dateTo' => $dateTo ?? null,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 pl-2">
        <div class="col-lg-8 col-md-12 mb-2 pl-0 pr-2">
            <div class="kub-chart border pt-4 pl-2 pr-2 pb-2">
                <?= $this->render($chartView[3], [
                    'model' => $palModel,
                    // consts
                    'LEFT_DATE_OFFSET' => 6,
                    'RIGHT_DATE_OFFSET' => 6,
                    'MOVE_OFFSET' => 1,
                    // vars
                    'chartPeriodsX' => $chartPeriodsX,
                    'totalRevenue' => $chart1['totalRevenue'],
                    'netIncomeLoss' => $chart1['netIncomeLoss'],
                ]); ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 mb-2 pl-0 pr-2" style="overflow: auto">
            <div class="kub-chart border" style="min-width:335px; min-height:250px; padding-bottom:33px">
                <div class="ht-caption" style="position: absolute; top:1.5rem; left:0.5rem;">
                    <b>СТРУКТУРА</b>
                    <span style="text-transform: none; font-weight:400;">
                        <span class="text-pie-chart-period">
                            <?= OddsSearch::getSubtitlePeriod($dateFrom, $dateTo) ?>
                        </span>
                    </span>
                </div>
                <?= $this->render($chartView[4], [
                    'id' => 'dashboard_pie_chart_revenue',
                    'palModel' => $palModel,
                    'customAmountKey' => $customAmountKey,
                    'title' => null,
                    'height' => 250,
                    'startAngle' => 0,
                    'dateFrom' => $dateFrom ?? null,
                    'dateTo' => $dateTo ?? null,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<script>

    ChartDetailingPie = {
        PlanFact: null, // external object
        activeColumn: null,
        columnColor: {
            _pattern: function(color) { return {'pattern': {
                'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                'width': 5,
                'height': 5,
                'color': color,
            }}},
            income: 'rgba(46,159,191,1)',
            expense: 'rgba(243,183,46,1)',
            revenue: 'rgba(46,159,191,1)',
        },
        chartPoints: {
            income: {},
            expense: {},
            revenue: {}
        },
        chartID: {
            income: '#dashboard_pie_chart_income',
            expense: '#dashboard_pie_chart_expense',
            revenue: '#dashboard_pie_chart_revenue'
        },
        total: {
            income: null,
            expense: null,
        },
        periodText: '<?= $periodText ?>',
        init: function() {
            if (typeof window.ChartPlanFactDays === 'object') {
                this.bindEvents();
                this.PlanFact = window.ChartPlanFactDays;
            } else {
                console.log('ChartPlanFactDays not found!');
            }
        },
        sync: true, // sync plan-fact & revenue line charts
        bindEvents: function() {
            const that = this;
            
            // change plan-fact-chart purse
            $('#chart-plan-fact-purse-type').on('change', function() {
                that.redrawIncomePie();
                that.redrawExpensePie();
                that.redrawRevenuePie();
            });
            
            // move plan-fact-chart
            $('.chart-plan-fact-days-arrow').on('click', function() {
                if (that.activeColumn) {
                    that.activeColumn = null;
                    that.resetAllPoints();
                    that.redrawIncomePie();
                    that.redrawExpensePie();
                    that.redrawRevenuePie();
                }

                if (that.sync) {
                    ChartsProfitAndLoss.Revenue.offset.months
                        = ChartPlanFactDays.offset.months + 1;

                    ChartsProfitAndLoss.redrawByClick('revenue');
                }

                setTimeout(function () {
                    that.bindPlanFactColumnEvents();
                }, 100);
            });

            // move revenue-chart
            $('.chart-profit-loss-revenue-arrow').on('click', function() {
                if (that.activeColumn) {
                    that.activeColumn = null;
                    that.resetAllPoints();
                    that.redrawIncomePie();
                    that.redrawExpensePie();
                    that.redrawRevenuePie();
                }

                if (that.sync) {
                    ChartPlanFactDays.offset.months =
                        ChartsProfitAndLoss.Revenue.offset.months - 1;

                    ChartPlanFactDays.redrawByClick();
                }

                setTimeout(function () {
                    that.bindPlanFactColumnEvents();
                }, 100);
            });

            // change plan-fact-chart day|month (+ unsync line charts)
            $('.chart-plan-fact-days-tab').on('click', function() {
                if (that.activeColumn) {
                    that.activeColumn = null;
                    that.resetAllPoints();
                    that.redrawIncomePie();
                    that.redrawExpensePie();
                    that.redrawRevenuePie();
                }

                that.sync = $(this).data('period') === 'months';
                if (that.sync) {
                    const offsetPlanFact = ChartPlanFactDays.offset.months;
                    const offsetRevenue = ChartsProfitAndLoss.Revenue.offset.months;
                    if (offsetPlanFact !== offsetRevenue - 1) {
                        ChartPlanFactDays.offset.months = offsetRevenue - 1;
                        ChartPlanFactDays.redrawByClick();
                    }
                }

                setTimeout(function () {
                    that.bindPlanFactColumnEvents();
                }, 100);
            });

            // click on plan-fact-chart column
            that.bindPlanFactColumnEvents();

            // click on plan-fact-chart column
            that.bindRevenueColumnEvents();
        },
        bindRevenueColumnEvents: function() {
            const that = this;
            let _chartToLoad = window.setInterval(function () {
                const chartRevenue = $('#chart-profit-loss-revenue').highcharts();
                if (typeof(chartRevenue) !== 'undefined' && !ChartsProfitAndLoss._inProcess) {
                    // revenue
                    chartRevenue.series[0].options.point.events = {
                        click: function(){
                            const pointRevenue = this;
                            that._onClickRevenueColumn(pointRevenue);

                        }
                    };
                    // profit
                    chartRevenue.series[1].options.point.events = {
                        click: function(){
                            const pointRevenue = chartRevenue.series[0].points[this.index];
                            that._onClickRevenueColumn(pointRevenue);

                        }
                    };
                    window.clearInterval(_chartToLoad);
                }
            }, 100);
        },
        bindPlanFactColumnEvents: function() {
            const that = this;
            let _chartToLoad = window.setInterval(function () {
                const chartPlanFact = $('#chart-plan-fact').highcharts();
                if (typeof(chartPlanFact) !== 'undefined' && !ChartPlanFactDays._inProcess) {
                    // income
                    chartPlanFact.series[0].options.point.events = {
                        click: function(){
                            const pointIncome = this;
                            const pointExpense = chartPlanFact.series[1].points[this.index];
                            that._onClickPlanFactColumn(pointIncome, pointExpense);
                        }
                    };
                    // expense
                    chartPlanFact.series[1].options.point.events = {
                        click: function(){
                            const pointExpense = this;
                            const pointIncome = chartPlanFact.series[0].points[this.index];
                            that._onClickPlanFactColumn(pointIncome, pointExpense);
                        }
                    };
                    window.clearInterval(_chartToLoad);
                }
            }, 100);
        },
        _onClickPlanFactColumn: function(pointIncome, pointExpense) {

            const that = this;
            const activeColumn = pointIncome.index;
            const maxFactAmountColumn = that.PlanFact.currDayPos || 9999;

            const chartRevenue = $('#chart-profit-loss-revenue').highcharts();
            const chartPlanFact = $('#chart-plan-fact').highcharts();
            const pointRevenue = chartRevenue.series[0].points[activeColumn];

            if (activeColumn > maxFactAmountColumn)
                return;

            if (that.activeColumn === null || activeColumn !== that.activeColumn) {
                that.activeColumn = activeColumn;
                that.setPoint(pointIncome, that.columnColor.income);
                that.setPoint(pointExpense, that.columnColor.expense);
                if (that.sync)
                    that.setPoint(pointRevenue, that.columnColor.revenue);
            } else {
                that.activeColumn = null;
                that.resetPoints(pointIncome, that.columnColor.income);
                that.resetPoints(pointExpense, that.columnColor.expense);
                if (that.sync)
                    that.resetPointsRevenue(pointRevenue, that.columnColor.revenue);
            }
            
            that.redrawIncomePie();
            that.redrawExpensePie();
            that.redrawRevenuePie();
        },
        _onClickRevenueColumn: function(pointRevenue) {

            const that = this;
            const activeColumn = pointRevenue.index;
            const chartRevenue = $('#chart-profit-loss-revenue').highcharts();
            const chartPlanFact = $('#chart-plan-fact').highcharts();
            const pointIncome = chartPlanFact.series[0].points[activeColumn];
            const pointExpense = chartPlanFact.series[1].points[activeColumn];

            if (that.activeColumn === null) {
                // set
                that.activeColumn = activeColumn;
                that.setPoint(pointRevenue, that.columnColor.revenue);
                if (that.sync) {
                    that.setPoint(pointIncome, that.columnColor.income);
                    that.setPoint(pointExpense, that.columnColor.expense);
                }
            } else if (activeColumn !== that.activeColumn) {
                // reset old point
                that.resetPointsRevenue(pointRevenue, that.columnColor.revenue);
                if (that.sync) {
                    that.resetPoints(pointIncome, that.columnColor.income);
                    that.resetPoints(pointExpense, that.columnColor.expense);
                }
                // set new
                that.activeColumn = activeColumn;
                that.setPoint(pointRevenue, that.columnColor.revenue);
                if (that.sync) {
                    that.setPoint(pointIncome, that.columnColor.income);
                    that.setPoint(pointExpense, that.columnColor.expense);
                }
            } else {
                // unset revenue
                that.activeColumn = null;
                that.resetPointsRevenue(pointRevenue, that.columnColor.revenue);
                if (that.sync) {
                    that.resetPoints(pointIncome, that.columnColor.income);
                    that.resetPoints(pointExpense, that.columnColor.expense);
                }
            }
            
            that.redrawIncomePie();
            that.redrawExpensePie();
            that.redrawRevenuePie();
        },
        setPoint: function(point, color) {
            const that = this;
            that.resetPoints(point, color);
            if (point)
                point.update({
                    color: that.columnColor._pattern(color)
                });
        },
        resetPoints: function(point, color) {
            const that = this;
            const maxIndex = that.PlanFact.currDayPos || 9999;
            if (point)
            point.series.data.forEach(function(p,i) {
                if (p && i <= maxIndex)
                    p.update({
                        color: color
                    });
            });
        },
        resetAllPoints: function() {
            const that = this;
            const chartRevenue = $('#chart-profit-loss-revenue').highcharts();
            const chartPlanFact = $('#chart-plan-fact').highcharts();
            const maxIndex = that.PlanFact.currDayPos || 9999;

            chartPlanFact.series[0].data.forEach(function(p,i) {
                if (p && i <= maxIndex)
                    p.update({
                        color: that.columnColor.income
                    });
            });
            chartPlanFact.series[1].data.forEach(function(p,i) {
                if (p && i <= maxIndex)
                    p.update({
                        color: that.columnColor.expense
                    });
            });
            chartRevenue.series[0].data.forEach(function(p,i) {
                if (p)
                    p.update({
                        color: that.columnColor.income
                    });
            });
        },
        resetPointsRevenue: function(point, color) {
            const that = this;
            const chart2 = $('#chart-profit-loss-revenue').highcharts();
            point.series.data.forEach(function(p,i) {
                p.update({
                    color: color
                });
            });
        },
        redrawIncomePie: function() {
            const pointIndex = this.activeColumn;
            this.redrawByClick('income', (pointIndex) ? (this.PlanFact.dateX[pointIndex] || null) : null);
        },
        redrawExpensePie: function() {
            const pointIndex = this.activeColumn;
            this.redrawByClick('expense', (pointIndex) ? (this.PlanFact.dateX[pointIndex] || null) : null);
        },
        redrawRevenuePie: function() {
            const pointIndex = this.activeColumn;
            this.redrawByClick('revenue', (pointIndex) ? (this.PlanFact.dateX[pointIndex] || null) : null);
        },
        redrawByClick: function(chartType, date) {
            const that = this;
            $.post('/analytics/detailing-ajax/get-dashboard-pie-data', {
                    "chart-dashboard-pie-ajax": true,
                    "chartType": chartType,
                    "purse": that.PlanFact.byPurse,
                    "pageFilter": that.PlanFact.pageFilter,
                    "date": date,
                },
                function(data) {
                    data = JSON.parse(data);
                    that.chartPoints[chartType] = data.optionsChart;
                    that.periodText = data.title;
                    that.total[chartType] = data.total;
                    $(that.chartID[chartType]).highcharts().update(that.chartPoints[chartType]);
                    $(that.chartID[chartType]).closest('.kub-chart').find('.text-pie-chart-period').html(data.title);
                    if (chartType === 'revenue') {
                        $(that.chartID[chartType]).find('.pie-revenue').html(number_format(data.revenue, 0, ',', ' '));
                        $(that.chartID[chartType]).find('.pie-net-income').html(number_format(data.netIncome, 0, ',', ' '));
                        $(that.chartID[chartType]).find('.pie-percent ').html(number_format(data.percent, 0, ',', ' '));
                    } else {
                        $(that.chartID[chartType]).find('.pie-total').html(number_format(that.total[chartType], 0, ',', ' '));
                    }
                }
            );
        }
    };

    $(document).ready(function() {
        ChartDetailingPie.init();
    })

</script>

<?php ///////////////////////////////////////////////////////////////////
/// todo: move scripts into files! (double#2 with _chart_profit_loss.php
?>
<script>
    // MOVE CHART
    ChartsProfitAndLoss = {
        isLoaded: false,
        pageFilter: <?= json_encode($pageFilter) ?>,
        year: "<?= $palModel->year ?>",
        period: '<?= $customPeriod ?>',
        month: "<?= $customMonth ?>",
        byAmountKey: "<?= $customAmountKey ?>",
        Revenue: {
            offset: {
                days: 0,
                months: <?= $customOffset ?>
            },
            currDayPos: <?= (int)$currDayPos; ?>,
            wrapPointPos: <?= json_encode($wrapPointPos) ?>,
            labelsX: <?= json_encode($chartLabelsX) ?>,
            freeDays: <?= json_encode($chartFreeDays) ?>,
            chartPoints: {},
            getWrapPointXLabel: function(x) { return ChartsProfitAndLoss._getWrapPointXLabel(x); }
        },
        <?php /*
        //Clients: {
        //    chartHeight: "<?= $chart2['calculatedChartHeight'] ?>",
        //    totalAmount: "<?= $chart2['totalAmount'] ?>",
        //    chartPoints: {}
        //},
        //Products: {
        //    chartHeight: "<?= $chart3['calculatedChartHeight'] ?>",
        //    quantity: <?= json_encode($chart3['quantity']) ?>,
        //    marginPercent: <?= json_encode($chart3['marginPercent']) ?>,
        //    chartPoints: {}
        //},
        //Structure: {
        //    totalExpenseAmount: "<?= $chart4['totalExpenseAmount'] ?>",
        //    isExpense: <?= json_encode($chart4['isExpense']) ?>,
        //    chartPoints: {}
        //},
        */ ?>
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('.chart-profit-loss-revenue-arrow').on('click', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                if ($(this).data('move') === 'left') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period] += <?= $MOVE_OFFSET ?>;
                }
                ChartsProfitAndLoss.redrawByClick('revenue');
            });

            $('#chart-profit-loss-revenue-type').on('change', function() {
                ChartsProfitAndLoss.byAmountKey = $(this).val();
                ChartsProfitAndLoss.redrawByClick();
            });

            $('#chart-profit-loss-structure-month').on('change', function() {
                // prevent double-click
                if (ChartsProfitAndLoss._inProcess) {
                    return false;
                }
                ChartsProfitAndLoss.month = $(this).val();
                ChartsProfitAndLoss.redrawByClick('top');
            });
        },
        redrawByClick: function(chartsGroup) {

            return ChartsProfitAndLoss._getData(chartsGroup).done(function() {
                var revenue = $('#chart-profit-loss-revenue').highcharts();
                var clients = $('#chart-profit-loss-clients').highcharts();
                var products = $('#chart-profit-loss-products').highcharts();
                var structure = $('#chart-profit-loss-structure').highcharts();
                if (!chartsGroup || chartsGroup == 'revenue') {
                    revenue.update(ChartsProfitAndLoss.Revenue.chartPoints);
                }

                ChartsProfitAndLoss._inProcess = false;
            });
        },
        _getData: function(chartsGroup) {
            ChartsProfitAndLoss._inProcess = true;
            return $.post('/analytics/finance-ajax/get-profit-loss-charts-data', {
                    "chart-profit-loss-ajax": true,
                    "chartsGroup": chartsGroup,
                    "year": ChartsProfitAndLoss.year,
                    "month": ChartsProfitAndLoss.month,
                    "offset": ChartsProfitAndLoss.Revenue.offset[ChartsProfitAndLoss.period],
                    "amountKey": ChartsProfitAndLoss.byAmountKey,
                    "pageFilter": this.pageFilter
                    //"period": ChartsProfitAndLoss.period,
                },
                function(data) {
                    data = JSON.parse(data);
                    const revenue = data.revenue;
                    const clients = data.clients;
                    const products = data.products;
                    const structure = data.structure;
                    if (!chartsGroup || chartsGroup == 'revenue') {
                        ChartsProfitAndLoss.Revenue.freeDays = revenue.freeDays;
                        ChartsProfitAndLoss.Revenue.currDayPos = revenue.currDayPos;
                        ChartsProfitAndLoss.Revenue.labelsX = revenue.labelsX;
                        ChartsProfitAndLoss.Revenue.wrapPointPos = revenue.wrapPointPos;
                        ChartsProfitAndLoss.Revenue.chartPoints = revenue.optionsChart;
                    }
                }
            );
        },
        _getWrapPointXLabel: function(x, chartId) {

            var chart = $('#chart-profit-loss-revenue').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (ChartsProfitAndLoss.period == 'months') ? 0.705 : 0.625;

            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].prev;
                left = ChartsProfitAndLoss.Revenue.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 11) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (ChartsProfitAndLoss.Revenue.wrapPointPos[x]) {
                name = ChartsProfitAndLoss.Revenue.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    ///////////////////////////
    ChartsProfitAndLoss.init();
    ///////////////////////////

    $(document).ready(function() {

        ChartDetailingPie.sync = $('.chart-plan-fact-days-tab.active').data('period') === 'months';

        window._chartProfitAndLossArrowZ = 1;
        $('#chart-profit-loss-revenue').mousemove(function(event) {
            let setZ = function() { $('.chart-profit-loss-revenue-arrow').css({"z-index": window._chartProfitAndLossArrowZ - 1}); };
            let y = event.pageY - $(this).offset().top;
            let x = event.pageX - $(this).offset().left;

            if (y > 186) {
                if (window._chartProfitAndLossArrowZ === 1) {
                    window._chartProfitAndLossArrowZ = 2;
                    setZ();
                }
            }
            else if (window._chartProfitAndLossArrowZ === 2) {
                window._chartProfitAndLossArrowZ = 1;
                setZ();
            }

        });

    });
</script>