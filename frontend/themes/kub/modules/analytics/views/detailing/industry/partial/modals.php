<?php

use common\components\helpers\Html;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyInfoIndustry;
use common\widgets\Modal;
use kartik\widgets\Select2;
use yii\helpers\Url; ?>

<?php Modal::begin([
    'id' => 'change_status_modal',
]); ?>
<h4 class="modal-title">Изменить статус</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">

    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="company-industry-status" class="label">
                Статус
            </label>
            <?= Select2::widget([
                'hideSearch' => true,
                'name' => 'status',
                'options' => [
                    'id' => 'many-change-status-select',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => [
                    CompanyIndustry::STATUS_ACTIVE => CompanyIndustry::STRING_ACTIVE,
                    CompanyIndustry::STATUS_CLOSED => CompanyIndustry::STRING_CLOSED,
                ],
            ]); ?>
        </div>
    </div>

</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/analytics/detailing/change-industry-status']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'many-delete-industry',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите удалить выбранные направления?
</h4>
<div class="text-center">
    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
        'data-url' => Url::to(['/analytics/detailing/many-delete-industry']),
    ]); ?>
    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
        Нет
    </button>
</div>
<?php Modal::end(); ?>