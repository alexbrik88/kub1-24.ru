<?php

use common\components\TextHelper;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\AnalyticsSimpleSearch as AnalyticsSS;
use frontend\modules\analytics\models\AbstractFinance;
use yii\web\JsExpression;

/** @var OddsSearch $oddsModel */

////// const ////////
$MAX_PIE_PIECES = 7;
/////////////////////

$company = Yii::$app->user->identity->company;
$model = new AnalyticsSS($company);
$currYear = $oddsModel->year;

///// user changes ///////////////////////////
$dateFrom = $dateFrom ?? "{$currYear}-01-01";
$dateTo = $dateTo ?? "{$currYear}-12-31";
$customPurse = $customPurse ?? "";
//////////////////////////////////////////////

// expense
$rawData = $model->getPieByFlows($dateFrom, $dateTo, $customPurse, $pageFilter, AnalyticsSS::GROUP_BY_EXPENDITURE_ITEM);
$data = [];
$piece = $total = 0;
foreach ($rawData as $k => $v) {
    $amount = round($v['amount'] / 100, 2);
    if (++$piece > $MAX_PIE_PIECES) {
        $data[$MAX_PIE_PIECES - 1]['name'] = 'Остальное';
        $data[$MAX_PIE_PIECES - 1]['y'] += $amount;
        continue;
    }
    $data[$piece - 1] = [
        'name' => $oddsModel->expenditureItemName[$v['item_id']] ?? "id={$v['item_id']}",
        'y' => $amount,
        'color' => $model->getPieColor($k, AnalyticsSS::GROUP_BY_EXPENDITURE_ITEM)
    ];
    $total += $amount;
}

$series = [
    'name' => 'Pie Highchart',
    'colorByPoint' => true,
    'data' => $data
];

if (Yii::$app->request->post('chart-dashboard-pie-ajax')) {

    $df = explode('-', $dateFrom);
    $dt = explode('-', $dateTo);
    if (($df[2] ?? 0) == ($dt[2] ?? 0))
        $title = $df[2] . ' ' . (AbstractFinance::$monthEnd[$df[1]] ?? '') . ' ' . ($df[0] ?? '') . ' года';
    elseif (($df[1] ?? 0) == ($dt[1] ?? 0))
        $title = mb_strtolower(AbstractFinance::$month[$df[1]] ?? '') . ' ' . ($df[0] ?? '') . ' года';
    else
        $title = ($df[0] ?? '') . ' год';

    // RETURN ONLY CHART DATA
    echo json_encode([
        'title' => $title,
        'total' => $total,
        'optionsChart' => [
            'series' => $series,
        ],
    ]);
    exit;
}

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'marginTop' => 30,
                'spacingTop' => 20,
                'height' => $height ?? 250,
                'events' => new JsExpression('
                    setTimeout(function() {
                        $(".detailing-pie-chart-legend-expense").appendTo("#'.$id.' > .highcharts-container").fadeIn(250);
                    }, 500)')
            ],
            'title' => [
                'text' => $title
            ],
            'series' => [$series],
            'tooltip' => [
                'formatter' => new JsExpression("
                    function(args) {
                    
                        const serieName = this.key;
                        const periodName = (ChartDetailingPie.periodText || '');
                        const total = ChartDetailingPie.total.expense || {$total};
                        
                        return ('<table class=\"indicators\">' +
                                '<span class=\"title\">' + periodName[0].toUpperCase() + periodName.slice(1) + '</span>' + 
                                '<tr>' +
                                    '<td class=\"gray-text text-left\" colspan=\"2\">' + serieName + '</td>' +
                                '</tr>' +                                 
                                '<tr>' +
                                    '<td class=\"gray-text\">Сумма:</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td class=\"gray-text\">Доля в расходах: ' + '</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * this.y / (total || 9E9), 2, ',', ' ') + ' %</td>' +
                                '</tr>' +
                            '</table>');
                        }
        ")
            ],
            'legend' => [
                'floating' => true,
                'layout' => 'vertical',
                'verticalAlign' => 'middle',
                'align' => 'left',
                'y' => 5,
                'x' => 195,
                'itemMarginBottom' => 5,
                'labelFormatter' => new JsExpression('function () {
                        const name = (this.name.length > 20) ? (this.name.substr(0, 17) + "...") : this.name;
                        return \'<span class="name-name">\' + name + \'</span>\';
                    }'),
            ],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => false,
                    'borderWidth' => 0,
                    'size' => '90%',
                    'innerSize' => '50%',
                    'cursor' => 'pointer',
                    'dataLabels' => false,
                    'center' => ['80', '100']
                ],
            ],
        ],

    ], DC::CHART_PIE)
);
?>

<div class="detailing-pie-chart-hint detailing-pie-chart-legend-expense" style="display: none">
    <div class="pie-label">Расход</div>
    <div class="pie-label mb-2">
        <span class="pie-total bold">
            <?= TextHelper::moneyFormat($total, 0) ?> ₽
        </span>
    </div>
</div>
