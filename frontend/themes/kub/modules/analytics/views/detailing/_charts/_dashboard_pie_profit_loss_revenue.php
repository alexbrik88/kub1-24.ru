<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\AnalyticsSimpleSearch as AnalyticsSS;
use frontend\modules\analytics\models\AbstractFinance;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use common\components\TextHelper;

/** @var ProfitAndLossSearchModel $palModel */

////// const ////////
$MAX_PIE_PIECES = 10;
/////////////////////

$company = Yii::$app->user->identity->company;
$model = new AnalyticsSS($company);
$currYear = $palModel->year;
$customAmountKey = $customAmountKey ?? 'amount'; // amount||amountTax

///// user changes ///////////////////////////
$dateFrom = $dateFrom ?? "{$currYear}-01-01";
$dateTo = $dateTo ?? "{$currYear}-12-31";
$customPurse = $customPurse ?? "";
//////////////////////////////////////////////

$revenueVal = round($palModel->getPeriodSum('totalRevenue', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$anotherIncomeVal = round($palModel->getPeriodSum('totalAnotherIncome', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$variableCostsVal = round($palModel->getPeriodSum('totalVariableCosts', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$primeCostsVal = round($palModel->getPeriodSum('totalPrimeCosts', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$fixedCostsVal = round($palModel->getPeriodSum('totalFixedCosts', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$netIncomeLossVal = round($palModel->getPeriodSum('netIncomeLoss', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$taxVal = round($palModel->getPeriodSum('tax', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$amortizationVal = round($palModel->getPeriodSum('totalAmortization', $dateFrom, $dateTo, $customAmountKey) / 100, 2);
$paymentPercentVal = round($palModel->getPeriodSum('totalPaymentPercent', $dateFrom, $dateTo, $customAmountKey) / 100, 2);

$rawData = [
    [
        'name' => ($netIncomeLossVal >= 0) ? 'Прибыль' : 'Убыток',
        'amount' => abs($netIncomeLossVal),
        'color' => ($netIncomeLossVal >= 0)
            ? 'rgba(57,194,176,0.9)'
            : 'rgba(227,6,17,0.9)',
    ],
    [
        'name' => 'Себестоимость',
        'amount' => $primeCostsVal,
        'color' => 'rgba(243,183,46,1)',
    ],
    [
        'name' => 'Постоянные расходы',
        'amount' => $fixedCostsVal,
        'color' => 'rgba(228,228,103,1)',
    ],
    [
        'name' => 'Переменные расходы',
        'amount' => $variableCostsVal,
        'color' => 'rgba(220,213,103,1)',
    ],
    [
        'name' => 'Амортизация',
        'amount' => $amortizationVal,
        'color' => 'rgba(220,203,163,1)',
    ],
    [
        'name' => 'Проценты уплаченные',
        'amount' => $paymentPercentVal,
        'color' => 'rgba(220,203,199,1)',
    ],
    [
        'name' => 'Налог на прибыль',
        'amount' => $taxVal,
        'color' => 'rgba(220,203,199,1)',
    ],
];

$totalRevenue = $revenueVal;
$totalNetIncomeLoss = $netIncomeLossVal;

$data = [];
$piece = 0;
foreach ($rawData as $k => $v) {
    $amount = round($v['amount'] / 1, 2);
    if (++$piece > $MAX_PIE_PIECES) {
        $data[$MAX_PIE_PIECES - 1]['name'] = 'Остальное';
        $data[$MAX_PIE_PIECES - 1]['y'] += $amount;
        continue;
    }
    $data[$piece - 1] = [
        'name' => $v['name'],
        'y' => $amount,
        'color' => $v['color'] ?? $model->getPieColor($k, "default")
    ];
}

$series = [
    'name' => 'Pie Highchart',
    'colorByPoint' => true,
    'data' => $data
];

if (Yii::$app->request->post('chart-dashboard-pie-ajax')) {

    $df = explode('-', $dateFrom);
    $dt = explode('-', $dateTo);
    if (($df[2] ?? 0) == ($dt[2] ?? 0))
        // todo: there no profit-loss data by days
        //$title = $df[2] . ' ' . (AbstractFinance::$monthEnd[$df[1]] ?? '') . ' ' . ($df[0] ?? '') . ' года';
        $title = mb_strtolower(AbstractFinance::$month[$df[1]] ?? '') . ' ' . ($df[0] ?? '') . ' года';
    elseif (($df[1] ?? 0) == ($dt[1] ?? 0))
        $title = mb_strtolower(AbstractFinance::$month[$df[1]] ?? '') . ' ' . ($df[0] ?? '') . ' года';
    else
        $title = ($df[0] ?? '') . ' год';

    // RETURN ONLY CHART DATA
    echo json_encode([
        'title' => $title,
        'revenue' => $revenueVal,
        'netIncome' => $netIncomeLossVal,
        'percent' => $totalNetIncomeLoss / ($totalRevenue ?: 9E99) * 100,
        'optionsChart' => [
            'series' => $series,
        ],
    ]);
    exit;
}

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'margin' => 0,
                'spacingTop' => 20,
                'height' => 275,
                'events' => new JsExpression('
                    setTimeout(function() {
                        $(".detailing-pie-chart-legend-revenue").appendTo("#'.$id.' > .highcharts-container").fadeIn(250);
                    }, 500)')
            ],
            'title' => [
                'text' => $title
            ],
            'series' => [$series],
            'tooltip' => [
                'formatter' => new JsExpression("
                    function(args) {
                    
                        const serieName = this.key;
                        const periodName = (ChartDetailingPie.periodText || '');
                        const y = (this.key === 'Убыток') ? -this.y : this.y;
                        
                        return ('<table class=\"indicators\">' +
                                '<span class=\"title\">' + periodName[0].toUpperCase() + periodName.slice(1) + '</span>' + 
                                '<tr>' +
                                    '<td class=\"gray-text text-left\" colspan=\"2\">' + serieName + '</td>' +
                                '</tr>' +                                 
                                '<tr>' +
                                    '<td class=\"gray-text\">Сумма:</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(y, 2, ',', ' ') + ' ₽</td>' +
                                '</tr>' + ".
                            ($totalRevenue === null ? "" :
                                "'<tr>' +
                                    '<td class=\"gray-text\">Доля в выручке: ' + '</td>' +
                                    '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * y / ({$totalRevenue} || 9E9), 2, ',', ' ') + ' %</td>' +
                                '</tr>' + ")."
                            '</table>');
                        }
                    "),
            ],
            'legend' => false,
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => true,
                    'borderWidth' => 0,
                    'size' => '90%',
                    'innerSize' => '50%',
                    'cursor' => 'pointer',
                    'dataLabels' => false,
                    'startAngle' => $startAngle ?? 0,
                    'center' => ['30%', '50%']
                ]
            ],
        ],

    ], DC::CHART_PIE)
);
?>

<div class="detailing-pie-chart-legend detailing-pie-chart-legend-revenue" style="display: none">
    <div class="pie-label bold">Выручка</div>
    <div class="pie-label mb-2">
        <span class="pie-revenue"><?= TextHelper::moneyFormat($totalRevenue, 0) ?></span> ₽
    </div>
    <div class="pie-label bold">Прибыль</div>
    <div class="pie-label">
        <span class="pie-net-income"><?= TextHelper::moneyFormat($totalNetIncomeLoss, 0) ?></span> ₽
    </div>
    <div class="pie-label">
        <span class="pie-percent"><?= TextHelper::numberFormat($totalNetIncomeLoss / ($totalRevenue ?: 9E99) * 100, 0) ?></span> %
    </div>
</div>