<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;

$id = $id ?? 'chart_21';
$height = $height ?? 250;

$htmlTooltip = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th class="text-left" colspan="3">{title}</th>
        </tr>
        <tr>
            <td class="text-grey">{itemName}: </td>
            <td></td>
            <td class="text-grey"><b>{itemData}</b></td>
        </tr>
    </table>
HTML;

$htmlTooltip = str_replace(["\r", "\n", "'"], "", $htmlTooltip);

$dropdownItems = $dropdownItems ?? [];
$checkedDropdownItems = $checkedDropdownItems ?? [];
$checkedAllDropdownItems = $checkedAllDropdownItems ?? true;
$showCog = $showCog ?? true;
$showMenu = $showMenu ?? true;
?>

<div class="graph_block">
    <div class="graph_info">
        <div class="graph_header_block">
            <div class="graph_header" style="text-transform: unset">
                ДИНАМИКА ПОКАЗАТЕЛЕЙ
                <?= ($showCog) ? $this->render('_index_charts_config') : '' ?>
            </div>
        </div>
        <div class="row" style="position: relative">
            <div class="column pr-0" style="max-width: 69%">
                <?= ($showMenu) ? Nav::widget([
                    'id' => $id.'_nav',
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100',
                    ],
                    'items' => [
                        [
                            'label' => 'Приход',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == Config::CHART_TYPE_INCOME,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link' . ($customChartType == Config::CHART_TYPE_INCOME ? ' active':''), 'data-chart-type' => Config::CHART_TYPE_INCOME]
                        ],
                        [
                            'label' => 'Расход',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == Config::CHART_TYPE_EXPENSE,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link' . ($customChartType == Config::CHART_TYPE_EXPENSE ? ' active':''), 'data-chart-type' => Config::CHART_TYPE_EXPENSE]
                        ],
                        [
                            'label' => 'Выручка',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == Config::CHART_TYPE_REVENUE,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link' . ($customChartType == Config::CHART_TYPE_REVENUE ? ' active':''), 'data-chart-type' => Config::CHART_TYPE_REVENUE]
                        ],
                        [
                            'label' => 'Прибыль',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == Config::CHART_TYPE_PROFIT,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link' . ($customChartType == Config::CHART_TYPE_PROFIT ? ' active':''), 'data-chart-type' => Config::CHART_TYPE_PROFIT]
                        ],
                        [
                            'label' => 'Рент-ть',
                            'url' => 'javascript:void(0)',
                            'active' => $customChartType == Config::CHART_TYPE_PROFITABILITY,
                            'options' => ['class' => 'nav-item', 'style' => 'padding-left: 0'],
                            'linkOptions' => ['class' => 'nav-link' . ($customChartType == Config::CHART_TYPE_PROFITABILITY ? ' active':''), 'data-chart-type' => Config::CHART_TYPE_PROFITABILITY]
                        ],
                    ],
                ]) : '' ?>
            </div>
            <?php if ($dropdownItems): ?>
            <div style="position: absolute; top:0; right:23px;">
                <div class="form-group mb-3">
                    <div class="dropdown">
                        <a class="a-filter button-regular button-hover-content-red text-left pl-3 pr-5" href="#" style="width: 175px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="nowrap">Проекты: <span class="filter-title"><?= ($checkedDropdownItems) ? (count($checkedDropdownItems) . ' шт.') : 'все' ?></span></span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu pt-2 pb-0" data-id="dropdown3" style="width: 175px;">
                            <div style=" max-height: 195px; overflow-y: auto ">
                                <div class="pl-2 pr-0">
                                    <?= Html::checkbox('user_only_project_all', $checkedAllDropdownItems, [
                                        'class' => 'form-group user_only_project_all',
                                        'label' => '<span class="label">Все проекты</span>',
                                        'value' => 'all',
                                        'autocomplete' => 'off',
                                    ]) ?>
                                </div>
                                <?php foreach ($dropdownItems as $item): ?>
                                    <div class="pl-2 pr-0">
                                        <?php $checked = in_array($item['id'], $checkedDropdownItems) ?>
                                        <?php $name = (mb_strlen($item['name']) > 16) ? (mb_substr($item['name'], 0, 14).'...') : $item['name'] ?>
                                        <?= Html::checkbox('user_only_project', $checked, [
                                            'class' => 'form-group user_only_project',
                                            'label' => '<span class="label">'.htmlspecialchars($name).'</span>',
                                            'value' => $item['id'],
                                            'autocomplete' => 'off',
                                        ]) ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <div class="mt-2 p-2 text-center">
                                <button class="user_only_project_apply button-width button-regular button-hover-grey" type="button">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

        </div>
        
        <?= $this->render('_index_chart_arrows', ['chartId' => $id, 'offsetStep' => 1, 'class' => 'chart-offset-2', 'zIndex' => 2]) ?>

        <div style="min-height: <?= $height ?>px">
        <?= Highcharts::widget([
            'id' => $id,
            'scripts' => [
                'modules/exporting',
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'title' => false,
                'credits' => [
                    'enabled' => false
                ],
                'exporting' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'line',
                    'marginLeft' => '55',
                    'marginTop' => '20',
                    'marginBottom' => null,
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    'reflow' => true,
                    'height' => $height,
                ],
                'legend' => [
                    'useHTML' => true,
                    'layout' => 'horizontal',
                    'verticalAlign' => 'bottom',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0'
                    ],
                    'labelFormatter' => new JsExpression('function () {
                        const name = (this.name.length > 20) ? (this.name.substr(0, 17) + "...") : this.name;
                        return \'<span class="name-name">\' + name + \'</span>\';
                    }'),
                    //'itemDistance' => 10
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                        
                            const index = this.series.data.indexOf( this.point );
                            const data = Highcharts.numberFormat(this.y, 2, ',', ' ');

                            return '{$htmlTooltip}'
                                .replace(\"{title}\", window.chart_21.labelsX[index])
                                .replace(\"{itemName}\", this.series.name)
                                .replace(\"{itemData}\", data);
                        }
                    ")
                ],
                'yAxis' => [
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'lineWidth' => 0,
                ],
                'xAxis' => [
                    'min' => 0,
                    'categories' => $categories,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return this.pos == window.chart_21.currDayPos ? ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                            (window.chart_21.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>')); }"),
                        'useHTML' => true,
                    ],
                ],
                'series' => $series,
                'plotOptions' => [
                    'scatter' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ]
                    ],
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        'groupPadding' => 0.05,
                        'pointPadding' => 0.1,
                        'borderRadius' => 3,
                    ]
                ],
            ],
        ]) ?>
        </div>

    </div>
</div>