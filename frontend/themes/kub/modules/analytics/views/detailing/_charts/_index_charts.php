<?php

use common\models\Company;
use common\models\project\Project;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use frontend\modules\analytics\models\detailing\ProjectSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\AnalyticsSimpleSearch as AnalyticsSS;
use frontend\components\StatisticPeriod;
use yii\helpers\ArrayHelper;

/** @var Company $company */
/** @var null|ProfitAndLossSearchModel $palModel */
/** @var null|AnalyticsSS $analyticsModel */

$id = $id ?? 'chart_21';
$id2 = $id2 ?? 'chart_22';

////////////////////////////////////////////////////////////
$MAX_PIE_PIECES = 9;
$xPeriod = 9;
$customPeriod = 'months';
$customOffset = $customOffset ?? 0;
$customChartType = $customChartType ?? Config::getIndexChartType();
$currDayPos = $xPeriod - $customOffset - 1;
$byMonth = ($customPeriod === 'months');
$onPage = $onPage ?? 'industry'; // industry || sale_point || project
$showCog = $showCog ?? true;
$showMenu = $showMenu ?? true;
$lineChartHeight = $lineChartHeight ?? 250;
$pieChartHeight = $pieChartHeight ?? 299;
$colors = [
    'rgba(104,212,205,1)',
    'rgba(207,246,123,1)',
    'rgba(148,218,251,1)',
    'rgba(109,132,142,1)',
    'rgba(38,160,252,1)',
    'rgba(38,231,166,1)',
    'rgba(254,188,59,1)',
    'rgba(139,117,215,1)',
    'rgba(223, 169, 250,1)',
    'rgba(193,167,98,1)',
];
////////////////////////////////////////////////////////////

// show table projects only /////////////////////////////////////////////////////////////////////////////////////////////
if ($onPage == 'project') {
    $searchModel = new ProjectSearch();
    $tableProjectsIds = ArrayHelper::getColumn(($searchModel->search(Yii::$app->request->get(), true)), 'id');
    $userOnlyProjectsIds = Yii::$app->request->cookies->getValue('project_chart_series_user_filter', []);
} else {
    $tableProjectsIds = $userOnlyProjectsIds = [];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$templateSerie = [
    'type' => 'line',
    'name' => '---',
    'color' => '#000',
    'data' => [],
    'marker' => [
        'symbol' => 'circle',
        'enabled' => false
    ],
    'showInLegend' => true,
    'visible' => true
];

if ($onPage === 'industry') {
    $items = CompanyIndustry::getSelect2Data();
    $paramLinesGroupBy = AnalyticsSS::GROUP_BY_INDUSTRY;
    //if (isset($items[0]))
    //    unset($items[0]);
} elseif ($onPage === 'sale_point') {
    $items = SalePoint::getSelect2Data();
    $paramLinesGroupBy = AnalyticsSS::GROUP_BY_SALE_POINT;
    //if (isset($items[0]))
    //    unset($items[0]);
} elseif ($onPage === 'project') {
    $items = Project::getSelect2Data();
    $paramLinesGroupBy = AnalyticsSS::GROUP_BY_PROJECT;
    if (isset($items[0]))
        unset($items[0]);
} else {
    $items = [0 => '-'];
    $paramLinesGroupBy = null;
}


// CHART PIE ////////////////////////////////////////////////////////////////////////////////

$userPeriodFrom = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'from');
$userPeriodTo = ArrayHelper::getValue(StatisticPeriod::getSessionPeriod(), 'to');
switch ($customChartType) {
    case Config::CHART_TYPE_INCOME:
        $rawItemsData2 = $analyticsModel->getAllPiesByFlowsByPeriod($userPeriodFrom, $userPeriodTo, CashFlowsBase::FLOW_TYPE_INCOME, $paramLinesGroupBy);
        break;
    case Config::CHART_TYPE_EXPENSE:
        $rawItemsData2 = $analyticsModel->getAllPiesByFlowsByPeriod($userPeriodFrom, $userPeriodTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $paramLinesGroupBy);
        break;
    case Config::CHART_TYPE_REVENUE:
        $div100 = function($v) { return round($v / 100, 2); };
        $rawItemsData2 = array_map($div100, $palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo));
        break;
    case Config::CHART_TYPE_PROFIT:
        $div100 = function($v) { return round($v / 100, 2); };
        $rawItemsData2 = array_map($div100, $palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo));
        break;
    case Config::CHART_TYPE_PROFITABILITY:
        $rawItemsData2 = [];
        $netIncomeLoss = $palModel->getPeriodSumGroupedByMonth('netIncomeLoss', $userPeriodFrom, $userPeriodTo);
        $revenue = $palModel->getPeriodSumGroupedByMonth('totalRevenue', $userPeriodFrom, $userPeriodTo);
        foreach (array_keys($revenue) as $pid) {
            $r = ArrayHelper::getValue($revenue, $pid, 0);
            $n = ArrayHelper::getValue($netIncomeLoss, $pid, 0);
            $rawItemsData2[$pid] = 100 * $n / ($r ?: 9E99);
        }
        break;
}

$itemsData2 = [];
foreach ($items as $itemId => $itemName) {
    $itemsData2[] = [
        'name' => $itemName,
        'amount' => $rawItemsData2[$itemId] ?? 0,
        'id' => $itemId
    ];
}

usort($itemsData2, function ($a, $b) {
    if ($b['amount'] == $a['amount']) {
        return $a['id'] <=> $b['id'];
    }
    return ($b['amount'] < $a['amount']) ? -1 : 1;
});

// calc avg total percents
$otherAvgTotal2 = $allAvgTotal2 = [
    'revenue' => 0,
    'profit' => 0
];
if ($customChartType == Config::CHART_TYPE_PROFITABILITY) {
    $piece = 0;
    foreach ($itemsData2 as $p) {
        if (++$piece >= $MAX_PIE_PIECES) {
            $r = ArrayHelper::getValue($revenue, $p['id'], 0);
            $n = ArrayHelper::getValue($netIncomeLoss, $p['id'], 0);
            $otherAvgTotal2['revenue'] += $r;
            $otherAvgTotal2['profit'] += $n;
        }
        $allAvgTotal2['revenue'] += $r;
        $allAvgTotal2['profit'] += $n;
    }
}

$sortingProjectsArray = ($itemsData2) ? ArrayHelper::getColumn($itemsData2, 'id') : [];

switch ($customChartType) {
    case Config::CHART_TYPE_INCOME:
        $title2 = 'Приход';
        break;
    case Config::CHART_TYPE_EXPENSE:
        $title2 = 'Расход';
        break;
    case Config::CHART_TYPE_REVENUE:
        $title2 = 'Выручка';
        break;
    case Config::CHART_TYPE_PROFIT:
        $title2 = 'Прибыль';
        break;
    case Config::CHART_TYPE_PROFITABILITY:
        $title2 = 'Рент-ть';
        break;
}

$activeLinesCount = 0;
foreach ($itemsData2 as $k => $v) {
    if ($v['amount'] > 0)
        $activeLinesCount++;
}

$data2 = [];
$piece = $total2 = 0;
foreach ($itemsData2 as $k => $v) {

    if ($tableProjectsIds && !in_array($v['id'], $tableProjectsIds)
        || ($userOnlyProjectsIds && !in_array($v['id'], $userOnlyProjectsIds)))
            continue;

    if ($v['amount'] == 0)
        continue;

    $amount = $v['amount'];

    if ($customChartType == Config::CHART_TYPE_PROFITABILITY) {
        // avg
        $total2 = 100 * $allAvgTotal2['profit'] / ($allAvgTotal2['revenue'] ?: 9E99);
    } else {
        // sum
        $total2 += $amount;
    }

    if (++$piece > $MAX_PIE_PIECES) {
        $data2[$MAX_PIE_PIECES - 1]['name'] = 'Остальное';
        if ($customChartType == Config::CHART_TYPE_PROFITABILITY) {
            // precalculated percents avg
            $data2[$MAX_PIE_PIECES - 1]['y'] = 100 * $otherAvgTotal2['profit'] / ($otherAvgTotal2['revenue'] ?: 9E99);
        } else {
            // sum
            $data2[$MAX_PIE_PIECES - 1]['y'] += $amount;
        }
        continue;
    }

    $data2[$piece - 1] = [
        'name' => $v['name'],
        'y' => $amount,
        'color' => $colors[$piece - 1] ?? $colors[count($colors) - 1]
    ];
}

$series2 = [
    'name' => 'Pie Highchart',
    'colorByPoint' => true,
    'data' => $data2
];


// CHART LINE ////////////////////////////////////////////////////////////////////////////////

// prepare X
$categories = [];
$labelsX = [];
for ($x = $xPeriod - 1; $x >= 0; $x--) {
    $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
    $month = $date->format('n');
    $categories[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
    $labelsX[] = Month::$monthFullRU[$month] . ' ' . $date->format('Y');
}

// prepare Y
$dateFrom = (new DateTime())->modify("-{$xPeriod} months")->modify("+{$customOffset} months")->format('Y-m-01');
$dateTo = (new DateTime())->modify("+{$customOffset} months")->format('Y-m-t');
$itemsData = [];
switch ($customChartType) {
    case Config::CHART_TYPE_INCOME:
        $rawItemsData = $analyticsModel->getAllLinesByFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_INCOME, $paramLinesGroupBy);
        for ($x = $xPeriod - 1; $x >= 0; $x--) {
            $ym = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months")->format('Ym');
            foreach ($items as $itemId => $itemName) {
                $itemsData[$itemId][] = $rawItemsData[$itemId][$ym] ?? 0;
            }
        }
        break;
    case Config::CHART_TYPE_EXPENSE:
        $rawItemsData = $analyticsModel->getAllLinesByFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $paramLinesGroupBy);
        for ($x = $xPeriod - 1; $x >= 0; $x--) {
            $ym = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months")->format('Ym');
            foreach ($items as $itemId => $itemName) {
                $itemsData[$itemId][] = $rawItemsData[$itemId][$ym] ?? 0;
            }
        }
        break;
    case Config::CHART_TYPE_REVENUE:
        for ($x = $xPeriod - 1; $x >= 0; $x--) {
            $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
            $y = $date->format('Y');
            $m = $date->format('m');
            $revenueByYear = $palModel->getMonthDataGroupedByMonth($y, 'totalRevenue');
            foreach ($items as $itemId => $itemName) {
                $itemsData[$itemId][] = round(($revenueByYear[$itemId][$m] ?? 0) / 100, 2);
            }
        }
        break;
    case Config::CHART_TYPE_PROFIT:
        for ($x = $xPeriod - 1; $x >= 0; $x--) {
            $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
            $y = $date->format('Y');
            $m = $date->format('m');
            $profitByYear = $palModel->getMonthDataGroupedByMonth($y, 'netIncomeLoss');
            foreach ($items as $itemId => $itemName) {
                $itemsData[$itemId][] = round(($profitByYear[$itemId][$m] ?? 0) / 100, 2);
            }
        }
        break;
    case Config::CHART_TYPE_PROFITABILITY:
        for ($x = $xPeriod - 1; $x >= 0; $x--) {
            $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
            $y = $date->format('Y');
            $m = $date->format('m');
            $profitByYear = $palModel->getMonthDataGroupedByMonth($y, 'netIncomeLoss');
            $revenueByYear = $palModel->getMonthDataGroupedByMonth($y, 'totalRevenue');
            foreach ($items as $itemId => $itemName) {
                $p = $profitByYear[$itemId][$m] ?? 0;
                $r = $revenueByYear[$itemId][$m] ?? 0;
                $itemsData[$itemId][] = round(100 * $p / ($r ?: 9E99), 2);
            }
        }
        break;
}

// sort line chart projects by pie chart projects
uksort($itemsData, function ($aKey, $bKey) use ($sortingProjectsArray) {
    return (array_search($aKey, $sortingProjectsArray) > array_search($bKey, $sortingProjectsArray));
});

if ($customChartType == Config::CHART_TYPE_PROFITABILITY) {
    $otherSerieData = [];
    for ($x = $xPeriod - 1; $x >= 0; $x--) {
        $date = (new DateTime())->modify("-{$x} months")->modify("+{$customOffset} months");
        $y = $date->format('Y');
        $m = $date->format('m');
        $profitByYear = $palModel->getMonthDataGroupedByMonth($y, 'netIncomeLoss');
        $revenueByYear = $palModel->getMonthDataGroupedByMonth($y, 'totalRevenue');

        $piece = 0;
        $p = $r = 0;
        foreach ($itemsData as $itemId => $itemMonthData) {
            if (++$piece >= $MAX_PIE_PIECES) {
                $p += $profitByYear[$itemId][$m] ?? 0;
                $r += $revenueByYear[$itemId][$m] ?? 0;
            }
        }
        $otherSerieData[] = 100 * $p / ($r ?: 9E99);
    }
}

$piece = 0;
$series = [];
foreach ($itemsData as $itemId => $data) {

    if ($tableProjectsIds && !in_array($itemId, $tableProjectsIds)
        || ($userOnlyProjectsIds && !in_array($itemId, $userOnlyProjectsIds)))
            continue;

    $itemName = $items[$itemId] ?? "id={$itemId}";

    if (++$piece > $MAX_PIE_PIECES) {
        $series[$MAX_PIE_PIECES - 1]['name'] = 'Остальное';
        foreach ($series[$MAX_PIE_PIECES - 1]['data'] as $k => $d) {
            if ($customChartType == Config::CHART_TYPE_PROFITABILITY) {
                // get precalculated avg percents
                $series[$MAX_PIE_PIECES - 1]['data'][$k] = $otherSerieData[$k];
            } else {
                $series[$MAX_PIE_PIECES - 1]['data'][$k] += $data[$k];
            }
        }

        continue;
    }

    $series[$piece - 1] = array_replace($templateSerie, [
        'name' => $itemName,
        'color' => $colors[$piece - 1] ?? $colors[count($colors)-1],
        'data' => $data
    ]);
}

?>

<script>
    if (window.chart_21) {
        chart_21.currDayPos = <?= $currDayPos ?>;
        chart_21.labelsX = <?= json_encode($labelsX) ?>;
        chart_21.data = <?= json_encode([
            'series' => $series,
            'xAxis' => [
                'categories' => $categories
            ]
        ]) ?>;
    }
    if (window.chart_22) {
        chart_22.title = '<?= $title2 ?>';
        chart_22.total = <?= $total2 ?>;
        chart_22.data = <?= json_encode([
            'series' => $series2
        ]) ?>;
    }
</script>

<?php if (isset($isAjax)) { return; } ?>

<div class="row m-0">
    <div class="col-sm-12 col-md-8 p-2">
        <?= $this->render('_index_dynamics_line_chart', [
            'company' => $company,
            'palModel' => $palModel,
            'categories' => $categories,
            'series' => $series,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'height' => $lineChartHeight,
            'dropdownItems' => ($onPage == 'project') ? $itemsData2 : [],
            'checkedAllDropdownItems' => empty($userOnlyProjectsIds),
            'checkedDropdownItems' => $userOnlyProjectsIds,
            'showCog' => $showCog,
            'showMenu' => $showMenu
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-4 pl-0 pr-2 pt-2 pb-2">
        <?= $this->render('_index_dynamics_pie_chart', [
            'company' => $company,
            'palModel' => $palModel,
            'series' => $series2,
            'onPage' => $onPage,
            'customChartType' => $customChartType,
            'total' => $total2,
            'title' => $title2,
            'height' => $pieChartHeight,
            'showCog' => $showCog,
            'showMenu' => $showMenu
        ]) ?>
    </div>

    <div id="chart-transmitter"><!-- js update charts data --></div>

</div>

<script>
    window.chart_21 = {
        id: '<?= $id ?>',
        data: {},
        labelsX: <?= json_encode($labelsX) ?>,
        freeDays: [],
        currDayPos: <?= $currDayPos ?>,
        offset: 0,
        period: 'months',
        chartType: '<?= $customChartType ?>',
        onPage: '<?= $onPage ?>',
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            const that = this;

            // move chart
            $(document).on("click", ".chart-offset-2", function () {
                const offset = $(this).data('offset');

                if (that._inProcess) {
                    return false;
                }

                that.offset += offset;
                that.refresh();
            });

            // change chart type (income, expense, revenue, profit)
            $('#chart_21_nav a').on('click', function(e) {
                e.preventDefault();
                const nav = $('#chart_21_nav');
                const chartType = $(this).data('chart-type');

                if (that._inProcess) {
                    return false;
                }

                $(nav).find('li.active, a.active').removeClass('active');
                $(this).addClass('active').parent().addClass('active');

                that.chartType = Number(chartType) || 1;
                that.refresh();
            });

            // change default chart type
            $('.chart_user_config_radio').on('change', function() {
                $.post('/analytics/detailing-ajax/set-index-page-chart-type', {
                        "chart-type": this.value,
                        "on-page": that.onPage || '',
                    }, function(data) {
                        if (data.success)
                            location.href = location.href;
                    }
                );
            });

            // show series checkboxes

            $('.user_only_project_all').on('change', function() {
                let title;
                if ($(this).prop('checked')) {
                    $('.user_only_project:checked').prop('checked', false).uniform('refresh');
                    title = 'все';
                } else {
                    $('.user_only_project').prop('checked', true).uniform('refresh');
                    title = $('.user_only_project:checked').length + ' шт.';
                }

                $(this).closest('.dropdown').find('.filter-title').html(title);
                $('.user_only_project_apply').removeClass('button-hover-grey').addClass('button-regular_red');
                //that.updateSeriesFilter();
            });

            $('.user_only_project').on('change', function() {
                let title;
                if ($('.user_only_project:checked').length === 0) {
                    $('.user_only_project_all').prop('checked', true).uniform('refresh');
                    title = 'все';
                } else {
                    $('.user_only_project_all').prop('checked', false).uniform('refresh');
                    title = $('.user_only_project:checked').length + ' шт.';
                }

                $(this).closest('.dropdown').find('.filter-title').html(title);
                $('.user_only_project_apply').removeClass('button-hover-grey').addClass('button-regular_red');
                //that.updateSeriesFilter();
            });

            $('.user_only_project_apply').on('click', function() {
                that.updateSeriesFilter();
            });
        },
        updateSeriesFilter: function() {
            const that = this;
            let checked = [];
            $('.user_only_project_all, .user_only_project').each(function() {
                if ($(this).prop('checked'))
                    checked.push($(this).val());
            });

            $.post('/analytics/detailing-ajax/set-user-chart-series-filter', {
                    "on-page": that.onPage || '',
                    "checked": checked || ['all']
                }, function(data) {
                    if (data.success)
                        location.href = location.href;
                }
            );
        },
        refresh: function() {
            const that = this;
            that._inProcess = true;
            that._getData().done(function() {
                that._repaint();
                that._inProcess = false;
                // pie
                if (window.chart_22) {
                    window.chart_22.chartType = that.chartType;
                    window.chart_22._repaint();
                }
            });
        },
        _getData: function() {
            const that = this;
            return $.post('/analytics/detailing-ajax/get-index-page-data', {
                    "chart-detailing-index-ajax": 1,
                    "period": that.period || 'months',
                    "offset": that.offset || 0,
                    "chart-type": that.chartType || 1,
                    "on-page": that.onPage || '',
                },
                function(data) {
                    $('#chart-transmitter').append(data).html(null);
                }
            );
        },
        _repaint: function() {
            const that = this;
            $('#' + that.id).highcharts().update(that.data);
        },
    };

    window.chart_22 = {
        id: '<?= $id2 ?>',
        chartType: '<?= $customChartType ?>',
        total: <?= $total2 ?>,
        title: '<?= $title2 ?>',
        periodText: '???',
        data: {},
        onPage: '<?= $onPage ?>',
        _repaint: function() {
            const that = this;
            const chart = $('#' + that.id);
            const symbol = String(that.chartType) === "5" ? ' %' : ' ₽';
            chart.highcharts().update(that.data);
            chart.find('.pie-title').html(that.title);
            chart.find('.pie-total').html(number_format(that.total, 0, ',', ' ') + symbol);

            if (Number(that.chartType) === 4 || 5 === Number(that.chartType)) {
                chart.find('.detailing-index-chart-legend').hide();
                chart.highcharts().series[0].update({
                    type: 'column'
                });
            } else {
                chart.find('.detailing-index-chart-legend').show();
                chart.highcharts().series[0].update({
                    type: 'pie'
                });
            }
        },
    };

    ///////////////////////////////
    $(document).ready(function() {
        window.chart_21.init();
    });
    ///////////////////////////////

</script>