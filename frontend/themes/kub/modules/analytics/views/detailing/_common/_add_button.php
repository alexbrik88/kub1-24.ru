<?php use frontend\components\Icon;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;

$dropItems = [
    [
        'label' => 'Направление',
        'url' => 'javascript:void(0)',
        'linkOptions' => [
            'class' => 'add-modal-new-industry dropdown-item',
        ]
    ],
    [
        'label' => 'Точка продаж',
        'url' => 'javascript:void(0)',
        'linkOptions' => [
            'data-toggle' => 'modal',
            'data-target' => '#add-sale-point-menu'
        ]
    ],
    [
        'label' => 'Проект',
        'url' => '/project/create',
    ]
];
?>

<div class="dropdown pl-0 pr-0 ml-auto">
    <?= Html::button(Icon::get('add-icon') . '<span class="mr-3 ml-2">Добавить</span>' . '<span style="margin-top: 0;">' . Icon::get('shevron') . '</span>',
        ['class' => 'button-regular button-regular_padding_medium button-regular_red button-clr', 'data-toggle' => 'dropdown']); ?>
    <?= Dropdown::widget([
        'options' => [
            'id' => 'add-all-button',
            'class' => 'dropdown-menu form-filter-list list-clr dropdown-w-100 text-center',
            'style' => 'z-index: 1003'
        ],
        'items' => $dropItems,
    ]); ?>
</div>
