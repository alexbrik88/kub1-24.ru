<?php
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

// Add CompanyIndustry
Modal::begin([
    'id' => 'company-industry-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'company-industry-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();

// Add SalePoint
Modal::begin([
    'id' => 'add-sale-point-menu',
    'title' => 'Добавить точку продаж',
    'titleOptions' => [
        'class' => 'mb-3',
    ],
    'closeButton' => [
        'label' => \frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>

<div>
    Точки продаж помогут структурировать загружаемые данные и структурировать отчеты
</div>

<div class="row mt-4 align-items-stretch" style="height: 120px;">
    <div class="col text-center">
        <?= Html::button(Html::img('/images/sale-point-department.png', ['class' => 'mr-3']) . 'Отдел продаж', [
            'class' => 'add-sale-point button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
            'data-type' => 1
        ]) ?>
    </div>
    <div class="col text-center">
        <?= Html::button(Html::img('/images/sale-point-shop.png', ['class' => 'mr-3']) . 'Магазин', [
            'class' => 'add-sale-point button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
            'data-type' => 2
        ]) ?>
    </div>
    <div class="col text-center">
        <?= Html::button(Html::img('/images/sale-point-i-shop.png', ['class' => 'mr-3']) . 'Интернет-магазин', [
            'class' => 'add-sale-point button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
            'data-type' => 3
        ]) ?>
    </div>
</div>
<div class="row mt-4 align-items-stretch" style="height: 120px;">
    <div class="col text-center">
        <?= Html::button(Html::img('/images/sale-point-restaurant.png', ['class' => 'mr-3']) . 'Кафе/ресторан', [
            'class' => 'add-sale-point button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
            'data-type' => 4
        ]) ?>
    </div>
    <div class="col text-center">
        <?= Html::button(Html::img('/images/sale-point-filial.png', ['class' => 'mr-3']) . 'Филиал', [
            'class' => 'add-sale-point button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
            'data-type' => 5
        ]) ?>
    </div>
    <div class="col text-center">
        <?= Html::button('Другое', [
            'class' => 'send-proposal button-regular button-hover-content-red w-100 h-100',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>


<?php // Add SalePoint
Modal::begin([
    'id' => 'sale-point-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'sale-point-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Store
Modal::begin([
    'id' => 'store-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'store-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Cashbox
Modal::begin([
    'id' => 'cashbox-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'cashbox-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Employee
Modal::begin([
    'id' => 'employee-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'employee-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();
?>
<?php // Add Proposal
Modal::begin([
    'id' => 'proposal-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);
Pjax::begin([
    'id' => 'proposal-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
]);
Pjax::end();
Modal::end();
?>

<script>

    // industry
    $(document).on("click", ".add-modal-new-industry", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/analytics/detailing/create-industry",
            container: "#company-industry-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
            data: {}
        });

        $(document).on("pjax:success", "#company-industry-pjax", function() {
            $("#company-industry-modal").modal("show");
            $('#add-all-button').removeClass('show');
        });

        return false;
    });
    $(document).on("click", ".update-modal-industry", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/analytics/detailing/update-industry/?id=" + $(this).data('id'),
            container: "#company-industry-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
            data: {}
        });

        $(document).on("pjax:success", "#company-industry-pjax", function() {
            $("#company-industry-modal").modal("show");
        });

        return false;
    });

    // sale_point
    currentCashboxPos = 0;
    currentCashboxIsAdd = false;
    $(document).on("click", ".add-sale-point", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/company-structure/add-sale-point",
            container: "#sale-point-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
            data: {
                'type_id': $(this).attr('data-type')
            }
        });

        $(document).on("pjax:success", "#sale-point-pjax", function() {
            $("#sale-point-modal").modal("show");
        });

        return false;
    });
    $(document).on("change", "#salepoint-store_id", function(e) {
        var value = $(this).val() || $(this).text();
        if (value == "add-modal") {
            e.preventDefault();

            $.pjax({
                url: "/company-structure/add-store",
                container: "#store-pjax",
                push: false,
                timeout: 5000,
                scrollTo: false,
            });

            $(document).on("pjax:success", "#store-pjax", function() {
                $("#store-modal").modal("show");
            });
            $("#salepoint-store_id").val("").trigger("change");
        }
    });
    $(document).on("change", ".salepoint-cashbox_ids", function(e) {

        console.log($(this).val())

        var value = $(this).val() || $(this).text();
        if (value == "add-modal") {
            e.preventDefault();

            $.pjax({
                url: "/company-structure/add-cashbox",
                container: "#cashbox-pjax",
                push: false,
                timeout: 5000,
                scrollTo: false
            });

            $(document).on("pjax:success", "#cashbox-pjax", function() {
                $("#cashbox-modal").modal("show");
            });

            window.currentCashboxIsAdd = true;
            window.currentCashboxPos = $(this).closest('.cashbox-item-modal').index();

            $(this).val("").trigger("change");
        }

        else if (!$(this).val() && !window.currentCashboxIsAdd) {
            if ($('.cashbox-item-modal:visible').length > 1) {
                $(this).closest('.cashbox-item-modal').hide().appendTo('.cashbox-list-modal');
                $(".add-cashbox-field").show();
            }
        }

        else {
            $('.invalid-feedback-cashbox').remove();
            window.currentCashboxIsAdd = false;
        }
    });
    $(document).on("click", ".add-cashbox-field", function(e) {
    e.preventDefault();
    var hasEmpty = false;
    $('.cashbox-item-modal:visible').each(function() {
        if (!$(this).find('select').val()) {
            hasEmpty = true;
        }
    });

    if (hasEmpty)
        return false;

    $('.cashbox-item-modal:hidden').first().show();

    if ($('.cashbox-item-modal:visible').length >= ({$maxCashboxes})) {
        $(".add-cashbox-field").hide();
    }

    return false;
});
    $(document).on('click', '.delete-sale-point', function(e) {
        e.preventDefault();
        $('.delete-sale-point-modal-btn').data('id', $(this).attr('data-id'));
        $('#delete-sale-point-modal').modal('show');
    });
    $(document).on('click', '.delete-sale-point-modal-btn', function(e) {
        e.preventDefault();
        var id = $('.delete-sale-point-modal-btn').data('id');
        $.get('/company-structure/delete-sale-point', {id: id}, function (data) {
            if (data.result) {
                $('.sale-point').filter('[data-id = "' + id + '"]').remove();
                window.toastr.success("Отдел удален");
                $.pjax.reload({container: '#sale-point-pjax-container'});
            }

            $('#delete-sale-point-modal').modal('hide');
        });

        return false;
    });
    $(document).on("click", ".edit-sale-point", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/company-structure/update-sale-point",
            container: "#sale-point-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
            data: {
                'id': $(this).attr('data-id')
            }
        });

        $(document).on("pjax:success", "#sale-point-pjax", function() {
            $("#sale-point-modal").modal("show");
        });

        return false;
    });
    $(document).on("click", ".edit-cashbox", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/company-structure/update-cashbox",
            container: "#cashbox-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
            data: {
                'id': $(this).parents('.cashbox-item-modal').attr('data-id')
            }
        });

        $(document).on("pjax:success", "#cashbox-pjax", function() {
            $("#cashbox-modal").modal("show");
        });

        return false;
    });
    $(document).on('click', '.delete-cashbox', function(e) {
        e.preventDefault();
        $('.delete-cashbox-modal-btn').data('id', $(this).parents('.cashbox-item-modal').attr('data-id'));
        $('#delete-cashbox-modal').modal('show');
    });
    $(document).on('click', '.delete-cashbox-modal-btn', function(e) {
        e.preventDefault();
        var id = $('.delete-cashbox-modal-btn').data('id');
        $.get('delete-cashbox', {id: id}, function (data) {
            if (data.result) {
                //
            } else {

                window.toastr.success("Не удалось удалить кассу");

            }

            $('#delete-cashbox-modal').modal('hide');
        });

        return false;
    });
    $(document).on("click", ".send-proposal", function(e) {
        e.preventDefault();

        $.pjax({
            url: "/company-structure/send-proposal",
            container: "#proposal-pjax",
            push: false,
            timeout: 5000,
            scrollTo: false,
        });

        $(document).on("pjax:success", "#proposal-pjax", function() {
            $("#proposal-modal").modal("show");
        });

        return false;
    });
</script>


