<?php

use frontend\modules\analytics\controllers\DetailingController;
use frontend\modules\analytics\models\AbstractFinance;
use kartik\widgets\Select2;
use common\models\Company;
use yii\helpers\Html;

/** @var Company $company */

$year = $year ?? date('Y');
$tab = $tab ?? null;
if ($tab == DetailingController::TAB_VIEW_PAL) {
    $id = 'profitandlosssearchmodel-year';
    $name = 'ProfitAndLossSearchModel[year]';
} else {
    $id = 'oddssearch-year';
    $name = 'OddsSearch[year]';
}

echo Html::beginForm('', 'GET');
echo Select2::widget([
    'name' => $name,
    'data' => AbstractFinance::getYearFilterByCompany($company),
    'hideSearch' => true,
    'value' => $year,
    'options' => [
        'id' => $id,
    ],
    'pluginOptions' => [
        'width' => 'auto',
    ],
]);
echo Html::endForm();