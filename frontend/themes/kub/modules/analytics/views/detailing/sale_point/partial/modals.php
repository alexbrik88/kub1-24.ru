<?php

use common\components\helpers\ArrayHelper;
use common\models\companyStructure\SalePoint;
use frontend\themes\kub\helpers\Icon;
use kartik\widgets\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>


<?php Modal::begin([
    'id' => 'change_employers_modal',
    'title' => 'Изменить сотрудников',
    'closeButton' => false,
    'titleOptions' => [
        'class' => 'text-left',
    ],
    'options' => [
        'class' => 'fade',
    ],
]); ?>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div class="form-body">
        <div class="row form-horizontal">
            <div class="col-12 mb-3">
                <strong>
                    изменить на:
                </strong>
            </div>
            <div class="col-6">
                <label for="company-sale-point-type" class="label">
                    Сотрудники
                </label>

                <?php echo Select2::widget([
                    'id' => 'many-change-sale-point-employers-select',
                    'name' => 'employers',
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => '',
                        'multiple' => true,
                        'value' => [],
                    ],
                    'pluginOptions' => [
                        'closeOnSelect' => false,
                        'width' => '100%',
                        'escapeMarkup' => new \yii\web\JsExpression('function(text) {return text;}')
                    ],
                    'showToggleAll' => false,
                    'data' => ArrayHelper::map(
                        Yii::$app->user->identity->company->getEmployeeCompanies()
                            ->joinWith('employee')
                            ->andWhere(['employee_company.is_working' => true])
                            ->andWhere(['employee.is_deleted' => false])
                            ->andFilterWhere(['not', ['employee.id' => ($_excludeEmployers = -1)]])
                            ->orderBy([
                                'lastname' => SORT_ASC,
                                'firstname' => SORT_ASC,
                                'patronymic' => SORT_ASC,
                            ])->all(), 'employee_id', 'fio'),
                ]) ?>
            </div>
        </div>
    </div>
    <br>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'modal-many-change-responsible button-regular button-width button-regular_red button-clr',
            'data-url' => Url::to(['/analytics/detailing/change-sale-point-employers']),
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'change_status_modal',
]); ?>
<h4 class="modal-title">Изменить статус</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">

    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="company-sale-point-status" class="label">
                Статус
            </label>
            <?= Select2::widget([
                'hideSearch' => true,
                'name' => 'status',
                'options' => [
                    'id' => 'many-change-status-select',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => [
                    SalePoint::STATUS_ACTIVE => SalePoint::STRING_ACTIVE,
                    SalePoint::STATUS_CLOSED => SalePoint::STRING_CLOSED,
                ],
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= \common\components\helpers\Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-status button-regular button-width button-regular_red button-clr',
        'data-url' => Url::to(['/analytics/detailing/change-sale-point-status']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'many-delete-sale-point',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите удалить выбранные направления?
</h4>
<div class="text-center">
    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
        'data-url' => Url::to(['/analytics/detailing/many-delete-sale-point']),
    ]); ?>
    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
        Нет
    </button>
</div>
<?php Modal::end(); ?>

<?php
$maxCashboxes = SalePoint::MAX_CASHBOXES; // to use in heredoc..
$this->registerJs(<<<JS

$(document).on("change", "#salepoint-employers", function(e) {

    var selectedEmployers = $(this).val();
    
    $.each(selectedEmployers, function(pos,value) {
        if (value == "add-modal") {
            $.pjax({
                url: "/company-structure/add-employee",
                container: "#employee-pjax",
                push: false,
                timeout: 5000,
                scrollTo: false,
            });
    
            $(document).on("pjax:success", "#employee-pjax", function() {
                $("#employee-modal").modal("show");
            });
    
            selectedEmployers.splice(pos,1);
            $("#salepoint-employers").val(selectedEmployers).trigger("change").select2("close");
        }
    });
});
JS
);