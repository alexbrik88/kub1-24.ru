<?php
/* @var $this yii\web\View */

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\TextHelper;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\View;
use frontend\themes\kub\widgets\SummarySelectContractorWidget;
use common\models\companyStructure\SalePoint;
use frontend\modules\analytics\models\detailing\SalePointSearch;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel SalePointSearch */
/* @var $userConfig Config */
/* @var $palModel \frontend\modules\analytics\models\ProfitAndLossSearchModel */
/* @var $analyticsModel \frontend\modules\analytics\models\AnalyticsSimpleSearch */

$this->title = 'Точки продаж';

$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;
$company = $user->company;

$period = StatisticPeriod::getSessionName();

$exists = SalePoint::find()
    ->andWhere(['company_id' => $company->id])
    ->exists();

if ($exists) {
    $emptyMessage = "В выбранном периоде «{$period}», у вас нет направлений. Измените период, чтобы увидеть имеющиеся направления.";
} else {
    $emptyMessage = 'Вы еще не добавили ни одного направления. '
        . Html::a('Создать направление', '#', ['class' => 'link'])
        . '.';
}

$tabViewClass = $userConfig->getTableViewClass('table_view_detailing');

$showHelpPanel = $userConfig->report_odds_help ?? false;
$showChartPanel = $userConfig->report_odds_chart ?? false;
?>

<div class="stop-zone-for-fixed-elems project-index">
    <!-- HEADER -->
    <div class="wrap pt-1 pb-0 pl-4 pr-3" style="margin-bottom:12px;">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="column mr-auto">
                    <h4 class="mb-2"><?= $this->title ?></h4>
                </div>
                <div class="column pl-1 pr-2">
                    <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                        [
                            'id' => 'btnChartCollapse',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                            'data-toggle' => 'collapse',
                            'href' => '#chartCollapse',
                            'data-tooltip-content' => '#tooltip_chart_collapse',
                            'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                        ]) ?>
                </div>
                <div class="column pl-1 pr-2">
                    <?= Html::button(Icon::get('book'),
                        [
                            'id' => 'btnHelpCollapse',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                            'data-toggle' => 'collapse',
                            'href' => '#helpCollapse',
                            'data-tooltip-content' => '#tooltip_help_collapse',
                            'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                        ]) ?>
                </div>

                <div class="column pl-1 pr-0 select2-wrapper" style="margin-top:-9px">
                    <?= frontend\widgets\RangeButtonWidget::widget([
                        'byMonths' => true
                    ]); ?>
                </div>
            </div>
        </div>

        <div style="display: none">
            <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
            <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
        </div>

    </div>

    <div class="jsSaveStateCollapse wrap p-0 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_odds_chart">
        <?= $this->render('../_charts/_index_charts', [
            'company' => $company,
            'palModel' => $palModel,
            'analyticsModel' => $analyticsModel,
            'onPage' => $onPage,
            'currentYear' => $currentYear
        ]) ?>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_odds_help">
        <div class="pt-4 pb-3">
            <span class="text-justify">
                На этой странице вы можете проанализировать движение денежных средств, прибыль и рентабельность
                ваших точек продаж, а также состояние расчетов с дебиторами и кредиторами, состояние запасов и
                задолженность по кредитам и займам.  Это позволит оценить какие точки продаж приносят вам
                прибыль, а какие съедают ваши деньги.
            </span>
        </div>
    </div>
    <!-- END OF HEADER -->

    <div class="table-settings row row_indents_s" style="margin-top:0!important;">

        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    // todo
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_detailing']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'searchBy', [
                        'type' => 'search',
                        'placeholder' => 'Название направления',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'sale-point-pjax-container',
        'enablePushState' => false,
        'scrollTo' => false
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list ' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model['id']) ? Html::checkbox('id[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $model['id'],
                    ]) : null;
                },
            ],
            [
                'attribute' => 'name',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => 'sorting',
                    //'width' => '180px',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model['name']), ['view-sale-point', 'id' => $model['id']], ['class' => 'link', 'data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'type',
                'label' => 'Тип',
                'headerOptions' => [
                    //'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                's2width' => '150px',
                'value' => function ($model) {
                    if ($model['id']) {
                        return $model['type'];
                    }
                    return '---';
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    //'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                's2width' => '150px',
                'value' => function ($model) {
                    if ($model['id']) {
                        return $model['status_id'] === SalePoint::STATUS_ACTIVE
                            ? SalePoint::STRING_ACTIVE
                            : SalePoint::STRING_CLOSED;
                    }

                    return '---';
                },
            ],
            [
                'attribute' => 'cashboxes',
                'label' => 'Касса',
                'headerOptions' => [
                    //'width' => '20%',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model['id']) {
                        return implode('<br/>', $model['cashboxes']);    
                    }

                    return '---';
                },
            ],
            [
                'attribute' => 'store',
                'label' => 'Склад',
                'headerOptions' => [
                    //'width' => '20%',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model['id']) {
                        return Html::encode($model['store']);
                    }

                    return '---';
                },
            ],
            [
                'attribute' => 'employers',
                'label' => 'Сотрудники',
                'headerOptions' => [
                    //'width' => '20%',
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model['id']) {
                        return implode('<br/>', $model['employers']);
                    }

                    return '---';
                },
            ],            
            [
                'attribute' => 'income_amount',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => 'sorting',
                    //'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['income_amount']);
                },
            ],
            [
                'attribute' => 'expense_amount',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => 'sorting',
                    //'width' => '120px',
                ],
                'contentOptions' => [
                    'class' => '',
                ],
                'value' => function ($model) {
                    return TextHelper::invoiceMoneyFormat($model['expense_amount']);
                },
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?= SummarySelectContractorWidget::widget([
    'buttons' => [
        Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-width button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Статус',
                        'url' => '#change_status_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Сотрудники',
                        'url' => '#change_employers_modal',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup dropup-right-align-sm']),
        Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', '#many-delete-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ])
    ],
]); ?>

<?= $this->render('partial/modals') ?>
<?= $this->render('../_common/_modals') ?>

<?= $this->registerJs('
    $(document).on("pjax:success", "#sale-point-pjax-container", function(event) {
        $("#summary-container").removeClass("visible");
    });

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("close"));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_chart_collapse").html($("#tooltip_chart_collapse").data("open"));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("close"));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $("#tooltip_help_collapse").html($("#tooltip_help_collapse").data("open"));
    });    
') ?>