<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\modules\analytics\controllers\DetailingController;
use common\models\companyStructure\SalePoint;

/* @var $this yii\web\View */
/* @var $model SalePoint */
/* @var $tab string */
/* @var $type string */
/* @var $activeYear int */

$this->title = $model->name;

$tabLink = Url::toRoute(['/analytics/detailing/view-sale-point', 'id' => $model->id]);

$tabFile = ArrayHelper::remove($tabData, 'tabFile');
$type = ArrayHelper::remove($tabData, 'type');

$user = Yii::$app->user->identity;

$logArray = Log::find()
    ->andWhere(['model_name' => $model->getClassName()])
    ->andWhere(['model_id' => $model->id])
    ->orderBy(['id' => SORT_ASC])
    ->all();
?>
    <a class="link mb-2" href="<?= Url::to(['/analytics/detailing/index', 'tab' => DetailingController::TAB_SALE_POINT]) ?>">
        Назад к списку
    </a>

    <!-- KUB -->
    <div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <div class="pr-2">
                        <div class="row align-items-center justify-content-between mb-3">
                            <h4 class="column mb-2" style="max-width: 550px;"><?= Html::encode($this->title); ?></h4>
                            <div class="column" style="margin-bottom: auto;">
                                <!-- info -->
                                <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                        type="button"
                                        data-toggle="modal"
                                        title="Последние действия"
                                        href="#basic">
                                    <svg class="svg-icon svg-icon_size_">
                                        <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                    </svg>
                                </button>
                                <!-- update -->
                                <span class="edit-sale-point button-regular button-regular_red button-clr w-44 mb-2 ml-1" title="Изменить" data-id="<?= $model->id ?>">
                                    <svg class="svg-icon">
                                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Тип</div>
                                    <?= Html::encode($model->type->name) ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Касса</div>
                                    <?= implode('<br/>', ArrayHelper::map($model->cashboxes, 'id', 'name')) ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Склад</div>
                                    <?= $model->store->name ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Сотрудники</div>
                                    <?= implode('<br/>', ArrayHelper::map($model->employers, 'id', function($e) { return $e->getShortFio(); })) ?>
                                </div>

                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="column col-12">
                                <div class="col-3 ml-0 pl-0 pull-left">
                                    <div class="label col-12 mb-3">Город</div>
                                    <?= Html::encode($model->city) ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Адрес</div>
                                    <?= Html::encode($model->address) ?>
                                </div>
                                <div class="col-3 pull-left">
                                    <div class="label col-12 mb-3">Статус</div>
                                    <div id="projectStatuses">
                                        <?= ($model['status_id'] === SalePoint::STATUS_ACTIVE)
                                            ? SalePoint::STRING_ACTIVE
                                            : SalePoint::STRING_CLOSED;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 column pl-0">
                    <div class="mb-1 text-right">
                        <?= $this->render('../_common/_select_year', [
                            'company' => $model->company,
                            'year' => $activeYear,
                            'tab' => $tab
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs-row">
        <?= Nav::widget([
            'id' => 'contractor-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-2 pb-1'],
            'items' => [
                [
                    'label' => 'Дашборд',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DASHBOARD,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_DASHBOARD ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'ОДДС',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_ODDS,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_ODDS ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'ОПиУ',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_PAL,
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($tab == DetailingController::TAB_VIEW_PAL ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Нам должны',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DEBTOR . '&type=2',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . (($tab == DetailingController::TAB_VIEW_DEBTOR && $type == 2) ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Мы должны',
                    'url' => $tabLink . '&tab=' . DetailingController::TAB_VIEW_DEBTOR . '&type=1',
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . (($tab == DetailingController::TAB_VIEW_DEBTOR && $type == 1) ? ' active' : '')
                    ],
                ],
            ],
        ]); ?>
    </div>

<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab active">
            <?= $this->render("{$tabFile}", $tabData); ?>
        </div>
    </div>
<?php endif ?>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>
    <table class="table table-style">
        <thead>
        <tr>
            <th>Дата</th>
            <th>Тип</th>
            <th>Сотрудник</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($logArray as $log): ?>
            <tr>
                <td><?= DateHelper::format(date('Y-m-d', $log->created_at), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></td>
                <td><?= ArrayHelper::getValue(DocumentLogWidget::$eventsF, $log->log_event_id) ?></td>
                <td><?= $log->employeeCompany ? $log->employeeCompany->fio : ($log->employee ? $log->employee->fio : '') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php Modal::end(); ?>

<?= $this->render('partial/modals') ?>