<script>
    window.ChartGroup = {

        chart_11: {},
        chart_12: {},
        chart_13: {},
        chart_14: {},

        chart_21: {},
        chart_22: {},
        chart_23: {},
        chart_24: {},

        chart_31: {},
        chart_32: {},
        chart_33: {},
        chart_33b: {},
        chart_34: {},
        chart_34b: {},

        chart_41: {},
        chart_42: {},
        chart_43: {},
        chart_44: {},
    };
</script>

<div class="wrap wrap_count analytics-chart-group" style="margin-top:20px;">
    <div class="row">
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_11.php', [
                    'id' => 'chart_11',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_12.php', [
                    'id' => 'chart_12',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_13.php', [
                    'id' => 'chart_13',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border border-red">
                <?= $this->render('chart_group/chart_14.php', [
                    'id' => 'chart_14',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_21.php', [
                    'id' => 'chart_21',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border border-red">
                <?= $this->render('chart_group/chart_22.php', [
                    'id' => 'chart_22',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border border-red">
                <?= $this->render('chart_group/chart_23.php', [
                    'id' => 'chart_23',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border border-red">
                <?= $this->render('chart_group/chart_24.php', [
                    'id' => 'chart_24',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_31_32.php', [
                    'id1' => 'chart_31',
                    'id2' => 'chart_32',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_33.php', [
                    'id' => 'chart_33',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
            <div class="kub-chart border mt-12px">
                <?= $this->render('chart_group/chart_33b.php', [
                    'id' => 'chart_33b',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border">
                <?= $this->render('chart_group/chart_34.php', [
                    'id' => 'chart_34',
                    'company' => $company,
                    'searchModel' => $searchModel,
                    'cashStatisticInfo' => $cashStatisticInfo,
                    'foraignCashStatisticInfo' => $foraignCashStatisticInfo
                ]) ?>
            </div>
            <div class="kub-chart border mt-12px">
                <?= $this->render('chart_group/chart_34b.php', [
                    'id' => 'chart_34b',
                    'company' => $company,
                    'searchModel' => $searchModel,
                    'currencyInfo' => $currencyInfo
                ]) ?>
            </div>
        </div>
        <div class="col-9">
            <div class="kub-chart border">
                <div class="row">
                    <div class="col-4">
                        <?= $this->render('chart_group/chart_41.php', [
                            'id' => 'chart_41',
                            'company' => $company,
                            'searchModel' => $searchModel,
                        ]) ?>
                    </div>
                    <div class="col-4">
                        <div class="wrap wrap-block border-right" style="min-height: 245px">
                            <div style="margin-top:-15px">
                                <?= $this->render('chart_group/chart_42.php', [
                                    'id' => 'chart_42',
                                    'company' => $company,
                                    'searchModel' => $searchModel,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <?= $this->render('chart_group/chart_43.php', [
                            'id' => 'chart_43',
                            'company' => $company,
                            'searchModel' => $searchModel,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="kub-chart border border-red">
                <?= $this->render('chart_group/chart_44.php', [
                    'id' => 'chart_44',
                    'company' => $company,
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });
</script>

<style>

    .analytics-chart-group .kub-chart .wrap-block {
        padding: 0 10px;
        margin: 15px 0;
        border-radius: unset!important;
    }
    .analytics-chart-group .kub-chart .ht-caption {
        font-size: 14px;
    }
    .analytics-chart-group .kub-chart .ht-table {
        font-size: 12px;
        font-weight: normal;
    }
    .analytics-chart-group .kub-chart .ht-table .toggle-string td,
    .analytics-chart-group .kub-chart .ht-table .single-string td {
        font-size: 14px;
    }

    #chart_11, #chart_12, #chart_13, #chart_14 {
        height: 210px;
    }
    #chart_21, #chart_22, #chart_23, #chart_24 {
        height: 275px;
    }
    #chart_31 {
        height: 200px;
    }
    #chart_32 {
        height: 175px;
    }
    #chart_33, #chart_34b {
        height: 135px;
    }
    #chart_33b, #chart_34, #chart_42 {
        height: 225px;
    }
    #chart_43 {
        height: 275px;
    }
    #chart_44 {
        height: 256px;
    }

    #chart_11 .highcharts-axis-labels, #chart_12 .highcharts-axis-labels, #chart_13 .highcharts-axis-labels, #chart_14 .highcharts-axis-labels,
    #chart_21 .highcharts-axis-labels, #chart_22 .highcharts-axis-labels, #chart_23 .highcharts-axis-labels, #chart_24 .highcharts-axis-labels,
    #chart_31 .highcharts-axis-labels, #chart_32 .highcharts-axis-labels, #chart_33 .highcharts-axis-labels, #chart_33b .highcharts-axis-labels,
    #chart_42 .highcharts-axis-labels {
        z-index: -1!important;
    }
    /* tooltip overflow */
    #chart_31, #chart_31 .highcharts-container , #chart_31 svg,
    #chart_42, #chart_42 .highcharts-container , #chart_42 svg
    {
        overflow: visible!important;
        z-index: 1!important;
    }
</style>