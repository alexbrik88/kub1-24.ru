<?php
/* @var $this yii\web\View */

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\image\EasyThumbnailImage;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var Company $company */
/** @var array $cashStatisticInfo */
/** @var array $foraignCashStatisticInfo */
/** @var array $currencyInfo */
/** @var DashboardChartSearch $searchModel */

$this->title = 'Личный кабинет';
$this->context->layoutWrapperCssClass = 'home-page home-page-analytics';
$invoiceTextVariants = ['счёт', 'счёта', 'счетов'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;

//
//$ioType = Documents::IO_TYPE_OUT;
//$invCount = $company->getInvoiceLeft($ioType);
//$dateRange = StatisticPeriod::getSessionPeriod();
?>
<div class="stop-zone-for-fixed-elems site-index">

    <div class="wrap d-flex flex-wrap align-items-center" style="padding: 12px 24px">
        <h4>
            <?= $company->getTitle(true) ?>
            <?= Html::a(Icon::get('cog'), Url::to(['/company/index']), [
                'id' => 'update-company_profile',
                'class' => 'company-cog-btn dropdown-btn-icon ml-1'
            ]); ?>
        </h4>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Service::HOME_LOGO)): ?>
            <div class="ml-auto">
                <?php if (is_file($path = $company->getImage('logoImage'))) : ?>
                    <img src="<?= EasyThumbnailImage::thumbnailSrc($path, $logoWidth, $logoHeight, EasyThumbnailImage::THUMBNAIL_INSET, []) ?>"
                         style="max-height: 90px"/>
                <?php endif ?>
            </div>
        <?php endif; ?>
    </div>

    <?= $this->render('_chart_group', [
        'company' => $company,
        'searchModel' => $searchModel,
        'cashStatisticInfo' => $cashStatisticInfo,
        'foraignCashStatisticInfo' => $foraignCashStatisticInfo,
        'currencyInfo' => $currencyInfo
    ]) ?>

</div>