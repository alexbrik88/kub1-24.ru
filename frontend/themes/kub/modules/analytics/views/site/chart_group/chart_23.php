<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

$jsModel = DC::getJsModel($id);

$customMonth = $customMonth ?? date('m');

$factAmount = 1585400;
$planAmount = 3000000;


echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'series' => [
                [
                    'name' => 'Факт/План',
                    'data' => [round($factAmount / $planAmount * 100)],
                ]
            ],
            'title' => [
                'text' => '<b>МАРЖА</b> за январь' // todo
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Факт/План',
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipSimple(DC::TOOLTIP_PERCENT)
            ]
        ]
    ], DC::CHART_GAUGE)
);
?>
