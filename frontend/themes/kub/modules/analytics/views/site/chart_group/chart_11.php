<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var DashboardChartSearch $searchModel */

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisHours($leftOffset, $rightOffset, $customOffset);
$y = $searchModel->getSalesToday($xAxis['datePeriods']);

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> за сегодня'
            ],
            'series' => [
                [
                    'name' => 'Сумма',
                    'color' => 'rgba(54,195,176,1)',
                    'data' => $y,
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterHours($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ])
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
</script>