<?php

use common\components\debts\DebtsHelper;
use frontend\modules\reports\models\PaymentCalendarSearch;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\db\Query;

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

//$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
//$yAxis = ['y' => [100000, 50000, 100000, 50000, 100000, 50000, 100000]]; // todo: NEED CALC, array of dates: $xAxis['datePeriods']


//todo: temp, need get data with 1 sql-query

/* @var $pcModel \frontend\modules\reports\models\PaymentCalendarSearch
 */
$LEFT_DAYS = 7;
$CENTER_DATE = date('Y-m-d');


$pcModel = new PaymentCalendarSearch();
$datePeriods = $pcModel->getFromCurrentDaysPeriods($LEFT_DAYS, 0);
$prevYearDatePeriods = [];
$xAxis['x'] = [];
$xAxis['labelsX'] = [];
$daysMin = $LEFT_DAYS;
$y = [];
$xAxis['freeDays'] = [];
$xAxis['currDayPos'] = -1;
foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $prevYearDatePeriods[] = $dateArr[0] -1 .'-'. $dateArr[1] .'-'. $dateArr[2];
    $day = $dateArr[2];
    $xAxis['x'][] = $day;
    $xAxis['freeDays'][] = (in_array(date('w', strtotime($date['from'])), [0,6]));

    $xAxis['labelsX'][] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
            'format' => 'F',
            'monthInflected' => true,
            'date' => $date['from'],
        ]);

    $query = DebtsHelper::getOverdueDebtsByPeriod(['min' => $daysMin], 1);
    $y[] = 1/100 * (float)(new Query)->from(['t' => $query])->sum('sum');
    $daysMin--;

    if ($CENTER_DATE == $date['from'])
        $xAxis['currDayPos'] = $i;
}

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>ДИНАМИКА</b> по кредиторке'
            ],
            'series' => [
                [
                    'name' => 'Сумма',
                    'color' => 'rgba(51, 109, 154, 1)',
                    'data' => $y,
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'yAxis' => [
                'labels' => [
                    'enabled' => false
                ]
            ],
            'legend' => [
                'enabled' => false
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ])
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>