<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var DashboardChartSearch $searchModel */

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$y = $searchModel->getAverageCheckByDays($xAxis['datePeriods']);


echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>Средний чек</b> по дням'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => 'rgba(243,183,46,1)',
                    'data' => $y['fact'],
                ],
                [
                    'name' => 'План',
                    'color' => 'rgba(0,0,0,1)',
                    'data' => $y['plan'],
                    'dashStyle' => 'ShortDash',
                    'marker' => [
                        'enabled' => false
                    ]
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ])
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>