<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var DashboardChartSearch $searchModel */

$jsModel = DC::getJsModel($id);

$color = 'rgba(46,159,191,1)';
$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$y = $searchModel->getSalesByDays($xAxis['datePeriods']);

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column'
            ],
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> по дням'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'data' => $y['fact'],
                ],
                [
                    'type' => 'scatter',
                    'name' => 'План',
                    'color' => 'rgba(0,0,0,1)',
                    'data' => $y['plan'],
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ], DC::CHART_COLUMN)
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>