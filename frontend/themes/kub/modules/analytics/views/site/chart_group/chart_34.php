<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;

/**
 * @var $cashStatisticInfo array
 * @var $foraignCashStatisticInfo array
 */

$cashStatisticsDate = date('Y-m-d');
?>
<div id="<?=($id)?>" class="wrap wrap-block" style="overflow: auto">
    <div class="ht-caption">
        <b>ДЕНЬГИ</b> <span style="text-transform: none"> на </span> <?= DateHelper::format($cashStatisticsDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
    </div>
    <table class="ht-table dashboard-cash">
        <?php foreach ($cashStatisticInfo as $key => $item) : ?>
            <?php $isSubstring = in_array($item['cssClass'], ['cash-bank-substring', 'cash-order-substring', 'cash-emoney-substring']); ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <?= $item['typeName']; ?>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <?= TextHelper::invoiceMoneyFormat($item['endBalance'], 2); ?>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php foreach ($foraignCashStatisticInfo as $key => $item) : ?>
            <?php $item['cssClass'] = $item['cssClass'] ?? ''; ?>
            <?php $item['target'] = $item['target'] ?? ''; ?>
            <?php $item['typeName'] = $item['typeName'] ?? ''; ?>
            <?php $item['endBalance'] = $item['endBalance'] ?? '' ?>
            <?php $isSubstring = strpos($item['cssClass'], 'substring') !== false; ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td style="<?= $isSubstring ? 'padding-left: 5px;' : '' ?>">
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <?= $item['typeName']; ?>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <?= TextHelper::invoiceMoneyFormat($item['endBalance'], 2); ?>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<script>
    $(document).on("click", "#<?=($id)?> table.dashboard-cash .toggle-string .flow-label", function() {
        $("#<?=($id)?> table.dashboard-cash " + $(this).data("target") + "").slideToggle(150);
    });
</script>