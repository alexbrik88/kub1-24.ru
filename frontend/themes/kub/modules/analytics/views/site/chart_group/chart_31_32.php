<?php

use yii\web\JsExpression;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

/** @var string $id1 */
/** @var string $id2 */
/** @var $model PaymentCalendarSearch */

///////////////// dynamic vars /////////
$customOffset = $customOffset ?? 0;
$customPeriod = "days";
$customPurse = $customPurse ?? null;
/////////////// consts /////////////////
$ON_PAGE = 'dashboard';
/////////// chart params ///////////////
$leftOffset = 6;
$rightOffset = 6;
$moveOffset = 1;
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';
////////////////////////////////////////

$jsModel = DC::getJsModel($id1);
$model = new PaymentCalendarSearch();

// x-axis
$xAxis = DC::getXAxisDaysWithWrapPoints($leftOffset, $rightOffset, $customOffset);
$prevYearXAxis = DC::getXAxisDaysWithWrapPoints($leftOffset, $rightOffset, $customOffset, -1);
// data
$mainData = $model->getPlanFactSeriesData($xAxis['datePeriods'], $customPurse, $customPeriod);
$prevMainData = $model->getPlanFactSeriesData($prevYearXAxis['datePeriods'], $customPurse, $customPeriod);
// y-axis
$yAxis = DC::recalcChartPlanFactSeries($model, $xAxis, $mainData, $prevMainData);
// tooltip
$tooltipData = DC::getChartPlanFactTooltipData($model, $xAxis);
$tooltipTemplate = DC::getChartPlanFactTooltipTemplate();
?>

<?= \miloschuman\highcharts\Highcharts::widget(DC::getOptions([
    'id' => $id1,
    'options' => [
        'chart' => [
            'type' => 'column',
            'events' => [
                'load' => DC::getEvents($id1, ['redrawPlanMonths'])
            ],
            'marginBottom' => '50',
            'marginLeft' => '55',
        ],
        'legend' => [
            'symbolRadius' => 2,
            'itemDistance' => 15
        ],
        'tooltip' => [
            'formatter' => DC::getTooltipChartPlanFactTop($jsModel),
            'positioner' => DC::getPositionerChartPlanFactTop($jsModel),
            //'hideDelay' => 99999,
        ],
        'title' => [
            'text' => '<b>ПРИХОД - РАСХОД</b> по дням',
        ],
        'yAxis' => [
            'min' => 0,
            'index' => 0,
            'title' => '',
            'minorGridLineWidth' => 0,
        ],
        'xAxis' => [
            'min' => 1,
            'max' => ($leftOffset + $rightOffset - 1),
            'categories' => $xAxis['x'],
            'labels' => [
                'formatter' => DC::getXAxisFormatterDaysWithWrapPoints($jsModel),
            ],
        ],
        'series' => [
            [
                'name' => 'Приход Факт',
                'data' => $yAxis['incomeFlowsFactPlan'],
                'color' => $color1,
                'borderColor' => 'rgba(46,159,191,.3)',
                'states' => DC::getSerieState($color1)
            ],
            [
                'name' => 'Расход Факт',
                'data' => $yAxis['outcomeFlowsFactPlan'],
                'color' => $color2,
                'borderColor' => $color2_opacity,
                'states' => DC::getSerieState($color2)
            ],
            [
                'name' => 'Приход План',
                'data' => $yAxis['incomeFlowsPlan'],
                'marker' => [
                    'symbol' => 'c-rect',
                    'lineWidth' => 3,
                    'lineColor' => 'rgba(21,67,96,1)',
                    'radius' => 9.5
                ],
                'type' => 'scatter',
                'pointPlacement' => -0.205,
                'stickyTracking' => false,
            ],
            [
                'name' => 'Расход План',
                'data' => $yAxis['outcomeFlowsPlan'],
                'marker' => [
                    'symbol' => 'c-rect',
                    'lineWidth' => 3,
                    'lineColor' => 'rgba(50,50,50,1)',
                    'radius' => 9.5
                ],
                'type' => 'scatter',
                'pointPlacement' => 0.19,
                'stickyTracking' => false,
            ]
        ],
        'plotOptions' => [
            'column' => [

                'pointWidth' => 18,
                'shadow' => false,
                'groupPadding' => 0.1,
                'pointPadding' => 0.0,
                'borderRadius' => 3,

                //'groupPadding' => 0.05,
                //'pointPadding' => 0.1,
                //'borderRadius' => 3,
            ]
        ],
    ],
])); ?>

<?= \miloschuman\highcharts\Highcharts::widget(DC::getOptions([
    'id' => $id2,
    'options' => [
        'chart' => [
            'type' => 'areaspline',
            'marginBottom' => '50',
            'marginLeft' => '55',
        ],
        'legend' => [
            'symbolRadius' => 2,
            'itemDistance' => 15
        ],
        'tooltip' => [
            'formatter' => DC::getTooltipChartPlanFactBottom($jsModel)
            //'hideDelay' => 9999,
        ],
        'title' => [
            'text' => '<b>ОСТАТОК ДЕНЕЖНЫХ СРЕДСТВ</b> по дням',
        ],
        'yAxis' => [
            'min' => null,
            'max' => null,
            'index' => 0,
            'title' => '',
            'minorGridLineWidth' => 0,
        ],
        'xAxis' => [
            'min' => 1,
            'max' => ($leftOffset + $rightOffset - 1),
            'categories' => $xAxis['x'],
            'title' => '',
            'minorGridLineWidth' => 0,
            'labels' => [
                'formatter' => DC::getXAxisFormatterDaysWithWrapPoints($jsModel),
            ],
        ],
        'series' => [
            [
                'name' => 'Остаток Факт (предыдущий год)',
                'data' => $yAxis['balanceFactPrevYear'],
                'color' => 'rgba(129,145,146,1)',
                'fillColor' => 'rgba(149,165,166,1)',
                'dataLabels' => [
                    'enabled' => false
                ],
                'legendIndex' => 2
            ],
            [
                'name' => 'Остаток План',
                'data' => $yAxis['balancePlan'],
                'color' => 'rgba(26,184,93,1)',
                'negativeColor' => 'red',
                'fillColor' => [
                    'pattern' => [
                        //'image' => '/img/pattern1.png',
                        'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                        'color' => '#27ae60',
                        'width' => 10,
                        'height' => 10
                    ]
                ],
                'negativeFillColor' => [
                    'pattern' => [
                        'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                        'color' => '#e74c3c',
                        'width' => 10,
                        'height' => 10
                    ]
                ],
                'marker' => [
                    'symbol' => 'square'
                ],
                'dataLabels' => [
                    'enabled' => false
                ],
                'legendIndex' => 1,
                'showInLegend' => false
            ],
            [
                'name' => 'Остаток Факт',
                'data' => $yAxis['balanceFact'],
                'color' => 'rgba(26,184,93,1)',
                'fillColor' => 'rgba(46,204,113,1)',
                'negativeColor' => 'red',
                'negativeFillColor' => 'rgba(231,76,60,1)',
                'dataLabels' => [
                    'enabled' => false
                ],
                'legendIndex' => 0
            ],
        ],
        'plotOptions' => [
            'areaspline' => [
                'fillOpacity' => .9,
                'marker' => [
                    'enabled' => false,
                    'symbol' => 'circle',
                ],
                'dataLabels' => [
                    'enabled' => true
                ],
            ],
            'series' => [
                'stickyTracking' => false,
            ]
        ],
    ],
])); ?>

<script>
    <?=($jsModel)?>.period = '<?= $customPeriod ?>';
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.wrapPointPos = <?= json_encode($xAxis['wrapPointPos']) ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
    <?=($jsModel)?>.tooltipData = {
        jsArrIncome: <?= json_encode($tooltipData['arrIncome']) ?>,
        jsArrOutcome: <?= json_encode($tooltipData['arrOutcome']) ?>,
        jsArrIncomeTotal: <?= json_encode($tooltipData['arrIncomeTotal']) ?>,
        jsArrOutcomeTotal: <?= json_encode($tooltipData['arrOutcomeTotal']) ?>,
        jsArrDate: <?= json_encode($tooltipData['arrDate']) ?>,
    };
    <?=($jsModel)?>.tooltipTemplate = {
        htmlHeaderFact: '<?= $tooltipTemplate['htmlHeaderFact'] ?>',
        htmlHeaderPlan: '<?= $tooltipTemplate['htmlHeaderPlan'] ?>',
        htmlData: '<?= $tooltipTemplate['htmlData'] ?>',
        htmlFooter: '<?= $tooltipTemplate['htmlFooter'] ?>',
    };
    <?=($jsModel)?>.color1 = '<?= $color1 ?>';
    <?=($jsModel)?>.color2 = '<?= $color2 ?>';

    <?=($jsModel)?>.getWrapPointXLabel = <?= DC::jsGetXAxisWrapPointXLabel($id1, $jsModel) ?>;
</script>