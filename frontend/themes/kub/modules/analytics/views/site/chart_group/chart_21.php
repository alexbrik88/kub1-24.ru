<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var DashboardChartSearch $searchModel */

$jsModel = DC::getJsModel($id);

$customMonth = $customMonth ?? date('m');
$customYear = date('Y');

$y = $searchModel->getSalesByMonthScalar($customYear, $customMonth);

$value = round($y['fact'] / ($y['plan'] ?: $y['fact']) * 100);




echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'series' => [
                [
                    'name' => 'Факт/План',
                    'data' => [$value],
                ]
            ],
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> за январь' // todo
            ],
            'yAxis' => [
                'min' => 0,
                'max' => max($value, 125),
                'title' => [
                    'text' => 'Факт/План',
                ],
                'plotBands' => [
                    4 => [
                        'from' => 100,
                        'to' => max($value, 125),
                        'color' => '#4bdf4e', // green
                        'thickness' => '35%'
                    ]
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipSimple(DC::TOOLTIP_PERCENT)
            ]
        ]
    ], DC::CHART_GAUGE)
);
?>
