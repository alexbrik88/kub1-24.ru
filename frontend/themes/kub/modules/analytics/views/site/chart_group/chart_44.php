<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;

$creditStatisticsInfo = [
    [
        'typeName' => 'Текущая задолженность',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 6.5E8,
        'inTotal' => true,
        'target' => '.credit-substring-1',
        'cssClass' => 'toggle-string',
    ],
    [
        'typeName' => 'ПАО "Сбербанк"',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 5E8,
        'inTotal' => false,
        'target' => null,
        'cssClass' => 'credit-substring-1',
    ],
    [
        'typeName' => 'ПАО "Альфабанк"',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 1.5E8,
        'inTotal' => false,
        'target' => null,
        'cssClass' => 'credit-substring-1',
    ],
    [
        'typeName' => 'Погасить в октябре',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 1.3E8,
        'inTotal' => true,
        'target' => '.credit-substring-2',
        'cssClass' => 'toggle-string',
    ],
    [
        'typeName' => 'Кредит',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 1E8,
        'inTotal' => false,
        'target' => null,
        'cssClass' => 'credit-substring-2',
    ],
    [
        'typeName' => 'Проценты',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 0.3E8,
        'inTotal' => false,
        'target' => null,
        'cssClass' => 'credit-substring-2',
    ],
    [
        'typeName' => 'Доступный лимит',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 4.5E8,
        'inTotal' => true,
        'target' => '.credit-substring-3',
        'cssClass' => 'toggle-string',
    ],
    [
        'typeName' => 'ПАО "Альфабанк"',
        'bik' => null,
        'beginBalance' => 0,
        'income' => 0,
        'expense' => 0,
        'endBalance' => 4.5E8,
        'inTotal' => false,
        'target' => null,
        'cssClass' => 'credit-substring-3',
    ],
];

/**
 * @var $creditStatisticsInfo array
 */

$creditStatisticsDate = date('Y-m-d');
?>
<div id="<?=($id)?>" class="wrap wrap-block" style="overflow: auto">
    <div class="ht-caption">
        <b>КРЕДИТЫ</b> <span style="text-transform: none"> на </span> <?= DateHelper::format($creditStatisticsDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
    </div>
    <table class="ht-table dashboard-cash">
        <?php foreach ($creditStatisticsInfo as $key => $item) : ?>
            <?php $isSubstring = in_array($item['cssClass'], ['credit-substring-1', 'credit-substring-2', 'credit-substring-3']); ?>
            <tr class="<?= $item['cssClass'] ?>" <?= ($isSubstring) ? 'style="display:none"' : '' ?>>
                <td>
                    <div>
                        <span class="ht-empty-wrap flow-label <?= 'toggle-string' == $item['cssClass'] ? ' link' : '' ?>" data-target="<?= $item['target'] ?>">
                            <?= $item['typeName']; ?>
                            <?php if ('toggle-string' != $item['cssClass']): ?><i class="ht-empty left"></i><?php endif; ?>
                        </span>
                    </div>
                </td>
                <td class="nowrap">
                    <span class="ht-empty-wrap">
                        <?= TextHelper::invoiceMoneyFormat($item['endBalance'], 2); ?>
                        <i class="ht-empty right"></i>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<script>
    $(document).on("click", "#<?=($id)?> table.dashboard-cash .toggle-string .flow-label", function() {
        $("#<?=($id)?> table.dashboard-cash " + $(this).data("target") + "").slideToggle(150);
    });
</script>