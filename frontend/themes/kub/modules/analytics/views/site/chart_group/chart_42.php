<?php
use common\models\Contractor;
use frontend\models\Documents;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

/** @var  $docType int */
/** @var $model DebtReportSearch2 */

$jsModel = DC::getJsModel($id);
$model = new DebtReportSearch2();

/////////////// consts //////////////////
$color = 'rgba(51, 109, 154, 1)';
$maxRowsCount = 8;
/////////////////////////////////////////

$docType = Documents::IO_TYPE_IN;
$model->report_type = DebtReportSearch2::REPORT_TYPE_MONEY;
$model->search_by = DebtReportSearch2::SEARCH_BY_DOCUMENTS;
$model->findDebtor(Yii::$app->request->get(), $docType); // construct main query

$topData = $model->getTopByContractors($maxRowsCount);
$jsTopData = DC::getChartTopCreditorsTooltipData($topData);

$categories = $debt = $contractorsIds = [];
if ($topData) {
    foreach ($topData as $data) {
        $categories[] = ($c = Contractor::findOne($data['id'])) ? $c->getNameWithType() : $data['name'];
        $debt[] = round($data['debt_all_sum'] / 100, 2);
        $contractorsIds[] = $data['id'];
    }
} else {
    $categories[] = 'Нет данных';
    $debt[] = 0;
    $contractorsIds = [];
}

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => DC::calcChartColumnHeight(count($categories)),
                'events' => [
                    'load' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                    'redraw' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                ]
            ],
            'title' => [
                'text' => ($docType == 2) ? '<b>ТОП ДОЛЖНИКОВ</b>' : '<b>ТОП КРЕДИТОРОВ</b>'
            ],
            'series' => [
                [
                    'name' => ($docType == 2) ? 'Сумма долга' : 'Сумма кредита',
                    'data' => $debt,
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'pointPadding' => 0.14,
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' '); }"),
                        'style' => [
                            'fontSize' => '11px',
                            'fontWeight' => '400',
                            'color' => '#001424',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                ],
            ],
            'yAxis' => [
                'labels' => false,
                'gridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => $categories,
                'gridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0,
                'reserveSpace' => true,
                'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
            ],
            'tooltip' => [
                'positioner' => DC::getChartTopCreditorsTooltipPositioner(),
                'formatter' => DC::getTooltipChartTopCreditors($jsModel, $color),
            ],
        ]
    ], DC::CHART_COLUMN)
);
?>

<script>
    <?=($jsModel)?>.topData = <?= $jsTopData; ?>;
</script>