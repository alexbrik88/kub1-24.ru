<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

////////// chart params /////////////////////////
$chartMaxRows = 5;
$customPurse = $customPurse ?? null;
$customMonth = $customMonth ?? date('m');
////////////////////////////////////////////////

$model = new ExpensesSearch();
$jsModel = DC::getJsModel($id);

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$mainData = $model->getChartStructureByArticles($dateFrom, $dateTo, $customPurse);
$yAxis['categories'] = array_slice(array_column($mainData, 'name'), 0, $chartMaxRows);
$yAxis['factData'] = array_slice(array_column($mainData, 'amountFact'), 0, $chartMaxRows);
$yAxis['planData'] = array_slice(array_column($mainData, 'amountPlan'), 0, $chartMaxRows);
$yAxis['articlesIds'] = array_slice(array_column($mainData, 'id'), 0, $chartMaxRows);
$yAxis['expenseTotalAmount'] = array_sum($yAxis['factData']);

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => DC::calcChartColumnHeight(count($yAxis['categories'])),
                'events' => [
                    'load' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                    'redraw' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                ]
            ],
            'title' => [
                'text' => '<b>РАСХОД</b> по статьям'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => 'rgba(243,183,46,1)',
                    'states' => DC::getSerieState('rgba(243,183,46,1)'),
                    'data' => $yAxis['factData'],
                    'pointPadding' => 0.14,
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' '); }"),
                        'style' => [
                            'fontSize' => '11px',
                            'fontWeight' => '400',
                            'color' => '#001424',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                ],
                [
                    'name' => 'План',
                    'color' => 'rgba(0,0,0,1)',
                    'data' => $yAxis['planData'],
                    'pointWidth' => 4,
                    'borderWidth' => 0,
                ],
            ],
            'yAxis' => [
                'labels' => false,
                'gridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => $yAxis['categories'],
                'gridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0,
                'reserveSpace' => true,
                'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipChartExpensesByArticles($jsModel)
            ],
        ]
    ], DC::CHART_COLUMN)
);
?>

<script>
    <?=($jsModel)?>.expenseTotalAmount = <?= $yAxis['expenseTotalAmount']; ?>;
</script>
