<?php

use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$color = 'rgba(131,142,201,1)';
$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$yAxis = ['y' => [100, 50, 100, 50, 100, 50, 100]]; // todo: NEED CALC

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column'
            ],
            'title' => [
                'text' => '<b>Посещаемость</b> сайта'
            ],
            'series' => [
                [
                    'name' => 'Кол-во',
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'data' => $yAxis['y'],
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel, DC::TOOLTIP_NO_UNITS)
            ]
        ]
    ], DC::CHART_COLUMN)
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>