<?php

use php_rutils\RUtils;
use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use frontend\modules\analytics\models\dashboardChart\DashboardChartSearch;

/** @var DashboardChartSearch $searchModel */

$jsModel = DC::getJsModel($id);

$customMonth = $customMonth ?? date('m');
$customYear = $customYear ?? date('Y');

$prevMonth = str_pad(($customMonth > 1) ? ($customMonth - 1) : 12, 2, "0", STR_PAD_LEFT);
$prevYear = ($customMonth > 1) ? $customYear : ($customYear - 1);

$categories = [
    DC::getMonthYearName($prevYear, $prevMonth),
    DC::getMonthYearName($customYear, $customMonth)

];
// todo: NEED CALC
$y = [
    $searchModel->getIncomeCashByMonthScalar($prevYear, $prevMonth) / 100,
    $searchModel->getIncomeCashByMonthScalar($customYear, $customMonth) / 100
];


$yAxis = [
    'y' => $y
];

$color = 'rgba(132,197,201,1)';

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'events' => [
                    'load' => DC::getEvents($id, ['alignRightColumnDataLabels'], ['offset' => 50]),
                    'redraw' => DC::getEvents($id, ['alignRightColumnDataLabels'], ['offset' => 50]),
                ],
                'marginRight' => 50,
                'marginBottom' => 0
            ],
            'title' => [
                'text' => '<b>ПРИХОД</b>'
            ],
            'legend' => [
                'enabled' => false
            ],
            'series' => [
                [
                    'name' => 'Приход',
                    'data' => $yAxis['y'],
                    'groupPadding' => 0.22,
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽'; }"),
                        'style' => [
                            'fontSize' => '12px',
                            'fontWeight' => '400',
                            'color' => '#001424',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'colorByPoint' => true,
                    'colors' => [
                        'rgba(132,197,201,1)',
                        'rgba(46,159,191,1)'
                    ],
                    'states' => DC::getSerieState('rgba(132,197,201,1)'),

                ],
            ],
            'yAxis' => [
                'labels' => false,
                'gridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => $categories,
                'gridLineWidth' => 0,
                'lineWidth' => 0,
                'labels' => [
                    'x' => 2,
                    'y' => -14,
                    'align' => 'left',
                    'style' => [
                        'fontSize' => '12px',
                        'fontWeight' => '700',
                        'color' => '#9198a0',
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        'whiteSpace' => 'nowrap'
                    ]
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipSimple()
            ],
        ]
    ], DC::CHART_COLUMN)
);
?>