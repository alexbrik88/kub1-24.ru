<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

$jsModel = DC::getJsModel($id);

$color = 'rgba(46,159,191,1)';
$categories = ['Магазин 1', 'Магазин 1', 'Сайт', 'Магазин 3', 'Магазин 4'];
// todo: NEED CALC
$yAxis = [
    'y' => [
        'fact' => [100000, 50000, 100000, 50000, 100000],
        'plan' => [75000, 75000, 75000, 75000, 75000]
    ]
];

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => DC::calcChartColumnHeight(count($categories)),
                'events' => [
                    'load' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                    'redraw' => DC::getEvents($id, ['alignRightColumnDataLabels']),
                ]
            ],
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> по магазинам'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'data' => $yAxis['y']['fact'],
                    'pointPadding' => 0.14,
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 2, ',', ' '); }"),
                        'style' => [
                            'fontSize' => '11px',
                            'fontWeight' => '400',
                            'color' => '#001424',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                ],
                [
                    'name' => 'План',
                    'color' => 'rgba(0,0,0,1)',
                    'data' => $yAxis['y']['plan'],
                    'pointWidth' => 4,
                    'borderWidth' => 0
                ],
            ],
            'yAxis' => [
                'labels' => false,
                'gridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => $categories,
                'gridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0,
                'reserveSpace' => true,
                'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipSimple()
            ],
        ]
    ], DC::CHART_COLUMN )
);
?>