<?php

use miloschuman\highcharts\Highcharts;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

$jsModel = DC::getJsModel($id);

$series = [
    'name' => 'Источник',
    'colorByPoint' => true,
    'data' => [
        ['name' => 'Яндекс', 'y' => 36, 'color' => '#f3b73b'],
        ['name' => 'Google', 'y' => 23, 'color' => '#001424'],
        ['name' => 'Соц сети', 'y' => 12, 'color' => '#848dc9'],
        ['name' => 'SEO', 'y' => 15, 'color' => '#84c5c9'],
        ['name' => 'Прямые', 'y' => 14, 'color' => '#c98484']
    ]
];

echo Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>ИСТОЧНИК ТРАФИКА</b> на САЙТ'
            ],
            'series' => [$series],
            'tooltip' => [
                'formatter' => DC::getTooltipPie(DC::TOOLTIP_PERCENT)
            ]
        ]
    ], DC::CHART_PIE)
);
?>