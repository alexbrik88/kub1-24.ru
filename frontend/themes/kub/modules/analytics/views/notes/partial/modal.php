<?php

use common\components\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\analytics\models\Notes;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/** @var Notes $model */
/** @var Employee $employee */
/** @var Company $company */

?>
<?php
    Pjax::begin([
        'id' => 'modal-notes-item-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
    ]);
?>
<?php $form = ActiveForm::begin([
        'id' => 'notes-form',
        'method' => 'POST',
        'validationUrl' => Url::to(['validate']),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'label text-bold',
            ],
        ],
        'options' => [
            'data-pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('id', $model->id); ?>
    <div class="form-group mt-3">
        <?= $form->field($model, 'title')->textInput([
            'maxlength' => 255,
            'id' => 'notes-title',
            'class' => 'form-control col-12',
        ]); ?>
    </div>
    <div class="form-group mt-3">
        <?= $form->field($model, 'description')
            ->textarea([
                'rows' => 7,
                'maxlength' => 500,
                'id' => 'notes-description',
                'class' => 'form-control col-12'
            ]); ?>
    </div>
    <div class="mt-4 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>