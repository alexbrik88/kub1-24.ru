<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use common\widgets\Modal;
use frontend\modules\analytics\models\Notes;
use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\web\View;

$this->title = 'Заметки';

/** @var Company $company */
/** @var Employee $employee */
/** @var Notes $notes */
/** @var Notes $note */
/** @var integer $limit */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);

$employee = Yii::$app->user->identity;
$company = $employee->company;

$this->registerCss(<<<CSS
    .note {
        display: flex;
        align-items: flex-start;
        
        border-bottom: solid 1px #c9c9c9;
    }

    .note-preview {
        display: block;
        
        width: 89%;
        float: left;
        
        margin-left: 1%;
        
        text-align: justify;
    }

    .note-controls {
        display: block;
        
        width: 9%;
        float:left;
        
        text-align: right;
        
        margin: 10px 0 30px 1%;
    }
    
    .note-controls .notes-move,
    .note-controls .notes-update,
    .note-controls .notes-delete {
        color: #9198a0;
    }
    
    .note-controls .notes-move {
        cursor: grab;
    }
    
    .note-controls .notes-move:active {
        cursor: grabbing;
    }

    .note-preview .note-preview-date {
        font-size: 1em;
        
        color: #9198a0;
        
        margin: 0 0 20px 0;    
    }

    .note-preview .note-preview-header {
        max-height: 28px;
        overflow: hidden;
        
        font-size: 1.2em;
        font-weight: bold;
        
        margin: 10px 0 10px 0;
    }

    .note-preview .note-preview-body {
        max-height: 40px;
        overflow: hidden;
        
        font-size: 1em;
        
        margin: 0 0 10px 0;
    }

    .empty-notes {
        padding: 10px 20px;
    }

    #date-creation-note {
        color: #9198a0;
    }
CSS
);

?>

    <div class="analytics-notes">
        <div class="page-head d-flex flex-wrap">
            <h4><?= Html::encode($this->title) ?></h4>
            <?= Html::a('<svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                          </svg>
                          <span>Добавить</span>',
                '#modal-notes-item',
                [
                    'class' => 'button-regular button-regular_red button-width '
                        . ($limit ? ' disabled tooltip2 ' : '')
                        . ' ml-auto notes-update ',
                    'data-tooltip-content' => '#tooltip-content',
                    'data-date-creation' => '',
                ]
            ); ?>
            <div id="template" style="display: none;">
                <div id="tooltip-content">Достингут лимит заметок</div>
            </div>
        </div>

        <div class="notes-body wrap">
            <?php if (!$notes): ?>
                <div class="empty-notes">
                    У вас пока нет ни одной заметки. Чтобы написать заметку нажмите по кнопке добавить.
                </div>
            <?php endif; ?>
            <div class="notes-container">
                <?php foreach ($notes as $note): ?>
                    <div class="note" id="note-<?= $note->index; ?>" data-index="<?= $note->index; ?>">
                        <div class="note-preview">
                            <div class="note-preview-header"><?= $note->title; ?></div>
                            <div class="note-preview-date"><?= date(DateHelper::FORMAT_USER_DATE, $note->created_at); ?></div>
                            <div class="note-preview-body"><?= $note->description; ?></div>
                        </div>
                        <div class="note-controls">
                            <?= Html::a(Icon::get('pencil'), ['#edit'], [
                                'data-id' => $note->id,
                                'data-date-creation' => date(DateHelper::FORMAT_USER_DATE, $note->created_at),
                                'data-toggle' => 'modal',
                                'class' => 'notes-update'
                            ]); ?>
                            <?= Html::a(Icon::get('circle-close'), ['#delete'], [
                                'data-id' => $note->id,
                                'class' => 'notes-delete'
                            ]); ?>
                            <?= Html::a(Icon::get('menu'), ['#move'], [
                                'data-id' => $note->id,
                                'class' => 'notes-move sortable'
                            ]); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

<?php

Modal::begin([
    'header' => '<h4></h4><div class="pull-right" id="date-creation-note"></div>',
    'id' => 'modal-notes-item',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]);

echo $this->render('partial/modal', [
    'model' => (new Notes()),
    'company' => $company,
    'employee' => $employee,
]);

Modal::end(); ?>


<?php

$this->registerJs(<<<JS
    $('.notes-update').click(function(e){
        e.preventDefault();
        var id = $(this).data('id'),
            date = $(this).data('date-creation'),
            note = $('#modal-notes-item'),
            header = "";
        
        if (!$('.notes-update.disabled').length) {
            $.pjax({
                'url': id ? 'update' : 'create',
                'type': 'post',
                'data': {'id': id},
                'container': '#modal-notes-item-pjax',
                'push': false
            });
            
            header = id ? "Редактировать заметку" : "Добавить заметку";
            
            $(document).on('pjax:complete', '#modal-notes-item-pjax', function (e) {
                note.find('.modal-header h4').html(header);
                note.find('#date-creation-note').html(date);
                note.modal('show');
            });
        }
    })
    
    $('.notes-delete').click(function(e){
        e.preventDefault();
        var url = 'delete',
            id = $(this).data('id');
        
        $.post(url, {'id': id}, function(data){
            if ('true' === data.success) {
                location.reload();
            }
        }, 'json');
    })
    
    $('.notes-container').sortable({
        start: function(event, ui) {
            ui.item.startPos = ui.item.index();
        },
        stop: function(event, ui) {
            $.post('sort', {from: ui.item.startPos, to: ui.item.index()}, function(data) {
                return 'true' === data.success;
            }, 'json');
        }
    });
JS, View::POS_READY
);