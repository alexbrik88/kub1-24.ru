<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.12.2019
 * Time: 20:03
 */

use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use frontend\modules\reference\models\SellBalanceArticleForm;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use common\models\balance\BalanceArticle;
use frontend\modules\reference\models\BalanceArticlesCategories;
use yii\bootstrap4\ActiveForm;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use yii\helpers\Url;
use common\models\cash\CashFactory;
use frontend\themes\kub\helpers\Icon;
use yii\web\JsExpression;
use yii\grid\CheckboxColumn;

/* @var $model      SellBalanceArticleForm
 * @var $type       int
 * @var $flowType   int
 * @var $contractor Contractor|null
 */

$customerArray = ["add-modal-contractor" => Icon::PLUS . ' Добавить покупателя'] + Contractor::getALLContractorList(Contractor::TYPE_CUSTOMER, false);

Pjax::begin([
    'id' => 'sell-balance-article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style>
    #sellbalancearticleform-flow_type > div.radio {
        width: 140px;
    }
</style>

<div class="sell-balance-article-form"
     data-header="Продажа <?= $type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'основного средства' : 'нематериального актива' ?>">
    <?php $form = ActiveForm::begin([
        'id' => 'sell-balance-article-form',
        'enableClientValidation' => false,
        'action' => ['sell', 'type' => $type],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]); ?>

    <table class="table table-style table-count-list form-group">
        <thead>
        <tr id="w6-filters" class="filters">
            <th width="40px">№№</th>
            <th class="sorting" width="20%">Наименование</th>
            <th class="sorting" width="20%">Кол-во</th>
            <th class="sorting" width="60%">Вид</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->getArticles() as $key => $article): ?>
            <tr>
                <td>
                    <?= ++$key ?>
                    <?= $form->field($model, 'articlesIds[]')
                        ->hiddenInput(['value' => $article->id])
                        ->label(false); ?>
                </td>
                <td>
                    <?= $article->name ?>
                </td>
                <td>
                    <?= $article->count; ?>
                </td>
                <td>
                    <?= BalanceArticlesCategories::MAP[$article->category] ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div style="float: left; width: 50%;">
        <div class="row">
            <?= Html::activeLabel($model, 'sold_at', [
                'class' => 'col-4 control-label',
                'style' => 'margin-top: 12px; max-width: 120px;'
            ]); ?>
            <div class="col-8">
                <?= $form->field($model, 'sold_at')->label(false)->textInput([
                    'class' => 'form-control date-picker ico width_date',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <div style="float: left; width: 50%;">
        <div class="row">
            <?= Html::activeLabel($model, 'amount', [
                'class' => 'col-4 control-label',
                'style' => 'margin-top: 12px; max-width: 120px;'
            ]); ?>

            <div class="col-md-8">
                <?= $form->field($model, 'amount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                    'value' => $model->amount ? TextHelper::moneyFormatFromIntToFloat($model->amount) : null,
                ])->label(false); ?>
            </div>
        </div>
    </div>

    <div style="float: left; width: 100%;">
        <div class="row">
            <?= Html::activeLabel($model, 'contractor_id', [
                'class' => 'col-4 control-label',
                'style' => 'margin-top: 12px; max-width: 120px;'
            ]); ?>
            <div class="col-10">
                <?= $form->field($model, 'contractor_id', [
                    'template' => "{input}{hint}{error}",
                ])->widget(Select2::class, [
                    'data' => $customerArray,
                    'theme' => Select2::THEME_KRAJEE_BS4,
                    'options' => [
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                    ]
                ])->label(false) ?>
            </div>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'flow-list-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
        'timeout' => 10000,
    ]); ?>

    <div class="row form-group">
        <?php $flowTypeName = Html::getInputName($model, 'flow_type'); ?>
        <div class="col-4" style="max-width: 150px;">
            <a class="flow_type" href="<?= Url::current(['flowType' => CashFactory::TYPE_BANK]) ?>"
               style="text-decoration: none;color: #333333!important;">
                <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_BANK || $flowType === null, [
                    'value' => CashFactory::TYPE_BANK,
                ]) ?>
                через банк
            </a>
        </div>
        <?php if ($model->getCashboxList()): ?>
            <div class="col-4" style="max-width: 150px;">
                <a class="flow_type" href="<?= Url::current(['flowType' => CashFactory::TYPE_ORDER]) ?>"
                   style="text-decoration: none;color: #333333!important;">
                    <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_ORDER, [
                        'value' => CashFactory::TYPE_ORDER,
                    ]) ?>
                    через кассу
                </a>
            </div>
        <?php endif; ?>
        <div class="col-4" style="max-width: 175px;">
            <a class="flow_type" href="<?= Url::current(['flowType' => CashFactory::TYPE_EMONEY]) ?>"
               style="text-decoration: none;color: #333333!important;">
                <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_EMONEY, [
                    'value' => CashFactory::TYPE_EMONEY,
                ]) ?>
                через e-money
            </a>
        </div>
    </div>

    <?php if ($flowType == CashFactory::TYPE_ORDER && $model->getCashboxList()) : ?>
        <?= $form->field($model, 'cashbox_id', [
            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group',
            ],
            'labelOptions' => [
                'class' => 'col-sm-4',
                'style' => 'display: block; max-width: 130px;',
            ]
        ])->radioList($model->getCashboxList(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif; ?>

    <?php if ($flowType == CashFactory::TYPE_EMONEY && $model->getEmoneyList()) : ?>
        <?= $form->field($model, 'emoney_id', [
            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group',
            ],
            'labelOptions' => [
                'class' => 'col-sm-4',
                'style' => 'display: block; max-width: 130px;',
            ]
        ])->radioList($model->getEmoneyList(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif ?>

    <?php
    $query = null;
    if ($contractor !== null) {
        switch ($flowType) {
            case null:
            case CashFactory::TYPE_BANK:
                $query = $contractor->getNotLinkedBankFlows($model->getUser()->currentEmployeeCompany->company);
                break;
            case CashFactory::TYPE_ORDER:
                $query = $contractor->getNotLinkedOrderFlows($model->getUser()->currentEmployeeCompany->company);
                break;
            case CashFactory::TYPE_EMONEY:
                $query = $contractor->getNotLinkedEmoneyFlows($model->getUser()->currentEmployeeCompany->company);
                break;
        }
    }

    if ($query !== null) {
        $flowProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $flowProvider->pagination->pageSize = 0;
        if ($flowProvider->totalCount) {
            echo GridView::widget([
                'dataProvider' => $flowProvider,
                'filterModel' => $model,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list form-group',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'class' => CheckboxColumn::class,
                        'cssClass' => 'flow_item_check',
                        'name' => Html::getInputName($model, 'selected_flows'),
                        'header' => false,
                        'headerOptions' => [
                            'width' => '40px',
                        ],
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'label' => 'Приход',
                        'attribute' => 'amount',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'value' => function ($model) {
                            return '<span class="sum" style="display:none;">' . intval($model->availableAmount) / 100 . '</span>' .
                                TextHelper::invoiceMoneyFormat($model->amount, 2);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '60%',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($invoiceArray = $model->getInvoices()->all()) {
                                $linkArray = [];
                                foreach ($invoiceArray as $invoice) {
                                    $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                        '/documents/invoice/view',
                                        'type' => $invoice->type,
                                        'id' => $invoice->id,
                                    ]);
                                }
                                return join(', ', $linkArray);
                            } else {
                                $description = mb_substr($model->description, 0, 50) . '<br>' . mb_substr($model->description, 50, 50);
                                return Html::label(strlen($model->description) > 100 ? $description . '...' : $description, null, ['title' => $model->description]);
                            }
                        },
                    ],
                ],
            ]);
            echo $form->field($model, 'create_new', [
                'wrapperOptions' => [
                    'class' => 'col-sm-8',
                ],
            ])->checkbox();
        } else {
            echo $form->field($model, 'create_new')->hiddenInput(['value' => 1])->label(false);
        }
    }

    if ($flowType != CashFactory::TYPE_BANK && $flowType !== null) {
        echo $form->field($model, 'is_accounting')->checkbox();
    } ?>

    <?php Pjax::end(); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
            'title' => 'Отменить',
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php Pjax::end() ?>


