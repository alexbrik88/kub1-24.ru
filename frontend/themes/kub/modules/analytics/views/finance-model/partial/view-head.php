<?php

use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;

?>
<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-12 column pr-0">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;">
                            <?= htmlspecialchars($model->name); ?>
                        </h4>
                        <div class="column" style="margin-bottom: auto;">
                            <?= Html::button(SpriteIconWidget::widget(['icon' => 'pencil']), [
                                'title' => 'Редактировать',
                                'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                'data-toggle' => "modal",
                                'data-target' => "#add-finance-model"]) ?>
                            <?php /* Html::button(SpriteIconWidget::widget(['icon' => 'diagram']), [
                                'title' => 'График',
                                'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2']) */ ?>
                            <?php /* Html::button(SpriteIconWidget::widget(['icon' => 'book']), [
                                'title' => 'Описание',
                                'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2']) */ ?>
                        </div>
                    </div>
                    <div class="row" style="min-width: 550px;">
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Начало
                            </div>
                            <div style="text-transform: capitalize">
                                <?= Yii::$app->formatter->asDate($model->dateFrom, 'LLLL Y') ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Окончание
                            </div>
                            <div style="text-transform: capitalize">
                                <?= Yii::$app->formatter->asDate($model->dateTill, 'LLLL Y') ?>
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Период
                            </div>
                            <div>
                                <?= $model->period ?> мес.
                            </div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Комментарий
                            </div>
                            <div>
                                <?= htmlspecialchars($model->comment) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>