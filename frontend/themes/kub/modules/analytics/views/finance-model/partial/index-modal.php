<?php

use frontend\modules\analytics\models\financeModel\FinanceModel;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\View;

/** @var $model FinanceModel */

?>
<div id="add-finance-model" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">
                <?= ($model->id ? 'Редактировать' : 'Добавить') . ' финансовую модель' ?>
            </h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?= $this->render('_model_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
</div>

<!-- Confirm delete -->
<?php Modal::begin([
    'id' => 'many-delete',
    'title' => 'Вы уверены, что хотите удалить выбранные модели?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
        'data-url' => Url::to(['many-delete']),
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>
<!-- Confirm update -->
<?php Modal::begin([
    'id' => 'confirm-update-finance-model',
    'title' => 'Внимание! Изменение периода финмодели может привести к безвозвратной потере данных. Вы можете воспользоваться функцией копирования модели. Данные с «[fromDate]» по «[toDate]» будут удалены! Продолжить?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-confirm-update-yes',
        'data-form-id' => '#form-finance-model',
        'data-dismiss' => 'modal',
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1 modal-confirm-update-no',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>
<!-- Confirm copy -->
<?php Modal::begin([
    'id' => 'copy-model',
    'title' => 'Вы уверены, что хотите скопировать финансовую модель?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::a('Да', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-confirm-copy-yes',
            'data-url' => Url::to(['many-delete']),
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS
    $('.modal-confirm-copy-yes').on('click', function() {
        const modelId = $('.table-finance-models').find('.joint-operation-checkbox:checked').first().data('id');
        if (modelId)
            location.href = '/analytics/finance-model/copy' + '?id=' + modelId;
        else
            $('#copy-model').modal('hide');
    });
    $(document).on('change', '.joint-operation-checkbox', function() {
        const count = $('.table-count-list input.joint-operation-checkbox:checked').length;
        $('#btn-copy-model').attr('disabled', (count > 1)).tooltipster('content', (count > 1) ? 'Нужно выбрать только одну финмодель' : 'Копировать');
    });

JS);

if ($model->id) {

    $this->registerJs(<<<JS
    $('#form-finance-model').on('submit', function() {
        const oldDateFrom = new Date(Date.parse($('#old-date-from').val()));
        const oldDateTill = new Date(Date.parse($('#old-date-till').val()));
        const newDateFrom = new Date(Date.parse($('#financemodel-datefrom').val()));
        const newDateTill = new Date(Date.parse($('#financemodel-datetill').val()));
        const confirmModalLabel = $('#confirm-update-finance-model-label');
        const confirmInput = $('#is-update-confirmed');
        const isConfirmed = $(confirmInput).val();
        
        if (isConfirmed)
            return true;
                
        if (newDateFrom > oldDateFrom) {
            $(confirmModalLabel).text($(confirmModalLabel).text().replace('[fromDate]', oldDateFrom.toLocaleString('ru', {year: 'numeric', month: 'long'})));
            $(confirmModalLabel).text($(confirmModalLabel).text().replace('[toDate]', newDateFrom.toLocaleString('ru', {year: 'numeric', month: 'long'})));
            $('#confirm-update-finance-model').modal('show');
            return false;
        }
        
        if (newDateTill < oldDateTill) {
            $(confirmModalLabel).text($(confirmModalLabel).text().replace('[fromDate]', newDateTill.toLocaleString('ru', {year: 'numeric', month: 'long'})));
            $(confirmModalLabel).text($(confirmModalLabel).text().replace('[toDate]', oldDateTill.toLocaleString('ru', {year: 'numeric', month: 'long'})));
            $('#confirm-update-finance-model').modal('show');
            return false;
        }
        
        return true;
    });
    $('.modal-confirm-update-yes').on('click', function() {
        const formID = $(this).data('form-id');
        const confirmInput = $('#is-update-confirmed');
        $(confirmInput).val(1);
        $(formID).submit();
    });
    $('.modal-confirm-update-no').on('click', function() {
        Ladda.stopAll();
    });
JS, View::POS_READY);

}