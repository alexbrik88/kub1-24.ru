<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;

/** @var $quarters array */
/** @var $_cellIds array */
/** @var $_dMonthes array */
/** @var $data array */
/** @var $avg float */
/** @var $title string */
/** @var $trClass string */
/** @var $level int */
/** @var $year int */

$trData  = '';
$trData .= (isset($dataPaymentType)) ? ('data-payment_type="'.$dataPaymentType.'"') : '';
$trData .= (isset($dataFlowType)) ? ('data-flow_type="'.$dataFlowType.'"') : '';
$trData .= (isset($dataItemId)) ? ('data-item_id="'.$dataItemId.'"') : '';
$trData .= (isset($dataId)) ? ('data-id="'.$dataId.'"') : '';
$trData .= (isset($dataType)) ? ('data-type_id="'.$dataType.'"') : '';
$trData .= (isset($dataAttr)) ? ('data-attr="'.$dataAttr.'"') : '';
$trData .= (isset($dataPercent)) ? (' data-percent="'.$dataPercent.'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

$isSortable = $isSortable ?? false;
$isButtonable = $isButtonable ?? false;
$isBold = $isBold ?? false;
$isActiveRow = $isActiveRow ?? false;
$isEmptyRow = $isEmptyRow ?? false;
$isInt = $isInt ?? false;
$editUrl = $editUrl ?? null;
$editAttrJS = $editAttrJS ?? false;
$isTotal = $isTotal ?? false;
$negativeRed = true;
$positiveRed = false;
$units = $units ?? null;
$isAvgRow = $isAvgRow ?? false;
?>
<tr class="<?=($trClass)?> <?=($level > 1 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="<?=($isActiveRow || $isSortable ? 'checkbox-td':'')?> <?=($isBold ? 'tooltip-td':'')?>">
        <?php if ($isButtonable): ?>
            <div class="d-flex pl-1">
            <button class="table-collapse-btn button-clr d-block text-left <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="<?=($isBold ? 'weight-700':'text-grey')?> text_size_14"><?= $title; ?></span>
            </button>
            <?php if ($editUrl): ?>
                <a href="<?=($editUrl)?>" class="link d-block ml-auto" title="Редактировать">
                    <svg class="svg-icon text_size-14">
                        <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                    </svg>
                </a>
            <?php endif; ?>
            </div>
        <?php else: ?>
            <div class="d-flex pl-1">
                <div class="text_size_14 mr-2 nowrap <?=(!$isTotal) ? 'ml-sub-row ':'' ?><?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
                <?php if ($editAttrJS): ?>
                    <div class="edit-costs-js link d-block ml-auto" title="Редактировать">
                        <svg class="svg-icon text_size-14">
                            <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                        </svg>
                    </div>
                <?php endif; ?>
                <?php if ($editUrl): ?>
                    <a href="<?=($editUrl)?>" class="link d-block ml-auto" title="Редактировать">
                        <svg class="svg-icon text_size-14">
                            <use xlink:href="/img/svg/svgSprite.svg#pencil"></use>
                        </svg>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </td>

    <?php if (isset($avg)): ?>
    <td class="nowrap avg">
        <div class="pl-1 pr-1 <?=(($negativeRed && $avg < 0 || $positiveRed && $avg > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
            <?php if (!$isEmptyRow) {
                echo TextHelper::moneyFormat($avg, $isInt ? 0 : 2);
                echo $units;
            } ?>
        </div>
    </td>
    <?php endif; ?>

    <?php if (isset($percent)): ?>
        <td class="nowrap avg">
            <div class="pl-1 pr-1 <?=(($negativeRed && $percent < 0 || $positiveRed && $percent > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                <?php if (!$isEmptyRow && !$isTotal) {
                    echo TextHelper::moneyFormat($percent, $isInt ? 0 : 2);
                    echo '%';
                } ?>
            </div>
        </td>
    <?php endif; ?>

    <?php $quarterSum = $totalSum = 0; ?>
    <?php $quarterAvg = $totalAvg = ['amount' => 0, 'count' => 0] ?>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <?php
            $amount = ArrayHelper::getValue($data, $month, 0);
            $quarterSum += $amount;
            $totalSum += $amount;
            $quarterAvg['amount'] += $amount; $quarterAvg['count'] += 1;
            $totalAvg['amount'] += $amount; $totalAvg['count'] += 1;
            ?>
            <td class="nowrap <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                <div class="pl-1 pr-1 <?=(($negativeRed && $amount < 0 || $positiveRed && $amount > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?php if (!$isEmptyRow) {
                        echo TextHelper::moneyFormat($amount, $isInt ? 0 : 2);
                        echo $units;
                    }
                    ?>
                </div>
            </td>
        <?php endforeach; ?>
        <td class="quarter-block nowrap <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-collapse-cell-total>
            <div class="pl-1 pr-1 <?=(($negativeRed && $quarterSum < 0 || $positiveRed && $quarterSum > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                <?php if (!$isEmptyRow) {
                    echo TextHelper::moneyFormat(($isAvgRow) ? $quarterAvg['amount'] / $quarterAvg['count'] : $quarterSum, $isInt ? 0 : 2);
                    echo $units;
                }
                $quarterSum = 0;
                $quarterAvg = ['amount' => 0, 'count' => 0]; ?>
            </div>
        </td>
    <?php endforeach; ?>
    <td class="nowrap total-block <?=($isBold ? 'weight-700':'')?>">
        <div class="pl-1 pr-1 <?=(($negativeRed && $totalSum < 0 || $positiveRed && $totalSum > 0) ? 'red-link' : 'text-dark-alternative')?>">
            <?php if (!$isEmptyRow) {
                if ($units == '%') {
                    if (isset($avg)) echo TextHelper::moneyFormat($avg, $isInt ? 0 : 2);
                } else {
                    echo TextHelper::moneyFormat(($isAvgRow) ? $totalAvg['amount'] / $totalAvg['count'] : $totalSum, $isInt ? 0 : 2);
                }
                echo $units;
            } ?>
        </div>
    </td>
</tr>