<?php
use common\components\grid\GridView;
use yii\bootstrap4\Html;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-finance-models' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', [
        'totalCount' => $dataProvider->totalCount,
        'scroll' => true,
    ]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (FinanceModel $model) {
                return Html::checkbox('FinanceModel[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data-id' => $model->id
                ]);
            },
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Дата',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (FinanceModel $model) {
                return date('d.m.Y', strtotime($model->created_at));
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'format' => 'raw',
            'value' => function (FinanceModel $model) {
                return Html::a($model->name, ['view', 'id' => $model->id]);
            },
        ],
        [
            'attribute' => 'periodFrom',
            'label' => 'Начало',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (FinanceModel $model) {

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($model->month_from, 2, "0", STR_PAD_LEFT)).' '.$model->year_from;
            },
        ],
        [
            'attribute' => 'periodTill',
            'label' => 'Окончание',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (FinanceModel $model) {

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($model->month_till, 2, "0", STR_PAD_LEFT)).' '.$model->year_till;
            },
        ],
        [
            'attribute' => 'total_revenue',
            'label' => 'Выручка',
            'headerOptions' => [
                'class' => $tabConfigClass['revenue'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['revenue'],
            ],
            'value' => function (FinanceModel $model) {

                return TextHelper::invoiceMoneyFormat($model->total_revenue, 2);
            },
        ],
        [
            'attribute' => 'total_net_profit',
            'label' => 'Чистая прибыль',
            'headerOptions' => [
                'class' => $tabConfigClass['net_profit'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['net_profit'],
            ],
            'value' => function (FinanceModel $model) {

                return TextHelper::invoiceMoneyFormat($model->total_net_profit, 2);
            },
        ],
        [
            'attribute' => 'total_marginality',
            'label' => 'Маржинальность, %',
            'headerOptions' => [
                'class' => $tabConfigClass['margin'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['margin'],
            ],
            'value' => function (FinanceModel $model) {

                return TextHelper::numberFormat($model->total_marginality, 2) . '%';
            },
        ],
        [
            'attribute' => 'total_sales_profit',
            'label' => 'Рентабельность продаж, %',
            'headerOptions' => [
                'class' => $tabConfigClass['sales_profit'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['sales_profit'],
            ],
            'value' => function (FinanceModel $model) {

                return TextHelper::numberFormat($model->total_sales_profit, 2) . '%';
            },
        ],
        [
            'attribute' => 'total_capital_profit',
            'label' => 'Рентабельность капитала, %',
            'headerOptions' => [
                'class' => $tabConfigClass['capital_profit'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['capital_profit'],
            ],
            'value' => function (FinanceModel $model) {

                return TextHelper::numberFormat($model->total_capital_profit, 2) . '%';
            },
        ],
        [
            'attribute' => 'comment',
            'label' => 'Комментарий',
            'headerOptions' => [
                'class' => 'dropdown-filter ' . $tabConfigClass['comment'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['comment'],
            ],
            'value' => function (FinanceModel $model) {

                return mb_substr($model->comment, 0, 200);
            },
        ],
        [
            'attribute' => 'employee_id',
            'label' => 'Ответственный',
            'headerOptions' => [
                'class' => 'dropdown-filter ' . $tabConfigClass['employee'],
                'width' => '15%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['employee'],
                'style' => 'white-space: initial;',
            ],
            'format' => 'raw',
            'filter' => $searchModel->getEmployeeFilter(),
            's2width' => '300px',
            'value' => function (FinanceModel $model) {
                return $model->employee->getFio(true);
            },
        ],
    ],
]); ?>