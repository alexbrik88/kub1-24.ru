<?php

use frontend\modules\analytics\models\financeModel\FinanceModel;
use yii\bootstrap\Nav;
use yii\helpers\Url;
?>

<div class="nav-finance">

<?= Nav::widget([
    'id' => 'finance-model-menu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-3'],
    'items' => [
        [
            'label' => 'ОПиУ',
            'url' => Url::current(['tab' => FinanceModel::TAB_PROFIT_AND_LOSS]),
            'active' => !$tab || $tab == FinanceModel::TAB_PROFIT_AND_LOSS,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'ОДДС',
            'url' => Url::current(['tab' => FinanceModel::TAB_ODDS]),
            'active' => $tab == FinanceModel::TAB_ODDS,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'Основные средства',
            'url' => Url::current(['tab' => FinanceModel::TAB_ASSETS]),
            'active' => $tab == FinanceModel::TAB_ASSETS,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'visible' => YII_ENV_DEV
        ],
        [
            'label' => 'Финансовая деятельность',
            'url' => Url::current(['tab' => FinanceModel::TAB_ACTIVITIES]),
            'active' => $tab == FinanceModel::TAB_ACTIVITIES,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'visible' => YII_ENV_DEV
        ],
        [
            'label' => 'Оборотный капитал',
            'url' => Url::current(['tab' => FinanceModel::TAB_CAPITAL]),
            'active' => $tab == FinanceModel::TAB_CAPITAL,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'visible' => YII_ENV_DEV
        ],
        [
            'label' => 'Баланс',
            'url' => Url::current(['tab' => FinanceModel::TAB_BALANCE]),
            'active' => $tab == FinanceModel::TAB_BALANCE,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'visible' => YII_ENV_DEV
        ],
    ],
]);
?>

</div>
