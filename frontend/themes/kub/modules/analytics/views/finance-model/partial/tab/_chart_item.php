<?php
use frontend\components\Icon;
?>

<div class="d-flex">
    <?= Icon::get($icon, ['class' => 'item-icon']) ?>
    <div class="ml-4">
        <div style="margin-bottom: 3px;">
            <?= $label ?>
        </div>
        <div class="d-flex" style="font-size: 18px;">
            <div class="font-weight-bold">
                <?= $value ?>
            </div>
        </div>
    </div>
</div>