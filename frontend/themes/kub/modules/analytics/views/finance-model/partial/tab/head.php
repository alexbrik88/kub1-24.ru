<?php
use yii\bootstrap4\Html;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\models\financeModel\FinanceModel;

/** @var string $tab */
/** @var FinanceModel $model */

$title = FinanceModel::getTabTitle($tab);
$showHelpPanel = $userConfig->finance_model_tab_help ?? false;
$showChartPanel = $userConfig->finance_model_tab_chart ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $title ?></h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <?php if (!$tab || $tab == FinanceModel::TAB_PROFIT_AND_LOSS): ?>
                <div class="column pl-1 pr-0">
                    <?= Html::a('<svg class="svg-icon"> <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use> </svg><span>Точка продаж</span>',
                        ['create-shop', 'model_id' => $model->id], [
                        'class' => 'button-regular button-regular_red mb-2',
                        'title' => 'Добавить точку продаж'
                    ]); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse">Описание отчёта</div>
        <div id="tooltip_chart_collapse">График</div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="finance_model_tab_chart">
    <div class="pt-4 pb-3">
        <?= $this->render('chart', ['model' => $model]) ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="finance_model_tab_help">
    <div class="pt-4 pb-3">

    </div>
</div>