<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\ActiveForm;

$from = $model->dateFrom ? date_create_from_format('Y-m-d', $model->dateFrom) : (new DateTime());
$till = $model->dateTill ? date_create_from_format('Y-m-d', $model->dateTill) : (new DateTime())->modify('+11 month');

$form = ActiveForm::begin([
    'id' => 'form-finance-model',
    'action' => $model->id ? Url::to(['update', 'id' => $model->id]) : Url::to(['create']),
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
    <?php if ($model->id): ?>
        <?= Html::hiddenInput('is-update-confirmed', null, ['id' => 'is-update-confirmed']); ?>
        <?= Html::hiddenInput('old-date-from', $from->format('Y-m-01'), ['id' => 'old-date-from']); ?>
        <?= Html::hiddenInput('old-date-till', $till->format('Y-m-01'), ['id' => 'old-date-till']); ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'name'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'textFrom', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                            'value' => Yii::$app->formatter->asDate($from->format('Y-m-01'), 'LLLL Y'),
                            'class' => 'form-control field-datepicker field-from-datepicker ico',
                            'style' => 'text-transform: capitalize;',
                            'autocomplete' => 'off',
                            'readonly' => true
                        ])->label('Месяц начала'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'dateFrom')
                            ->hiddenInput(['value' => $from->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'textTill', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                            'value' => Yii::$app->formatter->asDate($till->format('Y-m-01'), 'LLLL Y'),
                            'class' => 'form-control field-datepicker field-to-datepicker ico',
                            'style' => 'text-transform: capitalize;',
                            'autocomplete' => 'off',
                            'readonly' => true
                        ])->label('Месяц окончания'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'dateTill')
                            ->hiddenInput(['value' => $till->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'template_id')->textInput(['disabled' => true, 'placeholder' => 'Скоро']); ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'comment')->textarea([
                'placeholder' => '',
                'rows' => '3']); ?>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?php

$jsFrom = $from->format('Y').','.($from->format('m')-1).',1';
$jsTill = $till->format('Y').','.($till->format('m')-1).',1';

$this->registerJs(<<<JS

    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear();
      var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1);
      var dd = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
      return "".concat(yyyy).concat('-').concat(mm).concat('-').concat(dd);
    };

    const DatePickerFrom = $('.field-from-datepicker');
    const DatePickerTo = $('.field-to-datepicker');
    let datepickerConfig = kubDatepickerConfig;
    let datepickerConfigFrom;
    let datepickerConfigTo;
    
    datepickerConfig.view = 'months';
    datepickerConfig.minView = 'months';
    datepickerConfig.dateFormat = 'MM yyyy';
    datepickerConfig.language = 'ru';
    datepickerConfigFrom = Object.assign({}, datepickerConfig);    
    datepickerConfigFrom.onSelect = function(formattedDate, date) {
        $('#financemodel-datefrom').val(date.yyyymmdd());
    };
    datepickerConfigTo = Object.assign({}, datepickerConfig);
    datepickerConfigTo.onSelect = function(formattedDate, date) {
        $('#financemodel-datetill').val(date.yyyymmdd());
    };
    
    DatePickerFrom.datepicker(datepickerConfigFrom);
    DatePickerTo.datepicker(datepickerConfigTo);

    DatePickerFrom.data('datepicker').selectDate(new Date({$jsFrom}));
    DatePickerTo.data('datepicker').selectDate(new Date({$jsTill}));
    DatePickerFrom.data('datepicker').date = (new Date({$jsFrom}));
    DatePickerTo.data('datepicker').date = (new Date({$jsTill}));
    
JS, View::POS_READY);
?>