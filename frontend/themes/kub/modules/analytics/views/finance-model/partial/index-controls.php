<?php

use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Html;

?>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'finance_model_revenue'],
                ['attribute' => 'finance_model_net_profit'],
                ['attribute' => 'finance_model_margin'],
                ['attribute' => 'finance_model_sales_profit'],
                ['attribute' => 'finance_model_capital_profit'],
                ['attribute' => 'finance_model_comment'],
                ['attribute' => 'finance_model_employee'],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_finance_model']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'name', [
                'type' => 'search',
                'placeholder' => 'Поиск по названию...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
