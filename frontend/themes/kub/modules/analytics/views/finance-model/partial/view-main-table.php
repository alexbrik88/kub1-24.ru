<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;

/* @var $tab string
 * @var $model FinanceModel
 * @var $userConfig Config
 * @var $employee Employee
 * @var $revenue array
 * @var $costs array
 */

$quarters = $model->getQuarters();
$floorMap = Yii::$app->request->post('floorMap', []);

$_cellIds = $_dMonthes = [];
$q = 0;

$flatQuarters = array_keys($quarters);
$firstQuarter = $flatQuarters[0];
$lastQuarter = $flatQuarters[count($flatQuarters) - 1];
$currMonth = date('Ym');
foreach ($quarters as $quarter => $monthes) {

    $isQuarterOpened =
        ((in_array($currMonth, $monthes))
        || ($quarter == $firstQuarter && $currMonth <= $monthes[0])
        || ($quarter == $lastQuarter && $currMonth >= $monthes[count($monthes) - 1])) ? 1 : 0;

    $q++;
    $_cellIds[$quarter] = 'cell-' . $q;
    $_dMonthes[$quarter] = ArrayHelper::getValue($floorMap, 'cell-' . $q, $isQuarterOpened);
}

?>

<div id="cs-table-11x" class="custom-scroll-table-double cs-top">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="cs-table-2x">
    <div class="table-wrap">
        <table class="table table-style table-count-list">
            <!--CSTable.setStickyCell-->
        </table>
    </div>
    <div class="fixed-first-cell">
        <!--CSTable.setStickyHeader-->
    </div>
</div>

<div class="wrap wrap_padding_none" style="position: relative">
<div id="cs-table-1x" class="custom-scroll-table-double">
    <div class="table-wrap">
    <table class="flow-of-funds odds-table by_purse table table-style table-count-list mb-0 table-compact table-bleak">
        <thead>
            <?= $this->render('table_plus/_head_sticky', [
                'model' => $model,
                'quarters' => $quarters,
                '_dMonthes' => $_dMonthes,
                '_cellIds' => $_cellIds,
            ]) ?>
        </thead>

        <tbody>
        <?php

        /////////////////
        //   REVENUE   //
        /////////////////

        // LEVEL 1
        foreach ($revenue as $shopId => $level1) {

            $floorKey = "shop-{$shopId}";
            $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, false);

            echo $this->render('table_plus/_row', [
                'level' => 1,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'avg' => $level1['avg'],
                'trClass' => 'level-1',
                'floorKey' => $floorKey,
                'isOpenedFloor' => $isOpenedFloor,
                'quarters' => $quarters,
                '_dMonthes' => $_dMonthes,
                '_cellIds' => $_cellIds,
                'isBold' => true,
                'isButtonable' => $level1['isButtonable'] ?? null,
                'isActiveRow' => true,
                'isEmptyRow' => empty($level1['data']),
                'editUrl' => $level1['editUrl'] ?? null,
                'isTotal' => 1,
                'units' => $level1['units'] ?? null
            ]);

            if (!isset($level1['levels']))
                continue;

            // LEVEL 2
            foreach ($level1['levels'] as $attr => $level2) {

                echo $this->render('table_plus/_row', [
                    'level' => 2,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'avg' => $level2['avg'],
                    'trClass' => 'level-2',
                    'floorKey' => $floorKey,
                    'dataId' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    //'isBold' => (isset($level1['totalBlock'])),
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'isInt' => (in_array($attr, FinanceModelRevenue::INTEGER_ATTRIBUTES)),
                    'units' => $level2['units'] ?? null,
                    'isAvgRow' => ($attr == 'average_check' || 'conversion' == $attr)
                ]);
            }
        }

        echo $this->render('table_plus/_row_empty', [
            'quarters' => $quarters,
            '_dMonthes' => $_dMonthes,
            '_cellIds' => $_cellIds,
            'avg' => '',
        ]);

        ///////////////
        //   COSTS   //
        ///////////////

        // LEVEL 1
        foreach ($costs as $costKey1 => $level1) {
            $floorKey1 = "{$costKey1}";
            $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey1, false);
            echo $this->render('table_plus/_row', [
                'level' => 1,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'avg' => $level1['avg'],
                'trClass' => 'level-1',
                'dataAttr' => $level1['editAttrJS'] ?? false,
                'floorKey' => $floorKey1,
                'isOpenedFloor' => $isOpenedFloor,
                'quarters' => $quarters,
                '_dMonthes' => $_dMonthes,
                '_cellIds' => $_cellIds,
                'isBold' => true,
                'isButtonable' => $level1['isButtonable'] ?? false,
                'isActiveRow' => true,
                'isEmptyRow' => empty($level1['data']),
                'editUrl' => $level1['editUrl'] ?? null,
                'editAttrJS' => $level1['editAttrJS'] ?? false,
                'isTotal' => isset($level1['totalBlock']),
                'units' => $level1['units'] ?? null,
                'isAvgRow' => ($costKey1 == 'marginality')
            ]);

            if (!isset($level1['levels']))
                continue;

            // LEVEL 2
            foreach ($level1['levels'] as $costKey2 => $level2) {

                echo $this->render('table_plus/_row', [
                    'level' => 2,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'avg' => $level2['avg'],
                    'trClass' => 'level-2',
                    'isOpenedFloor' => $isOpenedFloor,
                    'dataId' => $floorKey1,
                    'editUrl' => $level2['editUrl'] ?? null,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'units' => $level2['units'] ?? null,
                    'isAvgRow' => ($costKey1 == 'marginality')
                ]);
            }
        }

        ?>
        </tbody>
    </table>
    </div>
</div>

<!-- update item-->
<div id="update-finance-model-item-panel" class="wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['update-other-cost-item', 'model_id' => $model->id]),
            'id' => 'update-other-cost-item',
        ]); ?>
        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <div style="display: flex">
            <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center;">
                    Средний показатель
                </span>
                <?= Html::textInput("_amount[avg]", null, [
                    'class' => 'form-control amount-avg-input',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off'
                ]); ?>
            </div>
            <div>
                <div class="row mb-2">
                    <?php foreach ($model->monthes as $ym): ?>
                        <?php
                        $year = substr($ym, 0, 4);
                        $month = substr($ym, 4, 2); ?>
                        <div class="column p-1" style="width: 5%;  min-width: 150px">
                            <span class="label" style="text-align: center;">
                                <?= ArrayHelper::getValue(AbstractFinance::$month, $month, $month) ?> <?= $year ?>
                            </span>
                            <?= Html::textInput("amount[{$ym}]", null, [
                                'class' => 'form-control amount-input',
                                'style' => 'width: 100%;',
                                'data-month' => $ym
                            ]); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6 p-1 text-right">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-1',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="col-6 p-1 ml-auto text-left">
                <?= Html::a('Отменить', 'javascript:;', [
                    'class' => 'cancel-edit-costs-js button-regular button-width button-hover-transparent button-clr',
                ]); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>

<?php
$this->registerJS('
    
    FinanceModelEditCostsOnFly.init();
    FinanceModelCSTable.init();    
    
', \yii\web\View::POS_READY);
