<?php
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\AbstractFinance;
use yii\helpers\ArrayHelper;

/** @var $model FinanceModel */
/** @var $monthes array */
/** @var $_dMonthes array */
/** @var $_cellIds array */
/** @var $typeCosts int */
?>
<tr class="quarters-flow-of-funds">
    <th class="align-top" rowspan="2">
        <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1">Параметры</span>
        </button>
    </th>
    <th class="align-top" rowspan="2">
        <?= (isset($avgTitle)) ? $avgTitle : 'Средний показатель' ?>
    </th>
    <?php foreach ($quarters as $quarter => $monthes): ?>
    <th class="align-top" <?= $_dMonthes[$quarter] ? 'colspan="'.count($monthes).'"' : '' ?> data-collapse-cell-title data-id="<?= $_cellIds[$quarter] ?>" data-quarter="<?=($quarter)?>">
        <button class="table-collapse-btn button-clr ml-1 <?= $_dMonthes[$quarter] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="<?= $_cellIds[$quarter] ?>" data-columns-count="<?=(count($monthes))?>">
            <span class="table-collapse-icon">&nbsp;</span>
            <span class="text-grey weight-700 ml-1 nowrap"><?= substr($quarter, 4, 2) ?> кв <?= substr($quarter, 0, 4); ?></span>
        </button>
    </th>
    <?php endforeach; ?>
    <th class="align-top" rowspan="2">
        <div class="pl-1 pr-1">
            Итого за период
        </div>
    </th>
</tr>
<tr>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <th class="<?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $_cellIds[$quarter] ?>" data-month="<?= $month; ?>">
                <div class="pl-1 pr-1">
                    <?= ArrayHelper::getValue(AbstractFinance::$month, substr($month, 4, 2), $month); ?>
                </div>
            </th>
        <?php endforeach; ?>
        <th class="<?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $_cellIds[$quarter] ?>">
            <div class="pl-1 pr-1">Итого</div>
        </th>
    <?php endforeach; ?>
</tr>

