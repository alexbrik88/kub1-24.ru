<?php

use yii\helpers\Html;
use \frontend\modules\analytics\models\financeModel\FinanceModel;
use common\components\TextHelper;

/** @var $model FinanceModel */
?>

<div class="wrap my-0 finance-model-modeling-result">
    <div class="d-flex">
        <div class="col p-3 border-right">
            <?= $this->render('_chart_item', [
                'icon' => 'proceeds',
                'label' => 'Выручка',
                'value' => TextHelper::invoiceMoneyFormat($model->total_revenue, 2)
            ]) ?>
        </div>
        <div class="col p-3 border-right">
            <?= $this->render('_chart_item', [
                'icon' => 'rentability',
                'label' => 'Маржинальный доход',
                'value' => TextHelper::invoiceMoneyFormat($model->total_margin, 2),
            ]) ?>
        </div>
        <div class="col p-3">
            <?= $this->render('_chart_item', [
                'icon' => 'profit_ebitda',
                'label' => 'Чистая прибыль',
                'value' => TextHelper::invoiceMoneyFormat($model->total_net_profit, 2),
            ]) ?>
        </div>
    </div>
    <div class="d-flex border-top">
        <div class="col p-3 border-right">
            <?= $this->render('_chart_item', [
                'icon' => 'average_check',
                'label' => 'Средний чек',
                'value' => TextHelper::invoiceMoneyFormat($model->total_average_check, 2),
            ]) ?>
        </div>
        <div class="col p-3 border-right">
            <?= $this->render('_chart_item', [
                'icon' => 'marginality',
                'label' => 'Маржинальность',
                'value' => TextHelper::numberFormat($model->total_marginality, 2) . '%',
            ]) ?>
        </div>
        <div class="col p-3">
            <?= $this->render('_chart_item', [
                'icon' => 'rentability_costs',
                'label' => 'Рентабельность продаж (ROS)',
                'value' => TextHelper::numberFormat($model->total_capital_profit, 2) . '%',
            ]) ?>
        </div>
    </div>
</div>
