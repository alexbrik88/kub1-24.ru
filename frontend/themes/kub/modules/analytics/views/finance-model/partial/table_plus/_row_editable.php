<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;

/** @var $attr string */
/** @var $quarters array */
/** @var $_cellIds array */
/** @var $_dMonthes array */
/** @var $data array */
/** @var $avg float */
/** @var $title string */
/** @var $trClass string */
/** @var $level int */

$trData  = '';
$isInt = $isInt ?? false;
$isBold = $isBold ?? false;
$calcJS = $calcJS ?? false;
$trClass = $trClass ?? false;
$isAvgRow = $isAvgRow ?? false;
$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';
$trData .= (isset($attr)) ? (' data-attr="'.$attr.'"') : '';
$trData .= (isset($dataId)) ? (' data-id="'.$dataId.'"') : '';
$trData .= (isset($dataPercent)) ? (' data-percent="'.$dataPercent.'"') : '';
?>
<tr class="<?=($trClass)?> <?=($level > 1 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="text-grey text_size_14 <?=($isBold ? 'weight-700':'')?>">
        <?= $title ?>
    </td>

    <?php if (isset($avg)): ?>
    <td class="nowrap avg text-right <?=($isBold ? 'weight-700':'text-dark-alternative')?>">
        <?= TextHelper::moneyFormat($avg, $isInt ? 0 : 2) ?>
    </td>
    <?php endif; ?>

    <?php $quarterSum = $totalSum = 0; ?>
    <?php $quarterAvg = $totalAvg = ['amount' => 0, 'count' => 0] ?>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <?php
            $amount = ArrayHelper::getValue($data, $month, 0);
            $quarterSum += $amount;
            $totalSum += $amount;
            $quarterAvg['amount'] += $amount; $quarterAvg['count'] += 1;
            $totalAvg['amount'] += $amount; $totalAvg['count'] += 1;
            ?>

            <?php if ($calcJS): ?>
                <td class="month-block nowrap text-right <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>  <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                    <?= TextHelper::moneyFormat($amount, $isInt ? 0 : 2) ?>
                </td>
            <?php else: ?>
                <td class="td-custom-value month-block nowrap text-right <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>  <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                    <input type="text" name="<?=("{$attr}[{$month}]")?>" class="custom-value form-control <?=($isInt ? 'int':'')?>"
                       value="<?= TextHelper::moneyFormat($amount, $isInt ? 0 : 2) ?>">
                </td>
            <?php endif; ?>
        <?php endforeach; ?>
        <td class="quarter-block text-right nowrap <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-collapse-cell-total>
            <?= TextHelper::moneyFormat(($isAvgRow) ? $quarterAvg['amount'] / $quarterAvg['count'] : $quarterSum, $isInt ? 0 : 2) ?>
        </td>
    <?php endforeach; ?>
    <td class="nowrap text-right total-block <?=($isBold ? 'weight-700':'text-dark-alternative')?>">
        <?= TextHelper::moneyFormat(($isAvgRow) ? $totalAvg['amount'] / $totalAvg['count'] : $totalSum, $isInt ? 0 : 2) ?>
    </td>
</tr>