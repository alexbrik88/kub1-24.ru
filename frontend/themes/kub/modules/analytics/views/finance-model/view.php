<?php

namespace frontend\modules\crm\views;

use frontend\modules\analytics\models\financeModel\helpers\FinanceModelTableHelper;
use yii\bootstrap4\Html;
use common\models\employee\Employee;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;

/** @var Employee $employee */
/** @var FinanceModel $model */
/** @var string $tab */

$this->title = 'Финмодель ' . $model->name;
$userConfig = $employee->config;
$renderParams = [
    'tab' => $tab,
    'model' => $model,
    'employee' => $employee,
    'userConfig' => $userConfig,
];

echo Html::a('Назад к списку', ['index'], ['class' => 'link mb-2']);

echo $this->render('partial/view-head', $renderParams);

echo $this->render('partial/view-nav', $renderParams);

echo $this->render('partial/tab/head', $renderParams);

switch ($tab) {
    case FinanceModel::TAB_ODDS:
    case FinanceModel::TAB_ASSETS:
    case FinanceModel::TAB_ACTIVITIES:
    case FinanceModel::TAB_CAPITAL:
    case FinanceModel::TAB_BALANCE:
        echo $this->render('partial/tab/in_developing');
        break;
    case FinanceModel::TAB_PROFIT_AND_LOSS:
    default:
        if ($model->shops) {
            echo $this->render('partial/view-main-table', array_merge(
                $renderParams, [
                    'revenue' => FinanceModelTableHelper::getRevenue($model),
                    'costs' => FinanceModelTableHelper::getCosts($model)
                ]
            ));
        }
        break;
}

echo $this->render('partial/index-modal', $renderParams);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);