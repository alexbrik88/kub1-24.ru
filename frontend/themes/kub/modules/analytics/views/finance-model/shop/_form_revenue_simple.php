<?php
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
?>
<?php if (!$model->type || $model->type == FinanceModelShop::TYPE_SHOP): ?>
<div class="wrap wrap_padding_none shop-type-<?=(FinanceModelShop::TYPE_SHOP)?>" style="position: relative; <?=($model->type == FinanceModelShop::TYPE_SHOP) ? '' : 'display: none;'?>">
    <div id="sds-top-1" class="table-scroll-double top" style="display: none">
        <div class="table-wrap">&nbsp;</div>
    </div>
    <div id="sds-bottom-1" class="table-scroll-double bottom">
        <div class="table-wrap">
            <table class="table table-style table-count-list table-compact finance-model-shop-month" data-type="1">
                <thead>
                <tr>
                    <th style="min-width: 200px">Параметры</th>
                    <th>Средний показатель</th>
                    <?php
                    foreach ($monthes as $month): ?>
                        <th class="nowrap">
                            <?= ArrayHelper::getValue(AbstractFinance::$month, substr($month, 4, 2), $month) ?>
                            <?= substr($month, 0, 4) ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <!-- check_count -->
                <tr data-attr="check_count">
                    <td class="text-grey text_size_14">Количество чеков, шт</td>
                    <td class="text-right avg">0</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="td-custom-value month-block">
                            <input type="text" name="check_count[<?=($month)?>]" class="custom-value int form-control"
                               value="<?= ArrayHelper::getValue($monthesData, "check_count.{$month}", 0) ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- average_check -->
                <tr data-attr="average_check">
                    <td class="text-grey text_size_14">Средний чек, ₽</td>
                    <td class="text-right avg">0,00</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="td-custom-value month-block">
                            <input type="text" name="average_check[<?=($month)?>]" class="custom-value form-control"
                               value="<?= TextHelper::invoiceMoneyFormat(ArrayHelper::getValue($monthesData, "average_check.{$month}", 0), 2) ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- revenue (js calculated) -->
                <tr data-attr="revenue">
                    <td class="text-grey text_size_14 weight-700">Выручка, ₽</td>
                    <td class="text-right weight-700 avg"></td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="month-block text-right weight-700">

                        </td>
                    <?php endforeach; ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (!$model->type || $model->type == FinanceModelShop::TYPE_INTERNET_SHOP): ?>
<div class="wrap wrap_padding_none shop-type-<?=(FinanceModelShop::TYPE_INTERNET_SHOP)?>" style="position: relative; <?=($model->type == FinanceModelShop::TYPE_INTERNET_SHOP) ? '' : 'display: none;'?>">
    <div id="sds-top-2" class="table-scroll-double top" style="display: none">
        <div class="table-wrap">&nbsp;</div>
    </div>
    <div id="sds-bottom-2" class="table-scroll-double bottom">
        <div class="table-wrap">
            <table class="table table-style table-count-list table-compact finance-model-shop-month" data-type="2">
                <thead>
                <tr>
                    <th style="min-width: 200px">Параметры</th>
                    <th>Средний показатель</th>
                    <?php
                    foreach ($monthes as $month): ?>
                        <th class="nowrap">
                            <?= ArrayHelper::getValue(AbstractFinance::$month, substr($month, 4, 2), $month) ?>
                            <?= substr($month, 0, 4) ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <!-- visit_count -->
                <tr data-attr="visit_count">
                    <td class="text-grey text_size_14">Трафик (заходы на сайт), шт</td>
                    <td class="text-right avg">0</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="td-custom-value month-block">
                            <input type="text" name="visit_count[<?=($month)?>]" class="custom-value int form-control"
                               value="<?= ArrayHelper::getValue($monthesData, "visit_count.{$month}", 0) ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- conversion -->
                <tr data-attr="conversion">
                    <td class="text-grey text_size_14">Конверсия в покупку, %</td>
                    <td class="text-right avg">0,00</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="td-custom-value month-block">
                            <input type="text" name="conversion[<?=($month)?>]" class="custom-value form-control"
                               value="<?= TextHelper::moneyFormat(ArrayHelper::getValue($monthesData, "conversion.{$month}", 0), 2) ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- check_count (js calculated) -->
                <tr data-attr="check_count">
                    <td class="text-grey text_size_14">Количество покупок, шт</td>
                    <td class="text-right avg">0</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="month-block text-right">
                            0
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- average_check -->
                <tr data-attr="average_check">
                    <td class="text-grey text_size_14">Средний чек, ₽</td>
                    <td class="text-right avg">0,00</td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="td-custom-value month-block">
                            <input type="text" name="average_check[<?=($month)?>]" class="custom-value form-control"
                               value="<?= TextHelper::invoiceMoneyFormat(ArrayHelper::getValue($monthesData, "average_check.{$month}", 0), 2) ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- revenue (js calculated) -->
                <tr data-attr="revenue">
                    <td class="text-grey text_size_14 weight-700">Выручка, ₽</td>
                    <td class="text-right weight-700 avg"></td>
                    <?php foreach ($monthes as $month): ?>
                        <td class="month-block text-right weight-700">

                        </td>
                    <?php endforeach; ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif; ?>

<?php $this->registerJs(<<<JS
    FMTable1 = Object.create(FinanceModelCSTableSimple);
    FMTable1.options = {
        selectorTop: '#sds-top-1',
        selectorBottom: '#sds-bottom-1',
        fixedColumnWidth: 200,
        refreshByClick: [],
        refreshByChange: ['#finance-model-shop-type'],
    };
    FMTable2 = Object.create(FinanceModelCSTableSimple);
    FMTable2.options = {
        selectorTop: '#sds-top-2',
        selectorBottom: '#sds-bottom-2',
        fixedColumnWidth: 200,
        refreshByClick: [],
        refreshByChange: ['#finance-model-shop-type'],
    };

    $(document).ready(function() {
        ////////////////
        FMTable1.init();
        FMTable2.init();
        ////////////////
    });
JS)
?>