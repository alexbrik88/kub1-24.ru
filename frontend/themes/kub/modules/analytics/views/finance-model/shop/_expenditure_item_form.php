<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use common\components\helpers\Html;
use common\components\helpers\ArrayHelper;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\modules\reference\models\ArticleDropDownForm;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;

/**
 * @var $financeModel FinanceModel
 * @var $shopModel FinanceModelShop
 * @var $model ArticleDropDownForm
 * @var $flowType int
 * @var $blockType int
 * @var $selectedShops array
 * @var $selectedItems array
 */

Pjax::begin([
    'id' => 'pjax-break-even-article-modal',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false,
]) ?>
    <div class="article-form">
        <?php $form = ActiveForm::begin([
            'id' => 'article-form',
            'enableClientValidation' => false,
            'action' => ['/analytics/finance-model/add-expenditure-item', 'model_id' => $financeModel->id, 'shop_id' => $shopModel->id, 'type' => $blockType],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            'options' => [
                'data-pjax' => true,
            ],
        ]) ?>

        <?php
        echo '<br/>';
        echo Html::label('Название точки продаж, в которую добавить статью', null, ['class' => 'label']);
        echo '<br/>';
        echo Html::checkbox('ArticleDropDownForm[shops][all]', empty($selectedShops) || in_array('all', $selectedShops), [
            'class' => 'shops-checkboxes all',
            'label' => 'Во все точки продаж'
        ]);
        echo '<br/>';
        foreach ($financeModel->shops as $shop) {
            echo Html::checkbox('ArticleDropDownForm[shops]['.$shop->id.']', in_array($shop->id, $selectedShops), [
                'class' => 'shops-checkboxes by-id',
                'label' => htmlspecialchars($shop->name)
            ]);
            echo '<br/>';
        } ?>

        <?= $form->field($model, 'isNew', [
            'options' => [
                'class' => 'form-group',
            ]])
            ->radioList([
                ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN => 'Добавить статью ' . ($flowType == ArticlesSearch::TYPE_INCOME ? 'приходов' : 'расходов') . ' из имеющегося списка',
                ArticleDropDownForm::TYPE_EXISTS_ITEM => 'Добавить статьи ' . ($flowType == ArticlesSearch::TYPE_INCOME ? 'приходов' : 'расходов') . ', которые еще у вас не подключены',
                ArticleDropDownForm::TYPE_NEW_ITEM => 'Добавить свою статью ' . ($flowType == ArticlesSearch::TYPE_INCOME ? 'прихода' : 'расхода'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) use ($form, $model, $selectedItems) {
                    $result = Html::radio($name, $checked, [
                        'class' => 'flow-type-toggle-input',
                        'value' => $value,
                        'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                        'labelOptions' => [
                            'class' => 'label mb-3 mr-3 mt-2',
                        ],
                    ]);

                    ob_start();

                    if ($value === ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN) {
                        $items = ArrayHelper::map($model->getExistsBreakEvenItems(), 'id', 'name');
                        $partLength = ceil(count($items) / 3);
                        ?>
                        <div class="row mt-3 mb-3 field-articledropdownform-items2" style="display: <?=($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN ? 'block;' : 'none;')?>">
                            <?php $i=0; ?>
                            <?php foreach ($items as $itemId => $itemName): ?>
                                <?php if ($i==0): ?>
                                    <div class="col-4">
                                <?php endif; ?>
                                <div class="text-truncate w-100">
                                    <input
                                        id="i<?= $itemId ?>"
                                        type="checkbox"
                                        name="ArticleDropDownForm[items2][]"
                                        value="<?= $itemId ?>"
                                        <?=(in_array($itemId, $selectedItems)) ? 'checked' : '' ?>
                                    >
                                    <label style="display: inline!important; line-height: 1.25" for="i<?= $itemId ?>">
                                        <?= $itemName ?>
                                    </label>
                                </div>
                                <?php $i++; ?>
                                <?php if ($i >= $partLength): ?>
                                    </div>
                                    <?php $i=0; ?>
                                <?php endif; ?>
                            <?php endforeach ?>
                            <?= ($i > 0) ? '</div>' : '' ?>
                        </div>
                    <?php
                    }
                    elseif ($value === ArticleDropDownForm::TYPE_EXISTS_ITEM) {
                        $items = ArrayHelper::map($model->getExistsItems(), 'id', 'name');
                        $partLength = ceil(count($items) / 3);
                        ?>
                        <div class="row mt-3 mb-3 field-articledropdownform-items" style="display: <?=($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_UPDATE_BREAK_EVEN_COLUMN ? 'block;' : 'none;')?>">
                            <?php $i=0; ?>
                            <?php foreach ($items as $itemId => $itemName): ?>
                                <?php if ($i==0): ?>
                                    <div class="col-4">
                                <?php endif; ?>
                                <div class="text-truncate w-100">
                                    <input
                                        id="i<?= $itemId ?>"
                                        type="checkbox"
                                        name="ArticleDropDownForm[items][]"
                                        value="<?= $itemId ?>"
                                        <?=(in_array($itemId, $selectedItems)) ? 'checked' : '' ?>
                                    >
                                    <label style="display: inline!important; line-height: 1.25" for="i<?= $itemId ?>">
                                        <?= $itemName ?>
                                    </label>
                                </div>
                                <?php $i++; ?>
                                <?php if ($i >= $partLength): ?>
                                    </div>
                                    <?php $i=0; ?>
                                <?php endif; ?>
                            <?php endforeach ?>
                            <?= ($i > 0) ? '</div>' : '' ?>
                        </div>
                        <?php
                        //$result .= $form->field($model, 'items', [
                        //    'options' => [
                        //        'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' && $model->isNew == ArticleDropDownForm::TYPE_EXISTS_ITEM ? 'block;' : 'none;'),
                        //    ],
                        //])->checkboxList(ArrayHelper::map($model->getExistsItems(), 'id', 'name'))->label(false);



                    }
                    else {
                        $result .= $form->field($model, 'name', [
                            'options' => [
                                'style' => 'display: ' . ($model->isNew !== null && $model->isNew !== '' &&  $model->isNew == ArticleDropDownForm::TYPE_NEW_ITEM ? 'block;' : 'none;'),
                            ],
                            'inputOptions' => [
                                'placeholder' => 'Введите название статьи',
                            ],
                        ])->label(false);
                    }

                    $result .= ob_get_contents();
                    ob_end_clean();

                    return $result;
                },
            ])->label(false); ?>

        <span>
            Всю информацию об используемых и неиспользуемых статьях смотрите в
            <?= Html::a('справочнике статей', Url::to(['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY])); ?>
        </span>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                    data-dismiss="modal">Отменить
            </button>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php Pjax::end() ?>