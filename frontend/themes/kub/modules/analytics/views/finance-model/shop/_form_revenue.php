<?php

use yii\bootstrap4\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\helpers\FinanceModelShopTableHelper as TableHelper;

/** @var $financeModel FinanceModel */
/** @var $model FinanceModelShop */
/** @var $monthes array */

$monthes = $financeModel->getMonthes();
$monthesData = $model->getRevenueMonthsData();

$otherShops = FinanceModelShop::find()
    ->where(['model_id' => $model->model_id])
    ->andWhere(['<>', 'id', (int)$model->id])
    ->select(['id', 'model_id', 'type', 'name'])
    ->asArray()
    ->orderBy(['id' => SORT_DESC])
    ->limit(10)
    ->all();

$fillByShops = [
    FinanceModelShop::TYPE_SHOP => [],
    FinanceModelShop::TYPE_INTERNET_SHOP => []
];
foreach ($otherShops as $shop) {
    $fillByShops[$shop['type']][] = [
        'label' => $shop['name'],
        'url' => 'javascript:void(0)',
        'linkOptions' => [
            'class' => 'button-fill-by dropdown-item',
            'data-model-id' => $shop['model_id'],
            'data-shop-id' => $shop['id']
        ]
    ];
}

?>

<?php $form = ActiveForm::begin([
    'id' => 'finance-model-shop-form',
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>

<div class="hidden">
    <input id="shop_id" type="hidden" name="_shop_id" value="<?=($model->id)?>">
    <?php foreach ($monthes as $month): ?>
        <input type="hidden" name="monthes[]" value="<?=($month)?>">
    <?php endforeach; ?>
</div>

<div class="wrap">

    <div class="form-title mb-3">
        <?= $this->title ?>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'type')->widget(Select2::class, [
                'model' => $model,
                'disabled' => (bool)$model->id,
                'data' => [
                    FinanceModelShop::TYPE_SHOP => 'Оффлайн Магазин',
                    FinanceModelShop::TYPE_INTERNET_SHOP => 'Интернет Магазин'
                ],
                'hideSearch' => true,
                'options' => [
                    'id' => 'finance-model-shop-type',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => 'Выберите тип точки продаж'
                ],
            ]) ?>
        </div>
    </div>
</div>

<?php if (!$model->id): ?>
    <!-- simple revenue table -->
    <?= $this->render('_form_revenue_simple', [
        'model' => $model,
        'financeModel' => $financeModel,
        'monthes' => $monthes,
        'monthesData' => $monthesData
    ]); ?>

<?php else: ?>
    <!-- full finance model table -->
    <?= $this->render('_form_revenue_table', [
        'model' => $model,
        'financeModel' => $financeModel,
        'revenue' => TableHelper::getShopRevenue($financeModel, $model),
        'costs' => TableHelper::getShopCosts($financeModel, $model)
    ]); ?>

<?php endif; ?>

<?php ActiveForm::end(); ?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-start">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'form' => 'finance-model-shop-form',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <?php if ($model->id): ?>
            <div class="column">
                <?= Html::a('Копировать', ['create-shop', 'model_id' => $financeModel->id, 'shop_id' => $model->id], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>
        <?php endif; ?>
        <!-- заполнить по -->
        <div class="column dropdown-fill-by" data-type="1" style="display: <?=(!$model->id || $model->type == 2 ? 'none' : '')?>">
            <div class="dropup">
                <?= \yii\helpers\Html::button('Заполнить по...', [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                    'data-toggle' => 'dropdown',
                    'disabled' => !$fillByShops[FinanceModelShop::TYPE_SHOP]
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => $fillByShops[FinanceModelShop::TYPE_SHOP],
                ]); ?>
            </div>
        </div>
        <div class="column dropdown-fill-by" data-type="2" style="display: <?=(!$model->id || $model->type == 1 ? 'none' : '')?>">
            <div class="dropup">
                <?= \yii\helpers\Html::button('Заполнить по...', [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                    'data-toggle' => 'dropdown',
                    'disabled' => !$fillByShops[FinanceModelShop::TYPE_INTERNET_SHOP]
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => $fillByShops[FinanceModelShop::TYPE_INTERNET_SHOP],
                ]); ?>
            </div>
        </div>
        <?php if ($model->id): ?>
            <div class="ml-auto column">
                <?= Html::a('Удалить', '#modal-delete-shop', [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                    'data-toggle' => 'modal'
                ]); ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', ['view', 'id' => $financeModel->id], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>
        <?php else: ?>
            <div class="column ml-auto">
                <?= Html::a('Отменить', ['view', 'id' => $financeModel->id], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php $this->registerJs('FinanceModelEditShop.init();', \yii\web\View::POS_READY);