<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;

/** @var $financeModel FinanceModel */
/** @var $model FinanceModelShop */
?>

<!-- Confirm delete -->
<?php Modal::begin([
    'id' => 'modal-delete-shop',
    'title' => 'Вы уверены, что хотите удалить Точку продаж?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', ['/analytics/finance-model/delete-shop', 'model_id' => $financeModel->id, 'shop_id' => $model->id], [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>