<?php
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use yii\bootstrap4\Html;

/** @var $model FinanceModelShop */
/** @var $financeModel FinanceModel */
/** @var $type int */

$this->title = 'Редактирование ' . ($type == FinanceModel::TYPE_FIXED_COSTS ? 'постоянных расходов' : 'переменных расходов');

echo $this->render('_form_costs', [
    'model' => $model,
    'financeModel' => $financeModel,
    'type' => $type
]);

echo $this->render('update-costs-modal', [
    'model' => $model,
    'financeModel' => $financeModel
]);