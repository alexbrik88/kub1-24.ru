<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;

/* @var $tab string
 * @var $model FinanceModelShop
 * @var $financeModel FinanceModel
 * @var $costs array
 */

$pathTablePlus = '@frontend/modules/analytics/views/finance-model/partial/table_plus/';

$quarters = $financeModel->getQuarters();
$floorMap = Yii::$app->request->post('floorMap', []);

$_cellIds = $_dMonthes = [];
$q = 0;
foreach ($quarters as $quarter => $monthes) {
    $q++;
    $_cellIds[$quarter] = 'cell-' . $q;
    $_dMonthes[$quarter] = ArrayHelper::getValue($floorMap, 'cell-' . $q, 1);
}

?>

<div class="wrap wrap_padding_none" style="position: relative">

    <div id="sds-top-3" class="table-scroll-double top" style="display: none">
        <div class="table-wrap">&nbsp;</div>
    </div>

    <div id="sds-bottom-3" class="table-scroll-double bottom">
        <div class="table-wrap">
            <table class="finance-model-shop-month table table-style table-count-list table-compact" data-type="<?=($model->type)?>">
                <thead>
                <?= $this->render("{$pathTablePlus}/_head_sticky", [
                    'model' => $financeModel,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                ]) ?>
                </thead>

                <tbody>
                <?php

                /////////////////
                //   REVENUE   //
                /////////////////

                // LEVEL 1
                foreach ($revenue as $attr => $level1) {
                    $floorKey = "floor-{$attr}";
                    $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, false);

                    echo $this->render("{$pathTablePlus}/_row_editable", [
                        'level' => 1,
                        'title' => $level1['title'],
                        'data' => $level1['data'],
                        'trClass' => 'revenues',
                        'isBold' => $level1['isBold'] ?? false,
                        'calcJS' => $level1['calcJS'] ?? false,
                        'isInt' => $level1['isInt'] ?? false,
                        'attr' => $attr,
                        'avg' => 0,
                        'floorKey' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isAvgRow' => ($attr == 'average_check' || 'conversion' == $attr)
                    ]);
                }

                echo $this->render("{$pathTablePlus}/_row_empty", [
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'avg' => '',
                ]);

                ///////////////
                //   COSTS   //
                ///////////////

                // LEVEL 1
                foreach ($costs as $costKey1 => $level1) {
                    $floorKey1 = "{$costKey1}";
                    $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey1, false);
                    echo $this->render("{$pathTablePlus}/_row", [
                        'level' => 1,
                        'title' => $level1['title'],
                        'data' => $level1['data'],
                        'dataPercent' => $level1['dataPercent'] ?? false,
                        'avg' => $level1['avg'],
                        'trClass' => 'costs',
                        'dataAttr' => $costKey1,
                        'floorKey' => $floorKey1,
                        'isOpenedFloor' => $isOpenedFloor,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isBold' => false,
                        'isButtonable' => $level1['isButtonable'] ?? false,
                        'isActiveRow' => true,
                        'isEmptyRow' => empty($level1['data']),
                        'editUrl' => $level1['editUrl'] ?? null,
                        'editAttrJS' => $level1['editAttrJS'] ?? false,
                        'isTotal' => isset($level1['totalBlock']),
                        'units' => $level1['units'] ?? null
                    ]);
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- update item-->
    <div id="update-finance-model-item-panel" class="wrap wrap_padding_none" style="display: none">
        <div class="ml-3 mr-3 p-2">
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['update-other-cost-item', 'model_id' => $financeModel->id]),
                'id' => 'update-other-cost-item',
            ]); ?>
            <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
            <div style="display: flex">
                <div class="p-1" style="width: 5%; min-width: 150px; margin-right: 16px;">
            <span class="label w-700" style="text-align: center;">
                Средний показатель
            </span>
                    <?= Html::textInput("_amount[avg]", null, [
                        'class' => 'form-control amount-avg-input',
                        'style' => 'width: 100%;',
                        'autocomplete' => 'off'
                    ]); ?>
                </div>
                <div>
                    <div class="row mb-2">
                        <?php foreach ($financeModel->monthes as $ym): ?>
                            <?php
                            $year = substr($ym, 0, 4);
                            $month = substr($ym, 4, 2); ?>
                            <div class="column p-1" style="width: 5%;  min-width: 150px">
                        <span class="label" style="text-align: center;">
                            <?= ArrayHelper::getValue(AbstractFinance::$month, $month, $month) ?> <?= $year ?>
                        </span>
                                <?= Html::textInput("amount[{$ym}]", null, [
                                    'class' => 'form-control amount-input',
                                    'style' => 'width: 100%;',
                                    'data-month' => $ym
                                ]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 p-1 text-right">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-1',
                        'data-style' => 'expand-right',
                    ]); ?>
                </div>
                <div class="col-6 p-1 ml-auto text-left">
                    <?= Html::a('Отменить', 'javascript:;', [
                        'class' => 'cancel-edit-costs-js button-regular button-width button-hover-transparent button-clr',
                    ]); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJS(<<<JS
    
    FMTable1 = Object.create(FinanceModelCSTableSimple);
    FMTable1.options = {
        selectorTop: '#sds-top-3',
        selectorBottom: '#sds-bottom-3',
        fixedColumnWidth: 275,
        refreshByClick: [],
        refreshByChange: [],
    };
    
    FinanceModelEditCostsOnFly.init();
    FMTable1.init();
    
JS, \yii\web\View::POS_READY);
?>