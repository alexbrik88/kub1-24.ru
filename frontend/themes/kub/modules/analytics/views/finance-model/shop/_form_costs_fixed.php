<?php
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\document\InvoiceExpenditureItem;
use common\components\TextHelper;

/** @var array $costsData */
?>
<table class="table table-style table-count-list table-compact finance-model-shop-month">
    <thead>
    <tr>
        <th style="min-width: 200px">ПОСТОЯННЫЕ РАСХОДЫ</th>
        <th>Средний показатель</th>
        <?php
        foreach ($monthes as $ym): ?>
            <?php
            $year = substr($ym, 0, 4);
            $month = substr($ym, 4, 2); ?>
            <th class="nowrap">
                <?= ArrayHelper::getValue(AbstractFinance::$month, $month, $month) ?> <?= $year ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <!-- items -->
    <?php foreach ($costsData as $itemId => $monthesData): ?>
    <tr data-attr="<?= $itemId ?>">
        <td class="text-grey text_size_14">
            <?= ($expenditureModel = InvoiceExpenditureItem::findOne($itemId)) ? $expenditureModel->name : $itemId ?>
        </td>
        <td class="td-custom-value">
            <input type="text" name="_amountByItem[<?=($itemId)?>]" class="custom-value-avg form-control"
               value="">
        </td>
        <?php foreach ($monthesData as $month => $amount): ?>
            <td class="td-custom-value">
                <input type="text" name="amountByItem[<?=($itemId)?>][<?=($month)?>]" class="custom-value form-control"
                   value="<?= TextHelper::invoiceMoneyFormat($amount, 2) ?>">
            </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    <!-- total -->
    <tr data-attr="total">
        <td class="text-grey text_size_14 weight-700">ИТОГО, ₽</td>
        <td class="text-right weight-700 avg"></td>
        <?php foreach ($monthes as $month): ?>
            <td class="text-right weight-700">
                <!-- (js calculated) -->
            </td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>