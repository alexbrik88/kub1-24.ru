<?php use yii\widgets\Pjax; ?>

<div class="modal fade" id="break-even-article-modal" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить статью расходов в финансовую модель</h4>
            <span class="modal-header-description">
                Перед тем как добавить свою статью, проверьте, есть ли такая статья в предустановленных,
                но еще не используемых вами
            </span>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php
                Pjax::begin([
                    'id' => 'pjax-break-even-article-modal',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'scrollTo' => false,
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click', '.add-article-btn', function() {
        $.pjax({url: $(this).data('url'), container: '#pjax-break-even-article-modal', push: false, scrollTo: false});
        $(document).on('pjax:success', '#pjax-break-even-article-modal', function(e, data) {
            $('#break-even-article-modal').modal();
            $('#break-even-customer-inputs').find('input').each(function(i,v) {
                let input = $(this).clone();
                if ($(input).hasClass('custom-value')) {
                    $(input)
                        .attr('type', 'hidden')
                        .removeAttr('class')
                        .val($(input).val().sanitizeKub())
                        .appendTo('#article-form');
                }
            });
        });
    });
    $(document).off('change', '#articledropdownform-isnew');
    $(document).on('change', '#articledropdownform-isnew', function(e) {
        const isNew = +$(this).find(':checked').val();

        const blockItems = $('.field-articledropdownform-items');
        const blockItems2 = $('.field-articledropdownform-items2');
        const blockNewItem = $('.field-articledropdownform-name');

        if (isNew === 2) {
            blockItems2.slideDown();
            blockNewItem.slideUp();
            blockItems.slideUp();
        }
        else if (isNew === 1) {
            blockNewItem.slideDown();
            blockItems2.slideUp();
            blockItems.slideUp();
        } else if (isNew === 0) {
            blockItems.slideDown();
            blockItems2.slideUp();
            blockNewItem.slideUp();
        }
    });
    $(document).on('change', '.shops-checkboxes', function() {
        if ($(this).hasClass('all')) {
            $('#break-even-article-modal').find('.shops-checkboxes').filter('.by-id').prop("checked", false).uniform("refresh");
        } else {
            $('#break-even-article-modal').find('.shops-checkboxes').filter('.all').prop("checked", false).uniform("refresh");
        }
    });
</script>