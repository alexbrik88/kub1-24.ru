<?php
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;

/** @var $model FinanceModelShop title */
/** @var $financeModel FinanceModel */

$this->title = 'Добавление точки продаж';

echo $this->render('_form_revenue', [
    'model' => $model,
    'financeModel' => $financeModel
]);

echo $this->render('modal', [
    'model' => $model,
    'financeModel' => $financeModel
]);