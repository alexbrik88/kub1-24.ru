<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;

/** @var $type int */
/** @var $financeModel FinanceModel */
/** @var $model FinanceModelShop */
/** @var $monthes array */

$monthes = $financeModel->getMonthes();
if ($type == FinanceModel::TYPE_FIXED_COSTS) {
    $costsData = $model->fixedCostMonthsData;
    $revenueData = null;
} else {
    $costsData = $model->variableCostMonthsPercentData;
    $revenueData = $model->revenueMonthsAmountData;
}
?>

<?php $form = ActiveForm::begin([
    'id' => 'finance-model-shop-form',
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
<div class="wrap mb-0">

    <div class="form-title mb-3">
        <?= $this->title ?>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'type')->widget(Select2::class, [
                'model' => $model,
                'disabled' => (bool)$model->id,
                'data' => [
                    FinanceModelShop::TYPE_SHOP => 'Оффлайн Магазин',
                    FinanceModelShop::TYPE_INTERNET_SHOP => 'Интернет Магазин'
                ],
                'hideSearch' => true,
                'options' => [
                    'id' => 'finance-model-shop-type',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => 'Выберите тип точки продаж'
                ],
            ]) ?>
        </div>
    </div>
</div>

<div class="row mt-2 mb-2">
    <div class="column ml-auto">
        <?= Html::button('<svg class="svg-icon"> <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use> </svg><span>Добавить статью</span>', [
            'class' => 'add-article-btn button-regular button-regular_red',
            'data-url' => Url::to(['add-expenditure-item', 'model_id' => $financeModel->id, 'shop_id' => $model->id, 'type' => $type])
        ]);
        ?>
    </div>
</div>

<div class="wrap wrap_padding_none" style="position: relative">

    <div id="sds-top-3" class="table-scroll-double top" style="display: none">
        <div class="table-wrap">&nbsp;</div>
    </div>

    <div id="sds-bottom-3" class="table-scroll-double bottom">
        <div class="table-wrap">
            <?= $this->render(($type == FinanceModel::TYPE_FIXED_COSTS) ? '_form_costs_fixed' : '_form_costs_variable', [
                'monthes' => $monthes,
                'costsData' => $costsData,
                'revenueData' => $revenueData
            ]) ?>
        </div>
    </div>
</div>

<div class="hidden">
    <?php foreach ($monthes as $month): ?>
        <input type="hidden" name="monthes[]" value="<?=($month)?>">
    <?php endforeach; ?>
</div>

<?php ActiveForm::end(); ?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-start">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'form' => 'finance-model-shop-form',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', ['view', 'id' => $financeModel->id], [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
            ]); ?>
        </div>
    </div>
</div>

<?php $this->registerJs(($type == FinanceModel::TYPE_FIXED_COSTS) ?
    'FinanceModelEditShopCostsFixed.init();' : 'FinanceModelEditShopCostsVariable.init();'
, \yii\web\View::POS_READY);


$this->registerJS(<<<JS
    
    FMTable1 = Object.create(FinanceModelCSTableSimple);
    FMTable1.options = {
        selectorTop: '#sds-top-3',
        selectorBottom: '#sds-bottom-3',
        fixedColumnWidth: 275,
        refreshByClick: [],
        refreshByChange: [],
    };
    
    $(document).ready(function() {
        FMTable1.init();
    });
    
JS);
