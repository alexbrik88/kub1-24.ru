<?php
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\document\InvoiceExpenditureItem;
use common\components\TextHelper;

/** @var array $monthes */
/** @var array $costsData */
/** @var array $revenueData */
?>
<table class="table table-style table-count-list table-compact finance-model-shop-month">
    <thead>
    <tr>
        <th style="min-width: 200px">ПЕРЕМЕННЫЕ РАСХОДЫ</th>
        <th>%</th>
        <?php
        foreach ($monthes as $ym): ?>
            <?php
            $year = substr($ym, 0, 4);
            $month = substr($ym, 4, 2); ?>
            <th class="nowrap">
                <?= ArrayHelper::getValue(AbstractFinance::$month, $month, $month) ?> <?= $year ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <!-- items -->
    <?php foreach ($costsData as $itemId => $percent): ?>
        <tr data-attr="<?= $itemId ?>">
            <td class="text-grey text_size_14">
                <?= ($expenditureModel = InvoiceExpenditureItem::findOne($itemId)) ? $expenditureModel->name : $itemId ?>
            </td>
            <td class="td-custom-value">
                <input type="text" name="percentByItem[<?=($itemId)?>]" class="custom-value-percent form-control"
                       value="<?= TextHelper::moneyFormat($percent,2) ?>">
            </td>
            <?php foreach ($monthes as $month): ?>
                <?php $revenue = ArrayHelper::getValue($revenueData, $month, 0); ?>
                <td class="td-value text-right weight-700" data-item-id="<?=($itemId)?>" data-month="<?=($month)?>" data-revenue="<?=($revenue)?>">
                    <!-- (js calculated) -->
                </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    <!-- total -->
    <tr data-attr="total">
        <td class="text-grey text_size_14 weight-700">ИТОГО, ₽</td>
        <td class="text-right weight-700 percent"></td>
        <?php foreach ($monthes as $month): ?>
            <td class="text-right weight-700">
                <!-- (js calculated) -->
            </td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>