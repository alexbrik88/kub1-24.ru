<?php

use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use common\models\User;
use frontend\modules\analytics\models\financeModel\FinanceModelSearch;
use frontend\modules\analytics\models\financeModel\FinanceModel;

/** @var View $this */
/** @var FinanceModelSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var User $user */

$this->title = 'Финансовые модели';
$tabViewClass = $user->identity->config->getTableViewClass('table_view_finance_model');
$tabConfigClass = [
    'margin' => 'col_finance_model_margin' . ($user->identity->config->finance_model_margin ? '' : ' hidden'),
    'revenue' => 'col_finance_model_revenue' . ($user->identity->config->finance_model_revenue ? '' : ' hidden'),
    'comment' =>  'col_finance_model_comment' . ($user->identity->config->finance_model_comment ? '' : ' hidden'),
    'employee' =>  'col_finance_model_employee' . ($user->identity->config->finance_model_employee ? '' : ' hidden'),
    'net_profit' => 'col_finance_model_net_profit' . ($user->identity->config->finance_model_net_profit ? '' : ' hidden'),
    'sales_profit' => 'col_finance_model_sales_profit' . ($user->identity->config->finance_model_sales_profit ? '' : ' hidden'),
    'capital_profit' => 'col_finance_model_capital_profit' . ($user->identity->config->finance_model_capital_profit ? '' : ' hidden'),
];
$emptyMessage = ($searchModel->name) ? 'По вашему запросу ничего не найдено.' : 'Вы еще не создали ни одной модели.';
$canDelete = Yii::$app->user->can(UserRole::ROLE_CHIEF);
$canCopy = Yii::$app->user->can(UserRole::ROLE_CHIEF);
?>

<div class="stop-zone-for-fixed-elems">

    <div class="page-head d-flex flex-wrap align-items-center mb-2">
        <h4><?= Html::encode($this->title) ?></h4>
        <?= Html::button('<svg class="svg-icon"> <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use> </svg><span>Добавить</span>', [
            'class' => 'button-regular button-regular_red button-width ml-auto',
            'data-toggle' => "modal",
            'data-target' => "#add-finance-model"
        ]); ?>
    </div>

    <?= $this->render('partial/index-controls', [
        'searchModel' => $searchModel
    ]) ?>

    <?= $this->render('partial/index-table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'tabViewClass' => $tabViewClass,
        'tabConfigClass' => $tabConfigClass,
        'emptyMessage' => $emptyMessage
    ]) ?>

</div>

<?= $this->render('partial/index-modal', ['model' => new FinanceModel()]) ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'hideCalculatedFields' => true,
    'buttons' => [
        $canCopy ? Html::button(Icon::get('copied').' <span>Копировать</span>', [
            'id' => 'btn-copy-model',
            'class' => 'button-clr button-regular button-width button-hover-transparent tooltip3',
            'data-toggle' => 'modal',
            'data-target' => '#copy-model',
            'data-tooltip-content' => '#tooltip-copy-model',
            'title' => 'Копировать'
        ]) : null,
        $canDelete ? Html::button(Icon::get('garbage').' <span>Удалить</span>', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-target' => '#many-delete',
            'title' => 'Удалить'
        ]) : null,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentCloning' => true,
    ],
]);

?>

