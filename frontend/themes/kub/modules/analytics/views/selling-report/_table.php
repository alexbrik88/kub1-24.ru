<?php

use common\components\helpers\ArrayHelper;
use frontend\components\PageSize;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\TextHelper;
use frontend\modules\analytics\models\SellingReportSearch2;
use common\models\employee\Config;
use yii\data\SqlDataProvider;
use frontend\modules\analytics\assets\CSTable2Asset;
use common\models\Contractor;

/* @var $this yii\web\View
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel SellingReportSearch2
 * @var $dataProvider SqlDataProvider
 * @var $userConfig Config
 */

CSTable2Asset::register($this);

$dataArray = $dataProvider->getModels();
$totals = $searchModel->getTotals();
if ($searchModel->searchBy == $searchModel::BY_FLOWS) {
    $undistributed = $searchModel->getUndistributedFlows();
    if (empty($undistributed['amount_y'])) // sum by year
        unset($undistributed);
}

$cell_id = [
    1 => 'first-cell',
    2 => 'second-cell',
    3 => 'third-cell',
    4 => 'fourth-cell'
];

$isCurrentYear = $searchModel->year == date('Y');
$currentMonthNumber = date('m');

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}
$tabViewClass = $userConfig->getTableViewClass('table_view_report_invoice');

$showColumn2 = $searchModel->showPercentTotal;
$showColumn3 = $searchModel->showPercent;
$columnsCount = 3 * ($showColumn2 + $showColumn3 + 2);
$columnsCountCollapsed = 1 * ($showColumn2 + $showColumn3 + 2);

$colspan = [
    'single' => 'colspan="' . $columnsCountCollapsed . '"',
    'multi' => 'colspan="' . $columnsCount . '"',
];
?>

<div id="cs-table-11x" class="custom-scroll-table-double cs-top">
    <div class="table-wrap">&nbsp;</div>
</div>

<div class="wrap wrap_padding_none_all" style="position:relative!important;">
    <div id="cs-table-1x" class="custom-scroll-table-double">
        <div class="table-wrap">
            <table id="table_selling_report" class="table-bleak table table-style table-count-list <?= $tabViewClass ?> mb-0">
                <thead>
                <tr class="quarters-flow-of-funds-double">
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                        <div class="pl-1 pr-1">
                            <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => ($searchModel->groupByContractor) ? 'Заказчики' : 'Товары/Услуги', 'attr' => 'item_name']) ?>
                        </div>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                        </button>
                    </th>
                    <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? $colspan['multi'] : $colspan['single'] ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                        <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell" data-columns-count-collapsed="<?= $columnsCountCollapsed ?>" data-columns-count="<?= $columnsCount ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                        </button>
                    </th>
                    <th <?= $colspan['single'] ?> class="pl-2 pr-2 pt-3 pb-3 align-top">
                        <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
                    </th>
                </tr>
                <tr>
                    <th><!----></th>
                    <?php foreach (AbstractFinance::$month as $key => $month): ?>
                        <?php $quarter = (int)ceil($key / 3); ?>
                        <th <?= $colspan['single'] ?> class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" style="border-bottom: 1px solid;"
                            data-collapse-cell
                            data-id="<?= $cell_id[$quarter] ?>"
                            data-month="<?= $key; ?>"
                        >
                            <div class="pl-1 pr-1"><?= $month; ?></div>
                        </th>
                        <?php if ($key % 3 == 0): ?>
                            <th <?= $colspan['single'] ?> class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" style="border-bottom: 1px solid;"
                                data-collapse-cell-total
                                data-id="<?= $cell_id[$quarter] ?>"
                            >
                                <div class="pl-1 pr-1">Итого</div>
                            </th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <th <?= $colspan['single'] ?> class="pl-2 pr-2" style="border-top-color: transparent; border-bottom: 1px solid;"><div class="pl-1 pr-1">Итого</div></th>
                </tr>
                <tr class="th-flow-of-funds-double">
                    <th><!----></th>
                    <?php foreach (AbstractFinance::$month as $key => $month): ?>
                        <?php
                        $quarter = (int)ceil($key / 3);
                        $monthNumber = "m{$key}";
                        $quarterNumber = "q{$quarter}";
                        $yearNumber = "y";
                        ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                            <div class="pl-1 pr-1 nowrap">
                                <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Кол-во', 'attr' => "quantity_{$monthNumber}"]) ?>
                            </div>
                        </th>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                            <div class="pl-1 pr-1 nowrap">
                                <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Сумма', 'attr' => "amount_{$monthNumber}"]) ?>
                            </div>
                        </th>
                        <?php if ($showColumn2): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                                <div class="pl-1 pr-1 nowrap">
                                    <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%', 'attr' => "percent_total_{$monthNumber}"]) ?>
                                </div>
                            </th>
                        <?php endif; ?>
                        <?php if ($showColumn3): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-month="<?= $key; ?>" data-month-name="<?= mb_strtolower($month) ?>">
                                <div class="pl-1 pr-1 nowrap">
                                    <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%%', 'attr' => $searchModel::PREFIX_PERCENT.$monthNumber]) ?>
                                </div>
                            </th>
                        <?php endif; ?>

                        <?php if ($key % 3 == 0): ?>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                <div class="pl-1 pr-1 nowrap">
                                    <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Кол-во', 'attr' => "quantity_{$quarterNumber}"]) ?>
                                </div>
                            </th>
                            <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                <div class="pl-1 pr-1 nowrap">
                                    <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Сумма', 'attr' => "amount_{$quarterNumber}"]) ?>
                                </div>
                            </th>
                            <?php if ($showColumn2): ?>
                                <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 nowrap">
                                        <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%', 'attr' => "percent_total_{$quarterNumber}"]) ?>
                                    </div>
                                </th>
                            <?php endif; ?>
                            <?php if ($showColumn3): ?>
                                <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 nowrap">
                                        <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%%', 'attr' => $searchModel::PREFIX_PERCENT.$quarterNumber]) ?>
                                    </div>
                                </th>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                        <div class="pl-1 pr-1 nowrap">
                            <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Кол-во', 'attr' => "quantity_y"]) ?>
                        </div>
                    </th>
                    <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                        <div class="pl-1 pr-1 nowrap">
                            <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => 'Сумма', 'attr' => 'amount_y']) ?>
                        </div>
                    </th>
                    <?php if ($showColumn2): ?>
                        <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                            <div class="pl-1 pr-1 nowrap">
                                <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%', 'attr' => 'percent_total_y']) ?>
                            </div>
                        </th>
                    <?php endif; ?>
                    <?php if ($showColumn3): ?>
                        <th class="pl-2 pr-2" style="border-top-color: transparent" data-year="<?= $searchModel->year ?>">
                            <div class="pl-1 pr-1 nowrap">
                                <?= $this->render('_sort_arrows', ['model' => $searchModel, 'title' => '%%', 'attr' => $searchModel::PREFIX_PERCENT.'y']) ?>
                            </div>
                        </th>
                    <?php endif; ?>
                </tr>
                </thead>

                <tbody>
    
                <!-- TOTALS -->
                <tr class="not-drag cancel-drag">
                    <td class="pl-2 pr-1 pt-3 pb-3">
                        <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                            <div class="text_size_14 weight-700 mr-2 nowrap">
                                Итого продажи
                            </div>
                        </div>
                    </td>
                    <?php $key = 0; ?>
                    <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): $key++; ?>
                        <?php
                        $quarter = (int)ceil($key / 3);
                        $monthNumber = "m{$monthNumber}";
                        $quarterNumber = "q{$quarter}";
                        $yearNumber = "y";
                        ?>

                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat($totals[$searchModel::PREFIX_QUANTITY.$monthNumber] ?? 0, 2); ?>
                            </div>
                        </td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::invoiceMoneyFormat($totals[$searchModel::PREFIX_AMOUNT.$monthNumber] ?? 0, 2); ?>
                            </div>
                        </td>
                        <?php if ($showColumn2): ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::numberFormat(100, 2); ?>
                                </div>
                            </td>
                        <?php endif; ?>
                        <?php if ($showColumn3): ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::numberFormat($totals[$searchModel::PREFIX_PERCENT.$monthNumber] ?? 0, 2); ?>
                                </div>
                            </td>
                        <?php endif; ?>
                        <?php if ($key % 3 == 0): ?>
                            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?php echo TextHelper::numberFormat($totals[$searchModel::PREFIX_QUANTITY.$quarterNumber] ?? 0, 2) ?>
                                </div>
                            </td>
                            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($totals[$searchModel::PREFIX_AMOUNT.$quarterNumber] ?? 0, 2) ?>
                                </div>
                            </td>
                            <?php if ($showColumn2): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?= TextHelper::numberFormat(100, 2) ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                            <?php if ($showColumn3): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?= TextHelper::numberFormat($totals[$searchModel::PREFIX_PERCENT.$quarterNumber] ?? 0, 2); ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::numberFormat($totals[$searchModel::PREFIX_QUANTITY.$yearNumber] ?? 0, 2); ?>
                        </div>
                    </td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($totals[$searchModel::PREFIX_AMOUNT.$yearNumber] ?? 0, 2); ?>
                        </div>
                    </td>
                    <?php if ($showColumn2): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat(100, 2); ?>
                            </div>
                        </td>
                    <?php endif; ?>
                    <?php if ($showColumn3): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::numberFormat($totals[$searchModel::PREFIX_PERCENT.$yearNumber] ?? 0, 2); ?>
                            </div>
                        </td>
                    <?php endif; ?>
                </tr>
                <!-- end TOTALS -->

                <?php if (!empty($undistributed)): ?>
                <!-- UNDISTRIBUTED FLOWS -->
                <tr class="not-drag cancel-drag">
                    <td class="pl-2 pr-1 pt-3 pb-3">
                        <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                            <div class="text_size_14 color-red weight-700 mr-2 nowrap">
                                Неразнесенные оплаты
                            </div>
                        </div>
                    </td>
                    <?php $key = 0; ?>
                    <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): $key++; ?>
                        <?php
                        $quarter = (int)ceil($key / 3);
                        $monthNumber = "m{$monthNumber}";
                        $quarterNumber = "q{$quarter}";
                        $yearNumber = "y";
                        ?>

                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 color-red weight-700">
                                <!--not used-->
                            </div>
                        </td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 color-red weight-700">
                                <?= TextHelper::invoiceMoneyFormat($undistributed[$searchModel::PREFIX_AMOUNT.$monthNumber] ?? 0, 2); ?>
                            </div>
                        </td>
                        <?php if ($showColumn2): ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 color-red weight-700">
                                    <!--not used-->
                                </div>
                            </td>
                        <?php endif; ?>
                        <?php if ($showColumn3): ?>
                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 color-red weight-700">
                                    <!--not used-->
                                </div>
                            </td>
                        <?php endif; ?>
                        <?php if ($key % 3 == 0): ?>
                            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 color-red weight-700">
                                    <!--not used-->
                                </div>
                            </td>
                            <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 color-red weight-700">
                                    <?= TextHelper::invoiceMoneyFormat($undistributed[$searchModel::PREFIX_AMOUNT.$quarterNumber] ?? 0, 2) ?>
                                </div>
                            </td>
                            <?php if ($showColumn2): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 color-red weight-700">
                                        <!--not used-->
                                    </div>
                                </td>
                            <?php endif; ?>
                            <?php if ($showColumn3): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 color-red weight-700">
                                        <!--not used-->
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                        <div class="pl-1 pr-1 color-red weight-700">
                            <!--not used-->
                        </div>
                    </td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                        <div class="pl-1 pr-1 color-red weight-700">
                            <?= TextHelper::invoiceMoneyFormat($undistributed[$searchModel::PREFIX_AMOUNT.$yearNumber] ?? 0, 2); ?>
                        </div>
                    </td>
                    <?php if ($showColumn2): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                            <div class="pl-1 pr-1 color-red weight-700">
                                <!--not used-->
                            </div>
                        </td>
                    <?php endif; ?>
                    <?php if ($showColumn3): ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                            <div class="pl-1 pr-1 color-red weight-700">
                                <!--not used-->
                            </div>
                        </td>
                    <?php endif; ?>
                </tr>
                <!-- end UNDISTRIBUTED FLOWS -->
                <?php endif; ?>

                <?php if (!empty($dataArray)): ?>
                    <?php foreach ($dataArray as $data): ?>

                        <?php
                        if ($searchModel->groupByContractor) {
                            $itemID = $data['contractor_id'];
                            $itemName = ($c = Contractor::findOne(['id' => $itemID])) ? $c->getShortName() : "id={$itemID}";
                        } else {
                            $itemID = $data['product_id'];
                            $itemName = $data['item_name'];
                        }

                        $floorKey1 = "first-floor-{$itemID}";
                        $isOpenedFloor1 = false; // todo: ArrayHelper::getValue($floorMap, $floorKey1, false); ?>

                        <tr class="item-block" data-item_id="<?= $itemID; ?>">
                            <td class="pl-2 pr-1 pt-3 pb-3">
                                <?php if ($searchModel->groupByContractor): ?>
                                    <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="text-grey text_size_14 ml-1 nowrap">
                                            <?= htmlspecialchars($itemName) ?>
                                        </span>
                                    </button>
                                <?php else: ?>
                                    <div class="text-grey text_size_14 title-cell pl-1 pr-1" style="min-width: 275px; width: 275px; max-width: 275px;">
                                        <span title="<?= htmlspecialchars($itemName) ?>">
                                            <?= htmlspecialchars((mb_strlen($itemName) > 80) ? (mb_substr($itemName, 0, 77) . '...') : $itemName); // because 2 lines max row height ?>
                                        </span>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <?php $key = 0; ?>
                            <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): $key++; ?>
                                <?php
                                $quarter = (int)ceil($key / 3);
                                $monthNumber = "m{$monthNumber}";
                                $quarterNumber = "q{$quarter}";
                                $yearNumber = "y";
                                ?>

                                <!-- MONTH ---------------->
                                <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::numberFormat($data[$searchModel::PREFIX_QUANTITY.$monthNumber] ?? 0, 2) ?>
                                    </div>
                                </td>
                                <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat($data[$searchModel::PREFIX_AMOUNT.$monthNumber] ?? 0, 2) // percent Month ?>
                                    </div>
                                </td>
                                <?php if ($showColumn2): ?>
                                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT_TOTAL.$monthNumber] ?? 0, 2) // percent Month ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                                <?php if ($showColumn3): ?>
                                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT.$monthNumber] ?? 0, 2) // percent2 Month ?>
                                        </div>
                                    </td>
                                <?php endif; ?>

                                <!-- QUARTER ---------------->
                                <?php if ($key % 3 == 0): ?>
                                    <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::numberFormat($data[$searchModel::PREFIX_QUANTITY.$quarterNumber] ?? 0, 2); ?>
                                        </div>
                                    </td>
                                    <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($data[$searchModel::PREFIX_AMOUNT.$quarterNumber] ?? 0, 2) // percent Quarter ?>
                                        </div>
                                    </td>
                                    <?php if ($showColumn2): ?>
                                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT_TOTAL.$quarterNumber] ?? 0, 2) // percent Quarter ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                    <?php if ($showColumn3): ?>
                                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT.$quarterNumber] ?? 0, 2) // percent2 Quarter ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach ?>

                            <!-- YEAR ---------------->
                            <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>">
                                <div class="pl-1 pr-1">
                                    <?= TextHelper::numberFormat($data[$searchModel::PREFIX_QUANTITY.$yearNumber] ?? 0, 2) ?>
                                </div>
                            </td>
                            <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>">
                                <div class="pl-1 pr-1">
                                    <?= TextHelper::invoiceMoneyFormat($data[$searchModel::PREFIX_AMOUNT.$yearNumber] ?? 0, 2) // percent Year ?>
                                </div>
                            </td>
                            <?php if ($showColumn2): ?>
                                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT_TOTAL.$yearNumber] ?? 0, 2) // percent Year ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                            <?php if ($showColumn3): ?>
                                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::numberFormat($data[$searchModel::PREFIX_PERCENT.$yearNumber] ?? 0, 2) // percent2 Year ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        </tr>

                        <?php if ($searchModel->groupByContractor): ?>
                            <tr class="item-product-block <?= !$isOpenedFloor1 ? 'd-none':'' ?>" data-id="<?= $floorKey1 ?>" data-item_id="<?= $itemID; ?>">
                                <td class="text-grey text_size_14 pl-2 pr-1 pt-3 pb-3" data-col="item_name">
                                    <div class="title-cell pl-1 pr-1" style="min-width: 275px; width: 275px; max-width: 275px;">
                                        <!--js-->
                                    </div>
                                </td>
                                <?php $key = 0; ?>
                                <?php foreach (AbstractFinance::$month as $monthNumber => $monthText): $key++; ?>
                                    <?php
                                    $quarter = (int)ceil($key / 3);
                                    $monthNumber = "m{$monthNumber}";
                                    $quarterNumber = "q{$quarter}";
                                    $yearNumber = "y";
                                    ?>

                                    <!-- MONTH ---------------->
                                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_QUANTITY.$monthNumber ?>">
                                        <div class="pl-1 pr-1">
                                            <!--js-->
                                        </div>
                                    </td>
                                    <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_AMOUNT.$monthNumber ?>">
                                        <div class="pl-1 pr-1">
                                            <!--js-->
                                        </div>
                                    </td>
                                    <?php if ($showColumn2): ?>
                                        <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_PERCENT_TOTAL.$monthNumber ?>">
                                            <div class="pl-1 pr-1">
                                                <!--js-->
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                    <?php if ($showColumn3): ?>
                                        <td class="cant-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_PERCENT.$monthNumber ?>">
                                            <div class="pl-1 pr-1">
                                                <!--js-->
                                            </div>
                                        </td>
                                    <?php endif; ?>

                                    <!-- QUARTER ---------------->
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_QUANTITY.$quarterNumber ?>">
                                            <div class="pl-1 pr-1">
                                                <!--js-->
                                            </div>
                                        </td>
                                        <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_AMOUNT.$quarterNumber ?>">
                                            <div class="pl-1 pr-1">
                                                <!--js-->
                                            </div>
                                        </td>
                                        <?php if ($showColumn2): ?>
                                            <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_PERCENT_TOTAL.$quarterNumber ?>">
                                                <div class="pl-1 pr-1">
                                                    <!--js-->
                                                </div>
                                            </td>
                                        <?php endif; ?>
                                        <?php if ($showColumn3): ?>
                                            <td class="cant-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" data-col="<?= $searchModel::PREFIX_PERCENT.$quarterNumber ?>">
                                                <div class="pl-1 pr-1">
                                                    <!--js-->
                                                </div>
                                            </td>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach ?>

                                <!-- YEAR ---------------->
                                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>" data-col="<?= $searchModel::PREFIX_QUANTITY.$yearNumber ?>">
                                    <div class="pl-1 pr-1">
                                        <!--js-->
                                    </div>
                                </td>
                                <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>" data-col="<?= $searchModel::PREFIX_AMOUNT.$yearNumber ?>">
                                    <div class="pl-1 pr-1">
                                        <!--js-->
                                    </div>
                                </td>
                                <?php if ($showColumn2): ?>
                                    <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>" data-col="<?= $searchModel::PREFIX_PERCENT_TOTAL.$yearNumber ?>">
                                        <div class="pl-1 pr-1">
                                            <!--js-->
                                        </div>
                                    </td>
                                <?php endif; ?>
                                <?php if ($showColumn3): ?>
                                    <td class="cant-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $itemID; ?>" data-col="<?= $searchModel::PREFIX_PERCENT.$yearNumber ?>">
                                        <div class="pl-1 pr-1">
                                            <!--js-->
                                        </div>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php else: ?>
                        <tr class="empty-block">
                            <td class="pl-2 pr-1 pt-3 pb-3 text-grey text_size_14 ">
                                <div class="title-cell" style="min-width: 275px; width: 275px; max-width: 275px;">
                                    По вашему запросу ничего не найдено
                                </div>
                            </td>
                            <td colspan="999"></td>
                        </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="table-settings-view row align-items-center">
    <div class="col-8">
        <nav>
            <?= \common\components\grid\KubLinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]) ?>
        </nav>
    </div>
    <div class="col-4">
        <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
            'maxTitle' => $dataProvider->totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => 'per-page',
        ]) ?>
    </div>
</div>
