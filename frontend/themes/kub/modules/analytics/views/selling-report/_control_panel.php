<?php
use common\components\helpers\Html;
use frontend\modules\analytics\models\SellingReportSearch2;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableViewWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/** @var $searchBy */
/** @var $searchModel SellingReportSearch2 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$projectId = $projectId ?? null;
$xlsGetParams = ($projectId) ? ['project_id' => $projectId] : [];
?>

<div class="row row_indents_s" style="margin-bottom: -5px">
    <div class="col-6">
        <div class="d-flex flex-nowrap">
                <!-- Options -->
                <div class="ml-0">
                    <div class="dropdown dropdown-settings d-inline-block" title="Настройка таблицы" style="z-index: 1001">
                        <?= Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>', [
                            'class' => 'button-list button-hover-transparent button-clr mr-2',
                            'data-toggle' => 'dropdown',
                        ]) ?>
                        <ul id="config_items_box" class="dropdown-popup dropdown-menu" role="menu" style="padding: 10px 15px;">
                            <li>
                                <label style="font-weight: bold;">Настройка таблицы</label>
                            </li>
                            <li class="bold" style="border-top: 1px solid #ddd;">
                                <label style="font-weight: bold;">Выводить по</label>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::radio('selling_report_search_by', $searchModel->searchBy == $searchModel::BY_FLOWS, [
                                    'class' => 'selling_report_search_by',
                                    'label' => 'По оплатам',
                                    'data-url' => '/analytics/selling-report/change-view',
                                    'value' => $searchModel::BY_FLOWS
                                ]); ?>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::radio('selling_report_search_by', $searchModel->searchBy == $searchModel::BY_INVOICES, [
                                    'class' => 'selling_report_search_by',
                                    'label' => 'По счетам',
                                    'data-url' => '/analytics/selling-report/change-view',
                                    'value' => $searchModel::BY_INVOICES
                                ]); ?>
                            </li>
                            <li class="pl-4" style="white-space: nowrap;">
                                <?= Html::radio('selling_report_type', $searchModel->type == $searchModel::TYPE_ALL, [
                                    'class' => 'selling_report_type',
                                    'label' => 'Все',
                                    'data-url' => '/analytics/selling-report/change-view',
                                    'value' => $searchModel::TYPE_ALL,
                                    'disabled' => $searchModel->searchBy == $searchModel::BY_FLOWS
                                ]); ?>
                            </li>
                            <li class="pl-4" style="white-space: nowrap;">
                                <?= Html::radio('selling_report_type', $searchModel->type == $searchModel::TYPE_PAID, [
                                    'class' => 'selling_report_type',
                                    'label' => 'Оплаченные',
                                    'data-url' => '/analytics/selling-report/change-view',
                                    'value' => $searchModel::TYPE_PAID,
                                    'disabled' => $searchModel->searchBy == $searchModel::BY_FLOWS
                                ]); ?>
                            </li>
                            <li class="pl-4" style="white-space: nowrap;">
                                <?= Html::radio('selling_report_type', $searchModel->type == $searchModel::TYPE_NOT_PAID, [
                                    'class' => 'selling_report_type',
                                    'label' => 'Неоплаченные',
                                    'data-url' => '/analytics/selling-report/change-view',
                                    'value' => $searchModel::TYPE_NOT_PAID,
                                    'disabled' => $searchModel->searchBy == $searchModel::BY_FLOWS
                                ]); ?>
                            </li>
                            <li style="white-space: nowrap; border-top: 1px solid #ddd;">
                                <?= Html::checkbox('show_percent_total', $searchModel->showPercentTotal, [
                                    'class' => 'selling_report_options_checkbox',
                                    'label' => '% от итого',
                                    'data-url' => '/analytics/selling-report/change-view',
                                ]) . Icon::get('question', [
                                    'class' => 'ml-2 tooltip-question-icon',
                                    'title' => 'Процент от итого за период',
                                    'title-as-html' => 1
                                ]); ?>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::checkbox('show_percent', $searchModel->showPercent, [
                                    'class' => 'selling_report_options_checkbox',
                                    'label' => '%% от предыдущего',
                                    'data-url' => '/analytics/selling-report/change-view',
                                ]) . Icon::get('question', [
                                    'class' => 'ml-2 tooltip-question-icon',
                                    'title' => 'Процент разницы от предыдущего<br/>периода (месяц, квартал, год)',
                                    'title-as-html' => 1
                                ]); ?>
                            </li>
                            <li style="white-space: nowrap;">
                                <?= Html::checkbox('group_by_contractor', $searchModel->groupByContractor, [
                                    'class' => 'selling_report_options_checkbox',
                                    'label' => 'Группировать по заказчикам',
                                    'data-url' => '/analytics/selling-report/change-view',
                                ]); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php if (YII_ENV_DEV || !Yii::$app->user->identity->company->isFreeTariff): ?>
                <div class="pl-1">
                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                        Url::to(['/analytics/selling-report/get-xls',
                            'year' => $searchModel->year] + $xlsGetParams), [
                            'class' => 'button-list button-hover-transparent button-clr mb-2 disabled',
                            'title' => 'Скачать в Excel',
                        ]); ?>
                </div>
            <?php endif; ?>
            <div class="ml-2 pl-1">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_invoice']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'id' => 'selling-report-title-filter',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'name', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<script>
    // OPTIONS
    $('.selling_report_search_by').on('change', function() {
        $.ajax({
            "type": "post",
            "url": $(this).attr("data-url") + '?selling_report_search_by=' + $(this).val(),
            "data": $(this).serialize(),
            "success": function(data) {
                location.href = location.href;
            }
        });
    });
    $('.selling_report_type').on('change', function() {
        $.ajax({
            "type": "post",
            "url": $(this).attr("data-url") + '?selling_report_type=' + $(this).val(),
            "data": $(this).serialize(),
            "success": function(data) {
                location.href = location.href;
            }
        });
    });
    $('.selling_report_options_checkbox').on('change', function() {
        $.ajax({
            "type": "post",
            "url": $(this).attr("data-url") + '?' + $(this).attr('name') + '=' + ($(this).prop('checked') ? '1' : '0'),
            "data": $(this).serialize(),
            "success": function(data) {
                location.href = location.href;
            }
        });
    });
</script>