<?php
use yii\widgets\Pjax;
use frontend\modules\analytics\models\SellingReportSearch2;
use yii\data\SqlDataProvider;

/** @var SellingReportSearch2 $searchModel */
/** @var SqlDataProvider $dataProvider */

$this->title = 'Отчет по продажам';

if ($searchModel->searchBy == $searchModel::BY_FLOWS) {
    $this->title .= ' (по оплатам)';
} else {
    if ($searchModel->type == $searchModel::TYPE_PAID) {
        $this->title .= ' (по оплаченным счетам)';
    } elseif ($searchModel->type == $searchModel::TYPE_NOT_PAID) {
        $this->title .= ' (по неоплаченным счетам)';
    } else {
        $this->title .= ' (по всем счетам)';
    }
}

$showMenu = $showMenu ?? true;
$projectId = $projectId ?? null;

$renderParams = [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'userConfig' => Yii::$app->user->identity->config,
    'floorMap' => Yii::$app->request->post('floorMap', []),
    'projectId' => $projectId,
];

$pjaxParams = [
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'timeout' => 10000,
    'formSelector' => '#selling-report-title-filter',
];

if (!Yii::$app->request->isPjax) {
    // menu
    if ($showMenu) {
        echo $this->render('@frontend/themes/kub/modules/analytics/views/layouts/finance_submenu');
        echo $this->render('@frontend/themes/kub/modules/analytics/views/layouts/_by_selling_report_submenu');
    }

    // title & year & charts
    echo $this->render('_header', $renderParams);

    // options & search
    echo $this->render('_control_panel', $renderParams);
}

// table
Pjax::begin(array_merge($pjaxParams, ['id' => 'pjax_selling_report']));
echo $this->render('_table', $renderParams);
Pjax::end();
?>

<script>

    $(document).ready(function() {
        CSTable2.init({
            wrapper: "#cs-table-1x",
            topScrollbar: "#cs-table-11x",
            fixedColumnWidth: 300,
            refreshEvents: [
                {event: 'click', selector: '.table-collapse-btn'}
            ],
            reinitEvents: [
                {event: 'pjax:success', selector: '#pjax_selling_report'}
            ]
        });
    });

    SellingReport = {
        pjaxSelector: '#pjax_selling_report',
        tableSelector: '#table_selling_report',
        yearSelector: '#sellingreportsearch2-year',
        __loading: false,
        init: function() {
            this.bindPjaxEvents();
            this.bindUserEvents();
        },
        bindPjaxEvents: function () {
            const that = this;
            // main request (pjax)
            $(that.pjaxSelector).on('pjax:beforeSend', function (event, xhr, settings) {
                if (settings.data === undefined) {

                    let floorMap = {};

                    $(that.tableSelector).find('[data-collapse-row-trigger], [data-collapse-trigger]').each(function(i,v) {
                        floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
                    });

                    jQuery.pjax({
                        url: settings.url,
                        type: 'POST',
                        data: {'floorMap': floorMap},
                        container: that.pjaxSelector,
                        timeout: 15000,
                        push: false,
                        replace: false,
                        scrollTo: false
                    });

                    return false;
                }
            });
            // reload table (pjax)
            $(document).on("pjax:success", that.pjaxSelector, function(event, data) {
                // col
                $('thead [data-collapse-trigger]', that.tableSelector).click(function() {
                    var target = $(this).data('target');
                    var collapseCount = $(this).data('columns-count') || 3;
                    var uncollapseCount = $(this).data('columns-count-collapsed') || 1;
                    $(this).toggleClass('active');
                    $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                    $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                    if ( $(this).hasClass('active') ) {
                        $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                    } else {
                        $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', uncollapseCount);
                    }
                    $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
                    $(this).trigger('dataCollapseTriggerEnd');
                });
                // row
                $('tbody [data-collapse-row-trigger]', that.tableSelector).click(function() {
                    var target = $(this).data('target');
                    $(this).toggleClass('active');
                    if ( $(this).hasClass('active') ) {
                        $('[data-id="'+target+'"]').removeClass('d-none');
                    } else {
                        // level 1
                        $('[data-id="'+target+'"]').addClass('d-none');
                        $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    }
                    if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                        $('[data-collapse-all-trigger]').removeClass('active');
                    } else {
                        $('[data-collapse-all-trigger]').addClass('active');
                    }
                    $(this).trigger('dataCollapseTriggerEnd');
                });
            });
        },
        bindUserEvents: function() {
            const that = this;
            // change year
            $(that.yearSelector).change(function () {
                location.href = $(this).closest('form').attr('action') + '?' + $(this).closest('form').serialize();
            });
            $(document).on('click', that.tableSelector + ' [data-collapse-row-trigger]', function() {
                const button = $(this);
                const tr = $(this).closest('tr');
                const itemId = tr.data('item_id') || null;
                const trTemplate = $('tr.item-product-block[data-item_id=' + itemId + ']');

                if (!button.hasClass('active') || tr.hasClass('loaded'))
                    return;

                if (trTemplate.length === 0) {
                    window.toastr.error('Контрагент не найден');
                    return;
                }

                if (that.__loading) {
                    window.toastr.error('Дождитесь окончания загрузки');
                    button.removeClass('active');
                    return;
                } else {
                    that.__loading = true;
                }

                $.ajax({
                    "type": "post",
                    "url": '/analytics/selling-report/get-table-data',
                    "data": {
                        year: $('#sellingreportsearch2-year').val() || null,
                        group_by: 'contractor',
                        project: "<?= $projectId ?>",
                        contractor: itemId
                    },
                    "success": function(data) {
                        if (data) {
                            // products
                            if (data.products && data.products.length) {
                                $.each(data.products, function (i, product) {
                                    let row = $(trTemplate).clone();
                                    $(row).find('td').each(function (ii, td) {
                                        let attr = $(td).data('col');
                                        $(td).find('> div').html(product[attr] || null);
                                        if (attr === 'item_name')
                                            $(td).find('> div').attr('title', product[attr] || null);
                                    });
                                    $(trTemplate).before(row);
                                });
                            }
                            // undistributed flows
                            if (data.undistributed && data.undistributed.length) {
                                $.each(data.undistributed, function (i, product) {
                                    let row = $(trTemplate).clone();
                                    $(row).find('td').each(function (ii, td) {
                                        let attr = $(td).data('col');
                                        $(td).addClass('color-red').find('> div').html(product[attr] || null);
                                    });
                                    $(trTemplate).before(row);
                                });
                            }
                        }

                        $(tr).addClass('loaded');
                        $(trTemplate).remove();
                        that.__loading = false;
                    }
                });

            });
        },
    };

    SellingReport.init();


</script>