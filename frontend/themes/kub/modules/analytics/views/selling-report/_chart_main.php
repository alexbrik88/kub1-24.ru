<?php

use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\ChartHelper;
use frontend\modules\analytics\models\SellingReportSearch2;
use frontend\themes\kub\components\Icon;

/** @var $searchModel SellingReportSearch2 */

///////////////// page filters ////////////////////
$projectId = (int)$searchModel->project_id ?: null;
///////////////////////////////////////////////////

///////////////// dynamic vars ///////////
$customPeriod = "months";
$customOffset = $customOffset ?? 0;
$customDebtPeriod = $customDebtPeriod ?? null; // todo
/////////////////////////////////////////

///////////////// colors /////////////////
$color = 'rgba(0, 193, 155, 1)';
$color_opacity = 'rgba(0, 193, 155, .5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 12;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 6;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {
    $datePeriods = ChartHelper::getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= (count($datePeriods)))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
} else {
    die('Unknown period');
}

$data = $searchModel->getChartData($datePeriods);

if (Yii::$app->request->post('chart-selling-report-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $data,
                ],
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-selling-report { height: 235px; }
</style>

<div style="position: relative; margin-right:20px;">
    <div style="width: 100%;">

        <div class="chart-selling-report-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-selling-report-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            ДИНАМИКА ПРОДАЖ
        </div>
        <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
            <?= \kartik\select2\Select2::widget([
                'id' => 'chart-selling-report-period',
                'name' => 'purseType',
                'data' => [
                    '' => 'Итого',
                ],
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                    'options' => [
                        '' => ['data-chart-title' => 'Итого'],
                    ]
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '170px'
                ]
            ]); ?>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-selling-report',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;                                

                                return '<span class=\"title\">' + window.ChartSellingReport.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + window.ChartSellingReport.getSerieName() + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                    '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        [
                            'min' => 0,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ]
                        ],
                        [
                            'min' => 0,
                            'max' => 100,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ],
                            'opposite' => true,
                            'visible' => false
                        ],
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() {
                                        var result = (this.pos == window.ChartSellingReport.currDayPos) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (window.ChartSellingReport.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                        if (window.ChartSellingReport.wrapPointPos) {
                                            result += window.ChartSellingReport.getWrapPointXLabel(this.pos);
                                        }

                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'yAxis' => 0,
                            'name' => 'Продажи',
                            'data' => $data,
                            'color' => $color,
                            'borderColor' => $color_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            'pointWidth' => 20,

                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'groupPadding' => 0.05,
                            'pointPadding' => 0.1,
                            'borderRadius' => 3,
                            'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    // MOVE CHART
    window.ChartSellingReport = {
        chart: 'main',
        year: "<?= $searchModel->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: <?= $customOffset ?>
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        //byDebtorPeriod: "<?= $customDebtPeriod ?>",
        project: "<?= $projectId ?>",
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $(document).on('click', '.chart-selling-report-arrow', function() {

                // prevent double-click
                if (window.ChartSellingReport._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartSellingReport.offset[ChartSellingReport.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartSellingReport.offset[ChartSellingReport.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartSellingReport.redrawByClick();
            });

            $('#chart-selling-report-period').on('change', function() {
                //window.ChartSellingReport.byDebtorPeriod = $(this).val();
                window.ChartSellingReport.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartSellingReport._getData().done(function() {
                $('#chart-selling-report').highcharts().update(ChartSellingReport.chartPoints);
                window.ChartSellingReport._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartSellingReport._inProcess = true;
            return $.post('/analytics/selling-report/get-chart-data', {
                    "chart-selling-report-ajax": true,
                    "chart": "main",
                    "period": ChartSellingReport.period,
                    "offset": window.ChartSellingReport.offset[ChartSellingReport.period],
                    "year": ChartSellingReport.year,
                    "project": ChartSellingReport.project,
                    //"debtor-period": ChartSellingReport.byDebtorPeriod,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartSellingReport.freeDays = data.freeDays;
                    ChartSellingReport.currDayPos = data.currDayPos;
                    ChartSellingReport.labelsX = data.labelsX;
                    ChartSellingReport.wrapPointPos = data.wrapPointPos;
                    ChartSellingReport.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-selling-report').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartSellingReport.period == 'months') ? 0.705 : 0.625;

            if (window.ChartSellingReport.wrapPointPos[x + 1]) {
                name = window.ChartSellingReport.wrapPointPos[x + 1].prev;
                left = window.ChartSellingReport.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartSellingReport.wrapPointPos[x]) {
                name = window.ChartSellingReport.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        getSerieName: function() {
            return $('#chart-selling-report-period').find('option:selected').data('chart-title');
        }
    };

    /////////////////////////////////
    window.ChartSellingReport.init();
    /////////////////////////////////

</script>