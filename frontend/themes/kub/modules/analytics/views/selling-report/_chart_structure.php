<?php

use frontend\modules\analytics\models\SellingReportSearch2;
use yii\web\JsExpression;
use frontend\modules\analytics\models\AbstractFinance;

/** @var $searchModel SellingReportSearch2 */

///////////////// page filters ////////////////////
$projectId = (int)$searchModel->project_id ?: null;
///////////////////////////////////////////////////

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($searchModel->year == date('Y') ? date('m') : 12);
$customGroup = $customGroup ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////

///////////////// consts /////////////////
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 10;
$cropNamesLength = 12;
$croppedNameLength = 10;
//////////////////////////////////////////

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$searchModel->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');
$mainData = $searchModel->getStructureChartData($dateFrom, $dateTo, $chartMaxRows);

$categories = array_slice(array_column($mainData, 'title'), 0, $chartMaxRows);
$data = array_slice(array_column($mainData, 'amount'), 0, $chartMaxRows);
$totalAmount = array_sum(array_column($mainData, 'amount'));

if (empty($data)) {
    $categories = [['Нет данных']];
    $data = [[null]];
}

///////////////// calc /////////////////
$calculatedChartHeight = count($data) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
//////////////////////////////////////////

if (Yii::$app->request->post('chart-selling-report-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'chartHeight' => $calculatedChartHeight,
        'totalAmount' => $totalAmount,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $categories,
            ],
            'series' => [
                [
                    'data' => $data,
                ]
            ],
        ],
    ]);

    exit;
}

$chartTitle = 'ТОП-10 ПРОДАЖ';
$tooltipSubtitle = 'Доля в продажах';
?>
<style>
    #chart-selling-report-structure { height: auto; }
    #chart-selling-report-structure .highcharts-container { z-index: 2!important; }
</style>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            <?= $chartTitle ?>
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'hideSearch' => true,
                    'id' => 'chart-selling-report-structure-month',
                    'name' => 'chart-selling-report-structure-month',
                    'data' => array_filter(array_reverse(AbstractFinance::$month, true),
                        function($k) use ($searchModel) {
                            return ($searchModel->year < date('Y') || (int)$k <= date('n'));
                        }, ARRAY_FILTER_USE_KEY),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'value' => $customMonth,
                    'pluginOptions' => [
                        'width' => '106px',
                        'containerCssClass' => 'select2-balance-structure'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-selling-report-structure',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ],
                        'marginTop' => '32',
                        'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {

                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var totalAmount = (ChartSellingReportStructure.totalAmount > 0) ? ChartSellingReportStructure.totalAmount : 9E9;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' +
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">{$tooltipSubtitle}: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[0].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                                    '</table>';


                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('
                            function (boxWidth, boxHeight, point) {
                                var x = this.chart.containerWidth - boxWidth;
                                var y = point.plotY + 50;
                                //console.log(x)
                                return {x: x, y: y};
                            }
                        '),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'showInLegend' => false,
                            'name' => 'Сумма',
                            'pointPadding' => 0,
                            'data' => $data,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    ChartSellingReportStructure = {
        chart: 'structure',
        isLoaded: false,
        year: "<?= $searchModel->year ?>",
        month: "<?= $customMonth ?>",
        chartPoints: {},
        chartHeight: "<?= $calculatedChartHeight ?>",
        totalAmount: "<?= $totalAmount ?>",
        project: "<?= $projectId ?>",
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('#chart-selling-report-structure-month').on('change', function() {

                // prevent double-click
                if (ChartSellingReportStructure._inProcess) {
                    return false;
                }

                ChartSellingReportStructure.year = $('#sellingreportsearch2-year').val();
                ChartSellingReportStructure.month = $(this).val();
                ChartSellingReportStructure.redrawByClick();
            });

        },
        redrawByClick: function() {
            return ChartSellingReportStructure._getData().done(function() {
                const $chart = $('#chart-selling-report-structure').highcharts();
                $chart.update(ChartSellingReportStructure.chartPoints);
                $chart.update({"chart": {"height": ChartSellingReportStructure.chartHeight + 'px'}});
                ChartSellingReportStructure._inProcess = false;
            });
        },
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': '<?= $color1 ?>',
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        resetPoints: function() {
            var chart = $('#chart-selling-report-structure').highcharts();
            chart.series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color1 ?>'
                });
            });
        },
        _getData: function() {
            ChartSellingReportStructure._inProcess = true;
            return $.post('/analytics/selling-report/get-chart-data', {
                    "chart-selling-report-ajax": true,
                    "chart": ChartSellingReportStructure.chart,
                    "year": ChartSellingReportStructure.year,
                    "month": ChartSellingReportStructure.month,
                    "group": ChartSellingReportStructure.byGroup,
                    "project": ChartSellingReportStructure.project,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartSellingReportStructure.chartPoints = data.optionsChart;
                    ChartSellingReportStructure.chartHeight = data.chartHeight;
                    ChartSellingReportStructure.totalAmount = data.totalAmount;
                }
            );
        },
    };

    ChartSellingReportStructure.init();
</script>