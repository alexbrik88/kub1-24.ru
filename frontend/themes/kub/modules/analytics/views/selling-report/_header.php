<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use frontend\themes\kub\helpers\Icon;

$showHelpPanel = $userConfig->selling_speed_help ?? false; // todo
$showChartPanel = $userConfig->selling_speed_chart ?? false; // todo
?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="max-height: 44px">
                <?= Html::beginForm(Url::current(), 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="selling_speed_chart">
    <div class="pt-3 pb-3 mb-2">
        <div class="row mb-4">
            <div class="col-9 pr-2" style="z-index: 2">
                <?php
                // Charts
                echo $this->render('_chart_main', [
                    'searchModel' => $searchModel,
                ]);
                ?>
            </div>
            <div class="col-3 pl-2" style="z-index: 1">
                <?php
                // Charts
                echo $this->render('_chart_structure', [
                    'searchModel' => $searchModel,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="selling_speed_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_help', [])
        ?>
    </div>
</div>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>