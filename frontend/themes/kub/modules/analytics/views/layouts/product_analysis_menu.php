<?php
$controller =  Yii::$app->controller->id;
$action =  Yii::$app->controller->action->id;
?>
<div class="nav-tabs-row mb-2 pb-1">
    <?php

    use frontend\rbac\UserRole;
    use yii\bootstrap4\Nav;

    echo Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'Дашборд',
                'url' => ['product-analysis/dashboard'],
                'active' => $action == 'dashboard',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'ABC анализ',
                'url' => ['product-analysis/abc'],
                'active' => $action == 'abc',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'XYZ анализ',
                'url' => ['product-analysis/xyz'],
                'active' => $action == 'xyz',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
        ],
    ]);
    ?>
</div>