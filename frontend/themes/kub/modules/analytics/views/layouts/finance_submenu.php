<?php
$controller =  Yii::$app->controller->id;
$action =  Yii::$app->controller->action->id;
?>
<div class="nav-tabs-row mb-2">
    <?php

    use frontend\rbac\UserRole;
    use yii\bootstrap4\Nav;

    echo Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'По клиентам',
                'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ? ['/analytics/analysis/index'] : ['/analytics/debt-report/debtor'],
                'active' => in_array($controller, ['debt-report', 'analysis', 'discipline']),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'По продажам',
                'url' => ['/analytics/selling-report/index'],
                'active' => $controller == 'selling-report',
                'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_SUPERVISOR),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'По счетам',
                'url' => ['/analytics/invoice-report/created'],
                'active' => $controller == 'invoice-report',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'По поставщикам',
                'url' => ['/analytics/debt-report-seller/debtor'],
                'active' => $controller == 'debt-report-seller',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
            [
                'label' => 'По сотрудникам',
                'url' => ['/analytics/employees/index'],
                'active' => $controller == 'employees',
                'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link']
            ],
        ],
    ]);
    ?>
</div>