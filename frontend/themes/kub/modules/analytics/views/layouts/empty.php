<?php

use frontend\themes\kub\assets\KubAsset;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php $this->head(); ?>

<?php $this->beginBody() ?>

<?php echo $content ?>

<?php $this->endBody(); ?>

<?php $this->endPage(); ?>
