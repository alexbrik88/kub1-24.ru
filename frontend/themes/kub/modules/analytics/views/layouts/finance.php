<?php

use common\models\balance\BalanceArticle;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

$type = Yii::$app->request->get('type');

$showAddButton = !(
    (Yii::$app->controller->id === 'balance-articles' && Yii::$app->controller->action->id === 'index') ||
    (Yii::$app->controller->id === 'finance' && Yii::$app->controller->action->id == 'loans') ||
    (Yii::$app->controller->id == 'articles')
);
?>

<?php if ($showAddButton) {
    echo $this->render('@frontend/modules/cash/views/default/modal_upload_statements', ['company' => Yii::$app->user->identity->company]);
    echo $this->render('_add_all_button');
    echo OfdModalWidget::widget();
    echo BankingModalWidget::widget();
    echo ImportDialogWidget::widget();
} ?>

<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2">
        <?php
        if (in_array(Yii::$app->controller->action->id, ['debtor', 'loans'])) {
            // REVENUE CONTROL
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Нам должны',
                        'url' => ['/analytics/finance/debtor', 'type' => Documents::IO_TYPE_OUT],
                        'active' => Yii::$app->controller->action->id == 'debtor' && $type == Documents::IO_TYPE_OUT,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Мы должны',
                        'url' => ['/analytics/finance/debtor', 'type' => Documents::IO_TYPE_IN],
                        'active' => Yii::$app->controller->action->id == 'debtor' && $type == Documents::IO_TYPE_IN,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Кредиты',
                        'url' => ['/analytics/finance/loans'],
                        'active' => Yii::$app->controller->action->id == 'loans',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);
        } elseif (in_array(Yii::$app->controller->action->id, ['balance', 'balance-initial', 'stocks', 'leasing']) || Yii::$app->controller->id == 'balance-articles') {
            // BALANCE
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Баланс',
                        'url' => ['/analytics/finance/balance'],
                        'active' => in_array(Yii::$app->controller->action->id, ['balance', 'balance-initial']),
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Запасы',
                        'url' => ['/analytics/finance/stocks'],
                        'active' => Yii::$app->controller->action->id == 'stocks',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Основные средства',
                        'url' => ['/analytics/balance-articles/index', 'type' => BalanceArticle::TYPE_FIXED_ASSERTS, 'layout' => 2],
                        'active' => Yii::$app->controller->id === 'balance-articles'
                            && Yii::$app->controller->action->id === 'index'
                            && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_FIXED_ASSERTS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'НМА',
                        'url' => ['/analytics/balance-articles/index', 'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS, 'layout' => 2],
                        'active' => Yii::$app->controller->id === 'balance-articles'
                            && Yii::$app->controller->action->id === 'index'
                            && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_INTANGIBLE_ASSETS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);

        } elseif (in_array(Yii::$app->controller->action->id, ['profit-and-loss', 'break-even-point', 'operational-efficiency'])) {
            // EFFECTIVENESS
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'ОПиУ (P&L)',
                        'url' => ['/analytics/finance/profit-and-loss'],
                        'active' => Yii::$app->controller->action->id == 'profit-and-loss',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Точка безубыточности',
                        'url' => ['/analytics/finance/break-even-point'],
                        'active' => Yii::$app->controller->action->id == 'break-even-point',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Показатели эффективности',
                        'url' => ['/analytics/finance/operational-efficiency'],
                        'active' => Yii::$app->controller->action->id == 'operational-efficiency',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);
        } else {
            // STADNDART MENU
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'ОДДС',
                        'url' => ['/analytics/finance/odds'],
                        'active' => Yii::$app->controller->action->id == 'odds',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Платёжный календарь',
                        'url' => ['/analytics/finance/payment-calendar'],
                        'active' => Yii::$app->controller->action->id == 'payment-calendar',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'План-Факт',
                        'url' => ['/analytics/finance/plan-fact'],
                        'active' => Yii::$app->controller->action->id == 'plan-fact',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Приходы',
                        'url' => ['/analytics/finance/income'],
                        'active' => Yii::$app->controller->action->id == 'income',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Расходы',
                        'url' => ['/analytics/finance/expenses'],
                        'active' => Yii::$app->controller->action->id == 'expenses',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Статьи операций',
                        'url' => ['/analytics/articles/index', 'type' => 2],
                        'active' => Yii::$app->controller->id == 'articles',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Заявки на платеж',
                        'url' => ['/analytics/plan-payments-request/index'],
                        'active' => Yii::$app->controller->id == 'plan-payments-request',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ]
                ],
            ]);
        }
        ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>