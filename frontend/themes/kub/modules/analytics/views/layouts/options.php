<?php

use \yii\widgets\Menu;

?>

<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<div class="business-analytics">
    <div class="menu-nav-tabs nav-tabs-row pb-3 mb-1">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 justify-content-around',
            ],
            'itemOptions' => [
                'class' => 'nav-item pl-2 pr-2 d-flex flex-column',
                'style' => 'flex: 1 1 0;',
            ],
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => 'Ключевые <br> возможности',
                    'url' => ['/analytics/options/start'],
                    'template' => $this->render('_options_step_link', ['i' => 'rocket']),
                ],
                [
                    'label' => 'Моя <br> компания',
                    'url' => ['/analytics/options/company'],
                    'template' => $this->render('_options_step_link', ['i' => 'bag']),
                ],
                [
                    'label' => 'Уровни <br> бизнес аналитики',
                    'url' => ['/analytics/options/levels'],
                    'template' => $this->render('_options_step_link', ['i' => 'analytics-levels']),
                ],
                [
                    'label' => 'Структура <br> компании',
                    'url' => ['/analytics/options/company-structure'],
                    'template' => $this->render('_options_step_link', ['i' => 'diagram-custom']),
                ],
                //[
                //    'label' => 'Выбор <br> отчетов',
                //    'url' => ['/analytics/options/reports'],
                //    'template' => $this->render('_options_step_link', ['i' => 'checked-items']),
                //],
                [
                    'label' => 'Каталог <br> интеграций',
                    'url' => ['/analytics/options/statements'],
                    'template' => $this->render('_options_step_link', ['i' => 'download-data']),
                ],
                //[
                //    'label' => 'Уведомления',
                //    'url' => ['/analytics/options/notifications'],
                //    'template' => $this->render('_options_step_link', ['i' => 'mobile-notif']),
                //],
                //[
                //    'label' => 'Реферальная <br> программа',
                //    'url' => ['/analytics/options/referrals'],
                //    'template' => $this->render('_options_step_link', ['i' => 'ref-network']),
                //],
            ],
        ]); ?>
    </div>

    <?= $content ?>
</div>

<?php $this->endContent(); ?>
