<?php

use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

// todo:temp
$visibleAnalyticsDevModule = (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [112, 486, 628, 11270, 23083, 53146]));

?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2">
        <?php
        if (in_array(Yii::$app->controller->action->id, ['what-if', 'scenario'])) {
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Анализ «Что, если…»',
                        'url' => ['/analytics/planning/what-if'],
                        'active' => Yii::$app->controller->action->id == 'what-if',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Сценарный анализ',
                        'url' => ['/analytics/planning/scenario'],
                        'active' => Yii::$app->controller->action->id == 'scenario',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);
        }
        ?>
    </div>
    <div class="planning-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>