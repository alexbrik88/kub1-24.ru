<?php

use yii\helpers\Url;
use yii\bootstrap\Nav;
use \frontend\modules\analytics\models\DebtReportSearch2;

/** @var $reportType */

echo Nav::widget([
    'id' => 'debtor-submenu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-3'],
    'items' => [
        [
            'label' => 'Деньги',
            'url' => Url::current(['report' => DebtReportSearch2::REPORT_TYPE_MONEY, 'search_by' => null]),
            'active' => Yii::$app->controller->action->id == 'debtor' && $reportType == DebtReportSearch2::REPORT_TYPE_MONEY,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'Обязательства',
            'url' => Url::current(['report' => DebtReportSearch2::REPORT_TYPE_GUARANTY, 'search_by' => null]),
            'active' => Yii::$app->controller->action->id == 'debtor' && $reportType == DebtReportSearch2::REPORT_TYPE_GUARANTY,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'Итого',
            'url' => Url::current(['report' => DebtReportSearch2::REPORT_TYPE_NET_TOTAL, 'search_by' => null]),
            'active' => Yii::$app->controller->action->id == 'debtor' && $reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
    ],
]);