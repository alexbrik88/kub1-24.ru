<?php

use common\models\balance\BalanceArticle;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use frontend\modules\analytics\controllers\DetailingController;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

$tab = Yii::$app->request->get('tab', DetailingController::TAB_INDUSTRY);

$dropItems = [
    [
        'label' => 'Направление',
        'url' => 'javascript:void(0)',
        'linkOptions' => [
            'class' => 'add-modal-new-industry dropdown-item',
        ]
    ],
    [
        'label' => 'Точка продаж',
        'url' => 'javascript:void(0)',
        'linkOptions' => [
            'data-toggle' => 'modal',
            'data-target' => '#add-sale-point-menu'
        ]
    ],
    [
        'label' => 'Проект',
        'url' => '/project/create',
    ]
];
?>

<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-3">
        <div class="row">
        <div class="column">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Направления',
                    'url' => ['/analytics/detailing/index', 'tab' => DetailingController::TAB_INDUSTRY],
                    'active' => $tab == DetailingController::TAB_INDUSTRY,
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link', 'data-pjax' => 0],
                ],
                [
                    'label' => 'Точки продаж',
                    'url' => ['/analytics/detailing/index', 'tab' => DetailingController::TAB_SALE_POINT],
                    'active' => $tab == DetailingController::TAB_SALE_POINT,
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link', 'data-pjax' => 0],
                ],
                [
                    'label' => 'Проекты',
                    'url' => ['/analytics/detailing/index', 'tab' => DetailingController::TAB_PROJECT],
                    'active' => $tab == DetailingController::TAB_PROJECT,
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link', 'data-pjax' => 0],
                ],
            ],
        ]);
        ?>
        </div>
        <div class="column ml-auto">
            <div class="dropdown pl-0 pr-0">
                <?= Html::button(Icon::get('add-icon') . '<span class="mr-3 ml-2">Добавить</span>' . '<span style="margin-top: 0;">' . Icon::get('shevron') . '</span>',
                    ['class' => 'button-regular button-regular_padding_medium button-regular_red button-clr', 'data-toggle' => 'dropdown']); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'id' => 'add-all-button',
                        'class' => 'dropdown-menu form-filter-list list-clr dropdown-w-100 text-center',
                        'style' => 'z-index: 1003'
                    ],
                    'items' => $dropItems,
                ]); ?>
            </div>
        </div>
        </div>
    </div>
    <div class="detailing-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>