<?php

use common\models\balance\BalanceArticle;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use frontend\modules\analytics\controllers\DetailingController;

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

$tab = Yii::$app->request->get('tab', DetailingController::TAB_INDUSTRY);
?>

<div class="debt-report-content nav-finance">
    <div class="detailing-item finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>