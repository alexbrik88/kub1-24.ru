<?php

use common\models\cash\CashBankFlows;
use frontend\rbac\permissions\Cash;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

$canCreate = (Yii::$app->user) ? Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE) : false;

?>
<div id="analytics-create-all-buttons" class="page-head d-flex flex-wrap align-items-center">
    <?= $this->render('@frontend/modules/cash/views/default/_partial/create-buttons', [
        'canCreate' => $canCreate
    ]) ?>
</div>


