<?php

use yii\helpers\Url;

$subTab = $subTab ?? '-';

echo \yii\bootstrap4\Nav::widget([
    'id' => 'payment-calendar-menu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-2'],
    'items' => [
        [
            'label' => 'Платежный календарь',
            'url' => Url::to(['/analytics/finance/payment-calendar', 'subTab' => 'table', 'activeTab' => null, 'PaymentCalendarSearch' => null]),
            'active' => !$subTab || $subTab == 'table',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'Реестр плановых платежей',
            'url' => Url::to(['/analytics/finance/payment-calendar', 'subTab' => 'register', 'activeTab' => null, 'PaymentCalendarSearch' => null]),
            'active' => $subTab == 'register',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'АвтоПланирование',
            'url' => Url::to(['/analytics/finance/payment-calendar', 'subTab' => 'autoplan', 'activeTab' => null, 'PaymentCalendarSearch' => null]),
            'active' => $subTab == 'autoplan',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
    ],
]);
