<?php
use frontend\modules\analytics\models\DebtReportSearch2;
use yii\bootstrap4\Nav;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var $searchModel */
/** @var $searchBy */
/** @var $reportType */

if ($reportType == DebtReportSearch2::REPORT_TYPE_MONEY) {

    echo Nav::widget([
    'id' => 'debtor-sub-submenu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-3'],
    'items' => [
        [
            'label' => 'По обязательствам',
            'url' => Url::current(['search_by' => DebtReportSearch2::SEARCH_BY_DOCUMENTS]),
            'active' => $searchBy == DebtReportSearch2::SEARCH_BY_DOCUMENTS,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'По счетам',
            'url' => Url::current(['search_by' => DebtReportSearch2::SEARCH_BY_INVOICES]),
            'active' => $searchBy == DebtReportSearch2::SEARCH_BY_INVOICES,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'По срокам',
            'url' => Url::current(['search_by' => DebtReportSearch2::SEARCH_BY_DEBT_PERIODS]),
            'active' => $searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS,
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
    ],
]);

}