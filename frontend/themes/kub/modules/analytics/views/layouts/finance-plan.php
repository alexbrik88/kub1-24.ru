<?php
use \frontend\modules\analytics\assets\AnalyticsAsset;
use \frontend\modules\analytics\assets\FinanceModelAsset;

AnalyticsAsset::register($this);
FinanceModelAsset::register($this);

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

?>
<div class="finance-model-index">
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>