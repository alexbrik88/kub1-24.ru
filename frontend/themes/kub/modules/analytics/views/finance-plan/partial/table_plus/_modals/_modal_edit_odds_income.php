<?php

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem as IncomeItem;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\ArrayHelper;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$item = new IncomeItem;
$item->relation_type = IncomeItem::RELATION_TYPE_UNSET;
$item->action = null;
?>

<div id="edit-odds-income-item-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-edit-odds-income',
            'action' => Url::to(['edit-odds-item-ajax', 'group' => $model->id, 'plan' => $activePlan->id, 'io_type' => FinancePlan::ODDS_IO_TYPE_INCOME]),
            'method' => 'POST',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ]
        ]);
        ?>

        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('row_id', null, ['class' => 'row_id']); ?>
        <?= Html::hiddenInput('row_type', null, ['class' => 'row_type']); ?>
        <?= Html::hiddenInput('is_deleted', null, ['class' => 'is_deleted']); ?>

        <div style="display: flex">

            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Добавить строку
                </span>
                <div class="toggled-element">
                    <?= $form->field($item, 'income_item_id')->widget(ExpenditureDropdownWidget::class, [
                        'income' => true,
                        'loadAssets' => false,
                        'exclude' => [
                            'items' => array_filter(
                                ArrayHelper::getColumn($activePlan->oddsIncomeItems, 'income_item_id'), function ($incomeItemId) {
                                    return !in_array($incomeItemId, FinancePlan::SYSTEM_INCOME_ITEMS_IDS);
                                }
                            )
                        ],
                        'options' => [
                            'id' => 'odds-income_item_id',
                            'class' => 'flow-expense-items',
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'placeholder' => '',
                        ]
                    ]); ?>
                    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                        'inputId' => 'odds-income_item_id',
                        'type' => 'income'
                    ]) ?>                    
                </div>
                <div class="toggled-element" style="display: none">
                    <?= Html::input('text', null, 'odds_exists_income_item_id', [
                        'id' => 'odds-exists_income_item_id',
                        'class' => 'form-control',
                        'disabled' => true,
                    ]) ?>
                </div>
            </div>
            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px; ">
                <?= $form->field($item, 'relation_type')->hiddenInput() ?>
                <?= $form->field($item, 'action')->hiddenInput() ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-odds-income-js button-clr button-width button-regular button-hover-transparent ml-1 mr-2',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-odds-income-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php Modal::begin([
    'id' => 'confirm-delete-odds-income',
    'title' => 'Вы уверены, что хотите удалить эту строку?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'id' => 'confirm-delete-odds-income-js',
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>