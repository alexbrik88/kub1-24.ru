<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem as OddsIncomeItem;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$item = new OddsIncomeItem;
?>

<div id="edit-odds-system-income-item-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-edit-odds-system-income',
            'action' => Url::to(['edit-odds-system-item-ajax', 'group' => $model->id, 'plan' => $activePlan->id, 'io_type' => FinancePlan::ODDS_IO_TYPE_INCOME]),
            'method' => 'POST',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ]
        ]);
        ?>

        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('row_id', null, ['class' => 'row_id']); ?>
        <?= Html::hiddenInput('row_type', null, ['class' => 'row_type']); ?>
        <?= Html::hiddenInput('is_deleted', null, ['class' => 'is_deleted']); ?>

        <div style="display: flex">

            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Добавить строку
                </span>
                <?= Html::textInput('odds-system-income-item-name', 'Оплата от покупателя (предоплата)', [
                    'class' => 'toggle-name form-control',
                    'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT => 'Оплата от покупателя (предоплата)',
                    'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE => 'Оплата от покупателя (отсрочка)',
                    'disabled' => true,
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Зависит от строки
                </span>
                <?= Html::textInput('odds-system-income-item-relation_type', 'Выручка', [
                    'class' => 'form-control',
                    'disabled' => true,
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px; position: relative">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    <?= Html::tag('span', '% предоплаты', [
                        'class' => 'toggle-action-label',
                        'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT => '% предоплаты',
                        'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE => '% отсрочки',
                    ]) ?>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'toggle-action-tooltip tooltip2',
                        'title' => 'Укажите процент предоплаты от покупателей',
                        'title-as-html' => 1,
                        'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT => 'Укажите процент предоплаты от покупателей',
                        'data-payment_type-' . FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE => 'Укажите процент отсрочки от покупателей',
                    ]) ?>
                </span>
                <?= $form->field($item, 'action', [
                    'options' => [
                        'style' => 'width: 100%;',
                        'autocomplete' => 'off',
                    ],
                ])->textInput([
                    'id' => 'odds-system-income-item-action',
                    'class' => 'action-input form-control'
                ])
                ?>
            </div>
            <div class="_p_1 toggle-field" style="width: 10%; min-width: 150px; margin-right: 16px; ">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Отсрочка на
                </span>
                <?= $form->field($item, 'payment_delay')->widget(Select2::class, [
                    'hideSearch' => true,
                    'data' => FinancePlanOddsItem::$paymentDelayList,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                    'options' => [
                        'class' => 'payment_delay-select form-control'
                    ]
                ]) ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-odds-system-income-js button-clr button-width button-regular button-hover-transparent ml-1 mr-2',
                'style' => 'width: 130px!important;',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>