<?php

use yii\bootstrap4\Html;
use common\models\employee\Config;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;

/**
 * @var string $tab
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 * @var FinancePlanChart $modelChart
 * @var Config $userConfig
 */

$editablePlanId = ($activePlan->id === FinancePlanGroup::PLAN_TOTAL_ID)
    ? $activePlan->modelGroup->financePlans[0]->id
    : $activePlan->id;


$updateUrl = ($tab == FinancePlanGroup::TAB_EXPENSES)
    ? ['update-expenses', 'group' => $model->id]
    : ['update-plan', 'group' => $model->id, 'plan' => $editablePlanId];

$showChartPanel = $userConfig->finance_plan_view_chart;
$showHelpPanel = $userConfig->finance_plan_view_help;
?>

<?php if ($IS_EDIT_MODE): ?>

<div class="wrap wrap_padding_small pl-2 pr-3 pb-2 mb-2">
    <h4 class="column mb-2" style="max-width: 550px;">
        Редактирование: <?= htmlspecialchars($model->name); ?>
    </h4>
</div>

<?php else: ?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="page-in row">
            <div class="col-12 column">
                <div class="row align-items-center">
                    <h4 class="column mb-2" style="max-width: 550px;">
                        <?= htmlspecialchars($model->name); ?>
                    </h4>
                    <div class="column ml-auto pl-2 pr-1">
                        <?= Html::button(\frontend\themes\kub\helpers\Icon::get('diagram'),
                            [
                                'id' => 'btnChartCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#chartCollapse',
                                'data-tooltip-content' => '#tooltip_chart_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>
                    <div class="column pl-2 pr-1">
                        <?= Html::button(Icon::get('book'),
                            [
                                'id' => 'btnHelpCollapse',
                                'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                                'data-toggle' => 'collapse',
                                'href' => '#helpCollapse',
                                'data-tooltip-content' => '#tooltip_help_collapse',
                                'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                            ]) ?>
                    </div>
                    <div class="column pl-2 pr-1">
                        <?= Html::button(SpriteIconWidget::widget(['icon' => 'pencil']), [
                            'title' => 'Редактировать',
                            'class' => 'button-regular button-regular_red button-clr w-44 mb-2',
                            'data-toggle' => 'modal',
                            'data-target' => '#add-finance-model-group'
                        ]) ?>
                    </div>
                    <div class="column pl-2 pr-0">
                        <?= Html::button(Icon::get('add-icon', ['class' => 'mr-1']) . '<span>Направление</span>', [
                            'title' => 'Добавить направление',
                            'class' => 'button-regular button-regular_red mb-2',
                            'data-toggle' => 'modal',
                            'data-target' => '#add-finance-plan'
                        ]) ?>
                    </div>
                </div>
                <div class="jsSaveStateCollapse collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="finance_plan_view_help">
                    <div class="row mt-3" style="min-width: 550px;">
                    <div class="column mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Начало
                        </div>
                        <div style="text-transform: capitalize">
                            <?= Yii::$app->formatter->asDate($model->dateFrom, 'LLLL Y') ?>
                        </div>
                    </div>
                    <div class="column mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Окончание
                        </div>
                        <div style="text-transform: capitalize">
                            <?= Yii::$app->formatter->asDate($model->dateTill, 'LLLL Y') ?>
                        </div>
                    </div>
                    <div class="column mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Период
                        </div>
                        <div>
                            <?= $model->period ?> мес.
                        </div>
                    </div>
                    <div class="column mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Валюта
                        </div>
                        <div>
                            <?= htmlspecialchars($model->getCurrencyName()) ?>
                        </div>
                    </div>
                    <div class="column mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Комментарий
                        </div>
                        <div>
                            <?= htmlspecialchars($model->comment) ?>
                        </div>
                    </div>
                    <div class="column ml-auto mb-4 pb-2">
                        <div class="label weight-700 mb-3">
                            Тип основного направления бизнеса
                        </div>
                        <div>
                            <?= htmlspecialchars($model->mainFinancePlan ? $model->mainFinancePlan->industryType->name : '---') ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="finance_plan_view_chart">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_chart', [
            'model' => $modelChart
        ]); ?>
    </div>
</div>

<?php endif; ?>

<div class="table-settings row row_indents_s mt-2 pt-1">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'finance_model_round_numbers', 'refresh-page' => true],
                ['subtitle' => 'Разделы'],
                ['attribute' => 'finance_model_other_income'],
                ['attribute' => 'finance_model_other_expense'],
                ['attribute' => 'finance_model_amortization'],
                ['attribute' => 'finance_model_ebit'],
                ['attribute' => 'finance_model_percent_received'],
                ['attribute' => 'finance_model_percent_paid'],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_finance_plan']) ?>
    </div>
    <div class="col-6">
        <div class="row align-items-center justify-content-end">
            <div class="column">
                <?= Html::a(SpriteIconWidget::widget(['icon' => 'pencil']), $updateUrl, [
                    'title' => 'Редактировать направление',
                    'class' => 'button-regular button-regular_red button-clr w-44',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        const ebitDefaultValue = '<?= ($userConfig->finance_model_amortization) ? '' : '1' ?>';
        const ebitCheckbox = $('#config-finance_model_ebit');
        ebitCheckbox.prop('disabled', Boolean(ebitDefaultValue));
    });
</script>
