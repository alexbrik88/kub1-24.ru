<?php

use frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\company\CompanyInfoIndustry;
use common\models\currency\Currency;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/** @var FinancePlanGroup $model */

$from = $model->dateFrom ? date_create_from_format('Y-m-d', $model->dateFrom) : (new DateTime());
$till = $model->dateTill ? date_create_from_format('Y-m-d', $model->dateTill) : (new DateTime())->modify('+11 month');

$industryList = ArrayHelper::map(CompanyInfoIndustry::find()
    ->select(['id', 'name'])
    ->orderBy(new Expression('IF (id = '.CompanyInfoIndustry::OTHER.', 1, 0), name'))
    ->asArray()
    ->all(), 'id', 'name');
$currencyList = ArrayHelper::map(Currency::find()
    ->select(new Expression('id, CONCAT(label, ", ", name) name'))
    ->orderBy(new Expression('IF (id = 643, 0, 1), label'))
    ->andWhere(['not', ['id' => 974]])
    ->asArray()
    ->all(), 'id', 'name');

$additionalPlans = $model->getFinancePlans()->andWhere(['is_main' => 0])->all();

$helpIcon = [
    'industry' => Html::tag('span', Icon::QUESTION, [
        'class' => 'tooltip3',
        'data-tooltip-content' => '#tooltip_industry_help',
    ])
];

$form = ActiveForm::begin([
    'id' => 'form-finance-model',
    'action' => $model->id ? Url::to(['update', 'id' => $model->id]) : Url::to(['create']),
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
    <?php if ($model->id): ?>
        <?= Html::hiddenInput('is-update-confirmed', null, ['id' => 'is-update-confirmed']); ?>
        <?= Html::hiddenInput('old-date-from', $from->format('Y-m-01'), ['id' => 'old-date-from']); ?>
        <?= Html::hiddenInput('old-date-till', $till->format('Y-m-01'), ['id' => 'old-date-till']); ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'name'); ?>
                </div>
                <div class="col-6"></div>
            </div>
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'textFrom', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                            'value' => Yii::$app->formatter->asDate($from->format('Y-m-01'), 'LLLL Y'),
                            'class' => 'form-control field-datepicker field-from-datepicker ico',
                            'style' => 'text-transform: capitalize;',
                            'autocomplete' => 'off',
                            'readonly' => true
                        ])->label('Месяц начала'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'dateFrom')
                            ->hiddenInput(['value' => $from->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'textTill', [
                        'labelOptions' => [
                            'class' => 'label bold-text',
                        ],
                        'options' => [
                            'class' => 'form-group',
                            'required' => 'required',
                        ],
                    ])->textInput([
                            'value' => Yii::$app->formatter->asDate($till->format('Y-m-01'), 'LLLL Y'),
                            'class' => 'form-control field-datepicker field-to-datepicker ico',
                            'style' => 'text-transform: capitalize;',
                            'autocomplete' => 'off',
                            'readonly' => true
                        ])->label('Месяц окончания'); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'dateTill')
                            ->hiddenInput(['value' => $till->format('Y-m-01')])
                            ->label(false); ?>
                    </div>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'currency_id')->widget(Select2::class, [
                        'data' => $currencyList,
                        'options' => [
                            'disabled' => false,
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                    ])->label('Валюта') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'mainIndustryName')
                        ->textInput()
                        ->label('Название основного направления бизнеса'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'mainIndustryId')->widget(Select2::class, [
                        'data' => $industryList,
                        'options' => [
                            'disabled' => !$model->isNewRecord
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                    ])->label('Тип основного направления бизнеса' . $helpIcon['industry']) ?>
                    <?php if (!$model->isNewRecord): ?>
                        <?= $form->field($model, 'mainIndustryId')->hiddenInput()->label(false) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ($additionalPlans): ?>
            <div class="col-12" style=" position: relative">
                <table id="form-additional-plans" class="table table-style table-count-list table-bleak table-no-border" style="table-layout: fixed; position: relative">
                    <thead>
                        <tr>
                            <th width="50%" style="padding-left:0; padding-right:15px; font-weight: normal">Название дополнительного направления</th>
                            <th style="padding-left:15px; padding-right:15px; font-weight: normal">Тип дополнительного направления<?= $helpIcon['industry'] ?></th>
                            <th width="55px"></th>
                        </tr>
                    </thead>
                    <tbody id="form-additional-plans-body">
                        <?php foreach ($additionalPlans as $plan): ?>
                        <tr class="plan<?=($plan->id)?>">
                            <td style="padding-left:0; padding-right:15px">
                                <?= $form->field($plan, 'name', [
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])
                                ->textInput()
                                ->label(false)
                                ?>
                            </td>
                            <td style="padding-left:15px; padding-right:15px">
                                <?= $form->field($plan, 'industry_id', [
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->widget(Select2::class, [
                                    'data' => $industryList,
                                    'options' => [
                                        'disabled' => true
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%',
                                    ],
                                ])->label(false) ?>
                            </td>
                            <td class="nowrap text-center">
                                <div style="max-width: 45px;">
                                    <?php if (count($additionalPlans) > 1): ?>
                                        <button class="sortable-row-icon button-clr" type="button" title="Переместить направление">
                                            <svg class="table-count-icon table-count-icon_small svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                            </svg>
                                        </button>
                                    <?php endif; ?>
                                    <button class="remove-additional-plan button-clr" type="button" data-plan_id="<?=($plan->id)?>" title="Удалить направление">
                                        <svg class="table-count-icon svg-icon">
                                            <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                                        </svg>
                                    </button>
                                </div>
                                <?= $form->field($plan, 'sort', [
                                    'template' => '{input}',
                                    'options' => [
                                        'class' => ''
                                    ],
                                    'inputOptions' => [
                                        'class' => 'sort-input',
                                    ],
                                ])->hiddenInput()->label(false) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>

        <div class="col-12">
            <?= $form->field($model, 'comment')->textarea([
                'placeholder' => '',
                'rows' => '3']); ?>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

    <div style="display:none;">
        <div id="tooltip_industry_help">
            В зависимости от выбранного,<br/>
            будет использован соответствующий<br/>
            шаблон финмодели
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php

$jsFrom = $from->format('Y').','.($from->format('m')-1).',1';
$jsTill = $till->format('Y').','.($till->format('m')-1).',1';

$this->registerJs(<<<JS

    $(".remove-additional-plan").on('click', function() {
        $('#confirm-delete-plan-yes').data('deleted_plan_id', $(this).data('plan_id'));
        $('#confirm-delete-plan').modal('show');
    });

    $("#confirm-delete-plan-yes").on('click', function() {
        const form = $('#form-finance-model');
        const table = $('#form-additional-plans');
        const deleted_plan_id = $(this).data('deleted_plan_id'); 
        table.find('tr.plan' + deleted_plan_id).remove();
        if (table.find('tbody > tr').length === 0)
            table.remove();

        form.append('<input type="hidden" name="removedPlans[]" value="'+deleted_plan_id+'"></input>');

        $('#confirm-delete-plan').modal('hide');
    });

    $("#form-additional-plans-body").sortable({
        containerSelector: "table",
        handle: ".sortable-row-icon",
        cancel: "",
        itemPath: "> tbody",
        itemSelector: "tr",
        placeholder: "<tr class=\"placeholder\"/>",
        onDrag: function (item, position, _super, event) {
            position.left -= 45;
            position.top -= 25;
            item.css(position);
        },
        start: function(event, ui) {
            //
        },
        stop: function(event, ui) {
            let sort = 1;
            $(this).find('.sort-input').each(function(i,input) {
                $(input).val(++sort);
            });
        }
    });

    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear();
      var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1);
      var dd = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
      return "".concat(yyyy).concat('-').concat(mm).concat('-').concat(dd);
    };

    const DatePickerFrom = $('.field-from-datepicker');
    const DatePickerTo = $('.field-to-datepicker');
    let datepickerConfig = kubDatepickerConfig;
    let datepickerConfigFrom;
    let datepickerConfigTo;
    
    datepickerConfig.view = 'months';
    datepickerConfig.minView = 'months';
    datepickerConfig.dateFormat = 'MM yyyy';
    datepickerConfig.language = 'ru';
    datepickerConfigFrom = Object.assign({}, datepickerConfig);    
    datepickerConfigFrom.onSelect = function(formattedDate, date) {
        $('#financeplangroup-datefrom').val(date.yyyymmdd());
    };
    datepickerConfigTo = Object.assign({}, datepickerConfig);
    datepickerConfigTo.onSelect = function(formattedDate, date) {
        $('#financeplangroup-datetill').val(date.yyyymmdd());
    };
    
    DatePickerFrom.datepicker(datepickerConfigFrom);
    DatePickerTo.datepicker(datepickerConfigTo);

    DatePickerFrom.data('datepicker').selectDate(new Date({$jsFrom}));
    DatePickerTo.data('datepicker').selectDate(new Date({$jsTill}));
    DatePickerFrom.data('datepicker').date = (new Date({$jsFrom}));
    DatePickerTo.data('datepicker').date = (new Date({$jsTill}));
    
JS, View::POS_READY);
?>