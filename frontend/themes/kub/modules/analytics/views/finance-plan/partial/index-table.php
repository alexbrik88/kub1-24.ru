<?php
use common\components\grid\GridView;
use yii\bootstrap4\Html;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\AbstractFinance;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-finance-models' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', [
        'totalCount' => $dataProvider->totalCount,
        'scroll' => true,
    ]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (FinancePlanGroup $model) {
                return Html::checkbox('FinancePlanGroup[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data-id' => $model->id
                ]);
            },
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Дата',
            'headerOptions' => [
                'width' => '5%',
                'class' => $tabConfigClass['date'],
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['date'],
            ],
            'value' => function (FinancePlanGroup $model) {
                return date('d.m.Y', strtotime($model->created_at));
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'format' => 'raw',
            'value' => function (FinancePlanGroup $model) {
                return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
            },
        ],
        [
            'attribute' => 'periodFrom',
            'label' => 'Начало',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (FinancePlanGroup $model) {

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($model->month_from, 2, "0", STR_PAD_LEFT)).' '.$model->year_from;
            },
        ],
        [
            'attribute' => 'periodTill',
            'label' => 'Окончание',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => '',
            ],
            'value' => function (FinancePlanGroup $model) {

                return ArrayHelper::getValue(AbstractFinance::$month, str_pad($model->month_till, 2, "0", STR_PAD_LEFT)).' '.$model->year_till;
            },
        ],
        [
            'attribute' => 'period',
            'label' => 'Период',
            'headerOptions' => [
                'class' => $tabConfigClass['period'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['period'],
            ],
            'value' => function (FinancePlanGroup $model) {

                return $model->period . ' мес.';
            },
        ],
        [
            'attribute' => 'currency_id',
            'label' => 'Валюта',
            'headerOptions' => [
                'class' => $tabConfigClass['currency'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['currency'],
            ],
            'value' => function (FinancePlanGroup $model) {

                return $model->getCurrencyName();
            },
        ],
        [
            'attribute' => 'total_revenue',
            'label' => 'Выручка',
            'headerOptions' => [
                'class' => $tabConfigClass['revenue'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['revenue'],
            ],
            'value' => function (FinancePlanGroup $model) {

                return TextHelper::invoiceMoneyFormat($model->total_revenue);
            },
        ],
        [
            'attribute' => 'total_marginality',
            'label' => 'Маржинальность, %',
            'headerOptions' => [
                'class' => $tabConfigClass['margin'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['margin'],
            ],
            'value' => function (FinancePlanGroup $model) {

                return TextHelper::numberFormat($model->total_marginality, 2);
            },
        ],
        [
            'attribute' => 'total_net_profit',
            'label' => 'Чистая прибыль',
            'headerOptions' => [
                'class' => $tabConfigClass['net_profit'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['net_profit'],
            ],
            'value' => function (FinancePlanGroup $model) {

                return TextHelper::invoiceMoneyFormat($model->total_net_profit);
            },
        ],
        [
            'attribute' => 'comment',
            'label' => 'Комментарий',
            'headerOptions' => [
                'class' => 'dropdown-filter ' . $tabConfigClass['comment'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['comment'],
            ],
            'format' => 'raw',
            'value' => function (FinancePlanGroup $model) {

                return Html::encode(mb_substr($model->comment, 0, 200));
            },
        ],
        [
            'attribute' => 'employee_id',
            'label' => 'Ответственный',
            'headerOptions' => [
                'class' => 'dropdown-filter ' . $tabConfigClass['employee'],
                'width' => '15%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['employee'],
                'style' => 'white-space: initial;',
            ],
            'format' => 'raw',
            'filter' => $searchModel->getEmployeeFilter(),
            's2width' => '300px',
            'value' => function (FinancePlanGroup $model) {
                return $model->employee->getFio(true);
            },
        ],
    ],
]); ?>