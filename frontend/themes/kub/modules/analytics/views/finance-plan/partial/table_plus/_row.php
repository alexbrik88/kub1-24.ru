<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;

/** @var $attr string */
/** @var $quarters array */
/** @var $_cellIds array */
/** @var $_dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $trClass string */
/** @var $level int */
/** @var $year int */

$trData  = '';
$trData .= (isset($attr)) ? (' data-attr="'.$attr.'"') : '';
$trData .= (isset($dataId)) ? ('data-id="'.$dataId.'"') : '';
$trData .= (isset($dataRowID)) ? (' data-row_id="'.$dataRowID.'"') : '';
$trData .= (isset($dataRowType)) ? (' data-row_type="'.$dataRowType.'"') : '';
$trData .= (isset($dataRowPaymentType)) ? (' data-row_payment_type="'.$dataRowPaymentType.'"') : '';
$trData .= (isset($dataCreditID)) ? (' data-credit_id="'.$dataCreditID.'"') : '';
$trData .= (isset($dataBalanceArticleID)) ? (' data-balance_article_id="'.$dataBalanceArticleID.'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

$isSortable = $isSortable ?? false;
$isButtonable = $isButtonable ?? false;
$isExpandable = $isExpandable ?? false;
$isBold = $isBold ?? false;
$isActiveRow = $isActiveRow ?? false;
$isEmptyRow = $isEmptyRow ?? false;
$isInt = $isInt ?? false;
$isTotal = $isTotal ?? false;
$negativeRed = true;
$positiveRed = false;
$units = $units ?? null;
$isAvgRow = $isAvgRow ?? false;
$isBalanceRow = $isBalanceRow ?? false;
$editAttrJS = $editAttrJS ?? false;
$updateAttrJS = $updateAttrJS ?? false;
$deleteAttrJS = $deleteAttrJS ?? false;
$visibilityAttrJS = $visibilityAttrJS ?? false;
$visibilityRelatedFrom = $visibilityRelatedFrom ?? false;
$isEditMode = $isEditMode ?? false;
$isFirstColumnFixed = $isFirstColumnFixed ?? true;
$isActiveFloorButton = $isActiveFloorButton ?? false;
$isPlanTotal = $isPlanTotal ?? false;

if ($visibilityAttrJS && isset($userConfig->{$visibilityAttrJS})) {
    $trClass .= ' col_' . $visibilityAttrJS;
    $trClass .= ($userConfig->{$visibilityAttrJS}) ? '' : ' hidden';
}
if ($visibilityRelatedFrom) {
    $_hidden = true;
    foreach ((array)$visibilityRelatedFrom as $_attr) {
        if (isset($userConfig->{$_attr})) {
            $trClass .= ' rel_' . $_attr;
            if ($userConfig->{$_attr})
                $_hidden = false;
        }
    }

    if ($_hidden) $trClass .= ' hidden';
}

?>
<tr class="<?=($trClass)?> <?=($level > 1 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="<?=($isActiveRow || $isSortable ? 'checkbox-td':'')?> <?=($isBold ? 'tooltip-td':'')?> <?=($isFirstColumnFixed ? 'fixed-column':'')?> text_size_14">
        <?php if ($isButtonable): ?>
            <div class="d-flex pl-1 <?=($isBold || $isPlanTotal) ? '' : 'ml-sub-row' ?>">
                <button class="table-collapse-btn button-clr d-block text-left <?=($isBold ? 'weight-700':'text-grey')?> <?= $isActiveFloorButton ? 'active':'' ?>" type="button" data-collapse-row-tiger data-target="<?= $floorKey ?>">
                    <span class="table-collapse-icon">&nbsp;</span>
                    <span class=""><?= $title; ?></span>
                </button>
                <?php if ($isExpandable && $isEditMode): ?>
                    <button class="<?= $editAttrJS ?> button-clr d-block link ml-auto" type="button" data-add-row-tiger data-target="<?= $attr ?>" title="Добавить строку">
                        <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
                    </button>
                <?php endif; ?>
            </div>
        <?php else: ?>
            <div class="d-flex pl-1">
                <div class="text_size_14 mr-2 nowrap <?=(!$isTotal) ? ($level === 4 ? 'ml-sub-row-2 ' : 'ml-sub-row '):'' ?><?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
                <?php if ($editAttrJS && $isEditMode): ?>
                    <div class="<?= $editAttrJS ?> link d-block ml-auto" title="Редактировать">
                        <svg class="svg-icon text_size-14"><use xlink:href="/img/svg/svgSprite.svg#cog"></use></svg>
                    </div>
                <?php endif; ?>
                <?php if ($deleteAttrJS && $isEditMode): ?>
                    <div class="<?= $deleteAttrJS ?> link d-block ml-auto" title="Удалить">
                        <svg class="svg-icon text_size-14"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg>
                    </div>
                <?php endif; ?>
                <?php if ($updateAttrJS && $isEditMode): ?>
                    <div class="<?= $updateAttrJS ?> link d-block ml-2" title="Редактировать">
                        <svg class="svg-icon text_size-14"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </td>

    <?php $quarterSum = $totalSum = 0; ?>
    <?php $quarterAvg = $totalAvg = ['amount' => 0, 'count' => 0] ?>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <?php
            $amount = ArrayHelper::getValue($data, $month, 0);
            if ($isBalanceRow) {
                $quarterSum = $amount;
                $totalSum = $amount;
            } else {
                $quarterSum += $amount;
                $totalSum += $amount;
            }
            $quarterAvg['amount'] += $amount; $quarterAvg['count'] += 1;
            $totalAvg['amount'] += $amount; $totalAvg['count'] += 1;
            ?>
            <td class="nowrap <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                <div class="pl-1 pr-1 <?=(($negativeRed && $amount < 0 || $positiveRed && $amount > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?php if (!$isEmptyRow) {
                        echo TextHelper::moneyFormat($amount, $isInt ? 0 : 2);
                        echo $units;
                    }
                    ?>
                </div>
            </td>
        <?php endforeach; ?>
        <td class="quarter-block nowrap <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-collapse-cell-total>
            <div class="pl-1 pr-1 <?=(($negativeRed && $quarterSum < 0 || $positiveRed && $quarterSum > 0) ? 'red-link' : 'text-dark-alternative')?> <?=($isBold ? 'weight-700':'text-grey')?>">
                <?php if (!$isEmptyRow) {
                    if ($isAvgRow) {
                        $quarterAvgSum = $quarterAvg['amount'] / $quarterAvg['count'];
                        echo ($quarterAvgSum >= 0) ? TextHelper::moneyFormat($quarterAvgSum, $isInt ? 0 : 2) : '';
                        echo $units;
                    } else {
                        echo TextHelper::moneyFormat($quarterSum, $isInt ? 0 : 2);
                        echo $units;
                    }
                }
                $quarterSum = 0;
                $quarterAvg = ['amount' => 0, 'count' => 0]; ?>
            </div>
        </td>
    <?php endforeach; ?>
    <td class="nowrap total-block <?=($isBold ? 'weight-700':'')?>">
        <div class="pl-1 pr-1 <?=(($negativeRed && $totalSum < 0 || $positiveRed && $totalSum > 0) ? 'red-link' : 'text-dark-alternative')?>">
            <?php if (!$isEmptyRow) {
                if ($isAvgRow) {
                    $totalAvgSum = $totalAvg['amount'] / $totalAvg['count'];
                    echo ($totalAvgSum >= 0) ? TextHelper::moneyFormat($totalAvgSum, $isInt ? 0 : 2) : '';
                    echo $units;
                } else {

                    // for self calculated total amount
                    if (isset($calculatedTotalSum))
                        $totalSum = $calculatedTotalSum;

                    echo TextHelper::moneyFormat($totalSum, $isInt ? 0 : 2);
                    echo $units;
                }
            } ?>
        </div>
    </td>
</tr>