<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use frontend\modules\reference\models\BalanceArticlesSubcategories;
use frontend\modules\analytics\models\financePlan\FinancePlanBalanceArticle as BalanceArticle;
?>

<?php Modal::begin([
    'id' => 'modal-balance-article',
    'title' => 'Добавить Основное средство / НМА',
    'titleOptions' => [
        'class' => 'mb-3 text-left modal-title',
        'data' => [
            'title-' . BalanceArticle::TYPE_FIXED_ASSERTS => 'Добавить Основное средство',
            'title-' . BalanceArticle::TYPE_INTANGIBLE_ASSETS => 'Добавить НМА',
        ]
    ],
    'closeButton' => [
        'label' => \frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<?php $pjax = Pjax::begin([
    'id' => 'pjax-balance-article',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'timeout' => 60000,
]); ?>

<?php Pjax::end(); ?>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'confirm-delete-odds-balance-article',
    'title' => 'Вы уверены, что хотите удалить это приобретение?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::button('Да', [
        'id' => 'confirm-delete-odds-balance-article-js',
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
        'data-style' => 'expand-right',
        'data-credit_id' => null
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<script>
    window.balanceArticlesSubcategoriesMapByCategories = <?= json_encode(BalanceArticlesSubcategories::MAP_BY_CATEGORIES) ?>;
    window.usefulLifeInMonthSubcategoriesMap = <?= json_encode(BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP) ?>
</script>

<style type="text/css">
    .balance-article-form .control-label {font-weight: bold;}
    .balance-article-form .form-control {width: 100%;}
    #updatebalancearticleform-type > .radio {width: 200px}
</style>