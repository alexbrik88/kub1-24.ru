<?php

use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Html;

?>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => [
                ['attribute' => 'finance_plan_date'],
                ['attribute' => 'finance_plan_currency'],
                ['attribute' => 'finance_plan_period'],
                ['attribute' => 'finance_plan_revenue'],
                ['attribute' => 'finance_plan_margin'],
                ['attribute' => 'finance_plan_net_profit'],
                ['attribute' => 'finance_plan_comment'],
                ['attribute' => 'finance_plan_employee'],
            ],
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_finance_plan']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'name', [
                'type' => 'search',
                'placeholder' => 'Поиск по названию...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
