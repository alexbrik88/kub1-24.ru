<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; ?>

<?php Modal::begin([
    'id' => 'modal-plan-credit',
    'title' => 'Добавить кредит / займ',
    'titleOptions' => [
        'class' => 'mb-3 text-left modal-title',
        'data' => [
            'title-0' => 'Добавить кредит',
            'title-1' => 'Добавить займ',
        ]
    ],
    'closeButton' => [
        'label' => \frontend\components\Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<?php $pjax = Pjax::begin([
    'id' => 'pjax-plan-credit',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'scrollTo' => false,
    'timeout' => 60000,
]); ?>

<?php Pjax::end(); ?>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'confirm-delete-odds-credit',
    'title' => 'Вы уверены, что хотите удалить этот кредит?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::button('Да', [
        'id' => 'confirm-delete-odds-credit-js',
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
        'data-style' => 'expand-right',
        'data-credit_id' => null
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>
