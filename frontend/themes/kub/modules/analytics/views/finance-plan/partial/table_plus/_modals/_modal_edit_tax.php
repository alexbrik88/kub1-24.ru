<?php

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use frontend\modules\analytics\models\financePlan\FinancePlanExpenditureItem as ExpenditureItem;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\ArrayHelper;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$relationTypesList = [
    ExpenditureItem::RELATION_TYPE_UNSET => 'Не зависит',
    ExpenditureItem::RELATION_TYPE_REVENUE => 'Выручка',
    ExpenditureItem::RELATION_TYPE_EBIDTA => 'Прибыль (EBIDTA)',
];

$item = new ExpenditureItem;
?>

<div id="edit-tax-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-edit-tax',
            'action' => Url::to(['edit-expense-item-ajax', 'group' => $model->id, 'plan' => $activePlan->id, 'row_type' => FinancePlan::ROW_TYPE_EXPENSE_TAX]),
            'method' => 'POST',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ]
        ]);
        ?>

        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('row_id', null, ['class' => 'row_id']); ?>
        <?= Html::hiddenInput('is_deleted', null, ['class' => 'is_deleted']); ?>

        <div style="display: flex">

            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    &nbsp;
                </span>
                <div class="toggled-element">
                    <?= Html::input('text', '_exists_tax_item_id', 'Налог', [
                        'id' => 'financeplanexpenditureitem-exists_tax_item_id',
                        'class' => 'form-control',
                        'disabled' => true,
                    ]) ?>
                </div>
            </div>
            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px; ">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Зависит от строки
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => 'Зависит от строки',
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= $form->field($item, 'relation_type')->widget(Select2::class, [
                    'hideSearch' => true,
                    'data' => $relationTypesList,
                    'options' => [
                        'id' => 'financeplanexpenditureitem-tax_relation_type',
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px; position: relative">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    <?= Html::tag('span', 'Умножить на', [
                        'class' => 'action-label',
                        'data-relation-' . ExpenditureItem::RELATION_TYPE_UNSET => 'Сумма налога',
                        'data-relation-' . ExpenditureItem::RELATION_TYPE_REVENUE => 'Умножить на',
                        'data-relation-' . ExpenditureItem::RELATION_TYPE_EBIDTA => 'Умножить на',
                    ]) ?>
                </span>
                <?= $form->field($item, 'action', [
                    'options' => [
                        'style' => 'width: 100%;',
                        'autocomplete' => 'off',
                    ],
                ])->textInput()
                ?>
                <?= Html::tag('div', null, [
                    'class' => 'action-symbol',
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_UNSET => $model->getCurrencySymbol(),
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_REVENUE => '%',
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_EBIDTA => '%',
                    'style' => 'position: absolute; top: 37px; right: -22px; font-size: 18px;'
                ]) ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-tax-js button-clr button-width button-regular button-hover-transparent ml-1 mr-2',
                'style' => 'width: 130px!important;',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>