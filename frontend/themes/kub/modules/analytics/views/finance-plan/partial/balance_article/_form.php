<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2019
 * Time: 23:26
 */

use yii\widgets\Pjax;
use common\models\balance\BalanceArticle;
use yii\bootstrap4\ActiveForm;
use frontend\modules\reference\models\BalanceArticlesCategories;
use frontend\modules\reference\models\BalanceArticlesSubcategories;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use frontend\modules\analytics\models\financePlan\FinancePlanBalanceArticle;

/* @var $model FinancePlanBalanceArticle */

$usefulLifeInMonthInputPostfix = ''; //Html::tag('span', Icon::QUESTION, ['class' => 'tooltip2', 'data-tooltip-content' => '#tooltip_useful_life_in_month_description']);
$calendarIco = '<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>';

$categoriesList = ($model->type === BalanceArticle::TYPE_FIXED_ASSERTS)
    ? BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP
    : BalanceArticlesCategories::INTANGIBLE_ASSETS_CATEGORIES_MAP;
$subcategoriesList = [null => ''] + ($model->type === BalanceArticle::TYPE_FIXED_ASSERTS
    ? BalanceArticlesSubcategories::MAP_BY_CATEGORIES[$model->category ?? BalanceArticlesCategories::REAL_ESTATE]
    : BalanceArticlesSubcategories::MAP_BY_CATEGORIES[$model->category ?? BalanceArticlesCategories::SOFTWARE]);

Pjax::begin([
    'id' => 'balance-article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<div class="balance-article-form" data-header="Обновить <?= $model->type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'основное средство' : 'нематериальный актив' ?>">
    <?php $form = ActiveForm::begin([
        'id' => 'balanceArticleForm',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <?= $form->field($model, 'type', [
        'options' => [
            'class' => 'form-group',
        ]])->radioList([
        $model->type => $model->type === BalanceArticle::TYPE_FIXED_ASSERTS
            ? 'Основное средство'
            : 'Нематериальный актив',
    ], [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'class' => 'flow-type-toggle-input',
                'value' => $value,
                'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                'labelOptions' => [
                    'class' => 'label mb-3 mr-3 mt-2',
                ],
            ]);
        },
    ]); ?>

    <div class="row">
        <?= $form->field($model, 'name', [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'template' => "{label}\n{input}\n{error}",
        ]) ?>

        <?= $form->field($model, 'count', [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'template' => "{label}\n{input}\n{error}",
        ])->textInput([
            'type' => 'number',
            'min' => 1,
            'value' => 1,
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'category', [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'template' => "{label}\n{input}\n{error}",
        ])->widget(Select2::class, [
            'hideSearch' => true,
            'data' => $categoriesList,
            'options' => [
                'placeholder' => '',
                'class' => 'form-control balance-article-category',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]); ?>

        <?= $form->field($model, 'subcategory', [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'template' => "{label}\n{input}\n{error}",
        ])->widget(Select2::class, [
            'hideSearch' => true,
            'data' => $subcategoriesList,
            'options' => [
                'placeholder' => '',
                'class' => 'form-control balance-article-subcategory',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'purchased_at', [
            'options' => [
                'class' => 'form-group col-3',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter date-picker-wrap',
            ],
            'template' => "{label}\n{beginWrapper}\n{input}{$calendarIco}\n{error}\n{hint}\n{endWrapper}",
        ])->textInput([
            'class' => 'form-control date-picker',
            'data-date-viewmode' => 'years',
            'value' => $model->purchased_at
                ? DateHelper::format($model->purchased_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                : null,
        ]) ?>

        <?= $form->field($model, 'useful_life_in_month', [
            'options' => [
                'class' => 'form-group col-3',
            ],
            'template' => "{label}\n{input}{$usefulLifeInMonthInputPostfix}\n{error}",
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control balance-article-useful-life',
            'value' => $model->subcategory ? BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP[$model->subcategory] : null,
        ])->label('Срок использования (мес.)') ?>

        <?= $form->field($model, 'amount', [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'template' => "{label}\n{input}\n{error}",
        ])->textInput([
            'class' => 'form-control js_input_to_money_format',
            'value' => $model->amount ? TextHelper::moneyFormatFromIntToFloat($model->amount) : null,
        ]); ?>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
            'title' => 'Отменить',
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php Pjax::end() ?>
