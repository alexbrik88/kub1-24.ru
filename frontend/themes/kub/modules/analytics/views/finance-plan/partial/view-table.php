<?php

use yii\helpers\Html;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;

/* @var bool $IS_EDIT_MODE
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 * @var $userConfig Config
 * @var $employee Employee
 * @var $salesFunnel array
 * @var $profitAndLoss array
 * @var $odds array
 * @var $groupExpenses array
 */

if (!isset($salesFunnel)) $salesFunnel = [];
if (!isset($profitAndLoss)) $profitAndLoss = [];
if (!isset($odds)) $odds = [];
if (!isset($groupExpenses)) $groupExpenses = [];

$quarters = $model->getQuarters();
$floorMap = Yii::$app->request->post('floorMap', []);

$q = 0;
$_cellIds = $_dMonthes = [];
$flatQuarters = array_keys($quarters);
$firstQuarter = $flatQuarters[0];
$lastQuarter = $flatQuarters[count($flatQuarters) - 1];
$currMonth = date('Ym');
foreach ($quarters as $quarter => $monthes) {

    $isQuarterOpened =
        ((in_array($currMonth, $monthes))
        || ($quarter == $firstQuarter && $currMonth <= $monthes[0])
        || ($quarter == $lastQuarter && $currMonth >= $monthes[count($monthes) - 1])) ? 1 : 0;

    $q++;
    $_cellIds[$quarter] = 'cell-' . $q;
    $_dMonthes[$quarter] = ArrayHelper::getValue($floorMap, 'cell-' . $q, $isQuarterOpened);
}

$tabViewClass = $userConfig->getTableViewClass('table_view_finance_plan');

$isRoundByUser = !$IS_EDIT_MODE && $userConfig->finance_model_round_numbers;
$isPlanTotal = $activePlan->isTotal;
?>

<div class="wrap wrap_padding_none" style="position: relative">

    <?= $this->render('table_plus/_modals', [
        'model' => $model,
        'activePlan' => $activePlan
    ]) ?>

    <table class="scrollable-table double-scrollbar-top finance-model-shop-month table table-style table-count-list <?= $tabViewClass ?> table-bleak">
            <thead>
                <?= $this->render('table_plus/_head', [
                    'model' => $model,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                ]) ?>
            </thead>

            <tbody>
            <?php

            ////////////////////
            //  SALES FUNNEL  //
            ////////////////////

if (!Yii::$app->request->get('debug')): // todo: TEMP

            // LEVEL 1
            foreach ($salesFunnel as $rowId => $level1) {

                $floorKey = "row-{$rowId}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, true);

                echo $this->render('table_plus/_row', [
                    'level' => 1,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'level-1',
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'isActiveFloorButton' => $isOpenedFloor,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'isBold' => true,
                    'isButtonable' => $level1['isButtonable'] ?? null,
                    'isActiveRow' => true,
                    'isEmptyRow' => empty($level1['data']),
                    'isInt' => $level1['isInt'] ?? $isRoundByUser,
                    'isTotal' => 1,
                    'units' => $level1['units'] ?? null,
                    'isEditMode' => $IS_EDIT_MODE,
                    'editAttrJS' => $level1['editAttrJS'] ?? false,
                ]);

                if (!isset($level1['levels']))
                    continue;

                // LEVEL 2
                foreach ($level1['levels'] as $attr => $level2) {

                    echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level2['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                        'level' => 2,
                        'attr' => $attr,
                        'dataRowID' => $level2['rowID'] ?? null,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'level-2',
                        'floorKey' => $floorKey,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isActiveFloorButton' => $isOpenedFloor,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isInt' => $level2['isInt'] ?? $isRoundByUser,
                        'units' => $level2['units'] ?? null,
                        'isEditMode' => $IS_EDIT_MODE,
                        'editAttrJS' => $level2['editAttrJS'] ?? false
                    ]);
                }
            }

            if ($salesFunnel && $profitAndLoss) {
                echo $this->render('table_plus/_row_empty', [
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                ]);
            }

            /////////////////////////
            //   PROFIT AND LOSS   //
            /////////////////////////

            // LEVEL 1
            foreach ($profitAndLoss as $rowId => $level1) {

                $floorKey = "row-{$rowId}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, true);

                echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level1['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                    'level' => 1,
                    'attr' => $rowId,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'level-1',
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'isActiveFloorButton' => $isOpenedFloor,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'isBold' => $level1['isBold'] ?? true,
                    'isButtonable' => $level1['isButtonable'] ?? false,
                    'isExpandable' => $level1['isExpandable'] ?? false,
                    'isActiveRow' => true,
                    'isEmptyRow' => empty($level1['data']),
                    'isInt' => $level1['isInt'] ?? $isRoundByUser,
                    'isTotal' => 1,
                    'isAvgRow' => $level1['isAvgRow'] ?? false,
                    'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                    'units' => $level1['units'] ?? null,
                    'isEditMode' => $IS_EDIT_MODE,
                    'editAttrJS' => $level1['editAttrJS'] ?? false,
                    'userConfig' => $userConfig,
                    'visibilityAttrJS' => $level1['visibilityAttrJS'] ?? false,
                    'visibilityRelatedFrom' => $level1['visibilityRelatedFrom'] ?? false,
                    'isPlanTotal' => $isPlanTotal,
                    'calculatedTotalSum' => $level1['calculatedTotalSum'] ?? null,
                ]);

                if (!isset($level1['levels']))
                    continue;

                // LEVEL 2
                foreach ($level1['levels'] as $attr => $level2) {

                    $floorKey2 = "attr-{$rowId}-{$attr}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2, true);

                    echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level2['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                        'level' => 2,
                        'attr' => $attr,
                        'dataRowID' => $level2['rowID'] ?? null,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'level-2',
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isActiveFloorButton' => $isOpenedFloor2,
                        'floorKey' => $floorKey2,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isButtonable' => $level2['isButtonable'] ?? false,
                        'isBold' => $level2['isBold'] ?? false,
                        'isInt' => $level2['isInt'] ?? $isRoundByUser,
                        'isAvgRow' => $level2['isAvgRow'] ?? false,
                        'isBalanceRow' => $level2['isBalanceRow'] ?? false,
                        'units' => $level2['units'] ?? null,
                        'isEditMode' => $IS_EDIT_MODE,
                        'editAttrJS' => $level2['editAttrJS'] ?? false,
                        'userConfig' => $userConfig,
                        'visibilityAttrJS' => $level2['visibilityAttrJS'] ?? false,
                        'visibilityRelatedFrom' => $level2['visibilityRelatedFrom'] ?? false,
                        'isPlanTotal' => $isPlanTotal,
                    ]);

                    if (!isset($level2['levels']))
                        continue;

                    // LEVEL 3
                    foreach ($level2['levels'] as $planId => $level3) {

                        echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level3['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                            'level' => 3,
                            'attr' => $planId,
                            'dataRowID' => $level3['rowID'] ?? null,
                            'title' => $level3['title'],
                            'data' => $level3['data'],
                            'trClass' => 'level-2',
                            'floorKey' => $floorKey2,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'quarters' => $quarters,
                            '_dMonthes' => $_dMonthes,
                            '_cellIds' => $_cellIds,
                            'isInt' => $level3['isInt'] ?? $isRoundByUser,
                            'isAvgRow' => $level3['isAvgRow'] ?? false,
                            'isBalanceRow' => $level3['isBalanceRow'] ?? false,
                            'units' => $level3['units'] ?? null,
                            'isEditMode' => $IS_EDIT_MODE,
                            'editAttrJS' => $level3['editAttrJS'] ?? false,
                            'userConfig' => $userConfig,
                            'visibilityAttrJS' => $level3['visibilityAttrJS'] ?? false,
                            'visibilityRelatedFrom' => $level3['visibilityRelatedFrom'] ?? false,
                            'isPlanTotal' => $isPlanTotal,
                        ]);

                    }
                }
            }

            if ($profitAndLoss && $odds) {
                echo $this->render('table_plus/_row_empty', [
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                ]);
            }
endif;
            //////////////
            //   ODDS   //
            //////////////

            // LEVEL 1
            foreach ($odds as $rowId => $level1) {

                //$floorKey = "odds-row-{$rowId}";
                //$isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, true);

                echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level1['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                    'level' => 1,
                    'attr' => $rowId,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'level-1',
                    //'floorKey' => $floorKey,
                    //'isOpenedFloor' => $isOpenedFloor,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'isBold' => $level1['isBold'] ?? true,
                    'isButtonable' => $level1['isButtonable'] ?? false,
                    'isExpandable' => $level1['isExpandable'] ?? false,
                    'isActiveRow' => true,
                    'isEmptyRow' => empty($level1['data']),
                    'isInt' => $level1['isInt'] ?? $isRoundByUser,
                    'isTotal' => 1,
                    'isAvgRow' => $level1['isAvgRow'] ?? false,
                    'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                    'units' => $level1['units'] ?? null,
                    'isEditMode' => $IS_EDIT_MODE,
                    'editAttrJS' => $level1['editAttrJS'] ?? false
                ]);

                if (!isset($level1['levels']))
                    continue;

                // LEVEL 2
                foreach ($level1['levels'] as $attr => $level2) {
                    $floorKey2 = "odds-attr-{$rowId}-{$attr}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2, true);

                    echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level2['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                        'level' => 2,
                        'attr' => $attr,
                        'dataRowID' => $level2['rowID'] ?? null,
                        'dataRowType' => $level2['rowType'] ?? null,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'level-2',
                        'dataId' => null, //$floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isActiveFloorButton' => $isOpenedFloor2,
                        'floorKey' => $floorKey2,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isButtonable' => $level2['isButtonable'] ?? false,
                        'isExpandable' => $level2['isExpandable'] ?? false,
                        'isBold' => $level2['isBold'] ?? false,
                        'isInt' => $level2['isInt'] ?? $isRoundByUser,
                        'isAvgRow' => $level1['isAvgRow'] ?? false,
                        'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                        'units' => $level2['units'] ?? null,
                        'isEditMode' => $IS_EDIT_MODE,
                        'editAttrJS' => $level2['editAttrJS'] ?? false
                    ]);

                    if (!isset($level2['levels']))
                        continue;

                    // LEVEL 3
                    foreach ($level2['levels'] as $itemId => $level3) {
                        $floorKey3 = "odds-item-{$rowId}-{$attr}-{$itemId}";
                        $isOpenedFloor3 = ArrayHelper::getValue($floorMap, $floorKey3, true);

                        echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level3['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                            'level' => 3,
                            'attr' => $itemId,
                            'dataRowID' => $level3['rowID'] ?? null,
                            'dataRowType' => $level3['rowType'] ?? null,
                            'dataRowPaymentType' => $level3['rowPaymentType'] ?? null,
                            'title' => $level3['title'],
                            'data' => $level3['data'],
                            'trClass' => 'level-3',
                            'floorKey' => $floorKey3,
                            'dataId' => $floorKey2,
                            'isOpenedFloor' => $isOpenedFloor2,
                            'isActiveFloorButton' => $isOpenedFloor3,
                            'quarters' => $quarters,
                            '_dMonthes' => $_dMonthes,
                            '_cellIds' => $_cellIds,
                            'isButtonable' => $level3['isButtonable'] ?? false,
                            'isInt' => $level3['isInt'] ?? $isRoundByUser,
                            'isAvgRow' => $level1['isAvgRow'] ?? false,
                            'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                            'units' => $level3['units'] ?? null,
                            'isEditMode' => $IS_EDIT_MODE,
                            'editAttrJS' => $level3['editAttrJS'] ?? false
                        ]);

                        if (!isset($level3['levels']))
                            continue;

                        // LEVEL 4
                        foreach ($level3['levels'] as $subItemId => $level4) {

                            echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level4['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                                'level' => 4,
                                'attr' => $itemId,
                                'dataRowID' => $level4['rowID'] ?? null,
                                'dataRowType' => $level4['rowType'] ?? null,
                                'dataRowPaymentType' => $level4['rowPaymentType'] ?? null,
                                'title' => $level4['title'],
                                'data' => $level4['data'],
                                'trClass' => 'level-3',
                                'floorKey' => null,
                                'dataId' => $floorKey3,
                                'isOpenedFloor' => $isOpenedFloor3,
                                'quarters' => $quarters,
                                '_dMonthes' => $_dMonthes,
                                '_cellIds' => $_cellIds,
                                'isInt' => $level4['isInt'] ?? $isRoundByUser,
                                'isAvgRow' => $level1['isAvgRow'] ?? false,
                                'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                                'units' => $level4['units'] ?? null,
                                'isEditMode' => $IS_EDIT_MODE,
                                'editAttrJS' => $level4['editAttrJS'] ?? false,
                                'updateAttrJS' => $level4['updateAttrJS'] ?? false,
                                'deleteAttrJS' => $level4['deleteAttrJS'] ?? false,
                                'dataCreditID' => $level4['creditID'] ?? null,
                                'dataBalanceArticleID' => $level4['balanceArticleID'] ?? null,
                            ]);

                            if (!isset($level4['levels']))
                                continue;

                        }                        
                    }
                }
            }

            ////////////////////////
            //   GROUP EXPENSES   //
            ////////////////////////

            // LEVEL 1
            foreach ($groupExpenses as $rowId => $level1) {

                $floorKey = "group-row-{$rowId}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey, true);

                echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level1['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                    'level' => 1,
                    'attr' => $rowId,
                    'title' => $level1['title'],
                    'data' => $level1['data'],
                    'trClass' => 'level-1',
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                    'isActiveFloorButton' => $isOpenedFloor,
                    'quarters' => $quarters,
                    '_dMonthes' => $_dMonthes,
                    '_cellIds' => $_cellIds,
                    'isBold' => $level1['isBold'] ?? true,
                    'isButtonable' => $level1['isButtonable'] ?? false,
                    'isExpandable' => $level1['isExpandable'] ?? false,
                    'isActiveRow' => true,
                    'isEmptyRow' => empty($level1['data']),
                    'isInt' => $level1['isInt'] ?? $isRoundByUser,
                    'isTotal' => 1,
                    'isAvgRow' => $level1['isAvgRow'] ?? false,
                    'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                    'units' => $level1['units'] ?? null,
                    'isEditMode' => $IS_EDIT_MODE,
                    'editAttrJS' => $level1['editAttrJS'] ?? false
                ]);

                if (!isset($level1['levels']))
                    continue;

                // LEVEL 2
                foreach ($level1['levels'] as $attr => $level2) {

                    $floorKey2 = "group-attr-{$rowId}-{$attr}";
                    $isOpenedFloor2 = ArrayHelper::getValue($floorMap, $floorKey2, true);

                    echo $this->render('table_plus/' . ($IS_EDIT_MODE && ($level2['isEditable'] ?? false) ? '_row_editable' : '_row'), [
                        'level' => 2,
                        'attr' => $attr,
                        'dataRowID' => $level2['rowID'] ?? null,
                        'dataRowType' => $level2['rowType'] ?? null,
                        'title' => $level2['title'],
                        'data' => $level2['data'],
                        'trClass' => 'level-2',
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isActiveFloorButton' => $isOpenedFloor2,
                        'floorKey' => $floorKey2,
                        'quarters' => $quarters,
                        '_dMonthes' => $_dMonthes,
                        '_cellIds' => $_cellIds,
                        'isButtonable' => $level2['isButtonable'] ?? false,
                        'isExpandable' => $level2['isExpandable'] ?? false,
                        'isBold' => $level2['isBold'] ?? false,
                        'isInt' => $level2['isInt'] ?? $isRoundByUser,
                        'isAvgRow' => $level1['isAvgRow'] ?? false,
                        'isBalanceRow' => $level1['isBalanceRow'] ?? false,
                        'units' => $level2['units'] ?? null,
                        'isEditMode' => $IS_EDIT_MODE,
                        'editAttrJS' => $level2['editAttrJS'] ?? false,
                        'classCustomValue' => 'custom-value-group'
                    ]);

                    if (!isset($level2['levels']))
                        continue;
                }
            }


            ?>
            </tbody>
        </table>


</div>

<?php if ($IS_EDIT_MODE): ?>
    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-start">
            <div class="column">
                <?= Html::button('Сохранить', [
                    'id' => 'save-plan',
                    'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                    'data-url' => common\components\helpers\Url::to(['update-plan', 'group' => $model->id, 'plan' => $activePlan->id, 'save' => 1]),
                    'data-style' => 'expand-right',
                    'onclick' => new \yii\web\JsExpression('location.href = $(this).data("url")')
                ]); ?>
            </div>
            <div class="column ml-auto">
                <?= Html::a('Отменить', ['view', 'id' => $model->id, 'plan' => $activePlan->id], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<style>
    table.table-count-list.finance-model-shop-month thead tr th {
        height: 30px!important;
        padding: 5px 10px 1px 10px;
    }
    .finance-model-shop-month td {
        height: 43px!important;
    }
    .finance-model-shop-month.table-compact td {
        height: 33px!important;
    }
    .finance-model-shop-month.table tbody tr.empty-row td:first-child {
        --height: .6rem!important;
    }
    li[aria-disabled='true'] {
        display: none!important;
    }
</style>