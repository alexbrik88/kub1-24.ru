<?php

use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper;
use frontend\modules\analytics\models\financePlan\FinancePlanGroupExpenditureItem as ExpenditureItem;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$item = new ExpenditureItem;
$item->relation_type = ExpenditureItem::RELATION_TYPE_UNSET;
$item->action = null;

$tooltipText = FinancePlanAutofillHelper::getTooltipText();
?>

<div id="edit-group-expense-item-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-edit-group-expense',
            'action' => Url::to(['edit-group-item-ajax', 'group' => $model->id, 'io_type' => FinancePlan::GROUP_IO_TYPE_EXPENSE]),
            'method' => 'POST',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ]
        ]);
        ?>

        <!-- js changeable -->
        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('row_id', null, ['class' => 'row_id']); ?>
        <?= Html::hiddenInput('row_type', null, ['class' => 'row_type']); ?>
        <?= Html::hiddenInput('is_deleted', null, ['class' => 'is_deleted']); ?>

        <div style="display: flex">

            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Добавить строку
                </span>
                <div class="toggled-element">
                    <?= $form->field($item, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                        'loadAssets' => false,
                        'exclude' => [
                            'items' => ArrayHelper::getColumn($model->expenditureItems, 'expenditure_item_id')
                        ],
                        'options' => [
                            'id' => 'group-expenditure_item_id',
                            'class' => 'flow-expense-items',
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'placeholder' => '',
                        ]
                    ]); ?>
                    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                        'inputId' => 'group-expenditure_item_id',
                    ]) ?>

                    <!-- required fields defaults -->
                    <?= $form->field($item, 'relation_type')->hiddenInput() ?>
                    <?= $form->field($item, 'action')->hiddenInput() ?>
                    <?= Html::hiddenInput('autofill[start_month]', min($model->monthes)) ?>

                </div>
                <div class="toggled-element" style="display: none">
                    <?= Html::input('text', null, 'group_exists_expenditure_item_id', [
                        'id' => 'group-exists_expenditure_item_id',
                        'class' => 'form-control',
                        'disabled' => true,
                    ]) ?>
                </div>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Нач. значение
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['startAmount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[start_amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'group-new-autofill-start_amount', // id not used
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                ]); ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Действие
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['action'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Select2::widget([
                    'hideSearch' => true,
                    'id' => 'group-new-autofill-action',
                    'name' => 'autofill[action]',
                    'value' => min(array_keys(FinancePlanAutofillHelper::$autoPlanningActionsMap)),
                    'data' => FinancePlanAutofillHelper::$autoPlanningActionsMap,
                    'options' => [
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Число
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['amount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'group-new-autofill-amount',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    <span class="dynamic-label">Макс. значение</span>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['limitAmount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[limit_amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'group-new-autofill-limit_amount',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                    'disabled' => true,
                ]); ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-group-expense-js button-clr button-width button-regular button-hover-transparent ml-1 mr-2',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-group-expense-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php Modal::begin([
    'id' => 'confirm-delete-group-expense',
    'title' => 'Вы уверены, что хотите удалить эту строку?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'id' => 'confirm-delete-group-expense-js',
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>