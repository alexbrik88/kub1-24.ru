<?php

use frontend\themes\kub\components\Icon;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;

/** @var FinancePlanChart $model */

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPlan = $customPlan ?? FinancePlanChart::PLANS_TOTAL;
/////////////////////////////////////////

///////////////// colors /////////////////
$color = 'rgba(46,159,191,1)';
$color_opacity = 'rgba(46,159,191,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 0;
$RIGHT_DATE_OFFSET = 18;
$MOVE_OFFSET = 6;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

$currDayPos = null;
if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$datePeriods = $model->getPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

$wrapPointPos = [];
$daysPeriods = [];
$chartPlanFactLabelsX = [];
$chartFreeDays = [];
$yearPrev = (int)substr($datePeriods[0],0 ,4);
foreach ($datePeriods as $i => $date) {
    $year  = (int)substr($date,0 ,4);
    $month = (int)substr($date, 4, 2);

    if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
        $wrapPointPos[$i] = [
            'prev' => $year - 1,
            'next' => $year
        ];
        // emulate align right
        switch ($year - 1) {
            case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
            $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
            default:
                $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
        }
    }

    $yearPrev = $year;

    $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
    $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
    if ($month == (int)date('m') && $year == (int)date('Y'))
        $currDayPos = $i;
}

$data = $model->getSingleChartData($datePeriods, $customPlan);
$dataRevenue = &$data['revenue'];
$dataNetTotal = &$data['net_total'];
$dataBalance = &$data['balance'];

$multiChartSeries = $model->getMultiChartSeries($datePeriods);

if (Yii::$app->request->post('chart-finance-plan-ajax')) {

    $series = [];
    $series2 = [];
    foreach ($multiChartSeries as $s) {
        $series[] = ['data' => $s['revenue']];
        $series[] = ['data' => $s['net_total']];
        $series2[] = ['data' => $s['balance']];
    }

    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataRevenue,
                ],
                [
                    'data' => $dataNetTotal,
                ]
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataBalance,
                ],
            ],
        ],
        'optionsMultiChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => $series,
        ],
        'optionsMultiChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => $series2,
        ]
    ]);

    exit;
}

$selectPlansData = [
    FinancePlanChart::PLANS_TOTAL => 'Итого',
    FinancePlanChart::PLANS_ALL => 'По направлениям',
] + \yii\helpers\ArrayHelper::map($model->getPlans(), 'id', 'name');

$selectPlansOptions = [
    FinancePlanChart::PLANS_TOTAL => ['data-chart-title' => ''],
    FinancePlanChart::PLANS_ALL => ['data-chart-title' => ''],
] + \yii\helpers\ArrayHelper::map($model->getPlans(), 'id', function($p) { return ['data-chart-title' => $p->name]; });

?>
<style>
    #chart-finance-plan, #chart-finance-plan-2 { height: 235px; }
    #multi-chart-finance-plan, #multi-chart-finance-plan-2 { height: 235px; }
</style>

<div style="position: relative; margin-right:20px;">
    <div style="width: 100%;">

        <div class="chart-finance-plan-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:286px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-finance-plan-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:286px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-finance-plan-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-finance-plan-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
            <?= \kartik\select2\Select2::widget([
                'id' => 'chart-finance-plan-select2',
                'name' => 'selectPlans',
                'data' => $selectPlansData,
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                    'options' => $selectPlansOptions
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '170px'
                ]
            ]); ?>
        </div>
        <div class="clearfix"></div>

        <div class="toggle-single-multi-chart" data-type="single" style="display: block">
            <?= $this->render('chart/single_chart.php', [
                'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
                'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
                'daysPeriods' => $daysPeriods,
                'dataRevenue' => $dataRevenue,
                'dataNetTotal' => $dataNetTotal,
                'dataBalance' => $dataBalance,
                'color_opacity' => $color_opacity,
                'color' => $color,
            ]) ?>
        </div>

        <div class="toggle-single-multi-chart" data-type="multi" style="display: none">
            <?= $this->render('chart/multi_chart.php', [
                'LEFT_DATE_OFFSET' => $LEFT_DATE_OFFSET,
                'RIGHT_DATE_OFFSET' => $RIGHT_DATE_OFFSET,
                'daysPeriods' => $daysPeriods,
                'multiChartSeries' => $multiChartSeries,
                'color_opacity' => $color_opacity,
                'color' => $color,
            ]) ?>
        </div>

    </div>
</div>

<script>
    // MOVE CHART
    FinancePlanChart = {
        chart: 'main',
        chartTab: 'single',
        groupID: <?= $model->getGroupID() ?>,
        offset: <?= $customOffset ?>,
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        multiChartPoints: {},
        multiChartPoints2: {},
        byPlan: "<?= $customPlan ?>",
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $(document).on('click', '.chart-finance-plan-arrow', function() {
                // prevent double-click
                if (FinancePlanChart._inProcess) {
                    return false;
                }
                if ($(this).data('move') === 'left') {
                    FinancePlanChart.offset -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    FinancePlanChart.offset += <?= $MOVE_OFFSET ?>;
                }
                FinancePlanChart.redrawByClick();
            });

            $(document).on('change', '#chart-finance-plan-select2', function() {
                FinancePlanChart.byPlan = $(this).val();
                FinancePlanChart.setTitle($(this).find('option:selected').data('chart-title'));
                FinancePlanChart.redrawByClick();
            });
        },
        toggleChartTab: function() {
            if (this.byPlan === 'all' && this.chartTab === 'single') {
                $('.toggle-single-multi-chart').filter('[data-type=single]').hide();
                $('.toggle-single-multi-chart').filter('[data-type=multi]').show();
                this.chartTab = 'multi';
            } else if (this.byPlan !== 'all' && this.chartTab === 'multi') {
                $('.toggle-single-multi-chart').filter('[data-type=single]').show();
                $('.toggle-single-multi-chart').filter('[data-type=multi]').hide();
                this.chartTab = 'single';
            }
        },
        setTitle: function(title) {
            $('.plan-title').html(title);
        },
        redrawByClick: function() {

            return FinancePlanChart._getData().done(function() {

                FinancePlanChart.toggleChartTab();

                $('#chart-finance-plan').highcharts().update(FinancePlanChart.chartPoints);
                $('#chart-finance-plan-2').highcharts().update(FinancePlanChart.chartPoints2);
                $('#multi-chart-finance-plan').highcharts().update(FinancePlanChart.multiChartPoints);
                $('#multi-chart-finance-plan-2').highcharts().update(FinancePlanChart.multiChartPoints2);
                FinancePlanChart._inProcess = false;
            });
        },
        _getData: function() {
            FinancePlanChart._inProcess = true;
            return $.post('/analytics/finance-plan-ajax/get-chart-data/?groupID=' + FinancePlanChart.groupID, {
                    "chart-finance-plan-ajax": true,
                    "chart": FinancePlanChart.chart,
                    "offset": FinancePlanChart.offset,
                    "plan": FinancePlanChart.byPlan,
                },
                function(data) {
                    data = JSON.parse(data);
                    FinancePlanChart.freeDays = data.freeDays;
                    FinancePlanChart.currDayPos = data.currDayPos;
                    FinancePlanChart.labelsX = data.labelsX;
                    FinancePlanChart.wrapPointPos = data.wrapPointPos;
                    FinancePlanChart.chartPoints = data.optionsChart;
                    FinancePlanChart.chartPoints2 = data.optionsChart2;
                    FinancePlanChart.multiChartPoints = data.optionsMultiChart;
                    FinancePlanChart.multiChartPoints2 = data.optionsMultiChart2;

                    console.log(data.optionsMultiChart2)
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-finance-plan').highcharts() || $('#multi-chart-finance-plan').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (FinancePlanChart.period == 'months') ? 0.705 : 0.625;

            if (FinancePlanChart.wrapPointPos[x + 1]) {
                name = FinancePlanChart.wrapPointPos[x + 1].prev;
                left = FinancePlanChart.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (FinancePlanChart.wrapPointPos[x]) {
                name = FinancePlanChart.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        getSerieName: function() {
            return $('#chart-finance-plan-select2').find('option:selected').data('chart-title');
        }
    };

    //////////////////////////////
    FinancePlanChart.init();
    //////////////////////////////

</script>