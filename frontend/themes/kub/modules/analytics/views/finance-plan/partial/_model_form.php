<?php

use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use common\models\company\CompanyInfoIndustry;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;

/**
 * @var $model FinancePlan
 * @var $groupModel FinancePlanGroup
 */

$industryList = ArrayHelper::map(CompanyInfoIndustry::find()
    ->select(['id', 'name'])
    ->orderBy(new Expression('IF (id = '.CompanyInfoIndustry::OTHER.', 1, 0), name'))
    ->asArray()
    ->all(), 'id', 'name');

$form = ActiveForm::begin([
    'id' => 'form-finance-plan',
    'action' => $model->id
        ? Url::to(['update-plan', 'group_id' => $groupModel->id])
        : Url::to(['create-plan', 'group_id' => $groupModel->id]),
    'method' => 'POST',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]);
?>
    <div style="display: none">
        <?= $form->field($model, 'model_group_id')->hiddenInput()->label(false); ?>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'name')->label('Название дополнительного направления'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'industry_id')->widget(Select2::class, [
                        'data' => $industryList,
                        'options' => [
                            'disabled' => !$model->isNewRecord
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ],
                    ])->label('Тип дополнительного направления') ?>
                    <?php if (!$model->isNewRecord): ?>
                        <?= $form->field($model, 'industry_id')->hiddenInput() ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row wrap-copy-finance-plan" style="display: none">
                <div class="col-6">

                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label class="label">Скопировать структуру финмодели из</label>
                        <?= Select2::widget([
                            'id' => 'financeplan-cloned_plan_id',
                            'name' => 'clonedPlanID',
                            'hideSearch' => true,
                            'data' => [0 => 'Не копировать'] + ArrayHelper::map($groupModel->financePlans, 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Не копировать',
                                'options' => [0 => ['data-industry' => 0]] + ArrayHelper::map($groupModel->financePlans, 'id', function($p) { return ['data-industry' => $p->industry_id]; })
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                                'templateResult' => new JsExpression('function(data, container) { 
                                    const filterIndustry = $("#financeplan-industry_id").val();
                                    const industry = $(data.element).attr("data-industry");
                                    if (industry === filterIndustry || industry === "0") {
                                        $(container).removeClass("hidden");
                                    } else {
                                        $(container).addClass("hidden");
                                    }
                                   
                                    return data.text; 
                                } '),
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>
