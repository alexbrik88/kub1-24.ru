<?php

use yii\bootstrap\Nav;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;

/**
 * @var string $tab
 * @var bool $IS_EDIT_MODE
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$items = [];

if (!$IS_EDIT_MODE && count($model->financePlans) > 1) {
    $items[] = [
        'label' => 'Итого',
        'url' => Url::to(['view', 'id' => $model->id, 'plan' => FinancePlanGroup::PLAN_TOTAL_ID]),
        'active' => $activePlan->id == FinancePlanGroup::PLAN_TOTAL_ID,
        'options' => [
            'class' => 'nav-item',
            'data-tab' => FinancePlanGroup::TAB_PLAN,
            'data-plan' => FinancePlanGroup::PLAN_TOTAL_ID
        ],
        'linkOptions' => [
            'class' => 'nav-link'
        ],
    ];
}

foreach ($model->financePlans as $key => $plan) {
    $items[] = [
        'label' => $plan->name,
        'url' => ($IS_EDIT_MODE)
            ? Url::to(['update-plan-ajax', 'group' => $model->id, 'plan' => $plan->id])
            : Url::to(['view', 'id' => $model->id, 'plan' => $plan->id]),
        'active' => $activePlan->id == $plan->id,
        'options' => [
            'class' => 'nav-item',
            'data-tab' => FinancePlanGroup::TAB_PLAN,
            'data-plan' => $plan->id
        ],
        'linkOptions' => [
            'class' => 'nav-link'
        ],
    ];
}


if (count($model->financePlans) > 1) {
    $items[] = [
        'label' => 'Общие расходы',
        'url' => ($IS_EDIT_MODE)
            ? Url::to(['update-expenses-ajax', 'group' => $model->id])
            : Url::to(['view', 'id' => $model->id, 'tab' => FinancePlanGroup::TAB_EXPENSES]),
        'active' => $tab == FinancePlanGroup::TAB_EXPENSES,
        'options' => [
            'class' => 'nav-item',
            'data-tab' => FinancePlanGroup::TAB_EXPENSES,
            'data-plan' => null
        ],
        'linkOptions' => [
            'class' => 'nav-link'
        ],
    ];
}

?>

<div class="nav-finance">
    <?= Nav::widget([
        'id' => 'finance-plan-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3 mb-3'],
        'items' => $items,
    ]);
    ?>
</div>
