<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;

/** @var $attr string */
/** @var $quarters array */
/** @var $_cellIds array */
/** @var $_dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $trClass string */
/** @var $level int */

$trData  = '';
$trData .= (isset($attr)) ? (' data-attr="'.$attr.'"') : '';
$trData .= (isset($dataId)) ? (' data-id="'.$dataId.'"') : '';
$trData .= (isset($dataRowID)) ? (' data-row_id="'.$dataRowID.'"') : '';
$trData .= (isset($dataRowType)) ? (' data-row_type="'.$dataRowType.'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

$isInt = $isInt ?? false;
$isBold = $isBold ?? false;
$calcJS = $calcJS ?? false;
$editAttrJS = $editAttrJS ?? false;
$trClass = $trClass ?? false;
$isAvgRow = $isAvgRow ?? false;
$isFirstColumnFixed = $isFirstColumnFixed ?? true;
$classCustomValue = $classCustomValue ?? 'custom-value';
?>
<tr class="<?=($trClass)?> <?=($level > 1 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="text_size_14 <?=($isBold ? 'weight-700':'')?> <?=($isFirstColumnFixed ? 'fixed-column':'')?>">
        <div class="d-flex pl-1">
            <div class="text_size_14 mr-2 nowrap <?=($level > 1 ? 'ml-sub-row text-grey' : '') ?>">
                <?= $title ?>
            </div>
            <?php if ($editAttrJS): ?>
                <div class="<?= $editAttrJS ?> link d-block ml-auto" title="Редактировать">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#cog"></use>
                    </svg>
                </div>
            <?php endif; ?>
        </div>
    </td>

    <?php $quarterSum = $totalSum = 0; ?>
    <?php $quarterAvg = $totalAvg = ['amount' => 0, 'count' => 0] ?>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <?php
            $amount = ArrayHelper::getValue($data, $month, 0);
            $quarterSum += $amount;
            $totalSum += $amount;
            $quarterAvg['amount'] += $amount; $quarterAvg['count'] += 1;
            $totalAvg['amount'] += $amount; $totalAvg['count'] += 1;
            ?>

            <?php if ($calcJS): ?>
                <td class="month-block nowrap text-right <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>  <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                    <?= TextHelper::moneyFormat($amount, $isInt ? 0 : 2) ?>
                </td>
            <?php else: ?>
                <td class="td-custom-value month-block nowrap text-right <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>  <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell>
                    <input type="text" name="<?=("{$attr}[{$month}]")?>" class="<?= $classCustomValue ?> form-control <?=($isInt ? 'int':'')?>"
                       value="<?= TextHelper::moneyFormat($amount, $isInt ? 0 : 2) ?>">
                </td>
            <?php endif; ?>
        <?php endforeach; ?>
        <td class="quarter-block text-right nowrap <?= $_dMonthes[$quarter] ? 'd-none' : '' ?> <?=($isBold ? 'weight-700':'text-dark-alternative')?>" data-id="<?= $_cellIds[$quarter] ?>" data-collapse-cell-total>
            <?= TextHelper::moneyFormat(($isAvgRow) ? $quarterAvg['amount'] / $quarterAvg['count'] : $quarterSum, $isInt ? 0 : 2) ?>
        </td>
        <?php $quarterSum = 0; $quarterAvg = ['amount' => 0, 'count' => 0]; ?>
    <?php endforeach; ?>
    <td class="nowrap text-right total-block <?=($isBold ? 'weight-700':'text-dark-alternative')?>">
        <?= TextHelper::moneyFormat(($isAvgRow) ? $totalAvg['amount'] / $totalAvg['count'] : $totalSum, $isInt ? 0 : 2) ?>
    </td>
</tr>