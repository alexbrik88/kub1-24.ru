<?php

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\Html;
use frontend\modules\analytics\models\AbstractFinance;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$monthData = [];
foreach ($model->getMonthes() as $m) {
    $year = substr($m, 0, 4);
    $month = substr($m, 4, 2);
    $monthData[$m] = ArrayHelper::getValue(AbstractFinance::$month, $month, '---') . ' ' . $year;
}

$tooltipText = FinancePlanAutofillHelper::getTooltipText();
?>

<div id="update-planning-auto-fill-item-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?= Html::hiddenInput('autofill[attr]', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('autofill[row_id]', null, ['class' => 'row_id']); ?>
        <div style="display: flex">
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px">
                    С месяца
                </span>
                <?= Select2::widget([
                    'id' => 'autofill-start_month',
                    'name' => 'autofill[month_start]',
                    'value' => min(array_keys($monthData)),
                    'data' => $monthData,
                    'options' => [
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Нач. значение
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['startAmount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[start_amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'autofill-start_amount',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                ]); ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Действие
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['action'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Select2::widget([
                    'hideSearch' => true,
                    'id' => 'autofill-action',
                    'name' => 'autofill[action]',
                    'value' => min(array_keys(FinancePlanAutofillHelper::$autoPlanningActionsMap)),
                    'data' => FinancePlanAutofillHelper::$autoPlanningActionsMap,
                    'options' => [
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Число
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['amount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'autofill-amount',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    <span class="dynamic-label">Макс. значение</span>
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => $tooltipText['limitAmount'],
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= Html::textInput("autofill[limit_amount]", null, [
                    'class' => 'form-control amount-input',
                    'id' => 'autofill-limit_amount',
                    'style' => 'width: 100%;',
                    'autocomplete' => 'off',
                    'disabled' => true,
                ]); ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::button('Сохранить', [
                'class' => 'complete-auto-planning-item button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-autofill-js button-regular button-width button-hover-transparent button-clr ml-1 mr-2',
            ]); ?>

            <!-- delete row buttons -->
            <?= Html::button('Удалить', [
                'class' => 'delete-fixed-cost-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-odds-income-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-odds-expense-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-group-expense-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-other-income-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-other-expense-js delete-row-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
        </div>
    </div>
</div>
