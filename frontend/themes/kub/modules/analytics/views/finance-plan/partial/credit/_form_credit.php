<?php

namespace frontend\modules\analytics\views\credits;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use frontend\modules\analytics\assets\credits\CreditFormAsset;
use frontend\modules\analytics\models\credits\CreditLabelHelper;
use frontend\modules\analytics\models\financePlan\FinancePlanCredit as Credit;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var $this View
 * @var Company $company
 * @var Employee $employee
 * @var Credit $model
 */

CreditFormAsset::register($this);

$datePickerTemplate = <<<HTML
{label}
<div class="date-picker-wrap">
    {input}
    <svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>
</div>
HTML;

$headerOptions = [
    'enableError' => false,
    'options' => ['class' => 'form-group mb-0'],
];

$questionIconOptions = [
    'wallet_value' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_wallet_value', 'style' => 'position:absolute; top:0; left:137px'],
    'credit_tranche_depth' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_tranche_depth', 'style' => 'position:absolute; top:0; left:155px', 'id' => 'creditTrancheGroupHelp'],
    'credit_commission_rate' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_commission_rate', 'style' => 'position:absolute; top:0; left:130px'],
    'credit_expiration_rate' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_expiration_rate', 'style' => 'position:absolute; top:0; left:150px'],
    'payment_type' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_type', 'style' => 'position:absolute; top:0; left:165px'],
    'payment_mode' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_mode', 'style' => 'position:absolute; top:0; left:150px'],
    'payment_first' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_first', 'style' => 'position:absolute; top:0; left:199px'],
    'credit_year_length' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_year_length', 'style' => 'position:absolute; top:0; left:127px'],
];

?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'options' => [
            'id' => 'creditForm',
        ],
        'fieldConfig' => [
            'labelOptions' => ['class' => 'label'],
            'options' => ['class' => 'form-group'],
        ],
    ]) ?>

    <?= $form->field($model, 'agreement_date', ['template' => '{input}'])->hiddenInput([
        'value' => DateHelper::format($model->agreement_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
    ]) ?>

    <div class="credit-form-body">

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'credit_type')->widget(Select2::class, [
                    'data' => Credit::TYPE_LIST,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
                    'company' => $company,
                    'contractorType' => Contractor::TYPE_SELLER,
                    'staticData' => ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить поставщика'],
                    'options' => [
                        'id' => 'creditform-contractor_id',
                        'class' => 'contractor-items-depend seller',
                        'placeholder' => '',
                        'data' => [
                            'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                        ],
                    ],
                ])->label('Поставщик'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'credit_amount')
                    ->textInput()
                    ->label(CreditLabelHelper::getLabel('credit_amount', $model->credit_type), [
                        'data-label-'.Credit::TYPE_CREDIT => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_CREDIT),
                        'data-label-'.Credit::TYPE_LOAN => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_LOAN),
                        'data-label-'.Credit::TYPE_OVERDRAFT => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_OVERDRAFT),
                    ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <?= $form
                    ->field($model, 'credit_first_date', ['template' => $datePickerTemplate])
                    ->textInput([
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                        'data-date-format' => 'dd.mm.yyyy',
                        'value' => DateHelper::format($model->credit_first_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                    ]) ?>
            </div>

            <div class="col-3">
                <?= $form
                    ->field($model, 'credit_last_date', ['template' => $datePickerTemplate])
                    ->textInput([
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                        'data-date-format' => 'dd.mm.yyyy',
                        'value' => DateHelper::format($model->credit_last_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                    ]) ?>
            </div>

            <div class="col-3">
                <?= $form->field($model, 'credit_tranche_depth', ['options' => ['id' => 'creditTrancheGroup']])->textInput() ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_tranche_depth']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'credit_percent_rate')->textInput() ?>
            </div>

            <div class="col-3">
                <?= $form->field($model, 'credit_commission_rate')->textInput() ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_commission_rate']) ?>
            </div>

            <div class="col-3">
                <?= $form->field($model, 'credit_expiration_rate')->textInput() ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_expiration_rate']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'payment_type')->widget(Select2::class, [
                    'data' => Credit::PAYMENT_TYPE_LIST,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ])->label(CreditLabelHelper::getLabel('payment_type', $model->credit_type), [
                    'data-label-'.Credit::TYPE_CREDIT => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_CREDIT),
                    'data-label-'.Credit::TYPE_LOAN => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_LOAN),
                    'data-label-'.Credit::TYPE_OVERDRAFT => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_OVERDRAFT),
                    'data-label-left-'.Credit::TYPE_CREDIT => '160px',
                    'data-label-left-'.Credit::TYPE_LOAN => '148px',
                    'data-label-left-'.Credit::TYPE_OVERDRAFT => '183px',
                ])  ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_type']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'payment_mode')->widget(Select2::class, [
                    'data' => Credit::PAYMENT_MODE_LIST,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ]) ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_mode']) ?>
            </div>

            <div class="col-3">
                <?= $form->field($model, 'payment_day', ['options' => ['id' => 'paymentDayGroup']])->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'payment_first')->widget(Select2::class, [
                    'data' => Credit::PAYMENT_FIRST_LIST,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ]) ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_first']) ?>
            </div>

            <div class="col-3">
                <?= $form->field($model, 'credit_year_length')->widget(Select2::class, [
                    'data' => Credit::YEAR_LENGTH_LIST,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ]) ?>
                <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_year_length']) ?>
            </div>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <button type="submit" class="button-regular button-width button-regular_red" data-style="expand-right">Сохранить</button>
        <button type="button" class="button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

    <div style="display: none">
        <div id="tooltip_wallet_value">
            <strong>Счет для зачисления</strong> – расчетный счет, касса или электронная платежная система<br/>
            (Яндекс.Деньги, QIWI Кошелек и др.) куда будут зачислены денежные средства по кредиту или займу.<br/>
            По умолчанию установлен основной расчетный счет.
        </div>
        <div id="tooltip_credit_commission_rate">
            <strong>Комиссия/Дисконт</strong> – сумма или процентная ставка установленная кредитным договором помимо уплаты процентов.<br/>
        </div>
        <div id="tooltip_credit_tranche_depth">
            <strong>Глубина транша</strong> – этот термин актуален только для кредитования в форме овердрафта или кредитной линии,<br/>
            и подразумевает количество календарных дней (обычно 30, 45, 60, 90) на которое предоставляется каждая сумма (транш)<br/>
            в рамках установленного кредитного лимита.
        </div>
        <div id="tooltip_credit_expiration_rate">
            <strong>Ставка при просрочке</strong> – размер повышенной процентной ставки, пеня (иная штрафная санкция, установленная в %),<br/>
            которые применяется к заемщику в случае нарушения им сроков уплаты ежемесячных платежей.
        </div>
        <div id="tooltip_payment_type">
            <strong>Вид платежа по кредиту</strong> – это способ погашения основного долга по кредиту в зависимости от вида кредита.<br/>
            Предусмотрено три варианта:<br/>
            <ul class="pl-3">
                <li>В конце срока кредит (устанавливается по умолчанию) – кредит в полном объеме погашается в конце срока.<br/>Проценты начисляются на полную сумму кредита и уплачиваются ежемесячно;</li>
                <li>В конце срока кредит и %% по кредиту - кредит и проценты по кредиту в полном объеме погашаются в конце срока</li>
                <li>Дифференцированный – кредит погашается равными долями в течение срока действия кредитного договора.<br/>Проценты начисляются на фактический остаток задолженности, поэтому с каждым месяцем<br/>сумма платежа «кредит + проценты» снижается;</li>
                <li>Аннуитетный – сумма ежемесячного платежа «кредит + проценты» является постоянной на весь срок кредитования.<br/>Аннуитетный платеж включает в себя и погашение долга по кредиту, и уплату процентов;</li>
            </ul>
        </div>
        <div id="tooltip_payment_mode">
            <strong>Ежемесячные платежи</strong> – дата (день) осуществления платежа по процентам или платежа «кредит + проценты».<br/>
            По умолчанию устанавливается «последний день месяца»;
        </div>
        <div id="tooltip_payment_first">
            <strong>Первый ежемесячный платеж</strong> – период осуществления первого платежа (проценты или «кредит + проценты»).<br/>
            Существует практика предоставления кредитором льгот или каникул по первому платежу.<br/>
            <ul class="pl-3">
                <li>«Стандартно» - устанавливается по умолчанию при отсутствии льгот или каникул;</li>
                <li>«Первый платеж только %» - распространяется на кредиты с дифференцированным или аннуитетным видом платежа в случае <br/>предоставления кредитором каникул по погашению суммы основного долга по кредиту (тела кредита) в первый месяц;</li>
                <li>«Платеж со второго месяца» - выбирается в случае предоставления кредитором каникул по уплате платежей в полном объеме<br/> (кредита и процентов) в течение первого месяца.</li>
            </ul>
        </div>
        <div id="tooltip_credit_year_length">
            <strong>Число дней в году</strong> – расчетное количество дней в году для начисления процентов по кредиту.<br/>
            Обычно соответствует календарному количеству, поэтому по умолчанию установлено «по календарю».<br/>
            Однако кредитным договором может быть установлено отличное от календарного количество дней: 360 или 365.
        </div>
    </div>

    <?php ActiveForm::end() ?>

<?= $this->render('@frontend/modules/cash/views/order/_partial/modal-add-contractor') ?>

    <?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);