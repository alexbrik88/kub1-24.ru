<?php
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */
?>

<?= $this->render('_modals/_modal_autofill', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_prime_cost', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_variable_cost', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_fixed_cost', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_tax', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_odds_income', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_odds_expense', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_group_expense', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_odds_system_income', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_odds_system_expense', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_income', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>

<?= $this->render('_modals/_modal_edit_expense', [
    'model' => $model,
    'activePlan' => $activePlan
]) ?>
