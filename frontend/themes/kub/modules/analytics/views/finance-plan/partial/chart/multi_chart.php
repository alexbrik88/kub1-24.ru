<?php

use yii\web\JsExpression;

/**
 * @var $LEFT_DATE_OFFSET
 * @var $RIGHT_DATE_OFFSET
 * @var $daysPeriods
 * @var $multiChartSeries
 * @var $multiChartColors
 */

$series = [];
$series2 = [];

foreach ($multiChartSeries as $s) {
    $series[] = [
        'type' => 'spline',
        'name' => $s['name'] . ', выручка',
        'data' => $s['revenue'],
        'pointPlacement' => 0,
        'stickyTracking' => false,
        'color' => $s['color'],
        'marker' => [
            'enabled' => false,
        ],
    ];
    $series[] = [
        'type' => 'spline',
        'name' => $s['name'] . ', чистая прибыль',
        'data' => $s['net_total'],
        'pointPlacement' => 0,
        'stickyTracking' => false,
        'color' => $s['color'],
        'marker' => [
            'enabled' => false,
        ],
        'dashStyle' => 'shortdot'
    ];

    $series2[] = [
        'type' => 'spline',
        'name' => $s['name'] . ', остаток',
        'data' => $s['balance'],
        'color' => $s['color'],
        'marker' => [
            'enabled' => false,
        ],
        'legendIndex' => 0
    ];
}

?>

<div class="ht-caption noselect" style="margin-bottom: 16px">
    ВЫРУЧКА <span style="text-transform: lowercase">и</span> ЧИСТАЯ ПРИБЫЛЬ
    <span class="plan-title"></span>
</div>
<div class="finance-charts-group" style="min-height:235px;">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'multi-chart-finance-plan',
        'class' => 'finance-charts',
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'events' => [],
                'spacing' => [0,0,0,0],
                'marginBottom' => '50',
                'marginLeft' => '55',
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                ]
                //'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'shared' => false,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {

                        const index = this.series.data.indexOf( this.point );
                        const series_index = this.series.index;

                        return '<span class=\"title\">' + FinancePlanChart.labelsX[this.point.index] + '</span>' +
                            '<table class=\"indicators\">' +
                                ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>') +
                            '</table>';
                    }
                ")
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                [
                    'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'useHTML' => true,
                        'style' => [
                            'fontWeight' => '300',
                            'fontSize' => '13px',
                            'whiteSpace' => 'nowrap'
                        ]
                    ]
                ],
            ],
            'xAxis' => [
                [
                    'min' => 1,
                    'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                    'categories' => $daysPeriods,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("
                            function() {
                                var result = (this.pos == FinancePlanChart.currDayPos) ?
                                    ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                    (FinancePlanChart.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                if (FinancePlanChart.wrapPointPos) {
                                    result += FinancePlanChart.getWrapPointXLabel(this.pos);
                                }

                                return result;
                            }"),
                        'useHTML' => true,
                        'autoRotation' => false
                    ],
                ],
            ],
            'series' => $series,
            'plotOptions' => [
                'series' => [
                    'pointWidth' => 20,
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                    'groupPadding' => 0.05,
                    'pointPadding' => 0.1,
                    'borderRadius' => 3,
                    'borderWidth' => 1
                ]
            ],
        ],
    ]); ?>
</div>

<div class="ht-caption noselect mt-3">
    ОСТАТОК ДЕНЕГ
    <span class="plan-title"></span>
</div>
<div class="finance-charts-group" style="min-height:235px;">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'multi-chart-finance-plan-2',
        'scripts' => [
            // 'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill'
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'areaspline',
                'events' => [
                    'load' => null
                ],
                'spacing' => [0,0,0,0],
                'marginBottom' => '50',
                'marginLeft' => '55',
                'style' => [
                    'fontFamily' => '"Corpid E3 SCd", sans-serif'
                ]
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {

                        const index = this.series.data.indexOf( this.point );
                        const series_index = this.series.index;

                        return '<span class=\"title\">' + FinancePlanChart.labelsX[this.point.index] + '</span>' +
                            '<table class=\"indicators\">' +
                                ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>') +
                            '</table>';
                    }
                ")
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
                'labels' => [
                    'useHTML' => true,
                    'style' => [
                        'fontWeight' => '300',
                        'fontSize' => '13px',
                        'whiteSpace' => 'nowrap'
                    ]
                ]
            ],
            'xAxis' => [
                'min' => 1,
                'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                'categories' => $daysPeriods,
                'title' => '',
                'minorGridLineWidth' => 0,
                'labels' => [
                    'formatter' => new \yii\web\JsExpression("
                                function() {
                                    result = (this.pos == window.FinancePlanChart.currDayPos) ?
                                        ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                        (window.FinancePlanChart.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                                    if (window.FinancePlanChart.wrapPointPos) {
                                        result += window.FinancePlanChart.getWrapPointXLabel(this.pos);
                                    }

                                    return result;
                                }"),
                    'useHTML' => true,
                    'autoRotation' => false
                ],
            ],
            'series' => $series2,
            'plotOptions' => [
                'areaspline' => [
                    'fillOpacity' => .9,
                    'marker' => [
                        'enabled' => false,
                        'symbol' => 'circle',
                    ],
                    'dataLabels' => [
                        'enabled' => true
                    ],
                ],
                'series' => [
                    'stickyTracking' => false,
                ]
            ],
        ],
    ]); ?>
</div>
