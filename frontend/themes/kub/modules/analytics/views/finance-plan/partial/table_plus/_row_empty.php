<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;

/** @var $quarters array */
/** @var $_cellIds array */
/** @var $_dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $trClass string */
/** @var $level int */
/** @var $year int */

$isFirstColumnFixed = $isFirstColumnFixed ?? true;
?>
<tr class="empty-row">
    <td class="<?=($isFirstColumnFixed ? 'fixed-column':'')?>"></td>
    <?php foreach ($quarters as $quarter => $monthes): ?>
        <?php foreach ($monthes as $month): ?>
            <td class="nowrap <?= $_dMonthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-month="<?=($month)?>" data-collapse-cell></td>
        <?php endforeach; ?>
        <td class="quarter-block nowrap <?= $_dMonthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $_cellIds[$quarter] ?>" data-collapse-cell-total></td>
    <?php endforeach; ?>
    <td class="nowrap total-block"></td>
</tr>