<?php

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use frontend\modules\analytics\models\financePlan\FinancePlanExpenditureItem as ExpenditureItem;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\ArrayHelper;

/**
 * @var FinancePlanGroup $model
 * @var FinancePlan $activePlan
 */

$relationTypesList = [
    ExpenditureItem::RELATION_TYPE_UNSET => 'Не зависит',
    ExpenditureItem::RELATION_TYPE_REVENUE => 'Выручка',
    ExpenditureItem::RELATION_TYPE_SALES_COUNT => 'Количество продаж',
];

$item = new ExpenditureItem;
?>

<div id="edit-variable-cost-item-panel" class="finance-plan-float-panel wrap wrap_padding_none" style="display: none">
    <div class="ml-3 mr-3 p-2">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-edit-variable-cost',
            'action' => Url::to(['edit-expense-item-ajax', 'group' => $model->id, 'plan' => $activePlan->id, 'row_type' => FinancePlan::ROW_TYPE_EXPENSE_VARIABLE]),
            'method' => 'POST',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{input}\n{error}",
            ]
        ]);
        ?>

        <?= Html::hiddenInput('attr', null, ['class' => 'attr']); ?>
        <?= Html::hiddenInput('row_id', null, ['class' => 'row_id']); ?>
        <?= Html::hiddenInput('is_deleted', null, ['class' => 'is_deleted']); ?>

        <div style="display: flex">

            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px;">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Добавить строку
                </span>
                <div class="toggled-element">
                    <?= $form->field($item, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                        'loadAssets' => false,
                        'exclude' => [
                            'items' => ArrayHelper::getColumn($activePlan->expenditureItems, 'expenditure_item_id')
                        ],
                        'options' => [
                            'id' => 'financeplanexpenditureitem-expenditure_item_id',
                            'class' => 'flow-expense-items',
                            'prompt' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'placeholder' => '',
                        ]
                    ]); ?>
                    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                        'inputId' => 'financeplanexpenditureitem-expenditure_item_id',
                    ]) ?>                    
                    
                </div>
                <div class="toggled-element" style="display: none">
                    <?= Html::input('text', null, '_exists_item_id', [
                        'id' => 'financeplanexpenditureitem-exists_expenditure_item_id',
                        'class' => 'form-control',
                        'disabled' => true,
                    ]) ?>
                </div>
            </div>
            <div class="_p_1" style="width: 10%; min-width: 315px; margin-right: 16px; ">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Зависит от строки
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => 'Зависит от строки',
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= $form->field($item, 'relation_type')->widget(Select2::class, [
                    'hideSearch' => true,
                    'data' => $relationTypesList,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]) ?>
            </div>
            <div class="_p_1" style="width: 5%; min-width: 150px; margin-right: 16px; position: relative">
                <span class="label w-700" style="text-align: center; height: 18px;">
                    Умножить на
                    <?= Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'title' => 'Умножить на',
                        'title-as-html' => 1
                    ]) ?>
                </span>
                <?= $form->field($item, 'action', [
                    'options' => [
                        'style' => 'width: 100%;',
                        'autocomplete' => 'off',
                    ],
                ])->textInput()
                ?>
                <?= Html::tag('div', null, [
                    'class' => 'action-symbol',
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_UNSET => '',
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_REVENUE => '%',
                    'data-relation-' . ExpenditureItem::RELATION_TYPE_SALES_COUNT => $model->getCurrencySymbol(),
                    'style' => 'position: absolute; top: 37px; right: -22px; font-size: 18px;'
                ]) ?>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-center">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button mr-2',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'cancel-edit-variable-cost-js button-clr button-width button-regular button-hover-transparent ml-1 mr-2',
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Удалить', [
                'class' => 'delete-variable-cost-js button-clr button-width button-regular button-hover-transparent ml-1',
                'style' => 'width: 130px!important; display:none',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php Modal::begin([
    'id' => 'confirm-delete-variable-cost',
    'title' => 'Вы уверены, что хотите удалить эту строку?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'id' => 'confirm-delete-variable-cost-js',
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>