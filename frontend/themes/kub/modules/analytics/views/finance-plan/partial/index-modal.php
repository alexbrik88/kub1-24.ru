<?php

use frontend\modules\analytics\models\financePlan\FinancePlan;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\View;

/** @var $model \frontend\modules\analytics\models\financePlan\FinancePlanGroup */

$planModel = new FinancePlan([
    'model_group_id' => $model->id
]);
?>

<!-- Create / Update Plans Group -->
<div id="add-finance-model-group" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">
                <?= ($model->id ? 'Редактировать' : 'Добавить') . ' финансовую модель' ?>
            </h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?= $this->render('_model_group_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
</div>

<!-- Create / Update Plan -->
<div id="add-finance-plan" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">
                <?= ($planModel->id ? 'Редактировать' : 'Добавить') . ' направление' ?>
            </h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?= $this->render('_model_form', ['groupModel' => $model, 'model' => $planModel]) ?>
            </div>
        </div>
    </div>
</div>

<!-- Confirm delete -->
<?php Modal::begin([
    'id' => 'many-delete',
    'title' => 'Вы уверены, что хотите удалить выбранные модели?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
        'data-url' => Url::to(['many-delete']),
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<!-- Confirm delete plan -->
<?php Modal::begin([
    'id' => 'confirm-delete-plan',
    'title' => 'Вы уверены, что хотите удалить направление?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'id' => 'confirm-delete-plan-yes',
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
            'data-deleted_plan_id' => null
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>

<!-- Confirm update -->
<?php Modal::begin([
    'id' => 'confirm-update-finance-model',
    'title' => 'Внимание! Изменение периода финмодели может привести к безвозвратной потере данных. Вы можете воспользоваться функцией копирования модели. Продолжить?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
        'data-title' => 'Внимание! Изменение периода финмодели может привести к безвозвратной потере данных. Вы можете воспользоваться функцией копирования модели. Данные с «[fromDate]» по «[toDate]» будут удалены! Продолжить?',
    ],
]); ?>
<div class="text-center">
    <?= Html::a('Да', null, [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-confirm-update-yes',
        'data-form-id' => '#form-finance-model',
        'data-dismiss' => 'modal',
    ]); ?>
    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1 modal-confirm-update-no',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<!-- Confirm copy -->
<?php Modal::begin([
    'id' => 'copy-model',
    'title' => 'Вы уверены, что хотите скопировать финансовую модель?',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <div class="text-center">
        <?= Html::a('Да', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-confirm-copy-yes',
            'data-url' => Url::to(['many-delete']),
        ]); ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS

    _toggleFinancePlanCloneField = function() {
        const modal = $('#add-finance-plan');
        const wrap = $(modal).find('.wrap-copy-finance-plan');
        const masterSelect = $(modal).find('#financeplan-industry_id');
        const slaveSelect = $(modal).find('#financeplan-cloned_plan_id');
        
        slaveSelect.val(null).trigger('change');
        
        if (slaveSelect.find('option').filter('[data-industry="' + masterSelect.val() + '"]').length) {
            wrap.slideDown();
        } else {
            wrap.slideUp();
        }
    };

    $('#add-finance-plan').on('shown.bs.modal', function() {
        _toggleFinancePlanCloneField();
    });
    
    $('#financeplan-industry_id').on('change', function() {
        _toggleFinancePlanCloneField();
    });

JS);

if ($model->id) {

    $this->registerJs(<<<JS
    $('#form-finance-model').on('submit', function() {
        const oldDateFrom = new Date(Date.parse($('#old-date-from').val()));
        const oldDateTill = new Date(Date.parse($('#old-date-till').val()));
        const newDateFrom = new Date(Date.parse($('#financeplangroup-datefrom').val()));
        const newDateTill = new Date(Date.parse($('#financeplangroup-datetill').val()));
        const confirmModal = $('#confirm-update-finance-model');
        const confirmModalLabel = $('#confirm-update-finance-model-label');
        const confirmInput = $('#is-update-confirmed');
        const isConfirmed = $(confirmInput).val();
        
        let title = $(confirmModal).attr('data-title');
        
        if (isConfirmed)
            return true;
                
        if (newDateFrom > oldDateFrom) {
            newDateFrom.setMonth(newDateFrom.getMonth() - 1);
            $(confirmModalLabel).text(
                title
                .replace('[fromDate]', oldDateFrom.toLocaleString('ru', {year: 'numeric', month: 'long'}))
                .replace('[toDate]', newDateFrom.toLocaleString('ru', {year: 'numeric', month: 'long'}))
            );
            $('#confirm-update-finance-model').modal('show');
            return false;
        }
        
        if (newDateTill < oldDateTill) {
            newDateTill.setMonth(newDateTill.getMonth() + 1);
            $(confirmModalLabel).text(
                title
                .replace('[fromDate]', newDateTill.toLocaleString('ru', {year: 'numeric', month: 'long'}))
                .replace('[toDate]', oldDateTill.toLocaleString('ru', {year: 'numeric', month: 'long'}))
            );
            $('#confirm-update-finance-model').modal('show');
            return false;
        }
        
        return true;
    });
    $('.modal-confirm-update-yes').on('click', function() {
        const formID = $(this).data('form-id');
        const confirmInput = $('#is-update-confirmed');
        $(confirmInput).val(1);
        $(formID).submit();
    });
    $('.modal-confirm-update-no').on('click', function() {
        Ladda.stopAll();
    });
JS, View::POS_READY);

}