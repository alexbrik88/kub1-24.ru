<?php

namespace frontend\modules\crm\views;

use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper;
use Yii;
use yii\bootstrap4\Html;
use common\models\employee\Employee;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanTableHelper as TableHelper;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanGroupTableHelper as TotalTableHelper;
use yii\widgets\Pjax;

/** @var Employee $employee */
/** @var FinancePlanGroup $model */
/** @var FinancePlan $activePlan */

///////////////////////////////////////
$IS_EDIT_MODE = $IS_EDIT_MODE ?? false;
///////////////////////////////////////

$this->title = 'Финмодель ' . $model->name;
$userConfig = $employee->config;
$renderParams = [
    'IS_EDIT_MODE' => $IS_EDIT_MODE,
    'model' => $model,
    'activePlan' => $activePlan,
    'employee' => $employee,
    'userConfig' => $userConfig,
];

$groupPanelDataJS = FinancePlanAutofillHelper::getGroupPanelDataJS($model, $IS_EDIT_MODE);

echo Html::a('Назад к списку', ['index'], ['class' => 'link mb-2', 'id' => 'never-stop-ladda-button']);

Pjax::begin([
    'id' => 'pjax-finance-plan-table',
    'linkSelector' => '.nav-link',
    'enablePushState' => !$IS_EDIT_MODE,
    'scrollTo' => false
]);

    echo $this->render('partial/view-head', $renderParams);

    echo $this->render('partial/view-nav', $renderParams);

    echo $this->render('partial/view-table', array_merge($renderParams, [
        'groupExpenses' => TotalTableHelper::getGroupExpenses($model)
    ]));

    if (Yii::$app->request->isPjax): ?>
        <script>
            FinancePlans.panelData.groupAutofill.expense = <?= $groupPanelDataJS['groupExpenseAutofill'] ?>;
        </script>
    <? endif;

Pjax::end();

echo $this->render('partial/index-modal', $renderParams);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);
?>