<?php

namespace frontend\modules\crm\views;

use Yii;
use yii\web\View;
use yii\widgets\Pjax;
use yii\bootstrap4\Html;
use common\models\employee\Employee;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\assets\FinancePlanAsset;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper as AutofillHelper;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanTableHelper as TableHelper;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanGroupTableHelper as TotalTableHelper;

/** @var string $tab */
/** @var Employee $employee */
/** @var FinancePlanGroup $model */
/** @var FinancePlan $activePlan */
/** @var FinancePlanChart $modelChart */
/** @var array $itemsByFlowOfFundsType */

FinancePlanAsset::register($this);

//////////////////////////////////////////
$IS_EDIT_MODE = $IS_EDIT_MODE ?? false;
//////////////////////////////////////////

$this->title = 'Финмодель ' . $model->name;
$userConfig = $employee->config;
$renderParams = [
    'IS_EDIT_MODE' => $IS_EDIT_MODE,
    'tab' => $tab,
    'model' => $model,
    'activePlan' => $activePlan,
    'employee' => $employee,
    'userConfig' => $userConfig,
    'modelChart' => $modelChart ?? null
];

$panelDataJS = AutofillHelper::getPanelDataJS($activePlan, $IS_EDIT_MODE);
$groupPanelDataJS = AutofillHelper::getGroupPanelDataJS($model, $IS_EDIT_MODE);
$itemsByFlowOfFundsTypeJS = TableHelper::getItemsByFlowOfFundsTypeJS();

$tableParams = null;
switch ($tab) {
    case FinancePlanGroup::TAB_EXPENSES:
        $tableParams = [
            'groupExpenses' => TotalTableHelper::getGroupExpenses($model)
        ];
        break;
    case FinancePlanGroup::TAB_PLAN:
    default:
        $tableParams = [
            'salesFunnel' => ($activePlan->isTotal)
                ? TotalTableHelper::getSalesFunnel($activePlan)
                : TableHelper::getSalesFunnel($activePlan),
            'profitAndLoss' => ($activePlan->isTotal)
                ? TotalTableHelper::getProfitAndLoss($activePlan)
                : TableHelper::getProfitAndLoss($activePlan),
            'odds' => ($activePlan->isTotal)
                ? TotalTableHelper::getOdds($activePlan)
                : TableHelper::getOdds($activePlan),
        ];
        break;
}

echo Html::a('Назад к списку', ['index'], ['class' => 'link mb-2', 'id' => 'never-stop-ladda-button']);

echo $this->render('partial/view-head', $renderParams);

Pjax::begin([
    'id' => 'pjax-finance-plan-table',
    'linkSelector' => '.nav-link',
    'enablePushState' => !$IS_EDIT_MODE,
    'scrollTo' => false
]);

    echo $this->render('partial/view-nav', $renderParams);

    echo $this->render('partial/view-table', array_merge(
        $renderParams, $tableParams
    ));

    if (Yii::$app->request->isPjax): ?>
        <script>
            FinancePlans.panelData.autofill.income = <?= $panelDataJS['incomeAutofill'] ?>;
            FinancePlans.panelData.autofill.expense = <?= $panelDataJS['expenseAutofill'] ?>;
            FinancePlans.panelData.primeCost = <?= $panelDataJS['primeCost'] ?>;
            FinancePlans.panelData.variableCost = <?= $panelDataJS['variableCost'] ?>;
            FinancePlans.panelData.fixedCost = <?= $panelDataJS['fixedCost'] ?>;
            FinancePlans.panelData.taxCost = <?= $panelDataJS['taxCost'] ?>;
            FinancePlans.panelData.odds.income = <?= $panelDataJS['oddsIncomeAutofill'] ?>;
            FinancePlans.panelData.odds.expense = <?= $panelDataJS['oddsExpenseAutofill'] ?>;
            FinancePlans.panelData.groupAutofill.expense = <?= $groupPanelDataJS['groupExpenseAutofill'] ?>;
            FinancePlans.panelData.oddsSystem.income = <?= $panelDataJS['oddsSystemIncome'] ?>;
            FinancePlans.panelData.oddsSystem.expense = <?= $panelDataJS['oddsSystemExpense'] ?>;
            FinancePlans.panelData.otherIncome = <?= $panelDataJS['otherIncome'] ?>;
            FinancePlans.panelData.otherExpense = <?= $panelDataJS['otherExpense'] ?>;
        </script>
    <? endif;

Pjax::end();

if ($IS_EDIT_MODE) {
    echo $this->render('partial/credit/_modal_credit', $renderParams);
    echo $this->render('partial/balance_article/_modal', $renderParams);
} else {
    // operations with group modal
    echo $this->render('partial/index-modal', $renderParams);
}


echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

$this->registerJs("
    FinancePlans.init({
        groupID: {$model->id},
        tabID: '{$tab}',
        planID: ".(int)$activePlan->id.",
        panelData: {
            autofill: {
                income: {$panelDataJS['incomeAutofill']},
                expense: {$panelDataJS['expenseAutofill']},
            },
            primeCost: {$panelDataJS['primeCost']},
            variableCost: {$panelDataJS['variableCost']},
            fixedCost: {$panelDataJS['fixedCost']},
            taxCost: {$panelDataJS['taxCost']},
            otherIncome: {$panelDataJS['otherIncome']},
            otherExpense: {$panelDataJS['otherExpense']},
            odds: {
                income: {$panelDataJS['oddsIncomeAutofill']},
                expense: {$panelDataJS['oddsExpenseAutofill']},
            },
            groupAutofill: {
                expense: {$groupPanelDataJS['groupExpenseAutofill']},
            },
            oddsSystem: {
                income: {$panelDataJS['oddsSystemIncome']},
                expense: {$panelDataJS['oddsSystemExpense']},
            },
        },
        select2Data: {
            odds: {
                itemsByFlowOfFundsType: {$itemsByFlowOfFundsTypeJS}
            }
        },
    });
", View::POS_READY);