<?php

namespace frontend\modules\acquiring\views;

use common\components\helpers\Html;
use frontend\modules\acquiring\models\MonetaCredentialsForm;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

/**
 * @var View $this
 * @var MonetaCredentialsForm $form
 */

?>

<div class="statement-service-content" style="position: relative; min-height: 110px;">
    <?= $this->render('_header') ?>

    <?php $widget = ActiveForm::begin([
        'id' => 'import-dialog-form',
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'action' => ['connect'],
        'options' => [
            'data-pjax' => true,
        ],
    ]) ?>

    <?= $widget->field($form, 'username') ?>

    <?= $widget->field($form, 'password')->passwordInput() ?>

    <div class="d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Подключить интеграцию</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
            'style' => 'width: 220px;',
        ]); ?>
        <?= Html::button('Отмена', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
            'title' => 'Отмена',
            'style' => 'width: 150px;',
        ]); ?>
    </div>

    <?php ActiveForm::end() ?>
</div>
