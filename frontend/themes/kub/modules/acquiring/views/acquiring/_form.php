<?php

namespace frontend\modules\acquiring\views;

use common\models\Contractor;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $this
 * @var AcquiringOperation $model
 */

$company = Yii::$app->user->identity->company;
$acquiringData = ArrayHelper::map(
    Acquiring::find()->andWhere(['type' => $model->acquiring_type])->all(),
    'identifier',
    'identifier'
);

$income = 'income' . ($model->flow_type == AcquiringOperation::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == AcquiringOperation::FLOW_TYPE_EXPENSE ? '' : ' hidden');

switch ($model->flow_type) {
    case AcquiringOperation::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $contractorType = Contractor::TYPE_CUSTOMER;
        break;
    case AcquiringOperation::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $contractorType = Contractor::TYPE_SELLER;
        break;
    default:
        $contractor = 'Контрагент';
        $contractorType = null;
        break;
}
$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' операцию';

$typeItems = $model->isNewRecord ? AcquiringOperation::getFlowTypes() : [$model->flow_type => AcquiringOperation::getFlowTypes()[$model->flow_type]];

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

<?php Pjax::begin([
    'id' => $model->isNewRecord ? 'add_emoney_flow_pjax' : 'update_emoney_flow_pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->id],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'id' => 'cash-emoney-form',
    'options' => [
        'class' => 'form-horizontal',
        'data' => [
            'pjax' => true,
        ]
    ],
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'flow_type')->label('Тип')->radioList($typeItems, [
            'encode' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($model) {

                $input = Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ]
                ]);

                return Html::a($input, ['create', 'type' => $value], [
                    'class' => 'col-xs-5 m-l-n',
                    'style' => 'padding: 0; color: #333333; text-decoration: none;'
                ]);
            }
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'acquiring_identifier')->widget(Select2::class, [
            'data' => $acquiringData,
            'options' => [
                'id' => 'acquiringoperation-acquiring_identifier-' . $model->id,
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%'
            ],
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id', [
            'options' => [
                'class' => 'form-group cash-contractor_input',
            ],
        ])->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => $contractorType,
            'options' => [
                'id' => 'acquiringoperation-contractor_id-' . $model->id,
                'class' => 'contractor-items-depend form-group ' . ($model->flow_type ? 'customer' : 'seller'),
                'placeholder' => '',
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label($contractor); ?>
    </div>

    <?php if ($model->flow_type == AcquiringOperation::FLOW_TYPE_EXPENSE): ?>
        <div class="col-6">
            <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'options' => [
                    'id' => 'acquiringoperation-expenditure_item_id-' . $model->id,
                    'class' => 'flow-expense-items',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ]); ?>
        </div>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'acquiringoperation-expenditure_item_id-',
        ]); ?>
    <?php endif; ?>

    <?php if ($model->flow_type == AcquiringOperation::FLOW_TYPE_INCOME): ?>
        <div class="col-6">
            <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'income' => true,
                'options' => [
                    'id' => 'acquiringoperation-income_item_id-' . $model->id,
                    'class' => 'flow-income-items',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ]); ?>
        </div>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'acquiringoperation-income_item_id-',
            'type' => 'income',
        ]); ?>
    <?php endif; ?>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'amount')->textInput([
            'value' => !empty($model->amount) ? $model->amount / 100 : '',
            'class' => 'form-control js_input_to_money_format',
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'date', [
                    'inputTemplate' => $inputCalendarTemplate,
                    'options' => ['class' => 'form-group'],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ])->label('Дата оплаты'); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'datetime')->widget(MaskedInput::class, [
                    'mask' => '9{2}:9{2}:9{2}',
                    'options' => [
                        'class' => 'form-control',
                        'value' => ($model->datetime) ? date('H:i:s', strtotime($model->datetime)) : date('H:i:s')
                    ],
                ])->label('Время оплаты'); ?>
            </div>
        </div>
    </div>
</div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php $form->end(); ?>

<?php Pjax::end(); ?>
<script type="text/javascript">
    var currentScript = document.currentScript || (function() {
        var scripts = document.getElementsByTagName("script");
        return scripts[scripts.length - 1];
    })();
    var currentModalContent = $(currentScript.parentNode).closest(".modal-content");

    if (currentModalContent) {
        currentModalContent.find(".modal-header > h1").html("<?= $header ?>");
    }
</script>
