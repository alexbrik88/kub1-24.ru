<?php

namespace frontend\modules\acquiring\views;

use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\project\Project;
use common\modules\acquiring\models\Acquiring;
use frontend\components\Icon;
use frontend\modules\cash\models\CashSearch;
use common\models\Contractor;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\modules\acquiring\widgets\MonetaButtonWidget;
use frontend\modules\acquiring\widgets\YookassaButtonWidget;
use frontend\themes\kub\widgets\AcquiringFilterWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\RangeButtonWidget;
use common\modules\acquiring\models\AcquiringOperation;
use common\components\date\DateHelper;
use frontend\modules\cash\widgets\StatisticWidget;
use common\components\grid\GridView;
use common\components\TextHelper;
use frontend\modules\cash\widgets\SummarySelectWidget;
use Yii;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;

/**
 * @var $this yii\web\View
 * @var $activeItem \common\modules\acquiring\models\Acquiring|null
 * @var $company Company
 * @var $searchModel \common\modules\acquiring\models\AcquiringOperationsSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $emptyMessage string
 */

$this->title = 'Интернет-эквайринг';

$connectText = $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить эквайринг</span>';
$buttonText = $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Импорт операций</span>';

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_acquiring');
$tabConfigClass = [
    'incomeExpense' => 'col_acquiring_column_income_expense' . ($userConfig->acquiring_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_acquiring_column_income_expense'  . (!$userConfig->acquiring_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_acquiring_column_income_expense' . (!$userConfig->acquiring_column_income_expense ? '' : ' hidden'),
    'project' => 'col_acquiring_column_project' . ($userConfig->acquiring_column_project ? '' : ' hidden'),
    'salePoint' => 'col_acquiring_column_sale_point' . ($userConfig->acquiring_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_acquiring_column_industry' . ($userConfig->acquiring_column_industry ? '' : ' hidden'),
];
?>
<div class="stop-zone-for-fixed-elems cash-bank-flows-index">
    <div class="page-head d-flex flex-wrap align-items-center">
        <?= AcquiringFilterWidget::widget([
            'pageTitle' => $this->title,
            'activeAcquiring' => $activeItem ? $activeItem->id : null,
            'company' => $company,
        ]); ?>

        <?php if ($activeItem && $activeItem->type == Acquiring::TYPE_MONETA): ?>
            <?= MonetaButtonWidget::widget([
                'cssClass' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                'content' => $buttonText,
                'identifier' => $activeItem->identifier,
                'hasImport' => true,
                'hasBlock' => false,
            ]) ?>
        <?php elseif ($activeItem && $activeItem->type == Acquiring::TYPE_YOOKASSA): ?>
            <?= YookassaButtonWidget::widget([
                'cssClass' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                'content' => $buttonText,
                'identifier' => $activeItem->identifier,
                'hasImport' => true,
                'hasBlock' => false,
            ]) ?>
        <?php else: ?>
            <?= Html::a($connectText, '#', [
                'class' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                'data-toggle' => 'modal',
                'data-target' => '#add-acquiring',
            ]) ?>
        <?php endif; ?>
    </div>

    <?php if ($activeItem === null): ?>
        <div class="wrap wrap_count" style="font-size: 18px;padding: 12px;">
            Для подключения загрузки данных по продажам на вашем сайте, нажмите на кнопку "Импорт операций"
            и выберите нужного оператора по интернет-эквайрингу
        </div>
    <?php endif; ?>

    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $searchModel,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/modules/cash/views/default/_partial/table-filter', [
        'searchModel' => $searchModel,
        'filterUrl' => 'acquiring',
        'tableView' => [
            'attribute' => 'table_view_cash'
        ],
        'tableConfig' => [
            [
                'attribute' => 'acquiring_column_income_expense',
                'invert_attribute' => 'invert_acquiring_column_income_expense'
            ],
            [
                'attribute' => 'acquiring_column_project'
            ],
            [
                'attribute' => 'acquiring_column_sale_point'
            ],
            [
                'attribute' => 'acquiring_column_industry'
            ],
        ]
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading line-height-1em',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $typeCss = 'income-item';
                        $income = round($data['amount'] / 100, 2);
                        $expense = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $income : 0;
                    } else {
                        $typeCss = 'expense-item';
                        $expense = round($data['amount'] / 100, 2);
                        $income = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $expense : 0;
                    }

                    return Html::checkbox("flowId[{$data['tb']}][]", false, [
                        'class' => 'joint-operation-checkbox ' . $typeCss,
                        'value' => $data['id'],
                        'data' => [
                            'income' => $income,
                            'expense' => $expense,
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amountIncomeExpense',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => $tabConfigClass['incomeExpense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $income = '+ ' . TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                    $expense = '- ' . TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);

                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        @list($from, $to) = explode('_', $flows['transfer_key']);
                        if ($from == $flows['id'])
                            return Html::tag('div', CashSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        if ($to == $flows['id'])
                            return Html::tag('div', CashSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                    } else {
                        if ($flows['amountIncome'] > 0) {
                            return Html::tag('div', CashSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                        }
                        if ($flows['amountExpense'] > 0) {
                            return Html::tag('div', CashSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        }
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'amountIncome',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => $tabConfigClass['income'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'amountExpense',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'contractor_ids',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['is_internal_transfer'])
                        return '';

                    if ($data['contractor_id'] > 0 && ($contractor = Contractor::findOne($data['contractor_id'])) !== null) {
                        return Html::a(Html::encode($contractor->nameWithType), [
                            '/contractor/view',
                            'type' => $contractor->type,
                            'id' => $contractor->id,
                        ], ['target' => '_blank', 'title' => $contractor->nameWithType]);
                    }

                    return '---';
                },
                'filter' => ['' => 'Все контрагенты'] + $searchModel->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '250px'
            ],
            [
                's2width' => '200px',
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => $tabConfigClass['project'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
                ],
                'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                        return Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => $tabConfigClass['salePoint'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
                ],
                'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                        return Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => $tabConfigClass['companyIndustry'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
                ],
                'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                        return Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'purpose-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    if ($data['description']) {
                        $description = mb_substr($data['description'], 0, 50) . '<br>' . mb_substr($data['description'], 50, 50);

                        return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'reason_ids',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'clause-cell',
                ],
                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->reasonFilterItems),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function ($data) {

                    $reason = $data['flow_type'] == AcquiringOperation::FLOW_TYPE_INCOME ?
                        (($item = \common\models\document\InvoiceIncomeItem::findOne($data['income_item_id'])) ?
                            $item->fullName : "id={$data['income_item_id']}") :
                        (($item = \common\models\document\InvoiceExpenditureItem::findOne($data['expenditure_item_id'])) ?
                            $item->fullName : "id={$data['expenditure_item_id']}");

                    return $reason ? Html::tag('span', $reason, ['title' => htmlspecialchars($reason)]) : '-';

                },
                's2width' => '200px'
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '2%',
                ],
                'contentOptions' => [
                    'class' => 'text-nowrap',
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'urlCreator' => function ($action, $data, $key, $index, $actionColumn) {
                    if ($action == 'update' && $data['is_internal_transfer']) {
                        $action = 'update-internal';
                    }
                    $params = [
                        'id' => $data['id'],
                        'is_plan_flow' => ($data['wallet_id'] == 'plan') ? '1' : ''
                    ];
                    $params[0] = $actionColumn->controller ? $actionColumn->controller . '/' . $action : $action;

                    return Url::toRoute($params);
                },
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $options = [
                            'class' => 'button-clr link mr-1 update-flow-item',
                            'title' => 'Изменить',
                            'data' => [
                                'title' => 'Изменить операцию по кассе',
                                'pjax' => 0,
                                'toggle' => 'modal',
                                'target' => '#update-movement'
                            ],
                        ];

                        return Html::a(Icon::get('pencil'), $url, $options);
                    },
                    'delete' => function ($url) {
                        return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                            'theme' => 'gray',
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'button-clr link',
                                'tag' => 'button',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

    <div class="modal fade" id="update-movement" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title">Изменить операцию</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_partial/modal-many-delete'); ?>
    <?= $this->render('_partial/modal-many-item', [
        'model' => $searchModel,
    ]); ?>

    <?= SummarySelectWidget::widget([
        'buttons' => [
            Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
            ]),
            Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
                'class' => 'button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
            ]),
        ],
    ]); ?>
</div>

<?= ImportDialogWidget::widget() ?>

<?php $this->registerJs('
    $(document).on("show.bs.modal", "#update-movement", function(event) {
        $(".alert-success").remove();
        if (event.target.id === "update-movement") {
            $(this).find(".modal-body").empty();
            $(this).find("#js-modal_update_title").empty();
        }
    });

    $(document).on("hide.bs.modal", "#update-movement", function(event) {
        if (event.target.id === "update-movement") {
            $("#update-movement .modal-body").empty();
        }
    });

    // SHOW_AJAX_MODAL
    $("#update-movement").on("show.bs.modal", function (e) {
    
        var button = $(e.relatedTarget);
        var modal = $(this);
    
        if ($(button).attr("href"))
            modal.find(".modal-body").load(button.attr("href"));
    });

    // REFRESH_UNIFORMS
    $(document).on("shown.bs.modal", "#update-movement", function() {
        refreshUniform();
        refreshDatepicker();
    });

    // SEARCH
    $("input#acquiringoperationssearch-contractor_name").on("keydown", function(e) {
      if(e.keyCode == 13) {
        e.preventDefault();
        $("#cash_bank_filters").submit();
      }
    });
    
    refreshUniform();
    
    $(document).on("shown.bs.modal", "#many-item", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
    
        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        $(".joint-operation-checkbox:checked").each(function() {
            $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
        });
    });
    $(document).on("hidden.bs.modal", "#many-item", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
    });
    $(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
        var l = Ladda.create($(this).find(".btn-save")[0]);
        var $hasError = false;
    
        if (l) {
            l.start();
        }
        $("field-acquiringoperationssearch--incomeitemidmanyitem:visible, .field-acquiringoperationssearch--expenditureitemidmanyitem:visible").each(function () {
            $(this).removeClass("has-error");
            $(this).find(".help-block").text("");
            if ($(this).find("select").val() == "") {
                $hasError = true;
                $(this).addClass("has-error");
                $(this).find(".help-block").text("Необходимо заполнить.");
            }
        });
        if ($hasError) {
            return false;
        }
    });
');