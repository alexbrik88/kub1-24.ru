<?php

namespace frontend\modules\acquiring\views;

use common\modules\acquiring\models\AcquiringOperation;
use Yii;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var AcquiringOperation $model
 */

?>
<div class="cash-emoney-flows-update">
    <?php if (!Yii::$app->request->isAjax): ?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif; ?>

    <?= $this->render('_form', compact('model')); ?>
</div>
