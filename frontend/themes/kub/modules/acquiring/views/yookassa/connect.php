<?php

namespace frontend\modules\acquiring\views;

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var string $redirectUrl
 */

?>

<div class="row mb-5">
    <div class="col-5">
        <div class="logo-border text-center">
            <img class="img-fluid dialog-logo" src="/img/yookassa/integration.png" alt="" style="max-height: 200px;">
        </div>
    </div>
    <div class="col-7">
        <div class="bank-form-notify p-4">
            Для обеспечения безопасности данных используется
            протокол зашифрованного соединения. <br>
            SSL - надежный протокол для передачи конфиденциальной
            банковской информации и соблюдаются требования
            международного стандарта PCI DSS по хранению и передаче
            конфиденциальной информации в банковской сфере.
        </div>
    </div>
</div>

<div class="row">
    <div class="col-4">
        <?= Html::a('Подтвердить интеграцию', Url::to(['/acquiring/yookassa/code', 'redirectUrl' => $redirectUrl]), [
            'class' => 'button-clr button-regular button-regular_red w-100',
        ]) ?>
    </div>
    <div class="col-8 text-right">
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-clr button-width',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
</div>
