<?php

namespace frontend\modules\acquiring\views;

use frontend\modules\acquiring\models\ImportForm;
use frontend\themes\kub\widgets\ImportFormWidget;
use yii\web\View;

/**
 * @var View $this
 * @var ImportForm $form
 * @var string $disconnectUrl
 * @var string $identifier
 */

echo ImportFormWidget::widget([
    'form' => $form,
    'disconnectUrl' => $disconnectUrl,
    'identifier' => $identifier,
    'headerContent' => $this->render('_header'),
]);
