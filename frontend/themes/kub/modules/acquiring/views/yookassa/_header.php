<?php

namespace frontend\modules\acquiring\views;

use yii\web\View;

/**
 * @var View $this
 */

?>
<h4 class="modal-title">Импорт операций из ЮKassa</h4>
<div style="margin: 0 -5px 10px;">
    <div class="cont-img_bank-logo" style="float: left;">
        <img src="/img/yookassa/integration.png" style="width: 130px;">
    </div>
    <div id="statement-bank-info" style="margin-left: 150px; padding: 5px;">
        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee;">
            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
            - надежный протокол для передачи конфиденциальной банковской информации
            и соблудаются требования международного стандарта PCI DSS по хранению и передаче
            конфиденциальной информации в банковской сфере.
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<hr>
