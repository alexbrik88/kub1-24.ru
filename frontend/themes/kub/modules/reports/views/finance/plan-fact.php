<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 0:33
 */

use common\components\helpers\Html;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Url;
use frontend\modules\reports\models\PlanFactSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PlanFactSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $checkMonth boolean
 */
$this->title = 'План-Факт (ПФ)';
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-deviation',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_plan_fact_help ?? false;
$showChartPanel = $userConfig->report_plan_fact_chart ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list disabled button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'disabled' => true,
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= Html::beginForm(['plan-fact', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                        'disabled' => true,
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы)<br>
        в компанию и их возврат. <br>
        Выплата дивидендов.<br>
        Предоставление займов и депозитов.
    </span>
    <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
    </span>
    <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br>
        Затраты на новые проекты и поступление выручки от них. <br>
    </span>
    <span id="item-plan-tooltip_template" style="display: inline-block;">
        <span class="pull-left" style="min-width: 100px;">Факт</span>
        <span class="pull-right fact-amount"></span><br>

        <span class="pull-left" style="min-width: 100px;">План</span>
        <span class="pull-right plan-amount"></span><br>

        <span class="pull-left text-bold" style="min-width: 100px;">Разница</span>
        <span class="pull-right diff-amount text-bold"></span><br>

        <span class="pull-left text-bold deviation-text" style="min-width: 100px;">Отклонение</span>
        <span class="pull-right deviation text-bold"></span><br>
    </span>
    <span id="tooltip_deviation_block" class="text-center" style="display: inline-block;">
        Допустимое отклонение <br>
        Плана от Факта
    </span>
</div>

<?= $this->render('_to_analytics') ?>
