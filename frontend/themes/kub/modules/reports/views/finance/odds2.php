<?php

use frontend\modules\reports\models\OddsSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $warnings []
 * @var $activeTab integer
 * @var $periodSize string
 */

$this->title = 'Отчет о Движении Денежных Средств';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil($currentMonthNumber / 3);

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_odds_help ?? false;
$showChartPanel = $userConfig->report_odds_chart ?? false;
$isFreeTariff = Yii::$app->user->identity->company->isFreeTariff;
$collapseTriggers = implode(',', [
    '[data-collapse-trigger]',
    '[data-collapse-row-trigger]',
    '[data-collapse-all-trigger]',
    '[data-collapse-trigger-days]',
]);
$this->registerJs(<<<JS
    $(document).on('dataCollapseTriggerEnd', '{$collapseTriggers}', function() {
        let table = $(this).closest('table');
        let rowspan = 0,
            colspanLeft = 0,
            colspanRight = 0;
        $('.rowspan_row', table).each(function (i) {
            if ($(this).height() > 0) {
                rowspan++;
            }
        });
        $('.colspan_l_col', table).each(function (i) {
            if ($(this).width() > 0) {
                colspanLeft++;
            }
        });
        $('.colspan_r_col', table).each(function (i) {
            if ($(this).width() > 0) {
                colspanRight++;
            }
        });
        $('.rowspan_cell', table).attr('rowspan', rowspan);
        $('.colspan_l_cell', table).attr('colspan', colspanLeft);
        $('.colspan_r_cell', table).attr('colspan', colspanRight);
    });
JS
);
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2">Отчёт о Движении Денежных Средств</h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= Html::beginForm(['odds', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>
</div>

<?php if ($isFreeTariff) : ?>
    <?= $this->render('_to_analytics') ?>
<?php else : ?>
    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_odds_chart">
        <div class="pt-4 pb-3 text-center">
            <?= $this->render('_partial/_to_analytics') ?>
        </div>
    </div>

    <div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_odds_help">
        <div class="pt-4 pb-3">
            <?= $this->render('_partial/odds-panel') ?>
        </div>
    </div>

    <div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">
        <div class="d-flex flex-nowrap">
            <div class="radio_join mb-2">
                <?= Html::a('По видам деятельности', Url::current(['activeTab' => OddsSearch::TAB_ODDS]), [
                    'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == OddsSearch::TAB_ODDS ? ' button-regular_red' : null),
                ]); ?>
            </div>
            <div class="radio_join mb-2">
                <?= Html::a('ОДДС по кошелькам', Url::current(['activeTab' => OddsSearch::TAB_ODDS_BY_PURSE]), [
                    'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE ? ' button-regular_red' : null),
                ]); ?>
            </div>

            <?php /*if (YII_ENV_DEV || !$isFreeTariff): ?>
                <div class="ml-1 mr-1">
                    <?= Html::a(Icon::get('exel'), ['/reports/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year], [
                        'class' => 'download-odds-xls button-list button-hover-transparent button-clr ml-2 mb-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
                </div>
            <?php endif;*/ ?>

            <div class="ml-2">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_odds']) ?>
            </div>
        </div>
        <div class="d-flex flex-nowrap ml-auto">
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'days') ? 'button-regular_red' : '' ?> pl-4 pr-4 mb-0" href="<?= Url::current(['periodSize' => 'days']) ?>">День</a>
            </div>
            <div class="radio_join mb-2">
                <a class="button-regular <?= ($periodSize == 'months') ? 'button-regular_red' : '' ?> pl-3 pr-3 mb-0" href="<?= Url::current(['periodSize' => 'months']) ?>">Месяц</a>
            </div>
        </div>
    </div>

    <div class="wrap wrap_padding_none_all" style="margin-bottom: 12px; position: relative">
        <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
        <?= Html::hiddenInput('periodSize', $periodSize, ['id' => 'active-period-size']); ?>

        <?= $this->render('_partial/odds2_table' . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'userConfig' => $userConfig,
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'floorMap' => []
        ]); ?>

    </div>

    <?= $this->render('_partial/item_table', [
        'searchModel' => $searchModel,
    ]); ?>
    <?php if (!empty($warnings)): ?>
        <?= $this->render('_partial/odds-warnings', [
            'warnings' => $warnings,
        ]); ?>
    <?php endif; ?>

    <div class="tooltip-template" style="display: none;">
        <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
            Привлечение денег (кредиты, займы)<br>
            в компанию и их возврат. <br>
            Выплата дивидендов.<br>
            Предоставление займов и депозитов.
        </span>
        <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
                Движение денег, связанное с основной деятельностью компании <br>
                (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
        </span>
        <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
                Покупка и продажа оборудования и других основных средств. <br>
                Затраты на новые проекты и поступление выручки от них. <br>
        </span>
    </div>
    <div id="hellopreloader" style="display: none;">
        <div id="hellopreloader_preload"></div>
    </div>
    <div id="visible-right-menu" style="display: none;">
        <div id="visible-right-menu-wrapper"></div>
    </div>

    <script>

        FactItemTable = {
            init: function () {
                FactItemTable.bindEvents();
            },
            bindEvents: function() {
                // delete
                $(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
                    let l = Ladda.create(this);
                    l.start();

                    $.get($(this).data('url'), null, function(data) {
                        FactItemTable.reloadAll(function() {
                            Ladda.stopAll();
                            l.remove();
                            $('.modal:visible').modal('hide');
                            FactItemTable.showFlash(data.msg);
                        });
                    });

                    return false;
                });

                //update
                $(document).on('submit', '#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form', function(e) {
                    e.preventDefault();
                    var submitBtn = this.querySelector('button[type="submit"]');

                    $(this).prepend('<input type="hidden" name="fromOdds" value="1">');

                    $.post($(this).attr('action'), $(this).serialize(), function (data) {
                        let l = Ladda.create(submitBtn);
                        l.start();
                        FactItemTable.reloadAll(function() {
                            Ladda.stopAll();
                            l.remove();
                            $('.modal:visible').modal('hide');
                            FactItemTable.showFlash('Операция обновлена.'); // todo: show errors
                        });
                    });

                    return false;
                });

                // many delete
                $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                    let $this = $(this);
                    let l = Ladda.create(this);
                    l.start();

                    if (!$this.hasClass('clicked')) {
                        if ($('.joint-checkbox:checked').length > 0) {
                            $this.addClass('clicked');
                            $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {
                                FactItemTable.reloadAll(function() {
                                    Ladda.stopAll();
                                    l.remove();
                                    $('.modal:visible').modal('hide');
                                    FactItemTable.showFlash(data.msg);
                                    $this.removeClass('clicked');
                                });
                            });
                        }
                    }
                    return false;
                });

                // many item (update articles)
                $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                    e.preventDefault();

                    var l = Ladda.create($(this).find(".btn-save")[0]);
                    var $hasError = false;

                    l.start();
                    $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                        $(this).removeClass("has-error");
                        $(this).find(".help-block").text("");
                        if ($(this).find("select").val() == "") {
                            $hasError = true;
                            $(this).addClass("has-error");
                            $(this).find(".help-block").text("Необходимо заполнить.");
                        }
                    });

                    if ($hasError) {
                        Ladda.stopAll();
                        l.remove();
                        return false;
                    }

                    $.post($(this).attr('action'), $(this).serialize(), function (data) {
                        FactItemTable.reloadAll(function() {
                            $('.modal:visible').modal('hide');
                            FactItemTable.showFlash(data.msg);
                            Ladda.stopAll();
                            l.remove();
                        });
                    });

                    return false;
                });

                $(document).on("shown.bs.modal", "#many-item", function () {
                    var $includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
                    var $includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
                    var $modal = $(this);
                    var $header = $modal.find(".modal-header h1");
                    var $additionalHeaderText = null;

                    if ($includeExpenditureItem) {
                        $(".expenditure-item-block").removeClass("hidden");
                    }
                    if ($includeIncomeItem) {
                        $(".income-item-block").removeClass("hidden");
                    }
                    if ($includeExpenditureItem && $includeIncomeItem) {
                        $additionalHeaderText = " прихода / расхода";
                    } else if ($includeExpenditureItem) {
                        $additionalHeaderText = " расхода";
                    } else if ($includeIncomeItem) {
                        $additionalHeaderText = " прихода";
                    }
                    $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
                    $(".joint-checkbox:checked").each(function() {
                        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                    });
                });

                $(document).on("hidden.bs.modal", "#many-item", function () {
                    $(".expenditure-item-block").addClass("hidden");
                    $(".income-item-block").addClass("hidden");
                    $(".additional-header-text").remove();
                    $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
                });

            },
            reloadAll: function(callback)
            {
                let debugCurrTime = Date.now();
                return FactItemTable._reloadChart().done(function() {
                    console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                    FactItemTable._reloadSubTable().done(function() {
                        console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        FactItemTable._reloadMainTable().done(function() {
                            console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                            if (typeof callback === "function") {
                                return callback();
                            }

                        }).catch(function() { FactItemTable.showFlash('Ошибка сервера #1') });
                    }).catch(function() { FactItemTable.showFlash('Ошибка сервера #2') });
                }).catch(function() { FactItemTable.showFlash('Ошибка сервера #3') });
            },
            _reloadSubTable: function()
            {
                const $year = "OddsSearch%5Byear%5D=" + $('#oddssearch-year').val();
                $itemIDs['activeTab'] = $('#active-tab_report').val(); // todo: var from custom.js

                return jQuery.pjax({
                    url: "/reports/finance/item-list?" + $year,
                    type: 'POST',
                    data: {'items': JSON.stringify($itemIDs)},
                    container: '#odds-items_pjax',
                    timeout: 10000,
                    push: false,
                    scrollTo: false
                });
            },
            _reloadMainTable: function ()
            {
                let floorMap = {};
                $('table.flow-of-funds').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-days]').each(function(i,v) {
                    floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
                });
                let data = {
                    // OddsSearch: {year: $('#OddsSearch-year').val()},
                    floorMap: floorMap
                };
                let activeTab = $('#active-tab_report').val();
                let periodSize = $('#active-period-size').val();

                return $.post('/reports/finance-ajax/odds-part/?activeTab=' + activeTab + '&periodSize=' + periodSize, data, function ($data) {
                    let html = new DOMParser().parseFromString($data, "text/html");
                    let table = html.querySelector('table.flow-of-funds tbody');
                    let origTable = document.querySelector('table.flow-of-funds tbody');
                    origTable.innerHTML = table.innerHTML;

                    if ($coloredItemsCoords) {
                        $.each($coloredItemsCoords, function(i,v) {
                            $('table.flow-of-funds tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                        });
                    }

                    FactItemTable._bindMainTableEvents();
                });
            },
            _reloadChart: function() {
                return window.ChartPlanFactDays.redrawByClick();
            },
            _bindMainTableEvents: function() {

                // main.js
                $('[data-collapse-row-trigger]', 'table.flow-of-funds tbody').click(function() {
                    var target = $(this).data('target');
                    $(this).toggleClass('active');
                    if ( $(this).hasClass('active') ) {
                        $('[data-id="'+target+'"]').removeClass('d-none');
                    } else {
                        // level 1
                        $('[data-id="'+target+'"]').addClass('d-none');
                        $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+target+'"]').each(function(i, row) {
                            // level 2
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {
                                $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                            });
                        });
                    }
                    if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                        $('[data-collapse-all-trigger]').removeClass('active');
                    } else {
                        $('[data-collapse-all-trigger]').addClass('active');
                    }
                });
            },
            showFlash: function(text) {
                window.toastr.success(text, "", {
                    "closeButton": true,
                    "showDuration": 1000,
                    "hideDuration": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 1000,
                    "escapeHtml": false
                });
            },
        };

        /////////////////////
        FactItemTable.init();
        /////////////////////

        // COLLAPSES
        $("#chartCollapse").on("show.bs.collapse", function() {
            $("#helpCollapse").collapse("hide");
            $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
        });
        $("#chartCollapse").on("hide.bs.collapse", function() {
            $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
        });
        $("#helpCollapse").on("show.bs.collapse", function() {
            $("#chartCollapse").collapse("hide");
            $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
        });
        $("#helpCollapse").on("hide.bs.collapse", function() {
            $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
        });
    </script>

    <?php if ($periodSize == 'days') {
    $this->registerJs(<<<JS
    var _activeMCS = 0;
    var _offsetMCSLeft = 0;
    var _firstCSTableColumn = $('#cs-table-first-column');

    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS == 1)
                    window._activeMCS = 0;

                if (this.mcs.left == 0 && $(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: true,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                this.scrollInertia = 1000;
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: true,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $("[data-collapse-trigger-days]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 250);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).data('columns-count') || 3;

            $(collapseBtn).toggleClass('active');
            $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            if ( $(collapseBtn).hasClass('active') ) {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
            } else {
                $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
            }
            $(collapseBtn).closest('.custom-scroll-table').mCustomScrollbar("update");
        };

        _collapseToggle(collapseBtn);
        $(this).trigger('dataCollapseTriggerEnd');
    });

    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width());
        $("#cs-table-2").mCustomScrollbar("update");
    });

JS
    );
    } ?>

<?php endif ?>

<?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);