<?php

use common\models\service\ServiceModule;
use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

?>

<div class="wrap mt-3 text-center">
    <div>
        <?= Icon::get('block', [
            'style' => 'font-size: 100px;',
        ]) ?>
    </div>
    <div class="mt-5">
        Данный отчет доступен только
        в блоке ФинДиректор
    </div>
    <div class="mt-5">

        <?php /* Modal::begin([
            'id' => 'to-analytics-modal',
            'title' => 'Блок Бизнес Аналитики будет доступен с 1 августа 2020 года',
            'closeButton' => [
                'label' => Icon::get('close'),
                'class' => 'modal-close close',
            ],
            'toggleButton' => [
                'label' => 'Перейти в Бизнес Аналитику',
                'class' => 'button-regular button-hover-transparent px-3',
            ],
        ]) ?>
            <div class="mt-5 text-center">
                <?= Html::button('OK', [
                    'class' => 'button-regular button-hover-transparent px-3',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        <?php Modal::end() */ ?>

        <?php echo Html::a('Перейти в ФинДиректор', [
            '/analytics/options/start',
        ], [
            'class' => 'button-regular button-hover-transparent px-3',
        ]) ?>
    </div>
</div>