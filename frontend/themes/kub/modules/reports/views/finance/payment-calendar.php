<?php

use frontend\modules\reports\models\ExpensesSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Url;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\reports\models\PlanCashFlows;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $hasFlows bool
 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

$this->title = 'Платежный календарь';
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_pc_help ?? false;
$showChartPanel = $userConfig->report_pc_chart ?? false;
$subTab = Yii::$app->request->get('subTab');
$tableShowBy = ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? 'payment_calendar_by_activity' : 'payment_calendar_by_purse');

$subTabAutoplan = $subTabTable = $subTabRegister = false;
if ($subTab == 'register') {
    $subTabRegister = true;
} else if ($subTab == 'autoplan') {
    $subTabAutoplan = true;
} else {
    $subTabTable = true;
}

if (Yii::$app->request->get('show_add_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            let url = $(".add-operation").data("url");
            $("#add-movement").find(".modal-body").load(url);
            $("#add-movement").modal("show");
            window.history.pushState("object", document.title, location.href.split("?")[0]);
        });
    ');
}
?>

<div class="mb-2 d-flex flex-wrap align-items-center">
    <button class="add-operation disabled button-regular button-regular_red button-width ml-auto"
        data-toggle="modal" href="#add-movement" disabled
        data-url="<?= Url::to(['create-plan-item', 'activeTab' => $activeTab, 'subTab' => $subTab, 'year' => $searchModel->year]) ?>">
        <?= \frontend\components\Icon::get('add-icon') ?>
        <span class="ml-2">Добавить</span>
    </button>
</div>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip2 disabled button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")'),
                        'disabled' => true,
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip2 disabled button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")'),
                        'disabled' => true,
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['payment-calendar', 'activeTab' => $activeTab], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getRealFlowsYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                        'disabled' => true,
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_pc_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/payment-calendar-panel')
        ?>
    </div>
</div>

<?= \yii\bootstrap4\Nav::widget([
    'id' => 'payment-calendar-menu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-2'],
    'items' => [
        [
            'label' => 'Платежный календарь',
            'url' => Url::current(['subTab' => 'table']),
            'active' => !$subTab || $subTab == 'table',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => [
                'class' => 'nav-link disabled',
                'disabled' => true,
            ],
        ],
        [
            'label' => 'Реестр плановых операций',
            'url' => Url::current(['subTab' => 'register']),
            'active' => $subTab == 'register',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => [
                'class' => 'nav-link disabled',
                'disabled' => true,
            ],
        ],
        [
            'label' => 'АвтоПланирование',
            'url' => Url::current(['subTab' => 'autoplan']),
            'active' => $subTab == 'autoplan',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => [
                'class' => 'nav-link disabled',
                'disabled' => true,
            ],
        ],
    ],
]);
?>

<?= $this->render('_to_analytics') ?>
