<?php
/** @var $model \frontend\modules\reports\models\IncomeSearch */
//////////////////////////////////// TOP CHARTS /////////////////////

// consts top charts
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 20;
$cropNamesLength = 12;
$croppedNameLength = 10;

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customPurse = $customPurse ?? null;
/////////////////////////////////////////

if (!isset($dataBuyers) || !isset($dataProducts)) {
    $dataBuyers = $model->getChartStructureByBuyers($customMonth, $customPurse);
    $dataProducts = $model->getTopProductSeriesData($customMonth);
}

// arrays top charts
$topChart1 = [
    'categories' => array_slice(array_column($dataBuyers, 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataBuyers, 'amountFact'), 0, $chartMaxRows),
    'planData' => array_slice(array_column($dataBuyers, 'amountPlan'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataBuyers, 'amountFact')),
];

$topChart2 = [
    'categories' => array_slice(array_column($dataProducts['topProducts'], 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataProducts['topProducts'], 'amount'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataProducts['topProducts'], 'amount')),
];

$topChart3 = [
    'categories' => array_slice(array_column($dataProducts['topEmployers'], 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($dataProducts['topEmployers'], 'amount'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($dataProducts['topEmployers'], 'amount')),
];

if (empty($topChart1['categories'])) {
    $topChart1 = [
        'categories' => ["Нет приходов"],
        'factData' => [0],
        'planData' => [0],
        'totalAmount' => 0,
    ];
}
if (empty($topChart2['categories'])) {
    $topChart2 = [
        'categories' => ["Нет приходов"],
        'factData' => [0],
        'totalAmount' => 0,
    ];
}
if (empty($topChart3['categories'])) {
    $topChart3 = [
        'categories' => ["Нет приходов"],
        'factData' => [0],
        'totalAmount' => 0,
    ];
}
$topChart1['calculatedChartHeight'] = count($topChart1['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$topChart2['calculatedChartHeight'] = count($topChart2['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$topChart3['calculatedChartHeight'] = count($topChart3['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
///////////////////////////////////////////////////////////////////////////

if (Yii::$app->request->post('chart-income-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'top1' => [
            'chartHeight' => $topChart1['calculatedChartHeight'],
            'totalAmount' => $topChart1['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart1['categories']],
                'series' => [
                    ['data' => $topChart1['planData']],
                    ['data' => $topChart1['factData']],
                ],
            ],
        ],
        'top2' => [
            'chartHeight' => $topChart2['calculatedChartHeight'],
            'totalAmount' => $topChart2['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart2['categories']],
                'series' => [['data' => $topChart2['factData']]],
            ],
        ],
        'top3' => [
            'chartHeight' => $topChart3['calculatedChartHeight'],
            'totalAmount' => $topChart3['totalAmount'],
            'optionsChart' => [
                'xAxis' => ['categories' => $topChart3['categories']],
                'series' => [['data' => $topChart3['factData']]],
            ],
        ],        
    ]);

    exit;
}

$topChartConsts = [
    'chartWrapperHeight' => $chartWrapperHeight,
    'chartHeightHeader' => $chartHeightHeader,
    'chartHeightFooter' => $chartHeightFooter,
    'chartHeightColumn' => $chartHeightColumn,
    'chartMaxRows' => $chartMaxRows,
    'cropNamesLength' => $cropNamesLength,
    'croppedNameLength' => $croppedNameLength
];
?>

<?php if (isset($row) && $row == 1): ?>

    <?=
    // Row 1: show only employers chart
    $this->render('_chart_income_top_3', array_merge($topChartConsts, [
        'searchModel' => $model,
        'categories' => $topChart3['categories'],
        'factData' => $topChart3['factData'],
        'calculatedChartHeight' => $topChart3['calculatedChartHeight'],
    ])); ?>

<?php else: ?>

    <?php // Row 2: show full charts line ?>
    <div class="pt-4 pb-3" style="margin-top: 0px;">
        <div class="row">
            <div class="col-4 pr-2">
                <?= $this->render('_chart_income_top_1', array_merge($topChartConsts, [
                    'searchModel' => $model,
                    'categories' => $topChart1['categories'],
                    'planData' => $topChart1['planData'],
                    'factData' => $topChart1['factData'],
                    'calculatedChartHeight' => $topChart1['calculatedChartHeight'],
                ])); ?>
            </div>
            <div class="col-4 pl-2 pr-2">
                <?= $this->render('_chart_income_top_2', array_merge($topChartConsts, [
                    'searchModel' => $model,
                    'categories' => $topChart2['categories'],
                    'factData' => $topChart2['factData'],
                    'calculatedChartHeight' => $topChart2['calculatedChartHeight'],
                ])); ?>
            </div>
            <div class="col-4 pl-2">
                <?= $this->render('_chart_income_projects', [
                    'model' => $model,
                ]);
                ?>
            </div>
        </div>
    </div>

<?php endif; ?>

<script>
    // MOVE CHART
    window.ChartIncomeTop = {
        chart: 'top',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        byPurse: null,
        byArticle: null,
        articlesIds: {},
        _inProcess: false,
        Top1: {
            chartHeight: "<?= $topChart1['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart1['totalAmount'] ?>",
            chartPoints: {}
        },
        Top2: {
            chartHeight: "<?= $topChart2['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart2['totalAmount'] ?>",
            chartPoints: {}
        },
        Top3: {
            chartHeight: "<?= $topChart3['calculatedChartHeight'] ?>",
            totalAmount: "<?= $topChart3['totalAmount'] ?>",
            chartPoints: {}
        },
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

        },
        redrawByClick: function() {

            return window.ChartIncomeTop._getData().done(function() {
                $('#chart-top-1').highcharts().update(ChartIncomeTop.Top1.chartPoints);
                $('#chart-top-1').highcharts().update({"chart": {"height": ChartIncomeTop.Top1.chartHeight + 'px'}});
                $('#chart-top-2').highcharts().update(ChartIncomeTop.Top2.chartPoints);
                $('#chart-top-2').highcharts().update({"chart": {"height": ChartIncomeTop.Top2.chartHeight + 'px'}});
                $('#chart-top-3').highcharts().update(ChartIncomeTop.Top3.chartPoints);
                $('#chart-top-3').highcharts().update({"chart": {"height": ChartIncomeTop.Top3.chartHeight + 'px'}});
                window.ChartIncomeTop._inProcess = false;
            });
        },
        _getData: function() {
            window.ChartIncomeTop._inProcess = true;
            return $.post('/reports/finance-ajax/get-income-charts-data', {
                    "chart-income-ajax": true,
                    "purse": ChartIncomeTop.byPurse,
                    "chart": ChartIncomeTop.chart,
                    "year": ChartIncomeTop.year,
                    "month": ChartIncomeTop.month,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartIncomeTop.Top1.chartPoints = data.top1.optionsChart;
                    ChartIncomeTop.Top1.chartHeight = data.top1.chartHeight;
                    ChartIncomeTop.Top1.totalAmount = data.top1.totalAmount;
                    ChartIncomeTop.Top2.chartPoints = data.top2.optionsChart;
                    ChartIncomeTop.Top2.chartHeight = data.top2.chartHeight;
                    ChartIncomeTop.Top2.totalAmount = data.top2.totalAmount;
                    ChartIncomeTop.Top3.chartPoints = data.top3.optionsChart;
                    ChartIncomeTop.Top3.chartHeight = data.top3.chartHeight;
                    ChartIncomeTop.Top3.totalAmount = data.top3.totalAmount;
                }
            );
        },
    };

    /////////////////////////////
    window.ChartIncomeTop.init();
    /////////////////////////////

</script>