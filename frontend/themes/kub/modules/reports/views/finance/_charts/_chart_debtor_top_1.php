<?php

use common\components\TextHelper;
use frontend\modules\reports\components\DebtsHelper;

?>
<div class="ht-caption">
    <?= ($type == 2) ? 'НАМ ДОЛЖНЫ' : 'МЫ ДОЛЖНЫ' ?>
</div>
<table class="ht-table">
    <tr>
        <td class="gray bold size15">Текущая задолженность</td>
        <td class="bold nowrap size15"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL, false, true, null, $type); ?></td>
    </tr>
    <tr>
        <td class="red bold">Просрочка дней</td>
        <td></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">0-10 дней<i class="ht-empty left"></i></span></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, null, $type); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">11-30 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, null, $type); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">31-60 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, null, $type); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">61-90 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, null, $type); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr class="bb">
        <td><span class="ht-empty-wrap">Больше 90 дней<i class="ht-empty left"></i></td>
        <td class="nowrap"><span class="ht-empty-wrap"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, null, $type); ?><i class="ht-empty right"></i></span></td>
    </tr>
    <tr>
        <td class="red bold">Просроченая задолженность</td>
        <td class="nowrap red bold"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_OVERDUE, false, true, null, $type) ?></td>
    </tr>
    <tr>
        <td class="gray bold size15">ИТОГО задолженность</td>
        <td class="bold nowrap size15">
            <?= TextHelper::invoiceMoneyFormat(
                DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false, null, $type) +
                DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_ACTUAL, false, false, null, $type), 2) ?>
        </td>
    </tr>
</table>