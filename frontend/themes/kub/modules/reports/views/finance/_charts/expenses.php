<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 16:18
 */

use \miloschuman\highcharts\Highcharts;
use common\components\helpers\Month;
use frontend\modules\reports\models\ExpensesSearch;

/* @var $searchModel ExpensesSearch
 * @var $activeTab integer
 */
?>
<div class="charts-expenses row">
    <div class="col-7 expenses-chart">
        <?= Highcharts::widget([
            'id' => 'expenses-chart-1',
            'scripts' => [
                //'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'spacing' => [10,10,10,10],
                    'height' => '200px',
                    'events' => [
                        'load' => new \yii\web\JsExpression('function () {
                            var titleX = +$(".expenses-chart").width() - +$(".expenses-chart .highcharts-plot-border").width() - 12;
                            
                            $(".expenses-chart .highcharts-title").attr("x", titleX);
                        }'),
                    ],
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],
                'tooltip' => [
                    'shared' => true,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                ],
                'title' => [
                    'text' => 'Расходы по оплате',
                    'align' => 'left',
                    'floating' => true,
                    'style' => [
                        'font-size' => '12px',
                        'color' => '#9198a0',
                    ],
                    'x' => 0,
                ],
                'yAxis' => [
                    'min' => 0,
                    'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                ],
                'xAxis' => [
                    'categories' => array_values(Month::$monthShort),
                    'minorGridLineWidth' => 0,
                ],
                'series' => [
                    [
                        'name' => 'Факт',
                        'data' => $searchModel->getFlowAmountByPeriod(),
                        'color' => 'rgba(250,192,49,1)'
                    ],
                    [
                        'name' => 'План',
                        'data' => $searchModel->getFlowAmountByPeriod(true),
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(3,20,30,1)',
                            'radius' => 10
                        ],
                        'type' => 'spline',
                        'lineWidth' => 0,
                        'states' => [
                            'hover' => [
                                'lineWidthPlus' => 0,
                            ],
                        ],
                    ]
                ],
            ],
        ]); ?>
    </div>
    <div class="structure-chart col-5">
        <?= Highcharts::widget([
            'id' => 'expenses-chart-2',
            'scripts' => [
                //'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'spacing' => [10,10,10,10],
                    'height' => '200px',
                    'inverted' => true,
                    'events' => [
                        'load' => new \yii\web\JsExpression('function () {
                            var titleX = +$(".structure-chart").width() - +$(".structure-chart .highcharts-plot-border").width() - 12;
                            
                            $(".structure-chart .highcharts-title").attr("x", titleX);
                        }'),
                    ],
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],
                'plotOptions' => [
                    'column' => [
                        'dataLabels' => [
                            'enabled' => true,
                        ],
                        'grouping' => false,
                        'shadow' => false,
                        'borderWidth' => 0
                    ]
                ],
                'tooltip' => [
                    'shared' => true,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                ],
                'title' => [
                    'text' => 'Структура',
                    'align' => 'left',
                    'floating' => true,
                    'style' => [
                        'font-size' => '12px',
                        'color' => '#9198a0',
                    ],
                    'x' => 0,
                ],
                'yAxis' => [
                    'min' => 0,
                    'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                ],
                'xAxis' => [
                    'categories' => array_values($searchModel->getMaxItemsFlow(true)),
                    'minorGridLineWidth' => 0,

                ],
                'series' => [
                    [
                        'name' => 'Факт',
                        'pointPadding' => 0,
                        'data' => $searchModel->getFlowAmountByItems(),
                        'color' => 'rgba(99,132,225,1)',
                    ],
                    [
                        'name' => 'План',
                        'pointPadding' => 0,
                        'data' => $searchModel->getFlowAmountByItems(true),
                        'color' => 'rgba(226,229,236,.9)',
                    ]
                ]
            ],
        ]); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };
    });
</script>