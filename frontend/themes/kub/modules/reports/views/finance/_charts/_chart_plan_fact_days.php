<?php

use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\themes\kub\components\Icon;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use frontend\modules\reports\models\AbstractFinance;

$ON_PAGE = $ON_PAGE ?? 'PK';
$userConfig = Yii::$app->user->identity->config;
$attr = ($ON_PAGE == 'ODDS') ? 'report_odds_chart_period' : 'report_pc_chart_period';

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = $customPeriod ?? ($userConfig->$attr == 0 ? "days" : "months");
$customPurse = $customPurse ?? null;
/////////////////////////////////////////

/* @var $model \frontend\modules\reports\models\PaymentCalendarSearch */
$model = new PaymentCalendarSearch();

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$SHOW_PLAN = $SHOW_PLAN ?? true;
$LEFT_DATE_OFFSET = 10;
$RIGHT_DATE_OFFSET = 10;
$MOVE_OFFSET = 7;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $LEFT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $RIGHT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

elseif ($customPeriod == "days") {

    $datePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $monthPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($monthPrev != $month && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                'next' => Month::$monthFullRU[$month]
            ];
            // emulate align right
            switch ($month) {
                case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
            }
        }

        $monthPrev = $month;
        
        $daysPeriods[] = $day;
        $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
        $chartPlanFactLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);
        if ($CENTER_DATE == $date['from'])
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$mainData = $model->getPlanFactSeriesData($datePeriods, $customPurse, $customPeriod);
$prevMainData = $model->getPlanFactSeriesData($prevYearDatePeriods, $customPurse, $customPeriod);
$incomeFlowsFactPlan = &$mainData['incomeFlowsFact'];
$outcomeFlowsFactPlan = &$mainData['outcomeFlowsFact'];
$incomeFlowsPlan = &$mainData['incomeFlowsPlan'];
$outcomeFlowsPlan = &$mainData['outcomeFlowsPlan'];
$balanceFact = &$mainData['balanceFact'];
$balanceFactPrevYear = &$prevMainData['balanceFact'];
$balancePlanCurrMonth = &$mainData['balancePlanCurrMonth'];
$balancePlan = [];

if ($currDayPos >= 0 && $currDayPos < 9999) {
    // chart1
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $incomeFlowsFactPlan[$i] = $incomeFlowsPlan[$i];
        $outcomeFlowsFactPlan[$i] = $outcomeFlowsPlan[$i];
        $incomeFlowsPlan[$i] = null;
        $outcomeFlowsPlan[$i] = null;
    }

    // chart2: connect plan/fact edges
    for ($i = 0; $i < $currDayPos; $i++) {
        $balancePlan[$i] = null;
    }
    $balancePlan[$currDayPos] = $balanceFact[$currDayPos];
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $balanceFact[$i] = null;
        $balancePlan[$i] = $balancePlan[$i - 1] + ($incomeFlowsFactPlan[$i] - $outcomeFlowsFactPlan[$i]);
        // curr month
        if ($i == $currDayPos + 1) {
            $balancePlan[$i] += $balancePlanCurrMonth;
        }
    }
} elseif ($currDayPos == -1) {
    for ($i = 0; $i < count($datePeriods); $i++) {
        $balancePlan[$i] = null;
    }
    $balancePlan[0] = 0; // fix highcharts bug
}
elseif ($currDayPos === 9999) {
    $balancePlan[0] = $model->getChartPlanBalanceFuturePoint($datePeriods[0]['from'], $customPurse);
    for ($i = 1; $i < count($datePeriods); $i++) {
        $balancePlan[$i] = $balancePlan[$i - 1] + ($incomeFlowsPlan[$i] - $outcomeFlowsPlan[$i]);
    }
    for ($i = 0; $i < count($datePeriods); $i++) {
        $balanceFact[$i] = null;
        $incomeFlowsFactPlan[$i] = $incomeFlowsPlan[$i];
        $outcomeFlowsFactPlan[$i] = $outcomeFlowsPlan[$i];
    }
    $balanceFact[$i] = 0; // fix highcharts bug
    $incomeFlowsPlan = $outcomeFlowsPlan = [];
}

//////////////// TOOLTIP ///////////////////////
$arrIncome = [];
$arrOutcome = [];
$arrIncomeTotal = [];
$arrOutcomeTotal = [];
$arrDate = [];
if ($customPeriod == "days") {
    $inTooltipMaxRows = 5;
    $tooltipDateFrom = $datePeriods[0]['from'];
    $tooltipDateTo = $datePeriods[count($datePeriods) - 1]['to'];
    $incomeTooltipData = $model->getTooltipStructureByContractors($tooltipDateFrom, $tooltipDateTo, $customPurse, CashFlowsBase::FLOW_TYPE_INCOME);
    $outcomeTooltipData = $model->getTooltipStructureByContractors($tooltipDateFrom, $tooltipDateTo, $customPurse, CashFlowsBase::FLOW_TYPE_EXPENSE);

    foreach ($datePeriods as $pos => $date) {
        // let isPlanPeriod = (idx > window.ChartPlanFactDays.currDayPos && window.ChartPlanFactDays.currDayPos != -1) || window.ChartPlanFactDays.currDayPos == 9999;
        $amountKey = (($pos > $currDayPos && $currDayPos != -1) || $currDayPos == 9999) ? 'amountPlan' : 'amountFact';
        $countKey = (($pos > $currDayPos && $currDayPos != -1) || $currDayPos == 9999) ? 'countPlan' : 'countFact';

        $arrIncome[$pos] = null;
        $arrOutcome[$pos] = null;
        $arrIncomeTotal[$pos] = null;
        $arrOutcomeTotal[$pos] = null;
        $arrDate[$pos] = null;

        if ($income = ArrayHelper::getValue($incomeTooltipData, $date['from'])) {
            $maxAmount = max(array_column($income, $amountKey)) ?: 9E9;
            $row = 0;
            foreach ($income as $i) {
                if ($i[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
                $arrIncome[$pos][] = [
                    'name' => $i['name'],
                    'percent' => round($i[$amountKey] / $maxAmount * 100),
                    'sum' => $i[$amountKey]
                ];
            }
            $total = array_sum(array_column($income, $countKey));
            $arrIncomeTotal[$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
        }

        if ($outcome = ArrayHelper::getValue($outcomeTooltipData, $date['from'])) {
            $maxAmount = max(array_column($outcome, $amountKey));
            $row = 0;
            foreach ($outcome as $o) {
                if ($o[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
                $arrOutcome[$pos][] = [
                    'name' => $o['name'],
                    'percent' => round($o[$amountKey] / $maxAmount * 100),
                    'sum' => $o[$amountKey]
                ];
            }
            $total = array_sum(array_column($outcome, $countKey));
            $arrOutcomeTotal[$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
        }

        $dateArr = explode('-', $date['from']);
        $year = $dateArr[0];
        $month = $dateArr[1];
        $day = $dateArr[2];
        $arrDate[$pos] = (int)$day . ' ' . mb_strtolower(ArrayHelper::getValue(Month::$monthGenitiveRU, substr($date['from'], 5, 2)));
    }

}
////////////////////////////////////////////////

if (Yii::$app->request->post('chart-plan-fact-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $incomeFlowsFactPlan,
                ],
                [
                    'data' => $outcomeFlowsFactPlan,
                ],
                [
                    'data' => $incomeFlowsPlan,
                ],
                [
                    'data' => $outcomeFlowsPlan,
                ]
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $balanceFactPrevYear,
                ],
                [
                    'data' => $balancePlan,
                ],
                [
                    'data' => $balanceFact,
                ],
            ],
        ],
        'tooltipData' => [
            'jsArrIncome' => $arrIncome,
            'jsArrOutcome' => $arrOutcome,
            'jsArrIncomeTotal' => $arrIncomeTotal,
            'jsArrOutcomeTotal' => $arrOutcomeTotal,
            'jsArrDate' => $arrDate,
        ]
    ]);

    exit;
}

?>
<style>
    #chart-plan-fact { height: 213px; }
    #chart-plan-fact-2 { height: 213px; }
    #chart-plan-fact  .highcharts-axis-labels,
    #chart-plan-fact-2 .highcharts-axis-labels { z-index: -1!important; }
    /* tooltip */
    #chart-plan-fact, #chart-plan-fact .highcharts-container , #chart-plan-fact svg {
        overflow: visible!important;
        z-index: 1!important;
    }
</style>

<div style="position: relative">
    <div style="width: 100%">

        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="left" style="position: absolute; left:40px; top:212px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="right" style="position: absolute; right:0px; top:212px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; top:450px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:0px; top:450px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect mb-2">
            Приход-Расход
            <div class="wrap-select2-no-padding ml-1" style="float:right; margin-top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'id' => 'chart-plan-fact-purse-type',
                    'name' => 'purseType',
                    'data' => [
                        '' => 'Итого',
                        AbstractFinance::CASH_BANK_BLOCK => 'Банк',
                        AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
                        AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '106px'
                    ]
                ]); ?>
            </div>
            <ul class="nav nav-tabs" role="tablist" style="border-bottom: none; float: right; margin-top:-1px;">
                <li class="nav-item pr-2">
                    <a class="chart-plan-fact-days-tab nav-link <?= $customPeriod == 'days' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="days" data-toggle="tab">День</a>
                </li>
                <li class="nav-item pr-2">
                    <a class="chart-plan-fact-days-tab nav-link <?= $customPeriod == 'months' ? 'active':'' ?> pt-0 pb-0" href="javascript:;" data-period="months" data-toggle="tab">Месяц</a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>

        <div style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartPlanFactDays.redrawByLoad()')
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => $SHOW_PLAN ? new jsExpression("function(args) { return window.ChartPlanFactDays.formatter.call(this, args); }") : NULL,
                        'positioner' => new jsExpression("function(boxWidth, boxHeight, point) { return window.ChartPlanFactDays.positioner.call(this, boxWidth, boxHeight, point); }"),
                        //'hideDelay' => 99999,
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() { 
                                        let result = (this.pos == window.ChartPlanFactDays.currDayPos) ? 
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                            (window.ChartPlanFactDays.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (window.ChartPlanFactDays.wrapPointPos) {
                                            result += window.ChartPlanFactDays.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Приход Факт',
                            'data' => $incomeFlowsFactPlan,
                            'color' => $color1,
                            'borderColor' => 'rgba(46,159,191,.3)',
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Расход Факт',
                            'data' => $outcomeFlowsFactPlan,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Приход План',
                            'data' => $incomeFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(21,67,96,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => -0.225,
                            'stickyTracking' => false,
                            'visible' => $SHOW_PLAN,
                            'showInLegend' => $SHOW_PLAN
                        ],
                        [
                            'name' => 'Расход План',
                            'data' => $outcomeFlowsPlan,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' => 'rgba(50,50,50,1)',
                                'radius' => 10.22
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0.22,
                            'stickyTracking' => false,
                            'visible' => $SHOW_PLAN,
                            'showInLegend' => $SHOW_PLAN
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'groupPadding' => 0.05,
                            'pointPadding' => 0.1,
                            'borderRadius' => 3,
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div class="ht-caption noselect">
            Остаток денег
        </div>
        <div style="min-height:125px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-2',
                'scripts' => [
                   // 'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => $SHOW_PLAN ?
                            new jsExpression("
                                function(args) {
                                    let index = this.series.data.indexOf( this.point );
                                    let series_index = this.series.index;
                                    let factColor = args.chart.series[2].data[index].y >= 0 ? 'green-text' : 'red-text';
                                    let factColorB = args.chart.series[2].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                                    let planColor = args.chart.series[1].data[index].y >= 0 ? 'green-text' : 'red-text';
                                    let planColorB = args.chart.series[1].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                                    
                                    let toggleFactLines = args.chart.series[0].data[index].y > args.chart.series[2].data[index].y; 
                                    let togglePlanLines = args.chart.series[0].data[index].y > args.chart.series[1].data[index].y;

                                    let titleLine = '<span class=\"title\">' + window.ChartPlanFactDays.labelsX[this.point.index] + '</span>';
                                    let factLine = '<tr>' + '<td class=\"' + factColor + '\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"' + factColorB + '\">' + Highcharts.numberFormat(args.chart.series[2].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';
                                    let planLine = '<tr>' + '<td class=\"' + planColor + '\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"' + planColorB + '\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';
                                    let prevYearLine = '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';

                                    if ((index > window.ChartPlanFactDays.currDayPos && window.ChartPlanFactDays.currDayPos != -1) || window.ChartPlanFactDays.currDayPos == 9999) {
                                        return titleLine + '<table class=\"indicators\">' + (togglePlanLines ? (prevYearLine + planLine) : (planLine + prevYearLine)) + '</table>';
                                    }
                                        
                                    return titleLine + '<table class=\"indicators\">' + (toggleFactLines ? (prevYearLine + factLine) : (factLine + prevYearLine)) + '</table>';
                                }
                            ") : NULL

                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        'min' => 1,
                        'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                        'categories' => $daysPeriods,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("
                                function() { 
                                    result = (this.pos == window.ChartPlanFactDays.currDayPos) ? 
                                        ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                        (window.ChartPlanFactDays.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
                                        
                                    if (window.ChartPlanFactDays.wrapPointPos) {
                                        result += window.ChartPlanFactDays.getWrapPointXLabel(this.pos);
                                    }                                        
                                         
                                    return result; 
                                }"),
                            'useHTML' => true,
                            'autoRotation' => false
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток Факт (предыдущий год)',
                            'data' => $balanceFactPrevYear,
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 2
                        ],
                        [
                            'name' => 'Остаток План',
                            'data' => $balancePlan,
                            'color' => 'rgba(26,184,93,1)',
                            'negativeColor' => 'red',
                            'fillColor' => [
                                'pattern' => [
                                    //'image' => '/img/pattern1.png',
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#27ae60',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'negativeFillColor' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#e74c3c',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'marker' => [
                                'symbol' => 'square'
                            ],
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 1,
                            'visible' => $SHOW_PLAN,
                            'showInLegend' => $SHOW_PLAN
                        ],
                        [
                            'name' => 'Остаток Факт',
                            'data' => $balanceFact,
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>

    </div>
</div>

<?php
$htmlHeaderFact = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
        <tr>
            <th>Факт</th>
            <th></th>
            <th>{fact.y} ₽</th>
        </tr>
HTML;
$htmlHeaderPlan = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
HTML;
$htmlData = <<<HTML
        <tr>
            <td><div class="ht-title">{name}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart {color_css}" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum} ₽</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
    <table class="ht-in-footer-table">
        <tr>
            <td><strong>Всего {total}</strong></td>
        </tr>
    </table>
HTML;

$htmlHeaderFact = str_replace(["\r", "\n", "'"], "", $htmlHeaderFact);
$htmlHeaderPlan = str_replace(["\r", "\n", "'"], "", $htmlHeaderPlan);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);
?>

<script>

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });

    // MOVE CHART
    window.ChartPlanFactDays = {
        isLoaded: false,
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        byPurse: null,
        _inProcess: false,
        tooltipData: {
            jsArrIncome: <?= json_encode($arrIncome) ?>,
            jsArrOutcome: <?= json_encode($arrOutcome) ?>,
            jsArrIncomeTotal: <?= json_encode($arrIncomeTotal) ?>,
            jsArrOutcomeTotal: <?= json_encode($arrOutcomeTotal) ?>,
            jsArrDate: <?= json_encode($arrDate) ?>,
        },
        tooltipTemplate: {
            htmlHeaderFact: '<?= $htmlHeaderFact ?>',
            htmlHeaderPlan: '<?= $htmlHeaderPlan ?>',
            htmlData: '<?= $htmlData ?>',
            htmlFooter: '<?= $htmlFooter ?>',
        },
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-plan-fact-days-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartPlanFactDays._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left')
                    window.ChartPlanFactDays.offset[ChartPlanFactDays.period] -= <?= $MOVE_OFFSET ?>;
                if ($(this).data('move') === 'right')
                    window.ChartPlanFactDays.offset[ChartPlanFactDays.period] += <?= $MOVE_OFFSET ?>;

                window.ChartPlanFactDays.redrawByClick();
            });

            $('.chart-plan-fact-days-tab').on('show.bs.tab', function (e) {
                ChartPlanFactDays.period = $(this).data('period');
                ChartPlanFactDays.redrawByClick();
            });

            $('#chart-plan-fact-purse-type').on('change', function() {
                window.ChartPlanFactDays.byPurse = $(this).val();
                window.ChartPlanFactDays.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartPlanFactDays._getData().done(function() {
                $('#chart-plan-fact-2').highcharts().update(ChartPlanFactDays.chartPoints2);
                $('#chart-plan-fact').highcharts().update(ChartPlanFactDays.chartPoints);
                window.ChartPlanFactDays._redrawPlanMonths();
                window.ChartPlanFactDays._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartPlanFactDays.isLoaded)
                return;

            let chartToLoad = window.setInterval(function () {
                let chart = $('#chart-plan-fact').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.ChartPlanFactDays._redrawPlanMonths();
                    window.clearInterval(chartToLoad);
                    window.ChartPlanFactDays.isLoaded = true;
                }
            }, 100);
        },
        _redrawPlanMonths: function() {
            let custom_pattern = function (color) {
                return {
                    pattern: {
                        path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                        width: 10,
                        height: 10,
                        color: color
                    }
                }
            };
            let chart = $('#chart-plan-fact').highcharts();
            if (typeof(chart) !== 'undefined') {
                //console.log(chart.series);
                let pos = ChartPlanFactDays.currDayPos;
                if (pos == 9999)
                    pos = 0; // all plan
                if (pos == -1)
                    return; // all fact

                for (let i = (1+pos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color1 ?>");
                    chart.series[1].points[i].color = custom_pattern("<?= $color2 ?>");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();
            }
        },
        _getData: function() {
            window.ChartPlanFactDays._inProcess = true;
            return $.post('/reports/finance-ajax/get-plan-fact-data', {
                    "chart-plan-fact-ajax": true,
                    "period": ChartPlanFactDays.period,
                    "offset": window.ChartPlanFactDays.offset[ChartPlanFactDays.period],
                    "purse": ChartPlanFactDays.byPurse,
                    "onPage": "<?= $ON_PAGE ?>"
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartPlanFactDays.freeDays = data.freeDays;
                    ChartPlanFactDays.currDayPos = data.currDayPos;
                    ChartPlanFactDays.labelsX = data.labelsX;
                    ChartPlanFactDays.wrapPointPos = data.wrapPointPos;
                    ChartPlanFactDays.chartPoints = data.optionsChart;
                    ChartPlanFactDays.chartPoints2 = data.optionsChart2;
                    ChartPlanFactDays.tooltipData = data.tooltipData;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            let chart = $('#chart-plan-fact').highcharts();
            let colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            let name, left, txtLine, txtLabel;

            let k = (window.ChartPlanFactDays.period == 'months') ? 0.725 : 0.625;

            if (window.ChartPlanFactDays.wrapPointPos[x + 1]) {
                name = window.ChartPlanFactDays.wrapPointPos[x + 1].prev;
                left = window.ChartPlanFactDays.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartPlanFactDays.wrapPointPos[x]) {
                name = window.ChartPlanFactDays.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        formatter: function(args) {

            let idx = this.point.index;
            let isIncome = (this.series.index === 0 || this.series.index === 2);
            let isPlanPeriod = (idx > window.ChartPlanFactDays.currDayPos && window.ChartPlanFactDays.currDayPos != -1) || window.ChartPlanFactDays.currDayPos == 9999;

            if (window.ChartPlanFactDays.period === "months") {

                if (isPlanPeriod) {
                    return '<span class="title">' + window.ChartPlanFactDays.labelsX[this.point.index] + '</span>' +
                        '<table class="indicators">' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[2].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[3].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '</table>';
                }

                return '<span class="title">' + window.ChartPlanFactDays.labelsX[this.point.index] + '</span>' +
                    ((this.series.index === 0 || this.series.index === 2) ?
                            ('<table class="indicators">' +
                                '<tr>' + '<td class="gray-text">' + args.chart.series[0].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                '<tr>' + '<td class="gray-text">' + args.chart.series[2].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[2].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                '</table>') :
                            ('<table class="indicators">' +
                                '<tr>' + '<td class="gray-text">' + args.chart.series[1].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                '<tr>' + '<td class="gray-text">' + args.chart.series[3].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[3].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                '</table>')
                    );

            } else {


                if((this.series.index === 0 || this.series.index === 2) && this.series.chart.series[0].points[idx].y === 0)
                    return false;
                if ((this.series.index === 1 || this.series.index === 3) && this.series.chart.series[1].points[idx].y === 0)
                    return false;

                let arrIncome = ChartPlanFactDays.tooltipData.jsArrIncome;
                let arrOutcome = ChartPlanFactDays.tooltipData.jsArrOutcome;
                let arrIncomeTotal = ChartPlanFactDays.tooltipData.jsArrIncomeTotal;
                let arrOutcomeTotal = ChartPlanFactDays.tooltipData.jsArrOutcomeTotal;
                let arrDate = ChartPlanFactDays.tooltipData.jsArrDate;
                let htmlHeaderPlan = ChartPlanFactDays.tooltipTemplate.htmlHeaderPlan;
                let htmlHeaderFact = ChartPlanFactDays.tooltipTemplate.htmlHeaderFact;
                let htmlData = ChartPlanFactDays.tooltipTemplate.htmlData;
                let htmlFooter = ChartPlanFactDays.tooltipTemplate.htmlFooter;

                let arr = (isIncome) ? arrIncome : arrOutcome;
                let arrTotals = (isIncome) ? arrIncomeTotal : arrOutcomeTotal;
                let chart = this.series.chart;
                let planY;
                let factY;

                if (isPlanPeriod) {
                    factY = 0;
                    planY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
                } else {
                    planY = (isIncome) ? chart.series[2].points[idx].y : chart.series[3].points[idx].y;
                    factY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
                }

                let tooltipHtml = (isPlanPeriod) ? htmlHeaderPlan : htmlHeaderFact;

                tooltipHtml = tooltipHtml
                    .replace("{title}", isIncome ? 'Приход за' : 'Расход за')
                    .replace("{point.x}", arrDate[idx])
                    .replace("{plan.y}", Highcharts.numberFormat(planY, 0, ',', ' '))
                    .replace("{fact.y}", Highcharts.numberFormat(factY, 0, ',', ' '));

                if (arr[idx]) {
                    arr[idx].forEach(function(data) {
                        tooltipHtml += htmlData
                            .replace("{name}", data.name)
                            .replace("{percent}", data.percent)
                            .replace("{sum}", Highcharts.numberFormat(data.sum, 0, ',', ' '))
                            .replace("{color_css}", (isIncome ? 'blue' : 'yellow') + (isPlanPeriod ? ' plan' : ''))
                    });
                } else {
                    tooltipHtml += '<tr><td colspan="3"></td></tr>';
                }
                if (arrTotals[idx]) {
                    tooltipHtml += htmlFooter.replace("{total}", arrTotals[idx]);
                }

                return tooltipHtml;
            }
        },
        positioner: function(boxWidth, boxHeight, point) {

            if (ChartPlanFactDays.period === "months") {
                return {x: point.plotX, y: point.plotY - boxHeight / 2};
            } else {
                return {x: point.plotX + 69, y: point.plotY - 45};
            }
        }
    };

    ////////////////////////////////
    window.ChartPlanFactDays.init();
    ////////////////////////////////

    $(document).ready(function() {
        window._chartPlanFactDaysArrowZ = 1; 
        $('#chart-plan-fact').mousemove(function(event) {
            let setZ = function() { $('.chart-plan-fact-days-arrow').filter('.chart-1').css({"z-index": window._chartPlanFactDaysArrowZ}); };
            let y = event.pageY - $(this).offset().top;
            let x = event.pageX - $(this).offset().left;
            if (window.ChartPlanFactDays.period == "days") {
                if (y > 170 && (x < 60 || x > $(this).width() - 20) && window._chartPlanFactDaysArrowZ === 1) {
                    window._chartPlanFactDaysArrowZ = 2;
                    setZ();
                }
                if (y <= 170 && window._chartPlanFactDaysArrowZ === 2) {
                    window._chartPlanFactDaysArrowZ = 1;
                    setZ();
                }
            } else if (window._chartPlanFactDaysArrowZ === 1) {
                window._chartPlanFactDaysArrowZ = 2;
                setZ();
            }
        });

    });

</script>