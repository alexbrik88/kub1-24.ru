<?php

use frontend\modules\reports\models\IncomeSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\reports\models\AbstractFinance;

/** @var $model IncomeSearch */

///////////////// dynamic vars ///////////
$customMonth = $customMonth ?? ($model->year == date('Y') ? date('m') : 12);
$customPurse = $customPurse ?? null;
/////////////////////////////////////////

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////

///////////////// consts /////////////////
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 6;
$cropNamesLength = 12;
$croppedNameLength = 10;
//////////////////////////////////////////

$date = date_create_from_format('d.m.Y H:i:s', "01.{$customMonth}.{$model->year} 23:59:59");
$dateFrom = $date->format('Y-m-d');
$dateTo = $date->modify('last day of this month')->format('Y-m-d');

$mainData = $model->getChartStructureByArticles($dateFrom, $dateTo, $customPurse);
$categories = array_slice(array_column($mainData, 'name'), 0, $chartMaxRows);
$factData = array_slice(array_column($mainData, 'amountFact'), 0, $chartMaxRows);
$planData = array_slice(array_column($mainData, 'amountPlan'), 0, $chartMaxRows);
$articlesIds = array_slice(array_column($mainData, 'id'), 0, $chartMaxRows);
$incomeTotalAmount = array_sum($factData);

///////////////// calc /////////////////
$calculatedChartHeight = max(count($factData), count($planData)) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
//////////////////////////////////////////

if (Yii::$app->request->post('chart-income-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'chartHeight' => $calculatedChartHeight,
        'incomeTotalAmount' => $incomeTotalAmount,
        'articlesIds' => $articlesIds,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $categories,
            ],
            'series' => [
                [
                    'data' => $planData,
                ],
                [
                    'data' => $factData,
                ]
            ],
        ],
    ]);

    exit;
}



?>
<style>
    #chart-income-structure-1 { height: auto; }
    #chart-income-structure-1 .highcharts-container { z-index: 2!important; }
</style>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            СТРУКТУРА ПО СТАТЬЯМ
            <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
                <?= \kartik\select2\Select2::widget([
                    'hideSearch' => true,
                    'id' => 'chart-income-structure-month-1',
                    'name' => 'chart-income-structure-month-1',
                    'data' => array_filter(array_reverse(AbstractFinance::$month, true),
                        function($k) use ($model) {
                            return ($model->year < date('Y') || (int)$k <= date('n'));
                        }, ARRAY_FILTER_USE_KEY),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'value' => $customMonth,
                    'pluginOptions' => [
                        'width' => '106px',
                        'containerCssClass' => 'select2-balance-structure'
                    ]
                ]); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-income-structure-1',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        //'style' => [
                        //    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        //],
                        'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {
                        
                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var totalAmount = (ChartIncomeStructure1.incomeTotalAmount > 0) ? ChartIncomeStructure1.incomeTotalAmount : 9E9;

                                return '<span class=\"title\">' + this.point.category + '</span>' +
                                    '<table class=\"indicators\">' + 
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') + 
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') + 
                                        ('<tr>' + '<td class=\"gray-text\">Доля в приходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[1].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                                    '</table>';
                                        
                        
                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('
                            function (boxWidth, boxHeight, point) { 
                                var x = this.chart.containerWidth - boxWidth;
                                var y = point.plotY + 50;
                                console.log(x)
                                return {x: x, y: y}; 
                            }
                        '),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'План',
                            'pointPadding' => 0,
                            'data' => $planData,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Факт',
                            'pointPadding' => 0,
                            'data' => $factData,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'cursor' => 'pointer',
                            //'point' => [
                            //    'events' => [
                            //        'click' => new JsExpression("
                            //            function() {
                            //                var articleId = ChartIncomeStructure1.articlesIds[this.index];
                            //                if (!ChartIncomeMain.byArticle || ChartIncomeMain.byArticle != articleId) {
                            //                    ChartIncomeMain.byArticle = articleId;
                            //                    //ChartIncomeIncome.byArticle = articleId;
                            //                    ChartIncomeStructure1.setPoint(this);
                            //                    // old selection
                            //                    ChartIncomeMain.byClient = null;
                            //                    //ChartIncomeIncome.byClient = null;
                            //                    //ChartIncomeStructure2.resetPoints();
                            //                } else {
                            //                    ChartIncomeMain.byArticle = null;
                            //                    //ChartIncomeIncome.byArticle = null;
                            //                    ChartIncomeStructure1.resetPoints();
                            //                }
                            //
                            //                ChartIncomeMain.redrawByClick();
                            //                //ChartIncomeIncome.redrawByClick();
                            //            }
                            //        ")
                            //    ]
                            //],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    // MOVE CHART
    window.ChartIncomeStructure1 = {
        chart: 'income-structure-1',
        isLoaded: false,
        year: "<?= $model->year ?>",
        month: "<?= $customMonth ?>",
        chartPoints: {},
        chartHeight: "<?= $calculatedChartHeight ?>",
        byPurse: null,
        incomeTotalAmount: "<?= $incomeTotalAmount ?>",
        articlesIds: <?= json_encode($articlesIds) ?>,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {

            $('#chart-income-structure-month-1').on('change', function() {

                // prevent double-click
                if (window.ChartIncomeStructure1._inProcess) {
                    return false;
                }

                if (window.ChartIncomeMain.byClient || window.ChartIncomeMain.byArticle) {
                    window.ChartIncomeMain.byClient = null;
                    window.ChartIncomeMain.byArticle = null;
                    window.ChartIncomeStructure1.resetPoints();
                    window.ChartIncomeMain.redrawByClick();
                    //window.ChartIncomeIncome.byClient = null;
                    //window.ChartIncomeIncome.byArticle = null;
                    //window.ChartIncomeStructure2.resetPoints();
                    //window.ChartIncomeIncome.redrawByClick();
                }

                window.ChartIncomeStructure1.year = $('#incomesearch-year').val();
                window.ChartIncomeStructure1.month = $(this).val();
                window.ChartIncomeStructure1.redrawByClick();

                window.ChartIncomeTop.year = $('#incomesearch-year').val();
                window.ChartIncomeTop.month = $(this).val();
                window.ChartIncomeTop.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartIncomeStructure1._getData().done(function() {
                $('#chart-income-structure-1').highcharts().update(ChartIncomeStructure1.chartPoints);
                $('#chart-income-structure-1').highcharts().update({"chart": {"height": ChartIncomeStructure1.chartHeight + 'px'}});
                window.ChartIncomeStructure1._inProcess = false;
            });
        },
        setPoint: function(point) {
            this.resetPoints();
            point.update({
                color: {
                    pattern: {
                        'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color': '<?= $color1 ?>',
                        'width': 5,
                        'height': 5
                    }
                }
            });
        },
        resetPoints: function() {
            var chart = $('#chart-income-structure-1').highcharts();
            chart.series[1].data.forEach(function(p) {
                p.update({
                    color: '<?= $color1 ?>'
                });
            });
            chart.series[0].data.forEach(function(p) {
                p.update({
                    color: '<?= $color2 ?>'
                });
            });
        },
        _getData: function() {
            window.ChartIncomeStructure1._inProcess = true;
            return $.post('/reports/finance-ajax/get-income-charts-data', {
                    "chart-income-ajax": true,
                    "purse": ChartIncomeStructure1.byPurse,
                    "chart": ChartIncomeStructure1.chart,
                    "year": ChartIncomeStructure1.year,
                    "month": ChartIncomeStructure1.month,
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartIncomeStructure1.chartPoints = data.optionsChart;
                    ChartIncomeStructure1.chartHeight = data.chartHeight;
                    ChartIncomeStructure1.incomeTotalAmount = data.incomeTotalAmount;
                    ChartIncomeStructure1.articlesIds = data.articlesIds;
                }
            );
        },
    };

    window.ChartIncomeStructure1.init();
</script>