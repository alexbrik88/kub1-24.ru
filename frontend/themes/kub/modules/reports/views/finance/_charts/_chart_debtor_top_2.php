<?php

use common\models\Contractor;
use common\components\TextHelper;
use frontend\modules\reports\components\DebtsHelper;

/** @var  $type int */

/////////////// consts //////////////////
$color = '#336d9a';
$maxRowsCount = 8;
/////////////////////////////////////////

$jsLoadFunc = <<<JS
function() {
    var chart = $('#chart-debt').highcharts();
    $.each(chart.series[0].data,function(i,data){
        var offset = 54;
        var left = chart.plotWidth - data.dataLabel.width + offset;

        data.dataLabel.attr({
            x: left
        });
    });
}
JS;
$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX + 10,y:point.plotY + 15}; }
JS;

$htmlHeader = <<<HTML

    <table class="ht-in-table">
        <tr>
            <th colspan="3" class="ht-title text-left black" style="max-width:250px; padding-bottom:2px;">{serie_name}</th>
        </tr>
        <tr>
            <th class="red">Просрочка на</th>
            <th colspan="2"></th>
        </tr>
HTML;
$htmlDataBlue = <<<HTML
        <tr>
            <td><div class="ht-title">{period}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart" style="background-color:{$color}; width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
HTML;

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlDataBlue = str_replace(["\r", "\n", "'"], "", $htmlDataBlue);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

$categories = $debt = [];
if ($topData = DebtsHelper::getTop($maxRowsCount, $type)) {
    foreach ($topData as $contractorID => $sum) {
        $categories[] = Contractor::findOne($contractorID)->getShortName();
        $debt[] = round(bcdiv($sum, 100), 2);
    }
} else {
    $categories[] = 'Нет данных';
    $debt[] = 0;
}

// tooltip credit
$jsTopData = [];
foreach ($topData as $contractorId => $sum) {
    $sum_0_10 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, false, $contractorId, $type);
    $sum_11_30 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, false, $contractorId, $type);
    $sum_31_60 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, false, $contractorId, $type);
    $sum_61_90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, false, $contractorId, $type);
    $sum_more_90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, false, $contractorId, $type);
    $max_sum = max(1, $sum_0_10, $sum_11_30, $sum_31_60, $sum_61_90, $sum_more_90);

    $jsTopData[] = [
        ['period' => '0-10 дней', 'percent' => $max_sum ? round(100 * $sum_0_10 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_0_10)],
        ['period' => '11-30 дней', 'percent' => $max_sum ? round(100 * $sum_11_30 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_11_30)],
        ['period' => '31-60 дней', 'percent' => $max_sum ? round(100 * $sum_31_60 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_31_60)],
        ['period' => '61-90 дней', 'percent' => $max_sum ? round(100 * $sum_61_90 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_61_90)],
        ['period' => 'Больше 90 дней', 'percent' => $max_sum ? round(100 * $sum_more_90 / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($sum_more_90)],
    ];
}

$jsTopData = json_encode($jsTopData);

$jsFormatterFunc = <<<JS
function (a) {

      if(1 == this.point.series.index)
          return false;

      var idx = this.point.index;
      var tooltipHtml;

      var arr = $jsTopData;

      tooltipHtml = '$htmlHeader'.replace("{serie_name}", this.point.category);
      if (arr[idx] != undefined) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '$htmlDataBlue'
               .replace("{period}", data.period)
               .replace("{percent}", data.percent)
               .replace("{sum}", data.sum);
          });
      } else {
          tooltipHtml += '<tr><td colspan="3">Нет данных</td></tr>';
      }

      tooltipHtml += '$htmlFooter';

      return tooltipHtml.replace();
    }
JS;

?>

<div class="ht-caption">
    <?= ($type == 2) ? 'ТОП ДОЛЖНИКОВ' : 'ТОП КРЕДИТОРОВ' ?>
</div>
<div style="height: 150px!important;">
    <?php
    echo \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-debt',
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill'
        ],
        'options' => [
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => false,
                'style' => [
                    'font-size' => '15px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'credits' => [
                'enabled' => false
            ],
            'legend' => [
                'enabled' => false
            ],
            'exporting' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => max(30 * count($categories), 30),
                'spacing' => [0,0,0,0],
                'marginRight' => 50,
                'events' => [
                    'load' => new \yii\web\JsExpression($jsLoadFunc),
                    'redraw' => new \yii\web\JsExpression($jsLoadFunc),
                ]
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left'
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0
                ]
            ],
            'tooltip' => [
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'positioner' => new \yii\web\JsExpression($jsPositionerFunc),
                'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
                'useHTML' => true,
                'shape' => 'rect',
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'title' => '',
                'labels' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'labels' => [
                    //'useHTML' => true,
                    'align' => 'left',
                    'padding' => 100,
                    'style' => [
                        'width' => '100px'
                    ],
                    'reserveSpace' => true,
                    'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
                ],
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0
            ],
            'series' => [
                [
                    'name' => ($type == 2) ? 'Сумма долга' : 'Сумма кредита',
                    'data' => $debt,
                    'color' => $color,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ]
                    ],
                    'pointWidth' => 18,
                    'borderRadius' => 3
                ],
            ]
        ],
    ]);
    ?>
</div>
