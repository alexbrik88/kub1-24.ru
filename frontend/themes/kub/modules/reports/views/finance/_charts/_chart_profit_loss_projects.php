<?php

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////
///
?>

<style>
    #chart-profit-loss-projects { height: auto; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="ht-caption noselect">
            ТОП ПРИБЫЛЬНЫХ ПРОЕКТОВ
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="display: flex; min-height:233px; align-items: center; justify-content: center; opacity: .25;">
            <div style="margin-top:-30px"> Блок Проекты<br/>в разработке </div>
        </div>
    </div>
</div>
