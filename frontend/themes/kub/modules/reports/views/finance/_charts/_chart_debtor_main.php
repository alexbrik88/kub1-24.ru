<?php

use frontend\modules\reports\models\debtor\DebtorHelper;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\themes\kub\components\Icon;
use yii\db\Query;
use yii\web\JsExpression;
use common\components\helpers\Month;

/** @var $model DebtReportSearch */
/** @var $allDebtSum int */

///////////////// dynamic vars ///////////
$customPeriod = "months";
$customOffset = $customOffset ?? 0;
$customDebtPeriod = $customDebtPeriod ?? DebtorHelper::PERIOD_OVERDUE;
/////////////////////////////////////////

///////////////// colors /////////////////
$color = ($type == 2) ? 'rgba(227, 6, 17, 1)' : 'rgba(255, 126, 87, 1)';
$color_opacity = ($type == 2) ? 'rgba(227, 6, 17, .5)' : 'rgba(255, 126, 87, .5)';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 12;
$RIGHT_DATE_OFFSET = 6;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $RIGHT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $LEFT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);

    $daysPeriods = [];
    $chartPlanFactLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i >= 1 && $i <= ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartPlanFactLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

// data
$dataFact = $dataPercent = [];
foreach ($datePeriods as $key => $period) {

    if ($period['from'] > date('Y-m-d')) {
        $dataFact[] = null;
        $dataPercent[] = null;
        continue;
    }

    $sum = (int)(new Query)
        ->from(['t' => DebtorHelper::getDebts($customDebtPeriod, $period['from'], $period['to'], $type)])
        ->sum('sum', \Yii::$app->db2);

    $allDebtSum = (int)(new Query)
        ->from(['t' => DebtorHelper::getDebts(null, $period['from'], $period['to'], $type)])
        ->sum('sum', \Yii::$app->db2);

    $dataFact[] = $sum / 100;
    $dataPercent[] = ($allDebtSum > 0) ? 100 * $sum / $allDebtSum : null;
}

if (Yii::$app->request->post('chart-debtor-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartPlanFactLabelsX,
        'allDebtSum' => $allDebtSum,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $dataFact,
                ],
                [
                    'data' => $dataPercent,
                ]
            ],
        ],
    ]);

    exit;
}

?>
<style>
    #chart-debtor { height: 235px; }
</style>

<div style="position: relative; margin-right:20px;">
    <div style="width: 100%;">

        <div class="chart-debtor-arrow link cursor-pointer" data-move="left" style="position: absolute; left:0; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-debtor-arrow link cursor-pointer" data-move="right" style="position: absolute; right:-5px; bottom:25px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            ДИНАМИКА ПРОСРОЧЕННОЙ ЗАДОЛЖЕННОСТИ <span style="text-transform: lowercase">И</span> ЕЁ ДОЛЯ В ОБЩЕЙ ЗАДОЛЖЕННОСТИ
        </div>
        <div class="wrap-select2-no-padding ml-1" style="position: absolute; right:0; top:-13px;">
            <?= \kartik\select2\Select2::widget([
                'id' => 'chart-debtor-period',
                'name' => 'purseType',
                'data' => [
                    DebtorHelper::PERIOD_OVERDUE => 'Просроченная',
                    DebtorHelper::PERIOD_0_10 => '0-10 дней',
                    DebtorHelper::PERIOD_11_30 => '11-30 дней',
                    DebtorHelper::PERIOD_31_60 => '31-60 дней',
                    DebtorHelper::PERIOD_61_90 => '61-90 дней',
                    DebtorHelper::PERIOD_MORE_90 => 'Больше 90 дней',
                    DebtorHelper::PERIOD_ACTUAL => 'Текущая',
                    '' => 'Итого',
                ],
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '170px'
                ]
            ]); ?>
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group" style="min-height:235px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-debtor',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartDebtorMain.redrawByLoad()')
                        ],
                        'spacing' => [0,0,0,0],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {
                            
                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                        
                                if ((index > window.ChartDebtorMain.currDayPos && window.ChartDebtorMain.currDayPos != -1) || window.ChartDebtorMain.currDayPos == 9999) {
                                    return '<span class=\"title\">' + window.ChartDebtorMain.labelsX[this.point.index] + '</span>' +
                                        '<table class=\"indicators\">' + 
                                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '</table>';
                                }

                                return '<span class=\"title\">' + window.ChartDebtorMain.labelsX[this.point.index] + '</span>' +
                                    '<table class=\"indicators\">' + 
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>' +
                                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' %</td></tr>') +
                                    '</table>';

                            }
                        ")
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        [
                            'min' => 0,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ]
                        ],
                        [
                            'min' => 0,
                            'max' => 100,
                            'index' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => [
                                    'fontWeight' => '300',
                                    'fontSize' => '13px',
                                    'whiteSpace' => 'nowrap'
                                ]
                            ],
                            'opposite' => true
                        ],
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() { 
                                        var result = (this.pos == window.ChartDebtorMain.currDayPos) ? 
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                            (window.ChartDebtorMain.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (window.ChartDebtorMain.wrapPointPos) {
                                            result += window.ChartDebtorMain.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'yAxis' => 0,
                            'name' => 'Просроченная задолженность',
                            'data' => $dataFact,
                            'color' => $color,
                            'borderColor' => $color_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'yAxis' => 1,
                            'name' => 'Доля в общей задолженности',
                            'data' => $dataPercent,
                            'color' => 'rgba(149,165,166,1)',
                            'type' => 'line',
                            'pointPlacement' => 0,
                            'stickyTracking' => false,
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} ₽</b>',
                            ],
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                                'groupPadding' => 0.05,
                                'pointPadding' => 0.1,
                                'borderRadius' => 3,
                                'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    // MOVE CHART
    window.ChartDebtorMain = {
        chart: 'main',
        isLoaded: false,
        year: "<?= $model->year ?>",
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartPlanFactLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        byDebtorPeriod: <?= $customDebtPeriod ?>,
        _inProcess: false,
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-debtor-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartDebtorMain._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left') {
                    window.ChartDebtorMain.offset[ChartDebtorMain.period] -= <?= $MOVE_OFFSET ?>;
                }
                if ($(this).data('move') === 'right') {
                    window.ChartDebtorMain.offset[ChartDebtorMain.period] += <?= $MOVE_OFFSET ?>;
                }

                window.ChartDebtorMain.redrawByClick();
            });

            $('#chart-debtor-period').on('change', function() {
                window.ChartDebtorMain.byDebtorPeriod = $(this).val();
                window.ChartDebtorMain.redrawByClick();
            });

        },
        redrawByClick: function() {

            return window.ChartDebtorMain._getData().done(function() {
                $('#chart-debtor').highcharts().update(ChartDebtorMain.chartPoints);
                window.ChartDebtorMain._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartDebtorMain.isLoaded)
                return;

            var chartToLoad = window.setInterval(function () {
                var chart = $('#chart-debtor').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.clearInterval(chartToLoad);
                    window.ChartDebtorMain.isLoaded = true;
                }
            }, 100);
        },
        _getData: function() {
            window.ChartDebtorMain._inProcess = true;
            return $.post('/reports/finance-ajax/get-debtor-charts-data', {
                    "type": <?= $type ?>,
                    "chart-debtor-ajax": true,
                    "chart": ChartDebtorMain.chart,
                    "period": ChartDebtorMain.period,
                    "offset": window.ChartDebtorMain.offset[ChartDebtorMain.period],
                    "year": ChartDebtorMain.year,
                    "debtor-period": ChartDebtorMain.byDebtorPeriod
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartDebtorMain.freeDays = data.freeDays;
                    ChartDebtorMain.currDayPos = data.currDayPos;
                    ChartDebtorMain.labelsX = data.labelsX;
                    ChartDebtorMain.wrapPointPos = data.wrapPointPos;
                    ChartDebtorMain.chartPoints = data.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            var chart = $('#chart-debtor').highcharts();
            var colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            var name, left, txtLine, txtLabel;

            var k = (window.ChartDebtorMain.period == 'months') ? 0.705 : 0.625;

            if (window.ChartDebtorMain.wrapPointPos[x + 1]) {
                name = window.ChartDebtorMain.wrapPointPos[x + 1].prev;
                left = window.ChartDebtorMain.wrapPointPos[x + 1].leftMargin;

                txtLine = (x < 12) ? '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>' : '';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartDebtorMain.wrapPointPos[x]) {
                name = window.ChartDebtorMain.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        }
    };

    //////////////////////////////
    window.ChartDebtorMain.init();
    //////////////////////////////

</script>