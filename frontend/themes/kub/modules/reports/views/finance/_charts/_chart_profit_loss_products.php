<?php

use frontend\modules\reports\models\ExpensesSearch;
use frontend\themes\kub\components\Icon;
use yii\web\JsExpression;
use common\components\helpers\Month;
use frontend\modules\reports\models\AbstractFinance;

/** @var $model ExpensesSearch */
/** @var $marginData array */

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////

?>
<style>
    #chart-profit-loss-products { height: auto; }
</style>

<div style="position: relative">
    <div style="width: 100%; min-height: <?= $chartWrapperHeight ?>px;">

        <div class="ht-caption noselect" style="margin-bottom: 16px">
            ТОП МАРЖИНАЛЬНЫХ ТОВАРОВ
        </div>
        <div class="clearfix"></div>

        <div class="finance-charts-group">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-profit-loss-products',
                'class' => 'finance-charts',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'spacing' => [0,0,0,0],
                        'height' => $calculatedChartHeight,
                        'inverted' => true,
                        //'style' => [
                        //    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        //],
                        'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shape' => 'rect',
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("
                            function(args) {
                        
                                var index = this.series.data.indexOf( this.point );
                                var series_index = this.series.index;
                                var marginPercent = ChartsProfitAndLoss.Products.marginPercent[index];
                                var quantity = ChartsProfitAndLoss.Products.quantity[index];
                                var quantity_zeroes_after_coma = (parseInt(quantity) == parseFloat(quantity)) ? 0 : 2;

                                return '<span class=\"title\">' + this.point.category + '</span>' + '<br/>' +
                                    '<table class=\"indicators\">' + 
                                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                                        ('<tr>' + '<td class=\"gray-text\">% маржи: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(marginPercent, 0, ',', ' ') + '%</td></tr>') + 
                                        ('<tr>' + '<td class=\"gray-text\">Кол-во продано: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(quantity, quantity_zeroes_after_coma, ',', ' ') + ' шт.</td></tr>') +
                                    '</table>';
                            }
                        "),
                        'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x: point.plotX / 3 + 75, y: point.plotY + 50}; }'),
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'endOnTick' => false,
                        'tickPixelInterval' => 1,
                        'visible' => false
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                        'minorGridLineWidth' => 0,
                        'lineWidth' => 0,
                        'gridLineWidth' => 0,
                        'offset' => 0,
                        'labels' => [
                            'align' => 'left',
                            'reserveSpace' => true,
                            'formatter' => new JsExpression("
                                    function() { return (this.value.length > {$cropNamesLength}) ? (this.value.substring(0,{$croppedNameLength}) + '...') : this.value }"),
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Маржа',
                            'pointPadding' => 0,
                            'data' => $marginData,
                            'color' => $color1,
                            'borderColor' => $color1_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'pointWidth' => 18,
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'grouping' => false,
                            'shadow' => false,
                            'borderWidth' => 0,
                            'borderRadius' => 3,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>