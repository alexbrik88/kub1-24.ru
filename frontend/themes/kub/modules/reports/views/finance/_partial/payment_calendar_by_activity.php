<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.02.2019
 * Time: 23:51
 */

use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Url;
use frontend\modules\reports\models\PaymentCalendarSearch;
use kartik\checkbox\CheckboxX;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use common\components\ImageHelper;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $floorMap array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style <?= $tabViewClass ?> table-count-list mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <span class="text-grey weight-700 ml-1 nowrap"><?= $searchModel->year; ?></span>
                </th>
            </tr>
            <tr>
                <?php foreach (FlowOfFundsReportSearch::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2"><div class="pl-1 pr-1">Итого</div></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach (FlowOfFundsReportSearch::$types as $typeID => $typeName): ?>

                <?php $floorKey1 = "first-floor-{$typeID}"; ?>
                <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1); ?>

                <?php $class = in_array($typeID, [
                    FlowOfFundsReportSearch::RECEIPT_FINANCING_TYPE_FIRST,
                    FlowOfFundsReportSearch::INCOME_OPERATING_ACTIVITIES,
                    FlowOfFundsReportSearch::INCOME_INVESTMENT_ACTIVITIES]) ?
                    'income' : 'expense'; ?>

                <?php if ($class == 'income'): ?>
                    <tr class="main-block">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= FlowOfFundsReportSearch::$blocks[FlowOfFundsReportSearch::$blockByType[$typeID]]; ?>
                                    <?php if ($typeID == FlowOfFundsReportSearch::RECEIPT_FINANCING_TYPE_FIRST): ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_financial-operations-block',
                                        ]) ?>
                                    <?php elseif ($typeID == FlowOfFundsReportSearch::INCOME_OPERATING_ACTIVITIES): ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_operation-activities-block',
                                        ]) ?>
                                    <?php else: ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_investment-activities-block',
                                        ]) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($data['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]) ?
                                $data['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>

                            <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                </div>
                            </td>
                            <?php $quarterSum += $amount; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>
                        <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap operation-actitivities-block-total-flow-sum">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= isset($data['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum']) ?
                                    TextHelper::invoiceMoneyFormat($data['blocks'][FlowOfFundsReportSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum'], 2) : 0; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>

                <tr class="not-drag expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>">
                    <td class="checkbox-td pl-2 pr-2 pt-3 pb-3">
                        <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey text_size_14 ml-1"><?= $typeName; ?></span>
                        </button>
                    </td>
                    <?php $key = $quarterSum = 0; ?>
                    <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3);
                        $amount = isset($data['types'][$typeID][$monthNumber]) ? $data['types'][$typeID][$monthNumber]['flowSum'] : 0;
                        ?>
                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </div>
                        </td>
                        <?php $quarterSum += $amount; ?>
                        <?php if ($key % 3 == 0): ?>
                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                <div class="pl-1 pr-1">
                                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                    $quarterSum = 0; ?>
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $typeID; ?>">
                        <div class="pl-1 pr-1">
                            <?= isset($data['types'][$typeID]['totalFlowSum']) ?
                                TextHelper::invoiceMoneyFormat($data['types'][$typeID]['totalFlowSum'], 2) : 0; ?>
                        </div>
                    </td>
                </tr>

                <?php if (isset($data[$typeID])): ?>
                    <?php foreach ($data['itemName'][$typeID] as $expenditureItemID => $expenditureItemName): ?>
                        <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                            <tr class="item-block <?= $class ?> <?= !$isOpenedFloor1 ? 'd-none':'' ?>" data-id="<?= $floorKey1 ?>" data-type_id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>">
                                <td class="pl-3 pr-3 pt-3 pb-3 checkbox-td">
                                    <svg class="sortable-row-icon svg-icon text_size-14 text-grey mr-2 flex-shrink-0">
                                        <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                    </svg>
                                    <span class="text-grey text_size_14 ml-1 mr-1"><?= $expenditureItemName; ?></span>
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = (int)ceil($key / 3);
                                    $amount = isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                        $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                    ?>
                                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                        </div>
                                    </td>
                                    <?php $quarterSum += $amount; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                $quarterSum = 0; ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach ?>
                                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                            TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if ($class == 'expense'): ?>
                    <tr>
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= FlowOfFundsReportSearch::$growingBalanceLabelByType[$typeID]; ?> (План)
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($data['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]) ?
                                $data['growingBalanceByBlock'][FlowOfFundsReportSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>

                            <?php
                            $showPlanMonthAmount = (int)$monthNumber >= date('m') && $searchModel->year >= date('Y');
                            $showPlanQuarterAmount = (int)$quarter >= (int)ceil(date('m') / 3) && $searchModel->year >= date('Y');
                            ?>

                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= ($showPlanMonthAmount) ? TextHelper::invoiceMoneyFormat($amount, 2) : ''; ?>
                                </div>
                            </td>
                            <?php $quarterSum += $amount; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?= ($showPlanQuarterAmount) ? TextHelper::invoiceMoneyFormat($amount, 2) : '';
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap total-month-total-flow-sum">
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative"></div>
                        </td>
                    </tr>
                <?php endif; ?>

            <?php endforeach; ?>
            <!-- TOTALS -->
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Результат по месяцу
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = isset($data['balance'][$monthNumber]) ? $data['balance'][$monthNumber] : 0; ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                    <?php $quarterSum += $amount; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap total-month-total-flow-sum">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat(isset($data['balance']['totalFlowSum']) ? $data['balance']['totalFlowSum'] : 0, 2); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Остаток на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0; ?>

                    <?php
                    $showPlanMonthAmount = (int)$monthNumber >= date('m') && $searchModel->year >= date('Y');
                    $showPlanQuarterAmount = (int)$quarter >= (int)ceil(date('m') / 3) && $searchModel->year >= date('Y');
                    ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= ($showPlanMonthAmount) ? TextHelper::invoiceMoneyFormat($amount, 2) : ''; ?>
                        </div>
                    </td>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= ($showPlanQuarterAmount) ? TextHelper::invoiceMoneyFormat($amount, 2) : ''; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty" role="row">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы)<br>
        в компанию и их возврат. <br>
        Выплата дивидендов.<br>
        Предоставление займов и депозитов.
    </span>
    <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
    </span>
    <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br>
        Затраты на новые проекты и поступление выручки от них. <br>
    </span>
</div>
