<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:54
 */

use common\components\helpers\ArrayHelper;
use frontend\modules\reports\models\ExpensesSearch;
use common\components\helpers\Html;
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_expenses');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-funds by_purse table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
                </th>
            </tr>
            <tr>
                <?php foreach (ExpensesSearch::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2" style="border-top-color: transparent"><div class="pl-1 pr-1">Итого</div></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach (ExpensesSearch::$purseTypes as $typeID => $typeName): ?>

                <?php $floorKey1 = "first-floor-{$typeID}"; ?>
                <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1); ?>

                <?php $class = 'expense';
                if (in_array($typeID, [
                    ExpensesSearch::INCOME_CASH_BANK,
                    ExpensesSearch::INCOME_CASH_ORDER,
                    ExpensesSearch::INCOME_CASH_EMONEY]))

                        continue; ?>

                <tr class="main-block"
                    data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                    <td class="pl-2 pr-2 pt-3 pb-3">
                        <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                            <div class="text_size_14 weight-700 mr-2 nowrap">
                                <?= ExpensesSearch::$purseBlocks[ExpensesSearch::$purseBlockByType[$typeID]]; ?>
                            </div>
                        </div>
                    </td>
                    <?php $key = $quarterSum = 0; ?>
                    <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3); ?>

                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            </div>
                        </td>
                        <?php if ($key % 3 == 0): ?>
                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap cash-bank-block-month-total-flow-sum">
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        </div>
                    </td>
                </tr>

                <tr class="not-drag expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>"
                    data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                    <td class="pl-2 pr-1 pt-3 pb-3">
                        <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey text_size_14 ml-1"><?= $typeName; ?></span>
                        </button>
                    </td>
                    <?php $key = $quarterSum = 0; ?>
                    <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3);
                        $amount = isset($data['types'][$typeID][$monthNumber]) ? $data['types'][$typeID][$monthNumber]['flowSum'] : 0;
                        ?>
                        <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                            </div>
                        </td>
                        <?php $quarterSum += $amount; ?>
                        <?php if ($key % 3 == 0): ?>
                            <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                <div class="pl-1 pr-1">
                                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                    $quarterSum = 0; ?>
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $typeID; ?>">
                        <div class="pl-1 pr-1">
                            <?= isset($data['types'][$typeID]['totalFlowSum']) ?
                                TextHelper::invoiceMoneyFormat($data['types'][$typeID]['totalFlowSum'], 2) : 0; ?>
                        </div>
                    </td>
                </tr>

                <?php if (isset($data[$typeID])): ?>
                    <?php foreach ($data['itemName'][$typeID] as $expenditureItemID => $expenditureItemName): ?>
                        <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                            <tr class="item-block <?= !$isOpenedFloor1 ? 'd-none':'' ?> <?=  $class; ?>" data-id="<?= $floorKey1 ?>" data-type_id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>"
                                data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                                <td class="pl-2 pr-1 pt-3 pb-3">
                                    <span class="text-grey text_size_14 m-l-purse"><?= $expenditureItemName; ?></span>
                                    <!--<button class="table-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center" type="button" data-collapse-row-trigger data-target="second-floor">
                                        <svg class="svg-icon text_size-14 text-grey mr-2 flex-shrink-0">
                                            <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                        </svg>
                                        <span class="text-grey text_size_14 ml-1 mr-1">АйТи</span>
                                        <svg class="svg-icon-triangle svg-icon text_size_14 text-grey-light ml-auto flex-shrink-0">
                                            <use xlink:href="/img/svg/svgSprite.svg#triangle"></use>
                                        </svg>
                                    </button>-->
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = (int)ceil($key / 3);
                                    $amount = isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                        $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                    ?>
                                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                        </div>
                                    </td>
                                    <?php $quarterSum += $amount; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                            <div class="pl-1 pr-1">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                $quarterSum = 0; ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach ?>
                                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                            TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; ?>
            <!-- TOTALS -->
            <tr class="not-drag cancel-drag">
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Итого на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0; ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                    <?php $quarterSum += $amount; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance']['totalFlowSum']) ? $data['growingBalance']['totalFlowSum'] : 0, 2); ?>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<?php /*
<!-- OLD -->
<div class="portlet box darkblue" style="margin-bottom: 5px;">
    <?= Html::beginForm(['expenses', 'activeTab' => $activeTab], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-12 pull-right serveces-search" style="padding-left: 5px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-2 p-l-0">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year,
                        ]); ?>
                    </div>
                    <div class="col-md-10" style="padding: 0 0 7px 0;">
                        <?= Html::a('<i class="fa fa-file-excel-o"></i> Скачать в Excel', Url::to(['/reports/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                            'class' => 'download-odds-xls pull-right',
                            'style' => 'margin-right: 1px;',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper no-footer scroll-table"
                 id="datatable_ajax_wrapper">
                <div class="scroll-table-wrapper">
                    <table class="table table-striped table-bordered table-hover flow-of-funds">
                        <thead class="not-fixed">
                        <tr class="heading quarters-flow-of-funds" role="row">
                            <th width="20%">
                                <?= CheckboxX::widget([
                                    'id' => 'main-checkbox-side',
                                    'name' => 'main-checkbox-side',
                                    'value' => true,
                                    'options' => [
                                        'class' => 'main-checkbox-side',
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?> Статьи
                            </th>
                            <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="1"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'first-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentQuarter == 1 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-1',
                                            'total-quarter-class' => 'quarter-1',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="first-quarter">
                                    1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="2"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'second-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-2',
                                            'total-quarter-class' => 'quarter-2',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="second-quarter">
                                    2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="3"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'third-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-3',
                                            'total-quarter-class' => 'quarter-3',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="third-quarter">
                                    3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="4"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'fourth-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-4',
                                            'total-quarter-class' => 'quarter-4',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="fourth-quarter">
                                    4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                                <?= $searchModel->year; ?>
                            </th>
                        </tr>
                        <tr class="heading" role="row">
                            <th width="20%"></th>
                            <?php foreach (ExpensesSearch::$month as $key => $month): ?>
                                <?php $quarter = ceil($key / 3); ?>
                                <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                                    data-month="<?= $key; ?>"
                                    style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= $month; ?>
                                </th>
                                <?php if ($key % 3 == 0): ?>
                                    <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                        style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        Итого
                                    </th>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <th class="text-center" width="10%">Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach (ExpensesSearch::$purseTypes as $typeID => $typeName): ?>
                            <?php if (in_array($typeID, [
                                ExpensesSearch::INCOME_CASH_BANK,
                                ExpensesSearch::INCOME_CASH_ORDER,
                                ExpensesSearch::INCOME_CASH_EMONEY,
                            ])) {
                                continue;
                            } ?>
                            <tr class="main-block" data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                                <td class="odd bold cash-bank-block">
                                    <?= ExpensesSearch::$purseBlocks[ExpensesSearch::$purseBlockByType[$typeID]]; ?>
                                </td>
                                <?php $key = 0; ?>
                                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = ceil($key / 3); ?>
                                    <td class="odd text-right bold can-hover quarter-month-<?= ceil($key / 3); ?> cash-bank-block-month-<?= $key; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    </td>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="odd text-right bold quarter-block can-hover quarter-<?= $key / 3; ?> cash-bank-block-quarter-<?= $key / 3; ?>"
                                            role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="bold odd total-block text-right can-hover cash-bank-block-month-total-flow-sum"
                                    role="row">
                                </td>
                            </tr>
                            <tr class="expenditure_type sub-block expense" id="<?= $typeID; ?>"
                                data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                                <td role="row" class="checkbox-td">
                                    <?= CheckboxX::widget([
                                        'id' => 'flow-of-funds-type-' . $typeID,
                                        'name' => 'flow-of-funds-type',
                                        'value' => true,
                                        'options' => [
                                            'class' => 'flow-of-funds-type',
                                        ],
                                        'pluginOptions' => [
                                            'size' => 'xs',
                                            'threeState' => false,
                                            'inline' => false,
                                            'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                            'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                        ],
                                    ]); ?>
                                    <label for="flow-of-funds-type-<?= $typeID; ?>">
                                        <?= $typeName; ?>
                                    </label>
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = ceil($key / 3); ?>
                                    <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> quarter-month-<?= $typeID . '-' . $key; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                        <?= isset($data['types'][$typeID][$monthNumber]) ?
                                            TextHelper::invoiceMoneyFormat($data['types'][$typeID][$monthNumber]['flowSum'], 2) : 0; ?>
                                    </td>
                                    <?php $quarterSum += isset($data['types'][$typeID][$monthNumber]) ?
                                        $data['types'][$typeID][$monthNumber]['flowSum'] : 0; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="odd text-right can-hover quarter-block quarter-<?= $key / 3; ?> quarter-<?= $typeID . '-' . ($key / 3); ?>"
                                            role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                            $quarterSum = 0; ?>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="odd total-block text-right can-hover total-flow-sum-<?= $typeID; ?>"
                                    role="row">
                                    <?= isset($data['types'][$typeID]['totalFlowSum']) ?
                                        TextHelper::invoiceMoneyFormat($data['types'][$typeID]['totalFlowSum'], 2) : 0; ?>
                                </td>
                            </tr>
                            <?php if (isset($data[$typeID])): ?>
                                <?php foreach ($data['itemName'][$typeID] as $expenditureItemID => $expenditureItemName): ?>
                                    <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                                        <tr data-id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>"
                                            class="item-block hidden item-<?= $expenditureItemID . ' expense'; ?>"
                                            data-flow_type="<?= ExpensesSearch::$purseBlockByType[$typeID]; ?>">
                                            <td width="20%" class="item-name-<?= 'expense-' . $expenditureItemID; ?>"
                                                style="padding-left: 40px;">
                                                <?= $expenditureItemName; ?>
                                            </td>
                                            <?php $key = $quarterSum = 0; ?>
                                            <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                                <?php $key++;
                                                $quarter = ceil($key / 3); ?>
                                                <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> item-month-<?= $expenditureItemID . '-' . $key; ?>"
                                                    role="row" width="10%"
                                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                                    <?= isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                                        TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID][$monthNumber]['flowSum'], 2) :
                                                        0; ?>
                                                </td>
                                                <?php $quarterSum += isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                                    $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0; ?>
                                                <?php if ($key % 3 == 0): ?>
                                                    <td class="odd text-right quarter-block can-hover quarter-<?= $key / 3; ?> item-quarter-<?= $expenditureItemID . '-' . $key / 3 ?>"
                                                        role="row" width="10%"
                                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                        $quarterSum = 0; ?>
                                                    </td>
                                                <?php endif; ?>
                                            <?php endforeach ?>
                                            <td class="odd total-block text-right can-hover item-total-flow-sum-<?= $expenditureItemID; ?>"
                                                role="row" width="10%">
                                                <?= isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                                    TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <tr class="not-drag cancel-drag">
                            <td class="odd bold remainder-month-text" role="row">Итого на конец месяца</td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?> remainder-month-<?= $key; ?>"
                                    role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0, 2); ?>
                                </td>
                                <?php $quarterSum += isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?> remainder-month-quarter-<?= $key / 3; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="odd text-right bold remainder-month-empty" role="row">
                                <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance']['totalFlowSum']) ? $data['growingBalance']['totalFlowSum'] : 0, 2); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
*/ ?>