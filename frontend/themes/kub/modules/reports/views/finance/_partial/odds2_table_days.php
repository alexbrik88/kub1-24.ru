<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\OddsSearch;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = $d_days = [];
for ($month=1; $month<=12; $month++) {
    $cell_id[$month] = 'cell-' . $month;
    $d_days[$month] = ArrayHelper::getValue($floorMap, 'cell-' . $month);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');

$rowspan = 3;
foreach ($data as $paymentType => $level1) {
    $rowspan+=2;
    foreach ($level1['levels'] as $paymentFlowType => $level2) {
        $rowspan++;
    }
}
$isFirstRow = true;
?>

<script>
    // ODDS FIXED COLUMN
    $(document).ready(function() {
        OddsTable.fillFixedColumn();
    });
</script>

<div id="cs-table-2" class="custom-scroll-table-double cs-top">
    <table style="width: 1832px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
</div>

<!-- ODDS FIXED COLUMN -->
<div id="cs-table-first-column">
    <table class="table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
        <thead></thead>
        <tbody></tbody>
    </table>
</div>

<div id="cs-table-1" class="custom-scroll-table-double">
    <div class="table-wrap">
    <table class="flow-of-funds odds-table table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
        <?= $this->render('odds2_table_head_days', [
            'searchModel' => $searchModel,
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'currentMonthNumber' => $currentMonthNumber,
        ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'currentMonthNumber' => $currentMonthNumber,
                'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'isFirstRow' => $isFirstRow,
                'rowspan' => $rowspan,
            ]);
            $isFirstRow = false;

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == OddsSearch::TAB_ODDS) ? [1,3,4] : [1,3,5]);

                echo $this->render('odds2_table_row_days', [
                    'level' => 2,
                    'searchModel' => $searchModel,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dDays' => $d_days,
                    'cellIds' => $cell_id,
                    'currentMonthNumber' => $currentMonthNumber,
                    'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    echo $this->render('odds2_table_row_days', [
                        'level' => 3,
                        'searchModel' => $searchModel,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dDays' => $d_days,
                        'cellIds' => $cell_id,
                        'currentMonthNumber' => $currentMonthNumber,
                        'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => ($activeTab == OddsSearch::TAB_ODDS)
                    ]);

                }
            }

            // GROWING
            echo $this->render('odds2_table_row_days', [
                'level' => 1,
                'searchModel' => $searchModel,
                'title' => $growingData[$paymentType]['title'],
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dDays' => $d_days,
                'cellIds' => $cell_id,
                'currentMonthNumber' => $currentMonthNumber,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false
            ]);
        }

        // TOTAL BY MONTH
        echo $this->render('odds2_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'currentMonthNumber' => $currentMonthNumber,
            'canHover' => false
        ]);

        // TOTAL BY YEAR
        echo $this->render('odds2_table_row_days', [
            'level' => 1,
            'searchModel' => $searchModel,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dDays' => $d_days,
            'cellIds' => $cell_id,
            'currentMonthNumber' => $currentMonthNumber,
            'isSumQuarters' => false,
            'canHover' => false
        ]);

        ?>
        </tbody>

    </table>
    </div>
</div>