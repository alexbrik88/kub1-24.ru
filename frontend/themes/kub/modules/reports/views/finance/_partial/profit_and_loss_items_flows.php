<?php
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \frontend\modules\reports\models\ProfitAndLossSearchModel $searchModel */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');

$showIncomeColumn = !empty($searchModel->income_item_id) || $searchModel->isRevenue;
$showExpenseColumn = !empty($searchModel->expenditure_item_id);

/** @var $searchModel \frontend\modules\reports\models\ProfitAndLossSearchModel */

?>
<style>#flow-items-pjax .cash-bank-need-clarify { display:none; }</style>

<?php $pjax = Pjax::begin([
    'id' => 'flow-items-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>

<?php if (!empty($searchBy)) echo Html::hiddenInput('searchBy', $searchBy, ['id' => 'searchBy']) ?>
<?php if (!empty($isSearchByChanged)) echo Html::hiddenInput('isSearchByChanged', 1, ['id' => 'isSearchByChanged']) ?>

<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none mb-0">
        <div class="custom-scroll-table">
            <div class="table-wrap">
                <?= GridView::widget([
                    'dataProvider' => $itemsDataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center joint-checkbox-td',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) {
                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $typeCss = 'income-item';
                                    $income = bcdiv($flows['amount'], 100, 2);
                                    $expense = 0;
                                } else {
                                    $typeCss = 'expense-item';
                                    $income = 0;
                                    $expense = bcdiv($flows['amount'], 100, 2);
                                }

                                return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                    'class' => 'joint-checkbox ' . $typeCss,
                                    'value' => $flows['id'],
                                    'data' => [
                                        'income' => $income,
                                        'expense' => $expense,
                                    ],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'recognition_date',
                            'label' => 'Дата признания',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) {
                                return DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Дата оплаты',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) {
                                return DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'amountIncome',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . ($showIncomeColumn ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell',
                                'style' => 'display: ' . ($showIncomeColumn ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {

                                return \common\components\TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                            },
                        ],
                        [
                            'attribute' => 'amountExpense',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . ($showExpenseColumn ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'class' => 'sum-cell',
                                'style' => 'display: ' . ($showExpenseColumn ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {

                                return \common\components\TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);
                            },
                        ],
                        [
                            'attribute' => 'payment_type',
                            'label' => 'Тип оплаты',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'filter' => [
                                '' => 'Все',
                                FlowOfFundsReportSearch::CASH_BANK_BLOCK => 'Банк',
                                FlowOfFundsReportSearch::CASH_ORDER_BLOCK => 'Касса',
                                FlowOfFundsReportSearch::CASH_EMONEY_BLOCK => 'E-money'
                            ],
                            'value' => function ($flows) use ($searchModel) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        /* @var $checkingAccountant CheckingAccountant */
                                        $checkingAccountant = CheckingAccountant::find()->andWhere([
                                            'rs' => $model->rs,
                                            'company_id' => $model->company->id,
                                        ])->orderBy(['type' => SORT_ASC])->one();
                                        if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                            return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                    'class' => 'little_logo_bank',
                                                    'style' => 'display: inline-block;',
                                                ]);
                                        }
                                        return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                                    case CashOrderFlows::tableName():
                                        return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                                    case CashEmoneyFlows::tableName():
                                        return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                                    default:
                                        return '';
                                }
                            },
                        ],
                        [
                            's2width' => '250px',
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                            ],
                            'contentOptions' => [
                                'class' => 'contractor-cell'
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getContractorFilterItems(),
                            'value' => function ($flows) use ($searchModel) {
                                /* @var $contractor Contractor */
                                $contractor = Contractor::findOne($flows['contractor_id']);
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $model = CashEmoneyFlows::findOne($flows['id']);
                                        break;
                                    default:
                                        return '';
                                }
                                return $contractor !== null ?
                                    ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                    ($model->cashContractor ?
                                        ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                    );
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Назначение',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '30%',
                            ],
                            'contentOptions' => [
                                'class' => 'purpose-cell'
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if ($flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                } else {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        default:
                                            return '';
                                    }
                                    if ($invoiceArray = $model->getInvoices()->all()) {
                                        $linkArray = [];
                                        foreach ($invoiceArray as $invoice) {
                                            $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ]);
                                        }

                                        return join(', ', $linkArray);
                                    }
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'billPaying',
                            'label' => 'Опл. счета',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $model = CashEmoneyFlows::findOne($flows['id']);
                                        break;
                                    default:
                                        return '';
                                }

                                return $model->billPaying;
                            },
                        ],
                        [
                            's2width' => '200px',
                            'attribute' => 'reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()),
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                                return $reason ?: '-';
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>