<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashFlows;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $activeTab integer
 * @var $subTab integer
 */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');

echo GridView::widget([
    'dataProvider' => $itemsDataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center joint-checkbox-td',
            ],
            'format' => 'raw',
            'value' => function ($flows) {
                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $typeCss = 'income-item';
                    $income = bcdiv($flows['amount'], 100, 2);
                    $expense = 0;
                } else {
                    $typeCss = 'expense-item';
                    $income = 0;
                    $expense = bcdiv($flows['amount'], 100, 2);
                }

                return Html::checkbox("flowId[plan_cash_flows][]", false, [
                    'class' => 'joint-checkbox ' . $typeCss,
                    'value' => $flows['id'],
                    'data' => [
                        'income' => $income,
                        'expense' => $expense,
                    ],
                ]);
            },
        ],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'value' => function ($flows) use ($searchModel) {
                return !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE]) ?
                    DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
            },
        ],
        [
            'attribute' => 'amountIncome',
            'label' => 'Приход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: ' . (!empty($searchModel->income_item_id) || $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
                'style' => 'display: ' . (!empty($searchModel->income_item_id) || $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'value' => function ($flows) {
                return $flows['amountIncome'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2) : '-';
            },
        ],
        [
            'attribute' => 'amountExpense',
            'label' => 'Расход',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) || $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) || $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
            ],
            'value' => function ($flows) {
                return $flows['amountExpense'] > 0 ? TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2) : '-';
            },
        ],
        [
            'attribute' => 'payment_type',
            'label' => 'Тип оплаты',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'filter' => [
                '' => 'Все',
                FlowOfFundsReportSearch::CASH_BANK_BLOCK => 'Банк',
                FlowOfFundsReportSearch::CASH_ORDER_BLOCK => 'Касса',
                FlowOfFundsReportSearch::CASH_EMONEY_BLOCK => 'E-money'
            ],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_DATE])) {
                    switch ($flows['payment_type']) {
                        case PlanCashFlows::PAYMENT_TYPE_BANK:
                            $checkingAccountant = CheckingAccountant::find()->andWhere([
                                'id' => $flows['checking_accountant_id']
                            ])->orderBy(['type' => SORT_ASC])->one();
                            if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                        'class' => 'little_logo_bank',
                                        'style' => 'display: inline-block;',
                                    ]);
                            }
                            return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_ORDER:
                            return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                        case PlanCashFlows::PAYMENT_TYPE_EMONEY:
                            return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                        default:
                            return '';
                    }
                }
                return '';
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '30%',
                'class' => 'nowrap-normal max10list',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell'
            ],
            'format' => 'raw',
            'filter' => !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE]) ?
                $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
            'value' => function ($flows) use ($searchModel) {
                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE])) {
                    /* @var $contractor Contractor */
                    $contractor = Contractor::findOne($flows['contractor_id']);
                    $model = PlanCashFlows::findOne($flows['id']);

                    return $contractor !== null ?
                        ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                        ($model->cashContractor ?
                            ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                        );
                }

                return '';
            },
            's2width' => '250px',
        ],
        [
            'attribute' => 'description',
            'label' => 'Назначение',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '30%',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell'
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group) && $flows['description']) {
                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                }

                return '';
            },
        ],
        [
            'attribute' => 'reason_id',
            'label' => 'Статья',
            'headerOptions' => [
                'width' => '10%',
            ],
            'filter' => empty($searchModel->group) ?
                array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                ['' => 'Все статьи'],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if (empty($searchModel->group)) {
                    $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                    return $reason ?: '-';
                }

                return '';
            },
            's2width' => '250px',
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{update}<br>{delete}',
            'headerOptions' => [
                'width' => '3%',
            ],
            'contentOptions' => [
                'class' => 'action-line',
            ],
            'visible' => $canDelete,
            'buttons' => [
                'update' => function ($url, $model, $key) use ($searchModel) {
                    return Html::button('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', [
                        'title' => 'Изменить',
                        'class' => 'button-clr link mr-1',
                        'data-toggle' => 'modal',
                        'data-target' => '#update-movement',
                        'data-url' => Url::to([
                            '/reports/finance-ajax/update-plan-item',
                            'id' => $model['id'],
                            'tb' => 'plan_cash_flows'
                        ]),
                    ]);
                },
                'delete' => function ($url, $model) use ($searchModel) {
                    return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                            'class' => 'delete-item-payment-calendar button-clr link',
                            'tag' => 'a',
                        ],
                        'options' => [
                            'id' => 'delete-plan-item-' . $model['id'],
                            'class' => 'modal-delete-plan-item',
                        ],
                        'confirmUrl' => Url::to([
                            '/reports/finance-ajax/delete-flow-item',
                            'id' => $model['id'],
                            'tb' => 'plan_cash_flows'
                        ]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить операцию?',
                    ]);
                },
            ],
        ],
    ],
]);