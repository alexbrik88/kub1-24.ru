<div class="wrap wrap_padding_none mb-2 pt-1">
<div class="col-12 mt-3">

    <h4 class="bold">Описание способов планирования.</h4>
    <p>Планирование можно автоматизировать одним из способов, но лучше их совмещать по разным контрагентам и/или статьям расхода/прихода.</p>

    <!-- invoices -->
    <p class="bold pc-info-types-btn link mr-3" data-target="#pc-info-by-invoices" style="border-bottom: 2px dotted #001424; cursor: pointer;">
        ПО СЧЕТАМ
    </p>
    <br/>
    <div id="pc-info-by-invoices">
        <p class="italic bold" style="margin-bottom: 0">Удобно для: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li><b style="text-decoration: underline">Расходы:</b> Переменных платежей, таких как покупка товаров и услуг с отсрочкой платежа.</li>
            <li><b style="text-decoration: underline">Приходы:</b> По выставленным счетам, с учетом отсрочки.</li>
        </ul>
    
        <p class="italic bold" style="margin-bottom: 0">Как работает: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li>Что бы сумма и дата проставлялась в Платежном календаре нужно.</li>
            <li><b style="text-decoration: underline">Расходы:</b> Загружать счета от Поставщиков.</li>
            <li><b style="text-decoration: underline">Приходы:</b> Выставлять счета клиентам.</li>
        </ul>
    
        <p class="italic bold" style="margin-bottom: 0">Как настроить:</p>
        <ul style="list-style: decimal; padding-left: 17px;">
            <li>Перейдите на закладку <span class="italic">«По счетам».</span><br/>
            По кнопке «Добавить» можно добавить конкретных контрагентов.<br/>
            По кнопке «Настроить правила» можно задать правила по статьям прихода и расхода – это позволит не добавлять контрагентов по одному. Более того плановые операции по НОВЫМ контрагентам, будут автоматически добавляться в Платежный календарь.
            </li>
            <li>Кнопка «Настроить правила» имеет приоритет над добавлением через кнопку «Добавить».</li>
            <li>В списке отобранных контрагентов,  можно исправить количество дней на оплату (отсрочку). По умолчанию стоит 10 дней.</li>
            <li>Создаваемые плановые операции  попадут в «Реестр плановых операций» и в «Таблицу» Платежного календаря. При необходимости плановые операции можно удалить в «Реестре плановых операций»</li>
        </ul>
    </div>
    
    <!-- previous periods -->
    <p class="bold pc-info-types-btn link mr-3" data-target="#pc-info-by-prev-periods" style="border-bottom: 2px dotted #001424; cursor: pointer;">
        ПО ПРЕДЫДУЩИМ ПЛАТЕЖАМ
    </p>
    <br/>
    <div id="pc-info-by-prev-periods">
        <p>Позволяет планировать на несколько месяцев вперед.</p>
    
        <p class="italic bold" style="margin-bottom: 0">Удобно для: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li><b style="text-decoration: underline">Расходы:</b> Постоянные платежи, такие как: Аренда, Интернет, Комиссия банка, Телефония и т.д. Выплату заработной платы так же можно планировать на основе предыдущих данных.</li>
            <li><b style="text-decoration: underline">Приходы:</b> Платежи от постоянных клиентов.</li>
        </ul>
    
        <p class="italic bold" style="margin-bottom: 0">Как работает: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li>Автоматически вычисляется сумма среднего (берутся платежи за предыдущие 3 месяца)  и планируются платежи на каждый следующий месяц до конца текущего года или до даты, которую вы укажите. Дата платежа в месяце берется средняя по предыдущим платежам или указываете дату вы.</li>
        </ul>
    
        <p class="italic bold" style="margin-bottom: 0">Как настроить:</p>
        <ul style="list-style: decimal; padding-left: 17px;">
            <li>Перейдите на закладку <span class="italic">«По предыдущим платежам»</span>, нажмите кнопку «Добавить» и из списка контрагентов с регулярными платежами, выберите те, которые вам нужны для планирования операций. </li>
            <li>В списке отобранных контрагентов,  можно изменить суммы и даты, если это необходимо.</li>
            <li>Создаваемые плановые операции  попадут в «Реестр плановых операций» и в «Таблицу» Платежного календаря. При необходимости плановые операции можно удалить в «Реестре плановых операций»</li>
        </ul>
    </div>

    <!-- manual -->
    <p class="bold pc-info-types-btn link mr-3" data-target="#pc-info-manual">ВРУЧНУЮ</p>
    <br/>
    <div id="pc-info-manual">
        <p class="italic bold" style="margin-bottom: 0">Удобно для: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li>Разовых платежей, например погашение кредита.</li>
        </ul>

        <p class="italic bold" style="margin-bottom: 0">Как работает: </p>
        <ul style="list-style: none; padding-left: 17px;">
            <li>Нажимаете на кнопку «Добавить» в правом верхнем углу. Сумма и дата проставляется в Платежном календаре. </li>
        </ul>

        <p class="italic bold" style="margin-bottom: 0">Как настроить:</p>
        <ul style="list-style: none; padding-left: 17px;">
            <li>Только по нажатию кнопки «Добавить». Данные попадут в «Реестр плановых операций» и в «Таблицу» Платежного календаря. При необходимости плановые операции можно отредактировать или удалить в «Реестре плановых операций»</li>
        </ul>
    </div>

</div>
</div>
<script>
    $('.pc-info-types-btn').on('click', function() {
        $($(this).data("target")).slideToggle(150);
    });
</script>
