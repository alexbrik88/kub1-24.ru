<?php

use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\components\helpers\Html;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\components\grid\GridView;
use yii\widgets\Pjax;
use common\models\company\CheckingAccountant;
use common\components\ImageHelper;
use common\models\cash\CashFlowsBase;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use frontend\rbac\permissions;

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');

/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */
?>
<div class="items-table-block  pt-1 pb-1 hidden">
<div class="row align-items-center pb-1 flex-nowrap">
    <div class="column">
        <h4 class="caption mb-2">Детализация</h4>
    </div>
    <div class="column ml-auto">
        <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
            <!--<label class="label mr-3 mb-2 weight-700 text_size_14 text-dark" for="view">Суммировать</label>
            <div class="dropdown dropdown-view mb-2 mr-1 width-160">
                <input class="form-filter-control" value="Не сумировать" id="view" type="text" autocomplete="off" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="form-control-shevron svg-icon input-toggle text-grey-light">
                    <use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>
                </svg>
                <div class="form-filter-drop dropdown-menu" aria-labelledby="view">
                    <ul class="form-filter-list list-clr">
                        <li><a href="#">Не сумировать</a></li>
                        <li><a href="#">Cумировать</a></li>
                    </ul>
                </div>
            </div>-->
            <a class="button-regular button-regular_red ml-2 mb-2 --close-item-table close-odds" href="javascript:;">Закрыть</a>
        </div>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'odds-items_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>
<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none mb-0">
        <div class="custom-scroll-table">
            <div class="table-wrap">
            <?= GridView::widget([
                'dataProvider' => $itemsDataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list table-odds-flow-details ' . $tabViewClass,
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center joint-checkbox-td',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) {
                            if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                $typeCss = 'income-item';
                                $income = bcdiv($flows['amount'], 100, 2);
                                $expense = 0;
                            } else {
                                $typeCss = 'expense-item';
                                $income = 0;
                                $expense = bcdiv($flows['amount'], 100, 2);
                            }

                            return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                'class' => 'joint-checkbox ' . $typeCss,
                                'value' => $flows['id'],
                                'data' => [
                                    'income' => $income,
                                    'expense' => $expense,
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'value' => function ($flows) use ($searchModel) {
                            return !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE]) ?
                                DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
                        },
                    ],
                    [
                        'attribute' => 'amountIncome',
                        'label' => 'Приход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell',
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {

                            return \common\components\TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                        },
                    ],
                    [
                        'attribute' => 'amountExpense',
                        'label' => 'Расход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'class' => 'sum-cell',
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {

                            return \common\components\TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);
                        },
                    ],
                    [
                        'attribute' => 'payment_type',
                        'label' => 'Тип оплаты',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            FlowOfFundsReportSearch::CASH_BANK_BLOCK => 'Банк',
                            FlowOfFundsReportSearch::CASH_ORDER_BLOCK => 'Касса',
                            FlowOfFundsReportSearch::CASH_EMONEY_BLOCK => 'E-money',
                            FlowOfFundsReportSearch::CASH_ACQUIRING_BLOCK => 'Монета',
                        ],
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_DATE])) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        /* @var $checkingAccountant CheckingAccountant */
                                        $checkingAccountant = CheckingAccountant::find()->andWhere([
                                            'rs' => $model->rs,
                                            'company_id' => $model->company->id,
                                        ])->orderBy(['type' => SORT_ASC])->one();
                                        if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                            return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                    'class' => 'little_logo_bank',
                                                    'style' => 'display: inline-block;',
                                                ]);
                                        }
                                        return 'Банк <i class="fa fa-bank m-r-sm" style="color: #9198a0;"></i>';
                                    case CashOrderFlows::tableName():
                                        return 'Касса <i class="fa fa-money m-r-sm" style="color: #9198a0;"></i>';
                                    case CashEmoneyFlows::tableName():
                                        return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #9198a0;"></i>';
                                    case AcquiringOperation::tableName():
                                        return 'Монета';
                                    default:
                                        return '';
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        's2width' => '250px',
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'contractor-cell'
                        ],
                        'format' => 'raw',
                        'filter' => !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE]) ?
                            $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE])) {
                                /* @var $contractor Contractor */
                                $contractor = Contractor::findOne($flows['contractor_id']);
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $model = CashEmoneyFlows::findOne($flows['id']);
                                        break;
                                    case AcquiringOperation::tableName():
                                        $model = AcquiringOperation::findOne($flows['id']);
                                        break;
                                    default:
                                        return '';
                                }
                                return $contractor !== null ?
                                    ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                    ($model->cashContractor ?
                                        ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                    );
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '30%',
                        ],
                        'contentOptions' => [
                            'class' => 'purpose-cell'
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                if ($flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                } else {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        default:
                                            return '';
                                    }
                                    if ($flows['tb'] !== AcquiringOperation::tableName() && $invoiceArray = $model->getInvoices()->all()) {
                                        $linkArray = [];
                                        foreach ($invoiceArray as $invoice) {
                                            $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ]);
                                        }

                                        return join(', ', $linkArray);
                                    }
                                }
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'billPaying',
                        'label' => 'Опл. счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        $billInvoices = $model->getBillInvoices();
                                        if ($billInvoices && $model->getAvailableAmount() > 0) {
                                            return $billInvoices . '<br />' .
                                                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', '/cash/order/update?id=' . $model->id, [
                                                    'title' => 'Уточнить',
                                                    'data' => [
                                                        'toggle' => 'modal',
                                                        'target' => '#update-movement',
                                                    ],
                                                ]);
                                        }

                                        return $billInvoices;
                                    default:
                                        return '';
                                }

                                return $model->billPaying;
                            }

                            return '';
                        },
                    ],
                    [
                        's2width' => '200px',
                        'attribute' => 'reason_id',
                        'label' => 'Статья',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'filter' => empty($searchModel->group) ?
                            array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                            ['' => 'Все статьи'],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                                return $reason ?: '-';
                            }

                            return '';
                        },
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update}<br>{delete}',
                        'headerOptions' => [
                            'width' => '3%',
                        ],
                        'contentOptions' => [
                            'class' => 'action-line',
                        ],
                        'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                        'buttons' => [
                            'update' => function ($url, $model, $key) use ($searchModel) {
                                $url = 'javascript:;';
                                switch ($model['tb']) {
                                    case CashBankFlows::tableName():
                                        $url = Url::to(['/cash/bank/update', 'id' => $model['id']]);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $url = Url::to(['/cash/order/update', 'id' => $model['id']]);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $url = Url::to(['/cash/e-money/update', 'id' => $model['id']]);
                                        break;
                                    case AcquiringOperation::tableName():
                                        $url = Url::to(['/acquiring/acquiring/update', 'id' => $model['id']]);
                                        break;
                                }
                                return Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', $url, [
                                    'title' => 'Изменить',
                                    'class' => 'update-flow-item button-clr link mr-1',
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#update-movement',
                                        'pjax' => 0
                                    ],
                                ]);
                            },
                            'delete' => function ($url, $model) use ($searchModel) {
                                return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                                        'class' => 'delete-flow-item button-clr link',
                                        'tag' => 'a',
                                    ],
                                    'options' => [
                                        'id' => 'delete-flow-item-' . $model['id'],
                                        'class' => 'modal-delete-flow-item',
                                    ],
                                    'confirmUrl' => Url::to([
                                        '/reports/finance-ajax/delete-flow-item',
                                        'id' => $model['id'],
                                        'tb' => $model['tb']
                                    ]),
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите удалить операцию?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>

<?= $this->render('_summary_select/_summary_select', [
    'buttons' => [
        $canUpdate ? \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
        $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
    ],
]); ?>

<?= $this->render('_item_table/modals', ['searchModel' => $searchModel]) ?>

<?php
$js = <<<SCRIPT

// SummarySelect

$(document).on('change', '.joint-checkbox', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

$(document).on("pjax:complete", "#odds-items_pjax", function(e) {
    $('#summary-container').removeClass('visible check-true');
});



SCRIPT;

$this->registerJs($js);
?>

</div>

<?php $this->registerJs('
$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();

    var button = $(event.relatedTarget);
    var modal = $(this);

    modal.find(".modal-body").load(button.attr("href"));

});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(".close-item-table").on("click", function() {
    var $block = $(".items-table-block");
    $block.find(".caption").html("Детализация");
    $block.find(".wrap_padding_none").html("");
    $block.addClass("hidden");
});
'); ?>