<?php

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\OddsSearch;

/* @var $this yii\web\View
 * @var $searchModel OddsSearch
 * @var $data []
 * @var $growingData []
 * @var $totalData []
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 */

$isCurrentYear = $searchModel->isCurrentYear;

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');

$rowspan = 3;
foreach ($data as $paymentType => $level1) {
    $rowspan+=2;
    foreach ($level1['levels'] as $paymentFlowType => $level2) {
        $rowspan++;
    }
}
$isFirstRow = true;
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
    <table class="flow-of-funds odds-table by_purse table table-style table-count-list <?= $tabViewClass ?> mb-0">
        <thead>
            <?= $this->render('odds2_table_head', [
                'searchModel' => $searchModel,
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'currentMonthNumber' => $currentMonthNumber,
            ]) ?>
        </thead>

        <tbody>
        <?php

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $level1['title'],
                'data' => $level1['data'],
                'trClass' => 'main-block not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'currentMonthNumber' => $currentMonthNumber,
                'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                'question' => $level1['question'] ?? null,
                'isFirstRow' => $isFirstRow,
                'rowspan' => $rowspan,
            ]);
            $isFirstRow = false;

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $floorKey = "floor-{$paymentFlowType}";
                $isOpenedFloor = ArrayHelper::getValue($floorMap, $floorKey);
                $isIncome = in_array($paymentFlowType, ($activeTab == OddsSearch::TAB_ODDS) ? [1,3,4] : [1,3,5,7]);

                echo $this->render('odds2_table_row', [
                    'level' => 2,
                    'title' => $level2['title'],
                    'data' => $level2['data'],
                    'trClass' => 'not-drag expenditure_type sub-block ' . ($isIncome ? 'income' : 'expense'),
                    'trId' => $paymentFlowType,
                    'dMonthes' => $d_monthes,
                    'cellIds' => $cell_id,
                    'currentMonthNumber' => $currentMonthNumber,
                    'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                    'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                    'floorKey' => $floorKey,
                    'isOpenedFloor' => $isOpenedFloor,
                ]);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    echo $this->render('odds2_table_row', [
                        'level' => 3,
                        'title' => $level3['title'],
                        'data' => $level3['data'],
                        'trClass' => 'item-block ' . ($isIncome ? 'income' : 'expense'),
                        'dMonthes' => $d_monthes,
                        'cellIds' => $cell_id,
                        'currentMonthNumber' => $currentMonthNumber,
                        'dataPaymentType' => ($activeTab == OddsSearch::TAB_ODDS) ? null : $paymentType,
                        'dataFlowType' => ($activeTab == OddsSearch::TAB_ODDS ? null : ($isIncome ? '1' : '0')),
                        'dataType' => $paymentFlowType,
                        'dataItemId' => $itemId,
                        'dataId' => $floorKey,
                        'isOpenedFloor' => $isOpenedFloor,
                        'isSortable' => ($activeTab == OddsSearch::TAB_ODDS)
                    ]);

                }
            }

            // GROWING
            echo $this->render('odds2_table_row', [
                'level' => 1,
                'title' => $growingData[$paymentType]['title'],
                'data' => $growingData[$paymentType]['data'],
                'trClass' => 'not-drag cancel-drag',
                'dMonthes' => $d_monthes,
                'cellIds' => $cell_id,
                'currentMonthNumber' => $currentMonthNumber,
                'dataPaymentType' => $paymentType,
                'isSumQuarters' => false,
                'canHover' => false
            ]);
        }

        // TOTAL BY MONTH
        echo $this->render('odds2_table_row', [
            'level' => 1,
            'title' => $totalData['month']['title'],
            'data' => $totalData['month']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'currentMonthNumber' => $currentMonthNumber,
            'canHover' => false
        ]);

        // TOTAL BY YEAR
        echo $this->render('odds2_table_row', [
            'level' => 1,
            'title' => $totalData['growing']['title'],
            'data' => $totalData['growing']['data'],
            'trClass' => 'not-drag cancel-drag',
            'dMonthes' => $d_monthes,
            'cellIds' => $cell_id,
            'currentMonthNumber' => $currentMonthNumber,
            'isSumQuarters' => false,
            'canHover' => false
        ]);

        ?>
        </tbody>
    </table>
    </div>
</div>