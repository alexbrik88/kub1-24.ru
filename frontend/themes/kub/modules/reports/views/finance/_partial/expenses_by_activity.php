<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:54
 */

use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use frontend\modules\reports\models\ExpensesSearch;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_expenses');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
                </th>
            </tr>
            <tr>
                <?php foreach (ExpensesSearch::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2" style="border-top-color: transparent"><div class="pl-1 pr-1">Итого</div></th>
            </tr>
            </thead>

            <tbody>
            <?php $class = null ?>

            <?php $floorKey1 = "first-floor-".ExpensesSearch::BLOCK_EXPENSES; ?>
            <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1, true); ?>

            <tr class="main-block not-drag cancel-drag expenditure_type" id="<?= ExpensesSearch::BLOCK_EXPENSES; ?>">
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            <button class="table-collapse-btn button-clr <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="weight-700 text_size_14 ml-1">Расходы</span>
                            </button>
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = isset($data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]) ?
                        $data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]['flowSum'] : 0; ?>

                    <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                        </div>
                    </td>
                    <?php $quarterSum += $amount; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= ExpensesSearch::BLOCK_EXPENSES; ?>">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= isset($data['types'][ExpensesSearch::BLOCK_EXPENSES]['totalFlowSum']) ?
                            TextHelper::invoiceMoneyFormat($data['types'][ExpensesSearch::BLOCK_EXPENSES]['totalFlowSum'], 2) : 0; ?>
                    </div>
                </td>
            </tr>

            <?php if (isset($data[ExpensesSearch::BLOCK_EXPENSES])): ?>
                <?php foreach ($data['itemName'][ExpensesSearch::BLOCK_EXPENSES] as $expenditureItemID => $expenditureItemName): ?>
                    <?php if (isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID])): ?>
                        <tr class="item-block <?= !$isOpenedFloor1 ? 'd-none':'' ?> <?= $class; ?>" data-id="<?= $floorKey1 ?>" data-type_id="<?= ExpensesSearch::BLOCK_EXPENSES; ?>" data-item_id="<?= $expenditureItemID; ?>">
                            <td class="pl-2 pr-1 pt-3 pb-3">
                                <span class="text-grey text_size_14 m-l-purse"><?= $expenditureItemName; ?></span>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = (int)ceil($key / 3);
                                $amount = isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]) ?
                                    $data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                ?>
                                <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat($amount, 2) ?>
                                    </div>
                                </td>
                                <?php $quarterSum += $amount; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                        <div class="pl-1 pr-1">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                            $quarterSum = 0; ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach ?>
                            <td class="can-hover total-block pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>">
                                <div class="pl-1 pr-1">
                                    <?= isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID]['totalFlowSum']) ?
                                        TextHelper::invoiceMoneyFormat($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <!-- TOTALS -->
            <tr class="not-drag cancel-drag">
                <td class="pl-2 pr-1 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Итого на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $amount = isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0; ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </div>
                    </td>
                    <?php $quarterSum += $amount; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap remainder-month-empty">
                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                        <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance']['totalFlowSum']) ? $data['growingBalance']['totalFlowSum'] : 0, 2); ?>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>