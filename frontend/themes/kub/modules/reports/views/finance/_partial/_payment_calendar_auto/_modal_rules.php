<?php

use common\models\cash\CashFlowsBase;
use frontend\modules\reports\models\PlanCashRule;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$incomeSelection = PlanCashRule::find()->where(['company_id' => Yii::$app->user->identity->company->id, 'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])->select('income_item_id')->column();
$expenditureSelection = PlanCashRule::find()->where(['company_id' => Yii::$app->user->identity->company->id, 'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])->select('expenditure_item_id')->column();
?>

<!-- MODAL UPDATE AUTOPLAN RULES -->
<div class="modal fade" id="update-autoplan-rules" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Правила создания плановых операций</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'form_auto_plan_rules',
                    'action' => '/reports/auto-plan-ajax/update-rules',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => false,
                    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
                ]); ?>

                <?php /*
                    <?= Html::radioList('flowType', 1, [
                        CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
                        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
                    ], [
                        'uncheck' => null,
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::radio($name, $checked, [
                                'class' => 'js_autoplan_rules_flow_type_input',
                                'value' => $value,
                                'label' => $label,
                                'labelOptions' => [
                                    'class' => 'mb-3 mr-3 mt-2',
                                ],
                            ]);
                        },
                    ]); ?>
                */ ?>

                    <div class="js_autoplan_rules_flow_type income mb-4">

                        <?= \yii\helpers\Html::checkbox(null, !empty(array_filter($incomeSelection)), [
                            'class' => 'by-flow-type-main-checkbox',
                            'label' => '<strong>По Исходящим счетам</strong> (Создание плановых операций по Приходу)',
                        ]) ?>

                        <div class="mb-2 ml-2">
                            <div style="font-style: italic; margin-left: 20px;">Правила распространяется как на ТЕКУЩИХ, так и на НОВЫХ покупателей.</div>
                            <div style="margin-left: 20px;">
                            <?= Html::checkboxList('income_items_ids', $incomeSelection, PlanCashRule::INCOME_ITEMS_IDS, [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return Html::checkbox($name, $checked, [
                                            'value' => $value,
                                            'label' => $label,
                                            'class' => 'by-flow-type-checkbox',
                                            'labelOptions' => [
                                                'class' => 'mt-2 mb-2 label',
                                            ],
                                        ]).'<br/>';
                                },
                            ]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="js_autoplan_rules_flow_type expense">

                        <?= \yii\helpers\Html::checkbox(null, !empty(array_filter($expenditureSelection)), [
                            'class' => 'by-flow-type-main-checkbox',
                            'label' => '<strong>По Входящим счетам</strong> (Создание плановых операций по Расходу)',
                        ]) ?>

                        <div class="mb-2 ml-2">
                            <div style="font-style: italic; margin-left: 20px;">Правила распространяется как на ТЕКУЩИХ, так и на НОВЫХ поставщиков.</div>
                            <div style="margin-left: 20px;">
                            <?= Html::checkboxList('expenditure_items_ids', $expenditureSelection, PlanCashRule::EXPENDITURE_ITEMS_IDS, [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return Html::checkbox($name, $checked, [
                                        'value' => $value,
                                        'label' => $label,
                                        'class' => 'by-flow-type-checkbox',
                                        'labelOptions' => [
                                            'class' => 'mt-2 mb-2 label',
                                        ],
                                    ]).'<br/>';
                                },
                            ]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                        ]); ?>
                        <div class="mr-auto ml-2 mt-1 hidden msg-saved-rules italic" style="color:#9198a0">Настройка правил.<br>Может занять несколько минут.</div>
                        <button type="button" class="button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                <?php $form->end() ?>
            </div>
        </div>
    </div>
</div>