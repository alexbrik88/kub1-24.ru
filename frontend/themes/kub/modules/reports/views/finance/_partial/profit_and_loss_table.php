<?php

use common\components\helpers\ArrayHelper;use common\components\helpers\Html;
use common\components\TextHelper;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use frontend\themes\kub\helpers\Icon;
use kartik\checkbox\CheckboxX;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel ProfitAndLossSearchModel
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $data array
 * @var $activeTab int
 * @var $floorMap array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;
$floorMap = $floorMap ?? [];

if (!empty($floorMap)) {
    $d_monthes = [
        1 => ArrayHelper::getValue($floorMap, 'first-cell'),
        2 => ArrayHelper::getValue($floorMap, 'second-cell'),
        3 => ArrayHelper::getValue($floorMap, 'third-cell'),
        4 => ArrayHelper::getValue($floorMap, 'fourth-cell'),
    ];
} else {
    $d_monthes = [
        1 => $currentMonthNumber < 4 && $isCurrentYear,
        2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
        3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
        4 => $currentMonthNumber > 9 && $isCurrentYear
    ];
}

// for taxable view
$AMOUNT_KEY = ($activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'amountTax' : 'amount';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_profit_loss');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <span class="text-grey weight-700 ml-1 nowrap">
                        <?= $searchModel->year ?>
                    </span>
                </th>
            </tr>
            <tr>
                <?php foreach (ProfitAndLossSearchModel::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2">
                    <div class="pl-1 pr-1">Итого</div>
                </th>
            </tr>
            </thead>

            <tbody>

            <?php $floorID = 0; ?>
            <?php foreach ($data as $row): ?>
                <?php
                $currentQuarter = (int)ceil(date('m') / 3);
                $suffix = ArrayHelper::remove($row, 'suffix');
                $options = ArrayHelper::remove($row, 'options', []);
                $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
                $classBold = (!$includedRow) ? 'weight-700' : '';
                if (!empty($row['data-article'])) {
                    $canClick = 'can-click';
                    $dataClick = implode(' ', [
                        'data-article="' . $row['data-article'] . '"',
                        'data-movement-type="' . $row['data-movement-type'] . '"'
                    ]);
                } else {
                    $canClick = $dataClick = '';
                }
                ?>

                <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']): ?>

                    <?php $floorID++; ?>
                    <?php $floorKey1 = "first-floor-{$floorID}"; ?>
                    <?php $isOpenedFloor1 = ArrayHelper::getValue($floorMap, $floorKey1); ?>

                    <tr class="not-drag expenditure_type sub-block">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <button class="table-collapse-btn button-clr ml-1 text-left <?= $isOpenedFloor1 ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey1 ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="d-block weight-700 text_size_14 ml-1"><?= $row['label']; ?></span>
                            </button>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($row[$AMOUNT_KEY][$monthNumber]) ? $row[$AMOUNT_KEY][$monthNumber] : 0;
                            ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 weight-700">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                </div>
                            </td>
                            <?php $quarterSum = isset($row[$AMOUNT_KEY]['quarter-' . $quarter]) ? $row[$AMOUNT_KEY]['quarter-' . $quarter] : 0; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 weight-700">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 weight-700">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>
                    </tr>

                <?php else: ?>

                    <tr class="item-block <?= ($includedRow && !$isOpenedFloor1) ? 'd-none' : '' ?>" <?= ($includedRow) ? 'data-id="'.$floorKey1.'"' : '' ?> >
                        <td class="pl-2 pr-1 pt-3 pb-3">
                            <span class="d-block <?= $classBold ?: 'text-grey' ?> text_size_14 <?= $classBold ? 'ml-1 mr-1' : 'm-l-purse' ?>">
                                <?= $row['label']; ?>
                                <?php if ('Налоги' === $row['label']): ?>
                                    <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                        'class' => 'tooltip2',
                                        'data-tooltip-content' => '#tooltip_profit_loss_tax_item',
                                    ]) ?>
                                <?php endif; ?>
                            </span>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($row[$AMOUNT_KEY][$monthNumber]) ? $row[$AMOUNT_KEY][$monthNumber] : 0;
                            ?>
                            <td class="<?= $canClick ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-month="<?= (int)$monthNumber ?>">
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>

                                </div>
                            </td>
                            <?php $quarterSum = isset($row[$AMOUNT_KEY]['quarter-' . $quarter]) ? $row[$AMOUNT_KEY]['quarter-' . $quarter] : 0; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="<?= $canClick ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>" <?= $dataClick ?> data-quarter="<?= $quarter ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td class="<?= $canClick ?> total-value pl-2 pr-2 pt-3 pb-3 nowrap" <?= $dataClick ?>>
                            <div class="pl-1 pr-1 <?= $classBold ?>">
                                <?php $totalSum = isset($row[$AMOUNT_KEY]['total']) ? $row[$AMOUNT_KEY]['total'] : 0; ?>
                                <?php echo TextHelper::invoiceMoneyFormat($totalSum, 2) . $suffix; ?>
                            </div>
                        </td>
                    </tr>

                <?php endif; ?>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

<div style="display: none">
    <div id="tooltip_profit_loss_tax_item">
        Налог считается по операциям<br/>
        с пометкой "Для бухгалтерии".<br/>
        Сумма налога может отличаться<br/>
        от налога, рассчитанного вашей<br/>
        бухгалтерией, если вы не все<br/>
        данные внесли в КУБ24.
    </div>
</div>