<?php

use common\models\service\ServiceModule;
use frontend\components\Icon;
use yii\helpers\Html;

?>

<?= Icon::get('block', [
    'style' => 'font-size: 100px;',
]) ?>
<br>
<br>
Данный отчет полностью доступен только в блоке ФинДиректор
<br>
<br>
<?= Html::a('Перейти в ФинДиректор', [
    '/analytics/options/start',
], [
    'class' => 'button-regular button-hover-transparent px-3',
]) ?>
