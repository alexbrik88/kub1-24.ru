<?php
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\reports\models\PaymentCalendarSearch;
use yii\widgets\Pjax;
use yii\bootstrap4\Html;
use frontend\rbac\permissions;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $activeTab integer
 * @var $subTab integer
 */

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
?>

<?php $pjax = Pjax::begin([
    'id' => 'odds-items_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'scrollTo' => false,
    'formSelector' => '#searchByContractor'
]); ?>

<?php if (isset($itemsDataProvider)): ?>
    <?= $this->render('_plan_table', [
        'searchModel' => $searchModel,
        'itemsDataProvider' => $itemsDataProvider,
        'activeTab' => $activeTab,
        'subTab' => $subTab,
        'canDelete' => $canDelete
    ]) ?>
<?php endif; ?>

<?php Pjax::end(); ?>

<?= $this->render('_summary_select/_summary_select', [
    'buttons' => [
        $canUpdate ? \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
        $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
    ],
]); ?>