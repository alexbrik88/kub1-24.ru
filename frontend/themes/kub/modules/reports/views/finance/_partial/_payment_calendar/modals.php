<?php

use common\components\helpers\Html;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="modal fade" id="save-repeated-flows" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="font-size: 16px;">
                    Изменения сохранить для
                </div>
                <?= Html::radioList('update-repeated-operations', PlanCashFlows::UPDATE_REPEATED_ONLY_ONE, PlanCashFlows::$updateRepeatedOperations, [
                    'class' => 'col-md-12',
                    'id' => 'update-repeated-operations',
                    'style' => 'margin-top: 20px;',
                ]); ?>
                <div class="form-actions col-md-12" style="margin-top: 20px;">
                    <div class="row action-buttons">
                        <div class="col-sm-5 col-xs-5">
                            <?= Html::button('Сохранить', [
                                'class' => 'btn darkblue text-white widthe-100 update-repeated-operations-button',
                            ]); ?>
                        </div>
                        <div class="col-sm-2 col-xs-2"></div>
                        <div class="col-sm-5 col-xs-5">
                            <?= Html::button('Отменить', [
                                'class' => 'btn darkblue text-white pull-right widthe-100',
                                'data-dismiss' => 'modal',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add-movement" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить плановую операцию</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <!-- load by js -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="update-movement" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Редактировать плановую операцию</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <!-- load by js -->
            </div>
        </div>
    </div>
</div>
<div id="many-delete" class="modal-many-delete-plan-item confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['/reports/finance-ajax/many-delete-flow-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-many-plan-item" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['/reports/finance-ajax/many-flow-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-income_item_id_wrapper',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <?= Html::radio(null, true, [
                                        'label' => 'Расход изменить на:',
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-expenditure_item_id_wrapper required',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script>

    // SHOW_AJAX_MODAL ADD
    $("#add-movement").on("show.bs.modal", function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        if ($(button).attr("data-url")) {
            $.post($(button).attr("data-url"), null, function (data) {
                modal.find(".modal-body").html(data);
                refreshDatepicker();
                refreshUniform();
            });
        }
    });
    $("#add-movement").on("hidden.bs.modal", function(event) {
        if (event.target.id === "add-movement") {
            $("#add-movement .modal-body").empty();
        }
    });
    // SHOW_AJAX_MODAL UPDATE
    $("#update-movement").on("show.bs.modal", function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        if ($(button).attr("data-url")) {
            $.post($(button).attr("data-url"), null, function (data) {
                modal.find(".modal-body").html(data);
                refreshDatepicker();
                refreshUniform();
            });
        }
    });
    $("#update-movement").on("hidden.bs.modal", function(event) {
        if (event.target.id === "update-movement") {
            $("#update-movement .modal-body").empty();
        }
    });

</script>