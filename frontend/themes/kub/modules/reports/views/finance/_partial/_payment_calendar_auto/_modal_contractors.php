<?php

use common\models\document\InvoiceExpenditureItem;use common\models\document\InvoiceIncomeItem;use frontend\modules\reports\models\ContractorSimpleSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashContractor;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;

/**
 * @var $calendarSearchModel PaymentCalendarSearch
 */

$flowType = Yii::$app->request->get('flowType', 1);

$incomeItemsArr = InvoiceIncomeItem::find()->select(['name', 'id'])->where([
    'or',
    ['company_id' => null],
    ['company_id' =>Yii::$app->user->identity->company->id],
])->indexBy('id')->column();

$expensesItemsArr = InvoiceExpenditureItem::find()->select(['name', 'id'])->where([
    'or',
    ['company_id' => null],
    ['company_id' => Yii::$app->user->identity->company->id],
])->indexBy('id')->column();

?>

<!-- MODAL ADD CONTRACTOR BY INVOICES -->
<?php
$excludeContractorsIds = PlanCashContractor::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'flow_type' => $flowType
])->select('contractor_id')->column();

$searchModel = new ContractorSimpleSearch([
    'company_id' => Yii::$app->user->identity->company->id,
    'type' => $flowType + 1,
    'excludeIds' => $excludeContractorsIds
]);
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>
<div class="modal fade" id="add-autoplan-contractor" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Выбрать из списка</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $pjax = Pjax::begin([
                    'id' => 'autoplan_contractors_pjax',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                    'timeout' => 10000,
                    'scrollTo' => false,
                ]); ?>
                <?= $this->render('_contractors_table', [
                    'planType' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                    'flowType' => $flowType,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'calendarSearchModel' => $calendarSearchModel,
                    //'contractorsToItemsIds' => [],
                    'incomeItemsArr' => $incomeItemsArr,
                    'expenseItemsArr' => $expensesItemsArr,
                    'showAmountColumn' => false
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ADD CONTRACTOR BY PREV PERIODS -->
<?php
$findedContractorsItems = $calendarSearchModel->getAutoPlanContractors($flowType);
$contractorsToItemsIds = [];
foreach ($findedContractorsItems as $flow) {
    foreach ($flow as $iId => $cIds) {
        foreach ($cIds as $cId) {

            if (!isset($contractorsToItemsIds[$cId]))
                $contractorsToItemsIds[$cId] = [];

            $contractorsToItemsIds[$cId][] = $iId;
        }
    }
}
$byIds = array_keys($contractorsToItemsIds) ?: ['-1'];

$excludeContractorsIds = PlanCashContractor::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'flow_type' => $flowType,
    'plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
])->select('contractor_id')->column();

$searchModel2 = new ContractorSimpleSearch([
    'company_id' => Yii::$app->user->identity->company->id,
    'type' => $flowType + 1,
    'byIds' => array_unique($byIds),
    'excludeIds' => $excludeContractorsIds
]);
$dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
?>
<div class="modal fade" id="add-autoplan-contractor-prev" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Выбрать из списка</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $pjax = Pjax::begin([
                    'id' => 'autoplan_contractors_pjax_prev',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                    'timeout' => 10000,
                    'scrollTo' => false,
                ]); ?>
                <?= $this->render('_contractors_table', [
                    'planType' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS,
                    'flowType' => $flowType,
                    'searchModel' => $searchModel2,
                    'dataProvider' => $dataProvider2,
                    'calendarSearchModel' => $calendarSearchModel,
                    'contractorsToItemsIds' => $contractorsToItemsIds,
                    'incomeItemsArr' => $incomeItemsArr,
                    'expenseItemsArr' => $expensesItemsArr,
                    'showAmountColumn' => true
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<!-- MODAL REMOVE CONTRACTOR -->
<div id="delete-autoplan-contractor" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить контрагента?</h4>
            <div class="text-center">
                <?= Html::a('Да', null, [
                    'class' => 'delete-contractor-modal-btn button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => null,
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>