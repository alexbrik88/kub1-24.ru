<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 1:01
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\PlanFactSearch;
use common\components\helpers\Html;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PlanFactSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $checkMonth boolean
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_plan_fact');
?>

<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style table-count-list <?= $tabViewClass ?> mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="pl-1 pr-1"><?= $searchModel->year; ?></div>
                </th>
            </tr>
            <tr>
                <?php foreach (PlanFactSearch::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="pl-1 pr-1">Итого</div>
                </th>
            </tr>
            </thead>

            <tbody>
            <?php foreach (PlanFactSearch::$types as $typeID => $typeName): ?>
                <?php $class = in_array($typeID, [
                    PlanFactSearch::RECEIPT_FINANCING_TYPE_FIRST,
                    PlanFactSearch::INCOME_OPERATING_ACTIVITIES,
                    PlanFactSearch::INCOME_INVESTMENT_ACTIVITIES]) ?
                    'income' : 'expense'; ?>

                <?php if ($class == 'income'): ?>
                    <tr class="main-block">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= PlanFactSearch::$blocks[PlanFactSearch::$blockByType[$typeID]]; ?>
                                    <?php if ($typeID == PlanFactSearch::RECEIPT_FINANCING_TYPE_FIRST): ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_financial-operations-block',
                                        ]) ?>
                                    <?php elseif ($typeID == PlanFactSearch::INCOME_OPERATING_ACTIVITIES): ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_operation-activities-block',
                                        ]) ?>
                                    <?php else: ?>
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_investment-activities-block',
                                        ]) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $totalFlowSum = isset($data['blocks'][PlanFactSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum']) ?
                                $data['blocks'][PlanFactSearch::$blockByType[$typeID]]['totalFlowSum']['flowSum'] : 0;
                            $flowSum = isset($data['blocks'][PlanFactSearch::$blockByType[$typeID]][$monthNumber]) ?
                                $data['blocks'][PlanFactSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0;
                            if ($isCurrentYear && $monthNumber > date('m')) {
                                $totalFlowSum -= $flowSum;
                                $flowSum = 0;
                            }; ?>

                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($flowSum, 2) ?>
                                </div>
                            </td>
                            <?php $quarterSum += $flowSum; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td class="total-block pl-2 pr-2 pt-3 pb-3 nowrap weight-700">
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($totalFlowSum, 2) ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>

                <tr class="not-drag expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>">
                    <td class="pl-2 pr-2 pt-3 pb-3">
                        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-row-trigger data-target="first-floor-<?= $typeID ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey text_size_14 ml-1"><?= $typeName; ?></span>
                        </button>
                    </td>
                    <?php $key = $quarterSum = 0; ?>
                    <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3);
                        $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                        $totalFlowSum = isset($data['types'][$typeID]['totalFlowSum']) ?
                            $data['types'][$typeID]['totalFlowSum'] : 0;
                        $flowSum = isset($data['types'][$typeID][$monthNumber]) ?
                            $data['types'][$typeID][$monthNumber]['flowSum'] : 0;
                        if ($isFutureDate) {
                            $totalFlowSum -= $flowSum;
                            $flowSum = 0;
                        };
                        $tooltipData = $searchModel->getTooltipData(
                            PlanFactSearch::$flowTypeByActivityType[$typeID],
                            $searchModel->getItemsByActivityTypeID($typeID),
                            $monthNumber,
                            $monthNumber,
                            $flowSum
                        ); ?>
                        <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                            data-collapse-cell
                            data-id="<?= $cell_id[$quarter] ?>"
                            data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                            data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                            data-diff-amount="<?= $tooltipData['diff']; ?>"
                            data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                            style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation'], true); ?>;"
                        >
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                            </div>
                        </td>
                        <?php $quarterSum += $flowSum; ?>
                        <?php if ($key % 3 == 0): ?>
                            <?php $monthStart = ($monthNumber - 2) < 10 ? ('0' . ($monthNumber - 2)) : $monthNumber;
                            $tooltipData = $searchModel->getTooltipData(
                                PlanFactSearch::$flowTypeByActivityType[$typeID],
                                $searchModel->getItemsByActivityTypeID($typeID),
                                $monthStart,
                                $monthNumber,
                                $quarterSum
                            ); ?>
                            <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                data-collapse-cell-total
                                data-id="<?= $cell_id[$quarter] ?>"
                                data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                data-diff-amount="<?= $tooltipData['diff']; ?>"
                                data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation'], true); ?>;"
                            >
                                <div class="pl-1 pr-1">
                                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                    $quarterSum = 0; ?>
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php $tooltipData = $searchModel->getTooltipData(
                        PlanFactSearch::$flowTypeByActivityType[$typeID],
                        $searchModel->getItemsByActivityTypeID($typeID),
                        PlanFactSearch::JANUARY,
                        date('m'),
                        $totalFlowSum
                    ); ?>
                    <td class="total-block tooltip2-hover pl-2 pr-2 pt-3 pb-3 nowrap total-flow-sum-<?= $typeID; ?>"
                        data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                        data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                        data-diff-amount="<?= $tooltipData['diff']; ?>"
                        data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                        style="background-color: <?= $searchModel->getItemColor($tooltipData['deviation'], true); ?>;">
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($totalFlowSum, 2); ?>
                            </div>
                    </td>
                </tr>

                <?php if (isset($data[$typeID])): ?>
                    <?php foreach ($data['itemName'][$typeID] as $expenditureItemID => $expenditureItemName): ?>
                        <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                            <tr class="item-block d-none <?=  $class; ?>" data-id="first-floor-<?= $typeID ?>" data-type_id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>">
                                <td class="pl-3 pr-3 pt-3 pb-3">
                                    <span class="text-grey text_size_14 m-l-repo"><?= $expenditureItemName; ?></span>
                                    <!--<button class="table-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center" type="button" data-collapse-row-trigger data-target="second-floor">
                                        <svg class="svg-icon text_size-14 text-grey mr-2 flex-shrink-0">
                                            <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                        </svg>
                                        <span class="text-grey text_size_14 ml-1 mr-1">АйТи</span>
                                        <svg class="svg-icon-triangle svg-icon text_size_14 text-grey-light ml-auto flex-shrink-0">
                                            <use xlink:href="/img/svg/svgSprite.svg#triangle"></use>
                                        </svg>
                                    </button>-->
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = (int)ceil($key / 3);
                                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                                    $totalFlowSum = isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                        $data[$typeID][$expenditureItemID]['totalFlowSum'] : 0;
                                    $flowSum = isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                        $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                    if ($isFutureDate) {
                                        $totalFlowSum -= $flowSum;
                                        $flowSum = 0;
                                    };
                                    $tooltipData = $searchModel->getTooltipData(
                                        PlanFactSearch::$flowTypeByActivityType[$typeID],
                                        [$expenditureItemID],
                                        $monthNumber,
                                        $monthNumber,
                                        $flowSum
                                    ); ?>
                                    <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                                        data-collapse-cell
                                        data-id="<?= $cell_id[$quarter] ?>"
                                        data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                        data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                        data-diff-amount="<?= $tooltipData['diff']; ?>"
                                        data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                        style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation'], true); ?>;"
                                    >
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2) ?>
                                        </div>
                                    </td>
                                    <?php $quarterSum += $flowSum; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <?php $monthStart = ($monthNumber - 2) < 10 ? ('0' . ($monthNumber - 2)) : $monthNumber;
                                        $tooltipData = $searchModel->getTooltipData(
                                            PlanFactSearch::$flowTypeByActivityType[$typeID],
                                            [$expenditureItemID],
                                            $monthStart,
                                            $monthNumber,
                                            $quarterSum
                                        ); ?>
                                        <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                            data-collapse-cell-total
                                            data-id="<?= $cell_id[$quarter] ?>"
                                            data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                            data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                            data-diff-amount="<?= $tooltipData['diff']; ?>"
                                            data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                            style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation'], true); ?>;"
                                        >
                                            <div class="pl-1 pr-1">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                $quarterSum = 0; ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach ?>
                                <?php $tooltipData = $searchModel->getTooltipData(
                                    PlanFactSearch::$flowTypeByActivityType[$typeID],
                                    [$expenditureItemID],
                                    PlanFactSearch::JANUARY,
                                    date('m'),
                                    $totalFlowSum
                                ); ?>
                                <td class="total-block tooltip2-hover pl-2 pr-2 pt-3 pb-3 nowrap item-total-flow-sum-<?= $expenditureItemID; ?>"
                                    style="background-color: <?= $searchModel->getItemColor($tooltipData['deviation'], true); ?>;"
                                    data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                    data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                    data-diff-amount="<?= $tooltipData['diff']; ?>"
                                    data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>">
                                    <div class="pl-1 pr-1">
                                        <?= TextHelper::invoiceMoneyFormat($totalFlowSum, 2); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if ($class == 'expense'): ?>
                    <tr>
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= PlanFactSearch::$growingBalanceLabelByType[$typeID]; ?>
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                            $flowSum = isset($data['growingBalanceByBlock'][PlanFactSearch::$blockByType[$typeID]][$monthNumber]) ?
                                $data['growingBalanceByBlock'][PlanFactSearch::$blockByType[$typeID]][$monthNumber]['flowSum'] : 0;
                            if ($isFutureDate) {
                                $flowSum = 0;
                            } ?>

                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                                </div>
                            </td>
                            <?php $quarterSum += $flowSum; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?php echo TextHelper::invoiceMoneyFormat($flowSum, 2);
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td class="total-block pl-2 pr-2 pt-3 pb-3 nowrap weight-700 total-month-total-flow-sum">
                        </td>
                    </tr>
                <?php endif; ?>

            <?php endforeach; ?>
            <!-- TOTALS -->
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Результат по месяцу
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                    $totalFlowSum = isset($data['balance']['totalFlowSum']) ? $data['balance']['totalFlowSum'] : 0;
                    $flowSum = isset($data['balance'][$monthNumber]) ? $data['balance'][$monthNumber] : 0;
                    if ($isFutureDate) {
                        $totalFlowSum -= $flowSum;
                        $flowSum = 0;
                    }
                    ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                        </div>
                    </td>
                    <?php $quarterSum += $flowSum; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap weight-700 total-month-total-flow-sum">
                    <div class="pl-1 pr-1">
                        <?= TextHelper::invoiceMoneyFormat($totalFlowSum, 2); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Остаток на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                    $flowSum = isset($data['growingBalance'][$monthNumber]) ?
                        $data['growingBalance'][$monthNumber] : 0;
                    if ($isFutureDate) {
                        $flowSum = 0;
                    } ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                        </div>
                    </td>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td class="pl-2 pr-2 pt-3 pb-3 nowrap weight-700 reminder-month-empty">
                    <div class="pl-1 pr-1">
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>