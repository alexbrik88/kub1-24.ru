<?php

use common\models\cash\CashFlowsBase;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\modules\reports\models\PlanCashContractor;
use php_rutils\RUtils;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use frontend\modules\reports\models\PlanCashRule;
use frontend\widgets\TableViewWidget;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $floorMap array
 */

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_pc');

if ($searchByName = Yii::$app->request->get('byContractorName')) {
    $filterContractorIds = Contractor::find()->where(['company_id' => Yii::$app->user->identity->company->id])
        ->andWhere(['or', ['like', 'name', $searchByName], ['like', 'ITN', $searchByName]])
        ->select('id')
        ->column() ?: [-1];
} else {
    $filterContractorIds = [];
}

$models = PlanCashContractor::find()
    ->where(['company_id' => Yii::$app->user->identity->company->id])
    ->andWhere(['plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES])
    ->andFilterWhere(['contractor_id' => $filterContractorIds])
    ->asArray()
    ->all();

// Priorities
$rulesPriority = PlanCashRule::getArticles(Yii::$app->user->identity->company->id);
$prevPeriodsPriority = PlanCashContractor::getPrevPeriodsContractors(Yii::$app->user->identity->company->id);

$data = [
    1 => [
        'name' => 'Приход',
        'items' => [
            1 => [
                'name' => 'Банк',
                'items' => []
            ],
            2 => [
                'name' => 'Касса',
                'items' => []
            ],
            3 => [
                'name' => 'E-money',
                'items' => []
            ],
        ]
    ],
    0 => [
        'name' => 'Расход',
        'items' => [
            1 => [
                'name' => 'Банк',
                'items' => []
            ],
            2 => [
                'name' => 'Касса',
                'items' => []
            ],
            3 => [
                'name' => 'E-money',
                'items' => []
            ],
        ]
    ]
];
foreach ($models as $m) {

    $isIncome = ($m['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME);

    $flow = $m['flow_type'];
    $payment = $m['payment_type'];
    $item = $m['item_id'] ?: 'all';
    $contractor = $m['contractor_id'];

    $flowName = ($isIncome) ? 'Приход' : 'Расход';
    $paymentName = PlanCashContractor::$PAYMENT_TO_NAME[$payment] ?? '---';
    $itemName = '---';
    if ($isIncome) {
        if ($incItemModel = InvoiceIncomeItem::findOne($item))
            $itemName = $incItemModel->name;
    } else {
        if ($expItemModel = InvoiceExpenditureItem::findOne($item))
            $itemName = $expItemModel->name;
    }
    $contractorName = '---';
    if ($contractorModel = \common\models\Contractor::findOne([$contractor]))
        $contractorName = $contractorModel->getShortName();

    // FLOW
    if (!isset($data[$flow]))
        $data[$flow] = [
            'name' =>$flowName,
            'items' => []
    ];
    // PAYMENT TYPE
    if (!isset($data[$flow]['items'][$payment]))
        $data[$flow]['items'][$payment] = [
            'name' => $paymentName,
            'items' => []
        ];
    // EXP. ITEM
    if (!isset($data[$flow]['items'][$payment]['items'][$item]))
        $data[$flow]['items'][$payment]['items'][$item] = [
            'name' => $itemName,
            'items' => []
        ];
    // CONTRACTORS
    if (!isset($data[$flow]['items'][$payment]['items'][$item]['items'][$contractor]))
        $data[$flow]['items'][$payment]['items'][$item]['items'][$contractor] = [
            'name' => $contractorName,
            'payment_delay' => $m['payment_delay']
        ];

}

// sort
foreach ([CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME] as $flow) {
    foreach ($data[$flow]['items'] as &$items1) {
        foreach ($items1['items'] as &$items2) {
            uasort($items2['items'], function ($i1, $i2) {
                return $i1['name'] <=> $i2['name'];
            });
        }
        uasort($items1['items'], function ($i1, $i2) {
            return $i1['name'] <=> $i2['name'];
        });
    }
}

//var_dump($data);die;

?>

<?php
Pjax::begin([
    'id' => 'plan-by-invoices-pjax',
    'enablePushState' => false,
    'formSelector' => '#search_auto_plan_by_invoices',
    'timeout' => 10000,
    'scrollTo' => false,
]);
?>

<div class="wrap wrap_padding_none mb-2 pt-1">
    <div class="col-12 pb-2 mt-2" style="display: flex">
        <div class="col-9 pl-0 mt-1">
            <span class="label mb-2">
                <strong style="color:#001424">Кнопка «Добавить контрагента»</strong> - добавляйте вручную в список Покупателей и Поставщиков,
                по которым будут формироваться Плановые операции. По добавленным контрагентам, после выставления счета Покупателю
                и/или добавления счета от Поставщика, будет формироваться плановая операция на сумму счета и дату,
                равную дате счета плюс кол-во дней отсрочки платежа.
            </span>
            <br/>
            <span class="label">
                <strong style="color:#001424">Кнопка «Настроить правила»</strong> - задайте правила, выбрав статьи прихода и расхода. Плановые
                операции будут формироваться по всем контрагентам с выбранными статьями. <span style="color:red">ВАЖНО</span> плановые операции будут
                формироваться не только по текущим контрагентам, но и по всем новым! Новых Покупателей и Поставщиков
                не нужно будет добавлять вручную, они будут попадать автоматически в Платежный календарь.
            </span>
        </div>
        <div class="col-3 pr-0 pl-2 mb-1">
            <button class="button-regular button-hover-transparent ml-auto d-block w-100"
                    data-toggle="modal" data-target="#add-autoplan-contractor">
                <?= \frontend\components\Icon::get('add-icon', ['style' => 'margin-top:-2px; margin-right:-4px;']) ?>
                <span class="ml-2">Добавить контрагента</span>
            </button>
            <div class="mb-2 pb-1"></div>
            <button class="button-regular button-hover-transparent ml-auto d-block mt-1 w-100"
                    data-toggle="modal"
                    data-target="#update-autoplan-rules">
                <span>Настройка правил</span>
            </button>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s mt-1 pt-1">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableViewWidget::widget(['attribute' => 'table_view_finance_pc']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'id' => 'search_auto_plan_by_invoices',
            'enableClientValidation' => false,
            'method' => 'GET',
        ]); ?>
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::input('search', 'byContractorName', $searchByName, ['class' => 'form-control search-by-contractor', 'placeholder' => 'Поиск по контрагенту...']); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="wrap wrap_padding_none mb-2 pt-1">
<div class="custom-scroll-table custom-scroll-with-table-borders-collapsed">
    <div class="table-wrap">

        <table class="table table-style table-count-list mb-0 plan-contractor <?= $tabViewClass ?>">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th width="65%" class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top">
                    <div class="text-grey weight-700 ml-1 nowrap">Дней отсрочки</div>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2" colspan="2">
                    <div class="weight-700 ml-1 nowrap"></div>
                </th>
            </tr>
            </thead>

            <tbody>

            <!-- 1. FLOW_TYPE -->
            <?php foreach ($data as $flowType => $flowTypeBlock): ?>
                <tr class="main-checkbox-side-sub-item">
                    <td class="expenditure_type pl-2 pr-2 pt-3 pb-3">
                        <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                            <div class="text_size_14 weight-700 mr-2 nowrap"><?= $flowTypeBlock['name']; ?></div>
                        </div>
                    </td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                </tr>

                <!-- 2. PAYMENT_TYPE -->
                <?php foreach ($flowTypeBlock['items'] as $paymentType => $paymentTypeBlock): ?>
                    <?php $firstFloor = "first-floor-{$flowType}-{$paymentType}"; ?>
                    <?php $isFirstOpened = ArrayHelper::getValue($floorMap, $firstFloor, 1) ?>
                    <tr class="main-checkbox-side-sub-item">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <button class="table-collapse-btn button-clr ml-1 <?= $isFirstOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $firstFloor ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-dark weight-700 text_size_14 ml-1">
                                    <?= $paymentTypeBlock['name']; ?>
                                </span>
                            </button>
                            <?php if (!empty($paymentTypeBlock['items'])): ?>
                                <div class="badge-contractors-count">
                                    <?php $countContractors = 0;
                                    foreach ($paymentTypeBlock['items'] as $ptb) { $countContractors += count($ptb['items']); } ?>
                                    <?= $countContractors . ' ' .
                                        RUtils::numeral()->choosePlural($countContractors, !$flowType ?
                                            ['Поставщик', 'Поставщика', 'Поставщиков'] :
                                            ['Покупатель', 'Покупателя', 'Покупателей']) ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                        <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1 weight-700 text-dark-alternative"></div></td>
                    </tr>

                    <!-- 3. EXPENDITURE ITEM (e.g. "Айти") -->
                    <?php foreach ($paymentTypeBlock['items'] as $item => $itemBlock): ?>

                        <?php if ($IN_RULE = isset($rulesPriority[$flowType][$item])): // add third floor ?>

                            <?php $secondFloor = "second-floor-{$flowType}-{$paymentType}-{$item}"; ?>
                            <?php $isSecondOpened = ArrayHelper::getValue($floorMap, $secondFloor, 1) ?>
                            <tr class="expenditure-item-line <?= $paymentType; ?> <?= $isFirstOpened ? '' : 'd-none' ?>" data-id="<?= $firstFloor ?>">
                                <td class="expenditure_type pl-3 pr-3 pt-3 pb-3">
                                    <button class="table-collapse-btn button-clr ml-1 <?= $isSecondOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $secondFloor ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="text-grey text_size_14 mr-5">
                                        <?= $itemBlock['name']; ?>
                                    </span>
                                    </button>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                            </tr>                        
                        
                            <?php $thirdFloor = "third-floor-{$flowType}-{$paymentType}-{$item}"; ?>
                            <?php $isThirdOpened = ArrayHelper::getValue($floorMap, $thirdFloor, 1) ?>
                            <tr class="expenditure-item-line <?= $paymentType; ?> <?= $isSecondOpened ? '' : 'd-none' ?>" data-id="<?= $secondFloor ?>">
                                <td class="expenditure_type pl-6 pr-3 pt-3 pb-3">
                                    <button class="table-collapse-btn button-clr ml-1 <?= $isThirdOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $thirdFloor ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="text-grey text_size_14 mr-5">
                                        Все
                                    </span>
                                    </button>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                            </tr>
                        
                        <?php else: ?>

                            <?php $thirdFloor = "second-floor-{$flowType}-{$paymentType}-{$item}"; ?>
                            <?php $isThirdOpened = ArrayHelper::getValue($floorMap, $thirdFloor, 1) ?>
                            <tr class="expenditure-item-line <?= $paymentType; ?> <?= $isFirstOpened ? '' : 'd-none' ?>" data-id="<?= $firstFloor ?>">
                                <td class="expenditure_type pl-3 pr-3 pt-3 pb-3">
                                    <button class="table-collapse-btn button-clr ml-1 <?= $isThirdOpened ? 'active' : '' ?>" type="button" data-collapse-row-trigger data-target="<?= $thirdFloor ?>">
                                        <span class="table-collapse-icon">&nbsp;</span>
                                        <span class="text-grey text_size_14 mr-5">
                                        <?= $itemBlock['name']; ?>
                                    </span>
                                    </button>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap"><div class="pl-1 pr-1"></div></td>
                            </tr>

                        <?php endif; ?>

                        <!-- 4. CONTRACTOR (e.g "ООО "Фирма"") -->
                        <?php foreach ($itemBlock['items'] as $contractor => $contractorBlock): ?>
                            <?php $IN_PREV_PERIODS = isset($prevPeriodsPriority[$contractor]) ?>
                            <tr class="contractor-item-line <?= $isThirdOpened ? '' : 'd-none' ?>" data-id="<?= $thirdFloor ?>" data-plan-type="<?= PlanCashContractor::PLAN_BY_FUTURE_INVOICES ?>">
                                <td class="<?= $IN_RULE ? 'pl-6' : 'pl-3' ?> pr-3 pt-3 pb-3 contractor-cell-main">
                                    <div class="deleted-auto-plan-item hidden"></div>
                                    <div class="pl-4">
                                        <span class="text-grey text_size_14 mr-5">
                                            <?= $contractorBlock['name']; ?>
                                            <?php if ($IN_PREV_PERIODS): ?>
                                                <span style="float:right; color:red; font-weight: bold; cursor: default" title='Используется в планировании "По предыдущим платежам"'>Отключено</span>
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap">
                                    <div class="pl-1 pr-1">
                                        <span class="payment-delay-value"><?= $contractorBlock['payment_delay']; ?></span>
                                    </div>
                                </td>
                                <td class="pl-2 pr-2 pt-3 pb-3 nowrap td-action">
                                    <div class="d-flex flex-nowrap">
                                        <?php if (!$IN_PREV_PERIODS): ?>
                                            <div class="dropdown dropdown_tr-settings d-inline-block mr-2" data-highlight="tr">

                                                <button class="button-clr link" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <svg class="svg-icon">
                                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                                    </svg>
                                                </button>

                                                <div class="dropdown-menu p-0 keep-open" aria-labelledby="filter">
                                                    <div class="popup-dropdown-in p-2">
                                                        <form class="form-control-wrap p-1" action="<?= Url::to([
                                                                '/reports/auto-plan-ajax/update-contractor',
                                                                'planType' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                                                                'contractorId' => $contractor,
                                                                'flowType' => $flowType,
                                                                'paymentType' => $paymentType,
                                                                'itemId' => $item
                                                            ]) ?>">
                                                            <div class="row row_indents_sm flex-nowrap">
                                                                <div class="col-9 form-group mb-3">
                                                                    <input name="PlanCashContractor[name]" class="custom-payment-delay-value form-control" type="text" disabled value="<?= htmlspecialchars($contractorBlock['name']); ?>">
                                                                </div>
                                                                <div class="col-3 form-group mb-3">
                                                                    <input name="PlanCashContractor[payment_delay]" class="form-control js_input_to_money_format" type="text" value="<?= htmlspecialchars($contractorBlock['payment_delay']); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="row row_indents_sm pt-1">
                                                                <div class="column">
                                                                    <button id="update-plan-contractor" class="update-plan-contractor button-clr button-regular-no-compact button-regular_red button-width pr-4 pl-4 ladda-button "
                                                                            type="button"
                                                                            onclick="AUTOPLAN.updatePlanContractor(this)"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button>
                                                                </div>
                                                                <div class="column ml-auto">
                                                                    <button class="button-clr button-regular-no-compact button-hover-transparent width-120 pl-4 pr-4" data-dropdown-disable type="button">Отменить</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!$IN_RULE): ?>
                                            <button class="delete-contractor-btn button-clr link" type="button" data-pjax="0" data-url="<?= Url::to([
                                                '/reports/auto-plan-ajax/delete-contractor',
                                                'planType' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                                                'contractorId' => $contractor,
                                                'flowType' => $flowType,
                                                'paymentType' => $paymentType,
                                                'itemId' => $item
                                            ]) ?>">
                                                <svg class="svg-icon">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use>
                                                </svg>
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>

                        <?php endforeach; ?>

                    <?php endforeach; ?>

                <?php endforeach; ?>

            <?php endforeach; ?>

            </tbody>
        </table>

    </div>
</div>
</div>

<?php Pjax::end() ?>
