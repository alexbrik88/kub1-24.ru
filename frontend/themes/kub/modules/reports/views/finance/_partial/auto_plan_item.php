<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.02.2019
 * Time: 13:04
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\Banking;
use kartik\checkbox\CheckboxX;
use frontend\modules\reports\models\PaymentCalendarSearch;
use common\components\TextHelper;
use frontend\rbac\permissions\CashOrder;
use \common\components\floatLabelField\FloatLabelAsset;
use frontend\widgets\ConfirmModalWidget;
use common\components\date\DateHelper;
use Cake\Utility\Text;

/* @var $hasFlows bool
 * @var $searchModel PaymentCalendarSearch
 * @var $activeTab integer
 * @var $year integer
 */

FloatLabelAsset::register($this);
$bankUrl = null;
if ($searchModel->company->mainCheckingAccountant) {
    $bik = $searchModel->company->mainCheckingAccountant->bik;
    $bankingClass = Banking::classByBik($bik);
    if ($bankingClass && $bankingClass::$hasAutoload) {
        $bankUrl = Url::to([
            "/cash/banking/{$bankingClass::$alias}/default/index",
            'account_id' => $searchModel->company->mainCheckingAccountant->id,
            'p' => Banking::routeEncode(['/cash/bank/index', 'bik' => $bik]),
        ]);
    }
} else {
    $bik = 'all';
}
if ($bankUrl === null) {
    $bankUrl = Url::to([
        '/cash/banking/default/index',
        'p' => Banking::routeEncode(['/cash/bank/index', 'bik' => $bik]),
    ]);
}
$cashBoxList = Yii::$app->user->identity->getCashboxes()
    ->select(['name', 'id'])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])->indexBy('id')->column();
reset($cashBoxList);
$cashBox = key($cashBoxList);
$data = $searchModel->searchAutoPlanItems();
$years = $data['years'];
unset($data['years']);
$key = 0;
?>
<?php if ($hasFlows): ?>
    <table class="table table-striped table-bordered table-hover flow-of-funds auto-plan-block hidden">
        <thead class="not-fixed">
        <tr class="heading quarters-flow-of-funds" role="row">
            <th width="19%">
                <?= CheckboxX::widget([
                    'id' => 'main-checkbox-side-auto-plan',
                    'name' => 'main-checkbox-side',
                    'value' => false,
                    'options' => [
                        'class' => 'main-checkbox-side-auto_plan',
                        'data-subitems' => 'main-checkbox-side-sub-item',
                    ],
                    'pluginOptions' => [
                        'size' => 'xs',
                        'threeState' => false,
                        'inline' => false,
                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                    ],
                ]); ?>
                <label for="main-checkbox-side-auto-plan">
                    Статьи
                </label>
            </th>
            <th colspan="<?= $years ? count($years) : 1; ?>" class="text-left quarter-th" data-quarter="1"
                style="border-bottom: 1px solid #ddd;">
                Средняя сумма ежемесячных платежей
            </th>
            <th class="text-left" width="14%">
                Плановая сумма расхода в месяц
            </th>
            <th class="text-left" width="14%">
                Дата расхода
            </th>
            <th class="text-left" width="14%">
                <?= Html::tag('span', 'Дата окончания планирования', [
                    'id' => 'all_end-plan-date_toggle',
                    'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                    'onclick' => "$('#auto_plan_all-end-plan-date').toggleClass('hidden');",
                ]); ?>
                <div id="auto_plan_all-end-plan-date" class="popover top hidden">
                    <div class="arrow" style="left: 50%;"></div>
                    <div style="text-align: center;">
                        Поменять дату
                        окончания планирования
                        для всех на
                    </div>
                    <div class="input-icon" style="margin: 5px;">
                        <i class="fa fa-calendar" style="margin: 4px 2px 4px 10px;"></i>
                        <?= Html::textInput(null, '31.12.' . date('Y'), [
                            'class' => 'form-control text-center date-picker',
                            'id' => 'all_end-plan-date',
                            'style' => 'height: 25px;',
                        ]); ?>
                    </div>
                    <div style="">
                        <span id="all_end-plan-date_submit" class="btn btn-sm darkblue text-white" style="margin: 0;">
                            Применить
                        </span>
                    </div>
                </div>
            </th>
            <th class="text-center" width="4%">
            </th>
        </tr>
        <tr class="heading" role="row">
            <th width="20%"></th>
            <?php if ($years): ?>
                <?php foreach ($years as $year): ?>
                    <th class="text-left" width="10%">
                        за <?= $year; ?> г.
                    </th>
                <?php endforeach; ?>
            <?php else: ?>
                <th class="text-left" width="35%"></th>
            <?php endif; ?>
            <th class="text-left"></th>
            <th class="text-left"></th>
            <th class="text-left"></th>
            <th class="text-left"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $flowType => $flowTypeBlock): ?>
            <tr class="main-checkbox-side-sub-item">
                <td class="expenditure_type odd">
                    <label class="bold">
                        <?= $flowTypeBlock['name']; ?>
                    </label>
                </td>
                <?php foreach ($years as $year): ?>
                    <td class="odd bold">
                        <?= TextHelper::invoiceMoneyFormat(isset($flowTypeBlock['flowSum'][$year]) ?
                            ($flowTypeBlock['flowSum'][$year]) : 0, 2); ?>
                    </td>
                <?php endforeach; ?>
                <td class="odd bold">
                    <?= TextHelper::invoiceMoneyFormat($searchModel->calculatePlanMonthAmount($flowType), 2); ?>
                </td>
                <td class="odd bold"></td>
                <td class="odd bold"></td>
                <td></td>
                <?php unset($flowTypeBlock['name']); ?>
            </tr>
            <?php foreach ($flowTypeBlock as $paymentType => $paymentTypeBlock): ?>
                <?php if ($paymentType == 'flowSum') {
                    continue;
                }
                $cloneBlock = $paymentTypeBlock;
                unset($cloneBlock['name']);
                unset($cloneBlock['flowSum']);
                $items = array_keys($cloneBlock);
                $contractors = [];
                foreach ($paymentTypeBlock as $item => $itemBlock) {
                    if ($item == 'flowSum' || $item == 'name') {
                        continue;
                    }
                    $cloneBlock = $itemBlock;
                    unset($cloneBlock['name']);
                    unset($cloneBlock['flowSum']);
                    $contractors = array_merge($contractors, array_keys($cloneBlock));
                }
                $contractors = array_map("unserialize", array_unique(array_map("serialize", $contractors)));
                $amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $items, $contractors);
                if ($amount == 0) {
                    continue;
                }
                ?>
                <tr class="main-checkbox-side-sub-item">
                    <td class="expenditure_type odd">
                        <?= CheckboxX::widget([
                            'id' => 'auto_plan_payment_type-' . $paymentType,
                            'name' => 'auto_plan_payment_type',
                            'value' => false,
                            'options' => [
                                'class' => 'auto_plan_payment_type',
                                'data-subitems' => 'auto_plan_payment_type_sub-item-' . $paymentType,
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label class="bold" for="auto_plan_payment_type-<?= $paymentType; ?>">
                            <?= $paymentTypeBlock['name']; ?>
                        </label>
                    </td>
                    <?php foreach ($years as $year): ?>
                        <td class="odd bold">
                            <?= TextHelper::invoiceMoneyFormat(isset($paymentTypeBlock['flowSum'][$year]) ?
                                ($paymentTypeBlock['flowSum'][$year]) : 0, 2); ?>
                        </td>
                    <?php endforeach; ?>
                    <td class="odd bold">
                        <?php unset($paymentTypeBlock['name']); ?>
                        <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                    </td>
                    <td class="odd bold"></td>
                    <td class="odd bold"></td>
                    <td></td>
                </tr>
                <?php foreach ($paymentTypeBlock as $item => $itemBlock): ?>
                    <?php if ($item == 'flowSum') {
                        continue;
                    }
                    $cloneBlock = $itemBlock;
                    unset($cloneBlock['flowSum']);
                    $contractors = array_keys($cloneBlock);
                    $amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $item, $contractors);
                    if ($amount == 0) {
                        continue;
                    } ?>
                    <tr class="auto_plan_payment_type_sub-item-<?= $paymentType; ?> expenditure-item-line <?= $paymentType; ?>">
                        <td class="expenditure_type odd" style="padding-left: 20px;">
                            <?= CheckboxX::widget([
                                'id' => 'auto_plan_item-' . $paymentType . $item,
                                'name' => 'auto_plan_item',
                                'value' => false,
                                'options' => [
                                    'class' => 'auto_plan_item',
                                    'data-subitems' => 'auto_plan_item-sub-item_' . $paymentType . $item,
                                ],
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'threeState' => false,
                                    'inline' => false,
                                    'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                    'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                ],
                            ]); ?>
                            <label for="auto_plan_item-<?= $paymentType . $item; ?>">
                                <?= $itemBlock['name']; ?>
                            </label>
                        </td>
                        <?php foreach ($years as $year): ?>
                            <td>
                                <?= TextHelper::invoiceMoneyFormat(isset($itemBlock['flowSum'][$year]) ?
                                    ($itemBlock['flowSum'][$year]) : 0, 2); ?>
                            </td>
                        <?php endforeach; ?>
                        <td>
                            <?php unset($itemBlock['name']); ?>
                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php foreach ($itemBlock as $contractor => $contractorBlock): ?>
                        <?php if ($contractor == 'flowSum') {
                            continue;
                        }
                        $cashFlowsAutoPlanItem = $searchModel->findCashFlowAutoPlan(
                            $flowType,
                            $searchModel->getPaymentTypeByTableName($paymentType),
                            $item,
                            $contractor
                        );
                        $amount = $searchModel->calculatePlanMonthAmount($flowType, $paymentType, $item, $contractor);
                        if ($amount == 0) {
                            continue;
                        }
                        $planMonthDate = $searchModel->calculatePlanMonthDate($flowType, $paymentType, $item, $contractor);
                        $endPlanDate = '31.12.' . date('Y'); ?>
                        <tr class="auto_plan_item-sub-item_<?= $paymentType . $item; ?> contractor-item-line"
                            id="<?= "item-{$flowType}-{$paymentType}-{$item}-{$contractor}" ?>">
                            <td style="padding-left: 40px;">
                                <div class="deleted-auto-plan-item hidden"></div>
                                <?= Text::truncate($contractorBlock['name'], 35, [
                                    'ellipsis' => '...',
                                    'exact' => true,
                                ]); ?>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][flow_type]", $flowType, [
                                    'class' => 'required-field',
                                ]); ?>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][payment_type]", $searchModel->getPaymentTypeByTableName($paymentType), [
                                    'class' => 'required-field',
                                ]); ?>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][item]", $item, [
                                    'class' => 'required-field',
                                ]); ?>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][contractor_id]", $contractor, [
                                    'class' => 'required-field',
                                ]); ?>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][is_deleted]", false, [
                                    'class' => 'required-field is_deleted-field',
                                ]); ?>
                            </td>
                            <?php foreach ($years as $year): ?>
                                <td>
                                    <?= TextHelper::invoiceMoneyFormat(isset($contractorBlock['flowSum'][$year]) ?
                                        ($contractorBlock['flowSum'][$year]) : 0, 2); ?>
                                </td>
                            <?php endforeach; ?>
                            <td>
                                <span class="amount-text">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                </span>
                                <div class="form-group form-md-line-input form-md-floating-label amount-input hidden"
                                     style="margin-bottom: 0;padding-top: 0;">
                                    <?= Html::textInput("CashFlowsAutoPlan[{$key}][amount]", TextHelper::invoiceMoneyFormat($amount, 2, '.', ''), [
                                        'id' => "plan_amount-{$flowType}-{$paymentType}-{$item}-{$contractor}",
                                        'class' => 'form-control input-sm js_input_to_money_format',
                                    ]); ?>
                                    <?= Html::label('', "plan_amount-{$flowType}-{$paymentType}-{$item}-{$contractor}"); ?>
                                </div>
                                <i class="fa fa-pencil update-amount"
                                   aria-hidden="true"
                                   style="color: #5b9bd1;cursor: pointer;"></i>
                            </td>
                            <td>
                                <span class="date-text">
                                    <?= $planMonthDate; ?>
                                </span>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][plan_date]", $planMonthDate); ?>
                                <i class="fa fa-pencil update-date"
                                   id="<?= "{$flowType}-{$paymentType}-{$item}-{$contractor}" ?>"
                                   aria-hidden="true"
                                   style="color: #5b9bd1;cursor: pointer;"></i>
                            </td>
                            <td>
                                <span class="date-text">
                                    <?= $endPlanDate; ?>
                                </span>
                                <?= Html::hiddenInput("CashFlowsAutoPlan[{$key}][end_plan_date]", $endPlanDate, [
                                    'class' => 'end-plan-date',
                                ]); ?>
                                <i class="fa fa-pencil update-date end_plan_date"
                                   id="<?= "{$flowType}-{$paymentType}-{$item}-{$contractor}" ?>"
                                   aria-hidden="true"
                                   style="color: #5b9bd1;cursor: pointer;"></i>
                            </td>
                            <td>
                                <?= ConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<i class="fa fa-trash" aria-hidden="true" style="color: #5b9bd1;"></i>',
                                        'tag' => 'a',
                                        'class' => 'delete-auto-plan-item',
                                    ],
                                    'options' => [
                                        'class' => 'delete-auto-plan-item-modal',
                                        'id' => "delete-auto-plan-item-{$flowType}-{$paymentType}-{$item}-{$contractor}",
                                        'data-id' => "{$flowType}-{$paymentType}-{$item}-{$contractor}",
                                    ],
                                    'confirmUrl' => "javascript:;",
                                    'message' => 'Вы уверены, что хотите удалить плановый расход?',
                                ]); ?>
                                <?= Html::a('<i class="glyphicon glyphicon-share-alt"></i>', 'javascript:;', [
                                    'class' => 'undo-delete-auto-plan-item hidden',
                                    'title' => 'Отменить удаление',
                                ]); ?>
                            </td>
                            <?php unset($contractorBlock['name']); ?>
                        </tr>
                        <?php $key++; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div id="save-auto-plan" class="confirm-modal fade modal" role="dialog" tabindex="-1">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            Сохранить внесенные изменения?
                            <br>
                            При нажатии кнопки "Сохранить" создадутся
                            <br>
                            плановые операции согласно настройкам.
                            <br>
                            При нажатии на кнопку "Отменить" все изменения отменятся.
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>',
                                'javascript:;', [
                                    'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button update-auto-plan',
                                    'data' => [
                                        'style' => 'expand-right',
                                        'method' => 'post',
                                        'action' => Url::to(['update-auto-plan', 'activeTab' => $activeTab, 'year' => $year]),
                                    ],
                                    'style' => 'width: 135px;',
                                ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <?= Html::a('Отменить', Url::to(['payment-calendar', 'activeTab' => $activeTab, ['PaymentCalendarSearch' => ['year' => $year]]]), [
                                'class' => 'btn darkblue',
                                'style' => 'width: 115px;',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="auto-plan-block hidden">
        <span class="text-bold">АвтоПланирование ежемесячных плановых операций, на основе предыдущих периодов.</span>
        <div>
            КУБ на основе ваших операций по банку и кассе <br>
            в предыдущих месяцах, может спрогнозировать ваши плановые операции. <br>
            Это поможет вам планировать ваши расходы. <br>
            Зная расходы на месяц, вы сможете планировать ваши продажи и знать вашу точку безубыточности.
        </div>
        <div style="margin-top: 15px;">
            Для этого:<br>
            <?= Html::a('Загрузите выписку из банка', $bankUrl); ?><br>
            <?= Html::a('Загрузите операции по кассе из Excel', Url::to(['/cash/order', 'modal' => true])); ?><br>
            <?php if (YII_ENV_DEV && Yii::$app->user->can(CashOrder::CREATE)): ?>
                <?= Html::a('Загрузите операции по кассе из вашего ОФД', Url::to([
                    '/cash/ofd/default/index',
                    'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode(['/cash/order/index', 'cashbox_id' => $cashBox]),
                    'cashbox_id' => $cashBox,
                ])); ?>
            <?php endif; ?>
        </div>
        <?= Html::button('ОК', [
            'class' => 'btn darkblue text-white pull-left submit-auto_plan',
            'style' => 'width: 10%!important;margin-top: 15px;',
        ]); ?>
    </div>
<?php endif; ?>
<?php $this->registerJs('
    $(".auto_plan_item").change(function (e) {
        var $subItems = $("." + $(this).data("subitems"));
        if ($(this).val() == 1) {
            $subItems.hide();
        } else {
            $subItems.show();
        }
    });
    
    $(".auto_plan_payment_type, .auto_plan_flow_type, .main-checkbox-side-auto_plan").change(function (e) {
        var $this = $(this);
        var $subItems = $("." + $(this).data("subitems"));
        var $val = $this.val();
        
        $subItems.find("input").each(function (e) {
            if (($val == 1 && $(this).val() == 0) || ($val == 0 && $(this).val() == 1 && $this.hasClass("main-checkbox-side-auto_plan"))) {
                $(this).click();
            }
        });
        if (!$this.hasClass("main-checkbox-side-auto_plan")) {
            if ($val == 1) {
                $subItems.hide();
            } else {
                $subItems.show();
            }
        }
    });
    
    $("i.update-date").datepicker({
        format: "dd.mm.yyyy",
        language:"ru",
        autoclose: true,
    })
    .each(function (e) {
        var $defaultVal = $(this).siblings("span.date-text").text().trim();
        
        if ($defaultVal !== "") {
            $(this).datepicker("setDate", $defaultVal);
        }
    })
    .on("changeDate", function(e) {
        var $formattedDate = "";
        if (e.date !== undefined) {
            var $date = new Date(e.date);
            
            $formattedDate = [pad($date.getDate()), pad($date.getMonth()+1), $date.getFullYear()].join(".");
            function pad(s) { 
                return (s < 10) ? "0" + s : s; 
            }
        }
        $(this).siblings("span.date-text").text($formattedDate);
        $(this).siblings("input").val($formattedDate);
        var $dates = [];
        $(".end-plan-date").each(function (e) {
            if ($(this).val()) {
                var $day = parseInt($(this).val().substring(0,2));
                var $month = parseInt($(this).val().substring(3,5));
                var $year = parseInt($(this).val().substring(6,10));
                $dates.push(new Date($year, $month-1, $day));
            }
        });
        if ($dates.length > 0) {
            var $maxDate = new Date(Math.max.apply(null, $dates));  
            var $day = $maxDate.getDate() < 10 ? ("0" + $maxDate.getDate()) : $maxDate.getDate();
            var $month = ($maxDate.getMonth() + 1) < 10 ? ("0" + ($maxDate.getMonth() + 1)) : ($maxDate.getMonth() + 1);
            var $newDate = $day + "." + $month + "." + $maxDate.getFullYear();
            
            $("#all_end-plan-date").datepicker("setDate", $newDate);
        }
    });
    
    $(".update-amount").click(function (e) {
        $(this).addClass("hidden");
        $(this).siblings(".amount-text").addClass("hidden");
        $(this).siblings(".amount-input").removeClass("hidden");
    });
    
    $("#all_end-plan-date_submit").click(function (e) {
        var $date = $("#all_end-plan-date").val();
        
        $("#auto_plan_all-end-plan-date").addClass("hidden");
        if ($date !== "") {
            $(".update-date.end_plan_date").datepicker("setDate", $date);
        }
    });
    
    $(document).mouseup(function (e) {
        if ($("#all_end-plan-date_toggle").is(e.target) || $(e.target).closest(".datepicker").length > 0) return;
        var container = $("#auto_plan_all-end-plan-date:not(hidden)");
        if (container && !container.is(e.target) && container.has(e.target).length === 0) {
            container.addClass("hidden");
        }
    });
    
    $(".update-auto-plan").click(function (e) {
        $params = $("table.flow-of-funds.auto-plan-block tr.contractor-item-line td input").serialize();
        $.post($(this).data("action"), $params, function (data) {
        });
    }); 
    
    $(".delete-auto-plan-item-modal a").click(function (e) {
        var $modal = $(this).closest(".modal");
        var $contractorItem = $("#item-" + $modal.data("id"));

        $contractorItem.find("input.is_deleted-field").val(1);
        $contractorItem.find(".deleted-auto-plan-item, .undo-delete-auto-plan-item").removeClass("hidden");
        $contractorItem.find(".update-amount, .update-date, .delete-auto-plan-item").hide();
        $modal.modal("hide");
    });
    
    $(".undo-delete-auto-plan-item").click(function (e) {
        var $contractorItem = $(this).closest("tr");
        
        Ladda.stopAll();
        $contractorItem.find("input.is_deleted-field").val(0);
        $contractorItem.find(".deleted-auto-plan-item, .undo-delete-auto-plan-item").addClass("hidden");
        $contractorItem.find(".update-amount, .update-date, .delete-auto-plan-item").show();
    });
'); ?>
