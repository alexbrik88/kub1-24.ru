<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:46
 */

use common\components\helpers\Html;
?>
<div class="expenses-panel panel-block">
    <div class="main-block">
        <p>
            <?= 'Отчет по ' . ($isDebtor ? 'клиентам' : 'поставщикам') ?>
        </p>
        <p class="bold" style="margin-bottom: 0;">
            Зачем нужен этот отчет?
        </p>
    </div>
    <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
</div>
