<?php

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use frontend\modules\reports\models\ContractorSimpleSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashFlows;
use \frontend\modules\reports\models\PlanCashContractor;
use yii\bootstrap4\ActiveForm;
use \yii\bootstrap4\Html;
use yii\web\View;
use frontend\modules\reports\models\PlanCashRule;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */

$section = Yii::$app->request->get('section');

?>
<div class="nav-tabs-row" id="nav-tabs-payment-calendar" style="margin-top: -10px">
   <?= \yii\bootstrap4\Tabs::widget([
       'options' => [
           'class' => 'nav nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 pr-4',
       ],
       'linkOptions' => [
           'class' => 'nav-link',
       ],
       'tabContentOptions' => [
           'class' => 'tab-pane mt-1',
           'style' => 'width:100%'
       ],
       'headerOptions' => [
           'class' => 'nav-item',
       ],
       'items' => [
           [
               'label' => 'Описание',
               'content' => $this->render('_payment_calendar_auto/tab_info', []),
               'linkOptions' => [
                   'class' => 'nav-link' . (!in_array($section, [2,3]) ? ' active' : ''),
               ],
               'active' => (!in_array($section, [2,3]))
           ],
           [
               'label' => 'По счетам',
               'content' => $this->render('_payment_calendar_auto/tab_by_invoices', [
                   'searchModel' => $searchModel,
                   'activeTab' => $activeTab,
                   'year' => $searchModel->year,
                   'floorMap' => Yii::$app->request->post('floorMap', [])
               ]),
               'linkOptions' => [
                   'class' => 'nav-link' . ($section == '2' ? ' active' : ''),
               ],
               'active' => ($section == '2')
           ],
           [
               'label' => 'По предыдущим платежам',
               'content' => $this->render('_payment_calendar_auto/tab_by_previous_periods', [
                   'searchModel' => $searchModel,
                   'activeTab' => $activeTab,
                   'year' => $searchModel->year,
                   'floorMap' => Yii::$app->request->post('floorMap', [])
               ]),
               'linkOptions' => [
                   'class' => 'nav-link' . ($section == '3' ? ' active' : ''),
               ],
               'active' => ($section == '3')
           ],
       ],
   ]); ?>
</div>

<?= $this->render('_payment_calendar_auto/_modal_rules') ?>
<?= $this->render('_payment_calendar_auto/_modal_contractors', ['year' => $searchModel->year, 'calendarSearchModel' => $searchModel]) ?>

<script>
    window.AUTOPLAN = {
        selectedContractors: null,
        basePjaxUrl: '<?= \yii\helpers\Url::to(['/reports/finance/payment-calendar', 'activeTab' => $activeTab, 'subTab' => $subTab, 'PaymentCalendarSearch[year]' => $year]) ?>',
        init: function()
        {
            this.bindTableEvents();
            this.bindContractorModalEvents();
            this.bindRuleModalEvents();
            this.bindPjaxEvents();
            this.bindClippedDropdownEvents();
        },
        showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
        pjaxReloadTable: function(pjaxContainer) {
            var floorMap = {};
            $(pjaxContainer).find('[data-collapse-row-trigger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            var $searchField = $(pjaxContainer).find('.search-by-contractor');
            var searchParam = $searchField.attr('name') + '=' + $searchField.val();

            return $.pjax({
                type: 'POST',
                push: false,
                scrollTo: false,
                timeout: 5000,
                url: AUTOPLAN.basePjaxUrl + '&' + searchParam,
                container: pjaxContainer,
                data: { floorMap: floorMap }
            });
        },
        refreshAddModal: function(pjaxContainer) {
            $.pjax.reload({"container": "#autoplan_contractors_pjax" + (pjaxContainer == '#plan-by-prev-periods-pjax' ? '_prev' : '') , "timeout": 5000})
        },
        updatePlanContractorByKey: function(el) {
            var tr = $(".dropdown_autoplan_clipped").filter('[data-id="' + $(el).data('dropdown-id') + '"]').closest('tr');
            return AUTOPLAN.updatePlanContractor(el, tr);
        },
        updatePlanContractor: function(el, closestTR) {
            var $form = $(el).closest('form');
            var $tr = closestTR || $(el).closest('tr');
            var pjaxContainer = "#" + $tr.closest('[data-pjax-container]').attr('id');

            var l = Ladda.create(el);
            l.start();

            $.post($form.attr('action'), $form.serialize(), function(data) {

                if (data.success) {

                    AUTOPLAN.pjaxReloadTable(pjaxContainer).done(function() {

                        Ladda.stopAll();
                        l.remove();
                        $('body > .dropdown_menu_autoplan_clipped').remove();

                        AUTOPLAN.showFlash("Контрагент обновлен");
                    });

                } else {

                    AUTOPLAN.showFlash((data.message) ? data.message : "Ошибка");
                    Ladda.stopAll();
                    l.remove();
                }
            });
        },
        deletePlanContractor: function(el) {
            var $tr = $(el).closest('tr');
            var pjaxContainer = "#" + $tr.closest('[data-pjax-container]').attr('id');

            var l = Ladda.create(el);
            l.start();

            $.post($(el).attr('data-url'), {}, function(data) {

                if (data.success) {

                    AUTOPLAN.pjaxReloadTable(pjaxContainer).done(function() {

                        AUTOPLAN.showFlash("Контрагент удален");
                        AUTOPLAN.refreshAddModal(pjaxContainer);
                    });

                } else {

                    AUTOPLAN.showFlash((data.message) ? data.message : "Ошибка");
                    Ladda.stopAll();
                    l.remove();
                }
            });
        },
        bindPjaxEvents: function() {

            function mainJSPart(container) {
                // CUSTOM SCROLL TABLE
                $('.custom-scroll-table', container).mCustomScrollbar({
                    horizontalScroll: true,
                    axis:"x",
                    scrollInertia: 300,
                    advanced:{
                        autoExpandHorizontalScroll: true,
                        updateOnContentResize: true,
                        updateOnImageLoad: false
                    },
                    mouseWheel:{ enable: false },
                });

                // PLUS/MINUS BUTTONS
                $('[data-collapse-row-trigger]', container).click(function() {
                    var target = $(this).data('target');
                    $(this).toggleClass('active');
                    if ( $(this).hasClass('active') ) {
                        $('[data-id="'+target+'"]').removeClass('d-none');
                    } else {
                        // level 1
                        $('[data-id="'+target+'"]').addClass('d-none');
                        $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+target+'"]').each(function(i, row) {

                            // level 2
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {

                                    $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                            });
                        });
                    }
                    if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                        $('[data-collapse-all-trigger]').removeClass('active')
                    } else {
                        $('[data-collapse-all-trigger]').addClass('active')
                    }
                });
                // PLUS/MINUS BUTTONS
                $('[data-collapse-all-trigger]', container).click(function() {
                    var _this = $(this);
                    var table = $(this).closest('.table');
                    var row = table.find('tr[data-id]');
                    _this.toggleClass('active');
                    if ( _this.hasClass('active') ) {
                        row.removeClass('d-none');
                        $(table).find('tbody .table-collapse-btn').addClass('active');
                    } else {
                        row.addClass('d-none')
                        $(table).find('tbody .table-collapse-btn').removeClass('active');
                    }
                });
                // INIT DROPDOWNS
                $('[data-toggle="dropdown"]', container).dropdown({
                    flip: false,
                });
                $('[data-toggle="dropdown-placement"]', container).dropdown();
                $('[data-dropdown-disable]', container).click(function() {
                    $(this).closest('.dropdown-menu').removeClass('open show')
                    if ($(this).closest('.dropdown-menu').parent().data('highlight') == 'tr') {
                        $(this).closest('tr').removeClass('background-grey');
                    }
                })
                $('.dropdown, .dropup', container).on('show.bs.dropdown', function(e) {
                    if ( $(this).closest('.invoice-wrap').length ) {
                        $('.invoice-wrap').addClass('stub');
                    }
                    if ( $(e.target).data('highlight') == 'tr' ) {
                        console.log('lalala')
                        $(e.target).closest('tr').addClass('background-grey')
                    }
                });
                $('.dropdown, .dropup', container).on('hide.bs.dropdown', function(e) {
                    if ( $(this).closest('.invoice-wrap').length ) {
                        $('.invoice-wrap').removeClass('stub');
                    }
                    if ( $(e.target).data('highlight') == 'tr' ) {
                        $(e.target).closest('tr').removeClass('background-grey')
                    }
                });
                // CLOSE DROPDOWN
                $('[data-dropdown-disable]', container).click(function() {
                    $(this).closest('.dropdown-menu').removeClass('open show')
                    if ($(this).closest('.dropdown-menu').parent().data('highlight') == 'tr') {
                        $(this).closest('tr').removeClass('background-grey');
                    }
                });

                // KEEP-OPEN DROPDOWN
                $('.dropdown-menu[aria-labelledby]', container).click(function(e) {
                    if ( $(this).hasClass('keep-open') ) {
                        console.log('lalalal')
                        e.stopPropagation();
                    }
                });

                // REINIT DATEPICKERS
                $('.date-picker', container).each(function(i, dp) {

                    if ($(dp).hasClass('hasDatepicker'))
                        return;

                    var v = $(dp).val().split('.');

                    $(dp).datepicker(kubDatepickerConfig);

                    $(dp).addClass('hasDatepicker').inputmask({"mask": "9{2}.9{2}.9{4}"});

                    if (v.length == 3) {
                        var date = new Date(v[2], v[1] - 1, v[0]);
                        if (date.getTime() === date.getTime()) {
                            $(dp).data('datepicker').selectDate(date);
                        }
                    }

                });

                // DATEPICKER ICO
                $('.input-toggle', container).click(function(evt) {
                    evt.stopPropagation();
                    if ( $(this).siblings('input').hasClass('date-picker') ) {
                        $(this).siblings('input').datepicker().data('datepicker').show();
                    } else {
                        $(this).siblings('input').trigger('click');
                    }
                });

            }

            $(document).on('pjax:success', '#plan-by-invoices-pjax', function() {
                mainJSPart('#plan-by-invoices-pjax');
                // redraw chart
                if (window.ChartPlanFactDays) window.ChartPlanFactDays.redrawByClick();
            });
            $(document).on('pjax:success', '#plan-by-prev-periods-pjax', function() {
                mainJSPart('#plan-by-prev-periods-pjax');
                // redraw chart
                if (window.ChartPlanFactDays) window.ChartPlanFactDays.redrawByClick();
            });
        },
        bindTableEvents: function() {
            var $delBtn;
            $(document).on('click', '.delete-contractor-btn', function(e) {
                e.preventDefault();
                $delBtn = this;
                $('#delete-autoplan-contractor').modal('show');
            });
            $(document).on('click', '.delete-contractor-modal-btn', function() {
                AUTOPLAN.deletePlanContractor($delBtn);
                $('#delete-autoplan-contractor').modal('hide');
            });
        },
        bindContractorModalEvents: function()
        {
            // change contractor type
            $(document).on('change', '.js_autoplan_contractor_flow_type_input', function() {
                $(this).closest('form').submit();
            });

            // add contractors
            $(document).on('click', '#add-contractors-to-autoplan', function() {
                var form = $('#form_add_auto_plan_contractor');

                // validate form
                if ($(form).find('[type="checkbox"]:checked').length === 0) {
                    setTimeout("Ladda.stopAll()", 100);
                    $(form).find('.empty-choose-error').show();
                    return false;
                } else {
                    $(form).find('.empty-choose-error').hide();
                }

                $.post($(form).data('save-url'), $(form).serialize(), function(data) {

                    Ladda.stopAll();

                    $('#add-autoplan-contractor').modal('hide');

                    $.pjax.reload({"container":"#plan-by-invoices-pjax", "timeout": 5000}).done(function() {
                        AUTOPLAN.showFlash((data.success) ? "Контрагенты добавлены" : "Ошибка сохранения");
                        $.pjax.reload({"container":"#autoplan_contractors_pjax", "timeout": 5000});
                    });
                })
            });

            // add contractors prev-periods tab
            $(document).on('click', '#add-contractors-to-autoplan-prev', function() {
                var form = $('#form_add_auto_plan_contractor_prev');

                // validate form
                if ($(form).find('[type="checkbox"]:checked').length === 0) {
                    setTimeout("Ladda.stopAll()", 100);
                    $(form).find('.empty-choose-error').show();
                    return false;
                } else {
                    $(form).find('.empty-choose-error').hide();
                }

                $.post($(form).data('save-url'), $(form).serialize(), function(data) {

                    Ladda.stopAll();

                    $('#add-autoplan-contractor-prev').modal('hide');

                    $.pjax.reload({"container":"#plan-by-prev-periods-pjax", "timeout": 5000}).done(function() {
                        AUTOPLAN.showFlash((data.success) ? "Контрагенты добавлены" : "Ошибка сохранения");
                        $.pjax.reload({"container": "#autoplan_contractors_pjax_prev", "timeout": 5000})
                    });
                })
            });

            // checkboxes
            $(document).on('click', '.joint-autoplan-main-checkbox', function() {
                $(this).closest('.modal.show').find('.joint-autoplan-checkbox').prop('checked', $(this).prop('checked')).uniform('refresh');
            });
            $(document).on('change', '.joint-autoplan-checkbox', function() {
                var $block = $(this).closest('.modal.show');
                var $checkboxes = $block.find('.joint-autoplan-checkbox');
                if ($checkboxes.length === $checkboxes.filter(':checked').length) {
                    $block.find('.joint-autoplan-main-checkbox').prop('checked', true).uniform('refresh');
                } else {
                    $block.find('.joint-autoplan-main-checkbox').prop('checked', false).uniform('refresh');
                }
            });

            // per-page, click to arrow
            $(document).on('click', '#add-autoplan-contractor .input-toggle, #add-autoplan-contractor-prev .input-toggle', function(evt) {
                evt.stopPropagation();
                if ( $(this).siblings('input').hasClass('date-picker') ) {
                    $(this).siblings('input').datepicker().data('datepicker').show();
                } else {
                    $(this).siblings('input').trigger('click');
                }
            });
        },
        bindRuleModalEvents: function()
        {
            // change flow type
            $('.js_autoplan_rules_flow_type_input').on('change', function() {
                var $tabs = $(this).parents('form').find('.js_autoplan_rules_flow_type');
                $tabs.addClass('hidden');
                if ($(this).val() == "1")
                    $tabs.filter('.income').removeClass('hidden');
                else
                    $tabs.filter('.expense').removeClass('hidden');
            });

            // update rules
            $(document).on("submit", "#form_auto_plan_rules", function(e) {
                e.preventDefault();

                $('#update-autoplan-rules').find('.msg-saved-rules').removeClass('hidden');

                $.ajax({
                    "type": "post",
                    "url": $(this).attr("action"),
                    "data": $(this).serialize(),
                    "success": function(data) {

                        Ladda.stopAll();

                        $('#update-autoplan-rules').find('.msg-saved-rules').addClass('hidden');
                        $('#update-autoplan-rules').modal('hide');

                        AUTOPLAN.showFlash((data.success) ? "Правила сохранены" : "Ошибка сохранения");

                        $.pjax.reload({"container":"#plan-by-invoices-pjax", "timeout": 5000});
                    }
                });
            });

            // main checkbox toggle
            $('.js_autoplan_rules_flow_type').find('.by-flow-type-main-checkbox').on('change', function() {
                var $block = $(this).closest('.js_autoplan_rules_flow_type');
                $block.find('.by-flow-type-checkbox').prop('checked', $(this).prop('checked')).uniform('refresh');
            });
            $('.js_autoplan_rules_flow_type').find('.by-flow-type-checkbox').on('change', function() {
                var $block = $(this).closest('.js_autoplan_rules_flow_type');
                if ($block.find('.by-flow-type-checkbox:checked').length) {
                    $block.find('.by-flow-type-main-checkbox').prop('checked', true).uniform('refresh');
                } else {
                    $block.find('.by-flow-type-main-checkbox').prop('checked', false).uniform('refresh');
                }
            });
        },
        bindClippedDropdownEvents: function()
        {
            // replace opened dropdown-menu to body
            $(document).on('show.bs.dropdown', '.dropdown_autoplan_clipped', function () {
                var dropdown = $(this);
                var dropdown_menu = $(this).find('.dropdown_menu_autoplan_clipped');
                $('body').append(dropdown_menu.css({
                    'position':'absolute',
                    'right':dropdown.offset().right,
                    'top':dropdown.offset().top
                }).detach());
            });
            $(document).on('hidden.bs.dropdown', '.dropdown_autoplan_clipped', function () {
                var dropdown = $(this);
                var dropdown_menu = $('body > .dropdown_menu_autoplan_clipped');
                $(dropdown).append(dropdown_menu.css({
                    'position':'static',
                    'left':'auto',
                    'top':'auto'
                }).detach());

                $(dropdown).closest('tr').removeClass('background-grey');
            });
        }
    };

    ///////////////////////
    window.AUTOPLAN.init();
    ///////////////////////

</script>