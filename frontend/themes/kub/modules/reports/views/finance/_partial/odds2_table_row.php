<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\themes\kub\helpers\Icon;

/** @var $cellIds array */
/** @var $dMonthes array */
/** @var $data array */
/** @var $title string */
/** @var $isSumQuarters */
/** @var $trClass string */
/** @var $level int */
/** @var $currentMonthNumber int */
/** @var $colspan1 int */
/** @var $colspan2 int */

$trData  = '';
$trData .= (isset($dataPaymentType)) ? ('data-payment_type="'.$dataPaymentType.'"') : '';
$trData .= (isset($dataFlowType)) ? ('data-flow_type="'.$dataFlowType.'"') : '';
$trData .= (isset($dataItemId)) ? ('data-item_id="'.$dataItemId.'"') : '';
$trData .= (isset($dataId)) ? ('data-id="'.$dataId.'"') : '';
$trData .= (isset($dataType)) ? ('data-type_id="'.$dataType.'"') : '';

$trId = (isset($trId)) ? ('id="'.$trId.'"') : '';

$canHover = $canHover ?? true;
$isSumQuarters = $isSumQuarters ?? true;
$isSortable = $isSortable ?? false;
$isBold = $level == 1;
$isActiveRow = $level == 2;
$isFirstRow = $isFirstRow ?? false;
$noResultContent = $this->render('_to_analytics');
?>
<tr class="rowspan_row <?=($trClass)?> <?=($level == 3 && !$isOpenedFloor ? 'd-none' : '')?>" <?=($trData)?> <?=($trId)?>>
    <td class="pl-2 pr-2 pt-3 pb-3 <?=($isActiveRow || $isSortable ? 'checkbox-td':'')?> <?=($isBold ? 'tooltip-td':'')?>">
        <?php if ($level == 3 && $isSortable): ?>
            <svg class="sortable-row-icon svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
            </svg>
            <span class="text-grey text_size_14"><?= $title; ?></span>
        <?php elseif ($level == 3): ?>
            <span class="text-grey text_size_14 mr-1 m-l-purse">
                <?= $title ?>
            </span>
        <?php elseif ($level == 2): ?>
            <button class="table-collapse-btn button-clr ml-1 <?= $isOpenedFloor ? 'active':'' ?>" type="button" data-collapse-row-trigger data-target="<?= $floorKey ?>">
                <span class="table-collapse-icon">&nbsp;</span>
                <span class="<?=($isBold ? 'weight-700':'text-grey')?> text_size_14 ml-1"><?= $title; ?></span>
            </button>
        <?php else: ?>
            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                <div class="text_size_14 mr-2 nowrap <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= $title ?>
                    <?php if (isset($question)): ?>
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $question,
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </td>
    <?php foreach (AbstractFinance::$month as $month => $monthText): ?>
        <?php if ($currentMonthNumber == $month) : ?>
            <?php
            $quarter = (int) ceil(intval($month) / 3);
            $amount = isset($data[$month]) ? $data[$month] : 0;
            ?>
            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $canHover ? 'can-hover':'' ?>">
                <div class="pl-1 pr-1 text-dark-alternative <?=($isBold ? 'weight-700':'text-grey')?>">
                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </div>
            </td>
        <?php elseif ($isFirstRow) : ?>
            <?php if (intval($month) == 1) : ?>
                <?php
                $colspan = floor(($currentMonthNumber-1)/3) + ($currentMonthNumber-1)%3;
                ?>
                <td class="px-2 py-3 rowspan_cell colspan_l_cell" rowspan="<?= $rowspan ?>" colspan="<?= $colspan ?>" style="vertical-align: middle; text-align: center !important;">
                    <?= $noResultContent ?>
                </td>
            <?php elseif (intval($month) == ($currentMonthNumber + 1)) : ?>
                <?php
                $colspan = floor((12-$currentMonthNumber)/3) + (12-$currentMonthNumber)%3 + 1;
                ?>
                <td class="px-2 py-3 rowspan_cell colspan_r_cell" rowspan="<?= $rowspan ?>" colspan="<?= $colspan ?>" style="vertical-align: middle; text-align: center !important;">
                    <?= $noResultContent ?>
                </td>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if ($isFirstRow && $currentMonthNumber == 12) : ?>
        <td class="px-2 py-3 rowspan_cell" rowspan="<?= $rowspan ?>" style="vertical-align: middle; text-align: center !important;">
            <?= $noResultContent ?>
        </td>
    <?php endif; ?>
</tr>