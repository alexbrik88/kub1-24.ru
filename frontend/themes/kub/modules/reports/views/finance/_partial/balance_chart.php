<?php

use frontend\modules\reports\models\BalanceSearch;
use miloschuman\highcharts\Highcharts;

/* @var $model BalanceSearch */
?>
<div class="row">
    <div class="col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart' => [
                    'type' => 'column',
                ],
                'plotOptions' => [
                    'column' => [
                        'dataLabels' => [
                            //'enabled' => true,
                        ],
                        'stacking' => true
                    ]
                ],
                'tooltip' => [
                    'shared' => true,
                    'headerFormat' => '<b>{point.x}</b><br>',
                    'pointFormat' => '<b>{series.name}:</b> {point.y}<br>',
                    'footerFormat' => '<b>Итого: {point.total:,.0f}</b>'
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => ['text' => 'Структура активов'],
                'yAxis' => [
                    [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',

                    ],
                ],
                'xAxis' => [
                    ['categories' => $model->getChartPeriod(true)],
                ],
                'series' => $model->getChartAmount(BalanceSearch::TYPE_ASSETS),
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart' => [
                    'type' => 'column',
                ],
                'plotOptions' => [
                    'column' => [
                        'dataLabels' => [
                            //'enabled' => true,
                        ],
                        'stacking' => true
                    ]
                ],
                'tooltip' => [
                    'shared' => true,
                    'headerFormat' => '<b>{point.x}</b><br>',
                    'pointFormat' => '<b>{series.name}:</b> {point.y}<br>',
                    'footerFormat' => '<b>Итого: {point.total:,.0f}</b>'
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => ['text' => 'Структура пассивов'],
                'yAxis' => [
                    ['min' => 0, 'index' => 0, 'title' => ''],
                ],
                'xAxis' => [
                    ['categories' => $model->getChartPeriod()],

                ],
                'series' => $model->getChartAmount(BalanceSearch::TYPE_PASSIVE),
            ],
        ]); ?>
    </div>
</div>