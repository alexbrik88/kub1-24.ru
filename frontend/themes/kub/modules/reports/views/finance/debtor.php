<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:14
 */

use common\components\helpers\Html;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use frontend\models\Documents;
use frontend\modules\reports\components\DebtsHelper;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use common\models\EmployeeCompany;
use kartik\select2\Select2;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\reports\models\DebtReportSearch
 * @var $currentDebt integer
 * @var $debtSum10 integer
 * @var $debtSum30 integer
 * @var $debtSum60 integer
 * @var $debtSum90 integer
 * @var $debtSumMore90 integer
 * @var $allDebtSum integer
 * @var $formatSum integer
 */

$isDebtor = ($type == 2);

$this->title = 'Отчет по ' . ($isDebtor ? 'клиентам' : 'поставщикам');

$debtSumCountAll = 0;
$currentSum = 0;

$debtSumCountInvoiceAll = 0;
$currentInvoiceCount = 0;

$company = Yii::$app->user->identity->company;
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass($isDebtor ? 'table_view_report_client' : 'table_view_report_supplier');

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_debtor_help ?? false;
$showChartPanel = $userConfig->report_debtor_chart ?? false;
?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= ($isDebtor) ? 'Нам должны' : 'Мы должны' ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">

            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_debtor_chart">
    <div class="pt-3 pb-3 mb-2">
        <div class="row mb-4">
            <div class="col-12 pr-2">
                <?php
                // Charts
                echo $this->render('_charts/_chart_debtor_main', [
                    'type' => $type,
                    'model' => $searchModel,
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-4 pr-2">
                <?php
                // Charts
                echo $this->render('_charts/_chart_debtor_top_1', [
                    'type' => $type,
                    'model' => $searchModel,
                ]);
                ?>
            </div>
            <div class="col-4 pl-2">
                <?php
                // Charts
                echo $this->render('_charts/_chart_debtor_top_2', [
                    'type' => $type,
                    'model' => $searchModel,
                ]);
                ?>
            </div>
            <div class="col-4 pl-2">
                <?php
                // Charts
                echo $this->render('_charts/_chart_debtor_top_3', [
                    'type' => $type,
                    'model' => $searchModel,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_debtor_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/debtor_panel', [
            'type' => $type,
            'isDebtor' => $isDebtor
        ])
        ?>
    </div>
</div>

<?php /* OLD
<div class="wrap">
    <div class="row">
        <div class="col-12">
            <table class="table table-style table-count-list compact-disallow" style="width: 100%;">
                <thead>
                    <tr role="row">
                        <th></th>
                        <th class="text-left" style="font-weight: 600;">Сумма</th>
                        <th class="text-left" style="font-weight: 600;">%%</th>
                        <th class="text-left" style="font-weight: 600;">Кол-во<br><?=($isDebtor) ? 'должников':'поставщиков' ?></th>
                        <th class="text-left" style="font-weight: 600;">Кол-во<br>счетов</th>
                        <th class="text-left" style="font-weight: 600; max-width: 140px;">Средняя сумма долга <?=($isDebtor) ? 'должника':'поставщику' ?></th>
                        <th class="text-left" style="font-weight: 600; max-width: 140px;">Средняя сумма просроченного счета</th>
                    </tr>
                </thead>
                <tbody>
                <tr role="row">
                    <td class="text-left">Текущие неоплаченные счета
                    </td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($currentDebt, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($currentDebt / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getCurrentDebtCount(null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getCurrentDebtCount(null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($currentDebt / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($currentDebt / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left">1-10 дней просрочено</td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($debtSum10, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($debtSum10 / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_0_10, false, null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_0_10, false, null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum10 / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum10 / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left">11-30 дней просрочено</td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($debtSum30, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($debtSum30 / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_11_30, false, null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_11_30, false, null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum30 / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum30 / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left">31-60 дней просрочено</td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($debtSum60, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($debtSum60 / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_31_60, false, null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_31_60, false, null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum60 / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum60 / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left">61-90 дней просрочено</td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($debtSum90, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($debtSum90 / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_61_90, false, null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_61_90, false, null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum90 / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum90 / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left">Больше 90 дней просрочено</td>
                    <td class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($debtSumMore90, 2); ?>
                    </td>
                    <td class="text-right">
                        <?= round(($debtSumMore90 / $formatSum) * 100, 2); ?>%
                    </td>
                    <td class="text-right">
                        <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_MORE_90, false, null, $type);
                        $debtSumCountAll += $currentSum; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_MORE_90, false, null, $type);
                        $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSumMore90 / $currentSum, 2), 2) : 0; ?>
                    </td>
                    <td class="text-right">
                        <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSumMore90 / $currentInvoiceCount, 2), 2) : 0; ?>
                    </td>
                </tr>
                <tr role="row">
                    <td class="text-left" style="font-weight: 600;">Вся задолженность</td>
                    <td class="text-right" style="font-weight: 600;">
                        <?= TextHelper::invoiceMoneyFormat($allDebtSum, 2); ?>
                    </td>
                    <td class="text-right" style="font-weight: 600;">100 %</td>
                    <td class="text-right" style="font-weight: 600;"><?= $debtSumCountAll; ?></td>
                    <td class="text-right" style="font-weight: 600;"><?= $debtSumCountInvoiceAll; ?></td>
                    <td class="text-right" style="font-weight: 600;">
                        <?= $debtSumCountAll != 0 ? TextHelper::invoiceMoneyFormat($allDebtSum / $debtSumCountAll, 2) : 0; ?>
                    </td>
                    <td class="text-right" style="font-weight: 600;">
                        <?= $debtSumCountInvoiceAll != 0 ? TextHelper::invoiceMoneyFormat($allDebtSum / $debtSumCountInvoiceAll, 2) : 0; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
*/ ?>

<div class="table-settings row row_indents_s" style="margin-top: 10px">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Список <?=($isDebtor) ? 'должников':'кредиторов' ?>: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => $isDebtor ? 'table_view_report_client' : 'table_view_report_supplier']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'name',
            'label' => 'Название',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '15%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
                'style' => 'max-width: 225px'
            ],
            'filter' => $searchModel->getContractorFilter($type),
            'value' => function (Contractor $model) {
                return '<div style="max-width: 255px; overflow: hidden; text-overflow: ellipsis;">' . Html::a($model->nameWithType, [
                    '/contractor/view',
                    'id' => $model->id,
                    'type' => $model->type
                ], [
                    'title' => html_entity_decode($model->nameWithType)
                ]) . '</div>';
            },
            's2width' => '300px'
        ],
        [
            'attribute' => 'current_debt_sum',
            'label' => 'Текущие неоплаченные счета',
            'format' => 'raw',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'value' => function (Contractor $data) {
                return TextHelper::invoiceMoneyFormat($data->current_debt_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_0_10_sum',
            'label' => '1-10 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_0_10_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_11_30_sum',
            'label' => '11-30 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_11_30_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_31_60_sum',
            'label' => '31-60 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_31_60_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_61_90_sum',
            'label' => '61-90 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_61_90_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_more_90_sum',
            'label' => 'Больше 90 Дней',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_more_90_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_all_sum',
            'label' => 'Итого',
            'headerOptions' => [
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_all_sum, 2);
            },
        ],
        [
            'attribute' => 'responsible_employee_id',
            'label' => 'От&shy;вет&shy;ствен&shy;ный',
            'headerOptions' => [
                'width' => '15%',
            ],
            'encodeLabel' => false,
            'filter' => $searchModel->getResponsibleItemsByQuery($dataProvider->query),
            'value' => function (Contractor $model) use ($company) {
                $employee = EmployeeCompany::findOne([
                    'employee_id' => $model->responsible_employee_id,
                    'company_id' => $company->id,
                ]);

                return $employee ? $employee->getFio(true) : '';
            },
            's2width' => '250px'
        ],
    ],
]); ?>

<script>
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>
