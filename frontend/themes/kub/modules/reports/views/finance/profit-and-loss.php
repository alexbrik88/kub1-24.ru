<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2019
 * Time: 15:25
 */

use common\components\helpers\Html;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\reports\models\BalanceSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanFactSearch;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel ProfitAndLossSearchModel
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $data array
 * @var $activeTab int
 */

$this->title = "Отчет о Прибылях и Убытках для {$searchModel->company->companyTaxationType->getName()}";

$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->report_profit_loss_help ?? false;
$showChartPanel = $userConfig->report_profit_loss_chart ?? false;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

?>

<div class="wrap pt-1 pb-0 pl-4 pr-3 mb-2">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('diagram'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'class' => 'button-list button-hover-transparent button-clr mb-2' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \yii\bootstrap\Html::beginForm(['profit-and-loss'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="report_profit_loss_chart">
    <div class="pt-3 pb-3">
        <?php
        // Charts
        echo $this->render('_charts/_chart_profit_loss', [
            'model' => $searchModel,
        ]);
        ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="report_profit_loss_help">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('_partial/profit_and_loss_panel')
        ?>
    </div>
</div>

<div class="d-flex flex-nowrap pt-1 pb-1 align-items-center">

    <div class="mr-1 d-flex flex-nowrap">
        <div class="d-flex flex-nowrap mr-2">
            <div class="radio_join mb-2">
                <?= \yii\bootstrap\Html::a('Все операции',
                    Url::current(['activeTab' => ProfitAndLossSearchModel::TAB_ALL_OPERATIONS]), [
                        'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == ProfitAndLossSearchModel::TAB_ALL_OPERATIONS ? ' button-regular_red' : null),
                        'style' => 'min-width: 180px;'
                    ]); ?>
            </div>
            <div class="radio_join mb-2">
                <?= Html::a('Для бухгалтерии',
                    Url::current(['activeTab' => ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY]), [
                        'class' => 'button-regular pl-3 pr-3 mb-0' . ($activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY ? ' button-regular_red' : null),
                        'style' => 'min-width: 180px;'
                    ]); ?>
            </div>
        </div>
        <?php if (YII_ENV_DEV || !$searchModel->company->isFreeTariff): ?>
            <div class="ml-1 mr-2">
                <?= \yii\helpers\Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                    Url::to(['/reports/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                        'class' => 'download-odds-xls button-list button-hover-transparent button-clr mb-2',
                        'title' => 'Скачать в Excel',
                    ]); ?>
            </div>
        <?php endif; ?>
        <div class="ml-1">
            <?= TableViewWidget::widget(['attribute' => 'table_view_finance_balance']) ?>
        </div>
    </div>
    <?= Html::hiddenInput('activeTab', $activeTab, ['id' => 'active-tab_report']); ?>
</div>

<div class="wrap wrap_padding_none mb-2">
    <?= $this->render('_partial/profit_and_loss_table', [
        'searchModel' => $searchModel,
        'currentMonthNumber' => $currentMonthNumber,
        'currentQuarter' => $currentQuarter,
        'data' => $data,
        'activeTab' => $activeTab
    ]); ?>
</div>

<?= $this->render('_partial/profit_and_loss_items', [
    'searchModel' => $searchModel
]) ?>

<?= $this->render('_partial/_item_table/modals', ['searchModel' => $searchModel]) ?>

<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>

<script>

    ProfitAndLoss = {
        year: <?= $searchModel->year ?>,
        month: null,
        quarter: null,
        article: null,
        movementType: null,
        customMovementType: "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS ?>",
        accountingOperationsOnly: <?= $activeTab == ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY ? 1:0 ?>,
        _alertEmptyTab: false,
        _enableTabsAutoToggle: false,
        init: function() {

            this.bindEvents();
            this.bindModalEvents();
            this.addListeners();
        },
        bindEvents: function() {

            // open items table
            $(document).on('click', 'table.flow-of-funds td.can-click', function () {

                ProfitAndLoss.year = $('#profitandlosssearchmodel-year').val();
                ProfitAndLoss.quarter = $(this).data('quarter');
                ProfitAndLoss.month = $(this).data('month');
                ProfitAndLoss.article = $(this).data('article');
                ProfitAndLoss.movementType = $(this).data('movement-type');
                ProfitAndLoss._alertEmptyTab = false;
                ProfitAndLoss._enableTabsAutoToggle = true;

                if (!ProfitAndLoss.article || '0,00' === $(this).text().trim()) {
                    ProfitAndLoss._showFlash('Нет данных');
                    return false;
                }

                ProfitAndLoss._setCellColor(this);
                ProfitAndLoss._setTableNameByCell(this);
                ProfitAndLoss._toggleManyItemBtn();
                ProfitAndLoss._showPreloader();
                ProfitAndLoss.refresh();
            });

            // change custom movement type
            $('li.custom-movement-type').on('click', function() {

                if ($(this).hasClass('disabled'))
                    return false;

                var $block = $(this).closest('ul');
                $block.find('li').removeClass('active');
                $(this).addClass('active');
                ProfitAndLoss.customMovementType = $(this).data('custom-movement-type');
                ProfitAndLoss._alertEmptyTab = true;
                ProfitAndLoss._enableTabsAutoToggle = false;
                ProfitAndLoss.refresh();
            });

            // close items table
            $(".close-profit-and-loss-items").on("click", function () {
                let $block = $(".items-table-block");
                $block.find(".caption").html("Детализация");
                $block.find(".wrap_padding_none").html("");
                $block.addClass("hidden");
                $('table.flow-of-funds td.hover-checked').removeClass('hover-checked');
            });
        },
        bindModalEvents: function() {
            // many delete
            $(document).on("click", ".modal-many-delete-plan-item .btn-confirm-yes", function() {

                let $this = $(this);
                let l = Ladda.create(this);
                let checkedCheckboxes = $('.joint-checkbox:checked');
                let customMovementType = $('.custom-movement-type.active').data('custom-movement-type');
                let deleteUrl = (customMovementType === 'docs') ? '/reports/finance-ajax/many-delete-doc-item' : '/reports/finance-ajax/many-delete-flow-item';
                l.start();

                if (!$this.hasClass('clicked')) {
                    if ($(checkedCheckboxes).length > 0) {
                        $this.addClass('clicked');
                        $.post(deleteUrl, $(checkedCheckboxes).serialize(), function(data) {
                            ProfitAndLoss.reloadAll(function() {
                                Ladda.stopAll();
                                l.remove();
                                $('.modal:visible').modal('hide');
                                ProfitAndLoss._showFlash(data.msg);
                                $this.removeClass('clicked');
                            });
                        });
                    }
                }
                return false;
            });

            // many item (update articles)
            $(document).on("shown.bs.modal", "#many-item", function () {

                let checkedCheckboxes = $('.joint-checkbox:checked');
                let $includeExpenditureItem = $(checkedCheckboxes).filter('.expense-item').length > 0;
                let $includeIncomeItem = $(checkedCheckboxes).filter('.income-item').length > 0;
                let $modal = $(this);
                let $header = $modal.find(".modal-header h1");
                let $additionalHeaderText = null;

                if ($includeExpenditureItem) {
                    $(".expenditure-item-block").removeClass("hidden");
                }
                if ($includeIncomeItem) {
                    $(".income-item-block").removeClass("hidden");
                }
                if ($includeExpenditureItem && $includeIncomeItem) {
                    $additionalHeaderText = " прихода / расхода";
                } else if ($includeExpenditureItem) {
                    $additionalHeaderText = " расхода";
                } else if ($includeIncomeItem) {
                    $additionalHeaderText = " прихода";
                }
                $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
                $(".joint-checkbox:checked").each(function() {
                    $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
                });
            });

            // many item (update articles)
            $(document).on("hidden.bs.modal", "#many-item", function () {
                $(".expenditure-item-block").addClass("hidden");
                $(".income-item-block").addClass("hidden");
                $(".additional-header-text").remove();
                $(".modal#many-item form#js-cash_flow_update_item_form .joint-checkbox").remove();
            });

            // many item (update articles)
            $(document).on('submit', '#js-cash_flow_update_item_form', function(e) {
                e.preventDefault();

                let l = Ladda.create($(this).find(".btn-save")[0]);
                let $hasError = false;

                l.start();
                $(".js-expenditure_item_id_wrapper:visible, .js-income_item_id_wrapper:visible").each(function () {
                    $(this).removeClass("has-error");
                    $(this).find(".help-block").text("");
                    if ($(this).find("select").val() == "") {
                        $hasError = true;
                        $(this).addClass("has-error");
                        $(this).find(".help-block").text("Необходимо заполнить.");
                    }
                });

                if ($hasError) {
                    Ladda.stopAll();
                    l.remove();
                    return false;
                }

                let customMovementType = $('.custom-movement-type.active').data('custom-movement-type');
                let updateUrl = (customMovementType === 'docs') ? '/reports/finance-ajax/many-doc-item' : '/reports/finance-ajax/many-flow-item';

                $.post(updateUrl, $(this).serialize(), function (data) {
                    ProfitAndLoss.reloadAll(function() {
                        $('.modal:visible').modal('hide');
                        ProfitAndLoss._showFlash(data.msg);
                        Ladda.stopAll();
                        l.remove();
                    });
                });

                return false;
            });
        },
        addListeners: function() {
            // pjax complete
            $('#flow-items-pjax').on("pjax:complete", function () {
                ProfitAndLoss._hidePreloader();
                ProfitAndLoss._setDocsFlowsMenu();
                $('#doc-items-pjax').html('');
                $('#summary-container').removeClass('visible check-true');
            });
            // pjax complete
            $('#doc-items-pjax').on("pjax:complete", function () {
                ProfitAndLoss._hidePreloader();
                ProfitAndLoss._setDocsFlowsMenu();
                $('#flow-items-pjax').html('');
                $('#summary-container').removeClass('visible check-true');
            });
            // pjax send wrap
            $('#flow-items-pjax').on('pjax:beforeSend', function (event, xhr, settings) {
                if (settings.data == undefined) {
                    jQuery.pjax({
                        type: 'POST',
                        container: '#flow-items-pjax',
                        url: settings.url,
                        data: {
                            'searchBy': 'flows',
                            'year':    ProfitAndLoss.year,
                            'quarter': ProfitAndLoss.quarter,
                            'month':   ProfitAndLoss.month,
                            'article': ProfitAndLoss.article,
                            'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                            'enableToggle': ProfitAndLoss._enableTabsAutoToggle ? 1 : 0
                        },
                        timeout: 5000,
                        push: false,
                        scrollTo: false
                    });

                    return false;
                }
            });
            // pjax send wrap
            $('#doc-items-pjax').on('pjax:beforeSend', function (event, xhr, settings) {
                if (settings.data == undefined) {
                    jQuery.pjax({
                        type: 'POST',
                        container: '#doc-items-pjax',
                        url: settings.url,
                        data: {
                            'searchBy': 'docs',
                            'year':    ProfitAndLoss.year,
                            'quarter': ProfitAndLoss.quarter,
                            'month':   ProfitAndLoss.month,
                            'article': ProfitAndLoss.article,
                            'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                            'enableToggle': ProfitAndLoss._enableTabsAutoToggle ? 1 : 0
                        },
                        timeout: 5000,
                        push: false,
                        scrollTo: false
                    });

                    return false;
                }
            });
        },
        refresh: function() {

            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_FLOWS ?>") {
                $('#custom-movement-type').hide();
                ProfitAndLoss._getItems("#flow-items-pjax");
            }
            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS ?>") {
                $('#custom-movement-type').hide();
                ProfitAndLoss._getItems("#doc-items-pjax");
            }
            if (ProfitAndLoss.movementType === "<?= ProfitAndLossSearchModel::MOVEMENT_TYPE_DOCS_AND_FLOWS ?>") {
                $('#custom-movement-type').show();
                if (ProfitAndLoss.customMovementType === 'docs') {
                    ProfitAndLoss._getItems("#doc-items-pjax");
                } else {
                    ProfitAndLoss._getItems("#flow-items-pjax");
                }
            }
        },
        _getItems: function(pjaxContainer) {

            $.pjax.reload(pjaxContainer, {
                url: '/reports/finance-ajax/get-profit-and-loss-items',
                timeout: 5000
            });

            $('.items-table-block').removeClass('hidden');
            $('html, body').animate({scrollTop: $('.items-table-block').offset().top - 50}, "slow");
        },
        _showPreloader: function() {
            $('#hellopreloader').show();
            $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
        },
        _hidePreloader: function() {
            let hellopreloader = document.getElementById("hellopreloader_preload");
            fadeOutPreloader(hellopreloader);
        },
        _setCellColor: function(td) {
            $('table.flow-of-funds td.hover-checked').removeClass('hover-checked');
            let $i = 0;
            if ($(td).hasClass('quarter-block')) {
                let $prevTd = $(td).prev('td');
                do {
                    $prevTd.addClass('hover-checked');
                    $prevTd = $prevTd.prev('td');
                    $i++
                }
                while ($i < 3);
            }
            $(td).addClass('hover-checked');
        },
        _setTableNameByCell: function(td) {
            const monthName = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'ноябрь', 'декабрь'];
            let year = $('#profitandlosssearchmodel-year').val();
            let quarter = $(td).data('quarter');
            let month = $(td).data('month');
            let articleName = $(td).closest('tr').find('td').first().text().trim();
            let result = 'Детализация' +
                (articleName ? (' "' + articleName + '" за ') : '') +
                (quarter ? (quarter + ' кв.') : '') +
                (month ? monthName[month - 1] : '') +
                (year ? (' ' + year + 'г.') : '');

            $('.items-table-block h4').html(result);
        },
        _setDocsFlowsMenu: function() {

            let searchBy = $('#searchBy');
            let changedSearchBy = $('#isSearchByChanged');
            let customMovementType = $('.custom-movement-type');

            if (searchBy.length) {

                let activeLiClass = ($(searchBy).val() === 'flows') ? '.flows' : '.docs';
                let disabledLiClass = ($(searchBy).val() === 'flows') ? '.docs' : '.flows';

                //$(customMovementType).removeClass('disabled');

                if ($(changedSearchBy).length) {
                    $(customMovementType).filter(activeLiClass).addClass('active');
                    $(customMovementType).filter(disabledLiClass).removeClass('active');
                    $(changedSearchBy).remove();
                    $(searchBy).remove();

                    //if (ProfitAndLoss._alertEmptyTab) {
                    //    ProfitAndLoss._showFlash('Нет данных в разделе "' + ($(searchBy).val() === 'flows' ? 'Документы' : 'Деньги') + '"');
                    //    ProfitAndLoss._alertEmptyTab = false;
                    //}

                } else {
                    $(customMovementType).filter(activeLiClass).addClass('active');
                    $(customMovementType).filter(disabledLiClass).removeClass('active');
                    $(searchBy).remove();
                }
            }
        },
        _toggleManyItemBtn: function() {
            let btn = $('#summary-container').find('a[href="#many-item"]');
            if (btn.length && this.article === "income-revenue_doc")
                $(btn).hide();
            else
                $(btn).show();
        },
        reloadAll: function(callback)
        {
            let debugCurrTime = Date.now();
            return ProfitAndLoss._reloadChart().done(function() {
                console.log('reloadSubTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();
                ProfitAndLoss._reloadSubTable().done(function() {
                    console.log('reloadMainTable: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                    ProfitAndLoss._reloadMainTable().done(function() {
                        console.log('reloadChart: ' + 1/1000 * (Date.now() - debugCurrTime)); debugCurrTime = Date.now();

                        if (typeof callback === "function") {
                            return callback();
                        }

                    }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #1') });
                }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #2') });
            }).catch(function() { ProfitAndLoss._showFlash('Ошибка сервера #3') });
        },
        _reloadSubTable: function()
        {
            const $year = "ProfitAndLossSearchModel%5Byear%5D=" + $('#profitandlosssearchmodel-year').val();

            return jQuery.pjax({
                url: '/reports/finance-ajax/get-profit-and-loss-items?' + $year,
                type: 'POST',
                data: {
                    'searchBy': 'flows',
                    'year':    ProfitAndLoss.year,
                    'quarter': ProfitAndLoss.quarter,
                    'month':   ProfitAndLoss.month,
                    'article': ProfitAndLoss.article,
                    'accountingOperationsOnly': ProfitAndLoss.accountingOperationsOnly,
                    'enableToggle': 0
                },
                container: (ProfitAndLoss.customMovementType === 'flows') ? "#flow-items-pjax" : "#doc-items-pjax",
                timeout: 10000,
                push: false,
                scrollTo: false
            });
        },
        _reloadMainTable: function ()
        {
            let floorMap = {};
            $('table.flow-of-funds').find('[data-collapse-row-trigger], [data-collapse-trigger], [data-collapse-trigger-days]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            let data = {
                floorMap: floorMap
            };
            let activeTab = $('#active-tab_report').val();

            return $.post('/reports/finance-ajax/profit-and-loss-part/?activeTab=' + activeTab, data, function ($data) {
                let html = new DOMParser().parseFromString($data, "text/html");
                let table = html.querySelector('table.flow-of-funds tbody');
                let origTable = document.querySelector('table.flow-of-funds tbody');
                origTable.innerHTML = table.innerHTML;

                if ($coloredItemsCoords) {
                    $.each($coloredItemsCoords, function(i,v) {
                        $('table.flow-of-funds tbody').find('tr').eq(v.tr).find('td').eq(v.td).addClass('hover-checked');
                    });
                }

                ProfitAndLoss._bindMainTableEvents();
            });
        },
        _reloadChart: function() {
            return window.ChartsProfitAndLoss.redrawByClick();
        },
        _bindMainTableEvents: function() {
            // main.js
            $('[data-collapse-row-trigger]', 'table.flow-of-funds tbody').click(function() {
                var target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    // level 1
                    $('[data-id="'+target+'"]').addClass('d-none');
                    $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
                    $('[data-id="'+target+'"]').each(function(i, row) {
                        // level 2
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').find('[data-collapse-row-trigger]').removeClass('active');
                        $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').each(function(i, row) {
                            $('[data-id="'+ $(row).find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                    $('[data-collapse-all-trigger]').removeClass('active');
                } else {
                    $('[data-collapse-all-trigger]').addClass('active');
                }
            });
        },
        _showFlash: function(text) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        },
    };

    /////////////////////
    ProfitAndLoss.init();
    /////////////////////

</script>

<?php /* $this->registerJs('
     $(".profit-and-loss-panel-trigger").click(function (e) {
        $(".profit-and-loss-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        let $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        let $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); */ ?>