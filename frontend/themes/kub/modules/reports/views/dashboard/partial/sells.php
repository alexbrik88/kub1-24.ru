<?php
$jsPositionerFunc = <<<JS
function (boxWidth, boxHeight, point) { return {x:point.plotX + 40,y:point.plotY}; }
JS;

$htmlHeader = <<<HTML
    <table class="ht-in-table">
        <tr>
            <th>Продажи за</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
HTML;
$htmlData = <<<HTML
        <tr>
            <td><div class="ht-title">{company}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart yellow" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>
HTML;
$htmlFooter = <<<HTML
    </table>
    <table class="ht-in-footer-table">
        <tr>
            <td><strong>Всего {total} продаж</strong></td>
            <td><a href="#" class="link">Посмотреть все</a></td>
        </tr>
    </table>
HTML;

$htmlHeader = str_replace(["\r", "\n", "'"], "", $htmlHeader);
$htmlData = str_replace(["\r", "\n", "'"], "", $htmlData);
$htmlFooter = str_replace(["\r", "\n", "'"], "", $htmlFooter);

$jsFormatterFunc = <<<JS

function (a) {
    
      if(1 == this.point.series.index) 
          return false;
    
      var idx = this.point.index;
      var tooltipHtml;

      var arr = [
          [
              {company: 'ООО "ОПТИМА"', percent: 100, sum: "100 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 75, sum: "75 000,00"},
              {company: 'ООО "Ремппром"', percent: 50, sum: "50 000,00"},
              {company: 'ООО "Аликов"', percent: 40, sum: "40 000,00"},
              {company: 'Ип Сатыренко', percent: 40, sum: "40 000,00"},            
          ],
          [
              {company: 'ООО "Аликов"', percent: 100, sum: "100 000,00"},
              {company: 'Ип Сатыренко', percent: 90, sum: "90 000,00"},              
              {company: 'ООО "ОПТИМА"', percent: 60, sum: "60 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 25, sum: "25 000,00"},
              {company: 'ООО "Ремппром"', percent: 20, sum: "20 000,00"}
          ],
          [
              {company: 'ООО "ОПТИМА"', percent: 100, sum: "100 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 75, sum: "75 000,00"},
              {company: 'ООО "Ремппром"', percent: 50, sum: "50 000,00"},
              {company: 'ООО "Аликов"', percent: 40, sum: "40 000,00"},
              {company: 'Ип Сатыренко', percent: 40, sum: "40 000,00"},   
          ],
          [
              {company: 'ООО "ОПТИМА"', percent: 50, sum: "50 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 25, sum: "25 000,00"},
              {company: 'ООО "Ремппром"', percent: 10, sum: "10 000,00"},
              {company: 'ООО "Аликов"', percent: 10, sum: "10 000,00"},
              {company: 'Ип Сатыренко', percent: 5, sum: " 5 000,00"},   
          ],
          [
              {company: 'ООО "ОПТИМА"', percent: 100, sum: "100 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 75, sum: "75 000,00"},
              {company: 'ООО "Ремппром"', percent: 50, sum: "50 000,00"},
              {company: 'ООО "Аликов"', percent: 40, sum: "40 000,00"},
              {company: 'Ип Сатыренко', percent: 40, sum: "40 000,00"},            
          ],
          [
              {company: 'ООО "Аликов"', percent: 100, sum: "100 000,00"},
              {company: 'Ип Сатыренко', percent: 90, sum: "90 000,00"},              
              {company: 'ООО "ОПТИМА"', percent: 60, sum: "60 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 25, sum: "25 000,00"},
              {company: 'ООО "Ремппром"', percent: 20, sum: "20 000,00"}
          ],
          [
              {company: 'ООО "ОПТИМА"', percent: 100, sum: "100 000,00"},
              {company: 'ООО "РОКФОЛ"', percent: 75, sum: "75 000,00"},
              {company: 'ООО "Ремппром"', percent: 50, sum: "50 000,00"},
              {company: 'ООО "Аликов"', percent: 40, sum: "40 000,00"},
              {company: 'Ип Сатыренко', percent: 40, sum: "40 000,00"},   
          ],          
      ];
      
      var arrTotals = [
          "23",
          "18",
          "25",
          "15",
          "23",
          "18",
          "25",          
      ]
      
      var arrDate = [
          "27.08.2019",
          "28.08.2019",
          "29.08.2019",
          "30.08.2019",
          "31.08.2019",
          "01.09.2019",
          "02.09.2019",
      ]
      
      //console.log(arr[idx]);
      
      tooltipHtml = '$htmlHeader'.replace("{point.x}", arrDate[idx]);
      if (arr[idx] != undefined) {
          arr[idx].forEach(function(data) {
           tooltipHtml += '$htmlData'
               .replace("{company}", data.company)
               .replace("{percent}", data.percent)
               .replace("{sum}", data.sum);              
          });
      } else {
          tooltipHtml += '<tr><td colspan="3">Нет данных</td></tr>';
      }
      if (arrTotals[idx] != undefined) {
          tooltipHtml += '$htmlFooter'.replace("{total}", arrTotals[idx]);          
      }

      return tooltipHtml;
    }

JS;

$jsXLabelFormatterFunc = <<<JS
function() { return this.value == 2 ? '<span class="red-date">' + this.value + '</span>' : this.value; }
JS;

?>
<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-sells',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
        'modules/pattern-fill'
    ],
    'options' => [
        'exporting' => [
            'enabled' => false
        ],
        'credits' => [
            'enabled' => false
        ],
        'chart' => [
            'type' => 'column',
            'spacing' => [0,0,0,0],
            'height' => 220,
            'animation' => false // TEMP FOR CUSTOM ANIMATION
        ],
        'tooltip' => [
            'backgroundColor' => "rgba(255,255,255,1)",
            'borderColor' => '#ddd',
            'borderWidth' => '1',
            'positioner' => new \yii\web\JsExpression($jsPositionerFunc),
            'formatter' => new \yii\web\JsExpression($jsFormatterFunc),
            'useHTML' => true,

        ],
        'title' => [
            'text' => 'Продажи',
            'align' => 'left',
            'floating' => true,
            'style' => [
                'font-size' => '12px',
                'color' => '#9198a0',
            ],
            'x' => 0,
        ],
        'legend' => [
            'layout' => 'horizontal',
            'align' => 'right',
            'verticalAlign' => 'top',
            'backgroundColor' => '#fff',
            'itemStyle' => [
                'fontSize' => '11px',
                'color' => '#9198a0'
            ],
            'symbolRadius' => 2
        ],
        'yAxis' => [
            'min' => 0,
            'max' => 250000, // TEMP FOR CUSTOM ANIMATION
            'index' => 0,
            'title' => '',
            'minorGridLineWidth' => 0,
        ],
        'xAxis' => [
            [
                'categories' => ['27', '28', '29', '30', '31', '1', '2'],
                'labels' => [
                    'formatter' => new \yii\web\JsExpression($jsXLabelFormatterFunc),
                    'useHTML' => true,
                ],
            ],

        ],
        'series' => [
            [
                'name' => 'Факт',
                'data' => [150000, 200000, 180000, 150000, 160000, 150000, 190000],
                'color' => 'rgba(243,183,58,1)',
                'states' => getPatternHover('rgba(243,183,58,1)'),
                'pointWidth' => 20,
                'borderRadius' => 3
            ],
            [
                'name' => 'План',
                'data' => [170000, 220000, 180000, 150000, 170000, 180000, 200000],
                'marker' => [
                    'symbol' => 'c-rect',
                    'lineWidth' => 3,
                    'lineColor' =>  'rgba(50,50,50,1)',
                    'radius' => 10
                ],
                'type' => 'spline',
                'lineWidth' => 0,
                'states' => [
                    'hover' => [
                        'lineWidthPlus' => 0
                    ]
                ],
                'pointPlacement' => 0.015,
                'stickyTracking' => false,
            ]
        ],
        'plotOptions' => [
            'spline' => [
                'tooltip' => [
                      'enabled' => false
                ],
                'states' => [
                    'inactive' => [
                        'opacity' => 1
                    ]
                ]
            ],
            'column' => [
                'grouping' => false,
                'shadow' => false,
                'borderWidth' => 0
            ],
            'series' => [
                'states' => [
                    'inactive' => [
                        'opacity' => 1
                    ]
                ],
            ]
        ]
    ],
]); ?>

<div class="ht-caption" style="margin-top: 10px;">
    Август
</div>
<table class="ht-in-table ht-table-2">
    <tr>
        <td><div class="ht-chart-wrap"><div class="ht-chart darkblue" style="width: 100%"></div></div></td>
        <td><div class="ht-total"><strong>601 270,00 ₽</strong></div></td>
        <td><div class="ht-total" style="text-align: right"><strong>(-14,22%)</strong></div></td>
    </tr>
    <tr>
        <td><div class="ht-chart-wrap"><div class="ht-chart blue" style="width: 70%"></div></div></td>
        <td><div class="ht-total gray"><span style="font-size: 12px">700 950,00 ₽</span></div></td>
        <td><div class="ht-total gray" style="text-align: right"><span style="font-size: 12px">(июль, 2019)</span></div></td>
    </tr>
</table>