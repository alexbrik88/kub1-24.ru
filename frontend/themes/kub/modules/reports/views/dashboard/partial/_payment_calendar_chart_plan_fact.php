<?php

use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use common\components\helpers\ArrayHelper;

/* @var $model \frontend\modules\reports\models\PaymentCalendarSearch
 * @var $currYear int
 */
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';

$currMonthPos = -1;
$isShowCurrentYear = $currYear == (int)date('Y');
$datePeriods = $model->getChartYearPeriods($currYear);
$prevYearDatePeriods = $model->getChartYearPeriods($currYear - 1);

$monthPeriods = [];
$chartFreeDays = [];
foreach ($datePeriods as $i => $date) {
    $dateArr = explode('-', $date['from']);
    $month = (int)$dateArr[1];
    $monthPeriods[] = Month::$monthFullRU[$month];
    if ($month == (int)date('m') && $isShowCurrentYear)
        $currMonthPos = $i;
}

// chart data
$incomeFlowsFactPlan = $model->getChartAmountByDate(CashFlowsBase::FLOW_TYPE_INCOME, $datePeriods);
$outcomeFlowsFactPlan = $model->getChartAmountByDate(CashFlowsBase::FLOW_TYPE_EXPENSE, $datePeriods);
$incomeFlowsPlan = $model->getChartAmountByDate(CashFlowsBase::FLOW_TYPE_INCOME, $datePeriods, true);
$outcomeFlowsPlan = $model->getChartAmountByDate(CashFlowsBase::FLOW_TYPE_EXPENSE, $datePeriods,true);

// chart2 data
$balanceFact = $model->getChartBalanceByDateFact($datePeriods);
$balanceFactPrevYear = $model->getChartBalanceByDateFact($prevYearDatePeriods);
$balancePlan = []; //$model->getChartBalanceByDatePlan($datePeriods, true, false);

// connect plan/fact edges
if ($isShowCurrentYear) {
    for ($i = 0; $i <= $currMonthPos; $i++) {
        $balancePlan[$i] = null;
    }
    $balancePlan[$currMonthPos] = $balanceFact[$currMonthPos];
    for ($i = $currMonthPos + 1; $i < count($datePeriods); $i++) {
        $balancePlan[$i] = $balancePlan[$i - 1] + $incomeFlowsFactPlan[$i] - $outcomeFlowsFactPlan[$i];
    }
}

?>

<div style="width: 100%">
    <div style="min-height:400px">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-plan-fact',
            'scripts' => [
                'modules/pattern-fill',
                'themes/grid-light',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'events' => [
                        'load' => ($isShowCurrentYear) ? new JsExpression('redrawPlanMonths()') : null
                    ],
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],

                'tooltip' => [
                    'useHtml' => true,
                    'split' => true,
                    'shared' => true,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'pointFormat' => '<span class="gray-text">{series.name}: </span><span class="gray-text-b">{point.y} ₽</span>',
                    // 'hideDelay' => 30000 // TEST
                ],

                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => ['text' => ''],
                'yAxis' => [
                    [
                        'height' => '45%',
                        'min' => 0,
                        'index' => 0,
                        'minorGridLineWidth' => 0,
                        'title' => '',
                    ],
                    [
                        'top' => '55%',
                        'height' => '45%',
                        'offset' => 0,
                        'lineWidth' => 2,
                        'title' => '',
                    ]
                ],
                'xAxis' => [
                    'min' => 1,
                    'max' => 11,
                    'categories' => $monthPeriods,
                    'labels' => [
                        'formatter' => new \yii\web\JsExpression("function() { return '<span class=\"x-axis\">' + this.value + '</span>'; }"),
                        'useHTML' => true,
                    ],
                    'crosshair' => [
                        'color' => '#000',
                        'width' => 2,
                        'snap' => true
                    ]
                ],
                'series' => [
                    [
                        'name' => 'Приход Факт',
                        'data' => $incomeFlowsFactPlan,
                        'legendIndex' => 0,
                        'color' => $color1,
                        'borderColor' => 'rgba(46,159,191,.3)',
                        'states' => [
                            'hover' => [
                                'color' => [
                                    'pattern' => [
                                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                        'color' => $color1,
                                        'width' => 5,
                                        'height' => 5
                                    ]
                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Расход Факт',
                        'data' => $outcomeFlowsFactPlan,
                        'legendIndex' => 1,
                        'color' => $color2,
                        'borderColor' => $color2_opacity,
                        'states' => [
                            'hover' => [
                                'color' => [
                                    'pattern' => [
                                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                        'color' => $color2,
                                        'width' => 5,
                                        'height' => 5
                                    ]
                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Приход План',
                        'data' => $incomeFlowsPlan,
                        'legendIndex' => 2,
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(21,67,96,1)',
                            'radius' => 15.525
                        ],
                        'type' => 'scatter',
                        'pointPlacement' => -0.22,
                        'enableMouseTracking' => false
                    ],
                    [
                        'name' => 'Расход План',
                        'data' => $outcomeFlowsPlan,
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(50,50,50,1)',
                            'radius' => 15.525
                        ],
                        'type' => 'scatter',
                        'pointPlacement' => 0.23,
                        'enableMouseTracking' => false
                    ],

                    ////////////////////////////////////// yAxis = 1

                    [
                        'yAxis' => 1,
                        'type' => 'areaspline',
                        'name' => 'Остаток Факт (предыдущий год)',
                        'data' => $balanceFactPrevYear,
                        'legendIndex' => 5,
                        'color' => 'rgba(129,145,146,1)',
                        'fillColor' => 'rgba(149,165,166,1)',
                        'dataLabels' => [
                            'enabled' => false
                        ],
                    ],
                    [
                        'yAxis' => 1,
                        'type' => 'areaspline',
                        'name' => 'Остаток План',
                        'data' => $balancePlan,
                        'legendIndex' => 4,
                        'color' => 'rgba(26,184,93,1)',
                        'negativeColor' => 'red',
                        'fillColor' => [
                            'pattern' => [
                                'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                'color' => '#27ae60',
                                'width' => 10,
                                'height' => 10
                            ]
                        ],
                        'negativeFillColor' => [
                            'pattern' => [
                                'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                'color' => '#e74c3c',
                                'width' => 10,
                                'height' => 10
                            ]
                        ],
                        'marker' => [
                            'symbol' => 'square'
                        ],
                        'dataLabels' => [
                            'enabled' => false
                        ],
                    ],
                    [
                        'yAxis' => 1,
                        'type' => 'areaspline',
                        'name' => 'Остаток Факт',
                        'data' => $balanceFact,
                        'legendIndex' => 3,
                        'color' => 'rgba(26,184,93,1)',
                        'fillColor' => 'rgba(46,204,113,1)',
                        'negativeColor' => 'red',
                        'negativeFillColor' => 'rgba(231,76,60,1)',
                        //'zIndex' => 2,
                        'dataLabels' => [
                            'enabled' => false
                        ],
                        //'states' => [
                        //    'inactive' => [
                        //        'opacity' => .1
                        //    ]
                        //]
                    ],
                ],
                'plotOptions' => [
                    'scatter' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ]
                    ],
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        'groupPadding' => 0.05,
                        'pointPadding' => 0.1,
                        'borderRadius' => 3
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>

<script>
    var chartCurrMonthPos = <?= (int)$currMonthPos; ?>;

    function redrawPlanMonths() {

        var custom_pattern = function (color) {
            return {
                pattern: {
                    path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                    width: 10,
                    height: 10,
                    color: color
                }
            }
        }

        var chartToLoad = window.setInterval(function () {
            var chart = $('#chart-plan-fact').highcharts();
            if (typeof(chart) !== 'undefined') {
                console.log(chart.series);
                for (var i = (1+chartCurrMonthPos); i < <?=(count($datePeriods))?>; i++) {
                    chart.series[0].points[i].color = custom_pattern("<?= $color1 ?>");
                    chart.series[1].points[i].color = custom_pattern("<?= $color2 ?>");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();

                window.clearInterval(chartToLoad);
            }

        }, 100);
    }

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });
</script>