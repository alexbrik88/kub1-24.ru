<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.03.2017
 * Time: 3:31
 */

use common\components\TextHelper;
use frontend\modules\reports\models\InvoiceReportSearch;
use common\models\document\Invoice;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use common\models\document\PaymentForm;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\status\InvoiceStatus;
use frontend\rbac\permissions\document\Document;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;

/* @var $newClients [] */
/* @var $allSum [] */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel InvoiceReportSearch */

$this->title = 'Отчет по новым клиентам';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_invoice');
?>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/_by_invoices_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <?= RangeButtonWidget::widget(['pjaxSelector' => '#abc-analysis-pjax']); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-9">
            <table class="table table-style table-count-list compact-disallow">
                <thead>
                <tr class="heading">
                    <th style="width: 24%;">Сотрудник</th>
                    <th style="width: 22%;">Долг на начало периода</th>
                    <th style="width: 22%;">Выставлено за период</th>
                    <th style="width: 22%;">Оплачено за период</th>
                    <th style="width: 10%;">%%</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($newClients as $newClient): ?>
                    <tr>
                        <td><?= $newClient['author']; ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($newClient['debtOnStartPeriod'], 2); ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($newClient['newInvoicesSum'], 2); ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($newClient['debtOnEndPeriod'], 2); ?></td>
                        <td><?= round(($newClient['debtOnEndPeriod'] / $allSum['allDebtOnEndPeriod']) * 100, 2); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><b>Итого</b></td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($allSum['allDebtOnStartPeriod'], 2); ?></b>
                    </td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($allSum['allNewInvoicesSum'], 2); ?></b>
                    </td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($allSum['allDebtOnEndPeriod'], 2); ?></b>
                    </td>
                    <td><b>100</b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-3">
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-12">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Список счетов по новым клиентам: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_invoice']) ?>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'invoice.contractor_id',
            'label' => 'Контрагент',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '25%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return $model->contractor->nameWithType;
            },
        ],
        [
            'attribute' => 'document_date',
            'label' => 'Дата счета',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ счета',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $model,
                ])
                    ? \yii\helpers\Html::a($model->fullNumber, ['/documents/invoice/view',
                        'type' => $model->type,
                        'id' => $model->id,
                        'contractorId' => $model->contractor_id,
                    ])
                    : $model->fullNumber;
            },
        ],
        [
            'attribute' => 'total_amount_with_nds',
            'label' => 'Сумма счета',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '15%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $amount = '<span>' . TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2) . '</span>';
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                    $amount .= ' / <span style="color: #45b6af;" title="Оплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . '</span>';
                } elseif ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null) {
                    $amount .= ' / <span style="color: #f3565d;" title="Неоплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . '</span>';
                }

                return $amount;
            },
        ],
        [
            'attribute' => 'invoice_status_updated_at',
            'label' => 'Дата оплаты',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return date(DateHelper::FORMAT_USER_DATE, $model->invoice_status_updated_at);
            },
        ],
        [
            'attribute' => 'payment_form_id',
            'label' => 'Тип оплаты',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '15%',
            ],
            'format' => 'raw',
            'filter' => $searchModel->paymentTypeNewClients,
            'value' => function (Invoice $model) {
                return PaymentForm::findOne($model->getPaymentType()) ? PaymentForm::findOne($model->getPaymentType())->name : null;
            },
            's2width' => '150px'
        ],
        [
            'attribute' => 'document_author_id',
            'label' => 'Выставил счет',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '15%',
            ],
            'format' => 'raw',
            'filter' => $searchModel->getAuthorFilter(),
            'value' => function (Invoice $model) {
                return $model->documentAuthor->getFio(true);
            },
            's2width' => '250px'
        ],
    ],
]); ?>