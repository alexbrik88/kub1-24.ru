<?php
use frontend\modules\reports\models\InvoiceReportSearch;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use frontend\models\Documents;

/**
 * @var array $data
 * @var string $attr
 * @var string $title
 * @var InvoiceReportSearch $searchModel
 */

$data = [];

switch ($attr) {
    case 'responsible_employee_id':
        $data = $searchModel->getResponsibleEmployersFilter();
        break;
    case 'out_contractor_id':
        $data = $searchModel->getContractorsFilter(Documents::IO_TYPE_OUT);
        break;
    case 'in_contractor_id':
        $data = $searchModel->getContractorsFilter(Documents::IO_TYPE_IN);
        break;
    case 'out_invoice_status_id':
        $data = $searchModel->getInvoicesStatusesFilter(Documents::IO_TYPE_OUT);
        break;
    case 'in_invoice_status_id':
        $data = $searchModel->getInvoicesStatusesFilter(Documents::IO_TYPE_IN);
        break;
}

?>
<div class="pl-1 pr-1">
    <div id="filter_<?=($attr)?>" class="th-title filter <?= $searchModel->{$attr} ? 'active' : '' ?>" >
        <span class="th-title-name">
            <?= $title ?>
        </span>
        <span class="th-title-btns">
            <?= \yii\helpers\Html::button(
                Icon::get('filter', ['class' => 'th-title-icon-filter']),
                ['class' => 'th-title-btn button-clr'])
            ?>
        </span>
        <div class="filter-select2-select-container">
            <?= Select2::widget([
                'id' => "select2_{$attr}",
                'name' => $attr,
                'data' => ['' => 'Все'] + $data,
                'value' => $searchModel->{$attr},
                'pluginOptions' => [
                    'width' => '300px',
                    'containerCssClass' => 'select2-filter-by-product'
                ],
            ]); ?>
        </div>
    </div>
</div>