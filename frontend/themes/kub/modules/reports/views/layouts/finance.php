<?php

use common\models\balance\BalanceArticle;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

$this->params['is_finance_reports_module'] = true;

// todo:temp
$visibleAnalyticsDevModule = (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [112, 486, 628, 11270, 23083, 53146]));
$type = Yii::$app->request->get('type');
?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2">
        <?php
        if (in_array(Yii::$app->controller->action->id, ['balance', 'debtor']) || Yii::$app->controller->id == 'balance-articles') {
            // WHERE IS MONEY?
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'Нам должны',
                        'url' => ['/reports/finance/debtor', 'type' => Documents::IO_TYPE_OUT],
                        'active' => Yii::$app->controller->action->id == 'debtor' && $type == Documents::IO_TYPE_OUT,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Мы должны',
                        'url' => ['/reports/finance/debtor', 'type' => Documents::IO_TYPE_IN],
                        'active' => Yii::$app->controller->action->id == 'debtor' && $type == Documents::IO_TYPE_IN,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Баланс',
                        'url' => ['/reports/finance/balance'],
                        'active' => Yii::$app->controller->action->id == 'balance',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => $visibleAnalyticsDevModule
                    ],
                    [
                        'label' => 'Основные средства',
                        'url' => ['/reference/balance-articles/index', 'type' => BalanceArticle::TYPE_FIXED_ASSERTS, 'layout' => 2],
                        'active' => Yii::$app->controller->id === 'balance-articles'
                            && Yii::$app->controller->action->id === 'index'
                            && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_FIXED_ASSERTS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'НМА',
                        'url' => ['/reference/balance-articles/index', 'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS, 'layout' => 2],
                        'active' => Yii::$app->controller->id === 'balance-articles'
                            && Yii::$app->controller->action->id === 'index'
                            && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_INTANGIBLE_ASSETS,
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                ],
            ]);

        } elseif (in_array(Yii::$app->controller->action->id, ['profit-and-loss', 'breakeven-point'])) {
            // EFFECTIVENESS
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'ОПиУ (P&L)',
                        'url' => ['/reports/finance/profit-and-loss'],
                        'active' => Yii::$app->controller->action->id == 'profit-and-loss',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => $visibleAnalyticsDevModule
                    ],
                    [
                        'label' => 'Точка безубыточности',
                        'url' => ['/reports/finance/breakeven-point'],
                        'active' => Yii::$app->controller->action->id == 'breakeven-point',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => $visibleAnalyticsDevModule
                    ],
                ],
            ]);
        } else {
            // STADNDART MENU
            echo Nav::widget([
                'id' => 'debt-report-menu',
                'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                'items' => [
                    [
                        'label' => 'ОДДС',
                        'url' => ['/reports/finance/odds'],
                        'active' => Yii::$app->controller->action->id == 'odds',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Платёжный календарь',
                        'url' => ['/reports/finance/payment-calendar'],
                        'active' => Yii::$app->controller->action->id == 'payment-calendar',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => $visibleAnalyticsDevModule
                    ],
                    [
                        'label' => 'План-Факт',
                        'url' => ['/reports/finance/plan-fact'],
                        'active' => Yii::$app->controller->action->id == 'plan-fact',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                        'visible' => $visibleAnalyticsDevModule
                    ],
                    [
                        'label' => 'Приходы',
                        'url' => ['/reports/finance/income'],
                        'active' => Yii::$app->controller->action->id == 'income',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    //[
                    //    'label' => 'ОПиУ (P&L)',
                    //    'url' => ['/reports/finance/profit-and-loss'],
                    //    'active' => Yii::$app->controller->action->id == 'profit-and-loss',
                    //    'options' => ['class' => 'nav-item'],
                    //    'linkOptions' => ['class' => 'nav-link'],
                    //    'visible' => $visibleAnalyticsDevModule
                    //],
                    //[
                    //    'label' => 'Баланс',
                    //    'url' => ['/reports/finance/balance'],
                    //    'active' => Yii::$app->controller->action->id == 'balance',
                    //    'options' => ['class' => 'nav-item'],
                    //    'linkOptions' => ['class' => 'nav-link'],
                    //    'visible' => $visibleAnalyticsDevModule
                    //],
                    [
                        'label' => 'Расходы',
                        'url' => ['/reports/finance/expenses'],
                        'active' => Yii::$app->controller->action->id == 'expenses',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ],
                    [
                        'label' => 'Статьи операций',
                        'url' => ['/reference/articles/index', 'type' => 2],
                        'active' => Yii::$app->controller->id == 'articles',
                        'options' => ['class' => 'nav-item'],
                        'linkOptions' => ['class' => 'nav-link'],
                    ]
                ],
            ]);
        }
        ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>