<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\Nav;

$controller =  Yii::$app->controller->id;
$action =  Yii::$app->controller->action->id;

echo Nav::widget([
    'id' => 'debt-report-menu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
    'items' => [
        [
            'label' => 'По выставленным счетам',
            'url' => ['/reports/invoice-report/created'],
            'active' => $controller == 'invoice-report' && $action == 'created',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'По оплаченным счетам',
            'url' => ['/reports/invoice-report/payed'],
            'active' => $controller == 'invoice-report' && $action == 'payed',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'По долгам',
            'url' => ['/reports/invoice-report/not-payed'],
            'active' => $controller == 'invoice-report' && $action == 'not-payed',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'По новым клиентам',
            'url' => ['/reports/invoice-report/new-clients'],
            'active' => $controller == 'invoice-report' && $action == 'new-clients',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
        [
            'label' => 'Связанные счета',
            'url' => ['/reports/invoice-report/linked'],
            'active' => $controller == 'invoice-report' && $action == 'linked',
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
        ],
    ],
]);