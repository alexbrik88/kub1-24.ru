<?php

use \yii\bootstrap4\Nav;
use \yii\widgets\Menu;

?>

<?php $this->beginContent('@frontend/modules/reports/views/layouts/product_analysis.php'); ?>

<div class="product-way-report-content container-fluid" style="padding: 0; margin-top: -10px;">
   <?= Nav::widget([
        'id' => 'one-s-menu',
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            'style' => 'margin-bottom: 20px;'
        ],
        'items' => [
            [
                'label' => 'В пути на склад',
                'url' => ['product-analysis/way-in'],
            ],
            [
                'label' => 'В пути клиентам',
                'url' => ['product-analysis/way-out'],
            ],
        ],
    ]);
    ?>
    <?= $content; ?>
</div>

<?php $this->endContent(); ?>
