<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\Nav;

$controller =  Yii::$app->controller->id;
$action =  Yii::$app->controller->action->id;

echo Nav::widget([
    'id' => 'debt-report-menu',
    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
    'items' => [
        [
            'label' => 'Должники',
            'url' => ['/reports/debt-report/debtor'],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'active' => $controller == 'debt-report' && $action == 'debtor'
        ],
        [
            'label' => 'ABC анализ',
            'url' => ['/reports/analysis/index'],
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'active' => $controller == 'analysis' && $action == 'index'
        ],
        [
            'label' => 'Платежная дисциплина',
            'url' => ['/reports/discipline/index',],
            'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_MANAGER),
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'active' => $controller == 'discipline' && $action == 'index'
        ],
        [
            'label' => 'По месяцам',
            'url' => ['/reports/debt-report/debtor-month'],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'active' => $controller == 'debt-report' && $action == 'debtor-month'
        ],
        [
            'label' => 'Новые клиенты',
            'url' => ['/reports/debt-report/new-clients'],
            'options' => ['class' => 'nav-item'],
            'linkOptions' => ['class' => 'nav-link'],
            'active' => $controller == 'debt-report' && $action == 'new-clients'
        ],
    ],
]);