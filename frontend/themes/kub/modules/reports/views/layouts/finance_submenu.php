<?php

use frontend\rbac\permissions;

$controller =  Yii::$app->controller->id;
$action =  Yii::$app->controller->action->id;
?>
<div class="nav-tabs-row mb-2 pb-1">
    <?php

    use frontend\rbac\UserRole;
    use yii\bootstrap4\Nav;

    echo Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'По клиентам',
                'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ? ['/reports/analysis/index'] : ['/reports/debt-report/debtor'],
                'active' => in_array($controller, ['debt-report', 'analysis', 'discipline']),
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
                'visible' => Yii::$app->user->can(permissions\Reports::REPORTS_CLIENTS),
            ],
            [
                'label' => 'По продажам',
                'url' => ['/reports/selling-report/index'],
                'active' => $controller == 'selling-report',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
                'visible' => Yii::$app->user->can(permissions\Reports::REPORTS_SALES),
            ],
            [
                'label' => 'По счетам',
                'url' => ['/reports/invoice-report/created'],
                'active' => $controller == 'invoice-report',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
                'visible' => Yii::$app->user->can(permissions\Reports::REPORTS_INVOICES),
            ],
            [
                'label' => 'По поставщикам',
                'url' => ['/reports/debt-report-seller/debtor'],
                'active' => $controller == 'debt-report-seller',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
                'visible' => Yii::$app->user->can(permissions\Reports::REPORTS_SUPPLIERS),
            ],
            [
                'label' => 'По сотрудникам',
                'url' => ['/reports/employees/index'],
                'active' => $controller == 'employees',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
                'visible' => Yii::$app->user->can(permissions\Reports::REPORTS_EMPLOYEES),
            ],
        ],
    ]);
    ?>
</div>