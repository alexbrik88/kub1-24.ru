<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2020
 * Time: 20:50
 */

use yii\bootstrap\Nav;

/* @var $content string */

$this->params['is_marketing_module'] = true;

$this->beginContent('@frontend/views/layouts/main.php');
?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2 pb-1">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Дашборд',
                    'url' => ['/reports/marketing/index'],
                    'active' => Yii::$app->controller->id === 'marketing' && Yii::$app->controller->action->id === 'index',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Яндекс.Директ',
                    'url' => ['/marketing/reports/yandex-direct'],
                    'active' => Yii::$app->controller->id === 'yandex-direct',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Google.Ads',
                    'url' => ['#'], // /marketing/reports/google-words
                    'active' => Yii::$app->controller->id === 'google-words',
                    'options' => ['class' => 'nav-item disabled'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Вконтакте',
                    'url' => ['#'], // /marketing/reports/vk-ads
                    'active' => Yii::$app->controller->id === 'vk-ads',
                    'options' => ['class' => 'nav-item disabled'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Facebook',
                    'url' => '#',
                    'active' => false,
                    'options' => ['class' => 'nav-item disabled'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Настройки',
                    'url' => ['/reports/marketing/upload-data'],
                    'active' => Yii::$app->controller->id === 'marketing'
                        && in_array(Yii::$app->controller->action->id, ['upload-data', 'notifications']),
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]) ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>
