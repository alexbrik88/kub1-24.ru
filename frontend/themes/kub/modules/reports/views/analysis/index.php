<?php

use common\components\grid\GridView;
use frontend\modules\reports\models\AnalysisSearch;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $searchModel \frontend\modules\reports\models\AnalysisSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'ABC анализ покупателей';

$this->registerJs('
    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#analysissearch-id").val("");
        $("#analysissearch-group--filter").val($(this).data("group"));
        $("#abc-analysis-grid").yiiGridView("applyFilter");
    });
    $(document).on("pjax:success", "#abc-analysis-pjax", function() {
		$(".custom-scroll-table").mCustomScrollbar({
			horizontalScroll: true,
			axis:"x",
			scrollInertia: 300,
			advanced:{
				autoExpandHorizontalScroll: true,
				updateOnContentResize: true,
				updateOnImageLoad: false
			},
			mouseWheel:{ enable: false },
		});
    });
');

$series = [];

$colorArray = [
    AnalysisSearch::GROUP_A => '#00c19b',
    AnalysisSearch::GROUP_B => '#e30611',
    AnalysisSearch::GROUP_C => '#fac031',
];

foreach ($searchModel->overallResult as $row) {
    $item = [
        'name' => $searchModel->getGroupLabel($row['group']),
        'color' => $colorArray[$row['group']],
        'data' => [
            round($row['contractor_count_part'], 2),
            round($row['group_sum_part'], 2),
        ],
    ];
    array_unshift($series, $item);
}

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');
?>

<style type="text/css">
.overal-result-table .result-row { cursor: pointer; }
.overal-result-table .result-row:hover td { background-color: #f1f1f1; }
</style>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/_by_clients_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <?= RangeButtonWidget::widget(['pjaxSelector' => '#abc-analysis-pjax']); ?>
            </div>
        </div>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'abc-analysis-pjax',
    'timeout' => 10000,
]); ?>

<div class="wrap">
    <div class="row">
        <div class="col-8">
            <table class="table table-style table-count-list overal-result-table table-hover compact-disallow" style="width: auto;">
                <thead>
                <tr class="heading">
                    <th>Группа</th>
                    <th style="text-align: center;">Сумма</th>
                    <th style="text-align: center;">% суммы</th>
                    <th style="text-align: center;">Кол-во покупателей</th>
                    <th style="text-align: center;">% покупателей</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($searchModel->overallResult as $row) : ?>
                    <tr class="result-row" data-group="<?= $row['group'] ?>">
                        <td style="color: <?= $colorArray[$row['group']] ?>;">
                            <?= $searchModel->getGroupLabel($row['group']) ?>
                        </td>
                        <td style="text-align: right;"><?= number_format($row['group_sum'] / 100, 2, ',', '&nbsp;') ?></td>
                        <td style="text-align: right; font-weight: bold; color: <?= $colorArray[$row['group']] ?>;">
                            <?= round($row['group_sum_part'], 2) ?>
                        </td>
                        <td style="text-align: right;"><?= $row['contractor_count'] ?></td>
                        <td style="text-align: right; font-weight: bold; color: <?= $colorArray[$row['group']] ?>;">
                            <?= round($row['contractor_count_part'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr class="result-row" data-group="" style="font-weight: bold;">
                    <td>Итого</td>
                    <td style="text-align: right;"><?= number_format($searchModel->totalSum / 100, 2, ',', ' ') ?></td>
                    <td style="text-align: right;">100</td>
                    <td style="text-align: right;"><?= $searchModel->tmpRowCount ?></td>
                    <td style="text-align: right;">100</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-4">
            <?php if ($series) : ?>
                <div class="row">
                    <div class="col-12">
                    <?= \miloschuman\highcharts\Highcharts::widget([
                        'htmlOptions' => [
                            'style' => 'height: 180px; padding: 0; margin-top: 22px; margin-bottom: -22px;',
                        ],
                        'options' => [
                            'chart' =>['type' => 'column'],
                            'title' => false,
                            'xAxis' => [
                                'categories' => ['Покупатели', 'Выручка'],
                            ],
                            'yAxis' => [
                                'title' => false,
                            ],
                            'series' => $series,
                            'plotOptions' => [
                                'column' => [
                                    'stacking' => 'percent',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'style' => [
                                            'color' => '#333333',
                                            'textOutline' => null
                                        ],
                                    ],
                                ],
                                'series' => [
                                    'showInLegend' => false,
                                    'borderWidth' => 0,
                                ],
                            ],
                            /////////////////////////////////// STD OPTS ////////////////////////////////////////////////
                            'credits' => [
                                'enabled' => false
                            ],
                            'tooltip' => [
                                'useHtml' => true,
                                //'split' => true,
                                //'shared' => true,
                                'backgroundColor' => "rgba(255,255,255,1)",
                                'borderColor' => '#ddd',
                                'borderWidth' => '1',
                                'borderRadius' => 8,
                                'pointFormat' => '<span class="gray-text">{series.name}: </span><span class="gray-text-b">{point.y} ₽</span>',
                            ],
                            'legend' => [
                                'layout' => 'horizontal',
                                'align' => 'right',
                                'verticalAlign' => 'top',
                                'backgroundColor' => '#fff',
                                'itemStyle' => [
                                    'fontSize' => '11px',
                                    'color' => '#9198a0'
                                ],
                                'symbolRadius' => 2
                            ]
                            //////////////////////////////////////////////////////////////////////////////////////////
                        ]
                    ]); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">Покупатели: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'id' => 'abc-analysis-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'id',
            'headerOptions' => [
                'width' => '25%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'format' => 'html',
            'filter' => $searchModel->getContractorFilterItems(),
            'value' => function ($data) {
                return Html::a($data->nameWithType, [
                    '/contractor/view',
                    'type' => $data->type,
                    'id' => $data->id,
                ], [
                    'title' => html_entity_decode($data->nameWithType)
                ]);
            },
            's2width' => '250px'
        ],
        [
            'attribute' => 'paid_sum',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'format' => 'html',
            'value' => function ($data) {
                return number_format($data['paid_sum'] / 100, 2, ',', '&nbsp;');
            }
        ],
        [
            'attribute' => 'part',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'value' => function ($data) {
                return round($data['part'], 2);
            }
        ],
        [
            'attribute' => 'group',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'filter' => ['' => 'Все'] + AnalysisSearch::$groups,
            'value' => 'groupValue',
            's2width' => '250px'
        ],
        [
            'attribute' => 'paid_count',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
        ],
        [
            'attribute' => 'average',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'format' => 'html',
            'value' => function ($data) {
                return number_format($data['average'] / 100, 2, ',', '&nbsp;');
            }
        ],
        [
            'attribute' => 'first',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'format' => 'html',
            'value' => function ($data) {
                return date('d.m.Y', $data['first']);
            }
        ],
        [
            'attribute' => 'last',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-right',
            ],
            'format' => 'html',
            'value' => function ($data) {
                return date('d.m.Y', $data['last']);
            }
        ],
    ],
]); ?>

<?php $pjax->end(); ?>
