<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $container string */
/* @var $categories array */
/* @var $yTitle string */
/* @var $format string */
/* @var $series array */

?>

<div class="param-chart-wrapper">
    <?= Highcharts::widget([
        'id' => $container,
        'options' => [
            'credits' => ['enabled' => false],
            'chart' => ['type' => 'column'],
            'title' => ['text' => null],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'endOnTick' => false,
                'title' => [
                    'text' => $yTitle,
                    'useHTML' => true,
                ]
            ],
            'tooltip' => [
                'headerFormat' => '<div style="text-align: center; font-size:10px">{point.key}</div>',
                'pointFormat' => $format,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'useHTML' => true,
            ],
            'series' => $series,
            'legend' => ['enabled' => false],
            'plotOptions' => [
                'column' => [
                    'pointPadding' => 0,
                    'groupPadding' => 0.1,
                ]
            ]
        ],
        'htmlOptions' => [
            'style' => 'width: 630px; height: 180px;'
        ]
    ]); ?>
</div>

