<?php
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Отчет по продажам';

$showHelpPanel = false;
$showChartPanel = false;
?>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/_by_selling_report_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap4\Html::button(\frontend\themes\kub\helpers\Icon::get('diagram'),
                    [
                        'class' => 'button-regular button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'disabled' => true
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= \yii\bootstrap\Html::button(Icon::get('book'),
                    [
                        'class' => 'button-regular button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'disabled' => true
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0" style="max-height: 44px">
                <?= Html::beginForm(Url::current(), 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'name' => 'SellingReportSearch[year]',
                    'data' => [date('Y')],
                    'disabled' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap mt-3 text-center">
    <div>
        <?= Icon::get('block', [
            'style' => 'font-size: 100px;',
        ]) ?>
    </div>
    <div class="mt-5">
        Данный отчет доступен только
        в блоке ФинДиректор
    </div>
    <div class="mt-5">
        <?= Html::a('Перейти в ФинДиректор', [
            '/analytics/options/start',
        ], [
            'class' => 'button-regular button-hover-transparent px-3',
        ]) ?>
    </div>
</div>