<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.11.2017
 * Time: 9:32
 */

use frontend\modules\reports\models\SellingReportSearch;
use yii\bootstrap\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Json;
use miloschuman\highcharts\HighchartsAsset;
use common\components\TextHelper;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var SellingReportSearch $searchModel */
/* @var $chartsOptions [] */
/* @var $tableData [] */
/* @var $totalDateData [] */

$this->title = 'Продажи по выставленным счетам';

$subPeriodColumns = $searchModel->getSubPeriodColumns();
$chartsOptions['legend'] = array_merge($chartsOptions['legend'], [
    'layout' => 'horizontal',
    'verticalAlign' => 'bottom',
    'backgroundColor' => '#fff',
    'itemStyle' => [
        'fontSize' => '12px',
        'color' => '#9198a0'
    ],
    'symbolRadius' => 2
]);
$chartsOptions['tooltip'] = array_merge($chartsOptions['tooltip'], [
    'useHtml' => true,
    'backgroundColor' => "rgba(255,255,255,1)",
    'borderColor' => '#ddd',
    'borderWidth' => '1',
    'borderRadius' => 8,
]);
$chartsOptions['credits'] = ['enabled' => false];

$highchartsOptions = Json::encode($chartsOptions);

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>

<style type="text/css">
#selling-report-table tr th {
    vertical-align: middle;
}
#selling-report-table tr td {
    padding: 5px;
}
#selling-report-table tr.total-row td {
    font-weight: bold;
}
#selling-report-table tr > .count,
#selling-report-table tr > .sum,
#selling-report-table tr > .percent {
    padding: 5px;
    padding-right: 0;
    text-align: right;
    font-size: 13px;
    line-height: 15px;
    font-weight: normal;
}
#selling-report-table .count,
#selling-report-table .sum {
    border-right-width: 0;
}
#selling-report-table tr > th.percent {
    text-align: left;
    width: 2%;
}
#selling-report-table tr > td.percent {
    padding-left: 0;
}
#selling-report-table td > div.percent {
    position: relative;
    width: 52px;
    margin: 0;
    margin-left: -2px;
    padding-right: 10px;
    text-align: right;
}
#selling-report-table .percent i {
    position: absolute;
    top: 0;
    right: 2px;
}
#selling-report-table .sum * {
    background-color: transparent;
}
#selling-report-table .sum table {
    width: auto;
    margin-right: 0px;
    margin-left: auto;
}
#selling-report-table .sum table td {
    padding: 0 5px;
    border-width: 0;
    border-right: 1px solid #333333;
}
.selling-report-list .fa-caret-down {
    color: red;
}
.selling-report-list .fa-caret-up {
    color: green;
}
</style>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/_by_selling_report_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
                <span class="dropdown">
                    <?= \yii\helpers\Html::a($searchModel->periodName, '#', [
                        'class' => 'button-regular button-hover-transparent w-100',
                        'data-toggle' => 'dropdown',
                    ]); ?>
                    <?= \yii\bootstrap4\Dropdown::widget([
                        'items' => $searchModel->periodItems,
                        'options' => [
                            'class' => 'form-filter-list list-clr '
                        ],
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
</div>

<?php Pjax::begin([
    'id' => 'selling-pjax',
    'linkSelector' => '.selling-pjax-link',
    'timeout' => 10000,
]); ?>
<div class="wrap  mb-12px">
    <div class="row">
        <div class="col-12" style="padding:5px;">
            <div id="high-charts-container" style="min-height: 400px;"></div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="column ml-auto mb-2">
            <?= Html::radioList('type', $searchModel->type, [
                null => 'Все',
                SellingReportSearch::TYPE_PAID => 'Оплаченные',
                SellingReportSearch::TYPE_NOT_PAID => 'Неоплаченные',
            ], [
                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                    return Html::a(
                        Html::radio($name, $checked, [
                            'value' => $value,
                            'label' => $label,
                            'labelOptions' => [
                                'class' => 'radio-inline',
                            ],
                        ]),
                        [
                            'index', 'period' => $searchModel->period, 'type' => $value,
                        ],
                        [
                            'class' => 'selling-pjax-link ml-3',
                            'style' => 'text-underline: none; color: #001424; vertical-align: baseline;',
                        ]
                    );
                },
            ]); ?>
        </div>
    </div>
    <table id="selling-report-table" class="table table-style table-count-list selling-report-list">
            <thead>
            <tr>
                <?php foreach ($searchModel->tableColumns as $column) : ?>
                    <?php $align = $column['attribute'] == 'expenses' ? '' : ' text-align: center;'; ?>
                    <?= Html::tag('th', $column['label'], [
                        'style' => 'padding: 5px 5px 0;' . $align,
                        'colspan' => $column['attribute'] == 'expenses' ? null : 2,
                        'rowspan' => $column['attribute'] == 'expenses' ? 2 : null,
                    ]); ?>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach ($searchModel->tableColumns as $column) : ?>
                    <?php if ($column['attribute'] != 'expenses') : ?>
                        <th class="sum">
                            <table>
                                <tr>
                                    <td>Кол-во</td>
                                    <td>Сумма</td>
                                </tr>
                            </table>
                        </th>
                        <th class="percent">%%</th>
                    <?php endif ?>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tableData as $oneLine): ?>
                <tr data-id="<?= $oneLine['product_id']; ?>">
                    <td>
                        <?= $oneLine['product_title']; ?>
                    </td>
                    <?php foreach ($subPeriodColumns as $period): ?>
                        <?php if (isset($oneLine['data'][$period['attribute']])) : ?>
                            <?php
                            $count = $oneLine['data'][$period['attribute']]['count'];
                            $amount = $oneLine['data'][$period['attribute']]['amount'];
                            $percent = $oneLine['data'][$period['attribute']]['percent'];
                            $arrow_status = $oneLine['data'][$period['attribute']]['arrow_status'];
                            ?>
                            <td class="sum">
                                <table>
                                    <tr>
                                        <td><?= $count; ?></td>
                                        <td><?= number_format($amount/100, 2, ',', '&nbsp;') ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="percent">
                                <div class="percent">
                                    <?= $percent ?>
                                    <?php if ($arrow_status !== null) : ?>
                                        <?php if ($arrow_status) : ?>
                                            <i class="fa fa-caret-up"></i>
                                        <?php else : ?>
                                            <i class="fa fa-caret-down"></i>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="sum">
                        <table>
                            <tr>
                                <td><?= $oneLine['totalCount']; ?></td>
                                <td><?= number_format($oneLine['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                            </tr>
                        </table>
                    </td>
                    <td class="percent">
                        <div class="percent">
                            <?php $total = $totalDateData['totalAmount'] ? $totalDateData['totalAmount'] : 1; ?>
                            <?= round($oneLine['totalAmount'] / $total * 100, 2); ?>
                            <?php if ($oneLine['arrow_status'] !== null) : ?>
                                <?php if ($oneLine['arrow_status']) : ?>
                                    <i class="fa fa-caret-up"></i>
                                <?php else : ?>
                                    <i class="fa fa-caret-down"></i>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr class="total-row">
                <td>Итого</td>
                <?php foreach ($subPeriodColumns as $period): ?>
                    <?php if (isset($totalDateData[$period['attribute']])): ?>
                        <td class="sum">
                            <table>
                                <tr>
                                    <td><?= $totalDateData[$period['attribute']]['totalCount'] ?></td>
                                    <td><?= number_format($totalDateData[$period['attribute']]['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                                </tr>
                            </table>
                        </td>
                        <td class="percent">
                            <div class="percent">
                                100,00
                            </div>
                        </td>
                    <?php else: ?>
                        <td class="sum">
                            <table>
                                <tr>
                                    <td>0</td>
                                    <td>0,00</td>
                                </tr>
                            </table>
                        </td>
                        <td class="percent"><div class="percent">0,00</div></td>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php if (!empty($totalDateData)): ?>
                    <td class="sum">
                        <table>
                            <tr>
                                <td><?= $totalDateData['totalCount'] ?></td>
                                <td><?= number_format($totalDateData['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                            </tr>
                        </table>
                    </td>
                    <td class="percent">
                        <div class="percent">
                            100,00
                        </div>
                    </td>
                <?php else: ?>
                    <td class="sum">
                        <table>
                            <tr>
                                <td>0</td>
                                <td>0,00</td>
                            </tr>
                        </table>
                    </td>
                    <td class="percent"><div class="percent">0,00</div></td>
                <?php endif; ?>
            </tr>
            </tbody>
    </table>
</div>

<script type="text/javascript">
    window.highchartsOptions = <?= $highchartsOptions ?>;
</script>
<?php Pjax::end(); ?>
<?php
$js = <<<JS
function createHighcharts() {
    Highcharts.chart('high-charts-container', window.highchartsOptions);
}

$(document).on("pjax:complete", function() {
    createHighcharts();
});

createHighcharts();
JS;

$this->registerJs($js);

