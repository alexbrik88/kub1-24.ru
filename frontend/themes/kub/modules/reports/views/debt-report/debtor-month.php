<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.01.2017
 * Time: 16:13
 */

use common\components\grid\GridView;
use common\models\Contractor;
use frontend\widgets\TableViewWidget;
use yii\bootstrap\Html;
use common\components\TextHelper;
use frontend\modules\reports\components\DebtsHelper;
use frontend\modules\reports\models\DebtReportSearch;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\reports\models\DebtReportSearch */
$this->title = 'Отчет по месяцам';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');
?>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/finance_submenu') ?>
<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/_by_clients_submenu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="col-9">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="col-3 pr-0">
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row">
        <div class="col-12">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Сумма']
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'color' => '#dfba49',
                            'name' => 'Выставленно',
                            'pointWidth' => 17,
                            'data' => [
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '01', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '02', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '03', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '04', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '05', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '06', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '07', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '08', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '09', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '10', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '11', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '12', $searchModel->year)),
                            ],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Оплачено',
                            'color' => '#45b6af',
                            'pointWidth' => 17,
                            'data' => [
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '01', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '02', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '03', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '04', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '05', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '06', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '07', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '08', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '09', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '10', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '11', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '12', $searchModel->year)),
                            ],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Не оплачено',
                            'color' => '#f3565d',
                            'pointWidth' => 17,
                            'data' => [
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '01', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '02', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '03', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '04', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '05', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '06', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '07', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '08', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '09', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '10', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '11', $searchModel->year)),
                                (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '12', $searchModel->year)),
                            ],
                        ],
                    ],
                    /////////////////////////////////// STD OPTS ////////////////////////////////////////////////
                    'credits' => [
                        'enabled' => false
                    ],
                    'tooltip' => [
                        'useHtml' => true,
                        //'split' => true,
                        //'shared' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'pointFormat' => '<span class="gray-text">{series.name}: </span><span class="gray-text-b">{point.y} ₽</span>',
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ]
                    //////////////////////////////////////////////////////////////////////////////////////////
                ]
            ]); ?>
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::begin([
    'timeout' => 10000,
]); ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <h4 class="caption mt-1">По покупателям: <?= $dataProvider->totalCount ?></h4>
            </div>
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'validateOnChange' => true,
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="column ml-auto p-0">
            <?= \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'debtType',
                'data' => $searchModel->debtItems,
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                ]
            ]); ?>
        </div>
        <div class="column ml-2 p-0">
            <?= \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'year',
                'data' => $searchModel->getYearFilter(),
                'value' => $searchModel->year ? $searchModel->year : date('Y'),
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                ]
            ]); ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'name',
            'label' => 'Название',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '20%',
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'filter' => $searchModel->getContractorFilter(),
            'value' => function (Contractor $model) {
                return Html::a($model->nameWithType, [
                    '/contractor/view',
                    'id' => $model->id,
                    'type' => $model->type
                ], [
                    'title' => html_entity_decode($model->nameWithType)
                ]);
            },
            's2width' => '300px'
        ],
        [
            'attribute' => 'debt_january_sum',
            'label' => 'янв.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_january_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_february_sum',
            'label' => 'февр.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_february_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_march_sum',
            'label' => 'март',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_march_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_april_sum',
            'label' => 'апр.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_april_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_may_sum',
            'label' => 'май',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_may_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_june_sum',
            'label' => 'июнь',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_june_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_jule_sum',
            'label' => 'июль',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_jule_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_august_sum',
            'label' => 'авг.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_august_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_september_sum',
            'label' => 'сент.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_september_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_october_sum',
            'label' => 'окт.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_october_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_november_sum',
            'label' => 'нояб.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_november_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_december_sum',
            'label' => 'дек.',
            'format' => 'raw',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_december_sum, 2);
            },
        ],
        [
            'attribute' => 'debt_all_sum',
            'label' => 'Итого',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (Contractor $model) {
                return TextHelper::invoiceMoneyFormat($model->debt_all_sum, 2);
            },
        ],
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>
