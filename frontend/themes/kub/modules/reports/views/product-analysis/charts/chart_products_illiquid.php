<?php

///////////////// colors /////////////////
$color1 = 'rgba(103,131,228,1)';
$color1_opacity = 'rgba(103,131,228,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////
///
use kartik\select2\Select2;
use yii\bootstrap\Html; ?>

<style>
    #chart-profit-loss-projects { height: auto; }
</style>

<div style="position: relative">
    <div style="width: 100%;">

        <div class="ht-caption noselect">
            ТОП НЕЛИКВИД
        </div>

        <div style="position: absolute; right:0; top:-14px;">
            <?= Html::beginForm(['dashboard'], 'GET', [
                'validateOnChange' => true,
            ]); ?>
            <?= Select2::widget([
                'model' => $searchModel,
                'attribute' => 'yearMonth',
                'data' => $searchModel->getYearFilter(),
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '170px',
                    'templateResult' => new \yii\web\JsExpression('function(el){ return ( el.text.match(/^(\d{4})$/) ) ? $("<strong>Итого " + el.text + "</strong>") : el.text; }'),
                    'templateSelection' => new \yii\web\JsExpression('function(el){ return ( el.text.match(/^(\d{4})$/) ) ? $("<strong>Итого " + el.text + "</strong>") : el.text; }'),
                ],
            ]); ?>
            <?= Html::endForm(); ?>
        </div>

        <div class="clearfix"></div>

        <div class="finance-charts-group" style="display: flex; min-height:233px; align-items: center; justify-content: center; opacity: .25; text-align: center">
            <div style="margin-top:-30px"> Отчет <br/>в разработке </div>
        </div>
    </div>
</div>
