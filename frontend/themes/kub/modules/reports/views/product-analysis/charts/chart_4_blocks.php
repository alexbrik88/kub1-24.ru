<?php
use common\components\TextHelper;
use common\models\product\Product;
use \common\models\product\ProductSearch;
use frontend\modules\reports\models\AbstractFinance;

$sells = $margin = $buys = $balance = [];

/** @var $model ProductSearch */
$model = new ProductSearch([
    'company_id' => Yii::$app->user->identity->company->id,
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
    'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT,
]);

$dateStart = (new DateTime())->setTime(0,0,0);
$dateEnd = (new DateTime())->setTime(0, 0, 0);
$model->setDateStart($dateStart);
$model->setDateEnd($dateEnd);
$sells['today'] = $model->turnAtPeriodSellPrice(false, null, false) / 100;
$buys['today'] = $model->turnAtPeriodPurchasePrice(true, null, false) / 100;
$margin['today'] = $model->marginAtPeriod(null, false) / 100;
$balance['today'] = $model->balanceAtDate(false, null, false) / 100;

$dateStart = $dateStart->modify("-1 day");
$dateEnd = $dateEnd->modify("-1 day");
$model->setDateStart($dateStart);
$model->setDateEnd($dateEnd);
$sells['yesterday'] = $model->turnAtPeriodSellPrice(false, null, false) / 100;
$buys['yesterday'] = $model->turnAtPeriodPurchasePrice(true, null, false) / 100;
$margin['yesterday'] = $model->marginAtPeriod(null, false) / 100;
$balance['yesterday'] = $model->balanceAtDate(false, null, false) / 100;

$dateStart = $dateStart->modify("first day of");
$dateEnd = $dateEnd->modify("last day of");
$model->setDateStart($dateStart);
$model->setDateEnd($dateEnd);
$sells['month'] = $model->turnAtPeriodSellPrice(false, null, false) / 100;
$buys['month'] = $model->turnAtPeriodPurchasePrice(true, null, false) / 100;
$margin['month'] = $model->marginAtPeriod(null, false) / 100;
$balance['month'] = $model->balanceAtDate(false, null, false) / 100;

$dateStart = $dateStart->modify("-1 second")->modify("first day of");
$dateEnd = (clone $dateStart)->modify("last day of");
$model->setDateStart($dateStart);
$model->setDateEnd($dateEnd);
$sells['prev_month'] = $model->turnAtPeriodSellPrice(false, null, false) / 100;
$buys['prev_month'] = $model->turnAtPeriodPurchasePrice(true, null, false) / 100;
$margin['prev_month'] = $model->marginAtPeriod(null, false) / 100;
$balance['prev_month'] = $model->balanceAtDate(false, null, false) / 100;

?>

<div class="row">

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Продано
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($sells['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($sells['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($sells['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($sells['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Маржа
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($margin['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($margin['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($margin['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($margin['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Закуплено
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($buys['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($buys['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($buys['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($buys['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-3" style="padding:0 5px;">
        <div class="wrap">
            <div class="ht-caption">
                Остаток
            </div>
            <table class="ht-table dashboard-cash">
                <tbody>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label">Сегодня</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($balance['today'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey">Вчера</span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($balance['yesterday'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label"><?= AbstractFinance::$month[date('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap"><?= TextHelper::moneyFormat($balance['month'], 2) ?> ₽</span></td>
                </tr>
                <tr class="single-string">
                    <td><span class="ht-empty-wrap flow-label text-grey"><?= AbstractFinance::$month[$dateStart->format('m')] ?></span></td>
                    <td class="nowrap"><span class="ht-empty-wrap text-grey"><?= TextHelper::moneyFormat($balance['prev_month'], 2) ?> ₽</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
