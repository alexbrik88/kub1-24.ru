<?php

use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\themes\kub\components\Icon;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use frontend\modules\reports\models\AbstractFinance;
use \frontend\modules\reports\models\productAnalysis\ProductAnalysisCharts;
use common\models\product\ProductGroup;

///////////////// dynamic vars ///////////
$customOffset = $customOffset ?? 0;
$customPeriod = $customPeriod ?? "months";
$customGroup = $customGroup ?? null;
/////////////////////////////////////////

/* @var $model ProductAnalysisCharts */
$model = new ProductAnalysisCharts();

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color2 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2_opacity = 'rgba(243,183,46,.5)';

$color3 = 'rgba(46,159,191,1)';
$color3_opacity = 'rgba(46,159,191,.5)';
$color4 = 'rgba(57,194,176,1)';
$color4_opacity = 'rgba(57,194,176,.95)';
$color4_negative = 'red';
//////////////////////////////////////////

/////////////// consts //////////////////
$LEFT_DATE_OFFSET = 10;
$RIGHT_DATE_OFFSET = 10;
$MOVE_OFFSET = 1;
$CENTER_DATE = date('Y-m-d');
////////////////////////////////////////

if ($customOffset + $LEFT_DATE_OFFSET < 0)
    $currDayPos = -1;
elseif ($customOffset - $RIGHT_DATE_OFFSET > 0)
    $currDayPos = 9999;

$wrapPointPos = [];

if ($customPeriod == "months") {

    $datePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentMonthsPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartZZZLabelsX = [];
    $chartFreeDays = [];
    $yearPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($yearPrev != $year && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => $year - 1,
                'next' => $year
            ];
            // emulate align right
            switch ($year - 1) {
                case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                    $wrapPointPos[$i]['leftMargin'] = '-30%'; break;
                default:
                    $wrapPointPos[$i]['leftMargin'] = '-28%'; break;
            }
        }

        $yearPrev = $year;

        $daysPeriods[] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
        $chartZZZLabelsX[] = Month::$monthFullRU[$month] . ' ' . $year;
        if ($month == (int)date('m') && $year == (int)date('Y'))
            $currDayPos = $i;
    }
}

elseif ($customPeriod == "days") {

    $datePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, 0, $customOffset);
    $prevYearDatePeriods = $model->getFromCurrentDaysPeriods($LEFT_DATE_OFFSET, $RIGHT_DATE_OFFSET, -1, $customOffset);

    $daysPeriods = [];
    $chartZZZLabelsX = [];
    $chartFreeDays = [];
    $monthPrev = $datePeriods[0];
    foreach ($datePeriods as $i => $date) {
        $dateArr = explode('-', $date['from']);
        $year  = (int)$dateArr[0];
        $month = (int)$dateArr[1];
        $day   = (int)$dateArr[2];

        if ($monthPrev != $month && ($i > 1 && $i < ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET))) {
            $wrapPointPos[$i] = [
                'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                'next' => Month::$monthFullRU[$month]
            ];
            // emulate align right
            switch ($month) {
                case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
            }
        }

        $monthPrev = $month;
        
        $daysPeriods[] = $day;
        $chartFreeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
        $chartZZZLabelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);
        if ($CENTER_DATE == $date['from'])
            $currDayPos = $i;
    }
}

else {

    die('Unknown period');

}

$mainData = $model->getTurnoverSeriesData($datePeriods, $customGroup, $customPeriod);
$prevMainData = $model->getTurnoverSeriesData($prevYearDatePeriods, $customGroup, $customPeriod);
$outcomeFact = &$mainData['outcomeFact'];
$incomeFact = &$mainData['incomeFact'];
$balanceFact = &$mainData['balanceFact'];
$balanceFactPrevYear = &$prevMainData['balanceFact'];

$revenueFact = &$mainData['outcomeFact'];
$marginFact = &$mainData['marginFact'];

if ($currDayPos >= 0 && $currDayPos < 9999) {
    // chart1
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $incomeFact[$i] = 0;
        $outcomeFact[$i] = 0;
    }

    // chart2: connect plan/fact edges
    for ($i = $currDayPos + 1; $i < count($datePeriods); $i++) {
        $balanceFact[$i] = null;
    }
} elseif ($currDayPos == -1) {

}
elseif ($currDayPos === 9999) {
    $balanceFact[0] = 0; // fix highcharts bug
}


//////////////////////////////////// TOP CHARTS /////////////////////

// consts top charts
$chartWrapperHeight = 260;
$chartHeightHeader = 30;
$chartHeightFooter = 6;
$chartHeightColumn = 30;
$chartMaxRows = 20;
$cropNamesLength = 12;
$croppedNameLength = 10;
$structureData = $model->getTopProductSeriesData($searchModel->yearMonth, $customGroup, $customPeriod);

// arrays top charts
$productChart1 = [
    'categories' => array_slice(array_column($structureData['topRevenue'], 'name'), 0, $chartMaxRows),
    'factData' => array_slice(array_column($structureData['topRevenue'], 'revenue'), 0, $chartMaxRows),
    'totalAmount' => array_sum(array_column($structureData['topRevenue'], 'revenue')),
];

$productChart2 = [
    'categories' => array_slice(array_column($structureData['topMargin'], 'name'), 0, $chartMaxRows),
    'margin' => array_slice(array_column($structureData['topMargin'], 'margin'), 0, $chartMaxRows),
    'marginPercent' => array_slice(array_column($structureData['topMargin'], 'marginPercent'), 0, $chartMaxRows),
    'quantity' => array_slice(array_column($structureData['topMargin'], 'quantity'), 0, $chartMaxRows),
];

if (empty($productChart1['categories'])) {
    $productChart1 = [
        'categories' => ["нет данных"],
        'factData' => [0],
        'totalAmount' => 0,
    ];
}
if (empty($productChart2['categories'])) {
    $productChart2 = [
        'categories' => ["нет данных"],
        'margin' => [0],
        'marginPercent' => [0],
        'quantity' => [0],
    ];
}
$productChart1['calculatedChartHeight'] = count($productChart1['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$productChart2['calculatedChartHeight'] = count($productChart2['categories']) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
///////////////////////////////////////////////////////////////////////////

if (Yii::$app->request->post('chart-plan-fact-ajax')) {
    // RETURN ONLY CHART DATA
    echo json_encode([
        'currDayPos' => $currDayPos,
        'wrapPointPos' => $wrapPointPos,
        'freeDays' => $chartFreeDays,
        'labelsX' => $chartZZZLabelsX,
        'optionsChart' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $outcomeFact,
                ],
                [
                    'data' => $incomeFact,
                ],
            ],
        ],
        'optionsChart2' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $balanceFactPrevYear,
                ],
                [
                    'data' => $balanceFact,
                ],
            ],
        ],
        'optionsChart3' => [
            'xAxis' => [
                'categories' => $daysPeriods,
            ],
            'series' => [
                [
                    'data' => $revenueFact,
                ],
                [
                    'data' => $marginFact,
                ],
            ],
        ],
        'products1' => [
            'chartHeight' => $productChart1['calculatedChartHeight'],
            'totalAmount' => $productChart1['totalAmount'],
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $productChart1['categories'],
                ],
                'series' => [
                    [
                        'data' => $productChart1['factData'],
                    ]
                ],
            ],
        ],
        'products2' => [
            'chartHeight' => $productChart2['calculatedChartHeight'],
            'quantity' => $productChart2['quantity'],
            'marginPercent' => $productChart2['marginPercent'],
            'optionsChart' => [
                'xAxis' => [
                    'categories' => $productChart2['categories'],
                ],
                'series' => [
                    [
                        'data' => $productChart2['margin'],
                    ]
                ],
            ],
        ],
    ]);

    exit;
}

$selectGroups = ['' => 'Все'] + ProductGroup::find()->where(['or', ['company_id' => Yii::$app->user->identity->company->id], ['company_id' => null]])->indexBy('id')->select('title')->column();

?>
<style>
    #chart-plan-fact { height: 213px; }
    #chart-plan-fact-2 { height: 213px; }
    #chart-plan-fact-3 { height: 213px; }
    #chart-plan-fact  .highcharts-axis-labels,
    #chart-plan-fact-2 .highcharts-axis-labels,
    #chart-plan-fact-3 .highcharts-axis-labels { z-index: -1!important; }
    /* tooltip */
    #chart-plan-fact, #chart-plan-fact .highcharts-container , #chart-plan-fact svg {
        overflow: visible!important;
        z-index: 1!important;
    }
</style>

<div style="position: relative">
    <div style="width: 100%">

        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="left" style="position: absolute; left:40px; top:212px; z-index: 2">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer chart-1" data-move="right" style="position: absolute; right:0px; top:212px; z-index: 2">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; top:450px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:0px; top:450px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="left" style="position: absolute; left:40px; top:700px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(90deg);', []])?>
        </div>
        <div class="chart-plan-fact-days-arrow link cursor-pointer" data-move="right" style="position: absolute; right:0px; top:700px; z-index: 1">
            <?= Icon::get('shevron', ['style' => 'transform: rotate(270deg);', []])?>
        </div>

        <div class="ht-caption noselect mb-2">
            Продажи и Закупки
            <div class="wrap-select2-no-padding ml-1" style="float:right; margin-top:-13px;44">
                <?= \kartik\select2\Select2::widget([
                    'id' => 'chart-plan-fact-purse-type',
                    'name' => 'purseType',
                    'data' => $selectGroups,
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'display: inline-block;',
                    ],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '136px'
                    ]
                ]); ?>
            </div>
            <ul class="nav nav-tabs" role="tablist" style="border-bottom: none; float: right; margin-top:-1px;">
                <li class="nav-item pr-2">
                    <a class="tooltip2 chart-plan-fact-days-tab nav-link <?= $customPeriod == 'days' ? 'active':'' ?> pt-0 pb-0" href="javascript:void(0);" data-period="days" data-toggle="tab" data-tooltip-content='#tooltip_dev'>День</a>
                </li>
                <li class="nav-item pr-2">
                    <a class="chart-plan-fact-days-tab nav-link <?= $customPeriod == 'months' ? 'active':'' ?> pt-0 pb-0" href="javascript:void(0);" data-period="months" data-toggle="tab">Месяц</a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div style="display: none"><div id="tooltip_dev">В разработке</div></div>

        <div style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartZDays.redrawByLoad()')
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("function(args) { return window.ChartZDays.formatter.call(this, args); }"),
                        'positioner' => new jsExpression("function(boxWidth, boxHeight, point) { return window.ChartZDays.positioner.call(this, boxWidth, boxHeight, point); }"),
                        //'hideDelay' => 99999,
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() { 
                                        let result = (this.pos == window.ChartZDays.currDayPos) ? 
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                            (window.ChartZDays.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (window.ChartZDays.wrapPointPos) {
                                            result += window.ChartZDays.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Продажи',
                            'data' => $outcomeFact,
                            'color' => $color1,
                            'borderColor' => 'rgba(46,159,191,.3)',
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color1,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Закупки',
                            'data' => $incomeFact,
                            'color' => $color2,
                            'borderColor' => $color2_opacity,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color2,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ]
                            ]
                        ],
                        'series' => [
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'groupPadding' => 0.05,
                            'pointPadding' => 0.1,
                            'borderRadius' => 3,
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div class="ht-caption noselect mt-2">
            Остаток товара <span style="text-transform: none">в закупочных ценах</span>
        </div>
        <div style="min-height:125px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-2',
                'scripts' => [
                   // 'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],
                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' => null
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' =>
                            new jsExpression("
                                function(args) {
                                    let index = this.series.data.indexOf( this.point );
                                    let series_index = this.series.index;
                                    let factColor = args.chart.series[1].data[index].y >= 0 ? 'green-text' : 'red-text';
                                    let factColorB = args.chart.series[1].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                                    
                                    let toggleFactLines = args.chart.series[0].data[index].y > args.chart.series[1].data[index].y; 

                                    let titleLine = '<span class=\"title\">' + window.ChartZDays.labelsX[this.point.index] + '</span>';
                                    let factLine = '<tr>' + '<td class=\"' + factColor + '\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"' + factColorB + '\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';
                                    let prevYearLine = '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';

                                    if ((index > window.ChartZDays.currDayPos && window.ChartZDays.currDayPos != -1) || window.ChartZDays.currDayPos == 9999) {
                                        return titleLine + '<table class=\"indicators\">' + (prevYearLine) + '</table>';
                                    }
                                        
                                    return titleLine + '<table class=\"indicators\">' + (toggleFactLines ? (prevYearLine + factLine) : (factLine + prevYearLine)) + '</table>';
                                }
                            ")

                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        'min' => 1,
                        'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                        'categories' => $daysPeriods,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("
                                function() { 
                                    result = (this.pos == window.ChartZDays.currDayPos) ? 
                                        ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                        (window.ChartZDays.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
                                        
                                    if (window.ChartZDays.wrapPointPos) {
                                        result += window.ChartZDays.getWrapPointXLabel(this.pos);
                                    }                                        
                                         
                                    return result; 
                                }"),
                            'useHTML' => true,
                            'autoRotation' => false
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток товара (предыдущий год)',
                            'data' => $balanceFactPrevYear,
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 2
                        ],
                        [
                            'name' => 'Остаток товара',
                            'data' => $balanceFact,
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'dataLabels' => [
                                'enabled' => false
                            ],
                            'legendIndex' => 0
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                        'series' => [
                            'stickyTracking' => false,
                        ]
                    ],
                ],
            ]); ?>
        </div>

        <div class="ht-caption noselect mt-2">
            Динамика выручки и маржи
        </div>
        <div class="clearfix"></div>

        <div style="min-height:125px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-3',
                'scripts' => [
                    //'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill',
                ],

                'options' => [
                    'credits' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('window.ChartZDays.redrawByLoad()')
                        ],
                        'marginBottom' => '50',
                        'marginLeft' => '55',
                        'style' => [
                            'fontFamily' => '"Corpid E3 SCd", sans-serif',
                        ]
                        //'animation' => false
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'backgroundColor' => '#fff',
                        'itemStyle' => [
                            'fontSize' => '11px',
                            'color' => '#9198a0'
                        ],
                        'symbolRadius' => 2
                    ],
                    'tooltip' => [
                        'useHTML' => true,
                        'shared' => false,
                        'backgroundColor' => "rgba(255,255,255,1)",
                        'borderColor' => '#ddd',
                        'borderWidth' => '1',
                        'borderRadius' => 8,
                        'formatter' => new jsExpression("function(args) { return window.ChartZDays.formatter3.call(this, args); }"),
                        'positioner' => new jsExpression("function(boxWidth, boxHeight, point) { return window.ChartZDays.positioner.call(this, boxWidth, boxHeight, point); }"),
                        //'hideDelay' => 99999,
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        'min' => 0,
                        'index' => 0,
                        'title' => '',
                        'minorGridLineWidth' => 0,
                        'labels' => [
                            'useHTML' => true,
                            'style' => [
                                'fontWeight' => '300',
                                'fontSize' => '13px',
                                'whiteSpace' => 'nowrap'
                            ]
                        ]
                    ],
                    'xAxis' => [
                        [
                            'min' => 1,
                            'max' => ($LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1),
                            'categories' => $daysPeriods,
                            'labels' => [
                                'formatter' => new \yii\web\JsExpression("
                                    function() { 
                                        let result = (this.pos == window.ChartZDays.currDayPos) ? 
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') : 
                                            (window.ChartZDays.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (window.ChartZDays.wrapPointPos) {
                                            result += window.ChartZDays.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }"),
                                'useHTML' => true,
                                'autoRotation' => false
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'name' => 'Выручка',
                            'data' => $revenueFact,
                            'color' => $color3,
                            'borderColor' => 'rgba(46,159,191,.3)',
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color3,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                        [
                            'name' => 'Маржа',
                            'type' => 'line',
                            'data' => $marginFact,
                            'color' => $color4,
                            'negativeColor' => $color4_negative,
                            'states' => [
                                'hover' => [
                                    'color' => [
                                        'pattern' => [
                                            'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                            'color' => $color4,
                                            'width' => 5,
                                            'height' => 5
                                        ]
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'plotOptions' => [
                        'series' => [
                            'pointWidth' => 20,
                            'states' => [
                                'inactive' => [
                                    'opacity' => 1
                                ],
                            ],
                            'borderRadius' => 3,
                            'borderWidth' => 1
                        ]
                    ],
                ],
            ]); ?>
        </div>

    </div>
</div>

<div class="pt-4 pb-3" style="margin-top: 0px;">
    <div class="row">
        <div class="col-4 pr-2">
            <?= $this->render('chart_products_top', [
                // consts
                'chartWrapperHeight' => $chartWrapperHeight,
                'chartHeightHeader' => $chartHeightHeader,
                'chartHeightFooter' => $chartHeightFooter,
                'chartHeightColumn' => $chartHeightColumn,
                'chartMaxRows' => $chartMaxRows,
                'cropNamesLength' => $cropNamesLength,
                'croppedNameLength' => $croppedNameLength,
                // vars
                'categories' => $productChart1['categories'],
                'factData' => $productChart1['factData'],
                'calculatedChartHeight' => $productChart1['calculatedChartHeight'],
            ]); ?>
        </div>
        <div class="col-4 pl-2 pr-2">
            <?= $this->render('chart_products_margin', [
                // consts
                'chartWrapperHeight' => $chartWrapperHeight,
                'chartHeightHeader' => $chartHeightHeader,
                'chartHeightFooter' => $chartHeightFooter,
                'chartHeightColumn' => $chartHeightColumn,
                'chartMaxRows' => $chartMaxRows,
                'cropNamesLength' => $cropNamesLength,
                'croppedNameLength' => $croppedNameLength,
                // vars
                'categories' => $productChart2['categories'],
                'marginData' => $productChart2['margin'],
                'calculatedChartHeight' => $productChart2['calculatedChartHeight'],
            ]); ?>
        </div>
        <div class="col-4 pl-2">
            <?php
            // Expense Charts
            echo $this->render('chart_products_illiquid', [
                'searchModel' => $searchModel
            ]);
            ?>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function (legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });

    // MOVE CHART
    window.ChartZDays = {
        isLoaded: false,
        period: '<?= $customPeriod ?>',
        offset: {
            days: 0,
            months: 0
        },
        currDayPos: <?= (int)$currDayPos; ?>,
        wrapPointPos: <?= json_encode($wrapPointPos) ?>,
        labelsX: <?= json_encode($chartZZZLabelsX) ?>,
        freeDays: <?= json_encode($chartFreeDays) ?>,
        chartPoints: {},
        chartPoints2: {},
        chartPoints3: {},
        byGroup: null,
        _inProcess: false,
        Products1: {
            chartHeight: "<?= $productChart1['calculatedChartHeight'] ?>",
            totalAmount: "<?= $productChart1['totalAmount'] ?>",
            chartPoints: {}
        },
        Products2: {
            chartHeight: "<?= $productChart2['calculatedChartHeight'] ?>",
            quantity: <?= json_encode($productChart2['quantity']) ?>,
            marginPercent: <?= json_encode($productChart2['marginPercent']) ?>,
            chartPoints: {}
        },
        productsYearMonth: "<?= $searchModel->yearMonth ?>",
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            $('.chart-plan-fact-days-arrow').on('click', function() {

                // prevent double-click
                if (window.ChartZDays._inProcess) {
                    return false;
                }

                if ($(this).data('move') === 'left')
                    window.ChartZDays.offset[ChartZDays.period] -= <?= $MOVE_OFFSET ?>;
                if ($(this).data('move') === 'right')
                    window.ChartZDays.offset[ChartZDays.period] += <?= $MOVE_OFFSET ?>;

                window.ChartZDays.redrawByClick();
            });

            $('.chart-plan-fact-days-tab').on('show.bs.tab', function (e) {
                return false;
                //ChartZDays.period = $(this).data('period');
                //ChartZDays.redrawByClick();
            });

            $('#chart-plan-fact-purse-type').on('change', function() {
                window.ChartZDays.byGroup = $(this).val();
                window.ChartZDays.redrawByClick();
            });

            $('#productanalysischarts-yearmonth').on('change', function() {
                window.ChartZDays.productsYearMonth = $(this).val();
                window.ChartZDays.redrawTopProductsOnly();
            });
        },
        redrawByClick: function() {

            return window.ChartZDays._getData().done(function() {
                $('#chart-plan-fact-3').highcharts().update(ChartZDays.chartPoints3);
                $('#chart-plan-fact-2').highcharts().update(ChartZDays.chartPoints2);
                $('#chart-plan-fact').highcharts().update(ChartZDays.chartPoints);
                $('#chart-products-1').highcharts().update(ChartZDays.Products1.chartPoints);
                $('#chart-products-1').highcharts().update({"chart": {"height": ChartZDays.Products1.chartHeight + 'px'}});
                $('#chart-products-2').highcharts().update(ChartZDays.Products2.chartPoints);
                $('#chart-products-2').highcharts().update({"chart": {"height": ChartZDays.Products2.chartHeight + 'px'}});

                window.ChartZDays._inProcess = false;
            });
        },
        redrawTopProductsOnly: function() {

            return window.ChartZDays._getData().done(function() {
                $('#chart-products-1').highcharts().update(ChartZDays.Products1.chartPoints);
                $('#chart-products-1').highcharts().update({"chart": {"height": ChartZDays.Products1.chartHeight + 'px'}});
                $('#chart-products-2').highcharts().update(ChartZDays.Products2.chartPoints);
                $('#chart-products-2').highcharts().update({"chart": {"height": ChartZDays.Products2.chartHeight + 'px'}});

                window.ChartZDays._inProcess = false;
            });
        },
        redrawByLoad: function() {
            if (ChartZDays.isLoaded)
                return;

            let chartToLoad = window.setInterval(function () {
                let chart = $('#chart-plan-fact').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.clearInterval(chartToLoad);
                    window.ChartZDays.isLoaded = true;
                }
            }, 100);
        },
        _getData: function() {
            window.ChartZDays._inProcess = true;
            return $.post('/reports/product-analysis-ajax/get-plan-fact-data', {
                    "chart-plan-fact-ajax": true,
                    "period": ChartZDays.period,
                    "offset": window.ChartZDays.offset[ChartZDays.period],
                    "group": ChartZDays.byGroup,
                    "yearMonth": ChartZDays.productsYearMonth
                },
                function(data) {
                    data = JSON.parse(data);
                    ChartZDays.freeDays = data.freeDays;
                    ChartZDays.currDayPos = data.currDayPos;
                    ChartZDays.labelsX = data.labelsX;
                    ChartZDays.wrapPointPos = data.wrapPointPos;
                    ChartZDays.chartPoints = data.optionsChart;
                    ChartZDays.chartPoints2 = data.optionsChart2;
                    ChartZDays.chartPoints3 = data.optionsChart3;
                    // structure charts
                    ChartZDays.Products1.totalAmount =  data.products1.totalAmount;
                    ChartZDays.Products1.chartHeight =  data.products1.chartHeight;
                    ChartZDays.Products1.chartPoints =  data.products1.optionsChart;
                    ChartZDays.Products2.totalAmount =  data.products2.totalAmount;
                    ChartZDays.Products2.quantity    =  data.products2.quantity;
                    ChartZDays.Products2.chartHeight =  data.products2.chartHeight;
                    ChartZDays.Products2.chartPoints =  data.products2.optionsChart;
                }
            );
        },
        getWrapPointXLabel: function(x) {

            let chart = $('#chart-plan-fact').highcharts();
            let colWidth = (chart.xAxis[0].width) / <?= $LEFT_DATE_OFFSET + $RIGHT_DATE_OFFSET - 1 ?>;
            let name, left, txtLine, txtLabel;

            let k = (window.ChartZDays.period == 'months') ? 0.725 : 0.625;

            if (window.ChartZDays.wrapPointPos[x + 1]) {
                name = window.ChartZDays.wrapPointPos[x + 1].prev;
                left = window.ChartZDays.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartZDays.wrapPointPos[x]) {
                name = window.ChartZDays.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        formatter: function(args) {

            let idx = this.point.index;

            if (window.ChartZDays.period === "months") {

                return '<span class="title">' + window.ChartZDays.labelsX[this.point.index] + '</span>' +

                    ('<table class="indicators">' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[0].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[1].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '</table>');

            } else {

                return "";
            }
        },
        formatter3: function(args) {

            let idx = this.point.index;

            if (window.ChartZDays.period === "months") {

                let revenue = (args.chart.series[0].data[idx].y > 0) ? args.chart.series[0].data[idx].y : 9E9;
                let margin = args.chart.series[1].data[idx].y;

                return '<span class="title">' + window.ChartZDays.labelsX[this.point.index] + '</span>' +

                    ('<table class="indicators">' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[0].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '<tr>' + '<td class="gray-text">' + args.chart.series[1].name + ': ' + '</td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '<tr>' + '<td class="gray-text">Доля маржи: </td>' + '<td class="gray-text-b">' + Highcharts.numberFormat(margin / revenue * 100, 2, ',', ' ') + ' %</td></tr>' +
                        '</table>');

            } else {

                return "";
            }
        },
        positioner: function(boxWidth, boxHeight, point) {

            if (ChartZDays.period === "months") {
                return {x: point.plotX, y: point.plotY - boxHeight / 2};
            } else {
                return {x: point.plotX + 69, y: point.plotY - 45};
            }
        }
    };

    ////////////////////////////////
    window.ChartZDays.init();
    ////////////////////////////////

    // recognize overflow arrows "left"/"right" with tooltip!
    //$(document).ready(function() {
    //    window._ChartZDaysArrowZ = 1;
    //    $('#chart-plan-fact').mousemove(function(event) {
    //        let setZ = function() { $('.chart-plan-fact-days-arrow').filter('.chart-1').css({"z-index": window._ChartZDaysArrowZ}); };
    //        let y = event.pageY - $(this).offset().top;
    //        let x = event.pageX - $(this).offset().left;
    //        if (window.ChartZDays.period == "days") {
    //            if (y > 170 && (x < 60 || x > $(this).width() - 20) && window._ChartZDaysArrowZ === 1) {
    //                window._ChartZDaysArrowZ = 2;
    //                setZ();
    //            }
    //            if (y <= 170 && window._ChartZDaysArrowZ === 2) {
    //                window._ChartZDaysArrowZ = 1;
    //                setZ();
    //            }
    //        } else if (window._ChartZDaysArrowZ === 1) {
    //            window._ChartZDaysArrowZ = 2;
    //            setZ();
    //        }
    //    });
    //
    //});

</script>