<?php

use common\components\grid\GridView;
use frontend\components\PageSize;
use frontend\modules\reports\models\ProductAnalysisABC;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\reports\models\ProductAnalysisSearch;
use frontend\themes\kub\components\Icon;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'XYZ анализ по единицам продаж';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');

$colspanRight = $colspanLeft = 0;
$vPrev = null;
foreach(ProductAnalysisSearch::$months as $k => $v) {
    if ($vPrev && substr($v, 0, 4) != substr($vPrev, 0, 4)) {
        $colspanRight = count(ProductAnalysisSearch::$months) - $k;
        break;
    }
    $vPrev = $v;
    $colspanLeft++;
}

$products = $dataProvider->getModels();

$filterByGroup = \yii\helpers\ArrayHelper::getValue(Yii::$app->request->get(), 'ProductAnalysisSearch.group_id');
$filterByProduct = \yii\helpers\ArrayHelper::getValue(Yii::$app->request->get(), 'ProductAnalysisSearch.product_id');
$collapsedLeft = Yii::$app->request->get('left');
$collapsedRight = Yii::$app->request->get('right');
?>

<style type="text/css">
.overal-result-table .result-row { cursor: pointer; }
.overal-result-table .result-row:hover td { background-color: #f1f1f1; }
#mCSB_1_dragger_horizontal { visibility: visible!important; }
.th-title-wrap.filter.active .th-title-icon-filter { color: #0097fd; }
</style>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-0 pl-1">
                <?= \yii\bootstrap\Html::beginForm(['index'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= \common\components\helpers\Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'xyz-analysis-pjax',
    'timeout' => 10000,
]); ?>

<div class="wrap">
    <div class="row">

        <div class="col-8">
            <table class="table table-style table-count-list overal-result-table table-hover compact-disallow" style="width: auto;">
                <thead>
                <tr class="heading">
                    <th>Группа товаров</th>
                    <th style="text-align: center;">Кол-во типов товаров</th>
                    <th style="text-align: center;">% товаров</th>
                    <th style="text-align: center;">Кол-во товаров на складе</th>
                    <th style="text-align: center;">% товаров на складе</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($searchModel->overallResult as $row) : ?>
                    <?php $color = ProductAnalysisSearch::$groupsColor[$row['group_id']] ?? "#000"; ?>
                    <tr class="result-row" data-group="<?= $row['group_id'] ?>">
                        <td style="color: <?= $color ?>;">
                            <?= $searchModel->getGroupLabel($row['group_id']) ?>
                        </td>
                        <td style="text-align: right;">
                            <?= $row['product_types'] ?>
                        </td>
                        <td style="text-align: right; font-weight: bold; color: <?= $color ?>;">
                            <?= ($searchModel->totalProductTypes > 0) ? round(100 * $row['product_types'] / $searchModel->totalProductTypes) : '' ?>
                        </td>
                        <td style="text-align: right;">
                            <?= round($row['product_quantity'], $row['product_quantity'] == (int)$row['product_quantity'] ? 0 : 3) ?>
                        </td>
                        <td style="text-align: right; font-weight: bold; color: <?= $color ?>;">
                            <?= ($searchModel->totalProductQuantity > 0) ? round(100 * $row['product_quantity'] / $searchModel->totalProductQuantity) : '' ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr class="result-row" data-group="" style="font-weight: bold;">
                    <td>Итого</td>
                    <td style="text-align: right;"><?= $searchModel->totalProductTypes ?></td>
                    <td style="text-align: right;"></td>
                    <td style="text-align: right;"><?= $searchModel->totalProductQuantity ?></td>
                    <td style="text-align: right;"></td>

                </tbody>
            </table>
        </div>

        <?php /*
        <div class="col-4">
            <?php if ($series) : ?>
                <div class="row">
                    <div class="col-12">
                    <?= \miloschuman\highcharts\Highcharts::widget([
                        'htmlOptions' => [
                            'style' => 'height: 180px; padding: 0; margin-top: 22px; margin-bottom: -22px;',
                        ],
                        'options' => [
                            'chart' =>['type' => 'column'],
                            'title' => false,
                            'xAxis' => [
                                'categories' => ['Покупатели', 'Выручка'],
                            ],
                            'yAxis' => [
                                'title' => false,
                            ],
                            'series' => $series,
                            'plotOptions' => [
                                'column' => [
                                    'stacking' => 'percent',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'style' => [
                                            'color' => '#333333',
                                            'textOutline' => null
                                        ],
                                    ],
                                ],
                                'series' => [
                                    'showInLegend' => false,
                                    'borderWidth' => 0,
                                ],
                            ],
                            /////////////////////////////////// STD OPTS ////////////////////////////////////////////////
                            'credits' => [
                                'enabled' => false
                            ],
                            'tooltip' => [
                                'useHtml' => true,
                                //'split' => true,
                                //'shared' => true,
                                'backgroundColor' => "rgba(255,255,255,1)",
                                'borderColor' => '#ddd',
                                'borderWidth' => '1',
                                'borderRadius' => 8,
                                'pointFormat' => '<span class="gray-text">{series.name}: </span><span class="gray-text-b">{point.y} ₽</span>',
                            ],
                            'legend' => [
                                'layout' => 'horizontal',
                                'align' => 'right',
                                'verticalAlign' => 'top',
                                'backgroundColor' => '#fff',
                                'itemStyle' => [
                                    'fontSize' => '11px',
                                    'color' => '#9198a0'
                                ],
                                'symbolRadius' => 2
                            ]
                            //////////////////////////////////////////////////////////////////////////////////////////
                        ]
                    ]); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>*/ ?>

    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
</div>

<div id="xyz-analysis-grid" class="dataTables_wrapper dataTables_extended_wrapper">
    <div class="wrap wrap_padding_none">
        <div class="custom-scroll-table">

            <table class="table table-style table-count-list <?= $tabViewClass ?>" aria-describedby="datatable_ajax_info" role="grid">
                <thead>
                    <tr>
                        <th width="20%" rowspan="2" class="th-title-wrap filter filter-open <?= $filterByProduct ? 'active' : '' ?>">
                            <div id="productanalysissearch-product_id-toggle" class="th-title filter">
                                <span class="th-title-name">Название</span>
                                <span class="th-title-btns">
                                    <?= \yii\helpers\Html::button(Icon::get('filter', ['class' => 'th-title-icon-filter']), ['class' => 'th-title-btn button-clr']) ?>
                                </span>
                                <div class="filter-select2-select-container">
                                    <?= Select2::widget([
                                        'id' => 'productanalysissearch-product_id',
                                        'name' => 'productanalysissearch-product_id',
                                        'data' => ['' => 'Все'] + $searchModel->getProductFilterItems(),
                                        'value' => $filterByProduct,
                                        'pluginOptions' => [
                                            'width' => '300px',
                                            'containerCssClass' => 'select2-filter-by-product'
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </th>
                        <th width="10%" rowspan="2">
                            <?= $this->render('partial/sort_arrows', [
                                'title' => 'Коэффициент вариации',
                                'attr' => "variation_coefficient"
                            ]) ?>
                        </th>
                        <th width="10%" rowspan="2" class="th-title-wrap filter filter-open <?= $filterByGroup ? 'active' : '' ?>">
                            <div id="productanalysissearch-group_id-toggle" class="th-title filter">
                                <span class="th-title-name">Группа</span>
                                <span class="th-title-btns">
                                    <?= \yii\helpers\Html::button(Icon::get('filter', ['class' => 'th-title-icon-filter']), ['class' => 'th-title-btn button-clr']) ?>
                                </span>
                                <div class="filter-select2-select-container">
                                    <?= Select2::widget([
                                        'id' => 'productanalysissearch-group_id',
                                        'name' => 'productanalysissearch-group_id',
                                        'data' => ['' => 'Все'] + ProductAnalysisSearch::$groups,
                                        'value' => $filterByGroup,
                                        'pluginOptions' => [
                                            'width' => '150px',
                                            'containerCssClass' => 'select2-filter-by-group'
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </th>
                        <th colspan="<?= ($collapsedLeft) ? 1 : $colspanLeft ?>" data-collapse-cell-title data-id="left">
                            <button id="collapsedLeft"
                                    class="table-collapse-btn button-clr ml-1 <?= ($collapsedLeft) ? '' : 'active' ?>"
                                    type="button"
                                    data-collapse-trigger-xyz
                                    data-target="left"
                                    data-columns-count="<?= $colspanLeft ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">
                                    <?= ProductAnalysisSearch::getYearName(0) ?>
                                </span>
                            </button>
                        </th>
                        <th colspan="<?= ($collapsedRight) ? 1 : $colspanRight ?>" data-collapse-cell-title  data-id="right">
                            <button id="collapsedRight"
                                    class="table-collapse-btn button-clr ml-1 <?= ($collapsedRight) ? '' : 'active' ?>"
                                    type="button"
                                    data-collapse-trigger-xyz
                                    data-target="right"
                                    data-columns-count="<?= $colspanRight ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">
                                    <?= ProductAnalysisSearch::getYearName(count(ProductAnalysisSearch::$months)-1) ?>
                                </span>
                            </button>
                        </th>
                    </tr>
                    <tr id="xyz-analysis-grid-filters" class="filters">
                        <?php foreach(ProductAnalysisSearch::$months as $k => $v): ?>
                            <th data-collapse-cell data-id="<?= ($k < $colspanLeft) ? 'left' : 'right' ?>" class="<?= (!(($k < $colspanLeft) ? $collapsedLeft : $collapsedRight)) ? '' : 'd-none' ?>">
                                <?= $this->render('partial/sort_arrows', [
                                    'title' => ProductAnalysisSearch::getMonthName($k),
                                    'attr' => "month_{$k}"
                                ]) ?>
                            </th>

                            <?php if ((1+$k) == $colspanLeft): ?>
                                <th data-collapse-cell-total class="totals <?= ($collapsedLeft) ? '' : 'd-none' ?>" data-id="left">Итого</th>
                            <?php endif; ?>

                        <?php endforeach; ?>
                        <th data-collapse-cell-total class="totals <?= ($collapsedRight) ? '' : 'd-none' ?>" data-id="right">Итого</th>
                    </tr>
                </thead>
                <thead>
                <tbody>
                    <?php foreach ($products as $data): ?>
                        <tr>
                            <td>
                                <?php
                                $title = (mb_strlen($data['product_title'], 'UTF-8') > 100) ? (mb_substr($data['product_title'], 0, 95, 'utf-8') . '...') : $data['product_title'];
                                echo Html::a(Html::encode($title), [
                                    '/product/view',
                                    'productionType' => $data['production_type'],
                                    'id' => $data['product_id'],
                                ], ['target' => '_blank', 'data-pjax' => 0]);
                                ?>
                            </td>
                            <td class="text-right">
                                <?= ($data['variation_coefficient'] < 999999) ? (round($data['variation_coefficient'], 0) . '%') : '' ?>
                            </td>
                            <td class="text-right">
                                <?= $data['variation_coefficient'] <= 10 ? 'X' : ($data['variation_coefficient'] <= 25 ? 'Y' : 'Z') ?>
                            </td>
                            <?php $total = 0; ?>
                            <?php foreach(ProductAnalysisSearch::$months as $k => $v): ?>
                                <?php $total += $data["month_{$k}"]; ?>
                                <td class="text-right <?= (!(($k < $colspanLeft) ? $collapsedLeft : $collapsedRight)) ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= ($k < $colspanLeft) ? 'left' : 'right' ?>">
                                    <?= ProductAnalysisSearch::getMonthValue($k, $data['first_date'], $data["month_{$k}"]); ?>
                                </td>

                                <?php if ((1+$k) == $colspanLeft): ?>
                                    <td data-collapse-cell class="text-right totals <?= ($collapsedLeft) ? '' : 'd-none' ?>" data-id="left">
                                        <?= $total ?>
                                        <?php $total = 0; ?>
                                    </td>
                                <?php endif; ?>

                            <?php endforeach; ?>
                            <td data-collapse-cell class="text-right totals <?= ($collapsedRight) ? '' : 'd-none' ?>" data-id="right">
                                <?= $total ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="table-settings-view row align-items-center">
        <div class="col-8">
            <nav>
                <?= \common\components\grid\KubLinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                ]) ?>
            </nav>
        </div>
        <div class="col-4">
            <?= $this->render('@frontend/themes/kub/views/layouts/grid/perPage', [
                'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
                'pageSizeParam' => 'per-page',
            ]) ?>
        </div>
    </div>

</div>

<script>
    $('[data-collapse-trigger-xyz]').click(function() {
        var target = $(this).data('target');
        var collapseCount = $(this).data('columns-count') || 3;
        $(this).toggleClass('active');
        $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
        $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
        if ( $(this).hasClass('active') ) {
            $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
        } else {
            $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
        }
        $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
        // console.log('lalalal');
    });
</script>

<?php $pjax->end(); ?>

<script>

    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#productanalysissearch-group_id").val($(this).data("group")).trigger('change');
    });

    $(document).on("pjax:success", "#xyz-analysis-pjax", function() {
        $(".custom-scroll-table").mCustomScrollbar({
            horizontalScroll: true,
            axis:"x",
            scrollInertia: 300,
            advanced:{
                autoExpandHorizontalScroll: true,
                updateOnContentResize: true,
                updateOnImageLoad: false
            },
            mouseWheel:{ enable: false },
        });
    });

    $(document).on("click", "#productanalysissearch-group_id-toggle", function (e) {
        e.preventDefault();
        if (!$("#productanalysissearch-group_id").data("select2").isOpen()) {
            $("#productanalysissearch-group_id").select2("open");
        }
        return false;
    });
    $(document).on("change", "#productanalysissearch-group_id", function() {
        $.pjax.reload("#xyz-analysis-pjax", {"push": true, "timeout": 10000, "url": addOrReplaceParam(document.location.search, 'ProductAnalysisSearch[group_id]', $(this).val())})
    });
    $(document).on("click", "#productanalysissearch-product_id-toggle", function (e) {
        e.preventDefault();
        if (!$("#productanalysissearch-product_id").data("select2").isOpen()) {
            $("#productanalysissearch-product_id").select2("open");
        }
        return false;
    });
    $(document).on("change", "#productanalysissearch-product_id", function() {
        $.pjax.reload("#xyz-analysis-pjax", {"push": true, "timeout": 10000, "url": addOrReplaceParam(document.location.search, 'ProductAnalysisSearch[product_id]', $(this).val())})
    });

    $(document).on('pjax:beforeSend', function(event, xhr, settings) {
        let left = $('#collapsedLeft').hasClass('active') ? 0 : 1;
        let right = $('#collapsedRight').hasClass('active') ? 0 : 1;
        settings.url += "&left=" + left + "&right=" + right;
    });

    $('select#productanalysissearch-year').change(function () {
        location.href = $(this).closest('form').attr('action') + '?' + $(this).closest('form').serialize();
    });

    /**
     * Add a URL parameter (or modify if already exists)
     * @param {string} url
     * @param {string} param the key to set
     * @param {string} value
     */
    var addOrReplaceParam = function(url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1"+str);
        if (q === a.search) {
            a.search += (a.search ? "&" : "") + str;
        } else {
            a.search = q;
        }
        return a.href;
    }
</script>