<?php
/**
  * @var $key int
  * @var $tabConfig array
  * @var $products array
  * @var $prevProducts array
 */
?>


<tr data-key="totals">
    <!--Название-->
    <td class="">
        <b>ИТОГО</b>
    </td>
    <!--Артикул-->
    <td class="<?= $tabConfig['article'] ?>">
    </td>

    <!--Оборот-->
    <td class="text-right" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * array_sum(array_column($products, 'turnover')),
            'prevValue' => 1/100 * array_sum(array_column(($prevProducts ?? []), 'turnover'))
        ]); ?>
    </td>
    <!--Себестоимость-->
    <td class="text-right <?= $tabConfig['cost_price'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * array_sum(array_column($products, 'cost_price')),
            'prevValue' => 1/100 * array_sum(array_column(($prevProducts ?? []), 'cost_price'))
        ]); ?>
    </td>
    <!--Цена продажи-->
    <td class="text-right <?= $tabConfig['selling_price'] ?>" data-collapse-cell data-id="cell-data">

    </td>
    <!--Цена покупки-->
    <td class="text-right <?= $tabConfig['purchase_price'] ?>" data-collapse-cell data-id="cell-data">

    </td>
    <!--Маржа Р-->
    <td class="text-right">
        <?= $this->render('triple_cell', [
            'value' => 1/100 * array_sum(array_column($products, 'margin')),
            'prevValue' => 1/100 * array_sum(array_column(($prevProducts ?? []), 'margin'))
        ]); ?>
    </td>
    <!--% Маржи-->
    <td class="text-right">
    </td>
    <!--Кол-во продано-->
    <td class="text-right">
        <?= $this->render('triple_cell', [
            'value' => array_sum(array_column($products, 'selling_quantity')),
            'prevValue' => array_sum(array_column(($prevProducts ?? []), 'selling_quantity'))
        ]); ?>
    </td>
    <!--Доля маржи в группе-->
    <td class="text-right <?= $tabConfig['group_margin'] ?>">
    </td>
    <!--Кол-во продаж-->
    <td class="text-right <?= $tabConfig['number_of_sales'] ?>" data-collapse-cell data-id="cell-data">
        <?= $this->render('triple_cell', [
            'value' => array_sum(array_column($products, 'number_of_sales')),
            'prevValue' => array_sum(array_column(($prevProducts ?? []), 'number_of_sales'))
        ]); ?>
    </td>
    <!--Средний чек-->
    <td class="text-right <?= $tabConfig['average_check'] ?>" data-collapse-cell data-id="cell-data">
    </td>

    <!--АВС комбинация-->
    <td class="text-right">
    </td>
    <!--XYZ значение-->
    <td class="text-right" data-collapse-cell data-id="cell-result">
    </td>

    <!-- GUIDANCE -->
    <td data-collapse-cell data-id="cell-guidance">
    </td>
    <td>
    </td>
    <td data-collapse-cell data-id="cell-guidance">
    </td>
    <td data-collapse-cell data-id="cell-guidance">
    </td>
    <td data-collapse-cell data-id="cell-guidance">
    </td>
    <td data-collapse-cell data-id="cell-guidance">
    </td>
    <td>
    </td>

    <!-- STORE -->
    <td data-collapse-cell data-id="cell-store">
    </td>
    <td>
    </td>
    <td data-collapse-cell data-id="cell-store">
    </td>

    <!-- ORDER -->
    <td data-collapse-cell data-id="cell-order">
    </td>
    <td>
    </td>
    <td data-collapse-cell data-id="cell-order">
    </td>

    <!--Комментарий-->
    <td class="<?= $tabConfig['comment'] ?>">
    </td>

</tr>