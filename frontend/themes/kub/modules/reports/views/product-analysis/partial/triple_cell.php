<?php
/**
 * @var int $value
 * @var int $prevValue
 * @var string $isNew
 */

use common\components\TextHelper;
use common\components\date\DateHelper;

$arrowClass = (round($value, 2) - round($prevValue, 2) > 0) ?
    $arrowClass = 'fa-arrow-up' : (round($value, 2) - round($prevValue, 2) < 0 ? $arrowClass = 'fa-arrow-down' : '');

$prevFirstDate = $prevFirstDate ?? null;
$currDate = $currDate ?? null;

$isNew = $isNew ?? false;
?>
<div class="grid-cell-3" style="min-width: <?= 110 + strlen(round($value)) * 10 ?>px">
    <div>
        <?= TextHelper::moneyFormat($value, 2) ?>
    </div>
    <div>
        <div>
            <?php if ($prevValue != 0): ?>
                <?= TextHelper::moneyFormat(($value - $prevValue) / $prevValue * 100, 2) ?>%
            <?php elseif ($isNew): ?>
                100%
            <?php else: ?>
                &nbsp;
            <?php endif; ?>
        </div>
        <div class="arrow"><i class="fa <?= $arrowClass ?>"></i></div>
        <div>
            <?php if ($prevValue != 0): ?>
                <?= TextHelper::moneyFormat($value - $prevValue, 2) ?>
            <?php elseif ($isNew): ?>
                Новый
            <?php else: ?>
                &nbsp;
            <?php endif; ?>
        </div>
    </div>
</div>