<?php

use common\components\TextHelper;

/**
 * @var $ico string
 * @var $title string
 * @var $value string
 * @var $prevValue string
 * @var $units string
 * @var $borders array
 */

?>
<table class="abc-table-totals <?= in_array('bottom', $borders) ? 'bb':''?> <?= in_array('right', $borders) ? 'br':''?>">
    <tr>
        <td rowspan="2" width="52">
            <img width="32" src="/img/abc/<?= $ico ?>"/>
        </td>
        <td class="abc-title">
            <?= $title ?>
            <?php if ($value != $prevValue): ?>
                <img class="abc-arrow" width="14" src="/img/abc/arrow_<?= ($value > $prevValue ? 'green':'red')?>.svg"
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="abc-gray">
            <?= TextHelper::moneyFormat($value) ?> <?= $units ?>
        </td>
    </tr>
</table>