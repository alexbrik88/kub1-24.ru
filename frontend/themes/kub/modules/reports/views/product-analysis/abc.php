<?php

use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\reports\models\ProductAnalysisABC;
use common\components\TextHelper;
use common\components\helpers\ArrayHelper;
use frontend\widgets\TableConfigWidget;
use \common\models\product\ProductGroup;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisABC */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'ABC анализ';
$userConfig = Yii::$app->user->identity->config;
$showHelpPanel = $userConfig->product_abc_help ?? false;
$showChartPanel = $userConfig->product_abc_chart ?? false;

$SHOW_BY_GROUPS = $userConfig->report_abc_show_groups;
$tabConfig = [
    'article' => 'col_report_abc_article' . ($userConfig->report_abc_article ? '' : ' hidden'),
    'cost_price' => 'col_report_abc_cost_price' . ($userConfig->report_abc_cost_price ? '' : ' hidden'),
    'purchase_price' => 'col_report_abc_purchase_price' . ($userConfig->report_abc_purchase_price ? '' : ' hidden'),
    'selling_price' => 'col_report_abc_selling_price' . ($userConfig->report_abc_selling_price ? '' : ' hidden'),
    'group_margin' => 'col_report_abc_group_margin' . ($userConfig->report_abc_group_margin ? '' : ' hidden'),
    'number_of_sales' => 'col_report_abc_number_of_sales' . ($userConfig->report_abc_number_of_sales ? '' : ' hidden'),
    'average_check' => 'col_report_abc_average_check' . ($userConfig->report_abc_average_check ? '' : ' hidden'),
    'comment' => 'col_report_abc_comment' . ($userConfig->report_abc_comment ? '' : ' hidden')
];
$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_abc');

$products = $dataProvider->getModels();
$prevProducts = &$searchModel->prevPeriodData;

$model = new \common\models\product\Product();
$model->company_id = Yii::$app->user->identity->company_id;
foreach ($products as &$product) {

    $model->setDateStart(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year)));
    $model->setDateEnd(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)));
    $product['balance_at_start'] = $model->balanceAtDate(true, [$product['product_id']]);
    $product['balance_at_end'] = $model->balanceAtDate(false,  [$product['product_id']]);
    $product['irreducible_quantity'] = array_sum($model->getIrreducibleQuantity());
}

if ($SHOW_BY_GROUPS) {
    $groups = $searchModel->getGroupsByProducts($products, true);
    $prevGroups = $searchModel->getGroupsByProducts($prevProducts, false);
} else {
    $groups = &$products;
    $prevGroups = &$prevProducts;
}
//var_dump($groups);exit;
$colors = [
    ProductAnalysisABC::GROUP_A => 'rgba(38, 205, 88, 0.25)',
    ProductAnalysisABC::GROUP_B => 'rgba(200, 205, 38, 0.25)',
    ProductAnalysisABC::GROUP_C => 'rgba(227, 6, 17, 0.25)',
]
?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-2">
                <?= \yii\bootstrap4\Html::button(Icon::get('diagram'),
                    [
                        'id' => 'btnChartCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showChartPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#chartCollapse',
                        'data-tooltip-content' => '#tooltip_chart_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pr-0 pl-1">
                <?= \yii\bootstrap\Html::beginForm(['abc'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= \common\components\helpers\Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="product_abc_chart">
    <div class="pt-4 pb-3">
        <div class="row">
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ВЫРУЧКА',
                    'ico' => 'ico_turnover.svg',
                    'value' => 1/100 * array_sum(array_column($products, 'turnover')),
                    'prevValue' => 1/100 * array_sum(array_column($prevProducts, 'turnover')),
                    'units' => '₽',
                    'borders' => ['bottom', 'right']
                ]) ?>
            </div>
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'МАРЖА',
                    'ico' => 'ico_margin.svg',
                    'value' => 1/100 * array_sum(array_column($products, 'margin')),
                    'prevValue' => 1/100 * array_sum(array_column($prevProducts, 'margin')),
                    'units' => '₽',
                    'borders' => ['bottom', 'right']
                ]) ?>
            </div>
            <div class="col-4 pl-0">
                <?php
                $model->setDateEnd(new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)));
                $stockQuantity = $model->balanceAtDate(false, array_column($products, 'product_id'));
                $model->setDateEnd((new DateTime(ProductAnalysisABC::_getDateFromYM($searchModel->year, true)))->modify("-1 month"));
                $prevStockQuantity = $model->balanceAtDate(false, array_column($products, 'product_id'));
                ?>
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ОСТАТОК НА СКЛАДЕ',
                    'ico' => 'ico_store.svg',
                    'value' => $stockQuantity,
                    'prevValue' => $prevStockQuantity,
                    'units' => 'шт.',
                    'borders' => ['bottom']
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-4 pr-0 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'ОБЪЕМ ПРОДАЖ',
                    'ico' => 'ico_sells_quantity.svg',
                    'value' => array_sum(array_column($products, 'selling_quantity')),
                    'prevValue' => array_sum(array_column($prevProducts, 'selling_quantity')),
                    'units' => 'шт.',
                    'borders' => ['right']
                ]) ?>
            </div>
            <div class="col-4 pl-0 pr-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => '% МАРЖИ',
                    'ico' => 'ico_margin_percent.svg',
                    'value' => (count($products) > 0) ? 100 * array_sum(array_column($products, 'margin_percent')) / count($products) : 0,
                    'prevValue' => (count($prevProducts) > 0) ? 100 * array_sum(array_column($prevProducts, 'margin_percent')) / count($prevProducts) : 0,
                    'units' => '%',
                    'borders' => ['right']
                ]) ?>
            </div>
            <div class="col-4 pl-0">
                <?= $this->render('partial/chart_totals_cell', [
                    'title' => 'СРЕДНИЙ ЧЕК',
                    'ico' => 'ico_average_check.svg',
                    'value' => (count($products) > 0) ? 1/100 * array_sum(array_column($products, 'average_check')) / count($products) : 0,
                    'prevValue' => (count($prevProducts) > 0) ? 1/100 * array_sum(array_column($prevProducts, 'average_check')) / count($prevProducts) : 0,
                    'units' => '₽',
                    'borders' => []
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="product_abc_help">
    <div class="pt-4 pb-3">
        ...
        <?php
        //echo $this->render('_partial/odds-panel')
        ?>
    </div>
</div>


<?php
//$pjax = Pjax::begin([
//    'id' => 'abc-analysis-pjax',
//    'timeout' => 10000,
//]);
?>

<div class="table-settings row row_indents_s mt-2">
    <div class="col-6">
        <?= TableConfigWidget::widget([
            'items' => array_filter([
                [
                    'attribute' => 'report_abc_article',
                ],
                [
                    'attribute' => 'report_abc_cost_price',
                ],
                [
                    'attribute' => 'report_abc_selling_price',
                ],
                [
                    'attribute' => 'report_abc_purchase_price',
                ],
                [
                    'attribute' => 'report_abc_group_margin',
                ],
                [
                    'attribute' => 'report_abc_number_of_sales',
                ],
                [
                    'attribute' => 'report_abc_average_check',
                ],
                [
                    'attribute' => 'report_abc_comment',
                ],
            ]),
            'sortingItemsTitle' => 'Выводить',
            'sortingItems' => [
                [
                    'attribute' => 'show_groups',
                    'label' => 'По группам товара',
                    'checked' => $SHOW_BY_GROUPS,
                ],
                [
                    'attribute' => 'dont_show_groups',
                    'label' => 'Без групп товара',
                    'checked' => !$SHOW_BY_GROUPS,
                ],
            ]
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_report_abc']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'title', [
                'type' => 'search',
                'placeholder' => 'Поиск по наименованию или артикулу',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div id="abc-analysis-grid">
    <div class="wrap wrap_padding_none_all" style="position:relative!important;">

        <div id="cs-table-2" class="custom-scroll-table-double cs-top">
            <table style="width: 1010px; font-size:0;"><tr><td>&nbsp;</td></tr></table>
        </div>

        <!-- ODDS FIXED COLUMN -->
        <div id="cs-table-first-column">
            <table class="table-fixed-first-column table table-style table-count-list <?= $tabViewClass ?>">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>

        <div id="cs-table-1" class="custom-scroll-table-double">
            <div class="table-wrap">
                <table class="table table-style table-count-list analysis-table <?= $tabViewClass ?>" aria-describedby="datatable_ajax_info" role="grid">
                    <thead>
                    <tr>
                        <th rowspan="2">
                            <?php if ($SHOW_BY_GROUPS): ?>
                                <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-all-trigger>
                                    <span class="table-collapse-icon">&nbsp;</span>
                                    <span class="text-grey weight-700 ml-1">Группа товара</span>
                                </button>
                            <?php else: ?>
                                Название товара
                            <?php endif; ?>
                        </th>
                        <th rowspan="2" class="<?= $tabConfig['article'] ?>">Артикул</th>
                        <th colspan="<?= ProductAnalysisABC::getFluidColspan($userConfig) ?>" class="height-30">
                            <button id="collapse-cell-data"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-data"
                                    data-colspan-close="3"
                                    data-colspan-open="<?= ProductAnalysisABC::getFluidColspan($userConfig) ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Данные</span>
                            </button>
                        </th>
                        <th colspan="2">
                            <button id="collapse-cell-result"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-result"
                                    data-colspan-close="1"
                                    data-colspan-open="2">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Результат</span>
                            </button>
                        </th>
                        <th colspan="7">
                            <button id="collapse-cell-guidance"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-guidance"
                                    data-colspan-close="4"
                                    data-colspan-open="7">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Рекомендации</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button id="collapse-cell-store"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-store"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Склад</span>
                            </button>
                        </th>
                        <th colspan="3">
                            <button id="collapse-cell-order"
                                    class="table-collapse-btn button-clr ml-1 active"
                                    type="button"
                                    data-collapse-trigger-abc
                                    data-target="cell-order"
                                    data-colspan-close="1"
                                    data-colspan-open="3">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="text-grey weight-700 ml-1 nowrap">Заказ</span>
                            </button>
                        </th>
                        <th rowspan="2" class="<?= $tabConfig['comment'] ?>">Комментарий</th>
                    </tr>
                    <tr id="abc-analysis-grid-filters" class="filters">

                        <!--<th width="10%">Название</th>
                        <th width="10%">Артикул</th>-->

                        <!-- DATA -->
                        <th width="10%" data-collapse-cell data-id="cell-data" class="height-30">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Выручка', 'attr' => 'turnover']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['cost_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Себестоимость', 'attr' => 'cost_price']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['selling_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Цена продажи', 'attr' => 'selling_price']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['purchase_price'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Цена покупки', 'attr' => 'purchase_price']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Маржа ₽', 'attr' => 'margin']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => '% Маржи', 'attr' => 'margin_percent']) ?>
                        </th>
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Объем продаж', 'attr' => 'selling_quantity']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['group_margin'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Доля маржи в группе', 'attr' => 'group_margin_percent']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['number_of_sales'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Кол-во сделок', 'attr' => 'number_of_sales']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-data" class="<?= $tabConfig['average_check'] ?>">
                            <?= $this->render('partial/sort_arrows', ['title' => 'Средний чек', 'attr' => 'average_check']) ?>
                        </th>

                        <!-- RESULTS -->
                        <th width="10%">
                            <?= $this->render('partial/sort_arrows', ['title' => 'АВС', 'attr' => 'abc']) ?>
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-result">
                            <?= $this->render('partial/sort_arrows', ['title' => 'XYZ', 'attr' => 'variation_coefficient']) ?>
                        </th>

                        <!-- GUIDANCE -->
                        <th width="10%">
                            Наличие
                        </th>
                        <th width="10%">
                            Реклама
                        </th>
                        <th width="10%">
                            Скидки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Акции
                        </th>
                        <th width="10%">
                            Цена продажи
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Цена закупки
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-guidance">
                            Вывод из ассортимента
                        </th>

                        <!-- STORE -->
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Остаток на начало
                        </th>
                        <th width="10%">
                            Остаток на конец
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-store">
                            Неснижаемый остаток
                        </th>

                        <!-- ORDER -->
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Мин. рекомендуе&shy;мый заказ (ед.)
                        </th>
                        <th width="10%">
                            Заказ (ед.)
                        </th>
                        <th width="10%" data-collapse-cell data-id="cell-order">
                            Сумма заказа
                        </th>

                    </tr>
                    </thead>
                    <?php if (!empty($products)): ?>
                    <tbody>
                    <?php foreach ($groups as $key => $group): ?>
                        <?php $isGroup = !empty($group['items']); ?>
                        <?php $prevGroup = $prevGroups[$key] ?? []; ?>
                        <?php $recommendations = $searchModel::getRecommendations($group['abc'], $group['variation_coefficient']); ?>

                        <?= $this->render('partial/table_row', [
                            'searchModel' => $searchModel,
                            'key' => $key,
                            'tabConfig' => $tabConfig,
                            'product' => $group,
                            'prevProduct' => $prevGroup,
                            'isGroup' => $isGroup,
                            'colors' => $colors,
                            'recommendations' => $recommendations
                        ]); ?>

                        <?php if (!empty($group['items'])) foreach ($group['items'] as &$product): ?>
                            <?php $prevProduct = $prevProducts[$product['product_id']] ?? []; ?>
                            <?php $recommendations = $searchModel::getRecommendations($product['abc'], $product['variation_coefficient']); ?>

                            <?= $this->render('partial/table_row', [
                                'searchModel' => $searchModel,
                                'key' => $key,
                                'tabConfig' => $tabConfig,
                                'product' => $product,
                                'prevProduct' => $prevProduct,
                                'isGroupRow' => true,
                                'colors' => $colors,
                                'recommendations' => $recommendations
                            ]); ?>

                        <?php endforeach; ?>
                    <?php endforeach; ?>

                    <?= $this->render('partial/table_totals', [
                        'searchModel' => $searchModel,
                        'key' => $key,
                        'tabConfig' => $tabConfig,
                        'products' => $products,
                        'prevProducts' => $prevProducts
                    ]); ?>

                    </tbody>
                    <?php else: ?>
                        <tr>
                            <td colspan="11">Нет данных за выбранный период</td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div>

    </div>
</div>

<?php
//$pjax->end();
?>

<?php

$this->registerJs(<<<JS

    _activeMCS = 0;
    _offsetMCSLeft = 0;
    _firstCSTableColumn = $('#cs-table-first-column');

    $('#cs-table-1').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 1;
            },
            onScroll: function() {
                if (window._activeMCS === 1)
                    window._activeMCS = 0;

                if (this.mcs.left === 0 && $(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).hide();
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS === 1)
                    $('#cs-table-2').mCustomScrollbar("scrollTo", window._offsetMCSLeft);

                if (this.mcs.left < 0 && !$(_firstCSTableColumn).is(':visible'))
                    $(_firstCSTableColumn).show();
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $('#cs-table-2').mCustomScrollbar({
        horizontalScroll: true,
        axis:"x",
        scrollInertia: 0,
        callbacks: {
            onScrollStart: function() {
                if (!window._activeMCS)
                    window._activeMCS = 2;
            },
            onScroll: function() {
                if (window._activeMCS == 2)
                    window._activeMCS = 0;
            },
            whileScrolling:function(){
                window._offsetMCSLeft = this.mcs.left;
                if (window._activeMCS == 2)
                    $('#cs-table-1').mCustomScrollbar("scrollTo", window._offsetMCSLeft);
            }
        },
        advanced:{
            autoExpandHorizontalScroll: 3,
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        mouseWheel:{ enable: false },
    });

    $("[data-collapse-trigger-abc]").click(function() {

        var collapseBtn = this;

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 240);

        var _collapseToggle = function(collapseBtn)
        {
            var target = $(collapseBtn).data('target');
            var collapseCount = $(collapseBtn).hasClass('active') ? $(collapseBtn).data('colspan-close') : $(collapseBtn).data('colspan-open');

            $(collapseBtn).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $('.analysis-table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
        };

        _collapseToggle(collapseBtn);

        $("#cs-table-1").mCustomScrollbar("update");
    });

    $(document).ready(function() {
        $("#cs-table-2 table").width($("#cs-table-1 table").width());
        $("#cs-table-2").mCustomScrollbar("update");
    });

    $('.table-settings [type="checkbox"]').on('change', function () {
        const DATA_CELLS_CNT = 10;
        var collapseBtn = $('#collapse-cell-data');
        var calcColspan = DATA_CELLS_CNT - (
            ($('#config-report_abc_cost_price').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_group_margin').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_number_of_sales').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_average_check').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_purchase_price').prop('checked') ? 0 : 1) +
            ($('#config-report_abc_selling_price').prop('checked') ? 0 : 1)
        );

        $(collapseBtn).data('colspan-open', calcColspan);
        if ($(collapseBtn).hasClass('active')) {
            $(collapseBtn).closest('th').attr('colspan', calcColspan);
        }

        setTimeout(function() {
            $("#cs-table-2 table").width($("#cs-table-1 table").width());
            $("#cs-table-2").mCustomScrollbar("update");
        }, 240);
        $("#cs-table-1").mCustomScrollbar("update");
    });

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
JS
);

if (!empty($products)) {

    $this->registerJs(<<<JS

    var ProductAnalysisTable = {
      fillFixedColumn: function() {

          var tableBlock = $('#cs-table-1');
          var columnBlock = $('#cs-table-first-column');
          // CLEAR
          $(columnBlock).find('thead').html('');
          $(columnBlock).find('tbody').html('');
          $(columnBlock).find('table').width($(tableBlock).find('table tr:last-child td:first-child').width());

          // CLONE FIRST COLUMN
          $('.analysis-table thead tr > th:first-child').each(function(i,v) {
              var col = $(v).clone();
              $('.table-fixed-first-column thead').append($('<tr/>').append(col));

              return false;
          });
          $('.analysis-table tbody tr > td:first-child').each(function(i,v) {
              var col = $(v).clone();
              var trClass = $(v).parent().attr('class') ? (' class="' + $(v).parent().attr('class') + '" ') : '';
              var trDataId = $(v).parent().attr('data-id') ? (' data-id="' + $(v).parent().attr('data-id') + '" ') : '';
              $(col).find('.sortable-row-icon').html('');
              $('.table-fixed-first-column tbody').append($('<tr' + trClass + trDataId + '/>').append(col));
          });

          // ADD "+" EVENTS
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-1').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });
          $('#cs-table-1 [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $('#cs-table-first-column').find('[data-collapse-row-trigger]').filter('[data-target="'+target+'"]').toggleClass('active');
          });

          // main.js
          $('#cs-table-first-column [data-collapse-row-trigger]').click(function() {
              var target = $(this).data('target');
              $(this).toggleClass('active');
              if ( $(this).hasClass('active') ) {
                  $('[data-id="'+target+'"]').removeClass('d-none');
              } else {
                  // level 1
                  $('[data-id="'+target+'"]').addClass('d-none');
                  $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
              }
              if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
                  $('[data-collapse-all-trigger]').removeClass('active');
              } else {
                  $('[data-collapse-all-trigger]').addClass('active');
              }
		  });
          $('#cs-table-first-column [data-collapse-all-trigger]').click(function() {
              var _this = $(this);
              var table = $(this).closest('.table');
              var row = table.find('tr[data-id]');
              _this.toggleClass('active');
              if ( _this.hasClass('active') ) {
                  row.removeClass('d-none');
                  $(table).find('tbody .table-collapse-btn').addClass('active');
                  // row.find('.table-collapse-btn').addClass('active');
              } else {
                  row.addClass('d-none');
                  $(table).find('tbody .table-collapse-btn').removeClass('active');
              }
          });
          // end main.js

        // ADD "+" EVENT TOGGLE ALL
        function tableFixedColumnToggleAll(syncTable, trigger) {
            var row = $(syncTable).find('tr[data-id]');
            console.log(syncTable, trigger)
            if ( $(trigger).hasClass('active') ) {
                row.removeClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(syncTable).find('tbody .table-collapse-btn').removeClass('active');
            }
        }
        $('#cs-table-first-column [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-1');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });
        $('#cs-table-1 [data-collapse-all-trigger]').click(function(e) {
            var syncTable = $('#cs-table-first-column');
            var trigger = $(syncTable).find('[data-collapse-all-trigger]');
            $(trigger).toggleClass('active');
            tableFixedColumnToggleAll(syncTable, trigger);
        });

      }
    };

    // FIXED FIRST COLUMN
    $(document).ready(function() {
        ProductAnalysisTable.fillFixedColumn();
    });
JS
    );
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);