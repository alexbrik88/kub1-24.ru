<?php

use common\components\grid\GridView;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \frontend\modules\reports\models\ProductAnalysisSearch;

/* @var $this \yii\web\View */
/* @var $searchModel ProductAnalysisSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'XYZ анализ по единицам продаж';

$this->registerJs('
    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#productanalysissearch-product_id--filter").val("");
        $("#productanalysissearch-group_id--filter").val($(this).data("group"));
        $("#xyz-analysis-grid").yiiGridView("applyFilter");
    });
    $(document).on("pjax:success", "#xyz-analysis-pjax", function() {
		$(".custom-scroll-table").mCustomScrollbar({
			horizontalScroll: true,
			axis:"x",
			scrollInertia: 300,
			advanced:{
				autoExpandHorizontalScroll: true,
				updateOnContentResize: true,
				updateOnImageLoad: false
			},
			mouseWheel:{ enable: false },
		});
    });
');

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_report_client');
?>

<style type="text/css">
.overal-result-table .result-row { cursor: pointer; }
.overal-result-table .result-row:hover td { background-color: #f1f1f1; }
</style>

<?= $this->render('@frontend/themes/kub/modules/reports/views/layouts/product_analysis_menu') ?>

<div class="wrap pt-2 pb-1 pl-4 pr-3 mt-12px mb-12px">
    <div class="pl-2 pr-2 pb-1">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mt-1 pt-h4-1"><?= $this->title ?></h4>
            </div>
            <div class="column pr-0 pl-1">
                <?= \yii\bootstrap\Html::beginForm(['index'], 'GET', [
                    'validateOnChange' => true,
                ]); ?>
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'year',
                    'data' => $searchModel->getYearFilter(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]); ?>
                <?= \common\components\helpers\Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'xyz-analysis-pjax',
    'timeout' => 10000,
]); ?>

<div class="wrap">
    <div class="row">

        <div class="col-8">
            <table class="table table-style table-count-list overal-result-table table-hover compact-disallow" style="width: auto;">
                <thead>
                <tr class="heading">
                    <th>Группа товаров</th>
                    <th style="text-align: center;">Кол-во типов товаров</th>
                    <th style="text-align: center;">% товаров</th>
                    <th style="text-align: center;">Кол-во товаров на складе</th>
                    <th style="text-align: center;">% товаров на складе</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($searchModel->overallResult as $row) : ?>
                    <?php $color = ProductAnalysisSearch::$groupsColor[$row['group_id']] ?? "#000"; ?>
                    <tr class="result-row" data-group="<?= $row['group_id'] ?>">
                        <td style="color: <?= $color ?>;">
                            <?= $searchModel->getGroupLabel($row['group_id']) ?>
                        </td>
                        <td style="text-align: right;">
                            <?= $row['product_types'] ?>
                        </td>
                        <td style="text-align: right; font-weight: bold; color: <?= $color ?>;">
                            <?= ($searchModel->totalProductTypes > 0) ? round(100 * $row['product_types'] / $searchModel->totalProductTypes) : '' ?>
                        </td>
                        <td style="text-align: right;">
                            <?= round($row['product_quantity'], $row['product_quantity'] == (int)$row['product_quantity'] ? 0 : 3) ?>
                        </td>
                        <td style="text-align: right; font-weight: bold; color: <?= $color ?>;">
                            <?= ($searchModel->totalProductQuantity > 0) ? round(100 * $row['product_quantity'] / $searchModel->totalProductQuantity) : '' ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr class="result-row" data-group="" style="font-weight: bold;">
                    <td>Итого</td>
                    <td style="text-align: right;"><?= $searchModel->totalProductTypes ?></td>
                    <td style="text-align: right;"></td>
                    <td style="text-align: right;"><?= $searchModel->totalProductQuantity ?></td>
                    <td style="text-align: right;"></td>

                </tbody>
            </table>
        </div>

        <?php /*
        <div class="col-4">
            <?php if ($series) : ?>
                <div class="row">
                    <div class="col-12">
                    <?= \miloschuman\highcharts\Highcharts::widget([
                        'htmlOptions' => [
                            'style' => 'height: 180px; padding: 0; margin-top: 22px; margin-bottom: -22px;',
                        ],
                        'options' => [
                            'chart' =>['type' => 'column'],
                            'title' => false,
                            'xAxis' => [
                                'categories' => ['Покупатели', 'Выручка'],
                            ],
                            'yAxis' => [
                                'title' => false,
                            ],
                            'series' => $series,
                            'plotOptions' => [
                                'column' => [
                                    'stacking' => 'percent',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'style' => [
                                            'color' => '#333333',
                                            'textOutline' => null
                                        ],
                                    ],
                                ],
                                'series' => [
                                    'showInLegend' => false,
                                    'borderWidth' => 0,
                                ],
                            ],
                            /////////////////////////////////// STD OPTS ////////////////////////////////////////////////
                            'credits' => [
                                'enabled' => false
                            ],
                            'tooltip' => [
                                'useHtml' => true,
                                //'split' => true,
                                //'shared' => true,
                                'backgroundColor' => "rgba(255,255,255,1)",
                                'borderColor' => '#ddd',
                                'borderWidth' => '1',
                                'borderRadius' => 8,
                                'pointFormat' => '<span class="gray-text">{series.name}: </span><span class="gray-text-b">{point.y} ₽</span>',
                            ],
                            'legend' => [
                                'layout' => 'horizontal',
                                'align' => 'right',
                                'verticalAlign' => 'top',
                                'backgroundColor' => '#fff',
                                'itemStyle' => [
                                    'fontSize' => '11px',
                                    'color' => '#9198a0'
                                ],
                                'symbolRadius' => 2
                            ]
                            //////////////////////////////////////////////////////////////////////////////////////////
                        ]
                    ]); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>*/ ?>

    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row">
            <div class="column">
                <?= TableViewWidget::widget(['attribute' => 'table_view_report_client']) ?>
            </div>
        </div>
    </div>
</div>

<?= GridView::widget([
    'id' => 'xyz-analysis-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'product_id',
            'label' => 'Название',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {

                $title = (mb_strlen($data['product_title'], 'UTF-8') > 100) ? (mb_substr($data['product_title'], 0, 95, 'utf-8') . '...') : $data['product_title'];

                if (Yii::$app->user->can(\frontend\rbac\permissions\Product::VIEW)) {
                    $content = Html::a(Html::encode($title), [
                        '/product/view',
                        'productionType' => $data['production_type'],
                        'id' => $data['product_id'],
                    ], ['target' => '_blank', 'data-pjax' => 0]);
                } else {
                    $content = Html::encode($title);
                }

                return $content;    
            },
            'filter' => $searchModel->getProductFilterItems(),
            's2width' => '250px'
        ],
        [
            'attribute' => 'variation_coefficient',
            'label' => 'Коеффициент вариации',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ($data['variation_coefficient'] < 999999) ? (round($data['variation_coefficient'], 0) . '%') : '';
            },
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа',
            'headerOptions' => [
                'width' => '10%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return $data['variation_coefficient'] <= 10 ? 'X' : ($data['variation_coefficient'] <= 25 ? 'Y' : 'Z');
            },
            'filter' => ['' => 'Все'] + ProductAnalysisSearch::$groups,
            's2width' => '250px'
        ],
        [
            'attribute' => 'month_0',
            'label' => ProductAnalysisSearch::getMonthName(0),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(0, $data['first_date'], $data['month_0']);
            },
        ],
        [
            'attribute' => 'month_1',
            'label' => ProductAnalysisSearch::getMonthName(1),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(1, $data['first_date'], $data['month_1']);
            },
        ],
        [
            'attribute' => 'month_2',
            'label' => ProductAnalysisSearch::getMonthName(2),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(2, $data['first_date'], $data['month_2']);
            },
        ],
        [
            'attribute' => 'month_3',
            'label' => ProductAnalysisSearch::getMonthName(3),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(3, $data['first_date'], $data['month_3']);
            },
        ],
        [
            'attribute' => 'month_4',
            'label' => ProductAnalysisSearch::getMonthName(4),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(4, $data['first_date'], $data['month_4']);
            },
        ],
        [
            'attribute' => 'month_5',
            'label' => ProductAnalysisSearch::getMonthName(5),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(5, $data['first_date'], $data['month_5']);
            },
        ],
        [
            'attribute' => 'month_6',
            'label' => ProductAnalysisSearch::getMonthName(6),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(6, $data['first_date'], $data['month_6']);
            },
        ],
        [
            'attribute' => 'month_7',
            'label' => ProductAnalysisSearch::getMonthName(7),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(7, $data['first_date'], $data['month_7']);
            },
        ],
        [
            'attribute' => 'month_8',
            'label' => ProductAnalysisSearch::getMonthName(8),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(8, $data['first_date'], $data['month_8']);
            },
        ],
        [
            'attribute' => 'month_9',
            'label' => ProductAnalysisSearch::getMonthName(9),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(9, $data['first_date'], $data['month_9']);
            },
        ],
        [
            'attribute' => 'month_10',
            'label' => ProductAnalysisSearch::getMonthName(10),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(10, $data['first_date'], $data['month_10']);
            },
        ],
        [
            'attribute' => 'month_11',
            'label' => ProductAnalysisSearch::getMonthName(11),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(11, $data['first_date'], $data['month_11']);
            },
        ],
        [
            'attribute' => 'month_12',
            'label' => ProductAnalysisSearch::getMonthName(12),
            'headerOptions' => [
                'width' => '1%'
            ],
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'format' => 'raw',
            'value' => function ($data) {
                return ProductAnalysisSearch::getMonthValue(12, $data['first_date'], $data['month_12']);
            },
        ],

        //[
        //    'attribute' => 'coef',
        //    'headerOptions' => [
        //        'width' => '10%',
        //    ],
        //    'contentOptions' => [
        //        'class' => 'text-right',
        //    ],
        //    'format' => 'raw',
        //    'value' => function ($data) {
        //        return $data['coef'];
        //    },
        //],
        //[
        //    'attribute' => 'group',
        //    'headerOptions' => [
        //        'width' => '10%',
        //    ],
        //    'contentOptions' => [
        //        'class' => 'text-right',
        //    ],
        //    'filter' => ['' => 'Все'] + AnalysisSearch::$groups,
        //    'value' => 'groupValue',
        //    's2width' => '250px'
        //],
    ],
]); ?>

<?php $pjax->end(); ?>
