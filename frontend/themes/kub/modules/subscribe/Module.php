<?php

namespace frontend\modules\subscribe;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\subscribe\controllers';

    public function init()
    {
        parent::init();
    }
}
