<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 4:27 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\models\service;
use php_rutils\RUtils;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \frontend\modules\subscribe\forms\OnlinePaymentForm $paymentForm */

$this->title = 'Онлайн платёж';
$subscribeDurationTextVariants = ['месяц', 'месяца', 'месяцев',];
$payment = $paymentForm->payment;
$tariff = $payment->tariff;
$companyCount = count($payment->orders);
$companyCountTextItems = ['компанию', 'компании', 'компаний'];
?>

<div class="text-left">
    <h1>Оплата подписки</h1>

    <br/>

    <p class="text-success font-size-20"><?= $paymentForm->Desc; ?></p>

    Оплата сервиса kub-24.ru на
    <?= $tariff->duration_month . ' ',
        RUtils::numeral()->choosePlural($tariff->duration_month, $subscribeDurationTextVariants),
        " за {$companyCount} ",
        RUtils::numeral()->choosePlural($companyCount, $companyCountTextItems);
    ?>.
    <br>
    Сумма к оплате <?= $paymentForm->OutSum ?> руб.

    <form id="robokassa-form"
          action="<?= \Yii::$app->params['robokassa']['merchantFormAction']; ?>" method="POST">
        <?php foreach ($paymentForm->getFormFields() as $name => $value) {
            echo Html::hiddenInput($name, $value);
        } ?>

        <br/>
        <br/>

        <div class="row">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 13.5%;">
                <?= Html::submitButton('<span class="ladda-label">Оплатить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-rub"></i>', [
                    'class' => 'btn btn-primary widthe-100 hidden-lg',
                    'title' => 'Оплатить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::a('Отменить', ['@subscribeUrl'], [
                    'class' => 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::a('<i class="fa fa-reply fa-2x"></i>', ['@subscribeUrl'], [
                    'class' => 'btn btn-primary widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </form>

</div>