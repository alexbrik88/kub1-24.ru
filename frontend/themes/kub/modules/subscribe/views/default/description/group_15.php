<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="mb-3">
    <div class="mb-2">В тариф входит:</div>
    <b>Кол-во пользователей:</b> 3
    <br>
    <b>Аналитика, отчеты и графики по разделам</b>
    <div class="ml-3">
        <b>Продажи</b>
        <ul class="mb-0">
            <li>
                Дашборд
            </li>
            <li>
                Воронка продаж
            </li>
            <li>
                АВС анализ по покупателям
            </li>
            <li>
                Платежная дисциплина
            </li>
            <li>
                Отчеты по должникам
            </li>
            <li>
                Отчеты по сотрудникам
            </li>
            <li>
                Еще 10 отчетов
            </li>
        </ul>
    </div>
</div>
