<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="row">
    <div class="col-6 mb-3">
        <div class="text-nowrap font-weight-bold mb-2" style="font-size: 18px">
            Прайс лист Простой
        </div>
        <div class="mb-2">В тариф входит:</div>
        <table style="width: 100%;">
            <tr>
                <td>Количество прайс-листов:</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Количество товаров в прайс-листе:</td>
                <td>до 100</td>
            </tr>
            <tr>
                <td>Онлайн обновления цены и остатков:</td>
                <td>НЕТ</td>
            </tr>
            <tr>
                <td>Просмотр карточки товара:</td>
                <td>НЕТ</td>
            </tr>
            <tr>
                <td>Онлайн оформление заказов:</td>
                <td>НЕТ</td>
            </tr>
            <tr>
                <td>Получение уведомлений:</td>
                <td>НЕТ</td>
            </tr>
            <tr>
                <td>Тариф «Выставление счетов»</td>
                <td>НЕ входит</td>
            </tr>
        </table>
    </div>

    <div class="col-6 mb-3">
        <div class="text-nowrap font-weight-bold mb-2" style="font-size: 18px">
            Прайс лист Онлайн
        </div>
        <div class="mb-2">В тариф входит:</div>
        <table style="width: 100%;">
            <tr>
                <td>Количество прайс-листов:</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Количество товаров в прайс-листе:</td>
                <td>до 500</td>
            </tr>
            <tr>
                <td>Онлайн обновления цены и остатков:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Просмотр карточки товара:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Онлайн оформление заказов:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Получение уведомлений:</td>
                <td>НЕТ</td>
            </tr>
            <tr>
                <td>Тариф «Выставление счетов»</td>
                <td>до 100 счетов</td>
            </tr>
        </table>
    </div>

    <div class="col-6 mb-3">
        <div class="text-nowrap font-weight-bold mb-2" style="font-size: 18px">
            Прайс лист Продающий
        </div>
        <div class="mb-2">В тариф входит:</div>
        <table style="width: 100%;">
            <tr>
                <td>Количество прайс-листов:</td>
                <td>Безлимит</td>
            </tr>
            <tr>
                <td>Количество товаров в прайс-листе:</td>
                <td>до 2000</td>
            </tr>
            <tr>
                <td>Онлайн обновления цены и остатков:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Просмотр карточки товара:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Онлайн оформление заказов:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Получение уведомлений:</td>
                <td>ДА</td>
            </tr>
            <tr>
                <td>Тариф «Выставление счетов»</td>
                <td>Безлимит</td>
            </tr>
        </table>
    </div>
</div>
