<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

$description = trim($group->description);
?>

<?php if ($description) : ?>
<pre class="mb-3">
<?= strtr($description, ['|' => "\n"]) ?>
</pre>
<?php endif ?>
