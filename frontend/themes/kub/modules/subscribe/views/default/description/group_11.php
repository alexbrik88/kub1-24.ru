<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="mb-3">
    <div class="mb-2">В тариф входит:</div>
    <b>Кол-во пользователей:</b> 3
    <br>
    <b>Аналитика, отчеты и графики по разделам</b>
    <div class="ml-3">
        <b>Контроль денег</b>
        <ul class="mb-0">
            <li>
                Отчет о Движении Денег
            </li>
            <li>
                Платежный календарь
            </li>
            <li>
                План-Факт
            </li>
            <li>
                Анализ Приходов
            </li>
            <li>
                Анализ Расходов
            </li>
        </ul>
        <b>Эффективность</b>
        <ul class="mb-0">
            <li>
                Отчет о Прибылях и Убытках
            </li>
            <li>
                Точка Безубыточности
            </li>
        </ul>
        <b>Где деньги</b>
        <ul class="mb-0">
            <li>
                Баланс
            </li>
            <li>
                Нам должны
            </li>
            <li>
                Мы должны
            </li>
            <li>
                Запасы
            </li>
            <li>
                Кредиты
            </li>
        </ul>
    </div>
    <b>Тариф «Выставление счетов»</b> до 100 счетов
</div>
