<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="mb-3">
    <div class="mb-2">В тариф входит:</div>
    <b>Кол-во пользователей:</b> 3
    <br>
    <b>Аналитика, отчеты и графики по разделам</b>
    <div class="ml-3">
        <b>Маркетинг</b>
        <ul class="mb-0">
            <li>
                Дашборд
            </li>
            <li>
                Аналитика по ЯндексДирект
            </li>
            <li>
                Аналитика по Google Adwords
            </li>
            <li>
                Аналитика по FaceBook & Instagrm
            </li>
            <li>
                Аналитика по ВКонтакте
            </li>
            <li>
                Аналитика по ЯндексМаркет
            </li>
            <li>
                Аналитика по MyTarget
            </li>
            <li>
                Аналитика по Creteo
            </li>
            <li>
                Уведомления
            </li>
        </ul>
    </div>
</div>
