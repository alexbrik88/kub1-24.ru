<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="mb-3">
    <div class="mb-2">В тариф входит:</div>
    <b>Кол-во пользователей:</b> 3
    <br>
    <b>Аналитика, отчеты и графики по разделам</b>
    <div class="ml-3">
        <b>Товары</b>
        <ul class="mb-0">
            <li>
                Дашборд
            </li>
            <li>
                АВС анализ по 3-м параметрам
            </li>
            <li>
                XYZ анализ
            </li>
            <li>
                Анализ запасов на складе
            </li>
            <li>
                Скорость продаж
            </li>
            <li>
                Товары в пути
            </li>
            <li>
                Еще 5 отчетов
            </li>
        </ul>
    </div>
</div>
