<?php

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */

?>

<div class="mb-3">
    <div class="mb-2">В тариф входит:</div>
    <div class="row">
        <div class="col-6 mb-2">
            <div class="descr_title">Документы</div>
            <div>
                Выставление Счетов
                <br>
                Подготовка Актов, Товарных Накладных, Счетов-фактур, УПД
                <br>
                Подготовка Доверенностей на получение товара
                <br>
                Добавление в счет логотипа, печати и подписи
                <br>
                Заполнение реквизитов по ИНН
                <br>
                Отправка счетов на e-mail клиентам
                <br>
                АвтоСчета и АвтоАкты
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Конструктор договоров</div>
            <div>
                Подготовка шаблона договора
                <br>
                Автоматическое заполнение реквизитов в шаблоне договора
                <br>
                Отправка Договора на e-mail клиента
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Банк</div>
            <div>
                Загрузка выписок из банка
                <br>
                Формирование платежек
                <br>
                Отправка платежек в банк
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Касса</div>
            <div>
                Внесение операций по кассе
                <br>
                Формирование и печать ПКО и РКО
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Контрагенты</div>
            <div>
                Список Покупателей и Поставщиков
                <br>
                Акты сверки с контрагентами
                <br>
                Анализ продаж по Покупателям
                <br>
                Анализ закупок по Поставщикам
                <br>
                АВС анализ по покупателям
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Должники</div>
            <div>
                Отчеты по должникам
                <br>
                Отчет «Платежная дисциплина»
                <br>
                Автоматическая отправка писем должникам о долге
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Учёт товаров и услуг</div>
            <div>
                Складские остатки
                <br>
                Анализ продаж по товарам
                <br>
                Анализ продаж по Покупателям
            </div>
        </div>
        <div class="col-6 mb-2">
            <div class="descr_title">Разное</div>
            <div>
                Многопользовательский режим
                <br>
                Отчеты по сотрудникам
                <br>
                <b>Выгрузка всех данных в 1С</b>
            </div>
        </div>
    </div>
</div>
