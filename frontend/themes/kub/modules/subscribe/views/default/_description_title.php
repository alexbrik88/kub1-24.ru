<?php

use frontend\components\Icon;

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */
/* @var $tariffLabel string */

?>

<div class="mb-0 d-flex">
    <div class="tariff-icon-wrap">
        <div class="tariff-icon">
            <?= Icon::get($group->icon) ?>
        </div>
    </div>
    <div class="tariff-group-name" style="min-height: auto;">
        <?= $tariffLabel ?>
    </div>
</div>