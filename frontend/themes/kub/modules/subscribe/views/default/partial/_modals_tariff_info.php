<?php // 1. ТАРИФ "Выставление счетов" ?>
<div class="modal fade" id="tariff-info-1" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Выставление счетов» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-6">
                        <p class="bold-italic">Документы</p>
                        <p>Выставление счетов.</p>
                        <p>Заполнение реквизитов по ИНН.</p>
                        <p>Отправка счетов на e-mail клиентам</p>
                        <p>Добавление в счет логотипа, печати и подписи.</p>
                        <p>АвтоСчета</p>
                        <p>АвтоАкты</p>
                        <p>Подготовка Актов, Товарных Накладных, Счетов-фактур, УПД в 1 клик</p>
                        <p class="bold-italic">Контрагенты</p>
                        <p>Акты сверки с контрагентами</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Должники</p>
                        <p>Контроль должников.</p>
                        <p>Автоматическая отправка писем должникам.</p>
                    </div>
                    <div class="col-md-6">
                        <p class="bold-italic">Банк</p>
                        <p>Загрузка выписки из банка</p>
                        <p>Формирование платежек</p>
                        <p>Отправка платежек в банк</p>
                        <p class="bold-italic">Касса</p>
                        <p>Формирование и печать ПКО и РКО</p>
                        <p class="bold-italic">Учёт товаров</p>
                        <p>Складские остатки</p>
                        <p>Анализ продаж по товарам</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Многопользовательский режим</p>
                        <p>Отчеты по сотрудникам</p>
                        <p class="bold-italic">Выгрузка всех данных в 1С</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 2. ТАРИФ "ЛОГИСТИКА" ?>
<div class="modal fade" id="tariff-info-2" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «ЛОГИСТИКА» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-4">
                        <p class="bold-italic">Документы</p>
                        <p>Выставление счетов.</p>
                        <p>Заполнение реквизитов по ИНН.</p>
                        <p>Отправка счетов на e-mail клиентам</p>
                        <p>Добавление в счет логотипа, печати и подписи.</p>
                        <p>АвтоСчета</p>
                        <p>АвтоАкты</p>
                        <p>Подготовка Актов, Товарных Накладных, Счетов-фактур, УПД в 1 клик</p>
                        <p class="bold-italic">Контрагенты</p>
                        <p>Акты сверки с контрагентами</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Должники</p>
                        <p>Контроль должников.</p>
                        <p>Автоматическая отправка писем должникам.</p>
                    </div>
                    <div class="col-md-4">
                        <p class="bold-italic">Банк</p>
                        <p>Загрузка выписки из банка</p>
                        <p>Формирование платежек</p>
                        <p>Отправка платежек в банк</p>
                        <p class="bold-italic">Касса</p>
                        <p>Формирование и печать ПКО и РКО</p>
                        <p class="bold-italic">Учёт товаров</p>
                        <p>Складские остатки</p>
                        <p>Анализ продаж по товарам</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Многопользовательский режим</p>
                        <p>Отчеты по сотрудникам</p>
                        <p class="bold-italic">Выгрузка всех данных в 1С</p>
                    </div>
                    <div class="col-md-4">
                        <p class="bold-italic red">Логистика</p>
                        <p>Заявки</p>
                        <p>Договора-заявки</p>
                        <p>Договора</p>
                        <p>ТТН</p>
                        <p>Путевые листы</p>
                        <p>Учет транспорта</p>
                        <p>Учет водителей</p>
                        <p>Маршруты</p>
                        <p>Отчеты</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 3. ТАРИФ "В2В" ?>
<div class="modal fade" id="tariff-info-3" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Модуль B2B» оплат входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-4">
                        <p class="bold-italic">Документы</p>
                        <p>Выставление счетов.</p>
                        <p>Заполнение реквизитов по ИНН.</p>
                        <p>Отправка счетов на e-mail клиентам</p>
                        <p>Добавление в счет логотипа, печати и подписи.</p>
                        <p>АвтоСчета</p>
                        <p>АвтоАкты</p>
                        <p>Подготовка Актов, Товарных Накладных, Счетов-фактур, УПД в 1 клик</p>
                        <p class="bold-italic">Контрагенты</p>
                        <p>Акты сверки с контрагентами</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Должники</p>
                        <p>Контроль должников.</p>
                        <p>Автоматическая отправка писем должникам.</p>
                    </div>
                    <div class="col-md-4">
                        <p class="bold-italic">Банк</p>
                        <p>Загрузка выписки из банка</p>
                        <p>Формирование платежек</p>
                        <p>Отправка платежек в банк</p>
                        <p class="bold-italic">Касса</p>
                        <p>Формирование и печать ПКО и РКО</p>
                        <p class="bold-italic">Учёт товаров</p>
                        <p>Складские остатки</p>
                        <p>Анализ продаж по товарам</p>
                        <p>Анализ продаж по клиентам</p>
                        <p class="bold-italic">Многопользовательский режим</p>
                        <p>Отчеты по сотрудникам</p>
                        <p class="bold-italic">Выгрузка всех данных в 1С</p>
                    </div>
                    <div class="col-md-4">
                        <p class="bold-italic red">На вашем сайте</p>
                        <p>Выставление счетов для ООО и ИП</p>
                        <p>Прием онлайн платежей от ООО и ИП<p>
                        <p>Подготовка покупателем Доверенности а получение у Вас оплаченного товара</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 4. ТАРИФ "Налоги + Декларация ИП на УСН 6%" ?>
<div class="modal fade" id="tariff-info-4" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Налоги + Декларация ИП на УСН 6%» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <p>Автоматический расчёт налогов</p>
                        <p>Автоматическая подготовка налоговых платёжек.</p>
                        <p>Отчётность в налоговую, кроме отчётов за сотрудников.</p>
                        <p>Автоматическая подготовка налоговой декларации и КУДиР</p>
                        <p>Подготовка описи.</p>
                        <p>Напоминания о сроках отчётности</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 7. ТАРИФ "Налоги ИП на УСН 6%" ?>
<div class="modal fade" id="tariff-info-7" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Налоги ИП на УСН 6%» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <p>Автоматический расчёт налогов</p>
                        <p>Автоматическая подготовка налоговых платёжек.</p>
                        <p>Напоминания о сроках отчётности</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 5. ТАРИФ "АНАЛИТИКА" ?>
<div class="modal fade" id="tariff-info-5" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Аналитика по бизнесу» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-6">
                        <p class="bold-italic">Отчеты</p>
                        <p>Отчет о Движении Денежных Средств</p>
                        <p>Платежный календарь</p>
                        <p>Отчет о Прибылях и Убытках</p>
                        <p>Баланс</p>
                        <p>Отчет по затратам</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Отчеты по продажам</p>
                        <p>Анализ продаж по товарам/услугам</p>
                        <p>Анализ продаж по клиентам</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Отчеты по контрагентам</p>
                        <p>Отчет по должникам</p>
                        <p>АВС анализ</p>
                        <p>Платежная дисциплина</p>
                    </div>
                    <div class="col-md-6">
                        <p class="bold-italic">Банк</p>
                        <p>Загрузка выписки из банка</p>
                        <p class="bold-italic">Касса</p>
                        <p>Загрузка данных из ОФД</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Отчеты по товарам и услугам</p>
                        <p>АВС анализ</p>
                        <p>XYZ анализ</p>
                        <p>Оборачиваемость товара</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Отчеты по сотрудникам</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Точка безубыточности</p>
                        <p>&nbsp;</p>
                        <p class="bold-italic">Многопользовательский режим</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // 6. ТАРИФ "Нулевая отчетность ООО на ОСНО" ?>
<div class="modal fade" id="tariff-info-6" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;" aria-hidden="true"></button>
                В тариф «Нулевая отчетность ООО на ОСНО» входит
            </div>
            <div class="modal-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <p>Автоматическая подготовка отчетности в налоговую</p>
                        <p>Автоматическая подготовка отчетности по сотрудникам</p>
                        <p>Подготовка описи</p>
                        <p>Напоминания о сроках отчетности</p>
                    </div>
                    <div class="col-md-12" style="text-align: center">
                        <a class="btn" data-dismiss="modal" style="background-color:#3175af; color:#fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>