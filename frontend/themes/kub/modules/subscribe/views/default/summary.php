<?php

use frontend\modules\subscribe\forms\PaymentForm;
use frontend\themes\kub\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model frontend\modules\subscribe\forms\PaymentForm */

$model->validate();
$durationMonth = (int) $model->durationMonth;
$payment = $model->getNewPayment();
$orders = [];
foreach ($payment->orders as $order) {
    $name = $order->tariff->tariffGroup->name;
    $orders[$name] = ($orders[$name] ?? 0) + $order->sum;
}
?>
<div id="selected_tariff_sum" data-sum="<?=$payment->sum ?>"></div>
<div class="d-flex justify-content-between pt-2 bor-t">
    <div>
        Количество компаний
    </div>
    <b class="ml-2">
        <?= $model->payMultiple ?>
    </b>
</div>

<?php if ($orders) : ?>
    <div class="my-2 bor-t">
        <?php foreach ($orders as $name => $sum) : ?>
            <div class="d-flex justify-content-between mt-2">
                <div>
                    <?= strtr($name, ['.' => '<br>']) ?>
                </div>
                <b class="ml-2">
                    <?= number_format($sum, 0, ',', '&nbsp;') ?>&nbsp;₽
                </b>
            </div>
        <?php endforeach ?>
    </div>
<?php endif ?>

<?php if ($durationMonth) : ?>
    <div class="d-flex justify-content-between mt-1 pt-2 bor-t">
        <div>
            Итого / месяц
        </div>
        <b class="ml-2">
            <?= number_format(round($payment->sum / $durationMonth), 0, ',', '&nbsp;') ?>&nbsp;₽
        </b>
    </div>
    <?php if ($durationMonth == 1) : ?>
        <div class="text-danger mt-2">
            Получитe скидку 10% при оплате за 4 месяца
        </div>
    <?php elseif ($durationMonth == 4) : ?>
        <div class="text-danger mt-2">
            Получитe скидку 20% при оплате за год
        </div>
    <?php endif ?>
    <div class="d-flex justify-content-between mt-2">
        <div class="font-weight-bold">
            Итого к оплате
            <br>
            за <?= php_rutils\RUtils::numeral()->getPlural($durationMonth, ['месяц', 'месяца', 'месяцев']); ?>
        </div>
        <b class="ml-2">
            <?= number_format($payment->sum, 0, ',', '&nbsp;') ?>&nbsp;₽
        </b>
    </div>
<?php endif ?>
