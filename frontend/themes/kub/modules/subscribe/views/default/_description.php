<?php

use common\models\service\SubscribeTariffGroup as Group;
use frontend\components\Icon;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $group common\models\service\SubscribeTariffGroup */
/* @var $tariffLabel string */
/* @var $tariffData array */
/* @var $dropdownData array */

try {
    echo $this->render('description/group_'.$group->id, [
        'group' => $group,
    ]);
} catch (Exception $e) {
    echo $this->render('description/default', [
        'group' => $group,
    ]);
}
$data = [];
if (!empty($dropdownData)) {
    foreach ($dropdownData as $key => $label) {
        foreach ($tariffData[$key] as $tariff) {
            $data[$tariff->duration_month][] = [
                'key' => $key,
                'tariff' => $tariff,
            ];
        }
    }
}
$dataCount = count($data);
?>

<div class="price_details">
    <div class="mb-3">
        Количество компаний
        <?= Html::textInput('paymentCount', 1, [
            'type' => 'number',
            'min' => 1,
            'step' => 1,
            'class' => 'price_details_payment_count form-control-number',
            'style' => 'width: 100px;'
        ]) ?>
    </div>

    <?php if ($group->getIsPayPerQuantity() && $dataCount > 0) : ?>
        <?php foreach ($data as $d => $dataByDuration) : ?>
            <div class="text-nowrap font-weight-bold">
                <?php if ($dataCount == 1) : ?>
                    Срок, в течение которого нужно израсходовать
                    <?= Group::$limitItemLabel[$group->id]['many'] ?? null; ?>:
                    <?= php_rutils\RUtils::numeral()->getPlural($d, ['месяц', 'месяца', 'месяцев']); ?>
                <?php else : ?>
                    При оплате за
                    <?= php_rutils\RUtils::numeral()->getPlural($d, ['месяц', 'месяца', 'месяцев']); ?>
                <?php endif ?>
            </div>
            <div class="d-flex mt-3 mr-n5 mb-3">
                <?php foreach ($dataByDuration as $value) : ?>
                    <?php
                    $label = $dropdownData[$value['key']];
                    $price = $value['tariff']->price;
                    $limit = $value['tariff']->tariff_limit;
                    ?>
                    <div class="mr-5 tariff_price_details" data-tariff="<?= $value['tariff']->id ?>">
                        <div class="text-nowrap">
                            <?= $label ?>
                        </div>
                        <div class="text-nowrap mt-2">
                            <span class="price_details_item" style="font-size: 28px;" data-value="<?= $price ?>">
                                <?= number_format($price, 0, ',', ' ') ?>
                            </span>
                            ₽
                        </div>
                        <div class="text-nowrap mt-1">
                            <span class="price_per_item price_details_item"  data-value="<?= round($price/$limit) ?>">
                                <?= number_format(round($price/$limit), 0, ',', ' ') ?>
                            </span>
                            ₽ /
                            <?= $group->perOneLabel() ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    <?php elseif (!empty($dropdownData)) : ?>
        <?php foreach ($dropdownData as $key => $label) : ?>
            <div class="text-nowrap font-weight-bold mt-3" style="font-size: 18px">
                <?= $label ?>
            </div>
            <div class="d-flex mt-2 mr-n5 mb-3">
                <?php foreach ($tariffData[$key] as $tariff) : ?>
                    <?php $d = $tariff->duration_month ?>
                    <div class="mr-5 tariff_price_details" data-tariff="<?= $tariff->id ?>">
                        <div class="text-nowrap">
                            При оплате за
                            <?= php_rutils\RUtils::numeral()->getPlural($d, ['месяц', 'месяца', 'месяцев']); ?>
                        </div>
                        <div class="text-nowrap mt-2">
                            <span class="price_details_item" style="font-size: 28px;" data-value="<?= $tariff->price ?>">
                                <?= number_format($tariff->price, 0, ',', ' ') ?>
                            </span>
                            ₽
                        </div>
                        <div class="text-nowrap mt-1">
                            <span class="price_per_item price_details_item" data-value="<?= round($tariff->price/$d) ?>">
                                <?= number_format(round($tariff->price/$d), 0, ',', ' ') ?>
                            </span>
                            ₽ / в месяц
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    <?php else : ?>
        <div class="d-flex mr-n5 mb-3">
            <?php foreach (reset($tariffData) as $tariff) : ?>
                <?php $d = $tariff->duration_month ?>
                <div class="mr-5 tariff_price_details" data-tariff="<?= $tariff->id ?>">
                    <div class="text-nowrap">
                        При оплате за
                        <?= php_rutils\RUtils::numeral()->getPlural($d, ['месяц', 'месяца', 'месяцев']); ?>
                    </div>
                    <div class="text-nowrap mt-2">
                        <span class="price_details_item" style="font-size: 28px;" data-value="<?= $tariff->price ?>">
                            <?= number_format($tariff->price, 0, ',', ' ') ?>
                        </span>
                        ₽
                    </div>
                    <div class="text-nowrap mt-1">
                        <span class="price_per_item price_details_item" data-value="<?= round($tariff->price/$d) ?>">
                            <?= number_format(round($tariff->price/$d), 0, ',', ' ') ?>
                        </span>
                        ₽ / в месяц
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>
