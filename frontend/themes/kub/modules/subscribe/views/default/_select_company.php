<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\company\CompanyType;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var $model frontend\modules\subscribe\forms\PaymentForm */
/** @var $form yii\bootstrap4\ActiveForm */
/** @var $company common\models\Company */
/** @var $companyArray array */
/** @var $tariffArray array */

$companyTariffDiscounts = [];
$companyList = [];
foreach ($companyArray as $company) {
    $companyList[$company->id] = $company->shortName;
    $companyTariffDiscounts[$company->id] = [];
    foreach ($tariffArray as $tariff) {
        if ($tariff->group->activeByCompany($company)) {
            $companyTariffDiscounts[$company->id][$tariff->id] = $discount = $company->getDiscount($tariff->id);
            if ($tariff->id == SubscribeTariffGroup::STANDART && $discount) {
                $companyList[$company->id] .= " <span style=\"font-weight: bold; color: red;\">Скидка -{$discount}%</span>";
            }
        }
    }
}

$paymentTypeArray = [
    [
        'id' => PaymentType::TYPE_ONLINE,
        'title' => 'Онлайн оплата',
        'view' => 'payment_type/online',
    ],
    [
        'id' => PaymentType::TYPE_INVOICE,
        'title' => 'Выставить счёт',
        'view' => 'payment_type/take_invoice',
    ],
    [
        'id' => PaymentType::TYPE_REWARD,
        'title' => 'Использовать вознаграждение',
        'view' => 'payment_type/reward',
    ],
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<?php $pjax = Pjax::begin([
    'id' => 'subscribe-tariff-block-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => false,
]); ?>

<div class="row">
    <div class="col-9">
        <div class="row">
            <?= $form->field($model, 'companyList', [
                'template' => "{label}\n<br>\n{input}\n{hint}\n{error}",
                'options' => [
                    'class' => 'col-12'
                ],
                'labelOptions' => [
                    'class' => 'label mb-3',
                ],
            ])->checkboxList($companyList, [
                'class' => 'row',
                'item' => function ($index, $label, $name, $checked, $value) use ($companyTariffDiscounts) {
                    $tarifDiscounts = ArrayHelper::getValue($companyTariffDiscounts, $value, []);

                    return Html::tag('label', Html::checkbox($name, $checked, [
                            'value' => $value,
                            'class' => 'company-id-checker',
                            'style' => 'display:block; float:left',
                            'data' => [
                                'tariffs' => array_keys($tarifDiscounts),
                                'discounts' => $tarifDiscounts,
                            ],
                        ]) . '<div style="display:block;margin-left:24px">'.$label.'</div>', [
                        'class' => 'col-4',
                    ]);
                },
            ]); ?>
        </div>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'payExtra')->textInput([
            'type' => 'number',
            'class' => 'form-control-number',
            'min' => 0,
            'max' => $model::PAY_EXTRA_MAX,
        ])->label($model->getAttributeLabel('payExtra').Html::tag('span', Icon::get('question', [
            'class' => 'tooltip-question-icon ml-1 tooltip-click',
            'data-tooltip-content' => '#pay-extra-tooltip',
        ]))) ?>
        <?php if ($model->getIsAdmin()) : ?>
            <?= $form->field($model, 'adminDiscount')->textInput([
                'type' => 'number',
                'class' => 'form-control-number',
                'min' => 0,
                'max'=> '99.9999',
                'step' => 'any',
            ]) ?>
        <?php endif ?>
    </div>
</div>

<?php $pjax->end(); ?>