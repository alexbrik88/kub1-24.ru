<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 4:27 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\product\PriceList;
use common\models\service;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\modules\subscribe\assets\SubscribeAsset;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\models\PaymentSearch $paymentSearchModel */
/* @var service\Subscribe[] $actualSubscribes */
/* @var common\models\Company $company */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $tariffId integer */
/* @var $model frontend\modules\subscribe\forms\PaymentForm */

$this->title = 'Оплата сервиса';

SubscribeAsset::register($this);

if (!isset($companyArray)) {
    $companyArray = \Yii::$app->user->identity->getCompanies()
        ->alias('company')
        ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
        ->isBlocked(false)
        ->orderBy([
            new \yii\db\Expression('IF({{company}}.[[id]] = :id, 0, 1) ASC'),
            new \yii\db\Expression("ISNULL({{companyType}}.[[name_short]])"),
            "companyType.name_short" => SORT_ASC,
            "company.name_short" => SORT_ASC,
        ])
        ->params([':id' => $company->id])
        ->all();
}

$hasIp = false;
foreach ($companyArray as $c) {
    if ($c->getIsLikeIP()) {
        $hasIp = true;
        break;
    }
}
$isOneCompany = count($companyArray) == 1;

$paymentDataProvider->pagination->setPageSize(Yii::$app->request->get('per-page', 10));

$formData = $model->getFormData($hasIp);

$session = Yii::$app->session;
$pdfUid = $session->remove('paymentDocument.uid');
$done = (Yii::$app->request->get('status') == 'done');
$paymentType = Yii::$app->request->get('payment-type');
$left = $company->getInvoiceLeft();

if ($done && $pdfUid) {
    $this->registerJs('
        var link = document.createElement("a");
        link.href = "' . Url::to(['document-print', 'uid' => $pdfUid]) . '"
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
    ');
}

if ($done && $paymentType == 3) {
    $this->registerJs('
       $(document).ready(function() {
        $("#info-after-pay").modal("show");
    })
    ');
}

$tariffArray = ArrayHelper::getValue($formData, 'tariffArray', []);
$tariffPrices = ArrayHelper::getValue($formData, 'tariffPrices', []);
$discountByCount = ArrayHelper::getValue($formData, 'discountByCount', []);
$groupArray = ArrayHelper::getValue($formData, 'groupArray', []);
$tariffDurations = ArrayHelper::getValue($formData, 'tariffDurations', []);
$tariffPerQuantity = ArrayHelper::getValue($formData, 'tariffPerQuantity', []);
$limit = SubscribeTariff::find()->select('tariff_limit')->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::STANDART,
])->orderBy(['price' => SORT_ASC])->scalar();

$expiresArray = [];
foreach ($groupArray as $group) {
    $subscribes = service\SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
    if ($subscribes) {
        $expiresArray[$group->id] = [
            'name' => $group->name,
            'date' => $date = service\SubscribeHelper::getExpireDate($subscribes),
            'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => service\SubscribeHelper::getExpireLeftDays($date),
            ]),
            'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
        ];
    }
}
$groupsSelected = Yii::$app->getRequest()->post('tariff_group_id', [SubscribeTariffGroup::STANDART]);
$durationList = (array) array_unique(ArrayHelper::getColumn($tariffArray, 'duration_month'));
rsort($durationList);
$defaultDuration = max($durationList);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => false,
        'interactive' => true,
    ],
]);
?>

<?= Yii::$app->session->getFlash('emptyCompany'); ?>

<?= $this->render('_style') ?>

<div class="alert alert-danger hidden" role="alert">
    В связи с работами на стороне Мерчанта Робокасса, оплата картами физ.лиц временно не доступна.
    Просим производить платежи с расчетного счета компании, по клику на кнопку ниже «Выставить счет».
    Приносим извинения за доставленные неудобства.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="wrap mb-0 pb-0" style="border-bottom-left-radius: 0; border-bottom-right-radius: 0;">
    <div class="row">
        <div class="column">
            <h4>Оплатить подписку</h4>
        </div>
        <div class="column ml-auto">
            <?= $this->render('_promoCodeForm', [
                'promoCodeForm' => new \frontend\modules\subscribe\forms\PromoCodeForm(),
            ]); ?>
        </div>
    </div>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'subscribe-tariff-form',
    'action' => ['payment'],
    'validationUrl' => ['validate'],
    'enableAjaxValidation' => true,
    'options' => [
        'data' => [
            'discounts' => $discountByCount,
            'prices' => $tariffPrices,
            'durations' => $tariffDurations,
            'durationlist' => $durationList,
            'quantity' => $tariffPerQuantity,
            'mutually-exclusive' => PaymentForm::$mutuallyExclusive,
        ],
    ],
]); ?>

    <div class="wrap mt-0 pt-0" style="border-top-left-radius: 0; border-top-right-radius: 0;">
        <?= $this->render('_select_company', [
            'model' => $model,
            'form' => $form,
            'company' => $company,
            'companyArray' => $companyArray,
            'tariffArray' => $tariffArray,
        ]); ?>
    </div>

    <div class="row">
        <div class="col-9">
            <div id="subscribe-tariff-items" class="row">
                <?php foreach ($groupArray as $group) : ?>
                    <?= $this->render('_select_tariff', [
                        'model' => $model,
                        'company' => $company,
                        'isOneCompany' => $isOneCompany,
                        'group' => $group,
                        'discountByCount' => $discountByCount,
                        'expiresArray' => $expiresArray,
                        'limit' => $limit,
                        'groupsSelected' => $groupsSelected,
                        'durationList' => $durationList,
                        'defaultDuration' => $defaultDuration,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-3">
            <div class="wrap p-3">
                <div class="mb-3" style="font-size: 18px; font-weight: bold;">
                    Оплатить за
                </div>
                <div class="">
                    <?= $form->field($model, 'durationMonth', [
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->radioList(array_combine($durationList, $durationList), [
                        'class' => 'd-flex',
                        'item' => function ($i, $label, $name, $checked, $value) use ($defaultDuration) {
                            $options = [
                                'label' => $label,
                                'value' => $value,
                                'class' => 'durationmonth_radio',
                            ];
                            $wrapperOptions = [
                                'class' => 'durationmonth_item durationmonth_'.$value,
                                'data-value' => $value,
                            ];

                            $html = Html::beginTag('div', $wrapperOptions) . "\n";
                            $html .= Html::radio($name, $value == $defaultDuration, $options) . "\n";
                            $html .= Html::endTag('div') . "\n";

                            return $html;
                        },
                    ])->label('Срок (месяцев)'); ?>
                </div>

                <div id="subscribe-tariff-form-summary" class="mb-3" data-url="<?=Url::to(['summary'])?>">
                    <?= $this->render('summary', ['model' => $model]); ?>
                </div>

                <div class="bordered">
                    <div class="text-center tariff_pay_btn">
                        <?= Html::tag('button', 'Оплатить', [
                            'class' => 'tariff button-clr button-regular button-regular_red tooltip2-pay tariff-pay-btn w-100',
                            'data-tooltip-content' => '#tooltip_pay_method_select',
                        ]) ?>
                        <?= Html::tag('button', 'Оплатить', [
                            'class' => 'tariff button-clr button-regular button-regular_red tooltip2-pay tariff-not-selected disabled w-100',
                            'title' => 'Для оплаты выберите тариф'
                        ]) ?>
                    </div>
                </div>
                <div class="hidden">
                    <span id="tooltip_pay_method_select" class="tooltip_pay_type" style="display: inline-block; text-align: center;">
                        <span class="bold pay-tariff-wrap-title">Способ оплаты</span>
                        <?= Html::tag('span', 'Картой', [
                            'class' => 'form-submit-button button-clr button-regular button-hover-transparent w-100',
                            'data-name' => Html::getInputName($model, 'paymentTypeId'),
                            'data-value' => PaymentType::TYPE_ONLINE,
                            'style' => 'margin-bottom:10px;'
                        ]); ?>
                        <?= Html::tag('span', 'Выставить счет', [
                            'id' => 'form-submit-button-for-invoice',
                            'class' => 'form-submit-button button-clr button-regular button-hover-transparent w-100',
                            'data-name' => Html::getInputName($model, 'paymentTypeId'),
                            'data-value' => PaymentType::TYPE_INVOICE,
                        ]); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
<?php $form->end(); ?>

<div style="display: none;">
    <?= Html::a('', null, [
        'id' => 'document-print-link',
        'target' => '_blank',
        'data-url' => Url::to(['document-print', 'uid' => 'pdfUid']),
    ]) ?>
</div>

<?= $this->render('partial/_companies_subscribes', [
    'company' => $company,
    'groupArray' => $groupArray
]); ?>

<div class="wrap">
    <div class="row" style="padding-bottom:15px;border-bottom:1px solid #f2f3f7;">
        <div class="column">
            <h4 style="margin-bottom:0;">Тариф БЕСПЛАТНО</h4>
        </div>
        <div class="column ml-auto" style="color:#9198a0; font-size: 13px;">
            (подключается автоматически после окончания оплаты)
        </div>
    </div>
    <div class="row" style="padding-top:15px;">
        <div class="col-12">
            <div class="bold" style="padding-bottom:10px">Ограничения на тарифе:</div>
            <ul class="free-tariff-list">
                <li>Не более 5 счетов в месяц</li>
                <li>
                    Только 1 организация.
                    <span style="color:#9198a0; font-size: 13px;">
                        Т.е. если вы владеете более 1-й компании,
                        то вы не сможете добавить еще компании.
                    </span>
                </li>
                <li>Не более 3-х сотрудников</li>
                <li>Место на диске - 1 ГБ</li>
            </ul>
        </div>
    </div>
</div>

<?= $this->render('_payment_history', [
    'paymentDataProvider' => $paymentDataProvider,
    'paymentSearchModel' => $paymentSearchModel,
]); ?>

<!-- Small modal -->
<div class="modal fade" id="payment-box-2" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none; padding: 0">
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
            </div>
            <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                <div class="text-center pad5">
                    <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                    <div>
                        <p>
                            Счет отправлен на
                            <br>
                            <strong id="send-to-email"></strong>.
                        </p>
                        <p>
                            Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                            <br>
                            <span>support@kub-24.ru</span>
                        </p>
                    </div>

                    <a class="button-list button-clr button-regular button-hover-transparent" data-dismiss="modal" style="color: #FFF;"> ОК </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bill-invoice-remuneration" tabindex="-1"
     role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Получить вознаграждение на счет</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <?php $affiliateForm = ActiveForm::begin([
                    'action' => Url::to(['/company/withdraw-money', 'page' => 'subscribe']),
                    'options' => [
                        'class' => 'form-horizontal form-checking-accountant',
                        'id' => 'form-bill-invoice-remuneration',
                    ],
                    'enableClientValidation' => true,
                    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
                ]); ?>

                <div class="form-group">
                    Доступно к выводу: <b><?= $company->affiliate_sum; ?> руб.</b>
                </div>

                <?= $affiliateForm->field($affiliateProgramForm, 'withdrawal_amount', [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                    'wrapperOptions' => [
                        'class' => 'form-filter',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                ])->textInput([
                    'placeholder' => 'Укажите сумму для вывода вознаграждения',
                ])->label('Вывести:'); ?>

                <?= $affiliateForm->field($affiliateProgramForm, 'type', [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                    'wrapperOptions' => [
                        'class' => 'form-filter',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                ])->radioList([
                    AffiliateProgramForm::RS_TYPE => 'на расчетный счет компании',
                    AffiliateProgramForm::INDIVIDUAL_ACCOUNT_TYPE => 'на счет физического лица',
                ])->label(false); ?>

                <div class="mt-3 d-flex justify-content-between">
                            <?= Html::submitButton('Оформить заказ', [
                                'class' => 'button-clr button-regular button-regular_red',
                            ]); ?>
                            <?= Html::a('Отменить', null, [
                                'class' => 'button-width button-clr button-regular button-hover-transparent',
                                'data-dismiss' => 'modal'
                            ]); ?>
                        </div>
                    </div>
                </div>
                <span>После оформления заказа, наш сотрудник свяжется с вами и обсудит детали.</span>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="tooltip-template hidden">
    <div id="pay-extra-tooltip">
        Вы можете оплатить больше компаний чем у вас есть в аккаунте.
        <br>
        После оплаты, вы сможете добавить новую компанию и ей сразу включится оплаченный тариф
        <br>
        <strong>Важно:</strong> в аккаунт можно добавить новую компанию, только если все имеющиеся компании уже оплачены.
    </div>
    <div id="tariff-details-pricelist">
        <?php
        $tariffList = SubscribeTariff::find()->actual()->andWhere([
            'tariff_group_id' => SubscribeTariffGroup::PRICE_LIST,
            'duration_month' => 1,
            'duration_day' => 0,
        ])->orderBy(['price' => SORT_ASC])->all();
        $icoPlus = '<span class="big-ico plus">+</span>';
        $icoMinus = '<span class="big-ico minus">—</span>';
        ?>
        <table class="table table-style mb-0">
            <thead>
                <tr>
                    <th></th>
                    <?php foreach ($tariffList as $tariff) : ?>
                        <th class="pl-1" style="width: 20%"><?= $tariff->label ?></th>
                    <?php endforeach ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="label">Количество прайс-листов</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <td class="pl-1"><?= $tariff->tariff_limit ?: 'Безлимит' ?></td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Количество товаров в прайс-листе</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $value = $tariff->getNamedParamValue('products_count', SubscribeTariffGroup::PRICE_LIST); ?>
                        <td class=""><?= $value ? ('до ' . $value): $icoMinus ?></td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Онлайн обновления цены и остатков</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $value = $tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST); ?>
                        <td class="pl-1"><?= $value ? $icoPlus : $icoMinus ?></td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Просмотр карточки товара</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $value = $tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST); ?>
                        <td class="pl-1"><?= $value ? $icoPlus : $icoMinus ?></td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Онлайн оформление заказов</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $value = $tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST); ?>
                        <td class="pl-1">
                            <?php if ($tariff->price > 0): ?>
                                <?= $value ? $icoPlus : $icoMinus ?>
                            <?php else: ?>
                                <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> заказов
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Получение уведомлений</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $value = $tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST); ?>
                        <td class="pl-1"><?= $value ? $icoPlus : $icoMinus ?></td>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <td class="label">Тариф Выставление счетов</td>
                    <?php foreach ($tariffList as $id => $tariff): ?>
                        <?php $tariffStandart = $tariff->containsStandartTariff; ?>
                        <td class="pl-1">
                            <?php if ($tariff->price > 0): ?>
                                <?= $tariffStandart ? (($tariffStandart->tariff_limit ? ($tariffStandart->tariff_limit . ' счетов') : 'Безлимит') . ' в месяц') : $icoMinus ?>
                            <?php else: ?>
                                <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> счетов
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>
