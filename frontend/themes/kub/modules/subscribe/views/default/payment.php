<?php
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\service;
use frontend\modules\subscribe\forms\PaymentMethodForm;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use frontend\models\AffiliateProgramForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\subscribe\forms\PaymentMethodForm */
/* @var $tariff common\models\service\SubscribeTariff */

$this->title = 'Оплата сервиса kub-24.ru';

$userCompanyList = ArrayHelper::map($model->forCompanyArray, 'id', 'shortName');
$companyCount = count($model->companyList);
$companyCountTextItems = ['компанию', 'компании', 'компаний'];
?>
<h3 class="page-title">
    <?= $this->title ?>
</h3>

<div class="form-group">
    Срок: <?= $tariff->getTariffName(); ?>
    <br>
    Количество компаний: <?= $companyCount ?>
    <br>
    Сумма к оплате: <?= $model->newPayment->sum ?> руб.
</div>

<?php $form = \yii\widgets\ActiveForm::begin([
    'id' => 'subscribe-tariff-form-2',
    'action' => ['payment', 'company' => 1],
    'validationUrl' => ['validate', 'company' => 1],
    'enableAjaxValidation' => true,
]); ?>
<?= Html::activeHiddenInput($model, 'tariffId') ?>
<?= Html::activeHiddenInput($model, 'paymentTypeId') ?>
<?= Html::activeHiddenInput($model, 'createInvoice') ?>

<?php foreach ((array) $model->companyList as $id) {
    echo Html::hiddenInput(Html::getInputName($model, 'companyList') . '[]', $id) . "\n";
} ?>

<?= $form->field($model, 'companyId', [
    'options' => [
        'style' => 'margin-top: 10px;',
    ],
    'labelOptions' => [
        'class' => 'subscribe-form-label',
    ],
])->radioList($userCompanyList, [
    'item' => function ($index, $label, $name, $checked, $value) {
        return Html::tag('label', Html::radio($name, $checked, [
            'value' => $value,
            'class' => 'company-id-radio'
        ]) . ' ' . $label, [
            'style' => 'display: block;',
        ]);
    },
]); ?>

<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td class="spinner-button pay-button-spinner" style="width: 50%;">
            <?= Html::submitButton('<span class="ladda-label">Оплатить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue text-white ladda-button',
                'data-style' => 'expand-right',
                'style' => 'float: left;',
            ]); ?>
        </td>
        <td style="width: 50%; text-align: right;">
            <?= Html::button('Отменить', [
                'id' => 'payment-cancel',
                'class' => 'btn darkblue text-white',
            ]); ?>
        </td>
    </tr>
</table>
<?php $form->end(); ?>
