<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/27/15
 * Time: 11:55 AM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\grid\DropDownDataColumn;
use common\models\document\Act;
use common\models\service\Payment;
use common\models\service\PaymentType;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\models\PaymentSearch $paymentSearchModel */
?>

<div class="wrap">
    <h4>История платежей</h4>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'payment_history_pjax',
        'enablePushState' => false,
        'timeout' => 5000,
    ]); ?>
    <?= common\components\grid\GridView::widget([
            'id' => 'payment_history',
            'dataProvider' => $paymentDataProvider,
            'filterModel' => $paymentSearchModel,

            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '---'],
            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-style table-count-list table-payment-history',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $paymentDataProvider->totalCount]),
            'columns' => [
                [
                    'attribute' => 'payment_date',
                    'format' => 'raw',
                    'headerOptions' => [
                        'style' => 'width: 25%;',
                    ],
                    'value' => function (Payment $model) {
                        return ($model->payment_date) ? date('d.m.Y', $model->payment_date) : null;
                    },
                ],
                [
                    'attribute' => 'sum',
                    'headerOptions' => [
                        'style' => 'width: 15%;',
                    ],
                    'value' => function (Payment $model) {
                        return TextHelper::moneyFormat($model->sum, 2);
                    },
                ],
                [
                    'attribute' => 'type_id',
                    'headerOptions' => [
                        'style' => 'width: 15%;',
                    ],
                    'filter' => FilterHelper::getFilterArray($paymentSearchModel->getTypeFilterItems(), 'id', 'name', true, false),
                    'value' => function (Payment $model) {
                        return $model->getPaymentName();
                    },
                    's2width' => '145px'
                ],
                [
                    'label' => 'Акт',
                    'headerOptions' => [
                        'style' => 'width: 10%;',
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model->type_id == PaymentType::TYPE_INVOICE && $model->outInvoice && $model->outInvoice->acts) {
                            $list = array_map(function ($act) {
                                if (empty($act->uid)) {
                                    $act->updateAttributes(['uid' => Act::generateUid()]);
                                }

                                return Html::a("№ {$act->fullNumber}", [
                                    '/documents/act/out-view',
                                    'view' => 'pdf',
                                    'uid' => $act->uid,
                                ], [
                                    'target' => '_blank',
                                    'data-pjax' => 0
                                ]);
                            }, $model->outInvoice->acts);

                            return implode(', ', $list);
                        }

                        return '---';
                    },
                ],
                [
                    'label' => 'Примечание',
                    'headerOptions' => [
                        'style' => 'width: 25%;',
                    ],
                    'value' => function ($model) {
                        if (in_array($model->type_id, [PaymentType::TYPE_RECEIPT, PaymentType::TYPE_INVOICE])) {
                            $text = '';
                            $date = new \DateTime("@$model->created_at");
                            if ($date) {
                                $text .= 'Отправлен' . ($model->type_id == PaymentType::TYPE_RECEIPT ? 'а ' : ' ');
                                $text .= 'на e-mail ' . $date->format('d.m.Y');
                            }

                            return $text;
                        }

                        return '';
                    },
                ],
            ],
        ]); ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
