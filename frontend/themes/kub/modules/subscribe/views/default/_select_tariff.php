<?php

use common\components\date\DateHelper;
use common\models\Discount;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $company \common\models\Company */
/* @var $isOneCompany bool */
/* @var $group common\models\service\SubscribeTariffGroup */
/* @var $groupsSelected array */
/* @var $durationList array */
/* @var $defaultDuration integer */

$actualTariffArray = $group->actualTariffs;
$groupDurationsArray = [];
foreach ($actualTariffArray as $tariff) {
    $groupDurationsArray[$tariff->duration_month] = $tariff->duration_month;
}
$tariffData = [];
$dropdownData = [];
$dropdownItems = [];

if (in_array($group->id, SubscribeTariffGroup::$hasLimit)) {
    foreach ($actualTariffArray as $tariff) {
        $dropdownLabel = $tariff->label ? 'Тариф '.$tariff->label : $group->limitLabel($tariff->tariff_limit, true);
        $key = $group->id.'-'.yii\helpers\Inflector::slug($dropdownLabel);
        $tariffData[$key][] = $tariff;
        if (!isset($dropdownData[$key])) {
            $dropdownData[$key] = $dropdownLabel;
        }
    }
} else {
    $tariffData['tariff_tab_'.$group->id] = $actualTariffArray;
}

foreach ($dropdownData as $key => $label) {
    $dropdownItems[] = [
        'label' => $label,
        'url' => '#',
        'linkOptions' => [
            'class' => 'tariff-toggle-link',
            'data-target' => '#'.$key,
        ]
    ];
}

$tariffLabel = implode('<br>ИП', explode(' ИП', $group->name));
$tariffLabelParts = explode('.', $tariffLabel);
if (count($tariffLabelParts) == 2) {
    $tariffLabel = Html::tag('span', $tariffLabelParts[0], [
        'style' => 'font-size: 80%;',
    ]) . Html::tag('br') . $tariffLabelParts[1];
}
$isChecked = in_array($group->id, $groupsSelected);
$boxCss = $isChecked ? 'checked' : '';
$durationIsNotSelectable = $group->getIsPayPerQuantity() && count($groupDurationsArray) == 1;
?>

<?php if ($actualTariffArray) : ?>
    <div class="col-<?= $group->id == SubscribeTariffGroup::BI_ALL_INCLUSIVE ? '8' : '4'; ?> d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1 tariff_group_box <?=$boxCss?>" data-group="<?=$group->id?>">
            <div class="subscribe-tariff-box d-flex flex-column flex-grow-1 pb-0">
                <label class="mb-0 d-flex cp" style="height: 70px;" for="tariff_group_select_<?=$group->id?>">
                    <div class="tariff-icon-wrap">
                        <div class="tariff-icon">
                            <?= Icon::get($group->icon) ?>
                        </div>
                    </div>
                    <div class="tariff-group-name">
                        <?= $tariffLabel ?>
                        <?= $group->id == SubscribeTariffGroup::FOREIGN_CURRENCY_INVOICE ? '$' : ''; ?>
                        <div class="tariff-rules mt-2" style="font-size: 14px;">
                            <?php if ($dropdownItems) : ?>
                                <div class="dropdown mb-2">
                                    <?= Html::tag('div', $dropdownItems[0]['label'], [
                                        'class' => 'dropdown-toggle form-title-link',
                                        'data-toggle' => 'dropdown',
                                        'style' => 'display: inline-block; cursor: pointer;',
                                    ])?>
                                    <?php if ($group->id == SubscribeTariffGroup::PRICE_LIST) : ?>
                                        <span style="position: absolute; top: 0; right: -25px;">
                                            <?= Icon::get('question', [
                                                'class' => 'tooltip-question-icon ml-1 tooltip-click',
                                                'data-tooltip-content' => '#tariff-details-pricelist',
                                            ]) ?>
                                        </span>
                                    <?php endif ?>
                                    <?= \yii\bootstrap4\Dropdown::widget([
                                        'id' => 'invoices-count-dropdown',
                                        'class' => 'dropdown-menu form-filter-list list-clr',
                                        'items' => $dropdownItems,
                                    ])?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </label>
                <div class="d-flex flex-column flex-grow-1 justify-content-between bor-t pt-3">
                    <div class="subscribe-tariff-content">
                        <?php $isTabActive = true; ?>
                        <?php foreach ($tariffData as $tab => $tariffArray) : ?>
                            <?php
                            $durationsExists = [];
                            $tariff = reset($tariffArray);
                            $durations = array_combine($durationList, $durationList);
                            $discount = $company->getDiscount($tariff->id);
                            ?>
                            <?= Html::beginTag('div', [
                                'id' => $tab,
                                'class' => 'tariff_subgroup' . ($isTabActive ? ' checked': ''),
                                'data' => [
                                    'tariff-group-id' => $group->id,
                                    'companies' => '',
                                    'durations' => $durationIsNotSelectable
                                        ? null
                                        : ArrayHelper::getColumn($tariffArray, 'duration_month'),
                                ],
                            ]) ?>

                            <div class="bordered">
                                <?php $tariffCount = count($tariffArray) ; ?>
                                <?php foreach ($tariffArray as $t): ?>
                                    <?php
                                    $d = $t->duration_month;
                                    $durationsExists[] = $t->duration_month;
                                    if (isset($durations[$d])) {
                                        unset($durations[$d]);
                                    }
                                    if ($group->getIsPayPerQuantity()) {
                                        $tariffPrice = round($t->price / $t->tariff_limit);
                                    } else {
                                        $tariffPrice = $tariffCount > 1 ? round($t->price / $d) : $t->price;
                                    }
                                    if ($isOneCompany && $t->id == Discount::TYPE_4_TARIFF_ID && ($discount = $company->getMaxDiscount($t->id))) {
                                        $discountPrice = round(max(0, $t->price - ($t->price * $discount->value / 100)));
                                    } else {
                                        $discountPrice = null;
                                    }
                                    $checkedScc = $t->duration_month == $defaultDuration ? ' checked' : '';
                                    $itemCss = $durationIsNotSelectable ? 'tariff_item_checked' : 'tariff_item'.$checkedScc;
                                    ?>

                                    <?= Html::beginTag('div', [
                                        'class' => $itemCss,
                                        'data-tariff' => $t->id,
                                        'data-price' => $t->price,
                                        'data-duration' => $durationIsNotSelectable ? null : $t->duration_month,
                                    ]) ?>

                                        <?php if ($group->getIsPayPerQuantity()) : ?>
                                            <span class="tariff-price">
                                                <span class="current-tariff-price">
                                                    <?= number_format($tariffPrice, 0, ',', '&nbsp;') ?>
                                                </span>
                                            </span>
                                            ₽ /
                                            <?= $group->perOneLabel() ?>
                                        <?php else : ?>
                                            <span class="tariff-price <?= isset($discountPrice) ? 'line-through-red' : ''; ?>">
                                                <span class="current-tariff-price">
                                                    <?= number_format($tariffPrice, 0, ',', '&nbsp;') ?>
                                                </span>
                                            </span>
                                            ₽ /
                                            <?= $tariffCount > 1 ? 'месяц' : $t->getReadableDuration() ?>

                                            <?php if (isset($discountPrice)) : ?>
                                                <div class="color-red">
                                                    <span class="tariff-price">
                                                        <span class="current-tariff-price">
                                                            <?= number_format($discountPrice, 0, ',', '&nbsp;') ?>
                                                        </span>
                                                    </span>
                                                    ₽ /
                                                    <?= $tariffCount > 1 ? 'месяц' : $t->getReadableDuration() ?>
                                                </div>
                                            <?php endif ?>
                                        <?php endif ?>

                                        <?= Html::activeHiddenInput($model, 'tariffId[]', [
                                            'id' => Html::getInputId($model, 'tariffId').'_'.$t->id,
                                            'class' => 'tariff_item_select',
                                            'value' => $t->id,
                                            'data-price' => $t->price,
                                            'data-duration' => $t->duration_month,
                                            'disabled' => true,
                                        ]); ?>

                                    <?= Html::endTag('div') ?>

                                <?php endforeach; ?>

                                <?php if (!$durationIsNotSelectable) : ?>
                                    <?php if ($durationsExists) {
                                        $last = array_pop($durationsExists);
                                        $payText = 'Оплата только за ';
                                        if ($durationsExists) {
                                            $payText .= implode(' и ', array_filter([
                                                implode(', ', $durationsExists),
                                                php_rutils\RUtils::numeral()->getPlural($last, [
                                                    'месяц',
                                                    'месяца',
                                                    'месяцев',
                                                ])
                                            ]));
                                        } elseif ($last == 12) {
                                            $payText .= 'год';
                                        } else {
                                            $payText .= php_rutils\RUtils::numeral()->getPlural($last, [
                                                'месяц',
                                                'месяца',
                                                'месяцев',
                                            ]);
                                        }
                                    } else {
                                        $payText = null;
                                    }
                                    ?>
                                    <?php foreach ($durations as $d): ?>
                                        <div class="tariff_item <?= $d == $defaultDuration ? ' checked' : '' ?>"
                                            data-tariff=""
                                            data-price=""
                                            data-duration="<?= $d ?>"
                                        >
                                            <div class="text-danger mt-2">
                                                <?php if ($payText) : ?>
                                                    <?= $payText ?>
                                                <?php else : ?>
                                                    Не найден тарифный план на
                                                    <?= php_rutils\RUtils::numeral()->getPlural($d, [
                                                        'месяц',
                                                        'месяца',
                                                        'месяцев',
                                                    ]); ?>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            </div>
                            <?= Html::endTag('div') ?>

                            <?php $isTabActive = false ?>
                        <?php endforeach ?>
                    </div>
                    <?php Modal::begin([
                        'title' => $this->render('_description_title', [
                            'group' => $group,
                            'tariffLabel' => $tariffLabel,
                        ]),
                        'titleOptions' => [
                            'class' => 'mb-3',
                        ],
                        'toggleButton' => [
                            'label' => 'Описание тарифа',
                            'tag' => 'div',
                            'class' => 'link cp pt-2',
                        ],
                        'closeButton' => [
                            'label' => \frontend\components\Icon::get('close'),
                            'class' => 'modal-close close',
                        ],
                        'footerOptions' => [
                            'class' => 'justify-content-center',
                        ],
                        'footer' => Html::button('OK', [
                            'class' => 'button-regular button-hover-transparent button-width-medium',
                            'data-dismiss' => 'modal',
                        ]),
                    ]) ?>
                        <?= $this->render('_description', [
                            'group' => $group,
                            'tariffLabel' => $tariffLabel,
                            'tariffData' => $tariffData,
                            'dropdownData' => $dropdownData,
                        ]) ?>
                    <?php Modal::end() ?>
                </div>
            </div>
            <span style="position: absolute; top: 5px; right: 0;">
                <?= Html::checkbox('tariff_group_id[]', $isChecked, [
                    'id' => 'tariff_group_select_' . $group->id,
                    'value' => $group->id,
                    'class' => 'tariff_group_select',
                ]) ?>
            </span>
        </div>
    </div>
<?php endif ?>
