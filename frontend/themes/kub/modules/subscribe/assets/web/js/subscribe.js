(function ($) {
    var paymentForm = $('#subscribe-tariff-form');
    var mutuallyExclusive = paymentForm.data('mutually-exclusive');
    var discountArray = eval(paymentForm.data('discounts'));
    var pricesArray = eval(paymentForm.data('prices'));
    var durationsArray = eval(paymentForm.data('durations'));
    var quantityArray = eval(paymentForm.data('quantity'));

    var tariffItemsBlock = $('#subscribe-tariff-items');
    var promocodeBlock = $('#promo-code-block');
    var currentTariffPrice = 0;

    var pracesByDiscount = function(price, discount) {
        return (price - (price * discount / 100));
    };

    var discountByQuantity = function(quantity, discounts) {
        let d = 0;
        let count = 0;
        if (discounts.hasOwnProperty(quantity)) {
            d = parseFloat(discounts[quantity]);
        } else {
            for (var q in discounts) {
                if (q < quantity && q > count) {
                    count = q;
                    d = parseFloat(discounts[q]);
                }
            }
        }

        return d;
    };

    var checkMutuallyExclusive = function(val) {
        var exclusiveList = mutuallyExclusive[val] || [];
        if (exclusiveList.length > 0) {
            exclusiveList.forEach(function(id) {
                $('.tariff_group_box[data-group='+id+']')
                    .toggleClass('checked', false)
                    .find('input.tariff_group_select')
                    .prop('checked', false)
                    .uniform('refresh');
            });
        }
    };

    var processingRequestData = function(data) {
        if (data.content) {
            $('#payment-box-2 .modal-body').html(data.content);
        } else if ($('#payment-box-2').is(':visible')) {
            $('#payment-box-2').modal('hide');
        }
        if (data.alert && typeof data.alert === 'object') {
            for (alertType in data.alert) {
                var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                $.alert('#js-alert', data.alert[alertType], {type: alertType, topOffset: offset});
            }
        }
        if (data.done) {
            if (data.email && (data.type == '2' || data.type == '3')) {
                $("#send-to-email").html(data.email);
                $("#info-after-pay").modal("show");
            }
            if (data.link) {
                $('#document-print-link').attr('href', data.link);
                $('#document-print-link')[0].click();
            }
        }
    };

    var tariffItemsCheck = function() {
        $('input.tariff_item_select')
            .prop('disabled', true);
        $('.tariff_group_box.checked .tariff_subgroup.checked .tariff_item.checked input.tariff_item_select')
            .prop('disabled', false);
        $('.tariff_group_box.checked .tariff_subgroup.checked .tariff_item_checked input.tariff_item_select')
            .prop('disabled', false);
    };

    var tariffDurationsCheck = function() {
        var d = $('#subscribe-tariff-form').data('durationlist');
        $('.tariff_group_box.checked .tariff_subgroup.checked').each(function() {
            let durations = $(this).data('durations') || [];
            if (durations.length > 0) {
                d = d.filter(x => durations.includes(x));
            }
        });
        $('#subscribe-tariff-form .durationmonth_item').each(function() {
            let month = parseInt($(this).data('value'));
            if (d.indexOf(month) == -1) {
                $(this).toggleClass('disabled', true).find('input').prop('disabled', true).prop('checked', false);
            } else {
                $(this).toggleClass('disabled', false).find('input').prop('disabled', false);
            }
        });
        if ($('input.durationmonth_radio:checked:enabled').length == 0) {
            $('input.durationmonth_radio:enabled').first().prop('checked', true).trigger('change');
        }
        $('input.durationmonth_radio').uniform('refresh');
    };

    var getSummary = function() {
        var form = $('form#subscribe-tariff-form');
        var summary = $('#subscribe-tariff-form-summary');
        var formData = form.serializeArray();
        var button = $('#form-submit-button-for-invoice');
        formData.push({ name: button.data('name'), value: button.data('value') });
        $.post(summary.data('url'), formData, function(data) {
            summary.html(data);
            $('.tariff_pay_btn').toggleClass('active', $('#selected_tariff_sum').data('sum') > 0);
        });
    };

    $(document).on('change', 'input.tariff_group_select[type="checkbox"]', function (e) {
        $(this).closest('.tariff_group_box').toggleClass('checked', this.checked);
        if (this.checked) {
            checkMutuallyExclusive(this.value);
        }
        tariffDurationsCheck();
        tariffItemsCheck();
        getSummary();
    });

    $(document).on('click', 'a.tariff-toggle-link', function (e) {
        e.preventDefault();
        var toggleBox = $(this).closest('.dropdown');
        toggleBox.toggleClass('open', false);
        toggleBox.find('.dropdown-toggle').text($(this).text());
        $('li', toggleBox).toggleClass('active', false);
        $(this).closest('li').toggleClass('active', true);
        var target = $($(this).data('target'));
        var targetWrap = target.closest('.subscribe-tariff-content');
        $('.tariff_subgroup', targetWrap).toggleClass('checked', false);
        target.toggleClass('checked', true);
        tariffItemsCheck();
        getSummary();
    });

    $(document).on('submit', 'form#subscribe-tariff-form', function (e) {
        if ($(this).data('allow-submit')) return;
        e.preventDefault();
    });

    $(document).on('click', '.form-submit-button', function (e) {
        e.preventDefault();
        $('.form-submit-button').prop('disabled', true);
        var l = Ladda.create(this);
        l.start();
        var button = $(this);
        var form = $('form#subscribe-tariff-form');
        var formData = form.serializeArray();
        formData.push({ name: button.data('name'), value: button.data('value') });
        $.post(form.attr('action'), formData, function(data) {
            processingRequestData(data);
            l.stop();
            $('.form-submit-button').prop('disabled', false);
        });
    });

    $(document).on('change', 'input.durationmonth_radio', function(e) {
        if ($('input.durationmonth_radio:checked:enabled').length) {
            let d = $('input.durationmonth_radio:checked:enabled').val();
            $('.tariff_group_box .tariff_subgroup .tariff_item.checked').toggleClass('checked', false);
            $('.tariff_group_box .tariff_subgroup .tariff_item[data-duration='+d+']').toggleClass('checked', true);
        }
        tariffItemsCheck();
        getSummary();
    });

    $(document).on('change', 'input.company-id-checker', function(e) {
        getSummary();
    });

    $(document).on('change paste keyup', '#paymentform-payextra', function(e) {
        let val = Math.max(0, Math.min(parseInt(this.value), this.max));
        if (this.value != val) {
            this.value = val;
        }
        clearTimeout(window.paymentformPayextraTimer);
        window.paymentformPayextraTimer = setTimeout(function () {
            getSummary();
        }, 200);
    });

    $(document).on('change paste keyup', '#paymentform-admindiscount', function(e) {
        let val = Math.max(0, Math.min(this.value*1, this.max));
        if (this.value != val) {
            this.value = val;
        }
        clearTimeout(window.paymentformPayextraTimer);
        window.paymentformPayextraTimer = setTimeout(function () {
            getSummary();
        }, 200);
    });

    $(document).on('change paste keyup', 'input.price_details_payment_count', function(e) {
        let val = Math.max(1, parseInt(this.value));
        if (this.value != val) {
            this.value = val;
        }
        let container = $(this).closest('.price_details');
        $('.tariff_price_details', container).each(function(index) {
            let tariffBox = $(this);
            let tariffDiscounts = discountArray[tariffBox.data('tariff')];
            if (tariffDiscounts) {
                let d = discountByQuantity(val, tariffDiscounts);
                $('.price_details_item', tariffBox).each(function(index) {
                    let v = Math.round(pracesByDiscount($(this).data('value')*val, d));
                    $(this).text(new Intl.NumberFormat('ru-RU').format(v));
                });
            }
        });
    });

    $(document).ready(function(e) {
        tariffDurationsCheck();
        tariffItemsCheck();
        getSummary();
    });
})(jQuery);