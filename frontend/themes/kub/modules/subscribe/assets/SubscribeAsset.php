<?php

namespace frontend\themes\kub\modules\subscribe\assets;

use yii\web\AssetBundle;

/**
 * SubscribeAsset
 */
class SubscribeAsset extends AssetBundle
{
    public $sourcePath = '@frontend/themes/kub/modules/subscribe/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/subscribe.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'js/subscribe.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\kub\assets\KubCommonAsset',
    ];
}
