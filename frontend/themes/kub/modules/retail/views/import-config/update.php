<?php

use common\models\ofd\OfdImportConfig;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\retail\models\ImportConfigForm */

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'ofd-import-config-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'ofd-import-config-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<?= $form->field($model, 'import_config_id', [
    'template' => "{beginWrapper}\n{error}\n{input}\n{hint}\n{endWrapper}",
    'parts' => [
        '{input}' => $this->render('_import_config_id', [
            'form' => $form,
            'model' => $model,
        ]),
    ],
]) ?>

<div class="form-group">
    <strong>
        Оплаты по картам (эквайринг)
    </strong>
    <div>
        будут отображаться в разделе «Банк» и «Операции» при их зачислении на ваш банковский счет.
    </div>
</div>

<div class="d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-hover-transparent button-width',
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-hover-transparent button-width',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php $form->end(); ?>

<?php if ($saved ?? false) : ?>
    <script type="text/javascript">
        $('#ofd-import-config-form').closest('.modile').modal('hide');
        window.location.href = window.location.href;
    </script>
<?php endif ?>

<?php \yii\widgets\Pjax::end(); ?>
