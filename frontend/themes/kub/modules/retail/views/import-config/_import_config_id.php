<?php

use common\models\ofd\OfdImportConfig;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model frontend\modules\retail\models\ImportConfigForm */

?>

<table>
    <?php foreach ($model::$items as $key => $item) : ?>
        <?php
        $collapseClass = ['collapse', $model->import_config_id == $key ? 'show' : ''];
        ?>
        <tr>
            <td style="vertical-align: top;">
                <?= Html::activeRadio($model, 'import_config_id', [
                    'value' => $key,
                    'label' => false,
                    'uncheck' => false,
                    'onclick' => "$(this).closest('table').find('.collapse').collapse('hide');".
                        " $(this).closest('tr').find('.collapse').collapse('show');"
                ]) ?>
            </td>
            <td class="pb-1">
                <label class="mb-0">
                    <strong>
                        <?= $item['label'] ?>
                    </strong>
                </label>
                <div class="<?= implode(' ', $collapseClass) ?>">
                    <?= implode(Html::tag('br'), $item['hint']) ?>
                    <?php if ($key == OfdImportConfig::GENERAL_CASHBOX) : ?>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'general_cashbox_name', [
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->textInput([
                                    'maxlength' => true,
                                ]) ?>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </td>
        </tr>
    <?php endforeach ?>
</table>