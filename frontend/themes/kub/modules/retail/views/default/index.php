<?php

use frontend\components\Icon;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\retail\models\OfdRetailSearch;
use frontend\modules\retail\widgets\PeriodDropdownWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use frontend\modules\retail\models\OfdChartModel;

/* @var $this yii\web\View
 * @var $searchModel OfdRetailSearch
 * @var $chartModel OfdChartModel
 * @var $data []
 * @var $activeTab integer
 * @var $periodSize string
 */

$this->title = 'Продажи по точкам продаж';

$showHelpPanel = $user->config->retail_sales_help ?? false;
$showChartPanel = $user->config->retail_sales_chart ?? false;

$pjaxReloadUrl = Url::to(['chart',
    'step' => $chartModel->step,
    'offset' => $chartModel->offset,
    'store' => $chartModel->store,
    'product' => $chartModel->product
]);

$urlCurrent = Url::current(['year' => null]);
?>

<div class="wrap pt-0 pb-0 pl-4 pr-3 mb-2">
    <div class="p-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-0"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="column pr-2">
                <?= Html::button(Icon::get('diagram'), [
                    'id' => 'btnChartCollapse',
                    'class' => 'tooltip3 button-list button-hover-transparent' . (!$showChartPanel ? ' collapsed' : ''),
                    'data-toggle' => 'collapse',
                    'href' => '#chartCollapse',
                    'data-tooltip-content' => '#tooltip_chart_collapse',
                    'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")'),
                ]) ?>
            </div>
            <div class="column pl-1 pr-2">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr' . (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <div class="column pl-1 pr-0">
                <?= Select2::widget([
                    'id' => 'retail_period_list',
                    'name' => 'retail_period_list',
                    'data' => $searchModel->getYearFilter(),
                    'pluginOptions' => [
                        'width' => 'auto'
                    ],
                    'value' => $searchModel->year,
                    'hideSearch' => true,
                    'options' => [
                        'onchange' => new \yii\web\JsExpression("location.href = '{$urlCurrent}?year=' + this.value")
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта">
            Открыть описание отчёта
        </div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график">
            Открыть график
        </div>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="retail_sales_chart" data-invert-attribute="retail_sales_help" style="margin-top:0.75rem">
    <div class="pt-4 pb-3" style="margin-top: -14px">
        <?php Pjax::begin([
            'id' => 'retail-charts-pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'linkSelector' => '.retail-charts-link',
            'timeout' => 30000,
        ]); ?>

        <div class="text-center">
            <img width="32" height="32" src="/img/ofd-preloader.gif"/>
        </div>

        <?php Pjax::end(); ?>
    </div>
</div>

<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 mb-2 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="retail_sales_help" data-invert-attribute="retail_sales_chart" style="margin-top:0.75rem">
    <div class="pt-4 pb-3" style="margin-top: -14px">
        <?= $this->render('_panel')?>
    </div>
</div>

<div class="d-flex flex-nowrap my-2 py-1 align-items-center">
    <?php if (YII_ENV_DEV || !$company->isFreeTariff) : ?>
        <div class="mr-2 pr-1">
            <?= Html::a(Icon::get('exel'), [
                'get-xls',
                'year' => $searchModel->year,
            ], [
                'class' => 'download-odds-xls button-list button-hover-transparent',
                'title' => 'Скачать в Excel',
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="pr-1">
        <?= TableConfigWidget::widget([
            'sortingItemsTitle' => 'Сортировка',
            'sortingItems' => [
                [
                    'attribute' => 'sort_by_name',
                    'label' => 'Сортировать по названию',
                    'checked' => $user->config->table_retail_sales_sort,
                    'data-url' => 'index'
                ],
                [
                    'attribute' => 'sort_by_place',
                    'label' => 'Сортировать по адресу',
                    'checked' => !$user->config->table_retail_sales_sort,
                    'data-url' => 'index'
                ],
            ],
            'showItems' => [
                [
                    'attribute' => 'table_retail_sales_zeroes',
                    'label' => 'Не выводить столбцы где все нули',
                ]
            ]
        ]); ?>
    </div>
    <div>
        <?= TableViewWidget::widget(['attribute' => 'table_retail_sales_view']) ?>
    </div>
</div>

<div class="wrap wrap_padding_none_all" style="margin-bottom: 12px; position: relative">
    <?= $this->render('_table', [
        'user' => $user,
        'company' => $company,
        'searchModel' => $searchModel,
        'data' => $data,
    ]); ?>
</div>

<script>

    ChartRetail = {
        url: '<?= $pjaxReloadUrl ?>',
        labelsX: [],
        currDayPos: [],
        freeDayPos: [],
        wrapPointPos: [],
        activeTopProductPos: false,
        activeTopStorePos: false,
        storeArray: <?= json_encode($storeArray ?: []) ?>,
        productArray: [], // loaded by pjax
        month: '<?= $chartModel->getMonth() ?>',
        offset: '<?= $chartModel->offset ?>',
        step: '<?= $chartModel->step ?>',
        _loading: false,
        bindEvents: function() {
            // collapse chart wrapper
            $(document).on("show.bs.collapse", "#chartCollapse", function() {
                ChartRetail.refresh();
            });
            // change point
            $(document).on("change", "#chart-retail-point-select", function() {
                ChartRetail.setFilter('store', this.value);
                ChartRetail.refresh();
            });
            // change month
            $(document).on("change", "#chart-top-store-select", function() {
                ChartRetail.setFilter('month', this.value);
                ChartRetail.refresh();
            });
            // change step
            $(document).on("click", ".chart-retail-step", function() {
                ChartRetail.setFilter('step', $(this).attr('data-step'));
                ChartRetail.refresh();
            });
            // change offset
            $(document).on("click", ".chart-retail-offset", function() {
                ChartRetail.setFilter('offset', $(this).attr('data-offset'));
                ChartRetail.refresh();
            });

            // prevent double click
            $(document).on("pjax:start", "#retail-charts-pjax", function() {
                ChartRetail._loading = true;
            });
            $(document).on("pjax:success", "#retail-charts-pjax", function() {
                ChartRetail._loading = false;
            });
        },
        setFilter: function(attr, value) {

            if (this._loading)
                return false;

            this[attr] = value;
            this.url = ChartRetail.addOrReplaceParam(this.url, attr, value);
        },
        resetFilter: function(attr) {

            if (this._loading)
                return false;

            this[attr] = null;
            this.url = ChartRetail.addOrReplaceParam(this.url, attr, null);
        },
        refresh: function() {
            $.pjax({
                url: this.url,
                container: "#retail-charts-pjax",
                push: false,
                replace: false,
                scrollTo: false,
                timeout: 30000
            });
        },
        setActiveTopProduct: function(pos) {

            if (pos === false) {
                window.ChartRetail.activeTopProductPos = false;
                return;
            }

            let chartToLoad = window.setInterval(function () {
                const chart = $('#chart-top-1').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.ChartRetail.activeTopProductPos = pos;
                    window.clearInterval(chartToLoad);
                    chart.series[0].points[pos].update({
                        color: {
                            pattern: {
                                'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                'color': 'rgba(57,194,176,1)',
                                'width': 5,
                                'height': 5
                            }
                        }
                    });
                }
            }, 100);
        },
        setActiveTopStore: function(pos) {

            if (pos === false) {
                window.ChartRetail.activeTopStorePos = false;
                return;
            }

            let chartToLoad = window.setInterval(function () {
                const chart = $('#chart-top-2').highcharts();
                if (typeof(chart) !== 'undefined') {
                    window.ChartRetail.activeTopStorePos = pos;
                    window.clearInterval(chartToLoad);
                    chart.series[0].points[pos].update({
                        color: {
                            pattern: {
                                'path': 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                'color': 'rgba(57,194,176,1)',
                                'width': 5,
                                'height': 5
                            }
                        }
                    });
                }
            }, 100);
        },
        getWrapPointXLabel: function(x) {
            const chart = $('#chart-retail').highcharts();
            const colWidth = (chart.xAxis[0].width) / chart.xAxis[0].dataMax + 1;
            let name, left, txtLine, txtLabel;
            let k = (window.ChartRetail.step === 'month') ? 0.565 : 0.565;

            if (window.ChartRetail.wrapPointPos[x + 1]) {
                name = window.ChartRetail.wrapPointPos[x + 1].prev;
                left = window.ChartRetail.wrapPointPos[x + 1].leftMargin;

                txtLine = '<div style="position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6"></div>';
                txtLabel = '<div style="position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLine + txtLabel;
            }
            if (window.ChartRetail.wrapPointPos[x]) {
                name = window.ChartRetail.wrapPointPos[x].next;

                txtLabel = '<div style="position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-family:\'Corpid E3 SCd\', sans-serif;">' + name + '</div>';

                return txtLabel;
            }

            return '';
        },
        addOrReplaceParam: function(url, param, value) {
            param = encodeURIComponent(param);
            var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
            var a = document.createElement('a');
            var regex = new RegExp(r);
            var str = param + (value ? "=" + encodeURIComponent(value) : "");
            a.href = url;
            var q = a.search.replace(regex, "$1"+str);
            if (q === a.search) {
                a.search += (a.search ? "&" : "") + str;
            } else {
                a.search = q;
            }
            return a.href;
        }
    };

    ////////////////////////////////////
    $(document).ready(function () {
        ChartRetail.bindEvents();
        <?php if ($showChartPanel): ?>
        ChartRetail.refresh();
        <?php endif; ?>
    });
    ////////////////////////////////////

    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("shown.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });

    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("shown.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
</script>

<?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);