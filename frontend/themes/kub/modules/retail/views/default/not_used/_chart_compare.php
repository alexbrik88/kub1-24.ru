<?php

use frontend\modules\retail\models\ChartCompareSearch;
use frontend\themes\kub\components\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;

/** @var $compareData array */

$data = $compareData['data'];
$maxValue = $compareData['max'];

$color1 = '#68c5d2';
$color2 = '#2d86ac';

?>

<div style="position: relative">
    <div class="ht-caption noselect d-flex justify-content-between align-items-center" style="min-height: 44px;">
        <div>
            СРАВНЕНИЕ
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="finance-charts-group">
    <?php if ($maxValue > 0) : ?>
        <div class="row">
            <div class="column pr-0" style="display: inline-block; margin-bottom: 10px;">
                <table  class="employee-rating-detail ht-in-table" style="width: 100%; line-height: 18px;">
                    <thead>
                        <tr>
                            <th width="80%"> </th>
                            <th width="10%"> </th>
                            <th width="10%"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key => $val) : ?>
                            <?php
                            $width1 = $val['this'] * 100 / $maxValue;
                            $width2 = $val['last'] * 100 / $maxValue;
                            $percentage = $val['last'] > 0 ? round(($val['this'] - $val['last']) * 100 / $val['last'], 2) : null;
                            ?>
                            <tr style="vertical-align: bottom;">
                                <td>
                                    <div>
                                        <?= ChartCompareSearch::$labels[$key]['this'] ?>
                                    </div>
                                    <div style="position: relative; min-width: 150px;">
                                        <?= Html::tag('div', '', [
                                            'class' => 'employee-rating-bar bar1',
                                            'style' => "background-color: {$color1};" .
                                                       ($width1 > 0 ? 'min-width: 1px;' : ''),
                                            'data-width' => "{$width1}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td nowrap style="padding: 0 8px; font-weight: bold; text-align: right;">
                                    <?= number_format($val['this'], 2, ',', ' ').'&nbsp;₽' ?>
                                </td>
                                <td nowrap style="padding: 0 8px; font-weight: bold;">
                                    <?php if ($percentage !== null) : ?>
                                        <?= '(' . ($percentage >= 0 ? '+' : '') . $percentage . '&nbsp;%)'; ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; line-height: 12px;">
                                <td style="padding: 0; padding-right: 8px;">
                                    <div style="position: relative; min-width: 150px;">
                                        <?= Html::tag('div', '', [
                                            'class' => 'employee-rating-bar bar2',
                                            'style' => "background-color: {$color2};" .
                                                       ($width2 > 0 ? 'min-width: 1px;' : ''),
                                            'data-width' => "{$width2}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td nowrap style="padding: 0 8px; text-align: right;">
                                    <?= number_format($val['last'], 2, ',', ' ').'&nbsp;₽' ?>
                                </td>
                                <td nowrap style="padding: 0 8px;">
                                    <?= ChartCompareSearch::$labels[$key]['last'] ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php else : ?>
        <div style="padding: 11px 0">
            Нет данных
        </div>
    <?php endif ?>
    </div>
</div>