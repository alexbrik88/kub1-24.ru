<?php

use frontend\modules\retail\models\OfdChartModel;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var $storeArray array */
/** @var $this yii\web\View */
/** @var $retailData array */
/** @var $model OfdChartModel */

///////////////// colors /////////////////
$color1 = 'rgba(46,159,191,1)';
$color1_opacity = 'rgba(46,159,191,.5)';
$color2 = 'rgba(57,194,176,1)';
$color2_opacity = 'rgba(57,194,176,.95)';
$color2_negative = 'red';
//////////////////////////////////////////

$data = &$retailData['sum'];
$categories = &$retailData['categories'];
$rangeInfo = &$retailData['rangeInfo'];
?>

<div style="position: relative">
    <div class="ht-caption noselect d-flex justify-content-between align-items-center">
        <div class="mr-auto">
            ВЫРУЧКА
        </div>

        <div class="wrap-select2-no-padding mr-3" style="max-height: 44px; text-transform: none;">
            <?= Select2::widget([
                'id' => 'chart-retail-point-select',
                'name' => 'chart-retail-point-select',
                'data' => ['' => 'Все точки'] + $storeArray,
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'value' => $model->store,
                'pluginOptions' => [
                    'width' => '200px',
                    'containerCssClass' => 'select2-retail-store'
                ]
            ]); ?>
        </div>

        <ul class="nav nav-tabs" role="tablist" style="border-bottom: none;">
            <li class="nav-item pr-2">
                <?= Html::tag('span', 'День', [
                    'data-step' => OfdChartModel::STEP_DAY,
                    'class' => 'chart-retail-step nav-link pt-0 pb-0'.
                        ($model->step == OfdChartModel::STEP_DAY ? ' active' : ''),
                ]) ?>
            </li>
            <li class="nav-item pr-2">
                <?= Html::tag('span', 'Месяц', [
                    'data-step' => OfdChartModel::STEP_MONTH,
                    'class' => 'chart-retail-step nav-link pt-0 pb-0'.
                        ($model->step == OfdChartModel::STEP_MONTH ? ' active' : ''),
                ]) ?>
            </li>
        </ul>
    </div>

    <?= Html::tag('span', Icon::get('shevron', ['style' => 'transform: rotate(90deg);']), [
        'data-offset' => $model->offset - ($model->byDays ? 7 : 1),
        'class' => 'link chart-retail-offset',
        'style' => 'position: absolute; left:40px; bottom:25px; z-index: 1;'
    ]) ?>

    <?= Html::tag('span', Icon::get('shevron', ['style' => 'transform: rotate(270deg);']), [
        'data-offset' => $model->offset + ($model->byDays ? 7 : 1),
        'class' => 'link chart-retail-offset',
        'style' => 'position: absolute; right:-5px; bottom:25px; z-index: 1;'
    ]) ?>

    <div class="finance-charts-group " style="min-height:125px; margin-right: 15px;">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-retail',
            'class' => 'finance-charts',
            'scripts' => [
                //'modules/exporting',
                'themes/grid-light',
                'modules/pattern-fill',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'spacing' => [0,0,0,0],
                    'marginBottom' => '50',
                    'marginLeft' => '55',
                    'height' => 250,
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ]
                    //'animation' => false
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                            return '<span class=\"title\">' + window.ChartRetail.labelsX[this.point.index] + '</span>' +
                                '<table class=\"indicators\">' +
                                    '<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>' +
                                '</table>';
                        }
                    ")
                ],
                'title' => ['text' => ''],
                'yAxis' => [
                    'min' => 0,
                    //'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'useHTML' => true,
                        'style' => [
                            'fontWeight' => '300',
                            'fontSize' => '13px',
                            'whiteSpace' => 'nowrap'
                        ]
                    ],
                ],
                'xAxis' => [
                    [
                        'lineColor' => '#9198a0',
                        'categories' => $categories,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("
                                function() {
                                    if (window.ChartRetail) {
                                        var result = (ChartRetail.currDayPos[this.pos]) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (ChartRetail.freeDayPos[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (ChartRetail.wrapPointPos) {
                                            result += ChartRetail.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }
                                }"),
                            'useHTML' => true,
                            'autoRotation' => false,
                        ],
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Выручка',
                        'data' => $data,
                        'color' => $color1,
                        'borderColor' => $color1_opacity,
                        'states' => [
                            'hover' => [
                                'color' => [
                                    'pattern' => [
                                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                        'color' => $color1,
                                        'width' => 5,
                                        'height' => 5
                                    ]
                                ]
                            ],
                        ]
                    ],
                ],
                'plotOptions' => [
                    'series' => [
                        'pointWidth' => 20,
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        //'groupPadding' => 0.05,
                        //'pointPadding' => 0.1,
                        'borderRadius' => 3,
                        'borderWidth' => 1
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>

<script>
    if (typeof ChartRetail === 'object') {
        ChartRetail.period = '<?= ($model->step == OfdChartModel::STEP_DAY ? 'days' : 'months') ?>';
        ChartRetail.labelsX = <?= json_encode($rangeInfo['labelsX'] ?? []) ?>;
        ChartRetail.currDayPos = <?= json_encode($rangeInfo['currDayPos'] ?? []) ?>;
        ChartRetail.freeDayPos = <?= json_encode($rangeInfo['freeDayPos'] ?? []) ?>;
        ChartRetail.wrapPointPos = <?= json_encode($rangeInfo['wrapPointPos'] ?? []) ?>;
    }
</script>