<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.06.2018
 * Time: 7:40
 */

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="odds-panel">
    <div class="main-block">

        <p>Отчет предназначен для предприятий розничной торговли и общественного питания.</p>
        <p class="bold">Зачем нужен отчет «Розничные продажи»?</p>
        <p>Отчет позволяет в режиме on-line видеть выручку по каждой точке продаж, количество чеков, средний чек, типы оплат (наличные и безналичные). Сравнивать точки продаж между собой по выручке, видеть лидеров и аутсайдеров. Также отчет позволяет видеть ТОП продаваемых товаров как по всем точкам, так и в разрезе каждой точки продаж. Вся информация отображается как в виде графиков, так и в табличном виде.</p>
        <p class="bold">Подключение ОФД</p>
        <p>Интеграция с вашим ОФД выполняется на одноименной закладке «ОФД». Процедура интеграции простая. Необходимо выбрать из списка вашего оператора ОФД, нажать «Подключить» и следовать инструкции, которая появится на экране.</p>
        <p>Количество точек продаж не лимитировано. Точки продаж загружаются автоматически из базы ОФД при интеграции.</p>
        <p class="bold">Загрузка чеков</p>
        <p>Нажимаете на кнопку кнопки «Загрузить чеки», расположенной в правом верхнем углу на любой закладке отчета. Загрузка чеков становится доступной после интеграции с вашим ОФД</p>
        <p class="bold">Закладка «Отчеты»</p>
        <p>Тут формируется расширенный Отчет о розничных продажах в разрезе проданной номенклатуры товаров. В отчете можно увидеть объем продаж, сумму продаж, среднюю цену и долю  в объеме продаж в %, по каждой товарной позиции.</p>
        <p>Отчет можно сформировать за любой, установленный вами диапазон времени. С помощью фильтра, можно получить выборку по отдельным точкам продаж, типам оплаты, отдельно по товарам или отдельно по услугам. При необходимости отчет можно сохранить в формате Excel.</p>
        <p class="bold">Закладка «Чеки»</p>
        <p>На этой закладке все чеки загруженные из ОФД. Отчет можно сформировать за любой период времени, а также настроить под себя таблицу включив в нее или исключив из нее те или иные столбцы из числа имеющихся. При необходимости, можно выбрать и удалить чеки, которые не должны участвовать в Отчете о розничных продажах.</p>

    </div>
</div>