<?php

use frontend\modules\retail\models\OfdRetailSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\ofd\OfdStore;
use common\models\ofd\OfdKkt;

\frontend\modules\analytics\assets\AnalyticsAsset::register($this);

/* @var $this yii\web\View
 * @var $searchModel OfdRetailSearch
 * @var $data []
 */

$tabViewClass = $user->config->getTableViewClass('table_retail_sales_view');

$stores = $company
    ->getOfdStores()
    ->joinWith('kkts', false)
    ->orderBy(($user->config->table_retail_sales_sort == $searchModel::SORT_BY_PLACE)
        ? (OfdKkt::tableName().'.name')
        : (OfdStore::tableName().'.local_name'))
    ->all();

$storeDataArray = [];
foreach ($stores as $store) {
    $kkts = $store->kkts;
    if ($user->config->table_retail_sales_zeroes) {
        $ids = $searchModel->getNotEmptyKkts();
        foreach ($kkts as $key => $kkt) {
            if (!in_array($kkt->id, $ids)) {
                unset($kkts[$key]);
            }
        }

        $store->populateRelation('kkts', $kkts);
    }
    $kktCount = count($kkts);

    if ($kktCount > 0) {
        $storeDataArray[] = [
            'store' => $store,
            'count' => $kktCount,
        ];
    }
}

$m = 0;
$active = true;
?>

<div id="cs-table-11x" class="custom-scroll-table-double cs-top">
    <div class="table-wrap">&nbsp;</div>
</div>

<div id="cs-table-2x">
    <div class="table-wrap">
        <table class="table table-style table-count-list">
            <!--CSTable.setStickyCell-->
        </table>
    </div>
    <div class="fixed-first-cell">
        <!--CSTable.setStickyHeader-->
    </div>
</div>


<div class="wrap wrap_padding_none_all" style="position:relative!important;">
<div id="cs-table-1x" class="custom-scroll-table-double">
<div class="table-wrap">
<table id="retail-table" class="table table-style table-count-list <?= $tabViewClass ?>">
    <thead>
        <!-- STORE's -->
        <tr>
            <th style="width: 150px; border-bottom-width: 0;">Период</th>
            <th style="border-bottom-width: 0;">Итого</th>
            <?php foreach ($storeDataArray as $key => $storeData) : ?>
                <?php
                $count = max(1, $storeData['count']);
                $notOne = $count > 1;
                $store = $storeData['store'];
                ?>
                <?= Html::beginTag('th', [
                    'colspan' => $count,
                    'class' => 'nowrap store_column_'.$store->id.($notOne ? ' hidden' : ''),
                ]) ?>
                    <?php if ($notOne) : ?>
                        <?= Html::tag('span', '<span class="table-collapse-icon">&nbsp;</span>' . $store->local_name, [
                            'class' => 'table-collapse-btn store_toggle active',
                            'data-target' => '#retail-table .store_column_'.$store->id,
                        ]) ?>
                    <?php else : ?>
                        <?= $store->local_name ?>
                    <?php endif ?>
                <?= Html::endTag('th') ?>
                <?php if ($notOne) : ?>
                    <th class="nowrap store_column_<?= $store->id ?>">
                        <?= Html::tag('span', '<span class="table-collapse-icon">&nbsp;</span>' . $store->local_name, [
                            'class' => 'table-collapse-btn store_toggle',
                            'data-target' => '#retail-table .store_column_'.$store->id,
                        ]) ?>
                    </th>
                <?php endif ?>
            <?php endforeach ?>
        </tr>
        <!-- KKT's -->
        <?php if ($storeDataArray) : ?>
            <tr>
                <th style="border-top-width: 0;">&nbsp;</th>
                <th style="border-top-width: 0;">&nbsp;</th>
                <?php foreach ($storeDataArray as $key => $storeData) : ?>
                    <?php
                    $count = max(1, $storeData['count']);
                    $notOne = $count > 1;
                    $store = $storeData['store'];
                    ?>
                    <?php if ($store->kkts) : ?>
                        <?php foreach ($store->getKkts()->orderBy('name')->all() as $kkt) : ?>
                            <?= Html::tag('th', $kkt->name, [
                                'class' => 'store_column_'.$store->id.($notOne ? ' hidden' : ''),
                            ]) ?>
                        <?php endforeach ?>
                        <?php if ($notOne) : ?>
                            <th class="store_column_<?= $store->id ?>">
                                Итого
                            </th>
                        <?php endif ?>
                    <?php else : // если нет терминалов ?>
                        <th>--</th>
                    <?php endif ?>
                <?php endforeach ?>
            </tr>
        <?php endif ?>
    </thead>
    <tbody>
        <tr class="total_by_kkt">
            <td class="nowrap weight-700 text_size_14">Итого <?= $searchModel->year ?></td>
            <td class="text-right nowrap weight-700">
                <?= number_format(($data['total'] ?? 0) / 100, 2, ',', ' ') ?>
            </td>
            <?php foreach ($storeDataArray as $key => $storeData) : ?>
                <?php
                $count = max(1, $storeData['count']);
                $notOne = $count > 1;
                $store = $storeData['store'];
                ?>
                <?php if ($store->kkts) : ?>
                    <?php foreach ($store->kkts as $kkt) : ?>
                        <?php
                        $value = $searchModel->getTotalByKkt($kkt->id);
                        ?>
                        <?= Html::tag('td', number_format($value / 100, 2, ',', ' '), [
                            'class' => 'text-right nowrap weight-700 store_column_'.$store->id.($notOne ? ' hidden' : ''),
                        ]) ?>
                    <?php endforeach ?>
                    <?php if ($notOne) : ?>
                        <?php
                        $value = $searchModel->getTotalByStore($store->id);
                        ?>
                        <td class="text-right nowrap weight-700 store_column_<?= $store->id ?>">
                            <?= number_format($value / 100, 2, ',', ' ') ?>
                        </td>
                    <?php endif ?>
                <?php else : // если нет терминалов ?>
                    <td class="text-right">--</td>
                <?php endif ?>
            <?php endforeach ?>
        </tr>

        <?php foreach ($searchModel->getPeriodDateRange() as $date) : ?>
            <?php
            $month = $date->format('n');
            $d = $date->format('Y-m-d');
            $w = $date->format('N');
            ?>
            <?php if ($m != $month) : ?>
                <?php
                $active = $m == 0;
                $m = $month;
                $M = OfdRetailSearch::$month[$date->format('m')];
                ?>
                <tr>
                    <td class="weight-700 text_size_14">
                        <?= Html::tag('span', '<span class="table-collapse-icon">&nbsp;</span>' . $M, [
                            'class' => 'table-collapse-btn month_toggle '.($active ? 'active' : ''),
                            'data-target' => '#retail-table .month_days_'.$m,
                        ]) ?>
                    </td>
                    <td class="text-right nowrap font-weight-bold">
                        <?= number_format(($data[$m]['summary']['total'] ?? 0) / 100, 2, ',', ' ') ?>
                    </td>
                    <?php foreach ($storeDataArray as $key => $storeData) : ?>
                        <?php
                        $count = max(1, $storeData['count']);
                        $notOne = $count > 1;
                        $store = $storeData['store'];
                        ?>
                        <?php if ($store->kkts) : ?>
                            <?php foreach ($store->kkts as $kkt) : ?>
                                <?php
                                $value = $data[$m]['summary'][$store->id][$kkt->id] ?? 0;
                                ?>
                                <?= Html::tag('td', number_format($value / 100, 2, ',', ' '), [
                                    'class' => 'text-right nowrap font-weight-bold store_column_'.$store->id.($notOne ? ' hidden' : ''),
                                ]) ?>
                            <?php endforeach ?>
                            <?php if ($notOne) : ?>
                                <?php
                                $value = $data[$m]['summary'][$store->id]['total'] ?? 0;
                                ?>
                                <td class="text-right font-weight-bold nowrap store_column_<?= $store->id ?>">
                                    <?= number_format($value / 100, 2, ',', ' ') ?>
                                </td>
                            <?php endif ?>
                        <?php else : // если нет терминалов ?>
                            <th>--</th>
                        <?php endif ?>
                    <?php endforeach ?>
                </tr>
            <?php endif ?>
            <tr class="month_days_<?= $m ?> <?= $active ? '' : 'hidden' ?>">
                <td>
                    <div class="nowrap text-grey text_size_14" style="padding-left: 22px;">
                        <?= $date->format('d.m.Y'); ?>
                        <?= Html::tag('span', OfdRetailSearch::$day[$date->format('N')], [
                            'class' => $w > 5 ? 'text-danger' : '',
                        ]) ?>
                    </div>
                </td>
                <td class="text-right nowrap">
                    <?= number_format(($data[$m][$d]['total'] ?? 0) / 100, 2, ',', ' ') ?>
                </td>
                <?php foreach ($storeDataArray as $key => $storeData) : ?>
                    <?php
                    $count = max(1, $storeData['count']);
                    $notOne = $count > 1;
                    $store = $storeData['store'];
                    ?>
                    <?php if ($store->kkts) : ?>
                        <?php foreach ($store->kkts as $kkt) : ?>
                            <?php
                            $value = $data[$m][$d][$store->id][$kkt->id] ?? 0;
                            ?>
                            <?= Html::tag('td', number_format($value / 100, 2, ',', ' '), [
                                'class' => 'text-right nowrap store_column_'.$store->id.($notOne ? ' hidden' : ''),
                            ]) ?>
                        <?php endforeach ?>
                        <?php if ($notOne) : ?>
                            <?php
                            $value = $data[$m][$d][$store->id]['total'] ?? 0;
                            ?>
                            <td class="text-right nowrap store_column_<?= $store->id ?>">
                                <?= number_format($value / 100, 2, ',', ' ') ?>
                            </td>
                        <?php endif ?>
                    <?php else : // если нет терминалов ?>
                        <td class="text-right">--</td>
                    <?php endif ?>
                <?php endforeach ?>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
</div>
</div>
</div>

<script>


</script>

<?php $this->registerJs(<<<JS

    CSTable2 = CSTable;
    CSTable2._refreshStickyColumnsHeight = function() {
        const headRow = $(this.wrapper).find('thead tr:first-child th');
        const firstHeaderColumn = $(headRow).first();
        const totalHeaderColumn = $(headRow).eq(1);
        $(firstHeaderColumn).css({'height': totalHeaderColumn.outerHeight() + 'px'});
    };
    CSTable2._refreshTopScrollbar = function() {
        $(this.topScrollbar + ' > .table-wrap').width($(this.wrapper + ' > .table-wrap > .table').width() + 150 /*padding-left*/);
    };
    
    CSTable2.refresh = function() {
        this._refreshTopScrollbar();
        this._refreshStickyColumnsHeight();
    }; 
    
    CSTable2.init = function() {
        // bindEvents
        this._bindScrollEvent();
        // setSticky
        this._setStickyColumn();
        this._refreshTopScrollbar();
        // sync
        this.syncScrollbars();
        // firstColumn height
        this._refreshStickyColumnsHeight();
    };
    
    window.__csTable2_refreshed = false;
    $(window).on('resize', function() {
        if (!__csTable2_refreshed) {
            window.__csTable2_refreshed = true;
            setTimeout(function() { CSTable2.refresh(); window.__csTable2_refreshed = false; }, 500);
        }
    });

    $(document).ready(function() {
        ////////////////
        CSTable2.init();
        ////////////////
    });

    $(document).on("click", "#retail-table .month_toggle", function (e) {
        let isShown = $(this).hasClass("active");
        let target = $(this).data("target");
        $(target).toggleClass("hidden", isShown);
        $(this).toggleClass("active", !isShown);
    });
    $(document).on("click", "#retail-table .store_toggle", function (e) {
        $($(this).data("target")).toggleClass("hidden");
    });
JS
) ?>

<!-- fix fixed elements width/height -->
<style>
    #retail-table td {
        height: 45px!important;
    }
    #retail-table.table-compact td {
        height: 30px!important;
    }
    #cs-table-1x .table.table-compact tbody tr td:first-child {
        height: 30px!important;
    }
    #cs-table-1x.sticky .table-wrap {
        padding-left: 150px;
    }
    #cs-table-1x.sticky .table thead tr th:first-child,
    #cs-table-1x.sticky .table tbody tr td:first-child {
        opacity: 1;
        width: 150px;
        padding-top: 12px!important;
    }
    #cs-table-1x.sticky .table-compact thead tr th:first-child,
    #cs-table-1x.sticky .table-compact tbody tr td:first-child {
        padding-top: 8px!important;
    }
</style>
