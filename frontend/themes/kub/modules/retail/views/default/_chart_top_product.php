<?php

use yii\web\JsExpression;
use frontend\modules\retail\models\OfdChartModel;

/** @var $topProductData array */
/** @var $model OfdChartModel */

///////////////// colors /////////////////
$color1 = 'rgba(57,194,176,1)';
$color1_opacity = 'rgba(57,194,176,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
//////////////////////////////////////////

///////////////// consts /////////////////
$chartHeightHeader = 42;
$chartHeightFooter = 0;
$chartHeightColumn = 30;
//////////////////////////////////////////

$categories  = &$topProductData['categories'];
$productsIds = &$topProductData['id'];
$data        = &$topProductData['data'];
$totalAmount = $model->getTotalByMonth() ?: 9E9;

///////////////// calc /////////////////
$calculatedChartHeight = count($data) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$activeProductPos = ($model->product) ? array_search($model->product, $productsIds) : false;
?>

<script>
    if (typeof ChartRetail !== "undefined") {
        ChartRetail.productArray = <?= json_encode(array_combine($productsIds, $categories)) ?>;
        <?php if ($activeProductPos !== false): ?>
            ChartRetail.setActiveTopProduct(<?= $activeProductPos ?>);
        <?php else: ?>
            ChartRetail.setActiveTopProduct(false);
        <?php endif; ?>
    }
</script>

<div style="position: relative">
    <div class="ht-caption noselect d-flex justify-content-between align-items-center" style="min-height: 44px;">
        <div>
            ТОП ТОВАРОВ <span style="text-transform: none">по</span> ВЫРУЧКЕ
        </div>
    </div>
    <div class="finance-charts-group">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-top-1',
        'class' => 'finance-charts',
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'spacing' => [0,0,0,0],
                'height' => $calculatedChartHeight,
                'inverted' => true,
                //'style' => [
                //    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                //],
                'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'shape' => 'rect',
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {
                
                        const totalAmount = {$totalAmount};

                        return '<span class=\"title\">' + this.point.category + '</span>' +
                                '<table class=\"indicators\">' + 
                                    ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>') + 
                                    ('<tr>' + '<td class=\"gray-text\">Доля в выручке: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * this.y / totalAmount, 2, ',', ' ') + ' %</td></tr>') +
                                '</table>';
                    }
                "),
                'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x: point.plotX / 3 - 25, y: point.plotY + 50}; }'),
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'endOnTick' => false,
                'tickPixelInterval' => 1,
                'visible' => false
            ],
            'xAxis' => [
                'categories' => $categories,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'gridLineWidth' => 0,
                'offset' => 0,
                'labels' => [
                    'align' => 'left',
                    'reserveSpace' => true
                ],
            ],
            'series' => [
                [
                    'name' => 'Выручка',
                    'pointPadding' => 0,
                    'data' => $data,
                    'color' => $color1,
                    'borderColor' => $color1_opacity,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color1,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ],
                    ]
                ],
            ],
            'plotOptions' => [
                'column' => [
                    'animation' => false,
                    'pointWidth' => 18,
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'borderRadius' => 3,
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                    'point' => [
                        'events' => [
                            'click' => new JsExpression("
                                function() {     
                                                              
                                    if (window.ChartRetail) {

                                        let productId = null;
                                        let productName = this.category;

                                        $.each(ChartRetail.productArray, function(id,val) { 
                                            if (val == productName) {
                                                productId = id;
                                                return false;                                            
                                            } 
                                        });

                                        if (ChartRetail.activeTopProductPos === false || ChartRetail.activeTopProductPos !== this.index) {
                                            ChartRetail.setFilter('product', productId); 
                                        } else {
                                            ChartRetail.resetFilter('product');
                                        }    
                                  
                                        ChartRetail.refresh();                                     
                                        
                                    } else {

                                        window.toastr.error('Ошибка загрузки');                                 
                                    }                                   
                                }
                            ")
                        ]
                    ],
                ],
            ],
        ],
    ]); ?>
    </div>
</div>