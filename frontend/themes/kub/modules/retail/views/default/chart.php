<?php

use yii\widgets\Pjax;
use yii\helpers\Url;

/** @var $model \frontend\modules\retail\models\OfdChartModel */
/** @var $retailData array */
/** @var $receiptData array */
/** @var $topProductData array */
/** @var $cashTypeData array */
/** @var $topStoreData array */
/** @var $storeArray array */
?>

<?php Pjax::begin([
    'id' => 'retail-charts-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.retail-charts-link',
    'timeout' => 10000,
]); ?>

<div class="row mb-4 pb-3">
    <div class="col-8">
        <?= $this->render('_chart_retail', [
            'company' => $company,
            'model' => $model,
            'retailData' => $retailData,
            'storeArray' => $storeArray
        ]); ?>
    </div>
    <div class="col-4">
        <?= $this->render('_chart_top_store', [
            'company' => $company,
            'model' => $model,
            'topStoreData' => $topStoreData,
            'storeArray' => $storeArray
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-8">
        <div class="pb-3">
            <?= $this->render('_chart_receipt', [
                'company' => $company,
                'model' => $model,
                'receiptData' => $receiptData,
            ]); ?>
        </div>
        <div class="pb-3">
            <?= $this->render('_chart_cash_type', [
                'company' => $company,
                'model' => $model,
                'cashTypeData' => $cashTypeData,
            ]); ?>
        </div>
    </div>
    <div class="col-4">
        <div class="pb-3">
            <?= $this->render('_chart_top_product', [
                'company' => $company,
                'model' => $model,
                'topProductData' => $topProductData,
            ]) ?>
        </div>
    </div>
</div>

<?php Pjax::end(); ?>