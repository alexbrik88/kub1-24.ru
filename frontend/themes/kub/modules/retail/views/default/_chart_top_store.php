<?php

use yii\web\JsExpression;
use kartik\select2\Select2;
use frontend\modules\retail\models\OfdChartModel;

/** @var $topStoreData array */
/** @var $model OfdChartModel */

///////////////// dynamic vars ///////////
$customMonth = $model->getMonth();
///////////////// colors /////////////////
$color1 = 'rgba(57,194,176,1)';
$color1_opacity = 'rgba(57,194,176,.95)';
$color2 = 'rgba(226,229,234,1)';
$color2_opacity = 'rgba(226,229,234,.95)';
///////////////// consts /////////////////
$chartHeightHeader = 42;
$chartHeightFooter = 0;
$chartHeightColumn = 30;
////////////////// data //////////////////
$categories  = &$topStoreData['categories'];
$storesIds   = &$topStoreData['id'];
$data        = &$topStoreData['data'];

$jsTooltipData = [
    'percent_cash' => json_encode($topStoreData['percent_cash']),
    'percent_cashless' => json_encode($topStoreData['percent_cashless']),
    'total_amount' => $model->getTotalByMonth() ?: 9E9
];

///////////////// calc /////////////////
$calculatedChartHeight = count($data) * $chartHeightColumn + $chartHeightHeader + $chartHeightFooter;
$activeStorePos = ($model->store) ? array_search($model->store, $storesIds) : false;
?>

<script>
    if (typeof ChartRetail !== "undefined") {
        <?php if ($activeStorePos !== false): ?>
            ChartRetail.setActiveTopStore(<?= $activeStorePos ?>);
        <?php else: ?>
            ChartRetail.setActiveTopStore(false);
        <?php endif; ?>
    }
</script>

<div style="position: relative">
    <div class="ht-caption noselect d-flex justify-content-between align-items-center">
        <div class="mr-auto" style="min-height: 18px;">
            ТОП ТП <span style="text-transform: none">по</span> ВЫРУЧКЕ
        </div>
        <div class="wrap-select2-no-padding ml-1" style="max-height: 44px; text-transform: none;">
            <?= Select2::widget([
                'hideSearch' => true,
                'id' => 'chart-top-store-select',
                'name' => 'chart-top-store-select',
                'data' => array_filter(array_reverse(OfdChartModel::$month, true),
                    function($k) use ($model) {
                        return ($model->year < date('Y') || (int)$k <= date('n'));
                    }, ARRAY_FILTER_USE_KEY),
                'options' => [
                    'class' => 'form-control',
                    'style' => 'display: inline-block;',
                ],
                'value' => (int)$customMonth,
                'pluginOptions' => [
                    'width' => '106px',
                    'containerCssClass' => 'select2-retail-store'
                ]
            ]); ?>
        </div>
    </div>
    <div class="finance-charts-group">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'id' => 'chart-top-2',
        'class' => 'finance-charts',
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
            'modules/pattern-fill',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'spacing' => [0,0,0,0],
                'height' => $calculatedChartHeight,
                'inverted' => true,
                //'style' => [
                //    'fontFamily' => '"Corpid E3 SCd", sans-serif',
                //],
                'animation' => false
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'useHTML' => true,
                'shape' => 'rect',
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
                'borderRadius' => 8,
                'formatter' => new jsExpression("
                    function(args) {
                
                        const totalAmount = {$jsTooltipData['total_amount']};
                        const percentCashArr = {$jsTooltipData['percent_cash']};
                        const percentCashlessArr = {$jsTooltipData['percent_cashless']};
                        
                        const index = this.point.index;
                        const percentCash = percentCashArr[index] || 0;
                        const percentCashless = percentCashlessArr[index] || 0;

                        return '<span class=\"title\">' + this.point.category + '</span>' +
                                '<table class=\"indicators\">' + 
                                    ('<tr>' + '<td class=\"gray-text\">Выручка по ТП: ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>') + 
                                    ('<tr>' + '<td class=\"gray-text\">Нал / Безнал: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(percentCash, 2, ',', ' ') + ' % / ' + Highcharts.numberFormat(percentCashless, 2, ',', ' ') + ' %</td></tr>') +                                    
                                    ('<tr>' + '<td class=\"gray-text\">Доля в общей выручке: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * this.y / totalAmount, 2, ',', ' ') + ' %</td></tr>') +
                                '</table>';
                    }
                "),
                'positioner' => new \yii\web\JsExpression('function (boxWidth, boxHeight, point) { return {x: point.plotX / 3 - 25, y: point.plotY + 50}; }'),
            ],
            'title' => ['text' => ''],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'endOnTick' => false,
                'tickPixelInterval' => 1,
                'visible' => false
            ],
            'xAxis' => [
                'categories' => $categories,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'gridLineWidth' => 0,
                'offset' => 0,
                'labels' => [
                    'align' => 'left',
                    'reserveSpace' => true
                ],
            ],
            'series' => [
                [
                    'name' => 'Выручка',
                    'pointPadding' => 0,
                    'data' => $data,
                    'color' => $color1,
                    'borderColor' => $color1_opacity,
                    'states' => [
                        'hover' => [
                            'color' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                    'color' => $color1,
                                    'width' => 5,
                                    'height' => 5
                                ]
                            ]
                        ],
                    ]
                ],
            ],
            'plotOptions' => [
                'column' => [
                    'animation' => false,
                    'pointWidth' => 18,
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0,
                    'borderRadius' => 3,
                    'states' => [
                        'inactive' => [
                            'opacity' => 1
                        ],
                    ],
                    'point' => [
                        'events' => [
                            'click' => new JsExpression("
                                function() {                                   
                                    if (window.ChartRetail) {

                                        let storeId = null;
                                        let storeName = this.category;
                                        
                                        $.each(ChartRetail.storeArray, function(id,val) { 
                                            if (val == storeName) {
                                                storeId = id;
                                                return false;                                            
                                            } 
                                        });
                                        
                                        if (ChartRetail.activeTopStorePos === false || ChartRetail.activeTopStorePos !== this.index) {
                                            ChartRetail.setFilter('store', storeId); 
                                        } else {
                                            ChartRetail.resetFilter('store');
                                        }
                                        
                                        ChartRetail.refresh();
                                                                             
                                    } else {
                                    
                                        window.toastr.error('Ошибка загрузки');
                                    }
                                }
                            ")
                        ]
                    ],
                ],
            ],
        ],
    ]); ?>
    </div>
</div>