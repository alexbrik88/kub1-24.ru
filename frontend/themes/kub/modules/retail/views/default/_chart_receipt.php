<?php

use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var $this yii\web\View */
/** @var $receiptData array */

///////////////// colors /////////////////
$color1 = 'rgba(243,183,46,1)';
$color1_opacity = 'rgba(243,183,46,.5)';
$color2 = 'rgba(57,194,176,1)';
$color2_opacity = 'rgba(57,194,176,.95)';
$color2_negative = 'red';
//////////////////////////////////////////

$categories = $receiptData['categories'];
$averageCheck = $receiptData['avg'];
$checksCount = $receiptData['count'];
?>

<div style="position: relative">

    <?= Html::tag('span', Icon::get('shevron', ['style' => 'transform: rotate(90deg);']), [
        'data-offset' => $model->offset - ($model->byDays ? 7 : 1),
        'class' => 'link chart-retail-offset',
        'style' => 'position: absolute; left:40px; bottom:25px; z-index: 1;'
    ]) ?>

    <?= Html::tag('span', Icon::get('shevron', ['style' => 'transform: rotate(270deg);']), [
        'data-offset' => $model->offset + ($model->byDays ? 7 : 1),
        'class' => 'link chart-retail-offset',
        'style' => 'position: absolute; right:-5px; bottom:25px; z-index: 1;'
    ]) ?>

    <div class="ht-caption noselect d-flex justify-content-between align-items-center" style="min-height: 44px;">
        <div>
            КОЛ-ВО ЧЕКОВ <span style="text-transform: none">и</span> СРЕДНИЙ ЧЕК
        </div>
    </div>

    <div class="finance-charts-group" style="min-height:125px; margin-right: 15px;">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-receipt',
            'class' => 'finance-charts',
            'scripts' => [
                //'modules/exporting',
                'themes/grid-light',
                'modules/pattern-fill',
            ],
            'options' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'type' => 'column',
                    'spacing' => [0,0,0,0],
                    'marginBottom' => '50',
                    'marginLeft' => '55',
                    'height' => 250,
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif',
                    ],
                    //'animation' => false
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '11px',
                        'color' => '#9198a0'
                    ],
                    'symbolRadius' => 2
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    'formatter' => new jsExpression("
                        function(args) {
                                                    
                            return '<span class=\"title\">' + window.ChartRetail.labelsX[this.point.index] + '</span>' +
                                '<table class=\"indicators\">' +
                                    (this.series.index == 0 ?
                                    ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 2, ',', ' ') + ' ₽</td></tr>') :
                                    ('<tr>' + '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, 0, ',', ' ') + '</td></tr>')) +
                                '</table>';
                        }
                    "),
                ],
                'title' => ['text' => ''],
                'yAxis' => [
                        [
                            'min' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'labels' => [
                                'useHTML' => true,
                                'style' => ['fontWeight' => '300', 'fontSize' => '13px', 'whiteSpace' => 'nowrap']
                            ],
                        ],
                        [
                            'min' => 0,
                            'title' => '',
                            'minorGridLineWidth' => 0,
                            'max' => max($checksCount) * 2,
                            'opposite' => true,
                            'labels' => false
                        ],
                ],
                'xAxis' => [
                    [
                        'lineColor' => '#9198a0',
                        'categories' => $categories,
                        'labels' => [
                            'formatter' => new \yii\web\JsExpression("
                                function() {
                                    if (window.ChartRetail) {
                                        var result = (ChartRetail.currDayPos[this.pos]) ?
                                            ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                                            (ChartRetail.freeDayPos[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));
    
                                        if (ChartRetail.wrapPointPos) {
                                            result += ChartRetail.getWrapPointXLabel(this.pos);
                                        }
    
                                        return result;
                                    }
                                }"),
                            'useHTML' => true,
                            'autoRotation' => false,
                        ],
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Средний чек',
                        'data' => $averageCheck,
                        'color' => $color1,
                        'borderColor' => $color1_opacity,
                        'states' => [
                            'hover' => [
                                'color' => [
                                    'pattern' => [
                                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                        'color' => $color1,
                                        'width' => 5,
                                        'height' => 5
                                    ]
                                ]
                            ],
                        ],
                        'yAxis' => 0
                    ],
                    [
                        'name' => 'Количество чеков',
                        'type' => 'line',
                        'data' => $checksCount,
                        'color' => $color2,
                        'borderColor' => $color2_opacity,
                        'states' => [
                            'hover' => [
                                'color' => [
                                    'pattern' => [
                                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                                        'color' => $color2,
                                        'width' => 5,
                                        'height' => 5
                                    ]
                                ]
                            ],
                        ],
                        'yAxis' => 1
                    ],
                ],
                'plotOptions' => [
                    'series' => [
                        'pointWidth' => 20,
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ],
                        ],
                        'groupPadding' => 0.05,
                        'pointPadding' => 0.1,
                        'borderRadius' => 3,
                        'borderWidth' => 1
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>