<?php

use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ofd\OfdReceipt */

$date = date_create($model->date_time);
$this->title = "Чек № {$model->document_number} от {$date->format("d.m.Y")} в {$date->format("H:i")}";
$this->params['breadcrumbs'][] = ['label' => 'Ofd Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="modal-header">
    <h5 class="modal-title">
        <?= Html::encode($this->title) ?>
    </h5>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-hidden="true">
        <?= Icon::get('close') ?>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-6">
            <label>Тип</label>
            <div class="font-weight-bold">
                <?= Html::encode(ArrayHelper::getValue($model, ['operationType', 'name'])) ?>
            </div>
        </div>
        <div class="col-6">
            <label>Продажа №</label>
            <div class="font-weight-bold">
                <?= Html::encode($model->number) ?>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-6">
            <label>Точка продаж</label>
            <div class="font-weight-bold">
                <?= Html::encode(ArrayHelper::getValue($model, ['kkt', 'store', 'name'])) ?>
            </div>
        </div>
        <div class="col-6">
            <div>
                <label>Смена:</label>
                <strong>
                    <?= Html::encode($model->shift_number) ?>
                </strong>
            </div>
            <div>
                <label>Кассир:</label>
                <strong>
                    <?= Html::encode($model->operator_name) ?>
                </strong>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <table class="table table-style table-count-list">
            <thead>
                <tr>
                    <th>№№</th>
                    <th>Наименование</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <!-- <th>Скидка</th> -->
                    <th>Сумма</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->items as $key => $item) : ?>
                    <tr>
                        <td><?= ++$key ?></td>
                        <td><?= Html::encode($item->name) ?></td>
                        <td class="text-nowrap text-right"><?= Html::encode($item->quantity) ?></td>
                        <td class="text-nowrap text-right"><?= number_format($item->price / 100, 2, ',', ' ') ?></td>
                        <!-- <td></td> -->
                        <td class="text-nowrap text-right"><?= number_format($item->sum / 100, 2, ',', ' ') ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <table class="table table-style table-count-list">
            <tr>
                <td style="width: 80%"></td>
                <td>Итого:</td>
                <td class="text-nowrap text-right"><?= number_format($model->total_sum / 100, 2, ',', ' ') ?></td>
            </tr>
            <tr>
                <td style="width: 80%"></td>
                <td><strong>Оплата</strong></td>
                <td></td>
            </tr>
            <tr>
                <td style="width: 80%"></td>
                <td>Наличными:</td>
                <td class="text-nowrap text-right"><?= number_format($model->cash_sum / 100, 2, ',', ' ') ?></td>
            </tr>
            <tr>
                <td style="width: 80%"></td>
                <td>Безналичными:</td>
                <td class="text-nowrap text-right"><?= number_format($model->cashless_sum / 100, 2, ',', ' ') ?></td>
            </tr>
        </table>
    </div>
</div>
