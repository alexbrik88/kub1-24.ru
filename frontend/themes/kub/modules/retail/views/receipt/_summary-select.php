<?php

use common\models\cash\CashFlowsBase;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $buttons array */
$buttons = isset($buttons) ? (array)$buttons : [];
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-panel-checkbox" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Приход: <strong class="total-income ml-1">0</strong>
            <strong>₽</strong>
        </div>
        <div class="column column-expense total-txt-foot mr-3">
            Расход: <strong class="total-expense ml-1">0</strong>
            <strong>₽</strong>
        </div>

        <div class="column ml-auto"></div>

        <?php foreach (array_filter($buttons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>
    </div>
</div>

<?php
$this->registerJs(<<<SCRIPT

// fast joint operations checkboxes

$(document).on('change', '.joint-main-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');

    panelCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.last().trigger('change');
});

$(document).on('change', '.joint-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const mainCheckbox = table.find('.joint-main-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');
    const checkedCheckboxes = checkboxes.filter(':checked');
    const copyButton = $('#summary-container').find('#button-copy-flow-item');

    if (checkboxes.length === checkedCheckboxes.length) {
        mainCheckbox.prop('checked', true).uniform('refresh');
        panelCheckbox.prop('checked', true).uniform('refresh');
    } else {
        mainCheckbox.prop('checked', false).uniform('refresh');
        panelCheckbox.prop('checked', false).uniform('refresh');
    }

    // copy button

    if (checkedCheckboxes.length === 1 && $(checkedCheckboxes).data('copy-url')) {
        copyButton
            .attr('href', $(checkedCheckboxes).data('copy-url'))
            .addClass('ajax-modal-btn')
            .removeClass('hidden');
    } else {
        copyButton
            .attr('href', 'javascript:void(0)')
            .removeClass('ajax-modal-btn')
            .addClass('hidden');
    }
});

$(document).on('change', '.joint-panel-checkbox', function() {
    const table = $('.table-retail-data').first();
    const mainCheckbox = table.find('.joint-main-checkbox');

    mainCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh').trigger('change');
});

$(document).on('change', '.joint-checkbox', function(e) {
    let countChecked = 0;
    let inSum = 0;
    let outSum = 0;
    let diff = 0;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

SCRIPT);
?>