<?php

use common\models\ofd\OfdOperationType;
use frontend\components\Icon;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\retail\models\OfdReceiptSearch;
use common\models\ofd\OfdReceipt;

/* @var $this yii\web\View */
/* @var $searchModel OfdReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Чеки';

$userConfig = $user->config;
$tabConfigClass = [
    'expense' => 'col_table_retail_receipt_expense' . ($userConfig->table_retail_receipt_expense ? '' : ' hidden'),
    'payment' => 'col_table_retail_receipt_payment' . ($userConfig->table_retail_receipt_payment ? '' : ' hidden'),
    'store' => 'col_table_retail_receipt_store'  . ($userConfig->table_retail_receipt_store ? '' : ' hidden'),
    'kkt' => 'col_table_retail_receipt_kkt' . ($userConfig->table_retail_receipt_kkt ? '' : ' hidden'),
    'number' => 'col_table_retail_receipt_number' . ($userConfig->table_retail_receipt_number ? '' : ' hidden'),
    'document' => 'col_table_retail_receipt_document' . ($userConfig->table_retail_receipt_document ? '' : ' hidden'),
    'shift' => 'col_table_retail_receipt_shift' . ($userConfig->table_retail_receipt_shift ? '' : ' hidden'),
];
$tabViewClass = $userConfig->getTableViewClass('table_retail_sales_view');
$canDelete = Yii::$app->user->can(UserRole::ROLE_CHIEF);
?>

<div class="wrap pt-0 pb-0 pl-4 pr-3 mb-3">
    <div class="p-2">
        <div class="row align-items-center">
            <div class="col-6 col-xl-9">
                <h4 class="mb-0"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="col-6 col-xl-3">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-nowrap my-2 py-1 align-items-center">
    <?php if (YII_ENV_DEV || !$company->isFreeTariff) : ?>
        <div class="mr-2 pr-1">
            <?= \yii\bootstrap\Html::button(Icon::get('exel'), [
                'class' => 'button-list button-hover-transparent',
                //'title' => 'Скачать в Excel',
                'disabled' => true,
                'title' => 'В разработке',
                'style' => 'background-color: #335A82; border-color: #335A82; cursor: not-allowed;'
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="pr-1">
        <?= TableConfigWidget::widget([
            'items' => $tableConfig ?? [
                    ['attribute' => 'table_retail_receipt_document'],
                    ['attribute' => 'table_retail_receipt_number'],
                    ['attribute' => 'table_retail_receipt_expense'],
                    ['attribute' => 'table_retail_receipt_payment'],
                    ['attribute' => 'table_retail_receipt_store'],
                    ['attribute' => 'table_retail_receipt_kkt'],
                    ['attribute' => 'table_retail_receipt_shift'],
            ],
        ]); ?>
    </div>
    <div>
        <?= TableViewWidget::widget(['attribute' => 'table_retail_sales_view']) ?>
    </div>
    <div class="col-6 ml-auto pr-0">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => '',
            ],
        ]); ?>
        <div class="d-flex flex-nowrap align-items-center">
        <div class="flex-grow-1 mr-2">
            <?= \yii\bootstrap4\Html::activeTextInput($searchModel, 'document_number', [
                'type' => 'search',
                'placeholder' => 'Поиск по номеру чека',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="ofd-receipt-index">
    <?= common\components\grid\GridView::widget([
        'id' => 'ofd_receipt_grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table-retail-data table table-style table-count-list ' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'joint-main-checkbox-td text-left',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center joint-checkbox-td',
                ],
                'format' => 'raw',
                'value' => function ($flows) {
                    if (in_array($flows->operation_type_id, OfdOperationType::$incomes)) {
                        $typeCss = 'income-item';
                        $income = round($flows['total_sum'] / 100, 2);
                        $expense = 0;
                    } else {
                        $typeCss = 'expense-item';
                        $expense = round($flows['total_sum'] / 100, 2);
                        $income = 0;
                    }

                    return Html::checkbox("receiptId[]", false, [
                        'class' => 'joint-checkbox ' . $typeCss,
                        'value' => $flows['id'],
                        'data' => [
                            'income' => $income,
                            'expense' => $expense
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date_time',
                'label' => 'Дата',
                'headerOptions' => [
                    'width' => '15%',
                ],
            ],
            [
                'attribute' => 'document_number',
                'headerOptions' => [
                    'width' => '5%',
                    'class' => $tabConfigClass['document'],
                ],
                'contentOptions' => [
                    'class' => 'text-right ' . $tabConfigClass['document'],
                ],
                'format' => 'raw',
                'value' => function (OfdReceipt $model)
                {
                    return Html::tag('div', htmlspecialchars($model->document_number), [
                        'class' => 'link show_receipt_item',
                        'data-url' => Url::to([
                            '/retail/receipt/view',
                            'id' => $model->id,
                        ]),
                    ]);
                }
            ],
            [
                'attribute' => 'number',
                'headerOptions' => [
                    'width' => '5%',
                    'class' => $tabConfigClass['number'],
                ],
                'contentOptions' => [
                    'class' => 'text-right ' . $tabConfigClass['number'],
                ],
            ],
            [
                'attribute' => 'income_sum',
                'label' => 'Приход',
                'contentOptions' => [
                    'class' => 'text-right nowrap'
                ],
                'value' => function ($model) {
                    return $model->income_sum ? Yii::$app->formatter->asMoney($model->income_sum) : '-';
                }
            ],
            [
                'attribute' => 'return_income_sum',
                'label' => 'Возврат прихода',
                'contentOptions' => [
                    'class' => 'text-right nowrap'
                ],
                'value' => function ($model) {
                    return $model->return_income_sum ? Yii::$app->formatter->asMoney($model->return_income_sum) : '-';
                }
            ],
            [
                'attribute' => 'expense_sum',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap ' . $tabConfigClass['expense'],
                ],
                'value' => function ($model) {
                    return $model->expense_sum ? Yii::$app->formatter->asMoney($model->expense_sum) : '-';
                }
            ],
            [
                'attribute' => 'return_expense_sum',
                'label' => 'Возврат расхода',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap ' . $tabConfigClass['expense'],
                ],
                'value' => function ($model) {
                    return $model->return_expense_sum ? Yii::$app->formatter->asMoney($model->return_expense_sum) : '-';
                }
            ],
            [
                'attribute' => 'payment_type_id',
                'label' => 'Тип оплаты',
                'headerOptions' => [
                    'width' => '10%',
                    'class' => $tabConfigClass['payment'],
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['payment'],
                ],
                'value' => 'payTypeLabel',
                'filter' => $searchModel->getPayTypeFilter(),
                's2width' => '140px',
            ],
            [
                'attribute' => 'store_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'width' => '10%',
                    'class' => $tabConfigClass['store'],
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['store'],
                ],
                'value' => 'store.local_name',
                'filter' => $searchModel->getStoreFilter(),
                's2width' => '300px',
            ],
            [
                'attribute' => 'kkt_id',
                'label' => 'Наименование ККТ',
                'headerOptions' => [
                    'class' => $tabConfigClass['kkt'],
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['kkt'],
                ],
                'value' => 'kkt.name',
                'filter' => $searchModel->getKktFilter(),
                's2width' => '300px',
            ],
            [
                'attribute' => 'shift_number',
                'headerOptions' => [
                    'width' => '5%',
                    'class' => $tabConfigClass['shift'],
                ],
                'contentOptions' => [
                    'class' => 'text-right ' . $tabConfigClass['shift'],
                ],
                'filter' => $searchModel->getShiftNumberFilter(),
                's2width' => '150px',
            ],
        ],
    ]); ?>
</div>

<?= $this->render('_summary-select', [
    'buttons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ]
]) ?>

<?= $this->render('_modals') ?>