<?php
use yii\helpers\Url;
?>
<!-- Modal Many Delete -->
<div id="many-delete" class="modal-many-delete-item confirm-modal fade modal" role="modal" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные чеки?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                    'data-style' => 'expand-right',
                    'data-url' => Url::to(['/retail/receipt/many-delete']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<script>

    // confirm many delete
    $(document).on("click", ".modal-many-delete-item .btn-confirm-yes", function() {
        if (!$(this).hasClass('clicked')) {
            if ($('.joint-checkbox:checked').length > 0) {
                $(this).addClass('clicked');
                $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {});
            }
        }
    });

    //$(document).on("change", "input.select-on-check-all", function (e) {
    //    let checked = this.checked;
    //    console.log(checked);
    //    $('td.checkbox-cell input[type=checkbox]').prop('checked', checked).uniform('refresh');
    //});

</script>