<?php

use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\ofd\models\OfdStoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Точки продаж';

$storeArray = $dataProvider->models;
?>
<div class="ofd-store-index">
    <div class="row mb-3">
        <div class="col-3">
            <h4>Магазины</h4>
        </div>
        <div class="col-8">
            <h4>Кассы</h4>
        </div>
    </div>
    <div class="wrap wrap_padding_none">
        <div class="row ml-0">
            <ul class="col-3 nav flex-column ofd_store_list pr-0">
                <?php foreach ($storeArray as $key => $store) : ?>
                    <li class="nav-item  pr-0">
                        <?= Html::a(Html::tag('span', $store->local_name) . Icon::get('arrow-right', [
                            'class' => 'visible_by_active ml-1',
                        ]), '#ofd_store_ktk_'.$store->id, [
                            'id' => 'ofd_store_'.$store->id,
                            'class' => 'nav-link d-flex ofd_store_link justify-content-between '.($key === 0 ? 'active' : ''),
                            'data-toggle' => 'tab',
                            'data-url' => Url::to(['view', 'id' => $store->id]),
                            'role' => 'tablist',
                            'style' => 'padding-left: 15px;',
                        ]) ?>
                    </li>
                <?php endforeach ?>
            </ul>
            <div class="col-8 tab-content wrap">
                <?php foreach ($storeArray as $key => $store) : ?>
                    <?= Html::beginTag('div', [
                        'id' => 'ofd_store_ktk_'.$store->id,
                        'aria-labelledby' => 'ofd_store_'.$store->id,
                        'class' => 'tab-pane fade '.($key === 0 ? 'show active' : ''),
                        'role' => 'tabpanel',
                    ]) ?>
                        <?php
                        $kktArray = $store->kkts;
                        ?>
                        <table class="kkt_list_table table table-style table-count-list">
                            <thead>
                                <tr>
                                    <th scope="col">
                                        <?= Html::checkbox('selection_all', false, [
                                            'class' => 'select-on-check-all',
                                        ]) ?>
                                    </th>
                                    <th scope="col">
                                        Наименование
                                    </th>
                                    <th scope="col">
                                        Была в сети
                                    </th>
                                    <th scope="col">
                                        Активирована
                                    </th>
                                    <th scope="col">
                                        Статус
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($kktArray as $key => $kkt) : ?>
                                    <tr class="kkt_list_row" data-url="<?= Url::to(['kkt/view', 'id' => $kkt->id]) ?>">
                                        <td class="row_checkbox">
                                            <?= Html::checkbox('selection[]', false, [
                                                'class' => 'select-on-check-all',
                                                'value' => $kkt->id,
                                            ]) ?>
                                        </td>
                                        <td>
                                            <?= Html::encode($kkt->name) ?>
                                        </td>
                                        <td>
                                            --
                                        </td>
                                        <td>
                                            --
                                        </td>
                                        <td>
                                            --
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    <?= Html::endTag('div') ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="store_view_modal">
    <div class="modal-dialog">
        <div id="store_view_content" class="modal-content"></div>
    </div>
</div>

<div class="modal fade" id="kkt_view_modal">
    <div class="modal-dialog">
        <div id="kkt_view_content" class="modal-content"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("click", ".ofd_store_link.active", function (e) {
        $("#store_view_content").load($(this).data("url"));
        $("#store_view_modal").modal("show");
    });
    $(document).on("hidden.bs.modal", "#store_view_modal", function (e) {
        $("#store_view_content", this).html("");
    });

    $(document).on("click", ".kkt_list_row > td:not(.row_checkbox)", function (e) {
        $("#kkt_view_content").load($(this).closest(".kkt_list_row").data("url"));
        $("#kkt_view_modal").modal("show");
    });
    $(document).on("hidden.bs.modal", "#kkt_view_modal", function (e) {
        $("#kkt_view_content", this).html("");
    });
</script>