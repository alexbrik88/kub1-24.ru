<?php

use common\models\ofd\OfdStore;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\ofd\OfdStore */

$this->title = $model->name;
?>

<div class="modal-header h-0">
    <h5>Карточка Точки продаж</h5>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php Pjax::begin([
    'id' => 'store_view_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'options' => [
        'class' => 'modal-body mt-4',
    ],
]); ?>

    <?php $form = ActiveForm::begin([
        'action' => ['update', 'id' => $model->id],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-pjax' => 1,
        ],
    ]) ?>

        <?= $form->field($model, 'local_name')->textInput(); ?>

        <?= $form->field($model, 'store_type_id')->widget(Select2::class, [
            'data' => OfdStore::$typeList,
            'options' => [
                'class' => '',
                'placeholder' => '',
            ],
        ]); ?>

        <div class="d-flex justify-content-between mt-4">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-hover-transparent min-w-130',
            ]) ?>
            <?= Html::a('Отменить', ['view', 'id' => $model->id], [
                'class' => 'button-regular button-hover-transparent min-w-130 ml-auto',
            ]) ?>
        </div>
    <?php $form->end(); ?>

<?php Pjax::end(); ?>
