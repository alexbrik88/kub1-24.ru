<?php

use common\models\ofd\OfdStore;
use frontend\components\Icon;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\ofd\OfdStore */

$this->title = $model->name;
?>

<div class="modal-header h-0">
    <h5>Карточка Точки продаж</h5>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php Pjax::begin([
    'id' => 'store_view_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
    'options' => [
        'class' => 'modal-body mt-4',
    ],
]); ?>

    <div class="form-group">
        <label class="label">
            <?= $model->getAttributeLabel('local_name') ?>
            <?= Html::a(Icon::get('pencil'), [
                'update',
                'id' => $model->id,
            ], [
                'class' => 'ml-5',
            ]) ?>
        </label>
        <div style="padding: 13px 17px 11px;">
            <?= $model->local_name ?>
        </div>
    </div>
    <div class="form-group">
        <label class="label">
            <?= $model->getAttributeLabel('storeTypeLabel') ?>
        </label>
        <div style="padding: 13px 17px 11px;">
            <?= $model->storeTypeLabel ?>
        </div>
    </div>

    <div class="modal-footer">
        <?= Html::button('Ok', [
            'class' => 'button-regular button-hover-transparent min-w-130',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php Pjax::end(); ?>
