<?php

use common\models\ofd\Ofd;
use frontend\components\Icon;
use frontend\modules\ofd\components\OfdHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $employee common\models\employee\Employee */
/* @var $searchModel frontend\modules\retail\models\OfdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подключение ОФД для автоматической загрузки чеков';

$hasOfdImportConfig = $employee->company->ofdImportConfig !== null;
?>
<div class="ofd-provider-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'attribute' => 'logo',
                'label' => false,
                'enableSorting' => false,
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'py-0 px-1',
                ],
                'value' => function ($model) {
                    return Html::img('/img/ofd/' . $model->logo, [
                        'style' => 'height: 100px;',
                    ]);
                }
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
                'label' => 'ОФД',
            ],
            [
                'attribute' => 'is_active',
                'enableSorting' => false,
                'label' => 'Статус',
                'format' => 'raw',
                'value' => function (Ofd $model) use ($employee, $hasOfdImportConfig) {
                    if ($model->is_active) {
                        if ($employee->getOfdUser($model)->exists()) {
                            if ($hasOfdImportConfig) {
                                return Html::a('Загрузить чеки', [
                                    "/ofd/{$model->alias}/default/receipt",
                                ], [
                                    'class' => 'button-regular button-hover-transparent ofd_module_open_link',
                                    'style' => 'width: 130px;',
                                ]);
                            } else {
                                return Html::button(Icon::get('add-icon', ['class' => 'mr-2']).'Загрузить чеки', [
                                    'class' => 'button-regular button-hover-transparent ajax-modal-btn',
                                    'data-title' => 'Настройка загрузки данных из ОФД в раздел кассы',
                                    'data-url' => Url::to(['/retail/import-config/update']),
                                ]);
                            }
                        } else {

                            $buttonTitle = 'Подключить';
                            if ($integration = $model->getHelper($employee, $employee->company)->getOfdUser()) {
                                if ($integration->created_at) {
                                    $buttonTitle = 'Отключить';
                                }
                            }

                            return Html::a($buttonTitle, [
                                "/ofd/{$model->alias}/default/integration",
                            ], [
                                'class' => 'button-regular button-hover-transparent ofd_module_open_link',
                                'style' => 'width: 130px;',
                            ]);
                        }
                    } else {
                        return Html::button('Скоро', [
                            'class' => 'button-regular button-hover-transparent disabled',
                            'style' => 'width: 130px; background-color: #eee; color: #999;',
                            'disabled' => true,
                        ]);
                    }
                }
            ],
            [
                'label' => 'Дата подключения',
                'enableSorting' => false,
                'value' => function ($model) use ($employee) {
                    if ($model->is_active) {
                        if ($integration = $model->getHelper($employee, $employee->company)->getOfdUser()) {
                            return date('d.m.Y', $integration->created_at);
                        }
                    }

                    return '--';
                }
            ],
        ],
    ]); ?>


</div>
