<?php

use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\retail\models\OfdReportSearch */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
var checkIsChanged = function() {
    var changed = false;
    $("#retail-report-filter select").each(function() {
        if (this.value != $(this).data("value")) {
            changed = true;
        }
    });
    $("#retail-report-filter-submit")
        .toggleClass("button-hover-transparent", !changed)
        .toggleClass("button-regular_red", changed);
};
$(document).on("change", "#retail-report-filter select", function (e) {
    checkIsChanged();
});
$(document).on("click", "#retail-report-filter-reset", function (e) {
    $("#retail-report-filter select").each(function() {
        $(this).val("").trigger("change");
    });
});
$(document).on("change", "#ofdreportsearch-store_id", function (e) {
    $.pjax({
        url: $(this).data("filter-url"),
        container: "#kkt_id_pjax",
        data: $(this.form).serialize(),
        push: false,
        timeout: 10000,
        scrollTo: false,
        method: "post"
    });
});
JS;

$this->registerJs($js);
?>

<div class="retail-report-search">
    <?php $form = ActiveForm::begin([
        'id' => 'retail-report-filter',
        'action' => ['index'],
        'method' => 'get',
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'store_id')->widget(Select2::class, [
            'data' => $model->storeDropdownList(),
            'options' => [
                'class' => '',
                'data-filter-url' => Url::to('kkt-filter'),
                'data-value' => $model->store_id,
            ],
            'pluginOptions' => [
                'minimumResultsForSearch' => 10,
                'width' => '100%',
            ],
        ]) ?>
        </div>
        <div class="col-3">
            <?php Pjax::begin([
                'id' => 'kkt_id_pjax',
                'linkSelector' => false,
                'enablePushState' => false,
                'enableReplaceState' => false,
                'timeout' => 10000,
            ]); ?>

            <?= $form->field($model, 'kkt_id')->widget(Select2::class, [
                'data' => $model->kktDropdownList(),
                'options' => [
                    'class' => '',
                    'data-value' => $model->kkt_id,
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%',
                ],
            ]) ?>

            <?php Pjax::end(); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'operator_id')->widget(Select2::class, [
                'data' => $model->operatorDropdownList(),
                'options' => [
                    'class' => '',
                    'data-value' => $model->operator_id,
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%',
                ],
            ]) ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'return')->widget(Select2::class, [
                'data' => $model->returnDropdownList(),
                'options' => [
                    'class' => '',
                    'data-value' => $model->return,
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'payment_type_id')->widget(Select2::class, [
                'data' => $model->paymentTypeDropdownList(),
                'options' => [
                    'class' => '',
                    'data-value' => $model->payment_type_id,
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%',
                ],
            ]) ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'product_type')->widget(Select2::class, [
                'data' => $model->productTypeDropdownList(),
                'options' => [
                    'class' => '',
                    'data-value' => $model->product_type,
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%',
                ],
            ]) ?>
        </div>
        <div class="col-3">
            <label class="label">&nbsp;</label>
            <div>
                <?= Html::submitButton('Применить', [
                    'id' => 'retail-report-filter-submit',
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>
            </div>
        </div>
        <div class="col-3">
            <label class="label">&nbsp;</label>
            <div>
                <?= Html::button('Сбросить', [
                    'id' => 'retail-report-filter-reset',
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
