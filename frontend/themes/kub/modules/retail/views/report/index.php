<?php

use frontend\components\Icon;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\TextHelper;

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\retail\models\OfdReportSearch */
/* @var $dataProvider yii\data\SqlDataProvider */

$this->title = 'Отчет о розничных продажах';

$tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_finance_odds');
?>

<div class="wrap pt-0 pb-0 pl-4 pr-3 mb-3">
    <div class="p-2">
        <div class="row align-items-center">
            <div class="col-6 col-xl-9">
                <h4 class="mb-0"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="col-6 col-xl-3">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <h5>Фильтр</h5>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="table-settings row row_indents_s py-1">
    <div class="col-6">
        <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
            'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
            'title' => 'Скачать в Excel',
        ]); ?>
        <?= TableViewWidget::widget(['attribute' => 'table_view_finance_odds']) ?>
    </div>
    <div class="col-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'action' => Url::current(['name' => null]),
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
            <div class="form-group flex-grow-1 mr-2 pr-1">
                <?= Html::activeTextInput($searchModel, 'name', [
                    'type' => 'search',
                    'placeholder' => 'Название товара или услуги',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="ofd-receipt-index">
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table-retail-data table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'joint-main-checkbox-td text-left',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center joint-checkbox-td',
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    return Html::checkbox("reportRow[]", false, [
                        'class' => 'joint-checkbox',
                        'value' => null,
                        'data' => [
                            'count' => $data['count'],
                            'amount' => $data['amount'] / 100,
                            'average_price' => $data['average_price'],
                            'part' => $data['part'],
                        ],
                    ]);
                },
            ],
            'name',
            [
                'attribute' => 'count',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['class' => 'text-right nowrap'],
                'value' => function($data) {
                    return TextHelper::numberFormat($data['count'], 0);
                }
            ],
            [
                'attribute' => 'amount',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['class' => 'text-right nowrap'],
                'value' => function($data) {
                    return TextHelper::invoiceMoneyFormat($data['amount'], 2);
                }
            ],
            [
                'attribute' => 'average_price',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['class' => 'text-right nowrap'],
                'value' => function($data) {
                    return TextHelper::invoiceMoneyFormat($data['average_price'], 2);
                }
            ],
            [
                'attribute' => 'part',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['class' => 'text-right nowrap'],
                'value' => function($data) {
                    return TextHelper::numberFormat($data['part'], 2);
                }
            ],
        ],
    ]); ?>
</div>

<?= $this->render('_summary-select', []) ?>