<?php

use common\models\cash\CashFlowsBase;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $buttons array */
$buttons = isset($buttons) ? (array)$buttons : [];
?>
<div id="summary-container" class="wrap wrap_btns check-condition" style="min-height: 70px; padding-top: 18px">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-panel-checkbox" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column total-txt-foot mr-3">
            Кол-во: <strong class="total-quantity ml-1">0</strong>
        </div>
        <div class="column total-txt-foot mr-3">
            Сумма продаж: <strong class="total-amount ml-1">0</strong>
            <strong>₽</strong>
        </div>
        <div class="column total-txt-foot mr-3">
            Доля: <strong class="total-part ml-1">0</strong>
            <strong>%</strong>
        </div>

        <div class="column ml-auto"></div>

        <?php foreach (array_filter($buttons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>
    </div>
</div>

<?php
$this->registerJs(<<<SCRIPT

// fast joint operations checkboxes

$(document).on('change', '.joint-main-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');

    panelCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.last().trigger('change');
});

$(document).on('change', '.joint-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const mainCheckbox = table.find('.joint-main-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');
    const checkedCheckboxes = checkboxes.filter(':checked');
    const copyButton = $('#summary-container').find('#button-copy-flow-item');

    if (checkboxes.length === checkedCheckboxes.length) {
        mainCheckbox.prop('checked', true).uniform('refresh');
        panelCheckbox.prop('checked', true).uniform('refresh');
    } else {
        mainCheckbox.prop('checked', false).uniform('refresh');
        panelCheckbox.prop('checked', false).uniform('refresh');
    }

    // copy button

    if (checkedCheckboxes.length === 1 && $(checkedCheckboxes).data('copy-url')) {
        copyButton
            .attr('href', $(checkedCheckboxes).data('copy-url'))
            .addClass('ajax-modal-btn')
            .removeClass('hidden');
    } else {
        copyButton
            .attr('href', 'javascript:void(0)')
            .removeClass('ajax-modal-btn')
            .addClass('hidden');
    }
});

$(document).on('change', '.joint-panel-checkbox', function() {
    const table = $('.table-retail-data').first();
    const mainCheckbox = table.find('.joint-main-checkbox');

    mainCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh').trigger('change');
});

$(document).on('change', '.joint-checkbox', function(e) {
    let countChecked = 0;
    let amountSum = 0;
    let quantitySum = 0;
    let partSum = 0;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        amountSum += parseFloat($(this).data('amount'));
        quantitySum += parseFloat($(this).data('count'));
        partSum += parseFloat($(this).data('part'));
    });
    
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-amount').text(number_format(amountSum, 2, ',', ' '));
        $('#summary-container .total-quantity').text(number_format(quantitySum, 2, ',', ' '));
        $('#summary-container .total-part').text(number_format(partSum, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

SCRIPT);
?>