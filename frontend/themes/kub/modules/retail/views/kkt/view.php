<?php

use frontend\components\Icon;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ofd\OfdStore */

$this->title = $model->name;
?>

<div class="modal-header h-0">
    <h5><?= Html::encode($this->title) ?></h5>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<div class="modal-body">
    <h6 class="mt-4">
        Техническая информация о кассе
    </h6>

    <?= DetailView::widget([
        'model' => $model,
        'options' => [
            'class' => 'table detail-view mt-4',
        ],
        'attributes' => [
            'registration_number',
            'format_fd',
            'model',
            'factory_number',
            'registration_status',
            'activated_at',
            'address',
        ],
    ]) ?>
</div>

<div class="modal-footer">
    <?= Html::button('Ok', [
        'class' => 'button-regular button-hover-transparent min-w-130',
        'data-dismiss' => 'modal',
    ]) ?>
</div>
