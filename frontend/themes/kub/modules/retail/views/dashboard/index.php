<?php

use frontend\components\Icon;
use frontend\modules\analytics\models\OddsSearch;
use frontend\widgets\RangeButtonWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\bootstrap4\Dropdown;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $storeArray array
 */

$this->title = 'Розничные продажи';
$refreshUrl = Url::current(['store' => null]);
?>

<?= $this->render('partial/_chart_group', [
    'searchModel' => $searchModel,
]) ?>

<div class="wrap wrap_count">
    <div class="row align-items-center">
        <div class="column col-xxx mr-auto">
            <h4 class="mb-0">Продажи за сегодня</h4>
        </div>
        <div class="column col-xxx">
            <?= $this->render('partial/_select_store', [
                'list' => ['' => 'Все точки'] + $storeArray,
                'selected' => $searchModel->store,
                'refreshUrl' => $refreshUrl,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="kub-chart border">
                <?= $this->render('partial/chart_11_amount_by_hour', [
                    'id' => 'chart_11',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-4">
            <div class="kub-chart border">
                <?= $this->render('partial/chart_12_amount_by_day', [
                    'id' => 'chart_12',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-4">
            <div class="kub-chart border">
                <?= $this->render('partial/chart_13_average_by_day', [
                    'id' => 'chart_13',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_count">
    <div class="row align-items-center">
        <div class="column col-xxx mr-auto">
            <h4 class="mb-0">ТОП-10</h4>
        </div>
        <div class="column col-xxx">
            <?= frontend\widgets\RangeButtonWidget::widget(); ?>
        </div>
        <div class="column col-xxx">
            <?= $this->render('partial/_select_store', [
                'list' => ['' => 'Все точки'] + $storeArray,
                'selected' => $searchModel->store,
                'refreshUrl' => $refreshUrl,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="kub-chart">
                <?= $this->render('partial/chart_21_top_store', [
                    'id' => 'chart_21',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-4">
            <div class="kub-chart">
                <?= $this->render('partial/chart_22_top_product_sum', [
                    'id' => 'chart_22',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
        <div class="col-4">
            <div class="kub-chart">
                <?= $this->render('partial/chart_23_top_product_quantity', [
                    'id' => 'chart_23',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_count">
    <div class="row">
        <div class="col-12">
            <div style="position: absolute; right:6px;">
                <?= $this->render('partial/_select_store', [
                    'list' => ['' => 'Все точки'] + $storeArray,
                    'selected' => $searchModel->store,
                    'refreshUrl' => $refreshUrl
                ]) ?>
            </div>
            <div class="kub-chart">
                <?= $this->render('partial/chart_31_employee_rating', [
                    'id' => 'chart_31',
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
</div>