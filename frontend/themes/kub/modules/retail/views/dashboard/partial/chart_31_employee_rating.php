<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\modules\retail\models\RetailEmployeeRating;
use frontend\modules\retail\models\OfdDashboardChartModel;

/* @var \yii\web\View $this */
/* @var OfdDashboardChartModel $searchModel */

if (empty($rating)) {
    $rating = new RetailEmployeeRating;
    if (isset($searchModel))
        $rating->store = $searchModel->store;
}
$data = $rating->ratingData;
$prefDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'F Y',
    'monthInflected' => false,
    'date' => $rating->prevDate->format('Y-m-d')]);
$urlDate = $rating->date->format('Y-m-01');
$dateItems = [];
foreach ($rating->dateItems as $key => $value) {
    $dateItems[] = [
        'label' => $value,
        'url' => ['/retail/dashboard/employee-rating', 'date' => $key, 'type' => $rating->type, 'store' => $rating->store],
        'linkOptions' => [
            'class' => $urlDate == $key ? 'active' : '',
        ],
    ];
}

$colors = [
    RetailEmployeeRating::TYPE_AMOUNT => [1 => '#68c5d2', 2 => '#2d86ac'],
    RetailEmployeeRating::TYPE_COUNT => [1 => '#68c5d2', 2 => '#2d86ac'],
];
$color1 = $colors[$rating->getType()][1];
$color2 = $colors[$rating->getType()][2];

$labelDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'F Y',
    'monthInflected' => false,
    'date' => $rating->date->format('Y-m-d')]);
?>

<?php Pjax::begin([
    'id' => 'employee-rating-pjax',
    'enableReplaceState' => false,
    'enablePushState' => false,
    'timeout' => 10000,
]); ?>

    <div class="ht-caption" style="padding-top:13px;">
        ЛУЧШИЕ СОТРУДНИКИ ЗА
        <div style="display: inline-block; width: 200px;">
            <div class="dropdown">
                <?= Html::tag('div', $labelDate, [
                    'class' => 'dropdown-toggle link',
                    'data-toggle' => 'dropdown',
                    'style' => 'display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;',
                    'aria-expanded' => 'false',
                ])?>
                <?= \yii\bootstrap4\Dropdown::widget([
                    'id' => 'employee-rating-dropdown',
                    'class' => 'form-filter-list list-clr',
                    'items' => array_reverse($dateItems),
                ])?>
            </div>
        </div>
    </div>
    <div class="tools arrow-tools">
        <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
    </div>

<div class="portlet-body portlet-body-checkall">
    <div class="nav-tabs-row">
    <?php
    echo Nav::widget([
        'id' => 'employee-rating-menu',
        'encodeLabels' => false,
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100'],
        'items' => [
            [
                'label' => 'ПРОДАЖИ ₽',
                'url' => ['/retail/dashboard/employee-rating', 'date' => $urlDate, 'type' => 'amount', 'store' => $rating->store],
                'active' => $rating->type == 'amount',
                'options' => [
                    'class' => 'nav-item',
                    'style' => 'padding-left: 0'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ( $rating->type == 'amount' ? ' active' : ''),
                ]
            ],
            [
                'label' => 'ПРОДАЖИ (шт.)',
                'url' => ['/retail/dashboard/employee-rating', 'date' => $urlDate, 'type' => 'count', 'store' => $rating->store],
                'active' => $rating->type == 'count',
                'options' => [
                    'class' => 'nav-item',
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ( $rating->type == 'count' ? ' active' : '')
                ]
            ],
        ],
    ]);
    ?>
    </div>

    <?php if (count($data['current']) > 0) : ?>
        <div class="row">
            <div class="col-8 pr-0" style="display: inline-block; margin-bottom: 10px;">
                <table  class="employee-rating-detail ht-in-table" style="width: 100%; line-height: 18px;">
                    <thead>
                        <tr>
                            <th width="80%"> </th>
                            <th width="10%"> </th>
                            <th width="10%"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $top = $data['current'] ?>
                        <?php foreach ($top as $key => $val1) : ?>
                            <?php
                            $width1 = $val1 * 100 / ($data['max'] ? : 1);
                            $val2 = isset($data['previous'][$key]) ? $data['previous'][$key] : 0;
                            $width2 = $val2 * 100 / ($data['max'] ? : 1);
                            $percentage = $val2 > 0 ? round(($val1 - $val2) * 100 / $val2, 2) : null;
                            ?>
                            <tr style="vertical-align: bottom;">
                                <td>
                                    <div>
                                        <?= $rating->getFio($key) ?>
                                    </div>
                                    <div style="position: relative; min-width: 150px;">
                                        <?= Html::tag('div', '', [
                                            'class' => 'employee-rating-bar bar1',
                                            'style' => "background-color: {$color1};" .
                                                       ($width1 > 0 ? 'min-width: 1px;' : ''),
                                            'data-width' => "{$width1}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td nowrap style="padding: 0 8px; font-weight: bold; text-align: right;">
                                    <?= $rating->format($val1) ?>
                                </td>
                                <td nowrap style="padding: 0 8px; font-weight: bold;">
                                    <?php if ($percentage !== null) : ?>
                                        <?= '(' . ($percentage >= 0 ? '+' : '') . $percentage . '&nbsp;%)'; ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; line-height: 12px;">
                                <td style="padding: 0; padding-right: 8px;">
                                    <div style="position: relative; min-width: 150px;">
                                        <?= Html::tag('div', '', [
                                            'class' => 'employee-rating-bar bar2',
                                            'style' => "background-color: {$color2};" .
                                                       ($width2 > 0 ? 'min-width: 1px;' : ''),
                                            'data-width' => "{$width2}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td nowrap style="padding: 0 8px; text-align: right;">
                                    <?= $rating->format($val2) ?>
                                </td>
                                <td nowrap style="padding: 0 8px;">
                                    <?= $prefDate ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php
            $currTotal = array_sum($data['current']);
            $prevTotal = array_sum($data['previous']);
            $maxTotal = max($currTotal, $prevTotal);
            $percTotal = $prevTotal > 0 ? round(($currTotal - $prevTotal) * 100 / $prevTotal, 2) : null;
            if ($percTotal > 1000) $percTotal = round($percTotal);
            $tooltip = $percTotal !== null ? Html::tag('div', ($percTotal >= 0 ? '+' : '') . $percTotal . '&nbsp;%', [
                'class' => 'total-month-tooltip',
            ]) : '';
            $height1 = $prevTotal * 100 / ($maxTotal ? : 1);
            $height2 = $currTotal * 100 / ($maxTotal ? : 1);
            ?>
            <div class="col-4 pl-0 employee-rating-total-wrap" style="display: inline-block;">
                <table class="employee-rating-total ht-in-table ml-auto">
                    <thead>
                        <tr>
                            <th width="60px"> </th>
                            <th width="60px"> </th>
                            <th > </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3" class="employee-rating-total-title text-left pl-2" style="padding-bottom: 1.125rem">
                                По всем сотрудникам
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #666;">
                                <div class="total-month-wrap">
                                    <?= Html::tag('div', '', [
                                        'class' => 'total-month-value',
                                        'style' => "background-color: {$color2};" .
                                                   ($height1 > 0 ? 'min-width: 1px;' : ''),
                                        'data-height' => "{$height1}%",
                                    ]) ?>
                                </div>
                            </td>
                            <td style="border-bottom: 1px solid #666;">
                                <div class="total-month-wrap">
                                    <?= Html::tag('div', $tooltip, [
                                        'class' => 'total-month-value',
                                        'style' => "background-color: {$color1};" .
                                                   ($height2 > 0 ? 'min-height: 1px;' : ''),
                                        'data-height' => "{$height2}%",
                                    ]) ?>
                                </div>
                            </td>
                            <td style="vertical-align: middle; padding-left: 25px;">
                                <table style="width: auto;">
                                    <tbody>
                                        <tr>
                                            <td style="padding-right: 5px;">
                                                <div class="total-month-mark" style="background-color: <?=$color1?>;"></div>
                                            </td>
                                            <td style="text-align: right; font-weight: bold;">
                                                <?= $rating->format($currTotal) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-right: 5px;">
                                                <div class="total-month-mark" style="background-color: <?=$color2?>"></div>
                                            </td>
                                            <td style="text-align: right;">
                                                <?= $rating->format($prevTotal) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                    'format' => 'F Y',
                                    'monthInflected' => false,
                                    'date' => $rating->prevDate->format('Y-m-d')]);; ?>
                            </td>
                            <td style="text-align: center;">
                                <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                    'format' => 'F Y',
                                    'monthInflected' => false,
                                    'date' => $rating->date->format('Y-m-d')]);; ?>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.employee-rating-bar').each(function() {
                    $(this).animate({'width': $(this).data('width')});
                });
                $('.total-month-value').each(function() {
                    $(this).animate({'height': $(this).data('height')});
                });
            });
        </script>
    <?php else : ?>
        <div style="padding: 11px 0">
            Нет данных
        </div>
    <?php endif ?>
</div>

<?php Pjax::end() ?>

<script>
    $(document).on('pjax:success', '#employee-rating-pjax', function() {
        // refresh dropdown
        $('#employee-rating-dropdown').closest('.dropdown').find('.dropdown-toggle').click();
    });
</script>
