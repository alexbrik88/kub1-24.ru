<style>
    /* Charts height */
    #chart_11, #chart_12, #chart_13 { height: 210px; }
    #chart_21, #chart_22, #chart_23 { min-height: 210px; }

    #chart_21, #chart_21 .highcharts-container , #chart_21 svg {
        overflow: visible!important;
        z-index: 5!important;
    }
    #chart_22, #chart_22 .highcharts-container , #chart_22 svg {
        overflow: visible!important;
        z-index: 4!important;
    }
    #chart_23, #chart_23 .highcharts-container , #chart_23 svg {
        overflow: visible!important;
        z-index: 3!important;
    }
    #chart_21  .highcharts-axis-labels,
    #chart_22  .highcharts-axis-labels,
    #chart_23  .highcharts-axis-labels {
        z-index: -1!important;
    }
</style>

<script>
    window.ChartGroup = {
        store: <?= $searchModel->store ?: "null" ?>,
        _inProcess: false,
        chart_11: {
            data: {},
            labelsX: [],
            currDayPos: null,
            offset: 0
        },
        chart_12: {
            data: {},
            labelsX: [],
            currDayPos: null,
            offset: 0
        },
        chart_13: {
            data: {},
            labelsX: [],
            currDayPos: null,
            offset: 0
        },
        chart_21: {},
        chart_22: {},
        chart_23: {},
        bindEvents: function()
        {
            // change offset
            $(document).on("click", ".dashboard-chart-offset", function() {

                if (ChartGroup._inProcess) {
                    return false;
                }

                const chartId = $(this).data('chart');
                const syncChartId = $(this).data('sync-chart');
                const offset = $(this).data('offset');

                if (typeof ChartGroup[chartId] !== 'undefined') {
                    ChartGroup[chartId].offset += offset;
                    ChartGroup.refresh(chartId);
                    if (syncChartId && typeof ChartGroup[syncChartId] !== 'undefined') {
                        ChartGroup[syncChartId].offset += offset;
                        ChartGroup.refresh(syncChartId);
                    }
                }
            });
        },
        refresh: function(chartId) {
            ChartGroup._getData(chartId).done(function() {
                ChartGroup._repaint(chartId);
                ChartGroup._inProcess = false;
            });
        },
        _repaint: function(chartId)
        {
            const chart = ChartGroup[chartId];
            if (typeof chart !== 'undefined') {
                $('#' + chartId).highcharts().update(chart.data);
            } else {
                console.log('Chart ' + chartId + ' not found!');
            }
        },
        _getData: function(chartId)
        {
            ChartGroup._inProcess = true;

            return $.post('/retail/dashboard/get-data-ajax?chart=' + chartId, {
                    "chart-ajax": true,
                    "store":  ChartGroup.store || null,
                    "offset": ChartGroup[chartId].offset || 0,
                },
                function(data) {
                    $('#chart-transmitter').append(data).html(null);
                }
            );
        }
    };

    $(document).ready(function () {
        ////////////////////////
        ChartGroup.bindEvents();
        ////////////////////////
        Highcharts.SVGRenderer.prototype.symbols['c-rect']
            = function (x, y, w, h) { return ['M', x, y + h / 2, 'L', x + w, y + h / 2]; };
    });

</script>

<div id="chart-transmitter">
    <!-- js updated data -->
</div>