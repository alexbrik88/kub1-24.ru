<?php

use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use miloschuman\highcharts\Highcharts;
use frontend\modules\retail\models\OfdDashboardChartModel;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\helpers\Html;

/** @var OfdDashboardChartModel $searchModel */

$jsModel = DC::getJsModel($id);

$color = 'rgba(46,159,191,1)';
$color2 = 'rgba(51,90,130,1)';
$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$currWeekPeriods = ArrayHelper::getValue($xAxis, 'datePeriods');
$prevWeekPeriods = ArrayHelper::getValue(DC::getXAxisDays($leftOffset, $rightOffset, $customOffset - 7), 'datePeriods');

$y = $searchModel->getSalesByDays($currWeekPeriods, $prevWeekPeriods); ?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.data = <?= json_encode([
        'series' => [
            ['data' => $y['curr']],
            ['data' => $y['prev']],
        ],
        'xAxis' => [
            'categories' => $xAxis['x']
        ]
    ]) ?>;
</script>

<?php if (isset($isAjax)) { exit; } ?>

<?= $this->render('_arrows', ['chartId' => $id, 'offsetStep' => 1, 'syncChart' => 'chart_13']) ?>

<?= Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column'
            ],
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> по дням'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'data' => $y['curr'],
                ],
                [
                    'type' => 'scatter',
                    'name' => 'Неделя до',
                    'color' => $color2,
                    'data' => $y['prev'],
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ],
            'plotOptions' => [
                'scatter' => [
                    'marker' => [
                        'symbol' => 'c-rect',
                        'lineWidth' => 3,
                        'lineColor' => $color2,
                        'radius' => 8.5
                    ],
                ]
            ],
        ]
    ], DC::CHART_COLUMN)
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>