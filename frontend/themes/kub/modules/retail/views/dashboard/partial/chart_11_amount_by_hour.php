<?php

use frontend\components\Icon;
use miloschuman\highcharts\Highcharts;
use frontend\modules\retail\models\OfdDashboardChartModel;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\helpers\Html;

/** @var OfdDashboardChartModel $searchModel */

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisHours($leftOffset, $rightOffset, $customOffset);
$y = $searchModel->getSalesToday($xAxis['datePeriods']);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.data = <?= json_encode([
        'series' => [
            'data' => $y,
        ],
        'xAxis' => [
            'categories' => $xAxis['x']
        ]
    ]) ?>;
</script>

<?php if (isset($isAjax)) { exit; } ?>

<?= $this->render('_arrows', ['chartId' => $id, 'offsetStep' => 1]) ?>

<?= Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>ПРОДАЖИ</b> за сегодня'
            ],
            'series' => [
                [
                    'name' => 'Сумма',
                    'color' => 'rgba(54,195,176,1)',
                    'data' => $y,
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterHours($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ])
); ?>

