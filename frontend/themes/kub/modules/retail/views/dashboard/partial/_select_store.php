<?php
use kartik\select2\Select2;

/** @var $list */
/** @var $selected */
/** @var $refreshUrl */
?>
<div style="max-height: 44px">
    <?= Select2::widget([
        'name' => 'ofd-point-select',
        'data' => $list,
        'pluginOptions' => [
            'width' => '200px'
        ],
        'value' => $selected,
        'options' => [
            'onchange' => new \yii\web\JsExpression("location.href = '{$refreshUrl}?store=' + this.value")
        ]
    ]) ?>
</div>