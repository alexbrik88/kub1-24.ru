<?php
use miloschuman\highcharts\Highcharts;
use frontend\modules\retail\models\OfdDashboardChartModel;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;

/** @var OfdDashboardChartModel $searchModel */

$jsModel = DC::getJsModel($id);

/////////////// consts //////////////////
$color = 'rgba(132,197,201,1)';
$maxRowsCount = 10;
/////////////////////////////////////////

$topData = $searchModel->getTopProductData($maxRowsCount, 'quantity');
$totalQuantity = $searchModel->getTotalByPeriod($searchModel::TOTAL_QUANTITY);
$categories = &$topData['categories'];
$data = &$topData['quantity'];
$productIds = &$topData['id'];

if (empty($categories)) {
    $categories[] = 'Нет данных';
    $revenue[] = 0;
} ?>

<script>
    <?php /*($jsModel)?>.topData = <?= $jsTopData; ?>; */ ?>
</script>

<?= Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true,
                'height' => DC::calcChartColumnHeight(count($categories), 24),
                'spacing' => [0,0,0,0],
                'marginTop' => '28',
                'marginRight' => '50',
                'events' => [
                    'load' => DC::getEvents($id, ['alignRightColumnDataLabels'], ['offset' => 50]),
                    'redraw' => DC::getEvents($id, ['alignRightColumnDataLabels'], ['offset' => 50]),
                ],
            ],
            'title' => [
                'text' => '<b>ТОВАРЫ</b> по кол-ву продаж',
            ],
            'series' => [
                [
                    'name' => 'Количество',
                    'data' => $data,
                    'color' => $color,
                    'states' => DC::getSerieState($color),
                    'pointPadding' => 0.14,
                    'showInLegend' => false,
                    'dataLabels' => [
                        'enabled' => true,
                        'position' => 'left',
                        'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 0, ',', ' '); }"),
                        'style' => [
                            'fontSize' => '14px',
                            'fontWeight' => '400',
                            'color' => '#001424',
                            'fontFamily' => '"Corpid E3 SCd", sans-serif'
                        ]
                    ],
                ],
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'title' => '',
                'labels' => false,
            ],
            'xAxis' => [
                'categories' => $categories,
                'labels' => [
                    //'useHTML' => true,
                    'align' => 'left',
                    'reserveSpace' => true,
                    'style' => [
                        'fontSize' => '14px',
                    ],
                    'formatter' => new \yii\web\JsExpression('function() { return (this.value.length > 12) ? this.value.substr(0, 10) + "..." : this.value; }')
                ],
                'gridLineWidth' => 0,
                'minorGridLineWidth' => 0,
                'lineWidth' => 0,
                'offset' => 0,
            ],
            'tooltip' => [
                'shape' => 'rect',
                'formatter' => DC::getTooltipWithTitle(DC::TOOLTIP_NO_UNITS + ['total' => $totalQuantity]),
                'positioner' => DC::getInvertedChartTooltipPositioner()
            ]
        ]
    ], DC::CHART_COLUMN)
);
?>