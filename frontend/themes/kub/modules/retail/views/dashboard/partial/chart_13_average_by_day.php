<?php

use frontend\components\Icon;
use miloschuman\highcharts\Highcharts;
use frontend\modules\retail\models\OfdDashboardChartModel;
use frontend\modules\analytics\models\dashboardChart\DashboardChart as DC;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var OfdDashboardChartModel $searchModel */

$jsModel = DC::getJsModel($id);

$leftOffset = 6;
$rightOffset = 0;
$customOffset = $customOffset ?? 0;

$xAxis = DC::getXAxisDays($leftOffset, $rightOffset, $customOffset);
$currWeekPeriods = ArrayHelper::getValue($xAxis, 'datePeriods');
$prevWeekPeriods = ArrayHelper::getValue(DC::getXAxisDays($leftOffset, $rightOffset, $customOffset - 7), 'datePeriods');

$y = $searchModel->getAverageCheckByDays($currWeekPeriods, $prevWeekPeriods); ?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.data = <?= json_encode([
        'series' => [
            ['data' => $y['curr']],
            ['data' => $y['prev']],
        ],
        'xAxis' => [
            'categories' => $xAxis['x']
        ]
    ]) ?>;
</script>

<?php if (isset($isAjax)) { exit; } ?>

<?= $this->render('_arrows', ['chartId' => $id, 'offsetStep' => 1, 'syncChart' => 'chart_12']) ?>

<?= Highcharts::widget(
    DC::getOptions([
        'id' => $id,
        'options' => [
            'title' => [
                'text' => '<b>Средний чек</b> по дням'
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'color' => 'rgba(243,183,46,1)',
                    'data' => $y['curr'],
                ],
                [
                    'name' => 'Неделя до',
                    'color' => 'rgba(51,90,130,1)',
                    'data' => $y['prev'],
                    'dashStyle' => 'ShortDash',
                    'marker' => [
                        'enabled' => false
                    ]
                ],
            ],
            'xAxis' => [
                'categories' => $xAxis['x'],
                'labels' => [
                    'formatter' => DC::getXAxisFormatterDays($jsModel)
                ]
            ],
            'tooltip' => [
                'formatter' => DC::getTooltipDays($jsModel)
            ]
        ]
    ])
);
?>

<script>
    <?=($jsModel)?>.currDayPos = <?= $xAxis['currDayPos']; ?>;
    <?=($jsModel)?>.labelsX = <?= json_encode($xAxis['labelsX']) ?>;
    <?=($jsModel)?>.freeDays = <?= json_encode($xAxis['freeDays']) ?>;
</script>