<?php

use common\models\Company;
use frontend\components\Icon;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\rbac\permissions;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;
use yii\helpers\Url;


$this->beginContent('@frontend/views/layouts/main.php');

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;

$employee = Yii::$app->user->identity;
$company = $employee->company;

$ofdUserArray = $company->getOfdUsers()->groupBy('ofd_id')->all();

$p = Banking::currentRouteEncode();

if (count($ofdUserArray) === 1) {
    $uploadUrl = [
        "/ofd/{$ofdUserArray[0]->ofd->alias}/default/receipt",
        'p' => $p,
    ];
} else {
    $uploadUrl = [
        '/ofd/default/select',
        'p' => $p,
    ];
}
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <div class="d-flex">
        <h4 class="mt-1 mr-auto">
            Розничные продажи
        </h4>
        <?= Html::button(Icon::get('cog'), [
            'class' => 'button-regular button-regular_red ajax-modal-btn w-44 ml-3',
            'title' => 'Настройка загрузки данных из ОФД в раздел кассы',
            'data-title' => 'Настройка загрузки данных из ОФД в раздел кассы',
            'data-url' => Url::to(['/retail/import-config/update']),
        ]) ?>
        <?php if ($company->ofdImportConfig) : ?>
            <?= Html::a(Icon::get('add-icon', ['class' => 'mr-2']).'Загрузить чеки', $uploadUrl, [
                'class' => 'button-regular button-regular_red ofd_module_open_link ml-3',
            ]) ?>
        <?php else : ?>
            <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']).'Загрузить чеки', [
                'class' => 'button-regular button-regular_red ajax-modal-btn ml-3',
                'data-title' => 'Настройка загрузки данных из ОФД в раздел кассы',
                'data-url' => Url::to(['/retail/import-config/update']),
            ]) ?>
        <?php endif ?>
    </div>
    <?= Nav::widget([
        'id' => 'ofd-menu',
        'items' => [
            [
                'label' => 'Дашборд',
                'url' => ['/retail/dashboard/index'],
            ],
            [
                'label' => 'Продажи',
                'url' => ['/retail/default/index'],
            ],
            [
                'label' => 'Отчеты',
                'url' => ['/retail/report/index'],
            ],
            [
                'label' => 'Чеки',
                'url' => ['/retail/receipt/index'],
            ],
            [
                'label' => 'Точки продаж',
                'url' => ['/retail/store/index'],
            ],
            [
                'label' => 'ОФД',
                'url' => ['/retail/ofd/index'],
            ],
        ],
        'options' => [
            'class' => 'nav-tabs nav-tabs_border_bottom_grey w-100 mr-3 mb-2',
        ],
    ]) ?>

    <?= $content; ?>

    <?= frontend\modules\ofd\widgets\OfdModalWidget::widget() ?>
</div>
<?php $this->endContent(); ?>
