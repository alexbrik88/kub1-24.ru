<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Dropdown;

/** $this yii\web\View */
/** $year integer */
/** $items array */

?>

<style type="text/css">
    #retail_period_list {
        width: 100px !important;
        min-width: 100px !important;
    }
</style>
<div class="dropdown">
    <?= Html::button('<span>'.$year.'</span><span class="wropdown_arrow"></span>', [
        'class' => 'button-regular button-hover-transparent dropdown-toggle text-left',
        'style' => 'min-width: 100px;
                    padding-left: 23px;
                    font-weight: 400;
                    font-size: 16px;',
        'data-toggle' => 'dropdown',
    ]) ?>
    <?= Dropdown::widget([
        'id' => 'retail_period_list',
        'items' => $items,
        'options' => [],
    ]) ?>
</div>
