<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */

use frontend\modules\reports\models\ExpensesSearch;
use frontend\widgets\RangeButtonWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Выгрузка в 1С';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stop-zone-for-fixed-elems">
    <div class="wrap pt-2 pb-1 pl-4 pr-3">
        <div class="pt-2 pb-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="col-9 mr-auto">
                    <div class="row">
                        <div class="column" style="margin-top:-2px;">
                            <h4 class="mb-2">
                                <?= $this->title ?>
                            </h4>
                        </div>
                        <div class="column" style="margin-top:-7px;">
                            <button class="button-regular button-hover-transparent button-width" data-toggle="toggleVisible" data-target="invoice">
                                Инструкция
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-3 pl-1 pr-0" style="margin-top:-9px">
                    <?= RangeButtonWidget::widget(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap mt-2">
        <div class="row">
            <?= $this->render('_viewPartials/_form', [
                'exportModel' => $exportModel,
            ]); ?>
        </div>
    </div>

    <div class="wrap wrap_count mt-2">
        <div class="col-12">
            <div class="mt-1 mb-2"><strong>История выгрузок</strong></div>

            <?php Pjax::begin([
                'id' => 'export-list',
            ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php
echo $this->render('_viewPartials/export-help-panel')
?>