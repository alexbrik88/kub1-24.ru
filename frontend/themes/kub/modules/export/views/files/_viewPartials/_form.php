<?php
use frontend\models\Documents;
use frontend\modules\export\models\export\ExportFiles;
use yii\helpers\Html;
use yii\helpers\Url;

$documentOutList = array_intersect_key(ExportFiles::$documentTypes, array_flip(ExportFiles::$documentOutKeys));
$documentInList = array_intersect_key(ExportFiles::$documentTypes, array_flip(ExportFiles::$documentInKeys));

$form = \yii\widgets\ActiveForm::begin([
    'id' => 'one-s-export-form',
    'action' => ['create'],
    'method' => 'post',
//    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
//    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => '',
        ],
    ],
    'options' => [
        'data' => [
            'progress-url' => Url::to(['/export/progress/files']),
        ],
    ],
]); ?>
    <div class="col-12">
        <div class="form-group">
            <div class="label mb-3 bold">Параметры выгрузки</div>
            <?= $form->field($exportFilesModel, 'only_new')->checkbox(); ?>
        </div>
        <div class="form-group">
            <div class="label mb-3 bold">Документы для выгрузки</div>
            <?= $form->field($exportFilesModel, 'io_type_out')->checkbox(['id' => 'export-type-out']); ?>
            <div class="pb-2 pt-2" style="padding-left: 27px">
                <?= $form->field($exportFilesModel, 'outItems')->label(false)->checkboxList($documentOutList, ['class' => 'export-type-out']); ?>
            </div>
            <?= $form->field($exportFilesModel, 'io_type_in')->checkbox(['id' => 'export-type-in']); ?>
            <div class="pb-2 pt-2" style="padding-left: 27px">
                <?= $form->field($exportFilesModel, 'inItems')->label(false)->checkboxList($documentInList, ['class' => 'export-type-in']); ?>
            </div>
            <?= $form->field($exportFilesModel, 'io_type_specific')->checkbox(['id' => 'export-type-specific']); ?>

            <?= $form->field($exportFilesModel, 'period_start_date')->label(false)->hiddenInput(); ?>
            <?= $form->field($exportFilesModel, 'period_end_date')->label(false)->hiddenInput(); ?>
            <?= Html::hiddenInput('user_id', \Yii::$app->user->id, ['id' => 'user_id']); ?>

            <div class="load_button mt-3 mb-3">
                <?= Html::submitButton('Сформировать архив', ['class' => 'button-regular button-hover-content-red button-clr']); ?>
                <div class="progress-status mt-3 files" style="display: none;">
                    <label style="font-size: 14px"><span class="export-progress-action"
                                 data-in-progress="Идет формирование архива... "
                                 data-completed="Архив сформирован "></span><span
                            class="progress-value"></span></label>

                    <div
                        class="progress progress-striped active export-progressbar">
                        <div class="progress-bar" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width:0%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-left contragent-label text-grey">
            Если у вас возникли трудности при выгрузке документов,
            обращайтесь в службу
            поддержки: <?= Html::a(Yii::$app->params['emailList']['support'], 'mailto:' . Yii::$app->params['emailList']['support']); ?>
        </div>

    </div>
<?php $form->end(); ?>