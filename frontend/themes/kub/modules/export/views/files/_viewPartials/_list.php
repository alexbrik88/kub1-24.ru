<?php
use common\components\grid\DropDownDataColumn;
use common\models\employee\Employee;
use frontend\modules\export\models\export\exportFiles;

?>
<?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],
            //'options' => [
            //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            //],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата выгрузки',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'format' => 'date',
                ],
                [
                    'attribute' => 'user_id',
                    'label' => 'Ответственный',
                    'filter' => $searchModel->getEmployeeByCompanyArray(\Yii::$app->user->identity->company->id),
                    'format' => 'raw',
                    'value' => function (ExportFiles $data) {
                        return $data->employee instanceof Employee ? $data->employee->getFio(true) : '-';
                    },
                    's2width' => '200px'
                ],
                [
                    'attribute' => 'period_start_date',
                    'label' => 'Период выгрузки',
                    'format' => 'raw',
                    'value' => function (ExportFiles $data) {
                        return $data->getPeriodStartDate() . ' - ' . $data->getPeriodEndDate();
                    },
                ],
                [
                    'attribute' => 'only_new',
                    'label' => 'Только новые',
                    'format' => 'boolean',
                ],
                [
                    'attribute' => 'io_type_out',
                    'label' => 'Исходящие документы',
                    'format' => 'raw',
                    'value' => function (ExportFiles $data) {
                        if ($data->io_type_out == true) {
                            $out = 'Все';
                        } elseif ($data->io_type_out_items) {
                            $list = array_intersect_key(ExportFiles::$documentTypes, array_flip($data->outItems));
                            $out = implode('<br />', $list);
                        } else {
                            $out = '-';
                        }

                        return $out;
                    }
                ],
                [
                    'attribute' => 'io_type_in',
                    'label' => 'Входящие документы',
                    'format' => 'raw',
                    'value' => function (ExportFiles $data) {
                        if ($data->io_type_in == true) {
                            $out = 'Все';
                        } elseif ($data->io_type_in_items) {
                            $list = array_intersect_key(ExportFiles::$documentTypes, array_flip($data->inItems));
                            $out = implode('<br />', $list);
                        } else {
                            $out = '-';
                        }

                        return $out;
                    }
                ],
                [
                    'attribute' => 'io_type_specific',
                    'label' => 'Мои документы',
                    'format' => 'boolean',
                ],
            ],
        ]); ?>
