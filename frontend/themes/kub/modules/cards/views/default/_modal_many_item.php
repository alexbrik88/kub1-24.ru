<?php

use common\components\helpers\Html;
use frontend\modules\cards\models\OperationSelectForm;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var $model OperationSelectForm */
?>

<!-- Modal Many Item -->
<div id="changeItemDialog" class="modal fade modal-changeItemDialog" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => null,
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'income_item_id', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-income_item_id_wrapper',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'id' => 'select_income_item_id',
                                'prompt' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья прихода'); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <?= Html::radio(null, true, [
                                        'label' => 'Расход изменить на:',
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'expenditure_item_id', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'select_expenditure_item_id',
                                'class' => 'form-group js-expenditure_item_id_wrapper required',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья расхода'); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'data-style' => 'expand-right',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#changeItemDialog", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income:checked").length > 0;
        var $modal = $(this);
        var $header = $modal.find(".modal-header h1");
        var $additionalHeaderText = null;
    
        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        if ($includeExpenditureItem && $includeIncomeItem) {
            $additionalHeaderText = " прихода / расхода";
        } else if ($includeExpenditureItem) {
            $additionalHeaderText = " расхода";
        } else if ($includeIncomeItem) {
            $additionalHeaderText = " прихода";
        }
        $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
        $(".joint-operation-checkbox:checked").each(function() {
            $modal.find("form").prepend($(this).clone().hide());
        });
    });
    
    $(document).on("hidden.bs.modal", "#changeItemDialog", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
        $(".additional-header-text").remove();
        $(".modal#changeItemDialog .joint-operation-checkbox").remove();
    });
');