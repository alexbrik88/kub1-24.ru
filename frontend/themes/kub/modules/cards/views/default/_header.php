<?php

namespace frontend\modules\cards\views;

use common\modules\cards\models\CardAccount;
use common\modules\import\models\ImportJobData;
use frontend\modules\cards\widgets\ZenmoneyButtonWidget;
use frontend\themes\kub\widgets\CardAccountWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\themes\kub\widgets\QuickImportWidget;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var CardAccount|null $account
 * @var ImportJobData $lastJobData
 */

?>

<div class="stop-zone-for-fixed-elems cash-bank-flows-index">
    <div class="page-head d-flex flex-wrap align-items-center">
        <?= CardAccountWidget::widget() ?>
        <?php if ($account): ?>
            <?= QuickImportWidget::widget([
                'importJobData' => $lastJobData,
                'action' => Url::to([
                    '/cards/zenmoney/quick-import',// TODO:
                    'identifier' => $account->identifier,
                    'redirectUrl' => Url::current(),
                ]),
                'statusUrl' => Url::to([
                    '/cards/zenmoney/job-status', // TODO:
                    'identifier' => $account->identifier,
                ]),
                'logoSrc' => '/img/zenmoney/logo.png', // TODO:
            ]) ?>
        <?php endif; ?>
        <?php if ($account && $account->account_type == CardAccount::ACCOUNT_TYPE_ZENMONEY) :?>
            <?= ZenmoneyButtonWidget::widget([
                'cssClass' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                'content' => $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Импорт операций</span>',
                'identifier' => $account->identifier,
                'hasImport' => true,
                'hasBlock' => true,
            ]) ?>
        <?php else: ?>
            <?= ZenmoneyButtonWidget::widget([
                'cssClass' => 'button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
                'content' => $this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Добавить аккаунт</span>',
                'hasImport' => false,
                'hasBlock' => false,
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<?= ImportDialogWidget::widget() ?>
