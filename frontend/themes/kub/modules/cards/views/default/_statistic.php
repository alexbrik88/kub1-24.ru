<?php

namespace frontend\modules\cards\views;

use common\components\date\DateHelper;
use common\modules\cards\models\CardOperationRepository;
use frontend\modules\cash\widgets\Statistic;
use frontend\widgets\RangeButtonWidget;
use yii\web\View;

/**
 * @var View $this
 * @var CardOperationRepository $repository
 */

$summary = $repository->getSummary();

?>

<div class="wrap wrap_count">
    <div class="row">
        <?= Statistic::widget([
            'items' => [
                [
                    'class' => 'count-card_yellow',
                    'text' => 'Баланс на начало',
                    'amount' => $summary->getStartBalance(),
                    'statistic_text' => $repository->getDateFrom()->format(DateHelper::FORMAT_USER_DATE),
                    'statistic_text_class' => '_yellow_statistic',
                ],
                [
                    'class' => 'count-card_turquoise',
                    'text' => 'Приход',
                    'amount' => $summary->getTotalIncome(),
                    'statistic_text' => 'Всего поступлений: ' . $summary->getCountIncome(),
                    'statistic_text_class' => '_green_statistic',
                ],
                [
                    'class' => 'count-card_red',
                    'text' => 'Расход',
                    'amount' => $summary->getTotalExpense(),
                    'statistic_text' => 'Всего списаний: ' . $summary->getCountExpense(),
                    'statistic_text_class' => '_red_statistic',
                ],
                [
                    'class' => 'count-card_yellow',
                    'text' => 'Баланс на конец',
                    'amount' => $summary->getEndBalance(),
                    'statistic_text' =>  $repository->getDateTo()->format(DateHelper::FORMAT_USER_DATE),
                    'statistic_text_class' => '_yellow_statistic',
                ],
            ],
        ]) ?>

        <div class="count-card-column col-6 d-flex flex-column">
            <?= RangeButtonWidget::widget(); ?>
        </div>
    </div>
</div>
