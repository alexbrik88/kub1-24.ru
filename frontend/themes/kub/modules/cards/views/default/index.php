<?php

namespace frontend\modules\cards\views;

use common\models\employee\Employee;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardBillList;
use common\modules\cards\models\CardOperationRepository;
use common\modules\cards\models\ContractorList;
use common\modules\cards\models\ProjectList;
use common\modules\cards\models\ReasonList;
use common\modules\import\models\ImportJobData;
use frontend\modules\cards\models\OperationSelectForm;use yii\web\View;

/**
 * @var View $this
 * @var OperationSelectForm $form
 * @var Employee $employee
 * @var CardOperationRepository $repository
 * @var CardBillList $billList
 * @var ContractorList $contractorList
 * @var ReasonList $reasonList
 * @var ProjectList $projectList
 * @var CardAccount|null $account
 * @var ImportJobData $lastJobData
 */

$this->title = 'Карты';

?>

<?= $this->render('_header', [
    'account' => $account,
    'lastJobData' => $lastJobData,
]) ?>

<?= $this->render('_statistic', [
    'repository' => $repository,
]) ?>

<?= $this->render('_filter', [
    'repository' => $repository,
    'billList' => $billList,
    'contractorList' => $contractorList,
    'reasonList' => $reasonList,
]) ?>

<?= $this->render('_grid', [
    'form' => $form,
    'employee' => $employee,
    'repository' => $repository,
    'billList' => $billList,
    'contractorList' => $contractorList,
    'reasonList' => $reasonList,
    'projectList' => $projectList
]) ?>
