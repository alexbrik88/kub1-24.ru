<?php

namespace frontend\modules\cards\views;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\employee\Employee;
use common\models\project\Project;
use common\modules\cards\models\CardBillList;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\CardOperationRepository;
use common\modules\cards\models\ContractorList;
use common\modules\cards\models\ProjectList;
use common\modules\cards\models\ReasonList;
use frontend\modules\cards\models\OperationSelectForm;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\models\CashSearch;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ContentDecorator;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;

/**
 * @var View $this
 * @var OperationSelectForm $form
 * @var Employee $employee
 * @var CardOperationRepository $repository
 * @var CardBillList $billList
 * @var ContractorList $contractorList
 * @var ReasonList $reasonList
 * @var ProjectList $projectList
 */

$dataProvider = $repository->getDataProvider();
$dataProvider->pagination->pageSize = \frontend\components\PageSize::get();
$tableViewClass = $employee->config->getTableViewClass('card_operation_table');
$userConfig = $employee->config;
$tabConfigClass = [
    'incomeExpense' => 'col_card_column_income_expense' . ($userConfig->card_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_card_column_income_expense'  . (!$userConfig->card_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_card_column_income_expense' . (!$userConfig->card_column_income_expense ? '' : ' hidden'),
    'project' => 'col_card_column_project' . ($userConfig->card_column_project ? '' : ' hidden'),
    'salePoint' => 'col_card_column_sale_point' . ($userConfig->card_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_card_column_industry' . ($userConfig->card_column_industry ? '' : ' hidden'),
];
$emptyMessage = 'В указанном периоде нет операций.';

?>

<?php ContentDecorator::begin([
    'viewFile' => __DIR__ . '/_select.php',
    'params' => [
        'form' => $form,
    ],
]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $repository,
    'emptyText' => $emptyMessage,
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $tableViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => Html::getInputName($form, 'id'), // TODO:
            'headerOptions' => [
                'style' => 'width: 20px;',
            ],
            'checkboxOptions' => function (CardOperation $operation) {
                return [
                    'class' => 'joint-operation-checkbox ' . ($operation->flow_type == CardOperation::FLOW_TYPE_INCOME ? 'income' : 'expense'),
                    'value' => $operation['id'],
                    'data-income' => (float) $operation->getIncomeAmount(),
                    'data-expense' => (float) $operation->getExpenseAmount(),
                ];
            },
        ],
        [
            'label' => 'Дата',
            'attribute' => 'date',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'contentOptions' => [
                'class' => 'nowrap',
            ],
        ],
        [
            'attribute' => 'amount_income_expense',
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => $tabConfigClass['incomeExpense'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
            ],
            'format' => 'raw',
            'value' => function (CardOperation $operation) {

                $flowsAdapter = [
                    'id' => $operation->id,
                    'tb' => CardOperation::tableName(),
                    'amount' => $operation->amount,
                    'amountIncome' => $operation->getAmountIncome(),
                    'amountExpense' => $operation->getAmountExpense(),
                    'flow_type' => $operation->flow_type,
                    'is_internal_transfer' => null,
                    'transfer_key' => null,
                    'plan' => null
                ];

                $income = '+ ' . TextHelper::invoiceMoneyFormat(($flowsAdapter['amountIncome'] > 0) ? $flowsAdapter['amount'] : 0, 2);
                $expense = '- ' . TextHelper::invoiceMoneyFormat(($flowsAdapter['amountExpense'] > 0) ? $flowsAdapter['amount'] : 0, 2);
                $updateLink = Html::a(($flowsAdapter['amountIncome'] > 0) ? $income : $expense, '#', [
                    'class' => 'import-dialog link link-bleak',
                    'data-url' => CashSearch::getUpdateFlowLink($income, $flowsAdapter, false)
                ]);

                if ($flowsAdapter['amountIncome'] > 0) {
                    return Html::tag('div', $updateLink, ['class' => 'green-link']);
                }
                if ($flowsAdapter['amountExpense'] > 0) {
                    return Html::tag('div', $updateLink, ['class' => 'red-link']);
                }

                return '';
            },
        ],        
        [
            'label' => 'Приход',
            'attribute' => 'amount_income',
            'headerOptions' => [
                'class' => $tabConfigClass['income'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
            ],
            'value' => function (CardOperation $operation): string {
                if ($operation->flow_type == CardOperation::FLOW_TYPE_INCOME) {
                    return $operation->getAmountText();
                }

                return '-';
            },
        ],
        [
            'label' => 'Расход',
            'attribute' => 'amount_expense',
            'headerOptions' => [
                'class' => $tabConfigClass['expense'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
            ],
            'value' => function (CardOperation $operation): string {
                if ($operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE) {
                    return $operation->getAmountText();
                }

                return '-';
            },
        ],
        [
            'label' => 'Счёт',
            'attribute' => 'bill_name',
            'value' => 'bill.fullName',
            'filter' => ['' => 'Все'] + $billList->getItems(),
            's2width' => '200px',
            'hideSearch' => false,
        ],
        [
            'label' => 'Контрагент',
            'attribute' => 'contractor_id',
            'filter' => ['' => 'Все'] + $contractorList->getItems(),
            's2width' => '200px',
            'hideSearch' => false,
            'value' => function (CardOperation $operation): string {
                if ($operation->contractor) {
                    return $operation->contractor->name;
                } elseif ($operation->contractor_id == CashContractorType::BALANCE_TEXT) {
                    return 'Баланс начальный';
                }

                return '';
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'project_id',
            'label' => 'Проект',
            'headerOptions' => [
                'class' => $tabConfigClass['project'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
            ],
            'filter' => ['' => 'Все проекты'] + $projectList->getItems(),
            'format' => 'raw',
            'value' => function ($flows) {
                if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                    return $project->name;
                }
                return '';
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'sale_point_id',
            'label' => 'Точка продаж',
            'headerOptions' => [
                'class' => $tabConfigClass['salePoint'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
            ],
            //'filter' => ['' => 'Все точки продаж'],
            'format' => 'raw',
            'value' => function ($flows) {
                if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                    return \yii\helpers\Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                }
                return '';
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'industry_id',
            'label' => 'Направление',
            'headerOptions' => [
                'class' => $tabConfigClass['companyIndustry'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
            ],
            //'filter' => ['' => 'Все направления'],
            'format' => 'raw',
            'value' => function ($flows) {
                if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                    return Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                }
                return '';
            },
        ],
        [
            'label' => 'Назначение',
            'attribute' => 'description',
        ],
        [
            'label' => 'Статья',
            'attribute' => 'reason',
            'value' => 'reasonItem.reasonName',
            'filter' => ['' => 'Все'] + $reasonList->getItems(),
            's2width' => '200px',
            'hideSearch' => false,
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}{delete}',
            'contentOptions' => ['class' => 'text-nowrap'],
            'headerOptions' => ['class' => 'action-column', 'style' => 'width: 1%'],
            'buttons' => [
                'update' => function (string $url, CardOperation $operation): string {
                    return Html::a(SpriteIconWidget::widget(['icon' => 'pencil']), '#', [
                        'class' => 'button-clr link mr-2 import-dialog',
                        'title' => 'Изменить',
                        'data-url' => Url::to([
                            '/cards/operation/update',
                            'id' => $operation->id,
                            'redirectUrl' => Url::current(),
                        ]),
                    ]);
                },
                'delete' => function (string $url, CardOperation $operation): string {
                    return Html::a(SpriteIconWidget::widget(['icon' => 'garbage']), '#', [
                        'class' => 'button-clr link import-dialog',
                        'title' => 'Удалить',
                        'data-url' => Url::to([
                            '/cards/operation/delete',
                            'id' => $operation->id,
                            'redirectUrl' => Url::current(),
                        ]),
                    ]);
                },
            ],
        ],
    ],
]) ?>

<?php ContentDecorator::end(); ?>
