<?php

namespace frontend\modules\crm\views;

use common\modules\cards\models\CardBillList;
use common\modules\cards\models\CardOperationRepository;
use common\modules\cards\models\ContractorList;
use common\modules\cards\models\FlowTypeList;
use common\modules\cards\models\ReasonList;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var CardOperationRepository $repository
 * @var CardBillList $billList
 * @var ContractorList $contractorList
 * @var ReasonList $reasonList
 */

$itemsSelected = strlen(implode('', [
    $repository->contractor_id,
    $repository->flow_type,
    $repository->reason,
    $repository->bill_name,
])) ? 'itemsSelected' : '';

?>

<?php $widget = ActiveForm::begin([
    'id' => 'operationFilterForm',
    'method' => 'GET',
    'action' => Url::current([$repository->formName() => null]),
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6 mb-3'],
    ],
]) ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableConfigWidget::widget([
                    'items' => [
                        [
                            'attribute' => 'card_column_income_expense',
                            'invert_attribute' => 'invert_card_column_income_expense'
                        ],
                        [
                            'attribute' => 'card_column_project'
                        ],
                        [
                            'attribute' => 'card_column_sale_point'
                        ],
                        [
                            'attribute' => 'card_column_industry'
                        ],
                    ],
                ]); ?>                
                <?= TableViewWidget::widget(['attribute' => 'card_operation_table']) ?>
                <div class="dropdown popup-dropdown popup-dropdown_filter <?= $itemsSelected ?>">
                    <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="button-txt">Фильтр</span>
                        <svg class="svg-icon svg-icon-shevron">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>
                    <div class="dropdown-menu keep-open" aria-labelledby="filter">
                        <div class="popup-dropdown-in p-3">
                            <div class="p-1">
                                <div class="row">

<?= $widget->field($repository, 'flow_type')->widget(Select2::class, [
    'data' => ['' => 'Все'] + (new FlowTypeList)->getItems(),
    'pluginOptions' => ['width' => '100%'],
    'hideSearch' => true,
])->label('Приход/Расход') ?>

<?= $widget->field($repository, 'reason')->widget(Select2::class, [
    'data' => ['' => 'Все'] + $reasonList->getItems(),
    'pluginOptions' => ['width' => '100%'],
    'hideSearch' => true,
])->label('Статья') ?>

<?= $widget->field($repository, 'contractor_id')->widget(Select2::class, [
    'data' => ['' => 'Все'] + $contractorList->getItems(),
    'pluginOptions' => ['width' => '100%'],
    'hideSearch' => true,
])->label('Контрагенты') ?>

<?= $widget->field($repository, 'bill_name')->widget(Select2::class, [
    'data' => ['' => 'Все'] + $billList->getItems(),
    'pluginOptions' => ['width' => '100%'],
    'hideSearch' => true,
])->label('Счёт') ?>

                                    <div class="col-12 pt-4">
                                        <div class="row justify-content-between">
                                            <div class="form-group column">
                                                <?= Html::button('Применить', [
                                                    'class' => 'button-regular button-hover-content-red button-width-medium button-clr',
                                                    'type' => 'submit',
                                                ]) ?>
                                            </div>
                                            <div class="form-group column">
                                                <?= Html::button('Сбросить', [
                                                    'class' => 'button-regular button-hover-content-red button-width-medium button-clr',
                                                    'type' => 'reset',
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="d-flex flex-nowrap align-items-center">
            <?= $widget->field($repository, 'search', [
                'options' => ['class' => 'form-group flex-grow-1 mr-2'],
            ])->textInput([
                'placeholder' => 'Поиск по контрагенту...',
                'type' => 'search',
            ])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>

<?php $this->registerJs(<<<JS
    $(document).ready(function () {
        $(document).on('reset', '#operationFilterForm', function () {
            var select = $(this).find('select');

            select.val('');
            select.trigger('change');
    
            return false;
        });
    });
JS) ?>
