<?php

namespace frontend\modules\analytics\views\credits;

use common\models\project\ProjectSearch;
use frontend\themes\kub\helpers\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use Yii;
use common\models\project\Project;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\FlowTypeList;
use frontend\modules\cards\models\OperationUpdateForm;
use frontend\modules\cash\models\CashContractorType;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $this
 * @var OperationUpdateForm $form
 */

$flowTypes = (new FlowTypeList)->getItems();

if ($form->operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE) {
    unset($flowTypes[CardOperation::FLOW_TYPE_INCOME]);
}

if ($form->operation->flow_type == CardOperation::FLOW_TYPE_INCOME) {
    unset($flowTypes[CardOperation::FLOW_TYPE_EXPENSE]);
}

$hasProject = Yii::$app->user->identity->menuItem->project_item;

$datePickerTemplate = <<<HTML
<div class="date-picker-wrap">
{input}
<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>
</div>
HTML;

?>
<h4 class="modal-title">Изменить операцию по карте</h4>
<?php $widget = ActiveForm::begin([
    'options' => [
        'id' => 'operationUpdateForm',
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'options' => ['class' => 'form-group col-6'],
        'labelOptions' => ['class' => 'label'],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => ['class' => 'label'],
        ],
    ],
]); ?>

<div class="row">
    <?= $widget->field($form, 'flow_type', [
        'inline' => true,
        'labelOptions' => [
            'style' => 'margin-top:27px'
        ],
        'radioOptions' => [
            'class' => '',
            'labelOptions' => ['class' => 'radio-label radio-txt-bold'],
        ],
    ])->radioList($flowTypes)->label(' ') ?>

    <?= $widget->field($form, 'account_id')->widget(Select2::class, [
        'data' => $form->getAccountList()->getItems(),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'contractor_id', [
        'options' => ['class' => 'form-group col-6 cash-contractor_input'],
    ])->widget(ContractorDropdown::class, [
        'company' => $form->operation->company,
        'contractorType' => $form->getContractorType(),
        'staticData' => [CashContractorType::BALANCE_TEXT => 'Баланс начальный'],
        'options' => [
            'id' => 'contractor_id',
            'class' => 'contractor-items-depend form-group ' . ($form->flow_type ? 'customer' : 'seller'), // TODO:
            'placeholder' => '',
            'data' => [
                'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
            ],
        ],
    ]) ?>

<?php if($form->operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE): ?>
    <?= $widget->field($form, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
        'loadAssets' => false,
        'income' => false,
        'options' => [
            'id' => 'expenditure_item_id',
            'class' => 'flow-expense-items',
            'prompt' => '--',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]); ?>
<?php endif; ?>

<?php if($form->operation->flow_type == CardOperation::FLOW_TYPE_INCOME): ?>
    <?= $widget->field($form, 'income_item_id')->widget(ExpenditureDropdownWidget::class, [
        'loadAssets' => false,
        'income' => true,
        'options' => [
            'id' => 'income_item_id',
            'class' => 'flow-income-items',
            'prompt' => '--',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]); ?>
<?php endif; ?>

    <?= $widget->field($form, 'amount')->textInput() ?>

    <div class="form-group col-6"></div>

    <?= $widget->field($form, 'date', [
        'options' => ['class' => 'form-group col-3'],
        'inputTemplate' => $datePickerTemplate,
        'enableClientValidation' => false,
    ])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
        'onchange' => new JsExpression("$('#operationupdateform-recognition_date').val($('#operationupdateform-date').val())")
    ])->label('Дата оплаты') ?>

    <?= $widget->field($form, 'time', [
        'options' => ['class' => 'form-group col-3'],
    ])->widget(MaskedInput::class, [
        'mask' => '9{2}:9{2}:9{2}',
    ]) ?>

    <?= $widget->field($form, 'recognition_date', [
        'options' => ['class' => 'form-group col-3'],
        'inputTemplate' => $datePickerTemplate,
        'enableClientValidation' => false,
    ])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
        'disabled' => true
    ])->label('Дата признания операции') ?>

    <div class="form-group col-3"></div>

    <?= $widget->field($form, 'description', ['options' => ['class' => 'form-group col-12']])->textarea() ?>

    <?php if ($hasProject): ?>
        <?= $widget->field($form, 'project_id')
            ->widget(Select2::class, [
                'data' => ProjectSearch::getSelect2Data($form->project_id),
                'options' => [
                    'placeholder' => 'Без проекта'
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ])
            ->label('Проект' . \yii\helpers\Html::tag('span', Icon::QUESTION, ['class' => 'tooltip-modal-help', 'data-tooltip-content' => '#tooltip_modal_help_project'])); ?>
    <?php endif; ?>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php if($form->operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE): ?>
    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'expenditure_item_id',
        'type' => 'expenditure',
    ]) ?>
<?php endif; ?>

<?php if($form->operation->flow_type == CardOperation::FLOW_TYPE_INCOME): ?>
    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'income_item_id',
        'type' => 'income',
    ]) ?>
<?php endif; ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-modal-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);
?>

<div style="display: none">
    <div id="tooltip_modal_help_project">
        Если у вас есть проекты, то для учета денежных<br/>операций по проектам, выберите проект.
    </div>
</div>

