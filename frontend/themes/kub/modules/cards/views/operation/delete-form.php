<?php

namespace frontend\modules\cards\views;

use frontend\modules\cards\models\OperationDeleteForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var OperationDeleteForm $form
 */

?>

<?= Html::beginForm('', 'post', [
    'data-pjax' => true,
]); ?>
<h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить операцию?</h4>
<div class="form-group text-center mb-0">
    <?= Html::hiddenInput(Html::getInputName($form, 'delete'), 1) ?>

    <?= Html::submitButton('Да', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>
<?= Html::endForm() ?>
