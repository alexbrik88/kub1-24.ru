<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\cards\models\CardBill */

$this->title = 'Изменить Карту: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Карты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
