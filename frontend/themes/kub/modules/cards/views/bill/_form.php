<?php

use common\models\currency\Currency;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use common\modules\cards\models\CardBillForm;

/* @var $this yii\web\View */
/* @var $model CardBillForm */

$currency = Currency::find()->select(['name', 'id'])->indexBy('id')->column();
$employeeArray = $model->account->company->getEmployeeCompanies()
    ->andWhere(['is_working' => true])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all();
$accessibleList = ['all' => 'Доступна пользователям с ролью Руководитель, Учредитель, ФинДиректор'] + ArrayHelper::map($employeeArray, 'employee_id', 'fio');
$templateDateInput = "{label}<div class='date-picker-wrap' style='width: 130px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/img/svg/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

$helpIsAccounting = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Если операции по этой карте отображаются в бухгалтерии, поставьте тут галочку. Это поможет вам сверяться с бух. отчетностью',
    'title-as-html' => 1
]);
$helpStartBalance = Icon::get('question', [
    'class' => 'label-help',
    'title' => 'Если у вас в карте есть сумма на момент заведения ее в КУБ24,<br/> то укажите начальный остаток',
    'title-as-html' => 1
]);

$canCreateStartBalance = true;
?>

<?php if (Yii::$app->request->isAjax && !empty($model->saved)) : ?>
    <?= Html::script('
        if ($("#card-bill-pjax-container").length > 0) {
            $.pjax.reload("#card-bill-pjax-container", {timeout: 5000});
            $(".modal.show").modal("hide");
        }
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>


    <?php $form = ActiveForm::begin([
        'id' => 'card-form',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
            'checkOptions' => [
                'class' => '',
                'labelOptions' => [
                    'class' => 'label'
                ],
            ],
        ],
    ]); ?>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'name')
                        ->textInput(['disabled' => true])
                    ->label('Название, которое будет отображаться') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
                    'data' => $currency,
                    'options' => [
                        'data-currency' => $currency,
                        'class' => 'select-accountant-currency_id',
                        'disabled' => true
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                        'minimumResultsForSearch' => 10,
                    ]
                ]); ?>
            </div>
        </div>

        <?= $form->field($model, 'accessible')->widget(Select2::class, [
            'data' => $accessibleList,
            'options' => [
                'placeholder' => '',
                'disabled' => true
            ],
            'pluginOptions' => [
                'width' => '100%'
            ]
        ])->label('Доступна') ?>

        <div class="row">
            <?php if ($canCreateStartBalance) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'createStartBalance')->checkbox([
                        'class' => 'account_start_balance_checkbox',
                        'labelOptions' => [
                            'class' => 'label mb-0',
                        ],
                    ], true)
                        ->label('Указать начальный остаток' . $helpStartBalance) ?>
                </div>
            <?php endif ?>
            <div class="col-6">
                <?= $form->field($model, 'is_accounting')->checkbox([
                    'labelOptions' => [
                        'class' => 'label mb-0',
                    ],
                ], true)
                    ->label('Для учета в бухгалтерии' . $helpIsAccounting) ?>
            </div>
        </div>

        <?php if ($canCreateStartBalance) : ?>
            <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : null; ?>">
                <div class="col">
                    <?= $form->field($model, 'startBalanceAmount')->textInput([
                        'class' => 'form-control js_input_to_money_format',
                    ]) ?>
                </div>
                <div class="col">
                    <?= $form->field($model, 'startBalanceDate', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                    ])->label('на дату') ?>
                </div>
            </div>
        <?php endif ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-regular_red button-width button-clr',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
                'data-dismiss' => 'modal'
            ]); ?>
        </div>

    <?php ActiveForm::end(); ?>


<?= Html::script('
$("#card-form input:checkbox:not(.md-check)").uniform();

refreshDatepicker();

', [
    'type' => 'text/javascript',
]); ?>
