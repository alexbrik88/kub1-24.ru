<?php

namespace frontend\modules\cards\views;

use yii\web\View;

/**
 * @var View $this
 */

?>
<h4 class="modal-title">Импорт операций из Дзен мани</h4>
<div style="margin: 0 0 10px;">
    <div class="cont-img_bank-logo" style="float: left;">
        <img src="/img/zenmoney/integration.png" style="width: 66px;">
    </div>
    <div id="statement-bank-info" style="margin-left: 80px;">
        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee;">
            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
            - надежный протокол для передачи конфиденциальной банковской информации
            и соблудаются требования международного стандарта PCI DSS по хранению и передаче
            конфиденциальной информации в банковской сфере.
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<hr>
