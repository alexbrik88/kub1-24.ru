<?php
$downloadForAndroid = 'https://play.google.com/store/apps/details?id=ru.zenmoney.androidsub';
$downloadForIOS = 'https://apps.apple.com/ru/app/zenmoney/id905934786';
?>

<button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreZenmoneyDetails" aria-expanded="false" aria-controls="moreZenmoneyDetails">
    <span class="link-txt">Инструкция по автозагрузке операций по картам физ.лица</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
    </svg>
</button>
<div class="collapse" id="moreZenmoneyDetails">
    <div class="font-14 mt-2">
        <br/>
        <div class="mb-1">
            <strong>ДзенМани инструкция подключения</strong>
        </div>
        <div class="mb-1">
            Интеграция позволяет автоматически получать список операций прихода и расхода по картам физ.лица в сервисе КУБ24.<br/>
            При совершении покупки для вашей компании и ее оплаты картой физ.лица, ДзенМани автоматически передаст данную операцию в КУБ24, чтобы все расходы по компании были учтены.
        </div>
        <br/>
        <div class="mb-1">
            <strong>КАК ПОДКЛЮЧИТЬ ИНТЕГРАЦИЮ?</strong>
        </div>
        <div class="mb-1">
            Подключить интеграцию нужно в 2 этапа:
        </div>

        <br/>
        <!-- STAGE 1 -->
        <div class="mb-1">
            <strong>Этап 1. Подключение Банка к ДзенМани</strong>
        </div>
        <div class="mb-1 ml-2">
            1. Скачайте приложение на свой телефон
            <div class="ml-2">
                a. Для Android <a href="<?= $downloadForAndroid ?>" target="_blank">Скачать</a>
            </div>
            <div class="ml-2">
                b. Для iOS <a href="<?= $downloadForIOS ?>" target="_blank">Скачать</a>
            </div>
        </div>
        <div class="mb-1 ml-2">
            2. Зарегистрируйтесь в ДзенМани
        </div>
        <div class="mb-1 ml-2">
            3. В разделе Счета, выберите свой банк
            <br/><img style="width:257px" class="pt-2 pb-2" src="/images/zenmoney/help1.png"/><br/>
        </div>
        <div class="mb-1 ml-2">
            4. И пройдите авторизацию в банке
            <br/><img style="width:258px" class="pt-2 pb-2" src="/images/zenmoney/help2.png"/><br/>
            <strong>Важно!</strong> Сотрудникам ДзенМани недоступны данные подключения. Они шифруются и хранятся на вашем телефоне. ДзенМани не может переводить деньги с вашего счета.
        </div>

        <br/>
        <!-- STAGE 2 -->
        <div class="mb-1">
            <strong>Этап 2. Подключение ДзенМани к КУБ24</strong>
        </div>
        <div class="mb-1 ml-2">
            1. Войдите в КУБ24. Перейдите в левом меню в раздел «Деньги» -> «Карты»
            <br/><img style="width:249px" class="pt-2 pb-2" src="/images/zenmoney/help3.png"/><br/>
        </div>
        <div class="mb-1 ml-2">
            2. Нажмите по кнопке «Добавить Аккаунт»
            <div class="ml-2">
                a. Далее в появившемся окне нажмите по кнопке «Подтвердить интеграцию»
            </div>
            <div class="ml-2">
                b. Войдите в свой аккунт ДзенМани, где уже подключен банк
                <br/><img style="width:360px" class="pt-2 pb-2" src="/images/zenmoney/help4.png"/><br/>
            </div>
        </div>
        <div class="mb-1 ml-2">
            3. После авторизации, ДзенМани вернет вас обратно в КУБ24. Где вы сможете запросить список операций и настроить автозагрузку операций по расписанию
            <br/><img style="width:100%" class="pt-2 pb-2" src="/images/zenmoney/help5.png"/><br/>
        </div>

        <br/>
        <div class="mb-1">
            <strong>Нужна помощь по подключению?</strong>
        </div>
        <div class="mb-1">
            Пишите в техническую поддержу, в чат или на почту support@kub-24.ru
        </div>
    </div>
</div>