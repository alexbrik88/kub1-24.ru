<?php

use common\models\cash\CashBankStatementUpload;
use frontend\themes\kub\assets\ManualFlowsAsset;
use frontend\components\Icon;
use frontend\components\StatisticPeriod;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model common\models\cash\CashFlowsBase */
/** @var $canUpdate boolean */
/** @var $foreign boolean */

?>

<?php ManualFlowsAsset::register($this); ?>

<?php Modal::begin([
    'id' => 'manual_flows_modal',
    'title' => 'Ручные операции',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>

    <div class="content_step_1">
        <div class="mb-3">
            Данная функция отфильтрует операции по банку, добавленные вручную.
            <br>
            Если вы загружаете выписку из банка,
            то операции добавленные вручную могут дублировать операции загруженные из банка.
            Используйте данную функцию и удалите операции добавленные вручную.
            <div class="no_manual_flows hidden mt-2 text-danger">
                Операции, добавленные вручную, не найдены за период
                «<?= StatisticPeriod::getSessionName() ?>»
            </div>
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Применить', [
                'class' => 'button-regular button-width button-regular_red has_manual_flows',
                'data-check-url' => Url::to([
                    'has-manual',
                ]),
                'data-index-url' => Url::to([
                    'index',
                    'rs' => 'all',
                    $model->formName() => [
                        'source' => CashBankStatementUpload::SOURCE_MANUAL,
                    ],
                ]),
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-width button-hover-transparent',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <div class="content_step_2 hidden">
    </div>

<?php Modal::end() ?>
