<?php

use frontend\themes\kub\assets\LinkFlowToInvoiceAsset;
use frontend\components\Icon;
use frontend\components\StatisticPeriod;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model common\models\cash\CashFlowsBase */
/** @var $canUpdate boolean */
/** @var $foreign boolean */

?>

<?php if ($canUpdate) : ?>

<?php LinkFlowToInvoiceAsset::register($this); ?>

<?php Modal::begin([
    'id' => 'link_invoices_modal',
    'title' => 'Привязать платежи по выписке к счетам',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]) ?>

    <div class="content_step_1">
        <div class="mb-3">
            Если вы сначала загрузили банковскую выписку, и только потом добавили счета в КУБ24,
            то данная функция привяжет платежи по выписке к неоплаченным счетам.
            <br>
            Каким способом вы добавляли счета в КУБ24 не важно – вручную или подгрузили их из 1С.
            <br>
            Платежи за период, указанный в блоке периода –
            «<?= StatisticPeriod::getSessionName() ?>»
            , будут привязаны к счетам.
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Применить', [
                'class' => 'button-regular button-width button-regular_red link_invoices_find',
                'data-url' => Url::to(['link-invoices', 'foreign' => $foreign]),
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-width button-hover-transparent',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <div class="content_step_2 hidden">
    </div>

<?php Modal::end() ?>

<?php endif ?>
