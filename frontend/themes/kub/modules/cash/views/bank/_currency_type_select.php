<?php

use common\models\cash\CashBankForeignCurrencyFlows;
use yii\helpers\Html;
use yii\helpers\Url;

$isForeign = $model instanceof CashBankForeignCurrencyFlows;
?>

<style type="text/css">
    .currency_type_link:hover {
        color: #001424;
        text-decoration: none;
    }
</style>
<div class="form-group">
    <label>&nbsp;</label>
    <div class="form-filter">
        <?php if ($model->isNewRecord || !$isForeign) : ?>
            <?= Html::a(Html::radio(null, !$isForeign).' В рублях', Url::current([
                'foreign' => null,
            ]), [
                'class' => 'currency_type_link radio-txt-bold mr-4',
            ]) ?>
        <?php endif ?>
        <?php if ($model->isNewRecord || $isForeign) : ?>
            <?= Html::a(Html::radio(null, $isForeign).' В валюте', Url::current([
                'foreign' => 1,
            ]), [
                'class' => 'currency_type_link radio-txt-bold mr-auto',
            ]) ?>
        <?php endif ?>
    </div>
</div>