<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\cash\CashBankReasonType;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\project\ProjectSearch;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use \frontend\themes\kub\helpers\Icon;
use common\models\company\CompanyIndustry;

CashFormAsset::register($this);

if ($model->flow_type === null) {
    $model->flow_type = CashBankFlows::FLOW_TYPE_INCOME;
}

if (empty($multiWallet)) {
    $multiWallet = false;
}
if (empty($isPlan)) {
    $isPlan = $isPlanRepeated = false;
    $stylePlanShow = $stylePlanRepeatedShow = 'display:none';
    $styleFactShow = '';
} else {
    $isPlanRepeated = $isPlan && ($model->is_repeated || $model->first_flow_id);
    $styleFactShow = 'display:none';
    $stylePlanShow = '';
    $stylePlanRepeatedShow = 'display:none';
    $model->date = date('d.m.Y', strtotime($model->date));
    $model->planEndDate = $model->date;
}

$user = Yii::$app->user->identity;
$company = Yii::$app->user->identity->company;
$accounArray = $company->getCheckingAccountants()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

//if (Yii::$app->request->post('CashBankFlowsForm') === null) {
//    if ($lastBik = Yii::$app->session->get('lastBik')) {
//        foreach ($accounArray as $acc) {
//            if ($lastBik == $acc->bik) {
//                $model->rs = $acc->rs;
//                break;
//            }
//        }
//    }
//}

//$flowTypeItems = CashBankFlows::getFlowTypes();
//$flowTypeItems[2] = 'Перевод между счетами';

if (isset($canAddContractor) || Yii::$app->request->get('canAddContractor')) {
    $sellerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' <span class="bold">Добавить поставщика</span>'];
    $customerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' <span class="bold">Добавить покупателя</span>'];
} else {
    $sellerStaticItems = [];
    $customerStaticItems = [];
}

$sellerStaticItems = array_merge($sellerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
    ->all(), 'name', 'text'));
$customerStaticItems = array_merge($customerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->all(), 'name', 'text'));

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($sellerStaticItems as $key => $seller) {
        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($sellerStaticItems[$key]);
        else
            $model->contractor_id = $key;
    }
}

$income = 'income' . ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Редактировать')) . ($multiWallet ? (($isPlan ? ' плановую' : '') . ' операцию') : ' операцию по банку');

$taxItemsIds = InvoiceExpenditureItem::findTaxItems()->select('id')->column();
$taxFields = in_array($model->expenditure_item_id, $taxItemsIds) ? '' : 'hidden';

$taxpayersStatus = TaxpayersStatus::find()->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->select('name')->indexBy('id')->column();
$paymentDetails = PaymentDetails::find()->select('name')->indexBy('id')->column();
$paymentType = PaymentType::find()->select('name')->indexBy('id')->column();

$hasProject = $user->menuItem->project_item;
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
$hasCompanyIndustry = \common\models\company\CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';


$accounCurArray = $company->getForeignCurrencyAccounts()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$amountLabel = $model->getAttributeLabel('amount');
if (!empty($accounCurArray)) {
    $amountLabel .= ', RUB';
}
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'js-cash_flow_update_form',
    'options' => [
        'class' => 'cash_movement_form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data' => [
            'type-income' => CashBankFlows::FLOW_TYPE_INCOME,
            'type-expense' => CashBankFlows::FLOW_TYPE_EXPENSE,
            'foreign' => 0,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= !$model->isNewRecord ? null : $this->render('/bank/_flow_plan_fact_select', ['date' => $model->date]) ?>

<?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
    <?= Html::hiddenInput('redirect', $redirect) ?>
<?php endif ?>

<?= Html::hiddenInput('is_plan_flow', $isPlan, ['id' => 'is_plan_flow']) ?>

<div class="row">
    <div class="col-6">
        <?= $this->render('_flow_type_select', [
            'is_create' => $model->isNewRecord && (!Yii::$app->request->get('originFlow')),
            'flow_type' => $model->flow_type,
            'can_internal' => $model->isNewRecord && Yii::$app->controller->id == 'bank' && !$model->getFlowInvoices()->exists(),
            'input_name' => Html::getInputName($model, 'flow_type'),
            'label' => ($multiWallet) ? 'Добавить операцию' : null
        ]) ?>
    </div>
    <div class="col-6">
        <?php if ($multiWallet): ?>
            <?= $this->render('_account_select_multi', [
                'company' => $company,
                'model' => $model,
                'form' => $form
            ]) ?>
        <?php else: ?>
            <?= $this->render('_account_select', [
                'company' => $company,
                'model' => $model,
                'form' => $form,
                'accounRubArray' => $accounArray,
                'accounCurArray' => $accounCurArray,
                'foreign' => 0,
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<div class="flow-type-toggle <?= $expense ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->label('Поставщик')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_SELLER,
            'staticData' => $sellerStaticItems,
            'options' => [
                'id' => 'seller_contractor_id',
                'class' => 'contractor-items-depend seller',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                    ->where(['id' => [28, 46, 48, 53]])
                    ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                    ->all(), 'id', 'name'),
                'options' => [
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'tax-items' => Json::encode($taxItemsIds),
                        'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                    ],
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ]
            ]); ?>
        <?php else: ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::classname(), [
                'loadAssets' => false,
                'options' => [
                    'id' => 'cashbankflowsform-expenditure_item_id',
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'tax-items' => Json::encode($taxItemsIds),
                        'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                    ],
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ]
            ]); ?>
            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashbankflowsform-expenditure_item_id',
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<div class="flow-type-toggle <?= $income ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $customerStaticItems,
            'options' => [
                'id' => 'customer_contractor_id',
                'class' => 'contractor-items-depend customer cash_contractor_id_select',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::classname(), [
            'loadAssets' => false,
            'income' => true,
            'options' => [
                'id' => 'cashbankflowsform-income_item_id',
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
            ],
            'pluginOptions' => [
                'width' => '100%',
                'placeholder' => '',
            ]
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-income_item_id',
            'type' => 'income',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'amount')->textInput([
            'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
            'class' => 'form-control js_input_to_money_format',
        ])->label($amountLabel); ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'payment_order_number', [
            'options' => ['class' => 'form-group'],
        ])->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'date', [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'label repeated-date-start'
                    ]
                ])->textInput([
                    'value' => ($model->date && ($d = date_create($model->date))) ? $d->format('d.m.Y') : $model->date,
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ]); ?>
            </div>
            <!-- PLAN FLOW PART -->
            <?php if ($model->isNewRecord): ?>
                <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Плановая дата последняя</label>
                        <?= Html::input('text', 'PlanCashFlows[planEndDate]', date('d.m.Y'), [
                            'id' => 'flow_repeated_date_end',
                            'class' => 'form-control date-picker ico',
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php elseif ($isPlan && $model->date != DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d')): ?>
                <div class="col-6 in-plan-flow-show" style="<?=($stylePlanShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Дата по первонач. плану</label>
                        <input class="form-control" disabled style="color:#e30611" value="<?= DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d') ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-6">
        <!-- FACT FLOW PART -->
        <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
            <div class="col-6">
                <?= $form->field($model, 'recognitionDateInput', [
                    //'inputTemplate' => $inputCalendarTemplate,
                    'options' => ['class' => 'form-group'],
                    'labelOptions' => [
                        'class' => 'label bold-text',
                        'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                'class' => 'flow-type-toggle ' . $income,
                            ]) . Html::tag('span', 'расхода', [
                                'class' => 'flow-type-toggle ' . $expense,
                            ]),
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                    'disabled' => (bool)$model->is_prepaid_expense
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'is_prepaid_expense', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'padding-top: 36px'
                    ],
                ])->checkbox(); ?>
            </div>
        </div>
        <!-- PLAN FLOW PART -->
        <div class="row in-plan-flow-show pt-4" style="<?=($stylePlanShow)?>">
            <div class="col-12">
                <?= Html::checkbox('PlanCashFlows[is_repeated]', $isPlanRepeated, [
                    'id' => 'is_repeated_plan_flow',
                    'class' => 'form-group',
                    'label' => '<span class="label">Повторяющаяся плановая операция</span>',
                    'disabled' => !$model->isNewRecord
                ]) ?>
            </div>
        </div>
    </div>
    <!-- PLAN FLOW PART -->
    <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
        <div class="form-group">
            <label class="label repeated-date-end">Периодичность</label>
            <?= Select2::widget([
                'id' => '_plan_period',
                'name' => 'PlanCashFlows[period]',
                'data' => \frontend\modules\analytics\models\PlanCashFlows::$periods,
                'hideSearch' => true,
                'options' => ['id' => 'select2_plan_cash_flows_period'],
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>
    </div>
</div>

<div class="flow-type-toggle <?= $expense ?> in-fact-flow-show" style="<?=($styleFactShow)?>">
    <div class="tax-payment-fields <?= $taxFields ?>">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'taxpayers_status_id')->widget(Select2::classname(), [
                    'data' => $taxpayersStatus,
                    'options' => [
                        'placeholder' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ],
                    'pluginOptions' => [
                        //'allowClear' => true,
                        'width' => '100%',
                    ],
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'kbk')->textInput([
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ])->hint('Необходим для точного расчета налогов к уплате'); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'oktmo_code')->textInput([
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'payment_details_id')->widget(Select2::classname(), [
                    'data' => $paymentDetails,
                    'options' => [
                        'placeholder' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ],
                    'pluginOptions' => [
                        //'allowClear' => true,
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'tax_period_code')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '**.99.9999',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'XX.XX.XXXX',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ],
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'document_number_budget_payment')->textInput([
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'dateBudgetPayment', [
                    'inputTemplate' => $inputCalendarTemplate,
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'payment_type_id')->widget(Select2::classname(), [
                    'data' => $paymentType,
                    'options' => [
                        'placeholder' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    ],
                    'pluginOptions' => [
                        //'allowClear' => true,
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'uin_code')->textInput([
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<div id="invoiceList" class="in-fact-flow-show" style="<?=($styleFactShow)?>">
<?= $form->field($model, 'invoices_list', [
    'wrapperOptions' => [
        'class' => 'row',
    ],
    'options' => [
        'class' => 'mb-0 pb-0',
    ]
])->widget(\frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget::class)->label(false); ?>
</div>

<?php if (CreditFlowBehavior::canAccess()): ?>
    <?php $creditList = $model->getCreditListWithOptions() ?>
    <div id="creditList" class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
        <div class="col-6">
        <?= $form->field($model, 'credit_id')->widget(Select2::class, [
            'data' => $creditList['items'],
            'options' => [
                'placeholder' => 'Без договора',
                'options' => $creditList['options'],
                'data-filter-target' => '#' . ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'customer_contractor_id' : 'seller_contractor_id'),
                'data-filter-attribute' => 'data-contractor',
            ],
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
        </div>
        <div class="col-6">
        <?= $form->field($model, 'credit_amount')->textInput([
            'disabled' => true,
        ]) ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($hasProject || $hasSalePoint || $hasCompanyIndustry): ?>
<div class="row">
    <?php if ($hasProject): ?>
        <div class="col-6">
            <?= $form->field($model, 'project_id')
                ->widget(Select2::class, [
                    'data' => ProjectSearch::getSelect2Data($model->project_id),
                    'options' => [
                        'placeholder' => 'Без проекта',
                        'disabled' => !$model->canChangeProject(),
                        'class' => 'flow-project-id'
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])->label('Проект' . Html::tag('span', Icon::QUESTION, ['class' => 'tooltip-modal-help', 'data-tooltip-content' => '#tooltip_modal_help_project'])); ?>
            <div class="invalid-feedback addon-invalid-feedback" style="margin-top: -26px"></div>
        </div>
    <?php endif; ?>
    <?php if ($hasCompanyIndustry): ?>
        <div class="<?= ($hasSalePoint) ? 'col-3' : 'col-6' ?>">
            <?= $form->field($model, 'industry_id')
                ->widget(Select2::class, [
                    'data' => CompanyIndustry::getSelect2Data(),
                    'options' => [
                        'placeholder' => 'Без направления',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])->label('Направление'); ?>
        </div>
    <?php endif; ?>
    <?php if ($hasSalePoint): ?>
        <div class="<?= ($hasCompanyIndustry) ? 'col-3' : 'col-6' ?>">
            <?= $form->field($model, 'sale_point_id')
                ->widget(Select2::class, [
                    'data' => SalePoint::getSelect2Data(),
                    'options' => [
                        'placeholder' => 'Без точки продаж',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ])->label('Точка продаж'); ?>
        </div>
    <?php endif; ?>
</div>
<?php endif; ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php $form->end(); ?>

<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#js-cash_flow_update_form").closest(".modal-content");
        if (modalContent) {
            if (!modalContent.find(".modal-title").html().length)
                modalContent.find(".modal-title").html("<?= $header ?>");
            modalContent.parent().removeClass('modal-dialog-extra-large');
            // REFRESH_UNIFORMS
            refreshUniform();
            refreshDatepicker();
        }
    <?php endif ?>

    $(document).on("change", "#cashbankflowsform-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashbankflowsform-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });

    $('form [name$="[income_item_id]"], form [name$="[expenditure_item_id]"]').trigger('change');
</script>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-modal-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);
?>

<div style="display: none">
    <div id="tooltip_modal_help_project">
        Если у вас есть проекты, то для учета денежных<br/>операций по проектам, выберите проект.
    </div>
</div>

<?php \yii\widgets\Pjax::end(); ?>
