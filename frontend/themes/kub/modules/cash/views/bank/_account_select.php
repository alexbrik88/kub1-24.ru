<?php

use common\models\currency\Currency;
use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\cash\CashFlowsBase */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $accounRubArray array */
/* @var $accounCurArray array */
/* @var $foreign int */

$rsArray = [];
$rsOtions = [];
$accounRubArray = $accounRubArray ?? $company->getRubleAccounts()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$accountRubBalanceArray = (new \yii\db\Query())
    ->from(\common\models\cash\CashBankFlows::tableName())
    ->where(['company_id' => $company->id])
    ->groupBy('rs')
    ->indexBy('rs')
    ->select(new Expression('SUM(CASE WHEN flow_type = 1 THEN amount ELSE -amount END)'))
    ->column();

foreach ($accounRubArray as $account) {
    $rsArray[$account->rs] = $account->name;
    $rsOtions[$account->rs] = [
        'data-foreign' => 0,
        'data-currency' => Currency::DEFAULT_NAME,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, Currency::DEFAULT_NAME),
        'data-balance' => ArrayHelper::getValue($accountRubBalanceArray, $account->rs, 0),
        'data-rs' => $account->rs,
        'data-url' => Url::current([
            'rs' => $account->rs,
            'foreign' => null,
        ])
    ];
}

$accounCurArray = $accounCurArray ?? $company->getForeignCurrencyAccounts()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$accountCurBalanceArray = (new \yii\db\Query())
    ->from(\common\models\cash\CashBankForeignCurrencyFlows::tableName())
    ->where(['company_id' => $company->id])
    ->groupBy('rs')
    ->indexBy('rs')
    ->select(new Expression('SUM(CASE WHEN flow_type = 1 THEN amount ELSE -amount END)'))
    ->column();

foreach ($accounCurArray as $account) {
    $rsArray[$account->rs] = $account->name;
    $rsOtions[$account->rs] = [
        'data-foreign' => 1,
        'data-currency' => $account->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
        'data-balance' => ArrayHelper::getValue($accountCurBalanceArray, $account->rs, 0),
        'data-rs' => $account->rs,
        'data-url' => Url::current([
            'rs' => $account->rs,
            'foreign' => 1,
        ])
    ];
}

$jsTemplate = new yii\web\JsExpression(<<<JS
    function(data) {
        if (data.element) {
            const bank_name = data.text;
            const balance = number_format(Number(data.element.dataset.balance) / 100, 2, ',', ' ');
            const currency_symbol = data.element.dataset.symbol;
            const rs = data.element.dataset.rs;
            return '<div class="row">' +
                     '<div class="col-6 pr-1 no-overflow font-14">' + bank_name +
                        '<br/>' +
                        '<span class="font-12 text-grey">' + rs + '</span>' +
                      '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' +
                       '<div style="padding-top:7px">' + balance + ' ' + currency_symbol + '</div>' +
                     '</div>' +
                   '</div>';
        }

        return data.text;
    }
JS);

?>

<?= $form->field($model, 'rs')->widget(Select2::class, [
    'data' => $rsArray,
    'options' => [
        'data-foreign' => $foreign,
        'class' => 'flow_form_rs_select',
        'placeholder' => '',
        'options' => $rsOtions,
        'disabled' => $disabled ?? false
    ],
    'pluginOptions' => [
        'width' => '100%',
        'templateResult' => $jsTemplate,
        'templateSelection' => $jsTemplate,
        'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
        'containerCssClass' => 'select2-checking-account-container',
        'dropdownCssClass' => 'select2-checking-account-dropdown'
    ],
]); ?>

<div class="hidden">
    <?= Html::a('', Url::current(), [
        'class' => 'flow_form_rs_toggle_link currency_type_link',
    ]) ?>
</div>