<?php

use common\models\cash\CashFlowsBase;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cash\models\InternalTransferForm */

?>

<div class="internal-transfer-create">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_flow_type_select', [
        'is_create' => true,
        'flow_type' => common\models\cash\CashFlowsBase::INTERNAL_TRANSFER,
        'input_name' => null,
    ]) ?>
    <?= $this->render('_form_internal', [
        'model' => $model,
    ]) ?>
</div>
