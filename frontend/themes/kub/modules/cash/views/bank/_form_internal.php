<?php

use common\models\currency\Currency;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this  yii\web\View */
/* @var $model frontend\modules\cash\models\InternalTransferForm */

$jsTemplate = new yii\web\JsExpression(<<<JS
    function(data) {
        if (data.element) {
            const name = data.text;
            const balance = number_format(Number(data.element.dataset.balance) / 100, 2, ',', ' ');
            const currency_symbol = data.element.dataset.symbol;
            const rs = data.element.dataset.rs;
            return (rs) ?
                   '<div class="row rs-selection-two-columns">' +
                     '<div class="col-6 pr-1 no-overflow font-14">' + name +
                        '<br/>' +
                        '<span class="font-12 text-grey">' + rs + '</span>' +
                      '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' +
                       '<div style="padding-top:6px">' + balance + ' ' + currency_symbol + '</div>' +
                     '</div>' +
                   '</div>'
                   :
                   '<div class="row rs-selection-one-column">' +
                     '<div class="col-6 pr-1 no-overflow font-16">' + name + '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' + balance + ' ' + currency_symbol + '</div>' +
                   '</div>';
        }

        return data.text;
    }
JS);

$rateLabel = sprintf('Курс %s/%s', Html::tag('span', $model->curr_1_name, [
    'class' => 'rate_curr_1_name',
]), Html::tag('span', $model->curr_2_name, [
    'class' => 'rate_curr_2_name',
]));
?>

<?php $form = ActiveForm::begin([
    'id' => 'internal_transfer_form',
    'options' => [
        'class' => 'cash_movement_form',
        'data' => [
            'default-currency' => Currency::DEFAULT_NAME,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>
    <?php endif ?>

    <div class="flow-type-toggle row">
        <div class="col-6">
            <?= $form->field($model, 'account_from')->widget(Select2::class, [
                'data' => $model->getAccountList(),
                'options' => [
                    'class' => '',
                    'placeholder' => '',
                    'options' => $model->getAccountListData(),
                ],
                'pluginOptions' => [
                    //'allowClear' => true,
                    'width' => '100%',
                    'templateResult' => $jsTemplate,
                    'templateSelection' => $jsTemplate,
                    'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
                    'containerCssClass' => 'select2-checking-account-container',
                    'dropdownCssClass' => 'select2-checking-account-dropdown'
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'account_to')->widget(Select2::class, [
                'data' => $model->getAccountList(),
                'options' => [
                    'class' => '',
                    'placeholder' => '',
                    'options' => $model->getAccountListData(),
                ],
                'pluginOptions' => [
                    //'allowClear' => true,
                    'width' => '100%',
                    'templateResult' => $jsTemplate,
                    'templateSelection' => $jsTemplate,
                    'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
                    'containerCssClass' => 'select2-checking-account-container',
                    'dropdownCssClass' => 'select2-checking-account-dropdown'
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'amount_from')->textInput([
                'class' => 'form-control js_input_to_money_format',
                'data' => [
                    'currency' => (string) $model->amount_from_currency,
                ],
            ])->label('Сумма списания'.Html::tag('span', $model->amount_from_suffix, ['class' => 'amount_from_suffix'])); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'date')->textInput([
                'class' => 'form-control date-picker ico',
                'data' => [
                    'date-viewmode' => 'years',
                ],
            ]); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'amount_to')->textInput([
                'class' => 'form-control js_input_to_money_format',
                'readonly' => $model->amount_from_currency == $model->amount_to_currency,
                'data' => [
                    'currency' => (string) $model->amount_to_currency,
                ],
            ])->label('Сумма зачисления'.Html::tag('span', $model->amount_to_suffix, ['class' => 'amount_to_suffix'])); ?>
        </div>
        <div class="col-3 rate_value_vrap <?= $model->rate_value ? '' : 'hidden'; ?>">
            <?= $form->field($model, 'rate_value')->textInput()->label($rateLabel); ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea([
        'style' => 'resize: none;',
        'rows' => '2',
    ]); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">
    $("#internal_transfer_form .date-picker").datepicker(kubDatepickerConfig);
</script>