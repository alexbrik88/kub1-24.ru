<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $piecesModels common\models\cash\CashBankFlows[]
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\project\ProjectSearch;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use \frontend\themes\kub\helpers\Icon;
use common\models\company\CompanyIndustry;
use common\components\TextHelper;
use common\models\cash\CashBankFlowToInvoice;

CashFormAsset::register($this);

if ($model->flow_type === null) {
    $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
}

if (empty($multiWallet)) {
    $multiWallet = false;
}
if (empty($isPlan)) {
    $isPlan = $isPlanRepeated = false;
    $stylePlanShow = $stylePlanRepeatedShow = 'display:none';
    $styleFactShow = '';
} else {
    $isPlanRepeated = $isPlan && ($model->is_repeated || $model->first_flow_id);
    $styleFactShow = 'display:none';
    $stylePlanShow = '';
    $stylePlanRepeatedShow = 'display:none';
    $model->date = date('d.m.Y', strtotime($model->date));
    $model->planEndDate = $model->date;
}

$isIncome = $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;

$user = Yii::$app->user->identity;
$userConfig = $user->config;
$company = Yii::$app->user->identity->company;
$accounArray = $company->getCheckingAccountants()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

if (isset($canAddContractor) || Yii::$app->request->get('canAddContractor')) {
    $sellerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' <span class="bold">Добавить поставщика</span>'];
    $customerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' <span class="bold">Добавить покупателя</span>'];
} else {
    $sellerStaticItems = [];
    $customerStaticItems = [];
}

$sellerStaticItems = array_merge($sellerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
    ->all(), 'name', 'text'));
$customerStaticItems = array_merge($customerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->all(), 'name', 'text'));

$income = 'income' . ($isIncome ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
//$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Редактировать')) . ($multiWallet ? (($isPlan ? ' плановую' : '') . ' операцию') : ' операцию по банку');

//$taxItemsIds = InvoiceExpenditureItem::findTaxItems()->select('id')->column();
//$taxFields = in_array($model->expenditure_item_id, $taxItemsIds) ? '' : 'hidden';
//$taxpayersStatus = TaxpayersStatus::find()->select('name')->indexBy('id')->column();
//$paymentDetails = PaymentDetails::find()->select('name')->indexBy('id')->column();
//$paymentType = PaymentType::find()->select('name')->indexBy('id')->column();

$hasProject = $user->menuItem->project_item;
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
$hasCompanyIndustry = \common\models\company\CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';


$accounCurArray = $company->getForeignCurrencyAccounts()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$amountLabel = $model->getAttributeLabel('amount');
if (!empty($accounCurArray)) {
    $amountLabel .= ', RUB';
}

// table options //
$VISIBLE_MODELS_COUNT = count($model->children) ?: 3;
$incomeItems = InvoiceIncomeItem::getSelect2Data(Yii::$app->user->identity->company->id);
$expenditureItems = InvoiceExpenditureItem::getSelect2Data(Yii::$app->user->identity->company->id);
$tabConfigClass = [
    'recognition_date' => 'col_cash_pieces_recognition_date' . ($userConfig->cash_pieces_recognition_date ? '' : ' hidden'),
    'invoices_list' => 'col_cash_pieces_invoices_list' . ($userConfig->cash_pieces_invoices_list ? '' : ' hidden'),
    'project' => 'col_cash_pieces_project' . ($userConfig->cash_pieces_project ? '' : ' hidden'),
    'sale_point' => 'col_cash_pieces_sale_point' . ($userConfig->cash_pieces_sale_point ? '' : ' hidden'),
    'direction' => 'col_cash_pieces_direction' . ($userConfig->cash_pieces_direction ? '' : ' hidden'),
];
$tabConfigWidth = [
    'amount' => '135',
    'recognition_date' => '135',
    'article' => '99%',
    'invoices_list' => '99%',
    'project' => '99%',
    'sale_point' => '99%',
    'direction' => '99%',
    '_btn' => '15'
];

$selectedInvoices = [
    'data' => ArrayHelper::map($model->invoices, 'id' , function($i) { return '№' . $i->fullNumber;}),
    'options' => ArrayHelper::map($model->invoices, 'id' , function($i) {
        return [
            'data-id' => $i->id,
            'data-number' => $i->fullNumber,
            'data-date' => ($d = date_create_from_format('Y-m-d', $i->document_date)) ? $d->format('d.m.Y') : '',
            'data-amount' => $i->total_amount_with_nds / 100,
            'data-amountPrint' => TextHelper::invoiceMoneyFormat($i->total_amount_with_nds, 2),
        ]; })
];

if ($model->contractor instanceof Contractor) {
    $_isExpense = $model->flow_type != CashFlowsBase::FLOW_TYPE_INCOME;
    $availableInvoicesList = $model::getAvailableInvoices($model->company, $model->contractor, $_isExpense, $model);
    $parentInvoicePayments = CashBankFlowToInvoice::find()->where(['flow_id' => $model->id])->select(['amount', 'invoice_id'])->indexBy('invoice_id')->column();
    $availableInvoices = [
        'data' => ArrayHelper::map($availableInvoicesList, 'id', function ($i) {
            return '№' . $i['number'];
        }),
        'options' => ArrayHelper::map($availableInvoicesList, 'id', function ($i) use ($parentInvoicePayments) {
            return [
                'data-id' => $i['id'],
                'data-number' => $i['number'],
                'data-date' => $i['date'],
                'data-amount' => $i['amount'],
                'data-amountPrint' => $i['amountPrint'],
                'data-amountRemaining' => $i['amountRemaining'] + (($parentInvoicePayments[$i['id']] ?? 0) / 100),
                //'data-amountChild'
            ];
        })
    ];
} else {
    $availableInvoices = [
        'data' => [],
        'options' => []
    ];
}

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'js-cash_flow_update_form',
    'options' => [
        'class' => 'cash_movement_form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data' => [
            'type-income' => CashFlowsBase::FLOW_TYPE_INCOME,
            'type-expense' => CashBankFlows::FLOW_TYPE_EXPENSE,
            'foreign' => 0,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
    <?= Html::hiddenInput('redirect', $redirect) ?>
<?php endif ?>

<?= Html::hiddenInput('is_plan_flow', $isPlan, ['id' => 'is_plan_flow']) ?>

<table class="table-cash-bank-flows-pieces-head w-100">
    <tr>
        <td width="100px" style="padding-top: 36px">
            <?= Html::radio(Html::getInputName($model, 'flow_type'), true, [
                'class' => '--flow-type-toggle-input',
                'value' => $model->flow_type,
                'label' => '<span class="radio-txt-bold">'.($isIncome ? 'Приход':'Расход').'</span>',
                'labelOptions' => [
                    'class' => 'mb-1',
                ],
            ]) ?>
        </td>
        <td width="145px">
            <?= $form->field($model, 'date', [
                'options' => [
                    'class' => 'form-group',
                ],
                'labelOptions' => [
                    'class' => 'label repeated-date-start'
                ]
            ])->textInput([
                'value' => ($model->date && ($d = date_create($model->date))) ? $d->format('d.m.Y') : $model->date,
                'class' => 'form-control date-picker ico',
                'data' => [
                    'date-viewmode' => 'years',
                ],
            ]); ?>
        </td>
        <td width="20%">
            <?= $form->field($model, 'amount')->textInput([
                'value' => !empty($model->amount) ? TextHelper::numberFormat($model->amount / 100, 2) : '',
                'class' => 'form-control',
                'disabled' => true,
                'id' =>  '__mainAmount',
                'name' => '__mainAmount',
            ])->label($amountLabel); ?>
            <?= $form->field($model, 'amount', ['template' => '{input}'])->label(false)
                ->hiddenInput(['value' => !empty($model->amount) ? $model->amount / 100 : '']); ?>
        </td>
        <td width="25%">
            <?php if ($isIncome): ?>
                <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
                    'company' => $company,
                    'contractorType' => Contractor::TYPE_CUSTOMER,
                    'staticData' => $customerStaticItems,
                    'options' => [
                        'id' => 'customer_contractor_id',
                        'class' => 'contractor-items-depend customer cash_contractor_id_select',
                        'placeholder' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                        'data' => [
                            'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                        ],
                    ],
                ]); ?>
            <?php else: ?>
                <?= $form->field($model, 'contractor_id')->label('Поставщик')->widget(ContractorDropdown::class, [
                    'company' => $company,
                    'contractorType' => Contractor::TYPE_SELLER,
                    'staticData' => $sellerStaticItems,
                    'options' => [
                        'id' => 'seller_contractor_id',
                        'class' => 'contractor-items-depend seller',
                        'placeholder' => '',
                        'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                        'data' => [
                            'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
        </td>
        <td>
            <?php if ($multiWallet): ?>
                <?= $this->render('_account_select_multi', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form,
                    'disabled' => true
                ]) ?>
            <?php else: ?>
                <?= $this->render('_account_select', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form,
                    'accounRubArray' => $accounArray,
                    'accounCurArray' => $accounCurArray,
                    'foreign' => 0,
                    'disabled' => true
                ]) ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td colspan="2">
            <div id="invoiceList" class="in-fact-flow-show" style="<?=($styleFactShow)?>">
                <?php /* todo
                <div class="form-group">
                    <label class="label repeated-date-end">Опл. счета</label>
                    <?= Select2::widget([
                        'id' => '_main_invoices_list',
                        'name' => 'CashBankFlowsForm[invoices_list]',
                        'data' => $selectedInvoices['data'],
                        'value' => $model->invoices_list,
                        'hideSearch' => true,
                        'options' => [
                            'options' => $selectedInvoices['options']
                        ],
                        'pluginOptions' => [
                            'multiple' => true,
                            'disabled' => true,
                            'width' => '100%'
                        ],
                    ]) ?>
                </div>
                */ ?>
                <?= $form->field($model, 'invoices_list', [
                    'wrapperOptions' => [
                        'class' => 'row',
                    ],
                    'options' => [
                        'class' => 'mb-0 pb-0',
                    ]
                ])->widget(InvoiceListInputWidget::class, [
                    'showSum' => false,
                    'dropdownCssClass' => 'cash-modal-pieces-main-invoices-list'
                ])->label('Опл. счета'); ?>
            </div>
        </td>
        <td colspan="2">
            <?= $form->field($model, 'description')->textarea([
                'style' => 'height:44px',
                'rows' => '1',
            ]); ?>
        </td>
    </tr>
</table>



<!----------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------->

<div class="row">

    <div class="col-12">
        <?= \frontend\widgets\TableConfigWidget::widget([ // todo
            'items' => [
                ['attribute' => 'cash_pieces_recognition_date'],
                ['attribute' => 'cash_pieces_invoices_list'],
                ['attribute' => 'cash_pieces_project'],
                ['attribute' => 'cash_pieces_sale_point'],
                ['attribute' => 'cash_pieces_direction']
            ]
        ]); ?>
    </div>

    <div class="col-12">
        <table class="table-order-many-create table-cash-bank-flow-pieces">
            <thead>
                <tr>
                    <th width="<?= $tabConfigWidth['amount'] ?>">Разбить на</th>
                    <th width="<?= $tabConfigWidth['recognition_date'] ?>" class="<?= $tabConfigClass['recognition_date'] ?>">Дата признания</th>
                    <th width="<?= $tabConfigWidth['article'] ?>" ><?=($isIncome) ? 'Статья прихода' : 'Статья расхода' ?></th>
                    <th width="<?= $tabConfigWidth['project'] ?>" class="<?= $tabConfigClass['project'] ?>">Проект</th>
                    <th width="<?= $tabConfigWidth['sale_point'] ?>" class="<?= $tabConfigClass['sale_point'] ?>">Точка продаж</th>
                    <th width="<?= $tabConfigWidth['direction'] ?>" class="<?= $tabConfigClass['direction'] ?>">Направ&shy;ление</th>
                    <th width="<?= $tabConfigWidth['invoices_list'] ?>" class="<?= $tabConfigClass['invoices_list'] ?>">Опл. счета</th>
                    <th width="<?= $tabConfigWidth['_btn'] ?>" style="max-width: <?= $tabConfigWidth['_btn'] ?>"><!-- delete --></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($piecesModels as $k => $m): /** @var $m CashBankFlows */ ?>
                    <tr class="<?=($k >= $VISIBLE_MODELS_COUNT ? 'hidden':'')?>">
                        <!-- Amount -->
                        <td data-attr="amount">
                            <?= $form->field($m, 'amount')->textInput([
                                'class' => 'form-control js_input_to_money_format',
                                'style' => ''
                            ])->label(false); ?>
                        </td>
                        <!-- Recognition Date -->
                        <td data-attr="recognition_date" class="<?= $tabConfigClass['recognition_date'] ?>">
                            <?= $form->field($m, 'recognitionDateInput', [
                                'inputTemplate' => $inputCalendarTemplate,
                                'options' => [
                                    'class' => 'form-group',
                                    'style' => ''
                                ],
                                'labelOptions' => [
                                    'class' => 'label repeated-date-start'
                                ]
                            ])->textInput([
                                'class' => 'form-control date-picker',
                                'value' => ($m->recognition_date && ($d = date_create($m->recognition_date))) ? $d->format('d.m.Y') : $m->recognition_date,
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                            ])->label(false); ?>
                        </td>
                        <!-- Income/Expenditure Item -->
                        <td data-attr="article">
                            <?php if ($m->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE) : ?>
                                <?= $form->field($m, 'expenditure_item_id', [
                                    'options' => ['style' => ''],
                                ])->dropdownList(['add-modal-expense-item' => Icon::get('add-icon', ['class' => 'add-button-icon']) . 'Добавить статью расхода'] + $expenditureItems['list'], [
                                    'prompt' => '',
                                    'class' => 'form-control flow-expense-items-pieces'
                                ])->label(false); ?>
                            <?php else: ?>
                                <?= $form->field($m, 'income_item_id', [
                                    'options' => ['style' => ''],
                                ])->dropdownList(['add-modal-income-item' => Icon::get('add-icon', ['class' => 'add-button-icon']) . 'Добавить статью прихода'] + $incomeItems['list'], [
                                    'prompt' => '',
                                    'class' => 'form-control flow-income-items-pieces',
                                ])->label(false); ?>
                            <?php endif; ?>
                        </td>
                        <!-- Project -->
                        <td data-attr="project" class="<?= $tabConfigClass['project'] ?>">

                            <?= $form->field($m, 'project_id', [
                                'options' => ['style' => ''],
                            ])->dropdownList(ProjectSearch::getSelect2Data($m->project_id), [ // const list
                                'prompt' => '',
                                'class' => 'form-control',
                                'disabled' => !$hasProject
                            ])->label(false); ?>
                        </td>
                        <!-- Sale point -->
                        <td data-attr="sale_point" class="<?= $tabConfigClass['sale_point'] ?>">
                            <?= $form->field($m, 'sale_point_id', [
                                'options' => ['style' => ''],
                            ])->dropdownList(SalePoint::getSelect2Data(), [ // const list
                                'prompt' => '',
                                'class' => 'form-control',
                                'disabled' => !$hasSalePoint
                            ])->label(false); ?>
                        </td>
                        <!-- Direction -->
                        <td data-attr="direction" class="<?= $tabConfigClass['direction'] ?>">
                            <?= $form->field($m, 'industry_id', [
                                'options' => ['style' => ''],
                            ])->dropdownList(CompanyIndustry::getSelect2Data(), [ // const list
                                'prompt' => '',
                                'class' => 'form-control',
                                'disabled' => !$hasCompanyIndustry
                            ])->label(false); ?>
                        </td>
                        <!-- Invoices list -->
                        <td data-attr="invoices_list" class="<?= $tabConfigClass['invoices_list'] ?> show-invalid-feedback">
                            <?= $form->field($m, 'invoices_list', [
                                'options' => [
                                    'class' => 'invoice_list_widget_invoices',
                                    'style' => '',
                                ],
                            ])->dropdownList($availableInvoices['data'], [
                                'multiple' => true,
                                'class' => 'invoices-items-depend-in-table-row',
                                'style' => ['height' => 0],
                                'options' => $availableInvoices['options'],
                            ])->label(false); ?>
                        </td>
                        <!-- _delete -->
                        <td>
                            <button class="delete-many-create-order-position button-clr" type="button" title="Удалить">
                                <svg class="table-count-icon svg-icon"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="mb-3">
            <span id="add-many-create-order-position" class="button-regular button-hover-content-red" style="width: <?= $tabConfigWidth['amount'] ?>">
                <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
                <span>Добавить</span>
            </span>
        </div>

    </div>
</div>

<!----------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------->

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php $form->end(); ?>

<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#js-cash_flow_update_form").closest(".modal-content");
        var allCashPage = $('.table-all-cash').length;
        if (modalContent) {
            modalContent.find(".modal-title").html(allCashPage ? "Редактировать операцию" : "Редактировать операцию по банку");
            modalContent.parent().addClass('modal-dialog-extra-large');
            // REFRESH_UNIFORMS
            CashModalUpdatePieces.render();
            CashModalUpdatePieces.setS2DefaultValues();

            //todo: render after modal rendered (by event shownBsModal)
            setTimeout(function() {
                CashModalUpdatePieces.render();
                CashModalUpdatePieces.setS2DefaultValues();
            }, 1000);
        }
    <?php endif ?>

    // $(document).on("change", "#cashbankflowsform-is_prepaid_expense", function (e) {
    //     var $dateInput = $(this).closest('form').find('#cashbankflowsform-recognitiondateinput');
    //     if ($(this).is(":checked")) {
    //         $dateInput.val('').attr('disabled', true);
    //     } else {
    //         $dateInput.removeAttr('disabled');
    //     }
    // });

    $('form [name$="[income_item_id]"], form [name$="[expenditure_item_id]"]').trigger('change');
</script>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-modal-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);
?>

<div style="display: none">
    <div id="tooltip_modal_help_project">
        Если у вас есть проекты, то для учета денежных<br/>операций по проектам, выберите проект.
    </div>
</div>

<?php \yii\widgets\Pjax::end(); ?>
