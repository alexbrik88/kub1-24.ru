<?php

use common\models\cash\CashFlowsBase;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $is_create boolean */
/* @var $can_income boolean */
/* @var $can_expense boolean */
/* @var $flow_type integer */
/* @var $input_name string */
/* @var $label string */

if (!isset($can_income)) {
    $can_income = false;
}
if (!isset($can_expense)) {
    $can_expense = false;
}
if (!isset($can_internal)) {
    $can_internal = false;
}
if (empty($label)) {
    switch (Yii::$app->controller->id) {
        case 'bank':
            $label = 'Добавить операцию по банку';
            break;

        case 'order':
            $label = 'Добавить операцию по кассе';
            break;

        case 'e-money':
            $label = 'Добавить операцию по e-money';
            break;

        default:
            $label = '';
            break;
    }
}

$urlCreate = $is_create ? 'create' : 'update';
$urlCreateInternal = $is_create ? 'create-internal' : 'update-internal';
if (Yii::$app->controller->id == 'multi-wallet') {
    $can_income = false;
    $can_expense = false;
    $hasBanks = (bool)Yii::$app->user->identity->company->mainCheckingAccountant;
    $urlCreate = ($hasBanks) ? 'create-bank-flow' : 'create-order-flow';
    if ($_lastUrl = Yii::$app->session->get('multi_wallet_last_create_url')) {
        if (is_array($_lastUrl)) {
            $urlCreate = $_lastUrl[0];
        }
    }
}

?>

<div class="form-group">
    <label>&nbsp;</label>
    <div class="form-filter d-flex flex-wrap">
        <?php if ($is_create || $can_income || $flow_type == CashFlowsBase::FLOW_TYPE_INCOME) : ?>
            <?= Html::radio($input_name ?? null, $flow_type == CashFlowsBase::FLOW_TYPE_INCOME, [
                'class' => 'flow-type-toggle-input',
                'value' => CashFlowsBase::FLOW_TYPE_INCOME,
                'label' => '<span class="radio-txt-bold">Приход</span>',
                'labelOptions' => [
                    'class' => 'mb-2 mr-4 mt-2  ajax-modal-btn',
                    'data' => [
                        'title' => $label,
                        'url' => Url::current([
                            $urlCreate,
                            'type' => CashFlowsBase::FLOW_TYPE_INCOME,
                            'internal' => null,
                        ]),
                    ],
                ],
            ]) ?>
        <?php endif ?>
        <?php if ($is_create || $can_expense || $flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) : ?>
            <?= Html::radio($input_name ?? null, $flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE, [
                'class' => 'flow-type-toggle-input',
                'value' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'label' => '<span class="radio-txt-bold">Расход</span>',
                'labelOptions' => [
                    'class' => 'mb-2 mr-4 mt-2  ajax-modal-btn',
                    'data' => [
                        'title' => $label,
                        'url' => Url::current([
                            $urlCreate,
                            'type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                            'internal' => null,
                        ]),
                    ],
                ],
            ]) ?>
        <?php endif ?>
        <?php if ($can_internal || $is_create || $flow_type == CashFlowsBase::INTERNAL_TRANSFER) : ?>
            <?= Html::radio($input_name ?? null, $flow_type == CashFlowsBase::INTERNAL_TRANSFER, [
                'class' => 'flow-type-toggle-input',
                'value' => CashFlowsBase::INTERNAL_TRANSFER,
                'label' => '<span class="radio-txt-bold">Между счетами</span>',
                'labelOptions' => [
                    'class' => 'mb-3 mr-4 mt-2 ajax-modal-btn',
                    'data' => [
                        'title' => 'Добавить перевод между счетами',
                        'url' => Url::current([
                            $urlCreateInternal,
                            'type' => CashFlowsBase::INTERNAL_TRANSFER,
                            'internal' => null,
                        ]),
                    ],
                ],
            ]) ?>
        <?php endif ?>
    </div>
</div>
