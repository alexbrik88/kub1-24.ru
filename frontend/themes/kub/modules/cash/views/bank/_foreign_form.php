<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\models\Contractor;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankReasonType;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\project\Project;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use \frontend\themes\kub\helpers\Icon;
use common\components\date\DateHelper;

CashFormAsset::register($this);

if ($model->flow_type === null) {
    $model->flow_type = CashBankFlows::FLOW_TYPE_INCOME;
}

if (empty($multiWallet)) {
    $multiWallet = false;
}
if (empty($isPlan)) {
    $isPlan = $isPlanRepeated = false;
    $stylePlanShow = $stylePlanRepeatedShow = 'display:none';
    $styleFactShow = '';
} else {
    $isPlanRepeated = $isPlan && ($model->is_repeated || $model->first_flow_id);
    $styleFactShow = 'display:none';
    $stylePlanShow = '';
    $stylePlanRepeatedShow = 'display:none';
    $model->date = date('d.m.Y', strtotime($model->date));
    $model->planEndDate = $model->date;
}

$company = Yii::$app->user->identity->company;
$accounArray = $company->getForeignCurrencyAccounts()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

//if (Yii::$app->request->post('CashBankFlowsForm') === null) {
//    if (empty($model->rs) && $lastBik = Yii::$app->session->get('lastBik')) {
//        foreach ($accounArray as $acc) {
//            if ($lastBik == $acc->bik) {
//                $model->rs = $acc->rs;
//                break;
//            }
//        }
//    }
//}

$flowTypeItems = CashBankForeignCurrencyFlows::getFlowTypes();
$flowTypeItems[2] = 'Перевод между счетами';
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}

if (Yii::$app->request->get('canAddContractor')) {
    $sellerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить поставщика'];
    $customerStaticItems = ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить покупателя'];
} else {
    $sellerStaticItems = [];
    $customerStaticItems = [];
}

$sellerStaticItems = array_merge($sellerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
    ->all(), 'name', 'text'));
$customerStaticItems = array_merge($customerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->all(), 'name', 'text'));

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($sellerStaticItems as $key => $seller) {
        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($sellerStaticItems[$key]);
        else
            $model->contractor_id = $key;
    }
}

if ($model->flow_type === null) {
    $model->flow_type = CashBankFlows::FLOW_TYPE_INCOME;
}
$income = 'income' . ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = ($model->isNewRecord ? 'Добавить' : 'Редактировать') . ($multiWallet ? (($isPlan ? ' плановую' : '') . ' операцию') : ' операцию по банку');
$amountLabel = $model->getAttributeLabel('amount_input').', ';
$amountLabel .= Html::tag('span', $model->foreignCurrencyAccount->currency->name ?? '', [
    'class' => 'currency_name_tag',
]);
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'js-cash_flow_update_form',
    'options' => [
        'class' => 'update-movement-form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data' => [
            'type-income' => CashBankFlows::FLOW_TYPE_INCOME,
            'type-expense' => CashBankFlows::FLOW_TYPE_EXPENSE,
            'foreign' => 1,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= !$model->isNewRecord ? null : $this->render('/bank/_flow_plan_fact_select', ['date' => $model->date_input]) ?>

<?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
    <?= Html::hiddenInput('redirect', $redirect) ?>
<?php endif ?>

<?= Html::hiddenInput('is_plan_flow', $isPlan, ['id' => 'is_plan_flow']) ?>

<div class="row">
    <div class="col-6">
        <?= $this->render('_flow_type_select', [
            'is_create' => $model->isNewRecord && (!Yii::$app->request->get('originFlow')),
            'flow_type' => $model->flow_type,
            'can_internal' => Yii::$app->controller->id == 'bank' && !$model->getFlowInvoices()->exists(),
            'input_name' => Html::getInputName($model, 'input_name'),
            'label' => ($multiWallet) ? 'Добавить операцию' : null
        ]) ?>
    </div>
    <div class="col-6">
        <?php if ($multiWallet): ?>
            <?= $this->render('_account_select_multi', [
                'company' => $company,
                'model' => $model,
                'form' => $form
            ]) ?>
        <?php else: ?>
            <?= $this->render('_account_select', [
                'company' => $company,
                'model' => $model,
                'form' => $form,
                'accounCurArray' => $accounArray,
                'foreign' => 1,
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<div class="flow-type-toggle <?= $expense ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->label('Поставщик')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_SELLER,
            'staticData' => $sellerStaticItems,
            'options' => [
                'id' => 'seller_contractor_id',
                'class' => 'contractor-items-depend seller',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                    ->where(['id' => [28, 46, 48, 53]])
                    ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                    ->all(), 'id', 'name'),
                'options' => [
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ]
            ]); ?>
        <?php else: ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::classname(), [
                'loadAssets' => false,
                'options' => [
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ]
            ]); ?>
            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashbankflowsform-expenditure_item_id',
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<div class="flow-type-toggle <?= $income ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $customerStaticItems,
            'options' => [
                'id' => 'customer_contractor_id',
                'class' => 'contractor-items-depend customer cash_contractor_id_select',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::classname(), [
            'loadAssets' => false,
            'income' => true,
            'options' => [
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
            ],
            'pluginOptions' => [
                'width' => '100%',
                'placeholder' => '',
            ]
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-income_item_id',
            'type' => 'income',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'amount_input')->textInput([
            'class' => 'form-control js_input_to_money_format',
        ])->label($amountLabel); ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'payment_order_number', [
            'options' => ['class' => 'form-group'],
        ])->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'date_input', [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'label repeated-date-start'
                    ]
                ])->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                ]); ?>
            </div>
            <!-- PLAN FLOW PART -->
            <?php if ($model->isNewRecord): ?>
                <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Плановая дата последняя</label>
                        <?= Html::input('text', 'PlanCashFlows[planEndDate]', date('d.m.Y'), [
                            'id' => 'flow_repeated_date_end',
                            'class' => 'form-control date-picker ico',
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php elseif ($isPlan && $model->date != DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d')): ?>
                <div class="col-6 in-plan-flow-show" style="<?=($stylePlanShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Дата по первонач. плану</label>
                        <input class="form-control" disabled style="color:#e30611" value="<?= DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d') ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-6">
        <!-- FACT FLOW PART -->
        <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
            <div class="col-6">
                <?= $form->field($model, 'recognition_date_input', [
                    'options' => ['class' => 'form-group'],
                    'labelOptions' => [
                        'class' => 'label bold-text',
                        'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                'class' => 'flow-type-toggle ' . $income,
                            ]) . Html::tag('span', 'расхода', [
                                'class' => 'flow-type-toggle ' . $expense,
                            ]),
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                    'disabled' => (bool)$model->is_prepaid_expense
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'is_prepaid_expense', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'padding-top: 36px'
                    ],
                ])->checkbox(); ?>
            </div>
        </div>
        <!-- PLAN FLOW PART -->
        <div class="row in-plan-flow-show pt-4" style="<?=($stylePlanShow)?>">
            <div class="col-12">
                <?= Html::checkbox('PlanCashFlows[is_repeated]', $isPlanRepeated, [
                    'id' => 'is_repeated_plan_flow',
                    'class' => 'form-group',
                    'label' => '<span class="label">Повторяющаяся плановая операция</span>',
                    'disabled' => !$model->isNewRecord
                ]) ?>
            </div>
        </div>
    </div>
    <!-- PLAN FLOW PART -->
    <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
        <div class="form-group">
            <label class="label repeated-date-end">Периодичность</label>
            <?= Select2::widget([
                'name' => 'PlanCashFlows[period]',
                'data' => \frontend\modules\analytics\models\PlanCashFlows::$periods,
                'hideSearch' => true,
                'options' => ['id' => 'select2_plan_cash_flows_period'],
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>
    </div>
</div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<?php /**
<?= $form->field($model, 'invoices_list', [
    'wrapperOptions' => [
        'class' => 'row',
    ]
])->widget(\frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget::class, [
    'date_attribute' => 'date_input',
    'amount_attribute' => 'amount_input',
])->label(false); ?>
*/ ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php $form->end(); ?>

<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#js-cash_flow_update_form").closest(".modal-content");
        if (modalContent) {
            modalContent.find(".modal-title").html("<?= $header ?>");
            // REFRESH_UNIFORMS
            refreshUniform();
            refreshDatepicker();
        }
    <?php endif ?>

    $(document).on("change", "#cashbankflowsform-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashbankflowsform-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });


</script>

<?php \yii\widgets\Pjax::end(); ?>
