<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\cash\CashBankFlows $model */

$multiWallet = $multiWallet ?? false;
$buttonUri = empty($active) ? '/cash/bank/update-pieces' : '/cash/bank/update';
$buttonStyle = empty($active) ? '' : 'color: #0097FD!important; border-color: #0097FD!important;';
$cssClass = empty($active) && $model->isNewRecord ? '' : 'type_create_many';
?>
<div class="select2-change-one-many-create-orders <?= $cssClass ?>">
    <?= Html::button('Разбить сумму', [
        'class' => 'cash-modal-split-to-pieces button-regular w-100 button-hover-content-red',
        'style' => $buttonStyle,
        'data-title' => 'Редактировать операцию',
        'data-url' => Url::to([$buttonUri, 'id' => $model->id, 'type' => $model->flow_type]),
        'disabled' => ($model->has_tin_children) ? true : false
    ]) ?>
</div>