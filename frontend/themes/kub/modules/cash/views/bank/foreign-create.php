<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows | frontend\modules\cash\models\InternalTransferForm */

?>

<div class="cash-bank-flows-create">
    <?= $this->render('_foreign_form', [
        'model' => $model,
        'multiWallet' => $multiWallet ?? false
    ]) ?>
</div>
