<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

?>

<div class="cash-bank-flows-update cash-flows-update" data-flow-id="<?= $model->id ?>">
    <?= $this->render('_foreign_form', [
        'model' => $model,
        'multiWallet' => $multiWallet ?? false,
        'isPlan' => $isPlan ?? false
    ]) ?>
</div>
