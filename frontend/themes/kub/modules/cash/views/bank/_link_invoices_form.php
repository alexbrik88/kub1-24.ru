<?php

use common\models\cash\CashFlowsBase;
use common\models\document\ForeignCurrencyInvoice;
use frontend\components\StatisticPeriod;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model LinkForm */
/* @var $dataProvider ActiveDataProvider */

$incomeData = $formData[CashFlowsBase::FLOW_TYPE_INCOME];
$exppenseData = $formData[CashFlowsBase::FLOW_TYPE_EXPENSE];
$period = StatisticPeriod::getSessionName();
?>

<?php $form = ActiveForm::begin([
    'id' => 'link_invoices_form',
    'action' => [
        'link-invoices',
        'foreign' => $foreign ?? null,
    ],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
]); ?>
    <div class="vidimus-table-wrap">
        <h5>
            Приходы за период: "<?=$period?>"
        </h5>
        <table id="link_invoices_table" class="table vidimus-table" style="table-layout: fixed; width: 646px;">
            <tr class="vidimus-table-head">
                <td class="" style="word-wrap: break-word; width: 3%"></td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">Дата</td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">№ П/П</td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">Сумма</td>
                <td class="text-left" style="word-wrap: break-word; width: 30%">Контрагент</td>
                <td class="text-left" style="word-wrap: break-word; width: 37%">Назначение</td>
            </tr>
            <?php if (empty($incomeData)) : ?>
                <tr>
                    <td colspan="6" class="text-left">
                        Нет счетов по платежам
                    </td>
                </tr>
            <?php else : ?>
                <?php foreach ($incomeData as $data) : ?>
                    <?php
                    $flow = $data['flow'];
                    $paidInvoices = $data['paidInvoices'];
                    $unpaidInvoices = $data['unpaidInvoices'];
                    $rowspan = count($paidInvoices) + count($unpaidInvoices) + 1;
                    $checked = !empty($paidInvoices);
                    ?>
                    <tr>
                        <td rowspan="2" style="width: 3%">
                            <?= Html::activeCheckbox($model, "flow_ids[{$flow->id}]", [
                                'uncheck' => null,
                                'value' => $flow->id,
                                'label' => false,
                                'checked' => $checked,
                                'class' => 'selected_flow',
                                'data-target' => '.selected_invoice.flow_'.$flow->id,
                            ]) ?>
                        </td>
                        <td class="text-center text-nowrap" style="width: 10%">
                            <?= ($d = date_create($flow->date)) ? $d->format('d.m.Y') : ''; ?>
                        </td>
                        <td class="text-center text-nowrap" style="width: 10%">
                            <?= $flow->payment_order_number ?>
                        </td>
                        <td class="text-right text-nowrap" style="width: 10%">
                            <?= Yii::$app->formatter->asMoney($flow->amount) ?>
                        </td>
                        <td class="text-left" style="width: 30%">
                            <?php if ($flow->contractor) : ?>
                                <?= Html::a(Html::encode($flow->contractor->nameWithType), [
                                    '/contractor/view',
                                    'type' => $flow->contractor->type,
                                    'id' => $flow->contractor->id,
                                ], [
                                    'target' => '_blank',
                                ]) ?>
                            <?php elseif ($flow->cashContractor) : ?>
                                <?= Html::encode($flow->cashContractor->text) ?>
                            <?php endif ?>
                        </td>
                        <td class="text-left" style="width: 37%">
                            <?= Html::encode($flow->description) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="pt-1">
                            <div class="d-flex justify-content-start">
                                <div class="mr-1">
                                    Оплаченные счета:
                                </div>
                                <div class="text-left">
                                    <?php foreach ($paidInvoices as $invoice) : ?>
                                        <?php
                                        $label = $invoice->getTitle();
                                        $label .= ', ';
                                        $label .= Yii::$app->formatter->asMoney($invoice->total_amount_with_nds);
                                        $label .= $invoice instanceof ForeignCurrencyInvoice ? ' '.$invoice->currency_name : ' руб.';
                                        ?>
                                        <div>
                                            <?= Html::activeCheckbox($model, "invoice_ids[{$flow->id}][{$invoice->id}]", [
                                                'uncheck' => null,
                                                'value' => $invoice->id,
                                                'label' => $label,
                                                'checked' => true,
                                                'disabled' => !$checked,
                                                'class' => 'selected_invoice flow_'.$flow->id,
                                            ]) ?>
                                        </div>
                                    <?php endforeach ?>
                                    <?php foreach ($unpaidInvoices as $invoice) : ?>
                                        <?php
                                        $label = $invoice->getTitle();
                                        $label .= ', ';
                                        $label .= Yii::$app->formatter->asMoney($invoice->total_amount_with_nds);
                                        $label .= $invoice instanceof ForeignCurrencyInvoice ? ' '.$invoice->currency_name : ' руб.';
                                        $flowDate = date_create($flow->date);
                                        $invoiceDate = date_create_from_format('Y-m-d|', $invoice->document_date);
                                        ?>
                                        <div>
                                            <?= Html::activeCheckbox($model, "invoice_ids[{$flow->id}][{$invoice->id}]", [
                                                'uncheck' => null,
                                                'value' => $invoice->id,
                                                'label' => $label,
                                                'checked' => false,
                                                'disabled' => !$checked,
                                                'class' => 'selected_invoice flow_'.$flow->id,
                                            ]) ?>
                                            <?php if ($invoiceDate > $flowDate) : ?>
                                                <i class="text-danger">Дата счета больше даты платежа</i>
                                            <?php endif ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </table>
        <h5 class="mt-3">
            Расходы за период: "<?=$period?>"
        </h5>
        <table id="link_invoices_table" class="table vidimus-table" style="table-layout: fixed; width: 646px;">
            <tr class="vidimus-table-head">
                <td class="" style="word-wrap: break-word; width: 3%"></td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">Дата</td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">№ П/П</td>
                <td class="text-center" style="word-wrap: break-word; width: 10%; text-align: center !important;">Сумма</td>
                <td class="text-left" style="word-wrap: break-word; width: 30%">Контрагент</td>
                <td class="text-left" style="word-wrap: break-word; width: 37%">Назначение</td>
            </tr>
            <?php if (empty($exppenseData)) : ?>
                <tr>
                    <td colspan="6" class="text-left">
                        Нет счетов по платежам
                    </td>
                </tr>
            <?php else : ?>
                <?php foreach ($exppenseData as $data) : ?>
                    <?php
                    $flow = $data['flow'];
                    $paidInvoices = $data['paidInvoices'];
                    $unpaidInvoices = $data['unpaidInvoices'];
                    $rowspan = count($paidInvoices) + count($unpaidInvoices) + 1;
                    $checked = !empty($paidInvoices);
                    ?>
                    <tr>
                        <td rowspan="2" style="width: 3%">
                            <?= Html::activeCheckbox($model, "flow_ids[{$flow->id}]", [
                                'uncheck' => null,
                                'value' => $flow->id,
                                'label' => false,
                                'checked' => $checked,
                                'class' => 'selected_flow',
                                'data-target' => '.selected_invoice.flow_'.$flow->id,
                            ]) ?>
                        </td>
                        <td class="text-center text-nowrap" style="width: 10%">
                            <?= ($d = date_create($flow->date)) ? $d->format('d.m.Y') : ''; ?>
                        </td>
                        <td class="text-center text-nowrap" style="width: 10%">
                            <?= $flow->payment_order_number ?>
                        </td>
                        <td class="text-right text-nowrap" style="width: 10%">
                            <?= Yii::$app->formatter->asMoney($flow->amount) ?>
                        </td>
                        <td class="text-left" style="width: 30%">
                            <?php if ($flow->contractor) : ?>
                                <?= Html::a(Html::encode($flow->contractor->nameWithType), [
                                    '/contractor/view',
                                    'type' => $flow->contractor->type,
                                    'id' => $flow->contractor->id,
                                ], [
                                    'target' => '_blank',
                                ]) ?>
                            <?php elseif ($flow->cashContractor) : ?>
                                <?= Html::encode($flow->cashContractor->text) ?>
                            <?php endif ?>
                        </td>
                        <td class="text-left" style="width: 37%">
                            <?= Html::encode($flow->description) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="pt-1">
                            <div class="d-flex justify-content-start">
                                <div class="mr-1">
                                    Оплаченные счета:
                                </div>
                                <div class="text-left">
                                    <?php foreach ($paidInvoices as $invoice) : ?>
                                        <?php
                                        $label = $invoice->getTitle();
                                        $label .= ', ';
                                        $label .= Yii::$app->formatter->asMoney($invoice->total_amount_with_nds);
                                        $label .= $invoice instanceof ForeignCurrencyInvoice ? ' '.$invoice->currency_name : ' руб.';
                                        ?>
                                        <div>
                                            <?= Html::activeCheckbox($model, "invoice_ids[{$flow->id}][{$invoice->id}]", [
                                                'uncheck' => null,
                                                'value' => $invoice->id,
                                                'label' => $label,
                                                'checked' => true,
                                                'disabled' => !$checked,
                                                'class' => 'selected_invoice flow_'.$flow->id,
                                            ]) ?>
                                        </div>
                                    <?php endforeach ?>
                                    <?php foreach ($unpaidInvoices as $invoice) : ?>
                                        <?php
                                        $label = $invoice->getTitle();
                                        $label .= ', ';
                                        $label .= Yii::$app->formatter->asMoney($invoice->total_amount_with_nds);
                                        $label .= $invoice instanceof ForeignCurrencyInvoice ? ' '.$invoice->currency_name : ' руб.';
                                        $flowDate = date_create($flow->date);
                                        $invoiceDate = date_create_from_format('Y-m-d|', $invoice->document_date);
                                        ?>
                                        <div>
                                            <?= Html::activeCheckbox($model, "invoice_ids[{$flow->id}][{$invoice->id}]", [
                                                'uncheck' => null,
                                                'value' => $invoice->id,
                                                'label' => $label,
                                                'checked' => false,
                                                'disabled' => !$checked,
                                                'class' => 'selected_invoice flow_'.$flow->id,
                                            ]) ?>
                                            <?php if ($invoiceDate > $flowDate) : ?>
                                                <i class="text-danger">Дата счета больше даты платежа</i>
                                            <?php endif ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </table>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Применить', [
                'class' => 'button-regular button-width button-regular_red ladda-button',
                'data-url' => Url::to(['link-invoices', 'foreign' => $foreign]),
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-width button-hover-transparent',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
<?php $form->end(); ?>
