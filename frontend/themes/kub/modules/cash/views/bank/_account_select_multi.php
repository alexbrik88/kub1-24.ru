<?php

use common\models\currency\Currency;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\employee\Employee;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use yii\db\Query;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\cash\models\multiWallet\AccountSelectHelper as ASH;

/* @var $bankList array */
/* @var $cashboxList array */
/* @var $emoneyList array */
/* @var Employee $user */
/* @var CashBankFlows|CashOrderFlows|CashEmoneyFlows $model */

$user = Yii::$app->user->identity;

// lists
$bankList = ASH::getBankList($user);
$cashboxList = ASH::getCashboxList($user);
$emoneyList = ASH::getEmoneyList($user);

// balance lists
$bankBalance = ASH::getBankBalanceList($user);
$cashboxBalance = ASH::getCashboxBalanceList($user);
$emoneyBalance = ASH::getEmoneyBalanceList($user);

// sort lists
$bankSort = ASH::getBankSortList($user);
$cashboxSort = ASH::getCashboxSortList($user);
$emoneySort = ASH::getEmoneySortList($user);

$result = [];
$resultSort = [];
$resultOptions = [];

foreach ($bankSort as $rs => $rate) {
    if ($account = ArrayHelper::getValue($bankList, $rs)) {
        $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_BANK : CashFlowsBase::WALLET_BANK;
        $resultSort[$walletType .'_'. $rs] = $rate;
    }
}
foreach ($cashboxSort as $id => $rate) {
    if ($account = ArrayHelper::getValue($cashboxList, $id)) {
        $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_CASHBOX : CashFlowsBase::WALLET_CASHBOX;
        $resultSort[$walletType .'_'. $id] = $rate;
    }
}
foreach ($emoneySort as $id => $rate) {
    if ($account = ArrayHelper::getValue($emoneyList, $id)) {
        $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_EMONEY : CashFlowsBase::WALLET_EMONEY;
        $resultSort[$walletType .'_'. $id] = $rate;
    }
}

arsort($resultSort);
$resultSort = array_slice($resultSort, 0, ASH::FAVORITES_COUNT);

// Favorite accounts
foreach ($resultSort as $_key => $rate) {
    @list($walletType, $walletId) = explode('_', $_key);
    switch ($walletType) {
        case CashFlowsBase::WALLET_BANK:
        case CashFlowsBase::WALLET_FOREIGN_BANK:
            $rs = $walletId;
            if ($account = ArrayHelper::getValue($bankList, $rs)) {
                $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
                $resultOptions[$_key] = [
                    'data-foreign' => $account->isForeign,
                    'data-wallet' => $walletType,
                    'data-currency' => $account->currency->name,
                    'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
                    'data-balance' => ArrayHelper::getValue($bankBalance, $account->rs, 0),
                    'data-id' => $account->id,
                    'data-rs' => $account->rs
                ];
                unset($bankList[$rs]);
            }
            break;
        case CashFlowsBase::WALLET_CASHBOX:
        case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
            $id = $walletId;
            if ($account = ArrayHelper::getValue($cashboxList, $id)) {
                $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
                $resultOptions[$_key] = [
                    'data-foreign' => $account->isForeign,
                    'data-wallet' => $walletType,
                    'data-currency' => $account->currency->name,
                    'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
                    'data-balance' => ArrayHelper::getValue($cashboxBalance, $account->id, 0),
                    'data-id' => $account->id,
                    'data-rs' => ''
                ];
                unset($cashboxList[$id]);
            }
            break;
        case CashFlowsBase::WALLET_EMONEY:
        case CashFlowsBase::WALLET_FOREIGN_EMONEY:
            $id = $walletId;
            if ($account = ArrayHelper::getValue($emoneyList, $id)) {
                $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
                $resultOptions[$_key] = [
                    'data-foreign' => $account->isForeign,
                    'data-wallet' => $walletType,
                    'data-currency' => $account->currency->name,
                    'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
                    'data-balance' => ArrayHelper::getValue($emoneyBalance, $account->id, 0),
                    'data-id' => $account->id,
                    'data-rs' => ''
                ];
                unset($emoneyList[$id]);
            }
            break;
    }
}

if (!empty($resultOptions)) {
    $resultOptions[array_key_last($resultOptions)]['data-bottom_line'] = 1;
}

// Remaining accounts

foreach ($bankList as $account) {
    $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_BANK : CashFlowsBase::WALLET_BANK;
    $_key = $walletType .'_'. $account->rs;
    $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
    $resultOptions[$_key] = [
        'data-foreign' => $account->isForeign,
        'data-wallet' => $walletType,
        'data-currency' => $account->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
        'data-balance' => ArrayHelper::getValue($bankBalance, $account->rs, 0),
        'data-id' => $account->id,
        'data-rs' => $account->rs
    ];
}

foreach ($cashboxList as $account) {
    $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_CASHBOX : CashFlowsBase::WALLET_CASHBOX;
    $_key = $walletType .'_'. $account->id;
    $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
    $resultOptions[$_key] = [
        'data-foreign' => $account->isForeign,
        'data-wallet' => $walletType,
        'data-currency' => $account->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
        'data-balance' => ArrayHelper::getValue($cashboxBalance, $account->id, 0),
        'data-id' => $account->id,
        'data-rs' => ''
    ];
}
foreach ($emoneyList as $account) {
    $walletType = ($account->isForeign) ? CashFlowsBase::WALLET_FOREIGN_EMONEY : CashFlowsBase::WALLET_EMONEY;
    $_key = $walletType .'_'. $account->id;
    $result[$_key] = ASH::getItemName($account->name, $account->currency->name);
    $resultOptions[$_key] = [
        'data-foreign' => $account->isForeign,
        'data-wallet' => $walletType,
        'data-currency' => $account->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $account->currency->name),
        'data-balance' => ArrayHelper::getValue($emoneyBalance, $account->id, 0),
        'data-id' => $account->id,
        'data-rs' => ''
    ];
}

$result['add-modal-rs'] = Icon::PLUS . ' <span class="bold ml-1">Добавить счет или кассу</span>';

$jsTemplate = new yii\web\JsExpression(<<<JS
    function(data) {

        if (data.id === "add-modal-rs") {
            return '<div class="row rs-selection-one-column">' +
                     '<div class="col-12 pr-1 no-overflow font-16">' + data.text + '</div>' +
                   '</div>';
        }

        if (data.element) {
            const name = data.text;
            const balance = number_format(Number(data.element.dataset.balance) / 100, 2, ',', ' ');
            const currency_symbol = data.element.dataset.symbol;
            const rs = data.element.dataset.rs;
            const bottom_line = data.element.dataset.bottom_line;
            return (rs) ?
                   '<div class="row rs-selection-two-columns">' +
                     '<div class="col-6 pr-1 no-overflow font-14">' + name +
                        '<br/>' +
                        '<span class="font-12 text-grey">' + rs + '</span>' +
                      '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' +
                       '<div style="padding-top:6px">' + balance + ' ' + currency_symbol + '</div>' +
                     '</div>' +
                     (bottom_line ? '<div class="bottom-line"></div>' : '') +
                   '</div>'
                   :
                   '<div class="row rs-selection-one-column">' +
                     '<div class="col-6 pr-1 no-overflow font-16">' + name + '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' + balance + ' ' + currency_symbol + '</div>' +
                     (bottom_line ? '<div class="bottom-line"></div>' : '') +
                   '</div>';
        }

        return data.text;
    }
JS);

// Current choice
switch ($model->walletType) {
    case CashFlowsBase::WALLET_BANK:
    case CashFlowsBase::WALLET_FOREIGN_BANK:
        $currentLabel = 'Банковский счет';
        $currentKey = $model->walletType .'_'. $model->rs;
        $currentValue = $model->rs;
        break;
    case CashFlowsBase::WALLET_CASHBOX:
    case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
        $currentLabel = 'Касса';
        $currentKey = $model->walletType .'_'. $model->cashbox_id;
        $currentValue = $model->cashbox_id;
        break;
    case CashFlowsBase::WALLET_EMONEY:
    case CashFlowsBase::WALLET_FOREIGN_EMONEY:
        $currentLabel = 'E-money';
        $currentKey = $model->walletType .'_'. $model->emoney_id;
        $currentValue = $model->emoney_id;
        break;
    default:
        $currentLabel = '---';
        $currentKey = '';
        $currentValue = '';
        break;
}
?>

<div class="form-group">

    <label class="label">
        <?= $currentLabel ?>
    </label>

    <?php
    switch ($model->walletType) {
        case CashFlowsBase::WALLET_BANK:
            echo Html::hiddenInput('CashBankFlowsForm[rs]', $currentValue, ['id' => 'cash_bank_flows_id']);
            break;
        case CashFlowsBase::WALLET_FOREIGN_BANK:
            echo Html::hiddenInput('CashBankForeignCurrencyFlows[rs]', $currentValue, ['id' => 'cash_bank_flows_id']);
            break;
        case CashFlowsBase::WALLET_CASHBOX:
            echo Html::hiddenInput('CashOrderFlows[cashbox_id]', $currentValue, ['id' => 'cash_order_flows_id']);
            break;
        case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
            echo Html::hiddenInput('CashOrderForeignCurrencyFlows[cashbox_id]', $currentValue, ['id' => 'cash_order_flows_id']);
            break;
        case CashFlowsBase::WALLET_EMONEY:
            echo Html::hiddenInput('CashEmoneyFlows[emoney_id]', $currentValue, ['id' => 'cash_emoney_flows_id']);
            break;
        case CashFlowsBase::WALLET_FOREIGN_EMONEY:
            echo Html::hiddenInput('CashEmoneyForeignCurrencyFlows[emoney_id]', $currentValue, ['id' => 'cash_emoney_flows_id']);
            break;
    }
    ?>

    <?= Select2::widget([
        'id' => 'multi_wallet_select',
        'data' => $result,
        'name' => 'wallet',
        'value' => $currentKey,
        'options' => [
            'data-foreign' => $model->isForeign,
            'class' => 'flow_form_wallet_select',
            'placeholder' => '',
            'options' => $resultOptions,
            'disabled' => $disabled ?? false
        ],
        'pluginOptions' => [
            'width' => '100%',
            'templateResult' => $jsTemplate,
            'templateSelection' => $jsTemplate,
            'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
            'containerCssClass' => 'select2-checking-account-container',
            'dropdownCssClass' => 'select2-checking-account-dropdown'
        ],
    ]); ?>
</div>