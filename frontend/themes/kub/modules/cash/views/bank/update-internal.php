<?php

use common\models\cash\CashFlowsBase;
use frontend\modules\cash\models\InternalTransferForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cash\models\InternalTransferForm */

$flow = $model->model;
$noOtherFlow = $flow->internalTransferFlow === null;
$notInternalFlow = !$flow->is_internal_transfer;
?>

<div class="internal-transfer-update">
    <?= $this->render('_flow_type_select', [
        'is_create' => false,
        'flow_type' => CashFlowsBase::INTERNAL_TRANSFER,
        'can_income' => $flow->flow_type == CashFlowsBase::FLOW_TYPE_INCOME && $noOtherFlow && $notInternalFlow,
        'can_expense' => $flow->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE && $noOtherFlow && $notInternalFlow,
    ]) ?>

    <?= $this->render('_form_internal', [
        'model' => $model,
    ]) ?>
</div>
