<?php

use frontend\modules\cash\models\InternalTransferForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\cash\form\CashBankFlowsForm */

$canSplit = empty($isPlan) && empty($model->isForeign) && empty($model->is_internal_transfer) && empty($isClone);
?>

<div class="cash-bank-flows-update cash-flows-update" data-flow-id="<?= $model->id ?>">

    <?php if ($canSplit): ?>
        <?= $this->render('_button_pieces', ['model' => $model]) ?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'canAddContractor' => true,
        'multiWallet' => $multiWallet ?? false,
        'isPlan' => $isPlan ?? false
    ]) ?>
</div>
