<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\project\Project;
use frontend\components\Icon;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\rbac\permissions;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Dropdown;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\themes\kub\assets\CashModalAsset;
use common\components\TextHelper;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;
use frontend\modules\cash\models\CashBankSearch;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\cash\CashBankTinChildrenHelper as TinChildrenHelper;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashBankFlows
 * @var $company Company
 * @var $currentRsModel CheckingAccountant|null
 * @var $foreign
 */

if (!$foreign) { // todo
    CashModalAsset::register($this);
}

$this->title = 'Банк';
$this->params['breadcrumbs'][] = $this->title;

$code = Yii::$app->request->get('code');
$state = Yii::$app->request->get('state');
$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate && Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate && CompanyIndustry::find()->where(['company_id' => $company->id])->exists();
$pageRoute = ['/cash/bank/index', 'rs' => $rs];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_cash');
$tabConfigClass = [
    'billPaying' => 'col_bank_column_paying' . ($userConfig->bank_column_paying ? '' : ' hidden'),
    'incomeExpense' => 'col_bank_column_income_expense' . ($userConfig->bank_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_bank_column_income_expense'  . (!$userConfig->bank_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_bank_column_income_expense' . (!$userConfig->bank_column_income_expense ? '' : ' hidden'),
    'project' => 'col_bank_column_project' . ($userConfig->bank_column_project ? '' : ' hidden'),
    'salePoint' => 'col_bank_column_sale_point' . ($userConfig->bank_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_bank_column_industry' . ($userConfig->bank_column_industry ? '' : ' hidden'),
    'recognitionDate' => 'col_bank_column_recognition_date' . ($userConfig->bank_column_recognition_date ? '' : ' hidden'),
];

if (empty($foreign)) {
    $foreign = null;
}

$moreItems = [];
if (Yii::$app->user->can(\frontend\rbac\permissions\Cash::LINKAGE)) {
    $moreItems[] = [
        'label' => 'Настройка связок',
        'url' => '#',
        'linkOptions' => [
            'class' => 'ajax-modal-btn',
            'data-title' => 'Настройка связок по операциям',
            'data-url' => Url::to(['/cash/linkage/index']),
        ]
    ];
}
if ($canUpdate) {
    $moreItems[] = [
        'label' => 'Привязать платежи к счетам',
        'url' => '#link_invoices_modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ]
    ];
}
$moreItems[] = [
    'label' => 'Поиск дублей',
    'url' => Url::current(['duplicates' => 1]),
];
if (!$foreign) {
    $moreItems[] = [
        'label' => 'Ручные операции',
        'url' => '#manual_flows_modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ]
    ];
}
?>

<div class="stop-zone-for-fixed-elems  cash-bank-flows-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <?= \frontend\themes\kub\widgets\BankAccountsFilterWidget::widget([
            'pageTitle' => $this->title,
            'rs' => $rs,
            'company' => $company,
        ]); ?>
        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Загрузить выписку</span>', [
            '/cash/banking/default/index',
            'p' => Banking::routeEncode($pageRoute),
        ], [
            'class' => 'banking-module-open-link button-regular button-regular_padding_medium button-regular_red ml-auto button-clr',
        ]) ?>
    </div>

    <?php /* \yii\widgets\Pjax::begin([
        'id' => 'cash-bank-flows-pjax-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]); */ ?>
    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <div class="dropdown popup-dropdown popup-dropdown_position-shevron" style="margin-bottom: 12px;">
                    <?php if ($canCreate) : ?>
                        <?php if (Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) : ?>
                            <button class="add-operation button-regular w-100 button-hover-content-red disabled"
                                title="Чтобы добавить операцию по банку, укажите расчетный счет в разделе Настройки-Профиль компании">
                                <span>Добавить</span>
                            </button>
                        <?php else : ?>
                            <?= Html::button(Icon::get('add-icon') . '<span>Добавить</span>', [
                                'class' => "ajax-modal-btn add-operation button-regular w-100 button-hover-content-red",
                                'title' => "Добавить операцию по банку",
                                'data-title' => "Добавить операцию по банку",
                                'data-pjax' => "0",
                                'data-url' => Url::to([
                                    'create',
                                    'type' => CashBankFlows::FLOW_TYPE_INCOME,
                                    'rs' => $rs,
                                    'foreign' => $foreign ?? null,
                                ]),
                            ]) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if (count($moreItems) > 0) : ?>
                    <div class="dropdown">
                        <?= Html::a('<span>Еще</span>' . Icon::get('shevron', [
                            'class' => 'svg-icon_grey svg-icon_shevron svg-icon_absolute',
                        ]), '#', [
                            'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => 'false'
                        ]); ?>

                        <?= Dropdown::widget([
                            'options' => [
                                'class' => 'dropdown-menu form-filter-list list-clr w-100'
                            ],
                            'items' => $moreItems
                        ]); ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/modules/cash/views/default/_partial/table-filter', [
        'searchModel' => $model,
        'filterUrl' => 'index',
        'tableView' => [
            'attribute' => 'table_view_cash'
        ],
        'tableConfig' => [
            [
                'attribute' => 'bank_column_income_expense',
                'invert_attribute' => 'invert_bank_column_income_expense'
            ],
            [
                'attribute' => 'bank_column_recognition_date'
            ],
            [
                'attribute' => 'bank_column_paying'
            ],
            [
                'attribute' => 'bank_column_project'
            ],
            [
                'attribute' => 'bank_column_sale_point'
            ],
            [
                'attribute' => 'bank_column_industry'
            ],
            [
                'attribute' => 'cash_index_hide_plan',
                'style' => 'border-top: 1px solid #ddd',
                'refresh-page' => true
            ],
        ]
    ]) ?>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'emptyText' => $model->duplicates ? 'В выбанном периоде дублей нет' : ($model->getIsExists() ?
            'В указанном периоде нет операций по банку ' :
            'Еще не загружена ни одна операция по банку ').Html::encode($model->getBankName()
        ),
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $typeCss = 'income-item';
                        $income = round($flows['amount'] / 100, 2);
                        $expense = 0; //($flows['is_internal_transfer'] && $flows['wallet_id'] != 'plan') ? $income : 0;
                    } else {
                        $typeCss = 'expense-item';
                        $expense = round($flows['amount'] / 100, 2);
                        $income = 0; //($flows['is_internal_transfer'] && $flows['wallet_id'] != 'plan') ? $expense : 0;
                    }

                    $copyUrl = ($flows['tb'] == CashBankFlows::tableName())
                        ? Url::to(['/cash/bank/create-copy', 'id' => $flows['id'], 'redirect' => Url::current()])
                        : Url::to(['/cash/plan/create-copy', 'id' => $flows['id'], 'redirect' => Url::current()]);

                    return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                        'class' => 'joint-operation-checkbox ' . $typeCss,
                        'value' => $flows['id'],
                        'data' => [
                            'income' => $income,
                            'expense' => $expense,
                            'copy-url' => $copyUrl
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'recognition_date',
                'label' => 'Дата признания',
                'headerOptions' => [
                    'class' => 'sorting ' . $tabConfigClass['recognitionDate'],
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['recognitionDate']
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $ret = DateHelper::format($flows['recognition_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    if ($pieces = TinChildrenHelper::getPieces($flows, 'recognition_date'))
                        $ret .= $pieces;

                    return $ret;

                }
            ],
            [
                'attribute' => 'amountIncomeExpense',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => $tabConfigClass['incomeExpense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $income = '+ ' . TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                    $expense = '- ' . TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);

                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        @list($from, $to) = explode('_', $flows['transfer_key']);
                        if ($from == $flows['id'])
                            return Html::tag('div', CashBankSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        if ($to == $flows['id'])
                            return Html::tag('div', CashBankSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                    } else {
                        if ($flows['amountIncome'] > 0) {

                            $link = CashBankSearch::getUpdateFlowLink($income, $flows);
                            if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                                $link .= $pieces;

                            return Html::tag('div', $link, ['class' => 'green-link']);
                        }
                        if ($flows['amountExpense'] > 0) {

                            $link = CashBankSearch::getUpdateFlowLink($expense, $flows);
                            if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                                $link .= $pieces;

                            return Html::tag('div', $link, ['class' => 'red-link']);
                        }
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'amountIncome',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => $tabConfigClass['income'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $formattedAmount = TextHelper::invoiceMoneyFormat($flows['amount'], 2);
                    if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                        $formattedAmount .= $pieces;

                    return ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME)
                        ? Html::tag('span', $formattedAmount, ['class' => $flows['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'amountExpense',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $formattedAmount = TextHelper::invoiceMoneyFormat($flows['amount'], 2);
                    if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                        $formattedAmount .= $pieces;

                    return ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE)
                        ? Html::tag('span', $formattedAmount, ['class' => $flows['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'rs',
                'label' => 'Расч/счет',
                'format' => 'html',
                'value' => function ($data) use ($company) {
                    if ($account = $company->accountByRs($data['rs'])) {
                        return Html::encode($account->name).Html::tag('br').Html::encode($account->rs);
                    }

                    return '---';
                },
                'filter' => ['' => 'Все счета'] + $model->getAccountsFilterItems(),
                's2width' => '250px',
                'hideSearch' => false,
                'visible' => $rs == 'all' || in_array($rs, $foreignItems),
            ],
            [
                'attribute' => 'contractor_ids',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['is_internal_transfer'])
                        return '';

                    if ($flows['contractor_id'] > 0 && ($contractor = Contractor::findOne($flows['contractor_id'])) !== null) {
                        return Html::a(Html::encode($contractor->nameWithType), [
                            '/contractor/view',
                            'type' => $contractor->type,
                            'id' => $contractor->id,
                        ], ['target' => '_blank', 'title' => $contractor->nameWithType]);
                    } else {

                        $model = ($flows['wallet_id'] == 'plan')
                            ? PlanCashFlows::findOne($flows['id'])
                            : CashBankFlows::findOne($flows['id']);

                        if ($model && $model->cashContractor)
                            return Html::tag('span', Html::encode($model->cashContractor->text),
                                ['title' => $model->cashContractor->text]);
                    }

                    return '---';
                },
                'filter' => ['' => 'Все контрагенты'] + $model->getContractorFilterItems(),
                's2width' => '250px',
                'hideSearch' => false,
            ],
            [
                's2width' => '200px',
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => $tabConfigClass['project'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
                ],
                'filter' => ['' => 'Все проекты'] + $model->getProjectFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {

                    $ret = '';

                    if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                        $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                    } else {
                        $ret .= '-';
                    }

                    if ($pieces = TinChildrenHelper::getPieces($flows, 'project_id'))
                        $ret .= $pieces;

                    return $ret;
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => $tabConfigClass['salePoint'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
                ],
                'filter' => ['' => 'Все точки продаж'] + $model->getSalePointFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {

                    $ret = '';

                    if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                        $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                    } else {
                        $ret .= '-';
                    }

                    if ($pieces = TinChildrenHelper::getPieces($flows, 'sale_point_id'))
                        $ret .= $pieces;

                    return $ret;
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => $tabConfigClass['companyIndustry'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
                ],
                'filter' => ['' => 'Все направления'] + $model->getCompanyIndustryFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {

                    $ret = '';

                    if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                        $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                    } else {
                        $ret .= '-';
                    }

                    if ($pieces = TinChildrenHelper::getPieces($flows, 'industry_id'))
                        $ret .= $pieces;

                    return $ret;
                },
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'purpose-cell',
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    if ($flows['description']) {
                        $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                        return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'billPaying',
                'headerOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    switch ($flows['tb']) {
                        case CashBankFlows::tableName():
                            $model = CashBankFlows::findOne($flows['id']);
                            return '<div style="max-width:50px">' . ($model->credit_id ? $model->creditPaying : $model->billPaying) . '</div>';
                        case PlanCashFlows::tableName():
                            $model = PlanCashFlows::findOne($flows['id']);
                            $currDate = date('Y-m-d');
                            if ($model->first_date < $currDate && $model->date != $model->first_date)
                                return 'Перенос';
                            elseif ($model->date >= $currDate)
                                return 'План';
                            elseif ($model->date < $currDate)
                                return 'Просрочен';
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'reason_ids',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'clause-cell',
                ],
                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                'format' => 'raw',
                'value' => function ($flows) {

                    if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {

                        $mainReason = $reason = (($item = InvoiceIncomeItem::findOne($flows['income_item_id'])) ?
                            $item->fullName : "id={$flows['income_item_id']}");

                        if ($pieces = TinChildrenHelper::getPieces($flows, 'income_item_id'))
                            $reason .= $pieces;

                    } else {

                        $mainReason = $reason = (($item = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ?
                            $item->fullName : "id={$flows['expenditure_item_id']}");

                        if ($pieces = TinChildrenHelper::getPieces($flows, 'expenditure_item_id'))
                            $reason .= $pieces;
                    }

                    return $reason ? Html::tag('span', $reason, ['title-as-html' => 1, 'title' => htmlspecialchars($mainReason)]) : '-';
                },
                's2width' => '200px',
                'hideSearch' => false,
            ],
            [
                'class' => ActionColumn::class,
                'template' => '<div class="table-count-cell" style="min-width:60px">{update}{delete}</div>',
                'headerOptions' => [
                    'width' => '5%',
                    'style' => 'max-width:60px'
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'urlCreator' => function ($action, $flows, $key, $index, $actionColumn) use ($foreign) {
                    if ($action == 'update' && $flows['is_internal_transfer']) {
                        $action = 'update-internal';
                    }
                    $params = [
                        'id' => $flows['id'],
                        'foreign' => $foreign,
                        'is_plan_flow' => ($flows['wallet_id'] == 'plan') ? '1' : ''
                    ];
                    $params[0] = $actionColumn->controller ? $actionColumn->controller . '/' . $action : $action;

                    return Url::toRoute($params);
                },
                'buttons' => [
                    'update' => function ($url, $flows, $key) {
                        $options = [
                            'class' => 'button-clr link mr-1 update-flow-item',
                            'title' => 'Редактировать',
                            'data' => [
                                'title' => 'Редактировать операцию по банку',
                                'pjax' => 0,
                                'toggle' => 'modal',
                                'target' => '#update-movement',
                                'data-url' => $url,
                            ],
                        ];

                        return Html::a(Icon::get('pencil'), $url, $options);
                    },
                    'delete' => function ($url, $flows) {

                        return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                            'theme' => 'gray',
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'button-clr link',
                                'tag' => 'button',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php // \yii\widgets\Pjax::end(); ?>
</div>

<?= SummarySelectWidget::widget([
    'buttonsViewType' => ($canProjectUpdate || $canSalePointUpdate || $canIndustryUpdate) ? SummarySelectWidget::VIEW_DROPDOWN : SummarySelectWidget::VIEW_INLINE,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ]
]); ?>

<?= BankingModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to($pageRoute),
]) ?>

<?= $this->render('_partial/modal-add-contractor'); ?>
<?= $this->render('_partial/modal-many-delete'); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
    'foreign' => $foreign,
]); ?>
<?= $this->render('_partial/modal_link_invoices', [
    'model' => $model,
    'canUpdate' => $canUpdate,
    'foreign' => $foreign,
]) ?>
<?= $this->render('_partial/modal_manual_flows', [
    'model' => $model,
    'foreign' => $foreign,
]) ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-project'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-sale-point'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-company-industry'); ?>

<div class="modal fade" id="update-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Редактировать операцию по банку</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
$.pjax.defaults.timeout = 10000;

$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "update-movement") {
        $(this).find(".modal-body").empty();
        $(this).find("#js-modal_update_title").empty();
    }
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(document).on("show.bs.modal", "#add-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "add-movement") {
        $(this).find(".modal-body").empty();
    }
});

$(document).on("hide.bs.modal", "#add-movement", function(event) {
    if (event.target.id === "add-movement") {
        $("#add-movement .modal-body").empty();
    }
});

$(document).on("submit", "#js-cash_flow_update_form", function(event) {
    var $isNewRecord = $(this).attr("is_new_record");
    $.ajax({
        "type": "post",
        "url": $(this).attr("action"),
        "data": $(this).serialize(),
        "success": function(data) {
                $("#add-movement, #update-movement").modal("hide");
                if ($("#cash-bank-flows-pjax-container").length)
                    $.pjax.reload({"container":"#cash-bank-flows-pjax-container", "timeout": 5000});
                else
                    location.href = location.href;
        }
    });

    return false;
});

$(document).on("pjax:complete", function(event) {
    // REFRESH_UNIFORMS
    refreshUniform();
    refreshDatepicker();
});

$(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);
    var $header = $modal.find("h4.modal-title");
    var $additionalHeaderText = null;

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    if ($includeExpenditureItem && $includeIncomeItem) {
        $additionalHeaderText = " прихода / расхода";
    } else if ($includeExpenditureItem) {
        $additionalHeaderText = " расхода";
    } else if ($includeIncomeItem) {
        $additionalHeaderText = " прихода";
    }
    $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
    $(".additional-header-text").remove();
    $(".modal#many-item form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    // var l = Ladda.create($(this).find(".btn-save")[0]);
    var $hasError = false;

    // l.start();
    $(".field-cashbanksearch-incomeitemidmanyitem:visible, .field-cashbanksearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        return false;
    }
});

// SHOW_AJAX_MODAL
$("#add-movement").on("show.bs.modal", function (e) {

    var button = $(e.relatedTarget);
    var modal = $(this);
    if ($(button).attr("href"))
        modal.find(".modal-body").load(button.attr("href"));
});

$("#update-movement").on("show.bs.modal", function (e) {

    var button = $(e.relatedTarget);
    var modal = $(this);
    if ($(button).attr("href"))
        modal.find(".modal-body").load(button.attr("href"));
});

// REFRESH_UNIFORMS
$(document).on("pjax:complete", "#cash-bank-flows-pjax-container", function() {
    refreshUniform();
});

// SEARCH
$("input#cashbanksearch-contractor_name").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});

/* SELECTS */
$(document).on("select2:open", "#cashbankflowsform-rs", function() {});

/* VIDIMUS BUTTON */
$(document).on(\'mouseover\', \'input[name="uploadfile"]\', function() {
    $(\'.add-vidimus-file\').css({\'color\': \'#335A82\', \'cursor\':\'pointer\'});
});
$(document).on(\'mouseout\', \'input[name="uploadfile"]\', function() {
    $(\'.add-vidimus-file\').css(\'color\', \'#001424\');
});

/////////////////////////////////
if (window.PlanModal)
    PlanModal.init();
/////////////////////////////////

/////////////////////////////////
if (window.CashModalUpdatePieces)
    CashModalUpdatePieces.init();
/////////////////////////////////

');

if (Yii::$app->request->get('show_add_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            let url = $(".add-operation").attr("href");
            $("#add-movement").find(".modal-body").load(url);
            $("#add-movement").modal("show");
            window.history.pushState("object", document.title, location.href.split("?")[0]);
        });
    ');
}
?>
