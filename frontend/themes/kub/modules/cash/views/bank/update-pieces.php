<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */
/* @var $piecesModels common\models\cash\CashBankFlows[] */

$isPlan = $isPlan ?? false;
$multiWallet = $multiWallet ?? false;
?>

<div class="cash-bank-flows-update cash-flows-update" data-flow-id="<?= $model->id ?>">

    <?php if (empty($isPlan) && empty($multiWallet) && empty($model->isForeign) && empty($model->is_internal_transfer) && empty($isClone)): ?>
        <?= $this->render('_button_pieces', ['model' => $model, 'active' => true]) ?>
    <?php endif; ?>

    <?= $this->render('_form_pieces', [
        'model' => $model,
        'piecesModels' => $piecesModels,
        'canAddContractor' => true,
        'multiWallet' => $multiWallet,
        'isPlan' => $isPlan
    ]) ?>
</div>
