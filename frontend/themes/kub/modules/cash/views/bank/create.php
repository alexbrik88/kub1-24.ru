<?php

use frontend\modules\cash\models\InternalTransferForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

?>

<div class="cash-bank-flows-create">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_form', [
        'model' => $model,
        'canAddContractor' => true,
        'multiWallet' => $multiWallet ?? false,
        'isClone' => $isClone ?? false
    ]) ?>
</div>
