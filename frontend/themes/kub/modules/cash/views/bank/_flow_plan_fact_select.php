<?php

/** $this yii\web\View */
/** $date string */

?>

<div style="position: absolute; right: 0; top: -66px; width: 120px;">
    <?= \kartik\select2\Select2::widget([
        'id' => 'flow_plan_fact_select',
        'name' => 'flow_plan_fact_select',
        'value' => (date_create($date) ?: date_create()) > date_create() ? 'plan' : 'fact',
        'hideSearch' => true,
        'data' => [
            'fact' => 'Факт',
            'plan' => 'План',
        ],
        'options' => [
            'class' => 'flow_plan_fact_select',
            'options' => [
                'fact' => ['data-date' => date('d.m.Y')],
                'plan' => ['data-date' => date_create('+1 days')->format('d.m.Y')],
            ],
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>
</div>
