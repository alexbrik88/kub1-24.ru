<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2018
 * Time: 7:02
 */

use common\models\Company;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use frontend\rbac\permissions;


$this->beginContent('@frontend/views/layouts/main.php');

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;
?>
<div class="cash-default-content container-fluid nav-tabs-row" style="padding: 0; margin-top: -10px;">
<?php
    echo Nav::widget([
        'id' => 'cash-default-menu',
        'options' => [
            'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            'style' => 'margin-bottom: 20px;'
        ],
        'items' => [
            [
                'label' => 'Итого',
                'url' => ['/cash',],
                'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($module == 'cash' && $controller == 'default' && $action == 'index' ? ' active' : '')
                ]
            ],
            [
                'label' => 'ОДДС',
                'url' => ['/cash/default/odds',],
                'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                'options' => [
                    'class' => 'nav-item'
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($module == 'cash' && $controller == 'default' && $action == 'odds' ? ' active' : '')
                ]
            ],
        ],
    ]);
    ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
