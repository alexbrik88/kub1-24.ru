<?php

use common\models\company\CheckingAccountant;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\company\CheckingAccountant */
/* @var $save bool */

?>

<div class="modal-header">
    <h5 class="modal-title">Редактировать банковский счет</h5>
</div>

<?= $this->render('//company/form/modal_rs/_partial/_form', [
    'checkingAccountant' => $model,
    'company' => $company,
    'actionUrl' => Url::current(),
]) ?>

<?php if ($save) : ?>
    <script type="text/javascript">
        window.bankAccountFilterReload();
    </script>
<?php endif ?>
