<?php

use common\models\company\CheckingAccountant;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\company\CheckingAccountant */

?>

<div class="modal-header">
    <h5 class="modal-title">Добавить E-money</h5>
</div>

<?= $this->render('//emoney/_form', [
    'model' => $model,
]) ?>
