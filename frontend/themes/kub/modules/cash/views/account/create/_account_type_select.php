<?php

use common\models\cash\CashBankForeignCurrencyFlows;
use yii\helpers\Html;
use yii\helpers\Url;

$action = $this->context->action->id;
?>

<style type="text/css">
    .company_account_type_link:hover {
        color: #001424;
        text-decoration: none;
    }
</style>
<div class="form-group">
    <div class="form-filter d-flex flex-nowrap">
        <?= Html::a(Html::radio(null, 'bank' == $action).' Банковский счет', [
            '/cash/account/create/bank',
        ], [
            'class' => 'company_account_type_link radio-txt-bold mr-4',
        ]) ?>
        <?= Html::a(Html::radio(null, 'cashbox' == $action).' Кассу', [
            '/cash/account/create/cashbox',
        ], [
            'class' => 'company_account_type_link radio-txt-bold mr-4',
        ]) ?>
        <?= Html::a(Html::radio(null, 'emoney' == $action).' E-money', [
            '/cash/account/create/emoney',
        ], [
            'class' => 'company_account_type_link radio-txt-bold mr-4',
        ]) ?>
    </div>
</div>