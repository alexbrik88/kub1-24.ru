<?php

use common\models\currency\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $data array */
/* @var $view string */
/* @var $save bool */

$save = $save ?? false;
$action = $this->context->action;
?>

<?php Pjax::begin([
    'id' => 'add_company_account_pjax',
    'enablePushState' => false,
    'linkSelector' => '.company_account_type_link',
    'formSelector' => '#add_company_account_pjax form',
]) ?>

<div class="modal-header d-flex justify-content-between align-items-baseline">
    <h5 class="modal-title w-auto">Добавить</h5>
    <div class="ml-auto">
        <?= $this->render('_account_type_select') ?>
    </div>
</div>

<?= Html::tag('div', null, [
    'id' => 'add_company_account_model',
    'class' => 'hidden',
    'data-model' => Json::encode($model->toArray()),
    'data-type' => $action->id,
]) ?>

<?= $this->render($view, $data) ?>

<?php if ($save) : ?>
    <?php $data = [
        'id' => $model->getInternalTransferId(),
        'bank_name' => $model->getInternalTransferName(),
        'data_wallet' => $model->getWalletId(),
        'data_currency' => $model->getCurrencyName(),
        'data_symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $model->getCurrencyName()),
        'data_balance' => $model->getBalance(),
        'data_id' => $model->id,
        'data_rs' => $model->rs ?? null,
    ]; ?>
    <script type="text/javascript">
        window.companyAccountAfterSave(<?= Json::encode($data) ?>);
    </script>
<?php endif ?>

<?php Pjax::end() ?>
