<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->registerJs(<<<SCRIPT
$('#add_company_account_modal').on('show.bs.modal', function (e) {
    let url = $(this).data('url');
    $.ajax({
        url: url,
    }).done(function(data) {
        $('#add_company_account_modal_content').html(data);
    });
});
$('#add_company_account_modal').on('hidden.bs.modal', function (e) {
    $('#add_company_account_modal_content').html('');
});
SCRIPT
);
?>

<?php Modal::begin([
    'id' => 'add_company_account_modal',
    'closeButton' => false,
    'options' => [
        'data-url' => Url::to(['/cash/account/create/bank']),
    ],
]) ?>
    <?= Html::button(Icon::get('close'), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ]) ?>
    <div id="add_company_account_modal_content"></div>
<?php Modal::end() ?>
