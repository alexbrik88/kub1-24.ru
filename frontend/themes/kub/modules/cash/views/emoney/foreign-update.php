<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

$this->title = 'Редактировать операцию по e-money';

?>
<div class="cash-emoney-flows-update cash-flows-update" data-flow-id="<?= $model->id ?>">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_foreign_form', [
        'model' => $model,
        'company' => $company,
        'isClone' => isset($isClone),
        'multiWallet' => $multiWallet ?? false,
        'isPlan' => $isPlan ?? false,
    ]) ?>
</div>
