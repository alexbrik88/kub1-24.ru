<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashEmoneyFlows
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\project\Project;
use frontend\components\Icon;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\rbac\permissions;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\themes\kub\assets\CashModalAsset;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;
use frontend\modules\cash\models\CashEmoneySearch;

if (!$foreign) { // todo
    CashModalAsset::register($this);
}

$this->title = 'E-money';
$this->params['breadcrumbs'][] = $this->title;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate && Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate && CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_cash');
$tabConfigClass = [
    'billPaying' => 'col_emoney_column_paying' . ($userConfig->emoney_column_paying ? '' : ' hidden'),
    'incomeExpense' => 'col_emoney_column_income_expense' . ($userConfig->emoney_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_emoney_column_income_expense'  . (!$userConfig->emoney_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_emoney_column_income_expense' . (!$userConfig->emoney_column_income_expense ? '' : ' hidden'),
    'project' => 'col_emoney_column_project' . ($userConfig->emoney_column_project ? '' : ' hidden'),
    'salePoint' => 'col_emoney_column_sale_point' . ($userConfig->emoney_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_emoney_column_industry' . ($userConfig->emoney_column_industry ? '' : ' hidden'),
];
?>

<div class="stop-zone-for-fixed-elems  cash-emoney-flows-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <?= \frontend\themes\kub\widgets\EmoneyAccountFilterWidget::widget([
            'pageTitle' => $this->title,
            'emoney' => $emoney,
            'company' => $company,
        ]); ?>
        <?php if ($canCreate): ?>
            <?php if (Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE): ?>
                <button class="button-regular button-regular_padding_medium button-regular_red ml-auto button-clr">
                    <?= Icon::get('add-icon') . '<span>Добавить</span>' ?>
                </button>
            <?php else: ?>
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить</span>', [
                    'class' => "ajax-modal-btn button-regular button-regular_padding_medium button-regular_red ml-auto",
                    'title' => "Добавить операцию по e-money",
                    'data-pjax' => "0",
                    'data-title' => "Добавить операцию по e-money",
                    'data-url' => Url::to([
                        'create',
                        'type' => CashFlowsBase::FLOW_TYPE_INCOME,
                        'showForm' => true,
                        'id' => $emoney,
                    ]),
                ]) ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/modules/cash/views/default/_partial/table-filter', [
        'searchModel' => $model,
        'filterUrl' => ['index', 'emoney' => $emoney],
        'tableView' => [
            'attribute' => 'table_view_cash'
        ],
        'tableConfig' => [
            [
                'attribute' => 'emoney_column_income_expense',
                'invert_attribute' => 'invert_emoney_column_income_expense'
            ],
            [
                'attribute' => 'emoney_column_paying'
            ],
            [
                'attribute' => 'emoney_column_project'
            ],
            [
                'attribute' => 'emoney_column_sale_point'
            ],
            [
                'attribute' => 'emoney_column_industry'
            ],
            [
                'attribute' => 'cash_index_hide_plan',
                'style' => 'border-top: 1px solid #ddd',
                'refresh-page' => true
            ],
        ]
    ]) ?>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'emptyText' => ($model->getIsExists() ?
            'В указанном периоде нет операций по e-money ' :
            'Еще не загружена ни одна операция по e-money ').
            (($n = $model->getEmoneyName()) ? '"'.Html::encode($n).'"' : ''),
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading line-height-1em',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $typeCss = 'income-item';
                        $income = round($data['amount'] / 100, 2);
                        $expense = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $income : 0;
                    } else {
                        $typeCss = 'expense-item';
                        $expense = round($data['amount'] / 100, 2);
                        $income = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $expense : 0;
                    }

                    $copyUrl = ($data['tb'] == CashEmoneyFlows::tableName())
                        ? Url::to(['/cash/e-money/create-copy', 'id' => $data['id'], 'redirect' => Url::current()])
                        : Url::to(['/cash/plan/create-copy', 'id' => $data['id'], 'redirect' => Url::current()]);

                    return Html::checkbox("flowId[{$data['tb']}][]", false, [
                        'class' => 'joint-operation-checkbox ' . $typeCss,
                        'value' => $data['id'],
                        'data' => [
                            'income' => $income,
                            'expense' => $expense,
                            'copy-url' => $copyUrl
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amountIncomeExpense',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => $tabConfigClass['incomeExpense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $income = '+ ' . TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                    $expense = '- ' . TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);

                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        @list($from, $to) = explode('_', $flows['transfer_key']);
                        if ($from == $flows['id'])
                            return Html::tag('div', CashEmoneySearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        if ($to == $flows['id'])
                            return Html::tag('div', CashEmoneySearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                    } else {
                        if ($flows['amountIncome'] > 0) {
                            return Html::tag('div', CashEmoneySearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                        }
                        if ($flows['amountExpense'] > 0) {
                            return Html::tag('div', CashEmoneySearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        }
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'amountIncome',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => $tabConfigClass['income'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'amountExpense',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'contractor_ids',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['is_internal_transfer'])
                        return '';

                    if ($data['contractor_id'] > 0 && ($contractor = Contractor::findOne($data['contractor_id'])) !== null) {
                        return Html::a(Html::encode($contractor->nameWithType), [
                            '/contractor/view',
                            'type' => $contractor->type,
                            'id' => $contractor->id,
                        ], ['target' => '_blank', 'title' => $contractor->nameWithType]);
                    } else {

                        $model = ($data['wallet_id'] == 'plan')
                            ? PlanCashFlows::findOne($data['id'])
                            : CashEmoneyFlows::findOne($data['id']);

                        if ($model && $model->cashContractor)
                            return Html::tag('span', Html::encode($model->cashContractor->text),
                                ['title' => $model->cashContractor->text]);
                    }

                    return '---';
                },
                'filter' =>['' => 'Все контрагенты'] + $model->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '250px'
            ],
            [
                's2width' => '200px',
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => $tabConfigClass['project'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
                ],
                'filter' => ['' => 'Все проекты'] + $model->getProjectFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                        return Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => $tabConfigClass['salePoint'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
                ],
                'filter' => ['' => 'Все точки продаж'] + $model->getSalePointFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                        return Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => $tabConfigClass['companyIndustry'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
                ],
                'filter' => ['' => 'Все направления'] + $model->getCompanyIndustryFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                        return Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'purpose-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    if ($data['description']) {
                        $description = mb_substr($data['description'], 0, 50) . '<br>' . mb_substr($data['description'], 50, 50);

                        return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'billPaying',
                'label' => 'Опл. счета',
                'headerOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    switch ($data['tb']) {
                        case CashEmoneyFlows::tableName():
                            $model = CashEmoneyFlows::findOne($data['id']);
                            return '<div style="max-width:50px">' . ($model->credit_id ? $model->creditPaying : $model->billPaying) . '</div>';
                        case PlanCashFlows::tableName():
                            $model = PlanCashFlows::findOne($data['id']);
                            $currDate = date('Y-m-d');
                            if ($model->first_date < $currDate && $model->date != $model->first_date)
                                return 'Перенос';
                            elseif ($model->date >= $currDate)
                                return 'План';
                            elseif ($model->date < $currDate)
                                return 'Просрочен';
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'reason_ids',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'clause-cell',
                ],
                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function ($data) {

                    $reason = $data['flow_type'] == CashEmoneyFlows::FLOW_TYPE_INCOME ?
                        (($item = \common\models\document\InvoiceIncomeItem::findOne($data['income_item_id'])) ?
                            $item->fullName : "id={$data['income_item_id']}") :
                        (($item = \common\models\document\InvoiceExpenditureItem::findOne($data['expenditure_item_id'])) ?
                            $item->fullName : "id={$data['expenditure_item_id']}");

                    return $reason ? Html::tag('span', $reason, ['title' => htmlspecialchars($reason)]) : '-';

                },
                's2width' => '200px'
            ],
            [
                'class' => ActionColumn::class,
                'template' => '<div class="table-count-cell" style="min-width:60px">{update}{delete}</div>',
                'headerOptions' => [
                    'width' => '5%',
                    'style' => 'max-width:60px'
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'urlCreator' => function ($action, $data, $key, $index, $actionColumn) use ($foreign) {
                    if ($action == 'update' && $data['is_internal_transfer']) {
                        $action = 'update-internal';
                    }
                    $params = [
                        'id' => $data['id'],
                        'foreign' => $foreign,
                        'is_plan_flow' => ($data['wallet_id'] == 'plan') ? '1' : ''
                    ];
                    $params[0] = $actionColumn->controller ? $actionColumn->controller . '/' . $action : $action;

                    return Url::toRoute($params);
                },
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $options = [
                            'class' => 'button-clr link mr-1 update-flow-item',
                            'title' => 'Редактировать',
                            'data' => [
                                'title' => 'Редактировать операцию по e-money',
                                'pjax' => 0,
                                'toggle' => 'modal',
                                'target' => '#update-movement'
                            ],
                        ];

                        return Html::a(Icon::get('pencil'), $url, $options);
                    },
                    'delete' => function ($url) {
                        return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                            'theme' => 'gray',
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'button-clr link',
                                'tag' => 'button',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<div class="modal fade" id="add-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить операцию по e-money</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Редактировать операцию по e-money</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<?= $this->render('_partial/modal-add-contractor'); ?>
<?= $this->render('_partial/modal-many-delete'); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
    'foreign' => $foreign,
]); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-project'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-sale-point'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-company-industry'); ?>

<?= SummarySelectWidget::widget([
    'buttonsViewType' => ($canProjectUpdate || $canSalePointUpdate || $canIndustryUpdate) ? SummarySelectWidget::VIEW_DROPDOWN : SummarySelectWidget::VIEW_INLINE,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ]
]); ?>

<?php $this->registerJs('
$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "update-movement") {
        $(this).find(".modal-body").empty();
        $(this).find("#js-modal_update_title").empty();
    }
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(document).on("show.bs.modal", "#add-movement", function(e) {
    $(".alert-success").remove();
    var button = $(e.relatedTarget); console.log(button)
    var modal = $(this);
    if ($(button).attr("href")) {
        modal.find(".modal-body").load(button.attr("href"), function() {
            $("input", this).uniform("refresh");
        });
    }
});
$(document).on("shown.bs.modal", "#add-movement", function(e) {
    refreshDatepicker();
});

$(document).on("hide.bs.modal", "#add-movement", function(event) {
    if (event.target.id === "add-movement") {
        $("#add-movement .modal-body").empty();
    }
});


$(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    var l = Ladda.create($(this).find(".btn-save")[0]);
    var $hasError = false;

    if (l) {
        l.start();
    }
    $(".field-cashemoneysearch-incomeitemidmanyitem:visible, .field-cashemoneysearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        return false;
    }
});

// SHOW_AJAX_MODAL
$("#update-movement").on("show.bs.modal", function (e) {

    var button = $(e.relatedTarget);
    var modal = $(this);

    if ($(button).attr("href"))
        modal.find(".modal-body").load(button.attr("href"));
});

// REFRESH_UNIFORMS
$(document).on("shown.bs.modal", "#update-movement", function() {
    refreshUniform();
    refreshDatepicker();
});

// SEARCH
$("input#cashemoneysearch-contractor_name").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});

$(document).on("pjax:complete", "#add_emoney_flow_pjax", function() {
  refreshDatepicker();
});

refreshUniform();
refreshDatepicker();

//////////////////////////////
if (window.PlanModal)
    PlanModal.init();
//////////////////////////////

');