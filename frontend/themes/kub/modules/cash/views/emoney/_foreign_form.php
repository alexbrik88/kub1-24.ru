<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashEmoneyFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\date\DateHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\cash\models\CashContractorType;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

CashFormAsset::register($this);

$company = Yii::$app->user->identity->company;

if (empty($multiWallet)) {
    $multiWallet = false;
}
if (empty($isPlan)) {
    $isPlan = $isPlanRepeated = false;
    $stylePlanShow = $stylePlanRepeatedShow = 'display:none';
    $styleFactShow = '';
} else {
    $isPlanRepeated = $isPlan && ($model->is_repeated || $model->first_flow_id);
    $styleFactShow = 'display:none';
    $stylePlanShow = '';
    $stylePlanRepeatedShow = 'display:none';
    $model->date = date('d.m.Y', strtotime($model->date));
    $model->planEndDate = $model->date;
}

$emoneyData = $company->getEmoneys()->select('name')->orderBy([
    'is_closed' => SORT_ASC,
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->indexBy('id')->column();

$income = 'income' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');

switch ($model->flow_type) {
    case CashEmoneyFlows::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $contractorType = Contractor::TYPE_CUSTOMER;
        $contractorData = ['add-modal-contractor' =>  Icon::PLUS . ' <span class="bold">Добавить покупателя</span> '] +
            ArrayHelper::map(CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->all(), 'name', 'text');
        break;
    case CashEmoneyFlows::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $contractorType = Contractor::TYPE_SELLER;
        $contractorData = ['add-modal-contractor' => Icon::PLUS . ' <span class="bold">Добавить поставщика</span> '] +
            ArrayHelper::map(CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
            ->all(), 'name', 'text');
        break;

    default:
        $contractor = 'Контрагент';
        $contractorType = null;
        $contractorData = [];
        break;
}
$header = ($model->isNewRecord ? 'Добавить' : 'Редактировать') . ($multiWallet ? (($isPlan ? ' плановую' : '') . ' операцию') : ' операцию по e-money');

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

<?php Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::current(['showForm' => null]) : Url::current(),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'id' => 'cash-emoney-form',
    'options' => [
        'class' => 'form-horizontal cash_movement_form',
        'data' => [
            'foreign' => 1,
            'pjax' => true,
        ],
    ],
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= !$model->isNewRecord ? null : $this->render('/bank/_flow_plan_fact_select', ['date' => $model->date_input]) ?>

<?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
    <?= Html::hiddenInput('redirect', $redirect) ?>
<?php endif ?>

<?= Html::hiddenInput('is_plan_flow', $isPlan, ['id' => 'is_plan_flow']) ?>

<div class="row">
    <div class="col-6">
        <?= $this->render('/bank/_flow_type_select', [
            'is_create' => $model->isNewRecord && (!Yii::$app->request->get('originFlow')),
            'flow_type' => $model->flow_type,
            'input_name' => Html::getInputName($model, 'flow_type'),
            'label' => ($multiWallet) ? 'Добавить операцию' : null
        ]) ?>
    </div>
    <div class="col-6">
        <?php if ($multiWallet): ?>
            <?= $this->render('/bank/_account_select_multi', [
                'company' => $company,
                'model' => $model,
                'form' => $form
            ]) ?>
        <?php else: ?>
            <?= $this->render('_emoney_select', [
                'company' => $company,
                'model' => $model,
                'form' => $form,
                'emoneyData' => $emoneyData,
            ]) ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id', [
            'options' => [
                'class' => 'form-group cash-contractor_input',
            ],
        ])->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => $contractorType,
            'staticData' => $contractorData,
            'options' => [
                'id' => 'cashemoneyforeigncurrencyflows-contractor_id-' . $model->id,
                'class' => 'contractor-items-depend form-group cash_contractor_id_select ' . ($model->flow_type ? 'customer' : 'seller'),
                'placeholder' => '',
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label($contractor); ?>
    </div>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE): ?>
        <div class="col-6">
            <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'options' => [
                    'id' => 'cashemoneyforeigncurrencyflows-expenditure_item_id-' . $model->id,
                    'class' => 'flow-expense-items',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ]); ?>
        </div>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashemoneyforeigncurrencyflows-expenditure_item_id-',
        ]); ?>
    <?php endif; ?>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME): ?>
        <div class="col-6">
            <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'income' => true,
                'options' => [
                    'id' => 'cashemoneyforeigncurrencyflows-income_item_id-' . $model->id,
                    'class' => 'flow-income-items cash_income_item_id_select',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ]); ?>
        </div>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashemoneyforeigncurrencyflows-income_item_id-',
            'type' => 'income',
        ]); ?>
    <?php endif; ?>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'amount_input')->textInput([
            'value' => !empty($model->amount) ? $model->amount / 100 : '',
            'class' => 'form-control js_input_to_money_format',
        ]); ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'number', [
            'options' => ['class' => 'form-group'],
        ])->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'date_input')->textInput([
                    'class' => 'form-control date-picker ico',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                    'labelOptions' => [
                        'class' => 'label repeated-date-start'
                    ]
                ])->label('Дата оплаты'); ?>
            </div>
            <!-- FACT FLOW PART -->
            <div class="col-6 in-fact-flow-show" style="<?=($styleFactShow)?>">
                <?= $form->field($model, 'date_time')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '9{2}:9{2}:9{2}',
                    'options' => [
                        'class' => 'form-control',
                        'value' => ($model->date_time) ? date('H:i:s', strtotime($model->date_time)) : date('H:i:s')
                    ],
                ])->label('Время оплаты'); ?>
            </div>
            <!-- PLAN FLOW PART -->
            <?php if ($model->isNewRecord): ?>
                <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Плановая дата последняя</label>
                        <?= Html::input('text', 'PlanCashFlows[planEndDate]', date('d.m.Y'), [
                            'id' => 'flow_repeated_date_end',
                            'class' => 'form-control date-picker ico',
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php elseif ($isPlan && $model->date != DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d')): ?>
                <div class="col-6 in-plan-flow-show" style="<?=($stylePlanShow)?>">
                    <div class="form-group">
                        <label class="label repeated-date-end">Дата по первонач. плану</label>
                        <input class="form-control" disabled style="color:#e30611" value="<?= DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d') ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-6">
        <!-- FACT FLOW PART -->
        <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
            <div class="col-6">
                <?= $form->field($model, 'recognition_date_input', [
                    'inputTemplate' => $inputCalendarTemplate,
                    'options' => ['class' => 'form-group'],
                    'labelOptions' => [
                        'class' => 'label bold-text',
                        'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                'class' => 'flow-type-toggle ' . $income,
                            ]) . Html::tag('span', 'расхода', [
                                'class' => 'flow-type-toggle ' . $expense,
                            ]),
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                    'data' => [
                        'date-viewmode' => 'years',
                    ],
                    'disabled' => (bool)$model->is_prepaid_expense
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'is_prepaid_expense', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'padding-top: 36px'
                    ],
                ])->checkbox(); ?>
            </div>
        </div>
        <!-- PLAN FLOW PART -->
        <div class="row in-plan-flow-show pt-4" style="<?=($stylePlanShow)?>">
            <div class="col-12">
                <?= Html::checkbox('PlanCashFlows[is_repeated]', $isPlanRepeated, [
                    'id' => 'is_repeated_plan_flow',
                    'class' => 'form-group',
                    'label' => '<span class="label">Повторяющаяся плановая операция</span>',
                    'disabled' => !$model->isNewRecord
                ]) ?>
            </div>
        </div>
    </div>
    <!-- PLAN FLOW PART -->
    <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
        <div class="form-group">
            <label class="label repeated-date-end">Периодичность</label>
            <?= Select2::widget([
                'name' => 'PlanCashFlows[period]',
                'data' => \frontend\modules\analytics\models\PlanCashFlows::$periods,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>
    </div>
</div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<div class="in-fact-flow-show" style="<?=($styleFactShow)?>">
    <?= $form->field($model, 'is_accounting')->checkbox(); ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php $form->end(); ?>

<?php Pjax::end(); ?>
<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#update_emoney_flow_pjax").closest(".modal-content");
        if (modalContent) {
            modalContent.find(".modal-title").html("<?= $header ?>");
            // REFRESH_UNIFORMS
            refreshUniform();
            refreshDatepicker();
        }
    <?php endif ?>

    $(document).on("change", "#cashemoneyforeigncurrencyflows-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashemoneyforeigncurrencyflows-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });

    $('form [name$="[income_item_id]"], form [name$="[expenditure_item_id]"]').trigger('change');
</script>
