<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

$this->title = 'Добавить операцию по e-money';

?>

<div class="cash-order-flows-create">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company ?? Yii::$app->user->identity->company,
        'isClone' => isset($isClone),
        'isPlan' => $isPlan ?? false,
        'multiWallet' => $multiWallet ?? false,
    ]) ?>
</div>
