<?php

use common\models\currency\Currency;
use kartik\select2\Select2;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var array $emoneyData */

if (!isset($emoneyArray)) {
    $emoneyArray = $company->getEmoneys()->orderBy([
        'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
        'is_main' => SORT_DESC,
        'is_closed' => SORT_ASC,
        'name' => SORT_ASC,
    ])->indexBy('id')->all();
}

if (!isset($emoneyList)) {
    $emoneyList = ArrayHelper::map($emoneyArray, 'id', 'name');
}

$emoneyBalance = $company->getCashEmoneyFlows()
    ->select([
        'sum' => 'SUM( IF([[flow_type]] = 1, [[amount]], -[[amount]]) )',
        'emoney_id',
    ])
    ->groupBy('emoney_id')
    ->indexBy('emoney_id')
    ->column() +
    $company->getCashEmoneyForeignCurrencyFlows()
    ->select([
        'sum' => 'SUM( IF([[flow_type]] = 1, [[amount]], -[[amount]]) )',
        'emoney_id',
    ])
    ->groupBy('emoney_id')
    ->indexBy('emoney_id')
    ->column();

$emoneyOptions = [];
foreach ($emoneyArray as $emoney) {
    $emoneyOptions[$emoney->id] = [
        'data-balance' => ArrayHelper::getValue($emoneyBalance, $emoney->id, 0),
        'data-foreign' => $emoney->getIsForeign() ? 1 : 0,
        'data-currency' => $emoney->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $emoney->currency->name),
        'data-url' => Url::current([
            'type' => $model->flow_type,
            'id' => $emoney->id,
        ])
    ];
}

$jsTemplate = new yii\web\JsExpression(<<<JS
    function(data) {
        if (data.element) {
            const emoney_name = data.text;
            const balance = number_format(Number(data.element.dataset.balance) / 100, 2, ',', ' ');
            const currency_symbol = data.element.dataset.symbol;
            return '<div class="row">' +
                     '<div class="col-6 pr-1 no-overflow font-16">' + emoney_name + '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' + balance + ' ' + currency_symbol + '</div>' +
                   '</div>';
        }

        return data.text;
    }
JS
);
?>

<?= $form->field($model, 'emoney_id')->widget(Select2::classname(), [
    'data' => $emoneyData,
    'options' => [
        'class' => 'flow_form_rs_select',
        'id' => 'cashemoneyforeigncurrencyflows-emoney_id-' . $model->id,
        'placeholder' => '',
        'options' => $emoneyOptions,
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'templateResult' => $jsTemplate,
        'templateSelection' => $jsTemplate,
        'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
    ],
]); ?>

<div class="hidden">
    <?= Html::a('', Url::current(), [
        'class' => 'flow_form_rs_toggle_link currency_type_link',
    ]) ?>
</div>
