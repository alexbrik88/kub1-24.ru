<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashOrderFlows */

$this->title = 'Добавить операцию по кассе';

if (!isset($company)) {
    $company = Yii::$app->user->identity->company;
}
if (!isset($userCashboxArray)) {
    $userCashboxArray = Yii::$app->user->identity->getCashboxes()->with('currency')->orderBy([
        'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])->indexBy('id')->all();
}
?>

<div class="cash-order-flows-create">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company ?? Yii::$app->user->identity->company,
        'userCashboxArray' => $userCashboxArray ?? [],
        'isClone' => isset($isClone),
        'multiWallet' => $multiWallet ?? false
    ]) ?>
</div>
