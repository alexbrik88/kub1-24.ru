<?php
use frontend\modules\cash\controllers\OrderController;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var int $create_type */
/** @var int $flow_type */
/** @var int $cashbox_id */

$cssClass = $create_type == OrderController::TYPE_CREATE_MANY ? 'type_create_many' : '';
?>

<div class="select2-change-one-many-create-orders <?= $cssClass ?>">
    <?= Select2::widget([
        'id' => 'change-one-many-create-orders',
        'hideSearch' => true,
        'name' => 'change-one-many-create-orders',
        'data' => [
            OrderController::TYPE_CREATE_ONE  => '1 операция',
            OrderController::TYPE_CREATE_MANY => 'Несколько'
        ],
        'value' => $create_type,
        'pluginOptions' => [
            'width' => '150px'
        ],
        'options' => [
            'data-url1' => Url::to(['create', 'type' => $flow_type, 'id' => $cashbox_id]),
            'data-url2' => Url::to(['many-create', 'type' => $flow_type, 'id' => $cashbox_id]),
            'onchange' => new JsExpression('$.pjax({url: $(this).data("url" + $(this).val()), push: false, container: "#update-movement-pjax"});')
        ]
    ]) ?>
</div>
