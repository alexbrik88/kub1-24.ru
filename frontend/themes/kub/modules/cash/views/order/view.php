<?php
/**
 * @var \yii\web\View $this
 * @var CashOrderFlows $model
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\rbac\permissions\Cash;
use frontend\rbac\UserRole;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use php_rutils\RUtils;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = RUtils::dt()->ruStrFTime([
    'date' => $model->date,
    'format' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? 'Приходный' : 'Расходный') . " кассовый ордер №{$model->number} от j F Y г.",
    'monthInflected' => true,
    'preposition' => true,]);

$backUrl = ['index', 'cashbox' => $model->cashbox_id];

?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, ['class' => 'link mb-2']); ?>
<?php endif; ?>

<!-- KUB -->
<div class="wrap wrap_padding_small pl-4 pr-3 pb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="font-size:20px;font-weight:700;"><?= Html::encode($this->title); ?></h4>
                        <div class="column">
                            <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                    type="button"
                                    data-toggle="modal"
                                    title="Последние действия"
                                    href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/img/svg/svgSprite.svg#info"></use>
                                </svg>
                            </button>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE)): ?>
                                <?= Html::a(Icon::get('pencil'), [
                                    '/cash/order/update',
                                    'id' => $model->id,
                                    'foreign' => $model->isForeign ? 1 : null,
                                ], [
                                    'class' => 'ajax-modal-btn update-order-btn button-list button-hover-transparent button-clr mb-2 ml-1',
                                    'data-pjax' => 0,
                                ]) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Сумма</div>
                            <div><?= TextHelper::invoiceMoneyFormat($model->amount, 2, '.', ''); ?></div>
                        </div>
                        <div class="col-6 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Контрагент</div>
                            <div><?= $model->contractorText; ?></div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-3 mb-4 pb-2">
                                    <div class="label weight-700 mb-3">Основание</div>
                                    <div>
                                        <?php
                                        echo $model->reason ? $model->reason->name : '—';
                                        if ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER) {
                                            echo ': ' . $model->other;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 mb-4 pb-2">
                                    <div class="label weight-700 mb-3">Приложение</div>
                                    <div><?= $model->application; ?></div>
                                </div>
                                <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE): ?>
                                    <div class="col-3 mb-4 pb-2">
                                        <div class="label weight-700 mb-3">Статья расхода</div>
                                        <div><?= $model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? $model->expenditureReason->name : '(не задано)'; ?></div>
                                    </div>
                                <?php else: ?>
                                    <div class="col-3 mb-4 pb-2">
                                        <div class="label weight-700 mb-3">Статья прихода</div>
                                        <div><?= $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ?
                                                ($model->incomeReason ? $model->incomeReason->name : '(не задано)') : '(не задано)'; ?></div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-3 mb-4 pb-2">
                                    <div class="label weight-700 mb-3">Учитывать в бухгалтерии</div>
                                    <div><?= $model->is_accounting ? 'Да' : 'Нет'; ?></div>
                                </div>
                                <?php if ($model->company->getCashboxes()->count() > 1) : ?>
                                <div class="col-3 mb-4 pb-2">
                                    <div class="label weight-700 mb-3">Касса</div>
                                    <div><?= $model->cashbox ? $model->cashbox->name : '---'; ?></div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <!-- sidebar -->
                <?php $color = '#26cd58'; ?>
                <div class="sidebar-title d-flex flex-wrap align-items-center">
                    <strong class="column mb-2 mb-xl-3 doc-status-price">
                        <?= TextHelper::invoiceMoneyFormat($model->amount, 2); ?>
                        <?= $model->currencySymbol ?>
                    </strong>
                    <div class="column flex-grow-1 mt-1 mt-xl-0" >
                        <div class="button-regular mb-3 w-100" style="
                                background-color: <?=$color?>;
                                border-color: <?=$color?>;
                                color: #ffffff;
                                ">
                            <?= $this->render('//svg-sprite', ['ico' => 'check']) ?>
                            <span><?= "Оплачено" ?></span>
                        </div>
                    </div>
                </div>
                <div class="about-card mb-3 mt-1 pl-3 pr-3">
                    <div class="about-card-item pb-1 pl-1 pr-1">
                        <span class="text-grey weight-700"><?= ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) ? 'Покупатель' : 'Продавец' ?>:</span>
                        <br>
                        <?php if ($model->contractor): ?>
                            <?= Html::a($model->contractor->getShortName(), [
                                '/contractor/view',
                                'type' => $model->contractor->type,
                                'id' => $model->contractor->id,
                            ], [
                                'class' => 'link mt-1',
                            ]) ?>
                        <?php else: ?>
                            <?= $model->contractorText; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'filename' => $model->getPrintTitle(),]; ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', $printUrl, [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php $pdfUrl = Url::to(['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),]); ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'pdf']).'<span>PDF</span>', $pdfUrl, [
                    'target' => '_blank',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE)): ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>', [
                    'class' => "ajax-modal-btn button-clr button-regular button-width button-hover-transparent",
                    'data-pjax' => "0",
                    'data-url' => Url::to([
                        'create-copy',
                        'id' => $model->id,
                        'foreign' => $model->isForeign ? 1 : null,
                    ]),
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::DELETE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id,]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить кассовый ордер?',
            ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

<div class="created-by">
    <?= date('d.m.Y', $model->created_at) ?>
    Создал
    <?= $model->author ? $model->author->fio : ''; ?>
</div>

<?php Modal::end(); ?>
