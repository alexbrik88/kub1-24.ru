<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\employee\EmployeeRole;
use common\models\project\Project;
use frontend\components\Icon;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\telegram\widgets\CodeButtonWidget;
use frontend\modules\telegram\widgets\CodeModalWidget;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\XlsHelper;
use frontend\themes\kub\assets\CashModalAsset;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;
use frontend\modules\cash\models\CashOrderSearch;

CashModalAsset::register($this);

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashOrderFlows
 * @var $company \common\models\Company
 * @var $showImportModal integer
 */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;

if (empty($foreign)) {
    $foreign = null;
}

$employeeRoleId = Yii::$app->user->identity->employee_role_id;
$canManager = (EmployeeRole::getRoleName($employeeRoleId) === UserRole::ROLE_MANAGER);

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate && Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate && CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

$currentCashboxId = $currentCashboxId ?? (array)$model->cashbox_id;

$pageRoute = ['/cash/order/index'];

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_cash');
$tabConfigClass = [
    'billPaying' => 'col_order_column_paying' . ($userConfig->order_column_paying ? '' : ' hidden'),
    'incomeExpense' => 'col_order_column_income_expense' . ($userConfig->order_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_order_column_income_expense'  . (!$userConfig->order_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_order_column_income_expense' . (!$userConfig->order_column_income_expense ? '' : ' hidden'),
    'project' => 'col_order_column_project' . ($userConfig->order_column_project ? '' : ' hidden'),
    'salePoint' => 'col_order_column_sale_point' . ($userConfig->order_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_order_column_industry' . ($userConfig->order_column_industry ? '' : ' hidden'),
];

$moreItems = [];
if (!$canManager && Yii::$app->user->can(permissions\CashOrder::CREATE)) {
    $moreItems[] = [
        'label' => 'из ОФД',
        'url' => [
            '/cash/ofd/default/index',
            'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
            'cashbox_id' => reset($currentCashboxId)
        ],
        'linkOptions' => [
            'data-toggle' => 'modal',
            'class' => 'ofd-module-open-link text-center',
            'disabled' => true
        ]
    ];
    $moreItems[] = [
        'label' => 'из Excel',
        'url' => '#import-xls',
        'linkOptions' => [
            'data-toggle' => 'modal',
            'class' => 'text-center',
        ]
    ];
}
$accountsFilterItems = $model->getAccountsFilterItems();
?>

<div class="stop-zone-for-fixed-elems  cash-order-flows-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <?= \frontend\themes\kub\widgets\CashboxAccountFilterWidget::widget([
            'pageTitle' => $this->title,
            'cashbox' => $cashbox,
            'company' => $company,
        ]); ?>
        <?php if ($canCreate): ?>
            <?= Html::button(Icon::get('add-icon') . '<span>Добавить</span>', [
                'class' => "ajax-modal-btn button-regular button-regular_padding_medium button-regular_red ml-auto button-clr add-operation",
                'title' => "Добавить операцию по кассе",
                'data-title' => "Добавить операцию по кассе",
                'data-pjax' => "0",
                'data-url' => Url::to([
                    'create',
                    'type' => CashOrderFlows::FLOW_TYPE_INCOME,
                    'id' => reset($currentCashboxId),
                ]),
            ]) ?>
        <?php endif; ?>
    </div>

    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if (count($moreItems) > 0) : ?>
                    <div class="dropdown">
                        <?= Html::a('<span>Импорт</span>' . Icon::get('shevron', [
                                'class' => 'svg-icon_grey svg-icon_shevron svg-icon_absolute',
                            ]), '#', [
                            'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => 'false'
                        ]); ?>

                        <?= Dropdown::widget([
                            'options' => [
                                'class' => 'dropdown-menu form-filter-list list-clr w-100'
                            ],
                            'items' => $moreItems
                        ]); ?>
                    </div>
                <?php endif; ?>
                <?php if (!$canManager && Yii::$app->user->can(permissions\CashOrder::CREATE)) {
                    echo CodeButtonWidget::widget(['options' => [
                        'label' => CodeButtonWidget::isActive() ? 'Telegram бот' : 'Telegram бот включен',
                        'icon' => '/img/telegram/logo.svg'
                    ]]);
                } ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/modules/cash/views/default/_partial/table-filter', [
        'searchModel' => $model,
        'filterUrl' => ['index', 'cashbox' => $cashbox],
        'tableView' => [
            'attribute' => 'table_view_cash'
        ],
        'tableConfig' => [
            [
                'attribute' => 'order_column_income_expense',
                'invert_attribute' => 'invert_order_column_income_expense'
            ],
            [
                'attribute' => 'order_column_paying'
            ],
            [
                'attribute' => 'order_column_project'
            ],
            [
                'attribute' => 'order_column_sale_point'
            ],
            [
                'attribute' => 'order_column_industry'
            ],
            [
                'attribute' => 'cash_index_hide_plan',
                'style' => 'border-top: 1px solid #ddd',
                'refresh-page' => true
            ],
        ]
    ]) ?>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'emptyText' => ($model->getIsExists() ?
            'В указанном периоде нет операций по кассе ' :
            'Еще не загружена ни одна операция по кассе ').
            (($n = $model->getCashboxName()) ? '"'.Html::encode($n).'"' : ''),
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => [
            'class' => 'heading line-height-1em',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => '',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $typeCss = 'income-item';
                        $income = round($data['amount'] / 100, 2);
                        $expense = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $income : 0;
                    } else {
                        $typeCss = 'expense-item';
                        $expense = round($data['amount'] / 100, 2);
                        $income = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $expense : 0;
                    }

                    $copyUrl = ($data['tb'] == CashOrderFlows::tableName())
                        ? Url::to(['/cash/order/create-copy', 'id' => $data['id'], 'redirect' => Url::current()])
                        : Url::to(['/cash/plan/create-copy', 'id' => $data['id'], 'redirect' => Url::current()]);

                    return Html::checkbox("flowId[{$data['tb']}][]", false, [
                        'class' => 'joint-operation-checkbox ' . $typeCss,
                        'value' => $data['id'],
                        'data' => [
                            'income' => $income,
                            'expense' => $expense,
                            'copy-url' => $copyUrl
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amountIncomeExpense',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => $tabConfigClass['incomeExpense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
                ],
                'format' => 'raw',
                'value' => function ($flows) {

                    $income = '+ ' . TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                    $expense = '- ' . TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);

                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        @list($from, $to) = explode('_', $flows['transfer_key']);
                        if ($from == $flows['id'])
                            return Html::tag('div', CashOrderSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        if ($to == $flows['id'])
                            return Html::tag('div', CashOrderSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                    } else {
                        if ($flows['amountIncome'] > 0) {
                            return Html::tag('div', CashOrderSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']);
                        }
                        if ($flows['amountExpense'] > 0) {
                            return Html::tag('div', CashOrderSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                        }
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'amountIncome',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => $tabConfigClass['income'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'amountExpense',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => $tabConfigClass['expense'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE)
                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                        : '-';
                },
            ],
            [
                'attribute' => 'cashbox_id',
                'label' => 'Касса',
                'format' => 'html',
                'value' => function ($data) use ($accountsFilterItems) {
                    return $accountsFilterItems[$data['cashbox_id']] ?? '---';
                },
                'filter' => ['' => 'Все кассы'] + $accountsFilterItems,
                's2width' => '250px',
                'hideSearch' => false,
                'visible' => $cashbox == 'all' || in_array($cashbox, $foreignItems),
            ],
            [
                'attribute' => 'contractor_ids',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['is_internal_transfer'])
                        return '';

                    if ($data['contractor_id'] > 0 && ($contractor = Contractor::findOne($data['contractor_id'])) !== null) {
                        return Html::a(Html::encode($contractor->nameWithType), [
                            '/contractor/view',
                            'type' => $contractor->type,
                            'id' => $contractor->id,
                        ], ['target' => '_blank', 'title' => $contractor->nameWithType]);
                    } else {

                        $model = ($data['wallet_id'] == 'plan')
                            ? PlanCashFlows::findOne($data['id'])
                            : CashOrderFlows::findOne($data['id']);

                        if ($model && $model->cashContractor)
                            return Html::tag('span', Html::encode($model->cashContractor->text),
                                ['title' => $model->cashContractor->text]);
                    }

                    return '---';
                },
                'filter' => ['' => 'Все контрагенты'] + $model->getContractorFilterItems(),
                'hideSearch' => false,
                's2width' => '250px'
            ],
            [
                's2width' => '200px',
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => $tabConfigClass['project'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
                ],
                'filter' => ['' => 'Все проекты'] + $model->getProjectFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                        return Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => $tabConfigClass['salePoint'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
                ],
                'filter' => ['' => 'Все точки продаж'] + $model->getSalePointFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                        return Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                    }
                    return '';
                },
            ],
            [
                's2width' => '200px',
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => $tabConfigClass['companyIndustry'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
                ],
                'filter' => ['' => 'Все направления'] + $model->getCompanyIndustryFilterItems(),
                'format' => 'raw',
                'value' => function ($flows) {
                    if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                        return Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '20%',
                ],
                'contentOptions' => [
                    'class' => 'purpose-cell',
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    if ($data['description']) {
                        $description = mb_substr($data['description'], 0, 50) . '<br>' . mb_substr($data['description'], 50, 50);

                        return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'billPaying',
                'label' => 'Опл. счета',
                'headerOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => $tabConfigClass['billPaying'],
                ],
                'format' => 'raw',
                'value' => function ($data) {

                    switch ($data['tb']) {
                        case CashOrderFlows::tableName():
                            $model = CashOrderFlows::findOne($data['id']);
                            return '<div style="max-width:50px">' . ($model->credit_id ? $model->creditPaying : $model->billPaying) . '</div>';
                        case PlanCashFlows::tableName():
                            $model = PlanCashFlows::findOne($data['id']);
                            $currDate = date('Y-m-d');
                            if ($model->first_date < $currDate && $model->date != $model->first_date)
                                return 'Перенос';
                            elseif ($model->date >= $currDate)
                                return 'План';
                            elseif ($model->date < $currDate)
                                return 'Просрочен';
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'reason_ids',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '10%',
                    'style' => 'max-width:50px'
                ],
                'contentOptions' => [
                    'class' => 'clause-cell',
                ],
                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function ($data) {

                    $reason = $data['flow_type'] == CashOrderFlows::FLOW_TYPE_INCOME ?
                        (($item = \common\models\document\InvoiceIncomeItem::findOne($data['income_item_id'])) ?
                            $item->fullName : "id={$data['income_item_id']}") :
                        (($item = \common\models\document\InvoiceExpenditureItem::findOne($data['expenditure_item_id'])) ?
                            $item->fullName : "id={$data['expenditure_item_id']}");

                    return $reason ? Html::tag('span', $reason, ['title' => htmlspecialchars($reason)]) : '-';

                },
                's2width' => '200px'
            ],
            [
                'attribute' => 'number',
                'label' => 'Ордер',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '7%',
                    'style' => 'max-width:50px'
                ],
                'format' => 'raw',
                'value' => function ($data) use ($foreign) {

                    if ($data['tb'] == PlanCashFlows::tableName())
                        return;

                    return Html::a(($data['flow_type'] == CashOrderFlows::FLOW_TYPE_INCOME ? 'ПКО' : 'РКО') . ' №' . $data['number'],
                        ['view', 'id' => $data['id'], 'foreign' => $foreign]);
                },
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '2%',
                ],
                'contentOptions' => [
                    'class' => 'text-nowrap',
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'urlCreator' => function ($action, $data, $key, $index, $actionColumn) use ($foreign) {
                    if ($action == 'update' && $data['is_internal_transfer']) {
                        $action = 'update-internal';
                    }
                    $params = [
                        'id' => $data['id'],
                        'foreign' => $foreign,
                        'is_plan_flow' => ($data['wallet_id'] == 'plan') ? '1' : ''
                    ];
                    $params[0] = $actionColumn->controller ? $actionColumn->controller . '/' . $action : $action;

                    return Url::toRoute($params);
                },
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $options = [
                            'class' => 'button-clr link mr-1 update-flow-item',
                            'title' => 'Редактировать',
                            'data' => [
                                'title' => 'Редактировать операцию по кассе',
                                'pjax' => 0,
                                'toggle' => 'modal',
                                'target' => '#update-movement'
                            ],
                        ];

                        return Html::a(Icon::get('pencil'), $url, $options);
                    },
                    'delete' => function ($url) {
                        return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                            'theme' => 'gray',
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'button-clr link',
                                'tag' => 'button',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<?= SummarySelectWidget::widget([
    'buttonsViewType' => SummarySelectWidget::VIEW_DROPDOWN,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'cashbox']).' <span>Кассу</span>', '#many-cashbox', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ]
]); ?>

<?php if ($showImportModal): ?>
    <?php $this->registerJs('
        $("#import-xls").modal();
    '); ?>
<?php endif; ?>

<?= $this->render('_partial/modal-add-contractor'); ?>
<?= $this->render('_partial/modal-many-delete'); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
    'foreign' => $foreign,
]); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-project'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-sale-point'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-cashbox'); ?>
<?= $this->render('@frontend/modules/cash/views/default/_modal/modal-many-company-industry'); ?>

<div class="modal fade" id="update-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Редактировать операцию по кассе</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php if (!$canManager && Yii::$app->user->can(permissions\CashOrder::CREATE)): ?>
    <?= CodeModalWidget::widget([
        'title' => 'Добавлять операции через Telegram',
        'viewFile' => 'code-cashbox-modal-content'
    ]) ?>
    <?= \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget::widget([
        'pageTitle' => $this->title,
        'pageUrl' => Url::to($pageRoute),
    ]) ?>
    <?= $this->render('//xls/_import_xls', [
        'header' => 'Загрузка операций по кассе из Excel',
        'text' => '<p>Для загрузки списка операций по кассе из Excel, <br> заполните шаблон таблицы и загрузите ее тут. </p>',
        'formData' => Html::hiddenInput('className', 'CashOrderFlows') . Html::hiddenInput('cashbox', reset($currentCashboxId)),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_ORDER]),
    ]); ?>
<?php endif; ?>

<?php $this->registerJs('
    $(document).on("show.bs.modal", "#update-movement", function(event) {
        $(".alert-success").remove();
        if (event.target.id === "update-movement") {
            $(this).find(".modal-body").empty();
            $(this).find("#js-modal_update_title").empty();
        }
    });
    
    $(document).on("hide.bs.modal", "#update-movement", function(event) {
        if (event.target.id === "update-movement") {
            $("#update-movement .modal-body").empty();
        }
    });
    $("#update-movement").on("show.bs.modal", function (e) {

        var button = $(e.relatedTarget);
        var modal = $(this);
    
        if ($(button).attr("href"))
            modal.find(".modal-body").load(button.attr("href"));
    });

    $(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    var l = Ladda.create($(this).find(":submit")[0]);
    var $hasError = false;

    l.start();
    $(".field-cashordersearch-incomeitemidmanyitem:visible, .field-cashordersearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        l.stop();
        return false;
    }
});
$( document ).ajaxComplete(function() {
    Ladda.stopAll();
});

$(document).on("pjax:complete", function(event) {
    $("#cash-bank-flows-pjax-container input[type=radio]:not(.md-radiobtn)").uniform();
});

// REFRESH_UNIFORMS
$(document).on("shown.bs.modal", "#ajax-modal-box", function() {
    refreshUniform();
    refreshDatepicker();
});

// SEARCH
$("input#cashordersearch-contractor_name").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});

$(document).on("pjax:complete", "#ofd_module_pjax", function(event) {
    // REFRESH_UNIFORMS
    refreshUniform();
    refreshDatepicker();
    if ($("#kktRegNumber").length) {
        createSimpleSelect2("kktRegNumber");
    }
});

/* VIDIMUS BUTTON */
$(document).on(\'mouseover\', \'input[name="uploadfile"]\', function() {
    $(\'.add-xls-file-2\').css({\'color\': \'#335A82\', \'cursor\':\'pointer\'});
});
$(document).on(\'mouseout\', \'input[name="uploadfile"]\', function() {
    $(\'.add-xls-file-2\').css(\'color\', \'#001424\');
});

//////////////////////////////
if (window.PlanModal)
    PlanModal.init();
//////////////////////////////

//////////////////////////////
if (window.ManyCreateModal)
    ManyCreateModal.init();
//////////////////////////////
');

if (Yii::$app->request->get('show_add_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            let url = $(".add-operation").data("url");
            $("#ajax-modal-content").load(url, function() {
                $("#ajax-modal-box").modal("show");
                window.history.pushState("object", document.title, location.href.split("?")[0]);
            });
        });
    ');
}
if (Yii::$app->request->get('show_xls_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            $("#import-xls").modal("show");
            window.history.pushState("object", document.title, location.href.split("?")[0]);
        });
    ');
}
?>