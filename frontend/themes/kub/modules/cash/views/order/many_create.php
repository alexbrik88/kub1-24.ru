<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $_controlModel common\models\cash\CashOrderFlows  */
/* @var $models common\models\cash\CashOrderFlows[] */
/* @var $foreign int */

$this->title = 'Добавить движение по кассе';

if (!isset($company)) {
    $company = Yii::$app->user->identity->company;
}
if (!isset($userCashboxArray)) {
    $userCashboxArray = Yii::$app->user->identity->getCashboxes()->with('currency')->orderBy([
        'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])->indexBy('id')->all();
}
?>

<div class="cash-order-flows-create">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>
    <?= $this->render('_many_create_form', [
        '_controlModel' => $_controlModel,
        'models' => $models,
        'company' => $company ?? Yii::$app->user->identity->company,
        'userCashboxArray' => $userCashboxArray,
        'isClone' => isset($isClone),
        'multiWallet' => $multiWallet ?? false,
        'foreign' => $foreign
    ]) ?>
</div>
