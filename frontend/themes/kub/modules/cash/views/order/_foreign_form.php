<?php

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\project\Project;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\cash\controllers\OrderController;
use common\models\project\ProjectSearch;

CashFormAsset::register($this);

$company = Yii::$app->user->identity->company;

if (empty($multiWallet)) {
    $multiWallet = false;
}

if (empty($isPlan)) {
    $isPlan = $isPlanRepeated = false;
    $stylePlanShow = $stylePlanRepeatedShow = 'display:none';
    $styleFactShow = '';
} else {
    $isPlanRepeated = $isPlan && ($model->is_repeated || $model->first_flow_id);
    $styleFactShow = 'display:none';
    $stylePlanShow = '';
    $stylePlanRepeatedShow = 'display:none';
    $model->date = date('d.m.Y', strtotime($model->date));
    $model->planEndDate = $model->date;
}

$userCashboxList = ArrayHelper::map($userCashboxArray, 'id', 'name');

$contractorData = [
    'add-modal-contractor' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) ?
        (Icon::PLUS . ' <span class="bold">Добавить поставщика</span> ') :
        (Icon::PLUS . ' <span class="bold">Добавить покупателя</span> ') ,
];

if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $contractorData = array_merge($contractorData, ArrayHelper::map(CashContractorType::find()
        ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
        ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
        ->all(), 'name', 'text'));
} else {
    $contractorData = array_merge($contractorData, ArrayHelper::map(CashContractorType::find()
        ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
        ->all(), 'name', 'text'));
}

switch ($model->flow_type) {
    case CashOrderFlows::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $contractorType = Contractor::TYPE_CUSTOMER;
        $numberLabel = 'Номер ПКО';
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_INCOME);
        break;
    case CashOrderFlows::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $contractorType = Contractor::TYPE_SELLER;
        $numberLabel = 'Номер РКО';
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_EXPENSE);
        break;

    default:
        $contractor = 'Контрагент';
        $contractorType = null;
        $numberLabel = 'Номер';
        $reasonTypeArray = [];
        $contractorData = [];
        break;
}

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($contractorData as $key=>$seller) {

        if ('add-modal-contractor' == $key)
            continue;

        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($contractorData[$key]);
        else
            $model->contractor_id = $key;
    }
}

if ($hasProject = Yii::$app->user->identity->menuItem->project_item) {
}

$income = 'income' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Редактировать')) . ($multiWallet ? (($isPlan ? ' плановую' : '') . ' операцию') : ' операцию по кассе');

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$multiWallet = $multiWallet ?? false;
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?php if ($model->isNewRecord && !$multiWallet && empty($isClone)): ?>
    <?= $this->render('_change_one_many_create', [
        'create_type' => OrderController::TYPE_CREATE_ONE,
        'flow_type' => $model->flow_type,
        'cashbox_id' => $model->cashbox_id
    ]) ?>
<?php endif; ?>

<?php $form = ActiveForm::begin([
    'id' => 'cash-order-form',
    'action' => empty($action) ? '' : $action,
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'data-foreign' => 1,
        'class' => 'cash_movement_form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <?= !$model->isNewRecord ? null : $this->render('/bank/_flow_plan_fact_select', ['date' => $model->date_input]) ?>

    <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>
    <?php endif ?>

    <?= Html::hiddenInput('is_plan_flow', $isPlan, ['id' => 'is_plan_flow']) ?>

    <div class="row">
        <div class="col-6">
            <?= $this->render('/bank/_flow_type_select', [
                'is_create' => $model->isNewRecord && (!Yii::$app->request->get('originFlow')),
                'flow_type' => $model->flow_type,
                'input_name' => Html::getInputName($model, 'flow_type'),
                'label' => ($multiWallet) ? 'Добавить операцию' : null
            ]) ?>
        </div>
        <div class="col-6">
            <?php if ($multiWallet): ?>
                <?= $this->render('/bank/_account_select_multi', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form
                ]) ?>
            <?php elseif (count($userCashboxList) == 1) : ?>
                <?= $form->field($model, 'cashbox_id')->hiddenInput(['value' => key($userCashboxList)])->label(false) ?>
            <?php else : ?>
                <?= $this->render('_cashbox_select', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form,
                    'foreign' => 1,
                    'userCashboxArray' => $userCashboxArray,
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'contractor_id', [
                'options' => [
                    'class' => 'form-group cash-contractor_input',
                ],
            ])->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => $contractorType,
                'staticData' => $contractorData,
                'options' => [
                    'placeholder' => '',
                    'class' => 'contractor-items-depend cash_contractor_id_select ' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? 'seller' : 'customer'),
                    'data' => [
                        'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                    ],
                ],
            ])->label($contractor); ?>
        </div>

        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'expenditure_item_id', [
                        'enableClientValidation' => false,
                        'options' => [
                            'class' => 'form-group',
                            'jsValue' => $model->expenditure_item_id
                        ],
                    ])->widget(Select2::class, [
                        'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                                    ->where(['id' => [28, 46, 48, 53]])
                                    ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                                    ->all(), 'id', 'name'),
                        'options' => [
                            'class' => 'flow-expense-items',
                            'prompt' => '--',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]) ?>
                </div>
            <?php endif ?>
        <?php else: ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'expenditure_item_id', [
                        'enableClientValidation' => false,
                        'options' => [
                        'class' => 'form-group',
                            'jsValue' => $model->expenditure_item_id
                        ],
                    ])->widget(ExpenditureDropdownWidget::class, [
                        'loadAssets' => false,
                        'options' => [
                            'class' => 'flow-expense-items',
                            'prompt' => '--',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]) ?>
                </div>
            <?php endif ?>

            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashorderforeigncurrencyflows-expenditure_item_id',
            ]); ?>
        <?php endif; ?>

        <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) : ?>
            <div class="col-6">
                <?= $form->field($model, 'income_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group',
                        'jsValue' => $model->income_item_id
                    ],
                ])->widget(ExpenditureDropdownWidget::class, [
                    'loadAssets' => false,
                    'income' => true,
                    'options' => [
                        'class' => 'flow-income-items cash_income_item_id_select',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
        <?php endif ?>

        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashorderforeigncurrencyflows-income_item_id',
            'type' => 'income',
        ]); ?>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'amount_input')->textInput([
                'class' => 'form-control js_input_to_money_format',
            ]); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'number', [
                'options' => ['class' => 'form-group'],
            ])->textInput()->label($numberLabel); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'date_input', [
                        'inputTemplate' => $inputCalendarTemplate,
                        'options' => [
                            'class' => 'form-group'
                        ],
                        'labelOptions' => [
                            'class' => 'label repeated-date-start'
                        ]
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                    ]); ?>
                </div>
                <!-- PLAN FLOW PART -->
                <?php if ($model->isNewRecord): ?>
                    <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
                        <div class="form-group">
                            <label class="label repeated-date-end">Плановая дата последняя</label>
                            <?= Html::input('text', 'PlanCashFlows[planEndDate]', date('d.m.Y'), [
                                'id' => 'flow_repeated_date_end',
                                'class' => 'form-control date-picker ico',
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                            ]) ?>
                        </div>
                    </div>
                <?php elseif ($isPlan && $model->date != DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d')): ?>
                    <div class="col-6 in-plan-flow-show" style="<?=($stylePlanShow)?>">
                        <div class="form-group">
                            <label class="label repeated-date-end">Дата по первонач. плану</label>
                            <input class="form-control" disabled style="color:#e30611" value="<?= DateHelper::format($model->first_date, 'd.m.Y', 'Y-m-d') ?>">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-6">
            <!-- FACT FLOW PART -->
            <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
                <div class="col-6">
                    <?= $form->field($model, 'recognition_date_input', [
                        'inputTemplate' => $inputCalendarTemplate,
                        'options' => ['class' => 'form-group'],
                        'labelOptions' => [
                            'class' => 'label bold-text',
                            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                    'class' => 'flow-type-toggle ' . $income,
                                ]) . Html::tag('span', 'расхода', [
                                    'class' => 'flow-type-toggle ' . $expense,
                                ]),
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                        'disabled' => (bool)$model->is_prepaid_expense
                    ]); ?>

                </div>
                <div class="col-6">
                    <?= $form->field($model, 'is_prepaid_expense', [
                        'options' => [
                            'class' => 'form-group',
                            'style' => 'padding-top: 36px'
                        ],
                    ])->checkbox(); ?>
                </div>
            </div>
            <!-- PLAN FLOW PART -->
            <div class="row in-plan-flow-show pt-4" style="<?=($stylePlanShow)?>">
                <div class="col-12">
                    <?= Html::checkbox('PlanCashFlows[is_repeated]', $isPlanRepeated, [
                        'id' => 'is_repeated_plan_flow',
                        'class' => 'form-group',
                        'label' => '<span class="label">Повторяющаяся плановая операция</span>',
                        'disabled' => !$model->isNewRecord
                    ]) ?>
                </div>
            </div>
        </div>
        <!-- PLAN FLOW PART -->
        <div class="col-6 in-plan-flow-show-repeated" style="<?=($stylePlanRepeatedShow)?>">
            <div class="form-group">
                <label class="label repeated-date-end">Периодичность</label>
                <?= Select2::widget([
                    'name' => 'PlanCashFlows[period]',
                    'data' => \frontend\modules\analytics\models\PlanCashFlows::$periods,
                    'hideSearch' => true,
                    'pluginOptions' => ['width' => '100%'],
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea([
        'style' => 'resize: none;',
        'rows' => '2',
    ]); ?>

    <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
        <div class="col-6">
            <?= $form->field($model, 'reason_id')->widget(Select2::class, [
                'data' => ArrayHelper::map($reasonTypeArray, 'id', 'name'),
                'options' => [
                    'placeholder' => 'Необходимо для печатного документа',
                    'jsValue' => $model->reason_id,
                    'data-related-target' => '#other-reason-collapse',
                    'data-related-value' => CashOrdersReasonsTypes::VALUE_OTHER,
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'application')->textInput([
                'placeholder' => 'Необходимо для печатного документа',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
        <?php $show = $model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER ? 'show' : '' ?>
        <div id="other-reason-collapse" class="col-6 collapse <?= $show ?>">
            <?= $form->field($model, 'other')->textInput(); ?>
        </div>
    </div>

    <?php if ($hasProject): ?>
        <div class="row in-fact-flow-show" style="<?=($styleFactShow)?>">
            <div class="col-6">
                <?= $form->field($model, 'project_id')
                    ->widget(Select2::class, [
                        'data' => ProjectSearch::getSelect2Data($model->project_id),
                        'options' => [
                            'placeholder' => '',
                            'class' => 'flow-project-id'
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])
                    ->label('Проект'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="in-fact-flow-show" style="<?=($styleFactShow)?>">
        <?= $form->field($model, 'is_accounting')->checkbox(); ?>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#update-movement-pjax").closest(".modal-content");
        if (modalContent) {
            $(modalContent).parent().removeClass('modal-dialog-extra-large');
            modalContent.find(".modal-title").html("<?= $header ?>");
            // REFRESH_UNIFORMS
            refreshUniform();
            refreshDatepicker();
        }
    <?php endif ?>
    $(document).on("change", "#cashorderforeigncurrencyflows-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashorderforeigncurrencyflows-recognition_date_input');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });

    $('form [name$="[income_item_id]"], form [name$="[expenditure_item_id]"]').trigger('change');
</script>

<?php \yii\widgets\Pjax::end(); ?>
